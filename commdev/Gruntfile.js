module.exports = function(grunt){

	/**
	 * Credentials and endpoint information needed to connect to Salesforce.
	 */
	var orgOptions = {
		'clientId': '<%= SFDC_CLIENT_ID %>',
		'clientSecret': '<%= SFDC_CLIENT_SECRET %>',
		'username': '<%= SFDC_USERNAME %>',
		'password': '<%= SFDC_PASSWORD %>', //<%= env[BUILD_ENV].PASSWORD %>
		'nforceOptions': {
			'apiVersion': 'v35.0',  
			'environment': '<%= SFDC_ENV %>', 
			'url': '<%= SFDC_URL %>/services/oauth2/token'
		}
	};

	/**
	 * Initialize a configuration object for the current project. Any specified <% %> template 
	 *	strings will be processed when config data is retrieved.
	 */
	grunt.initConfig({
		'env' : {
			'sandbox' : {
				'src': 'build-deploy/env-configs/build.<%= ENV_NAME %>.ini'
			}
		},
		'skuid-pull':{
			'options': orgOptions,
			'sandbox':{
				'options':{
					'dest': '<%= SKUID_ROOT_DIR %><%= SKUID_MODULE %>/',
					'module':['<%= SKUID_MODULE %>'], //can be array or comma separated values
				}
			}  
		},
		'skuid-push':{
			'options': orgOptions,
			'sandbox':{
				'files':{
					'src': ['<%= SKUID_ROOT_DIR %><%= SKUID_MODULE %>/*'],
					// Specific file(s).
					// 'src': ['<%= SKUID_ROOT_DIR %><%= SKUID_MODULE %>/<%= SKUID_MODULE %>_JobSearch__new.*'],
					'module':['<%= SKUID_MODULE %>']
				}
			}
		}
	});

	/**
	 * Allow the target Salesforce sandbox/env to be specified via command line argument. For example, 
	 * 	grunt skuid-retrieve --envName=comdev2
	 */
	var envName = grunt.option('envName');
	if (envName === undefined || envName === null || envName === '') {
		grunt.fail.fatal('envName argument must be specified. For example, \'--envName=comdev2\'')
	} else {
		// Set the value on the config object.
		grunt.config('ENV_NAME', envName);
	}

	/**
	 * Allow the Skuid pages directory to be specified via command line argument. For example, 
	 * 	grunt skuid-retrieve --skuidRootDir=app-src-com/skuid/pages/
	 */
	var skuidRootDir = grunt.option('skuidRootDir');
	if (skuidRootDir === undefined || skuidRootDir === null || skuidRootDir === '') {
		grunt.fail.fatal('skuidRootDir argument must be specified. For example, \'--skuidRootDir=app-src-com/skuid/pages/\'')
	} else {
		// Set the value on the config object.
		grunt.config('SKUID_ROOT_DIR', skuidRootDir);
	}

	/**
	 * Allow the Skuid pages module to be specified via command line argument. For example, 
	 * 	grunt skuid-retrieve --skuidModule=Communities
	 */
	var skuidModule = grunt.option('skuidModule');
	if (skuidModule === undefined || skuidModule === null || skuidModule === '') {
		grunt.fail.fatal('skuidModule argument must be specified. For example, \'--skuidModule=Communities\'')
	} else {
		// Set the value on the config object.
		grunt.config('SKUID_MODULE', skuidModule);
	}

	/**
	 * Allow the username and password to be specified via command line arguments. Neither should be committed 
	 * to source control (in the ini file - see env.sandbox.src), but both are needed by the CI deployment. 
	 * For example: 
	 *  grunt skuid-retrieve --envName=comci --skuidModule=Communities --username=user@allegisgroup.com.comci --password=myPass
	 */
	var username = grunt.option('username');
	if (username !== undefined && username !== null || username !== '') {
		grunt.config('SFDC_USERNAME', username);
	}
	var password = grunt.option('password');
	if (password !== undefined && password !== null || password !== '') {
		grunt.config('SFDC_PASSWORD', password);
	}

	/**
	 * Load in constants, sourced from the file pointed at by env.sandbox.src.
	 */
	grunt.registerTask('loadconst', 'Load constants', function() {
		// TODO Obviate need to define each env var.
	    //grunt.config('BUILD_ENV', process.env.ENV);

	    // username, password, and serverUrl are inheritied from the pre-existing Ant properties files.
	    grunt.config('SFDC_CLIENT_ID', process.env.SFDC_CLIENT_ID);
	    grunt.config('SFDC_CLIENT_SECRET', process.env.SFDC_CLIENT_SECRET);

	    // Allow command line arguments to override the ini file values.
	    if (grunt.config('SFDC_USERNAME') === undefined) {
		    grunt.config('SFDC_USERNAME', process.env.SFDC_USERNAME);
		    grunt.config('SFDC_USERNAME', process.env.username);
	    }
	    if (grunt.config('SFDC_PASSWORD') === undefined) {
		    grunt.config('SFDC_PASSWORD', process.env.SFDC_PASSWORD);
		    grunt.config('SFDC_PASSWORD', process.env.password);
	    }
	    
	    grunt.config('SFDC_ENV', process.env.SFDC_ENV);
	    grunt.config('SFDC_URL', process.env.serverUrl);
	});

	/**
	 * Retrieve individual Skuid pages from Salesforce.
	 */
	grunt.registerTask('skuid-retrieve', [
		'env:sandbox',
    	'loadconst',
		'skuid-pull:sandbox'
	]);

	/**
	 * Deploy individual Skuid pages to Salesforce.
	 */
	grunt.registerTask('skuid-deploy', [
		'env:sandbox',
    	'loadconst',
		'skuid-push:sandbox'
	]);

	grunt.loadNpmTasks('grunt-env');
	grunt.loadNpmTasks('skuid-grunt');
}
