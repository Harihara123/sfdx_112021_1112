# ATS Connected Front Office

This is the root directory of the ATS Connected Front Office project. If you are looking for project source files, they are located in the src/ directory.

To configure the development environment, run the following commands from this directory in a terminal:
* npm install
* npm install -g typescript
* npm install -g babel-polyfill
* npm install -g webpack

To build the project, run "webpack" from this directory in a terminal. You can optionally pass the flag "--watch" with this command to continually build the project on save.

The structure of this project is as follows:

* Front-Office (the directory this file resides in)
    * Files
        * .gitignore (a list of files and folders that git should ignore)
        * package.json (project dependency configuration)
        * README.md (this file)
        * tsconfig.json (typescript configuration)
        * tsd.json (typescript definitions configuration)
        * webpack.config.js (webpack configuration)
    * Directories
        * .git (READ ONLY: used by git)
        * build-deploy
        * built (the compiled typescript files)
            * Any JS files in this directory should be considered READ ONLY
        * node_modules (READ ONLY: used by node for project dependecies)
        * src (the source files used for development)
