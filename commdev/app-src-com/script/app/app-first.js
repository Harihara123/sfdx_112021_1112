'use strict';

// Globally expose jQuery.
require('expose?$!expose?jQuery!jquery');

// The Promise object is not implemented in IE v11 and below.
require("native-promise-only");

var svg4everybody = require("svg4everybody");

var globalHandlers = require("./common/global-handlers");
var polyfills = require("./common/polyfills");
var webpackUtils = require("./common/webpack-utils");
var wiringFirst = require("./wiring-first");

/**
 * Return the name of the current file. For example, "app.js".
 */
var _calcCurrentFilename = function() {
	/*
	 * For example, app-src-com\app\app.js
	 *
	 * Made possible by the following config in webpack.config.js: 
	 * 	context: __dirname,
	 * 	node: {
	 * 	    __filename: true
	 * 	}
	 *
	 * More info - http://stackoverflow.com/questions/25553868/current-file-path-in-webpack#25565222
	 */
	var pathPlusFilename = __filename;

	// Be prepared to handle both types of path delimiters.
	var idx = pathPlusFilename.lastIndexOf('\\') + 1;
	if (idx <= 0) {
		idx = pathPlusFilename.lastIndexOf('/') + 1;
	}

	var result = pathPlusFilename.substring(idx);
	return result;
};

/**
 * This is the entry point.
 */
(function($) {

	polyfills.init();

	var currentFilename = _calcCurrentFilename();
	webpackUtils.configureWebpackPublicPath(currentFilename);

	// App-global hook for page loads.
    globalHandlers.handlePageLoaded();
    
    // Wire in the page "handlers".
    wiringFirst.wireUpPageHandlers();

	// Enable SVG support for IE v11.
	svg4everybody();

})(jQuery);

