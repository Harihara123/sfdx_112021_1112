'use strict';

/**
 * Article (i.e., help, news) helper functions.
 */
module.exports = {

	/**
	 * Register a Skuid component for displaying/rendering an article.
	 *
	 * @param componentType - Skuid component type to be registered.
	 * @param modelId - ID of the backing Skuid model for the article to be rendered.
	 * @param indexPageUrlPath - Path portion of the URL for the articles index page.
	 * @param renderArticleFunc - Callback function for rendering the article.
	 */
	registerArticleSkuidComponent: function(componentType, modelId, indexPageUrlPath, renderArticleFunc) {
	    skuid.componentType.register(componentType, function(element){

	        var model = skuid.$M(modelId),
	            row = model.getFirstRow();

	        if(row) {
	            element.html(renderArticleFunc(row));
	        } else {
	            element.html('<p><a href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}') + '/' + indexPageUrlPath +'" class="u-viewall__back">' + skuid.$L('TC_View_All') + '</a></p><p>' + skuid.$L('TC_Article_Missing') + '</p>');
	        }

	    });
	}

}
