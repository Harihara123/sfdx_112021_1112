'use strict';

var _ = require('lodash/core');
var moment = require('moment');

var assignmentStatus = require("../common/assignment-status");
var eventUtils = require('../common/event-utils');
var i18nUtils = require('../common/i18n-utils');
var modelUtils = require('../common/model-utils');
var templateUtils = require('../common/template-utils');
var uiUtils = require("../common/ui-utils");

var assignEndNoticeTemplate = require('../templates/dashboard-assignment-end-notice.hbs');

/**
 * Assignment end date notice functionality.
 */
module.exports = {

    /**
     * If/when appropriate, display a notification/warning that the user's assignment end date is 
     * approaching, allowing them to click a button for their recruiter to contact them. The display-
     * determing logic is as follows:
     * IF the context user DOES NOT have an in-flight assignment end date change request 
     *  (Contact.End_Date_Change_Requested__c)...
     * AND IF the context user HAS a valid assignment end date...
     * AND IF the current date is in the cumulative display window... 
     * AND IF the context user HAS NOT closed/dismissed the approaching assignment end date 
     *  notification/warning during the CURRENT display window (out of a possible two windows)...
     * AND IF NEITHER an assignment end date change request case or an approaching assignment end date 
     *  notice case has been created for the context user in the cumulative display window...
     * THEN DISPLAY the approaching assignment end date notification/warning.
     *
     * @param callback - Called on completion. Initially added to facilitate unit testing.
     */
    condDisplayAssignmentEndDateNotice: function(callback) {
        if (!callback) {
            callback = function() {

            };
        }

        var user = modelUtils.retrieveModelData('RunningUser');

        if (!_calcAssignEndDateChangeRequestExists(user)) {
            var assignEndMoment = assignmentStatus.calcAssignmentEndDate(user);
            if (assignEndMoment.isValid()) {
                /**
                * The current time is compared with date objects from backend, where timestamp is set to start of the day.
                * To make the comparison work properly, convert the time to start of day.
                */
                var nowMoment = moment().startOf('day');
                var windowMoments = _buildAssignEndNoticeDisplayWindowStarts(assignEndMoment);

                if (_calcCurrentlyInAssignEndNoticeDisplayWindow(nowMoment, windowMoments, assignEndMoment)) {
                    if (!_calcAssignEndNoticeClosedDuringCurrentWindow(nowMoment, windowMoments, user)) {
                        _searchForAssignEndNoticeCasesAlreadyCreatedDuringWindow(function(casesModel) {
                            if (!_calcAssignEndNoticeCaseAlreadyCreatedDuringWindow(casesModel, windowMoments)) {
                                _displayAssignmentEndDateNotice(nowMoment, assignEndMoment);
                                callback();
                            } else {
                                console.log('Assignment end notice case already created during current window.');
                                callback();
                            }
                        });
                    } else {
                        console.log('Assignment end notice already closed during current window.');
                        callback();
                    }
                } else {
                    console.log('Not in assignment end notice window.');
                    callback();
                }
            } else {
                console.log('Assignment end date is blank or invalid.');
                callback();
            }
        } else {
            console.log('Assignment end date change request currently exists.');
            callback();
        }
    }, 

    /**
     * Handle the click of the "Contact Me" button from the approaching assignment end date notification.
     *
     * @param callback - Called on completion. Initially added to facilitate unit testing.
     */
    handleOnClickAssignEndNoticeConfirm : function(callback) {
        uiUtils.showLoadingMask();

        var contactModel = modelUtils.retrieveModelDefinition('ContactForUpdate');
        /*
         * This Contact model is _not_ configured to query for data on page load. As such, load _from_ 
         * the server before saving _to_ the server.
         */
        modelUtils.fetchModelData([contactModel], function() {
            var contactObj = modelUtils.retrieveModelData(contactModel);
            modelUtils.updateModelValues(contactObj, {
                End_Date_Change_Requested__c: 'change requested'
            }, contactModel);

            // Try to save the model.
            modelUtils.saveModelData(null, null, contactModel)
            .then(function() {
                // Save successful.
                uiUtils.hideLoadingMask();
                var successMsg = i18nUtils.retrieveText("TC_approaching_end_date_notice_response");
                eventUtils.publishEvent('com.header.displayGlobalSuccess', [{ notifContent: successMsg }]);

                try {
                    //Refresh the end date card on success
                    assignmentStatus.refreshEndDateCard();
                }
                catch (err) {}

                try {
                    if (callback) {
                        callback(true);
                    }
                }
                catch (err) {}
            })
            .fail(function() {
                uiUtils.hideLoadingMask();
                eventUtils.publishEvent('com.header.displayCatchAllGlobalError');
                try {
                    if (callback) {
                        callback(false);
                    }
                }
                catch (err) {}
            });
        });
    },

    /**
     * Handle the close of the approaching assignment end date notification. We don't want to keep 
     * displaying the notification if the user has actively dismissed it.
     *
     * @param callback - Called on completion. Initially added to facilitate unit testing.
     */
    handleOnClickAssignEndNoticeClose: function(callback) {
        var accountModel = modelUtils.retrieveModelDefinition("AccountForApproachingEndDateNotice");
        /*
         * This Account model is _not_ configured to query for data on page load. As such, load _from_ 
         * the server before saving _to_ the server.
         */
        modelUtils.fetchModelData([accountModel], function() {
            var account = modelUtils.retrieveModelData(accountModel);
            var nowStr = moment().format('YYYY-MM-DD');
            //TODO use skuid-utils time conversion
            modelUtils.updateModelValues(account, {
                Assign_End_Notice_Close_Date__c: nowStr
            }, accountModel);

            /*
             * Clear the approaching assignment end date notice quickly, before the save request is 
             * even sent to the server. The user's interaction with the app is not dependent on the 
             * response.
             */
            eventUtils.publishEvent('com.header.clearGlobalWarning');
            modelUtils.saveModelData(null, null, accountModel)
            .then(function() {
                // Save successful.
                console.log('Save successful.');
                try {
                    if (callback) {
                        callback(true);
                    }
                }
                catch (err) {}
            })
            .fail(function() {
                console.log('Unable to save assignment end notice close date.');
                // Don't tell the user about any persistence errors.
                try {
                    if (callback) {
                        callback(false);
                    }
                }
                catch (err) {}
            });
        });
    }

}

/**
 * Return true if the context user currently has an outstanding assignment end date change request, 
 * else false.
 */
var _calcAssignEndDateChangeRequestExists = function(user) {
    var result = true;
    var assignEndDateChangeRequested = user.Contact.End_Date_Change_Requested__c; 
    if (assignEndDateChangeRequested === undefined || assignEndDateChangeRequested === null) {
        result = false;
    }
    return result;
};

/**
 * Search for any assignment end date-related cases (assignment end date change request 
 * case or approaching assignment end date notice case) created during the given window, 
 * passing the resultant Case(s) model to the given callback function.
 */
var _searchForAssignEndNoticeCasesAlreadyCreatedDuringWindow = function(callback) {
    var casesModel = modelUtils.retrieveModelDefinition('CasesSearchForApproachingEndDateNotice');
    // Query for the data lazily, as needed.
    modelUtils.fetchModelData([casesModel], function(data) {
        callback(data.models.CasesSearchForApproachingEndDateNotice);
    });
};

/**
 * Return true if a case (assignment end date change request case or an approaching assignment 
 * end date notice case) has been created within the cumulative display window, else false.
 */
var _calcAssignEndNoticeCaseAlreadyCreatedDuringWindow = function(casesModel, windowMoments) {
    var result = false;
    /*
     * The Case model here is configured to return one record, sorted by CreatedDate DESC. As such, 
     * we only need to inspect one record.
     */
    var caseObj = modelUtils.retrieveModelData(casesModel);
    if (caseObj !== undefined) {
        var caseCreatedDateStr = caseObj.CreatedDate;
        var caseCreatedMoment = moment(caseCreatedDateStr, 'YYYY-MM-DDTHH:mm:ss.000Z');
        if (caseCreatedMoment.isAfter(windowMoments.start1)) {
            result = true;
        }
    }
    return result;
};

/**
 * Return true if the given Moment is within the start of the given window Moment and 
 * the given assignment end date Moment, else false.
 */
var _calcCurrentlyInAssignEndNoticeDisplayWindow = function(nowMoment, windowMoments, assignEndMoment) {
    var result = true;
    if (nowMoment.isBefore(windowMoments.start1) || nowMoment.isAfter(assignEndMoment)) {
        // Now is before the first window or after the assignment end date.
        result = false;
    }
    return result;
};

/**
 * Build and return an object that contains the start Moments of the assignment end date 
 * notice display windows, based on the given assignment end Moment.
 */
var _buildAssignEndNoticeDisplayWindowStarts = function(assignEndMoment) {
    var result = {};
    var opCoMappings = modelUtils.retrieveModelData('OpCoMappings');
    var assignEndNoticeDays1 = opCoMappings.Assign_End_Notice_Days_1__c;
    var assignEndNoticeDays2 = opCoMappings.Assign_End_Notice_Days_2__c;
    if (assignEndNoticeDays1 < assignEndNoticeDays2) {
        console.log('Assignment end notice days are incorrectly configured.');
    }
    result.start1 = assignEndMoment.clone().subtract(assignEndNoticeDays1, 'days');
    result.start2 = assignEndMoment.clone().subtract(assignEndNoticeDays2, 'days');
    return result;
};

/**
 * Return true if the assignment end date notice was closed (by the user) during the current 
 * notice display window, else false.
 */
var _calcAssignEndNoticeClosedDuringCurrentWindow = function(nowMoment, windowMoments, user) {
    var result = false;

    var assignEndNoticeCloseDateStr = user.Account.Assign_End_Notice_Close_Date__c;
    if (assignEndNoticeCloseDateStr !== undefined && assignEndNoticeCloseDateStr.trim().length > 0) {
        var assignEndNoticeCloseMoment = moment(assignEndNoticeCloseDateStr, 'YYYY-MM-DD');

        // Assume we are in the first window.
        var currentWindowStartMoment = windowMoments.start1;
        if (nowMoment.isSameOrAfter(windowMoments.start2)) {
            // Correct if we are actually in the second window.
            currentWindowStartMoment = windowMoments.start2;
        }

        if (!assignEndNoticeCloseMoment.isBefore(currentWindowStartMoment)) {
            result = true;
        }
    }

    return result;
};

/**
 * If/when appropriate, display a notification/warning that the user's assignment end date is 
 * approaching, allowing them to click a button for their recruiter to contact them.
 */
var _displayAssignmentEndDateNotice = function(nowMoment, assignEndMoment) {
    var templCtx = templateUtils.buildBaseTemplateContext();
    templCtx.endDateDaysCount = assignEndMoment.diff(nowMoment, 'days');
    var assignEndNoticeMarkup = assignEndNoticeTemplate(templCtx);
    eventUtils.publishEvent('com.header.displayGlobalWarning', [{ 
        notifContent: assignEndNoticeMarkup, 
        closeEvent: 'com.dashboard.onClickAssignEndNoticeClose' 
    }]);
};

