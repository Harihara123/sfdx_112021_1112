'use strict';

var chartjs = require("chart.js");
var moment = require('moment');

var modelUtils = require('../common/model-utils');
var eventUtils = require('../common/event-utils');
var templateUtils = require('../common/template-utils');

var assignmentStatusTemplate = require('../templates/dashboard-assignment-status.hbs');

/**
 * Talent assignment status functionality.
 */
module.exports = {

	TALENT_STATUS_ACTIVE: 'active', 
	TALENT_STATUS_INACTIVE: 'inactive',
	TALENT_STATUS_STARTING: 'beginning',
	TALENT_STATUS_DATE_CHANGE_REQUESTED: 'change',
	TALENT_STATUS_UNKNOWN: 'nodata',

    TALENT_STATUS_CURRENT: 'current',
    TALENT_STATUS_FORMER: 'former', 

	/**
	 * Given a collection of TalentWorkHistory objects, build and return an object encapsulating information 
	 *	about the current/next assignment, including assignment status and the start/end date for that 
	 *	assignment.
	 *
     * @param endDateChangeRequested - Date (string) of when the user requested a change to their assignment 
     *  end date.
	 * @param openApproachingEndDateNoticeCase - Any currently-open case created when the user clicked the 
	 *	"Contact Me" button from the approaching assignment end date notification. 
	 */
    buildTalentAssignmentStatus: function(talentWorkHistoryModels, endDateChangeRequested, openApproachingEndDateNoticeCase) {
        var result = {};

        // first check if assignment end date has been requested (string)
        if (endDateChangeRequested)  {

            result = {
                'status': this.TALENT_STATUS_DATE_CHANGE_REQUESTED
            }

        } else if (openApproachingEndDateNoticeCase) {
            // If an approaching end date notice case is currently open, set the status to "change".
            result = {
                'status': this.TALENT_STATUS_DATE_CHANGE_REQUESTED
            }
        } else {
            if (talentWorkHistoryModels !== null && typeof talentWorkHistoryModels !== 'undefined') {
                var statusToWorkHistoryMap = _mapWorkHistoryToStatuses(talentWorkHistoryModels);
                result = _buildStatusBasedOnWorkHistory(statusToWorkHistoryMap);
            }

        }

        return result;
    }, 

    /**
     * Display the talent assignment status card, appending it in the DOM to the given DOM element.
     *
     * @param $rootEle - jQuery-wrapped DOM element.
     * @param talentAssignmentStatus - Object encapsulating information about the current/next 
     *	assignment, including assignment status and the start/end date for that assignment.
     */
	displayAssignmentStatus: function($rootEle, talentAssignmentStatus) {
        if (talentAssignmentStatus === null || typeof talentAssignmentStatus === 'undefined') {
            talentAssignmentStatus = {};
        }

	    var cardMarkup = _buildMarkupForAssignmentStatusCard(talentAssignmentStatus);
	    $rootEle.append(cardMarkup);

	    var $chartRootEle = $rootEle.find('#current-assignment');
	    _displayChartForAssignmentStatus($chartRootEle, talentAssignmentStatus);
	},

	/**
	 * Handle the action of a user requesting a change to their assignment end date.
	 */
	handleRequestEndDateChange: function() {
	    $('.c-current-assignment__request-enddate').empty().append('<p>Requesting...</p>');
 
        //Hide pop up banner, if currently showing
        eventUtils.publishEvent('com.header.clearGlobalWarning', null)

	    var contactModel = modelUtils.retrieveModelDefinition('ContactForUpdate');
	    /*
	     * This Contact model is _not_ configured to query for data on page load. As such, load _from_ 
	     * the server before saving _to_ the server.
	     */
	    modelUtils.fetchModelData([contactModel], function() {
	    	var contact = modelUtils.retrieveModelData(contactModel);
	    	modelUtils.updateModelValues(contact, {
	            End_Date_Change_Requested__c: 'change requested'
	        }, contactModel);

	    	modelUtils.saveModelData(contact, {
	        	callback: function(result){
		            if (result.totalsuccess) {
					    var templCtx = templateUtils.buildBaseTemplateContext();
					    templCtx.assignStatusChange = true;
					    var cardMarkup = assignmentStatusTemplate(templCtx);
		                $('.c-current-assignment').replaceWith(cardMarkup);
		            } else {
		                $('.c-current-assignment__request-enddate').empty().append(
		                	'<p>' + skuid.$L('TC_End_date_change_request_failed_Please_contact_the_community_manager') + '</p>');
		            }
		        }
	        }, contactModel);
	    });
	}, 

    refreshEndDateCard: function() {
        var templCtx = templateUtils.buildBaseTemplateContext();
        templCtx.assignStatusChange = true;

        var cardMarkup = assignmentStatusTemplate(templCtx);
        $('.c-current-assignment').replaceWith(cardMarkup);
    },

    /**
     * Calculate the context user's current assignment end date.
     *
     * @param user - User information object, which is assumed to contain an Account property, which is 
     *  assumed to contain an Talent_End_Date__c property.
     * @return the context user's assignment end date, as a Moment.
     */
    calcAssignmentEndDate: function(user) {
        var result = null;
        var assignEndDateStr = user.Account.Talent_End_Date__c;
        result = moment(assignEndDateStr, 'YYYY-MM-DD');
        return result;
    }, 

    /**
     * Calculate the context user's status, as it relates to their current assignment.
     *
     * @param user - User information object, which is assumed to contain an Account property, which is 
     *  assumed to contain an Talent_End_Date__c property.
     * @return TALENT_STATUS_CURRENT, TALENT_STATUS_FORMER, or null.
     */
    calcTalentStatus: function(user) {
        var result = null;
        var assignEndMoment = this.calcAssignmentEndDate(user);
        if (assignEndMoment !== null && assignEndMoment.isValid()) {
            var nowMoment = moment();
            if (nowMoment.isSameOrBefore(assignEndMoment, 'day')) {
                result = this.TALENT_STATUS_CURRENT;
            } else {
                result = this.TALENT_STATUS_FORMER;
            }
        }
        return result;
    }, 

    /**
     * Return true if the context user was formerly on-assignment, but no longer is; else false.
     *
     * @param user - User information object, which is assumed to contain an Account property, which is 
     *  assumed to contain an Talent_End_Date__c property.
     */
    calcTalentIsFormer: function(user) {
        var result = false;
        var talentStatus = this.calcTalentStatus(user);
        if (talentStatus === this.TALENT_STATUS_FORMER) {
            result = true;
        }
        return result;
    }, 

    /**
     * Return true if the context user was formerly on-assignment, but no longer is, and that
     * assignment ended more than 10 days ago; else false.
     *
     * @param user - User information object, which is assumed to contain an Account property, which is 
     *  assumed to contain an Talent_End_Date__c property.
     */
    calcTalentIsFormerNotRecent: function(user) {
        var result = false;
        var isTalentFormer = this.calcTalentIsFormer(user);
        if (isTalentFormer === true) {
            var assignEndMoment = this.calcAssignmentEndDate(user);
            if (assignEndMoment !== null && assignEndMoment.isValid()) {
                var assignEndPlusTenDaysMoment = assignEndMoment.add(10, 'days');
                var nowMoment = moment();
                if (nowMoment.isAfter(assignEndPlusTenDaysMoment, 'day')) {
                    result = true;
                }
            }
        }
        return result;
    }
};

var _buildStatusBasedOnWorkHistory = function(statusToWorkHistoryMap) {
    var result = {};

    var moduleScope = module.exports;
    if (statusToWorkHistoryMap[moduleScope.TALENT_STATUS_ACTIVE].length > 0) {

        $.each(statusToWorkHistoryMap[moduleScope.TALENT_STATUS_ACTIVE], function(k,v){
            if ($.isEmptyObject(result)){
                result = {
                    'startDate': v.Start_Date__c,
                    'endDate': v.End_Date__c,
                    'status': moduleScope.TALENT_STATUS_ACTIVE
                }
            } else if (v.End_Date__c > result.endDate){
                result = {
                    'startDate': v.Start_Date__c,
                    'endDate': v.End_Date__c,
                    'status': moduleScope.TALENT_STATUS_ACTIVE
                }
            }
        });

    } else if (statusToWorkHistoryMap[moduleScope.TALENT_STATUS_STARTING].length > 0) {

        $.each(statusToWorkHistoryMap[moduleScope.TALENT_STATUS_STARTING], function(k,v){
            if ($.isEmptyObject(result)){
                result = {
                    'startDate': v.Start_Date__c,
                    'endDate': v.End_Date__c,
                    'status': moduleScope.TALENT_STATUS_STARTING
                }
            } else if (v.Start_Date__c < result.startDate){
                result = {
                    'startDate': v.Start_Date__c,
                    'endDate': v.End_Date__c,
                    'status': moduleScope.TALENT_STATUS_STARTING
                }
            }
        });

    } else if (statusToWorkHistoryMap[moduleScope.TALENT_STATUS_UNKNOWN].length > 0) {

        result = {
            'startDate': '',
            'endDate': '',
            'status': moduleScope.TALENT_STATUS_UNKNOWN
        }

    } else if (statusToWorkHistoryMap[moduleScope.TALENT_STATUS_INACTIVE].length > 0) {

        $.each(statusToWorkHistoryMap[moduleScope.TALENT_STATUS_INACTIVE], function(k,v){
            if ($.isEmptyObject(result)){
                result = {
                    'startDate': v.Start_Date__c,
                    'endDate': v.End_Date__c,
                    'status': moduleScope.TALENT_STATUS_INACTIVE
                }
            } else if (v.Start_Date__c > result.startDate){
                result = {
                    'startDate': v.Start_Date__c,
                    'endDate': v.End_Date__c,
                    'status': moduleScope.TALENT_STATUS_INACTIVE
                }
            }
        });

    }

    return result;
};

/**
 * Map each item in the given array of talentWorkHistoryModels to an assignment status, returning 
 *  the resultant map.
 */
var _mapWorkHistoryToStatuses = function(talentWorkHistoryModels) {
    var statusToWorkHistoryMap = {};

    var moduleScope = module.exports;
    statusToWorkHistoryMap[moduleScope.TALENT_STATUS_ACTIVE] = [];
    statusToWorkHistoryMap[moduleScope.TALENT_STATUS_INACTIVE] = [];
    statusToWorkHistoryMap[moduleScope.TALENT_STATUS_STARTING] = [];
    statusToWorkHistoryMap[moduleScope.TALENT_STATUS_DATE_CHANGE_REQUESTED] = [];
    statusToWorkHistoryMap[moduleScope.TALENT_STATUS_UNKNOWN] = [];

    $.each(talentWorkHistoryModels, function(i, v){
        if (v.Start_Date__c === null || typeof v.Start_Date__c === 'undefined' || 
            v.End_Date__c === null || typeof v.End_Date__c === 'undefined') 
        {
            statusToWorkHistoryMap[moduleScope.TALENT_STATUS_UNKNOWN].push(v);
        } else if (moment().isBetween(v.Start_Date__c, v.End_Date__c, 'days', '[]')) {
            statusToWorkHistoryMap[moduleScope.TALENT_STATUS_ACTIVE].push(v);
        } else if (moment().isBefore(v.Start_Date__c, 'days')) {
            statusToWorkHistoryMap[moduleScope.TALENT_STATUS_STARTING].push(v);
        } else if (moment().isAfter(v.End_Date__c, 'days')) {
            statusToWorkHistoryMap[moduleScope.TALENT_STATUS_INACTIVE].push(v);
        } else {
            statusToWorkHistoryMap[moduleScope.TALENT_STATUS_UNKNOWN].push(v);
        }
    });

    return statusToWorkHistoryMap;
};

/**
 * Build the markup for the talent assignment status card.
 */ 
var _buildMarkupForAssignmentStatusCard = function(talentAssignmentStatus) {
    var moduleScope = module.exports;

    var templCtx = templateUtils.buildBaseTemplateContext();
    var talentStatus = talentAssignmentStatus.status;
    if (talentAssignmentStatus.status === null || typeof talentAssignmentStatus.status === 'undefined') {
    	talentStatus = moduleScope.TALENT_STATUS_INACTIVE;
    }
    var ctxKey = 'assignStatus' + talentStatus.charAt(0).toUpperCase() + talentStatus.slice(1);
    templCtx[ctxKey] = true;

    var today = Date.now();
    switch(talentAssignmentStatus.status) {
        case moduleScope.TALENT_STATUS_ACTIVE:
            var daysRemaining = _calcDaysRemaining(talentAssignmentStatus.endDate);
        	var endDateFormatted = moment(talentAssignmentStatus.endDate).format("MMMM DD, YYYY");
        	templCtx.endDateFormatted = endDateFormatted;
        	templCtx.daysRemaining = daysRemaining;
            break;

        case moduleScope.TALENT_STATUS_STARTING:
	        var startMoment = moment(talentAssignmentStatus.startDate);
	        var startDateFormatted = startMoment.format("MMMM DD, YYYY");
	        var daysToBegin = (startMoment.diff(today, "days") + 1);
	        templCtx.startDateFormatted = startDateFormatted;
	        templCtx.daysToBegin = daysToBegin;
            break;
    }

    var cardMarkup = assignmentStatusTemplate(templCtx);

    return cardMarkup;
};

/**
 * Display the chart portion of the talent assignment status card, appending it in the DOM to 
 *	the given DOM element.
 *
 * @param $chartRootEle - jQuery-wrapped DOM element.
 */
var _displayChartForAssignmentStatus = function($chartRootEle, talentAssignmentStatus) {
    var moduleScope = module.exports;

    // Only display the chart if the user has an active assignment.
    if (talentAssignmentStatus.status === moduleScope.TALENT_STATUS_ACTIVE) {
	    var startMoment = moment(talentAssignmentStatus.startDate);
	    var endMoment = moment(talentAssignmentStatus.endDate);
	    var daysInAssignment = endMoment.diff(startMoment, "days");
	    var daysRemaining = _calcDaysRemaining(talentAssignmentStatus.endDate);

	    var chartData = {
	        labels: [],
	        datasets: [{
	            data: [daysInAssignment-daysRemaining, daysRemaining],
	            backgroundColor: ["#415365","rgba(0,0,0,.3)"],
	            hoverBackgroundColor: ["#415365","rgba(0,0,0,.3)"]
	        }]
	    };

	    _initializeChart($chartRootEle, chartData);
    }
};

/**
 * Given the end date of the talent's assignment, calculate and return the days remaining in the assignment.
 */
var _calcDaysRemaining = function(assignmentEndDate) {
    var result = -1;
    var today = Date.now();
    var endMoment = moment(assignmentEndDate);

    /*
     * By default, Moment.diff() rounds down to zero. So, if the current moment was 8am on 12/14 and the 
     *  assignmentEndDate was (essentially) 12:01am on 12/15, the difference would be less than 1 day, so 
     *  Moment would return 0. However, in this situation, where the days are actually different, we want 
     *  the function's return value to be 1 day remaining. So, we round up and then check to see if the 
     *  days are actually the same, compensating if so.
     */
    var daysRemaining = (endMoment.diff(today, "days")) + 1;
    if (moment(today).isSame(endMoment, 'day')) {
        daysRemaining = 0;
    }

    result = daysRemaining;
    return result;
};

/**
 * Initialize the chart, which is used to visualize the remaining duration of the user's current assignment.
 */
var _initializeChart = function(chartCtx, chartData) {
    var options = {
        scaleShowLabels : false, 
        cutoutPercentage: 80, 
        legend: {
        	display: false
        }, 
        tooltips: {
        	enabled: false
        }, 
        elements: {
        	arc: {
        		borderWidth: 0
        	}
        }
    };

    /*
     * If Chart.js has not yet been loaded, we'll get a reference error. Do not allow this to torpedo the 
     * entire page.
     */
    try {
        new chartjs(chartCtx, {
            type: 'doughnut',
            data: chartData,
            options: options,
            animation:{
                animateScale: true
            }
        });
    } catch(error) {
        console.error("Unable to load the assignment status chart.");
        console.error(error);
    }
};
