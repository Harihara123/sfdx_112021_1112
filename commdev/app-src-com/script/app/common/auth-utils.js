'use strict';

/**
 * Authentication-related helper functions.
 */
module.exports = {

	/**
	 * Handle any click events on the terms & conditions checkbox.
	 */
	handleOnClickTermsCheckbox: function() {
	    //console.log('in handleOnClickTermsCheckbox');

	    var $termsCheckbox = $('.js-t-and-c-input');
	    if ($termsCheckbox.prop('checked')) {
	        _termsAccepted = true;
	    } else {
	        _termsAccepted = false;
	    }

	    // Enable/disable the submit button, as appropriate.
	    _enableSubmitButtonIfRulesMet();
	}, 

	/**
	 * Handle any change (input) events on the primary password form input.
	 */
	handleOnChangePasswordInput: function() {
	    //console.log('in handleOnChangePasswordInput');
	    var moduleScope = this;

	    var password1Value = $('.js-password1-input').val();
	    var rule1Met = _communicatePasswordRuleCompliance(function() {
	        return moduleScope.verifyStringContainsAtLeastEightLetters(password1Value);
	    }, 'characters');

	    var rule2Met = _communicatePasswordRuleCompliance(function() {
	        return moduleScope.verifyStringContainsAtLeastOneCharacter(password1Value);
	    }, 'letters');

	    var rule3Met = _communicatePasswordRuleCompliance(function() {
	        return moduleScope.verifyStringContainsAtLeastOneDigit(password1Value);
	    }, 'numbers');

	    var rule4Met = _communicatePasswordRuleCompliance(function() {
	        return moduleScope.verifyStringContainsAtLeastOneSpecialCharacter(password1Value);
	    }, 'special-characters');

	    if (rule1Met && rule2Met && rule3Met && rule4Met) {
	        _passwordRulesMet = true;
	    } else {
	        _passwordRulesMet = false;
	    }

	    var password2Value = $('.js-password2-input').val();
	    if (password2Value.length > 0) {
	        _passwordsMatch = _communicatePasswordsMismatch(password1Value, password2Value);
	    }

	    // Enable/disable the submit button, as appropriate.
	    _enableSubmitButtonIfRulesMet();
	},
	
	handleOnChangePasswordInputJobSeeker: function() {
	    //console.log('in handleOnChangePasswordInput');
	    var moduleScope = this;

	    var password1Value = $('.js-password1-input').val();
	    var rule1Met = _communicatePasswordRuleCompliance(function() {
	        return moduleScope.verifyStringContainsAtLeastEightLetters(password1Value);
	    }, 'characters');

	    var rule2Met = _communicatePasswordRuleCompliance(function() {
	        return moduleScope.verifyStringContainsAtLeastOneCharacter(password1Value);
	    }, 'letters');

	    var rule3Met = _communicatePasswordRuleCompliance(function() {
	        return moduleScope.verifyStringContainsAtLeastOneDigit(password1Value);
	    }, 'numbers');

	    
	    if (rule1Met && rule2Met && rule3Met) {
	        _passwordRulesMet = true;
	    } else {
	        _passwordRulesMet = false;
	    }

	    var password2Value = $('.js-password2-input').val();
	    if (password2Value.length > 0) {
	        _passwordsMatch = _communicatePasswordsMismatch(password1Value, password2Value);
	    }

	    // Enable/disable the submit button, as appropriate.
	    _enableSubmitButtonIfRulesMetJobSeeker();
	},
	/**
	 * Handle any change (input) events on the password verification form input.
	 */
	handleOnChangePasswordConfirmInput: function() {
	    //console.log('in handleOnChangePasswordConfirmInput');

	    var password1Value = $('.js-password1-input').val();
	    var password2Value = $('.js-password2-input').val();
	    _passwordsMatch = _communicatePasswordsMismatch(password1Value, password2Value);

	    // Enable/disable the submit button, as appropriate.
	    _enableSubmitButtonIfRulesMet();
	}, 

	/**
	 * Handle any change (input) events on the old/current password verification form input.
	 */
	handleOnChangePasswordOldInput: function() {
	    //console.log('in handleOnChangePasswordOldInput');

	    var passwordOldValue = $('.js-password-old-input').val();
	    if (passwordOldValue.length > 0) {
	    	_passwordOldProvided = true;
	    } else {
	    	_passwordOldProvided = false;
	    }

	    // Enable/disable the submit button, as appropriate.
	    _enableSubmitButtonIfRulesMet();
	}, 

	/**
	 * Handle any click events on the "reveal password" input/widget.
	 */
	handleOnClickPasswordRevealInput: function() {
	    //console.log('in handleOnClickPasswordRevealInput');

	    var $revealCheckbox = $('.js-passwords-reveal-input');
	    if ($revealCheckbox.prop('checked')) {
	    	/*
	    	 * Convert the password inputs to text inputs, thereby revealing their contents. 
	    	 * Account for an "existing password" input, if present.
	    	 */
	        $('.js-password1-input').attr('type', 'text');
	        $('.js-password2-input').attr('type', 'text');
	        $('.js-password-old-input').attr('type', 'text');
	    } else {
	    	/*
	    	 * Convert the text inputs to password inputs, thereby masking their contents. 
	    	 * Account for an "existing password" input, if present.
	    	 */
	        $('.js-password1-input').attr('type', 'password');
	        $('.js-password2-input').attr('type', 'password');
	        $('.js-password-old-input').attr('type', 'password');
	    }
	}, 

	/**
	 * Reset checkbox and textbox values on button click
	 */
	handleFormSubmitResetPasswordType: function() {
	    $('.js-passwords-reveal-input').checked = false;
		$('.js-password1-input').attr('type', 'password');
		$('.js-password2-input').attr('type', 'password');
		$('.js-password-old-input').attr('type', 'password');
	}, 

	/**
	 * Configure the "change" listeners on the password form inputs, converting them from 
	 * change listeners to input listeners.
	 *
     * Salesforce Visualforce pages doesn't recognize "oninput" as a valid attribute. We want to 
     * use "oninput" instead of "onchange" to listen for changes as they are made, not wait until 
     * the context input loses focus. More info: 
     *  http://stackoverflow.com/questions/574941/best-way-to-track-onchange-as-you-type-in-input-type-text
	 */
	configChangeListenersOnInputs: function(pageSlug) {
	    /*
	     * Even though we remove the "change" listeners here, we want them in the markup to give future 
	     * developers a clear indication that listeners are in play.
	     */
	    $('.js-password1-input').on('input', function() {
	        pubSub.publish('com.' + pageSlug + '.onChangePasswordInput');
	    });
	    $('.js-password1-input').off('change');

	    $('.js-password2-input').on('input', function() {
	        pubSub.publish('com.' + pageSlug + '.onChangePasswordConfirmInput');
	    });
	    $('.js-password2-input').off('change');

	    $('.js-password-old-input').on('input', function() {
	        pubSub.publish('com.' + pageSlug + '.onChangePasswordOldInput');
	    });
	    $('.js-password-old-input').off('change');
	}, 

	configFormInputElements: function() {
		if (isUpdateProfile) {
			_configChangePasswordSubmitButton();
		}
		

	    // Invoke the related handler to resolve any resultant impacts.
	    var callback = this.handleOnClickTermsCheckbox;
	    _configTermsAndConditionsCheckbox(callback);

	    var $passwordOldInput = $('.js-password-old-input');
	    console.log('$passwordOldInput.length = ' + $passwordOldInput.length);
	    /*
	     * If there is no old/current password input present on the page, set the 
	     * "old password provided" state to true. The old/current password input is only displayed 
	     * during a password expired scenario (as opposed to a forgot password scenario).
	     */
	    if ($passwordOldInput.length <= 0) {
	    	_passwordOldProvided = true;
	    }
	},

	verifyStringContainsAtLeastEightLetters: function(stringToTest) {
		var result = false;
		if (stringToTest !== null && typeof stringToTest !== 'undefined') {
			result = stringToTest.length >= 8;
		}
		return result;
	}, 

	verifyStringContainsAtLeastOneCharacter: function(stringToTest) {
		var result = false;
		if (stringToTest !== null && typeof stringToTest !== 'undefined' && stringToTest.length > 0) {
		    // [A-z] will count '_' and '\' as letters.
		    result = stringToTest.match(/(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z)/i);
		}
		return result;
	}, 

	verifyStringContainsAtLeastOneDigit: function(stringToTest) {
		var result = false;
		if (stringToTest !== null && typeof stringToTest !== 'undefined' && stringToTest.length > 0) {
		    result = stringToTest.match(/\d/);
		}
		return result;
	}, 

	verifyStringContainsAtLeastOneSpecialCharacter: function(stringToTest) {
		var result = false;
		if (stringToTest !== null && typeof stringToTest !== 'undefined' && stringToTest.length > 0) {
		    if (!stringToTest.match(/(\%|\+|\&)/)) {
		    	result = stringToTest.match(/(\$|\!|#|-|=|_|<|>|\?|\(|\)|\{|\}|\||\~|\.|,|\'|\"|\^|\[|\]|\\|\@|\;|\:|\/|\*|\`|\‘|\’)/);
		    }
		}
		return result;
	},

	isPasswordRulesMet: function() {
		return _passwordRulesMet;
	},

	isPasswordsMatch: function() {
		return _passwordsMatch;
	},

	isTermsAccepted: function() {
		return _termsAccepted;
	},

	isPasswordOldProvided: function() {
		return _passwordOldProvided;
	}

}

/**
 * If the individual password rules are met, true; else false.
 */
var _passwordRulesMet = false;

/**
 * If the password and password confirmation values match, true; else false.
 */
var _passwordsMatch = false;

/**
 * If the terms & conditions have been accepted, true; else false.
 */
var _termsAccepted = false;

/**
 * If the old/current password verification has been provided, true; else false.
 */
var _passwordOldProvided = false;

var _enableSubmitButtonIfRulesMet = function() {
    var $submitButton = $('.js-submit-button');
	var $revealCkeckBox = $('.js-passwords-reveal-input');
    if (_passwordRulesMet && _passwordsMatch && _termsAccepted && _passwordOldProvided) {
        $submitButton.prop('disabled', false);
		$revealCkeckBox.prop('Checked', false);
    } else {
        $submitButton.prop('disabled', true);
		$revealCkeckBox.prop('Checked', false);
		
    }
};

var _enableSubmitButtonIfRulesMetJobSeeker = function() {
    var $submitButton = $('.js-submit-button');
	var $revealCkeckBox = $('.js-passwords-reveal-input');
    if (_passwordRulesMet && _passwordsMatch && _passwordOldProvided) {
        $submitButton.prop('disabled', false);
		$revealCkeckBox.prop('Checked', false);
    } else {
        $submitButton.prop('disabled', true);
		$revealCkeckBox.prop('Checked', false);
		
    }
};

/**
 * Communicate to the user whether or not the specified password rule has been met, 
 * as determined by executing the given evaluation function.
 */
var _communicatePasswordRuleCompliance = function(evalFunc, ruleSlug) {
    var result = false;
    if (evalFunc()) {
        $('.js-password-rule-' + ruleSlug + '-fail-box').css('display', 'none');
        $('.js-password-rule-' + ruleSlug + '-pass-box').css('display', 'inline');
        result = true;
    } else {
        $('.js-password-rule-' + ruleSlug + '-fail-box').css('display', 'inline');
        $('.js-password-rule-' + ruleSlug + '-pass-box').css('display', 'none');
    }
    return result;
};

var _communicatePasswordsMismatch = function(password1, password2) {
    var result = false;
    if (password1 !== password2) {
        $('.js-password-inputs-match-pass-box').css('display', 'none');
        $('.js-password-inputs-match-fail-box').css('display', 'inline');

        /*
         * Allow the password input fields to be outlined in red or green, depending on whether 
         * or not the values match.
         */
        $('.js-password1-input').removeClass('input-state__success-green js-password-inputs-match-pass');
        $('.js-password2-input').removeClass('input-state__success-green js-password-inputs-match-pass');
        $('.js-password1-input').addClass('input-state__fail-red js-password-inputs-match-fail');
        $('.js-password2-input').addClass('input-state__fail-red js-password-inputs-match-fail');
    } else {
        $('.js-password-inputs-match-fail-box').css('display', 'none');
        $('.js-password-inputs-match-pass-box').css('display', 'inline');

        /*
         * Allow the password input fields to be outlined in red or green, depending on whether 
         * or not the values match.
         */
        $('.js-password1-input').removeClass('input-state__fail-red js-password-inputs-match-fail');
        $('.js-password2-input').removeClass('input-state__fail-red js-password-inputs-match-fail');
        $('.js-password1-input').addClass('input-state__success-green js-password-inputs-match-pass');
        $('.js-password2-input').addClass('input-state__success-green js-password-inputs-match-pass');

        result = true;
    }
    return result;
};

/**
 * Configure the submit button so that it starts out as disabled, only becoming enabled 
 * once everything necessary has been supplied by the user.
 *
 * We need to do this using JavaScript because setting the "disabled" attribute on a 
 * Visualforce apex:commandButton to "true" prevents the target Apex class method from 
 * being invoked.
 */
var _configChangePasswordSubmitButton = function() {
	// Try to set the styling in a way to reduce flicker/churn.
	$('.js-submit-button').prop('disabled', true);
	$('.js-submit-button').css('visibility', 'visible');
};

/** 
 * Prepare the terms & conditions checkbox to reflect the current state of acceptance.
 */
var _configTermsAndConditionsCheckbox = function(callback) {
    var $termsCheckbox = $('.js-t-and-c-input');
    console.log('$termsCheckbox.length = ' + $termsCheckbox.length);

    /*
     * If there is no checkbox present on the page, set the "terms accepted" state to 
     * true. The terms checkbox is not displayed during a standard password change 
     * scenario (as opposed to a first-time-login create password scenario).
     */
    if ($termsCheckbox.length <= 0) {
    	_termsAccepted = true;
    } else {
    	/*
    	 * Salesforce Visualforce won't allow for dynamically setting the "selected" attribute, like so:
    	 *	<apex:inputCheckbox selected="{!NOT(termsAndConditionsAccepted)}" />
    	 * However, you _can_ dynamically set the "rendered" attribute. As such, we use an invisible 
    	 * element as a proxy to indicate the intended "selected" state.
    	 */
	    var $termsCheckboxControl = $('.js-t-and-c-input-control');
	    console.log('$termsCheckboxControl.length = ' + $termsCheckboxControl.length);

	    if ($termsCheckboxControl.length > 0) {
	        $termsCheckbox.prop('checked', true);
	    } else {
	        $termsCheckbox.prop('checked', false);
	    }

	    callback();
    }
};

var isUpdateProfile = function() {
	var url = window.location.href;
	if (url.includes("updateemail")) {
		return false;
	}else{
		return true;
	}
}
	
