'use strict';

/*
 * Make reference to the "externals"-defined jQuery - in the WebPack config. This isn't necessary for normal 
 * runtime resolution. However, it is necessary to facilitate unit testing. More info: 
 *  http://stackoverflow.com/questions/22530254/webpack-and-external-libraries#22619421
 */
//var $ = require('jquery');

/**
 * Browser-related functions.
 */
module.exports = {

	/**
	 * Perform an asynchronous HTTP (Ajax) request. Originally added to facilitate unit testing.
	 */
	makeAjaxRequest: function(settingsObj) {
		$.ajax(settingsObj);
	}, 

	/**
	 * Set the current browser HREF (URL). Originally added to facilitate unit testing.
	 *
	 * @param pathStr - For example, "https://allegisbenefits.employeediscounts.co/perks/process/login?token=6t4UP4eoomoSveq3Bosm7j7XnP28AC4c"
	 */
	populateBrowserHref: function(hrefStr) {
		window.location.href = hrefStr;
	}, 

	/**
	 * Set the current browser URL path. Originally added to facilitate unit testing.
	 *
	 * @param pathStr - For example, "/teksystems/tc_login"
	 */
	populateBrowserPath: function(pathStr) {
		window.location.pathname = pathStr;
	}, 

	/**
	 * Return the current browser host. Initially added to facilitate unit testing.
	 */
	retrieveBrowserHost: function() {
		return window.location.host;
	}, 

	/**
	 * Return the current browser protocol. Initially added to facilitate unit testing.
	 */
	retrieveBrowserProtocol: function() {
		return window.location.protocol;
	}, 

	/**
	 * Return the current browser "search" (querystring). Initially added to facilitate unit testing.
	 */
	retrieveBrowserSearch: function() {
		return window.location.search;
	}, 

	/**
	 * Return the browser-native localStorage instance. Initially added to facilitate 
	 * unit testing.
	 */
	retrieveLocalStorage: function() {
		return localStorage;
	}

}
