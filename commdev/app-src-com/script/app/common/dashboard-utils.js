'use strict';

var assignmentStatus = require("../common/assignment-status");
var eventUtils = require('../common/event-utils');
var jobsTeaser = require("../common/jobs-teaser");
var modelUtils = require('../common/model-utils');
var templateUtils = require('../common/template-utils');

var heroBarTemplate = require('../templates/dashboard-hero-bar.hbs');
var assignmentStatusAndJobsTeaserTemplate = require('../templates/dashboard-assign-status-jobs-teaser.hbs');
var jobsTeaserOnlyTemplate = require('../templates/dashboard-jobs-teaser-only.hbs');

/**
 * Dashboard helper functions.
 */
module.exports = {

    /**
     * Display the dashboard's hero bar (useful links).
     */
    displayHeroBar: function() {
        var templCtx = templateUtils.buildBaseTemplateContext();

        // TODO Centralize if required in more templates. (Beware of circular dependencies.)
        /*
         * RunningUser is defined in the header Skuid page, and the header is always displayed on the 
         * dashboard, so should always be defined.
         */
        var user = modelUtils.retrieveModelData('RunningUser');
        var isTalentFormer = assignmentStatus.calcTalentIsFormer(user);
        templCtx.isTalentFormer = isTalentFormer;
        var isTalentFormerNotRecent = assignmentStatus.calcTalentIsFormerNotRecent(user);
        templCtx.isTalentFormerNotRecent = isTalentFormerNotRecent;

        $("#dashboard-hero-bar").append(heroBarTemplate(templCtx));
    },

    /**
     * Display one or both of the assignment status and jobs teaser cards.
     *
     * @param jobSearchDaysCount - The maximum age (in days) of any jobs included in the search results 
     *  displayed by the jobs teaser card.
     */
    displayAssignmentStatusAndJobsTeaser: function(jobSearchDaysCount) {
        var assignments = modelUtils.retrieveCollectionData('TalentWorkHistory');

        var user = modelUtils.retrieveModelData('RunningUser');
        var endDateChangeRequested = user.Contact.End_Date_Change_Requested__c;

        /*
         * Currently-open case(s) created when the user clicked the "Contact Me" button from the approaching 
         * assignment end date notification. 
         */
        var openApproachingEndDateNoticeCase = modelUtils.retrieveModelData('OpenApproachingEndDateNoticeCase');

        /*
         * Retrieve the current talent status, plus any start and end dates for the most recent/next 
         * assignment.
         */
        var talentAssignmentStatus = assignmentStatus.buildTalentAssignmentStatus(assignments, endDateChangeRequested, openApproachingEndDateNoticeCase);

        var isTalentFormer = assignmentStatus.calcTalentIsFormer(user);

        // Prepare the DOM to display the card(s).
        var $rootEle = $('.js-assignment-status-and-jobs-teaser');        
        if (isTalentFormer) {
            /*
             * If the talent is a FORMER, force the assignment status to reflect that, even if they have requested 
             * and end date review.
             */
            talentAssignmentStatus.status = assignmentStatus.TALENT_STATUS_INACTIVE;

            var rootMarkup = jobsTeaserOnlyTemplate();
            $rootEle.append(rootMarkup);
        } else {
            var rootMarkup = assignmentStatusAndJobsTeaserTemplate();
            $rootEle.append(rootMarkup);

            // The assignment status card is only displayed to currents.
            eventUtils.subscribeToEvent('com.dashboard.onClickRequestEndDateChange', assignmentStatus.handleRequestEndDateChange);
            var $assignStatusRootEle = $rootEle.find('#assignment-status-box');
            assignmentStatus.displayAssignmentStatus($assignStatusRootEle, talentAssignmentStatus);
        }

        // The jobs teaser card is displayed for all users.
        var $jobsTeaserRootEle = $rootEle.find('#jobs-teaser-box');
        jobsTeaser.displayJobsTeaser($jobsTeaserRootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
            eventUtils.publishEvent('com.dashboard.resizeCards');
        });
    }
}
