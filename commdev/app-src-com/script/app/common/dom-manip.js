'use strict';

/* 
 * Need the compiler, which is not included in the runtime. More info: 
 *	https://github.com/wycats/handlebars.js/issues/953
 */
var Handlebars = require('handlebars/dist/handlebars');

/**
 * DOM-manipulation functions.
 */
module.exports = {

	/**
	 * Replace Handlebars-compliant placeholders in DOM-resident text with dynamically-determined values. 
	 * For example, turn "Hi, {{user}}." into "Hi, Joe."
	 * @param eleId - DOM-compliant ID of the DOM element whose containing text is to be manipulated.
	 * @param mergeMap - Object mapping placeholder keys to replacement values. For example, 
	 *	{'user': 'Joe'}
	 * @return the DOM element identified by the given DOM-compliant ID.
	 */
	replaceMergeFields: function(eleId, mergeMap) {
		var domEle = document.getElementById(eleId);
		var textNodes = this.retrieveTextNodesIn(domEle, false);
		var limit = textNodes.length;
		for (var i = 0; i < limit; i++) {
			var textNode = textNodes[i];
			var containingText = textNode.nodeValue;
			var template = Handlebars.compile(containingText);
			var mergedText = template(mergeMap);
			textNode.nodeValue = mergedText;
		}

		return domEle;
	}, 

	/**
	 * Retrieve the text node children of the given node. Sourced from 
	 *	http://stackoverflow.com/questions/298750/how-do-i-select-text-nodes-with-jquery#4399718
	 *
	 * @param includeWhitespaceNodes - True if whitespace text nodes should be included, 
	 * 	false otherwise.
	 */
	retrieveTextNodesIn: function(node, includeWhitespaceNodes) {
	    var textNodes = [], nonWhitespaceMatcher = /\S/;

	    function getTextNodes(node) {
	        if (node.nodeType == 3) {
	            if (includeWhitespaceNodes || nonWhitespaceMatcher.test(node.nodeValue)) {
	                textNodes.push(node);
	            }
	        } else {
	            for (var i = 0, len = node.childNodes.length; i < len; ++i) {
	                getTextNodes(node.childNodes[i]);
	            }
	        }
	    }

	    if (node !== null) {
		    getTextNodes(node);
	    }
	    return textNodes;
	}, 

	/**
	 * Build and return a <script> element ready for insertion into the DOM.
	 *
	 * @param srcAttrValue - If provided, used as the "src" attribute of the returned element.
	 * @param isAsync - True if the resultant script should be configured to load asynchronously.
	 */
	buildScriptElement: function(srcAttrValue, isAsync) {
		var scriptEle = document.createElement('script');
		scriptEle.type = 'text/javascript';
		scriptEle.charset = 'utf-8';
		if (isAsync) {
			scriptEle.async = true;
		}
		if (srcAttrValue) {
			scriptEle.src = srcAttrValue;
		}
		return scriptEle;
	}

}
