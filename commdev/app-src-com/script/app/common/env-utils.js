'use strict';

var parseUri = require('parse-uri')

/**
 * Environment helper functions.
 */
module.exports = {

	/**
	 * Return true if the context environment is production, else false.
	 */
	calcIsProd: function() {
		/*
		 * The most universal mechanism we have for making a determination between prod and
		 * non-prod, on both post-login and pre-login pages, is based on the current URL.
		 */
		var url = parseUri(window.location.href);
		var isProd = true;
		// For example, https://com073016-allegisconnected.cs83.force.com/teksystems/LoginT...
		if (url.host.endsWith('.force.com') || url.host.endsWith('.salesforce.com')) {
			isProd = false;
		}
		return isProd;
	},

	/**
	 * Calculate the context OpCo, represented by an identifying slug. Current known values:
	 *	- teksystems
	 *	- aerotek
	 */
	calcOpCo: function() {
		var opCoSlug = null;
		/*
		 * The most universal mechanism we have for determining the OpCo, on both post-login
		 * and pre-login pages, is based on the current URL.
		 */
		var url = parseUri(window.location.href);
		var isProd = this.calcIsProd();
		if (isProd) {
			// For example, https://connect.teksystems.com
			var hostTokensArr = url.host.split('.');
			// Pluck out the token preceding the TLD.
			opCoSlug = hostTokensArr[hostTokensArr.length - 2];
		} else {
			// For example, url https://com073016-allegisconnected.cs83.force.com/teksystems/LoginT...
			// For example, path /teksystems/tc_login_teksystems
			var pathTokensArr = url.path.split('/');
			// Be prepared to handle paths both with and without a leading slash.
			opCoSlug = pathTokensArr[0];
			if (opCoSlug === null || opCoSlug.length < 1) {
				opCoSlug = pathTokensArr[1];
			}
		}

		if (opCoSlug == 'astoncartercom') {
			opCoSlug = 'astoncarter';
		}

		if (opCoSlug == 'tesksystemscom' || opCoSlug == 'teksystemscom') {
			opCoSlug = 'teksystems';
		}

		return opCoSlug;
	},

	/**
	 * Calculate and return the context (Salesforce) site prefix. For example:
	 * - /aerotek
	 * - /teksystems
	 */
	calcSitePrefix: function() {
		var result = skuid.page.sitePrefix;
		return result;
	}

}
