'use strict';

/**
 * Event (publish-subscribe) helper functions.
 *
 * Initially developed to silo references to skuid so that they do not impede unit testing..
 */
module.exports = {

	/**
	 * Publish an event with the given ID, coupled with the given data.
	 */
	publishEvent: function(eventId, dataArray) {
	    skuid.events.publish(eventId, dataArray);
	}, 

	/**
	 * Subscribe to an event with the given ID, having the given callback invoked whenever 
	 *	the event is triggered.
	 */
	subscribeToEvent: function(eventId, callback) {
	    skuid.events.subscribe(eventId, function(data) {
	    	callback(data);
	    });
	}
	
}
