'use strict';

var moment = require('moment');

var domManip = require("./dom-manip");
var envUtils = require("./env-utils");

/**
 * App-global, non-page-specific handlers.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for all pages.
	jQuery(document).ready(_handleDomReady);
};

var _handleDomReady = function() {
	//_initAnalyticsScripts();

	_addViewportMetaElementIfMissing();
};

/**
 * Initialize the analytics assets/scripts/logic.
 */
var _initAnalyticsScripts = function() {
	var isProd = envUtils.calcIsProd();
	var opCoSlug = envUtils.calcOpCo();
	console.log('opCoSlug = ' + opCoSlug);

	/*
	 * The script hosts should be contextualized based on OpCo and environment (production
	 * vs. non-production).
	 */
	var analyticsSubdomain = 'connect-analytics';

	/*
	Taking out non-prod WCS environments. They're unstable. JSON matched between the two. I don't see any reason
	if (!isProd) {
		analyticsSubdomain = 'connect-analytics-test';
	}*/
	// Make the protocol/scheme relative to that of the content URL.
	if (opCoSlug == 'aerotekcom') opCoSlug = 'aerotek';
	if (opCoSlug == 'teksystemscom') opCoSlug = 'teksystems';
	if (opCoSlug == 'astoncartercom') opCoSlug = 'astoncarter';
	//var analyticsHost = '//' + analyticsSubdomain + '.' + opCoSlug + '.com';

//	var maxTries = 50;
	//var tries = 0;
	//_addAnalyticsAssetsToDom(analyticsHost, tries, maxTries);
};

/**
 * Add references to the analytics scripts to the DOM.
 *
 * @param analyticsHost - The host of the analytics scripts.
 * @param tries - The current number of times that we've tried to insert the elements into
 *	the DOM.
 * @param maxTries - The maximum number of times that we should try to insert the elements
 *	into the DOM.
 */
var _addAnalyticsAssetsToDom = function(analyticsHost, tries, maxTries) {
	var isDomReady = _calcIfDomIsReadyForAnalyticsToBeAdded();

	// Prevent infinite loops.
	//if (!isDomReady && tries < maxTries) {
		//setTimeout(function(){
			//_addAnalyticsAssetsToDom(analyticsHost, ++tries, maxTries);
		//}, 250);
	//}

	if (isDomReady) {
		var body = document.getElementsByTagName('body')[0];

		/*
		 * The below-referenced scripts expect a global metricData to already be defined.
		 */
		window.metricData = {};

		/*
		 * Need to explicitly set the page name for some reason. If we do not, Omniture will lump it
		 * into "Other". Shouldn't be necessary.
		 */
		var pageTitle = document.getElementsByTagName("title")[0].innerHTML;

		/*
		 * From an analytics perspective, give each user profile page the same name - "User Profile".
		 * Example out-of-the-box page title: "User: Michael Scepaniak-aero1 ~ Aerotek Talent Community"
		 */
		if (pageTitle.indexOf('User: ') === 0 && pageTitle.endsWith(' Community')) {
			pageTitle = 'User Profile';
		}
		window.metricData.page_name = pageTitle;

		// Force a fresh load of the analytics script each hour, allowing for updates.
		//var cacheBuster = moment().format('YYYYMMDDHH');
		//body.appendChild(domManip.buildScriptElement(analyticsHost + '/_res/js/reportsuites.js?' + cacheBuster, true));

		//tries = 0;
		//_addSecondAnalyticsScriptToDom(body, analyticsHost, tries, maxTries);
	}
};

/**
 * Determine if the page's DOM is ready for the analytics assets to be added. This is needed
 * to handle the reculiarities of the profile (Salesforce standard) page.
 */
var _calcIfDomIsReadyForAnalyticsToBeAdded = function() {
	var isDomReady = true;
	var profilePageIframe = jQuery('iframe.profileMainFrame');
	if (profilePageIframe.length > 0) {
		/*
		 * If the analytics assets are added into the DOM too early, the profile page will break
		 * (be empty). It seems like the presence of the below <div> elements indicates a safe
		 * state and that we can go about inserting our elements.
		 */
		var testEle1 = document.getElementById('uploadPhoto');
		var testEle2 = document.getElementById('contactInfo');
		// "helpBubble" isn't there for TEKsystems, only Aerotek.
		var testEle3 = ''; //document.getElementById('helpBubble');
		var testEle4 = document.getElementById('aboutMe');
		if (testEle1 === null || testEle2 === null || testEle3 === null || testEle4 === null) {
			isDomReady = false;
		}
	}
	return isDomReady;
};

/**
 * Add the "second" analytics script reference to the DOM. We're doing this so as to prevent the
 * following (sporadically-appearing) error:
 *	s_code.js:130 Uncaught TypeError: metricData.getReportSuiteInfo is not a function
 * s_code.js is dependent on reportsuites.js.
 *
 * @param body - The <body> DOM element into which the <script> element should be inserted.
 * @param analyticsHost - The host of the analytics scripts.
 * @param tries - The current number of times that we've tried to insert the element into
 *	the DOM.
 * @param maxTries - The maximum number of times that we should try to insert the element
 *	into the DOM.
 */
//var _addSecondAnalyticsScriptToDom = function(body, analyticsHost, tries, maxTries) {
	//if (!metricData.getReportSuiteInfo && tries < maxTries) {
		//setTimeout(function(){
			//_addSecondAnalyticsScriptToDom(body, analyticsHost, ++tries, maxTries);
		//}, 250);
	//}

	//if (metricData.getReportSuiteInfo) {
		// Force a fresh load of the analytics script each hour, allowing for updates.
		//var cacheBuster = moment().format('YYYYMMDDHH');
		//body.appendChild(domManip.buildScriptElement(analyticsHost + '/_res/js/s_code.js?' + cacheBuster, true));
	//}
//};

/**
 * Add the viewport meta element to the page (if not found). This is needed for "Salesforce standard"
 * pages where the <head> element cannot be directly manipulated. In other pages, it is set in
 * TC_Template.page.
 * For example, <meta name="viewport" content="width=device-width, initial-scale=1"/>
 */
var _addViewportMetaElementIfMissing = function() {
	var headEle = document.getElementsByTagName('head')[0];
	var viewportMetaEleArr = headEle.querySelectorAll('meta[name=viewport]');
	if (viewportMetaEleArr === null || viewportMetaEleArr.length < 1) {
		var viewportMetaEle = document.createElement('meta');
		viewportMetaEle.name = "viewport";
		viewportMetaEle.content = "width=device-width, initial-scale=1";
		headEle.appendChild(viewportMetaEle);
	}
};
