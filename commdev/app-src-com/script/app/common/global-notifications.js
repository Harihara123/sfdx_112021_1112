'use strict';

/* 
 * Need the compiler, which is not included in the runtime. More info: 
 *	https://github.com/wycats/handlebars.js/issues/953
 */
var Handlebars = require('handlebars/dist/handlebars');

var i18nUtils = require('../common/i18n-utils');
var templateUtils = require('../common/template-utils');
var uiUtils = require('../common/ui-utils');

var globalErrorTemplate = require('../templates/header-global-error.hbs');
var globalInfoTemplate = require('../templates/header-global-info.hbs');
var globalSuccessTemplate = require('../templates/header-global-success.hbs');
var globalWarningTemplate = require('../templates/header-global-warning.hbs');

/**
 * Page-level success, error, info, and warning notifications.
 */
module.exports = {

	/**
	 * Prepare the DOM for usage of global notifications.
	 */
	initGlobalNotifications: function() {
		var data = templateUtils.buildBaseTemplateContext();
		var globalErrorMarkup = globalErrorTemplate(data);
		var globalInfoMarkup = globalInfoTemplate(data);
		var globalSuccessMarkup = globalSuccessTemplate(data);
		var globalWarningMarkup = globalWarningTemplate(data);

		$(_uiSelectors.globalError).html(globalErrorMarkup);
		$(_uiSelectors.globalInfo).html(globalInfoMarkup);
		$(_uiSelectors.globalSuccess).html(globalSuccessMarkup);
		$(_uiSelectors.globalWarning).html(globalWarningMarkup);
	},
	
	/**
	 * Display a global error notification.
	 *
	 * @param notifContent - Markup to be used as the content of the notification.
	 * @param initiatorEleId - The ID of the initiating element, if any. By specifying an initiating 
	 *	element for a notification, this allows the notification to be cleared if/when the initiating 
	 *	element is destroyed/removed. The provided value need not be a jQuery-style selector.
	 * @param closeEvent - Event to be published when the notification's close button/icon is clicked. 
	 *	Optional. If not specified, the notification will simply close.
	 * @param scrollToDomEle - Optional DOM element that should be brought into the viewport. This 
	 *	allows a form-level error message to be brought into view.
	 */
	displayNotificationError: function(notifContent, initiatorEleId, closeEvent, scrollToDomEle) {
		_displayNotification('globalError', notifContent, initiatorEleId, closeEvent);

		if (scrollToDomEle !== null) {
			var bufferBetweenNotificationAndDomEle = 60;
			uiUtils.scrollToElement(scrollToDomEle, -bufferBetweenNotificationAndDomEle);
		}
	},

	/**
	 * Display a global info notification.
	 *
	 * @param notifContent - Markup to be used as the content of the notification.
	 * @param initiatorEleId - The ID of the initiating element, if any. By specifying an initiating 
	 *	element for a notification, this allows the notification to be cleared if/when the initiating 
	 *	element is destroyed/removed. The provided value need not be a jQuery-style selector.
	 * @param closeEvent - Event to be published when the notification's close button/icon is clicked.
	 *	Optional. If not specified, the notification will simply close.
	 */
	displayNotificationInfo: function(notifContent, initiatorEleId, closeEvent) {
		_displayNotification('globalInfo', notifContent, initiatorEleId, closeEvent);
	},

	/**
	 * Display a global success notification.
	 *
	 * @param notifContent - Markup to be used as the content of the notification.
	 * @param initiatorEleId - The ID of the initiating element, if any. By specifying an initiating 
	 *	element for a notification, this allows the notification to be cleared if/when the initiating 
	 *	element is destroyed/removed. The provided value need not be a jQuery-style selector.
	 * @param closeEvent - Event to be published when the notification's close button/icon is clicked.
	 *	Optional. If not specified, the notification will simply close.
	 */
	displayNotificationSuccess: function(notifContent, initiatorEleId, closeEvent) {
		_displayNotification('globalSuccess', notifContent, initiatorEleId, closeEvent);
	},

	/**
	 * Display a global warning notification.
	 *
	 * @param notifContent - Markup to be used as the content of the notification.
	 * @param initiatorEleId - The ID of the initiating element, if any. By specifying an initiating 
	 *	element for a notification, this allows the notification to be cleared if/when the initiating 
	 *	element is destroyed/removed. The provided value need not be a jQuery-style selector.
	 * @param closeEvent - Event to be published when the notification's close button/icon is clicked.
	 *	Optional. If not specified, the notification will simply close.
	 */
	displayNotificationWarning: function(notifContent, initiatorEleId, closeEvent) {
		_displayNotification('globalWarning', notifContent, initiatorEleId, closeEvent);
	},

	displayNotificationCatchAllError: function(data) {
        data = (!data) ? {} : data;
        var catchAllErrorMsg = i18nUtils.retrieveText('TC_global_notification_catch_all_error');
        var catchAllErrorMsgTempl = Handlebars.compile(catchAllErrorMsg);
        var templCtx = {
            '$Site': {
                'Prefix': null
            }
        };
        if (typeof skuid !== 'undefined') {
        	templCtx.$Site.Prefix = skuid.page.sitePrefix;
        }
        var catchAllErrorMsgMarkup = catchAllErrorMsgTempl(templCtx);
        this.displayNotificationError(catchAllErrorMsgMarkup);
	},

	/**
	 * Clear all global notifications so they are no longer displayed.
	 */
	clearGlobalNotifications: function() {
		this.clearNotificationError();
		this.clearNotificationInfo();
		this.clearNotificationSuccess();
		this.clearNotificationWarning();
	},

	clearNotificationError: function() {
		_clearNotification('globalError');
	},

	clearNotificationInfo: function() {
		_clearNotification('globalInfo');
	},

	clearNotificationSuccess: function() {
		_clearNotification('globalSuccess');
	},

	clearNotificationWarning: function() {
		_clearNotification('globalWarning');
	},

	/**
	 * Clear (no longer display) any currently-displayed notifications associated with the identified 
	 * element. 
	 *
	 * @param initiatorEleId - The ID of the initiating element, which is assumed to have been passed to a 
	 *	previous call to one of the displayNotification...() functions. The provided value need not be a 
	 *	jQuery-style selector.
	 */
	clearNotificationsForInitiator: function(initiatorEleId) {
		if (initiatorEleId) {
			var initiatedNotifs = $('[data-initiator=' + initiatorEleId + ']');
			var clearFunc = _clearNotificationElement;
			initiatedNotifs.each(function(idx, ele) {
				clearFunc($(this));
			});
		}
	}

}

/**
 * Centralize the jQuery selectors to help prevent typos.
 */
var _uiSelectors = {
	// Found in the Skuid header page.
	globalError: '#global-error-toast',
	globalInfo: '#global-info-toast',
	globalSuccess: '#global-success-toast', 
	globalWarning: '#global-warning-toast'
};

var _displayNotification = function(notifType, notifContent, initiatorEleId, closeEvent) {
	module.exports.clearGlobalNotifications();

	/*
	 * We are not re-rendering the notification from scratch. Rather, we are mutating what is 
	 * already in the DOM. So far, this is working out well.
	 */
	$(_uiSelectors[notifType]).find('.slds-text-heading--small').html(notifContent);
	$(_uiSelectors[notifType]).attr('data-initiator', initiatorEleId);

	var closeButton = $(_uiSelectors[notifType]).find('.js-notify-close-button')[0];
	// If a custom close event name/ID is provided, use it; else use a default.
	if (closeEvent !== undefined && closeEvent !== null) {
		closeButton.onclick = function(event) {
			/*
			 * Skuid is available on the post-auth pages, whereas pubSub is available on the 
			 * pre-auth pages.
			 */
			if (typeof skuid !== 'undefined') {
				skuid.events.publish(closeEvent);
			} else if (typeof pubSub !== 'undefined') {
				pubSub.publish(closeEvent);
			} else {
				console.log('No known event publisher found.');
			}
		};
	} else {
		closeButton.onclick = function(event) {
			var eventName = 'com.header.clear' + notifType.split('')[0].toUpperCase() + notifType.substring(1);
			/*
			 * Skuid is available on the post-auth pages, whereas pubSub is available on the 
			 * pre-auth pages.
			 */
			if (typeof skuid !== 'undefined') {
				skuid.events.publish(eventName);
			} else if (typeof pubSub !== 'undefined') {
				pubSub.publish(eventName);
			} else {
				console.log('No known event publisher found.');
			}
		};
	}

	// Show the notification.
	$(_uiSelectors[notifType]).removeClass('hidden');

    // Auto-hide success messages after a few seconds.
    if (notifType === 'globalSuccess') {
        var clearFunc = _clearNotification;
	    setTimeout(function() {
	        clearFunc(notifType, _uiSelectors);
	    }, 3400);
    }
};

var _clearNotification = function(notifType) {
	_clearNotificationElement($(_uiSelectors[notifType]));
};

var _clearNotificationElement = function($notifEle) {
	/*
	 * Tried to transition the opacity to 0 to avoid a sudden blip, but it got too 
	 * complicated in IE. In Chrome, setting the opacity to 0 (appears to) automatically 
	 * hide (as in display) the element. Not so in IE. Also, in IE, two elements needed 
	 * to be hidden when using opacity.
	 */
	$notifEle.addClass('hidden');
	
	$notifEle.attr('data-initiator', '');
};

