'use strict';

/**
 * Internationalization helper functions.
 *
 * Initially developed to silo references to skuid so that they do not impede unit testing..
 */
module.exports = {

	/**
	 * Retrieve text with the given ID.
	 */
	retrieveText: function(textId) {
	    var result = skuid.$L(textId);
	    return result;
	}

}
