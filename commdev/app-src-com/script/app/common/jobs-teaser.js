'use strict';

var _ = require('lodash/core');
var _take = require('lodash/take');
var moment = require('moment');

var assignmentStatus = require("../common/assignment-status");
var i18nUtils = require('../common/i18n-utils');
var modelUtils = require('../common/model-utils');
var templateUtils = require('../common/template-utils');

var jobsTeaserExploreTemplate = require('../templates/dashboard-jobs-teaser-explore.hbs');
var jobsTeaserListingTemplate = require('../templates/dashboard-jobs-teaser-listing.hbs');
var jobsTeaserMatchesTemplate = require('../templates/dashboard-jobs-teaser-matches.hbs');

/**
 * Jobs teaser functionality.
 */
module.exports = {

    /**
     * The number of jobs to display when in "listing" mode (for formers).
     */
    JOBS_LISTING_DISPLAY_COUNT: 3, 

    /**
     * Display the jobs teaser card, appending it in the DOM to the given DOM element. Three versions 
     * of the card exist:
     *  - a "relaxed" version, encouraging talent to explore jobs.
     *  - a somewhat more attention-grabbing version that displays the count of matched jobs.
     *  - an engaging version that displays a listing of the top-most matched jobs.
     *
     * @param $rootEle - jQuery-wrapped DOM element.
     * @param jobSearchDaysCount - The maximum age (in days) of any jobs included in the search results 
     *  displayed by the jobs teaser card.
     * @param talentAssignmentStatus - Object encapsulating information about the current/next 
     *  assignment, including assignment status and the start/end date for that assignment.
     * @callback - Invoked to indicate completion of the logic.
     */
    displayJobsTeaser: function($rootEle, jobSearchDaysCount, talentAssignmentStatus, callback) {
        // By default, the version of the card that displays the count of matched jobs is displayed.
        var jobsTeaserMode = 'matches';
        if (talentAssignmentStatus.status === assignmentStatus.TALENT_STATUS_INACTIVE) {
            // For FORMERS, the listing of the top-most matched jobs is displayed.
            jobsTeaserMode = 'listing';
        } else if (talentAssignmentStatus.status === assignmentStatus.TALENT_STATUS_STARTING || 
            talentAssignmentStatus.status === assignmentStatus.TALENT_STATUS_DATE_CHANGE_REQUESTED) 
        {
            /*
             * If the user is about to start an assignment, is on assignment (and not near the end of that 
             *  assignment), or has indicated that their assignment end date needs to be changed, display 
             *  the "relaxed" version of the card.
             */
            jobsTeaserMode = 'explore';
        } else if (talentAssignmentStatus.status === assignmentStatus.TALENT_STATUS_ACTIVE) {
            var today = Date.now();
            var daysRemaining = (moment(talentAssignmentStatus.endDate).diff(today, 'days') + 1);
            // Unless the talent's assignment is ending soon, the "relaxed" version is displayed.
            if (daysRemaining > jobSearchDaysCount) {
                jobsTeaserMode = 'explore';
            }
        }

        var cardMarkup = '';
        if (jobsTeaserMode === 'matches' || jobsTeaserMode === 'listing') {
            var queryModel = modelUtils.retrieveModelDefinition('jobsearch');
            var queryPromise = _queryForJobs(queryModel);
            var exportsScope = this;
            queryPromise.done(function() {
                var queryData = modelUtils.retrieveModelData(queryModel);
                var jobsCount = queryData.searchParameters.recordCount;
                var radius = queryData.searchParameters.radius;

                if (jobsTeaserMode === 'listing') {
                    // To formers, show a listing of matched jobs.
                    cardMarkup = _buildJobsListingCard(queryData.jobs, exportsScope.JOBS_LISTING_DISPLAY_COUNT);
                } else if (jobsCount > 0 && radius !== 'Everywhere') {
                    cardMarkup = _buildJobsMatchesCard(jobsCount);
                } else {
                    cardMarkup = _buildJobsExploreCard();
                }

                $rootEle.append(cardMarkup);
                callback();
            });
        } else {
            cardMarkup = _buildJobsExploreCard();
            $rootEle.append(cardMarkup);
            callback();
        }
    }
}

var _queryForJobs = function(queryModel) {
    var user = modelUtils.retrieveModelData('RunningUser');

    var UserTitle = user.Contact.Title ? user.Contact.Title : ' ';
    var UserCity = user.Contact.MailingCity ? user.Contact.MailingCity : ' ';
    var UserState = user.Contact.MailingState ? user.Contact.MailingState.toUpperCase() : '';

    var mccity = queryModel.getConditionByName('city',true);
    var mcstate = queryModel.getConditionByName('state',true);
    var mckeywords = queryModel.getConditionByName('keywords',true);
    var mcdistance = queryModel.getConditionByName('distance',true);

    queryModel.setCondition(mccity, UserCity, false);
    queryModel.setCondition(mcstate, UserState, false);
    queryModel.setCondition(mckeywords, UserTitle, false);
    queryModel.setCondition(mcdistance,'50',false);

    var promise = modelUtils.fetchModelData(queryModel);
    return promise;
};

/**
 * Build a "relaxed" version of the card that exposes the job search in a less attention-grabbing way.
 */
var _buildJobsExploreCard = function() {
    var templCtx = templateUtils.buildBaseTemplateContext();
    return jobsTeaserExploreTemplate(templCtx);
};

/**
 * Build an attention-grabbing version of the card that shows the number of job matches.
 */
var _buildJobsMatchesCard = function(jobsCount) {
    var templCtx = templateUtils.buildBaseTemplateContext();
    templCtx.jobsCount = jobsCount;
    templCtx.newOpportunitiesText = i18nUtils.retrieveText('TC_new_opportunity_you_might_be_interested_in');
    if (jobsCount > 1) {
        templCtx.newOpportunitiesText = i18nUtils.retrieveText('TC_new_opportunities_you_might_be_interested_in');
    }
    return jobsTeaserMatchesTemplate(templCtx);
};

/**
 * Build a version of the card that shows 5 recently-qualified reqs (given) that are suitable matches 
 * for the user.
 */
var _buildJobsListingCard = function(reqsArr, jobsDisplayCount) {
    var templCtx = templateUtils.buildBaseTemplateContext();
    templCtx.jobsFound = false;
    if (reqsArr.length > 0) {
        templCtx.jobsFound = true;
    }

    _.forEach(reqsArr, function(ele, idx, collection) {
        if (ele.qualifieddate && ele.qualifieddate.length > 0) {
            ele.reqDateFromNowStr = moment(ele.qualifieddate).fromNow();
        }
    });
    // Prune the array to the first n elements.
    templCtx.reqs = _take(reqsArr, jobsDisplayCount);
    //console.log('templCtx.reqs = ' + JSON.stringify(templCtx.reqs));

    return jobsTeaserListingTemplate(templCtx);
};


