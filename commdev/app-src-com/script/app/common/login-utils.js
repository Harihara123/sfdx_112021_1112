'use strict';

var browserUtils = require("../common/browser-utils");
var globalNotifications = require("../common/global-notifications");
var loginUtils = require("../common/login-utils");
var envUtils = require("../common/env-utils");
var templateUtils = require("../common/template-utils");

/**
 * Login helper functions.
 */
module.exports = {

	/** 
	 * Chevron drop down on mobile for TEKsystems.
	 **/
	initAccordions: function() {
	    $(function(){
	        $('.u-accordion dt').on('click', function(){
	            $(this).toggleClass('is-active');
	            $(this).next('dd').toggleClass('is-active');
	        });
	    });
	},
	
	/**
	* User Inactive
	*/

	popupForInactive: function() {
	    var inactiveUserFlag = $('.js-inactive-user-flag').prop('checked');
	    var opcoSlug = envUtils.calcOpCo();
	    if (profileNameSlug == '') profileNameSlug = opcoSlug;

	    //Can take this 'if' out once the class action stuff is dead
	    if (profileNameSlug != 'jobaccess') {
			var loginDisabledMessageTemplate = require('../templates/login-disabled-message-' + profileNameSlug + '.hbs');
	    
		    var templCtx = templateUtils.buildBaseTemplateContext();
		    var loginDisabledMessage = loginDisabledMessageTemplate(templCtx);

		    if(inactiveUserFlag) {
	            pubSub.publish("com.header.displayGlobalInfo", {
	                notifContent:loginDisabledMessage
	            });
		    }
		}
	}
}
