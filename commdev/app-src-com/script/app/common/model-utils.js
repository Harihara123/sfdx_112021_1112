'use strict';

var _ = require('lodash/core');

/**
 * Model (as in "data model") helper functions.
 *
 * Initially developed to silo references to skuid so that they do not impede unit testing..
 */
module.exports = {

	/**
	 * Retrieve a "handle" to the identified model. This reflects a Skuid-ish structure where the model 
	 *	"handle" and that model's data are not one and the same.
	 */
	retrieveModelDefinition: function(modelId) {
	    var model = skuid.$M(modelId);
	    return model;
	}, 

	/**
	 * Retrieve a "handle" to the identified collection (model). This reflects a Skuid-ish structure where 
	 *	the collection (model) "handle" and that collection's data are not one and the same.
	 */
	retrieveCollectionDefinition: function(collectionId) {
	    var collection = this.retrieveModelDefinition(collectionId);
	    return collection;
	}, 

	/**
	 * Retrieve the data for the identified model or the given model "handle". This reflects a 
	 * 	Skuid-ish structure where the model "handle" and that model's data are not one and the same.
	 *
	 * @param modelIdOrModel - Either a string identifier for a model "handle" or the "handle" itself. 
	 */
	retrieveModelData: function(modelIdOrModel) {
		var modelData = null;

		/*
		 * In Skuid, every model is a collection. Grab the first row, which is (hopefully) the only row 
		 *	that is populated.
		 */
		if (_.isString(modelIdOrModel)) {
			var model = this.retrieveModelDefinition(modelIdOrModel);
			if (model !== undefined) {
				modelData = model.getFirstRow();
			}
		} else if (_.isFunction(modelIdOrModel.getFirstRow)) {
			modelData = modelIdOrModel.getFirstRow();
		}
		return modelData;
	}, 

	/**
	 * Retrieve the data for the identified collection (model) or the given collection (model) "handle". 
	 *	This reflects a Skuid-ish structure where the model "handle" and that model's data are not one 
	 *	and the same.
	 *
	 * @param collectionIdOrCollection - Either a string identifier for a collection (model) "handle" 
	 *	or the "handle" itself. 
	 */
	retrieveCollectionData: function(collectionIdOrCollection) {
		var collectionData = null;

		if (_.isString(collectionIdOrCollection)) {
			var collection = this.retrieveCollectionDefinition(collectionIdOrCollection);
			if (collection !== undefined) {
				collectionData = collection.getRows();
			}
		} else if (_.isFunction(collectionIdOrCollection.getRows)) {
			collectionData = collectionIdOrCollection.getRows();
		}
		return collectionData;
	}, 

	/**
	 * Fetch (from the server) the data for the given model or array of models into said model(s).
	 *
	 * @param modelOrModelArray - Either one model "handle" or an array of (potentially different) model 
	 *	"handles".
	 * @param callback - If given, called on completion of the fetch operation, passed the data 
	 *	fetched from the server.
	 */
	fetchModelData: function(modelOrModelArray, callback) {
		var promise = null;
		if (_.isArray(modelOrModelArray)) {
			promise = skuid.model.updateData(modelOrModelArray, function(data) {
				if (callback) {
					callback(data);
				}
			});
		} else if (_.isFunction(modelOrModelArray.updateData)) {
			promise = modelOrModelArray.updateData();
		}
		return promise;
	},

	/**
	 * Save (to the server) the data currently associated with the given model or array of models.
	 *
	 * @param modelData - The actual model data. Currently NOT used.
	 * @param options - Customizes the functionality of the save operation.
	 * @param modelOrModelArray - Either one model "handle" or an array of (potentially different) model 
	 *	"handles".
	 * @return a promise to wait on completion of the save operation.
	 */
	saveModelData: function(modelData, options, modelOrModelArray) {
		var promise = null;
		if (_.isArray(modelOrModelArray)) {
			promise = skuid.model.save(modelOrModelArray, options);
		} else if (_.isFunction(modelOrModelArray.save)) {
			promise = modelOrModelArray.save(options);
		}
		return promise;
	}, 

	/**
	 * Update the values in the given model (locally) using the given map of values (changesObj).
	 *
	 * @param modelData - The actual model data which is to be updated. This reflects a 
	 * 	Skuid-ish structure where the model "handle" and that model's data are not one and the same.
	 * @param changesObj - A map of attribute names to attribute values to be applied to the model data.
	 * @param model - A "handle" to the model that sorresponds to the given modelData.
	 */
	updateModelValues: function(modelData, changesObj, model) {
		model.updateRow(modelData, changesObj);
	},

	/**
	 * Retrieve the user ID of the context user.
	 */
	retrieveUserId: function() {
		var result = null;
		if (typeof skuid !== 'undefined') {
			result = skuid.utils.userInfo.userId;
		}
		return result;
	}

}
