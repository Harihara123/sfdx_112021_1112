'use strict';

/**
 * Polyfill functions. In web development, a polyfill is code that implements a feature on web 
 * browsers that do not support the feature.
 */
module.exports = {

	/**
	 * Initialize one or more polyfills.
	 */
	init: function() {
		/*
		 * Internet Explorer does not implement endsWith().
		 * Originally sourced from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith
		 */
		if (!String.prototype.endsWith) {
			String.prototype.endsWith = function(searchString, position) {
				var subjectString = this.toString();
				if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
					position = subjectString.length;
				}
				position -= searchString.length;
				var lastIndex = subjectString.indexOf(searchString, position);
				return lastIndex !== -1 && lastIndex === position;
			};
		}
	}

}
