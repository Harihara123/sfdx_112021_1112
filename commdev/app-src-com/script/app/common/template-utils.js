'use strict';

var _ = require('lodash/core');
// Pull in lodash functions that aren't in the core build.
var _capitalize = require('lodash/capitalize');
var _forOwn = require('lodash/forOwn');

var envUtils = require("./env-utils");

/**
 * Templating utility functions. By "templating", we mean Handlebars, Mustache, Skuid, etc.
 */
module.exports = {

	/**
	 * Build and return a standard/base context that contains properties frequently used by many templates 
	 * in the app.
	 */
	buildBaseTemplateContext: function() {
	    /*
	     * Mimic standard Skuid global merge variables: http://help.skuid.com/m/11720/l/187263 
	     * This allows templates to more easily move between Handlebars and Skuid.
	     */
	    var ctx = {
	        '$Site': {
	            'Prefix': null
	        }, '$Label': {
	        	// To be populated. See below.
	        }, 'CustomSettings': {
	        	// To be populated. See below.
	        }, 'OpCoMappings': {
	        	// To be populated. See below.
	        }
	    };


	    /*
	     * Skuid is only available on the post-auth pages. However, we'd like to make use of similar 
	     * constructs to those provided by Skuid in the pre-auth pages. If a substitute "sfdcContext" 
	     * has been initialized (see tc_template_first.page), use it as a drop-in replacement.
	     */
	    if (typeof sfdcContext !== 'undefined') {
			skuid = sfdcContext;

			/*
			 * In case anything tries to publish an event via skuid.events, have pubSub handle it. 
			 * pubSub is used in place of Skuid events on the pre-auth (non-Skuid) pages.
			 */
			skuid.events = {};
			skuid.events.publish = pubSub.publish;
	    }

	    if (typeof skuid !== 'undefined') {
	    	ctx.$Site.Prefix = skuid.page.sitePrefix;

		    /*
		     * Pull in the labels configured for the context Skuid page.
		     */
		    _.forEach(skuid.label.map, function(ele, key, collection) {
		    	// Let's be conservative and only pull in "our" labels, skipping Skuid's standard labels.
		        if (key.indexOf('TC_') === 0) {
		            ctx.$Label[key] = ele;
		        }
		    });

		    // TODO Support the use of custom settings on pre-auth pages.
		    if (typeof skuid.$M !== 'undefined') {
			    // Pull in the custom settings, global to all Skuid pages.
			    var customSettings = skuid.$M('CustomSettings').getRows()[0];
			    _forOwn(customSettings, function(ele, key, obj) {
			    	ctx.CustomSettings[key] = ele;
			    });

			    var opCoMappings = skuid.$M('OpCoMappings').getRows()[0];
			    _forOwn(opCoMappings, function(ele, key, obj) {
			    	ctx.OpCoMappings[key] = ele;
			    });
		    }
	    }

	    // Add in OpCo-detection flags. For example, {{#if isAerotek}}
	    var opCoSlug = envUtils.calcOpCo();
	    ctx['is' + _capitalize(opCoSlug)] = true;

	    return ctx
	}
}
