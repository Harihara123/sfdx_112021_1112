'use strict';

var globalNotifications = require('../common/global-notifications');

/**
 * UI helper functions.
 */
module.exports = {

	/**
	 * Show a loading mask that prevents the user from interacting with the screen until 
	 * the mask is taken away.
	 *
	 * @param message - Optional message to be displayed instead of the default ("Please wait...").
	 */
	showLoadingMask: function(message) {
		if (skuid) {
			var params = {};
			if (message !== null) {
				params.message = message;
			}
			skuid.$.blockUI(params);
		} else {
			$('body').addClass('is-loading');
		}
	},
	
	/**
	 * Remove the loading mask, allowing the user to again interact with the screen.
	 */
	hideLoadingMask: function() {
		if (skuid) {
			skuid.$.unblockUI();
		} else {
			$('body').removeClass('is-loading');
		}
	},

	/**
	 * Scroll the viewport to the top of the screen.
	 */
	scrollToTop: function() {
		$('html,body').scrollTop(0);
	},

	/**
	 * Scroll the viewport to the given DOM element.
	 *
	 * @param domEle - The DOM element to scroll into view.
	 * @param scrollAdjustAmount - The amount by which the scroll should "overshoot" or 
	 *	"undershoot". Negative numbers move the viewport up, positive down.
	 */
	scrollToElement: function(domEle, scrollAdjustAmount) {
		var pos = this.findPositionOfElement(domEle);
		if (scrollAdjustAmount !== null) {
			pos += scrollAdjustAmount;
		}
		window.scroll(0, pos);
	},

	/**
	 * Return the position (top) of the given DOM element.
	 * Originally sourced from http://stackoverflow.com/a/11986374/176247
	 */
	findPositionOfElement: function(domEle) {
	    var curtop = 0;
	    if (domEle && domEle.offsetParent) {
	        do {
	            curtop += domEle.offsetTop;
	        } while (domEle = domEle.offsetParent);
	    	return curtop;
	    }
	},

	/**
	 * Make the given invisible DOM element visible.
	 */
	makeVisible: function(domEle) {
		var $domEle = $(domEle);
		$domEle.removeClass('slds-hidden');
	}, 

	/**
	 * Close the modal with the specified ID, if given. If not given, the currently-displayed modal is 
	 * closed. An attempt will be made to clear-out any errors displayed in the modal, as well.
	 *
	 * @param modalId - The ID of the modal to close. Will be converted into a jQuery-style ID selector. If 
	 *	provided, global notifications initiated by the identified modal will be cleared, as well.
	 */
	closeModal: function(modalId) {
		// Assume the Lightning Design System.
		var modalSelector = '.slds-modal';
		var backdropSelector = '.slds-backdrop';
		if (modalId) {
		    modalSelector = '#' + modalId + ' ' + modalSelector;
		    backdropSelector = '#' + modalId + ' ' + backdropSelector;
		}

		$(modalSelector).removeClass('slds-fade-in-open');
		$(backdropSelector).removeClass('slds-backdrop--open');

		if (modalId) {
			// TODO Figure out why need to invoke via pub/sub instead of direct call.
			skuid.events.publish('com.header.clearGlobalNotificationsForInitiator', [{ initiatorEleId: modalId }])
		}

		// Clear out any form-level errors.
		$(modalSelector).find('.js-form-level-errors').html('');
	}, 

	/**
	 * Helper function to make all elements in a row the same height.
	 *
	 * @param rowSelector - Query selector for the row whose contents are to be smoothed.
	 * @param rowChildSelector - Query selector for the contents of the row whose heights are 
	 *	to be smoothed.
	 */
	smoothHeightOfRowContents: function(rowSelector, rowChildSelector) {
	    var target = skuid.$(rowSelector);

	    // Each JS active row
	    skuid.$.each(target, function(i, e) {

	        // Setup
	        var height = 0;
	        var chernses = skuid.$(rowChildSelector, skuid.$(this));
	        chernses.css('min-height', '');

	        // Each Card in Row
	        skuid.$.each(chernses, function(i2, e2){

	            if(skuid.$(this).outerHeight() > height){
	                height = skuid.$(this).outerHeight();
	            }

	        });

	        // set all elements to height
	        chernses.css('min-height', height);

	        // set class flag so we know this row is
	        // height adjusted
	        skuid.$(this).addClass('is-height-adjusted');

	    });
	}

}
