'use strict';

var _ = require('lodash/core');

var skuidUIHelpers = require("../../../../app-src/helpers/skuid/ui");
var templateUtils = require('../common/template-utils.js');

var formLevelErrorTemplate = require('../templates/global-form-level-error.hbs');

/**
 * Validation-related functions.
 */
module.exports = {

	/**
	 * Validate that any input elements marked as required in the given form DOM element have non-blank 
	 * values. If any elements are found to contain blank values, a ready-to-display error message is returned. 
	 *
	 * @param formDomEle - Form DOM element parent of the input elements to be validated.
	 * @return a blank string if no violations found, else return a string such as the following: 
	 *	"Required Fields have no Value: Friend's First Name, Friend's Last Name, Friend's Email"
	 *	The input names included in the error message are pulled from <label> elements that are "for" each 
	 *	<input> element.
	 */
	validateRequiredFields: function(formDomEle) {
		var result = '';

		var $form = $(formDomEle);
		// Limit ourselves to input and select elements that are required.
		var requiredInputsArr = $form.find('input[required=required], select[required=required]');
	    var errorInputLabelsArr = [];
	    _.forEach(requiredInputsArr, function(ele, idx, collection) {
	        var $requiredInput = $(ele);
	        var inputValue = $requiredInput.val();
	        if (!inputValue || inputValue.trim() === '') {
	        	// Get the ID of the blank input.
	            var inputId = $requiredInput.attr('id');
	            // Get the label for the blank input. Does not have to be displayed,
	            var inputLabelArr = $('label[for=' + inputId + ']');
	            if (inputLabelArr.length > 0) {
	            	// Just take the first label found.
	            	var inputLabelEle = inputLabelArr[0];
	            	var inputLabel = $(inputLabelEle).text();
	            	errorInputLabelsArr.push(inputLabel);
	            } else {
	            	errorInputLabelsArr.push('[no name found]');
	            }
	        }
	    });

	    // Were any violations found?
	    if (errorInputLabelsArr.length > 0) {
	    	// Mimic the standard Skuid "required values missing" error message.
		    var errorMsgPrefix = skuidUIHelpers.labelFromOrFail('required_fields_have_no_value') + ': ';
	        var errorMsgContent = errorInputLabelsArr.join(', ');
	        result = errorMsgPrefix + errorMsgContent;
	    }

	    return result;
	},

	/**
	 * Display the given ready-to-display error message (or array of error messages) as form-level errors. 
	 *
	 * @param error - Ready-to-display error message (or array of error messages). This could also be HTML 
	 *	markup.
	 * @param formLevelErrorDomEle - The DOM element into which the generated error(s) display markup is 
	 *	to be set.
	 * @param initiatorEleId - The ID of the initiating element, if any. By specifying an initiating 
	 *	element for a notification, this allows the notification to be cleared if/when the initiating 
	 *	element is destroyed/removed. The provided value need not be a jQuery-style selector.
	 */
	displayFormLevelError: function(error, formLevelErrorDomEle, initiatorEleId) {
	    var templCtx = templateUtils.buildBaseTemplateContext();
	    if (_.isArray(error)) {
	        templCtx.errorsArr = error;
	    } else {
	        templCtx.errorMessageMarkup = error;
	    }
	    var errorMarkup = formLevelErrorTemplate(templCtx);
	    $(formLevelErrorDomEle).html(errorMarkup);

	    var validationErrorMsg = skuidUIHelpers.labelFromOrFail("TC_global_notification_validation_error");
	    skuid.events.publish('com.header.displayGlobalError', [{ 
	        notifContent: validationErrorMsg, 
	        initiatorEleId: initiatorEleId
	    }]);
	}

}
