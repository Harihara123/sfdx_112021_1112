'use strict';

/**
 * Webpack helper functions.
 */
module.exports = {

	/**
	 * Configure the "public path" used by Webpack in the generated bundle(s). We need to do this because our 
	 * bundles (one or more entry bundles and multiple chunk bundles) are served up from Salesforce via a 
	 * zipped-up archive. The URL of this archive has a path that is dynamically calculated by Salesforce 
	 * whenever a new version of the archive is uploaded to Salesforce. 
	 *
	 * In order for this entry module to be able to resolve lazily-loaded chunk bundles (meaning, in order 
	 * for the browser to properly resolve them), the __webpack_public_path__ variable must be dynamically 
	 * set (at page-load time) to account for the Salesforce-calculated path. We do this by scanning the DOM 
	 * for <script> elements, finding the one that loads the entry module, and setting __webpack_public_path__ 
	 * to the path of this asset. Example script reference: 
	 * <script src="/teksystems/resource/1469789271000/tc_script/app-first.js" type="text/javascript"></script>
	 *
	 * Originally sourced from https://github.com/iclanton/set-webpack-public-path-loader
	 *
	 * @param entryFilename - The name of an "entry" file (as far as Webpack is concerned). This is a file 
	 *	that Webpack will expose to the browser via a <script> tag. For example, "app.js".
	 */
	configureWebpackPublicPath: function(entryFilename) {
	    var scripts = document.getElementsByTagName('script');
	    var found = false;
	    console.log('entryFilename = ' + entryFilename);

	    if (scripts && scripts.length) {
	        for (var i = 0; i < scripts.length && !found; i++) {
	        	if (scripts[i]) {
		        	var path = scripts[i].getAttribute('src');
		        	if (path && path.endsWith('/' + entryFilename)) {
		        		var dynPath = path.substring(0, path.lastIndexOf('/') + 1);
		        		console.log('dynPath = ' + dynPath);
		        		__webpack_public_path__ = dynPath;
		        		found = true;
		        	}
	        	}
	        }
	    }
	}

}