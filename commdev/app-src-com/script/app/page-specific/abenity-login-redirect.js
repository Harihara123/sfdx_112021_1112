'use strict';

var _ = require('lodash/core');

var browserUtils = require('../common/browser-utils');
var envUtils = require('../common/env-utils');
var eventUtils = require('../common/event-utils');

/**
 * Handle the "pageload" event for the Abenity login redirect page.
 */
module.exports = {

    /**
     * URL path to the Abenity SSO server-side endpoint.
     */
    ABENITY_SSO_SERVICE_URL_PATH: '/services/apexrest/tc_rest_abenity_sso/', 

    /**
     * URL for the Abenity login page.
     */
    ABENITY_FALLBACK_LOGIN_URL: 'http://allegisbenefits.employeediscounts.co/perks/discountprogram',

    handlePageLoaded: function(redirectTimeout, callback) {
    	// Configure a DOM-ready handler for the page.
    	$(document).ready(function() {
            _handleDomReady(redirectTimeout, callback);
        }); 
    }

}

/**
 * Handle the DOM-ready callback.
 *
 * @param redirectTimeout - Number of milliseconds to wait before performing any browser redirection. Optional.
 * @param callback - Invoked upon completion of all logic. Initially added to facilitate unit testing.
 */
var _handleDomReady = function(redirectTimeout, callback) {
    // Set the redirection timeouts.
    var redirectTimeoutError = 10000;
    var redirectTimeoutSuccess = 4000;
    //console.log('redirectTimeout = ' + redirectTimeout);
    if (redirectTimeout !== undefined && redirectTimeout !== null) {
        redirectTimeoutError = redirectTimeout;
        redirectTimeoutSuccess = redirectTimeoutError - 3000;
    }
    //console.log('redirectTimeoutError = ' + redirectTimeoutError);

    var sessionId = $('.js-session-id').text();

    // For example, https://connect.teksystems.com/services/apexrest/tc_rest_abenity_sso/
    var requestUrl = browserUtils.retrieveBrowserProtocol() + '//' + 
        browserUtils.retrieveBrowserHost() + module.exports.ABENITY_SSO_SERVICE_URL_PATH;
    var isProd = envUtils.calcIsProd();
    if (!isProd) {
        var opCoSlug = envUtils.calcOpCo();
        // For example, https://test-allegisconnected.cs87.force.com/aerotek/services/apexrest/tc_rest_abenity_sso/
        requestUrl = browserUtils.retrieveBrowserProtocol() + '//' + 
            browserUtils.retrieveBrowserHost() + '/' + opCoSlug + module.exports.ABENITY_SSO_SERVICE_URL_PATH;
    }

    //console.log('jquery version = ' + $.fn.jquery);
    browserUtils.makeAjaxRequest({
        method: 'POST', 
        url: requestUrl, 
        data: null,
        headers: {
            'Authorization': 'Bearer ' + sessionId
        }, 
        contentType: 'application/json', 
        success: function(data, textStatus, xhr) {
            //console.log('data = ' + JSON.stringify(data));
            /*
             * When successful, we should receive back a short-lived URL that may be used to access 
             * Abenity as the context user. For example, 
             * {
             *     "success": "https://allegisbenefits.employeediscounts.co/perks/process/login?token=6t4UP4eoomoSveq3Bosm7j7XnP28AC4c"
             * }
             */

            //console.log('xhr = ' + JSON.stringify(xhr));
            var isSandbox = data.isSandbox;
            var abenityTempUrl = data.success;
            // If we're pointed at the Abenity sanbox, the returned URL won't actually work.
            console.log('Sandbox = ' + isSandbox);
            console.log('Redirecting to = ' + abenityTempUrl);
            if (!isProd) {
                /*
                 * When not in PROD, pause a few seconds to let the tester/developer see the destination URL
                 * in the browser console.
                 */
                setTimeout(function() {
                    browserUtils.populateBrowserHref(abenityTempUrl);
                    if (callback) { 
                        callback();
                    }
                }, redirectTimeoutSuccess);
            } else {
                browserUtils.populateBrowserHref(abenityTempUrl);
                if (callback) { 
                    callback();
                }
            }
        }, 
        error: function(xhr, textStatus, errorThrown) {
            // TODO send to log service
            console.log('error = ' + xhr.responseText);
            /*
             * Errors may be thrown from several tiers - Apex on Salesforce, PHP on Heroku, or Abenity.
             * Examples of errors raised directly from Salesforce: 
             * {
             *      "errorCode":"FORBIDDEN",
             *      "message":"You do not have access to the Apex class named: TC_RestAbenitySsoController"
             * }
             *
             * {
             *      "errorCode": "System.CalloutException", 
             *      "message": "Unauthorized endpoint, please check Setup->Security->Remote site settings. endpoint = https://radiant-bastion-67033.herokuapp.com/"
             * }
             *
             * Example of error raised from Abenity: 
             * {
             *     "messageData": {
             *         "error": {
             *             "u_surname": "Last Name is required",
             *             "u_firstname": "First Name is required",
             *             "u_email": "Email must be in a valid name@domain format"
             *         }
             *     },
             *     "message": "[See messageData for Abenity JSON response.]",
             *     "errorCode": "Abenity request error"
             * }
             */
            try {
                // Display an error message and send the user to the Abenity manual login page.
                $('.js-abenity-login-error-box').css('display', 'block');
                setTimeout(function() {
                    // Allow developers to short-circuit the redirect by appending '?stop' to the URL.
                    if (browserUtils.retrieveBrowserSearch().indexOf('?stop') < 0) {
                        browserUtils.populateBrowserHref(module.exports.ABENITY_FALLBACK_LOGIN_URL);
                    }
                    if (callback) { 
                        callback();
                    }
                }, redirectTimeoutError);
            } catch(e) {
                console.log(e);
                eventUtils.publishEvent('com.header.displayCatchAllGlobalError');
                if (callback) { 
                    callback();
                }
            }
        }, complete: function(xhr, textStatus) {
            
        }
    });
};

