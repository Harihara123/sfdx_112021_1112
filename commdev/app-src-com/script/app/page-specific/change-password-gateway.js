'use strict';

// Needed for the Bootstrap modal.
require('!bootstrap-webpack!./../../../../bootstrap.config-first.js');

require('jquery-validation');
var md5 = require('js-md5');

var authUtils = require("../common/auth-utils");
var loginUtils = require("../common/login-utils");

/**
 * Handle the "pageload" event for the change password gateway page.
 */
module.exports.handlePageLoaded = function() {
    //console.log('In handlePageLoaded for change-password-gateway');
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady); 

    // Inside of the referenced functions, bind "this" to a useful value.
    pubSub.subscribe('com.changePasswordGateway.onChangePasswordInput', 
        authUtils.handleOnChangePasswordInput.bind(authUtils));
	pubSub.subscribe('com.changePasswordGateway.onChangePasswordInputJobSeeker', 
        authUtils.handleOnChangePasswordInputJobSeeker.bind(authUtils));
    pubSub.subscribe('com.changePasswordGateway.onChangePasswordConfirmInput', 
        authUtils.handleOnChangePasswordConfirmInput.bind(authUtils));
    pubSub.subscribe('com.changePasswordGateway.onChangePasswordOldInput', 
        authUtils.handleOnChangePasswordOldInput.bind(authUtils));
    pubSub.subscribe('com.changePasswordGateway.onClickPasswordRevealInput', 
        authUtils.handleOnClickPasswordRevealInput.bind(authUtils));
    pubSub.subscribe('com.changePasswordGateway.onClickTermsCheckbox', 
        authUtils.handleOnClickTermsCheckbox.bind(authUtils));

    pubSub.subscribe('com.changePasswordGateway.onClickEmailAddressConfirm', 
        _handleOnClickEmailAddressConfirm);
    pubSub.subscribe('com.changePasswordGateway.onChangeEmailAddressInput', function(msg, data) {
        _handleOnChangeEmailAddressInput(data.evt);
    });
};

var _handleDomReady = function() {
    _condEngageEnhancedAuthentication();

    /*
     * In order to reduce the display "flicker" seen when enhanced authentication is 
     * engaged, the logo and change password form is hidden, by default. 
     */
    $('.js-auth-logo-box').css('display', 'block');
    $('.js-auth-form-box').css('display', 'block');

    authUtils.configFormInputElements();
    loginUtils.initAccordions();
    authUtils.configChangeListenersOnInputs('changePasswordGateway');
};

/**
 * "Enhanced authentication" means that the user is required to confirm the context email 
 * address. It is engaged when a "ea" parameter is included in the URL querystring.
 */
var _condEngageEnhancedAuthentication = function() {
    // It does not matter if the "ea" parameter is valued.
    if (window.location.search.indexOf('ea=') > -1) { 
        $('.js-change-password-box').css('display', 'none');
        $('.js-email-confirm-error').css('display', 'none');
        $('.js-enhanced-auth-box').css('display', 'block');

        /*
         * When enhanced authentication is engaged, the email display should be blanked-out 
         * using server-side logic. But, let's also blank it out using client-side logic, 
         * just to be safe. We do this to prevent a hacker from viewing the source to reveal 
         * the confirmation email address.
         */
        $('.js-email-display-box').html('');
    }
};

var _handleOnClickEmailAddressConfirm = function() {
    var emailDigestTgt = $('.js-email-digest-input').val();
    /*
     * In Salesforce, all email addresses are lowercase in the database. As such, force 
     * any user input to lowercase before hashing.
     */
    var emailConfirm = $('.js-email-confirm-input').val().trim().toLowerCase();
    var emailDigestSrc = md5.hex(emailConfirm);
    //console.log('emailDigestTgt = ' + emailDigestTgt);
    //console.log('emailConfirm = ' + emailConfirm);
    //console.log('emailDigestSrc = ' + emailDigestSrc);

    /*
     * If the hash of the given email address matches the hash of the expected email 
     * address, display the change password form; otherwise, display an error.
     */
    if (emailDigestSrc === emailDigestTgt) {
        //console.log('match!');

        /*
         * Take the confirmed email address and use it as the content of the email 
         * display. We need to do this dynamically so as to prevent a hacker from viewing the 
         * source to reveal the confirmation email address.
         */
        $('.js-email-display-box').html(emailConfirm);

        $('.js-enhanced-auth-box').css('display', 'none');
        $('.js-change-password-box').css('display', 'block');
    } else {
        //console.log('no match!');
        $('.js-email-confirm-error').css('display', 'block');
    }
};

/**
 * @param evt - A key-up event on which this function is invoked.
 */
var _handleOnChangeEmailAddressInput = function(evt) {
    /*
     * Ignore carriage returns (the user hitting enter to submit the form). Otherwise, on 
     * error, the error message would blink in/out of view.
     */
    if (evt.keyCode !== 13) {
        // Whenever the input email address changes, hide the mismatch error message.
        $('.js-email-confirm-error').css('display', 'none');
    }
};

