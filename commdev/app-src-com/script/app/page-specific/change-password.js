'use strict';

// Needed for the Bootstrap modal.
require('!bootstrap-webpack!./../../../../bootstrap.config-first.js');

require('jquery-validation');

var authUtils = require("../common/auth-utils");
var loginUtils = require("../common/login-utils");

/**
 * Handle the "pageload" event for the change password page.
 */
module.exports.handlePageLoaded = function() {
    //console.log('In handlePageLoaded for change-password');
    // Configure a DOM-ready handler for the page.
    $(document).ready(_handleDomReady); 

    // Inside of the referenced functions, bind "this" to a useful value.
    pubSub.subscribe('com.changePassword.onChangePasswordInput', 
        authUtils.handleOnChangePasswordInput.bind(authUtils));
	pubSub.subscribe('com.changePassword.onChangePasswordInputJobSeeker', 
        authUtils.handleOnChangePasswordInputJobSeeker.bind(authUtils));
    pubSub.subscribe('com.changePassword.onChangePasswordConfirmInput', 
        authUtils.handleOnChangePasswordConfirmInput.bind(authUtils));
    pubSub.subscribe('com.changePassword.onChangePasswordOldInput', 
        authUtils.handleOnChangePasswordOldInput.bind(authUtils));
    pubSub.subscribe('com.changePassword.onClickPasswordRevealInput', 
        authUtils.handleOnClickPasswordRevealInput.bind(authUtils));
    pubSub.subscribe('com.changePassword.onClickTermsCheckbox', 
        authUtils.handleOnClickTermsCheckbox.bind(authUtils));
};

var _handleDomReady = function() {
    authUtils.configFormInputElements();
    loginUtils.initAccordions();
    authUtils.configChangeListenersOnInputs('changePassword');

    /*
     * In order to reduce the display "flicker" seen when enhanced authentication is 
     * engaged, the logo and change password form is hidden, by default. "Enhanced 
     * authentication" means that the user is required to confirm the context email 
     * address. It is engaged when a "ea" parameter is included in the URL querystring.
     */
    $('.js-auth-logo-box').css('display', 'block');
    $('.js-auth-form-box').css('display', 'block');
};

