'use strict';

require("add-to-homescreen");

var assignmentEndDateNotice = require("../common/assignment-end-date-notice");
var dashboardUtils = require("../common/dashboard-utils");
var uiUtils = require("../common/ui-utils");

/**
 * Handle the "pageload" event for the dashboard for an OpCo.
 *
 * @param jobSearchDaysCnt - The maximum age (in days) of any jobs included in the search results 
 *  displayed by the jobs teaser card.
 */
module.exports.handlePageLoaded = function(jobSearchDaysCnt) {
	// Configure a DOM-ready handler for the page.
	$(document).ready(function() {
        _handleDomReady(jobSearchDaysCnt);
    }); 

    skuid.events.subscribe('com.dashboard.onClickAssignEndNoticeConfirm', assignmentEndDateNotice.handleOnClickAssignEndNoticeConfirm);
    skuid.events.subscribe('com.dashboard.onClickAssignEndNoticeClose', assignmentEndDateNotice.handleOnClickAssignEndNoticeClose);
};

/**
 * @param jobSearchDaysCnt - The maximum age (in days) of any jobs included in the search results 
 *  displayed by the jobs teaser card.
 */
var _handleDomReady = function(jobSearchDaysCnt) {
    _ensureDesiredCardSizing();

    dashboardUtils.displayHeroBar();

    dashboardUtils.displayAssignmentStatusAndJobsTeaser(jobSearchDaysCnt);

    assignmentEndDateNotice.condDisplayAssignmentEndDateNotice();
    
    _promptMobileBookmark();
};

/**
 * Trigger widget that opens an overlaying message inviting the user to add the web site/application to the home 
 * screen (if on a smartphone).
 */
var _promptMobileBookmark = function() {
    addToHomescreen({
        skipFirstVisit: true,
        maxDisplayCount: 1
    });
};

/**
 * Take various steps to ensure that the on-screen cards are sized as desired.
 */
var _ensureDesiredCardSizing = function() {
    _resizeCards();

    // Allow the assignment status card to invoke the resize.
    skuid.events.subscribe("com.dashboard.resizeCards", _resizeCards);

    skuid.$(window).bind('resize', function(){
        _resizeCards();
    });
};

/**
 * Make all cards in a row the same height.
 */
var _resizeCards = function() {
    uiUtils.smoothHeightOfRowContents('.js-card-height', '.c-card');
};
