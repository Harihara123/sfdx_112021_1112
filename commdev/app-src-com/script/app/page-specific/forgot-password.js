'use strict';

var browserUtils = require("../common/browser-utils");
var globalNotifications = require("../common/global-notifications");
var loginUtils = require("../common/login-utils");
var envUtils = require("../common/env-utils");
var templateUtils = require("../common/template-utils");

require('jquery-validation');

/**
 * Handle the "pageload" event for the forgot password page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady); 
};

var _handleDomReady = function() {
	globalNotifications.initGlobalNotifications();
    loginUtils.popupForInactive();
	// Apply out-of-the-box validation support to the form on the page.
	$('.validate').validate();
};
