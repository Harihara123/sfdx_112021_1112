'use strict';

var svg4everybody = require("svg4everybody");

var assignmentStatus = require("../common/assignment-status");
var domManip = require("../common/dom-manip");
var globalNotifications = require("../common/global-notifications");
var i18nUtils = require('../common/i18n-utils');
var modelUtils = require('../common/model-utils');
var templateUtils = require("../common/template-utils");
var uiUtils = require("../common/ui-utils");

var globalNavBarTemplate = require('../templates/global-navigation-bar.hbs');
var headerMenuTemplate = require('../templates/header-menu.hbs');

/**
 * Handle the "pageload" event for the header "page".
 */
module.exports = {

	handlePageLoaded: function(callback) {
		_ensureJqueryAvailable();

		// Configure a DOM-ready handler for the page.
		$(document).ready(function() {
	        _handleDomReady(callback);
	    }); 
	}

};

/**
 * Handle the DOM-ready callback.
 *
 * @param callback - Invoked upon completion of all logic. Initially added to facilitate unit testing.
 */
var _handleDomReady = function(callback) {
	_displayGlobalNavigationAndMenu();

	// Enable SVG support for IE v11.
	svg4everybody();

    if ('ontouchstart' in document.documentElement) {
		$('body').css('cursor', 'pointer');
	}

	globalNotifications.initGlobalNotifications();
	_configureMenuClickHandlers();

    if (callback && typeof callback === 'function') { 
        callback();
    }
};

/**
 * Ensure that jQuery is available. This only becomes an issue on Salesforce-standard pages where 
 * the header is included, but Skuid is not (at least not at the top-level). For example, the 
 * profile page.
 */
var _ensureJqueryAvailable = function() {
	if (typeof $ === 'undefined' && typeof skuid !== 'undefined') {
		$ = skuid.$;
	}
}

var _replaceHeaderMergeFields = function() {
	/*
	 * Allow the text to be customized by OpCo while also supporting i18n.
	 * TODO - Detect the context OpCo so this scales.
	 */
	var referFriendTagline = i18nUtils.retrieveText('TC_refer_a_friend_tagline');
	var referFriendTaglineAerotek = i18nUtils.retrieveText('TC_refer_a_friend_tagline_aerotek');
	var mergeMap = {
		'TC_refer_a_friend_tagline': referFriendTagline, 
		'TC_refer_a_friend_tagline_aerotek': referFriendTaglineAerotek
	};
	var domEle = domManip.replaceMergeFields('header-refer-friend-tagline-box', mergeMap);

	/*
	 * The modified DOM element is made invisible by default so the placeholder text does not show. 
	 * Now that the placeholder has been replaced, show the DOM element.
	 */
	uiUtils.makeVisible(domEle);
};

var _displayHeaderMenu = function() {
	$('.c-sidebar-menu').toggleClass('is-active');
	$('html').toggleClass('u-overflow-hidden');
	$('#menu-toggle').toggleClass('is-active');

	$('body').one('click', _closeHeaderMenu);
};

var _closeHeaderMenu = function() {
	$('.c-sidebar-menu').removeClass('is-active');
	$('html').removeClass('u-overflow-hidden');
	$('#menu-toggle').removeClass('is-active');
};

var _displayGlobalNavigationAndMenu = function() {
    var templCtx = templateUtils.buildBaseTemplateContext();
    
    // TODO Centralize if required in more templates. (Beware of circular dependencies.)
    /*
     * RunningUser is defined in the header Skuid page, so should always be defined.
     */
    var user = modelUtils.retrieveModelData('RunningUser');
    var isTalentFormer = assignmentStatus.calcTalentIsFormer(user);
    templCtx.isTalentFormer = isTalentFormer;
    var isTalentFormerNotRecent = assignmentStatus.calcTalentIsFormerNotRecent(user);
    templCtx.isTalentFormerNotRecent = isTalentFormerNotRecent;

    var userId = modelUtils.retrieveUserId();
    templCtx.userId = userId;

    var globalNavBarMarkup = globalNavBarTemplate(templCtx);
    var headerMenuMarkup = headerMenuTemplate(templCtx);
    $(".js-global-nav-bar-box").append(globalNavBarMarkup);
    $(".js-header-menu-box").append(headerMenuMarkup);

	_replaceHeaderMergeFields();
};

var _configureMenuClickHandlers = function() {
    $('#menu-toggle').bind('click', function(){
		_displayHeaderMenu();
	});

	$('.c-sidebar-menu, #menu-toggle').bind('click', function(e) {
		e.stopPropagation();
	});
};
