'use strict';

var moment = require('moment');

var articleUtils = require("../common/article-utils");

/**
 * Handle the "pageload" event for the "Help Article" page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady);
};

var _handleDomReady = function() {
	articleUtils.registerArticleSkuidComponent('faqArticle', 'FAQArticle', 'help', _renderArticle);

	var articleComp = skuid.component.getByType('faqArticle')[0];
	articleComp.render();
};

/**
 * Render a help article.
 *
 * @param row - Backing model object for the article to be rendered.
 */
var _renderArticle = function(row) {
    var html = '<a href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/tc_help" class="u-viewall__back">' + skuid.$L('TC_View_All') + '</a>';
    html += '<h2 class="u-margin-top-40 u-margin-bottom-20">' + row.Title + '</h2>';
    html += '<h3 class="u-margin-bottom-20">Last updated: ' +  moment(row.LastPublishedDate).format("dddd, MMMM Do, YYYY") + '</h3>';
    html += '<div class="c-knowledge-article__content">' + row.Description__c + '</div>';

    return html;
};
