'use strict';

/**
 * Handle the "pageload" event for the "Help" page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady);
};

var _handleDomReady = function() {

};
