'use strict';

//var jQuery = require('noop-loader?jquery');
// Needed for the Bootstrap tooltip.
require('!bootstrap-webpack!./../../../../bootstrap.config.js');

var _ = require('lodash/core');
var moment = require('moment');
require('twbs-pagination');

var skuidUIHelpers = require("../../../../app-src/helpers/skuid/ui");

var envUtils = require("../common/env-utils");
var globalNotifications = require('../common/global-notifications');
var templateUtils = require('../common/template-utils.js');
var uiUtils = require("../common/ui-utils");
var validationUtils = require("../common/validation-utils");

var reqDetailsTemplate = require('../templates/job-search-req-details.hbs');
var expressInterestCartTemplate = require('../templates/job-search-express-interest-cart-ligh.hbs');
var searchFormTemplate = require('../templates/job-search-form.hbs');
var shareJobModalTemplate = require('../templates/job-search-share-job-modal.hbs');
var searchResultsTemplate = require('../templates/job-search-results.hbs');
var searchResultsHeaderTemplate = require('../templates/job-search-results-header.hbs');
var searchResultsFooterTemplate = require('../templates/job-search-results-footer.hbs');

/**
 * Handle the "pageload" event for the job search page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(function() {
        _handleDomReady();
    }); 

    /*
     * Total hack to deal with $(document).ready() not being called on mobile when the page is accessed 
     * with a cleared cache. It works fine on desktop and everywhere when the page is refreshed or 
     * revisited.
     */
    setTimeout(function() {
        if (handleDomReadyCalled === false) {
            _handleDomReady();
        }
    }, 1000);
};

/**
 * State object to hold the jobs selected by the user. Since this array is localized to the component,
 * it will persist through pagination, filtration, and search operations (which are Ajax-based), but
 * will be cleared whenever the user leaves the job search page. This is as desired.
 */
var _cartArr = [];

/**
 * The currently-displayed reqs. Maintained at class level so that when a req is selected to be shared, the 
 * details for the req can be accessed for display in the share-job modal.
 */
var _displayedReqsArr = [];

/**
 * The previously-entered city and state, recorded for safe-keeping when the user sets "within" to 
 * "everywhere" and retrieved when the user sets "within" to a specific distance.
 */
var _previousCity, _previousState = null;

/**
 * Supporting flag for our hack to deal with job search not rendering on mobile when accessed with a 
 * cleared cache.
 */
var handleDomReadyCalled = false;

/**
 * Centralize the jQuery selectors to help prevent typos.
 */
var uiSelectors = {
    // Form-level errors for the job search form.
    jobSearchFormLevelErrorsParent: '#jobsearch-form .js-form-level-errors', 
    // Form-level errors for the share-a-job form.
    shareJobFormLevelErrorsParent: '#jobsearch-share-job-modal .js-form-level-errors'
};

var _handleDomReady = function() {
    handleDomReadyCalled = true;
    
    var templCtx = templateUtils.buildBaseTemplateContext();
    $('#jobsearch-form').html(searchFormTemplate(templCtx));
    $('#jobsearch-express-interest-cart-ligh').html(expressInterestCartTemplate(templCtx));

    var RunningUser = skuid.$M('RunningUser');
    var RunningUser_row = RunningUser.getFirstRow();
    var userTitle = RunningUser_row.Contact.Title ? RunningUser_row.Contact.Title : ' ';
    var userCity = RunningUser_row.Contact.MailingCity ? RunningUser_row.Contact.MailingCity : ' ';
    var userState = RunningUser_row.Contact.MailingState ? RunningUser_row.Contact.MailingState.toUpperCase() : '';

    skuid.$('#tsearch1').val(userTitle);
    skuid.$('#citysearch').val(userCity);
    skuid.$('#statesearch').val(userState);

    /*
     * No need to check for blank title/keywords. The back-end will do a search for all jobs if not supplied.
     */
    _jobSearch();

    skuid.$('#js-express-stickyfooter__expresscount').html(_getCartCount());
    _contactMeButton(_getCartCount());

    $('#radiussearch').bind('change', _handleOnChangeWithin);

	skuid.events.subscribe("com.jobsearch.searchForReqs", _submitTalentSearch);
	skuid.events.subscribe("com.jobsearch.selectReq", _addToCart);
	skuid.events.subscribe("com.jobsearch.expressInterest", _expressJobInterest);
	skuid.events.subscribe("com.jobsearch.expressInterestligh", _expressJobInterestligh);
	
	skuid.events.subscribe("com.jobsearch.filterReqs", function(data) {
        /*
         * Pass in a page number to prevent the search facets from being re-rendered. We want to 
         * maintain them unless the user initiates a new search (from the search form).
         */
        _jobSearch(1);
    });
    skuid.events.subscribe("com.jobsearch.shareJobSelect", function(data) {
        _showShareJobModal(data.reqId);
    });
    skuid.events.subscribe("com.jobsearch.shareJob", function(data) {
        _shareJob(data.reqId);
    });
    skuid.events.subscribe("com.jobsearch.toggleReqDescriptionDisplay", function(data) {
        _toggleReqDescriptionDisplay(data.reqId);
    });

	$(function() {
        var $popoverEles = $('[data-toggle="popover"]');
        /*
         * If the Bootstrap/jQuery popover plug-in fails to initialize for whatever reason, don't let it 
         * cause the entire page to error.
         */
        if ($popoverEles.popover) {
            $popoverEles.popover({
                'placement': 'auto',
                'content': 'no tooltip content provided'
            });
        }
	});

	skuid.events.publish("com.library.jqueryPaginationPlugin.hasLoaded");
};

/**
 * Handle the change event for the "within" input.
 */
var _handleOnChangeWithin = function() {
    if ($(this).val() === 'Everywhere'){
        /*
         * If "within" is set to "eveywhere", disable and empty the city and state inputs, but keep any 
         * cleared-out values for later retrieval.
         */
        $('#citysearch, #statesearch').attr('disabled', 'disabled');
        _previousCity = $('#citysearch').val();
        _previousState = $('#statesearch').val();
        $('#citysearch').val('');
        $('#statesearch').val('');
    } else {
        /*
         * If "within" is set to a specific distance, enable the city and state inputs and populate them 
         * with any previously-saved values. Only populate them if the inputs are blank, as the user could 
         * be changing from one specific distance to another.
         */
        $('#citysearch, #statesearch').removeAttr('disabled');
        var $cityInput = $('#citysearch');
        if ($cityInput.val().trim() === '') {
            $cityInput.val(_previousCity);
        }
        var $stateInput = $('#statesearch');
        if ($stateInput.val() === null || $stateInput.val().trim() === '') {
            $stateInput.val(_previousState);
        }
    }
};

var _submitTalentSearch = function(){
    // Remove any pre-existing error messages.
    skuidUIHelpers.removeErrors();

    _markRequiredJobSearchInputs();

    var requiredInputsErrorMsg = validationUtils.validateRequiredFields(document.getElementById('jobsearch-form'));
    if (requiredInputsErrorMsg.trim().length > 0) {
        var jobSearchFormLevelErrorsParent = $(uiSelectors.jobSearchFormLevelErrorsParent)[0];
        validationUtils.displayFormLevelError(requiredInputsErrorMsg, jobSearchFormLevelErrorsParent);
    } else {
        /*
         * The user is initiating a new search (from the search form). Clear any checked search facets 
         * before calling the server so that no facets are applied to the search.
         */
        $('.c-search-navigators input:checkbox').prop('checked', false);

        _jobSearch();
    }
};

/**
 * Dynamically determine and mark those job search inputs which are required.
 */
var _markRequiredJobSearchInputs = function() {
    var withinInputVal = $('#jobsearch-form #radiussearch').val();
    // If "within" is not "everywhere", city and state are required.
    if (withinInputVal !== 'Everywhere') {
        $('#jobsearch-form #citysearch').attr('required', 'required');
        $('#jobsearch-form #statesearch').attr('required', 'required');
    } else {
        $('#jobsearch-form #citysearch').removeAttr('required');
        $('#jobsearch-form #statesearch').removeAttr('required');
    }
};

/**
 * Call server to search for jobs.
 *
 * @param pageNbr - Optional page of results to return. If not specified, the first page is assumed. 
 */
var _jobSearch = function(pageNbr){
    uiUtils.showLoadingMask();

    // build filter array
    var filterList = '';

    var filterArr = $('.c-search-navigators input:checkbox:checked').map(function() {
        return $(this).val();
    }).get();

    var filterList = filterArr.join(',');
    var userCity = $('#citysearch').val();
    var userState = $('#statesearch').val();

    // Set search results conditions
    // TODO: get this model from the component xmlDef
    var m = skuid.$M('jobsearch');


    var mccity = m.getConditionByName('city',true);
    var mcstate = m.getConditionByName('state',true);
    var mcdistance = m.getConditionByName('distance',true);
    var mcfilters = m.getConditionByName('filters',true);
    var mcpageNumber = m.getConditionByName('pgnum',true);

    m.setCondition(mccity, jQuery('#citysearch').val(), false);
    m.setCondition(mcstate, jQuery('#statesearch').val(), false);
    m.setCondition(mcdistance, jQuery('#radiussearch').val(), false);

    //if there are filters applied activate filter condition
    if (filterArr.length > 0) {
        m.setCondition(mcfilters, filterList, false);
    } else {
        m.deactivateCondition(mcfilters);
    }

    //set Pagination
    if (pageNbr) {
        m.setCondition(mcpageNumber,pageNbr,false);
    } else {
        m.setCondition(mcpageNumber,1,false);
    }

    // Get values from inputs
    var keywords = jQuery('#tsearch1').val();

    if(keywords !== ''){
        var mckeywords = m.getConditionByName('keywords',true);
        m.setCondition(mckeywords,keywords,false);

        // Radius
        var radius = jQuery('#radiussearch').val();

        var mcradius = m.getConditionByName('radius',true);
        m.setCondition(mcradius,radius,false);

        var promise = m.updateData(function(){
            uiUtils.hideLoadingMask();

            var arr = [];
            arr.push(jQuery('#citysearch').val());
            arr.push(jQuery('#statesearch').val());

            arr = arr.filter(function(v){
                return v !== null && v !== ''
            });

            var location = arr.join(', ');

            jQuery('#tsearch1').attr('data-search-terms', keywords);
            jQuery('#tsearch1').attr('data-search-distance', radius);
            jQuery('#tsearch1').attr('data-search-location', location);
            jQuery('#tsearch1').attr('data-search-filters', filterList);
        });

        promise.done(function() {
            // See team/samples/job-search-response.json for a sample response.
            var response = m.data;
            var radius = response[0].searchParameters.radius

            if (radius === 'Everywhere') {
                _everywhereSearch();
            }

            /*
             * If a page number has been specified, we do NOT want to re-render the entire search results 
             * section of the page, which would cause the selected search facets to be lost. Instead, 
             * limit the page elements that are re-rendered.
             */
            var refreshAll = true;
            if (pageNbr) {
                refreshAll = false;
            }
            _refreshJobResultSection(response, refreshAll);
        });
    }

};

/**
 * Re-render the search results section of the page.
 *
 * @jsonResponse - Contains the server search response.
 * @refreshAll - True if the entire search results section of the page should be re-rendered, else 
 *  false if only a subset should be re-rendered. Our goal here is to prevent the search facets from 
 *  being re-rendered. We want to maintain them unless the user initiates a new search (from the search 
 *  form). When we request a facet-filtered search from the server, the response will reset the facet 
 *  options based on the facet-filtered results. We do not want this reduced set of facets options 
 *  reflected on-screen to the user.
 */
var _refreshJobResultSection = function(jsonResponse, refreshAll) {
    // Save the currently displayed reqs in an instance field for future reference.
    _displayedReqsArr = jsonResponse[0].jobs;

    var templCtx = templateUtils.buildBaseTemplateContext();
    templCtx.jobCount = jsonResponse[0].searchParameters.recordCount;
    templCtx.jobsFound = false;
    if (templCtx.jobCount > 0) {
        templCtx.jobsFound = true;
    }

    /*
     * If this is the initial/automatic search that is executed on page-load, we need to bootstrap 
     * some of the mark-up for the follow-on logic to work. We do this here instead of in the 
     * Skuid page because markup embedded in a Skuid page is XML-escaped (hard to read).
     */
    var existingContent = $("#jobResults").html();
    if (existingContent.length === 0) {
        $("#jobResults").append('<div id="job-results-header-box" />');
        $("#jobResults").append('<div id="job-results-and-facets-box" />');
        $("#jobResults").append('<div id="job-results-footer-box" />');
    }

    // Inject the search results header markup.
    var resultsHeaderMarkup = searchResultsHeaderTemplate(templCtx);
    $("#job-results-header-box").replaceWith(resultsHeaderMarkup);

    // Be selective with when we re-render the search facets.
    if (refreshAll) {
        templCtx.searchFacets = jsonResponse[0].nav;
        /*
         * If this is a user-initiated search (from the search form) that comes back with no results, 
         * don't show any search facets. (However, if the user selects a combination of facet options 
         * such that they get no results, we keep the search facets on-screen so that the user can 
         * unwind from their dead-end.
         */
        if (!templCtx.jobsFound) {
            templCtx.searchFacets = [];
        }

        // Get any selected search facet options from the Skuid model.
        var m = skuid.$M('jobsearch');
        var selectedFacets = _getParameterByName(m.query,'filters');
        selectedFacets = selectedFacets.substring(selectedFacets.indexOf(':') + 1);
        templCtx.selectedFacets = selectedFacets;

        var resultsAndFacetsMarkup = searchResultsTemplate(templCtx);
        $("#job-results-and-facets-box").replaceWith(resultsAndFacetsMarkup);
    }

    // Inject the search results footer markup.
    var resultsFooterMarkup = searchResultsFooterTemplate(templCtx);
    $("#job-results-footer-box").replaceWith(resultsFooterMarkup);

    // Inject the actual found/matched jobs markup.
    var foundJobsMarkup = _buildMarkupForFoundJobs(jsonResponse[0].jobs, _cartArr);
    $("#job-results-and-facets-box .js-search-results").empty().append(foundJobsMarkup);

    if(jsonResponse[0].jobs.length > 0) {
        // Now that the DOM has been modified/built, activate the pagination plugin.
        _initializePagination(jsonResponse[0].searchParameters);
    }

    $('input#tsearch1').focus();

    _configSelectInputsToSubmitFormOnEnter(['radiussearch', 'statesearch']);
};

/**
 * Configure the select inputs identified by the given the (DOM-compliant) ID's so that, if the 
 * user hits the enter/return key while focused on them, the form is submitted.
 *
 * @param selectInputsIdArray - Array of DOM-compliant (not jQuery-compliant) ID's. 
 */
var _configSelectInputsToSubmitFormOnEnter = function(selectInputsIdArray) {
    var limit = selectInputsIdArray.length;
    for (var i = 0; i < limit; i++) {
        var selectInputId = selectInputsIdArray[i];
        document.getElementById(selectInputId).onkeydown = function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $('#search-button').click();
            }
        };
    }
};

/**
 * Initialize the widget that allows the user to paginate through the search results. If there is only 
 * one page of search results, do not show the pagination widget.
 *
 * @param searchParams - Parameters passed to the search service which correspond with the results being 
 *  paged through.
 */
var _initializePagination = function(searchParams) {
    var totalPages = Math.ceil(searchParams.recordCount / searchParams.pagesize);

    // Prevent an infinite loop.
    //var maxPaginationInitTries = 100;
    //var paginationTriesCount = 0;

    var _initializeJQueryPagination = function() {
        var $paginator = $('.c-search-pagination__paginator');
        /*
         * Validate totalPages to prevent the error "Start page option is incorrect". Also, don't show 
         * pagination if there is only one page of results.
         */
        if (totalPages > 1) {
            // Initialize the pagination once, when it is empty.
            var $paginatorChildren = $paginator.children();
            if ($paginatorChildren.length === 0) {
                $('.c-search-pagination__paginator').twbsPagination({
                    totalPages: totalPages,
                    startPage: parseInt(searchParams.pgnum),
                    visiblePages: 5,
                    initiateStartPageClick: false,
                    first: '&laquo;',
                    prev: '&lsaquo;',
                    next: '&rsaquo;',
                    last: '&raquo;',
                    onPageClick: function (event, page) {
                        _jobSearch(page);
                    }
                });
            }
        } else {
            $paginator.empty();
        }
    };

    /*
     * Check for the presence of the jQuery pagination plugin. If not initially found, subscribe to an 
     * event which should fire when the plugin is loaded.
     */
    if ($.fn.twbsPagination) {
        _initializeJQueryPagination();
    } else {
        skuid.events.subscribe(
            "com.library.jqueryPaginationPlugin.hasLoaded",
            _initializeJQueryPagination
        );
    }
};

var _getParameterByName = function(query,name){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
    regex = new RegExp(regexS),
    results = regex.exec(query);
    if( results == null ){
        return '';
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
};

var _removeVal = function(arr, val) {
    for(var i = 0; i < arr.length; i++)
    {
        if (arr[i] == val)
            arr.splice(i, 1);
    }
}

var _addToCart = function() {

    // if job is checked, add it to the array; otherwise, remove it
    $('.c-search-results input[type=checkbox]').on('click', function(){
        if ($(this).is(':checked')) {
            if ($.inArray($(this).attr('id'), _cartArr)==-1) _cartArr.push($(this).attr('id'));
        } else {
            _removeVal(_cartArr, $(this).attr('id'));
        }
    });

    // update 'jobs selected' count in stickfooter
    $('#js-express-stickyfooter__expresscount').html( _getCartCount() );

    // enable/disable contact me button
    _contactMeButton(_getCartCount());

};

/**
 * Build and return markup for displaying the given array of reqs, marking those which have been selected 
 * by the user.
 *
 * @param reqsArr - Array of req objects (POJOs).
 * @param selectedReqIdsArr - Array of ID's for reqs that should be marked as selected.
 */
var _buildMarkupForFoundJobs = function(reqsArr, selectedReqIdsArr) {
    var result = '';

    _.forEach(reqsArr, function(ele, idx, collection) {
        // if reqId is in the local array, mark job checked
        ele.reqSelected = '';
        if ($.inArray(ele.reqId, selectedReqIdsArr) > -1) {
            ele.reqSelected = 'checked';
        }

        if (ele.qualifieddate && ele.qualifieddate.length > 0) {
            ele.reqDateStr = moment(ele.qualifieddate).format('MMMM D, YYYY');
        }

        if (ele.externalJobPosting) {
            var extDescriptionStr = ele.externalJobPosting;
            
            // Standardize the newlines/line breaks.
            extDescriptionStr = extDescriptionStr.replace(/\r\n/g,'\n');

            /*
             * Split the description by newlines and collect the results in an array, thereby allowing 
             * the description to be formatted nicely using HTML line breaks.
             */
            var extDescriptionArr = extDescriptionStr.split(/\n/);
            ele.extDescription = extDescriptionArr;

            var teaserLength = 80;
            if (extDescriptionStr.length > teaserLength) {
                // Create a shortened, teaser version of the description.
                ele.extDescriptionTease = extDescriptionStr.substring(0, teaserLength);
            }
        }

        if (ele.docVector) {
            _visuallyWeighDocVectors(ele);
        }
    });
    var templCtx = templateUtils.buildBaseTemplateContext();
    templCtx.reqs = reqsArr;
    // Allow the user to interact with and manipulate the list of reqs.
    templCtx.allowInteraction = true;
    result = reqDetailsTemplate(templCtx);

    return result;
};

/**
 * Construct markup for visually displaying the elements of the doc vector so that higher-weighted 
 * elements are signified as carrying more weight (e.g., larger font).
 * This is intended to be temporary code - to facilitate QA testing.
 */
var _visuallyWeighDocVectors = function(req) {
    try {
        var docVectorMarkupArr = [];
        // For example, "peoplesoft financials:1.0000;peoplesoft:0.92291886;developer:0.86539501;management:0.83630...""
        var docVectorSrcStr = req.docVector;
        var docVectorsArr = docVectorSrcStr.split(';');
        _.forEach(docVectorsArr, function(ele, idx, collection) {
            // For example, "developer:0.86539501"
            var docVectorArr = ele.split(':');
            // For example, "developer"
            var docVectorKey = docVectorArr[0];
            // For example, "0.86539501"
            var docVectorWeightStr = docVectorArr[1];
            var docVectorWeight = parseFloat(docVectorWeightStr);
            var displayPct = 100;
            if (docVectorWeight <= 0.250) {
                displayPct = 70;
            } else if (docVectorWeight <= 0.500) {
                displayPct = 90;
            } else if (docVectorWeight <= 0.750) {
                displayPct = 120;
            } else if (docVectorWeight <= 1) {
                displayPct = 150;
            }
            docVectorMarkupArr.push('<span style="font-size:' + displayPct + '%;" title="' + docVectorWeightStr + '">' + docVectorKey + '</span>');
        });
        req.docVector = docVectorMarkupArr.join(', ');
    } catch (e) {
        console.log('Error raised trying to parse the docVector for req ' + reqId);
        console.log(e);
    }
};

var _everywhereSearch = function() {
    $('#citysearch, #statesearch').attr('disabled', 'disabled');
    $("#radiussearch").val('Everywhere');
};

var _expressSuccess = function(){
    _expressReset();
    var sitePrefix = skuid.utils.mergeAsText('global','{{$Site.Prefix}}');
    window.location = sitePrefix + "/tc_jobs_thanks";
};

var _expressSuccessligh = function(){
    _expressReset();
    var sitePrefix = skuid.utils.mergeAsText('global','{{$Site.Prefix}}');
    window.location = sitePrefix + "/tc_jobs_lightening_thanks";
};

var _expressReset = function(){
    $('.js-express-searching').show();
    $('.js-express-success').hide();
    $('.js-no-results').hide();

    $('input:checkbox').removeAttr('checked');
    $('#js-express-stickyfooter__expresscount').html('0');

    $('.js-submit-job-interest').addClass('ui-state-disabled');
    $('.js-submit-job-interest').attr('disabled', 'disabled');

    // Clear the state object used to hold the jobs selected by the user.
    _cartArr = [];
};

var _expressJobInterest = function(showLoading){

    if(typeof showLoading === 'undefined'){
        showLoading = true;
    }

    if(showLoading){
        uiUtils.showLoadingMask();
    }

    // Pull in the jobs selected by the user.
    var reqIDs = _cartArr;

    // format reqIds as comma delimited list; xxxxx,xxxxx,xxxxx
    var formatted_reqIDs = reqIDs.join(',');

    // Create Talent_Saved_Job__c
    var tsj = skuid.$M('TalentSavedJobs');

    var runningUser = skuid.$M('RunningUser');
    var runningUser_row = runningUser.getFirstRow();

    var newRow = tsj.createRow({
        additionalConditions: [
            { field: 'Talent__c', value: runningUser_row.AccountId },
            { field: 'ReqIDs__c', value: formatted_reqIDs },
            { field: 'Search_Criteria__c', value: jQuery('#tsearch1').attr('data-search-terms') },
            { field: 'Location_Criteria__c', value: jQuery('#tsearch1').attr('data-search-location') },
            { field: 'Distance_Criteria__c', value: jQuery('#tsearch1').attr('data-search-distance') }
        ], doAppend: true
    });

    tsj.save({callback: function(result){
        if (result.totalsuccess) {
            _expressSuccess();
        } else {
            console.log('Request Failed: ', result.messages);
            console.log(result);
            $('.c-search-summary').empty().append('&lt;p&gt;Something went wrong with your request. The Community Mananger has been notified. Please try again later.&lt;/p&gt;');
            $('.js-express-stickyfooter').find('.row').empty();
            uiUtils.hideLoadingMask();
        }
    }});

};


var _expressJobInterestligh = function(showLoading){

    if(typeof showLoading === 'undefined'){
        showLoading = true;
    }

    if(showLoading){
        uiUtils.showLoadingMask();
    }

    // Pull in the jobs selected by the user.
    var reqIDs = _cartArr;

    // format reqIds as comma delimited list; xxxxx,xxxxx,xxxxx
    var formatted_reqIDs = reqIDs.join(',');

    // Create Talent_Saved_Job__c
    var tsj = skuid.$M('TalentSavedJobs');

    var runningUser = skuid.$M('RunningUser');
    var runningUser_row = runningUser.getFirstRow();

    var newRow = tsj.createRow({
        additionalConditions: [
            { field: 'Talent__c', value: runningUser_row.AccountId },
            { field: 'ReqIDs__c', value: formatted_reqIDs },
            { field: 'Search_Criteria__c', value: jQuery('#tsearch1').attr('data-search-terms') },
            { field: 'Location_Criteria__c', value: jQuery('#tsearch1').attr('data-search-location') },
            { field: 'Distance_Criteria__c', value: jQuery('#tsearch1').attr('data-search-distance') }
        ], doAppend: true
    });

    tsj.save({callback: function(result){
        if (result.totalsuccess) {
            _expressSuccessligh();
        } else {
            console.log('Request Failed: ', result.messages);
            console.log(result);
            $('.c-search-summary').empty().append('&lt;p&gt;Something went wrong with your request. The Community Mananger has been notified. Please try again later.&lt;/p&gt;');
            $('.js-express-stickyfooter').find('.row').empty();
            uiUtils.hideLoadingMask();
        }
    }});

};

/**
 * Retrieve the number of jobs selected by the user.
 */
var _getCartCount = function() {
    // _cartArr is initialized in the jobsearch component (tc_skuid_components).
    var cartCount = _cartArr.length;

    return cartCount;
};

var _contactMeButton = function(cartCount) {
    if(cartCount > 0){
        // show request buttons
        $('.js-submit-job-interest').removeClass('ui-state-disabled');
        $('.js-submit-job-interest').removeAttr('disabled');
    } else {
        // hide request buttons
        $('.js-submit-job-interest').addClass('ui-state-disabled');
        $('.js-submit-job-interest').attr('disabled', 'disabled');
    }
};

/**
 * Display a modal via which the user may share the context req with a friend.
 *
 * @param reqId - The ID of the currently-displayed req that has been selected to be shared.
 */
var _showShareJobModal = function(reqId) {
    console.log('selected req ID = ' + reqId);

    var templCtx = templateUtils.buildBaseTemplateContext();
    templCtx.reqId = reqId;
    var ctxReqsArr = _.filter(_displayedReqsArr, function(ele, idx, coll) {
        return ele.reqId === reqId;
    });

    if (ctxReqsArr.length !== 1) {
        skuid.events.publish('com.header.displayCatchAllGlobalError');
    } else {
        var templCtx2 = templateUtils.buildBaseTemplateContext();
        templCtx2.reqs = ctxReqsArr;
        // Present a view-only display of the req details.
        templCtx2.allowInteraction = false;
        templCtx.reqDetailsMarkup = reqDetailsTemplate(templCtx2);

        // Need to clear out previously-bound and displayed DOM fragment.
        $("#jobsearch-share-job-modal .slds-modal").remove();
        $("#jobsearch-share-job-modal .slds-backdrop").remove();
        
        $("#jobsearch-share-job-modal").append(shareJobModalTemplate(templCtx));

        $('#jobsearch-share-job-modal .slds-modal').addClass('slds-fade-in-open');
        $('#jobsearch-share-job-modal .slds-backdrop').addClass('slds-backdrop--open');

        $('input#share-job-first-name-input').focus();
    }
};

/**
 * Share the identified req. 
 * TODO Now that the request JSON has been flattened, convert to using Skuid REST model. 
 */
var _shareJob = function(reqId) {
    console.log('sharing req ID ' + reqId);
    globalNotifications.clearGlobalNotifications();

    /*
     * Don't try to validate the email address client-side, as we might not match up with the validation on the 
     * server. More here (in the comments): 
     *  http://stackoverflow.com/questions/46155/validate-email-address-in-javascript#46181
     * Also, this: http://www.regular-expressions.info/email.html
     */
    var requiredInputsErrorMsg = validationUtils.validateRequiredFields(document.getElementById('share-job-form'));
    if (requiredInputsErrorMsg.trim().length > 0) {
        var shareJobFormLevelErrorsParent = $(uiSelectors.shareJobFormLevelErrorsParent)[0];
        validationUtils.displayFormLevelError(requiredInputsErrorMsg, shareJobFormLevelErrorsParent, 'jobsearch-share-job-modal');
    } else {
        var messagingContainerDomSelector = '#refer-friend-form-field-group';

        uiUtils.showLoadingMask();

        // Remove any pre-existing error messages.
        skuidUIHelpers.removeErrors();
        skuidUIHelpers.removeErrorsInline();

        var friendFirstName = $('#share-job-first-name-input').val();
        var friendLastName = $('#share-job-last-name-input').val();
        var friendEmailAddr = $('#share-job-email-input').val();

        var RunningUserModel = skuid.$M('RunningUser');
        var RunningUser = RunningUserModel.getFirstRow();
        var talentId = RunningUser.AccountId;

        var shareJobModel = _buildShareJobModel(reqId, friendFirstName, friendLastName, friendEmailAddr, talentId);
        var serviceUrlPath = '/services/apexrest/CreateTalentJobReferral/';
        // For example, https://connect.teksystems.com/services/apexrest/CreateTalentJobReferral/
        var requestUrl = window.location.protocol + '//' + window.location.host + serviceUrlPath;
        var isProd = envUtils.calcIsProd();
        if (!isProd) {
            var opCoSlug = envUtils.calcOpCo();
            // For example, https://test-allegisconnected.cs87.force.com/aerotek/services/apexrest/CreateTalentJobReferral/
            requestUrl = window.location.protocol + '//' + window.location.host + '/' + opCoSlug + serviceUrlPath;
        }

        $.ajax({
            method: 'POST', 
            url: requestUrl, 
            data: JSON.stringify(shareJobModel),
            headers: {
                'Authorization': 'Bearer ' + skuid.utils.userInfo.sessionId
            }, 
            contentType: 'application/json', 
            success: function(data, textStatus, xhr) {
                uiUtils.closeModal();
                var shareJobSuccessMsg = skuidUIHelpers.labelFromOrFail("TC_share_job_success");
                skuid.events.publish('com.header.displayGlobalSuccess', [{ notifContent: shareJobSuccessMsg }]);
            }, 
            error: function(xhr, textStatus, errorThrown) {
                console.log('error = ' + xhr.responseText);
                try {
                    var errorObj = JSON.parse(xhr.responseText);
                    var errorMsg = errorObj.message;
                    if (errorMsg && errorMsg.indexOf('INVALID_EMAIL_ADDRESS') > -1) {
                        /*
                         * Salesforce's response to an invalid email address: 
                         *  {"errorCode": "System.DmlException", "message": "Insert failed. First exception on row 0; first error: INVALID_EMAIL_ADDRESS, Email: invalid email address: foo: [Email__c]"}
                         */
                        var errorMsg = skuidUIHelpers.labelFromOrFail("TC_error_email_invalid");
                        errorMsg += friendEmailAddr;
                    }
                    var shareJobFormLevelErrorsParent = $(uiSelectors.shareJobFormLevelErrorsParent)[0];
                    validationUtils.displayFormLevelError(errorMsg, shareJobFormLevelErrorsParent, 'jobsearch-share-job-modal');
                } catch(e) {
                    //console.log(e);
                    skuid.events.publish('com.header.displayCatchAllGlobalError');
                }
            }, complete: function(xhr, textStatus) {
                uiUtils.hideLoadingMask();
            }
        });
    }
};

/**
 * Build and return the model that needs to be sent in the share-a-job request to the back-end.
 *
 * @param reqId - ID of the req being shared.
 * @param talentId - ID of the context talent user who is sharing the job.
 */
var _buildShareJobModel = function(reqId, friendFirstName, friendLastName, friendEmailAddr, talentId) {
    var result = {
        "FirstName" : friendFirstName,
        "LastName" : friendLastName,
        "sharedJob" : true,
        "reqId" : reqId,
        "talentid" : talentId,
        "Email": friendEmailAddr
    };
    return result;
};

/**
 * Toggle the display/hiding of the external description of the req with the given ID.
 *
 * @param reqId - The ID of the req that is to be hidden or displayed.
 */
var _toggleReqDescriptionDisplay = function(reqId) {
    /*
     * By default, a truncated teaser for the job description is shown. The display of the teaser and full 
     * description are mutually exclusive of each other.
     */
    var $jobDescTeaseEle = $('[data-job-desc-tease-id="' + reqId + '"]');
    var $jobDescFullEle = $('[data-job-desc-full-id="' + reqId + '"]');
    if ($jobDescFullEle.hasClass('hidden')) {
        $jobDescFullEle.removeClass('hidden');
        $jobDescTeaseEle.addClass('hidden');
    } else {
        $jobDescFullEle.addClass('hidden');
        $jobDescTeaseEle.removeClass('hidden');
    }
};
