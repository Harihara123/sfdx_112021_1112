'use strict';

var skuidModelHelpers = require("../../../../app-src/helpers/skuid/model");
var skuidUIHelpers = require("../../../../app-src/helpers/skuid/ui");

var libOpt = require("../../../../app-src/library/optional");
var libText = require("../../../../app-src/library/text");

/**
 * Handle the "pageload" event for the "Job Search Thanks" page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady);
};

var _handleDomReady = function() {
	_registerJobApplyThanksSkuidComponent();

	/*
	 * Retrieve and execute/render the Skuid component, inserting the resultant DOM fragment 
	 * into the DOM where the component is referenced.
	 */
	var thanksComp = skuid.component.getByType('thankYouMessage')[0];
	thanksComp.render();
};

/**
 * Register a Skuid component for rendering the job search apply "thanks" page.
 * TODO Remove Skuid component aspect, as there is no need for this to be done via a component.
 */
var _registerJobApplyThanksSkuidComponent = function() {
	skuid.componentType.register('thankYouMessage', function(element) {

	    var userModel = libOpt.asSomeOrFail(skuidModelHelpers.getModelOpt("RunningUser"), libText.emptyString);
	    var userRow = skuidModelHelpers.getFirstRow(userModel);

	    // default message; show this unless the model returns an active recruiter
	    var recruiterTmpl = ('<p>' + skuidUIHelpers.labelFromOrFail('TC_Jobsearch_Thanks_No_Recruiter') + '</p>');

	    // set recruiter request status
	    var recruiterRequested = false;
	    if (userRow.Contact.Recruiter_Change_Requested__c && 
	        !((userRow.Contact.Recruiter_Change_Requested__c === '') || (userRow.Contact.Recruiter_Change_Requested__c === 'yes'))) {
	        recruiterRequested = true;
	    }

	    // check if user has recruiter and no recruiter has been requested
	    if (userRow.Account.Talent_Recruiter__r && recruiterRequested === false) {

	        // check if recruiter is active
	        if(userRow.Account.Talent_Recruiter__r.IsActive) {

	            recruiterTmpl = $('<p>')
	            	.append('<p>' + skuidUIHelpers.labelFromOrFail('TC_Jobsearch_Thanks') + '</p>')
	            	.append('<p>' + skuidUIHelpers.labelFromOrFail('TC_Have_questions_Please_contact') + '</p>');

	        }
	    }

	    element.append(recruiterTmpl);
	});
};
