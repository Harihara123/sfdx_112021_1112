'use strict';

var _ = require('lodash/core');

var browserUtils = require('../common/browser-utils');
var envUtils = require('../common/env-utils');
var eventUtils = require('../common/event-utils');
var i18nUtils = require('../common/i18n-utils');
var modelUtils = require('../common/model-utils');
var templateUtils = require('../common/template-utils');
var uiUtils = require("../common/ui-utils");

var pageContentTemplate = require('../templates/linkedin-import-choices.hbs');

/**
 * Handle the "pageload" event for the LinkedIn import choices page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady); 

    eventUtils.subscribeToEvent('com.linkedInImport.onFormCancel', _handleOnCancelLinkedInImport);
    eventUtils.subscribeToEvent('com.linkedInImport.onFormSubmit', _handleOnSubmitLinkedInImport);
    eventUtils.subscribeToEvent('com.linkedInImport.onClickSelectAll', _handleOnClickSelectAll);
    eventUtils.subscribeToEvent('com.linkedInImport.onClickChoiceCheckbox', _handleOnClickChoiceCheckbox);

    /*
     * Listen for the global error message being closed, as we want to manually hide the header 
     *  at the same time.
     */
    eventUtils.subscribeToEvent('com.header.clearGlobalError', _handleOnClearGlobalError);
};

var _handleDomReady = function() {
    var templCtx = templateUtils.buildBaseTemplateContext();

    templCtx.isTalentAstonCarter = (envUtils.calcOpCo().toLowerCase() == 'aston carter' || envUtils.calcOpCo().toLowerCase() == 'astoncarter' || envUtils.calcOpCo().toLowerCase() == 'teksystems');

    $(".js-content-box").append(pageContentTemplate(templCtx));
};

var _handleOnCancelLinkedInImport = function() {
    _sendUserToPrivateUserProfilePage();
};

var _sendUserToPrivateUserProfilePage = function() {
    var user = modelUtils.retrieveModelData('RunningUser');
    browserUtils.populateBrowserPath(envUtils.calcSitePrefix() + "/" + user.Id);
};

/**
 * Transfer values from LinkedIn to the user's profile. The LinkedIn data is expected to be already 
 * captured and embedded in the JSON stored in Account.Talent_Preference__c. So, this essentially 
 * amounts to copying values from one portion of the JSON structure to another. The specific values 
 * to copy are determined by the user's on-screen selections - which can be all or some.
 */
var _handleOnSubmitLinkedInImport = function() {
    if (_calcAnyInputsChecked()) {
        uiUtils.showLoadingMask();
        /*
         * It is expected that the source LinkedIn data is available, embedded in 
         *  the JSON-formatted Account.Talent_Preference__c. For an example, see 
         *  team/samples/account.talent-prefs.json
         */
        var accountObj = modelUtils.retrieveModelData('AccountLocal');
        var prefsJson = accountObj.Talent_Preference__c;
        var prefsObj = JSON.parse(prefsJson);
        var liData = prefsObj.linkedin;
        //console.log('liData = ' + liData);
        if (typeof liData !== 'undefined') {
            var contactDataUpdates = _buildUpdatesToContact(liData);
            var accountDataUpdates = _buildUpdatesToAccount(liData, prefsObj);

            if (!_.isEmpty(contactDataUpdates) || !_.isEmpty(accountDataUpdates)) {
                if (!_.isEmpty(contactDataUpdates)) {
                    var contactModel = modelUtils.retrieveModelDefinition('ContactForUpdateLocal');
                    /*
                     * This Contact model is _not_ configured to query for data on page load. As such, 
                     *  load _from_ the server before saving _to_ the server.
                     */
                    modelUtils.fetchModelData([contactModel], function() {
                        _updateAndSaveData(contactDataUpdates, accountDataUpdates);
                    });
                } else {
                    _updateAndSaveData(contactDataUpdates, accountDataUpdates);
                }
            } else {
                // Should not happen, given that we already verified that something is checked.
                console.log("No updates found for import.");
                uiUtils.hideLoadingMask();
                _sendUserToPrivateUserProfilePage();
            }
        } else {
            console.log("No LinkedIn data available for import.");
            _displayGlobalError();
        }
    } else {
        console.log('Nothing selected for import.');
        _sendUserToPrivateUserProfilePage();
    }
};

/**
 * For those checkboxes which have been checked/selected, copy over the corresponding data 
 *  from the given LinkedIn data to a target object, ready to be applied to the context 
 *  Contact model.
 */
var _buildUpdatesToContact = function(linkedInData) {
    var contactDataUpdates = {};
    if ($('.js-preferred-name-input').prop('checked')) {
        contactDataUpdates.Preferred_Name__c = linkedInData.firstName;
    }
    if ($('.js-title-input').prop('checked')) {
        contactDataUpdates.Title = linkedInData.headline;
    }
    if ($('.js-linkedin-url-input').prop('checked')) {
        contactDataUpdates.LinkedIn_URL__c = linkedInData.publicProfileUrl;
    }
    if ($('.js-summary-input').prop('checked')) {
        contactDataUpdates.About_Me__c = linkedInData.summary;
    }
    return contactDataUpdates;
};

/**
 * For those checkboxes which have been checked/selected, copy over the corresponding data 
 *  from the given LinkedIn data to a target object, ready to be applied to the context 
 *  Account model.
 *
 * @param linkedInData - Source LinkedIn data.
 * @param prefsObj - JSON-able object which is suitable for being set as the value of 
 *  Account.Talent_Preference__c.
 */
var _buildUpdatesToAccount = function(linkedInData, prefsObj) {
    var accountDataUpdates = {};
    if ($('.js-industry-input').prop('checked')) {
        /* 
         * Get the existing comma-separated "industries" string and append the LinkedIn-
         *  supplied industry to it.
         */
        var existingIndustriesStr = prefsObj.industries.data;
        if (existingIndustriesStr === null || existingIndustriesStr.length === 0) {
            existingIndustriesStr = linkedInData.industry;
        } else {
            existingIndustriesStr = existingIndustriesStr + ',' + linkedInData.industry;
        }

        // Replace the existing "industries" string in the user preferences object.
        prefsObj.industries.data = existingIndustriesStr;
        /*
         * Set the JSON preferences string on the target object. For an example, see 
         *  team/samples/account.talent-prefs.json
         */
        accountDataUpdates.Talent_Preference__c = JSON.stringify(prefsObj);
    }
    return accountDataUpdates;
};

/**
 * Update and save the appropriate model(s). Either Account, Contact, or both could be 
 *  updated here.
 * @param contactDataUpdates - Contains updates to be applied to the context Contact model.
 * @param accountDataUpdates - Contains updates to be applied to the context Account model.
 */
var _updateAndSaveData = function(contactDataUpdates, accountDataUpdates) {
    var contactUpdatePromise = _buildDoNothingPromise();
    var accountUpdatePromise = _buildDoNothingPromise();

    if (!_.isEmpty(contactDataUpdates)) {
        var contactModel = modelUtils.retrieveModelDefinition('ContactForUpdateLocal');
        var contactObj = modelUtils.retrieveModelData(contactModel);
        modelUtils.updateModelValues(contactObj, contactDataUpdates, contactModel);

        // Try to save the model.
        contactUpdatePromise = modelUtils.saveModelData(null, null, contactModel);
    }

    if (!_.isEmpty(accountDataUpdates)) {
        var accountModel = modelUtils.retrieveModelDefinition('AccountLocal');
        var accountObj = modelUtils.retrieveModelData('AccountLocal');
        modelUtils.updateModelValues(accountObj, accountDataUpdates, accountModel);

        // Try to save the model.
        accountUpdatePromise = modelUtils.saveModelData(null, null, accountModel);
    }

    /*
     * Wait for (possibly) both requests to complete before declaring victory. If 
     *  either one of them fails, we display an error. Unfortunately, if both Account and 
     *  Contact are being updated, one could theoretically succeed and the other could 
     *  fail. To make this fool-proof, the update logic would have to be pushed down 
     *  into an Apex class.
     */
    Promise.all([contactUpdatePromise, accountUpdatePromise])
    .then(function() {
        // Save successful.
        uiUtils.hideLoadingMask();
        _sendUserToPrivateUserProfilePage();
    })
    .catch(function(e) {
        console.log(e)
        _displayGlobalError();
    });
};

/**
 * Build and return an immediately-responding then-able object.
 */
var _buildDoNothingPromise = function() {
    var promise = {
        then: function(callback) {
            if (callback) {
                callback();
            }
        }
    };
    return promise;
};

var _handleOnClickSelectAll = function() {
    var $selectAllCheckbox = $('.js-select-all-input');
    var isChecked = $selectAllCheckbox.prop('checked');
    // Set all of the checkboxes to the same checked state of the "select all" checkbox.
    $('input:checkbox').prop('checked', isChecked);
    // If all of the choices are unchecked, disable the submit button.
    $('.js-submit-button').prop('disabled', !isChecked);
    //$('.js-submit-button').val(!isChecked);
};

var _calcAnyInputsChecked = function() {
    var result = false;
    var checkboxes = $('input:checkbox');
    var checkedInputs = _.filter(checkboxes, function(ele, idx, coll) {
        return $(ele).prop('checked');
    });
    if (checkedInputs.length > 0) {
        result = true;
    }
    return result;
};

var _handleOnClearGlobalError = function() {
    /*
     * On the LinkedIn import choices page, the header and footer are hidden. However, 
     *  the current implementation of the global error is embedded inside the header. 
     *  So, if/when the error needs to be displayed, we make the header visible. But, 
     *  if the user dismisses the error message, we want to hide the header again. 
     *  Not the cleanest presentation, but, for now, it does the job.
     */
    $('.c-header-wrapper').css('display', 'none');
};

var _displayGlobalError = function() {
    uiUtils.hideLoadingMask();
    eventUtils.publishEvent('com.header.displayCatchAllGlobalError');

    /*
     * On the LinkedIn import choices page, the header and footer are hidden. However, 
     *  the current implementation of the global error is embedded inside the header. 
     *  So, if/when the error needs to be displayed, we make the header visible.
     */
    $('.c-header-wrapper').css('display', 'block');
};

var _handleOnClickChoiceCheckbox = function() {
    var allChoicesUnchecked = _.every($('input:checkbox'), function(ele, idx, coll) {
        var unchecked = true;
        var $ele = $(ele);
        if (!$ele.hasClass('js-select-all-input')) {
            unchecked = !$ele.prop('checked');
        }
        return unchecked;
    });
    //console.log('allChoicesUnchecked = ' + allChoicesUnchecked);

    // If all of the choices are unchecked, disable the submit button.
    $('.js-submit-button').prop('disabled', allChoicesUnchecked);
    //$('.js-submit-button').val(allChoicesUnchecked);
};
