'use strict';

var browserUtils = require("../common/browser-utils");
var globalNotifications = require("../common/global-notifications");
var loginUtils = require("../common/login-utils");
var envUtils = require("../common/env-utils");
var templateUtils = require("../common/template-utils");

/**
 * Handle the "pageload" event for the login page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady); 
};

var _handleDomReady = function() {
	_initRememberUserCredsHandlers();
	_autopopulateUserCreds();
	loginUtils.initAccordions();
    globalNotifications.initGlobalNotifications();
    loginUtils.popupForInactive();
};

/**
 * Initialize the "remember me" functionality/handlers.
 */
var _initRememberUserCredsHandlers = function() {
    // Save creds when the "remember" button is checked
    $('#remember').on('change', function() {
        console.log('_initRememberUserCredsHandlers: remember change');
        _rememberOrClearUserCreds();
    });

    $('.username, .password').on('change, keyup', function(){
        console.log('_initRememberUserCredsHandlers: username/password change');
        _rememberOrClearUserCreds();
    });
};

/**
 * If available, auto-populate the login form using the locally-stored credentials.
 */
var _autopopulateUserCreds = function() {
    if (browserUtils.retrieveLocalStorage().checkBoxValidation && browserUtils.retrieveLocalStorage().checkBoxValidation != '') {
        $('#remember').attr('checked', 'checked');
        $('.username').val(browserUtils.retrieveLocalStorage().userName);
        $('.password').val(browserUtils.retrieveLocalStorage().password);
    }
};

/**
 * Store in- or clear out of local storage any values currently found in the login form, 
 * as dictated by the state of the toggle checkbox.
 */
var _rememberOrClearUserCreds = function() {
    if ($('#remember').is(':checked')) {
        // save username and password
        browserUtils.retrieveLocalStorage().userName = $('.username').val();
        browserUtils.retrieveLocalStorage().password = $('.password').val();
        browserUtils.retrieveLocalStorage().checkBoxValidation = $('#remember').val();
    } else {
        browserUtils.retrieveLocalStorage().userName = '';
        browserUtils.retrieveLocalStorage().password = '';
        browserUtils.retrieveLocalStorage().checkBoxValidation = '';
    }
};
