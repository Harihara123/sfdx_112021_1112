'use strict';

var moment = require('moment');

var articleUtils = require("../common/article-utils");

/**
 * Handle the "pageload" event for the "News Article" page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady);
};

var _handleDomReady = function() {
	articleUtils.registerArticleSkuidComponent('newsArticle', 'NewsArticle', 'news', _renderArticle);

	var articleComp = skuid.component.getByType('newsArticle')[0];
	articleComp.render();
};

/**
 * Render a news article.
 *
 * @param row - Backing model object for the article to be rendered.
 */
var _renderArticle = function(row) {
    var html = '<a href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/tc_news" class="u-viewall__back">' + skuid.$L('TC_View_All') + '</a>';
    html += '<h2 class="u-margin-top-40 u-margin-bottom-20">' + row.Title + '</h2>'
    html += '<h3 class="u-margin-bottom-20">' +  moment(row.CreatedDate).format("dddd, MMMM Do, YYYY") + '</h3>'
    html += '<p class="c-knowledge-article__content">' + row.Description__c + '</p>'

    return html;
};
