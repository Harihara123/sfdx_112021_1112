'use strict';

//var moment = require('moment');

/**
 * Handle the "pageload" event for the "News" page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady);

	/*
	skuid.events.subscribe("com.news.onRenderArticle", function(data) {
	    _handleOnRenderArticle(data.articleView);
	});
	*/
};

var _handleDomReady = function() {

};

/*
var _handleOnRenderArticle = function(articleView) {
	console.log('4');
	articleView.render = _renderArticle;
};

var _renderArticle = function( item ) {
	console.log('5');
    var row = item.row,
        model = item.list.model;

    var article = item.element
        .addClass( 'c-knowledge-table__row' );

    // list articles
    $( '<a>' ).addClass('c-knowledge-table__article').attr('href', skuid.utils.mergeAsText('global','{{$Site.Prefix}}') + '/tc_news_article?id=' + item.row.UrlName ).append([
        $('<h2>').text( item.row.Title )
    ])
    .append([
        $('<p>').addClass('c-knowledge-table__date').text( moment( item.row.CreatedDate ).format("dddd, MMMM Do, YYYY") )
    ])
    .append([
        $('<div>').addClass('c-knowledge-table__summary').text( item.row.Summary )
    ])
    .appendTo( article );
    console.log('6');
};
*/