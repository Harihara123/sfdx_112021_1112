'use strict';

var moment = require('moment');

/**
 * Handle the "pageload" event for the "Profile Wizard" page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady);
	
	skuid.events.subscribe("com.profileWizard.onShowEditEndDate", function(data) {
        _handleOnShowEditEndDate(data.endDateStr);
    });
	skuid.events.subscribe("com.profileWizard.onChangeEndDate", _handleOnChangeEndDate);
	skuid.events.subscribe("com.profileWizard.onSaveEndDate", _handleOnChangeEndDate);
};

var _handleDomReady = function() {

};

var _handleOnShowEditEndDate = function(endDateStr) {
	// populate year select

	var currentYear = new Date().getFullYear();
	for (var i = currentYear; i <= (currentYear + 20); i++){
	    $('#js-date-picker-year').append($('<option />').val(i).html(i));
	}

	/**
	 * Auto populate end date request form if talent has a date populated
	 */

	var parsedDate = moment(endDateStr);

	if(!parsedDate.isValid()){
	    parsedDate = moment(endDateStr, 'DD/MM/YYYY');
	}

	if(parsedDate.isValid()){
	    // Only prepopulate if the talent assignment end date is in the future
	    if(parsedDate.diff(moment(), 'days') >= 0){
	        var year,
	            month,
	            day;

	        year = parsedDate.format('YYYY');
	        day = parsedDate.format('DD');
	        month = parsedDate.format('MM');

	        $('#js-date-picker-day').val(day);
	        $('#js-date-picker-month').val(month);
	        $('#js-date-picker-year').val(year);
	    }
	}
};

/**
 * Create flag string denoting that talent requested a new end date
 * @return {void}
 */
var _handleOnChangeEndDate = function() {

    // Did they check "I don't know"?
    var unknown = $('#js-end-date-unknown').is(':checked');

    var datestring;
    if(unknown){
        datestring = 'I don\'t know';
    } else {
        var datestring = jQuery('#js-date-picker-year').val() + '-' + jQuery('#js-date-picker-month').val() + '-' + jQuery('#js-date-picker-day').val();
    }

    jQuery('.js-date-picker-target').val(datestring);
};

