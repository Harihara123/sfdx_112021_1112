'use strict';

var skuidModelHelpers = require("../../../../app-src/helpers/skuid/model");
var skuidUIHelpers = require("../../../../app-src/helpers/skuid/ui");

var libOpt = require("../../../../app-src/library/optional");
var libText = require("../../../../app-src/library/text");

var domManip = require("../common/dom-manip");
var uiUtils = require("../common/ui-utils");

var skuidUiHelpersCom = require("../../helpers/skuid/skuid-ui");

/**
 * Handle the "pageload" event for the "Refer a Friend" page.
 */
module.exports.handlePageLoaded = function() {
	// Configure a DOM-ready handler for the page.
	$(document).ready(_handleDomReady);

	skuid.events.subscribe("com.referFriend.onSave", _handleOnSaveTalentReferral);
	skuid.events.subscribe("com.referFriend.referAnother", _handleOnClickReferAnother);
};

var _handleDomReady = function() {
	_replaceReferFriendMergeFields();
};

var _replaceReferFriendMergeFields = function() {
	var runningUserModel = skuid.model.getModel('RunningUser').getFirstRow();
	var contactName = runningUserModel['Contact']['Full_Name__c'];
	domManip.replaceMergeFields('refer-friend-call-to-action-box', {'talentName': contactName});
};

/**
 * Handle the save action for the Talent_Referral model.
 */
var _handleOnSaveTalentReferral = function() {
	var messagingContainerDomSelector = '#refer-friend-form-field-group';

	uiUtils.showLoadingMask();

	// Remove any pre-existing error messages.
	skuidUIHelpers.removeErrors();
	skuidUIHelpers.removeErrorsInline();

	var talentReferralModel = libOpt.asSomeOrFail(skuidModelHelpers.getModelOpt("Talent_Referral"), libText.emptyString);

	// Try to save the model.
	skuidModelHelpers.save(talentReferralModel)
	.then(function() {
		// Save successful.
		uiUtils.hideLoadingMask();
		uiUtils.scrollToTop();

		// If the default Skuid validation did not raise any issues...
		if (!skuidUiHelpersCom.anyEditorErrorMessagesDisplayed(messagingContainerDomSelector)) {
			// Hide the initial copy, form, and submit button.
			$('#refer-friend-call-to-action-box').addClass('hidden');
			$('#refer-friend-explain-box').css('display', 'none');
			$('#refer-friend-form-field-group').css('display', 'none');
			$('#refer-friend-submit-button-box').css('display', 'none');

			// Show the success messaging and post-success buttons.
			$('#refer-friend-submit-success-box').removeClass('hidden');
			$('#refer-friend-success-buttons-box').removeClass('hidden');
			$('#refer-friend-success-buttons-box').addClass('c-refer-a-friend__call-to-action');
			
			// Reset/blank the form.
			var savedRow = skuidModelHelpers.getFirstRow(talentReferralModel);
			talentReferralModel.abandonRow(savedRow);
			talentReferralModel.createRow({'editModeForNewItems': true});
		}
	})
	.catch(function() {
		// Save failed. Skuid shows errors automatically.
		uiUtils.hideLoadingMask();
		uiUtils.scrollToTop();
	});

};

/**
 * Allow the user to refer another friend by restoring the display back to what it showed on initial 
 * render. 
 */
var _handleOnClickReferAnother = function() {
	/*
	 * This could have been handled in a simpler fashion by just reloading the page. However, this 
	 * impl is more responsive (faster).
	 */

	// Restore the initial copy, form, and submit button.
	$('#refer-friend-call-to-action-box').removeClass('hidden');
	$('#refer-friend-explain-box').css('display', 'block');
	$('#refer-friend-form-field-group').css('display', 'block');
	$('#refer-friend-submit-button-box').css('display', 'block');

	// Hide the success messaging and post-success buttons.
	$('#refer-friend-submit-success-box').addClass('hidden');
	$('#refer-friend-success-buttons-box').addClass('hidden');
	$('#refer-friend-success-buttons-box').removeClass('c-refer-a-friend__call-to-action');
};
