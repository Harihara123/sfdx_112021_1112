'use strict';

var _ = require('lodash/core');

/**
 * Handlebars helper used to determine if the given delimited text contains the specified 
 * value.
 *
 * @param delimitedText - The delimited text to be searched. For example, "one,two,three".
 * @param value - The value for which the given delimited text is to be searched.
 * @param delimiter - The delimeter which is to be used to tokenize the delimitedText. 
 *	For example, ",", ":", "|", etc.
 * @param checkboxMode - True if a true (boolean) result should be returned as "checked" 
 *	and a false (boolean) result should be returned as "". Else, boolean values are 
 *	returned. By setting checkboxMode to true, the result of this helper can be used to 
 *	set the "checked" state of an HTML checkbox input form element.
 */
module.exports = function (delimitedText, value, delimiter, checkboxMode) {
	var result = false;
	if (delimitedText !== undefined && delimiter !== undefined) {
		var textArr = delimitedText.split(delimiter);
		if (_.indexOf(textArr, value) > -1) {
			result = true;
		}
	}

	if (checkboxMode) {
		result = (result) ? 'checked' : '';
	}

    return result;
};