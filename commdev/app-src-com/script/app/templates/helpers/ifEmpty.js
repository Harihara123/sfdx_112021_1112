'use strict';

/**
 * Handlebars helper to be used to determine if a context property is blank.
 * For example: 
 *	{{#ifEmpty keywords}}
 *		...
 *	{{/ifEmpty}}
 */
module.exports = function (testSubject, options) {
    if (testSubject === null || testSubject.length <= 0) {
    	// Execute/render the contained content.
    	return options.fn(this);
    } else {
    	return '';
    }
};