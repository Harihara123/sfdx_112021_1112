'use strict';

/**
 * Handlebars helper to be used to determine if a context property is not blank.
 * For example: 
 *	{{#ifNotEmpty keywords}}
 *		...
 *	{{/ifNotEmpty}}
 */
module.exports = function (testSubject, options) {
    if (testSubject && testSubject.length > 0) {
    	// Execute/render the contained content.
    	return options.fn(this);
    } else {
    	return '';
    }
};