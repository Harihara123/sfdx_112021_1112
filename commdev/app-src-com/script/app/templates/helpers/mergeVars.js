'use strict';

/* 
 * Need the compiler, which is not included in the runtime. More info: 
 *	https://github.com/wycats/handlebars.js/issues/953
 */
var Handlebars = require('handlebars/dist/handlebars');

/**
 * Handlebars helper to be used to replace nested expressions within once-resolved expressions. 
 * This is (originally) being built to replace expressions within Custom Label values. For 
 * example, if a template contains the following: 
 *	<span>{{$Label.TC_approaching_end_date_notice}}</span>
 * and $Label.TC_approaching_end_date_notice, in turn, contains the following: 
 *	Your anticipated end date is approaching within {!endDateDaysCount} days.
 * Using the mergeVars helper, both expressions will be evaluated: 
 * 	<span>{{mergeVars $Label.TC_approaching_end_date_notice}}</span>
 * begets
 *	<span>Your anticipated end date is approaching within 12 days.</span>
 */
module.exports = function (varContainingText, options) {
	var result = '';
	if (varContainingText !== undefined) {
		var template = Handlebars.compile(varContainingText, options);
		// 'this' is the template context.
		result = template(this);
	}
    return result;
};