'use strict';

// Globally expose so that inline JavaScript can be embedded in each page to publish events.
var pubSub = require("expose?pubSub!pubsub-js");

var globalNotifications = require("./common/global-notifications");

/**
 * This could have been simplified by having inline Javascript on each relevant page directly 
 * invoke the handler functions. However, the benefit of this model is that it mirrors the 
 * architecture of the JavaScript on the Skuid pages.
 */
module.exports = {

    /**
     * Configure at least one "handler" per page, triggered by subscribing to pub/sub events.
     */
    wireUpPageHandlers: function() {

        this._wireUpLazilyLoadedHandlers();

        pubSub.subscribe("com.header.displayCatchAllGlobalError", function(msg, data) {
            globalNotifications.displayNotificationCatchAllError(data);
        });

        // Subscribe to requests to show/hide global notifications.
        pubSub.subscribe("com.header.displayGlobalError", function(msg, data) {
            data = data || {};
            globalNotifications.displayNotificationError(data.notifContent, data.initiatorEleId, data.closeEvent, data.scrollToDomEle);
        });

        pubSub.subscribe("com.header.displayGlobalInfo", function(msg, data) {
            data = data || {};
            globalNotifications.displayNotificationInfo(data.notifContent, data.initiatorEleId, data.closeEvent);
        });

        pubSub.subscribe("com.header.displayGlobalSuccess", function(msg, data) {
            data = data || {};
            globalNotifications.displayNotificationSuccess(data.notifContent, data.initiatorEleId, data.closeEvent);
        });

        pubSub.subscribe("com.header.displayGlobalWarning", function(msg, data) {
            data = data || {};
            globalNotifications.displayNotificationWarning(data.notifContent, data.initiatorEleId, data.closeEvent);
        });

        pubSub.subscribe("com.header.clearGlobalError", function(msg, data) {
            globalNotifications.clearNotificationError();
        });

        pubSub.subscribe("com.header.clearGlobalInfo", function(msg, data) {
            globalNotifications.clearNotificationInfo();
        });

        pubSub.subscribe("com.header.clearGlobalSuccess", function(msg, data) {
            globalNotifications.clearNotificationSuccess();
        });

        pubSub.subscribe("com.header.clearGlobalWarning", function(msg, data) {
            globalNotifications.clearNotificationWarning();
        });

        pubSub.subscribe("com.header.clearGlobalNotificationsForInitiator", function(msg, data) {
            data = data || {};
            globalNotifications.clearNotificationsForInitiator(data.initiatorEleId);
        });

    }, 

    /**
     * Wire up handlers that are configured in a way to support "code splitting". This allows modules to 
     * only be loaded when needed. Supply the third argument to require.ensure so that the generated chunk 
     * bundles have meaningful names. More info: https://webpack.github.io/docs/code-splitting.html
     */
    _wireUpLazilyLoadedHandlers: function() {
        pubSub.subscribe(
            "com.changePassword.hasLoaded",
            function(msg, data) {
                require.ensure(["./page-specific/change-password"], function(require) {
                    require("./page-specific/change-password").handlePageLoaded();
                }, "change-password");
            }
        );

        pubSub.subscribe(
            "com.changePasswordGateway.hasLoaded",
            function(msg, data) {
                require.ensure(["./page-specific/change-password-gateway"], function(require) {
                    require("./page-specific/change-password-gateway").handlePageLoaded();
                }, "change-password-gateway");
            }
        );

        pubSub.subscribe(
            "com.forgotPassword.hasLoaded",
            function(msg, data) {
                require.ensure(["./page-specific/forgot-password"], function(require) {
                    require("./page-specific/forgot-password").handlePageLoaded();
                }, "forgot-password");
            }
        );

        pubSub.subscribe(
            "com.login.hasLoaded",
            function(msg, data) {
                require.ensure(["./page-specific/login"], function(require) {
                    require("./page-specific/login").handlePageLoaded();
                }, "login");
            }
        );
    }
}
