'use strict';

var domManip = require("./common/dom-manip");
var globalNotifications = require('./common/global-notifications');
var header = require('./page-specific/header');
var uiUtils = require("./common/ui-utils");

module.exports = {

    /**
     * Configure at least one "handler" per Skuid page, triggered by subscribing to Skuid events.
     */
    wireUpPageHandlers: function() {

        skuid.events.subscribe(
            "com.header.hasLoaded",
            header.handlePageLoaded
        );

        skuid.events.subscribe("com.header.displayCatchAllGlobalError", function(data) {
            globalNotifications.displayNotificationCatchAllError(data);
        });

        // Subscribe to requests to show/hide global notifications.
        skuid.events.subscribe("com.header.displayGlobalError", function(data) {
            globalNotifications.displayNotificationError(data.notifContent, data.initiatorEleId, data.closeEvent, data.scrollToDomEle);
        });

        skuid.events.subscribe("com.header.displayGlobalInfo", function(data) {
            globalNotifications.displayNotificationInfo(data.notifContent, data.initiatorEleId, data.closeEvent);
        });

        skuid.events.subscribe("com.header.displayGlobalSuccess", function(data) {
            globalNotifications.displayNotificationSuccess(data.notifContent, data.initiatorEleId, data.closeEvent);
        });

        skuid.events.subscribe("com.header.displayGlobalWarning", function(data) {
            globalNotifications.displayNotificationWarning(data.notifContent, data.initiatorEleId, data.closeEvent);
        });

        skuid.events.subscribe("com.header.clearGlobalError", function(data) {
            globalNotifications.clearNotificationError();
        });

        skuid.events.subscribe("com.header.clearGlobalInfo", function(data) {
            globalNotifications.clearNotificationInfo();
        });

        skuid.events.subscribe("com.header.clearGlobalSuccess", function(data) {
            globalNotifications.clearNotificationSuccess();
        });

        skuid.events.subscribe("com.header.clearGlobalWarning", function(data) {
            globalNotifications.clearNotificationWarning();
        });

        skuid.events.subscribe("com.header.clearGlobalNotificationsForInitiator", function(data) {
            globalNotifications.clearNotificationsForInitiator(data.initiatorEleId);
        });

        skuid.events.subscribe(
            "com.global.modalClose", function(data) {
                data = (!data) ? {} : data;
                uiUtils.closeModal(data.modalId);
            }
        );

        this._wireUpLazilyLoadedHandlers();

    }, 

    /**
     * Wire up handlers that are configured in a way to support "code splitting". This allows modules to 
     * only be loaded when needed. Supply the third argument to require.ensure so that the generated chunk 
     * bundles have meaningful names. More info: https://webpack.github.io/docs/code-splitting.html
     */
    _wireUpLazilyLoadedHandlers: function() {
        skuid.events.subscribe(
            "com.abenityLoginRedirect.hasLoaded",
            function() {
                require.ensure(["./page-specific/abenity-login-redirect"], function(require) {
                    require("./page-specific/abenity-login-redirect").handlePageLoaded();
                }, "abenity-login-redirect");
            }
        );

        skuid.events.subscribe(
            "com.dashboard.hasLoaded",
            function(data) {
                require.ensure(["./page-specific/dashboard"], function(require) {
                    require("./page-specific/dashboard").handlePageLoaded(data.jobSearchDaysCnt);
                }, "dashboard");
            }
        );

        skuid.events.subscribe(
            "com.help.hasLoaded",
            function() {
                require.ensure(["./page-specific/help"], function(require) {
                    require("./page-specific/help").handlePageLoaded();
                }, "help");
            }
        );

        skuid.events.subscribe(
            "com.helpArticle.hasLoaded",
            function() {
                require.ensure(["./page-specific/help-article"], function(require) {
                    require("./page-specific/help-article").handlePageLoaded();
                }, "help-article");
            }
        );

        skuid.events.subscribe(
            "com.jobSearch.hasLoaded",
            function() {
                require.ensure(["./page-specific/job-search"], function(require) {
                    require("./page-specific/job-search").handlePageLoaded();
                }, "job-search");
            }
        );
		
		skuid.events.subscribe(
            "com.jobSearchlightening.hasLoaded",
            function() {
                require.ensure(["./page-specific/job-search-lightening"], function(require) {
                    require("./page-specific/job-search-lightening").handlePageLoaded();
                }, "job-search-lightening");
            }
        );

        skuid.events.subscribe(
            "com.jobSearchThanks.hasLoaded",
            function() {
                require.ensure(["./page-specific/job-search-thanks"], function(require) {
                    require("./page-specific/job-search-thanks").handlePageLoaded();
                }, "job-search-thanks");
            }
        );

        skuid.events.subscribe(
            "com.linkedInImportChoices.hasLoaded",
            function() {
                require.ensure(["./page-specific/linkedin-import-choices"], function(require) {
                    require("./page-specific/linkedin-import-choices").handlePageLoaded();
                }, "linkedin-import-choices");
            }
        );

        skuid.events.subscribe(
            "com.news.hasLoaded",
            function() {
                require.ensure(["./page-specific/news"], function(require) {
                    require("./page-specific/news").handlePageLoaded();
                }, "news");
            }
        );

        skuid.events.subscribe(
            "com.newsArticle.hasLoaded",
            function() {
                require.ensure(["./page-specific/news-article"], function(require) {
                    require("./page-specific/news-article").handlePageLoaded();
                }, "news-article");
            }
        );

        skuid.events.subscribe(
            "com.profileWizard.hasLoaded",
            function() {
                require.ensure(["./page-specific/profile-wizard"], function(require) {
                    require("./page-specific/profile-wizard").handlePageLoaded();
                }, "profile-wizard");
            }
        );

        skuid.events.subscribe(
            "com.referFriend.hasLoaded",
            function() {
                require.ensure(["./page-specific/refer-friend"], function(require) {
                    require("./page-specific/refer-friend").handlePageLoaded();
                }, "refer-friend");
            }
        );

   }
}
