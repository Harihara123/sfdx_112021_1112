'use strict';

var _ = require('lodash/core');

var skuidUiHelpersCom = require("../../helpers/skuid/skuid-ui");
var validationUtils = require("../../common/validation-utils");

/**
 * Skuid Model-specific functions.
 */
module.exports = {

	/**
	 * Build and return the SFDC remoting request data object, with the model(s) embedded inside as the body. 
	 * This is shamelessly (foolishly?) re-creating the raw remoting Ajax call made by Skuid.
	 *
	 * @param modelsArr - Array of models to be included in the request body.
	 * @param serviceUrlPath - Path of the web service to be targetted by the request.
	 * @param httpMethod - Most likely POST or PUT.
	 */
	buildRemotingRequestData: function(modelsArr, serviceUrlPath, httpMethod) {
	    var sfdcRestDataSource = skuid.service.getService('SFDCREST');
	    var sfdcRestUrl = sfdcRestDataSource.serviceUrl;

	    /*
	    var shareJobRequest = {
	        records: modelsArr
	    };
	    */
	    // The web service is only coded to support a single request object.
	    var shareJobRequest = modelsArr[0];

	    var remoteReqData = {
	       "url":sfdcRestUrl + serviceUrlPath,
	       "headers":{
	          "User-Agent":navigator.userAgent,
	          // {{$User.sessionId}}
	          "Authorization":"Bearer " + skuid.utils.userInfo.sessionId,
	          "Content-Type":"application/json"
	       },
	       "method":httpMethod,
	       "modelService":"SFDCREST",
	       "contentType":"application/json",
	       "body":JSON.stringify(shareJobRequest)
	    };

	    var vfRemoteProvider = Visualforce.remoting.last;
	    var stubsArr = vfRemoteProvider.actions['skuid.RemotingStubs'].ms;
	    var proxyStub = _.find(stubsArr, function(ele, idx, collection) {
	        return ele.name === 'proxy';
	    });
	    var csrf = proxyStub.csrf;

	    var remoteReq = {
	       "action":"skuid.RemotingStubs",
	       "method":"proxy",
	       "data":[JSON.stringify(remoteReqData)],
	       "type":"rpc",
	       /*
	        * Probably not a good idea to hard-code tid: 
	        *  http://salesforce.stackexchange.com/questions/80232/what-are-tid-and-len-in-visual-force-remoting-actions
	        */
	       "tid":3,
	       "ctx":{
	          "csrf":csrf,
	          "vid":vfRemoteProvider.id,
	          "ns":"skuid",
	          "ver":36
	       }
	    };

	    return remoteReq;
	},

	/**
	 * Handle the remoting response, which could be an error wrapped in a 200 (success) response.
	 *
	 * @param responseData - jQuery XHR response data object.
	 * @param initiatorEleId - The ID of the initiating element, if any. By specifying an initiating 
	 *  element for a notification, this allows the notification to be cleared if/when the initiating 
	 *  element is destroyed/removed. The provided value need not be a jQuery-style selector.
	 * @param formLevelErrorDomEle - DOM element off of which form level errors should hang.
	 * @param successCallback - Function to be invoked if the response communicates success.
	 */
	handleRemotingResponse: function(responseData, initiatorEleId, formLevelErrorDomEle, successCallback) {
	    /* 
	     * See the following under team/samples/sfdc-remoting/ for examples: 
	     * - empty-request-error.json
	     * - rest-api-error.json
	     * - rest-api-perm-error.json
	     */
	    //console.log('responseData = ' + JSON.stringify(responseData));
	    
	    if (_.isArray(responseData)) {
	        var responseType = responseData[0].type;
	        var reqResultStr = responseData[0].result;
	        if (reqResultStr) {
	            var reqResult = JSON.parse(reqResultStr);
	            var reqResultHeaderStatusCode = reqResult.statusCode;
	            if (reqResultHeaderStatusCode >= 400) {
	                var reqResultHeaderStatusText = reqResult.status;
	                var reqResultHeaderError = reqResult.error;
	                console.log('Header - status code = ' + reqResultHeaderStatusCode + ', status = ' + reqResultHeaderStatusText + ', error = ' + reqResultHeaderError);
	                var reqResultBodyStr = reqResult.body;
	                var reqResultBody = JSON.parse(reqResultBodyStr);

	                var errorsArr = [];
	                if (_.isArray(reqResultBody)) {
	                    // rest-api-perm-error.json
	                    _.forEach(reqResultBody, function(ele, idx, collection1) {
	                        errorsArr.push(ele.message);
	                    });
	                } else {
	                    // rest-api-error.json
	                    var reqResultBodyHasErrors = reqResultBody.hasErrors;
	                    var reqResultBodyResultsArr = reqResultBody.results;
	                    var reqResultBodyErrorCode = reqResultBody.errorCode;
	                    _.forEach(reqResultBodyResultsArr, function(ele, idx, collection) {
	                        var reqResultBodyErrorsArr = ele.errors;
	                        _.forEach(reqResultBodyErrorsArr, function(ele1, idx1, collection1) {
	                            errorsArr.push(ele1.message);
	                        });
	                    });
	                }

	                validationUtils.displayFormLevelError(errorsArr, formLevelErrorDomEle, initiatorEleId);
	            } else {
	                console.log('Header - status code = ' + reqResultHeaderStatusCode);
	                successCallback(responseData);
	            }
	        } else if (responseType === 'exception') {
	            // empty-request-error.json
	            skuid.events.publish('com.header.displayCatchAllGlobalError');
	        } else {
	            successCallback(responseData);
	        }
	    } else {
	        successCallback(responseData);
	    }
	}

}
