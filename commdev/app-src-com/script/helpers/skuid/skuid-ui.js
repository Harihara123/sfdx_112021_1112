
/**
 * Skuid UI-specific functions.
 */
module.exports = {

	/**
	 * If any Skuid editor error messages are displayed, return true; else return false.
	 * @param domSelector - jQuery-compliant selector for the DOM element(s) whose child elements 
	 *	with class "nx-error" are to be inspected.
	 * @return true or false, as explained above.
	 */
	anyEditorErrorMessagesDisplayed: function(domSelector) {
		var errorsFound = false;
		$(domSelector).find('.nx-error').each(function(idx) {
			// Search for non-empty text content. If found, assume it's an error message.
			var errorMessage = $(this).text();
			if (errorMessage !== null && errorMessage !== undefined && errorMessage.length > 0) {
				errorsFound = true;
			}
		})
		return errorsFound;
	}

}
