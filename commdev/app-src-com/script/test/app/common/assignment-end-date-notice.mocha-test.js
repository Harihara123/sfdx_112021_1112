'use strict';

var moment = require('moment');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');

var assignmentEndDateNotice = require('../../../app/common/assignment-end-date-notice');

describe('assignment-end-date-notice', function () {

	describe('condDisplayAssignmentEndDateNotice', function () {

	    beforeEach(function() {
	    	// Bootstrap some model objects.
			this.userDataMock = {
				Account: {
					Talent_End_Date__c: null, 
					Assign_End_Notice_Close_Date__c: ''
				}, 
				Contact: {
					End_Date_Change_Requested__c: new Date()
				}
			};

			this.opcoMappingsDataMock = {
				Assign_End_Notice_Days_1__c: 45, 
				Assign_End_Notice_Days_2__c: 30
			};

			this.casesSearchForApproachingEndDateNoticeDataMock = {
				CreatedDate: ''
			};

			var classScope = this;

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelData: function(modelId) {
					var result = null;
					if (modelId === 'RunningUser') {
						result = classScope.userDataMock;
					} else if (modelId === 'OpCoMappings') {
						result = classScope.opcoMappingsDataMock;
					} /* caseModel */ else {
						result = classScope.casesSearchForApproachingEndDateNoticeDataMock;
					}
					return result;
				}, 

				retrieveModelDefinition: function(modelId) {
					var result = null;
					if (modelId === 'CasesSearchForApproachingEndDateNotice') {
						result = {

						};
					}
					return result;
				}, 

				fetchModelData: function(modelOrModelArray, callback) {
					callback({
						models: {
							CasesSearchForApproachingEndDateNotice: {
								
							}
						}
					});
				}
			};

			this.eventUtilsMock = {
				// Spy on event-publishing. This is how the notice display is triggered.
				publishEvent: sinon.spy()
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-end-date-notice');
	        this.assignmentEndDateNoticeForTest = injector({
	        	'../common/event-utils': this.eventUtilsMock, 
	        	'../common/model-utils': this.modelUtilsMock, 
	        	'../common/template-utils': this.templateUtilsMock
	        });
	    });

	    it('outstanding assignment end date change requested', function (done) {
	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	/*
	        	 * If an outstanding request for an assignment end date change exists, don't display the 
	        	 *	assignment end date notice.
	        	 */
		        assert.isNotTrue(classScope.eventUtilsMock.publishEvent.called);

		        done();
	        });
	    });

	    it('no assignment end date', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// If there is no assignment end date, don't display the assignment end date notice.
		        assert.isNotTrue(classScope.eventUtilsMock.publishEvent.called);

		        done();
	        });
	    });

	    it('before notice window', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;

	    	// Set the assignment end date far in the future.
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(9, 'months').toDate();

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// If the assignment end date isn't near, don't display the assignment end date notice.
		        assert.isNotTrue(classScope.eventUtilsMock.publishEvent.called);

		        done();
	        });
	    });

	    it('after notice window', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;

	    	// Set the assignment end date in the past.
	    	this.userDataMock.Account.Talent_End_Date__c = moment().subtract(1, 'months').toDate();

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// If the assignment end date has passed, don't display the assignment end date notice.
		        assert.isNotTrue(classScope.eventUtilsMock.publishEvent.called);

		        done();
	        });
	    });

	    it('notice closed during window 1', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;
	    	
	    	// Set the assignment end date to be just inside the first display window.
	    	var endDateDaysAway = this.opcoMappingsDataMock.Assign_End_Notice_Days_1__c - 5;
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(endDateDaysAway, 'days').toDate();

	    	// Make it so that the user closed the notice yesterday (which is within the first display window).
	    	this.userDataMock.Account.Assign_End_Notice_Close_Date__c = moment().subtract(1, 'days').format('YYYY-MM-DD');

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// If the user has closed the notice within the current display window, don't display the assignment end date notice.
		        assert.isNotTrue(classScope.eventUtilsMock.publishEvent.called);

		        done();
	        });
	    });

	    it('notice closed during window 2', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;

	    	// Set the assignment end date to be just inside the second display window.
	    	var endDateDaysAway = this.opcoMappingsDataMock.Assign_End_Notice_Days_2__c - 5;
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(endDateDaysAway, 'days').toDate();

	    	// Make it so that the user closed the notice yesterday (which is within the second display window).
	    	this.userDataMock.Account.Assign_End_Notice_Close_Date__c = moment().subtract(1, 'days').format('YYYY-MM-DD');

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// If the user has closed the notice within the current display window, don't display the assignment end date notice.
		        assert.isNotTrue(classScope.eventUtilsMock.publishEvent.called);

		        done();
	        });
	    });

	    it('notice case already created', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;

	    	// Set the assignment end date to be just inside the second display window.
	    	var endDateDaysAway = this.opcoMappingsDataMock.Assign_End_Notice_Days_2__c - 5;
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(endDateDaysAway, 'days').toDate();

	    	// Make it so that, yesterday, the user asked to be contacted about their assignment end date.
	    	this.casesSearchForApproachingEndDateNoticeDataMock.CreatedDate = 
	    		moment().subtract(1, 'days').format('YYYY-MM-DDTHH:mm:ss.000Z');

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	/*
	        	 * If the user has already asked to be contacted about their assignment end date during the current 
	        	 *	(aggregated) display window, don't display the assignment end date notice.
	        	 */
		        assert.isNotTrue(classScope.eventUtilsMock.publishEvent.called);

		        done();
	        });
	    });

	    it('in window, no close, no case', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;

	    	// Set the assignment end date to be just inside the second display window.
	    	var endDateDaysAway = this.opcoMappingsDataMock.Assign_End_Notice_Days_2__c - 5;
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(endDateDaysAway, 'days').toDate();

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// In the absence of any close or "contact me" indicators, the assignment end date notice is displayed.
		        assert(classScope.eventUtilsMock.publishEvent.called);
        		assert(classScope.eventUtilsMock.publishEvent.calledWith('com.header.displayGlobalWarning', sinon.match.any));

		        done();
	        });
	    });

	    it('notice case created for previous display window', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;

	    	// Set the assignment end date to be just inside the second display window.
	    	var endDateDaysAway = this.opcoMappingsDataMock.Assign_End_Notice_Days_2__c - 5;
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(endDateDaysAway, 'days').toDate();

	    	// Make it so that, a while ago, the user asked to be contacted about their assignment end date.
	    	this.casesSearchForApproachingEndDateNoticeDataMock.CreatedDate = 
	    		moment().subtract(11, 'months').format('YYYY-MM-DDTHH:mm:ss.000Z');

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// In the absence of any close or current "contact me" indicators, the assignment end date notice is displayed.
		        assert(classScope.eventUtilsMock.publishEvent.called);
        		assert(classScope.eventUtilsMock.publishEvent.calledWith('com.header.displayGlobalWarning', sinon.match.any));

		        done();
	        });
	    });

	    it('notice closed during window 1, but now in window 2', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;
	    	
	    	// Set the assignment end date to be just inside the second display window.
	    	var endDateDaysAway = this.opcoMappingsDataMock.Assign_End_Notice_Days_2__c - 5;
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(endDateDaysAway, 'days').toDate();

	    	// Make it so that the user closed the notice just before the end of the first display window.
	    	this.userDataMock.Account.Assign_End_Notice_Close_Date__c = moment().subtract(6, 'days').format('YYYY-MM-DD');

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	// The user has not closed the notice during the current display window, so the assignment end date notice is displayed.
		        assert(classScope.eventUtilsMock.publishEvent.called);
        		assert(classScope.eventUtilsMock.publishEvent.calledWith('com.header.displayGlobalWarning', sinon.match.any));

		        done();
	        });
	    });

	    it('notice closed during old window', function (done) {
	    	// Blank out the request for an assignment end date change.
	    	this.userDataMock.Contact.End_Date_Change_Requested__c = null;
	    	
	    	// Set the assignment end date to be just inside the second display window.
	    	var endDateDaysAway = this.opcoMappingsDataMock.Assign_End_Notice_Days_2__c - 5;
	    	this.userDataMock.Account.Talent_End_Date__c = moment().add(endDateDaysAway, 'days').toDate();

	    	// Make it so that the user closed the notice during an old display window.
	    	this.userDataMock.Account.Assign_End_Notice_Close_Date__c = moment().subtract(11, 'months').format('YYYY-MM-DD');

	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.condDisplayAssignmentEndDateNotice(function() {
	        	/*
	        	 * The user has not closed the notice during the current (aggregated) display window, so the assignment 
	        	 *	end date notice is displayed.
	        	 */
		        assert(classScope.eventUtilsMock.publishEvent.called);
        		assert(classScope.eventUtilsMock.publishEvent.calledWith('com.header.displayGlobalWarning', sinon.match.any));

		        done();
	        });
	    });
	});

	describe('handleOnClickAssignEndNoticeConfirm', function () {

	    beforeEach(function() {
	    	// Silo UI-related operations from Skuid.
			this.uiUtilsMock = {
				showLoadingMask: function() {

				}, 

				hideLoadingMask: function() {

				}
			}

	    	// Bootstrap some model objects.
			this.contactDefinitionMock = {};
			this.contactDataMock = {};

			var classScope = this;

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelDefinition: function(modelId) {
					return classScope.contactDefinitionMock;
				}, 

				fetchModelData: function(modelArray, callback) {
					callback();
				}, 

				retrieveModelData: function(model) {
					return classScope.contactDataMock;
				}, 

				// Spy on how the model object(s) are updated, which what values.
				updateModelValues: sinon.spy(), 

				// Spy on the model object(s) being saved.
				saveModelData: sinon.spy(function(modelData, options, modelOrModelArray) {
					return {
						then: function(callback) {
							callback();
						}, 

						error: function(callback) {
							callback();
						} 
					};
				})
			};

			this.eventUtilsMock = {
				// Spy on event-publishing. This is how feedback is displayed to the user.
				publishEvent: sinon.spy()
			};

			// Silo i18n-related operations from Skuid.
			this.i18nUtilsMock = {
				retrieveText: function() {
					return '';
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-end-date-notice');
	        this.assignmentEndDateNoticeForTest = injector({
	        	'../common/event-utils': this.eventUtilsMock, 
	        	'../common/i18n-utils': this.i18nUtilsMock, 
	        	'../common/model-utils': this.modelUtilsMock, 
	        	'../common/ui-utils': this.uiUtilsMock
	        });
	    });

	    it('request successful', function (done) {
	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.handleOnClickAssignEndNoticeConfirm(function() {
		        assert(classScope.modelUtilsMock.updateModelValues.calledOnce);
		        // The End_Date_Change_Requested__c field should be updated with the expected value.
		        assert(classScope.modelUtilsMock.updateModelValues.calledWith(classScope.contactDataMock, {
			            End_Date_Change_Requested__c: 'change requested'
			        }, classScope.contactDefinitionMock)
		   		);

		        // The contact model should be saved (persisted).
		        assert(classScope.modelUtilsMock.saveModelData.calledOnce);
		        assert(classScope.modelUtilsMock.saveModelData.calledWith(
		        	sinon.match.any, sinon.match.any, classScope.contactDefinitionMock)
		   		);

		        // Feedback should be displayed to the user.
		        assert(classScope.eventUtilsMock.publishEvent.called);
        		assert(classScope.eventUtilsMock.publishEvent.calledWith('com.header.displayGlobalSuccess', sinon.match.any));

		        done();
	        });
	    });
	});

	describe('handleOnClickAssignEndNoticeClose', function () {

	    beforeEach(function() {
	    	// Bootstrap some model objects.
			this.accountDefinitionMock = {};
			this.accountDataMock = {};

			var classScope = this;

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelDefinition: function(modelId) {
					return classScope.accountDefinitionMock;
				}, 

				fetchModelData: function(modelArray, callback) {
					callback();
				}, 

				retrieveModelData: function(model) {
					return classScope.accountDataMock;
				}, 

				// Spy on how the model object(s) are updated, which what values.
				updateModelValues: sinon.spy(), 

				// Spy on the model object(s) being saved.
				saveModelData: sinon.spy(function(modelData, options, modelOrModelArray) {
					return {
						then: function(callback) {
							callback();
						}, 

						error: function(callback) {
							callback();
						} 
					};
				})
			};

			this.eventUtilsMock = {
				// Spy on event-publishing. This is how the assignment end date notice is dismissed.
				publishEvent: sinon.spy()
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-end-date-notice');
	        this.assignmentEndDateNoticeForTest = injector({
	        	'../common/event-utils': this.eventUtilsMock, 
	        	'../common/model-utils': this.modelUtilsMock
	        });
	    });

	    it('request successful', function (done) {
	    	var classScope = this;
	        var result = this.assignmentEndDateNoticeForTest.handleOnClickAssignEndNoticeClose(function() {
		        assert(classScope.modelUtilsMock.updateModelValues.calledOnce);
		        // The Assign_End_Notice_Close_Date__c field should be updated with the current date.
		        assert(classScope.modelUtilsMock.updateModelValues.calledWith(classScope.accountDataMock, {
			            Assign_End_Notice_Close_Date__c: moment().format('YYYY-MM-DD')
			        }, classScope.accountDefinitionMock)
		   		);

		        // The account model should be saved (persisted).
		        assert(classScope.modelUtilsMock.saveModelData.calledOnce);
		        assert(classScope.modelUtilsMock.saveModelData.calledWith(
		        	sinon.match.any, sinon.match.any, classScope.accountDefinitionMock)
		   		);

		        // The assignment end date notice should be dismissed.
		        assert(classScope.eventUtilsMock.publishEvent.called);
        		assert(classScope.eventUtilsMock.publishEvent.calledWith('com.header.clearGlobalWarning'));

		        done();
	        });
	    });
	});

});


