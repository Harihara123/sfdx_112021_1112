'use strict';

require('expose?$!expose?jQuery!jquery');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');
var moment = require('moment');

var assignmentStatus = require('../../../app/common/assignment-status');

describe('assignment-status', function () {

	describe('buildTalentAssignmentStatus', function () {

	    beforeEach(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-status');
	        this.assignmentStatusForTest = injector({

	        });
	    });

	    it('end date change requested', function () {
	    	// Build a couple work history records.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(3, 'months').toDate(), 
	    		End_Date__c: moment().add(1, 'months').toDate()
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().add(2, 'months').toDate(), 
	    		End_Date__c: moment().add(7, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];

	    	// Make it so that the user has requested a change to their assignment end date.
	    	var endDateChangeRequested = new Date();
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(
	        	talentWorkHistoryModels, endDateChangeRequested, null);

        	// If the user has requested an end date change, that becomes the assignment status.
	        assert.deepEqual(result, {
                'status': assignmentStatus.TALENT_STATUS_DATE_CHANGE_REQUESTED
            });
	    });

	    it('open approaching end date notice case', function () {
	    	// Build a couple work history records.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(3, 'months').toDate(), 
	    		End_Date__c: moment().add(1, 'months').toDate()
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().add(2, 'months').toDate(), 
	    		End_Date__c: moment().add(7, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];

	    	// Make it so that the user has asked to be contacted about their approaching assignment end date.
	    	var openApproachingEndDateNoticeCase = {};
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(
	        	talentWorkHistoryModels, null, openApproachingEndDateNoticeCase);

	        /*
	         * If the user has asked to be contacted about their approaching assignment end date, their 
	         *	status becomes that they have requested an end date change.
	         */
	        assert.deepEqual(result, {
                'status': assignmentStatus.TALENT_STATUS_DATE_CHANGE_REQUESTED
            });
	    });

	    it('multiple active positions', function () {
	    	// Build a couple work history records, both active (and overlapping).
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(3, 'months').toDate(), 
	    		End_Date__c: moment().add(1, 'months').toDate()
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().subtract(2, 'months').toDate(), 
	    		End_Date__c: moment().add(2, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        /*
	         * The user's status should be active, with the dates taken from the work history with the most 
	         *	recent start date.
	         */
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel2.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel2.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
            });
	    });

	    it('active and starting positions', function () {
	    	// Build a couple work history records, one active and on future.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(3, 'months').toDate(), 
	    		End_Date__c: moment().add(1, 'months').toDate()
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().add(2, 'months').toDate(), 
	    		End_Date__c: moment().add(7, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be active, with the dates taken from the active work history.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel1.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel1.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
            });
	    });

	    it('multiple starting positions', function () {
	    	// Build a couple work history records, both future.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().add(3, 'months').toDate(), 
	    		End_Date__c: moment().add(10, 'months').toDate()
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().add(1, 'months').toDate(), 
	    		End_Date__c: moment().add(2, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be starting, with the dates taken from the soonest work history.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel2.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel2.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_STARTING
            });
	    });

	    it('multiple inactive positions', function () {
	    	// Build a couple work history records, both in the past.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(15, 'months').toDate(), 
	    		End_Date__c: moment().subtract(7, 'months').toDate()
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().subtract(6, 'months').toDate(), 
	    		End_Date__c: moment().subtract(1, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be inactive, with the dates taken from the most recent work history.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel2.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel2.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_INACTIVE
            });
	    });

	    it('inactive and starting positions', function () {
	    	// Build a couple work history records, one in the past and one in the future.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(15, 'months').toDate(), 
	    		End_Date__c: moment().subtract(7, 'months').toDate()
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().add(1, 'months').toDate(), 
	    		End_Date__c: moment().add(9, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be starting, with the dates taken from the future work history.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel2.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel2.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_STARTING
            });
	    });

	    it('active and invalid positions', function () {
	    	// Build a couple work history records, one active and one invalid.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: null
	    	};
	    	var talentWorkHistoryModel2 = {
	    		Start_Date__c: moment().subtract(2, 'months').toDate(), 
	    		End_Date__c: moment().add(7, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be active, with the dates taken from the active work history.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel2.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel2.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
            });
	    });

	    it('multiple invalid positions', function () {
	    	// Build a couple work history records, both invalid.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: null
	    	};
	    	var talentWorkHistoryModel2 = {

	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1, talentWorkHistoryModel2];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be unknown, with no dates.
	        assert.deepEqual(result, {
	        	'startDate': '', 
	        	'endDate': '', 
                'status': assignmentStatus.TALENT_STATUS_UNKNOWN
            });
	    });

	    it('start date today', function () {
	    	// Build a work history record, with a start date of today.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment(), 
	    		End_Date__c: moment().add(7, 'months').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be active.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel1.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel1.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
            });
	    });

	    it('end date today', function () {
	    	// Build a work history record, with an end date of today.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(7, 'months').toDate(), 
	    		End_Date__c: moment()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be active.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel1.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel1.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
            });
	    });

	    it('end date yesterday', function () {
	    	// Build a work history record, with an end date of yesterday.
	    	var talentWorkHistoryModel1 = {
	    		Start_Date__c: moment().subtract(7, 'months').toDate(), 
	    		End_Date__c: moment().subtract(1, 'days').toDate()
	    	};
	    	var talentWorkHistoryModels = [talentWorkHistoryModel1];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be inactive.
	        assert.deepEqual(result, {
	        	'startDate': talentWorkHistoryModel1.Start_Date__c, 
	        	'endDate': talentWorkHistoryModel1.End_Date__c, 
                'status': assignmentStatus.TALENT_STATUS_INACTIVE
            });
	    });

	    it('no positions', function () {
	    	var talentWorkHistoryModels = [];
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be empty.
	        assert.deepEqual(result, {

            });
	    });

	    it('null positions', function () {
	    	var talentWorkHistoryModels = null;
	        var result = this.assignmentStatusForTest.buildTalentAssignmentStatus(talentWorkHistoryModels, null, null);

	        // The user's status should be empty.
	        assert.deepEqual(result, {

            });
	    });

	});

	describe('displayAssignmentStatus', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'</div>'
			);

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Spy on chart-creation.
			this.chartJsMock = sinon.spy()

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-status');
	        this.assignmentStatusForTest = injector({
	        	'../common/template-utils': this.templateUtilsMock, 
	        	'chart.js': this.chartJsMock
	        });
	    });

	    it('active status', function () {
	    	var $rootEle = $('#root-ele');

	    	// Put the user in active status.
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(7, 'months').toDate(), 
	        	'endDate': moment().add(1, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should properly refect the assignment status.
	        var $statusBox = $rootEle.find('.qa-status-active');
	        assert.equal($statusBox.length, 1);
	        $statusBox = $rootEle.find('.qa-status-unknown');
	        assert.equal($statusBox.length, 0);

	        // The chart should be initialized, with an insertion point in the DOM provided.
	        assert(this.chartJsMock.calledOnce);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 1);
	    });

	    it('ending today', function () {
	    	var $rootEle = $('#root-ele');

	    	// Put the user in active status, with their assignment ending today.
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(7, 'months').toDate(), 
	    		/*
	    		 * When we get close in to the end date, we need to be particular about how we set the 
	    		 *	endDate (so that the granularity is at the 'days' level, not hours or minutes). This 
	    		 *	is the granularity we get from the Salesforce data.
	    		 */
	        	'endDate': moment(moment().format('YYYYMMDD'), 'YYYYMMDD').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should properly refect the assignment status.
	        var $statusBox = $rootEle.find('.qa-status-active');
	        assert.equal($statusBox.length, 1);
	        $statusBox = $rootEle.find('.qa-status-unknown');
	        assert.equal($statusBox.length, 0);

	        // The chart should be initialized, with an insertion point in the DOM provided.
	        assert(this.chartJsMock.calledOnce);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 1);

	        // If the assignment is ending today, the shown days remaining should be 0.
	        var $daysRemainingBox = $rootEle.find('.qa-days-remaining');
	        assert.equal($daysRemainingBox.text(), '0');
	    });

	    it('ending tomorrow', function () {
	    	var $rootEle = $('#root-ele');

	    	// Put the user in active status, with their assignment ending tomorrow.
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(7, 'months').toDate(), 
	    		/*
	    		 * When we get close in to the end date, we need to be particular about how we set the 
	    		 *	endDate (so that the granularity is at the 'days' level, not hours or minutes). This 
	    		 *	is the granularity we get from the Salesforce data.
	    		 */
	        	'endDate': moment(moment().format('YYYYMMDD'), 'YYYYMMDD').add(1, 'days').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should properly refect the assignment status.
	        var $statusBox = $rootEle.find('.qa-status-active');
	        assert.equal($statusBox.length, 1);
	        $statusBox = $rootEle.find('.qa-status-unknown');
	        assert.equal($statusBox.length, 0);

	        // The chart should be initialized, with an insertion point in the DOM provided.
	        assert(this.chartJsMock.calledOnce);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 1);

	        // If the assignment is ending today, the shown days remaining should be 1.
	        var $daysRemainingBox = $rootEle.find('.qa-days-remaining');
	        assert.equal($daysRemainingBox.text(), '1');
	    });

	    it('inactive status', function () {
	    	var $rootEle = $('#root-ele');

	    	// Put the user in inactive status.
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(7, 'months').toDate(), 
	        	'endDate': moment().subtract(1, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_INACTIVE
	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should properly refect the assignment status.
	        var $statusBox = $rootEle.find('.qa-status-inactive');
	        assert.equal($statusBox.length, 1);
	        $statusBox = $rootEle.find('.qa-status-unknown');
	        assert.equal($statusBox.length, 0);

	        // The chart should not be initialized, with no insertion point in the DOM provided.
	        assert.equal(this.chartJsMock.callCount, 0);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 0);
	    });

	    it('starting status', function () {
	    	var $rootEle = $('#root-ele');

	    	// Put the user in starting status.
	    	var talentAssignmentStatus = {
	    		'startDate': moment().add(1, 'months').toDate(), 
	        	'endDate': moment().add(9, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_STARTING
	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should properly refect the assignment status.
	        var $statusBox = $rootEle.find('.qa-status-starting');
	        assert.equal($statusBox.length, 1);
	        $statusBox = $rootEle.find('.qa-status-unknown');
	        assert.equal($statusBox.length, 0);

	        // The chart should not be initialized, with no insertion point in the DOM provided.
	        assert.equal(this.chartJsMock.callCount, 0);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 0);
	    });

	    it('change status', function () {
	    	var $rootEle = $('#root-ele');

	    	// Put the user in 'end date change requested' status.
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(1, 'months').toDate(), 
	        	'endDate': moment().add(9, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_DATE_CHANGE_REQUESTED
	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should properly refect the assignment status.
	        var $statusBox = $rootEle.find('.qa-status-change');
	        assert.equal($statusBox.length, 1);
	        $statusBox = $rootEle.find('.qa-status-unknown');
	        assert.equal($statusBox.length, 0);

	        // The chart should not be initialized, with no insertion point in the DOM provided.
	        assert.equal(this.chartJsMock.callCount, 0);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 0);
	    });

	    it('unknown status', function () {
	    	var $rootEle = $('#root-ele');

	    	// Put the user in unknown status.
	    	var talentAssignmentStatus = {
	    		'startDate': '', 
	        	'endDate': '', 
                'status': assignmentStatus.TALENT_STATUS_UNKNOWN
	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should properly refect the assignment status.
	        var $statusBox = $rootEle.find('.qa-status-unknown');
	        assert.equal($statusBox.length, 1);

	        // The chart should not be initialized, with no insertion point in the DOM provided.
	        assert.equal(this.chartJsMock.callCount, 0);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 0);
	    });

	    it('empty status', function () {
	    	var $rootEle = $('#root-ele');

	    	// Create an empty status.
	    	var talentAssignmentStatus = {

	    	};
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should refect an inactive assignment status.
	        var $statusBox = $rootEle.find('.qa-status-inactive');
	        assert.equal($statusBox.length, 1);

	        // The chart should not be initialized, with no insertion point in the DOM provided.
	        assert.equal(this.chartJsMock.callCount, 0);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 0);
	    });

	    it('null status', function () {
	    	var $rootEle = $('#root-ele');

	    	var talentAssignmentStatus = null;
	        this.assignmentStatusForTest.displayAssignmentStatus($rootEle, talentAssignmentStatus);

	        // The card should refect an inactive assignment status.
	        var $statusBox = $rootEle.find('.qa-status-inactive');
	        assert.equal($statusBox.length, 1);

	        // The chart should not be initialized, with no insertion point in the DOM provided.
	        assert.equal(this.chartJsMock.callCount, 0);
	        var $chartRootEle = $rootEle.find('#current-assignment');
	        assert.equal($chartRootEle.length, 0);
	    });
	});

	describe('handleRequestEndDateChange', function () {

	    before(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div class="c-current-assignment">' + 
				'	</div>' + 
				'</div>'
			);

	    	// Bootstrap some model objects.
			this.contactDefinitionMock = {};
			this.contactDataMock = {};

			var classScope = this;

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelDefinition: function(modelId) {
					return classScope.contactDefinitionMock;
				}, 

				fetchModelData: function(modelArray, callback) {
					callback();
				}, 

				retrieveModelData: function(model) {
					return classScope.contactDataMock;
				}, 

				// Spy on how the model object(s) are updated, which what values.
				updateModelValues: sinon.spy(), 

				// Spy on the model object(s) being saved.
				saveModelData: sinon.spy(function(modelData, options, modelOrModelArray) {
					options.callback({
						totalsuccess: true
					});
				})
			};

			this.eventUtilsMock = {
				// Spy on event-publishing. This is how feedback is displayed to the user.
				publishEvent: sinon.spy()
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-status');
	        this.assignmentStatusForTest = injector({
	        	'../common/template-utils': this.templateUtilsMock, 
	            '../common/model-utils': this.modelUtilsMock,
	            '../common/event-utils': this.eventUtilsMock
	        });
	    });

	    it('request successful', function () {
	    	var $rootEle = $('#root-ele');
	        this.assignmentStatusForTest.handleRequestEndDateChange();

	        assert(this.modelUtilsMock.updateModelValues.calledOnce);
		        // The End_Date_Change_Requested__c field should be updated with the expected value.
	        assert(this.modelUtilsMock.updateModelValues.calledWith(this.contactDataMock, {
		            End_Date_Change_Requested__c: 'change requested'
		        }, this.contactDefinitionMock)
	   		);

		    // The contact model should be saved (persisted).
	        assert(this.modelUtilsMock.saveModelData.calledOnce);
	        assert(this.modelUtilsMock.saveModelData.calledWith(
	        	this.contactDataMock, sinon.match.any, this.contactDefinitionMock)
	   		);

	        // The card should reflect the status change - to end date change requested.
	        var $statusBox = $rootEle.find('.qa-status-change');
	        assert.equal($statusBox.length, 1);
	    });
	});


	describe('calcAssignmentEndDate', function () {

	    before(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-status');
	        this.assignmentStatusForTest = injector({

	        });
	    });

	    it('date valid 1', function () {
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": "2017-04-20"
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcAssignmentEndDate(user);

	        assert.equal(result.isValid(), true, 'A valid Moment instance should be returned.');
	        assert.equal(result.date(), 20, 'The day of month of the returned Moment instance should be 20.');
	        assert.equal(result.month(), 3, 'The month (zero-indexed) of the returned Moment instance should be 3.');
	        assert.equal(result.year(), 2017, 'The year of the returned Moment instance should be 2017.');
	    });

	    it('date valid 2', function () {
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": "2023-01-01"
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcAssignmentEndDate(user);

	        assert.equal(result.isValid(), true, 'A valid Moment instance should be returned.');
	        assert.equal(result.date(), 1, 'The day of month of the returned Moment instance should be 1.');
	        assert.equal(result.month(), 0, 'The month (zero-indexed) of the returned Moment instance should be 0.');
	        assert.equal(result.year(), 2023, 'The year of the returned Moment instance should be 2023.');
	    });

	    it('date invalid', function () {
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": "n/a"
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcAssignmentEndDate(user);

	        assert.equal(result.isValid(), false, 'An invalid Moment instance should be returned.');
	    });
	});

	describe('calcTalentStatus', function () {

	    before(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-status');
	        this.assignmentStatusForTest = injector({

	        });
	    });

	    it('talent status current', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.add(27, 'days').format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentStatus(user);

	        assert.equal(result, assignmentStatus.TALENT_STATUS_CURRENT, 'When the end date is in the future, a CURRENT status should be returned.');
	    });

	    it('talent status current same day', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentStatus(user);

	        assert.equal(result, assignmentStatus.TALENT_STATUS_CURRENT, 'When the end date is today, a CURRENT status should be returned.');
	    });

	    it('talent status former', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.subtract(3, 'days').format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentStatus(user);

	        assert.equal(result, assignmentStatus.TALENT_STATUS_FORMER, 'When the end date is in the past, a FORMER status should be returned.');
	    });

	    it('talent status invalid', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": ''
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentStatus(user);

	        assert.equal(result, null, 'When the end date is invalid, a null status should be returned.');
	    });

	});

	describe('calcTalentIsFormer', function () {

	    before(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-status');
	        this.assignmentStatusForTest = injector({

	        });
	    });

	    it('talent status current', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.add(27, 'days').format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormer(user);

	        assert.equal(result, false, 'When the end date is in the future, the user is not a FORMER.');
	    });

	    it('talent status current same day', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormer(user);

	        assert.equal(result, false, 'When the end date is today, the user is not a FORMER.');
	    });

	    it('talent status former', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.subtract(3, 'days').format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormer(user);

	        assert.equal(result, true, 'When the end date is in the past, the user is a FORMER.');
	    });

	    it('talent status invalid', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": ''
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormer(user);

	        assert.equal(result, false, 'When the end date is invalid, the user is not a FORMER.');
	    });

	});

	describe('calcTalentIsFormerNotRecent', function () {

	    before(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/assignment-status');
	        this.assignmentStatusForTest = injector({

	        });
	    });

	    it('talent status current', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.add(27, 'days').format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormerNotRecent(user);

	        assert.equal(result, false, 'When the end date is in the future, the user is not a FORMER, much less a recent FORMER.');
	    });

	    it('talent status current same day', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormerNotRecent(user);

	        assert.equal(result, false, 'When the end date is today, the user is not a FORMER, much less a recent FORMER.');
	    });

	    it('talent status former', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.subtract(3, 'days').format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormerNotRecent(user);

	        assert.equal(result, false, 'When the end date is in the recent past, the user is a FORMER, and a recent FORMER.');
	    });

	    it('talent status former not recent', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": nowMoment.subtract(34, 'days').format('YYYY-MM-DD')
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormerNotRecent(user);

	        assert.equal(result, true, 'When the end date is in the non-recent past, the user is a FORMER, but not a recent FORMER.');
	    });

	    it('talent status invalid', function () {
	    	var nowMoment = moment();
	    	var user = {
	    		Account: {
	    			"Talent_End_Date__c": ''
	    		}
	    	};
	        var result = this.assignmentStatusForTest.calcTalentIsFormerNotRecent(user);

	        assert.equal(result, false, 'When the end date is invalid, the user is not a FORMER, much less a recent FORMER.');
	    });

	});

});


