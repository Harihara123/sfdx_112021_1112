'use strict';

require('expose?$!expose?jQuery!jquery');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');
var moment = require('moment');

describe('auth-utils', function () {

	describe('handleOnClickTermsCheckbox', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="checkbox" class="js-t-and-c-input" />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('terms checked no', function () {
	    	var $rootEle = $('#root-ele');
	    	var $termsCheckbox = $rootEle.find('.js-t-and-c-input');
	    	$termsCheckbox.prop('checked', false);

	        this.authUtilsForTest.handleOnClickTermsCheckbox();

	        assert.equal(this.authUtilsForTest.isTermsAccepted(), false, 
	        	'If the terms click-handler is triggered when the terms checkbox is unchecked, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');
	    });

	    it('terms checked yes', function () {
	    	var $rootEle = $('#root-ele');
	    	var $termsCheckbox = $rootEle.find('.js-t-and-c-input');
	    	$termsCheckbox.prop('checked', true);

	        this.authUtilsForTest.handleOnClickTermsCheckbox();

	        assert.equal(this.authUtilsForTest.isTermsAccepted(), true, 
	        	'If the terms click-handler is triggered when the terms checkbox is checked, the module property should reflect a true state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');
	    });

	});

	describe('handleOnChangePasswordInput', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<span class="js-password-rule-characters-fail-box" />' +
				'	<span class="js-password-rule-letters-fail-box" />' +
				'	<span class="js-password-rule-numbers-fail-box" />' +
				'	<span class="js-password-rule-special-characters-fail-box" />' +
				'	<input type="password" class="js-password1-input" />' + 
				'	<input type="password" class="js-password2-input" />' + 
				'	<span class="js-password-inputs-match-fail-box" style="display: none;" />' + 
				'	<input type="checkbox" class="js-t-and-c-input" />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('password rules matched none, passwords match both empty', function () {
	    	var $rootEle = $('#root-ele');

	        this.authUtilsForTest.handleOnChangePasswordInput();

	        assert.equal(this.authUtilsForTest.isPasswordRulesMet(), false, 
	        	'If the password value does not meet all of the password rules, the module property should reflect a false state.');
	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), false, 
	        	'If the password values are both empty, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');

	        assert.equal($rootEle.find('.js-password-rule-characters-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the characters rule, that rule should show as failed.');
	        assert.equal($rootEle.find('.js-password-rule-letters-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the letters rule, that rule should show as failed.');
	        assert.equal($rootEle.find('.js-password-rule-numbers-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the numbers rule, that rule should show as failed.');
	        assert.equal($rootEle.find('.js-password-rule-special-characters-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the special characters rule, that rule should show as failed.');

	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'none', 
	        	'If the password values are both empty, the match fail message should not show.');
	    });

	    it('password rules matched two, passwords match confirm empty', function () {
	    	var $rootEle = $('#root-ele');
	    	var $password1Input = $rootEle.find('.js-password1-input');
	    	$password1Input.val('abcDefgh');
	    	var $password2Input = $rootEle.find('.js-password2-input');
	    	$password2Input.val('');

	        this.authUtilsForTest.handleOnChangePasswordInput();

	        assert.equal(this.authUtilsForTest.isPasswordRulesMet(), false, 
	        	'If the password value does not meet all of the password rules, the module property should reflect a false state.');
	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), false, 
	        	'If the password values do not match, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');

	        assert.equal($rootEle.find('.js-password-rule-characters-fail-box').css('display'), 'none', 
	        	'If the password value meets the characters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-letters-fail-box').css('display'), 'none', 
	        	'If the password value meets the letters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-numbers-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the numbers rule, that rule should show as failed.');
	        assert.equal($rootEle.find('.js-password-rule-special-characters-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the special characters rule, that rule should show as failed.');

	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'none', 
	        	'If the password confirm value is empty, the match fail message should not show.');
	    });

	    it('password rules matched two, passwords match no', function () {
	    	var $rootEle = $('#root-ele');
	    	var $password1Input = $rootEle.find('.js-password1-input');
	    	$password1Input.val('abcDefgh');
	    	var $password2Input = $rootEle.find('.js-password2-input');
	    	$password2Input.val('abcDef1!!');

	        this.authUtilsForTest.handleOnChangePasswordInput();

	        assert.equal(this.authUtilsForTest.isPasswordRulesMet(), false, 
	        	'If the password value does not meet all of the password rules, the module property should reflect a false state.');
	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), false, 
	        	'If the password values do not match, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');

	        assert.equal($rootEle.find('.js-password-rule-characters-fail-box').css('display'), 'none', 
	        	'If the password value meets the characters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-letters-fail-box').css('display'), 'none', 
	        	'If the password value meets the letters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-numbers-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the numbers rule, that rule should show as failed.');
	        assert.equal($rootEle.find('.js-password-rule-special-characters-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the special characters rule, that rule should show as failed.');

	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'inline', 
	        	'If the password values do not match, the match fail message should show.');
	    });

	    it('password rules matched three, passwords match no', function () {
	    	var $rootEle = $('#root-ele');
	    	var $password1Input = $rootEle.find('.js-password1-input');
	    	$password1Input.val('abcDef1h');
	    	var $password2Input = $rootEle.find('.js-password2-input');
	    	$password2Input.val('abcDef1!!');

	        this.authUtilsForTest.handleOnChangePasswordInput();

	        assert.equal(this.authUtilsForTest.isPasswordRulesMet(), false, 
	        	'If the password value does not meet all of the password rules, the module property should reflect a false state.');
	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), false, 
	        	'If the password values do not match, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');

	        assert.equal($rootEle.find('.js-password-rule-characters-fail-box').css('display'), 'none', 
	        	'If the password value meets the characters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-letters-fail-box').css('display'), 'none', 
	        	'If the password value meets the letters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-numbers-fail-box').css('display'), 'none', 
	        	'If the password value meets the numbers rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-special-characters-fail-box').css('display'), 'inline', 
	        	'If the password value does not meet the special characters rule, that rule should show as failed.');

	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'inline', 
	        	'If the password values do not match, the match fail message should show.');
	    });

	    it('password rules matched four, passwords match no', function () {
	    	var $rootEle = $('#root-ele');
	    	var $password1Input = $rootEle.find('.js-password1-input');
	    	$password1Input.val('abcDef1!');
	    	var $password2Input = $rootEle.find('.js-password2-input');
	    	$password2Input.val('abcDef1!!');

	        this.authUtilsForTest.handleOnChangePasswordInput();

	        assert.equal(this.authUtilsForTest.isPasswordRulesMet(), true, 
	        	'If the password value meets all of the password rules, the module property should reflect a true state.');
	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), false, 
	        	'If the password values do not match, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');

	        assert.equal($rootEle.find('.js-password-rule-characters-fail-box').css('display'), 'none', 
	        	'If the password value meets the characters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-letters-fail-box').css('display'), 'none', 
	        	'If the password value meets the letters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-numbers-fail-box').css('display'), 'none', 
	        	'If the password value meets the numbers rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-special-characters-fail-box').css('display'), 'none', 
	        	'If the password value meets the special characters rule, that rule should show as passed.');

	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'inline', 
	        	'If the password values do not match, the match fail message should show.');
	    });

	    it('password rules matched all, passwords match yes', function () {
	    	var $rootEle = $('#root-ele');
	    	var $password1Input = $rootEle.find('.js-password1-input');
	    	$password1Input.val('abcDef1!');
	    	var $password2Input = $rootEle.find('.js-password2-input');
	    	$password2Input.val('abcDef1!');

	        this.authUtilsForTest.handleOnChangePasswordInput();

	        assert.equal(this.authUtilsForTest.isPasswordRulesMet(), true, 
	        	'If the password value meets all of the password rules, the module property should reflect a true state.');
	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), true, 
	        	'If the password values match, the module property should reflect a true state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');

	        assert.equal($rootEle.find('.js-password-rule-characters-fail-box').css('display'), 'none', 
	        	'If the password value meets the characters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-letters-fail-box').css('display'), 'none', 
	        	'If the password value meets the letters rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-numbers-fail-box').css('display'), 'none', 
	        	'If the password value meets the numbers rule, that rule should show as passed.');
	        assert.equal($rootEle.find('.js-password-rule-special-characters-fail-box').css('display'), 'none', 
	        	'If the password value meets the special characters rule, that rule should show as passed.');

	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'none', 
	        	'If the password values match, the match fail message should not show.');
	    });

	});

	describe('handleOnChangePasswordConfirmInput', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="password" class="js-password1-input" />' + 
				'	<input type="password" class="js-password2-input" />' + 
				'	<span class="js-password-inputs-match-fail-box" />' + 
				'	<input type="checkbox" class="js-t-and-c-input" />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('passwords match empty', function () {
	    	var $rootEle = $('#root-ele');

	        this.authUtilsForTest.handleOnChangePasswordConfirmInput();

	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), true, 
	        	'If the password values are both empty, the module property (as triggered by the confirm input) should reflect a true state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');
	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'none', 
	        	'If the password values are both empty, the match fail message should not show.');
	    });

	    it('passwords match no', function () {
	    	var $rootEle = $('#root-ele');
	    	var $password1Input = $rootEle.find('.js-password1-input');
	    	$password1Input.val('abcDefgh');
	    	var $password2Input = $rootEle.find('.js-password2-input');
	    	$password2Input.val('abcDef1!!');

	        this.authUtilsForTest.handleOnChangePasswordConfirmInput();

	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), false, 
	        	'If the password values do not match, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');
	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'inline', 
	        	'If the password values do not match, the match fail message should show.');
	    });

	    it('passwords match yes', function () {
	    	var $rootEle = $('#root-ele');
	    	var $password1Input = $rootEle.find('.js-password1-input');
	    	$password1Input.val('abcDef1!');
	    	var $password2Input = $rootEle.find('.js-password2-input');
	    	$password2Input.val('abcDef1!');

	        this.authUtilsForTest.handleOnChangePasswordConfirmInput();

	        assert.equal(this.authUtilsForTest.isPasswordsMatch(), true, 
	        	'If the password values match, the module property should reflect a true state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');
	        assert.equal($rootEle.find('.js-password-inputs-match-fail-box').css('display'), 'none', 
	        	'If the password values match, the match fail message should not show.');
	    });

	});

	describe('handleOnChangePasswordOldInput', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="password" class="js-password-old-input" />' + 
				'	<input type="password" class="js-password1-input" />' + 
				'	<input type="password" class="js-password2-input" />' + 
				'	<span class="js-password-inputs-match-fail-box" />' + 
				'	<input type="checkbox" class="js-t-and-c-input" />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('password old entered no', function () {
	    	var $rootEle = $('#root-ele');

	        this.authUtilsForTest.handleOnChangePasswordOldInput();

	        assert.equal(this.authUtilsForTest.isPasswordOldProvided(), false, 
	        	'If the old/current password value is not entered, the module property should reflect a false state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');
	    });

	    it('password old entered yes', function () {
	    	var $rootEle = $('#root-ele');
	    	var $passwordOldInput = $rootEle.find('.js-password-old-input');
	    	$passwordOldInput.val('a');

	        this.authUtilsForTest.handleOnChangePasswordOldInput();

	        assert.equal(this.authUtilsForTest.isPasswordOldProvided(), true, 
	        	'If the old/current password value is entered, the module property should reflect a true state.');
	        var $submitButton = $rootEle.find('.js-submit-button');
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If all of the rules are not met, the submit button should be disabled.');
	    });

	});

	describe('handleOnClickPasswordRevealInput', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="password" class="js-password-old-input" />' + 
				'	<input type="password" class="js-password1-input" />' + 
				'	<input type="password" class="js-password2-input" />' + 
				'	<span class="js-password-inputs-match-fail-box" />' + 
				'	<input type="checkbox" class="js-passwords-reveal-input" />' + 
				'	<input type="checkbox" class="js-t-and-c-input" />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('password reveal checked no', function () {
	    	var $rootEle = $('#root-ele');
	    	var $passwordRevealCheckbox = $rootEle.find('.js-passwords-reveal-input');
	    	$passwordRevealCheckbox.prop('checked', false);

	        this.authUtilsForTest.handleOnClickPasswordRevealInput();

	        assert.equal($rootEle.find('.js-password1-input').attr('type'), 'password', 
	        	'If the password reveal input is not checked, the password input should be of type password.');
	        assert.equal($rootEle.find('.js-password2-input').attr('type'), 'password', 
	        	'If the password reveal input is not checked, the password confirm input should be of type password.');
	        assert.equal($rootEle.find('.js-password-old-input').attr('type'), 'password', 
	        	'If the password reveal input is not checked, the password old input should be of type password.');
	    });

	    it('password reveal checked yes', function () {
	    	var $rootEle = $('#root-ele');
	    	var $passwordRevealCheckbox = $rootEle.find('.js-passwords-reveal-input');
	    	$passwordRevealCheckbox.prop('checked', true);

	        this.authUtilsForTest.handleOnClickPasswordRevealInput();

	        assert.equal($rootEle.find('.js-password1-input').attr('type'), 'text', 
	        	'If the password reveal input is not checked, the password input should be of type text.');
	        assert.equal($rootEle.find('.js-password2-input').attr('type'), 'text', 
	        	'If the password reveal input is not checked, the password confirm input should be of type text.');
	        assert.equal($rootEle.find('.js-password-old-input').attr('type'), 'text', 
	        	'If the password reveal input is not checked, the password old input should be of type text.');
	    });

	});

	describe('configChangeListenersOnInputs', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="password" class="js-password-old-input" onchange="console.log(\'foo\');" />' + 
				'	<input type="password" class="js-password1-input" onchange="console.log(\'bar\');" />' + 
				'	<input type="password" class="js-password2-input" onchange="console.log(\'baz\');" />' + 
				'	<span class="js-password-inputs-match-fail-box" />' + 
				'	<input type="checkbox" class="js-passwords-reveal-input" />' + 
				'	<input type="checkbox" class="js-t-and-c-input" />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('execute', function () {
	    	var $rootEle = $('#root-ele');

	        assert.isUndefined($rootEle.find('.js-password1-input').attr('oninput'), 
	        	'Before configuring the input listeners, the password input should not have an input listener.');
	        assert.isUndefined($rootEle.find('.js-password2-input').attr('oninput'), 
	        	'Before configuring the input listeners, the password input should not have an input listener.');
	        assert.isUndefined($rootEle.find('.js-password-old-input').attr('oninput'), 
	        	'Before configuring the input listeners, the password input should not have an input listener.');

	        this.authUtilsForTest.configChangeListenersOnInputs();

	        assert.isNotNull($rootEle.find('.js-password1-input').attr('oninput'), 
	        	'After configuring the input listeners, the password input should have an input listener.');
	        assert.isNotNull($rootEle.find('.js-password2-input').attr('oninput'), 
	        	'After configuring the input listeners, the password confirmation input should have an input listener.');
	        assert.isNotNull($rootEle.find('.js-password-old-input').attr('oninput'), 
	        	'After configuring the input listeners, the password old input should have an input listener.');
	    });

	});

	describe('configFormInputElements', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="password" class="js-password-old-input" onchange="console.log(\'foo\');" />' + 
				'	<input type="password" class="js-password1-input" onchange="console.log(\'bar\');" />' + 
				'	<input type="password" class="js-password2-input" onchange="console.log(\'baz\');" />' + 
				'	<span class="js-password-inputs-match-fail-box"></span>' + 
				'	<input type="checkbox" class="js-passwords-reveal-input" />' + 
				'	<span class="js-t-and-c-input-control"></span>' + 
				'	<input type="checkbox" class="js-t-and-c-input" />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('terms checkbox visible and accepted yes, old password visible', function () {
	    	var $rootEle = $('#root-ele');

	        this.authUtilsForTest.configFormInputElements();

	        assert.equal($rootEle.find('.js-submit-button').prop('disabled'), true, 
	        	'After configuring the form input elements, the submit button should be disabled.');
	        assert.equal($rootEle.find('.js-t-and-c-input').prop('checked'), true, 
	        	'After configuring the form input elements in a scenario where the terms input control is present, the terms checkbox should be accepted.');
	        assert.equal(this.authUtilsForTest.isTermsAccepted(), true, 
	        	'After configuring the form input elements in a scenario where the terms checkbox is displayed and checked, the module property should reflect a true state.');
	        assert.equal(this.authUtilsForTest.isPasswordOldProvided(), false, 
	        	'After configuring the form input elements in a scenario where the old/current password input is displayed, the module property should reflect a false state.');
	    });

	    it('terms checkbox visible and accepted no, old password visible', function () {
	    	var $rootEle = $('#root-ele');
	    	// Remove the terms input control.
	    	var $termsInputControl = $rootEle.find('.js-t-and-c-input-control');
	    	$termsInputControl.remove();

	        this.authUtilsForTest.configFormInputElements();

	        assert.equal($rootEle.find('.js-submit-button').prop('disabled'), true, 
	        	'After configuring the form input elements, the submit button should be disabled.');
	        assert.equal($rootEle.find('.js-t-and-c-input').prop('checked'), false, 
	        	'After configuring the form input elements in a scenario where the terms input control is NOT present, the terms checkbox should NOT be accepted.');
	        assert.equal(this.authUtilsForTest.isTermsAccepted(), false, 
	        	'After configuring the form input elements in a scenario where the terms checkbox is displayed and NOT checked, the module property should reflect a false state.');
	        assert.equal(this.authUtilsForTest.isPasswordOldProvided(), false, 
	        	'After configuring the form input elements in a scenario where the old/current password input is displayed, the module property should reflect a false state.');
	    });

	    it('terms checkbox not visible, old password not visible', function () {
	    	var $rootEle = $('#root-ele');
	    	// Remove the terms input and control.
	    	var $termsInput = $rootEle.find('.js-t-and-c-input');
	    	$termsInput.remove();
	    	var $termsInputControl = $rootEle.find('.js-t-and-c-input-control');
	    	$termsInputControl.remove();
	    	// Remove the old/current password input.
	    	var $oldPasswordInput = $rootEle.find('.js-password-old-input');
	    	$oldPasswordInput.remove();

	        this.authUtilsForTest.configFormInputElements();

	        assert.equal($rootEle.find('.js-submit-button').prop('disabled'), true, 
	        	'After configuring the form input elements, the submit button should be disabled.');
	        assert.equal(this.authUtilsForTest.isTermsAccepted(), true, 
	        	'After configuring the form input elements in a scenario where the terms checkbox is NOT displayed, the module property should reflect a true state.');
	        assert.equal(this.authUtilsForTest.isPasswordOldProvided(), true, 
	        	'After configuring the form input elements in a scenario where the old/current password input is NOT displayed, the module property should reflect a true state.');
	    });

	});

	describe('handlers that impact submit button', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="password" class="js-password-old-input" value="Abcdef1!" />' + 
				'	<input type="password" class="js-password1-input" value="ghiJkl2!" />' + 
				'	<input type="password" class="js-password2-input" value="ghiJkl2!" />' + 
				'	<span class="js-t-and-c-input-control"></span>' + 
				'	<input type="checkbox" class="js-t-and-c-input" checked />' + 
				'	<input type="submit" class="js-submit-button" />' + 
				'</div>'
			);

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('all rules met', function () {
	    	var $rootEle = $('#root-ele');

	        var $submitButton = $rootEle.find('.js-submit-button');

	        this.authUtilsForTest.handleOnClickTermsCheckbox();
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If not all of the input rules are met, the submit button should be disabled.');

	        this.authUtilsForTest.handleOnChangePasswordInput();
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If not all of the input rules are met, the submit button should be disabled.');

	        this.authUtilsForTest.handleOnChangePasswordConfirmInput();
	        assert.equal($submitButton.prop('disabled'), true, 
	        	'If not all of the input rules are met, the submit button should be disabled.');

	        this.authUtilsForTest.handleOnChangePasswordOldInput();
	        assert.equal($submitButton.prop('disabled'), false, 
	        	'If all input rules are met, the submit button should be enabled.');
	    });

	});

	describe('verifyStringContainsAtLeastEightLetters', function () {

	    beforeEach(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('string null', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters(null), false, 
	        	'A null string should not evaluate as being at least eight letters.');
	    });

	    it('string undefined', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters({}.foo), false, 
	        	'An undefined string should not evaluate as being at least eight letters.');
	    });

	    it('string length 0', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters(''), false, 
	        	'A zero-length string should not evaluate as being at least eight letters.');
	    });

	    it('string length 7 letters', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters('abcdefg'), false, 
	        	'A seven-letter-length string should not evaluate as being at least eight letters.');
	    });

	    it('string length 7 digits', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters('1234567'), false, 
	        	'A seven-digit-length string should not evaluate as being at least eight letters.');
	    });

	    it('string length 8 letters', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters('abcdefgh'), true, 
	        	'A eight-letter-length string should not evaluate as being at least eight letters.');
	    });

	    it('string length 8 digits', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters('12345678'), true, 
	        	'A eight-digit-length string should not evaluate as being at least eight letters.');
	    });

	    it('string length 9 letters', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters('abcdefghi'), true, 
	        	'A nine-letter-length string should not evaluate as being at least eight letters.');
	    });

	    it('string length 9 digits', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastEightLetters('123456789'), true, 
	        	'A nine-digit-length string should not evaluate as being at least eight letters.');
	    });

	});

	describe('verifyStringContainsAtLeastOneCharacter', function () {

	    beforeEach(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('string null', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter(null), false, 
	        	'A null string should not evaluate as containing at least one letter.');
	    });

	    it('string undefined', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter({}.foo), false, 
	        	'An undefined string should not evaluate as containing at least one letter.');
	    });

	    it('string length 0', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter(''), false, 
	        	'A zero-length string should not evaluate as containing at least one letter.');
	    });

	    it('string with one uppercase letter', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter('A'), 
	        	'A string with one uppercase letter should evaluate as containing at least one letter.');
	    });

	    it('string with one lowercase letter', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter('b'), 
	        	'A string with one lowercase letter should evaluate as containing at least one letter.');
	    });

	    it('string with only digits', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter('12345678'), 
	        	'A string with only digits should not evaluate as containing at least one letter.');
	    });

	    it('string with only special characters', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter('!@#$%^&*()<>{}[]|\\//?"\'~`+=-_:;,.'), 
	        	'A string with only special characters should evaluate as containing at least one letter.');
	    });

	    it('string with several uppercase letters', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter('ABC'), 
	        	'A string with several uppercase letters should evaluate as containing at least one letter.');
	    });

	    it('string with several lowercase letters', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneCharacter('def'), 
	        	'A string with several lowercase letters should evaluate as containing at least one letter.');
	    });

	});

	describe('verifyStringContainsAtLeastOneDigit', function () {

	    beforeEach(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('string null', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit(null), false, 
	        	'A null string should not evaluate as containing at least one digit.');
	    });

	    it('string undefined', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit({}.foo), false, 
	        	'An undefined string should not evaluate as containing at least one digit.');
	    });

	    it('string length 0', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit(''), false, 
	        	'A zero-length string should not evaluate as containing at least one digit.');
	    });

	    it('string with one uppercase letter', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit('A'), 
	        	'A string with one uppercase letter should not evaluate as containing at least one digit.');
	    });

	    it('string with one lowercase letter', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit('b'), 
	        	'A string with one lowercase letter should not evaluate as containing at least one digit.');
	    });

	    it('string with one digit', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit('0'), 
	        	'A string with only digits should evaluate as containing at least one digit.');
	    });

	    it('string with only digits', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit('12345678'), 
	        	'A string with only digits should evaluate as containing at least one digit.');
	    });

	    it('string with only special characters', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit('!@#$%^&*()<>{}[]|\\//?"\'~`+=-_:;,.'), 
	        	'A string with only special characters should not evaluate as containing at least one digit.');
	    });

	    it('string with several uppercase letters', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit('ABC'), 
	        	'A string with several uppercase letters should not evaluate as containing at least one digit.');
	    });

	    it('string with several lowercase letters', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneDigit('def'), 
	        	'A string with several lowercase letters should not evaluate as containing at least one digit.');
	    });

	});

	describe('verifyStringContainsAtLeastOneSpecialCharacter', function () {

	    beforeEach(function() {
			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/auth-utils');
	        this.authUtilsForTest = injector({

	        });
	    });

	    it('string null', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter(null), false, 
	        	'A null string should not evaluate as containing at least one special character.');
	    });

	    it('string undefined', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter({}.foo), false, 
	        	'An undefined string should not evaluate as containing at least one special character.');
	    });

	    it('string length 0', function () {
	        assert.equal(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter(''), false, 
	        	'A zero-length string should not evaluate as containing at least one special character.');
	    });

	    it('string with one uppercase letter', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('A'), 
	        	'A string with one uppercase letter should not evaluate as containing at least one special character.');
	    });

	    it('string with one lowercase letter', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('b'), 
	        	'A string with one lowercase letter should not evaluate as containing at least one special character.');
	    });

	    it('string with one digit', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('0'), 
	        	'A string with only digits should not evaluate as containing at least one special character.');
	    });

	    it('string with only digits', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('12345678'), 
	        	'A string with only digits should not evaluate as containing at least one special character.');
	    });

	    it('string with only SFDC special characters', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('$!#%+-=_<>'), 
	        	'A string with only SFDC special characters should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - $', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('$'), 
	        	'A string with only one SFDC special character ($) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - !', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('!'), 
	        	'A string with only one SFDC special character (!) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - #', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('#'), 
	        	'A string with only one SFDC special character (#) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - %', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('%'), 
	        	'A string with only one SFDC special character ($) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - +', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('+'), 
	        	'A string with only one SFDC special character (+) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - -', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('-'), 
	        	'A string with only one SFDC special character (-) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - =', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('='), 
	        	'A string with only one SFDC special character (=) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - _', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('_'), 
	        	'A string with only one SFDC special character (_) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - <', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('<'), 
	        	'A string with only one SFDC special character (<) should evaluate as containing at least one special character.');
	    });

	    it('string with only one SFDC special character - >', function () {
	        assert.isOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('>'), 
	        	'A string with only one SFDC special character (>) should evaluate as containing at least one special character.');
	    });

	    it('string with only non-SFDC special characters', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('@^&*(){}[]|\\//?"\'~`:;,.'), 
	        	'A string with only non-SFDC special characters should not evaluate as containing at least one special character.');
	    });

	    it('string with several uppercase letters', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('ABC'), 
	        	'A string with several uppercase letters should not evaluate as containing at least one special character.');
	    });

	    it('string with several lowercase letters', function () {
	        assert.isNotOk(this.authUtilsForTest.verifyStringContainsAtLeastOneSpecialCharacter('def'), 
	        	'A string with several lowercase letters should not evaluate as containing at least one special character.');
	    });

	});

});


