'use strict';

var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');

var assignmentStatus = require("../../../app/common/assignment-status");

describe('dashboard-utils', function () {

	describe('displayHeroBar', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<div id="dashboard-hero-bar"></div>'
			);
	        	        
	        this.assignmentStatusMock = {
				calcTalentIsFormer: function() {
					return true;
				}, 
				calcTalentIsFormerNotRecent: function() {
					return false;
				}
			};

			this.heroBarTemplateMock = sinon.spy(function() {
				return 'inserted';
			});

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelData: function(modelId) {
					return {};
				}
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var dashboardUtilsInjector = require('inject-loader!../../../app/common/dashboard-utils');
	        this.dashboardUtilsForTest = dashboardUtilsInjector({
	            '../common/assignment-status': this.assignmentStatusMock, 
	            '../templates/dashboard-hero-bar.hbs': this.heroBarTemplateMock, 
	            '../common/model-utils': this.modelUtilsMock, 
	            '../common/template-utils': this.templateUtilsMock
	        });
	    });

	    it('displayHeroBar', function () {
	    	var domContent = $('#dashboard-hero-bar').html();
	    	assert.equal(domContent, '');
	        this.dashboardUtilsForTest.displayHeroBar();

	        assert(this.heroBarTemplateMock.calledOnce, 'The heroBarTemplateMock should be called once.');
	        assert(this.heroBarTemplateMock.calledWith({'isTalentFormer': true, 'isTalentFormerNotRecent': false}), 
	        	'The heroBarTemplateMock should be called with a context for a recent FORMER.');

	        domContent = $('#dashboard-hero-bar').html();
	    	assert.equal(domContent, 'inserted', 'The DOM should have the template content inserted into it.');
	    });

	});

	describe('displayAssignmentStatusAndJobsTeaser', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<div class="js-assignment-status-and-jobs-teaser"></div>'
			);
	        	        
	        this.assignmentStatusMock = {
	        	TALENT_STATUS_INACTIVE: assignmentStatus.TALENT_STATUS_INACTIVE, 
				buildTalentAssignmentStatus: sinon.spy(function() {
					return {
						'fake': 'status object'
					};
				}), 
				displayAssignmentStatus: sinon.spy(), 
				calcTalentIsFormer: function() {
					return false;
				}
			};

			this.jobsTeaserMock = {
				displayJobsTeaser: sinon.spy()
			};

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveCollectionData: function(collectionId) {
					return [{
						id: "fakeTalentWorkHistory"
					}];
				}, 

				retrieveModelData: function(modelId) {
					var result = null;
					if (modelId === 'RunningUser') {
						result = {
							Contact: {
								End_Date_Change_Requested__c: null
							}
						}
					} else if (modelId === 'OpenApproachingEndDateNoticeCase') {
						result = {
							id: "fakeOpenApproachingEndDateNoticeCase"
						}
					}
					return result;
				}
			};

			this.eventUtilsMock = {
				publishEvent: sinon.spy(), 
				subscribeToEvent: sinon.spy()
			};

			this.assignmentStatusAndJobsTeaserTemplateMock = sinon.spy(function() {
				return 'status and teaser content';
			});
			this.jobsTeaserOnlyTemplateMock = sinon.spy(function() {
				return 'teaser only content';
			});

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var dashboardUtilsInjector = require('inject-loader!../../../app/common/dashboard-utils');
	        this.dashboardUtilsForTest = dashboardUtilsInjector({
	        	'../templates/dashboard-assign-status-jobs-teaser.hbs': this.assignmentStatusAndJobsTeaserTemplateMock,
	        	'../templates/dashboard-jobs-teaser-only.hbs': this.jobsTeaserOnlyTemplateMock,
	            '../common/assignment-status': this.assignmentStatusMock, 
	            '../common/jobs-teaser': this.jobsTeaserMock, 
	            '../common/event-utils': this.eventUtilsMock, 
	            '../common/model-utils': this.modelUtilsMock
	        });
	    });

	    it('display for current', function () {
	    	var jobSearchDaysCount = 30;
	        this.dashboardUtilsForTest.displayAssignmentStatusAndJobsTeaser(jobSearchDaysCount);

	        var domContent = $('.js-assignment-status-and-jobs-teaser').html();
	    	assert.equal(domContent, 'status and teaser content', 'For a current, the DOM should have the status and teaser template content inserted into it.');
	        
	        assert(this.jobsTeaserMock.displayJobsTeaser.calledOnce, 'For a current, the jobs teaser card should display.');
	        assert(this.jobsTeaserMock.displayJobsTeaser.calledWith(sinon.match.any, jobSearchDaysCount, {'fake': 'status object'}, sinon.match.func), 
	        	'For a current, the jobs teaser card should display, using the expected assignment status data and days count.');

	        assert(this.eventUtilsMock.subscribeToEvent.calledWith('com.dashboard.onClickRequestEndDateChange', sinon.match.any), 
	        	'For a current, the system should be prepared to handle the user requesting an end date change.');
	    });

	    it('display for former', function () {
	    	var jobSearchDaysCount = 30;
	    	// Simulate a FORMER experience.
	    	this.assignmentStatusMock.calcTalentIsFormer = function() {
				return true;
			};
	        this.dashboardUtilsForTest.displayAssignmentStatusAndJobsTeaser(jobSearchDaysCount);

	        var domContent = $('.js-assignment-status-and-jobs-teaser').html();
	    	assert.equal(domContent, 'teaser only content', 'For a former, the DOM should have only the teaser template content inserted into it.');
	        
	        assert(this.jobsTeaserMock.displayJobsTeaser.calledOnce, 'For a former, the jobs teaser card should display.');
	        assert(this.jobsTeaserMock.displayJobsTeaser.calledWith(sinon.match.any, jobSearchDaysCount, 
	        	sinon.match.has('status', sinon.match(assignmentStatus.TALENT_STATUS_INACTIVE)), sinon.match.func), 
	        	'For a former, the jobs teaser card should display, using the expected assignment status data (with status property forced to inactive) and days count.');
	    });

	});

});


