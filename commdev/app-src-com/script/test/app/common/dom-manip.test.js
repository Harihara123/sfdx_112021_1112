import test from 'ava';

import domManip from '../../../app/common/dom-manip';

test.beforeEach('write to document', function(t) {
	document.write(
		'<div id="header-refer-friend-box" class="u-refer-a-friend">' + 
		'   <p id="header-refer-friend-tagline-box" class="u-refer-a-friend__text">{{{TC_refer_a_friend_tagline_aerotek}}}</p>' + 
		'   <a href="/aerotek/tc_refer_friend">' + 
		'   Refer a Friend' + 
		'   <span class="slds-icon__container">' + 
		'       <svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' + 
		'           <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/aerotek/resource/SLDS011/assets/icons/utility-sprite/svg/symbols.svg#forward"></use>' + 
		'       </svg>' + 
		'   </span>' + 
		'   </a>' + 
		'</div>'
	);
})

test('replaceMergeFields narrow', async function(t) {
	t.plan(1);

	var mergeMap = {
		'TC_refer_a_friend_tagline': 'Know someone looking for a job?', 
		'TC_refer_a_friend_tagline_aerotek': 'Don\'t let your friends job search alone!'
	};

	/*
	 * Target the immediate parent element of the text we want to merge.
	 */
	domManip.replaceMergeFields('header-refer-friend-tagline-box', mergeMap);

	var domEle = document.getElementById('header-refer-friend-tagline-box');
	t.is(domEle.textContent, 'Don\'t let your friends job search alone!');
});

test('replaceMergeFields wide', async function(t) {
	t.plan(1);

	var mergeMap = {
		'TC_refer_a_friend_tagline': 'Know someone looking for a job?', 
		'TC_refer_a_friend_tagline_aerotek': 'Don\'t let your friends job search alone!'
	};

	/*
	 * Target a higher-up parent element of the text we want to merge.
	 */
	domManip.replaceMergeFields('header-refer-friend-box', mergeMap);

	var domEle = document.getElementById('header-refer-friend-tagline-box');
	t.is(domEle.textContent, 'Don\'t let your friends job search alone!');
});

test('buildScriptElement', async function(t) {
	t.plan(2);

	var scriptNode = domManip.buildScriptElement('http://foo.com/_res/js/s_code.js', true);

	t.is(scriptNode.async, true);
	t.is(scriptNode.src, 'http://foo.com/_res/js/s_code.js');
});

