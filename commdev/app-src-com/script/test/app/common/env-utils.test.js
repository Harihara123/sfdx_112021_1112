import test from 'ava';

import envUtils from '../../../app/common/env-utils';

test('calcIsProd comdev', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://comdev-allegisconnected.cs83.force.com/teksystems'
		}
	};
	t.falsy(envUtils.calcIsProd());
});

test('calcIsProd comci', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://comci-allegisconnected.cs87.force.com/teksystems/tc_refer_friend'
		}
	};
	t.falsy(envUtils.calcIsProd());
});

test('calcIsProd dev', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://dev-allegisconnected.cs81.force.com/aerotek/tc_login_aerotek?retURL=tc_login_aerotek'
		}
	};
	t.falsy(envUtils.calcIsProd());
});

test('calcIsProd test', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://test-allegisconnected.cs81.force.com/aerotek/tc_jobs'
		}
	};
	t.falsy(envUtils.calcIsProd());
});

test('calcIsProd load', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://load-allegisconnected.cs80.force.com/teksystems/tc_jobs'
		}
	};
	//console.log(window.location.href);
	t.falsy(envUtils.calcIsProd());
});

test('calcIsProd prod aerotek', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://connect.aerotek.com/tc_jobs'
		}
	};
	t.truthy(envUtils.calcIsProd());
});

test('calcIsProd prod teksystems', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://connect.teksystems.com/tc_login_teksystems'
		}
	};
	t.truthy(envUtils.calcIsProd());
});

test('calcOpCo teksystems prod', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://connect.teksystems.com/tc_login_teksystems'
		}
	};
	var slug = envUtils.calcOpCo();
	t.is(slug, 'teksystems');
});

test('calcOpCo teksystems non-prod', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://comci-allegisconnected.cs87.force.com/teksystems/tc_refer_friend'
		}
	};
	var slug = envUtils.calcOpCo();
	t.is(slug, 'teksystems');
});

test('calcOpCo aerotek prod', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://connect.aerotek.com/tc_jobs'
		}
	};
	var slug = envUtils.calcOpCo();
	t.is(slug, 'aerotek');
});

test('calcOpCo aerotek non-prod', async function(t) {
	t.plan(1);

	window = {
		location: {
			href: 'https://test-allegisconnected.cs81.force.com/aerotek/tc_jobs'
		}
	};
	var slug = envUtils.calcOpCo();
	t.is(slug, 'aerotek');
});
