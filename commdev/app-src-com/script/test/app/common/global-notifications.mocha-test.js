'use strict';

require('expose?$!expose?jQuery!jquery');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');
var moment = require('moment');
var pubSub = require("expose?pubSub!pubsub-js");

describe('global-notifications', function () {

	describe('initGlobalNotifications', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast"></div>' + 
				'	<div id="global-info-toast"></div>' + 
				'	<div id="global-success-toast"></div>' + 
				'	<div id="global-warning-toast"></div>' + 
				'</div>'
			);

			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({
	        	'../common/template-utils': this.templateUtilsMock
	        });
	    });

	    it('execute', function () {
	    	var $rootEle = $('#root-ele');

	    	assert.equal($rootEle.find('#global-error-toast').html(), '', 
	    		'Before initialization, the global error box should be empty.');
	    	assert.equal($rootEle.find('#global-info-toast').html(), '', 
	    		'Before initialization, the global info box should be empty.');
	    	assert.equal($rootEle.find('#global-success-toast').html(), '', 
	    		'Before initialization, the global success box should be empty.');
	    	assert.equal($rootEle.find('#global-warning-toast').html(), '', 
	    		'Before initialization, the global warning box should be empty.');

	        this.globalNotificationsForTest.initGlobalNotifications();

	    	assert.notEqual($rootEle.find('#global-error-toast').html(), '', 
	    		'After initialization, the global error box should not be empty.');
	    	assert.notEqual($rootEle.find('#global-info-toast').html(), '', 
	    		'After initialization, the global info box should not be empty.');
	    	assert.notEqual($rootEle.find('#global-success-toast').html(), '', 
	    		'After initialization, the global success box should not be empty.');
	    	assert.notEqual($rootEle.find('#global-warning-toast').html(), '', 
	    		'After initialization, the global warning box should not be empty.');
	    });

	});

	describe('displayNotificationError', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-info-toast">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-success-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-warning-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'</div>'
			);

			var classScope = this;

			this.uiUtilsMock = {
				scrollToElement: sinon.spy()
			}

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({
	        	'../common/ui-utils': this.uiUtilsMock
	        });
	    });

	    it('custom close event yes, scroll target yes', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = 'com.sample.error';
	    	var scrollToDomEle = $rootEle.find('#global-error-toast');
	        this.globalNotificationsForTest.displayNotificationError(notificationContent, initiatorId, closeEvent, scrollToDomEle);

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-error-toast').html().indexOf(notificationContent) > -1, 
	    		'The global error box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-error-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global error box should be assigned to the box.');

	        assert(classScope.uiUtilsMock.scrollToElement.calledOnce, 
	        	'Because a scroll-to element is specified, it should be scrolled-to.');
	        assert(classScope.uiUtilsMock.scrollToElement.calledWith(scrollToDomEle, sinon.match.any), 
	        	'The specified scroll-to element should be scrolled-to.');

	       	var closeButton = $rootEle.find('#global-error-toast .js-notify-close-button')[0];
	        pubSub.subscribe(closeEvent, function(msg, data) {
	        	assert(true, 'The custom close event should be triggered when the global error box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	    it('custom close event no, scroll target no', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = null;
	    	var scrollToDomEle = null;
	        this.globalNotificationsForTest.displayNotificationError(notificationContent, initiatorId, closeEvent, scrollToDomEle);

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-error-toast').html().indexOf(notificationContent) > -1, 
	    		'The global error box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-error-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global error box should be assigned to the box.');

	        assert.isNotTrue(classScope.uiUtilsMock.scrollToElement.called, 
	        	'Because a scroll-to element is not specified, nothing should be scrolled-to.');

	       	var closeButton = $rootEle.find('#global-error-toast .js-notify-close-button')[0];
	        pubSub.subscribe('com.header.clearGlobalError', function(msg, data) {
	        	assert(true, 'The standard close event should be triggered when the global error box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	});

	describe('displayNotificationInfo', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-info-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-success-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-warning-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('custom close event yes', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = 'com.sample.info';
	        this.globalNotificationsForTest.displayNotificationInfo(notificationContent, initiatorId, closeEvent);

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').html().indexOf(notificationContent) > -1, 
	    		'The global info box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-info-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global info box should be assigned to the box.');

	       	var closeButton = $rootEle.find('#global-info-toast .js-notify-close-button')[0];
	        pubSub.subscribe(closeEvent, function(msg, data) {
	        	assert(true, 'The custom close event should be triggered when the global info box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	    it('custom close event no', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = null;
	        this.globalNotificationsForTest.displayNotificationInfo(notificationContent, initiatorId, closeEvent);

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').html().indexOf(notificationContent) > -1, 
	    		'The global info box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-info-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global info box should be assigned to the box.');

	       	var closeButton = $rootEle.find('#global-info-toast .js-notify-close-button')[0];
	        pubSub.subscribe('com.header.clearGlobalInfo', function(msg, data) {
	        	assert(true, 'The standard close event should be triggered when the global info box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	});

	describe('displayNotificationSuccess', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-info-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-success-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-warning-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('custom close event yes', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = 'com.sample.success';
	        this.globalNotificationsForTest.displayNotificationSuccess(notificationContent, initiatorId, closeEvent);

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').html().indexOf(notificationContent) > -1, 
	    		'The global info box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-success-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global success box should be assigned to the box.');

	       	var closeButton = $rootEle.find('#global-success-toast .js-notify-close-button')[0];
	        pubSub.subscribe(closeEvent, function(msg, data) {
	        	assert(true, 'The custom close event should be triggered when the global success box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	    it('custom close event no', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = null;
	        this.globalNotificationsForTest.displayNotificationSuccess(notificationContent, initiatorId, closeEvent);

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').html().indexOf(notificationContent) > -1, 
	    		'The global info box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-success-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global success box should be assigned to the box.');

	       	var closeButton = $rootEle.find('#global-success-toast .js-notify-close-button')[0];
	        pubSub.subscribe('com.header.clearGlobalSuccess', function(msg, data) {
	        	assert(true, 'The standard close event should be triggered when the global success box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	});

	describe('displayNotificationWarning', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-info-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-success-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-warning-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('custom close event yes', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = 'com.sample.warning';
	        this.globalNotificationsForTest.displayNotificationWarning(notificationContent, initiatorId, closeEvent);

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').html().indexOf(notificationContent) > -1, 
	    		'The global warning box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-warning-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global warning box should be assigned to the box.');

	       	var closeButton = $rootEle.find('#global-warning-toast .js-notify-close-button')[0];
	        pubSub.subscribe(closeEvent, function(msg, data) {
	        	assert(true, 'The custom close event should be triggered when the global warning box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	    it('custom close event no', function (done) {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	    	var notificationContent = 'Sample message';
	    	var initiatorId = 'sample-initiator';
	    	var closeEvent = null;
	        this.globalNotificationsForTest.displayNotificationWarning(notificationContent, initiatorId, closeEvent);

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').html().indexOf(notificationContent) > -1, 
	    		'The global warning box should contain the supplied notification content.');

	    	assert.equal($rootEle.find('#global-warning-toast').attr('data-initiator'), initiatorId, 
	    		'After display, the initiator of the global warning box should be assigned to the box.');

	       	var closeButton = $rootEle.find('#global-warning-toast .js-notify-close-button')[0];
	        pubSub.subscribe('com.header.clearGlobalWarning', function(msg, data) {
	        	assert(true, 'The standard close event should be triggered when the global warning box is closed.');
	        	done();
	        });
	        closeButton.click();
	    });

	});

	describe('displayNotificationCatchAllError', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-info-toast">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-success-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'	<div id="global-warning-toast" class="hidden">' + 
				'		<h2 class="slds-text-heading--small"></h2><button class="js-notify-close-button"></button>' + 
				'	</div>' + 
				'</div>'
			);

			var classScope = this;

			this.i18nUtilsMock = {
				retrieveText: function() {
					return 'An unexpected error occurred. Please reload the page and try again. ' + 
						'If the problem persists, please consult &lt;a href=&quot;{{$Site.Prefix}}/help&quot;&gt;Help and Support&lt;/a&gt;. ' + 
						'We apologize for the inconvenience.';
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({
	        	'../common/i18n-utils': this.i18nUtilsMock
	        });
	    });

	    it('execute', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before display, the global error box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before display, the global info box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before display, the global warning box should be hidden.');

	        this.globalNotificationsForTest.displayNotificationCatchAllError();

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After display, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After display, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After display, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After display, the global warning box should be hidden.');

	    	assert.isTrue($rootEle.find('#global-error-toast').html().indexOf('An unexpected error occurred') > -1, 
	    		'The message shown in the global error box should contain the standard label text.');
	    	assert.isFalse($rootEle.find('#global-error-toast').html().indexOf('Site.Prefix') > -1, 
	    		'The message shown in the global error box should not contain the to-be-replaced placeholder.');
	    });

	});

	describe('clearGlobalNotifications', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast"></div>' + 
				'	<div id="global-info-toast"></div>' + 
				'	<div id="global-success-toast"></div>' + 
				'	<div id="global-warning-toast"></div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('execute', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before clearing, the global error box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before clearing, the global info box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before clearing, the global success box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before clearing, the global warning box should not be hidden.');

	        this.globalNotificationsForTest.clearGlobalNotifications();

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After clearing, the global error box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After clearing, the global info box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After clearing, the global success box should be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After clearing, the global warning box should be hidden.');
	    });

	});

	describe('clearNotificationError', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast"></div>' + 
				'	<div id="global-info-toast"></div>' + 
				'	<div id="global-success-toast"></div>' + 
				'	<div id="global-warning-toast"></div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('execute', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before clearing, the global error box should not be hidden.');

	        this.globalNotificationsForTest.clearNotificationError();

	    	assert.isTrue($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After clearing, the global error box should be hidden.');
	    });

	});

	describe('clearNotificationInfo', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast"></div>' + 
				'	<div id="global-info-toast"></div>' + 
				'	<div id="global-success-toast"></div>' + 
				'	<div id="global-warning-toast"></div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('execute', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before clearing, the global info box should not be hidden.');

	        this.globalNotificationsForTest.clearNotificationInfo();

	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After clearing, the global info box should be hidden.');
	    });

	});

	describe('clearNotificationSuccess', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast"></div>' + 
				'	<div id="global-info-toast"></div>' + 
				'	<div id="global-success-toast"></div>' + 
				'	<div id="global-warning-toast"></div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('execute', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before clearing, the global success box should not be hidden.');

	        this.globalNotificationsForTest.clearNotificationSuccess();

	    	assert.isTrue($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After clearing, the global success box should be hidden.');
	    });

	});

	describe('clearNotificationWarning', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast"></div>' + 
				'	<div id="global-info-toast"></div>' + 
				'	<div id="global-success-toast"></div>' + 
				'	<div id="global-warning-toast"></div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('execute', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before clearing, the global warning box should not be hidden.');

	        this.globalNotificationsForTest.clearNotificationWarning();

	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After clearing, the global warning box should be hidden.');
	    });

	});

	describe('clearNotificationsForInitiator', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<div id="global-error-toast" data-initiator="init1"></div>' + 
				'	<div id="global-info-toast" data-initiator="init2"></div>' + 
				'	<div id="global-success-toast" data-initiator="init3"></div>' + 
				'	<div id="global-warning-toast" data-initiator="init4"></div>' + 
				'</div>'
			);

			var classScope = this;

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/global-notifications');
	        this.globalNotificationsForTest = injector({

	        });
	    });

	    it('clear for initiator 2', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 2, the global error box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 2, the global info box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 2, the global success box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 2, the global warning box should not be hidden.');

	        this.globalNotificationsForTest.clearNotificationsForInitiator('init2');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After clearing for initiator 2, the global error box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After clearing for initiator 2, the global info box should be hidden.');
	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After clearing for initiator 2, the global success box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After clearing for initiator 2, the global warning box should not be hidden.');
	    });

	    it('clear for initiator 4', function () {
	    	var classScope = this;
	    	var $rootEle = $('#root-ele');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 4, the global error box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 4, the global info box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 4, the global success box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'Before clearing for initiator 4, the global warning box should not be hidden.');

	        this.globalNotificationsForTest.clearNotificationsForInitiator('init4');

	    	assert.isFalse($rootEle.find('#global-error-toast').hasClass('hidden'), 
	    		'After clearing for initiator 4, the global error box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-info-toast').hasClass('hidden'), 
	    		'After clearing for initiator 4, the global info box should not be hidden.');
	    	assert.isFalse($rootEle.find('#global-success-toast').hasClass('hidden'), 
	    		'After clearing for initiator 4, the global success box should not be hidden.');
	    	assert.isTrue($rootEle.find('#global-warning-toast').hasClass('hidden'), 
	    		'After clearing for initiator 4, the global warning box should be hidden.');
	    });

	});

});


