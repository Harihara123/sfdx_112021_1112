'use strict';

require('expose?$!expose?jQuery!jquery');
var moment = require('moment');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');

var assignmentStatus = require('../../../app/common/assignment-status');
var jobsTeaser = require('../../../app/common/jobs-teaser');

describe('jobs-teaser', function () {

	describe('displayJobsTeaser', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'</div>'
			);

	    	// Bootstrap some model objects.
			this.queryDefinitionMock = {
				getConditionByName: function() {

				}, 

				setCondition: function() {

				}
			};

			this.userDataMock = {
				Contact: {
					Title: 'Developer', 
					MailingCity: 'Baltimore', 
					MailingState: 'MD'
				}
			};

			this.queryDataMock = {
				searchParameters: {
					recordCount: 15, 
					radius: 50
				}, 
				// Populate with more jobs than job-teaser.JOBS_LISTING_DISPLAY_COUNT
				jobs: [
					{
						'qualifieddate': '2017-04-18'
					}, {
						'qualifieddate': '2017-04-19'
					}, {
						'qualifieddate': '2017-04-20'
					}, {
						'qualifieddate': '2017-04-20'
					}
				]
			}

			var classScope = this;

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelDefinition: function(modelId) {
					return classScope.queryDefinitionMock;
				}, 

				retrieveModelData: function(modelIdOrModel) {
					var result = null;
					if (modelIdOrModel === 'RunningUser') {
						result = classScope.userDataMock;
					} /* queryModel */ else {
						result = classScope.queryDataMock;
					}
					return result;
				}, 

				// Spy on the model object(s) being fetched (from the server).
				fetchModelData: sinon.spy(function() {
					return {
						done: function(callback) {
							callback();
						}
					};
				}), 

				// Spy on how the model object(s) are updated, which what values.
				updateModelValues: sinon.spy(), 

				// Spy on the model object(s) being saved.
				saveModelData: sinon.spy(function(modelData, options, modelOrModelArray) {
					options.callback({
						totalsuccess: true
					});
				})
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Silo i18n operations from Skuid.
			this.i18nUtilsMock = {
				retrieveText: function() {
					return '';
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/jobs-teaser');
	        this.jobsTeaserForTest = injector({
	        	'../common/template-utils': this.templateUtilsMock, 
	            '../common/model-utils': this.modelUtilsMock, 
	            '../common/i18n-utils': this.i18nUtilsMock
	        });
	    });

	    it('starting status', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var jobSearchDaysCount = 30;
	    	var talentAssignmentStatus = {
	    		'startDate': moment().add(1, 'months').toDate(), 
	        	'endDate': moment().add(11, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_STARTING
	    	};

	        this.jobsTeaserForTest.displayJobsTeaser($rootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
	        	// When in starting status, the jobs teaser card demands less attention.
		        var $jobsTeaserBox = $rootEle.find('.qa-jobs-teaser-explore');
		        assert.equal($jobsTeaserBox.length, 1);

	        	done();
	        });
	    });

	    it('change status', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var jobSearchDaysCount = 30;
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(1, 'months').toDate(), 
	        	'endDate': moment().add(11, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_DATE_CHANGE_REQUESTED
	    	};

	        this.jobsTeaserForTest.displayJobsTeaser($rootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
	        	// When in 'end date change requested' status, the jobs teaser card demands less attention.
		        var $jobsTeaserBox = $rootEle.find('.qa-jobs-teaser-explore');
		        assert.equal($jobsTeaserBox.length, 1);

	        	done();
	        });
	    });

	    it('inactive status', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var jobSearchDaysCount = 30;
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(12, 'months').toDate(), 
	        	'endDate': moment().subtract(2, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_INACTIVE
	    	};

	        this.jobsTeaserForTest.displayJobsTeaser($rootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
	        	// When in inactive status, the jobs teaser card displays a listing of the top-most matched jobs.
		        var $jobsTeaserBox = $rootEle.find('.qa-jobs-teaser-listing');
		        assert.equal($jobsTeaserBox.length, 1);

		        var $reqItemListingBoxes = $rootEle.find('.qa-req-listing-item-box');
		        assert.equal($reqItemListingBoxes.length, jobsTeaser.JOBS_LISTING_DISPLAY_COUNT, 'The number of displayed jobs should match the maximum number to display.');

	        	done();
	        });
	    });

	    it('active status, far from end', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var jobSearchDaysCount = 30;
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(11, 'months').toDate(), 
	        	'endDate': moment().add(5, 'months').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
	    	};

	        this.jobsTeaserForTest.displayJobsTeaser($rootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
	        	/*
	        	 * When in active status (with the assignment end date a ways off), the jobs teaser card demands 
	        	 *	less attention.
	        	 */
		        var $jobsTeaserBox = $rootEle.find('.qa-jobs-teaser-explore');
		        assert.equal($jobsTeaserBox.length, 1);

	        	done();
	        });
	    });

	    it('active status, close to end, jobs found yes, radius specified', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var jobSearchDaysCount = 30;
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(11, 'months').toDate(), 
	        	'endDate': moment().add(5, 'days').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
	    	};

	        this.jobsTeaserForTest.displayJobsTeaser($rootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
	        	/*
	        	 * When in active status (with the assignment end date nearing), the jobs teaser card demands 
	        	 *	more attention.
	        	 */
		        var $jobsTeaserBox = $rootEle.find('.qa-jobs-teaser-matches');
		        assert.equal($jobsTeaserBox.length, 1);

	        	done();
	        });
	    });

	    it('active status, close to end, jobs found no, radius specified', function (done) {
	    	// Make it so that no jobs are found.
	    	this.queryDataMock.searchParameters.recordCount = 0;

	    	var $rootEle = $('#root-ele');
	    	var jobSearchDaysCount = 30;
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(11, 'months').toDate(), 
	        	'endDate': moment().add(5, 'days').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
	    	};

	        this.jobsTeaserForTest.displayJobsTeaser($rootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
	        	/*
	        	 * When in active status (with the assignment end date near), but when no jobs are found, the jobs 
	        	 *	teaser card demands less attention.
	        	 */
		        var $jobsTeaserBox = $rootEle.find('.qa-jobs-teaser-explore');
		        assert.equal($jobsTeaserBox.length, 1);

	        	done();
	        });

	    });

	    it('active status, close to end, jobs found yes, radius everywhere', function (done) {
	    	// Make it so that the user has not provided their location.
	    	this.queryDataMock.searchParameters.radius = 'Everywhere';

	    	var $rootEle = $('#root-ele');
	    	var jobSearchDaysCount = 30;
	    	var talentAssignmentStatus = {
	    		'startDate': moment().subtract(11, 'months').toDate(), 
	        	'endDate': moment().add(5, 'days').toDate(), 
                'status': assignmentStatus.TALENT_STATUS_ACTIVE
	    	};

	        this.jobsTeaserForTest.displayJobsTeaser($rootEle, jobSearchDaysCount, talentAssignmentStatus, function() {
	        	/*
	        	 * When in active status (with the assignment end date near), but when the user has not provided 
	        	 *	their location, the jobs teaser card demands less attention.
	        	 */
		        var $jobsTeaserBox = $rootEle.find('.qa-jobs-teaser-explore');
		        assert.equal($jobsTeaserBox.length, 1);

	        	done();
	        });
	    });
	});

});


