'use strict';

require('expose?$!expose?jQuery!jquery');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');
var moment = require('moment');

describe('template-utils', function () {

	describe('buildBaseTemplateContext', function () {

	    beforeEach(function() {
	    	var classScope = this;

			this.envUtilsMock = {
				calcOpCo: function() {
					return classScope.opCoSlug;
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/common/template-utils');
	        this.templateUtilsForTest = injector({
	        	'./env-utils': this.envUtilsMock
	        });
	    });

	    it('aerotek', function () {
	    	this.opCoSlug = 'aerotek';

	        var resultCtx = this.templateUtilsForTest.buildBaseTemplateContext();

	    	assert.isDefined(resultCtx.$Site.Prefix, 
	    		'The template context should always define a property for site prefix.');
	    	assert.equal(resultCtx.isAerotek, true, 
	    		'When the context is seeded with the opco Aerotek, the contained opco getter should reflect that.');
	    	assert.isUndefined(resultCtx.isTeksystems, 
	    		'When the context is seeded with the opco Aerotek, getters for other opco\'s should not be defined.');
	    });

	    it('teksystems', function () {
	    	this.opCoSlug = 'teksystems';

	        var resultCtx = this.templateUtilsForTest.buildBaseTemplateContext();

	    	assert.isDefined(resultCtx.$Site.Prefix, 
	    		'The template context should always define a property for site prefix.');
	    	assert.isUndefined(resultCtx.isAerotek, 
	    		'When the context is seeded with the opco TEKsystems, getters for other opco\'s should not be defined.');
	    	assert.equal(resultCtx.isTeksystems, true, 
	    		'When the context is seeded with the opco TEKsystems, the contained opco getter should reflect that.');
	    });

	});

});


