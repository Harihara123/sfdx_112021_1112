import test from 'ava';

import domManip from '../../../app/common/dom-manip';
import webpackUtils from '../../../app/common/webpack-utils';

test('configureWebpackPublicPath no script', async function(t) {
	t.plan(1);

	global.__webpack_public_path__ = null;

	webpackUtils.configureWebpackPublicPath('app-first.js');

	/*
	 * We did not seed the document with any <script> elements, so __webpack_public_path__ should not 
	 *	be set.
	 */
	t.is(__webpack_public_path__, null);
});

test('configureWebpackPublicPath one script', async function(t) {
	t.plan(1);

	global.__webpack_public_path__ = null;
	var scriptNode = domManip.buildScriptElement('/teksystems/resource/1469789271000/tc_script/app-first.js', false);
	document.head.appendChild(scriptNode);

	webpackUtils.configureWebpackPublicPath('app-first.js');

	/*
	 * We seeded the document with one matching <script> element. Assert that __webpack_public_path__ 
	 *	is set with the path.
	 */
	t.is(__webpack_public_path__, '/teksystems/resource/1469789271000/tc_script/');
});

test('configureWebpackPublicPath many scripts', async function(t) {
	t.plan(1);

	global.__webpack_public_path__ = null;
	var scriptNode1 = domManip.buildScriptElement('/aerotek/static/111213/js/perf/stub.js', false);
	document.head.appendChild(scriptNode1);
	var scriptNode2 = domManip.buildScriptElement('/aerotek/jslibrary/1479750578000/sfdc/VFRemote.js', false);
	document.head.appendChild(scriptNode2);
	var scriptNode3 = domManip.buildScriptElement('/aerotek/resource/1469789271000/tc_script/app.js', false);
	document.head.appendChild(scriptNode3);
	var scriptNode4 = domManip.buildScriptElement('/aerotek/resource/1480448132000/tc_script/dashboard-aerotek.js', false);
	document.head.appendChild(scriptNode4);

	webpackUtils.configureWebpackPublicPath('app.js');

	/*
	 * We seeded the document with several <script> elements. Assert that __webpack_public_path__ 
	 *	is set with the correct path.
	 */
	t.is(__webpack_public_path__, '/aerotek/resource/1469789271000/tc_script/');
});



