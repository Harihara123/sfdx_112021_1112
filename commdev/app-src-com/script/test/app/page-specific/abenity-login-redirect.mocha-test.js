'use strict';

require('expose?$!expose?jQuery!jquery');
var _ = require('lodash/core');
var moment = require('moment');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');

var abenityLoginRedirect = require('../../../app/page-specific/abenity-login-redirect');

describe('abenity-login-redirect', function () {

	describe('onPageLoaded', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<div class="js-session-id">00D9E000000CvEA!AQwAQCQjSorTcRaW3dKlfLTnai4ZCCkKW56CN3ib.x_oKYSfHTGPYS.32rpgNVpI4v1imXzSHR29mYsoF4IFOAtEvitIdVgg</div>' + 
				'<div class="js-abenity-login-error-box" style="display: none;"></div>'
			);

			this.simulateProd = true;

			this.ajaxSuccessDataMock = null;
			this.ajaxErrorDataMock = null;

			var beforeScope = this;

			// Spy on how the browser location is updated, with what values.
			this.browserUtilsMock = {
				makeAjaxRequest: sinon.spy(function(settingsObj) {
					if (beforeScope.ajaxSuccessDataMock !== null) {
						settingsObj.success(beforeScope.ajaxSuccessDataMock);
					}

					if (beforeScope.ajaxErrorDataMock !== null) {
						settingsObj.error(beforeScope.ajaxErrorDataMock);
					}
				}), 

				populateBrowserHref: sinon.spy(), 

				retrieveBrowserProtocol: function() {
					return 'https:';
				}, 

				retrieveBrowserHost: function() {
					return 'connect.aerotek.com';
				}, 

				retrieveBrowserSearch: function() {
					return '';
				}
			};

			// Silo Skuid-dependent functionality.
			this.envUtilsMock = {
				calcIsProd: function() {
					return beforeScope.simulateProd;
				}, 

				calcOpCo: function() {
					return 'aerotek';
				}
			};

			this.eventUtilsMock = {
				publishEvent: sinon.spy()
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/page-specific/abenity-login-redirect');
	        this.abenityLoginRedirectForTest = injector({
	        	'../common/browser-utils': this.browserUtilsMock, 
	        	'../common/env-utils': this.envUtilsMock, 
	            '../common/event-utils': this.eventUtilsMock
	        });
	    });

	    it('ajax success prod yes', function (done) {
	    	// Simulate a success Ajax response.
	    	this.ajaxSuccessDataMock = {
				isSandbox: false, 
				success: 'https://allegisbenefits.employeediscounts.co/perks/process/login?token=6t4UP4eoomoSveq3Bosm7j7XnP28AC4c'
			};

	    	var itScope = this;
	        this.abenityLoginRedirectForTest.handlePageLoaded(10, function() {
	        	var sessionId = $('.js-session-id').text();

	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('method', 'POST')), 
	        		'The method of the Ajax request should be POST');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('url', 
	        		'https://connect.aerotek.com' + abenityLoginRedirect.ABENITY_SSO_SERVICE_URL_PATH)), 
	        		'The URL of the Ajax request should be the current protocol and host, plus the fixed service path.');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('headers', 
	        		{'Authorization': 'Bearer ' + sessionId})), 
	        		'The headers of the Ajax request should contain an authorization property with the session ID.');

		        assert(itScope.browserUtilsMock.populateBrowserHref.calledWith(
		        	'https://allegisbenefits.employeediscounts.co/perks/process/login?token=6t4UP4eoomoSveq3Bosm7j7XnP28AC4c'), 
		        	'The user should be sent to the Abenity-provided SSO URL.');
				done();
	        });
	    });

	    it('ajax success prod no', function (done) {
	    	// Simulate a success Ajax response.
	    	this.ajaxSuccessDataMock = {
				isSandbox: false, 
				success: 'https://allegisbenefits.employeediscounts.co/perks/process/login?token=6t4UP4eoomoSveq3Bosm7j7XnP28AC4c'
			};
	    	this.simulateProd = false;

	    	var itScope = this;
	        this.abenityLoginRedirectForTest.handlePageLoaded(10, function() {
	        	var sessionId = $('.js-session-id').text();

	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('method', 'POST')), 
	        		'The method of the Ajax request should be POST');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('url', 
	        		'https://connect.aerotek.com/aerotek' + abenityLoginRedirect.ABENITY_SSO_SERVICE_URL_PATH)), 
	        		'The URL of the Ajax request should be the current protocol and host, plus site prefix, plus the fixed service path.');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('headers', 
	        		{'Authorization': 'Bearer ' + sessionId})), 
	        		'The headers of the Ajax request should contain an authorization property with the session ID.');

		        assert(itScope.browserUtilsMock.populateBrowserHref.calledWith(
		        	'https://allegisbenefits.employeediscounts.co/perks/process/login?token=6t4UP4eoomoSveq3Bosm7j7XnP28AC4c'), 
		        	'The user should be sent to the Abenity-provided SSO URL.');
				done();
	        });
	    });

	    it('ajax error prod yes', function (done) {
	    	// Simulate an error Ajax response.
	    	this.ajaxErrorDataMock = {
				responseText: JSON.stringify({
            		"messageData": {
                       "error": {
                           "u_surname": "Last Name is required",
                           "u_firstname": "First Name is required",
                           "u_email": "Email must be in a valid name@domain format"
                       }
                   },
                   "message": "[See messageData for Abenity JSON response.]",
                   "errorCode": "Abenity request error"
               })
			};

	    	var itScope = this;
	        this.abenityLoginRedirectForTest.handlePageLoaded(10, function() {
	        	var sessionId = $('.js-session-id').text();

	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('method', 'POST')), 
	        		'The method of the Ajax request should be POST');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('url', 
	        		'https://connect.aerotek.com' + abenityLoginRedirect.ABENITY_SSO_SERVICE_URL_PATH)), 
	        		'The URL of the Ajax request should be the current protocol and host, plus the fixed service path.');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('headers', 
	        		{'Authorization': 'Bearer ' + sessionId})), 
	        		'The headers of the Ajax request should contain an authorization property with the session ID.');

	        	assert.notEqual($('.js-abenity-login-error-box').css('display'), 'none', 
	        		'When the Ajax request returns an error, an error message should be displayed to the user.');
		        assert(itScope.browserUtilsMock.populateBrowserHref.calledWith(
		        	abenityLoginRedirect.ABENITY_FALLBACK_LOGIN_URL), 
		        	'The user should be sent to the fallback Abenity login URL.');
				done();
	        });
	    });

	    it('ajax error prod no', function (done) {
	    	// Simulate an error Ajax response.
	    	this.ajaxErrorDataMock = {
				responseText: JSON.stringify({
            		"messageData": {
                       "error": {
                           "u_surname": "Last Name is required",
                           "u_firstname": "First Name is required",
                           "u_email": "Email must be in a valid name@domain format"
                       }
                   },
                   "message": "[See messageData for Abenity JSON response.]",
                   "errorCode": "Abenity request error"
               })
			};
	    	this.simulateProd = false;

	    	var itScope = this;
	        this.abenityLoginRedirectForTest.handlePageLoaded(10, function() {
	        	var sessionId = $('.js-session-id').text();

	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('method', 'POST')), 
	        		'The method of the Ajax request should be POST');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('url', 
	        		'https://connect.aerotek.com/aerotek' + abenityLoginRedirect.ABENITY_SSO_SERVICE_URL_PATH)), 
	        		'The URL of the Ajax request should be the current protocol and host, plus site prefix, plus the fixed service path.');
	        	assert(itScope.browserUtilsMock.makeAjaxRequest.calledWith(sinon.match.has('headers', 
	        		{'Authorization': 'Bearer ' + sessionId})), 
	        		'The headers of the Ajax request should contain an authorization property with the session ID.');

	        	assert.notEqual($('.js-abenity-login-error-box').css('display'), 'none', 
	        		'When the Ajax request returns an error, an error message should be displayed to the user.');
		        assert(itScope.browserUtilsMock.populateBrowserHref.calledWith(
		        	abenityLoginRedirect.ABENITY_FALLBACK_LOGIN_URL), 
		        	'The user should be sent to the fallback Abenity login URL.');
				done();
	        });
	    });

	});

});


