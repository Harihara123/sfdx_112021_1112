'use strict';

require('expose?$!expose?jQuery!jquery');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');

describe('header', function () {

	describe('onPageLoaded', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<html>' + 
				'	<body>' + 
				'		<div class="js-global-nav-bar-box"></div>' + 
				'		<button id="menu-toggle"></button>' + 
				'		<div class="c-sidebar-menu"></div>' + 
				'		<div class="js-header-menu-box"></div>' + 
				'	</body>' + 
				'</html>'
			);
	        
	        this.assignmentStatusMock = {
				calcTalentIsFormer: function() {
					return true;
				}, 
				calcTalentIsFormerNotRecent: function() {
					return false;
				}
			};

			this.globalNavBarTemplateMock = sinon.spy(function() {
				return 'nav bar content';
			});
			this.headerMenuTemplateMock = sinon.spy(function() {
				return 'header menu content';
			});

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelData: function(modelId) {
					return {};
				}, 

				retrieveUserId: function() {
					return '01pU0000000xgrI';
				}
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Silo i18n operations from Skuid.
			this.i18nUtilsMock = {
				retrieveText: function() {
					return '';
				}
			};

			this.domManipMock = {
				replaceMergeFields: sinon.spy()
			};

			this.uiUtilsMock = {
				makeVisible: sinon.spy()
			};

			this.globalNotificationsMock = {
				initGlobalNotifications: sinon.spy()
			};

			this.svg4everybodyMock = sinon.spy();

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var headerInjector = require('inject-loader!../../../app/page-specific/header');
	        this.headerForTest = headerInjector({
	        	'svg4everybody': this.svg4everybodyMock, 
	            '../common/assignment-status': this.assignmentStatusMock, 
	            '../common/dom-manip': this.domManipMock, 
	            '../common/global-notifications': this.globalNotificationsMock, 
	            '../common/i18n-utils': this.i18nUtilsMock, 
	            '../common/model-utils': this.modelUtilsMock, 
	            '../common/template-utils': this.templateUtilsMock, 
	            '../common/ui-utils': this.uiUtilsMock, 
	            '../templates/global-navigation-bar.hbs': this.globalNavBarTemplateMock, 
	            '../templates/header-menu.hbs': this.headerMenuTemplateMock
	        });
	    });

	    it('prepare and display header', function (done) {
	    	var itScope = this;
	        this.headerForTest.handlePageLoaded(function() {
		        assert(itScope.globalNavBarTemplateMock.calledOnce, 'The globalNavBarTemplate should be called once.');
		        assert(itScope.globalNavBarTemplateMock.calledWith({'isTalentFormer': true, 'isTalentFormerNotRecent': false, 'userId': '01pU0000000xgrI'}), 
		        	'The globalNavBarTemplateMock should be called with a context for a recent FORMER, including user ID.');

		        assert(itScope.headerMenuTemplateMock.calledOnce, 'The headerMenuTemplate should be called once.');
		        assert(itScope.headerMenuTemplateMock.calledWith({'isTalentFormer': true, 'isTalentFormerNotRecent': false, 'userId': '01pU0000000xgrI'}), 
		        	'The headerMenuTemplate should be called with a context for a recent FORMER, including user ID.');

		        assert(itScope.domManipMock.replaceMergeFields.calledOnce, 'An attempt should be made to replace merge fields.');
		        assert(itScope.domManipMock.replaceMergeFields.calledWith('header-refer-friend-tagline-box', 
		        	{'TC_refer_a_friend_tagline': '', 'TC_refer_a_friend_tagline_aerotek': ''}), 
		        	'An attempt should be made to replace the refer-a-friend taglines.');

		        assert(itScope.svg4everybodyMock, 'SVG support should be activated.');
		        assert(itScope.globalNotificationsMock.initGlobalNotifications, 'Global notifications should be initialized.');

		        var globalNavBarContent = $('.js-global-nav-bar-box').html();
		    	assert.equal(globalNavBarContent, 'nav bar content', 'The DOM should have the global nav bar template content inserted into it.');
		        var headerMenuContent = $('.js-header-menu-box').html();
		    	assert.equal(headerMenuContent, 'header menu content', 'The DOM should have the header menu template content inserted into it.');

		    	$('#menu-toggle').click();
		    	assert($('.c-sidebar-menu').hasClass('is-active'), 'Upon clicking the menu button, the menu should be active (displayed).');

		    	$('body').click();
		    	assert.isFalse($('.c-sidebar-menu').hasClass('is-active'), 'Upon clicking the body, the menu should be inactive (hidden).');

				done();
	        });
	    });

	});

});


