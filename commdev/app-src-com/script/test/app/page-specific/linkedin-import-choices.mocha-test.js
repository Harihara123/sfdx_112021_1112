'use strict';

require('expose?$!expose?jQuery!jquery');
var _ = require('lodash/core');
var moment = require('moment');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');

var linkedInImportChoices = require('../../../app/page-specific/linkedin-import-choices');

describe('linkedin-import-choices', function () {

	describe('onFormCancel', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<div class="js-content-box">' + 
				'</div>'
			);

			this.userDataMock = {
				Id: '0054E000001Ija0'
			};

			var beforeScope = this;

			// Spy on how the browser URL path is updated, with what values.
			this.browserUtilsMock = {
				populateBrowserPath: sinon.spy()
			};

			// Silo Skuid-dependent functionality.
			this.envUtilsMock = {
				calcSitePrefix: function() {
					return '/aerotek';
				},
				calcOpCo: function() {
					return 'Aerotek';
				}
			};

			this.eventUtilsMock = {
				subscribeToEvent: function(eventId, callback) {
					/*
					 * After (in reality, as soon as) the event is subscribed to, trigger a form cancel 
					 *	event.
					 */
					if (eventId === 'com.linkedInImport.onFormCancel') {
						callback();
					}
				}
			};

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelData: function(modelIdOrModel) {
					var result = null;
					if (modelIdOrModel === 'RunningUser') {
						result = beforeScope.userDataMock;
					}
					return result;
				}
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/page-specific/linkedin-import-choices');
	        this.linkedInImportChoicesForTest = injector({
	        	'../common/browser-utils': this.browserUtilsMock, 
	        	'../common/env-utils': this.envUtilsMock, 
	            '../common/event-utils': this.eventUtilsMock, 
	            '../common/model-utils': this.modelUtilsMock, 
	            '../common/template-utils': this.templateUtilsMock
	        });
	    });

	    it('form canceled', function (done) {
	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert(this.browserUtilsMock.populateBrowserPath.calledWith('/aerotek/0054E000001Ija0'), 
	        	'The user should be sent to the private user profile page when canceling the form.');
			done();
	    });
	});

	describe('onFormSubmit', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<div class="js-content-box">' + 
				'</div>'
			);

			/*
			 * Stage the scene (prepare the DOM to reflect a specific scenario) before triggering the event. 
			 * We must do this here instead of before invoking handlePageLoaded() on the test subject because 
			 * the DOM is prepared with the view by that same handlePageLoaded() function. And we need that 
			 * view in place before "staging the scene".
			 */
			this.stageScene = function(callback) {
				callback();
			};

	    	// Bootstrap some model definitions and model data.
			this.userDataMock = {
				Id: '0054E000001Ija0'
			};

			this.accountDataMock = {
				Talent_Preference__c: ''
			};

			this.contactDataMock = {
				
			};

			this.accountDefinitionMock = {

			};

			this.contactDefinitionMock = {

			};

			// Spy on how the browser URL path is updated, with what values.
			this.browserUtilsMock = {
				populateBrowserPath: sinon.spy()
			};

			// Silo Skuid-dependent functionality.
			this.envUtilsMock = {
				calcSitePrefix: function() {
					return '/aerotek';
				},
				calcOpCo: function() {
					return 'Aerotek';
				}
			};

			var beforeScope = this;

			this.eventUtilsMock = {
				subscribeToEvent: function(eventId, callback) {
					/*
					 * After (in reality, as soon as) the event is subscribed to, trigger a form submit 
					 *	event.
					 */
					if (eventId === 'com.linkedInImport.onFormSubmit') {
						beforeScope.stageScene(callback);
					}
				},

				publishEvent: sinon.spy()
			};

			// Silo model-related operations from Skuid and return expected data.
			this.modelUtilsMock = {
				retrieveModelData: function(modelIdOrModel) {
					var result = null;
					if (modelIdOrModel === 'RunningUser') {
						result = beforeScope.userDataMock;
					} else if (modelIdOrModel === 'AccountLocal') {
						result = beforeScope.accountDataMock;
					} else if (modelIdOrModel === beforeScope.contactDefinitionMock) {
						result = beforeScope.contactDataMock;
					} else if (modelIdOrModel === beforeScope.accountDefinitionMock) {
						result = beforeScope.accountDataMock;
					}
					return result;
				}, 

				retrieveModelDefinition: function(modelIdOrModel) {
					var result = null;
					if (modelIdOrModel === 'ContactForUpdateLocal') {
						result = beforeScope.contactDefinitionMock;
					} else if (modelIdOrModel === 'AccountLocal') {
						result = beforeScope.accountDefinitionMock;
					}
					return result;
				},

				fetchModelData: function(modelArray, callback) {
					callback();
				}, 

				// Spy on how the model object(s) are updated, which what values.
				updateModelValues: sinon.spy(),

				// Spy on the model object(s) being saved.
				saveModelData: sinon.spy(function(modelData, options, modelOrModelArray) {
					return {
				        then: function(callback) {
				            if (callback) {
				                callback();
				            }
				        }
				    };
				})
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Silo Skuid-dependent functionality.
			this.uiUtilsMock = {
				showLoadingMask: function() {
					return;
				}, 

				hideLoadingMask: function() {
					return;
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/page-specific/linkedin-import-choices');
	        this.linkedInImportChoicesForTest = injector({
	        	'../common/browser-utils': this.browserUtilsMock, 
	        	'../common/env-utils': this.envUtilsMock, 
	            '../common/event-utils': this.eventUtilsMock, 
	            '../common/model-utils': this.modelUtilsMock, 
	            '../common/template-utils': this.templateUtilsMock, 
	            '../common/ui-utils': this.uiUtilsMock
	        });
	    });

	    it('nothing checked', function (done) {
	    	this.stageScene = function(callback) {
				// Un-check all of the checkboxes on the page.
				$('input:checkbox').prop('checked', false);

				callback();
			};

	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert(this.browserUtilsMock.populateBrowserPath.calledWith('/aerotek/0054E000001Ija0'), 
	        	'The user should be sent to the private user profile page when submitting the form, even ' + 
	        	'if nothing is checked.');
			done();
	    });

	    it('no LinkedIn data', function (done) {
	    	// Construct a Talent_Preference__c without the expected linkedin property.
	    	var prefsJson = JSON.stringify(require('json-loader!../../fixtures/account-talent-prefs-no-linkedin.json'));
	    	this.accountDataMock.Talent_Preference__c = prefsJson;

	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert(this.eventUtilsMock.publishEvent.calledWith('com.header.displayCatchAllGlobalError'), 
	        	'The display of a global error should be triggered.');
			done();
	    });

	    it('all checked', function (done) {
	    	// Construct a Talent_Preference__c with the expected linkedin property.
	    	var prefsJson = JSON.stringify(require('json-loader!../../fixtures/account-talent-prefs-with-linkedin.json'));
	    	this.accountDataMock.Talent_Preference__c = prefsJson;

	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert(this.modelUtilsMock.updateModelValues.calledTwice, 
	        	'updateModelValues should be called once for account and once for contact, since all of the ' + 
	        	'checkboxes were checked/selected.');

	        assert(this.modelUtilsMock.updateModelValues.calledWith(sinon.match.same(this.contactDataMock), {
		            Preferred_Name__c: 'Bob',
		            Title: 'UI Engineer at Allegis Group', 
		            LinkedIn_URL__c: 'https://www.linkedin.com/in/bobburdekin', 
		            About_Me__c: 'Specialties: Web Development, App Development, Project Management, Customer Service, Sales, Marketing, Promotions, Small Business Consulting, SEO and Social Media Marketing'
		        }, sinon.match.same(this.contactDefinitionMock)), 
	        	'The properties in Contact should be updated. The properties should be populated with the values ' +
	        	'sourced from the "linkedin" property found in Talent_Preference__c.'
	   		);

	        assert(this.modelUtilsMock.updateModelValues.calledWith(sinon.match.same(this.accountDataMock), 
	        	sinon.match.any, sinon.match.same(this.accountDefinitionMock)), 
	        	'The properties in Account should be updated.'
	   		);

	   		var updateModelValuesCall = this.modelUtilsMock.updateModelValues.getCall(1);
	   		var changesObj = updateModelValuesCall.args[1];
	   		assert.match(JSON.stringify(changesObj), /\\"industries\\":{\\"data\\":\\"aeronautics,Information Technology and Services\\"}/, 
	   			'The "industries" property value should be appended with the "industry" value sourced from the ' + 
	   			'"linkedin" property found in Talent_Preference__c.');

	        assert(this.modelUtilsMock.saveModelData.calledTwice, 
	        	'Both the contact and account models should be saved (persisted).');
	        assert(this.modelUtilsMock.saveModelData.calledWith(
	        	sinon.match.any, sinon.match.any, sinon.match.same(this.contactDefinitionMock)), 
	        	'The contact model should be saved (persisted).'
	   		);
	        assert(this.modelUtilsMock.saveModelData.calledWith(
	        	sinon.match.any, sinon.match.any, sinon.match.same(this.accountDefinitionMock)), 
	        	'The account model should be saved (persisted).'
	   		);

	        /*
	         * Deep inside the logic being tested, a Promise.all() is being invoked, with it's then() 
	         *	triggering the completion of the logic (a browser redirect). I can't figure out a good way 
	         *	to definitively trigger on this call, so use a crappy timeout, instead. Blah.
	         */
	        var itScope = this;
	        setTimeout(function() {
		        assert(itScope.browserUtilsMock.populateBrowserPath.calledWith('/aerotek/0054E000001Ija0'), 
		        	'The user should be sent to the private user profile page when submitting the form.');

				done();
	        }, 10);
	    });

	    it('nothing for account checked', function (done) {
	    	this.stageScene = function(callback) {
				// Un-check the "Industry" checkbox on the page.
				$('.qa-industry-input').prop('checked', false);

				callback();
			};

	    	// Construct a Talent_Preference__c with the expected linkedin property.
	    	var prefsJson = JSON.stringify(require('json-loader!../../fixtures/account-talent-prefs-with-linkedin.json'));
	    	this.accountDataMock.Talent_Preference__c = prefsJson;

	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert(this.modelUtilsMock.updateModelValues.calledOnce, 
	        	'updateModelValues should be called once for contact, but not at all for account, since the ' + 
	        	'"Industry" checkbox is unchecked.');

	        assert(this.modelUtilsMock.updateModelValues.calledWith(sinon.match.same(this.contactDataMock), {
		            Preferred_Name__c: 'Bob',
		            Title: 'UI Engineer at Allegis Group', 
		            LinkedIn_URL__c: 'https://www.linkedin.com/in/bobburdekin', 
		            About_Me__c: 'Specialties: Web Development, App Development, Project Management, Customer Service, Sales, Marketing, Promotions, Small Business Consulting, SEO and Social Media Marketing'
		        }, sinon.match.same(this.contactDefinitionMock)), 
		        'The properties in Contact should be updated. The properties should be populated with the values ' + 
		        'sourced from the linkedin property found in Talent_Preference__c.'
	   		);

	        assert(this.modelUtilsMock.saveModelData.calledOnce, 
	        	'Only one (the contact) model should be saved (persisted).');
	        assert(this.modelUtilsMock.saveModelData.calledWith(
	        	sinon.match.any, sinon.match.any, sinon.match.same(this.contactDefinitionMock)), 
	        	'Only the contact model should be saved (persisted).'
	   		);

	        /*
	         * Deep inside the logic being tested, a Promise.all() is being invoked, with it's then() 
	         *	triggering the completion of the logic (a browser redirect). I can't figure out a good way 
	         *	to definitively trigger on this call, so use a crappy timeout, instead. Blah.
	         */
	        var itScope = this;
	        setTimeout(function() {
		        assert(itScope.browserUtilsMock.populateBrowserPath.calledWith('/aerotek/0054E000001Ija0'), 
		        	'The user should be sent to the private user profile page when submitting the form.');

				done();
	        }, 10);
	    });

	    it('nothing for contact checked', function (done) {
	    	this.stageScene = function(callback) {
				// Un-check all of the checkboxes on the page.
				$('input:checkbox').prop('checked', false);

				// Check the "Industry" checkbox on the page.
				$('.qa-industry-input').prop('checked', true);

				callback();
			};

	    	// Construct a Talent_Preference__c with the expected "linkedin" property; and "industries" empty.
	    	var prefsJson = JSON.stringify(require('json-loader!../../fixtures/account-talent-prefs-with-linkedin-industries-empty.json'));
	    	this.accountDataMock.Talent_Preference__c = prefsJson;

	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert(this.modelUtilsMock.updateModelValues.calledOnce, 
	        	'updateModelValues should be called once for account, but not at all for contact, since only the ' + 
	        	'"Industry" checkbox is checked.');

	        assert(this.modelUtilsMock.updateModelValues.calledWith(sinon.match.same(this.accountDataMock), 
	        	sinon.match.any, sinon.match.same(this.accountDefinitionMock)), 
	        	'The properties in Account should be updated.'
	   		);
	   		var updateModelValuesCall = this.modelUtilsMock.updateModelValues.getCall(0);
	   		var changesObj = updateModelValuesCall.args[1];
	   		assert.match(JSON.stringify(changesObj), /\\"industries\\":{\\"data\\":\\"Information Technology and Services\\"}/, 
	   			'The "industries" property value should be appended (populated, in this case) with the "industry" ' + 
	   			'value sourced from the "linkedin" property found in Talent_Preference__c.');

	        assert(this.modelUtilsMock.saveModelData.calledOnce, 
	        	'Only one (the account) model should be saved (persisted).');
	        assert(this.modelUtilsMock.saveModelData.calledWith(
	        	sinon.match.any, sinon.match.any, sinon.match.same(this.accountDefinitionMock)), 
	        	'Only the account model should be saved (persisted).'
	   		);

	        /*
	         * Deep inside the logic being tested, a Promise.all() is being invoked, with it's then() 
	         *	triggering the completion of the logic (a browser redirect). I can't figure out a good way 
	         *	to definitively trigger on this call, so use a crappy timeout, instead. Blah.
	         */
	        var itScope = this;
	        setTimeout(function() {
		        assert(itScope.browserUtilsMock.populateBrowserPath.calledWith('/aerotek/0054E000001Ija0'), 
		        	'The user should be sent to the private user profile page when submitting the form.');

				done();
	        }, 10);
	    });
	});

	describe('onClickSelectAll', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<div class="js-content-box">' + 
				'</div>'
			);

			this.eventUtilsMock = {
				subscribeToEvent: function(eventId, callback) {
					/*
					 * After (in reality, as soon as) the event is subscribed to, trigger a "select all" 
					 *	event.
					 */
					if (eventId === 'com.linkedInImport.onClickSelectAll') {
				        // The "select all" checkbox should default to checked. Uncheck it.
				        $('.js-select-all-input').prop('checked', false);

						callback();
					}
				}
			};

			// Silo Skuid-dependent functionality.
			this.envUtilsMock = {
				calcSitePrefix: function() {
					return '/aerotek';
				},
				calcOpCo: function() {
					return 'Aerotek';
				}
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/page-specific/linkedin-import-choices');
	        this.linkedInImportChoicesForTest = injector({
	            '../common/event-utils': this.eventUtilsMock, 
	            '../common/env-utils': this.envUtilsMock, 
	            '../common/template-utils': this.templateUtilsMock
	        });
	    });

	    it('all unchecked', function (done) {
	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert.isFalse(_.every($('input:checkbox'), function(ele, idx, coll) {
	        	return $(ele).prop('checked');
	        }), 'By unchecking the "select all" checkbox, all of the checkboxes should uncheck.');
			done();
	    });
	});

	describe('onClickChoiceCheckbox', function () {

	    beforeEach(function() {
	    	// Bootstrap the DOM as necessary for the view to be created.
			document.write(
				'<div class="js-content-box">' + 
				'</div>'
			);

			var beforeScope = this;

			/*
			 * Stage the scene (prepare the DOM to reflect a specific scenario) before triggering the event. 
			 * We must do this here instead of before invoking handlePageLoaded() on the test subject because 
			 * the DOM is prepared with the view by that same handlePageLoaded() function. And we need that 
			 * view in place before "staging the scene".
			 */
			this.stageScene = function(callback) {
				callback();
			};

			this.eventUtilsMock = {
				subscribeToEvent: function(eventId, callback) {
					/*
					 * After (in reality, as soon as) the event is subscribed to, trigger a "select all" 
					 *	event.
					 */
					if (eventId === 'com.linkedInImport.onClickChoiceCheckbox') {
						beforeScope.stageScene(callback);
					}
				}
			};

			// Silo Skuid-dependent functionality.
			this.envUtilsMock = {
				calcSitePrefix: function() {
					return '/aerotek';
				},
				calcOpCo: function() {
					return 'Aerotek';
				}
			};

			// Silo template context operations from Skuid.
			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/page-specific/linkedin-import-choices');
	        this.linkedInImportChoicesForTest = injector({
	            '../common/event-utils': this.eventUtilsMock, 
	        	'../common/env-utils': this.envUtilsMock, 
	            '../common/template-utils': this.templateUtilsMock
	        });
	    });

	    it('uncheck one choice', function (done) {
	    	this.stageScene = function(callback) {
		        // All choices should default to checked. Uncheck one.
		        $('.qa-title-input').prop('checked', false);

				callback();
			};

	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert.isFalse($('.qa-submit-button').prop('disabled'), 
	        	'The submit button should be enabled when at least one choice is checked.');
			done();
	    });

	    it('uncheck all choices', function (done) {
	    	this.stageScene = function(callback) {
		        // All choices should default to checked. Uncheck all of them.
		        $('.qa-preferred-name-input').prop('checked', false);
		        $('.qa-title-input').prop('checked', false);
		        $('.qa-industry-input').prop('checked', false);
		        $('.qa-linkedin-url-input').prop('checked', false);
		        $('.qa-summary-input').prop('checked', false);

				callback();
			};

	        this.linkedInImportChoicesForTest.handlePageLoaded();

	        assert.isTrue($('.qa-submit-button').prop('disabled'), 
	        	'The submit button should be disabled when all of the choices are checked.');
			done();
	    });
	});

});


