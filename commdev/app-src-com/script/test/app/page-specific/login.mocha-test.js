'use strict';

require('expose?$!expose?jQuery!jquery');
var _ = require('lodash/core');
var moment = require('moment');
var sinon = require('imports?define=>false,require=>false!sinon/pkg/sinon.js');

var login = require('../../../app/page-specific/login');

describe('login', function () {

	describe('handlePageLoaded', function () {

	    beforeEach(function() {
	    	// Bootstrap something into the DOM which we will tell the function to manipulate.
			document.write(
				'<div id="root-ele">' + 
				'	<input type="checkbox" id="remember" />' + 
				'	<input type="text" class="username" />' + 
				'	<input type="password" class="password" />' + 
				'</div>'
			);

			this.localStorageMock = {
				checkBoxValidation: true, 
				userName: 'sample-username', 
				password: 'sample-password'
			};

			var beforeScope = this;

			this.browserUtilsMock = {
				retrieveLocalStorage: function() {
					return beforeScope.localStorageMock;
				}
			};

			this.loginUtilsMock = {
				initAccordions: sinon.spy(),
				popupForInactive: sinon.spy()
			};

			this.globalNotificationsMock = {
				initGlobalNotifications: sinon.spy()
			};

			this.templateUtilsMock = {
				buildBaseTemplateContext: function() {
					return {};
				}
			};

			// Inject mocks in place of legitimate dependencies to facilitate unit testing.
	        var injector = require('inject-loader!../../../app/page-specific/login');
	        this.loginForTest = injector({
	        	'../common/browser-utils': this.browserUtilsMock, 
	            '../common/login-utils': this.loginUtilsMock, 
	            '../common/global-notifications': this.globalNotificationsMock,
	            '../common/template-utils': this.templateUtilsMock
	        });
	    });

	    it('remember me checked previously yes', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var classScope = this;

	        this.loginForTest.handlePageLoaded();

	        assert.isTrue($rootEle.find('#remember').prop('checked'), 
	        	'When the page is loaded with localStorage indicating that credentials should be remembered, the remember-me checkbox should be checked.');
	        assert.equal($rootEle.find('.username').val(), classScope.localStorageMock.userName, 
	        	'When the page is loaded with localStorage indicating that credentials should be remembered, the username input should be pre-populated.');
	        assert.equal($rootEle.find('.password').val(), classScope.localStorageMock.password, 
	        	'When the page is loaded with localStorage indicating that credentials should be remembered, the password input should be pre-populated.');

	        assert(classScope.loginUtilsMock.initAccordions.calledOnce, 
	        	'The marketing accordions should be initialized on the login page.');
	        assert(classScope.globalNotificationsMock.initGlobalNotifications.calledOnce, 
	        	'The global notifications should be initialized on the login page.');
	        assert(classScope.loginUtilsMock.popupForInactive.calledOnce, 
            'The disabled account popup logic should be engaged.');

			done();
	    });

	    it('remember me checked previously no', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var classScope = this;
	    	// Initialize localStorage with remembering disabled.
	    	classScope.localStorageMock.checkBoxValidation = '';

	        this.loginForTest.handlePageLoaded();

	        assert.isFalse($rootEle.find('#remember').prop('checked'), 
	        	'When the page is loaded with localStorage indicating that credentials should not be remembered, the remember-me checkbox should not be checked.');
	        assert.equal($rootEle.find('.username').val(), '', 
	        	'When the page is loaded with localStorage indicating that credentials should not be remembered, the username input should not be pre-populated.');
	        assert.equal($rootEle.find('.password').val(), '', 
	        	'When the page is loaded with localStorage indicating that credentials should not be remembered, the password input should not be pre-populated.');

	        assert(classScope.loginUtilsMock.initAccordions.calledOnce, 
	        	'The marketing accordions should be initialized on the login page.');
	        assert(classScope.globalNotificationsMock.initGlobalNotifications.calledOnce, 
	        	'The global notifications should be initialized on the login page.');
	        assert(classScope.loginUtilsMock.popupForInactive.calledOnce, 
            'The disabled account popup logic should be engaged.');

			done();
	    });

	    it('remember me checked to unchaecked', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var classScope = this;

	        this.loginForTest.handlePageLoaded();
	        // Uncheck the remember-me box.
	        $rootEle.find('#remember').prop('checked', false).trigger('change');

	        assert.equal(classScope.localStorageMock.checkBoxValidation, '', 
	        	'When the remember-me checkbox is changed to unchecked, the remember-me property in localStorage should be blanked.');
	        assert.equal(classScope.localStorageMock.userName, '', 
	        	'When the remember-me checkbox is changed to unchecked, the username property in localStorage should be blanked.');
	        assert.equal(classScope.localStorageMock.password, '', 
	        	'When the remember-me checkbox is changed to unchecked, the password property in localStorage should be blanked.');

			done();
	    });


	    it('remember me unchecked to checked', function (done) {
	    	var $rootEle = $('#root-ele');
	    	var classScope = this;
	    	// Initialize localStorage with nothing remembered.
	    	classScope.localStorageMock.checkBoxValidation = '';
	    	classScope.localStorageMock.userName = '';
	    	classScope.localStorageMock.password = '';

	        this.loginForTest.handlePageLoaded();
	        var usernameEntered = 'diff-username';
	        var passwordEntered = 'diff-password';
	        // Fill in the username and password fields and check the remember-me box.
	        $rootEle.find('.username').val(usernameEntered);
	        $rootEle.find('.password').val(passwordEntered);
	        $rootEle.find('#remember').prop('checked', true).trigger('change');

	        assert.equal(classScope.localStorageMock.checkBoxValidation, 'on', 
	        	'When the remember-me checkbox is changed to checked, the remember-me property in localStorage should be populated.');
	        assert.equal(classScope.localStorageMock.userName, usernameEntered, 
	        	'When the remember-me checkbox is changed to checked, the username property in localStorage should be populated.');
	        assert.equal(classScope.localStorageMock.password, passwordEntered, 
	        	'When the remember-me checkbox is changed to checked, the password property in localStorage should be populated.');

			done();
	    });

	});

});


