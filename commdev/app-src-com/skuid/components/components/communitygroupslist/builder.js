(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "tc_components__communitygroupslist",
		name: "Community Groups List",
		icon: "sk-icon-contact",
		description: "This component shows a list of active community groups",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>Community Groups List</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>Community Groups List</div>"
			);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<tc_components__communitygroupslist/>");
		}
	}));
})(skuid);
