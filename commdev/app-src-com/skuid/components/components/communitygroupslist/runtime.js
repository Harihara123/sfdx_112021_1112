var $ = skuid.$;

(function(skuid){

// ===================
// List all community (Chatter) groups
// ===================
skuid.componentType.register("tc_components__communitygroupslist", function(element, xmlDef){

    var model = skuid.$M('CommunityGroupsList'),
        groups = model.getRows(),
        tmpl = '',
        output = '';

    // output
    if(groups) {
        if (groups.length) {
            element.html(renderGroupsList(groups,model));
        } else {
            element.html(renderNoGroupsMessage());
        }
    } else {
        element.html(renderNoGroupsMessage());
    }

});

})(skuid);

function renderNoGroupsMessage() {
    var message = '<p>{{ $Label.TC_There_are_no_Community_Groups }}</p>';
    return message;
}

function renderGroupsList(groups,model) {

    String.prototype.trimToLength = function(m) {
      return (this.length > m)
        ? $.trim(this).substring(0, m).split(" ").slice(0, -1).join(" ") + "..."
        : this;
    };

    var html = '<div class="c-community-groups">';

    $.each(groups, function(i, group) {
        html += '<div class="c-group">';
        html += '<a href="' + skuid.utils.mergeAsText('global','{{$Site.Prefix}}') + '/_ui/core/chatter/groups/GroupProfilePage?g=' + group.Id + '">';
        html += '<div class="u-avatar u-avatar--md"><img src="' + group.SmallPhotoUrl + '" /></div>';
        html += '<div class="c-group--body">';
        html += '<h2 class="u-header u-header--md">' + group.Name + '</h2>';

        // sanitize group description
        var group_description = '';
        if(typeof group.Description !== 'undefined'){
            group_description = group.Description.trimToLength(100);
        }

        html += '<p class="c-group--description">' + group_description + '</p>';
        // html += "<a class='u-btn u-btn--primary' href='#' onclick='addSubscribeRow(\"CommunityGroupMember\", \"" + group.Id + "\"); return false'>Join Group</a>";
        html += '</div>'; // close body
        html += '</a>';
        html += '</div>'; // close c-group
    });

    html += '</div>';

    return html;
}

function addSubscribeRow(model, followId){

    var dfremmodel = skuid.$M(model);
    var runningUser = skuid.$M('RunningUser');
    var runningUserRow = runningUser.getFirstRow();

    var newRow = dfremmodel.createRow({
        additionalConditions: [
            { field: 'MemberId', value: runningUserRow.Id},
            { field: 'CollaborationGroupId', value: followId}
        ], doAppend: true
    });

    dfremmodel.save({callback: function(result){
        if(result.totalsuccess){
            skuid.component.getByType('tc_components__communitygroupslist')[0].render();
        }
    }});
}