(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "tc_components__customheader",
		name: "Card Header",
		icon: "sk-icon-contact",
		description: "This is a card header",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>" + component.state.attr("header_title") + " (Basic Header)</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>" + component.state.attr("header_title") + " (Basic Header)</div>"
			);
		},
		propertiesRenderer: function (propertiesObj,component) {
			propertiesObj.setTitle("Card Header Properties");
			var state = component.state;
			var propCategories = [];
			var propsList = [
				{
					id: "header_title",
					type: "string",
					label: "Header text",
					helptext: "This text will display in the header",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "header_background",
					type: "picklist",
					label: "Background",
					picklistEntries: [
						{
							value: '',
							label: 'No Style'
						},
						{
							value: 'c-header__primary',
							label: 'Primary'
						},
						{
							value: 'c-header__secondary',
							label: 'Secondary'
						},
						{
							value: 'c-header__tertiary',
							label: 'Tertiary'
						},
						{
							value: 'c-header__quaternary',
							label: 'Quaternary'
						}

					],
					helptext: "Optional parameter that gives the header a predefined background color.",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "header_css_id",
					type: "string",
					label: "CSS ID",
					helptext: "Optional parameter that gives this component's DOM element a CSS ID.",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "header_css_class",
					type: "string",
					label: "CSS Class",
					helptext: "Optional parameter that gives this component's DOM element a CSS class.",
					onChange: function(){
						component.refresh();
					}
				}
			];
			propCategories.push({
				name: "",
				props: propsList,
			});
			if(skuid.mobile) propCategories.push({ name : "Remove", props : [{ type : "remove" }] });
			propertiesObj.applyPropsWithCategories(propCategories,state);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<tc_components__customheader header_title='Title Here' header_background='c-header__primary' header_edit_button='false' />");
		}
	}));
})(skuid);
