(function(skuid){
	skuid.componentType.register("tc_components__customheader", function(element,xmlDef){

        var cssId = '';
        if(typeof(xmlDef.attr("header_css_id")) != 'undefined') {
            cssId = xmlDef.attr("header_css_id");
        }

        var cssClass = ' ';
        if(typeof(xmlDef.attr("header_css_class")) != 'undefined') {
            var cssClass = cssClass + xmlDef.attr("header_css_class");
        }

		var template = '<header id="' + cssId + '" class="c-header c-basic-header ' + xmlDef.attr("header_background") + cssClass + '">' +
			'<span class="c-header__title">' + xmlDef.attr("header_title") + '</span>';

			if(xmlDef.attr("header_edit_button") !== 'false'){
				template += '<button style="float: right" class="js-card-edit"><span class="u-svg-icon slds-icon__container"><svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#edit"></use></svg></span></button>';
            }

		template += '</header>';

		element.html(skuid.utils.merge( 'global', template, null ));

	});
})(skuid);
