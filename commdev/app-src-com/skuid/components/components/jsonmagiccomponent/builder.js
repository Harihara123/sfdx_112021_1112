(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "tc_components__jsonmagiccomponent",
		name: "Json Renderer",
		icon: "sk-icon-contact",
		description: "This component renders UI based on Json Data",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>Welcome to the show!!!</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div class='hello-content'>Hello " + component.state.attr("person") + "!</div>"
			);
		},
		propertiesRenderer: function (propertiesObj,component) {
			propertiesObj.setTitle("Say Hello Component Properties");
			var state = component.state;
			var propCategories = [];
			var propsList = [
				{
					id: "schema",
					type: "template",
					label: "Schema (Move to static resource)",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "uidef",
					type: "template",
					label: "UI Definition",
					defaultValue: "",
					helptext: "HTML strcture for JSON",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "modelId",
					type: "model",
					label: "Model",
					required: true,
					location: "attribute",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "fieldProp",
					type: "field",
					label: "JSON Field",
					modelprop : "modelId",
					location: "attribute",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "mode",
					type: "picklist",
					label: "Default Mode",
					picklistEntries: [{
						value: "view",
						label: "View"
					},
					{
						value: "edit",
						label: "Edit"
					}],
					defaultValue: 'view',
					location: "attribute",
					onChange: function(){
						component.refresh();
					}
				}

			];
			propCategories.push({
				name: "",
				props: propsList,
			});
			if(skuid.mobile) propCategories.push({ name : "Remove", props : [{ type : "remove" }] });
			propertiesObj.applyPropsWithCategories(propCategories,state);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<tc_components__jsonmagiccomponent/>");
		}
	}));
})(skuid);
