var $ = skuid.$;
(function(skuid){
	skuid.componentType.register("tc_components__jsonmagiccomponent",function(element,xmlDef){
		var schema = xmlDef.find('schema').text();
		var uidef = xmlDef.find('uidef').text();
		var modelName = xmlDef.attr('modelId');
		var jsonFieldName = xmlDef.attr('fieldProp');
		var mode = xmlDef.attr('mode');
		element.html(renderUI(schema, uidef, modelName, jsonFieldName, element.attr('id'), mode));
	});
})(skuid);

function renderUI(schema, uidef, modelName, jsonFieldName, componentId, mode) {
	var ui = '<div id="' + componentId + '_dataForm">';

	if(mode !== 'view') {
		ui += "<div class='u-action-controller'>";
		ui += "<input type=\"button\" class=\"sk-button-cancel u-margin-right-5\" value=\"" + skuid.$L('TC_Cancel') + "\" onclick=\"return toggleView('view', '" + componentId + "');\"/>";
		ui += "<input type=\"button\" class=\"u-btn u-btn--ghost-primary\" value=\"" + skuid.$L('TC_Save') + "\" onclick=\"return updateJson('"+ componentId +"');\"/>";
		ui += "</div>";
	}

	var schemaReader = $.parseJSON(schema);

	var jsonData = skuid.model.getModel(modelName).data[0][jsonFieldName];
	if(typeof jsonData === 'undefined') {
		//populated template row
		jsonData = schemaReader.defaultValue;
	}

	var json;
	if(typeof jsonData == 'object') {
		json = jsonData;
	} else {
		json = JSON.parse(jsonData);
	}

	ui += storeComponentProperties(schema, json, uidef, modelName, jsonFieldName, componentId);

	$.each(json, function(index, element) {
		var token = '{' + index + '}';

		if(uidef.indexOf(token) >= 0) {
		    var html ='';
		    var elementRef = [componentId, index, schemaReader.properties[index].label, schemaReader.properties[index].options];
		    //, schemaReader.properties[index].labelv, schemaReader.properties[index].optionsv
		    var fieldType =  schemaReader.properties[index]['$ref'].split('/');
		    html = renderElements(json[index], schemaReader[fieldType[1]][fieldType[2]].properties, mode, fieldType[2], elementRef);
		    uidef = uidef.replace(token, html);
		}
	});
	ui += uidef;

	if (mode == 'view') {
		ui += "<input type=\"button\" class=\"u-btn\" value=\"" + skuid.$L('TC_Edit') + "\" onclick=\"return toggleView('edit', '" + componentId + "');\"/>";
	} else {
		ui += "<input type=\"button\" class=\"sk-button-cancel u-margin-right-5\" value=\"" + skuid.$L('TC_Cancel') + "\" onclick=\"return toggleView('view', '" + componentId + "');\"/>";
		ui += "<input type=\"button\" class=\"u-btn--ghost-primary\" value=\"" + skuid.$L('TC_Save') + "\" onclick=\"return updateJson('"+ componentId +"');\"/>";
	}

	// init tagsManager
	setTimeout(function(){

		// loop each input field and prefill values
		$.each(json, function(i) {

			$('.tm-input-'+i).tagsManager({
				prefilled: $('input[dataref="'+i+'"]').val(),
				delimiters: [13, 19]
			});

			$('.add-tag-'+i).on('click', function (e) {
				e.preventDefault();
				var newTag = $('.tm-input-'+i).val().replace(/[ ]*,[ ]*|[ ]+/g, ' ');
				$('.tm-input-'+i).tagsManager('pushTag',newTag);
			});
		});

		// show input fields
		$('.c-profile-preferences__preference > div.value').fadeIn();

    }, 1000);

	return ui;
}

function renderElements (json, def, mode, datatype, elementRef) {
	var elementUI = {
		"stringfieldtype" : function(json, def, mode, elementRef) {
			return renderStringField(json, def, mode, elementRef);

		},
		"picklistfieldtype" : function(json, def, mode, elementRef) {
			return renderPicklistField(json, def, mode, elementRef);
		},
		"multiselectfieldtype" : function(json, def, mode, elementRef) {
			return renderMultiSelectPicklistField(json, def, mode, elementRef);
		},
		"currencyfieldtype" : function(json, def, mode, elementRef) {
			return renderCurrencyField(json, def, mode, elementRef);
		},
		"textareafieldtype" : function(json, def, mode, elementRef) {
			return renderTextareaField(json, def, mode, elementRef);
		}
	};
	return elementUI[datatype](json, def, mode, elementRef);
}

function renderStringField(json, def, mode, elementRef) {

	if (mode == 'edit') {
		return '<div class="u-header u-header--md">' + skuid.$L(elementRef[2])+ '</div><br/><div class="value" style="display:none;"><input class="tm-input tm-input-' + elementRef[1] + '" type="text" formRef="' + elementRef[0] + '" dataRef="' + elementRef[1] + '" value="' + json.data + '" /><button class="btn add-tag-' + elementRef[1] + '">'+ skuid.$L('TC_Add') +'</button></div>';
	} else {
		// placeholder text if no data
		if(!json.data) {
			/*
			 * Have Skuid convert (what we assume to be) the label reference and use that in the default
			 * value. For example, "TC_Certifications".
			 */
			json.data = 'No ' + skuid.$L(elementRef[2]).toLowerCase() + ' provided.';
		}
		return '<div class="u-header u-header--md">' + skuid.$L(elementRef[2]) + '</div><div class="value">' + json.data.split(",").join(", ") + '</div>';
	}
}

function renderCurrencyField(json, def, mode, elementRef) {
	if (mode == 'edit') {
		return '<div class="u-header u-header--md">' + skuid.$L(elementRef[2]) + '</div><br/><div class="value"><input type="number"  formRef="' + elementRef[0] + '" dataRef="' + elementRef[1] + '" value="' + json.data + '" /></div>';
	} else {
		return '<div class="u-header u-header--md">' + skuid.$L(elementRef[2]) + '</div><div class="value">' + json.data + '</div>';
	}
}

function renderTextareaField(json, def, mode, elementRef) {
	if (mode == 'edit') {
		return '<div class="u-header u-header--md">' + skuid.$L(elementRef[2]) + '</div><br/><div class="value"><textarea  formRef="' + elementRef[0] + '" dataRef="' + elementRef[1] + '">' + json.data + '</textarea></div>';
	} else {
		return '<div class="u-header u-header--md">' + skuid.$L(elementRef[2]) + '</div><div class="value">' + json.data + '</div>';
	}
}

function renderPicklistField(json, def, mode, elementRef) {
	return picklistUtil(json, def, mode, '', elementRef);
}

function renderMultiSelectPicklistField(json, def, mode, elementRef) {
	return picklistUtil(json, def, mode, 'multiple', elementRef);
}

function picklistUtil(json, def, mode, multiple, elementRef) {
	var html;
	var data = json.data.split(';');
	if (mode == 'edit') {
		html =  '<div class="u-header u-header--md">' + skuid.$L(elementRef[2]) + '</div><br/><div class="value"><select  formRef="' + elementRef[0] + '" dataRef="' + elementRef[1] + '" ' + multiple +'>';

		for(var i=0; i< elementRef[3].length; i++) {
			if(data.indexOf(elementRef[3][i].value) >= 0) {
				html+= '<option value="' + elementRef[3][i].key + '" selected>' + elementRef[3][i].value + '</option>';
			} else {
				html+= '<option value="' + elementRef[3][i].key + '">' + elementRef[3][i].value + '</option>';
			}

		}
		html += '</select></div>';
	} else {
		html =  '<div class="u-header u-header--md">' + skuid.$L(elementRef[2]) + '</div><div class="value">' + json.data + '</div>';
	}

	return html;
}

function storeComponentProperties(schema, jsonData, uidef, modelName, jsonFieldName, componentId) {
	var html =  $('<div/>')
		.append(createElement('div', [{name:"id",value:"" + componentId + "_schema"},{name:"style",value:"display:none"}], JSON.stringify(schema)))
		.append(createElement('div', [{name:"id",value:"" + componentId + "_jsonData"},{name:"style",value:"display:none"}], JSON.stringify(jsonData)))
		.append(createElement('div', [{name:"id",value:"" + componentId + "_uidef"},{name:"style",value:"display:none"}], uidef))
		.append(createElement('div', [{name:"id",value:"" + componentId + "_modelName"},{name:"style",value:"display:none"}], modelName))
		.append(createElement('div', [{name:"id",value:"" + componentId + "_jsonFieldName"},{name:"style",value:"display:none"}], jsonFieldName));
	return html.html();
}

function toggleView(mode, componentId) {
	var schema = $('#' + componentId + '_schema').html();
	var uidef = $('#' + componentId + '_uidef').html();
	var modelName = $('#' + componentId + '_modelName').html();
	var jsonFieldName = $('#' + componentId + '_jsonFieldName').html();
	$('#' + componentId).html(renderUI(JSON.parse(schema), uidef, modelName, jsonFieldName, componentId, mode));
}

function updateJson(id) {

	var updatedData = "Updated Json:<br/>";
	//var jsonData = JSON.parse($('#' + id + '_jsonData').html());
	var modelName = $('#' + id + '_modelName').html();
	var jsonFieldName = $('#' + id + '_jsonFieldName').html();

	var inputFields = $('input[formref="' + id + '"]');
	var selectFields = $('select[formref="' + id + '"]');
	var textareaFields = $('textarea[formref="' + id + '"]');

	var jsonData = skuid.model.getModel(modelName).data[0][jsonFieldName];

	var json;
	if(typeof jsonData == 'object') {
		json = jsonData;
	} else {
		json = JSON.parse($('#' + id + '_jsonData').html());
	}

	$.each(inputFields, function(index,element){

		// get value of hidden tag manager field
		var hiddenFieldValue = $(inputFields[index]).next('input').get(0).value;

		// write tags if they exists
		if(hiddenFieldValue) {
			json[$(element).attr('dataref')].data = hiddenFieldValue;
		} else {
			json[$(element).attr('dataref')].data = element.value;
		}

	});

	$.each(textareaFields, function(index,element){
		json[$(element).attr('dataref')].data = element.value;
	});

	$.each(selectFields, function(index,element){
		var data = '';
		$.each($(element).find(":selected"),function(i,option){
			data += $(option).text() + ';';
		});
		data = data.substring(0, data.length-1);
		json[$(element).attr('dataref')].data = data;
	});

	$('#' + id + '_jsonData').html(JSON.stringify(json));

	var model = skuid.model.getModel(modelName);
	$.each(model.data,function(i,row){
		model.updateRow(row,{
			Talent_Preference__c: JSON.stringify(json)
		});
	});

	$('body', window.parent.document).addClass('is-loading');
	model.save({callback: function(){

		var schema = $('#' + id + '_schema').html();
		var uidef = $('#' + id + '_uidef').html();
		$('#' + id).html(renderUI(JSON.parse(schema), uidef, modelName, jsonFieldName, id, 'view'));
		$('body', window.parent.document).removeClass('is-loading');
	}});

}

function createElement(type, attributes, html) {
	var newElement = $("<"+type+"/>");
	$.each(attributes, function(index, element){
		newElement.attr(element.name,element.value);
	});
	newElement.html(html);
	return newElement;
}
