(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "tc_components__keycontacts",
		name: "Key Contacts",
		icon: "sk-icon-contact",
		description: "This component shows key contacts for the running user",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>Key Contacts</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>Key Contacts</div>"
			);
		},
		propertiesRenderer: function (propertiesObj,component) {
			propertiesObj.setTitle("Key Contact Properties");
			var state = component.state;
			var propCategories = [];
			var propsList = [
				{
					id: "key_contacts_recruiter",
					type: "boolean",
					label: "Recruiter",
					helptext: "Show user's recruiter",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "key_contacts_account_manager",
					type: "boolean",
					label: "Account Manager",
					helptext: "Show user's account manager",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "key_contacts_community_manager",
					type: "boolean",
					label: "Community Manager",
					helptext: "Show user's community manager",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "key_contacts_CSA",
					type: "boolean",
					label: "Customer Service Associate",
					helptext: "Customer Service Associate",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "key_contacts_recruiter_message",
					type: "string",
					label: "No Recruiter Message",
					helptext: "If a recruiter is not available for talent, talent sees friendly message.",
					onChange: function(){
						component.refresh();
					}
				}
			];
			propCategories.push({
				name: "",
				props: propsList,
			});
			if(skuid.mobile) propCategories.push({ name : "Remove", props : [{ type : "remove" }] });
			propertiesObj.applyPropsWithCategories(propCategories,state);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<tc_components__keycontacts/>");
		}
	}));
})(skuid);
