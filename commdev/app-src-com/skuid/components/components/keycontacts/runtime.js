(function(skuid){

    // ===================
    // Key Contacts
    // ===================
    skuid.componentType.register("tc_components__keycontacts", function(element, xmlDef){

        // ===================
        // RECRUITER REQUESTED
        // ===================

        var userModel = skuid.$M('RunningUser');
        var userRow = userModel.getFirstRow();

        // set column divisions based on number of contacts shown
        var contactCount = 0;
        var colDiv = 4;

        if(xmlDef.attr("key_contacts_recruiter") === 'true'){
            contactCount = contactCount + 1;
        }
        if(xmlDef.attr("key_contacts_account_manager") === 'true'){
            contactCount = contactCount + 1;
        }
        if(xmlDef.attr("key_contacts_community_manager") === 'true'){
            contactCount = contactCount + 1;
        }
        if(userRow.Profile_Name__c === 'TekSystem GS W2 Community User' || userRow.Profile_Name__c === 'TekSystem GS SUB Community User'){
            contactCount = contactCount;
        } else {
          if(xmlDef.attr("key_contacts_CSA") === 'true'){
              contactCount = contactCount + 1;
          }
        }

        colDiv = (12 / contactCount);

        // ===================
        // RECRUITER + ACCOUNT MANAGER data and markup
        // ===================

        var recruiterTmpl = '';
        var accountManagerTmpl = '';

        if(userRow.Account) {

            // ===================
            // RECRUITER COLUMN
            // ===================

            // check if recruiter should be displayed
            if(xmlDef.attr("key_contacts_recruiter") === 'true'){

                // check if recruiter case has been requested (string)
                if (userRow.Contact.Recruiter_Change_Requested__c && userRow.Contact.Recruiter_Change_Requested__c != 'yes')  {

                    recruiterTmpl = renderRecruiterChangeRequested(colDiv);

                } else {

                    //check if recruiter exits
                    if (userRow.Account.Talent_Recruiter__r) {

                        // check if recruiter is active
                        if(userRow.Account.Talent_Recruiter__r.IsActive) {
                            recruiterTmpl =
                            '<div class="col-md-'+colDiv+' c-key-contacts__contact">' +
                                '<div class="u-avatar u-avatar--md">' +
                                    '<img src="{{{ Talent_Recruiter__r.FullPhotoUrl }}}">' +
                                '</div>' +
                                '<dl>' +
                                    '<dt class="u-header u-header--md qa-recruiter-name">{{ Talent_Recruiter__r.First_Last_Name__c }}</dt>' +
                                    '<dd class="u-meta u-meta--md qa-recruiter-title">{{ Talent_Recruiter__r.Title }}</dd>' +
                                    '{{#Talent_Recruiter__r.Phone}}<dd class="u-meta u-meta--sm qa-recruiter-phone">' +
                                       '<span class="u-svg-icon slds-icon__container">' +
                                          '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#call"></use>' +
                                          '</svg>' +
                                       '</span>' +
                                       '<a href="tel:{{ Talent_Recruiter__r.Phone }}">{{ Talent_Recruiter__r.Phone }}</a>' +
                                    '</dd>{{/Talent_Recruiter__r.Phone}}' +
                                    '{{#Talent_Recruiter__r.Email}}<dd class="u-meta u-meta--sm qa-recruiter-email">' +
                                        '<span class="u-svg-icon slds-icon__container">' +
                                            '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#email"></use>' +
                                            '</svg>' +
                                       '</span>' +
                                       '<a class="qa-recruiter-email-link" href="mailto:{{ Talent_Recruiter__r.Email }}">Email</a>' +
                                    '</dd>{{/Talent_Recruiter__r.Email}}' +
                                '</dl>' +
                            '</div>';
                        } else {
                            recruiterTmpl = renderNoRecruiter(colDiv,xmlDef.attr('key_contacts_recruiter_message'));
                        }

                    } else {
                        recruiterTmpl = renderNoRecruiter(colDiv,xmlDef.attr('key_contacts_recruiter_message'));
                    }

                }
            }


            // ===================
            // ACCOUNT MANAGER COLUMN
            // ===================

            // check if account manager should be displayed
            if(xmlDef.attr("key_contacts_account_manager") === 'true'){

                // check if account manager exits
                if (userRow.Account.Talent_Account_Manager__r) {

                    // check if account mananger is active
                    if(userRow.Account.Talent_Account_Manager__r.IsActive) {
                        accountManagerTmpl =
                        '<div class="col-md-'+colDiv+' c-key-contacts__contact">' +
                            '<div class="u-avatar u-avatar--md">' +
                                '<img src="{{{ Talent_Account_Manager__r.FullPhotoUrl }}}">' +
                            '</div>' +
                            '<dl>' +
                                '<dt class="u-header u-header--md qa-am-name">{{ Talent_Account_Manager__r.First_Last_Name__c }}</dt>' +
                                '<dd class="u-meta u-meta--md qa-am-title">{{ Talent_Account_Manager__r.Title }}</dd>' +
                                '{{#Talent_Account_Manager__r.Phone}}<dd class="u-meta u-meta--sm qa-am-phone"">' +
                                   '<span class="u-svg-icon slds-icon__container">' +
                                      '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#call"></use>' +
                                      '</svg>' +
                                   '</span>' +
                                   '<a href="tel:{{ Talent_Account_Manager__r.Phone }}">{{ Talent_Account_Manager__r.Phone }}</a>' +
                                '</dd>{{/Talent_Account_Manager__r.Phone}}' +
                                '{{#Talent_Account_Manager__r.Email}}<dd class="u-meta u-meta--sm qa-am-email"">' +
                                    '<span class="u-svg-icon slds-icon__container">' +
                                        '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#email"></use>' +
                                        '</svg>' +
                                   '</span>' +
                                   '<a class="qa-acct-mgr-email-link" href="mailto:{{ Talent_Account_Manager__r.Email }}">Email</a>' +
                                '</dd>{{/Talent_Account_Manager__r.Email}}' +
                            '</dl>' +
                        '</div>';
                    } else {
                        accountManagerTmpl = renderNoAccountManager(colDiv);
                    }

                } else {
                    accountManagerTmpl = renderNoAccountManager(colDiv);
                }
            }

        }

        // ===================
        // COMMUNITY MANAGER COLUMN
        // ===================
        var communityManager = '';
        var communityManagerTmpl = '';
        var communityManagerOffice = 'none'; //default office lookup
        var customerServiceRep = '';
        var customerServiceTmpl = '';

        // If Teksystems Community Owned then show CSA
        if(xmlDef.attr("key_contacts_CSA") === 'true'){
            if(userRow.Account.Talent_CSA__r) {
                if(userRow.Account.Talent_CSA__r.IsActive) {
                    customerServiceTmpl = renderCustomerServiceCard(colDiv);
                } else {
                    customerServiceRep = 'none';
                    customerServiceTmpl = renderNoCustomerServiceCard(colDiv);
                }
            } else {
                customerServiceRep = 'none';
                customerServiceTmpl = renderNoCustomerServiceCard(colDiv);
            }
        }

        if(xmlDef.attr("key_contacts_community_manager") === 'true'){
            if(userRow.Account.Talent_Community_Manager__c != null) {
                if(userRow.Account.Talent_Community_Manager__r.IsActive) {
                    communityManagerTmpl = renderCommunityManagerCard(colDiv);
                } else {
                    communityManager = 'none';
                    communityManagerTmpl = renderNoCommunityManager(colDiv);
                }
            } else {
                    communityManager = 'none';
                    communityManagerTmpl = renderNoCommunityManager(colDiv);
            }
        }

        // ===================
        // Build tile logic
        // ===================
        var col1 = '';
        var col2 = '';
        var col3 = '';

        col1 = skuid.utils.merge( 'row', recruiterTmpl, null, userModel, userRow.Account );
        col2 = skuid.utils.merge( 'row', accountManagerTmpl, null, userModel, userRow.Account );
        //col3 = skuid.utils.merge( 'row', communityManagerTmpl, null, userModel, userRow.Account );
        if(userRow.Profile_Name__c === 'TekSystem GS W2 Community User' || userRow.Profile_Name__c === 'TekSystem GS SUB Community User'){
            if (xmlDef.attr("key_contacts_CSA") === 'true') {

            }
        } else {
          if (xmlDef.attr("key_contacts_CSA") === 'true') {
              switch(customerServiceRep) {
                  case 'none':
                      col3 = skuid.utils.merge( 'row', customerServiceTmpl, null, userModel, userRow.Account );
                      break;
                  default:
                      col3 = skuid.utils.merge( 'row', customerServiceTmpl, null, userModel, userRow.Account );
              }
          }
        }

            if(xmlDef.attr("key_contacts_community_manager") === 'true'){
                switch(communityManager) {
                    case 'none':
                        col3 = skuid.utils.merge( 'row', communityManagerTmpl, null, userModel, userRow.Account );
                        break;
                    default:
                        col3 = skuid.utils.merge( 'row', communityManagerTmpl, null, userModel, userRow.Account );
                }
            }


        if(col1[0]) {
            col1 = col1[0].innerHTML;
        }
        if(col2[0]) {
            col2 = col2[0].innerHTML;
        }
        if(col3[0]) {
            col3 = col3[0].innerHTML;
        }

        var card = col1 + col2 + col3;

        // card output
        element.html(card);

    });

})(skuid);


function renderNoRecruiter(colDiv,message) {
    return '<div class="col-md-'+ colDiv +' c-key-contacts__contact c-key-contacts__request-recruiter"><p>'+ message + '</p><a href="#" onClick="requestRecruiter(); return false;" class="u-btn u-btn--tertiary">' + skuid.$L('TC_Request_Recruiter') + '</a></div>';
}

function requestRecruiter() {
    $('.c-key-contacts__request-recruiter a').text(skuid.$L('TC_Requesting') + ' ...')
    .css({
        'background-color' : '#ccc',
        'cursor' : 'default'
    });

    var contactModel = skuid.$M('ContactForUpdate')
    /*
     * This Contact model is _not_ configured to query for data on page load. As such, load _from_
     * the server before saving _to_ the server.
     */
    skuid.model.updateData([contactModel], function() {
        var contactRow = contactModel.getFirstRow();
        contactModel.updateRow(contactRow, {
            Recruiter_Change_Requested__c: 'no'
        });

        contactModel.save({callback: function(result){
            if (result.totalsuccess) {
                //console.log('success');
                $('.c-key-contacts__request-recruiter').empty().html('<p>' + skuid.$L('TC_Recruiter_change_requested') + '</p>');
            } else {
                //console.log(result.updateResults);
                $('.c-key-contacts__request-recruiter').empty().html('<p>' + skuid.$L('TC_Recruiter_change_request_failed') + '</p>');
            }
        }});
    });
}

function renderNoAccountManager(colDiv) {
    return '<div class="col-md-'+ colDiv +' c-key-contacts__contact"><p>' + skuid.$L('TC_No_Account_Manager_defined') + '</p></div>';
}

function renderRecruiterChangeRequested(colDiv) {
    return '<div class="col-md-'+ colDiv +' c-key-contacts__contact"><p>' + skuid.$L('TC_Recruiter_change_requested') + '</p></div>';
}

function renderNoCommunityManager(colDiv) {
    return '<div class="col-md-'+ colDiv +' c-key-contacts__contact"><p>' + skuid.$L('TC_No_Community_Manager_defined') + '</p></div>';
}

function renderNoCustomerServiceCard(colDiv) {
    return '<div class="col-md-'+ colDiv +' c-key-contacts__contact"><p>' + skuid.$L('TC_No_CSA_defined') + '</p></div>';
}

function renderCommunityManagerCard(colDiv) {
    var communityManagerTmpl = '<div class="col-md-'+colDiv+' c-key-contacts__contact">' +
        '<div class="u-avatar u-avatar--md">' +
            '<img src="{{{ Talent_Community_Manager__r.FullPhotoUrl }}}">' +
        '</div>' +
        '<dl>' +
            '<dt class="u-header u-header--md qa-cm-name">{{ Talent_Community_Manager__r.First_Last_Name__c }}</dt>' +
            '<dd class="u-meta u-meta--md qa-cm-title">{{ Talent_Community_Manager__r.Title }}</dd>' +
            '{{#Talent_Community_Manager__r.Phone}}<dd class="u-meta u-meta--sm qa-cm-phone">' +
               '<span class="u-svg-icon slds-icon__container">' +
                  '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                        '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#call"></use>' +
                  '</svg>' +
               '</span>' +
               '<a href="tel:{{ Talent_Community_Manager__r.Phone }}">{{ Talent_Community_Manager__r.Phone }}</a>' +
            '</dd>{{/Talent_Community_Manager__r.Phone}}' +
            '{{#Talent_Community_Manager__r.Email}}<dd class="u-meta u-meta--sm qa-cm-email">' +
                '<span class="u-svg-icon slds-icon__container">' +
                    '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                        '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#email"></use>' +
                    '</svg>' +
               '</span>' +
               '<a class="qa-comm-mgr-email-link" href="mailto:{{ Talent_Community_Manager__r.Email }}">'+skuid.$L('TC_Email')+'</a>' +
            '</dd>{{/Talent_Community_Manager__r.Email}}' +
        '</dl>' +
    '</div>';

    return communityManagerTmpl;
}

function renderCustomerServiceCard(colDiv) {
    var customerServiceTmpl = '<div class="col-md-'+colDiv+' c-key-contacts__contact">' +
        '<div class="u-avatar u-avatar--md">' +
            '<img src="{{{ Talent_CSA__r.FullPhotoUrl }}}">' +
        '</div>' +
        '<dl>' +
            '<dt class="u-header u-header--md qa-cm-name">{{ Talent_CSA__r.First_Last_Name__c }}</dt>' +
            '<dd class="u-meta u-meta--md qa-cm-title">{{ Talent_CSA__r.Title }}</dd>' +
            '{{#Talent_CSA__r.Phone}}<dd class="u-meta u-meta--sm qa-cm-phone">' +
               '<span class="u-svg-icon slds-icon__container">' +
                  '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                        '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#call"></use>' +
                  '</svg>' +
               '</span>' +
               '<a href="tel:{{ Talent_CSA__r.Phone }}">{{ Talent_CSA__r.Phone }}</a>' +
            '</dd>{{/Talent_CSA__r.Phone}}' +
            '{{#Talent_CSA__r.Email}}<dd class="u-meta u-meta--sm qa-cm-email">' +
                '<span class="u-svg-icon slds-icon__container">' +
                    '<svg aria-hidden="true" class="slds-icon slds-icon-text-default slds-icon--x-small">' +
                        '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/resource/SLDS011/assets/icons/action-sprite/svg/symbols.svg#email"></use>' +
                    '</svg>' +
               '</span>' +
               '<a class="qa-comm-mgr-email-link" href="mailto:{{ Talent_CSA__r.Email }}">'+skuid.$L('TC_Email')+'</a>' +
            '</dd>{{/Talent_CSA__r.Email}}' +
        '</dl>' +
    '</div>';

    return customerServiceTmpl;
}
