(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "tc_components__myprofilesummary",
		name: "My Profile Summary",
		icon: "sk-icon-contact",
		description: "This component show the user profile",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>My Profile Summary</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>My Profile Summary</div>"
			);
		},
		propertiesRenderer: function (propertiesObj,component) {
			propertiesObj.setTitle("Card Header Properties");
			var state = component.state;
			var propCategories = [];
			var propsList = [
				{
					id: "profile_summary_type",
					type: "picklist",
					label: "Card Type",
					picklistEntries: [
						{
							value: 'social',
							label: 'Social'
						},
						{
							value: 'dashboard',
							label: 'Dashboard'
						}

					],
					helptext: "Display dashboard, or social in the profile summary card",
					onChange: function(){
						component.refresh();
					}
				}
			];
			propCategories.push({
				name: "",
				props: propsList,
			});
			if(skuid.mobile) propCategories.push({ name : "Remove", props : [{ type : "remove" }] });
			propertiesObj.applyPropsWithCategories(propCategories,state);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<tc_components__myprofilesummary/>");
		}
	}));
})(skuid);
