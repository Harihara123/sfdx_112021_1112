var $=skuid.$;

(function(skuid){

	// ===================
	// Profile summary base
	// ===================
	skuid.componentType.register("tc_components__myprofilesummary", function(element, xmlDef){

		// If we are specifying a user ID we need to pull that user, otherwise pull the running user
		if(getQueryVariable('id')){

			// ===================
			// Public profile template
			// ===================

			var model = skuid.$M('ActiveUser');

			var myEmploymentProfileTmpl = "<div class='c-profile__avatar u-avatar' style='background-image: url({{{ FullPhotoUrl }}})'></div>" +
					"<section class='c-profile__content'>" +
						"<dl>" +
							"<dt class='u-header u-header--md qa-FullName'>{{ First_Last_Name__c }}</dt>" +
							"<dd class='u-meta u-meta--sm qa-Title'>{{ Title }}" +
							"<span class='qa-Location'>{{#Address.city}}, {{ Address.city }}{{/Address.city}}{{#Address.state}}, {{ Address.state }}{{/Address.state}}</span></dd>";

						myEmploymentProfileTmpl += "</dl>";

					myEmploymentProfileTmpl += "</section>" +
			"</div>";

		} else {

			// ===================
			// Private Profile Template
			// ===================

			var model = skuid.$M('RunningUser');
			var row = model.getFirstRow();

			var arr = [];
			arr.push(row.Contact.Title);
			arr.push(row.Contact.MailingCity);
			arr.push(row.Contact.MailingState);

			arr = arr.filter(function(v){
					return v != null
				});

			var titlestring = arr.join(', ');

			var myEmploymentProfileTmpl = "<div class='c-profile__avatar u-avatar' style='background-image: url({{{ FullPhotoUrl }}})'></div>" +
					"<section class='c-profile__content'>" +
						"<dl>" +
							"<dt class='u-header u-header--md qa-FullName' data-hj-masked>{{ Contact.Full_Name__c }}</dt>" +
							"<dd class='u-meta u-meta--sm qa-Title-Location'><span class='qa-Title-Location'>"+ titlestring +"</span></dd>";

							// Peoplesoft ID
							myEmploymentProfileTmpl += "<dd class='u-meta u-meta--sm'>{{ $Label.TC_Employee_ID }}# <span class='qa-EmployeeId' data-hj-masked>{{ Contact.Peoplesoft_ID__c }}</span></dd>";

						myEmploymentProfileTmpl += "</dl>";

						if(xmlDef.attr("profile_summary_type") !== 'social'){
							myEmploymentProfileTmpl += "<a href='{{ $Site.Prefix }}/{{ Id }}' class='u-btn u-btn--tertiary qa-ImproveMyProfile'>{{ $Label.TC_Improve_My_Profile }}</a>";
						}

					myEmploymentProfileTmpl += "</section>" +
			"</div>";
		}

		var partOne = skuid.utils.merge( 'row', myEmploymentProfileTmpl, null, model, row );

		// ===================
		// Open social markup and data
		// ===================

		if(getQueryVariable('id')){

			// ===================
			// Public Social View
			// ===================

			var openSocialTmpl = [
			'<div class="c-tile-open-social">',
				"<div class='c-social'>",
					'<ul>',
						"{{#LinkedIn_URL__c}}",
						'<li><a href="{{ LinkedIn_URL__c }}" target="_blank"><i class="zmdi zmdi-linkedin-box zmdi-hc-fw zmdi-hc-lg"></i><span class="sr-only">{{ $Label.TC_LinkedIn }}</span></a></li>',
						"{{/LinkedIn_URL__c}}",
						"{{#Facebook_URL__c}}",
						'<li><a href="{{ Facebook_URL__c }}" target="_blank"><i class="zmdi zmdi-facebook zmdi-hc-fw zmdi-hc-lg"></i><span class="sr-only">{{ $Label.TC_Facebook }}</span></a></li>',
						"{{/Facebook_URL__c}}",
						"{{#Twitter_URL__c}}",
						'<li><a href="{{ Twitter_URL__c }}" target="_blank"><i class="zmdi zmdi-twitter zmdi-hc-fw zmdi-hc-lg"></i><span class="sr-only">{{ $Label.TC_Twitter }}</span></a></li>',
						"{{/Twitter_URL__c}}",
					'</ul>',
				"</div>",
				"{{#Phone}}",
				'<div class="phone">',
					'<a href="tel:{{{ Phone }}}"><i class="zmdi zmdi-phone zmdi-hc-fw"></i>{{{ Phone }}}',
				'</div>',
				'{{/Phone}}',
				'{{#Email}}',
				'<div class="email">',
					'<a href="mailto:{{{ Email }}}"><i class="zmdi zmdi-email zmdi-hc-fw"></i>Email',
				'</div>',
				'{{/Email}}',
			'</div>'
			].join("");

		} else {

			// ===================
			// Private Social View
			// ===================

			var openSocialTmpl = [
			'<div class="c-tile-open-social">',
				"<div class='c-social'>",
					'<ul>',
						"{{#Contact.LinkedIn_URL__c}}",
						'<li><a href="{{ Contact.LinkedIn_URL__c }}" target="_blank"><i class="zmdi zmdi-linkedin-box zmdi-hc-fw zmdi-hc-lg"></i><span class="sr-only">{{ $Label.TC_LinkedIn }}</span></a></li>',
						"{{/Contact.LinkedIn_URL__c}}",
						"{{#Contact.Facebook_URL__c}}",
						'<li><a href="{{ Contact.Facebook_URL__c }}" target="_blank"><i class="zmdi zmdi-facebook zmdi-hc-fw zmdi-hc-lg"></i><span class="sr-only">{{ $Label.TC_Facebook }}</span></a></li>',
						"{{/Contact.Facebook_URL__c}}",
						"{{#Contact.Twitter_URL__c}}",
						'<li><a href="{{ Contact.Twitter_URL__c }}" target="_blank"><i class="zmdi zmdi-twitter zmdi-hc-fw zmdi-hc-lg"></i><span class="sr-only">{{ $Label.TC_Twitter }}</span></a></li>',
						"{{/Contact.Twitter_URL__c}}",
					'</ul>',
				"</div>",
				"{{#Contact.Phone}}",
				'<div class="phone">',
					'<a href="tel:{{{ Contact.Phone }}}"><i class="zmdi zmdi-phone zmdi-hc-fw"></i>{{{ Contact.Phone }}}',
				'</div>',
				'{{/Contact.Phone}}',
				'{{#Contact.Email}}',
				'<div class="email">',
					'<a href="mailto:{{{ Contact.Email }}}"><i class="zmdi zmdi-email zmdi-hc-fw"></i>Email',
				'</div>',
				'{{/Contact.Email}}',
			'</div>'
			].join("");

		}

		// ===================
		// Build tile logic
		// ===================
		var partTwo = '';

		if(xmlDef.attr("profile_summary_type") === 'social'){
			partTwo = skuid.utils.merge( 'row', openSocialTmpl, null, model, row );
			element.html( partOne[0].innerHTML + partTwo[0].innerHTML );
		} else {
			element.html( partOne[0].innerHTML );
		}

	});

})(skuid);

// ===================
// Skuid Data Wrapper
// ===================
var DataFactory = {

	communityId: '0DBJ00000004CaOOAU',

	addSubscribeRow: function(model, followId){

		if(typeof followId === 'undefined'){
			alert('warning');
			var followId = getQueryVariable('id');
		}

		var dfremmodel = skuid.$M(model);
		var runningUser = skuid.$M('RunningUser');
		var runningUserRow = runningUser.getFirstRow();

		// TODO: Dynamically populate networkId
		var newRow = dfremmodel.createRow({
			additionalConditions: [
				{ field: 'NetworkId', value: DataFactory.communityId},
				{ field: 'ParentId', value: followId},
				{ field: 'SubscriberId ', value: runningUserRow.Id}
			], doAppend: true
		});

		dfremmodel.save({callback: function(result){
			if(result.totalsuccess){
				// good
				//console.log('successfully added row', newRow);
				skuid.component.getByType('tc_components__my_profile_summary')[0].render();
			} else {
				//console.log('messages', result.messages);
			}

		}});
	},

	removeRow: function(model, row){
		var dfremmodel = skuid.$M(model);
		var target = dfremmodel.getRowById(row);

		dfremmodel.deleteRow(target);
		dfremmodel.save({callback: function(result){
			if(result.totalsuccess){
				//console.log('successfully removed row', target);
				skuid.component.getByType('tc_components__my_profile_summary')[0].render();
			} else {
				//console.log('messages', result.messages);
			}
		}});
	}
}

function getQueryVariable(variable){
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable){return pair[1];}
	}
	return(false);
}