(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "tc_components__newslist",
		name: "News List",
		icon: "sk-icon-contact",
		description: "Displays a list of news articles.",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>News List</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>News List</div>"
			);
		},
		propertiesRenderer: function (propertiesObj,component) {
			propertiesObj.setTitle("News List Properties");
			var state = component.state;
			var propCategories = [];
			var propsList = [
				{
					id: "newslist_show_summary",
					type: "boolean",
					label: "Show Article Summaries",
					helptext: "Check this to show the news article summary under the title.",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "modelId",
					type: "model",
					label: "Model",
					required: true,
					location: "attribute",
					onChange: function(){
						component.refresh();
					}
				},
				{
					id: "newslist_show_viewall",
					type: "boolean",
					label: "Show View All Link",
					helptext: "Check this to add a 'View All News' link to the card footer.",
					onChange: function(){
						component.refresh();
					}
				}
			];
			propCategories.push({
				name: "",
				props: propsList,
			});
			if(skuid.mobile) propCategories.push({ name : "Remove", props : [{ type : "remove" }] });
			propertiesObj.applyPropsWithCategories(propCategories,state);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<tc_components__newslist />");
		}
	}));
})(skuid);
