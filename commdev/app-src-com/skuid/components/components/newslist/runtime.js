var $ = skuid.$;
(function(skuid){
    skuid.componentType.register("tc_components__newslist", function(element, xmlDef){
        var modelName = xmlDef.attr('modelId');
        var model = skuid.$M(modelName),
            newsArticles = model.getRows();

            if (model.objectName === 'News__kav') {
                linkurl = 'tc_news';
            } else { 
                linkurl = 'tc_help';
            }



            // output
            if(newsArticles) {
                if (newsArticles.length) {
                    element.html(renderNewsList(newsArticles,xmlDef,model,modelName,linkurl));
                } else {
                    element.html(renderNoNewsMessage());
                }
            } else {
                element.html(renderNoNewsMessage());
            }
        
    });
})(skuid);

function renderNoNewsMessage() {
    var message = '<p>'+ skuid.$L('TC_No_news')+'</p>';
    return message;
}

function renderNewsList(newsArticles,xmlDef,model,modelName,linkurl) {

    String.prototype.trimToLength = function(m) {
      return (this.length > m)
        ? $.trim(this).substring(0, m).split(" ").slice(0, -1).join(" ") + "..."
        : this;
    };
    link2 = linkurl;

    var html = '<ul class="c-list">';
    $.each(newsArticles, function(i,newsArticle,model,modelName,linkurl) {

        html += '<li>';
        html += '<a href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/'+link2+'_article?id=' + newsArticle.UrlName + '">' + newsArticle.Title + '</a>';
       

        if(xmlDef.attr("newslist_show_summary") === 'true'){
            html += '<br><span class="c-list--summary">' + newsArticle.Summary.trimToLength(100) + '</span>';
        }

        html += '</li>';
    });

    html += '</ul>';

    if(xmlDef.attr("newslist_show_viewall") === 'true'){
        html += '<p><a href="'+skuid.utils.mergeAsText('global','{{$Site.Prefix}}')+'/'+link2+'" class="u-viewall">'+ skuid.$L('TC_View_All') +'</a></p>';
    }  

    return html;
}






