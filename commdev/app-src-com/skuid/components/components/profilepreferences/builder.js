(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "tc_components__profilepreferences",
		name: "Profile Preferences",
		icon: "sk-icon-contact",
		description: "This component show the user's preferences.'",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>Profile Preferences</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>My Preferences</div>"
			);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<tc_components__profilepreferences/>");
		}
	}));
})(skuid);
