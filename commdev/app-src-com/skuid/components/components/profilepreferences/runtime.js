var $ = skuid.$;
(function(skuid){
	skuid.componentType.register("tc_components__profilepreferences", function(element, xmlDef){

		var model = skuid.$M('PublicUser'),
			row = model.getFirstRow();
            json = row.Talent_Preference__c;
            // console.log(json);

		// output
        if(json) {
            element.html(renderPreferences(json));
        } else {
            element.html('<p>' + skuid.utils.mergeAsText("global","{{$Label.TC_No_preferences_defined}}") + '</p>');
        }

	});
})(skuid);

function renderPreferences(json) {
    var html = '<div class="c-profile-preferences">';
        jsonParsed = JSON.parse(json);

    $.each(jsonParsed, function(index, element) {
		if (!((index.toLowerCase() === 'goals') || (index.toLowerCase() === 'linkedin')) && element.data.length > 0) {
            html += '<div class="c-profile-preferences__preference">';
            html += '<div class="u-header u-header--md u-capitalize">' + index + '</div>';
            $.each(element, function(index, data){
                if(data) { 
                    html += '<div class="value">' + data.split(",").join(", ") + '</div>';
                } 
            });
            html += '</div>'
        }
    });

    html += '</div>';

    return html;
}