function renderField(field, data, mode) {
	var fieldRender = {
		"STRING_View":  renderStringViewField(field, data),
		"STRING_Edit":  renderStringEditField(field, data),
		"DATE_View": renderDateViewField(field, data),
		"DATE_Edit": renderDateEditField(field, data),
		"PICKLIST_View" : renderPicklistViewField(field, data),
		"PICKLIST_Edit" : renderPicklistEditField(field, data),
	};
	if (typeof data == 'undefined') {
		data = '';
	}
	
	return fieldRender[field.displaytype + '_' + mode];
}


function renderStringViewField(field, data) {
	return "rendered " + data  + " as string";
}

function renderStringEditField(field, data) {
	return "rendered " + data  + " as string";
}

function renderDateViewField(field, data) {
	return "rendered " + field + " as date";
}

function renderDateEditField(field, data) {
	return "rendered " + field + " as date";
}

function renderPicklistViewField(field, data) {
	return "rendered " + field + " as pick list";
}


function renderPicklistEditField(field, data) {
	return "rendered " + field + " as pick list";
}