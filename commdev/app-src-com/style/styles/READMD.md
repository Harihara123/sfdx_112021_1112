# Styles
## The Grid
### .u-container
Gives the proper responsive widths to it's contents

### .u-full-width-container
An element that will accept a theme to create a grid-breaking container. This element will require a child DOM element with the class `.u-container` to ensure its contents respect the global responsive grid framework

## Mixins
### .material-container();
Material container is responsible for adding the "card" styles to an element. At a high level this will contain a default background color, some amount of padding, some amount of spacing (margin), and some sort to depth modifer (box-shadow, border, or something like that)

### .u-btn();
Applies sitewide button styling to element

### .u-btn--ghost();
## .u-btn-link();

## Components
### .c-card
Creates a "card" metaphor. Will most likely continue to be closely aligned with the mixin `.material-container();`

### .c-field-editor
Applies custom styling to skuid field editor component

### .c-social
Applies sitewide 'social' icon styling to elements inside

## JS Helpers
### .js-card-height
Any `.c-card` element inside a `.js-card-height` wrapper will be check for height, and have its height set equal to the tallest `.c-card` in that wrapper.