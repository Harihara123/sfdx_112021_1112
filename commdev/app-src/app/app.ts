import "native-promise-only";
import "object-assign";
import "../library/formatting/libphonenumber/i18nPhoneFormatter";
//import "jspdf";
import { partsWiring } from "./wiring";
import { componentWiring } from "./components/wiring";


(($) => {
    // HACK: Ensuring the skuid loading message is hidden on page load
    $(() => { $(".nx-page.sk-loading").hide() });

    // Wire parts
    partsWiring();

    // Wire components
    componentWiring();
})(jQuery);
