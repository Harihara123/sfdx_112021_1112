// Library
import * as array from "../../library/array";
import * as core from "../../library/core";
import * as optional from "../../library/optional";
import * as text from "../../library/text";
import * as ui from "../../library/ui";
import * as utils from "../../library/utils";

// Common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

// Data
import * as activitiesData from "./data";

// Helpers
import * as jqueryuiHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

interface Models {
    persistent: {
        open: skuid.model.Model;
        past: skuid.model.Model;
    };
    ephemeral: skuid.model.Model;
}

interface Elements {
    $timelineEditButton: JQuery;
    $timelineDeleteButton: JQuery;
    $modalCancelButton: JQuery;
    $modalCloseButton: JQuery;
    $modalSaveButton: JQuery;
}

interface Selectors {
    timelineEditButton: string;
    timelineDeleteButton: string;
    modalCancelButton: string;
    modalCloseButton: string;
    modalSaveButton: string;
}

interface Dialogs extends commonUI.dialogs.Dialogs {
    $markComplete: JQuery;
}

interface UIEvents extends commonUI.events.Events {
    markCompleteDialogHasOpened: string;
    markCompleteDialogHasClosed: string;
}

interface Events {
    model: commonModel.events.Events;
    ui: UIEvents;
}

interface Activity {
    models: Models;
    condition: skuid.model.Condition;
    events: Events;
    dialogs: commonUI.dialogs.Dialogs;
    selectors: Selectors;
}

export function candidateActivitiesFromOver(view: commonUI.page.Page) {
    return function candidateActivitiesAs() {

        let modelRow = commonModel.row.rowAs();

        commonUI.page.viaSomePage(view, {
            caseOfLandingPage: core.ignore,
            caseOfDetailsPage() {
                ui.setPageTitle(commonUI.labels.candidate.activities.title);
            },
            caseOfNeither: (reason) => core.fail<void>(reason)
        });

        const taskActivity = activityFrom(
            "EphemeralTaskModel",
            activitiesData.task.addOrEditTemplate,
            activitiesData.task.markCompleteTemplate,
            {
                model: {
                    wishCouldAdd: "ats.candidateActivities.taskActivity.wishCouldAdd",
                    wishCouldEdit: "ats.candidateActivities.taskActivity.wishCouldEdit",
                    wishCouldDelete: "ats.candidateActivities.taskActivity.wishCouldDelete",
                    wishCouldConfirmDelete: "ats.candidateActivities.taskActivity.wishCouldConfirmDelete",
                    wishCouldSave: "ats.candidateActivities.taskActivity.wishCouldSave",
                    wishCouldCancel: "ats.candidateActivities.taskActivity.wishCouldCancel",
                    hasUpdated: "ats.candidateActivities.taskActivity.modelHasUpdated"
                },
                ui: {
                    addOrEditDialogHasOpened: "ats.candidateActivities.taskActivity.addOrEditDialogOpened",
                    addOrEditDialogHasClosed: "ats.candidateActivities.taskActivity.addOrEditDialogClosed",
                    confirmDeleteDialogHasOpened: "ats.candidateActivities.taskActivity.deleteDialogOpened",
                    confirmDeleteDialogHasClosed: "ats.candidateActivities.taskActivity.deleteDialogClosed",
                    markCompleteDialogHasOpened: "ats.candidateActivities.taskActivity.markCompleteDialogOpened",
                    markCompleteDialogHasClosed: "ats.candidateActivities.taskActivity.markCompleteDialogClosed",
                    wishCouldReloadTimeline: "editAddActivityQueryModel"
                }
            },
            {
                timelineEditButton: ".timeline_editEmploymentButton",
                timelineDeleteButton: ".timeline_deleteEmploymentButton",
                modalCancelButton: "#ats_candidateActivities_taskActivity-modalCancelButton",
                modalCloseButton: "#ats_candidateActivities_taskActivity-modalCloseButton",
                modalSaveButton: "#ats_candidateActivities_taskActivity-modalSaveButton"
            }
        );

        const eventActivity = activityFrom(
            "EphemeralEventModel",
            activitiesData.event.addOrEditTemplate,
            activitiesData.event.markCompleteTemplate,
            {
                model: {
                    wishCouldAdd: "ats.candidateActivities.eventActivity.wishCouldAdd",
                    wishCouldEdit: "ats.candidateActivities.eventActivity.wishCouldEdit",
                    wishCouldDelete: "ats.candidateActivities.eventActivity.wishCouldDelete",
                    wishCouldConfirmDelete: "ats.candidateActivities.eventActivity.wishCouldConfirmDelete",
                    wishCouldSave: "ats.candidateActivities.eventActivity.wishCouldSave",
                    wishCouldCancel: "ats.candidateActivities.eventActivity.wishCouldCancel",
                    hasUpdated: "ats.candidateActivities.eventActivity.modelHasUpdated"
                },
                ui: {
                    addOrEditDialogHasOpened: "ats.candidateActivities.eventActivity.addOrEditDialogOpened",
                    addOrEditDialogHasClosed: "ats.candidateActivities.eventActivity.addOrEditDialogClosed",
                    confirmDeleteDialogHasOpened: "ats.candidateActivities.eventActivity.deleteDialogOpened",
                    confirmDeleteDialogHasClosed: "ats.candidateActivities.eventActivity.deleteDialogClosed",
                    markCompleteDialogHasOpened: "ats.candidateActivities.eventActivity.markCompleteDialogOpened",
                    markCompleteDialogHasClosed: "ats.candidateActivities.eventActivity.markCompleteDialogClosed",
                    wishCouldReloadTimeline: "editAddActivityQueryModel"
                }
            },
            {
                timelineEditButton: ".timeline_editEmploymentButton",
                timelineDeleteButton: ".timeline_deleteEmploymentButton",
                modalCancelButton: "#ats_candidateActivities_eventActivity-modalCancelButton",
                modalCloseButton: "#ats_candidateActivities_eventActivity-modalCloseButton",
                modalSaveButton: "#ats_candidateActivities_eventActivity-modalSaveButton"
            }
        );

        let registerTaskDomEvents = registerDomEventsOver(taskActivity.selectors, taskActivity.events, modelRow);
        let registerEventDomEvents = registerDomEventsOver(eventActivity.selectors, eventActivity.events, modelRow);

        registerTaskDomEvents();
        registerEventDomEvents();

        registerEventSubscriptions(taskActivity, modelRow, `${commonUI.labels.unsorted.task} `);
        registerEventSubscriptions(eventActivity, modelRow, `${commonUI.labels.unsorted.event} `);

    };
}

function activityFrom(
    ephemeralModelName: string,
    addOrEditTemplate: string,
    markCompleteTemplate: string,
    //ephemeralModelProperties: skuidModelHelpers.ModelProperties,
    events: Events,
    selectors: Selectors
): Activity {
    const models: Models = {
        persistent: {
            open: optional.asSomeOrFail(
                skuidModelHelpers.getModelOpt("PersistentOpenActivityModel"),
                "PersistentOpenActivityModel could not be found"
                //() => skuidModelHelpers.modelFromXML(activitiesData.persistentOpenActivityModelTemplate)
            ),
            past: optional.asSomeOrFail(
                skuidModelHelpers.getModelOpt("PersistentPastActivityModel"),
                "PersistentPastActivityModel could not be found"
                //() => skuidModelHelpers.modelFromXML(activitiesData.persistentPastActivityModelTemplate)
            )
        },
        ephemeral: optional.asSomeOrFail(
            skuidModelHelpers.getModelOpt(ephemeralModelName),
            `${ephemeralModelName} could not be found`
            //() => skuidModelHelpers.modelFrom(ephemeralModelProperties)
        )
    };

    //skuid.model.load(array.fromThree(models.persistent.open, models.persistent.past, models.ephemeral));

    const condition = optional.withSomeOrFail(
        skuidModelHelpers.getConditionOpt(models.ephemeral, "Id", false),
        core.bounce,
        text.emptyString
    );

    const dialogs: Dialogs = {
        $addOrEdit: jqueryuiHelper.dialog.from(
            addOrEditTemplate,
            [
                {
                    text: commonUI.labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: (event: Event) => skuid.events.publish(events.model.wishCouldCancel),
                    showText: true
                },
                {
                    text: commonUI.labels.generic.saveButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: (event: Event) => skuid.events.publish(events.model.wishCouldSave),
                    showText: true
                }
            ],
            events.ui.addOrEditDialogHasOpened,
            events.model.wishCouldCancel,
            false
        ),
        $markComplete: jqueryuiHelper.dialog.from(
            markCompleteTemplate,
            [
                {
                    text: commonUI.labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: (event: Event) => skuid.events.publish(events.model.wishCouldCancel),
                    showText: true
                },
                {
                    text: commonUI.labels.generic.saveButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: (event: Event) => skuid.events.publish(events.model.wishCouldSave),
                    showText: true
                }
            ],
            events.ui.markCompleteDialogHasOpened,
            events.model.wishCouldCancel,
            false
        ),
        $confirmDelete: jqueryuiHelper.dialog.from(
            commonUI.templates.confirmDeleteTemplate,
            [
                {
                    text: commonUI.labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: (event: Event) => skuid.events.publish(events.model.wishCouldCancel),
                    showText: true
                },
                {
                    text: commonUI.labels.generic.deleteButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: function(event: Event) {
                        skuid.events.publish(events.model.wishCouldDelete, [{
                            rowId: $(this).attr("data-rowId")
                        }]);
                    },
                    showText: true
                }
            ],
            events.ui.confirmDeleteDialogHasOpened,
            events.ui.confirmDeleteDialogHasClosed,
            false
        )
    };

    return { models, condition, events, dialogs, selectors };
}

function saveModelChangesOver(models: Models, events: Events, dialogs: commonUI.dialogs.Dialogs) {
    return function saveModelChanges() {
        optional.withSomeOfBothOrFail(
            skuidModelHelpers.getModelOpt("TalentAccount"),
            skuidModelHelpers.getModelOpt("TalentContact"),
            (talentAccountModel, talentContactModel) => {
                models.ephemeral.updateRow(
                    models.ephemeral.getFirstRow(), {
                    "OwnerId": skuid.utils.userInfo.userId,
                    "WhatId": skuidModelHelpers.getFieldValueOfFirstRow(talentAccountModel, "Id"),
                    "WhoId": skuidModelHelpers.getFieldValueOfFirstRow(talentContactModel, "Id")
                });
                skuidModelHelpers.save<void>(models.ephemeral)
                    .then(result => {
                        skuidModelHelpers.updateDataOfAll(array.fromThree(models.ephemeral, models.persistent.open, models.persistent.past))
                            .then(result => {
                                jqueryuiHelper.dialog.close(dialogs.$addOrEdit);
                                skuidUIHelpers.removeErrors();
                                skuid.events.publish(events.model.hasUpdated);
                            })
                            .catch(core.scheduleError);
                    })
                    .catch(core.scheduleError);
            },
            text.emptyString
        );
    };
}

function reloadTimelineOver(events: Events) {
    return function reloadTimeline() {
        skuid.events.publish(events.ui.wishCouldReloadTimeline, [[[], "DESC"]]);
    };
}

function elementsFrom(selectors: Selectors): Elements {
    return {
        $timelineEditButton: $(selectors.timelineEditButton),
        $timelineDeleteButton: $(selectors.timelineDeleteButton),
        $modalCancelButton: $(selectors.modalCancelButton),
        $modalCloseButton: $(selectors.modalCloseButton),
        $modalSaveButton: $(selectors.modalSaveButton)
    };
}

function registerDomEventsOver(selectors: Selectors, events: Events, employmentRow: commonModel.row.Row) {
    return function registerDomEvents() {
        let elements = elementsFrom(selectors);

        elements.$timelineEditButton.off("click").on(
            "click",
            (event) => {
                employmentRow.setRowId($(event.target).attr("data-rowId"));
                skuid.events.publish(events.model.wishCouldEdit);
            }
        );

        elements.$timelineDeleteButton.off("click").on(
            "click",
            () => {
                employmentRow.setRowId($(event.target).attr("data-rowId"));
                skuid.events.publish(events.model.wishCouldDelete);
            }
        );
    }
}

function registerEventSubscriptions(activity: Activity, modelRow: commonModel.row.Row, activityTypeHeader) {
    skuid.events.subscribe(
        activity.events.model.wishCouldAdd,
        () => {
            commonModel.row.addRowOver(
                activity.models.ephemeral,
                activity.condition,
                activity.dialogs.$addOrEdit,
                `${commonUI.labels.generic.add} ` + activityTypeHeader
            )();
            setDefaultEventModelStartEndDateValues(activity.models.ephemeral);
        }
    );

    skuid.events.subscribe(
        activity.events.model.wishCouldEdit,
        commonModel.row.editRowOver(
            activity.models.ephemeral,
            activity.condition,
            modelRow,
            activity.dialogs.$addOrEdit,
            `${commonUI.labels.generic.add} ${commonUI.labels.unsorted.task}`,
            activity.events.model.hasUpdated
        )
    );

    skuid.events.subscribe(
        activity.events.model.wishCouldDelete,
        commonModel.row.deleteRowOver(activity.dialogs.$confirmDelete)
    );

    // TODO: Doesn't take into account a case of open activity deletion
    skuid.events.subscribe(
        activity.events.model.wishCouldConfirmDelete,
        commonModel.row.confirmDeleteRowOver(
            modelRow,
            activity.models.persistent.past,
            activity.dialogs.$confirmDelete,
            activity.events.model.hasUpdated
        )
    );

    skuid.events.subscribe(
        activity.events.model.wishCouldSave,
        saveModelChangesOver(activity.models, activity.events, activity.dialogs)
    );

    skuid.events.subscribe(
        activity.events.model.wishCouldCancel,
        commonModel.cancelChangesOver(activity.models.ephemeral)
    );

    skuid.events.subscribe(
        activity.events.model.hasUpdated,
        registerDomEventsOver(activity.selectors, activity.events, modelRow)
    );

    skuid.events.subscribe(activity.events.model.hasUpdated, reloadTimelineOver(activity.events));
}

// =======================================================
// NOTE: Legacy code below
// =======================================================

module legacy {

    interface ViaSomeActivityType<a> {
        caseOfTask: () => a;
        caseOfEvent: () => a;
        caseOfNeither: (msg: string) => a;
    }

    enum ActivityTypes { "Task", "Event" }

    export class ActivityType {

        private activityType: string;

        constructor() {
            this.activityType = text.emptyString;
        }

        getActivityType() {
            return this.activityType;
        }

        setActivityType(activityType: string) {
            this.activityType = activityType;
        }

        viaSomeActivityType<a>(activityType: ActivityTypes, via: ViaSomeActivityType<a>) {
            switch(activityType) {
                case ActivityTypes.Task: return via.caseOfTask();
                case ActivityTypes.Event: return via.caseOfEvent();
                default: return via.caseOfNeither("Unexpected activityType: " + activityType);
            }
        }

    };

    function callEditDeletePopupActivity(
        model: skuid.model.Model,
        condition: skuid.model.Condition,
        recordId: string,
        $dialog: JQuery
    ) {
        model.cancel();
        model.deactivateCondition(condition);
        model.setCondition(condition, recordId);
        model.activateCondition(condition);

        Promise.resolve(model.updateData<skuid.model.Model>())
            .then(() => {
                $dialog.dialog("open");
            });


        // NOTE: Doing too much!

        // let $popupXML = $(skuid.utils.makeXMLDoc(popupXMLString));
        //
        // lo.withSomeOrFail(
        //     skh.model.retrieve(modelName),
        //     model => {
        //         model.emptyData();
        //
        //         lo.withSomeOrFail(
        //             lm.getConditionByName(model, modelConditionName, false),
        //             condition => {
        //                 model.cancel();
        //                 model.deactivateCondition(condition);
        //                 model.setCondition(condition, recordId);
        //                 model.activateCondition(condition);
        //
        //                 Promise.resolve(model.updateData<skuid.model.Model>())
        //                 .then(() => {
        //                     let row = model.getFirstRow();
        //                     let context = { row: row };
        //                     let popup = skuid.utils.createPopupFromPopupXML($popupXML, context);
        //                 });
        //             },
        //             lt.emptyString
        //         );
        //     },
        //     lt.emptyString
        // );
    }

    function editActivityFromTimeLine(
        model: skuid.model.Model,
        condition: skuid.model.Condition,
        popupTemplate: string,
        recordId: string
    ) {

        // NOTE: This logic should be handled by the caller
        // if (popupTemplate === 'task') {
        //      popupXMLString = cad.task.editPopupTemplate;
        // } else {
        //      popupXMLString = cad.event.editPopupTemplate;
        // }

        // NOTE: This should be called by the caller...
        // callEditDeletePopupActivity(
        //     model,
        //     condition,
        //     recordId,
        //     dialogFrom(
        //         popupTemplate,
        //         "ats.candidateActivities.editModalOpened",
        //         "ats.candidateActivities.editModalClosed"
        //     )
        // );
    }


    function deleteActivityFromTimeLine(
        model: skuid.model.Model,
        condition: skuid.model.Condition,
        activityType: string,
        recordId: string,
        popupTemplate: string
    ) {
        // NOTE: This should be called by the caller
        // callEditDeletePopupActivity(
        //     model,
        //     condition,
        //     recordId,
        //     dialogFrom(
        //         cad.activityDeleteConfirmationTemplate,
        //         "ats.CandidateActivities.deleteModalOpened",
        //         "ats.CandidateActivities.deleteModalClosed"
        //     )
        // );
     }

    function loadMoreOpenActivity() {
        skuid.events.publish("OpenActivities", array.fromTwo(queryFilter().join(","), 'DESC'));
    }

    function loadMorePastActivity() {
        skuid.events.publish("ActivityHistories", array.fromTwo(queryFilter().join(","), 'DESC'));
    }

    function queryFilter(): string[] {
        let selectedMapTask  = ui.getSelectedCheckboxValues("TASK");
        let selectedMapEvent = ui.getSelectedCheckboxValues("EVENT");

        if (!selectedMapTask.areAllSelected && !selectedMapEvent.areAllSelected) {
            return array.viaTwoArrays(selectedMapTask.selectedElements, selectedMapEvent.selectedElements, array.hasAny, {
                caseOfBoth(taskSelectedElements, eventSelectedElements) {
                    return array.concat(
                        array.map(taskSelectedElements, core.accessors.valueOf),
                        array.map(eventSelectedElements, core.accessors.valueOf)
                    );
                },
                caseOfFirst(taskSelectedElements) {
                    return array.map(taskSelectedElements, core.accessors.valueOf);
                },
                caseOfSecond(eventSelectedElements) {
                    return array.map(eventSelectedElements, core.accessors.valueOf);
                },
                caseOfNeither(msg) { return array.of<string>(); }
            });
        } else {
            return array.of<string>();
        }
    }

    function onAddEditActivityQueryModels() {
        timelineActivity.onAddEditActivityQueryModels('editAddActivityQueryModel', queryFilter(), 'DESC');
    }

    function saveModelTask() {
        let dfd = $.Deferred();

        function saveModels() {

            skuidUIHelpers.removeErrors();

            optional.withSomeOrFail(
                skuidModelHelpers.getModelOpt("Task"),
                model => {
                    //use a "trashbin" element to suppress standard errors
                    let editor = new skuid.ui.Editor($("#trashbin"), { showSaveCancel: false });
                    editor.registerModel(model);
                    let editorId = editor.id();

                    skuid.model.save(
                        array.fromOne(model), {
                        callback(result) {
                            result.totalsuccess ? dfd.resolve() : dfd.reject();
                        }
                        // NOTE: skuid.model.save only takes the previous two arguments
                        //, initiatorId: editorId
                    });
                },
                text.emptyString
            );


        }

        $.when( saveModels() );
        //.then(
        //  function( message ) {
        //    console.log(message);
        //  },
        //  function( message ) {
        //    console.log(message);
        //  }
        //);

        return dfd.promise();
    }

    function saveModelEvent() {
        let dfd = $.Deferred();

        function saveModels() {

            skuidUIHelpers.removeErrors();

            optional.withSomeOrFail(
                skuidModelHelpers.getModelOpt("Event"),
                model => {
                    //use a "trashbin" element to suppress standard errors
                    let editor = new skuid.ui.Editor($("#trashbin"), { showSaveCancel: false });
                    editor.registerModel(model);
                    let editorId = editor.id();

                    skuid.model.save(
                        array.fromOne(model), {
                        callback(result) {
                            result.totalsuccess ? dfd.resolve() : dfd.reject();
                        }
                        // NOTE: skuid.model.save only takes the previous two arguments
                        //, initiatorId: editorId
                    });
                },
                text.emptyString
            );
        }

        $.when( saveModels() );
        //.then(
        //  function( message ) {
        //    console.log(message);
        //  },
        //  function( message ) {
        //    console.log(message);
        //  }
        //);

        return dfd.promise();
    }

    function getActivityDeleteConfirmXMLString() {
        return commonUI.templates.confirmDeleteTemplate;
    }

    function CustomMessageHandler($element: JQuery) {
        let dueDate = skuidUIHelpers.labelFromOrFail("ATS_DUE_DATE_LABEL_DEFAULT");
        let dueDateNew = skuidUIHelpers.labelFromOrFail("ATS_DUE_DATE_LABEL_NEW");

        $element.on("click", skuidUIHelpers.removeErrors);

        skuid.events.subscribe('models.saved', (data: { messages: string[] }) => {
            if (array.hasAny(data.messages)) {
                let listItems = $("<div>").addClass("nx-error");
                data.messages.forEach(message => {
                    let newMessage = message.replace("[" + dueDate + "]", "[" + dueDateNew + "]");
                    listItems.append("<div>").text(skuid.utils.encodeHTML(newMessage));
                });
                $element.append(listItems);
            }
        });
    }

    function handleErrorClick() {
        skuidUIHelpers.removeErrors();
    }

    export module filter {

        export function resetFilter() {
            skuid.events.publish("resetFilterTASK");
            skuid.events.publish("resetFilterEVENT");

            applyFilter();
        }

        export function applyFilter() {

            filterOnActivities('filterOnActivity', queryFilter(), 'DESC');
        }

        function filterOnActivities(eventName, queryFilter, querySortOrder) {

            var arrayData = [timelineActivity.getQueryParamActivity(queryFilter, querySortOrder)];

            skuid.events.publish(eventName, arrayData);

     }

    }

    export module timelineActivity {

        export function getQueryParamActivity(queryFilter: string[], querySortOrder: string) {
            return  [queryFilter, querySortOrder];
        }

        export function filterOnActivities(eventName: string, queryFilter: string[], querySortOrder: string) {

            var arrayData = [getQueryParamActivity(queryFilter, querySortOrder)];

            skuid.events.publish(eventName, arrayData);

     }

     export function onAddEditActivityQueryModels(eventName: string, queryFilter: string[], querySortOrder: string) {

            var arrayData = array.fromOne(getQueryParamActivity(queryFilter, querySortOrder));

            skuid.events.publish(eventName, arrayData);

     }

    }

};

function setDefaultEventModelStartEndDateValues(model: skuid.model.Model) {

    if (model.id  === 'EphemeralEventModel') {
        const p = 60 * 60 * 1000;
        let date = new Date();
        let startTime = new Date(Math.ceil(new Date().getTime() / p ) * p);
        let endTime = new Date(Math.ceil(new Date().getTime() / p ) * p);
        endTime.setMinutes(30);
        model.updateRow(model.getFirstRow(), { StartDateTime: startTime, EndDateTime: endTime});
    }
}
