export module task {

    export let addOrEditTemplate = `
        <popup title="{{$Label.ATS_BUTTON_ADD}} {{$Label.ATS_TASK}}" width="90%">
            <components>
            <wrapper uniqueid="sk-3JTC4O-588" cssclass="">
                <components>
                    <template multiple="false" uniqueid="sk-3JTETh-un1232" allowhtml="true">
                    <contents>&lt;div class="slds-text-body--small"&gt;*{{$Label.ATS_REQUIRED_FIELDS_IN_RED}}&lt;/div&gt;</contents>
                    </template>
                    </components>
                    <styles>
                    <styleitem type="background"/>
                    <styleitem type="border"/>
                    <styleitem type="size"/>
                    </styles>
                </wrapper>
                <wrapper uniqueid="sk-286dZJ-572">
                    <components>
                        <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralTaskModel" buttonposition="" uniqueid="sk-287ANm-580" mode="edit" layout="above">
                            <columns layoutmode="responsive" columngutter="1px" rowgutter="1px">
                                <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                    <sections>
                                        <section title="Section A" collapsible="no" showheader="false">
                                            <fields>
                                                <field id="Type" required="true" valuehalign="" type="">
                                                    <label>{{$Label.ATS_TYPE}}</label>
                                                </field>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                                <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                                                    <column verticalalign="top" ratio="1" minwidth="300px" behavior="flex">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="Subject" valuehalign="" type="" required="true">
                                                                        <label>{{$Label.ATS_SUBJECT}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column verticalalign="top" ratio="4" minwidth="300px" behavior="flex">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields/>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                </columns>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                                <column behavior="flex" ratio="1" minwidth="100%" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <columns layoutmode="fixed" columngutter="4px" rowgutter="4px">
                                                    <column verticalalign="top" width="200px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="Status" valuehalign="" type="">
                                                                        <label>{{$Label.ATS_STATUS}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column verticalalign="top" width="200px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="Priority" valuehalign="" type="">
                                                                        <label>{{$Label.ATS_PRIORITY}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column verticalalign="top" width="200px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="ActivityDate" valuehalign="" monthpicker="true" yearpicker="true" yearpickermin="c-3" yearpickermax="c+3" required="true" type="">
                                                                        <label>{{$Label.ATS_DUE_DATE}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column verticalalign="top" width="200px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="Completed__c" valuehalign="" type="" readonly="true">
                                                                        <label>{{$Label.ATS_COMPLETED_DATE}}</label>
                                                                        <renderconditions logictype="and" onhidedatabehavior="keep">
                                                                           <rendercondition type="blank" operator="!=" fieldmodel="EphemeralTaskModel" sourcetype="fieldvalue" field="Completed__c" value="null" enclosevalueinquotes="false"/>
                                                                        </renderconditions>
                                                                        <enableconditions/>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                </columns>
                                            </fields>
                                        </section>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields/>
                                        </section>
                                    </sections>
                                </column>
                                <column behavior="flex" ratio="1" minwidth="100%" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <columns layoutmode="fixed" columngutter="4px" rowgutter="4px">
                                                    <column verticalalign="top" width="200px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="RecurrenceRegeneratedType" showhelp="true" valuehalign="" type="">
                                                                        <label>{{$Label.ATS_REPEAT_THIS_TASK}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column verticalalign="top" width="200px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="RecurrenceInterval" showhelp="true" valuehalign="" type="">
                                                                        <label>{{$Label.ATS_RECURRENCE_INTERVAL}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                </columns>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                                <column behavior="flex" ratio="3" minwidth="300px" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <field id="Description" valuehalign="" type="" displayrows="4">
                                                    <label>{{$Label.ATS_COMMENT}}</label>
                                                </field>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                            </columns>
                        </basicfieldeditor>
                    </components>
                    <styles>
                        <styleitem type="background"/>
                        <styleitem type="border"/>
                        <styleitem type="size"/>
                    </styles>
                </wrapper>
            </components>
        </popup>
    `;

    export let markCompleteTemplate = `
        <popup title="{{$Label.ACTIVITY_NOTES_ATS}}" width="90%">
            <components>
                <wrapper uniqueid="sk-286dZJ-572-activityCheckBoxPopup" cssclass="checkBoxPopupWrapperClassName">
                    <components>
                        <template multiple="false" uniqueid="sk-YoeqQ-382" allowhtml="true"  cssclass="checkBoxPopupTemplateClassName">
                            <contents>{{$Label.ATS_LEAVE_COMMENTS_TO_COMPELETE_TASK}}</contents>
                        </template>
                        <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralTaskModel" buttonposition="" uniqueid="sk-287ANm-580-activityCheckBoxPopup" mode="edit" layout="above">
                            <columns layoutmode="responsive" columngutter="1px" rowgutter="1px">
                                <column behavior="flex" ratio="1" minwidth="100%" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <field id="Status" valuehalign="" type="">
                                                    <label>{{$Label.ATS_STATUS}}</label>
                                                </field>
                                            </fields>
                                        </section>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields/>
                                        </section>
                                    </sections>
                                </column>
                                <column behavior="flex" ratio="1" minwidth="100%" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <field id="Description" valuehalign="" type="" displayrows="4">
                                                    <label>{{$Label.ATS_COMMENT}}</label>
                                                </field>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                            </columns>
                        </basicfieldeditor>
                        <wrapper uniqueid="sk-28IwWN-366-activityCheckBoxPopup">
                            <components>
                                <pagetitle uniqueid="sk-28I-Ia-378-activityCheckBoxPopup" model="EphemeralTaskModel">
                                    <actions>
                                        <action type="multi" label="{{$Label.ATS_CANCEL_BUTTON}}">
                                            <actions>
                                                <action type="cancel">
                                                    <models>
                                                        <model>EphemeralTaskModel</model>
                                                    </models>
                                                </action>
                                                <action type="emptyModelData">
                                                    <models>
                                                        <model>EphemeralTaskModel</model>
                                                    </models>
                                                </action>
                                                <action type="closeAllPopups"/>
                                            </actions>
                                        </action>
                                        <action type="multi" label="{{$Label.ATS_BUTTON_SAVE}}" uniqueid="saveEphemeralTaskModelButtonDomId">
                                            <actions>
                                                <action type="publish" event="onSaveCheckBoxPopupTaskValidateFields"/>
                                            </actions>
                                            <renderconditions logictype="and"/>
                                            <enableconditions/>
                                            <hotkeys/>
                                        </action>
                                    </actions>
                                </pagetitle>
                            </components>
                        </wrapper>
                    </components>
                </wrapper>
                <wrapper uniqueid="trashbin">
                    <components/>
                    <renderconditions logictype="and"/>
                </wrapper>
            </components>
        </popup>
    `;

}

export module event {

    export let addOrEditTemplate = `
        <popup title="{{$Label.ATS_BUTTON_ADD}} {{$Label.ATS_EVENT}}" width="90%">
            <components>
                <wrapper uniqueid="sk-3JTC4O-und1323" cssclass="">
                    <components>
                        <template multiple="false" uniqueid="sk-3JTETh-534536" allowhtml="true">
                        <contents>&lt;div class="slds-text-body--small"&gt;*{{$Label.ATS_REQUIRED_FIELDS_IN_RED}}&lt;/div&gt;</contents>
                        </template>
                    </components>
                    <styles>
                    <styleitem type="background"/>
                    <styleitem type="border"/>
                    <styleitem type="size"/>
                    </styles>
                </wrapper>
                <wrapper uniqueid="sk-2HHR9R-384">
                    <components>
                        <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralEventModel" buttonposition="" uniqueid="sk-2HHToo-392" mode="edit" layout="above">
                            <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                                <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                    <sections>
                                        <section title="Section A" collapsible="no" showheader="false">
                                            <fields>
                                                <field id="Type" required="true" valuehalign="" type="">
                                                    <label>{{$Label.ATS_TYPE}}</label>
                                                </field>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                                <column behavior="specified" verticalalign="top" width="100%">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <columns layoutmode="responsive" columngutter="2px" rowgutter="2px">
                                                    <column ratio="1" minwidth="300px" behavior="flex" verticalalign="top">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="Subject" valuehalign="" type="" required="true">
                                                                        <label>{{$Label.ATS_SUBJECT}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column behavior="flex" ratio="4" minwidth="300px" verticalalign="top">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields/>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                </columns>
                                            </fields>
                                        </section>
                                        <section title="Section B" collapsible="no" showheader="false">
                                            <fields/>
                                        </section>
                                    </sections>
                                </column>
                                <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <columns layoutmode="fixed">
                                                    <column width="400px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="StartDateTime" valuehalign="" monthpicker="true" yearpicker="true" yearpickermin="c-3" yearpickermax="c+3" type="" required="true">
                                                                        <label>{{$Label.ATS_START_DATE_TIME}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column width="400px">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="EndDateTime" valuehalign="" monthpicker="true" yearpicker="true" yearpickermin="c-3" yearpickermax="c+3" type="" required="true">
                                                                        <label>{{$Label.ATS_END_DATE_TIME}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                    </column>
                                                    <column width="33.3%">
                                                        <sections>
                                                            <section title="New Section" collapsible="no" showheader="false">
                                                                <fields>
                                                                    <field id="IsAllDayEvent" valuehalign="" type="">
                                                                        <label>{{$Label.ATS_ALL_DAY}}</label>
                                                                    </field>
                                                                </fields>
                                                            </section>
                                                        </sections>
                                                        <renderconditions logictype="and">
                                                            <rendercondition type="fieldvalue" operator="=" enclosevalueinquotes="true" fieldmodel="Event" sourcetype="param" sourceparam="enableAllDay" value="true"/>
                                                        </renderconditions>
                                                    </column>
                                                </columns>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                                <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <field id="Pre_Meeting_Notes__c" valuehalign="" type="" displayrows="3">
                                                    <label>{{$Label.ATS_PRE_MEETING}}</label>
                                                </field>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                                <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                    <sections>
                                        <section title="New Section" collapsible="no" showheader="false">
                                            <fields>
                                                <field id="Description" valuehalign="" type="" displayrows="3">
                                                    <label>{{$Label.ATS_POST_MEETING}}</label>
                                                </field>
                                            </fields>
                                        </section>
                                    </sections>
                                </column>
                            </columns>
                        </basicfieldeditor>
                    </components>
                    <styles>
                        <styleitem type="background"/>
                        <styleitem type="border"/>
                        <styleitem type="size"/>
                    </styles>
                </wrapper>
            </components>
        </popup>
    `;

    export let markCompleteTemplate = `
        <popup title="{{$Label.ACTIVITY_NOTES_ATS}}" width="90%">
            <components>
                <wrapper uniqueid="sk-2HHR9R-384-TaskCheckboxPopup">
                    <components>
                        <template multiple="false" uniqueid="sk-n_RrT-473-TaskCheckboxPopup">
                            <contents>{{$Label.ATS_LEAVE_COMMENTS_TO_COMPELETE_EVENT}}</contents>
                        </template>
                        <wrapper uniqueid="sk-n_G0g-456-TaskCheckboxPopup">
                            <components>
                                <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralEventModel" buttonposition="" uniqueid="sk-2HHToo-392-TaskCheckboxPopup" mode="edit" layout="above">
                                    <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                                        <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                            <sections>
                                                <section title="New Section" collapsible="no" showheader="false">
                                                    <fields>
                                                        <field id="Description" valuehalign="" type="" displayrows="4">
                                                            <label>{{$Label.ATS_POST_MEETING}}</label>
                                                        </field>
                                                    </fields>
                                                </section>
                                            </sections>
                                        </column>
                                    </columns>
                                </basicfieldeditor>
                                <pagetitle uniqueid="sk-2HKWXa-473-TaskCheckboxPopup" model="EphemeralEventModel">
                                    <actions>
                                        <action type="multi" label="{{$Label.ATS_CANCEL_BUTTON}}">
                                            <actions>
                                                <action type="cancel">
                                                    <models>
                                                        <model>EphemeralEventModel</model>
                                                    </models>
                                                </action>
                                                <action type="emptyModelData">
                                                    <models>
                                                        <model>EphemeralEventModel</model>
                                                    </models>
                                                </action>
                                                <action type="closeAllPopups"/>
                                            </actions>
                                        </action>
                                        <action type="multi" label="{{$Label.ATS_BUTTON_SAVE}}" uniqueid="saveEventButtonDomId">
                                            <actions>
                                                <action type="publish" event="onSaveCheckBoxPopupEventValidateFields"/>
                                            </actions>
                                        </action>
                                    </actions>
                                </pagetitle>
                            </components>
                        </wrapper>
                    </components>
                </wrapper>
                <wrapper uniqueid="trashbin">
                    <components/>
                    <renderconditions logictype="and"/>
                </wrapper>
            </components>
        </popup>
    `;

}
