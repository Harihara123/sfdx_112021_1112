import { repositoryAs } from "../repository";

// Common
import * as commonData from "../common/data";
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";
import * as commonValidation from "../common/ui/validation";

// Data
import { Country, countryOptById, State, stateOptById, willBeStates } from "../common/data/geodata";
import { CandidateContactModelRow, elementsFrom, selectorsAs } from "./data";

// Helpers
import { ajax, valueOf } from "../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as select2Helper from "../../helpers/select2-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

// Library
import * as array from "../../library/array";
import * as async from "../../library/async";
import * as core from "../../library/core";
import * as elements from "../../library/elements";
import { Event, watch } from "../../library/events";
import {
    asSomeOrElse,
    asSomeOrFail,
    bind,
    map as optMap,
    map3 as optMap3,
    of as optOf,
    Optional,
    withSomeOfBothOrFail,
    withSomeOrElse,
    withSomeOrFail
} from "../../library/optional";
import { alwaysEmptyString, emptyString, truncate, withSomeOrElse as textWithSomeOrElse } from "../../library/text";
import * as ui from "../../library/ui";
import * as utils from "../../library/utils";


let dialogs: any;

export function candidateContactFromOver(page: commonUI.page.Page) {
    return function candidateContactAs() {
        commonUI.page.viaSomePage(page, {
            caseOfLandingPage: caseOfLandingPage,
            caseOfDetailsPage: caseOfDetailsPage,
            caseOfNeither() { core.fail(`Unexpected case of view: ${page}`); }
        });
    };
}

function caseOfLandingPage() {
    /**
    * HACK: this should be handled in one of two ways:
    * -- (PREFERRED) injected and removed from the DOM on-demand.
    * -- hidden via CSS on page load, toggle visibility with JS.
    */
    $("#candidatecontactfulldetaildivID").hide();

    optMap(skuidModelHelpers.getModelOpt("TalentContact"), talentContactModel => {
        const mailingCityOpt = optOf(skuidModelHelpers.getFieldValueOfFirstRow<string>(
            talentContactModel,
            "MailingCity"
        ));
        const mailingStateCodeOpt = optOf(skuidModelHelpers.getFieldValueOfFirstRow<string>(
            talentContactModel,
            "MailingState"
        ));
        const mailingCountryCodeOpt = optOf(skuidModelHelpers.getFieldValueOfFirstRow<string>(
            talentContactModel,
            "MailingCountry"
        ));

        optMap(
            bind(mailingStateCodeOpt, mailingStateCode => willBeStates(
                skuidModelHelpers.getModelOpt("States"), truncate(mailingStateCode, 0, 2))
            ),
            willBeStatesModel => {
                willBeStatesModel.then(() => {
                    optMap3(
                        (country: Country, state: State, city: string) => {
                            elementsFrom(selectorsAs()).$talentContactLocation.text(
                                `${city}, ${state.stateName}, ${country.countryName}`
                            )
                        },
                        bind(mailingCountryCodeOpt, countryOptById),
                        bind(mailingStateCodeOpt, stateOptById),
                        mailingCityOpt
                    );
                });
            }
        );

        //(farid: i have comment out this code since the following methods needs to add addtional logic to include
        //phone and email in case if the preferrred selectoin is not selected , however i was able to
        //achive the same result with simple skuid  formula by using a UI Field so we will keep the methods for
        //future reference but only comment them out
        //setDisplayEmailValue(talentContactModel);
        //setDisplayPhoneValue(talentContactModel);
    });

    // Register DOM event handlers
    registerDOMHandlers();
}

function caseOfDetailsPage() {
    const candidateAccountModel = asSomeOrFail(
        skuidModelHelpers.getModelOpt("CandidateAccount"),
        emptyString
    );
    const candidateContactModel = asSomeOrFail(
        skuidModelHelpers.getModelOpt("CandidateContact"),
        emptyString
    );
    const countriesModel = asSomeOrFail(skuidModelHelpers.getModelOpt("Countries"), emptyString);

    // Hide the edit section first to hide model loading and resize of the section
    $('#addCandidateTitleUID').hide();

    //if the Id parameter is in the query param then it's a edit, need to load model
    //if not then just create new rows for add contact
    withSomeOrElse(
        utils.getQueryParamAtOpt("id"),

        // Edit existing candidate
        queryParam => {

            let accountId = utils.getQueryParamAtOrFail("id");

            $('#addEditInfoSectionId').hide();

            return withSomeOrFail(
                skuidModelHelpers.getConditionOpt(candidateContactModel, "AccountId", false),
                conditionTalent => {
                    candidateAccountModel.setCondition(
                        conditionTalent,
                        core.accessors.valueOf(accountId)
                    );
                    return skuidModelHelpers.updateData<skuid.model.Model>(candidateContactModel)
                    .then(result => {

                        withSomeOrFail(
                            skuidModelHelpers.getConditionOpt(candidateAccountModel, "Id", false),
                            condition => {
                                candidateContactModel.setCondition(
                                    condition,
                                    core.accessors.valueOf(accountId)
                                );
                                return skuidModelHelpers.updateData<skuid.model.Model>(candidateAccountModel)
                                .then(resultModelContact => {
                                    $('#addCandidateContactSettingsUID').show();
                                    $(".nx-basicfieldeditor-section-header").addClass('slds-text-heading--small');
                                    document.title = skuid.label.read("ATS_EDIT_CANDIDATE_FORM", "ADD/EDIT CANDIDATE");
                                    $('#editCandidateTitleUID').show();
                                    caseOfDetailsViewAfterModelLoaded(candidateAccountModel, candidateContactModel);
                                    initCountryAndStatePicklists(candidateContactModel, countriesModel);
                                })
                                .catch((error: string) => core.scheduleError(error));
                            },
                            emptyString
                        );
                    })
                    .catch((error: string) => core.scheduleError<skuid.model.Model>(error));
                },
                emptyString
            );
        },

        // Add new candidate
        none => {
            candidateContactModel.emptyData();
            candidateContactModel.createRow({});

            return getConctactTalenteUID('RecordTypeContact', 'Contact')
            .then(contactRecordTypeId => {

                document.title = skuid.label.read("ATS_CREATE_NEW_CANDIDATE_PROFILE", "ADD NEW CANDIDATE");
                $('#editCandidateTitleUID').hide();
                $('#addCandidateContactSettingsUID').hide();
                $('#addCandidateTitleUID').show();
                //add a class for section header (do not move it down otherwise the first class
                //the first class loads first showing default style first
                $(".nx-basicfieldeditor-section-header").addClass('slds-text-heading--small');

                let talentContactRow = candidateContactModel.getFirstRow();
                candidateContactModel.updateRow(talentContactRow, {'RecordTypeId': contactRecordTypeId});
                initFieldValuesFromDupeCheck(candidateContactModel, talentContactRow);

                candidateAccountModel.createRow({});
                getConctactTalenteUID('RecordTypeAccount', 'Account')
                .then(accountRecordTypeId => {
                    let talentAccountRow = candidateAccountModel.getFirstRow();
                    candidateAccountModel.updateRow(talentAccountRow, {'RecordTypeId': accountRecordTypeId});

                    caseOfDetailsViewAfterModelLoaded(candidateAccountModel, candidateContactModel);

                    initCountryAndStatePicklists(candidateContactModel, countriesModel);

                })
                .catch((error: string) => core.scheduleError(error));
            })
            .catch((error: string) => core.scheduleError(error));

        }
    );

    $('#addEditInfoSectionId').show();

    skuid.events.subscribe("ats.candidateContact.add.edit.cancel.redirect", onCancelAddEditCandidateRedirect);
    skuid.events.subscribe("ats.candidateContact.validate.add", onAddCandidateValidation);
    skuid.events.subscribe("ats.candidateContact.validate.edit", onEditCandidateValidation);
}

/*
 * Function populates data from the query parameters into the form fields.
 */
function initFieldValuesFromDupeCheck(contactModel: skuid.model.Model, therow) {
    optMap(utils.getQueryParamAtOpt("fname"),
        qParamFname => {contactModel.updateRow(therow, {'FirstName' : decodeURIComponent(qParamFname.value)})});
    optMap(utils.getQueryParamAtOpt("lname"),
        qParamLname => {contactModel.updateRow(therow, {'LastName' : decodeURIComponent(qParamLname.value)})});
    optMap(utils.getQueryParamAtOpt("phone"),
        qParamPhone => {contactModel.updateRow(therow, {'MobilePhone' : decodeURIComponent(qParamPhone.value)})});
    optMap(utils.getQueryParamAtOpt("email"),
        qParamEmail => {contactModel.updateRow(therow, {'Email' : decodeURIComponent(qParamEmail.value)})});
}

/*
  Validation checks (in order) for Create (Add) Candidate flow
*/
function onAddCandidateValidation() {
    onAddCandidateConcatValidatePrefOpt();
    onAllCandidateContactValidatei18nPhones();
}

/*
  Validation checks (in order) for Edit Candidate flow
*/
function onEditCandidateValidation() {
    onAddCandidateConcatValidatePrefOpt();
    if(onEditCandidateConcatCheckDNCFields()) {
        onAllCandidateContactValidatei18nPhones();
    }
}

function caseOfDetailsViewAfterModelLoaded(accountModel, talentContactModel) {

    var firstContact = talentContactModel.getFirstRow();
    //on edit candidate contact set preferred phone values if any
    let PreferredEmail__C = skuidModelHelpers.getFieldValueOfFirstRow(talentContactModel, "Preferred_Phone__c");
    if (PreferredEmail__C) {
        if (PreferredEmail__C === 'Home') {
            talentContactModel.updateRow(firstContact, {'PreferredHomePhone': true});
        } else if (PreferredEmail__C === "Mobile") {
            talentContactModel.updateRow(firstContact, {'PreferredMobilePhone': true});
        } else if (PreferredEmail__C === "Work") {
            talentContactModel.updateRow(firstContact, {'PreferredWorkPhone': true});
        }
    }

    //  //on edit candidate contact set preferred email values if any
    let Preferred_Email__c = skuidModelHelpers.getFieldValueOfFirstRow(talentContactModel, "Preferred_Email__c");
    if (Preferred_Email__c) {
        if (Preferred_Email__c === 'Email') {
            talentContactModel.updateRow(firstContact, {'PreferredEmail': true});
        } else if (Preferred_Email__c === "Alternate") {
            talentContactModel.updateRow(firstContact, {'PreferredEmailAlternative': true});
        }
    }


    // handles phone preferred options

    $('.homePhonePreferredClassId').on('change', 'input[type=checkbox]', e => {
        var preferredValue = '';
        withSomeOrFail(
            skuidModelHelpers.getModelOpt("CandidateContact"),
            candidateContactModel => {
                if (candidateContactModel.getFirstRow()["PreferredHomePhone"]) {
                    preferredValue =  'Home';
                }
            },
            emptyString
        );
        UnSelectPhonePreferredCheckBox('PreferredMobilePhone', 'PreferredWorkPhone', preferredValue);
    });

    $('.mobilePhonePreferredClassId').on('change', 'input[type=checkbox]', e => {
        var preferredValue = '';
        withSomeOrFail(
            skuidModelHelpers.getModelOpt("CandidateContact"),
            candidateContactModel => {
                if (candidateContactModel.getFirstRow()["PreferredMobilePhone"]) {
                    preferredValue =  'Mobile';
                }
            },
            emptyString
        );
        UnSelectPhonePreferredCheckBox('PreferredHomePhone', 'PreferredWorkPhone', preferredValue);
    });

    $('.workPhonePreferredClassId').on('change', 'input[type=checkbox]', e => {
        var preferredValue = '';
        withSomeOrFail(
            skuidModelHelpers.getModelOpt("CandidateContact"),
            candidateContactModel => {
                if (candidateContactModel.getFirstRow()["PreferredWorkPhone"]) {
                    preferredValue =  'Work';
                }
            },
            emptyString
        );
        UnSelectPhonePreferredCheckBox('PreferredHomePhone', 'PreferredMobilePhone', preferredValue);
    });

    //ends handling phone preferred options

    //handles email preferred options

    $('.emailPreferredClassId').on('change', 'input[type=checkbox]', e => {
        var preferredValue = '';
        withSomeOrFail(
            skuidModelHelpers.getModelOpt("CandidateContact"),
            candidateContactModel => {
                if (candidateContactModel.getFirstRow()["PreferredEmail"]) {
                    preferredValue =  'Email';
                }
            },
            emptyString
        );
        UnSelectEmailPreferredCheckBox('PreferredEmailAlternative', preferredValue);
    });

    $('.preferredEmailAlternativeClassId').on('change', 'input[type=checkbox]', e => {
        var preferredValue = '';
        withSomeOrFail(
            skuidModelHelpers.getModelOpt("CandidateContact"),
            candidateContactModel => {
                if (candidateContactModel.getFirstRow()["PreferredEmailAlternative"]) {
                    preferredValue =  'Alternate';
                }
            },
            emptyString
        );
        UnSelectEmailPreferredCheckBox('PreferredEmail', preferredValue);
    });

}

function UnSelectPhonePreferredCheckBox(unCheckbox1: string, unCheckbox2: string, preferredValue: string) {
    withSomeOrFail(
        skuidModelHelpers.getModelOpt("CandidateContact"),
        candidateContactModel => {
            let firstContact =  candidateContactModel.getFirstRow();
            candidateContactModel.updateRow(firstContact, {
                [unCheckbox1]: false,
                [unCheckbox2]: false,
                'Preferred_Phone__c': preferredValue
            });
        },
        emptyString
    );
}

function UnSelectEmailPreferredCheckBox(unCheckbox1, preferredValue) {
    withSomeOrFail(
        skuidModelHelpers.getModelOpt("CandidateContact"),
        candidateContactModel => {
            let firstContact =  candidateContactModel.getFirstRow();
            candidateContactModel.updateRow(firstContact, {
                [unCheckbox1]: false,
                'Preferred_Email__c': preferredValue
            });
        },
        emptyString
    );
}

function setDisplayEmailValue(model: skuid.model.Model): void {

    let email = {
        primary: skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "Email"),
        alternate: skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "Other_Email__c"),
        whichPreferred: skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "Preferred_Email__c")
    };

    let displayEmailValue = core.nullable.viaTwo(
        email.primary,
        email.alternate, {
            caseOfBoth(primary, alternate) {
                switch (email.whichPreferred) {
                    case "Email": return primary;
                    case "Alternate": return alternate;
                }
            },
            caseOfOne(primary) { return primary; },
            caseOfAnother(alternate) { return alternate; },
            caseOfNeither(reason) {
                return emptyString;
            }
        });

        $("#Display_Email").text(displayEmailValue);

}

function setDisplayPhoneValue(model: skuid.model.Model) {

    let phone = {
        home: skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "HomePhone"),
        mobile: skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "MobilePhone"),
        work: skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "Phone"),
        whichPreferred: skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "Preferred_Phone__c")
    };

    let displayPhoneValue = core.nullable.viaThree(
        phone.home, phone.mobile, phone.work, {
            caseOfAll(home, mobile, work) {
                switch (phone.whichPreferred) {
                    case "Home": return phone.home;
                    case "Mobile": return phone.mobile;
                    case "Work": return phone.work;
                }
            },
            caseOfFirst(home) { return home; },
            caseOfSecond(mobile) { return mobile; },
            caseOfThird(work) { return work; },
            caseOfFirstAndSecond(home, mobile) {
                switch (phone.whichPreferred) {
                    case "Home": return home;
                    case "Mobile": return mobile;
                }
            },
            caseOfFirstAndThird(home, work) {
                switch (phone.whichPreferred) {
                    case "Home": return home;
                    case "Work": return work;
                }
            },
            caseOfSecondAndThird(mobile, work) {
                switch (phone.whichPreferred) {
                    case "Mobile": return mobile;
                    case "Work": return work;
                }
            },
            caseOfNone(reason) {
                return emptyString;
            }
        });

        $("#Display_Phone").text(displayPhoneValue);
}

function toggleDetailsOver($toggleDetailsLink: JQuery): () => void {
    return function toggleDetails(): void {
        let viewLabel = skuidUIHelpers.labelFromOrFail("ATS_VIEW_DETAIL");
        let hideLabel = skuidUIHelpers.labelFromOrFail("ATS_HIDE_DETAIL");
        let toggleDetailsLinkText = $toggleDetailsLink.text();

        if (toggleDetailsLinkText === viewLabel) {
            $toggleDetailsLink.text(hideLabel);
        } else if (toggleDetailsLinkText === hideLabel) {
            $toggleDetailsLink.text(viewLabel);
        } else {
            core.fail(`Unexpected value of $toggleDetailsLink text: ${toggleDetailsLinkText}`);
        }

        $("#candidatecontactdivID, #candidatecontactfulldetaildivID").toggle();
    };
}

function registerDOMHandlers() {
    let $toggleDetailsLink = $("#sk-W8ijX-1499").find(".sk-navigation-item-label");

    $toggleDetailsLink.on("click", toggleDetailsOver($toggleDetailsLink));
}

function onAddCandidateConcatValidatePrefOpt() {

    const talentContactModel = asSomeOrFail(skuidModelHelpers.getModelOpt("CandidateContact"), emptyString);

    let phone = {
        homePhone : skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "HomePhone"),
        mobilePhone : skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "MobilePhone"),
        workPhone : skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "Phone"),
        requiredPreferred_PhoneFlag : skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "Preferred_Phone__c")
    }

    let email = {
        mail : skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "Email"),
        alternativeMail : skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "Other_Email__c"),
        requiredPreferred_EmailFlag : skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "Preferred_Email__c")
    }

    let mail = skuidModelHelpers.getFieldValueOfFirstRow(talentContactModel, "Email");
    let alternativeMail = skuidModelHelpers.getFieldValueOfFirstRow(talentContactModel, "Other_Email__c");

    let isPreferredPhoneSelected = false;
    let isPreferredEmailSelected = false;

    //validate phone set flag to true if any of the phone has a value + preferred selected
    if ( phone.homePhone  &&  phone.requiredPreferred_PhoneFlag === 'Home' ) {
        isPreferredPhoneSelected = true;
    } else  if ( phone.workPhone &&  phone.requiredPreferred_PhoneFlag === 'Work') {
        isPreferredPhoneSelected = true;
    } else  if ( phone.mobilePhone &&   phone.requiredPreferred_PhoneFlag === 'Mobile') {
        isPreferredPhoneSelected = true;
    }


    //validate email set flag to true if any of the phone has a value + preferred selected
    if (email.mail && email.requiredPreferred_EmailFlag === 'Email')  {
        isPreferredEmailSelected = true;
    } else if (email.alternativeMail  && email.requiredPreferred_EmailFlag === 'Alternate') {
        isPreferredEmailSelected = true;
    }

    let firstContact =  talentContactModel.getFirstRow();
    //in case one of the contact has selected preferred but has not enter any value need to remove the selectd option before saving the model
    if (!isPreferredPhoneSelected) {
        talentContactModel.updateRow(firstContact, {
            'Preferred_Phone__c': '',
            'PreferredMobilePhone': '',
            'PreferredWorkPhone': '',
            'PreferredHomePhone': ''
        });
    }

    if (!isPreferredEmailSelected) {
        talentContactModel.updateRow(firstContact, {
            'Preferred_Email__c': '',
            'PreferredEmail': '',
            'PreferredEmailAlternative': ''
        });
    }

    let streetAddress = skuidModelHelpers.getFieldValueOfFirstRow<string>(talentContactModel, "MailingStreet");
    if(streetAddress) {
        talentContactModel.updateRow(firstContact, {
            'MailingStreet': streetAddress.replace(/\r\n|\n|\r/g, ' ')
        });
    }

}

function onEditCandidateConcatCheckDNCFields() {

    const accountModel = asSomeOrFail(skuidModelHelpers.getModelOpt("CandidateAccount"), emptyString);
    const contactModel = asSomeOrFail(skuidModelHelpers.getModelOpt("CandidateContact"), emptyString);
    let doNotContact = skuidModelHelpers.getFieldValueOfFirstRow(accountModel, "Do_Not_Contact__c");
    let accountRow = accountModel.getFirstRow();

    // //If Do Not Contact is false, clear secondary fields.
    if (!doNotContact) {
        accountModel.updateRow(accountRow, {'Do_Not_Contact_Reason__c': '', 'Do_Not_Contact_Date__c': ''});
        //updateAndSave(accountModel, contactModel);
    }
    // //Make sure expiration date set is in the future
    let dncExpDate = skuidModelHelpers.getFieldValueOfFirstRow<string>(accountModel, "Do_Not_Contact_Date__c");
    if (dncExpDate !== null && dncExpDate !== undefined) {
        let jsToday = new Date();
        let jsDncExpDate = skuid.time.parseSFDate(dncExpDate);
        if (jsToday > jsDncExpDate) {
            skuidUIHelpers.displayEditorMessage(
                skuidUIHelpers.editorFromOrFail($("#editPageTitle")),
                "ATS_EXPIRATION_IN_PAST_ERROR",
                "ERROR"
            );
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}

/*
  Check all phone numbers are valid based on i18n formatter.
*/
function onAllCandidateContactValidatei18nPhones() {
  skuid.events.subscribe("ats.candidateContact.phone.validation.cancel", onCancelPhoneDialog);
  skuid.events.subscribe("ats.candidateContact.phone.validation.save", onSavePhoneDialog);

  const accountModel = asSomeOrFail(skuidModelHelpers.getModelOpt("CandidateAccount"), emptyString);
  const contactModel = asSomeOrFail(skuidModelHelpers.getModelOpt("CandidateContact"), emptyString);

  let invalidPhoneList = commonValidation.validatePhoneNumbers(commonValidation.contactPhoneFields, contactModel);

  if (invalidPhoneList.length > 0) {
    let template = commonUI.templates.confirmInvalidPhoneTemplate.replace("_replace_token_", invalidPhoneList);
    dialogs = {
      $confirmSave: jqueryUIHelper.dialog.from(
        template,
        [
          {
            text: commonUI.labels.generic.cancelButton,
            class: "slds-button slds-button--neutral",
            icons: { primary: "" },
            click: () => skuid.events.publish("ats.candidateContact.phone.validation.cancel"),
            showText: true
          },
          {
            text: commonUI.labels.generic.saveButton,
            class: "slds-button slds-button--brand",
            icons: { primary: "" },
            click: () => skuid.events.publish("ats.candidateContact.phone.validation.save"),
            showText: true
          }
        ],
        "",
        "",
        false
        )
    };
    jqueryUIHelper.dialog.open(dialogs.$confirmSave);
  } else {
    updateAndSave(accountModel, contactModel);
  }
}

function onCancelPhoneDialog() {
  jqueryUIHelper.dialog.close(dialogs.$confirmSave);
}

function onSavePhoneDialog() {
  const accountModel = asSomeOrFail(skuidModelHelpers.getModelOpt("CandidateAccount"), emptyString);
  const contactModel = asSomeOrFail(skuidModelHelpers.getModelOpt("CandidateContact"), emptyString);
  updateAndSave(accountModel, contactModel);
}

function updateAndSave(accountModel: skuid.model.Model, contactModel: skuid.model.Model) {
    const accountModelRow = accountModel.getFirstRow();
    const contactModelRow = contactModel.getFirstRow();

    accountModel.updateRow(
        accountModelRow,
        { "Name": `${contactModelRow["FirstName"]} ${contactModelRow["LastName"]}` }
    );

    updateAndSaveAll(accountModel, contactModel);
}

function getConctactTalenteUID(modelName, condValue): Promise<string> {

    const recordTypeModel = asSomeOrFail(skuidModelHelpers.getModelOpt(modelName), emptyString);

    return withSomeOfBothOrFail(
        skuidModelHelpers.getConditionOpt(recordTypeModel, 'SobjectType', false),
        skuidModelHelpers.getConditionOpt(recordTypeModel, 'Name', false),
        (sObjectTypeCondition, nameCondition) => {
            recordTypeModel.setCondition(
                sObjectTypeCondition,
                condValue
            );
            recordTypeModel.setCondition(
                nameCondition,
                'Talent'
            );
            return skuidModelHelpers.updateData(recordTypeModel)
            .then(result => skuidModelHelpers.getFieldValueOfFirstRow<string>(recordTypeModel, 'Id'))
            .catch((error: string) => core.scheduleError(error));
        },
        emptyString
    );

}

//implement the cancel method for add/edit account/talent contact
function onCancelAddEditCandidateRedirect() {

    //need to hide to prevent showing empty box when canceling as data model gets removed
    $("#parentWrapperId").hide();

    withSomeOrElse(
        utils.getQueryParamAtOpt("id"),
        queryParam => {
            window.location.replace(
                `${window.location.origin}/apex/c__CandidateSummary?id=${core.accessors.valueOf(
                    utils.getQueryParamAtOrFail("id")
                )}`
            );
        },
        none => {
            window.location.replace(`${window.location.origin}/apex/c__ATS_HOME_PAGE`);
        }
    );
}

function updateAndSaveAll(accountModel: skuid.model.Model, contactModel: skuid.model.Model) {

    $("#trashbin-editAccountContactId").empty();

    var editor = skuidUIHelpers.editorOf($("#trashbin-editAccountContactId"), false);
    editor.registerModel(contactModel);
    var editorId = editor.id();

    skuidModelHelpers.save(accountModel)
    .then(result => {
        skuidModelHelpers.save(contactModel, { initiatorId: editorId })
        .then(result => {
            skuidUIHelpers.removeErrors();
            let id = skuidModelHelpers.getFieldValueOfFirstRow(accountModel, "Id");
            window.location.replace(
                `${window.location.origin}/apex/c__CandidateSummary?id=` + id
            );
        })
    })

}

const initCountryAndStatePicklists = (candidateContactModel: skuid.model.Model, countriesModel: skuid.model.Model) => {
    // Render country and state picklists
    let $countryPicklist = ui.render(
        $(".candidate__contact--country-wrapper"),
        $("<select />", { class: "candidate__contact--country" })
    ).find(".candidate__contact--country");
    let $statePicklist = ui.render(
        $(".candidate__contact--state-wrapper"),
        $("<select />", { class: "candidate__contact--state" })
    ).find(".candidate__contact--state");

    watch($countryPicklist, Event.change, $countryPicklist => {
        // Update the stored value for country in our model
        skuidModelHelpers.updateRow<CandidateContactModelRow, { MailingCountry: string; }>(
            candidateContactModel,
            skuidModelHelpers.getFirstRow<CandidateContactModelRow>(candidateContactModel),
            { MailingCountry: asSomeOrElse(valueOf<string>($countryPicklist), alwaysEmptyString) }
        );

        // Populate the state/province dropdown
        populateStatePicklist(valueOf<string>($countryPicklist), $statePicklist, candidateContactModel);
    });

    watch($statePicklist, Event.change, $statePicklist => {
        // Update the stored value for state in our model
        skuidModelHelpers.updateRow<CandidateContactModelRow, { MailingState: string; }>(
            candidateContactModel,
            skuidModelHelpers.getFirstRow<CandidateContactModelRow>(candidateContactModel),
            { MailingState: asSomeOrElse(valueOf<string>($statePicklist), alwaysEmptyString) }
        );
    });

    // Enable select2 on the $countryPicklist jqElement and populate it with the countries model data
    // Working example here: https://jsfiddle.net/1zuja82v/
    $countryPicklist.select2({
        data: array.map(
            skuidModelHelpers.getRows<commonData.Country>(countriesModel),
            country => ({ id: country.countryCode, text: country.countryName })
        )
    });

    // Retrieve value of MailingCountry, or set the value to an empty string
    let mailingCountryCode: string = core.withKeyAt(
        "MailingCountry",
        (countryCode: string) => textWithSomeOrElse(
            countryCode,
            core.bounce,
            alwaysEmptyString
        ),
        alwaysEmptyString,
        skuidModelHelpers.getFirstRow<CandidateContactModelRow>(candidateContactModel)
    );

    // Set value of the $countryPicklist jqElement and trigger a change event
    $countryPicklist
        .val(mailingCountryCode)
        .trigger("change");
};

/**
 * TODO: Replace this function when the data has been migrated from Apigee to Salesforce
 */
const populateStatePicklist = (
    countryCodeOpt: Optional<string>,
    $statePicklist: JQuery,
    candidateContactModel: skuid.model.Model
): void => {
    bind(countryCodeOpt, countryCode => {
        return optMap(skuidModelHelpers.getModelOpt("States"), stModel => {
          // Set and enable URL merge condition
          optMap(
              skuidModelHelpers.getConditionOpt(stModel, "countryCode", false),
              countryCodeCondition => {
                  skuidModelHelpers.deactivateConditions(stModel, array.fromOne(countryCodeCondition));
                  stModel.setCondition(countryCodeCondition, countryCode);
                  skuidModelHelpers.activateConditions(stModel, array.fromOne(countryCodeCondition));

                  // Update the States skuid model
                  skuidModelHelpers.updateData(stModel)
                      .then(stModel => {
                          return array.map<commonData.State, IdTextPair>(
                            skuidModelHelpers.getRows<commonData.State>(stModel),
                            state => ({ id: state.stateCode, text: state.stateName })
                          );
                      })
                      .then(statesPairs => {
                        $statePicklist.empty();
                        $statePicklist.select2({ data: statesPairs });

                        // Check if MailingState has an existing value
                        let mailingStateCode: string = core.withKeyAt(
                            "MailingState",
                            (stateCode: string) => textWithSomeOrElse(
                                stateCode,
                                core.bounce,
                                alwaysEmptyString
                            ),
                            alwaysEmptyString,
                            candidateContactModel.getFirstRow()
                        );

                        $statePicklist
                            .val(mailingStateCode)
                            .trigger("change");
                      })
                      .catch((error: string) => core.scheduleError<IdTextPair[]>(error));
              }
          );



      /*const repository = repositoryAs()
      const geodata = repository.geodata<optional.Optional<commonData.State[]>>(countryCode)

      return geodata.states().then(statesOpt => {
          return optional.map(statesOpt, states => {
              return array.map<commonData.State, IdTextPair>(
                  core.keyAt<{ state?: commonData.State[] }, commonData.State[]>("state", states),
                  state => ({ id: state.stateCode, text: state.stateName }))
          })
      }).catch(error => core.scheduleError<optional.Optional<IdTextPair[]>>(error))*/
        })

    })
};
