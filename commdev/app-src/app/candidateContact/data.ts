export interface CandidateContactModelRow {
    MailingCountry: string;
    MailingState: string;
}

export interface Selectors {
    talentContactLocation: string;
}

export const selectorsAs = (): Selectors => ({
    talentContactLocation: ".talent-contact__location"
});

export interface Elements {
    $talentContactLocation: JQuery;
}

export const elementsFrom = (selectors: Selectors): Elements => ({
    $talentContactLocation: $(selectors.talentContactLocation)
});
