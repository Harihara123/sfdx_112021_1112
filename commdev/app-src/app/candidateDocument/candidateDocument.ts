// Library
import * as core from "../../library/core";
import * as utils from "../../library/utils";
import * as optional from "../../library/optional";
import * as ui from "../../library/ui";

// Common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

import * as text from "../../library/text";

// Data
import { Selectors, Elements, selectorsToElements } from "./data";

// Helpers
import * as jqueryHelper from "../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";


interface Events {
    model: commonModel.events.Events;
    ui: commonUI.events.Events;
}

interface Documents {
    events: Events;
    dialogs: commonUI.dialogs.Dialogs;
}

export const candidateDocumentFromOver = function(page: commonUI.page.Page) {
    return function candidateDocumentAs() {

        let modelRow = commonModel.row.rowAs();
        let documents = documentsAs();

        commonUI.page.viaSomePage(page, {
            caseOfLandingPage: core.ignore,
            caseOfDetailsPage: caseOfDetailPage,
            caseOfNeither(reason) { core.fail("reason"); }
        });

        // Register event subscriptions
        registerEventSubscriptions(documents, modelRow, documents.dialogs);
    };
};

function documentsAs() : Documents {
    const events: Events = {
        model: {
            wishCouldDelete: "ats.candidateDocuments.wishCouldDelete",
            wishCouldConfirmDelete: "ats.candidateDocuments.wishCouldConfirmDelete",
        },
        ui: {
            confirmDeleteDialogHasOpened: "ats.candidateDocuments.confirmDeleteDialogHasOpened",
            confirmDeleteDialogHasClosed: "ats.candidateDocuments.confirmDeleteDialogHasClosed"
        }
    };

    const dialogs: commonUI.dialogs.Dialogs = {
        $confirmDelete: jqueryUIHelper.dialog.from(
            commonUI.templates.confirmDeleteTemplate,
            [
                {
                    text: commonUI.labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldCancel),
                    showText: true
                },
                {
                    text: commonUI.labels.generic.deleteButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldConfirmDelete),
                    showText: true
                }
            ],
            events.ui.confirmDeleteDialogHasOpened,
            events.ui.confirmDeleteDialogHasClosed,
            false
        )
    };

    return { events, dialogs };
}

const caseOfDetailPage = function() {
    ui.setPageTitle(commonUI.labels.candidate.documents.title);
    $(".documents__view-document")
};

const registerEventSubscriptions = function(documents: Documents, modelRow: commonModel.row.Row, dialogs: commonUI.dialogs.Dialogs) {
    skuid.events.subscribe("ats.candidateDocuments.events.selectDefault", selectDefault);
    skuid.events.subscribe("ats.candidatedocument.show.hide.replace.resume", showHideReplaceResume);
    skuid.events.subscribe("ats.candidateDocuments.events.toggleReplaceSelection", toggleReplaceSelection);

    // Event handler for delete link click. Gets the id from the event data and opens the confirm dialog.
    skuid.events.subscribe(
        documents.events.model.wishCouldDelete,
        (data: { id: string }) => {
          modelRow.setRowId(data.id);
          commonModel.row.deleteRowOver(documents.dialogs.$confirmDelete)();
        }
    );

    skuid.events.subscribe(documents.events.model.wishCouldConfirmDelete, deleteDocOver(modelRow, dialogs));
    skuid.events.subscribe(documents.events.model.wishCouldCancel, cancelDeleteOver(dialogs));
}

/**
 * Function to toggle display of replace resume table.
 */
const showHideReplaceResume = function() {
    const newDocument = optional.asSomeOrFail(
        skuidModelHelpers.getModelOpt("NewDocument"),
        text.emptyString
    );

    let documentType = skuidModelHelpers.getFieldValueOfFirstRow(newDocument, "Document_Type__c");

    if (documentType === 'Resume') {
        $('#CandidateDocuments_replaceResumeTable').show();
        $('#CandidateDocument_replaceResumeMsg').show();
    } else {
        $('#CandidateDocuments_replaceResumeTable').hide();
        $('#CandidateDocument_replaceResumeMsg').hide();
    }
};

/**
 * Function to handle a default document change event.
 */
const selectDefault = function(id) {
    const tdModel = optional.asSomeOrFail(
        skuidModelHelpers.getModelOpt("TalentDocument"),
        text.emptyString
    );

    if (tdModel.getRows().length > 0) {
        // Toggle selected row default flag.
        var theRow = tdModel.getRowById(id);
        tdModel.updateRow(theRow, {'Default_Document__c': (!theRow['Default_Document__c'])});

        // If the flag was toggled to true, set same to false for all the other rows.
        if (theRow['Default_Document__c'] === true) {
            $.each(tdModel.getRows(), function (i, row) {
                if (row['Id'] != id) {
                    tdModel.updateRow(row, {'Default_Document__c' : false});
                }
            });
        }
    }
    tdModel.save();
}

/**
 * Function to handle a delete document event fired from the confirmation dialog.
 */
const deleteDocOver = (modelRow: commonModel.row.Row, dialogs: commonUI.dialogs.Dialogs) => {
  return function(data) {
      const tdModel = optional.asSomeOrFail(
          skuidModelHelpers.getModelOpt("TalentDocument"),
          text.emptyString
      );
      const replaceModel = optional.asSomeOrFail(
          skuidModelHelpers.getModelOpt("InternalResume"),
          text.emptyString
      );

      // Delete the selected row, save the model, and requery models and close dialog in callback.
      optional.withSomeOrFail(
        modelRow.getRowIdOpt(),
        rowId => {
          var theRow = tdModel.getRowById(rowId);
          skuidModelHelpers.updateRow(tdModel, theRow, {"Mark_For_Deletion__c": true});
          //tdModel.deleteRow(theRow);
          deleteAttachmentForDoc(theRow);
          tdModel.save({callback: function() {
              tdModel.updateData();
              replaceModel.updateData();
              jqueryUIHelper.dialog.close(dialogs.$confirmDelete);
          }});
        },
        text.emptyString
      );

  };
};

const deleteAttachmentForDoc = function(theRow) {
  const attModel = optional.asSomeOrFail(
      skuidModelHelpers.getModelOpt("Attachment"),
      text.emptyString
  );

  optional.map(
      skuidModelHelpers.getConditionOpt(attModel, "TalentDocId", false),
      talentDocCondition => {
          attModel.deactivateCondition(talentDocCondition);
          attModel.setCondition(talentDocCondition, theRow.Id);
          attModel.activateCondition(talentDocCondition);
          skuidModelHelpers.updateData(attModel).then(attModel => {
              var attRow = attModel.getFirstRow();
              attModel.deleteRow(attRow);
              attModel.save();
          });
      }
  );
}

/**
 * Function to handle a cancel from the delete confirmation dialog.
 */
const cancelDeleteOver = function(dialogs: commonUI.dialogs.Dialogs) {
  return function(data) {
      const tdModel = optional.asSomeOrFail(
          skuidModelHelpers.getModelOpt("TalentDocument"),
          text.emptyString
      );

      // Cancel model changes and close dialog.
      tdModel.cancel();
      tdModel.save();
      jqueryUIHelper.dialog.close($(dialogs.$confirmDelete));
  };
};

/**
 * Function to handle selection of a row on the replace resume table.
 * this is a placeholder for now.
 */
const toggleReplaceSelection = function(data) {
    console.log(data);
}
