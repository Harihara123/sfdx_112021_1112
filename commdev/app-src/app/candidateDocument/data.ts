export interface Selectors {
    viewDocument: string;
}

export interface Elements {
    $viewDocument: JQuery
}

export const selectorsToElements = function(selectors: Selectors): Elements {
    return {
        $viewDocument: $(selectors.viewDocument)
    };
};
