// Library
import * as array from "../../library/array";
import * as core from "../../library/core";
import * as optional from "../../library/optional";
import * as text from "../../library/text";
import * as ui from "../../library/ui";

// Common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

// Data
import * as employmentData from "./data";

// Helpers
import * as jqueryHelper from "../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

interface Elements {
    $timelineEditButton: JQuery;
    $timelineDeleteButton: JQuery;
    $modalCancelButton: JQuery;
    $modalCloseButton: JQuery;
    $modalSaveButton: JQuery;
}

interface Selectors {
    timelineEditButton: string;
    timelineDeleteButton: string;
    modalCancelButton: string;
    modalCloseButton: string;
    modalSaveButton: string;
}

interface Events {
    model: commonModel.events.Events;
    ui: commonUI.events.Events;
}

interface Employment {
    models: commonModel.Models;
    condition: skuid.model.Condition;
    events: Events;
    dialogs: commonUI.dialogs.Dialogs;
    selectors: Selectors;
}

export function candidateEmploymentFromOver(view: commonUI.page.Page) {
    return function candidateEmploymentAs() {

        let modelRow = commonModel.row.rowAs();
        let employment = employmentAs();
        let registerDomEvents = registerDomEventsOver(employment.selectors, employment.events, modelRow);

        // Consider the page view
        commonUI.page.viaSomePage(view, {
            caseOfLandingPage: core.ignore,
            caseOfDetailsPage() {
                // Set the page title
                ui.setPageTitle(commonUI.labels.candidate.employment.title);
            },
            caseOfNeither(reason) { core.fail(reason); }
        });

        // Register DOM events
        registerDomEvents();

        // Register event subscriptions
        registerEventSubscriptions(employment, modelRow);

    }
}

function employmentAs(): Employment {
    const models: commonModel.Models = {
        ephemeral: optional.asSomeOrFail(
            skuidModelHelpers.getModelOpt("EphemeralEmploymentModel"),
            `No model found with the name of EphemeralEmploymentModel!`
        ),
        persistent: optional.asSomeOrFail(
            skuidModelHelpers.getModelOpt("PersistentEmploymentModel"),
            `No model found with the name of PersistentEmploymentModel!`
        )
    };

    const condition = optional.withSomeOrFail(
        skuidModelHelpers.getConditionOpt(models.ephemeral, "Id", false),
        core.bounce,
        text.emptyString
    );

    const events: Events = {
        model: {
            wishCouldAdd: "ats.candidateEmployment.wishCouldAdd",
            wishCouldEdit: "ats.candidateEmployment.wishCouldEdit",
            wishCouldDelete: "ats.candidateEmployment.wishCouldDelete",
            wishCouldConfirmDelete: "ats.candidateEmployment.wishCouldConfirmDelete",
            wishCouldSave: "ats.candidateEmployment.wishCouldSave",
            wishCouldCancel: "ats.candidateEmployment.wishCouldCancel",
            hasUpdated: "ats.candidateEmployment.modelHasUpdated"
        },
        ui: {
            addOrEditDialogHasOpened: "ats.candidateEmployment.addOrEditDialogHasOpened",
            addOrEditDialogHasClosed: "ats.candidateEmployment.addOrEditDialogHasClosed",
            confirmDeleteDialogHasOpened: "ats.candidateEmployment.confirmDeleteDialogHasOpened",
            confirmDeleteDialogHasClosed: "ats.candidateEmployment.confirmDeleteDialogHasClosed"
        }
    };

    const addOrEditTemplate =
        skuidUIHelpers.popup.from(commonUI.labels.candidate.employment.addEmployment, "90%") +
        employmentData.addOrEditTemplate +
        skuidUIHelpers.popup.close();

    const dialogs: commonUI.dialogs.Dialogs = {
        $addOrEdit: jqueryUIHelper.dialog.from(
            addOrEditTemplate,
            [
                {
                    text: commonUI.labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldCancel),
                    showText: true
                },
                {
                    text: commonUI.labels.generic.saveButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldSave),
                    showText: true
                }
            ],
            events.ui.addOrEditDialogHasOpened,
            events.ui.addOrEditDialogHasClosed,
            false
        ),
        $confirmDelete: jqueryUIHelper.dialog.from(
            commonUI.templates.confirmDeleteTemplate,
            [
                {
                    text: commonUI.labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldCancel),
                    showText: true
                },
                {
                    text: commonUI.labels.generic.deleteButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldConfirmDelete),
                    showText: true
                }
            ],
            events.ui.confirmDeleteDialogHasOpened,
            events.ui.confirmDeleteDialogHasClosed,
            false
        )
    };

    const selectors: Selectors = {
        timelineEditButton: ".timeline_editEmploymentButton",
        timelineDeleteButton: ".timeline_deleteEmploymentButton",
        modalCancelButton: "#ats_candidateEmployment-modalCancelButton",
        modalCloseButton: "#ats_candidateEmployment-modalCloseButton",
        modalSaveButton: "#ats_candidateEmployment-modalSaveButton"
    };

    return { models, condition, events, dialogs, selectors };
}

function elementsFrom(selectors: Selectors): Elements {
    return {
        $timelineEditButton: $(selectors.timelineEditButton),
        $timelineDeleteButton: $(selectors.timelineDeleteButton),
        $modalCancelButton: $(selectors.modalCancelButton),
        $modalCloseButton: $(selectors.modalCloseButton),
        $modalSaveButton: $(selectors.modalSaveButton)
    };
}

function registerDomEventsOver(selectors: Selectors, events: Events, employmentRow: commonModel.row.Row) {
    return function registerDomEvents() {
        let elements = elementsFrom(selectors);

        elements.$timelineEditButton.off("click").on(
            "click",
            (event) => {
                employmentRow.setRowId($(event.target).attr("data-rowId"));
                skuid.events.publish(events.model.wishCouldEdit);
            }
        );

        elements.$timelineDeleteButton.off("click").on(
            "click",
            () => {
                employmentRow.setRowId($(event.target).attr("data-rowId"));
                skuid.events.publish(events.model.wishCouldDelete);
            }
        );
    }
}

function registerEventSubscriptions(employment: Employment, modelRow: commonModel.row.Row): void {
    skuid.events.subscribe(
        employment.events.model.wishCouldAdd,
        commonModel.row.addRowOver(
            employment.models.ephemeral,
            employment.condition,
            employment.dialogs.$addOrEdit,
            commonUI.labels.candidate.employment.addEmployment
        )
    );

    skuid.events.subscribe(
        employment.events.model.wishCouldEdit,
        commonModel.row.editRowOver(
            employment.models.ephemeral,
            employment.condition,
            modelRow,
            employment.dialogs.$addOrEdit,
            commonUI.labels.candidate.employment.editEmployment,
            text.emptyString
        )
    );

    skuid.events.subscribe(
        employment.events.model.wishCouldDelete,
        commonModel.row.deleteRowOver(employment.dialogs.$confirmDelete)
    );

    skuid.events.subscribe(
        employment.events.model.wishCouldConfirmDelete,
        commonModel.row.confirmDeleteRowOver(
            modelRow,
            employment.models.persistent,
            employment.dialogs.$confirmDelete,
            employment.events.model.hasUpdated
        )
    );

    skuid.events.subscribe(
        employment.events.model.wishCouldSave,
        commonModel.saveChangesOver(employment.models, employment.events.model.hasUpdated, employment.dialogs.$addOrEdit)
    );

    skuid.events.subscribe(
        employment.events.model.wishCouldCancel,
        commonModel.cancelChangesOver(employment.models.ephemeral)
    );

    skuid.events.subscribe(
        employment.events.model.hasUpdated,
        registerDomEventsOver(employment.selectors, employment.events, modelRow)
    );


    skuid.events.subscribe(employment.events.ui.confirmDeleteDialogHasClosed, registerDomEventsOver(
        employment.selectors,
        employment.events,
        modelRow
    ));


    // skuid.events.subscribe("models.saved", (data: employmentData.ModelSavedDataEmployment) => {
    //     optional.withSomeOfEitherOrElse(
    //         optional.of(data.models.PersistentEmploymentModel),
    //         optional.of(data.models.EphemeralEmploymentModel),
    //         persistentEmploymentModel => {
    //             console.log("persistentEmploymentModel model updated");
    //             //kuid.events.publish(employment.events.model.hasUpdated);
    //         },
    //         ephemeralEmploymentModel => {
    //             console.log("ephemeralEmploymentModel model updated");
    //             // skuid.events.publish(employment.events.model.hasUpdated);
    //          },
    //         core.ignore
    //     );
    // });



}
