import * as commonUI from "../common/ui";

export let addOrEditTemplate = `
        <components>
        <wrapper uniqueid="sk-3JTC4O-588" cssclass="">
            <components>
                <template multiple="false" uniqueid="sk-3JTETh-uni2342" allowhtml="true">
                <contents>&lt;div class="slds-text-body--small"&gt;*{{$Label.ATS_REQUIRED_FIELDS_IN_RED}}&lt;/div&gt;</contents>
                </template>
                </components>
                <styles>
                <styleitem type="background"/>
                <styleitem type="border"/>
                <styleitem type="size"/>
                </styles>
            </wrapper>
            <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralEmploymentModel" buttonposition="left" uniqueid="sk-2MPhIK-1332" mode="edit" layout="above">
                <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">

                    <column verticalalign="top" ratio="1" minwidth="200px" behavior="flex">
                        <sections>

                            <section title="Company Name" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Organization_Name__c" valuehalign="" type="" required="true" cssclass="">
                                        <label>{{$Label.ATS_COMPANY_NAME}}</label>
                                    </field>
                                </fields>
                            </section>

                            <section title="Date Range" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Start_Date__c" valuehalign="" type="" yearpicker="true" yearpickermin="c-50" yearpickermax="c+1" monthpicker="true" cssclass="form__date-field--small">
                                        <label>{{$Label.ATS_START_DATE}}</label>
                                    </field>
                                    <field id="End_Date__c" valuehalign="" type="" yearpicker="true" yearpickermin="c-50" yearpickermax="c+1" monthpicker="true" cssclass="form__date-field--small">
                                        <label>{{$Label.ATS_END_DATE}}</label>
                                    </field>
                                </fields>
                            </section>

                            <section title="Salary_Bill Rate" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Salary__c" decimalplaces="" valuehalign="" type="">
                                        <label>{{$Label.ATS_SALARY}}</label>
                                    </field>
                                </fields>
                            </section>

                            <section title="Bonus Percentage" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Bonus_Percentage__c" decimalplaces="" valuehalign="" type="">
                                        <label>{{$Label.ATS_BONUS_PERCENTAGE}}</label>
                                    </field>
                                </fields>
                            </section>

                        </sections>
                    </column>

                    <column verticalalign="top" ratio="1" minwidth="200px" behavior="flex">
                        <sections>

                            <section title="Title" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Title__c" valuehalign="" type="" required="true">
                                        <label>{{$Label.ATS_TITLE}}</label>
                                    </field>
                                </fields>
                            </section>

                            <section title="Reason for Leaving" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Reason_For_Leaving__c" valuehalign="" type="">
                                        <label>{{$Label.ATS_REASON_FOR_LEAVING}}</label>
                                    </field>
                                </fields>
                            </section>

                            <section title="Pay Rate" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Payrate__c" decimalplaces="" valuehalign="" type="">
                                        <label>{{$Label.ATS_PAY_RATE}}</label>
                                    </field>
                                </fields>
                            </section>

                            <section title="Additional Compensation" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Other_Compensation__c" decimalplaces="" valuehalign="" type="">
                                        <label>{{$Label.ATS_ADDL_COMPENSATION}}</label>
                                    </field>
                                </fields>
                            </section>

                        </sections>
                    </column>

                    <column verticalalign="top" ratio="1" minwidth="100px" behavior="flex">
                        <sections>

                            <section title="Department" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Department__c" valuehalign="" type="">
                                        <label>{{$Label.ATS_DEPARTMENT}}</label>
                                    </field>
                                </fields>
                            </section>

                            <section title="Placeholder" collapsible="no" showheader="false"  cssclass="form__input-field--empty"></section>

                            <section title="Bonus" collapsible="no" showheader="false">
                                <fields>
                                    <field id="Bonus__c" valuehalign="" type="">
                                        <label>${commonUI.labels.candidate.employment.bonus}</label>
                                    </field>
                                </fields>
                            </section>

                        </sections>
                    </column>
                    <column behavior="flex" verticalalign="top" ratio="1" minwidth="100%">
                         <sections>
                            <section title="Notes" collapsible="no" showheader="false">
                               <fields>
                                  <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                                     <column ratio="1" minwidth="100%" behavior="flex" verticalalign="top">
                                        <sections>
                                           <section title="Notes" collapsible="no" showheader="false">
                                              <fields>
                                                 <field id="Notes__c" valuehalign="" type="" displayrows="5">
                                                     <label>{{$Label.ATS_NOTES}}</label>
                                                 </field>
                                              </fields>
                                           </section>
                                        </sections>
                                        <renderconditions logictype="and"/>
                                     </column>
                                  </columns>
                               </fields>
                            </section>
                         </sections>
                      </column>
                </columns>
            </basicfieldeditor>
        </components>
`;

export let timelineJSON = {
   "line1":{
      "textline":"<div span class='employmentDuration'>{{{duration}}}</span>",
      "fieldproperty":{
         "field1":{
            "fieldname":"duration",
            "isrequired":"false"
         }
      }
   },
   "line2":{
      "textline":"<div class='itemList organizationName'>{{{Organization_Name__c}}}</div>",
      "fieldproperty":{
         "field1":{
            "fieldname":"Organization_Name__c",
            "isrequired":"false"
         }
      }
   },
   "line3":{
      "textline":"<div class='titleLink itemList'>{{{Title__c}}} {{#Department__c}} <span class='utility__pipe'>|</span> {{{Department__c}}} {{/Department__c}} {{#Salary__c}} <span class='utility__pipe'>|</span> {{Salary__c}} {{/Salary__c}} {{#Payrate__c}} <span class='utility__pipe'>|</span> {{Payrate__c}} {{/Payrate__c}} {{#Bonus__c}} <span class='utility__pipe'>|</span> {{Bonus__c}} {{/Bonus__c}}</div>",
      "fieldproperty":{
         "field1":{
            "fieldname":"Title__c",
            "isrequired":"false"
         }
      }
   },
   "line4":{
      "textline":"<div class='titleLink itemList'>{{{Reason_For_Leaving__c}}}</div>",
      "fieldproperty":{
         "field1":{
            "fieldname":"Reason_For_Leaving__c",
            "isrequired":"false"
         }
      }
   },
   "addinfobutton":{
      "classname":"timeline_editEmploymentButton slds-button slds-button--brand slds-button--small",
      "attributeName":"data-rowid",
      "attributeValue":"{{Id}}",
      "labelname":"add info"
   },
   "editbutton":{
      "classname":"timeline_editEmploymentButton slds-button slds-button--brand slds-button--small",
      "attributeName":"data-rowid",
      "attributeValue":"{{Id}}",
      "labelname":"{{$Label.ATS_EDIT}}"
   },
   "deletebutton":{
      "classname":"timeline_deleteEmploymentButton slds-button slds-button--neutral slds-button--small",
      "attributeName":"data-rowid",
      "attributeValue":"{{Id}}",
      "labelname":"{{$Label.ATS_DELETE_BUTTON}}"
   }
};


export interface ModelSavedDataEmployment {
    models: {
        PersistentEmploymentModel?: skuid.model.Model;
        EphemeralEmploymentModel?: skuid.model.Model;
    };
}
