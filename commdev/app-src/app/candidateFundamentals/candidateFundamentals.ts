// Common
import { Page, viaSomePage } from "../common/ui/page";
import { stateOptByName, willBeStates } from "../common/data/geodata";

// Data
import {
    Action,
    initModel,
    Model,
    modelFrom,
    Options,
    SkuidModel,
    viaSomeViewMode,
    ViewMode
} from "./common/data/internalData";
import { Preferences } from "./common/data/externalData";
import { elementsFrom as viewElementsFrom, selectorsAs as viewSelectorsAs } from "./view/data";
import { elementsFrom as editElementsFrom, selectorsAs as editSelectorsAs } from "./edit/data";

// Factories
import { dispatchFactory, renderFactory, skuidModelFactory } from "./common/factories";
import { renderFactory as renderFactoryView } from "./view/factories";
import {
    dispatchFactory as dispatchFactoryEdit,
    updateFactory as updateFactoryEdit,
    renderFactory as renderFactoryEdit
} from "./edit/factories";

// Helpers
import * as skuidModelHelpers from "../../helpers/skuid/model"

// Library
import { fromOne } from "../../library/array";
import { hasKey, ignore, scheduleError, withKeyAt } from "../../library/core";
import { asSomeOrFail, bind, of as optOf, map as optMap, Optional, withSomeOrElse } from "../../library/optional";
import { emptyString } from "../../library/text";
import { setPageTitle } from "../../library/ui";

// Parts
import { actionFrom as editActionFrom, caseOfEditMode } from "./edit/edit";
import { actionFrom as viewActionFrom, caseOfViewMode } from "./view/view";


export const candidateFundamentalsFromOver = (page: Page) => {
    return () => {
        skuid.events.subscribe("ats.component.htmlRender.hasRendered", () => {

            // MODEL //
            const skuidModel = skuidModelFactory();
            const model = initModel({skuidModel});
            const _ = emptyString;

            const dispatch = dispatchFactory(_);
            const options: Options = {
                common: {
                    skuidModel: skuidModel,
                    model: model,
                    dispatch: dispatchFactory(_)
                },
                view: {
                    elements: viewElementsFrom(viewSelectorsAs()),
                    render: renderFactoryView()
                },
                edit: {
                    elements: editElementsFrom(editSelectorsAs()),
                    render: renderFactoryEdit(editElementsFrom(editSelectorsAs())),
                    dispatch: dispatchFactoryEdit(
                        updateFactoryEdit(),
                        editElementsFrom(editSelectorsAs()),
                        renderFactoryEdit(editElementsFrom(editSelectorsAs()))
                    )
                }
            };

            withSomeOrElse(
                bind(
                    model.geographicPreferences.desiredLocation.country,
                    country => willBeStates(skuidModel.states, country.countryCode)
                ),
                willBeStatesModel => {
                    willBeStatesModel
                        .then(() => optMap(
                            model.geographicPreferences.desiredLocation.state, state => {
                                if (state.stateCode === emptyString) {
                                    model.geographicPreferences.desiredLocation.state = stateOptByName(state.stateName);
                                }

                            })
                        )
                        .then(() => dispatch(Action.WishCouldView, options))
                        .then(() => viaSomePage(page, {
                            caseOfLandingPage: () => {
                                $(".summary__heading").hide();
                                $(".employability-information", ".summary").hide();
                                $(".employment-prefs", ".summary").hide();
                                $(".button.button--edit", ".summary").hide();
                            },
                            caseOfDetailsPage: () => caseOfDetailsView(options),
                            caseOfNeither: ignore
                        }))
                        .catch((error: string) => scheduleError(error))
                },
                () => {
                    dispatch(Action.WishCouldView, options);
                    viaSomePage(page, {
                        caseOfLandingPage: () => {
                            $(".summary__heading").hide();
                            $(".employability-information", ".summary").hide();
                            $(".employment-prefs", ".summary").hide();
                            $(".button.button--edit", ".summary").hide();
                        },
                        caseOfDetailsPage: () => caseOfDetailsView(options),
                        caseOfNeither: ignore
                    });
                }
            );
        });
    }
};

const caseOfDetailsView = (options: Options) => {
    // Set the page title
    setPageTitle("Candidate Fundamentals");

    // UPDATE //
    editActionFrom(options);
    viewActionFrom(options);
};
