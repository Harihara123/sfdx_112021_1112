// Data
import { Model, LastModified, lastModifiedFrom } from "./internalData";

// Library
import * as array from "../../../../library/array";
import { booleanFrom } from "../../../../library/core";
import { asSomeOrElse, asSomeOrThat, withSomeOrThat } from "../../../../library/optional";
import { emptyString } from "../../../../library/text";

interface EmployabilityInformation {
    reliableTransportation: boolean;
    eligibleIn: string[],
    drugTest: boolean;
    backgroundCheck: boolean;
    securityClearance: boolean;
}

interface Qualifications {
    skills: string[];
    languages: string[];
}

module GeographicPreferences {
    export interface DesiredLocation {
        city: string;
        state: string;
        country: string;
    }

    export interface CommuteLength {
        distance: string;
        unit: string;
    }
}

interface GeographicPreferences {
    willingToRelocate: boolean;
    desiredLocation: GeographicPreferences.DesiredLocation;
    commuteLength: GeographicPreferences.CommuteLength;
    nationalOpportunities: boolean;
}

module EmploymentPreferences {
    export interface Rate {
        amount: string;
        rate: string;
    }
}

interface EmploymentPreferences {
    goalsAndInterests: string[];
    desiredRate: EmploymentPreferences.Rate;
    desiredPlacementType: string;
    desiredSchedule: string;
    mostRecentRate: EmploymentPreferences.Rate;
}

export interface Preferences {
    _lastModified: LastModified;
    candidateOverview: string;
    employabilityInformation: EmployabilityInformation;
    qualifications: Qualifications;
    geographicPrefs: GeographicPreferences;
    employmentPrefs: EmploymentPreferences;
}

const employabilityInformationFrom = (model: Model): EmployabilityInformation => ({
    reliableTransportation: asSomeOrThat(model.employabilityInformation.reliableTransportation, null),
    eligibleIn: model.employabilityInformation.eligibleIn,
    drugTest: asSomeOrThat(model.employabilityInformation.drugTest, null),
    backgroundCheck: asSomeOrThat(model.employabilityInformation.backgroundCheck, null),
    securityClearance: asSomeOrThat(model.employabilityInformation.securityClearance, null)
});

const qualificationsFrom = (model: Model): Qualifications => ({
    skills: model.qualifications.skills,
    languages: model.qualifications.languages
});

const desiredLocationFrom = (model: Model): GeographicPreferences.DesiredLocation => ({
    city: asSomeOrThat(model.geographicPreferences.desiredLocation.city, null),
    state: withSomeOrThat(model.geographicPreferences.desiredLocation.state, state => state.stateName, null),
    country: withSomeOrThat(
        model.geographicPreferences.desiredLocation.country, country => country.countryName, null)
});

const commuteLengthFrom = (model: Model): GeographicPreferences.CommuteLength => ({
    distance: asSomeOrThat(model.geographicPreferences.commuteLength.distance, null),
    unit: asSomeOrThat(model.geographicPreferences.commuteLength.unit, null)
});

const geographicPrefsFrom = (model: Model): GeographicPreferences => ({
    willingToRelocate: asSomeOrThat(model.geographicPreferences.willingToRelocate, null),
    desiredLocation: desiredLocationFrom(model),
    commuteLength: commuteLengthFrom(model),
    nationalOpportunities: asSomeOrThat(model.geographicPreferences.nationalOpportunities, null)
});

const rateFrom = (model: Model): EmploymentPreferences.Rate => ({
    amount: asSomeOrThat(model.employmentPreferences.desiredRate.amount, null),
    rate: asSomeOrThat(model.employmentPreferences.desiredRate.rate, null)
});

const employmentPrefsFrom = (model: Model): EmploymentPreferences => ({
    goalsAndInterests: model.employmentPreferences.goalsAndInterests,
    desiredRate: rateFrom(model),
    desiredPlacementType: asSomeOrThat(model.employmentPreferences.desiredPlacementType, null),
    desiredSchedule: asSomeOrThat(model.employmentPreferences.desiredSchedule, null),
    mostRecentRate: rateFrom(model)
});

export const preferencesFrom = (model: Model): Preferences => ({
    _lastModified: asSomeOrElse(model.lastModified, lastModifiedFrom),
    candidateOverview: asSomeOrThat(model.candidateOverview, null),
    employabilityInformation: employabilityInformationFrom(model),
    qualifications: qualificationsFrom(model),
    geographicPrefs: geographicPrefsFrom(model),
    employmentPrefs: employmentPrefsFrom(model)
});
