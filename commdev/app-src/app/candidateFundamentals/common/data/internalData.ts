// Data
import { City, Country, countryOptByName, State, stateOptByName } from "../../../common/data/geodata";
import { Preferences } from "./externalData";
import { Elements as ViewElements, Render as ViewRender } from "../../view/data";
import { Action as EditAction, Elements as EditElements, Render as EditRender } from "../../edit/data";

// Helpers
import * as skuidModelHelpers from "../../../../helpers/skuid/model";

// Library
import { filter, safeHeadOf, of as arrayOf } from "../../../../library/array";
import { bounce, hasKey, outOfBoolean } from "../../../../library/core";
import { now } from "../../../../library/date";
import { asSomeOrThat, bind, fromNoneOf, fromSome, map as optMap, of as optOf, Optional } from "../../../../library/optional";
import { emptyString } from "../../../../library/text";


export enum ViewMode { View, Edit }

export interface ViaSomeViewMode<r> {
    caseOfView: () => r;
    caseOfEdit: () => r;
}

export const viaSomeViewMode = <r>(viewMode: ViewMode, via: ViaSomeViewMode<r>): r => {
    switch(viewMode) {
        case ViewMode.View: return via.caseOfView();
        case ViewMode.Edit: return via.caseOfEdit();
    }
};

export interface LastModified {
    by: { userId: string; name: string; };
    date: Date;
}

export const lastModifiedFrom = (): LastModified => ({
    by: { userId: skuid.utils.userInfo.userId, name: skuid.utils.userInfo.name },
    date: now()
});

interface EmployabilityInformation {
    reliableTransportation: Optional<boolean>;
    eligibleIn: string[];
    drugTest: Optional<boolean>;
    backgroundCheck: Optional<boolean>;
    securityClearance: Optional<boolean>;
}

const initEmployabilityInformation = (): EmployabilityInformation => ({
    reliableTransportation: fromNoneOf<boolean>("Reliable transportation has not yet been set."),
    eligibleIn: arrayOf<string>(),
    drugTest: fromNoneOf<boolean>("Drug test has not yet been set."),
    backgroundCheck: fromNoneOf<boolean>("Background check has not yet been set."),
    securityClearance: fromNoneOf<boolean>("Security clearance has not yet been set.")
});

interface Qualifications {
    skills: string[];
    languages: string[];
}

const initQualifications = (): Qualifications => ({
    skills: arrayOf<string>(),
    languages: arrayOf<string>()
});

module GeographicPreferences {
    export interface DesiredLocation {
        country: Optional<Country>;
        state: Optional<State>;
        city: Optional<string>;
    }

    export const initDesiredLocation = (): DesiredLocation => ({
        country: fromNoneOf<Country>("Country has not yet been selected."),
        state: fromNoneOf<State>("State has not yet been selected."),
        city: fromNoneOf<string>("City has not yet been selected.")
    });

    export interface CommuteLength {
        distance: Optional<string>;
        unit: Optional<string>;
    }

    export const initCommuteLength = (): CommuteLength => ({
        distance: fromNoneOf<string>("Distance has not yet been set."),
        unit: fromNoneOf<string>("Unit has not yet been set.")
    });
}

interface GeographicPreferences {
    willingToRelocate: Optional<boolean>;
    desiredLocation: GeographicPreferences.DesiredLocation;
    commuteLength: GeographicPreferences.CommuteLength;
    nationalOpportunities: Optional<boolean>;
}

const initGeographicPreferences = (): GeographicPreferences => ({
    willingToRelocate: fromNoneOf<boolean>("Willing to relocate has not yet been set."),
    desiredLocation: GeographicPreferences.initDesiredLocation(),
    commuteLength: GeographicPreferences.initCommuteLength(),
    nationalOpportunities: fromNoneOf<boolean>("National opportunities has not yet been set.")
});

module EmploymentPreferences {
    export interface Rate {
        amount: Optional<string>;
        rate: Optional<string>;
    }

    export const initRate = (): Rate => ({
        amount: fromNoneOf<string>("Amount has not yet been set."),
        rate: fromNoneOf<string>("Rate has not yet been set.")
    });
}

interface EmploymentPreferences {
    goalsAndInterests: string[];
    desiredRate: EmploymentPreferences.Rate;
    desiredPlacementType: Optional<string>;
    desiredSchedule: Optional<string>;
    mostRecentRate: EmploymentPreferences.Rate;
}

const initEmploymentPreferences = (): EmploymentPreferences => ({
    goalsAndInterests: arrayOf<string>(),
    desiredRate: EmploymentPreferences.initRate(),
    desiredPlacementType: fromNoneOf<string>("Desired placement type has not yet been set."),
    desiredSchedule: fromNoneOf<string>("Desired schedule has not yet been set."),
    mostRecentRate: EmploymentPreferences.initRate()
});

export interface Model {
    lastModified: Optional<LastModified>;
    candidateOverview: Optional<string>;
    employabilityInformation: EmployabilityInformation;
    qualifications: Qualifications;
    geographicPreferences: GeographicPreferences;
    employmentPreferences: EmploymentPreferences;
}

interface InitModelOptions { skuidModel: SkuidModel }
type InitModel = (options: InitModelOptions) => Model;
type TalentRow = { Talent_Preference_Internal__c: string };
export const initModel: InitModel = ({skuidModel}): Model => {
    // Initialize empty model
    let model = {
        lastModified: fromNoneOf<LastModified>("Last modified has not yet been set."),
        candidateOverview: fromNoneOf<string>("Candidate overview has not yet been set."),
        employabilityInformation: initEmployabilityInformation(),
        qualifications: initQualifications(),
        geographicPreferences: initGeographicPreferences(),
        employmentPreferences: initEmploymentPreferences()
    };
    // Optionally fill model with data from Talent_Preference_Internal__c
    optMap(skuidModel.talentAccount, talentAccountModel => {
        skuidModelHelpers.withFirstRow((row: TalentRow) => {
            optMap(optOf(row.Talent_Preference_Internal__c), preferencesString => {
                const preferences = JSON.parse(preferencesString);
                if (hasKey("candidateOverview", preferences)) model = modelFrom(skuidModel, preferences);
            });
        }, talentAccountModel);
    });
    // Return model
    return model;
};

export const modelFrom = (skuidModel: SkuidModel, preferences: Preferences): Model => ({
    lastModified: optOf<LastModified>(preferences._lastModified),
    candidateOverview: optOf<string>(preferences.candidateOverview),
    employabilityInformation: {
        reliableTransportation: optOf<boolean>(preferences.employabilityInformation.reliableTransportation),
        eligibleIn: preferences.employabilityInformation.eligibleIn,
        drugTest: optOf<boolean>(preferences.employabilityInformation.drugTest),
        backgroundCheck: optOf<boolean>(preferences.employabilityInformation.backgroundCheck),
        securityClearance: optOf<boolean>(preferences.employabilityInformation.securityClearance)
    },
    qualifications: {
        skills: preferences.qualifications.skills,
        languages: preferences.qualifications.languages
    },
    geographicPreferences: {
        willingToRelocate: optOf<boolean>(preferences.geographicPrefs.willingToRelocate),
        desiredLocation: {
            country: bind(
                optOf<string>(preferences.geographicPrefs.desiredLocation.country),
                countryOptByName
            ),
            state: bind(
                optOf<string>(preferences.geographicPrefs.desiredLocation.state),
                stateName => optOf<State>(
                    asSomeOrThat(
                        stateOptByName(stateName),
                        { stateCode: "", stateName: preferences.geographicPrefs.desiredLocation.state }
                    )
                )
            ),
            city: optOf<string>(preferences.geographicPrefs.desiredLocation.city)
        },
        commuteLength: {
            distance: optOf<string>(preferences.geographicPrefs.commuteLength.distance),
            unit: optOf<string>(preferences.geographicPrefs.commuteLength.unit)
        },
        nationalOpportunities: optOf<boolean>(preferences.geographicPrefs.nationalOpportunities)
    },
    employmentPreferences: {
        goalsAndInterests: preferences.employmentPrefs.goalsAndInterests,
        desiredRate: {
            amount: optOf<string>(preferences.employmentPrefs.desiredRate.amount),
            rate: optOf<string>(preferences.employmentPrefs.desiredRate.rate)
        },
        desiredPlacementType: optOf<string>(preferences.employmentPrefs.desiredPlacementType),
        desiredSchedule: optOf<string>(preferences.employmentPrefs.desiredSchedule),
        mostRecentRate: {
            amount: optOf<string>(preferences.employmentPrefs.mostRecentRate.amount),
            rate: optOf<string>(preferences.employmentPrefs.mostRecentRate.rate)
        }
    }
});

export const enum Action { WishCouldEdit, WishCouldView }

export interface ViaSomeAction<a> {
    caseOfWishCouldEdit: () => a;
    caseOfWishCouldView: () => a;
    caseOfSomethingElse: (msg: string) => a;
}

export const viaSomeAction = <a>(action: Action, via: ViaSomeAction<a>): a => {
    switch(action) {
        case Action.WishCouldEdit: return via.caseOfWishCouldEdit();
        case Action.WishCouldView: return via.caseOfWishCouldView();
        default: return via.caseOfSomethingElse(`Unexpected action: ${action}`);
    }
};

export interface SkuidModel {
    countries: Optional<skuid.model.Model>;
    states: Optional<skuid.model.Model>;
    cities: Optional<skuid.model.Model>;
    talentAccount: Optional<skuid.model.Model>;
}

export interface Options {
    common: {
        skuidModel: SkuidModel;
        model: Model;
        dispatch: (action: Action, options: Options) => void;
    };
    view: {
        elements: ViewElements;
        render: ViewRender;
    };
    edit: {
        elements: EditElements;
        render: EditRender;
        dispatch: (action: EditAction, model: Model) => Model;
    };
}
