// 3rd party
import { curry } from "ramda";

// Data
import { Action, Options, viaSomeAction, viaSomeViewMode, ViewMode  } from "../data/internalData";

// Library
import { fail } from "../../../../library/core";

// Parts
import { caseOfEditMode } from "../../edit/edit";
import { caseOfViewMode } from "../../view/view";


export const dispatchFactory = curry((_: any, action: Action, options: Options) => {
    return viaSomeAction(action, {
        caseOfWishCouldEdit: () => caseOfEditMode(options),
        caseOfWishCouldView: () => caseOfViewMode(options),
        caseOfSomethingElse: (msg) => fail<void>(msg)
    });
});
