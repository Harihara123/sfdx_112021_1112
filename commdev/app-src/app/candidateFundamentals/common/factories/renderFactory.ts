// 3rd party
import { curry } from "ramda";

// Data
import { viaSomeViewMode, ViewMode } from "../data/internalData";

export const renderFactory = curry((viewMode: ViewMode) => {
    return viaSomeViewMode(viewMode, {
        caseOfView: () =>  $(".edit", ".summary").hide(),
        caseOfEdit: () =>  $(".view", ".summary").hide()
    });
});
