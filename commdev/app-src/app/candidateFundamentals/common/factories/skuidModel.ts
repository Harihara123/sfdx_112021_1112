// Data
import { SkuidModel } from "../data/internalData";

// Helpers
import { getModelOpt } from "../../../../helpers/skuid/model"

export const skuidModelFactory = (): SkuidModel => ({
    countries: getModelOpt("Countries"),
    states: getModelOpt("States"),
    cities: getModelOpt("Cities"),
    talentAccount: getModelOpt("TalentAccount")
});
