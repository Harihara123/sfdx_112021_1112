// Data
import { LastModified, Model, SkuidModel } from "../common/data/internalData";

// Library
import { Optional } from "../../../library/optional";

export interface Selectors {
    candidateOverview: string;
    employabilityInformation: {
        reliableTransportation: string;
        eligibleIn: string;
        drugTest: string;
        backgroundCheck: string;
        securityClearance: string;
    };
    qualifications: {
        skills: string;
        languages: string;
    };
    geographicPreferences: {
        willingToRelocate: string;
        desiredLocation: {
            city: string;
            state: string;
            country: string;
        };
        commuteLength: {
            distance: string;
            unit: string;
        };
        nationalOpportunities: string;
    };
    employmentPrefs: {
        goalsAndInterests: string;
        desiredRate: {
            amount: string;
            rate: string;
        };
        desiredPlacementType: string;
        desiredSchedule: string;
        mostRecentRate: {
            amount: string;
            rate: string;
        };
    };
    cancelBtn: string;
    saveBtn: string;
}

export const selectorsAs = (): Selectors => ({
    candidateOverview: ".candidate-overview__text",
    employabilityInformation: {
        reliableTransportation: ".employability-information__reliable-transportation-picklist",
        eligibleIn: ".employability-information__countries-eligible-in-picklist",
        drugTest: ".employability-information__drug-test-picklist",
        backgroundCheck: ".employability-information__background-check-picklist",
        securityClearance: ".employability-information__security-clearance-picklist"
    },
    qualifications: {
        skills: ".qualifications__skills-picklist",
        languages: ".qualifications__languages-picklist"
    },
    geographicPreferences: {
        willingToRelocate: ".geographic-prefs__willing-to-relocate-picklist",
        desiredLocation: {
            city: ".geographic-prefs__desired-location--city-input",
            state: ".geographic-prefs__desired-location--state-picklist",
            country: ".geographic-prefs__desired-location--country-picklist"
        },
        commuteLength: {
            distance: ".geographic-prefs__commute-length-distance",
            unit: ".geographic-prefs__commute-length-unit"
        },
        nationalOpportunities: ".geographic-prefs__national-opportunites-picklist"
    },
    employmentPrefs: {
        goalsAndInterests: ".employment-prefs__goals-and-interests-picklist",
        desiredRate: {
            amount: ".employment-prefs__desired-rate--amount",
            rate: ".employment-prefs__desired-rate--rate"
        },
        desiredPlacementType: ".employment-prefs__desired-placement-type--picklist",
        desiredSchedule: ".employment-prefs__desired-schedule--picklist",
        mostRecentRate: {
            amount: ".employment-prefs__most-recent-rate--amount",
            rate: ".employment-prefs__most-recent-rate--rate"
        }
    },
    cancelBtn: ".button.button--cancel",
    saveBtn: ".button.button--save"
});

export interface Elements {
    $candidateOverview: JQuery;
    employabilityInformation: {
        $reliableTransportation: JQuery;
        $eligibleIn: JQuery;
        $drugTest: JQuery;
        $backgroundCheck: JQuery;
        $securityClearance: JQuery;
    };
    qualifications: {
        $skills: JQuery;
        $languages: JQuery;
    };
    geographicPreferences: {
        $willingToRelocate: JQuery;
        desiredLocation: {
            $city: JQuery;
            $state: JQuery;
            $country: JQuery;
        };
        commuteLength: {
            $distance: JQuery;
            $unit: JQuery;
        };
        $nationalOpportunities: JQuery
    };
    employmentPreferences: {
        $goalsAndInterests: JQuery;
        desiredRate: {
            $amount: JQuery;
            $rate: JQuery;
        };
        $desiredPlacementType: JQuery;
        $desiredSchedule: JQuery;
        mostRecentRate: {
            $amount: JQuery;
            $rate: JQuery;
        };
    };
    $cancelBtn: JQuery;
    $saveBtn: JQuery;
}

export const elementsFrom = (selectors: Selectors): Elements => ({
    $candidateOverview: $(selectors.candidateOverview),
    employabilityInformation: {
        $reliableTransportation: $(selectors.employabilityInformation.reliableTransportation),
        $eligibleIn: $(selectors.employabilityInformation.eligibleIn),
        $drugTest: $(selectors.employabilityInformation.drugTest),
        $backgroundCheck: $(selectors.employabilityInformation.backgroundCheck),
        $securityClearance: $(selectors.employabilityInformation.securityClearance)
    },
    qualifications: {
        $skills: $(selectors.qualifications.skills),
        $languages: $(selectors.qualifications.languages)
    },
    geographicPreferences: {
        $willingToRelocate: $(selectors.geographicPreferences.willingToRelocate),
        desiredLocation: {
            $city: $(selectors.geographicPreferences.desiredLocation.city),
            $state: $(selectors.geographicPreferences.desiredLocation.state),
            $country: $(selectors.geographicPreferences.desiredLocation.country)
        },
        commuteLength: {
            $distance: $(selectors.geographicPreferences.commuteLength.distance),
            $unit: $(selectors.geographicPreferences.commuteLength.unit)
        },
        $nationalOpportunities: $(selectors.geographicPreferences.nationalOpportunities)
    },
    employmentPreferences: {
        $goalsAndInterests: $(selectors.employmentPrefs.goalsAndInterests),
        desiredRate: {
            $amount: $(selectors.employmentPrefs.desiredRate.amount),
            $rate: $(selectors.employmentPrefs.desiredRate.rate)
        },
        $desiredPlacementType: $(selectors.employmentPrefs.desiredPlacementType),
        $desiredSchedule: $(selectors.employmentPrefs.desiredSchedule),
        mostRecentRate: {
            $amount: $(selectors.employmentPrefs.mostRecentRate.amount),
            $rate: $(selectors.employmentPrefs.mostRecentRate.rate)
        }
    },
    $cancelBtn: $(selectors.cancelBtn),
    $saveBtn: $(selectors.saveBtn)
});

export interface Render {
    candidateOverview: ($candidateOverview: JQuery, model: Model) => void;
    employabilityInformation: {
        reliableTransportation: ($reliableTransportation: JQuery, model: Model) => void;
        eligibleIn: ($eligibleIn: JQuery, model: Model) => void;
        drugTest: ($drugTest: JQuery, model: Model) => void;
        backgroundCheck: ($backgroundCheck: JQuery, model: Model) => void;
        securityClearance: ($securityClearance: JQuery, model: Model) => void;
    };
    qualifications: {
        skills: ($skills: JQuery, model: Model) => void;
        languages: ($languages: JQuery, model: Model) => void;
    };
    geographicPreferences: {
        willingToRelocate: ($willingToRelocate: JQuery, model: Model) => void;
        desiredLocation: {
            countries: ($countries: JQuery, countriesModelOpt: Optional<skuid.model.Model>, model: Model) => void;
            states: ($states: JQuery, statesModelOpt: Optional<skuid.model.Model>, model: Model) => void;
            cities: ($cities: JQuery, citiesModelOpt: Optional<skuid.model.Model>, model: Model) => void;
        };
        commuteLength: {
            distance: ($distance: JQuery, model: Model) => void;
            unit: ($unit: JQuery, model: Model) => void;
        };
        nationalOpportunities: ($nationalOpportunities: JQuery, model: Model) => void;
    };
    employmentPreferences: {
        goalsAndInterests: ($goalsAndInterests: JQuery, model: Model) => void;
        desiredRate: {
            amount: ($amount: JQuery, model: Model) => void;
            rate: ($rate: JQuery, model: Model) => void;
        };
        desiredPlacementType: ($desiredPlacementType: JQuery, model: Model) => void;
        desiredSchedule: ($desiredSchedule: JQuery, model: Model) => void;
        mostRecentRate: {
            amount: ($amount: JQuery, model: Model) => void;
            rate: ($rate: JQuery, model: Model) => void;
        };
    };
}

export interface Update {
    lastModified: (model: Model) => Model;
    candidateOverview: (valueOpt: Optional<string>, model: Model) => Model;
    employabilityInformation: {
        reliableTransportation: (valueOpt: Optional<string>, model: Model) => Model;
        eligibleIn: (valuesOpt: Optional<string[]>, model: Model) => Model;
        drugTest: (valueOpt: Optional<string>, model: Model) => Model;
        backgroundCheck: (valueOpt: Optional<string>, model: Model) => Model;
        securityClearance: (valueOpt: Optional<string>, model: Model) => Model;
    };
    qualifications: {
        skills: (valuesOpt: Optional<string[]>, model: Model) => Model;
        languages: (valuesOpt: Optional<string[]>, model: Model) => Model;
    };
    geographicPreferences: {
        willingToRelocate: (valueOpt: Optional<string>, model: Model) => Model;
        desiredLocation: {
            country: (selectionOpt: Optional<string>, model: Model) => Model;
            state: (selectionOpt: Optional<string>, model: Model) => Model;
            city: (selectionOpt: Optional<string>, model: Model) => Model;
        };
        commuteLength: {
            distance: (valueOpt: Optional<string>, model: Model) => Model;
            unit: (valueOpt: Optional<string>, model: Model) => Model;
        };
        nationalOpportunities: (valueOpt: Optional<string>, model: Model) => Model;
    };
    employmentPreferences: {
        goalsAndInterests: (valueOpts: Optional<string[]>, model: Model) => Model;
        desiredRate: {
            amount: (valueOpt: Optional<string>, model: Model) => Model;
            rate: (valueOpt: Optional<string>, model: Model) => Model;
        };
        desiredPlacementType: (valueOpt: Optional<string>, model: Model) => Model;
        desiredSchedule: (valueOpt: Optional<string>, model: Model) => Model;
        mostRecentRate: {
            amount: (valueOpt: Optional<string>, model: Model) => Model;
            rate: (valueOpt: Optional<string>, model: Model) => Model;
        };
    };
}

export const enum Action {
    UpdateLastModified,
    UpdateCandidateOverview,

    // Employability information
    UpdateReliableTransportation,
    UpdateEligibleIn,
    UpdateDrugTest,
    UpdateBackgroundCheck,
    UpdateSecurityClearance,

    // Qualifications
    UpdateSkills,
    UpdateLanguages,

    // Geographic preferences
    UpdateWillingToRelocate,
    UpdateCountry,
    UpdateState,
    UpdateCity,
    UpdateDistance,
    UpdateUnit,
    UpdateNationalOpportunities,

    // Employment preferences
    UpdateGoalsAndInterests,
    UpdateDesiredRateAmount,
    UpdatedDesiredRateRate,
    UpdateDesiredPlacementType,
    UpdatedDesiredSchedule,
    UpdateMostRecentAmount,
    UpdateMostRecentRate
}

export interface ViaSomeActtion<r> {
    caseOfUpdateLastModified: () => r;
    caseOfUpdateCandidateOverview: () => r;

    // Employability information
    caseOfUpdateReliableTransportation: () => r;
    caseOfUpdateEligibleIn: () => r;
    caseOfUpdateDrugTest: () => r;
    caseOfUpdateBackgroundCheck: () => r;
    caseOfUpdateSecurityClearance: () => r;

    // Qualifications
    caseOfUpdateSkills: () => r;
    caseOfUpdateLanguages: () => r;

    // Geographic preferences
    caseOfUpdateWillingToRelocate: () => r;
    caseOfUpdateCountry: () => r;
    caseOfUpdateState: () => r;
    caseOfUpdateCity: () => r;
    caseOfUpdateDistance: () => r;
    caseOfUpdateUnit: () => r;
    caseOfUpdateNationalOpportunities: () => r;

    // Employment preferences
    caseOfUpdateGoalsAndInterests: () => r;
    caseOfUpdateDesiredRateAmount: () => r;
    caseOfUpdatedDesiredRateRate: () => r;
    caseOfUpdateDesiredPlacementType: () => r;
    caseOfUpdatedDesiredSchedule: () => r;
    caseOfUpdateMostRecentAmount: () => r;
    caseOfUpdateMostRecentRate: () => r;

    caseOfNone: (reason: string) => r;
}

export const viaSomeAction = <r>(action: Action, via: ViaSomeActtion<r>) => {
    switch(action) {
        case Action.UpdateLastModified: return via.caseOfUpdateLastModified();
        case Action.UpdateCandidateOverview: return via.caseOfUpdateCandidateOverview();

        // Employability information
        case Action.UpdateReliableTransportation: return via.caseOfUpdateReliableTransportation();
        case Action.UpdateEligibleIn: return via.caseOfUpdateEligibleIn();
        case Action.UpdateDrugTest: return via.caseOfUpdateDrugTest();
        case Action.UpdateBackgroundCheck: return via.caseOfUpdateBackgroundCheck();
        case Action.UpdateSecurityClearance: return via.caseOfUpdateSecurityClearance();

        // Qualifications
        case Action.UpdateSkills: return via.caseOfUpdateSkills();
        case Action.UpdateLanguages: return via.caseOfUpdateLanguages();

        // Geographic preferences
        case Action.UpdateWillingToRelocate: return via.caseOfUpdateWillingToRelocate();
        case Action.UpdateCountry: return via.caseOfUpdateCountry();
        case Action.UpdateState: return via.caseOfUpdateState();
        case Action.UpdateCity: return via.caseOfUpdateCity();
        case Action.UpdateDistance: return via.caseOfUpdateDistance();
        case Action.UpdateUnit: return via.caseOfUpdateUnit();
        case Action.UpdateNationalOpportunities: return via.caseOfUpdateNationalOpportunities();

        // Employment preferences
        case Action.UpdateGoalsAndInterests: return via.caseOfUpdateGoalsAndInterests();
        case Action.UpdateDesiredRateAmount: return via.caseOfUpdateDesiredRateAmount();
        case Action.UpdatedDesiredRateRate: return via.caseOfUpdatedDesiredRateRate();
        case Action.UpdateDesiredPlacementType: return via.caseOfUpdateDesiredPlacementType();
        case Action.UpdatedDesiredSchedule: return via.caseOfUpdatedDesiredSchedule();
        case Action.UpdateMostRecentAmount: return via.caseOfUpdateMostRecentAmount();
        case Action.UpdateMostRecentRate: return via.caseOfUpdateMostRecentRate();

        default: return via.caseOfNone(`Unexpected case of ${action}`)
    }
};

export interface Options {
    skuidModel: SkuidModel,
    model: Model,
    elements: Elements,
    render: Render,
    dispatch: (action: Action, model: Model) => Model
}
