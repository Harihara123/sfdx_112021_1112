// 3rd party
import { compose, curry } from "ramda";

// Data
import { willBeStates } from "../../common/data/geodata";
import { Action as CommonAction, initModel, Model, modelFrom, Options, SkuidModel, viaSomeViewMode, ViewMode } from "../common/data/internalData";
import { preferencesFrom } from "../common/data/externalData";
import { Action, Elements, elementsFrom, Render, Selectors, Update } from "./data";

// Factories
import { skuidModelFactory } from "../common/factories/skuidModel";
import { dispatchFactory, renderFactory, updateFactory } from "./factories";

// Helpers
import { valueOf } from "../../../helpers/jquery-helper";
import * as skuidModelHelpers from "../../../helpers/skuid/model"

// Library
import { fromOne, fromTwo } from "../../../library/array";
import { ignore, scheduleError, use } from "../../../library/core";
import { asSomeOrFail, bind, of as optOf, Optional, map as optMap, withSomeOrElse } from "../../../library/optional";

// Parts
import { caseOfViewMode } from "../view/view";


export const caseOfEditMode = (options: Options) => {

    const skuidModel = options.common.skuidModel;
    const model = options.common.model;
    const elements = options.edit.elements;
    const render = options.edit.render;
    const dispatch = options.edit.dispatch;

    // VIEW //
    $(".view", ".summary").hide();
    $(".edit", ".summary").show();

    // Render candidate overview
    render.candidateOverview(elements.$candidateOverview, model);

    // Render employability information fields
    render.employabilityInformation.reliableTransportation(
        elements.employabilityInformation.$reliableTransportation, model);
    render.employabilityInformation.eligibleIn(elements.employabilityInformation.$eligibleIn, model);
    render.employabilityInformation.drugTest(elements.employabilityInformation.$drugTest, model);
    render.employabilityInformation.backgroundCheck(elements.employabilityInformation.$backgroundCheck, model);
    render.employabilityInformation.securityClearance(elements.employabilityInformation.$securityClearance, model);

    // Render qualifications fields
    render.qualifications.skills(elements.qualifications.$skills, model);
    render.qualifications.languages(elements.qualifications.$languages, model);

    // Render geographic preferences fields
    render.geographicPreferences.willingToRelocate(elements.geographicPreferences.$willingToRelocate, model);
    render.geographicPreferences.desiredLocation.countries(
        elements.geographicPreferences.desiredLocation.$country, skuidModel.countries, model
    );
    render.geographicPreferences.desiredLocation.states(
        elements.geographicPreferences.desiredLocation.$state, skuidModel.states, model
    );
    render.geographicPreferences.desiredLocation.cities(
        elements.geographicPreferences.desiredLocation.$city, skuidModel.cities, model
    );
    render.geographicPreferences.commuteLength.distance(elements.geographicPreferences.commuteLength.$distance, model);
    render.geographicPreferences.commuteLength.unit(elements.geographicPreferences.commuteLength.$unit, model);
    render.geographicPreferences.nationalOpportunities(elements.geographicPreferences.$nationalOpportunities, model);

    // Render employment preferences
    render.employmentPreferences.goalsAndInterests(elements.employmentPreferences.$goalsAndInterests, model);
    render.employmentPreferences.desiredRate.amount(elements.employmentPreferences.desiredRate.$amount, model);
    render.employmentPreferences.desiredRate.rate(elements.employmentPreferences.desiredRate.$rate, model);
    render.employmentPreferences.desiredPlacementType(elements.employmentPreferences.$desiredPlacementType, model);
    render.employmentPreferences.desiredSchedule(elements.employmentPreferences.$desiredSchedule, model);
    render.employmentPreferences.mostRecentRate.amount(elements.employmentPreferences.mostRecentRate.$amount, model);
    render.employmentPreferences.mostRecentRate.rate(elements.employmentPreferences.mostRecentRate.$rate, model);

};

export const actionFrom = curry((options: Options) => {

    const skuidModel = options.common.skuidModel;
    const dispatch = options.edit.dispatch;
    const elements = options.edit.elements;
    const render = options.edit.render;

    // Candidate overview
    elements.$candidateOverview
        .on("change", () => options.common.model = dispatch(Action.UpdateCandidateOverview, options.common.model));

    // Employability information
    use.one(elements.employabilityInformation, elements => {
        elements.$reliableTransportation.on("change", () => options.common.model =
            dispatch(Action.UpdateReliableTransportation, options.common.model));
        elements.$eligibleIn.on("change", () => options.common.model =
            dispatch(Action.UpdateEligibleIn, options.common.model));
        elements.$drugTest.on("change", () => options.common.model =
            dispatch(Action.UpdateDrugTest, options.common.model));
        elements.$backgroundCheck.on("change", () => options.common.model =
            dispatch(Action.UpdateBackgroundCheck, options.common.model));
        elements.$securityClearance.on("change", () => options.common.model =
            dispatch(Action.UpdateSecurityClearance, options.common.model));
    });

    // Qualifications
    use.one(elements.qualifications, elements => {
        elements.$skills.on("change", () => options.common.model = dispatch(Action.UpdateSkills, options.common.model));
        elements.$languages.on("change", () => options.common.model =
            dispatch(Action.UpdateLanguages, options.common.model));
    });

    // Geographic preferences
    use.one(elements.geographicPreferences, elements => {
        elements.$willingToRelocate.on("change", () => options.common.model =
            dispatch(Action.UpdateWillingToRelocate, options.common.model));
        elements.desiredLocation.$country.on("change", (event, params: { shouldUpdate: boolean }) => {

            withSomeOrElse(optOf(params), ignore, none => {
                // Update model with selected country
                options.common.model = dispatch(Action.UpdateCountry, options.common.model);

                optMap(
                    valueOf<string>(elements.desiredLocation.$country),
                    countryCode => {
                        optMap(
                            willBeStates(skuidModel.states, countryCode),
                            willBeStates => {
                                willBeStates
                                    .then(() => {
                                        elements.desiredLocation.$state.empty();
                                        render.geographicPreferences.desiredLocation.states(
                                            elements.desiredLocation.$state, skuidModel.states, options.common.model
                                        );
                                    })
                                    .catch((error: string) => scheduleError(error));
                            }
                        )
                    }
                );

            });

        });
        elements.desiredLocation.$state.on("change", (event, params: { shouldUpdate: boolean }) => {
            withSomeOrElse(optOf(params), ignore, none => {
                options.common.model = dispatch(Action.UpdateState, options.common.model);

                render.geographicPreferences.desiredLocation.cities(
                    elements.desiredLocation.$city, skuidModel.cities, options.common.model
                );

                // NOTE: Disabled until the Cities Apigee Service is completed w/ filtering.
                // optMap(skuidModel.cities, citiesModel => {
                //
                //     // Enable URL merge conditions
                //     optMap(
                //         skuidModelHelpers.getConditionOpt(citiesModel, "countryCode", false),
                //         countryCodeCondition => {
                //             optMap(
                //                 skuidModelHelpers.getConditionOpt(citiesModel, "stateCode", false),
                //                 stateCodeCondition => {
                //                     skuidModelHelpers.deactivateConditions(
                //                         citiesModel, fromTwo(countryCodeCondition, stateCodeCondition)
                //                     );
                //                     citiesModel.setCondition(countryCodeCondition, asSomeOrFail(
                //                         valueOf(elements.desiredLocation.$country), "No country code!"))
                //                     citiesModel.setCondition(stateCodeCondition, asSomeOrFail(
                //                         valueOf(elements.desiredLocation.$state), "No state code!"
                //                     ));
                //                     skuidModelHelpers.activateConditions(
                //                         citiesModel, fromTwo(countryCodeCondition, stateCodeCondition)
                //                     );
                //                 }
                //             );
                //         }
                //     );
                //
                //     // Update the States skuid model
                //     skuidModelHelpers.updateData(citiesModel)
                //         .then(citiesModel => {
                //             elements.desiredLocation.$city.empty();
                //             render.geographicPreferences.desiredLocation.cities(
                //                 elements.desiredLocation.$city, skuidModel.cities, options.common.model
                //             );
                //         })
                //         .catch((error: string) => scheduleError(error));
                //
                // });
            });

        });
        elements.desiredLocation.$city.on("change", () => options.common.model =
            dispatch(Action.UpdateCity, options.common.model));
        elements.commuteLength.$distance.on("change", () => options.common.model =
            dispatch(Action.UpdateDistance, options.common.model));
        elements.commuteLength.$unit.on("change", () => options.common.model =
            dispatch(Action.UpdateUnit, options.common.model));
        elements.$nationalOpportunities.on("change", () => options.common.model =
            dispatch(Action.UpdateNationalOpportunities, options.common.model));
    });

    // Employment preferences
    use.one(elements.employmentPreferences, elements => {
        elements.$goalsAndInterests.on("change", () => options.common.model =
            dispatch(Action.UpdateGoalsAndInterests, options.common.model));
        elements.desiredRate.$amount.on("change", () => options.common.model =
            dispatch(Action.UpdateDesiredRateAmount, options.common.model));
        elements.desiredRate.$rate.on("change", () => options.common.model =
            dispatch(Action.UpdatedDesiredRateRate, options.common.model));
        elements.$desiredPlacementType.on("change", () => options.common.model =
            dispatch(Action.UpdateDesiredPlacementType, options.common.model));
        elements.$desiredSchedule.on("change", () => options.common.model =
            dispatch(Action.UpdatedDesiredSchedule, options.common.model));
        elements.mostRecentRate.$amount.on("change", () => options.common.model =
            dispatch(Action.UpdateMostRecentAmount, options.common.model));
        elements.mostRecentRate.$rate.on("change", () => options.common.model =
            dispatch(Action.UpdateMostRecentRate, options.common.model));
    });

    elements.$cancelBtn.on("click", () => {
        // Reset the model
        options.common.model = initModel({skuidModel: options.common.skuidModel});
        // Dispatch wishCouldView
        options.common.dispatch(CommonAction.WishCouldView, options);
    });

    elements.$saveBtn.on("click", () => {
            optMap(
                skuidModel.talentAccount,
                talentAccountModel => {

                    // Update last modified info
                    options.common.model = dispatch(Action.UpdateLastModified, options.common.model);

                    // Update Preferences__c with model data
                    skuidModelHelpers.updateRow(
                        talentAccountModel,
                        skuidModelHelpers.getFirstRow<{ Talent_Preference_Internal__c: string }>(talentAccountModel),
                        { Talent_Preference_Internal__c: JSON.stringify(preferencesFrom(options.common.model)) }
                    );

                    // Save Preferences__c
                    skuidModelHelpers.save(talentAccountModel)
                        .then(result => {
                            // Toggle view
                            options.common.dispatch(CommonAction.WishCouldView, options);
                        })
                        .catch((error: string) => scheduleError(error));
                }
            );
        }
    );

});
