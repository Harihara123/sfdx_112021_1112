import { dispatchFactory } from "./factories/dispatchFactory";
import { renderFactory } from "./factories/renderFactory";
import { updateFactory } from "./factories/updateFactory";

export { dispatchFactory, renderFactory, updateFactory }
