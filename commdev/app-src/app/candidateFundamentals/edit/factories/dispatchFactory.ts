// 3rd party
import { compose, curry } from "ramda";

// Data
import { Model } from "../../common/data/internalData";
import { Action, Elements, Render, Update, viaSomeAction} from "../data";

// Helpers
import { valueOf } from "../../../../helpers/jquery-helper";

// Library
import * as core from "../../../../library/core";

export const dispatchFactory = curry((
    update: Update,
    elements: Elements,
    render: Render,
    action: Action,
    model: Model
): Model => {
    return viaSomeAction(action, {

        caseOfUpdateLastModified: () => update.lastModified(model),

        caseOfUpdateCandidateOverview: () => update.candidateOverview(
            valueOf<string>(elements.$candidateOverview), model),

        // Employability information
        caseOfUpdateReliableTransportation: () => update.employabilityInformation.reliableTransportation(
            valueOf<string>(elements.employabilityInformation.$reliableTransportation), model),
        caseOfUpdateEligibleIn: () => update.employabilityInformation.eligibleIn(
            valueOf<string[]>(elements.employabilityInformation.$eligibleIn), model),
        caseOfUpdateDrugTest: () => update.employabilityInformation.drugTest(
            valueOf<string>(elements.employabilityInformation.$drugTest), model),
        caseOfUpdateBackgroundCheck: () => update.employabilityInformation.backgroundCheck(
            valueOf<string>(elements.employabilityInformation.$backgroundCheck), model),
        caseOfUpdateSecurityClearance: () => update.employabilityInformation.securityClearance(
            valueOf<string>(elements.employabilityInformation.$securityClearance), model),

        // Qualifications
        caseOfUpdateSkills: () => update.qualifications.skills(
            valueOf<string[]>(elements.qualifications.$skills), model),
        caseOfUpdateLanguages: () => update.qualifications.languages(
            valueOf<string[]>(elements.qualifications.$languages), model),

        // Geographic preferences
        caseOfUpdateWillingToRelocate: () => update.geographicPreferences.willingToRelocate(
            valueOf<string>(elements.geographicPreferences.$willingToRelocate), model),
        caseOfUpdateCountry: () => update.geographicPreferences.desiredLocation.country(
            valueOf<string>(elements.geographicPreferences.desiredLocation.$country), model),
        caseOfUpdateState: () => update.geographicPreferences.desiredLocation.state(
            valueOf<string>(elements.geographicPreferences.desiredLocation.$state), model),
        caseOfUpdateCity: () => update.geographicPreferences.desiredLocation.city(
            valueOf<string>(elements.geographicPreferences.desiredLocation.$city), model),
        caseOfUpdateDistance: () => update.geographicPreferences.commuteLength.distance(
            valueOf<string>(elements.geographicPreferences.commuteLength.$distance), model),
        caseOfUpdateUnit: () => update.geographicPreferences.commuteLength.unit(
            valueOf<string>(elements.geographicPreferences.commuteLength.$unit), model),
        caseOfUpdateNationalOpportunities: () => update.geographicPreferences.nationalOpportunities(
            valueOf<string>(elements.geographicPreferences.$nationalOpportunities), model),

        // Employment preferences
        caseOfUpdateGoalsAndInterests: () => update.employmentPreferences.goalsAndInterests(
            valueOf<string[]>(elements.employmentPreferences.$goalsAndInterests), model),
        caseOfUpdateDesiredRateAmount: () => update.employmentPreferences.desiredRate.amount(
            valueOf<string>(elements.employmentPreferences.desiredRate.$amount), model),
        caseOfUpdatedDesiredRateRate: () => update.employmentPreferences.desiredRate.rate(
            valueOf<string>(elements.employmentPreferences.desiredRate.$rate), model),
        caseOfUpdateDesiredPlacementType: () => update.employmentPreferences.desiredPlacementType(
            valueOf<string>(elements.employmentPreferences.$desiredPlacementType), model),
        caseOfUpdatedDesiredSchedule: () => update.employmentPreferences.desiredSchedule(
            valueOf<string>(elements.employmentPreferences.$desiredSchedule), model),
        caseOfUpdateMostRecentAmount: () => update.employmentPreferences.mostRecentRate.amount(
            valueOf<string>(elements.employmentPreferences.mostRecentRate.$amount), model),
        caseOfUpdateMostRecentRate: () => update.employmentPreferences.mostRecentRate.rate(
            valueOf<string>(elements.employmentPreferences.mostRecentRate.$rate), model),

        caseOfNone: () => model
    })
});
