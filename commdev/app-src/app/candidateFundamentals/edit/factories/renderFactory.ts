// Common
import { City, Country, State } from "../../../common/data";
import * as commonUI from "../../../common/ui";

// Data
import { Elements, Render } from "../data";

// Helpers
import {
    from as select2From,
    multiple as select2Multiple,
    noOptions as select2NoOptions,
    noSearch as select2NoSearch,
    tokenized as select2Tokenized
} from "../../../../helpers/select2-helper";
import { getRows } from "../../../../helpers/skuid/model";

// Library
import { of as arrayOf, fromFour, map as arrayMap, withSomeOrElse } from "../../../../library/array";
import { ignore, outOfBoolean } from "../../../../library/core";
import { map as optMap } from "../../../../library/optional";

export const renderFactory = (elements: Elements): Render => ({

    // Candidate overview
    candidateOverview: ($candidateOverview, model) => optMap(
        model.candidateOverview,
        candidateOverview => $candidateOverview.val(candidateOverview)
    ),

    // Employability preferences
    employabilityInformation: {
        reliableTransportation: ($reliableTransportation, model) => {
            select2NoSearch($reliableTransportation);
            optMap(model.employabilityInformation.reliableTransportation, reliableTransportation => {
                $reliableTransportation.val(outOfBoolean("yes", "no", reliableTransportation)).trigger("change");
            });
        },
        eligibleIn: ($eligibleIn, model) => {
            select2Tokenized($eligibleIn);
            $eligibleIn.empty();
            arrayMap(model.employabilityInformation.eligibleIn, country => {
                return $eligibleIn.append($("<option />").attr("value", country).text(country));
            });
            $eligibleIn.val(model.employabilityInformation.eligibleIn).trigger("change");
        },
        drugTest: ($drugTest, model) => {
            select2NoSearch($drugTest);
            optMap(model.employabilityInformation.drugTest, drugTest => {
                $drugTest.val(outOfBoolean("yes", "no", drugTest)).trigger("change");
            });
        },
        backgroundCheck: ($backgroundCheck, model) => {
            select2NoSearch($backgroundCheck);
            optMap(model.employabilityInformation.backgroundCheck, backgroundCheck => {
                $backgroundCheck.val(outOfBoolean("yes", "no", backgroundCheck)).trigger("change");
            });
        },
        securityClearance: ($securityClearance, model) => {
            select2NoSearch($securityClearance);
            optMap(model.employabilityInformation.securityClearance, securityClearance => {
                $securityClearance.val(outOfBoolean("yes", "no", securityClearance)).trigger("change");
            });
        }
    },

    // Qualifications
    qualifications: {
        skills: ($skills, model) => {
            select2Tokenized($skills);
            $skills.empty();
            arrayMap(model.qualifications.skills, skills => {
                return $skills.append($("<option />").attr("value", skills).text(skills));
            });
            $skills.val(model.qualifications.skills).trigger("change");
        },
        languages: ($languages, model) => {
            select2Multiple($languages);
            $languages.val(model.qualifications.languages).trigger("change");
        }
    },

    // Geographic preferences
    geographicPreferences: {
        willingToRelocate: ($willingToRelocate, model) => {
            select2NoSearch($willingToRelocate);
            optMap(model.geographicPreferences.willingToRelocate, willingToRelocate => {
                $willingToRelocate.val(outOfBoolean("yes", "no", willingToRelocate)).trigger("change");
            });
        },
        desiredLocation: {
            countries: ($countries, countriesModelOpt, model) => {
                optMap(
                    countriesModelOpt,
                    countriesModel => {
                        $countries.select2({ data: arrayMap(
                            getRows<Country>(countriesModel),
                            country => ({ id: country.countryCode, text: country.countryName })
                        )});
                        optMap(model.geographicPreferences.desiredLocation.country, country => {
                            $countries.val(country.countryCode).trigger("change", { shouldUpdate: false });
                        });
                    }
                );
            },
            states: ($states, statesModelOpt, model) => {
                optMap(
                    statesModelOpt,
                    statesModel => {
                        elements.geographicPreferences.desiredLocation.$state.select2({ data: arrayMap(
                            getRows<State>(statesModel),
                            state => ({ id: state.stateCode, text: state.stateName })
                        )});
                        optMap(model.geographicPreferences.desiredLocation.state, state => {
                            $states.val(state.stateCode).trigger("change", { shouldUpdate: false });
                        });
                    }
                );
            },
            cities: ($cities, citiesModelOpt, model) => {
                optMap(
                    citiesModelOpt,
                    citiesModel => {
                        optMap(model.geographicPreferences.desiredLocation.city, city => {
                            $cities.val(city).trigger("change");
                        });
                    }
                );

            }
        },
        commuteLength: {
            distance: ($distance, model) => {
                optMap(model.geographicPreferences.commuteLength.distance, distance => {
                    $distance.val(distance).trigger("change");
                });
            },
            unit: ($unit, model) => {
                select2NoSearch($unit);
                optMap(model.geographicPreferences.commuteLength.unit, unit => {
                    $unit.val(unit).trigger("change");
                });
            }
        },
        nationalOpportunities: ($nationalOpportunities, model) => {
            select2NoSearch($nationalOpportunities);
            optMap(model.geographicPreferences.nationalOpportunities, nationalOpportunities => {
                $nationalOpportunities.val(outOfBoolean("yes", "no", nationalOpportunities)).trigger("change");
            });
        }
    },

    // Employment preferences
    employmentPreferences: {
        goalsAndInterests: ($goalsAndInterests, model) => {
            select2Tokenized($goalsAndInterests);
            $goalsAndInterests.empty();
            arrayMap(model.employmentPreferences.goalsAndInterests, goalsAndInterests => {
                return $goalsAndInterests
                    .append($("<option />")
                        .attr("value", goalsAndInterests)
                        .text(goalsAndInterests)
                    );
            });
            $goalsAndInterests.val(model.employmentPreferences.goalsAndInterests).trigger("change");
        },
        desiredRate: {
            amount: ($amount, model) => {
                optMap(model.employmentPreferences.desiredRate.amount, amount => {
                    $amount.val(amount).trigger("change");
                });
            },
            rate: ($rate, model) => {
                select2NoSearch($rate);
                optMap(model.employmentPreferences.desiredRate.rate, rate => {
                    $rate.val(rate).trigger("change");
                });
            }
        },
        desiredPlacementType: ($desiredPlacementType, model) => {
            select2NoSearch($desiredPlacementType);
            optMap(model.employmentPreferences.desiredPlacementType, desiredPlacementType => {
                $desiredPlacementType.val(desiredPlacementType).trigger("change");
            });
        },
        desiredSchedule: ($desiredSchedule, model) => {
            select2NoSearch($desiredSchedule);
            optMap(model.employmentPreferences.desiredSchedule, desiredSchedule => {
                $desiredSchedule.val(desiredSchedule).trigger("change");
            });
        },
        mostRecentRate: {
            amount: ($amount, model) => {
                optMap(model.employmentPreferences.mostRecentRate.amount, amount => {
                    $amount.val(amount).trigger("change");
                });
            },
            rate: ($rate, model) => {
                select2NoSearch($rate);
                optMap(model.employmentPreferences.mostRecentRate.rate, rate => {
                    $rate.val(rate).trigger("change");
                });
            }
        }
    }
});
