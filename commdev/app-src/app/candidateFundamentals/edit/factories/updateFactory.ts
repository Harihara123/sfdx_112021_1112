// Data
import { City, cityOptById, Country, countryOptById, State, stateOptById } from "../../../common/data/geodata";
import { LastModified, lastModifiedFrom } from "../../common/data/internalData"
import { Update } from "../data";

// Library
import { concat, fromOne } from "../../../../library/array";
import { bounce, booleanFrom } from "../../../../library/core";
import { asSomeOrFail, bind, map as optMap, of as optOf, withSomeOrThat } from "../../../../library/optional";

export const updateFactory = (): Update => ({

    // Last modified
    lastModified: model => Object.assign(model, {
        lastModified: optOf<LastModified>(lastModifiedFrom())
    }),

    // Candidate overview
    candidateOverview: (valueOpt, model) => Object.assign(model, { candidateOverview: valueOpt }),

    // Employability information
    employabilityInformation: {
        reliableTransportation: (valueOpt, model) => Object.assign(model, {
            employabilityInformation: Object.assign(model.employabilityInformation, {
                reliableTransportation: optMap(valueOpt, booleanFrom("yes"))
            })
        }),
        eligibleIn: (valuesOpt, model) => Object.assign(model, {
            employabilityInformation: Object.assign(model.employabilityInformation, {
                eligibleIn: withSomeOrThat(valuesOpt, bounce, model.employabilityInformation.eligibleIn)
            })
        }),
        drugTest: (valueOpt, model) => Object.assign(model, {
            employabilityInformation: Object.assign(model.employabilityInformation, {
                drugTest: optMap(valueOpt, booleanFrom("yes"))
            })
        }),
        backgroundCheck: (valueOpt, model) => Object.assign(model, {
            employabilityInformation: Object.assign(model.employabilityInformation, {
                backgroundCheck: optMap(valueOpt, booleanFrom("yes"))
            })
        }),
        securityClearance: (valueOpt, model) => Object.assign(model, {
            employabilityInformation: Object.assign(model.employabilityInformation, {
                securityClearance: optMap(valueOpt, booleanFrom("yes"))
            })
        })
    },

    // Qualifications
    qualifications: {
        skills: (valuesOpt, model) => Object.assign(model, {
            qualifications: Object.assign(model.qualifications, {
                skills: withSomeOrThat(valuesOpt, bounce, model.qualifications.skills)
            })
        }),
        languages: (valuesOpt, model) => Object.assign(model, {
            qualifications: Object.assign(model.qualifications, {
                languages: withSomeOrThat(valuesOpt, bounce, model.qualifications.languages)
            })
        })
    },

    // Geographic preferences
    geographicPreferences: {
        willingToRelocate: (valueOpt, model) => Object.assign(model, {
            geographicPreferences: Object.assign(model.geographicPreferences, {
                willingToRelocate: optMap(valueOpt, booleanFrom("yes"))
            })
        }),
        desiredLocation: {
            country: (valueOpt, model) => Object.assign(model, {
                geographicPreferences: Object.assign(model.geographicPreferences, {
                    desiredLocation: Object.assign(model.geographicPreferences.desiredLocation, {
                        country: optMap<string, Country>(
                            valueOpt,
                            value => asSomeOrFail(countryOptById(value), "No country matched!")
                        )
                    })
                })
            }),
            state: (valueOpt, model) => Object.assign(model, {
                geographicPreferences: Object.assign(model.geographicPreferences, {
                    desiredLocation: Object.assign(model.geographicPreferences.desiredLocation, {
                        state: optMap<string, State>(
                            valueOpt,
                            value => asSomeOrFail(stateOptById(value), "No state matched!")
                        )
                    })
                })
            }),
            city: (valueOpt, model) => Object.assign(model, {
                geographicPreferences: Object.assign(model.geographicPreferences, {
                    desiredLocation: Object.assign(model.geographicPreferences.desiredLocation, {
                        city: valueOpt
                    })
                })
            })
        },
        commuteLength: {
            distance: (valueOpt, model) => Object.assign(model, {
                geographicPreferences: Object.assign(model.geographicPreferences, {
                    commuteLength: Object.assign(model.geographicPreferences.commuteLength, {
                        distance: valueOpt
                    })
                })
            }),
            unit: (valueOpt, model) => Object.assign(model, {
                geographicPreferences: Object.assign(model.geographicPreferences, {
                    commuteLength: Object.assign(model.geographicPreferences.commuteLength, {
                        unit: valueOpt
                    })
                })
            })
        },
        nationalOpportunities: (valueOpt, model) => Object.assign(model, {
            geographicPreferences: Object.assign(model.geographicPreferences, {
                nationalOpportunities: optMap(valueOpt, booleanFrom("yes"))
            })
        })
    },

    // Employment preferences
    employmentPreferences: {
        goalsAndInterests: (valuesOpt, model) => Object.assign(model, {
            employmentPreferences: Object.assign(model.employmentPreferences, {
                goalsAndInterests: withSomeOrThat(valuesOpt, bounce, model.employmentPreferences.goalsAndInterests)
            })
        }),
        desiredRate: {
            amount: (valueOpt, model) => Object.assign(model, {
                employmentPreferences: Object.assign(model.employmentPreferences, {
                    desiredRate: Object.assign(model.employmentPreferences.desiredRate, {
                        amount: valueOpt
                    })
                })
            }),
            rate: (valueOpt, model) => Object.assign(model, {
                employmentPreferences: Object.assign(model.employmentPreferences, {
                    desiredRate: Object.assign(model.employmentPreferences.desiredRate, {
                        rate: valueOpt
                    })
                })
            })
        },
        desiredPlacementType: (valueOpt, model) => Object.assign(model, {
            employmentPreferences: Object.assign(model.employmentPreferences, {
                desiredPlacementType: valueOpt
            })
        }),
        desiredSchedule: (valueOpt, model) => Object.assign(model, {
            employmentPreferences: Object.assign(model.employmentPreferences, {
                desiredSchedule: valueOpt
            })
        }),
        mostRecentRate: {
            amount: (valueOpt, model) => Object.assign(model, {
                employmentPreferences: Object.assign(model.employmentPreferences, {
                    mostRecentRate: Object.assign(model.employmentPreferences.mostRecentRate, {
                        amount: valueOpt
                    })
                })
            }),
            rate: (valueOpt, model) => Object.assign(model, {
                employmentPreferences: Object.assign(model.employmentPreferences, {
                    mostRecentRate: Object.assign(model.employmentPreferences.mostRecentRate, {
                        rate: valueOpt
                    })
                })
            })
        }
    }
});
