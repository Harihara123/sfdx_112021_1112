// const talentAccountModel = optional.asSomeOrFail(
//     skuidModelHelpers.getModelOpt("TalentAccount"), text.emptyString);

// const setLastModifiedOver = (elements: fundamentalsData.Elements, model: skuid.model.Model) => {
//     return (data: fundamentalsData.LastModified) => {
//         if (data.isOnSave) {
//             renderLastModified(new Date(data.lastModifiedDate), data.lastModifiedBy.name);
//         } else if (elements.$lastModified.is(":empty")) {
//             if (text.isNotEmpty(data.lastModifiedDate)) {
//                 renderLastModified(new Date(data.lastModifiedDate), data.lastModifiedBy.name);
//             } else {
//                 renderLastModified(
//                     new Date(skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "LastModifiedDate")),
//                     skuidModelHelpers.getFieldValueOfFirstRow<string>(model, "LastModifiedBy")["Name"]
//                 )
//             }
//         } else {
//             core.ignore();
//         }
//     };
// }

// Populate positions
// skuid.events.subscribe("ats.candidateFundamentals.toggleView.edit", (data: { $root: JQuery }) => {
//     optional.withSomeOrFail(
//         skuidModelHelpers.getModelOpt("Position"),
//         populatePositionsOver(data.$root.attr("id"), talentAccountModel),
//         text.emptyString
//     );
// });

// const populatePositionsOver = (rootId: string, talentAccountModel: skuid.model.Model) => {
//     return function populatePositionTitles(model: skuid.model.Model) {
//         let $element = $('select[dataref="position"]');
//         let schema = JSON.parse(JSON.parse($(`#${rootId}_schema`).html()));
//         let positions = array.map(
//             skuidModelHelpers.getRows<fundamentalsData.Position>(model),
//             row => ({ id: row.Id, text: row.Text_Value__c })
//         );
//
//         // TODO: savedData is nullable
//         let savedDataOpt = optional.of<string>(skuidModelHelpers.getFieldValueOfFirstRow<string>(
//             talentAccountModel, "Talent_Preference_Internal__c"));
//         let savedData = optional.withSomeOrElse(
//             savedDataOpt,
//             JSON.parse,
//             noData => ({})
//         );
//         let positionOpt = optional.of<{ data: string }>(savedData["position"]);
//
//         $element.select2({ data: positions });
//         $element.prepend(
//             $("<option></option>")
//                 .attr("value", "none")
//                 .attr("disabled", "disabled")
//                 .text("Select")
//         );
//         optional.withSomeOrElse(
//             positionOpt,
//             position => {
//                 $element
//                     .val(
//                         core.accessors.idOf(
//                             array.headOf(
//                                 positions.filter(somePosition => somePosition.text === position.data)
//                             )
//                         )
//                     )
//                     .trigger("change");
//             },
//             none => {
//                 $element.val("none").trigger("change");
//             }
//         );
//     };
// };
