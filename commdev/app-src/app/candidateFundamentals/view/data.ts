// Data
import { Action, LastModified, Model, SkuidModel } from "../common/data/internalData";

// Library
import { Optional } from "../../../library/optional";

export interface Selectors {
    lastModified: string;
    candidateOverview: string;
    employabilityInformation: {
        reliableTransportation: string;
        eligibleIn: string;
        drugTest: string;
        backgroundCheck: string;
        securityClearance: string;
    };
    qualifications: {
        skills: string;
        languages: string;
    };
    geographicPreferences: {
        willingToRelocate: string;
        desiredLocation: {
            country: string;
            state: string;
            city: string;
        };
        commuteLength: string;
        nationalOpportunities: string;
    };
    employmentPreferences: {
        goalsAndInterests: string;
        desiredRate: string;
        desiredPlacementType: string;
        desiredSchedule: string;
        mostRecentRate: string;
    };
    editBtn: string;
}

export const selectorsAs = (): Selectors => ({
    lastModified: ".summary__last-modified",
    candidateOverview: ".candidate-overview.view",
    employabilityInformation: {
        reliableTransportation: ".employability-information__reliable-transportation.view",
        eligibleIn: ".employability-information__countries-eligible-in.view",
        drugTest: ".employability-information__drug-test.view",
        backgroundCheck: ".employability-information__background-check.view",
        securityClearance: ".employability-information__security-clearance.view"
    },
    qualifications: {
        skills: ".qualifications__skills.view",
        languages: ".qualifications__languages.view"
    },
    geographicPreferences: {
        willingToRelocate: ".geographic-prefs__willing-to-relocate.view",
        desiredLocation: {
            country: ".geographic-prefs__desired-location--country.view",
            state: ".geographic-prefs__desired-location--state.view",
            city: ".geographic-prefs__desired-location--city.view"
        },
        commuteLength: ".geographic-prefs__commute-length.view",
        nationalOpportunities: ".geographic-prefs__national-opportunites.view"
    },
    employmentPreferences: {
        goalsAndInterests: ".employment-prefs__goals-and-interests.view",
        desiredRate: ".employment-prefs__desired-rate.view",
        desiredPlacementType: ".employment-prefs__desired-placement-type.view",
        desiredSchedule: ".employment-prefs__desired-schedule.view",
        mostRecentRate: ".employment-prefs__most-recent-rate.view"
    },
    editBtn: ".button.button--edit"
});

export interface Elements {
    $lastModified: JQuery;
    $candidateOverview: JQuery;
    employabilityInformation: {
        $reliableTransportation: JQuery;
        $eligibleIn: JQuery;
        $drugTest: JQuery;
        $backgroundCheck: JQuery;
        $securityClearance: JQuery;
    };
    qualifications: {
        $skills: JQuery;
        $languages: JQuery;
    };
    geographicPreferences: {
        $willingToRelocate: JQuery;
        desiredLocation: {
            $country: JQuery;
            $state: JQuery;
            $city: JQuery;
        };
        $commuteLength: JQuery;
        $nationalOpportunities: JQuery;
    };
    employmentPreferences: {
        $goalsAndInterests: JQuery;
        $desiredRate: JQuery;
        $desiredPlacementType: JQuery;
        $desiredSchedule: JQuery;
        $mostRecentRate: JQuery;
    };
    $editBtn: JQuery;
}

export const elementsFrom = (selectors: Selectors): Elements => ({
    $lastModified: $(selectors.lastModified),
    $candidateOverview: $(selectors.candidateOverview),
    employabilityInformation: {
        $reliableTransportation: $(selectors.employabilityInformation.reliableTransportation),
        $eligibleIn: $(selectors.employabilityInformation.eligibleIn),
        $drugTest: $(selectors.employabilityInformation.drugTest),
        $backgroundCheck: $(selectors.employabilityInformation.backgroundCheck),
        $securityClearance: $(selectors.employabilityInformation.securityClearance)
    },
    qualifications: {
        $skills: $(selectors.qualifications.skills),
        $languages: $(selectors.qualifications.languages)
    },
    geographicPreferences: {
        $willingToRelocate: $(selectors.geographicPreferences.willingToRelocate),
        desiredLocation: {
            $city: $(selectors.geographicPreferences.desiredLocation.city),
            $state: $(selectors.geographicPreferences.desiredLocation.state),
            $country: $(selectors.geographicPreferences.desiredLocation.country)
        },
        $commuteLength: $(selectors.geographicPreferences.commuteLength),
        $nationalOpportunities: $(selectors.geographicPreferences.nationalOpportunities)
    },
    employmentPreferences: {
        $goalsAndInterests: $(selectors.employmentPreferences.goalsAndInterests),
        $desiredRate: $(selectors.employmentPreferences.desiredRate),
        $desiredPlacementType: $(selectors.employmentPreferences.desiredPlacementType),
        $desiredSchedule: $(selectors.employmentPreferences.desiredSchedule),
        $mostRecentRate: $(selectors.employmentPreferences.mostRecentRate)
    },
    $editBtn: $(selectors.editBtn)
});

export interface Render {
    lastModified: ($lastModified: JQuery, model: Model) => void;
    candidateOverview: ($candidateOverview: JQuery, model: Model) => void;
    employabilityInformation: {
        reliableTransportation: ($reliableTransportation: JQuery, model: Model) => void;
        eligibleIn: ($eligibleIn: JQuery, model: Model) => void;
        drugTest: ($drugTest: JQuery, model: Model) => void;
        backgroundCheck: ($backgroundCheck: JQuery, model: Model) => void;
        securityClearance: ($securityClearance: JQuery, model: Model) => void;
    };
    qualifications: {
        skills: ($skills: JQuery, model: Model) => void;
        languages: ($languages: JQuery, model: Model) => void;
    };
    geographicPreferences: {
        willingToRelocate: ($willingToRelocate: JQuery, model: Model) => void;
        desiredLocation: {
            country: ($country: JQuery, model: Model) => void;
            state: ($state: JQuery, model: Model) => void;
            city: ($city: JQuery, model: Model) => void;
        };
        commuteLength: (commuteLength: JQuery, model: Model) => void;
        nationalOpportunities: ($nationalOpportunities: JQuery, model: Model) => void;
    };
    employmentPreferences: {
        goalsAndInterests: ($goalsAndInterests: JQuery, model: Model) => void;
        desiredRate: ($desiredRate: JQuery, model: Model) => void;
        desiredPlacementType: ($desiredPlacementType: JQuery, model: Model) => void;
        desiredSchedule: ($desiredSchedule: JQuery, model: Model) => void;
        mostRecentRate: ($mostRecentRate: JQuery, model: Model) => void;
    };
}

export interface Options {
    skuidModel: SkuidModel,
    model: Model,
    elements: Elements,
    render: Render,
    dispatch: (action: Action) => void
}
