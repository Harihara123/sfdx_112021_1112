// Common
import { City, Country, State } from "../../../common/data";
import * as commonUI from "../../../common/ui";

// Data
import { Elements, Render } from "../data";

// Helpers
import {
    from as select2From,
    multiple as select2Multiple,
    noOptions as select2NoOptions,
    noSearch as select2NoSearch,
    tokenized as select2Tokenized
} from "../../../../helpers/select2-helper";
import { getRows } from "../../../../helpers/skuid/model";

// Library
import { fromFour, join, map as arrayMap } from "../../../../library/array";
import { ignore, outOfBoolean } from "../../../../library/core";
import { map as optMap} from "../../../../library/optional";
import { capitalizeFirst } from "../../../../library/text";

export const renderFactory = (): Render => ({

    // Last modified
    lastModified: ($lastModified, model) => optMap(
        model.lastModified,
        lastModified => $lastModified.empty().append(fromFour(
            $("<span/>").text(`${commonUI.labels.generic.lastEdit}: `),
            $("<span/>").text(`${skuid.time.formatDate("m/dd/yy", lastModified.date)} ${skuid.time.formatTime("h:mm a", lastModified.date)} `),
            $("<span/>").text(`${commonUI.labels.generic.by} `),
            $("<span/>").text(lastModified.by.name)
        ))
    ),

    // Candidate overview
    candidateOverview: ($candidateOverview, model) => optMap(
        model.candidateOverview,
        candidateOverview => $candidateOverview.text(capitalizeFirst(candidateOverview))
    ),

    // Employability information
    employabilityInformation: {
        reliableTransportation: ($reliableTransportation, model) => optMap(
            model.employabilityInformation.reliableTransportation,
            reliableTransportation => $reliableTransportation.text(outOfBoolean("Yes", "No", reliableTransportation))
        ),
        eligibleIn: ($eligibleIn, model) => $eligibleIn.text(join(", ", model.employabilityInformation.eligibleIn)),
        drugTest: ($drugTest, model) => optMap(
            model.employabilityInformation.drugTest,
            drugTest => $drugTest.text(outOfBoolean("Yes", "No", drugTest))
        ),
        backgroundCheck: ($backgroundCheck, model) => optMap(
            model.employabilityInformation.backgroundCheck,
            backgroundCheck => $backgroundCheck.text(outOfBoolean("Yes", "No", backgroundCheck))
        ),
        securityClearance: ($securityClearance, model) => optMap(
            model.employabilityInformation.securityClearance,
            securityClearance => $securityClearance.text(outOfBoolean("Yes", "No", securityClearance))
        )
    },

    // Qualifications
    qualifications: {
        skills: ($skills, model) => $skills.text(join(", ", model.qualifications.skills)),
        languages: ($languages, model) => $languages.text(
            join(", ", arrayMap(model.qualifications.languages, capitalizeFirst))
        )
    },

    // Geographic preferences
    geographicPreferences: {
        willingToRelocate: ($willingToRelocate, model) => optMap(
            model.geographicPreferences.willingToRelocate,
            willingToRelocate => $willingToRelocate.text(outOfBoolean("Yes", "No", willingToRelocate))
        ),
        desiredLocation: {
            country: ($country, model) => optMap(
                model.geographicPreferences.desiredLocation.country,
                country => $country.text(country.countryName)
            ),
            state: ($state, model) => optMap(
                model.geographicPreferences.desiredLocation.state,
                state => $state.text(state.stateName)
            ),
            city: ($city, model) => optMap(
                model.geographicPreferences.desiredLocation.city,
                city => $city.text(city)
            )
        },
        commuteLength: ($commuteLength, model) => optMap(
            model.geographicPreferences.commuteLength.distance,
            distance => optMap(
                model.geographicPreferences.commuteLength.unit,
                unit => $commuteLength.text(`${distance} ${unit}`)
            )
        ),
        nationalOpportunities: ($nationalOpportunities, model) => optMap(
            model.geographicPreferences.nationalOpportunities,
            nationalOpportunities => $nationalOpportunities.text(outOfBoolean("Yes", "No", nationalOpportunities))
        )
    },
    employmentPreferences: {
        goalsAndInterests: ($goalsAndInterests, model) => $goalsAndInterests.text(
            join(", ", model.employmentPreferences.goalsAndInterests)
        ),
        desiredRate: ($desiredRate, model) => optMap(
            model.employmentPreferences.desiredRate.amount,
            amount => optMap(
                model.employmentPreferences.desiredRate.rate,
                rate => $desiredRate.text(`${amount} ${rate}`)
            )
        ),
        desiredPlacementType: ($desiredPlacementType, model) => optMap(
            model.employmentPreferences.desiredPlacementType,
            desiredPlacementType => $desiredPlacementType.text(capitalizeFirst(desiredPlacementType))
        ),
        desiredSchedule: ($desiredSchedule, model) => optMap(
            model.employmentPreferences.desiredSchedule,
            desiredSchedule => $desiredSchedule.text(capitalizeFirst(desiredSchedule))
        ),
        mostRecentRate: ($mostRecentRate, model) => optMap(
            model.employmentPreferences.mostRecentRate.amount,
            amount => optMap(
                model.employmentPreferences.mostRecentRate.rate,
                rate => $mostRecentRate.text(`${amount} ${rate}`)
            )
        )
    }

});
