// 3rd party
import { curry } from "ramda";

// Data
import { Action, initModel, Model, modelFrom, Options, SkuidModel } from "../common/data/internalData";
import { Elements, elementsFrom, Render, Selectors } from "./data";

// Factories
import { renderFactory } from "./factories";
import { skuidModelFactory } from "../common/factories/skuidModel";

// Helpers
//import { valueOf } from "../../../helpers/jquery-helper";
import * as skuidModelHelpers from "../../../helpers/skuid/model"

// Library
import { use } from "../../../library/core";
import { withSomeOrElse } from "../../../library/optional";

export const caseOfViewMode = (options: Options) => {

    const skuidModel = options.common.skuidModel;
    const model = options.common.model;
    const elements = options.view.elements;
    const render = options.view.render;
    const dispatch = options.common.dispatch;

    // VIEW //
    $(".edit", ".summary").hide();
    $(".select2", ".summary").remove();
    $(".view", ".summary").show();

    // Last Modified
    render.lastModified(elements.$lastModified, model);

    // Candidate overview
    render.candidateOverview(elements.$candidateOverview, model);

    // Employability information
    use.two(render.employabilityInformation, elements.employabilityInformation, (render, elements) => {
        render.reliableTransportation(elements.$reliableTransportation, model);
        render.eligibleIn(elements.$eligibleIn, model);
        render.drugTest(elements.$drugTest, model);
        render.backgroundCheck(elements.$backgroundCheck, model);
        render.securityClearance(elements.$securityClearance, model);
    });

    // Qualifications
    use.two(render.qualifications, elements.qualifications, (render, elements) => {
        render.skills(elements.$skills, model);
        render.languages(elements.$languages, model);
    });

    // Geographic preferences
    use.two(render.geographicPreferences, elements.geographicPreferences, (render, elements) => {
        render.willingToRelocate(elements.$willingToRelocate, model);
        render.desiredLocation.country(elements.desiredLocation.$country, model);
        render.desiredLocation.state(elements.desiredLocation.$state, model);
        render.desiredLocation.city(elements.desiredLocation.$city, model);
        render.commuteLength(elements.$commuteLength, model);
        render.nationalOpportunities(elements.$nationalOpportunities, model);
    });

    // Employment preferences
    use.two(render.employmentPreferences, elements.employmentPreferences, (render, elements) => {
        render.goalsAndInterests(elements.$goalsAndInterests, model);
        render.desiredRate(elements.$desiredRate, model);
        render.desiredPlacementType(elements.$desiredPlacementType, model);
        render.desiredSchedule(elements.$desiredSchedule, model);
        render.mostRecentRate(elements.$mostRecentRate, model);
    });

};

export const actionFrom = curry((options: Options) => {
    options.view.elements.$editBtn.on("click", () => options.common.dispatch(Action.WishCouldEdit, options));
});
