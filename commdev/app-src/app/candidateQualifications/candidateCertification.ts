// Library
import * as core from "../../library/core";
import * as optional from "../../library/optional";

// common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

// Data
import * as certificationData from "./data";

// Helpers
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

// Components
import { Qualification, Events, Selectors, Templates, Labels } from "./root";

class CandidateCertification extends Qualification {
    constructor(
        models: commonModel.Models,
        events: Events,
        selectors: Selectors,
        templates: Templates,
        labels: Labels
    ) {
        super(models, events, selectors, templates, labels);

        skuid.events.subscribe(events.wishCouldSave, wishCouldSaveOver(models, events, this._dialogs.addOrEdit));

        // interface ModelSavedData {
        //     models: {
        //         PersistentEducationModel?: skuid.model.Model;
        //         EphemeralEducationModel?: skuid.model.Model;
        //     };
        // }

        // HACK: Should be removed when timeline component can handle multiple instances of itself on a single page.
        skuid.events.subscribe("models.saved", (data: certificationData.ModelSavedDataCertification) => {
            optional.withSomeOrElse(
                                   optional.of(data.models.PersistentCertificationModel),
                                   persistentCertificationModel => {
                                       skuid.events.publish(events.modelHasUpdated)
                                   },
                                   none => {

                                   }
                               );
            // optional.withSomeOfEitherOrElse(
            //     optional.of(data.models.PersistentCertificationModel),
            //     optional.of(data.models.EphemeralCertificationModel),
            //     persistentCertificationModel => skuid.events.publish(events.modelHasUpdated),
            //     ephemeralCertificationModel => skuid.events.publish(events.modelHasUpdated),
            //     core.ignore
            // );
        });
    }
}

export function candidateCertificationAs() {
    return new CandidateCertification(
        {
            ephemeral: optional.asSomeOrFail(
                skuidModelHelpers.getModelOpt("EphemeralCertificationModel"),
                "EphemeralCertificationModel not found!"
            ),
            persistent: optional.asSomeOrFail(
                skuidModelHelpers.getModelOpt("PersistentCertificationModel"),
                "PersistentCertificationModel not found!"
            )
        },
        {
            wishCouldAdd: "ats.candidateCertification.wishCouldAdd",
            wishCouldEdit: "ats.candidateCertification.wishCouldEdit",
            wishCouldSave: "ats.candidateCertification.wishCouldSave",
            confirmDelete: "ats.candidateCertification.confirmDelete",
            wishCouldDelete: "ats.candidateCertification.wishCouldDelete",
            modelHasUpdated: "ats.candidateCertification.modelHasUpdated",
            addOrEditModalHasOpened: "ats.candidateCertification.addOrEditModalHasOpened",
            addOrEditModalHasClosed: "ats.candidateCertification.addOrEditModalHasClosed",
            confirmDeleteModalHasOpened: "ats.candidateCertification.confirmDeleteModalHasOpened",
            confirmDeleteModalHasClosed: "ats.candidateCertification.confirmDeleteModalHasClosed"
        },
        {
            timelineEditButton: ".timeline_editCertificationButton",
            timelineDeleteButton: ".qualification__deleteButton—certification",
            modalCancelButton: "#ats_candidateCertification-modalCancelButton",
            modalCloseButton: "#ats_candidateCertification-modalCloseButton",
            modalSaveButton: "#ats_candidateCertification-modalSaveButton"
        },
        {
            addOrEdit: certificationData.certification.addOrEditTemplate,
            confirmDelete: commonUI.templates.confirmDeleteTemplate
        },
        {
            add: skuidUIHelpers.labelFromOrFail("ATS_ADD_CERTIFICATION_HEADER"),
            edit: skuidUIHelpers.labelFromOrFail("ATS_EDIT_CERTIFICATION_HEADER")
        }
    );
}

function wishCouldSaveOver(models: commonModel.Models, events: Events, dialog: JQuery) {
    return function wishCouldSave() {
      commonModel.saveChangesOver(models, events.modelHasUpdated, dialog)();
    };
}
