// Library
import * as core from "../../library/core";
import * as optional  from "../../library/optional";

// Data
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";
import * as qualificationData from "./data";

// Helpers
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

// Components
import { Qualification, Events, Selectors, Templates, Labels } from "./root";

class CandidateEducation extends Qualification {
    constructor(
        models: commonModel.Models,
        events: Events,
        selectors: Selectors,
        templates: Templates,
        labels: Labels
    ) {
        super(models, events, selectors, templates, labels);

        // TODO: Shouldn't reference the element by string here
        $("#ats-candidateEducation-add-partTwo").find(".nx-messages").remove();

        skuid.events.subscribe(events.wishCouldSave, wishCouldSaveOver(models, events, this._dialogs.addOrEdit));

        skuid.events.subscribe("models.saved", (data: qualificationData.ModelSavedDataEducation) => {
             optional.withSomeOrElse(
                                    optional.of(data.models.PersistentEducationModel),
                                    persistentEducationModel => {
                                        skuid.events.publish(events.modelHasUpdated)
                                    },
                                    none => {

                                    }
                                );
            // optional.withSomeOfEitherOrElse(
            //     optional.of(data.models.PersistentEducationModel),
            //     optional.of(data.models.EphemeralEducationModel),
            //     persistentEducationModel => skuid.events.publish(events.modelHasUpdated),
            //     ephemeralEducationModel => skuid.events.publish(events.modelHasUpdated),
            //     core.ignore
            // );
        });
    }
}

export function candidateEducationAs() {
    return new CandidateEducation(
        {
            ephemeral: optional.asSomeOrFail(
                skuidModelHelpers.getModelOpt("EphemeralEducationModel"),
                "EphemeralEducationModel not found!"
            ),
            persistent: optional.asSomeOrFail(
                skuidModelHelpers.getModelOpt("PersistentEducationModel"),
                "PersistentEducationModel not found!"
            )
        },
        {
            wishCouldAdd: "ats.candidateEducation.wishCouldAdd",
            wishCouldEdit: "ats.candidateEducation.wishCouldEdit",
            wishCouldSave: "ats.candidateEducation.wishCouldSave",
            confirmDelete: "ats.candidateEducation.confirmDelete",
            wishCouldDelete: "ats.candidateEducation.wishCouldDelete",
            modelHasUpdated: "ats.candidateEducation.modelHasUpdated",
            addOrEditModalHasOpened: "ats.candidateEducation.addOrEditModalHasOpened",
            addOrEditModalHasClosed: "ats.candidateEducation.addOrEditModalHasClosed",
            confirmDeleteModalHasOpened: "ats.candidateEducation.confirmDeleteModalHasOpened",
            confirmDeleteModalHasClosed: "ats.candidateEducation.confirmDeleteModalHasClosed"
        },
        {
            timelineEditButton: ".timeline_editEducationButton",
            timelineDeleteButton: ".qualification__deleteButton—education",
            modalCancelButton: "#ats_candidateEducation-modalCancelButton",
            modalCloseButton: "#ats_candidateEducation-modalCloseButton",
            modalSaveButton: "#ats_candidateEducation-modalSaveButton"
        },
        {
            addOrEdit: qualificationData.education.addOrEditTemplate,
            confirmDelete: commonUI.templates.confirmDeleteTemplate
        },
        {
            add: skuid.label.read("ATS_ADD_EDUCATION_HEADER", "Add Education"),
            edit: skuid.label.read("ATS_EDIT_EDUCATION_HEADER", "Edit Education")
        }
    );
}

function wishCouldSaveOver(models: commonModel.Models, events: Events, dialog: JQuery) {
    return function wishCouldSave() {
        let editor;
        let editors = models.ephemeral.registeredEditors;
        $.each(editors, function(key, value) {
            editor = value;
            return false;
        });
        let gradYear = skuidModelHelpers.getFieldValueOfFirstRow<string>(models.ephemeral, 'Graduation_Year__c');

        //need to set the organization name from LOV value
        let OrgNameOpt = optional.of<{Text_Value__c: string;}>(skuidModelHelpers.getFieldValueOfFirstRow<{Text_Value__c: string;}>(models.ephemeral, 'Education_Organization__r'));

        optional.withSomeOrElse(
            OrgNameOpt,
            orgName => {
                optional.withSomeOrElse(
                    optional.of<string>(orgName.Text_Value__c),
                    value => {
                        models.ephemeral.updateRow(models.ephemeral.getFirstRow(), {'Organization_Name__c': value});
                    },
                    core.ignore
                );
            },
            core.ignore
        );

        if(gradYear){
          let pattern = /^(19[4-9]\d|20[0-4]\d|2050)$/;
          if (!pattern.test(gradYear)) {
              //Clear prior messages before presenting new one, in case prior message was acknowledged.
              skuidUIHelpers.displayEditorMessage(editor, 'ATS_GRAD_YEAR_ERROR', 'ERROR');
          } else {
            commonModel.saveChangesOver(models, events.modelHasUpdated, dialog)();
          }
        } else {
          commonModel.saveChangesOver(models, events.modelHasUpdated, dialog)();
        }
    };
}
