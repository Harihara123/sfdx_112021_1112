// Library
import * as lc from "../../library/core";
import * as le from "../../library/error";
import * as lu from "../../library/ui";

// helpers
import * as skuidUIHelpers from "../../helpers/skuid/ui";

// Data
import * as commonUI from "../common/ui";

// Components
import { candidateEducationAs } from "../candidateQualifications/candidateEducation";
import { candidateCertificationAs } from "../candidateQualifications/candidateCertification";

export function candidateQualificationsOver(view: commonUI.page.Page) {
    return function candidateQualifications() {
        commonUI.page.viaSomePage(view, {
            caseOfLandingPage() { lc.nothing; },
            caseOfDetailsPage() {
                // Set page title
                lu.setPageTitle(`${skuidUIHelpers.labelFromOrFail("ATS_EDUCATION")} & ${skuidUIHelpers.labelFromOrFail("ATS_CERTIFICATION")}`);
            },
            caseOfNeither(reason) { le.raise(reason) }
        });
        const candidateEducation = candidateEducationAs();
        const candidateCertification = candidateCertificationAs();
    };
};
