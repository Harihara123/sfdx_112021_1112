export module education {

    export let addOrEditTemplate = `
        <popup title="{{$Label.ATS_ADD_EDUCATION_HEADER}}" width="90%">
            <components>
            <wrapper uniqueid="sk-3JTC4O-588" cssclass="">
                <components>
                    <template multiple="false" uniqueid="sk-3JTETh-596" allowhtml="true">
                    <contents>&lt;div class="slds-text-body--small"&gt;*{{$Label.ATS_REQUIRED_FIELDS_IN_RED}}&lt;/div&gt;</contents>
                    </template>
                    </components>
                    <styles>
                    <styleitem type="background"/>
                    <styleitem type="border"/>
                    <styleitem type="size"/>
                    </styles>
                </wrapper>
                <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralEducationModel" buttonposition="" uniqueid="ats_CandidateEducationAdd-partOne" mode="edit" layout="above">
                    <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                        <column behavior="fit" verticalalign="top">
                            <sections>
                                <section title="New Section" collapsible="no" showheader="false">
                                <fields>
                                <field id="Graduation_Year__c" valuehalign="" type="" maxdisplaycharacters="">
                                <renderconditions logictype="and" onhidedatabehavior="keep"/>
                                <enableconditions logictype="and"/>
                                <label>{{$Label.ATS_YEAR}} &lt;span class="smallHelpText"&gt;{{$Label.ATS_GRAD_YEAR_HELP}}&lt;/span&gt;</label>
                                </field>
                                &lt;div class="smallHelpText"&gt;{{
                                    $Label.ATS_GRAD_YEAR_HELP}&lt;/div&gt;
                                    </fields>
                                </section>
                            </sections>
                        </column>
                        <column behavior="fit" verticalalign="top">
                            <sections>
                                <section title="New Section" collapsible="no" showheader="false">
                                    <fields>
                                        <field id="Degree_Level__c" valuehalign="" type="" required="true" addnoneoption="true">
                                            <renderconditions logictype="and" onhidedatabehavior="keep"/>
                                            <enableconditions/>
                                            <label>{{$Label.ATS_DEGREE}}</label>
                                        </field>
                                    </fields>
                                </section>
                            </sections>
                        </column>
                        <column behavior="fit" verticalalign="top">
                            <sections>
                                <section title="New Section" collapsible="no" showheader="false">
                                <fields>
                                   <field id="Education_Organization__c" valuehalign="" type="" optionsource="" displaytemplate="{{Text_Value__c}}" searchtemplate="{{Text_Value__c}}">
                                      <searchfields>
                                         <searchfield query="true" return="true" show="true" field="Text_Value__c" operator="contains"/>
                                      </searchfields>
                                      <filters>
                                         <filter type="fieldvalue" operator="=" enclosevalueinquotes="true" field="LOV_Name__c" value="Education  Institutions"/>
                                      </filters>
                                      <label>{{$Label.ATS_SCHOOL_NAME}}</label>
                                      <renderconditions logictype="and" onhidedatabehavior="keep"/>
                                      <enableconditions logictype="and"/>
                                   </field>
                                </fields>
                                </section>
                            </sections>
                        </column>
                    </columns>
                    <renderconditions logictype="and"/>
                    <conditions/>
                </basicfieldeditor>
                <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralEducationModel" buttonposition="" uniqueid="ats-candidateEducation-add-partTwo" mode="edit" layout="above">
                    <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                        <column behavior="flex" verticalalign="top" ratio="1" minwidth="100px">
                            <sections>
                                <section title="Section A" collapsible="no" showheader="false">
                                    <fields>
                                        <field id="Major__c" valuehalign="" type="">
                                            <label>{{$Label.ATS_ED_MAJOR}}</label>
                                        </field>
                                        <field id="Honor__c" valuehalign="" type="">
                                            <label>{{$Label.ATS_HONORS}}</label>
                                        </field>
                                        <field id="Notes__c" valuehalign="" type="" displayrows="10">
                                            <label>{{$Label.ATS_NOTES}}</label>
                                        </field>
                                    </fields>
                                </section>
                            </sections>
                        </column>
                    </columns>
                </basicfieldeditor>
            </components>
        </popup>
    `;

    export let timelineXML = `
        <connectedcomponents__timeline xmlns="http://www.w3.org/1999/xhtml" uniqueid="sk-r9TFc-200" timeLineModelId="PersistentEducationModel" shouldDisplayMessage="false" templateParamsjsonId="{} ">
            <components></components>
            <styles>
                <styleitem type="background" bgtype="none"></styleitem>
                <styleitem type="border"></styleitem>
                <styleitem type="size"></styleitem>
            </styles>
            <renderconditions logictype="and" logic="1"></renderconditions>
        </connectedcomponents__timeline>
    `;

    export let detailsTimelineJSON = {
        "line1": "{{{Graduation_Year__c}}}<button class='timeline_editEducationButton slds-button slds-button--brand slds-button--small' data-rowId='{{{Id}}}'>{{$Label.ATS_EDIT}}</button>",
        "line2": "{{{Degree_Level__c}}}",
        "line3": "{{{Major__c}}} | {{{Organization_Name__c}}} | {{{Honor__c}}}",
        "line4": "{{{Notes__c}}}"
    };

    export let summaryTimelineJSON = {
        "line1": "{{{Graduation_Year__c}}}",
        "line2": "{{{Degree_Level__c}}}",
        "line3": "{{{Major__c}}} | {{{Organization_Name__c}}} | {{{Honor__c}}}",
        "line4": "{{{Notes__c}}}"
    };

}



export module certification {

    export let addOrEditTemplate = `
        <popup title="{{$Label.ATS_EDIT_CERTIFICATION_HEADER}}" width="90%">
            <components>
                <wrapper uniqueid="sk-3JTC4O-588" cssclass="">
                    <components>
                        <template multiple="false" uniqueid="sk-3JTETh-534536" allowhtml="true">
                        <contents>&lt;div class="slds-text-body--small"&gt;*{{$Label.ATS_REQUIRED_FIELDS_IN_RED}}&lt;/div&gt;</contents>
                        </template>
                    </components>
                    <styles>
                    <styleitem type="background"/>
                    <styleitem type="border"/>
                    <styleitem type="size"/>
                    </styles>
                </wrapper>
                <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="false" model="EphemeralCertificationModel" buttonposition="" uniqueid="ats_CandidateCertification-edit" mode="edit" layout="above">
                    <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                        <column ratio="1" minwidth="300px" behavior="flex" verticalalign="top">
                            <sections>
                                <section title="New Section" collapsible="no" showheader="false">
                                    <fields>
                                        <field id="Graduation_Year__c" valuehalign="" type="">
                                            <label>{{$Label.ATS_YEAR}}</label>
                                        </field>
                                    </fields>
                                </section>
                            </sections>
                        </column>
                        <column ratio="1" minwidth="300px" behavior="flex">
                            <sections>
                                <section title="Section B" collapsible="no" showheader="false">
                                    <fields>
                                        <field id="Certification__c" valuehalign="" type="" required="true">
                                            <label>{{$Label.ATS_CERTIFICATION}}</label>
                                        </field>
                                    </fields>
                                </section>
                            </sections>
                        </column>
                        <column behavior="flex" ratio="1" minwidth="300px" verticalalign="top">
                            <sections>
                                <section title="New Section" collapsible="no" showheader="false">
                                    <fields>
                                        <field id="Notes__c" valuehalign="" type="">
                                            <label>{{$Label.ATS_NOTES}}</label>
                                        </field>
                                    </fields>
                                </section>
                            </sections>
                        </column>
                    </columns>
                    <renderconditions logictype="and"/>
                    <conditions/>
                </basicfieldeditor>
            </components>
        </popup>
    `;

    export let timelineXML = `
        <connectedcomponents__timeline xmlns="http://www.w3.org/1999/xhtml" uniqueid="ats_candidateCertification-timeline" timeLineModelId="PersistentCertificationModel" shouldDisplayMessage="false" templateParamsjsonId="{}">
            <components>
            </components>
            <styles>
                <styleitem type="background" bgtype="none">
                </styleitem>
                <styleitem type="border">
                </styleitem>
                <styleitem type="size">
                </styleitem>
            </styles>
            <renderconditions logictype="and" logic="1">
            </renderconditions>
        </connectedcomponents__timeline>
    `;

    export let detailsTimelineJSON = {
        "line1": "{{{Graduation_Year__c}}}<button class='timeline_editCertificationButton slds-button slds-button--brand slds-button--small' data-rowId='{{{Id}}}'>{{$Label.ATS_EDIT}}</button>",
        "line2": "{{{Certification__c}}}",
        "line3": "{{{Notes__c}}}"
    };

    export let summaryTimelineJSON = {
        "line1": "{{{Graduation_Year__c}}}",
        "line2": "{{{Certification__c}}}",
        "line3": "{{{Notes__c}}}"
    };

}

export interface ModelSavedDataEducation {
    models: {
        PersistentEducationModel?: skuid.model.Model;
        EphemeralEducationModel?: skuid.model.Model;
    };
}

export interface ModelSavedDataCertification {
    models: {
        PersistentCertificationModel?: skuid.model.Model;
        EphemeralCertificationModel?: skuid.model.Model;
    };
}
