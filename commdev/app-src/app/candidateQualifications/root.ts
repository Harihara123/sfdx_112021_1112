// Library
import * as array from "../../library/array";
import * as core from "../../library/core";
import * as optional from "../../library/optional";
import * as text from "../../library/text";
import * as ui from "../../library/ui";

// Common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

// Data
import * as qualificationData from "./data";

// Helpers
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

export interface Events {
    wishCouldAdd: string;
    wishCouldEdit: string;
    wishCouldSave: string;
    confirmDelete: string;
    wishCouldDelete: string;
    modelHasUpdated: string;
    addOrEditModalHasOpened: string;
    addOrEditModalHasClosed: string;
    confirmDeleteModalHasOpened: string;
    confirmDeleteModalHasClosed: string;
}

interface Dialogs {
    addOrEdit: JQuery;
    confirmDelete: JQuery;
}

export interface Labels {
    add: string;
    edit: string;
}

export interface Templates {
    addOrEdit: string;
    confirmDelete: string;
}

interface Elements {
    $timelineEditButton: JQuery;
    $timelineDeleteButton: JQuery;
    $modalCancelButton: JQuery;
    $modalCloseButton: JQuery;
    $modalSaveButton: JQuery;
}

export interface Selectors {
    timelineEditButton: string;
    timelineDeleteButton: string;
    modalCancelButton: string;
    modalCloseButton: string;
    modalSaveButton: string;
}

interface DomEvents {
    registerListeners: () => void;
    registerModalListeners: () => void;
}

export class Qualification {

    protected _dialogs: Dialogs;
    private _$dialog: JQuery;
    private _domEvents: DomEvents;

    constructor(
        private _models: commonModel.Models,
        private _events: Events,
        private _selectors: Selectors,
        private _templates: Templates,
        private _labels: Labels
    ) {
        this._dialogs = {
            addOrEdit: jqueryUIHelper.dialog.from(
                this._templates.addOrEdit,
                [
                    {
                        text: commonUI.labels.generic.cancelButton,
                        class: "slds-button slds-button--neutral",
                        icons: { primary: "" },
                        click: () => {
                            this._dialogs.addOrEdit.dialog("close");
                        },
                        showText: true
                    },
                    {
                        text: commonUI.labels.generic.saveButton,
                        class: "slds-button slds-button--brand",
                        icons: { primary: "" },
                        click: () => {
                            skuid.events.publish(this._events.wishCouldSave);
                            //this._dialogs.addOrEdit.dialog("close");
                        },
                        showText: true
                    }
                ],
                this._events.addOrEditModalHasOpened,
                this._events.addOrEditModalHasClosed,
                false
            ),
            confirmDelete: jqueryUIHelper.dialog.from(
                this._templates.confirmDelete,
                [
                    {
                        text: commonUI.labels.generic.cancelButton,
                        class: "slds-button slds-button--neutral",
                        icons: { primary: "" },
                        click: () => {
                            this._dialogs.confirmDelete.dialog("close");
                        },
                        showText: true
                    },
                    {
                        text: commonUI.labels.generic.deleteButton,
                        class: "slds-button slds-button--brand",
                        icons: { primary: "" },
                        click: () => {
                            skuid.events.publish(this._events.wishCouldDelete, [{
                                rowId: this._dialogs.confirmDelete.attr("data-rowId")
                            }]);
                            this._dialogs.confirmDelete.dialog("close");
                        },
                        showText: true
                    }
                ],
                this._events.addOrEditModalHasOpened,
                this._events.addOrEditModalHasClosed,
                false
            )
        };
        this._domEvents = registerDomEventsOver(this._selectors, this._events);
        this._domEvents.registerListeners();
        registerEventSubscriptions(this._events, this._models, this._domEvents, this._dialogs, this._labels);
    }
}

function wishCouldAddOver(ephemeralModel: skuid.model.Model, $dialog: JQuery, title: string, events: Events) {
    return function wishCouldAdd() {
        ephemeralModel.abandonAllRows();
        optional.withSomeOrFail(
            skuidModelHelpers.getConditionOpt(ephemeralModel, "Id", false),
            ephemeralModel.deactivateCondition,
            text.emptyString
        );
        ephemeralModel.createRow({});
        $dialog.dialog({ title: title });
        $dialog.dialog("open");
    };
}

function wishCouldEditOver(ephemeralModel: skuid.model.Model, $dialog: JQuery, title: string, events: Events) {
    return function wishCouldEdit(data: { rowId: string }) {
        optional.withSomeOrFail(
            skuidModelHelpers.getConditionOpt(ephemeralModel, "Id", false),
            condition => {
                ephemeralModel.abandonAllRows();
                ephemeralModel.deactivateCondition(condition);
                ephemeralModel.setCondition(condition, data.rowId);
                ephemeralModel.activateCondition(condition);
                ephemeralModel.updateData().then(() => {
                    $dialog.dialog({ title: title });
                    $dialog.dialog("open");
                    //skuid.events.publish(events.modelHasUpdated);
                });
            },
            text.emptyString
        );
    };
}

function confirmDeleteOver($confirmDeleteDialog: JQuery) {
    return function confirmDelete(data: { rowId: string }) {
        $confirmDeleteDialog.dialog("open");
        $confirmDeleteDialog.attr("data-rowId", data.rowId);
    }
}

function wishCouldDeleteOver(persistentModel: skuid.model.Model, events: Events) {
    return function wishCouldDelete(data: { rowId: string }) {
        optional.withSomeOrFail(
            skuidModelHelpers.getRowOpt(persistentModel, data.rowId),
            row => {
                persistentModel.deleteRow(row);
                skuidModelHelpers.save(persistentModel)
                    .then(result => {
                        skuidModelHelpers.updateData(persistentModel)
                            .then(core.ignore)
                            .catch((error: string) => core.scheduleError(error));
                    })
                    .catch((error: string) => core.scheduleError(error));
            },
            text.emptyString
        );
    };
}

function modelHasUpdatedOver(domEvents: DomEvents) {
    return function modelHasUpdated() {
        domEvents.registerListeners();
    };
}

function elementsFrom(selectors: Selectors): Elements {
    return {
        $timelineEditButton: $(selectors.timelineEditButton),
        $timelineDeleteButton: $(selectors.timelineDeleteButton),
        $modalCancelButton: $(selectors.modalCancelButton),
        $modalCloseButton: $(selectors.modalCloseButton),
        $modalSaveButton: $(selectors.modalSaveButton)
    };
}

function registerDomEventsOver(selectors: Selectors, events: Events): DomEvents {

    function registerListeners() {
        let elements = elementsFrom(selectors);

        elements.$timelineEditButton.off("click").on(
            "click",
            (event) => skuid.events.publish(events.wishCouldEdit, [{
                rowId: $(event.target).attr("data-rowId")
            }])
        );

        elements.$timelineDeleteButton.off("click").on(
            "click",
            () => skuid.events.publish(events.confirmDelete, [{
                rowId: $(event.target).attr("data-rowId")
            }])
        );
    }

    function registerModalListeners() {
        let elements = elementsFrom(selectors);

        array.fromTwo(elements.$modalCancelButton, elements.$modalCloseButton).map($element => {
            $element.on("click", () => {});
        });
    }

    return {
        registerListeners: registerListeners,
        registerModalListeners: registerModalListeners
    };

}

function registerEventSubscriptions(
    events: Events,
    models: commonModel.Models,
    domEvents: DomEvents,
    dialogs: Dialogs,
    labels: Labels
) {
    skuid.events.subscribe(events.wishCouldAdd, wishCouldAddOver(
        models.ephemeral,
        dialogs.addOrEdit,
        labels.add,
        events
    ));
    skuid.events.subscribe(events.wishCouldEdit, wishCouldEditOver(
        models.ephemeral,
        dialogs.addOrEdit,
        labels.edit,
        events
    ));

    //events.wishCouldSave now subscribed to from/implemented in the "child" classes (candidateEducation/candidateCertification)
    skuid.events.subscribe(events.confirmDelete, confirmDeleteOver(dialogs.confirmDelete));
    skuid.events.subscribe(events.wishCouldDelete, wishCouldDeleteOver(models.persistent, events));
    skuid.events.subscribe(events.modelHasUpdated, modelHasUpdatedOver(domEvents));

    // TODO: the timeline should emit an event on refresh that we can listen to, rather than modal closing
    skuid.events.subscribe(events.addOrEditModalHasClosed, () => {
        // TODO: uniqueid should be passed in as an element/property
        skuidUIHelpers.editorFromOrFail($("#ats_CandidateEducationAdd-partOne")).clearMessages();
        domEvents.registerListeners;
    });
}
