public class PrintReferences {

    private final List<Talent_Recommendation__c> references;

    public PrintReferences() {

    string refid =apexpages.currentpage().getparameters().get('refIds');
    list<String> lstRefs = new list<String>();
    lstRefs = refid.split('[,]{1}[\\s]?');
    references = [SELECT Relationship__c, Recommendation_From__r.Name, Recommendation_From__r.Id, Recommendation_From__r.AccountId, Recommendation_From__r.Account.Name, Recommendation_From__r.FirstName, Recommendation_From__r.MiddleName, Recommendation_From__r.LastName, Recommendation_From__r.Company_Name__c, Recommendation_From__r.Phone, Recommendation_From__r.Email, CreatedDate,Completion_Date__c, LastModifiedBy.Name, CreatedById,CreatedBy.Name, CreatedBy.FirstName, CreatedBy.LastName, Recommendation_To__r.Title, Talent_Job_Duration__c, Relationship_Duration__c, Collaboration__c, Collaboration_Description__c, Comments__c, Competence__c, Competence_Description__c, Leadership__c, Leadership_Description__c, Likeable__c, Likeable_Description__c, Outlook__c, Outlook_Description__c,
    Professionalism__c, Professionalism_Description__c, Quality__c, Quality_Description__c, Rating__c, Recommendation__c, Name, Rehire_Status__c, Status__c, Talent_Current_Job__c, Talent_Job_Duties__c, Talent_Job_Title__c, Team_Player__c, Team_Player_Description__c, Trustworthy__c, Trustworthy_Description__c, Workload__c, Workload_Description__c, Interaction_Frequency__c, Recommendation_To__r.FirstName, Recommendation_To__r.LastName, Growth_Opportunities__c, Recommendation_From__r.Title, Talent__c, Talent__r.Name, Recommendation_From__r.HomePhone, Recommendation_From__r.MobilePhone, LastModifiedDate, Id FROM Talent_Recommendation__c WHERE Id IN: lstRefs];
    }

    public Talent_Recommendation__c[] getReferences() {
        return references;
    }
}
