// 3rd party
import { curry } from "ramda";

// Common
import { rowAs } from "../common/model/row";
import { labels } from "../common/ui";
import { confirmDeleteTemplate, confirmInvalidPhoneTemplate } from "../common/ui/templates";
import { viaSomePage, Page } from "../common/ui/page";
import { referencePhoneFields, validatePhoneNumbers } from "../common/ui/validation";

// Data
import { Dialogs, DialogsReference, Elements, Events, Reference, SaveMode, Selectors, selectorsAs } from "./data";

// Helpers
import { dialog } from "../../helpers/jquery-ui-helper";
import { getConditionOpt, getFieldValueOfFirstRow, getModelOpt, save } from "../../helpers/skuid/model";

// Library
import { accessors, bounce, fail, scheduleError } from "../../library/core";
import { asSomeOrFail, withSomeOrElse, withSomeOrFail } from "../../library/optional";
import { emptyString } from "../../library/text";
import { getQueryParamAtOpt, getQueryParamAtOrFail } from "../../library/utils";

// Wiring
import { wireDom, wiring } from "./wiring";


export const candidateReferenceFrom = curry((page: Page, _: {}) => {
    viaSomePage(page, {
        caseOfLandingPage: caseOfReferenceList,
        caseOfDetailsPage: caseOfAddEditView,
        caseOfNeither: (reason: string) => fail<void>(reason)
    });
    // HACK: This should be in wiring!
    skuid.events.subscribe("ats.candidateReference.add.edit.cancel.redirect", redirectToSourcePage);
});

const caseOfReferenceList = () => {
    let modelRow = rowAs();
    let reference = referenceAs();

    // Wiring
    wireDom({selectors: reference.selectors, events: reference.events, modelRow}, {});
    wiring({reference, modelRow, selectors: reference.selectors});
};

const caseOfAddEditView = () => {
    document.title = skuid.label.read("ATS_ADD_EDIT_REFERENCE", "ADD/EDIT REFERENCE");
    // HACK: These should be in wiring!
    skuid.events.subscribe("ats.candidateReference.add.validate.save", () => onAddEditReference(SaveMode.ADD));
    skuid.events.subscribe("ats.candidateReference.edit.validate.save", () => onAddEditReference(SaveMode.EDIT));
};

const onAddEditReference = <a>(mode: SaveMode) => {
    const refModel = asSomeOrFail(getModelOpt("TalentReference"), emptyString);
    const refContactModel = asSomeOrFail(getModelOpt("ReferenceContact"), emptyString);
    const refAccountModel = asSomeOrFail(getModelOpt("ReferenceAccount"), emptyString);
    const recordTypeAcctModel = asSomeOrFail(getModelOpt("RecordTypeAccount"), emptyString);
    const recordTypeContactModel = asSomeOrFail(getModelOpt("RecordTypeContact"), emptyString);

    let refRow = refModel.getFirstRow();
    let refContactRow = refContactModel.getFirstRow();
    let refAcctRow = refAccountModel.getFirstRow();

    //Done as the first task for both add/edit flows
    let firstName = getFieldValueOfFirstRow(refContactModel, "FirstName");
    let lastName = getFieldValueOfFirstRow(refContactModel, "LastName");
    let fullName = firstName + " " + lastName;
    refAccountModel.updateRow(refAcctRow, {'Name': fullName});

    switch(mode) {
        //ADD Mode
        case SaveMode.ADD:
        let rtaId = getFieldValueOfFirstRow(recordTypeAcctModel, "Id");
        let refAcctId = getFieldValueOfFirstRow(refAccountModel, "Id");
        let rtcId = getFieldValueOfFirstRow(recordTypeContactModel, "Id");
        var currentDate = skuid.time.getSFDate(new Date());
        let rcId = getFieldValueOfFirstRow(refContactModel, "Id");

        refAccountModel.updateRow(refAcctRow, {'Talent_Status__c': 'Active'});
        refAccountModel.updateRow(refAcctRow, {'RecordTypeId': rtaId});

        refContactModel.updateRow(refContactRow, {'AccountId': refAcctId});
        refContactModel.updateRow(refContactRow, {'RecordTypeId': rtcId});

        refModel.updateRow(refRow, {'Talent__c': skuid.page.params.id});
        refModel.updateRow(refRow, {'Completion_Date__c': currentDate});
        refModel.updateRow(refRow, {'Recommended_By__c': refAcctId});

        refModel.updateRow(refRow, {'Recommendation_From__c': rcId});

        //EDIT Mode
        case SaveMode.EDIT:
        //EDIT mode only does common task from above, of setting full name on Account.
    }
    onReferenceValidatei18nPhones();
};

/*
Check all phone numbers are valid based on i18n formatter.
*/
const onReferenceValidatei18nPhones = () => {
    const refModel = asSomeOrFail(getModelOpt("TalentReference"), emptyString);
    const refContactModel = asSomeOrFail(getModelOpt("ReferenceContact"), emptyString);
    const refAccountModel = asSomeOrFail(getModelOpt("ReferenceAccount"), emptyString);
    let invalidPhoneList = validatePhoneNumbers(
        referencePhoneFields, refContactModel
    );
    let template = confirmInvalidPhoneTemplate.replace("_replace_token_", invalidPhoneList);
    let dialogs: Dialogs = {
        $confirmSave: dialog.from(
            template,
            [
                {
                    text: labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: () => skuid.events.publish("ats.reference.phone.validation.cancel"),
                    showText: true
                },
                {
                    text: labels.generic.saveButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: () => skuid.events.publish("ats.reference.phone.validation.save"),
                    showText: true
                }
            ],
            "",
            "",
            false
        )
    };

    // HACK: These should be in wiring!
    skuid.events.subscribe("ats.reference.phone.validation.cancel", onCancelPhoneDialog(dialogs));
    skuid.events.subscribe("ats.reference.phone.validation.save", onSavePhoneDialog(dialogs));

    invalidPhoneList.length > 0
        ? dialog.open(dialogs.$confirmSave)
        : updateAndSaveAll(refModel, refContactModel, refAccountModel);
};

const onCancelPhoneDialog = curry((dialogs: Dialogs, _: {}) => {
    dialog.close(dialogs.$confirmSave);
});

const onSavePhoneDialog = curry((dialogs: Dialogs, _: {}) => {
    const refModel = asSomeOrFail(getModelOpt("TalentReference"), emptyString);
    const refContactModel = asSomeOrFail(getModelOpt("ReferenceContact"), emptyString);
    const refAccountModel = asSomeOrFail(getModelOpt("ReferenceAccount"), emptyString);
    dialog.close(dialogs.$confirmSave);
    updateAndSaveAll(refModel, refContactModel, refAccountModel);
});

const updateAndSaveAll = (
    refModel: skuid.model.Model,
    refContactModel: skuid.model.Model,
    refAccountModel: skuid.model.Model
) => {
    save(refAccountModel)
        .then(result => {
            save(refContactModel)
                .then(result => {
                    if (refContactModel.data && refContactModel.data[0].Id
                        && refContactModel.data[0].Id.length > 10) {
                                save(refModel)
                                    .then(result => {
                                        redirectToSourcePage();
                                    })
                                    .catch((error: string) => scheduleError(error));
                    }
                }
            )
        }
    )
}

const redirectToSourcePage = () => {
    let referenceId = withSomeOrElse(
        getQueryParamAtOpt("referenceId"),
        refId => {
            return "?referenceId=" + refId.value + "&";
        },
        none => {
            return '?';
        }
    );

    window.location.replace(
        `${window.location.origin}`
        + "/apex/c__" + getQueryParamAtOrFail("sourcepage").value
        + referenceId + 'id='
        + `${accessors.valueOf(getQueryParamAtOrFail("id")) }`
    );
};

const referenceAs = (): Reference => {
    const model: skuid.model.Model = asSomeOrFail(getModelOpt("References"), emptyString);
    const condition = withSomeOrFail(getConditionOpt(model, "Talent__r.Id", false), bounce, emptyString);
    const events: Events = {
        model: {
            wishCouldDelete: "ats.candidateReference.wishCouldDelete",
            wishCouldConfirmDelete: "ats.candidateReference.wishCouldConfirmDelete",
            wishCouldCancel: "ats.candidateReference.wishCouldCancel",
            hasUpdated: "ats.candidateReference.modelHasUpdated"
        },
        ui: {
            confirmDeleteDialogHasOpened: "ats.candidateReference.confirmDeleteDialogHasOpened",
            confirmDeleteDialogHasClosed: "ats.candidateReference.confirmDeleteDialogHasClosed",
            wishCouldDownload: "ats.candidateReference.wishCouldDownload"
        }
    };
    const dialogs: DialogsReference = {
        $confirmDelete: dialog.from(
            confirmDeleteTemplate,
            [
                {
                    text: labels.generic.cancelButton,
                    class: "slds-button slds-button--neutral",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldCancel),
                    showText: true
                },
                {
                    text: labels.generic.deleteButton,
                    class: "slds-button slds-button--brand",
                    icons: { primary: "" },
                    click: () => skuid.events.publish(events.model.wishCouldConfirmDelete),
                    showText: true
                }
            ],
            events.ui.confirmDeleteDialogHasOpened,
            events.ui.confirmDeleteDialogHasClosed,
            false
        )
    };
    const selectors = selectorsAs();

    return { model, condition, events, dialogs, selectors };
};
