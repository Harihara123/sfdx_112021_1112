export enum SaveMode { ADD, EDIT }

interface Selector { selector: string; context: string; }

export interface Selectors {
    modalCancelButton: string;
    modalCloseButton: string;
    referenceCheckBox: Selector;
    referenceDeleteButton: string;
    referenceDownloadButton: string;
}

export const selectorsAs = (): Selectors => ({
    modalCancelButton: "#ats_candidateReference-modalCancelButton",
    modalCloseButton: "#ats_candidateReference-modalCloseButton",
    referenceCheckBox: { selector: "input[type='checkbox']", context: ".referenceCheckBox" },
    referenceDeleteButton: ".deleteReferenceButton",
    referenceDownloadButton: ".downloadReferenceButton"
});

export interface Elements {
    $modalCancelButton: JQuery;
    $modalCloseButton: JQuery;
    $referenceCheckBox: JQuery;
    $referenceDeleteButton: JQuery;
    $referenceDownloadButton: JQuery;
}

export const elementsFrom = (selectors: Selectors): Elements => ({
    $modalCancelButton: $(selectors.modalCancelButton),
    $modalCloseButton: $(selectors.modalCloseButton),
    $referenceCheckBox: $(selectors.referenceCheckBox.selector, selectors.referenceCheckBox.context),
    $referenceDeleteButton: $(selectors.referenceDeleteButton),
    $referenceDownloadButton: $(selectors.referenceDownloadButton)
});

export interface EventModelReference {
    wishCouldDelete: string;
    wishCouldConfirmDelete: string;
    wishCouldCancel: string;
    hasUpdated: string;
}

export interface EventsUIReference {
    confirmDeleteDialogHasOpened: string;
    confirmDeleteDialogHasClosed: string;
    wishCouldDownload: string;
}

export interface DialogsReference {
    $confirmDelete: JQuery;
}

export interface Events {
    model: EventModelReference;
    ui: EventsUIReference;
}

export interface Reference {
    model: skuid.model.Model;
    condition: skuid.model.Condition;
    events: Events;
    dialogs: DialogsReference;
    selectors: Selectors;
}

export interface Dialogs {
    $confirmSave: JQuery;
}
