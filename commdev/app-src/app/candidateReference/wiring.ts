// 3rd party
import { curry } from "ramda";

// Common
import { confirmDeleteRowOver, deleteRowOver, Row } from "../common/model/row";

// Data
import { elementsFrom, Events, Selectors, Reference } from "./data";

// Helpers
import { dialog } from "../../helpers/jquery-ui-helper";
import { getModelOpt, getRows } from "../../helpers/skuid/model";
import { removeErrors } from "../../helpers/skuid/ui";

// Library
import { filter, hasAny, reduce } from "../../library/array";
import { map as optMap } from "../../library/optional";


interface WiringParams { reference: Reference; modelRow: Row; selectors: Selectors; }
type Wiring = (params: WiringParams) => void;
export const wiring: Wiring = ({ reference, modelRow, selectors }) => {
    skuid.events.subscribe(
        reference.events.model.wishCouldDelete,
        deleteRowOver(reference.dialogs.$confirmDelete)
    );

    skuid.events.subscribe(
        reference.events.model.wishCouldConfirmDelete,
        confirmDeleteRowOver(
            modelRow,
            reference.model,
            reference.dialogs.$confirmDelete,
            reference.events.model.hasUpdated
        )
    );

    skuid.events.subscribe(
        reference.events.model.wishCouldCancel,
        () => { dialog.closeAll(); removeErrors(); }
    );

    skuid.events.subscribe(reference.events.ui.confirmDeleteDialogHasClosed, wireDom({
        selectors: reference.selectors,
        events: reference.events,
        modelRow
    }));

    skuid.events.subscribe(reference.events.ui.wishCouldDownload, () => {
        let elements = elementsFrom(selectors);
        if (!elements.$referenceDownloadButton.hasClass("ui-button-disabled")) {
            optMap(
                getModelOpt("References"),
                referencesModel => {
                    interface ReferenceRow { Id: string; selected: boolean }
                    let selectedRefIds = getRows<ReferenceRow>(referencesModel)
                        .filter(row => row.selected)
                        .reduce((acc, ref) => `${acc},${ref.Id}`, "")
                        .substr(1);
                    window.open(
                        `${window.location.origin}/apex/PrintReferencesAsPdf?refIds=${selectedRefIds}`,
                        "_blank"
                    );

                    // $("body")
                    //     .append($("<iframe />")
                    //         .hide()
                    //         .attr("src", `${window.location.origin}/apex/PrintReferencesAsPdf?refIds=${selectedRefIds}`)
                    //     ).promise()
                    //     .done(() => {
                    //         let doc = new jsPDF();
                    //         let specialElementHandlers = {
                    //             '#editor': function(element, renderer) {
                    //                 return true;
                    //             }
                    //         };
                    //         doc.fromHTML($('.references__content').html(), 15, 15, {
                    //             'width': 170,
                    //             'elementHandlers': specialElementHandlers
                    //         });
                    //         doc.save("References.pdf");
                    //     });
                }
            );
        }
    });
};

interface WireDomParams { selectors: Selectors; events: Events; modelRow: Row; }
type WireDom = R.CurriedFunction2<WireDomParams, {}, void>;
export const wireDom: WireDom = curry(({ selectors, events, modelRow }, _: {}) => {
    // TODO: Find a way to have this passed in, rather than creating a holding variable
    let elements = elementsFrom(selectors);

    elements.$referenceDeleteButton.off("click").on("click", () => {
        modelRow.setRowId($(event.target).attr("data-rowId"));
        skuid.events.publish(events.model.wishCouldDelete);
    });

    elements.$referenceCheckBox.off("click").on("click", ($event) => {
        let areChecked = elements.$referenceCheckBox.toArray().filter((element: HTMLInputElement) => element.checked);
        if (hasAny(areChecked)) elements.$referenceDownloadButton.removeClass("ui-button-disabled ui-state-disabled");
        else elements.$referenceDownloadButton.addClass("ui-button-disabled ui-state-disabled");
    });
});
