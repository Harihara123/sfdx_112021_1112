// Library
import { fail, ignore }  from "../../library/core";
import { setPageTitle } from "../../library/ui";

// Helpers
import * as skuidUIHelpers from "../../helpers/skuid/ui";

// Data
import * as commonUI from "../common/ui";

// Parts
import { candidateContactFromOver      } from "../candidateContact/candidateContact";
import { candidateActivitiesFromOver   } from "../candidateActivities/candidateActivities";
import { candidateEmploymentFromOver   } from "../candidateEmployment/candidateEmployment";
import { candidateEducationAs          } from "../candidateQualifications/candidateEducation";
import { candidateCertificationAs      } from "../candidateQualifications/candidateCertification";
import { candidateDocumentFromOver     } from "../candidateDocument/candidateDocument";
import { candidateFundamentalsFromOver } from "../candidateFundamentals/candidateFundamentals";

export function candidateSummaryFromOver(view: commonUI.page.Page) {
    return function candidateSummaryFrom() {
        commonUI.page.viaSomePage(view, {
            caseOfLandingPage() {
                // Set the page title
                setPageTitle(commonUI.labels.candidate.summary.title);

                // Load page components
                const candidateContact       = candidateContactFromOver(view)();
                const candidateActivities    = candidateActivitiesFromOver(view)();
                const candidateEmployment    = candidateEmploymentFromOver(view)();
                const candidateEducation     = candidateEducationAs();
                const candidateCertification = candidateCertificationAs();
                const candidateDocument      = candidateDocumentFromOver(view)();
                const candidateFundamentals  = candidateFundamentalsFromOver(view)();
            },
            caseOfDetailsPage: ignore,
            caseOfNeither(reason) { fail(reason); }
        });
    };
}
