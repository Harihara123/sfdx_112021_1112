const activityTimelineJSON = {  
    "event_1":{
        "liClass":"",
        "text":"title{{{Subject}}}"
    },
    "event_2":{
        "liClass":"lineBreakTop",
        "text":"Type: {{{ActivityType}}}"
    },
    "event_3":{
        "liClass":"",
        "text":"{{$Label.ATS_END_DATE}}:requiresDateFormat{{{EndDateTime}}}"
    },
    "event_4":{
        "liClass":"",
        "text":"{{{IsAllDayEvent}}}"
    },
    "event_5":{
        "liClass":"lineBreakTop",
        "text":"{{$Label.ATS_PRE_MEETING}} {{$Label.ATS_NOTES}}: {{{Pre_Meeting_Notes__c}}}"
    },
    "event_7":{
        "liClass":"lineBreakTop",
        "text":"{{$Label.ATS_POST_MEETING}} {{$Label.ATS_NOTES}}: {{{Description}}}"
    },
    "task_1":{
        "liClass":"",
        "text":"title{{{Subject}}}"
    },
    "task_2":{
        "liClass":"lineBreakTop",
        "text":"Type: {{{ActivityType}}}"
    },
    "task_3":{
        "liClass":"",
        "text":"{{$Label.ATS_PRIORITY}}: {{{Priority}}}"
    },
    "task_4":{
        "liClass":"",
        "text":"{{$Label.ATS_STATUS}}: {{{Status}}}"
    },
    "task_5":{
        "liClass":"",
        "text":"{{$Label.ATS_COMPLETED_DATE}}:requiresDateFormat{{{Completed__c}}}"
    },
    "task_6":{
        "liClass":"lineBreakTop",
        "text":" {{$Label.ATS_COMMENT}}: {{{Description}}}"
    },
    "ephemeral_model":{
        "task":"EphemeralTaskModel",
        "event":"EphemeralEventModel"
    },
    "excludebuttons":"true"
};
