// Helpers
import {
    activateConditions,
    deactivateConditions,
    getConditionOpt,
    getModelOpt,
    getRows,
    updateData
} from "../../../helpers/skuid/model";

// Library
import { filter, fromOne, safeHeadOf } from "../../../library/array";
import { bind, Optional, map as optMap } from "../../../library/optional";


export interface Country {
    countryCode: string;
    countryName: string;
}

export interface State {
    stateCode: string;
    stateName: string;
}

export interface City {
    cityCode: string;
    cityName: string;
}

export const willBeStates = (statesModelOpt: Optional<skuid.model.Model>, countryCode: string) => {
    return bind(statesModelOpt, statesModel => {
        // Set and enable URL merge condition
        return optMap(
            getConditionOpt(statesModel, "countryCode", false),
            countryCodeCondition => {
                deactivateConditions(statesModel, fromOne(countryCodeCondition));
                statesModel.setCondition(countryCodeCondition, countryCode);
                activateConditions(statesModel, fromOne(countryCodeCondition))

                // Update the States skuid model
                return updateData(statesModel);
            }
        );
    });
};

export const countriesAs = () => optMap(
    getModelOpt("Countries"),
    model => getRows<Country>(model)
);

export const countryOptById = (id: string): Optional<Country> => bind(
    countriesAs(),
    countries => safeHeadOf(
        filter(countries, country => country.countryCode === id)
    )
);

export const countryOptByName = (name: string): Optional<Country> => bind(
    countriesAs(),
    countries => safeHeadOf(
        filter(countries, country => country.countryName === name)
    )
);

export const statesAs = () => optMap(
    getModelOpt("States"),
    model => getRows<State>(model)
);

export const stateOptById = (id: string): Optional<State> => bind(
    statesAs(),
    states => safeHeadOf(
        filter(states, state => state.stateCode === id)
    )
);

export const stateOptByName = (name: string): Optional<State> => bind(
    statesAs(),
    states => safeHeadOf(
        filter(states, state => state.stateName === name)
    )
);

export const citiesAs = () => optMap(
    getModelOpt("Cities"),
    model => getRows<City>(model)
);

export const cityOptById = (id: string): Optional<City> => bind(
    citiesAs(),
    cities => safeHeadOf(
        filter(cities, city => city.cityCode === id)
    )
);
