// Library
import * as array from "../../library/array";
import * as core from "../../library/core";
import * as optional from "../../library/optional";
import * as text from "../../library/text";

// Common
import * as commonUI from "./ui";

import * as events from "./model/events";
import * as row from "./model/row";

// helpers
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

interface Models {
    ephemeral: skuid.model.Model;
    persistent: skuid.model.Model;
}

function saveChangesOver(models: Models, modelHasUpdatedEvent: string, $addOrEditDialog: JQuery) {
    return function saveChanges() {
        $.blockUI({ message: commonUI.labels.generic.loadingMessage });
        return skuidModelHelpers.save(models.ephemeral)
            .then(result => {
                skuidModelHelpers.updateDataOfAll(array.fromTwo(models.ephemeral, models.persistent))
                    .then(result => {
                        $.unblockUI();
                        jqueryUIHelper.dialog.close($addOrEditDialog);
                        skuidUIHelpers.removeErrors();
                        skuid.events.publish(modelHasUpdatedEvent);
                    })
                    .catch(core.scheduleError);
            })
            .catch($.unblockUI);
    };
}

function cancelChangesOver(ephemeralModel: skuid.model.Model) {
    return function cancelChanges() {
        jqueryUIHelper.dialog.closeAll();
        skuidUIHelpers.removeErrors();
        ephemeralModel.cancel();
    };
}

export { events, Models, saveChangesOver, cancelChangesOver, row };


//updates sets model condition and udpate the model and return it 
export function setModelConditionsAndLoadModel(modelName: string,
                                               modelConditionMap : ModelConditionMap<string>)
                                                        : Promise<skuid.model.Model> {
        return setModelCondition(modelName,modelConditionMap)
                    .then(
                            model => {
                                    skuidModelHelpers.updateData<skuid.model.Model>(model);
                                    return model;
                            }
                ).catch((error: string) => core.scheduleError<skuid.model.Model>(error));
}

function setModelCondition(model, conditionMap) : Promise<skuid.model.Model> {
        $.each(conditionMap.getKeys(), function(k, val) {
               optional.withSomeOrFail(
                 skuidModelHelpers.getConditionOpt(model, val, false),
                 conditionName => {
                        model.setCondition(
                            conditionName,
                            core.accessors.valueOf(conditionMap.get(val))
                        );

                    },
                    text.emptyString
                );
        });
        return model;
    }

//model map condition it will hold key/value (condition/value)
export class ModelConditionMap<T> {
    private items: { [key: string]: T };

    constructor() {
        this.items = {};
    }

    add(key: string, value: T): void {
        this.items[key] = value;
    }

    has(key: string): boolean {
        return key in this.items;
    }

    get(key: string): T {
        return this.items[key];
    }

    getKeys() {
        return Object.keys(this.items);
    }
}
