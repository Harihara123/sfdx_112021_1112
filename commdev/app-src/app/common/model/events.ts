export interface Events {
    wishCouldAdd?: string;
    wishCouldEdit?: string;
    wishCouldDelete?: string;
    wishCouldConfirmDelete?: string;
    wishCouldSave?: string;
    wishCouldCancel?: string;
    hasUpdated?: string;
}
