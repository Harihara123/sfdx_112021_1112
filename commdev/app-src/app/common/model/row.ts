// library
import * as array from "../../../library/array";
import * as core from "../../../library/core";
import * as optional from "../../../library/optional";
import * as text from "../../../library/text";

// Common
import * as commonUI from "../ui";

// Helpers
import * as jqueryUIHelper from "../../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../../helpers/skuid/model";
import * as skuidUIHelpers from "../../../helpers/skuid/ui";

export class Row {
    private _id: optional.Optional<string>;

    constructor() {
        this._id = optional.fromNoneOf<string>("Row id has not yet been initialized.");
    }

    setRowId(id: string) {
        this._id = optional.fromSome(id);
    }

    getRowIdOpt() {
        return this._id;
    }
}

export function rowAs() {
    return new Row();
}

export function addRowOver(
    ephemeralModel: skuid.model.Model,
    condition: skuid.model.Condition,
    $dialog: JQuery,
    title: string
) {
    return function addRow() {
        ephemeralModel.abandonAllRows();
        ephemeralModel.deactivateCondition(condition);
        ephemeralModel.createRow({});
        jqueryUIHelper.dialog.setTitle($dialog, title);
        jqueryUIHelper.dialog.open($dialog);
    };
}

/**
 * Displays the edit dialog with the data of the selected record pre-filled in the form.
 *
 * -- Retrieves and unwraps the model "rowId" optional
 * -- Empties the ephemeralModel's data
 * -- Deactivates the given condition, sets it to the "rowId", and reactives the condition.
 * -- Updates the model data from the server with the updated condition, pulling back a specific row
 *    -- Sets the title of the dialog and opens it
 *    -- Publishes the "modelHasUpdatedEvent"
 */
export function editRowOver(
    ephemeralModel: skuid.model.Model,
    condition: skuid.model.Condition,
    modelRow: Row,
    $dialog: JQuery,
    title: string,
    modelHasUpdatedEvent: string
) {
    return function editRow() {
        ephemeralModel.abandonAllRows();
        ephemeralModel.deactivateCondition(condition);
        optional.withSomeOrFail(
            modelRow.getRowIdOpt(),
            rowId => {
                ephemeralModel.setCondition(condition, rowId);
                ephemeralModel.activateCondition(condition);
                $.blockUI({ message: commonUI.labels.generic.loadingMessage });
                skuidModelHelpers.updateData(ephemeralModel)
                    .then(model => {
                        $.unblockUI();
                        jqueryUIHelper.dialog.setTitle($dialog, title);
                        jqueryUIHelper.dialog.open($dialog);
                        skuid.events.publish(modelHasUpdatedEvent);
                    })
                    .catch(core.scheduleError)
            },
            text.emptyString
        );
    };
}

export function deleteRowOver($confirmDeleteDialog: JQuery) {
    return function deleteRow() {
        jqueryUIHelper.dialog.open($confirmDeleteDialog);
    }
}

export function confirmDeleteRowOver(
    modelRow: Row,
    persistentModel: skuid.model.Model,
    $confirmDeleteDialog: JQuery,
    modelHasUpdatedEvent: string
) {
    return function confirmDeleteRow() {
        optional.withSomeOrFail(
            modelRow.getRowIdOpt(),
            rowId => optional.withSomeOrFail(
                skuidModelHelpers.getRowOpt(persistentModel, rowId),
                row => {
                    $.blockUI({ message: commonUI.labels.generic.loadingMessage });
                    persistentModel.deleteRow(row);
                    skuidModelHelpers.save(persistentModel)
                        .then(result => {
                            skuidModelHelpers.updateData(persistentModel)
                                .then(model => {
                                    $.unblockUI();
                                    jqueryUIHelper.dialog.close($confirmDeleteDialog);
                                    skuidUIHelpers.removeErrors();
                                    skuid.events.publish(modelHasUpdatedEvent);
                                })
                                .catch(core.scheduleError);
                        })
                        .catch(core.scheduleError);
                },
                text.emptyString
            ),
            text.emptyString
        );
    };
}
