import * as dialogs from "./ui/dialogs";
import * as events from "./ui/events";
import * as labels from "./ui/labels";
import * as templates from "./ui/templates";
import * as validation from "./ui/validation";
import * as page from "./ui/page";

export { dialogs, events, labels, templates, page };
