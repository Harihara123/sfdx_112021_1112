export interface Events {
    addOrEditDialogHasOpened?: string;
    addOrEditDialogHasClosed?: string;
    confirmDeleteDialogHasOpened?: string;
    confirmDeleteDialogHasClosed?: string;
    wishCouldReloadTimeline?: string;
}
