// Helpers
import * as skuidUIHelpers from "../../../helpers/skuid/ui";

export interface Labels {
    add: string;
    edit: string;
}

export const generic = {
    addNew: skuidUIHelpers.labelFromOrFail("ATS_ADD_NEW"),
    cancelButton: skuidUIHelpers.labelFromOrFail("ATS_BUTTON_CANCEL"),
    edit: skuidUIHelpers.labelFromOrFail("ATS_EDIT"),
    saveButton: skuidUIHelpers.labelFromOrFail("ATS_BUTTON_SAVE"),
    lastModifiedBy: skuidUIHelpers.labelFromOrFail("ATS_LASTMOD_BY"),
    loadingMessage: skuidUIHelpers.labelFromOrFail("ATS_LOADING_MESSAGE"),
    locale: skuidUIHelpers.labelFromOrFail("ATS_LOCALE"),
    numbers: skuidUIHelpers.labelFromOrFail("ATS_NUMBERS"),
    on: skuidUIHelpers.labelFromOrFail("ATS_ON"),
    other: skuidUIHelpers.labelFromOrFail("ATS_OTHER"),
    web: skuidUIHelpers.labelFromOrFail("ATS_WEB"),
    websiteURL: skuidUIHelpers.labelFromOrFail("ATS_WEBSITE_URL"),
    hideDetail: skuidUIHelpers.labelFromOrFail("ATS_HIDE_DETAIL"),
    viewDetail: skuidUIHelpers.labelFromOrFail("ATS_VIEW_DETAIL"),
    viewFullList: skuidUIHelpers.labelFromOrFail("ATS_VIEW_FULL_LIST"),
    website: skuidUIHelpers.labelFromOrFail("ATS_WEB_SITE"),
    year: skuidUIHelpers.labelFromOrFail("ATS_YEAR"),
    location: skuidUIHelpers.labelFromOrFail("ATS_LOCATION"),
    notes: skuidUIHelpers.labelFromOrFail("ATS_NOTES"),
    cancelButton2: skuidUIHelpers.labelFromOrFail("ATS_CANCEL_BUTTON"),
    saveAndCloseButton: skuidUIHelpers.labelFromOrFail("ATS_SAVE_AND_CLOSE_BUTTON"),
    by: skuidUIHelpers.labelFromOrFail("ATS_BY"),
    lastEdit: skuidUIHelpers.labelFromOrFail("ATS_LAST_EDIT"),
    startDate: skuidUIHelpers.labelFromOrFail("ATS_START_DATE"),
    endDate: skuidUIHelpers.labelFromOrFail("ATS_END_DATE"),
    save: skuidUIHelpers.labelFromOrFail("ATS_SAVE"),
    add: skuidUIHelpers.labelFromOrFail("ATS_ADD"),
    deleteButton: skuidUIHelpers.labelFromOrFail("ATS_DELETE_BUTTON"),
    title: skuidUIHelpers.labelFromOrFail("ATS_TITLE"),
    deleteConfirmationHeader: skuidUIHelpers.labelFromOrFail("ATS_DELETE_CONFIRMATION_HEADER"),
    deleteConfirmation: skuidUIHelpers.labelFromOrFail("ATS_DELETE_CONFIRMATION"),
    invaldPhoneFormatHeader: skuidUIHelpers.labelFromOrFail("ATS_PHONE_FORMAT_WARNING"),
    invaldPhoneFormatIntro: skuidUIHelpers.labelFromOrFail("ATS_POTENTIAL_INVALID_PHONES"),
    noRecordsToDisplay: skuidUIHelpers.labelFromOrFail("ATS_THERE_ARE_NO_RECORD_TO_DISPLAY"),
    noDateInformation: skuidUIHelpers.labelFromOrFail("ATS_NO_DATE_INFORMATION"),
    expirationInPastError: skuidUIHelpers.labelFromOrFail("ATS_EXPIRATION_IN_PAST_ERROR"),
    backToCandidateSummary: skuidUIHelpers.labelFromOrFail("Back_to_candidate_summary"),
    atsRequiredFieldsInRed: skuidUIHelpers.labelFromOrFail("ATS_REQUIRED_FIELDS_IN_RED")
};

export const candidate = {

    contact: {
        address: skuidUIHelpers.labelFromOrFail("ATS_ADDRESS"),
        alternateEmail: skuidUIHelpers.labelFromOrFail("ATS_ALTERNATIVE_EMAIL"),
        alternateEmailPreferred: skuidUIHelpers.labelFromOrFail("ATS_ALTERNATIVE_EMAIL_PREFERRED"),
        city: skuidUIHelpers.labelFromOrFail("ATS_CITY"),
        country: skuidUIHelpers.labelFromOrFail("ATS_COUNTRY"),
        contactInformation: skuidUIHelpers.labelFromOrFail("ATS_CONTACT_INFORMATION"),
        email: skuidUIHelpers.labelFromOrFail("ATS_EMAIL"),
        emailPreferred: skuidUIHelpers.labelFromOrFail("ATS_EMAIL_PREFERRED"),
        firstName: skuidUIHelpers.labelFromOrFail("ATS_FIRST"),
        home: skuidUIHelpers.labelFromOrFail("ATS_HOME_LABEL"),
        homePreferred: skuidUIHelpers.labelFromOrFail("ATS_HOME_PREFERRED_LABEL"),
        lastName: skuidUIHelpers.labelFromOrFail("ATS_LAST"),
        middleName: skuidUIHelpers.labelFromOrFail("ATS_MIDDLE"),
        mobile: skuidUIHelpers.labelFromOrFail("ATS_MOBILE_LABEL"),
        mobilePreferred: skuidUIHelpers.labelFromOrFail("ATS_MOBILE_PREFERRED"),
        name: skuidUIHelpers.labelFromOrFail("ATS_NAME"),
        phone: skuidUIHelpers.labelFromOrFail("ATS_PHONE"),
        postalCode: skuidUIHelpers.labelFromOrFail("ATS_POSTAL_CODE"),
        preferredLocation: skuidUIHelpers.labelFromOrFail("ATS_PREFERRED_LOCATION"),
        prefix: skuidUIHelpers.labelFromOrFail("ATS_PREFIX"),
        primaryPhone: skuidUIHelpers.labelFromOrFail("ATS_PRIMARY_PHONE"),
        stateOrProvince: skuidUIHelpers.labelFromOrFail("ATS_STATE_PROVIENCE"),
        streetAddress: skuidUIHelpers.labelFromOrFail("ATS_STREET_ADDRESS"),
        suffix: skuidUIHelpers.labelFromOrFail("ATS_SUFFIX"),
        currentLocation: skuidUIHelpers.labelFromOrFail("ATS_CURRENT_LOCATION"),
        candidateContactSettings: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_CONTACT_SETTINGS"),
        editCandidateContact: skuidUIHelpers.labelFromOrFail("ATS_EDIT_CANDIDATE_CONTACT"),
        doNotContactAlt: skuidUIHelpers.labelFromOrFail("ATS_DO_NOT_CONTACT_ALT"),
        doNotContactReason: skuidUIHelpers.labelFromOrFail("ATS_DO_NOT_CONTACT_REASON"),
        doNotContactExpiration: skuidUIHelpers.labelFromOrFail("ATS_DO_NOT_CONTACT_EXPIRATION"),
        editCandidateForm: skuidUIHelpers.labelFromOrFail("ATS_EDIT_CANDIDATE_FORM"),
        doNotContact: skuidUIHelpers.labelFromOrFail("ATS_DO_NOT_CONTACT"),
        compensation: skuidUIHelpers.labelFromOrFail("ATS_COMPENSATION")
    },

    summary: {
        addToHotlist: skuidUIHelpers.labelFromOrFail("ATS_ADD_TO_HOTLIST"),
        deactivate: skuidUIHelpers.labelFromOrFail("ATS_DEACTIVATE"),
        deactivated: skuidUIHelpers.labelFromOrFail("ATS_DEACTIVATED"),
        reactivate: skuidUIHelpers.labelFromOrFail("ATS_RE_ACTIVATE"),
        submitToAJob: skuidUIHelpers.labelFromOrFail("ATS_SUBMIT_TO_A_JOB"),
        title: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_SUMMARY"),
        viewContactDetails: skuidUIHelpers.labelFromOrFail("ATS_VIEW_CONTACT_DETAIL"),
        candidateId: skuidUIHelpers.labelFromOrFail("ATS_ID"),
        candidateCall: skuidUIHelpers.labelFromOrFail("ATS_CALL"),
        summaryTitle: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_SUMMARY_TITLE")
    },

    activities: {
        title: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_ACTIVITIES"),
        activity: skuidUIHelpers.labelFromOrFail("ATS_ACTIVITY"),
        noPastActivity: skuidUIHelpers.labelFromOrFail("ATS_NO_PAST_ACTIVITY"),
        noNextSteps: skuidUIHelpers.labelFromOrFail("ATS_NO_NEXT_STEP_NO_ACTIVITY"),
        pastActivity: skuidUIHelpers.labelFromOrFail("ATS_PAST_ACTIVITY"),
        activityLogHeader: skuidUIHelpers.labelFromOrFail("ATS_ACTIVITY_LOG_HEADER"),
        createdBy: skuidUIHelpers.labelFromOrFail("ATS_CREATED_BY"),
        owner: skuidUIHelpers.labelFromOrFail("ATS_OWNER")
    },

    qualification: {

        education: {
            degree: skuidUIHelpers.labelFromOrFail("ATS_DEGREE"),
            major: skuidUIHelpers.labelFromOrFail("ATS_ED_MAJOR"),
            major2: skuidUIHelpers.labelFromOrFail("ATS_MAJOR"),
            honors: skuidUIHelpers.labelFromOrFail("ATS_ED_HONORS"),
            honors2: skuidUIHelpers.labelFromOrFail("ATS_HONORS"),
            schoolName: skuidUIHelpers.labelFromOrFail("ATS_SCHOOL_NAME"),
            educationDetailBanner: skuidUIHelpers.labelFromOrFail("ATS_EDUCATION_DETAIL_BANNER"),
            educationQueueTitle: skuidUIHelpers.labelFromOrFail("ATS_EDUCATION_QUEUE_TITLE"),
            gradYearError: skuidUIHelpers.labelFromOrFail("ATS_GRAD_YEAR_ERROR"),
            gradYearHelp: skuidUIHelpers.labelFromOrFail("ATS_GRAD_YEAR_HELP"),
            educationDetails: skuidUIHelpers.labelFromOrFail("ATS_EDUCATION_DETAILS"),
        },

        certification: {
            certifications: skuidUIHelpers.labelFromOrFail("ATS_CERTIFICATIONS"),
        },

    },

    employment: {
        bonus: skuidUIHelpers.labelFromOrFail("ATS_EMPLOYMENT_BONUS"),
        title: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_EMPLOYMENT"),
        work: skuidUIHelpers.labelFromOrFail("ATS_WORK_LABEL"),
        workPreferred: skuidUIHelpers.labelFromOrFail("ATS_WORK_PREFERRED_LABEL"),
        employmentDetailView: skuidUIHelpers.labelFromOrFail("ATS_EMPLOYMENT_DETAIL_VIEW"),
        editEmployment: skuidUIHelpers.labelFromOrFail("ATS_EDIT_EMPLOYMENT"),
        addEmployment: skuidUIHelpers.labelFromOrFail("ATS_ADD_EMPLOYMENT"),
        addEmploymentButton: skuidUIHelpers.labelFromOrFail("ATS_ADD_EMPLOYMENT_BUTTON"),
        companyName: skuidUIHelpers.labelFromOrFail("ATS_COMPANY_NAME"),
        department: skuidUIHelpers.labelFromOrFail("ATS_DEPARTMENT"),
        reasonForLeaving: skuidUIHelpers.labelFromOrFail("ATS_REASON_FOR_LEAVING"),
        salary: skuidUIHelpers.labelFromOrFail("ATS_SALARY"),
        payRate: skuidUIHelpers.labelFromOrFail("ATS_PAY_RATE"),
    },

    references: {

        relation: skuidUIHelpers.labelFromOrFail("ATS_REFERENCE_RELATIONSHIP"),
        jobTitle: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_JOB_TITILE"),
        relationWithCandidate: skuidUIHelpers.labelFromOrFail("ATS_DURATION_OF_RELATIONSHIP_WITH_CANDIDATE"),
        frequenceInteraction: skuidUIHelpers.labelFromOrFail("ATS_FREQUENCY_OF_INTERACTION"),
        lengthOfPosition: skuidUIHelpers.labelFromOrFail("ATS_LENGTH_OF_TIME_IN_POSITION"),
        isStillInPosition: skuidUIHelpers.labelFromOrFail("ATS_STILL_IN_POSITION"),
        JobDutities: skuidUIHelpers.labelFromOrFail("ATS_JOB_DUTIES"),
        qualityOfWork: skuidUIHelpers.labelFromOrFail("ATS_QUALITY_OF_WORK"),
        QuantityOfWorkload: skuidUIHelpers.labelFromOrFail("ATS_QUANTITY_OF_WORKLOAD"),
        ability: skuidUIHelpers.labelFromOrFail("ATS_ABILITY"),
        initative: skuidUIHelpers.labelFromOrFail("ATS_INITIATIVE"),
        cooperationCommunication: skuidUIHelpers.labelFromOrFail("ATS_COOPERATION_COMMUNICATION"),
        attendanceREliability: skuidUIHelpers.labelFromOrFail("ATS_ATTENDANCE_RELIABILITY"),
        interofficeRelationLieability: skuidUIHelpers.labelFromOrFail("ATS_INTEROFFICE_RELATIONSHIP_LIEABILITY"),
        attitudeOutlook: skuidUIHelpers.labelFromOrFail("ATS_ATTITUDE_OUTLOOK"),
        teamProjectMember: skuidUIHelpers.labelFromOrFail("ATS_TEAM_PROJECT_MEMBER"),
        apperaranceProfessionalism: skuidUIHelpers.labelFromOrFail("ATS_APPERARANCE_PROFESSIONALISM"),
        strengths: skuidUIHelpers.labelFromOrFail("ATS_STRENGTHS"),
        opportunitiesForGrowth: skuidUIHelpers.labelFromOrFail("ATS_OPPORTUNITIES_FOR_GROWTH"),
        rehire: skuidUIHelpers.labelFromOrFail("ATS_REHIRE"),
        additionalInformationComments: skuidUIHelpers.labelFromOrFail("ATS_ADDITIONAL_INFORMATION_COMMENTS"),
        companyName: skuidUIHelpers.labelFromOrFail("ATS_COMPANY_NAME"),
        download : skuidUIHelpers.labelFromOrFail("ATS_DOWNLOAD"),
        addReference : skuidUIHelpers.labelFromOrFail("ATS_ADD_REFERENCE"),
        editReference : skuidUIHelpers.labelFromOrFail("ATS_EDIT_REFERENCE"),
        close : skuidUIHelpers.labelFromOrFail("ATS_CLOSE"),
        candidateReference : skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_REFERENCE"),
        referenceContactInformation : skuidUIHelpers.labelFromOrFail("ATS_REFERENCE_CONTACT_INFORMATION"),
        workSummaryOf : skuidUIHelpers.labelFromOrFail("ATS_WORK_SUMMARY_OF"),
        deleteButton : skuidUIHelpers.labelFromOrFail("ATS_DELETE_BUTTON"),
        referencePersonalInformation : skuidUIHelpers.labelFromOrFail("ATS_REFERENCE_PERSONAL_INFORMATION"),
        otherReferenceFor : skuidUIHelpers.labelFromOrFail("ATS_OTHER_REFERENCE_FOR")


    },

    documents: {
        title: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_DOCUMENTS"),
        documents: skuidUIHelpers.labelFromOrFail("ATS_DOCUMENTS"),
        upload: skuidUIHelpers.labelFromOrFail("ATS_UPLOAD")
    },

    duplicateCheck: {
        searchResults: skuidUIHelpers.labelFromOrFail("ATS_SEARCH_RESULTS")
    },

    search: {
        candidateSearch: skuidUIHelpers.labelFromOrFail("ATS_CANDIDATE_SEARCH")
    }

};

export const recruiter = {
    createNewCandidateProfile: skuidUIHelpers.labelFromOrFail("ATS_CREATE_NEW_CANDIDATE_PROFILE"),
    favoriteThisCandidate: skuidUIHelpers.labelFromOrFail("ATS_FAVORITE_THIS_CANDIDATE"),
    recruiterHome: skuidUIHelpers.labelFromOrFail("ATS_RECRUITER_HOME"),
};

export const unsorted = {
    event: skuidUIHelpers.labelFromOrFail("ATS_EVENT"),
    type: skuidUIHelpers.labelFromOrFail("ATS_TYPE"),
    subject: skuidUIHelpers.labelFromOrFail("ATS_SUBJECT"),
    startDateTime: skuidUIHelpers.labelFromOrFail("ATS_START_DATE_TIME"),
    endDateTime: skuidUIHelpers.labelFromOrFail("ATS_END_DATE_TIME"),
    allDay: skuidUIHelpers.labelFromOrFail("ATS_ALL_DAY"),
    preMeeting: skuidUIHelpers.labelFromOrFail("ATS_PRE_MEETING"),
    postMeeting: skuidUIHelpers.labelFromOrFail("ATS_POST_MEETING"),
    priority: skuidUIHelpers.labelFromOrFail("ATS_PRIORITY"),
    status: skuidUIHelpers.labelFromOrFail("ATS_STATUS"),
    dueDate: skuidUIHelpers.labelFromOrFail("ATS_DUE_DATE"),
    repeatThisTask: skuidUIHelpers.labelFromOrFail("ATS_REPEAT_THIS_TASK"),
    recurrenceInterval: skuidUIHelpers.labelFromOrFail("ATS_RECURRENCE_INTERVAL"),
    completedDate: skuidUIHelpers.labelFromOrFail("ATS_COMPLETED_DATE"),
    comment: skuidUIHelpers.labelFromOrFail("ATS_COMMENT"),
    newTask: skuidUIHelpers.labelFromOrFail("ATS_NEW_TASK"),
    newEvent: skuidUIHelpers.labelFromOrFail("ATS_NEW_EVENT"),
    addButton: skuidUIHelpers.labelFromOrFail("ATS_BUTTON_ADD"),
    task: skuidUIHelpers.labelFromOrFail("ATS_TASK"),
    addEducation: skuidUIHelpers.labelFromOrFail("ATS_ADD_EDUCATION"),
    addCertification: skuidUIHelpers.labelFromOrFail("ATS_ADD_CERTIFICATION"),
    education: skuidUIHelpers.labelFromOrFail("ATS_EDUCATION"),
    dueDateDefault: skuidUIHelpers.labelFromOrFail("ATS_DUE_DATE_LABEL_DEFAULT"),
    dueDateNew: skuidUIHelpers.labelFromOrFail("ATS_DUE_DATE_LABEL_NEW"),
    orgNameDefault: skuidUIHelpers.labelFromOrFail("ATS_ORG_NAME_DEFAULT"),
    orgNameNew: skuidUIHelpers.labelFromOrFail("ATS_ORG_NAME_NEW"),
    addEducationHeader: skuidUIHelpers.labelFromOrFail("ATS_ADD_EDUCATION_HEADER"),
    editEducationHeader: skuidUIHelpers.labelFromOrFail("ATS_EDIT_EDUCATION_HEADER"),
    addCertificationHeader: skuidUIHelpers.labelFromOrFail("ATS_ADD_CERTIFICATION_HEADER"),
    editCertificationHeader: skuidUIHelpers.labelFromOrFail("ATS_EDIT_CERTIFICATION_HEADER"),
    certification: skuidUIHelpers.labelFromOrFail("ATS_CERTIFICATION"),
    more: skuidUIHelpers.labelFromOrFail("ATS_MORE"),
    allDayEvent: skuidUIHelpers.labelFromOrFail("ATS_ALL_DAY_EVENT"),
    resetFilter: skuidUIHelpers.labelFromOrFail("ATS_RESET_FILTER"),
    applyFilter: skuidUIHelpers.labelFromOrFail("ATS_APPLY_FILTER"),
    selectAll: skuidUIHelpers.labelFromOrFail("ATS_SELECT_ALL"),
    loadMore: skuidUIHelpers.labelFromOrFail("ATS_LOAD_MORE"),
    noMoreRecords: skuidUIHelpers.labelFromOrFail("ATS_THERE_NO_MORE_RECORDS"),
    viewMore: skuidUIHelpers.labelFromOrFail("ATS_VIEW_MORE"),
    nextSteps: skuidUIHelpers.labelFromOrFail("ATS_NEXT_STEPS"),
    noActivitiesMatchFilterRecord: skuidUIHelpers.labelFromOrFail("ATS_NO_ACTIVITIES_MATCH_RECORD"),
    readMore: skuidUIHelpers.labelFromOrFail("ATS_READMORE")
};
