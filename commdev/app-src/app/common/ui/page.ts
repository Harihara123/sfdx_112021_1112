export enum Page { Landing, Details }

interface ViaSomePage<r> {
    caseOfLandingPage: () => r;
    caseOfDetailsPage: () => r;
    caseOfNeither: (msg: string) => r;
}

export function viaSomePage<r>(page: Page, via: ViaSomePage<r>): r {
    switch(page) {
        case Page.Landing: return via.caseOfLandingPage();
        case Page.Details: return via.caseOfDetailsPage();
        default: return via.caseOfNeither(`Unexpected page ${page}`);
    }
}
