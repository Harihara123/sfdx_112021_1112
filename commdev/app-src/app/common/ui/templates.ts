import * as commonUI from "../ui";

export interface Templates {
    addOrEdit?: string;
    confirmDelete?: string;
}

export const confirmDeleteTemplate = `
    <popup title="${commonUI.labels.generic.deleteConfirmationHeader}" width="40%">
        <components>
            <template multiple="false">
                <contents>${commonUI.labels.generic.deleteConfirmation}</contents>
            </template>
        </components>
    </popup>
`;

export const confirmInvalidPhoneTemplate = `
    <popup title="${commonUI.labels.generic.invaldPhoneFormatHeader}" width="40%">
        <components>
            <template multiple="false">
                <contents>${commonUI.labels.generic.invaldPhoneFormatIntro}_replace_token_</contents>
            </template>
        </components>
    </popup>
`;
