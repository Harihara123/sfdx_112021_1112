import * as optional from "../../../library/optional";
import * as skuidModelHelpers from "../../../helpers/skuid/model";
import * as text from "../../../library/text";

const defaultCountryCode = "US";
const phoneLabel = skuid.label.read("ATS_PHONE", "PHONE");
const homeLabel = skuid.label.read("ATS_HOME_LABEL", "HOME") + " " + phoneLabel;
const mobileLabel = skuid.label.read("ATS_MOBILE_LABEL", "MOBILE") + " " + phoneLabel;
const workLabel = skuid.label.read("ATS_WORK_LABEL", "WORK") + " " + phoneLabel;

export let contactPhoneFields = [
  { field: "MobilePhone", label: mobileLabel },
  { field: "HomePhone", label: homeLabel },
  { field: "Phone", label: workLabel }];

export let referencePhoneFields = [
  { field: "Phone", label: workLabel },
  { field: "MobilePhone", label: mobileLabel },
  { field: "HomePhone", label: homeLabel }];

export function validatePhoneNumbers(phoneFields, contactModel: skuid.model.Model) {

  let phoneObj = phoneFields.map(function(obj) {
    return {
      label: obj.label,
      value: skuidModelHelpers.getFieldValueOfFirstRow(contactModel, obj.field)
    };
  });

  let invalidPhoneList = phoneObj.reduce(function(retVal, obj) {
    if (obj.value !== undefined && obj.value !== "") {
      let isValid = isValidPhoneNumber(<string>obj.value, defaultCountryCode);
      if (!isValid) {
        if (retVal.length > 0) {
          retVal += ", ";
        } else {
          retVal += " ";
        }
        retVal += obj.label;
      }
    }
    return retVal;
  }, '');

  return invalidPhoneList;
}
