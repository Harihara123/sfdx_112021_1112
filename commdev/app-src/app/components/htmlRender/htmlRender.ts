// Data
import { Properties } from "./data";

// Helpers
import { documentReady } from "../../../helpers/jquery-helper";

export const htmlRenderFrom = (properties: Properties) => {
    documentReady().then($document => {
        render(properties.htmlString, properties.$root);
    });
};

const render = (htmlString: string, $root: JQuery) => {
    $root.html(htmlString).promise().done(() => {
        skuid.events.publish("ats.component.htmlRender.hasRendered");
    });
};
