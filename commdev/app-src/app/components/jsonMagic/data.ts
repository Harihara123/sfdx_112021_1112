export interface Parameters {
    $element: JQuery;
    schema: string;
    uidef: string;
    modelName: string;
    jsonFieldName: string;
    mode: string;
}

export interface Schema {
    title: string;
    description: string;
    defaultValue: Properties;
    type: string;
    properties: Properties;
}

export interface Properties {
    goalsandinterest: StringField;
    practice: StringField;
    practicegroup: StringField;
    firmstoconsider: StringField;
    firmsnottoconsider: StringField;
    firmsalreadyapproached: StringField;
    portables: CurrencyField;
    billingratemin: CurrencyField;
    billingratemax: CurrencyField;
    currentcompensation: CurrencyField;
    bonusmaximum: CurrencyField;
    bonusminimum: CurrencyField;
    additionalcompensation: CurrencyField;
    group: StringField;
    partofgroup: BooleanField;
    desiredsalarymin: CurrencyField;
    desiredsalarymax: CurrencyField;
    desiredsalaryunit: StringField;
    desiredjoblocation: StringField;
    desiredjobtitle: StringField;
    desiredplacementtype: StringField;
    desiredschedule: StringField;
    relocation: StringField;
    willingtorelocate: StringField;
    clients: StringField;
    positions: StringField;
    race: StringField;
    gender: StringField;
}

export interface ElementRef {
    id: string;
    field: string;
    label: string;
}

export interface Json {
    data: any;
}

export interface Option {
    key: string;
    value: string;
}

export interface ValidationRule {
    ruleexpr: string;
    comment: string;
    errormessage: string;
}

export interface StringField {
    data: string;
    label?: string;
    validationrule?: ValidationRule;
}

export interface BooleanField {
    data: boolean;
    label?: string;
    validationrule?: ValidationRule;
}

export interface TextareaField {
    data: string;
    label?: string;
    validationrule?: ValidationRule;
}

export interface CurrencyField {
    data: number;
    label?: string;
    validationrule?: ValidationRule;
}

export interface PicklistField {
    data: string;
    label?: string;
    options?: Option[];
    validationrule?: ValidationRule;
}

export interface SimplePicklistField {
    data: string;
    label?: string;
    options?: Option[];
    validationrule?: ValidationRule;
}

export interface MultiselectField {
    data: string;
    label?: string;
    options?: Option[];
    validationrule?: ValidationRule;
}

export interface FreetextTypeaheadField {
    data: string;
    label?: string;
    validationrule?: ValidationRule;
}
