// Common
import * as commonUI from "../../common/ui";

// Data
import * as jsonMagicData from "./data";

// Helpers
import * as skuidModelHelpers from "../../../helpers/skuid/model";

// Library
import * as array from "../../../library/array";
import * as core from "../../../library/core";
import * as optional from "../../../library/optional";
import * as textLib from "../../../library/text";

/**
 * TODO: PLEASE REFACTOR ME!
 * I'm so sad that I'm not fully typed, missing interfaces, and not using all the library functions I could be.
 */

export function jsonMagicFrom(properties: jsonMagicData.Parameters) {
    $(document).ready(() => {
        let ui = renderUI(
            properties.schema,
            properties.uidef,
            properties.modelName,
            properties.jsonFieldName,
            properties.$element.attr("id"),
            properties.mode,
            array.of<any>()
        );

        properties.$element.html(ui);
        registerDomEventHandlers(properties.$element);
    });
}

function renderUI(
    schema: string,
    uidef: string,
    modelName: string,
    jsonFieldName: string,
    componentId: string,
    mode: string,
    validationErrors: any[]
): string {
    var ui = '<div id="' + componentId + '_dataForm">';
    ui += appendErrors(ui, validationErrors);
    var schemaReader = $.parseJSON(schema);
    var jsonData;

    if (validationErrors != null && validationErrors.length > 0) {
        jsonData = $('#' + componentId + '_jsonData').html();
    } else {
        //jsonData = skuid.model.getModel(modelName).data[0][jsonFieldName];
        jsonData = optional.withSomeOrFail(
            skuidModelHelpers.getModelOpt(modelName),
            model => skuidModelHelpers.getFieldValueOfFirstRow<any>(model, jsonFieldName),
            textLib.emptyString
        );
    }

    if(typeof jsonData === 'undefined') {
        //populated template row
        jsonData = schemaReader.defaultValue;
    }

    var json;
    if(typeof jsonData == 'object') {
        json = jsonData;
    } else {
        json = JSON.parse(jsonData);
    }

    var uidefReference = uidef;

    var text;
    var pattern = /{(.*?)}/g;

    while(text = pattern.exec(uidef)) {
        let index = text[1];
        let token = text[0];
        let label = '';
        //console.log(index);
        if (index.indexOf('.VALUEONLY') > 0) {
            index = index.replace('.VALUEONLY','');
        } else {
            label = schemaReader.properties[index].label;
        }
        //var elementRef = [componentId, index, label, schemaReader.properties[index].options, schemaReader.properties[index].truefalsetext];
        let elementRef = {
            id: componentId,
            field: index,
            label: label,
            options: schemaReader.properties[index].options,
            booleanText: schemaReader.properties[index].truefalsetext
        };
        // elementRef.id = componentId;
        // elementRef.field = index;
        // elementRef.label = label;
        // elementRef.options = schemaReader.properties[index].options;
        // elementRef.booleanText = schemaReader.properties[index].truefalsetext;

        //elementRef.dropdown

        let fieldType =  schemaReader.properties[index]['$ref'].split('/');

        if (typeof json[index] == 'undefined') {
            json[index] = {"data" : schemaReader.defaultValue[index].data};
        }

        //console.log(json[index]);
        let html = renderElements(json[index], mode, fieldType[2], elementRef);
        uidef = uidef.replace(token, html);
    };

    ui += storeComponentProperties(schema, json, uidefReference, modelName, jsonFieldName, componentId);

    ui += uidef;

    if (mode == 'view') {
        ui += `<div class="align-right"><input type="button" class="slds-button slds-button--brand fundamentals__button--edit" value="${commonUI.labels.generic.edit}"</div>`;
    } else if (mode == 'edit') {
        ui += `<div class="align-right"><input type="button" class="slds-button slds-button--neutral fundamentals__button--cancel" value="${commonUI.labels.generic.cancelButton}"/>`;
        ui += `<input type="button" class="slds-button slds-button--brand fundamentals__button--save" value="${commonUI.labels.generic.saveAndCloseButton}"/></div>`;
    }

    // Display Last Modified Info
    skuid.events.publish("ats.candidateFundamentals.wishCouldSetLastModified", array.fromOne({
        lastModifiedBy: core.nullable.asSomeOrElse(
            json["lastmodifiedby"],
            { userId: textLib.emptyString, name: textLib.emptyString }
        ),
        lastModifiedDate: core.nullable.asSomeOrElse(json["lastmodifieddate"], textLib.emptyString),
        isOnSave: false
    }));

    ui += applyBodyOnloadJS(mode);

    return ui;
}

function renderElements(
    json: jsonMagicData.Json,
    mode: string,
    datatype: string,
    elementRef: jsonMagicData.ElementRef
) {
    var elementUI = {
        "stringfieldtype": function(json, mode, elementRef) {
            return renderStringField(json, mode, elementRef);
        },
        "picklistfieldtype": function(json, mode, elementRef) {
            elementRef.picklistclass = 'select2class';
            return renderPicklistField(json, mode, elementRef);
        },
        "simplepicklistfieldtype": function(json, mode, elementRef) {
            return renderSimplePicklistField(json, mode, elementRef);
        },
        "multiselectfieldtype": function(json, mode, elementRef) {
            elementRef.picklistclass = 'select2class';
            return renderMultiSelectPicklistField(json, mode, elementRef);
        },
        "currencyfieldtype": function(json, mode, elementRef) {
            return renderCurrencyField(json, mode, elementRef);
        },
        "percentagefieldtype": function(json, mode, elementRef) {
            return renderPercentageField(json, mode, elementRef);
        },
        "textareafieldtype": function(json, mode, elementRef) {
            return renderTextareaField(json, mode, elementRef);
        },
        "freetexttypeaheadfieldtype": function(json, mode, elementRef) {
            return renderFreetextTagField(json, mode, elementRef);
        },
        "booleanfieldtype": function(json, mode, elementRef) {
            return renderBooleanField(json, mode, elementRef);
        }
    };

    return elementUI[datatype](json, mode, elementRef);
}


function renderStringField(
    json: jsonMagicData.Json,
    mode: string,
    elementRef: jsonMagicData.ElementRef
) {
    if (mode == 'edit') {
        return '<div><div class="label">' + elementRef.label + '</div><div class="value"><input type="text" formRef="' + elementRef.id + '" dataRef="' + elementRef.field + '" value="' + json.data + '" /></div></div>';
    } else {
        return '<div><div class="label">' + elementRef.label + '</div><div class="value"><span>' + json.data + '</span></div></div>';
    }
}

function renderCurrencyField(json, mode, elementRef) {
    if (mode == 'edit') {
        return '<div>' + getLabelText(elementRef.label) + '<div class="value">' + getCurrencySymbol(skuid.utils.userInfo.defaultCurrency) + '<input type="number"  formRef="' + elementRef.id + '" dataRef="' + elementRef.field + '" value="' + json.data + '" /></div></div>';
    } else {
        return '<div>' + getLabelText(elementRef.label) + '<div class="value"><span>' + getCurrencySymbol(skuid.utils.userInfo.defaultCurrency) + formatNumber(json.data, 0, 3, ',') + '</span></div></div>';
    }
}

function renderPercentageField(json: { data: string }, mode, elementRef: { field: string; id: string; label: string; }) {
    if (mode == "edit") {
        return `<div>${getLabelText(elementRef.label)}<div class="value"><input type="number" formRef="${elementRef.id}" dataRef="${elementRef.field}" value="${json.data}" />&nbsp;%</div></div>`;
    } else {
        return `<div>${getLabelText(elementRef.label)}<div class="value"><span>${formatNumber(json.data, 0, 3, ",")}&nbsp;%</span></div></div>`;
    }
}

function renderTextareaField(json, mode, elementRef) {
    if (mode == 'edit') {
        return '<div><div class="label">' + elementRef.label + '</div><div class="value"><textarea  formRef="' + elementRef.id + '" dataRef="' + elementRef.field + '">' + json.data + '</textarea></div></div>';
    } else {
        return '<div><div class="label">' + elementRef.label + '</div><div class="value"><span>' + json.data + '</span></div></div>';
    }
}

function renderSimplePicklistField(json, mode, elementRef) {
    return picklistUtil(json, mode, '', elementRef);
}

function renderPicklistField(json, mode, elementRef) {
    return picklistUtil(json, mode, '', elementRef);
}

function renderMultiSelectPicklistField(json, mode, elementRef) {
    return picklistUtil(json, mode, 'multiple', elementRef);
}

function picklistUtil(
    json: { data: string },
    mode: string,
    multiple: string,
    elementRef: {
        id: string;
        label: string;
        field: string;
        picklistclass: string;
        options: { key: string; value: string; }[];
    }
) {
    let html = textLib.emptyString;
    let data = json.data.split(';');
    let className = textLib.emptyString;
    let optionList = json.data.split(';');
    let optionHtml = textLib.emptyString;
    let optionListLength = optionList.length;

    if (mode === "edit") {
        if (elementRef.picklistclass) className = "select2class";

        html = `
            <div>
                <div class="label">${elementRef.label}</div>
                <div class="value">
                    <select class="${className}" formRef="${elementRef.id}" dataRef="${elementRef.field}" ${multiple}>`;

        for(let i = 0; i < elementRef.options.length; i++) {
            if (data.indexOf(elementRef.options[i].value) >= 0) {
                html += `<option value="${elementRef.options[i].key}" selected>${elementRef.options[i].value}</option>`;
            } else {
                html += `<option value="${elementRef.options[i].key}">${elementRef.options[i].value}</option>`;
            }
        }

        html += `</select></div></div>`;
    } else {
        html += `<div><div class="label">${elementRef.label}</div><div class="value">`;
        array.forEach(optionList, (option, index) => {
            if (option !== "Select") html += `<span>${option}</span>`;
            if (index < optionListLength - 1) html += `<span>,</span>`;
        });
        html += `</div></div>`;
    }

    return html;
}

function renderFreetextTagField(json, mode, elementRef) {
    var optionList = json.data.split(';');
    var optionHtml = '';
    $.each(optionList, function(index, option) {
        optionHtml+= '<option selected>' + option + '</option>';
    });
    if (mode == 'edit') {
        return '<div class="label">' + elementRef.label + '</div><div class="value"><select class="freetexttag" formRef="' + elementRef.id + '" dataRef="' + elementRef.field + '" multiple>' + optionHtml  + '</select></div>';
    } else {
        return '<div class="label">' + elementRef.label + `</div><div class="value">${optionList.join(", ")}</div>`;
    }
}

function renderBooleanField(json, mode, elementRef) {
    var checked = '';
    if (mode == 'edit') {
        if (json.data) {
            checked = 'checked';
        }
        return '<div><div class="label">' + elementRef.label + '</div><div class="value"><input type="checkbox" value="true" formRef="' + elementRef.id + '" dataRef="' + elementRef.field + '" ' + checked + ' /></div></div>';
    } else {
        var booleanValue;
        if (json.data) {
            booleanValue = elementRef.booleanText.true;
        } else {
            booleanValue = elementRef.booleanText.false;
        }
        return '<div><div class="label">' + elementRef.label + '</div><span><div class="value">' + booleanValue + '</span></div></div>';
    }
}


function storeComponentProperties(schema, jsonData, uidef, modelName, jsonFieldName, componentId) {
    var html =  $('<div/>')
    .append(createElement('div', [{name:"id",value:"" + componentId + "_schema"},{name:"style",value:"display:none"}], JSON.stringify(schema)))
    .append(createElement('div', [{name:"id",value:"" + componentId + "_jsonData"},{name:"style",value:"display:none"}], JSON.stringify(jsonData)))
    .append(createElement('div', [{name:"id",value:"" + componentId + "_uidef"},{name:"style",value:"display:none"}], encodeURI(uidef)))
    .append(createElement('div', [{name:"id",value:"" + componentId + "_modelName"},{name:"style",value:"display:none"}], modelName))
    .append(createElement('div', [{name:"id",value:"" + componentId + "_jsonFieldName"},{name:"style",value:"display:none"}], jsonFieldName));
    return html.html();
}


function toggleView(mode: string, componentId: string) {
    var schema = $('#' + componentId + '_schema').html();
    var uidef = decodeURI($('#' + componentId + '_uidef').html());
    var modelName = $('#' + componentId + '_modelName').html();
    var jsonFieldName = $('#' + componentId + '_jsonFieldName').html();
    $('#' + componentId).html(renderUI(JSON.parse(schema), uidef, modelName, jsonFieldName, componentId, mode, null));
}

function updateJson(id) {
    var errors = [];
    var updatedData = "Updated Json:<br/>";
    var validationObj	;
    var schema = JSON.parse($('#' + id + '_schema').html());
    //var jsonData = JSON.parse($('#' + id + '_jsonData').html());
    var modelName = $('#' + id + '_modelName').html();
    var jsonFieldName = $('#' + id + '_jsonFieldName').html();

    var inputFields = $('input[formref="' + id + '"]');
    var selectFields = $('select[formref="' + id + '"]');
    var textareaFields = $('textarea[formref="' + id + '"]');


    //var jsonData = skuid.model.getModel(modelName).data[0][jsonFieldName];
    let jsonData = optional.withSomeOrFail(
        skuidModelHelpers.getModelOpt(modelName),
        model => skuidModelHelpers.getFieldValueOfFirstRow(model, jsonFieldName),
        textLib.emptyString
    );

    var json;
    if(typeof jsonData == 'object') {
        json = jsonData;
    } else {
        json = JSON.parse($('#' + id + '_jsonData').html());
    }

    $.each(inputFields, function(index,element){
        //console.log(element.type);
        if (element.type == 'checkbox') {
            if(element.checked) {
                validateData(true, JSON.parse(schema).properties[$(element).attr('dataref')].validationrule, errors);
                json[$(element).attr('dataref')].data = true;
            } else {
                validateData(false, JSON.parse(schema).properties[$(element).attr('dataref')].validationrule, errors);
                json[$(element).attr('dataref')].data = false;
            }
        } else if (element.type == 'number') {
            //console.log(JSON.parse(schema).properties[$(element).attr('dataref')].validationrule);
            validateData(element.value, JSON.parse(schema).properties[$(element).attr('dataref')].validationrule, errors);
            json[$(element).attr('dataref')].data = Number(element.value);
        } else {
            validateData(element.value, JSON.parse(schema).properties[$(element).attr('dataref')].validationrule, errors);
            json[$(element).attr('dataref')].data = element.value;
        }

    });
    $.each(textareaFields, function(index,element){
        validateData(element.value, JSON.parse(schema).properties[$(element).attr('dataref')].validationrule, errors);
        json[$(element).attr('dataref')].data = element.value;
    });
    $.each(selectFields, function(index,element){
        var data = '';
        $.each($(element).find(":selected"),function(i,option){
            data += $(option).text() + ';';
        });
        data = data.substring(0, data.length-1);
        validateData(data, validationObj, errors);
        json[$(element).attr('dataref')].data = data;
    });


    $('#' + id + '_jsonData').html(JSON.stringify(json));

    var uidef = decodeURI($('#' + id + '_uidef').html());

    if (errors.length <=0) {
        //var model = skuid.model.getModel(modelName);
        let model = optional.asSomeOrFail(skuidModelHelpers.getModelOpt(modelName), textLib.emptyString);

        json["lastmodifieddate"] = new Date();
        json["lastmodifiedby"] = {
            userId: skuid.utils.userInfo.userId,
            name: skuid.utils.userInfo.name
        };

        $.each(model.data, function(i,row){
            model.updateRow(row,{
                Talent_Preference_Internal__c: JSON.stringify(json)
            });
        });

        model.save().then(function() {
            skuid.events.publish("ats.candidateFundamentals.wishCouldSetLastModified", array.fromOne({
                lastModifiedBy: json["lastmodifiedby"],
                lastModifiedDate: json["lastmodifieddate"],
                isOnSave: true
            }));
        });
        $('#' + id).html(renderUI(schema, uidef, modelName, jsonFieldName, id, 'view', null));
    } else {
        $('#' + id).html(renderUI(schema, uidef, modelName, jsonFieldName, id, 'edit', errors));
    }

}

function createElement(type, attributes, html) {
    var newElement = $("<"+type+"/>");
    $.each(attributes, function(index, element){
        newElement.attr(element.name,element.value);
    });
    newElement.html(html);
    return newElement;

}

function getCurrencySymbol(isoCode) {
    switch(isoCode) {
        case 'USD':
        return '$ ';
    }
}

function applyBodyOnloadJS(mode: string) {
    //Apply select2 js for free text tag support
    var html = "<script language='javascript'>";

    if (mode == 'edit') {
        html += "$('.freetexttag').select2({tags: true,width: '100%',multiple:true}).on('select2:unselecting', function(event) { if ($(event.params.args.originalEvent.currentTarget).hasClass('select2-results__option')) { event.preventDefault(); $('.freetexttag').select2({tags: true,width: '100%',multiple:true}).trigger('close'); } });";
        html += "$('.select2class').select2({width: '100%'});";
        html += "$('.viewmodeonly').remove();";
    } else {
        html += "$('.freetexttag').select2({tags: true,width: '100%',multiple:true,disabled:true});";
        html += "$('.select2class').select2({width: '100%',disabled:true});";
        html += "$('.editmodeonly').remove();";
    }
    html += "</script>";
    return html;
}

function applyBodyOnloadJS2(mode: string) {
    if (mode === "edit") {
        $(".freetexttag")
            .select2({ tags: true, width: "100%", multiple: true })
            .on("select2:unselecting", event => {
                // check if originalEvent.currentTarget.className is "select2-results__option" (in other words if it was raised by a item in the dropdown)
                if ($(event.target).hasClass("select2-results__option")){
                    event.preventDefault();
                    // close the dropdown
                    $(".js-example-tags").select2().trigger("close");
                }
            });
        $(".select2class").select2({ width: "100%" });
        $(".viewmodeonly").remove();
    } else {
        $(".freetexttag").select2({ tags: true, width: "100%", multiple: true, disabled: true });
        $(".select2class").select2({ width: "100%", disabled: true });
        $(".editmodeonly").remove();
    }
}

function formatNumber(input, n, x, d) {
    if ((typeof input == 'undefined')) {
        return '';
    }
    var toNumber = parseInt(input);
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return toNumber.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1' + d);
}

function validateData(data, validationObj, errors) {
    if (typeof validationObj == 'undefined') {
        return errors;
    }
    var rule= validationObj.ruleexpr;
    var errorMessage = validationObj.errormessage;
    rule = rule.replace("{{value}}", data);
    if (eval($("<span />", { html: rule}).text())) {
        errors.push(errorMessage);
    }
    return errors;

}

function appendErrors(ui, errors) {
    if(errors != null && errors.length>0) {
        ui += "<ul class='validationErrors'>";
        $.each(errors, function(index, element) {
            ui += "<li>" + element + "</li>";
        });
        ui += "</ul>";
    }
    return ui;
}

function getLabelText(label) {
    if (label.length > 0) {
        return '<div class="label">' + label + '</div>';
    } else {
        return '';
    }
}

function registerDomEventHandlers($element: JQuery) {
    let elementId = $element.attr("id");
    let selectors = {
        buttons: {
            edit: ".fundamentals__button--edit",
            save: ".fundamentals__button--save",
            cancel: ".fundamentals__button--cancel"
        }
    };

    $element.on("click", selectors.buttons.edit, () => {
        toggleView("edit", elementId);
        skuid.events.publish("ats.candidateFundamentals.toggleView.edit", [{ $root: $element }]);
    });

    $element.on("click", selectors.buttons.save, () => {
        updateJson(elementId);
    });

    $element.on("click", selectors.buttons.cancel, () => {
        toggleView("view", elementId);
    });
}
