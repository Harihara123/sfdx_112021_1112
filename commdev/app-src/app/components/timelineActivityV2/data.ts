// Library
import * as core from "../../../library/core";
import * as date from "../../../library/date";
import * as text from "../../../library/text";

export interface Activity {
    model: skuid.model.Model;
    state: ActivityState;
    events: Events;
    stepBy: number;
    visibleCount: VisibleCount;
    hasMoreRows: boolean;
    selectors: Selectors;
    nodes: Nodes;
}

export interface Properties {
    elementId: string;
    nextStepsModelName: string;
    pastActivityModelName: string;
    includeButtons: boolean;
    includeLoadMore: boolean;
    loadMoreCount: number;
    events: {
        wishCouldMarkComplete: string;
        wishCouldEdit: string;
        wishCouldDelete: string;
    };
}

export interface Events {
    wishCouldMarkComplete: string;
    wishCouldEdit: string;
    wishCouldDelete: string;
    wishCouldLoadMore: string;
}

export interface Nodes {
    $header: JQuery;
    $body: JQuery;
    $footer: JQuery;
}

export class VisibleCount {
    private _count: number;
    constructor(count: number) { this._count = count; }
    setCount(count: number) { this._count = count; }
    getCount() { return this._count; }
}

export interface Elements {
    $markComplete: JQuery;
    $edit: JQuery;
    $delete: JQuery;
    $loadMore: JQuery;
}

export interface Selectors {
    markComplete: string;
    edit: string;
    delete: string;
    loadMore: string;
}

export function elementsFrom(selectors: Selectors): Elements {
    return {
        $markComplete: $(selectors.markComplete),
        $edit: $(selectors.edit),
        $delete: $(selectors.delete),
        $loadMore: $(selectors.loadMore)
    };
}

export enum ActivityState { nextSteps, pastActivity }

interface ViaSomeActivityState<r> {
    caseOfNextSteps: () => r;
    caseOfPastActivity: () => r;
    caseOfNeither: (msg: string) => r;
}

export function viaSomeActivityState<r>(activityState: ActivityState, via: ViaSomeActivityState<r>): r {
    switch(activityState) {
        case ActivityState.nextSteps: return via.caseOfNextSteps();
        case ActivityState.pastActivity: return via.caseOfPastActivity();
        default: return via.caseOfNeither(`Unexpected activityState ${activityState}`);
    }
}

export function outOfActivityState(activityState: ActivityState): string {
    switch(activityState) {
        case ActivityState.nextSteps: return "OpenActivities";
        case ActivityState.pastActivity: return "ActivityHistories";
        default: return core.fail<string>(`Unexpected activityState ${activityState}`);
    }
}

export enum SortOrder { ascending, descending }

export function outOfSortOrder(sortOrder: SortOrder): string {
    if (sortOrder === SortOrder.ascending) return "ASC";
    else if (sortOrder === SortOrder.descending) return "DESC";
    else return core.fail<string>(`Unexpected sortOrder ${sortOrder}`);
}

export enum QueryType { unfiltered, filtered }

export interface ViaSomeQueryType<r> {
    caseOfUnfiltered: () => r;
    caseOfFiltered: () => r;
    caseOfNeither: (msg: string) => r;
}

export function viaSomeQueryType<r>(queryType: QueryType, via: ViaSomeQueryType<r>): r {
    switch (queryType) {
        case QueryType.unfiltered: return via.caseOfUnfiltered();
        case QueryType.filtered: return via.caseOfFiltered();
        default: return via.caseOfNeither(`Unexpected queryType ${queryType}`);
    }
}

// export function queryFrom(
//     activityState: ActivityState,
//     sortOrder: SortOrder,
//     stepBy: number,
//     subQuery: string
// ): string {
//     let query = `SELECT (SELECT Id, ActivityDate, StartDateTime, EndDateTime, Pre_Meeting_Notes__C, ActivityType, Description, IsAllDayEvent, IsClosed, IsTask, Location, Priority, Status, Subject, Completed__c FROM ${outOfActivityState(activityState)} `;
//
//     if (text.isNotEmpty(subQuery)) query += subQuery;
//
//     query += `ORDER BY ActivityDate ${outOfSortOrder(sortOrder)} NULLS LAST, LastModifiedDate DESC LIMIT ${stepBy + 1}) FROM Account WHERE id = `;
//
//     return query;
// }

export interface ViaSomeRow<r> {
    caseOfTaskRow: (taskRow: TaskRow) => r;
    caseOfEventRow: (eventRow: EventRow) => r;
    caseOfNeither: (reason: string) => r;
}

export function viaSomeRow<a, r>(someRow: a, via: ViaSomeRow<r>): r {
    if (typeof someRow["IsTask"] === "boolean") {
        if (someRow["IsTask"]) return via.caseOfTaskRow(taskRowFrom(someRow));
        else return via.caseOfEventRow(eventRowFrom(someRow));
    } else {
        return via.caseOfNeither(`Unexpected boolean value ${someRow["IsTask"]}. Expected true or false.`);
    }
}

export interface TaskRow {
    id: string;
    activityType: string;
    subject: string;
    priority: string;
    status: string;
    comments: string;
    activityDate: Date;
    startDateTime: Date;
    isAllDayEvent: boolean;
    isClosed: boolean;
}

export function taskRowFrom<a>(row: a): TaskRow {
    return {
        id: core.nullable.asSomeOrElse<string>(row["Id"], text.emptyString),
        activityType: core.nullable.asSomeOrElse<string>(row["ActivityType"], text.emptyString),
        subject: core.nullable.asSomeOrElse<string>(row["Subject"], text.emptyString),
        priority: core.nullable.asSomeOrElse<string>(row["Priority"], text.emptyString),
        status: core.nullable.asSomeOrElse<string>(row["Status"], text.emptyString),
        comments: core.nullable.asSomeOrElse<string>(row["Comments"], text.emptyString),
        activityDate: core.nullable.asSomeOrElse<Date>(date.from(row["ActivityDate"]), date.alwaysNow),
        startDateTime: core.nullable.asSomeOrElse<Date>(date.from(row["StartDateTime"]), date.alwaysNow),
        isAllDayEvent: core.nullable.asSomeOrElse<boolean>(row["IsAllDayEvent"], false),
        isClosed: core.nullable.asSomeOrElse<boolean>(row["IsClosed"], false)
    };
}

export interface EventRow {
    id: string;
    activityType: string;
    subject: string;
    activityDate: Date;
    startDateTime: Date;
    endDateTime: Date;
    isAllDayEvent: boolean;
    isClosed: boolean;
    preMeetingNotes: string;
    postMeetingNotes: string;
}

export function eventRowFrom<a>(row: a): EventRow {
    return {
        id: core.nullable.asSomeOrElse<string>(row["Id"], text.emptyString),
        activityType: core.nullable.asSomeOrElse<string>(row["ActivityType"], text.emptyString),
        subject: core.nullable.asSomeOrElse<string>(row["Subject"], text.emptyString),
        activityDate: core.nullable.asSomeOrElse<Date>(date.from(row["ActivityDate"]), date.alwaysNow),
        startDateTime: core.nullable.asSomeOrElse<Date>(date.from(row["StartDateTime"]), date.alwaysNow),
        endDateTime: core.nullable.asSomeOrElse<Date>(date.from(row["EndDateTime"]), date.alwaysNow),
        isAllDayEvent: core.nullable.asSomeOrElse<boolean>(row["IsAllDayEvent"], false),
        isClosed: core.nullable.asSomeOrElse<boolean>(row["IsClosed"], false),
        preMeetingNotes: core.nullable.asSomeOrElse<string>(row["Pre_Meeting_Notes__c"], text.emptyString),
        postMeetingNotes: core.nullable.asSomeOrElse<string>(row["Post_Meeting_Notes__c"], text.emptyString)
    };
}

export function taskIconFrom(activity: string) {
    switch (activity) {
        case "Attempted Contact": return 'custom.custom6.slds-icon-standard-task.task';
        case "BizDev  Call": return 'standard.call.slds-icon-standard-log-a-call.call';
        case "Maintenance Call": return 'standard.call.slds-icon-standard-log-a-call.call';
        case "Tickler Call": return 'standard.call.slds-icon-standard-log-a-call.call';
        case "Correspondence": return 'custom.custom18.slds-icon-standard-task.task';
        case "Email": return 'standard.email.slds-icon-standard-email.email';
        case "To Do": return 'action.new_task.slds-icon-standard-task.task';
        default: return 'standard.task.slds-icon-standard-task.task';
    }
}

export function eventIconFrom(activity: string) {
    switch (activity) {
        case "Meeting": return 'standard.event.slds-icon-standard-event.event';
        case "Breakfast": return 'custom.custom51.slds-icon-standard-event.event';
        case "Lunch": return 'custom.custom51.slds-icon-standard-event.event';
        case "Dinner": return 'custom.custom51.slds-icon-standard-event.event';
        case "Out of Office": return 'custom.custom24.slds-icon-standard-event.event';
        case "Networking": return 'standard.social.slds-icon-standard-event.event';
        case "Contractor Meeting": return 'custom.custom14.slds-icon-standard-event.event';
        case "Other": return 'action.log_event.slds-icon-standard-event.event';
        default: return 'standard.event.slds-icon-standard-event.event';
    }
}
