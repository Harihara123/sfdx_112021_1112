// helpers
import * as skuidModelHelpers from "../../../helpers/skuid/model";

export const models = {

    persistentPastActivityModel: skuidModelHelpers.modelFromXML(`
        <model id="PastActivity" limit="20" query="false" createrowifnonefound="false" adapter="REST" processonclient="true" service="SFDCREST" type="" verb="GET" endpoint="/services/data/v34.0/query?q={{queryparam}}&apos;{{accountId}}&apos;" pathtocontent="records.ActivityHistories.records" unloadwarningifunsavedchanges="false" _parentpageid="sk-page-a1EJ0000001OP6dMAG-cmp-1452270093615-1">
            <fields>
                <field id="ActivityDate" label="ActivityDate" ispolymorphic="false" isnamepointing="false" displaytype="DATE" accessible="true" filterable="true"/>
                <field id="ActivityType" label="ActivityType" ispolymorphic="false" isnamepointing="false" displaytype="STRING" accessible="true" filterable="true"/>
                <field id="Description" label="Description" ispolymorphic="false" isnamepointing="false" displaytype="STRING" accessible="true" filterable="true"/>
                <field id="IsAllDayEvent" label="IsAllDayEvent" ispolymorphic="false" isnamepointing="false" displaytype="BOOLEAN" accessible="true" filterable="true"/>
                <field id="IsClosed" label="IsClosed" ispolymorphic="false" isnamepointing="false" displaytype="BOOLEAN" accessible="true" filterable="true"/>
                <field id="IsTask" label="IsTask" ispolymorphic="false" isnamepointing="false" displaytype="BOOLEAN" accessible="true" filterable="true"/>
                <field id="Location" label="Location" ispolymorphic="false" isnamepointing="false" displaytype="STRING" accessible="true" filterable="true"/>
                <field id="Priority" label="Priority" ispolymorphic="false" isnamepointing="false" displaytype="STRING" accessible="true" filterable="true"/>
                <field id="Status" label="Status" ispolymorphic="false" isnamepointing="false" displaytype="STRING" accessible="true" filterable="true"/>
                <field id="Subject" label="Subject" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
                <field id="Completed__c" label="Completed__c" ispolymorphic="false" isnamepointing="false" displaytype="DATETIME" accessible="true" filterable="true"/>
            </fields>
            <conditions>
                <condition type="fieldvalue" value="+SELECT++(SELECT+ActivityDate,+ActivityType,+Description,+IsAllDayEvent,+IsClosed,+IsTask,+Location,+Priority,+Status,+Subject,+Completed__c+FROM+ActivityHistories++ORDER+BY+ActivityDate+DESC+NULLS+LAST,+LastModifiedDate+DESC++LIMIT+1)+FROM+Account+WHERE+id+=+" enclosevalueinquotes="true" sourcetype="param" sourceparam="queryparam" state="filterableoff" inactive="true" name="queryparam"/>
                <condition type="param" value="accountId" sourcetype="param" sourceparam="accountId" operator="=" enclosevalueinquotes="true" novaluebehavior="" state="filterableoff" inactive="true" name="accountId"/>
            </conditions>
            <actions/>
        </model>
    `),

    persistentOpenActivityModel: skuidModelHelpers.modelFromXML(`
        <model id="PersistentOpenActivityModel" limit="20" query="false" createrowifnonefound="false" adapter="REST" processonclient="true" service="SFDCREST" type="" verb="GET" endpoint="/services/data/v34.0/query?q={{queryparam}}&apos;{{accountId}}&apos;" pathtocontent="records.OpenActivities.records" unloadwarningifunsavedchanges="false" _parentpageid="sk-page-a1EJ0000001OP6dMAG-cmp-1452270093615-1">
            <fields>
                <field id="ActivityDate" label="ActivityDate" ispolymorphic="false" isnamepointing="false" displaytype="DATE" accessible="true" filterable="true"/>
                <field id="ActivityType" label="ActivityType" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
                <field id="Description" label="Description" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
                <field id="IsAllDayEvent" label="IsAllDayEvent" ispolymorphic="false" isnamepointing="false" displaytype="BOOLEAN" accessible="true" filterable="true"/>
                <field id="IsClosed" label="IsClosed" ispolymorphic="false" isnamepointing="false" displaytype="BOOLEAN" accessible="true" filterable="true"/>
                <field id="IsTask" label="IsTask" ispolymorphic="false" isnamepointing="false" displaytype="BOOLEAN" accessible="true" filterable="true"/>
                <field id="Location" label="Location" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
                <field id="Priority" label="Priority" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
                <field id="Status" label="Status" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
                <field id="Subject" label="Subject" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
                <field id="Completed__c" label="Completed__c" ispolymorphic="false" isnamepointing="false" displaytype="TEXT" accessible="true" filterable="true"/>
            </fields>
            <conditions>
                <condition type="fieldvalue" value="SELECT++(SELECT+ActivityDate,+ActivityType,+Description,+IsAllDayEvent,+IsClosed,+IsTask,+Location,+Priority,+Status,+Subject,+Completed__c+FROM+OpenActivities++ORDER+BY+ActivityDate+ASC+NULLS+LAST,+LastModifiedDate+DESC++LIMIT+1)+FROM+Account+WHERE+id+=+" enclosevalueinquotes="true" sourcetype="param" sourceparam="queryparam" state="filterableoff" inactive="true" name="queryparam"/>
                <condition type="param" value="accountId" sourcetype="param" sourceparam="accountId" operator="=" enclosevalueinquotes="true" novaluebehavior="" state="filterableoff" inactive="true" name="accountId"/>
            </conditions>
            <actions/>
        </model>
    `)

};
