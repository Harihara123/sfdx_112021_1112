export function filterOnActivities(eventName, queryFilter, querySortOrder) {

    // var arrayData = [getQueryParamActivity(queryFilter, querySortOrder)];
    //
    // skuid.events.publish(eventName, arrayData);

}

export function onAddEditActivityQueryModeles(eventName, queryFilter, querySortOrder) {

    // var arrayData = [getQueryParamActivity(queryFilter, querySortOrder)];
    //
    // skuid.events.publish(eventName, arrayData);

}

export function getQueryLimitActivity(modelLength, constantValue, notOnLoadMore) {

    var queryLimit = 1;

    if (modelLength <= constantValue) {
        queryLimit = constantValue;
    } else {
        if (notOnLoadMore) {
            queryLimit = constantValue;
        } else {
            queryLimit = (modelLength -1) + constantValue;
        }
    }
    return queryLimit;

}

export function getQueryFilterTimeLineActivity() {
    // var selectedMapTask = getSelectedCheckboxValues("TASK");
    // var selectedMapEvent = getSelectedCheckboxValues("EVENT");

    var selectedValues = '';

    // if (selectedMapTask["allValuesSelected"] && selectedMapEvent["allValuesSelected"]) {
    //   selectedValues = '';
    // } else {
    //   if ((selectedMapTask["selectedValues"].length > 0) && (selectedMapEvent["selectedValues"].length > 0)) {
    //        selectedValues =  selectedMapTask["selectedValues"].concat(",").concat(selectedMapEvent["selectedValues"]);
    //   } else if (selectedMapTask["selectedValues"].length > 0) {
    //       selectedValues =  selectedMapTask["selectedValues"];
    //   } else if (selectedMapEvent["selectedValues"].length > 0) {
    //       selectedValues =  selectedMapEvent["selectedValues"];
    //   }
    // }
  //console.log("selectedValues: " + selectedValues);
  return selectedValues;
}
