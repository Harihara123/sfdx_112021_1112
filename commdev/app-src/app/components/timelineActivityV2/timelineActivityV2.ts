// Library
import * as array from "../../../library/array";
import * as core from "../../../library/core";
import * as numbers from "../../../library/numbers";
import * as optional from "../../../library/optional";
import * as text from "../../../library/text";
import * as utils from "../../../library/utils";

// Common
import * as commonUI from "../../common/ui";

// Data
import * as timelineData from "./data";

// Helpers
import * as skuidModelHelpers from "../../../helpers/skuid/model";
import * as skuidUIHelpers from "../../../helpers/skuid/ui";

// Parts
import * as view from "./view";

export function timelineActivityV2(properties: timelineData.Properties) {

    let nextSteps: timelineData.Activity = {
        model: optional.asSomeOrFail(
            skuidModelHelpers.getModelOpt(properties.nextStepsModelName),
            text.emptyString
        ),
        state: timelineData.ActivityState.nextSteps,
        events: {
            wishCouldMarkComplete: properties.events.wishCouldMarkComplete,
            wishCouldEdit: properties.events.wishCouldEdit,
            wishCouldDelete: properties.events.wishCouldDelete,
            wishCouldLoadMore: "ats.activityTimeline.nextSteps.wishCouldLoadMore"
        },
        stepBy: properties.loadMoreCount,
        visibleCount: new timelineData.VisibleCount(properties.loadMoreCount),
        hasMoreRows: false,
        selectors: {
            markComplete: ".activity-timeline__mark-complete",
            edit: ".activity-timeline__button--edit",
            delete: ".activity-timeline__button--delete",
            loadMore: ".activity-timeline__load-more--next-steps"
        },
        nodes: {
            $header: $("<div/>").addClass('activity-timeline__header').append(commonUI.labels.unsorted.nextSteps),
            $body: $("<div/>").addClass("activity-timeline__body activity-timeline__body--next-steps"),
            $footer: $("<div/>").addClass("activity-timeline__footer")
        }
    };

    let pastActivity: timelineData.Activity = {
        model: optional.asSomeOrFail(
            skuidModelHelpers.getModelOpt(properties.pastActivityModelName),
            text.emptyString
        ),
        state: timelineData.ActivityState.pastActivity,
        events: {
            wishCouldMarkComplete: properties.events.wishCouldMarkComplete,
            wishCouldEdit: properties.events.wishCouldEdit,
            wishCouldDelete: properties.events.wishCouldDelete,
            wishCouldLoadMore: "ats.activityTimeline.pastActivity.wishCouldLoadMore"
        },
        stepBy: properties.loadMoreCount,
        visibleCount: new timelineData.VisibleCount(properties.loadMoreCount),
        hasMoreRows: false,
        selectors: {
            markComplete: ".activity-timeline__mark-complete",
            edit: ".activity-timeline__button--edit",
            delete: ".activity-timeline__button--delete",
            loadMore: ".activity-timeline__load-more--past-activity"
        },
        nodes: {
            $header: $("<div/>")
                .addClass('activity-timeline__header')
                .append(commonUI.labels.candidate.activities.pastActivity),
            $body: $("<div/>").addClass("activity-timeline__body activity-timeline__body--past-activity"),
            $footer: $("<div/>").addClass("activity-timeline__footer")
        }
    };

    // Next Steps
    let willBeNextSteps = willBeLoadedActivitiesFrom(
        nextSteps.model,
        nextSteps.state,
        nextSteps.stepBy,
        text.emptyString,
        timelineData.QueryType.unfiltered
    ).then(nodesFromOver(
        nextSteps.nodes,
        nextSteps.state,
        nextSteps.visibleCount,
        nextSteps.events,
        nextSteps.selectors,
        nextSteps.stepBy,
        properties.includeLoadMore,
        properties.includeButtons,
        commonUI.labels.candidate.activities.noNextSteps
    ))
    .catch((error: string) => core.scheduleError(error));

    // Past Activity
    let willBePastActivity = willBeLoadedActivitiesFrom(
        pastActivity.model,
        pastActivity.state,
        pastActivity.stepBy,
        text.emptyString,
        timelineData.QueryType.unfiltered
    ).then(nodesFromOver(
        pastActivity.nodes,
        pastActivity.state,
        pastActivity.visibleCount,
        pastActivity.events,
        pastActivity.selectors,
        pastActivity.stepBy,
        properties.includeLoadMore,
        properties.includeButtons,
        commonUI.labels.candidate.activities.noPastActivity
    ))
    .catch((error: string) => core.scheduleError(error));

    // On document ready
    $(document).ready(() => {
        Promise.all(array.fromTwo(willBeNextSteps, willBePastActivity))
            .then(result => {
                // Render the timeline
                view.render(properties.elementId, nextSteps.nodes, pastActivity.nodes);

                // Register listeners for next steps
                registerDomEventListeners(nextSteps.events, nextSteps.selectors);
                registerEventSubscriptions(
                    nextSteps.model,
                    nextSteps.events,
                    nextSteps.selectors,
                    nextSteps.state,
                    nextSteps.stepBy,
                    nextSteps.nodes,
                    nextSteps.visibleCount,
                    properties.includeLoadMore,
                    properties.includeButtons,
                    commonUI.labels.candidate.activities.noNextSteps
                );

                // Register listeners for past activity
                registerDomEventListeners(pastActivity.events, pastActivity.selectors);
                registerEventSubscriptions(
                    pastActivity.model,
                    pastActivity.events,
                    pastActivity.selectors,
                    pastActivity.state,
                    pastActivity.stepBy,
                    pastActivity.nodes,
                    pastActivity.visibleCount,
                    properties.includeLoadMore,
                    properties.includeButtons,
                    commonUI.labels.candidate.activities.noPastActivity
                );
            })
            .catch((error: string) => core.scheduleError(error));
    });
}

function nodesFromOver(
    nodes: timelineData.Nodes,
    activityState: timelineData.ActivityState,
    visibleCount: timelineData.VisibleCount,
    events: timelineData.Events,
    selectors: timelineData.Selectors,
    stepBy: number,
    includeLoadMore: boolean,
    includeButtons: boolean,
    noContentLabel: string
) {
    return function nodesFrom(model: skuid.model.Model) {
        let rows = model.getRows();
        let count = visibleCount.getCount();

        if (hasMoreDataActivity(rows.length, count)) {
            rows = rows.slice(0, rows.length - 1);
            visibleCount.setCount(count + stepBy);
            if (includeLoadMore) {
                nodes.$footer.empty().append(view.footerFrom(activityState));
            } else {
                nodes.$footer.empty();
            }
        } else {
            nodes.$footer.empty();
        }

        nodes.$body.empty().append(
            array.withSomeOrElse(
                rows,
                rows => view.bodyFrom(model, rows),
                none => $("<span/>")
                    .addClass("activity-timeline__no-content")
                    .append(noContentLabel)
            )
        );

        if (!includeButtons) $(".activity-timeline__button", nodes.$body).remove();

        registerDomEventListeners(events, selectors);
    };
}

function hasMoreDataActivity(rowCount: number, visibleCount: number): boolean {
    if (rowCount > visibleCount) return true;
    else return false;
}

function updateConditions(
    model: skuid.model.Model,
    activityState: timelineData.ActivityState,
    stepBy: number,
    queryFilter: string,
    queryType: timelineData.QueryType
) {
    model.cancel();
    optional.withSomeOfAllOrFail(
        [
            skuidModelHelpers.getConditionOpt(model, "accountId", false),
            skuidModelHelpers.getConditionOpt(model, "activityState", false),
            skuidModelHelpers.getConditionOpt(model, "loadMoreCount", false),
            skuidModelHelpers.getConditionOpt(model, "sortOrder", false),
            skuidModelHelpers.getConditionOpt(model, "queryFilter", false)
        ],
        conditions => {
            let [
                accountIdCondition,
                activityStateCondition,
                loadMoreCountCondition,
                sortOrderCondition,
                queryFilterCondition
            ] = conditions;

            // Deactivate conditions
            skuidModelHelpers.deactivateConditions(model, conditions);

            // Update condition values
            model.setCondition(accountIdCondition, core.accessors.valueOf(utils.getQueryParamAtOrFail("accountId")));
            model.setCondition(activityStateCondition, timelineData.outOfActivityState(activityState));
            model.setCondition(loadMoreCountCondition, stepBy);

            timelineData.viaSomeQueryType(queryType, {
                caseOfUnfiltered() {
                    // Activate conditions
                    skuidModelHelpers.activateConditions(
                        model,
                        array.fromFour(
                            accountIdCondition,
                            activityStateCondition,
                            loadMoreCountCondition,
                            sortOrderCondition
                        )
                    );
                },
                caseOfFiltered() {
                    model.setCondition(queryFilterCondition, `ActivityType IN (${formatSubquery(queryFilter)})`);

                    // Activate conditions
                    skuidModelHelpers.activateConditions(model, conditions);
                },
                caseOfNeither(reason) { core.fail<void>(reason); }
            });
        },
        text.emptyString
    );
}

// function queryFrom(
//     model: skuid.model.Model,
//     activityState: timelineData.ActivityState,
//     stepBy: number,
//     queryFilter: string,
//     queryType: timelineData.QueryType
// ): string {
//     // HACK: Unsure of where query type should actually be coming from
//     let sortOrder = timelineData.viaSomeActivityState(activityState, {
//         caseOfNextSteps() { return timelineData.SortOrder.ascending; },
//         caseOfPastActivity() { return timelineData.SortOrder.descending; },
//         caseOfNeither(reason) { return core.fail<timelineData.SortOrder>(reason); }
//     });
//
//     return timelineData.viaSomeQueryType(queryType, {
//         caseOfUnfiltered() {
//             return timelineData.queryFrom(activityState, sortOrder, stepBy, text.emptyString);
//         },
//         caseOfFiltered() {
//             let subQuery = `ActivityType IN (${formatSubquery(queryFilter)})`;
//             return timelineData.queryFrom(activityState, sortOrder, stepBy, subQuery);
//         },
//         caseOfNeither(reason) { return core.fail<string>(reason); }
//     });
// }

function formatSubquery(queryFilter: string) {
    return queryFilter.split(',').reduce((prev, next, index, array) => {
        if (index < (array.length - 1)) return prev += "'" + next + "', ";
        else return prev += "'" + next + "'";
    }, text.emptyString);
}

function willBeLoadedActivitiesFrom(
    model: skuid.model.Model,
    activityState: timelineData.ActivityState,
    stepBy: number,
    queryFilter: string,
    queryType: timelineData.QueryType
): Promise<skuid.model.Model> {
    updateConditions(model, activityState, stepBy, queryFilter, queryType);
    return skuidModelHelpers.updateData(model);
}

function registerDomEventListeners(events: timelineData.Events, selectors: timelineData.Selectors) {
    let elements = timelineData.elementsFrom(selectors);

    elements.$markComplete.on("click", () => {
        skuid.events.publish(events.wishCouldMarkComplete);
    });
    elements.$edit.on("click", () => {
        skuid.events.publish(events.wishCouldEdit)
    });
    elements.$delete.on("click", () => {
        skuid.events.publish(events.wishCouldDelete)
    });
    elements.$loadMore.on("click", (event) => {
        event.preventDefault();
        skuid.events.publish(events.wishCouldLoadMore);
    });
};

function registerEventSubscriptions(
    model: skuid.model.Model,
    events: timelineData.Events,
    selectors: timelineData.Selectors,
    activityState: timelineData.ActivityState,
    stepBy: number,
    nodes: timelineData.Nodes,
    visibleCount: timelineData.VisibleCount,
    includeLoadMore: boolean,
    includeButtons: boolean,
    noContentLabel: string
) {
    skuid.events.subscribe(
        events.wishCouldLoadMore,
        () => {
            let willBeLoadedActivities = willBeLoadedActivitiesFrom(
                model,
                activityState,
                visibleCount.getCount(),
                text.emptyString,
                timelineData.QueryType.unfiltered
            )
            .then(nodesFromOver(
                nodes,
                activityState,
                visibleCount,
                events,
                selectors,
                stepBy,
                includeLoadMore,
                includeButtons,
                noContentLabel
            ))
            .catch((error: string) => core.scheduleError(error));
        }
    );
}
