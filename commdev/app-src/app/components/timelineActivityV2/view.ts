// Common
import * as commonUI from "../../common/ui";

// Data
import * as timelineData from "./data";

// Helpers
import * as skuidModelHelpers from "../../../helpers/skuid/model";

// Library
import * as array from "../../../library/array";
import * as core from "../../../library/core";

// Parts
import * as timeline from "./timelineActivityV2";

export function render(
    rootSelector: string,
    nextStepsNodes: timelineData.Nodes,
    pastActivityNodes: timelineData.Nodes
): void {
    $(rootSelector)
        .append(nextStepsNodes.$header)
        .append(nextStepsNodes.$body)
        .append(nextStepsNodes.$footer)
        .append($("<hr/>").addClass("activity-timeline__separator"))
        .append(pastActivityNodes.$header)
        .append(pastActivityNodes.$body)
        .append(pastActivityNodes.$footer);
}

export function bodyFrom<a>(
    model: skuid.model.Model,
    rows: a[]
): JQuery {
    return $("<ul/>").addClass("slds-timeline").append(
        rows.map(row => {
            return timelineData.viaSomeRow(row, {
                caseOfTaskRow: taskTemplateFrom,
                caseOfEventRow: eventTemplateFrom,
                caseOfNeither(reason) {
                    return core.fail<JQuery>(reason);
                }
            })
        })
    );
}

export function footerFrom(activityState: timelineData.ActivityState) {
    return timelineData.viaSomeActivityState(activityState, {
        caseOfNextSteps() {
            return $("<a/>")
                .attr("href", "#")
                .addClass("activity-timeline__load-more--next-steps")
                .text(`${commonUI.labels.unsorted.loadMore}`);
        },
        caseOfPastActivity() {
            return $("<a/>")
                .attr("href", "#")
                .addClass("activity-timeline__load-more--past-activity")
                .text(`${commonUI.labels.unsorted.loadMore}`);
        },
        caseOfNeither(reason) {
            return core.fail<JQuery>(reason);
        }
    });
}

export function taskTemplateFrom(taskRow: timelineData.TaskRow): JQuery {
    let [sprite, icon, iconColor, lineColor] = timelineData.taskIconFrom(taskRow.activityType).split(".");

    return $(`
        <li class="slds-timeline__item">
            <div class="slds-media">
                <div class="slds-media__body">
                    <div class="slds-media slds-media--timeline slds-timeline__media--${lineColor}">
                        <div class="slds-media__figure">
                            <svg aria-hidden="true" class="slds-icon ${iconColor} slds-timeline__icon">
                                <use xlink:href="/resource/LightningDesignSystem/assets/icons/${sprite}-sprite/svg/symbols.svg#${icon}"></use>
                            </svg>
                        </div>
                        <div class="slds-media__body">
                            <div class="slds-media slds-tile slds-media--small">
                                <div class="slds-media__figure">
                                    <label class="slds-checkbox" for="mark-complete_${taskRow.id}">
                                        <input name="checkbox" type="checkbox" class="activity-timeline__mark-complete" id="mark-complete_${taskRow.id}" />
                                        <span class="slds-checkbox--faux"></span>
                                        <span class="slds-form-element__label slds-assistive-text">mark-complete</span>
                                    </label>
                                </div>
                                <div class="slds-media__body">
                                    <p class="slds-tile__title slds-truncate">${taskRow.subject}</p>
                                    <ul class="slds-tile__detail">
                                        <li class="utility__line-break--top">
                                            <span class="utility__font-weight--bold">Type:</span>
                                            <span>${taskRow.activityType}</span>
                                        </li>
                                        <li>
                                            <span class="utility__font-weight--bold">Status:</span>
                                            <span>${taskRow.status}</span>
                                        </li>
                                        <li>
                                            <span class="utility__font-weight--bold">Priority:</span>
                                            <span>${taskRow.priority}</span>
                                        </li>
                                        <li class="utility__line-break--top">
                                            <span class="utility__font-weight--bold">Comments:</span>
                                            <span>${taskRow.comments}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slds-media__figure slds-media__figure--reverse">
                    <div class="slds-timeline__actions">
                        <p class="slds-tile__title">${skuid.time.formatDate("M dd, yy", taskRow.startDateTime)}</p>
                        <button class="slds-button slds-button--brand activity-timeline__button activity-timeline__button--edit">${commonUI.labels.generic.edit}</button>
                        <button class="slds-button slds-button--neutral activity-timeline__button activity-timeline__button--delete">${commonUI.labels.generic.deleteButton}</button>
                    </div>
                </div>
            </div>
        </li>
    `);
}

export function eventTemplateFrom(eventRow: timelineData.EventRow): JQuery {
    let [sprite, icon, iconColor, lineColor] = timelineData.eventIconFrom(eventRow.activityType).split(".");

    return $(`
        <li class="slds-timeline__item">
            <div class="slds-media">
                <div class="slds-media__body">
                    <div class="slds-media slds-media--timeline slds-timeline__media--${lineColor}">
                        <div class="slds-media__figure">
                            <svg aria-hidden="true" class="slds-icon ${iconColor} slds-timeline__icon">
                                <use xlink:href="/resource/LightningDesignSystem/assets/icons/${sprite}-sprite/svg/symbols.svg#${icon}"></use>
                            </svg>
                        </div>
                        <div class="slds-media__body">
                            <div class="slds-media slds-tile slds-media--small">
                                <div class="slds-media__figure">
                                    <label class="slds-checkbox" for="mark-complete_${eventRow.id}">
                                        <input name="checkbox" type="checkbox" class="activity-timeline__mark-complete" id="mark-complete_${eventRow.id}" />
                                        <span class="slds-checkbox--faux"></span>
                                        <span class="slds-form-element__label slds-assistive-text">mark-complete</span>
                                    </label>
                                </div>
                                <div class="slds-media__body">
                                    <p class="slds-tile__title slds-truncate">${eventRow.subject}</p>
                                    <ul class="slds-tile__detail">
                                        <li class="utility__line-break--top">
                                            <span class="utility__font-weight--bold">Type:</span>
                                            <span>${eventRow.activityType}</span>
                                        </li>
                                        <li>
                                            <span class="utility__font-weight--bold">End Date:</span>
                                            <span>
                                                ${skuid.time.formatDate("M dd, yy", eventRow.endDateTime)}
                                                ${skuid.time.formatTime("h:mm a", eventRow.endDateTime)}
                                            </span>
                                        </li>
                                        <li class="utility__line-break--top">
                                            <span class="utility__font-weight--bold">Pre-Meeting Notes:</span>
                                            <span>${eventRow.preMeetingNotes}</span>
                                        </li>
                                        <li class="utility__line-break--top">
                                            <span class="utility__font-weight--bold">Post-Meeting Notes:</span>
                                            <span>${eventRow.postMeetingNotes}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slds-media__figure slds-media__figure--reverse">
                    <div class="slds-timeline__actions">
                        <p class="slds-tile__title">${skuid.time.formatDate("M dd, yy", eventRow.startDateTime)}</p>
                        <p class="slds-tile__title timeMonthDayYear">${skuid.time.formatTime("h:mm", eventRow.startDateTime)}</p>
                        <p class="slds-tile__title">${skuid.time.formatTime("a", eventRow.startDateTime)}</p>
                        <button class="slds-button slds-button--brand activity-timeline__button activity-timeline__button--edit">${commonUI.labels.generic.edit}</button>
                        <button class="slds-button slds-button--neutral activity-timeline__button activity-timeline__button--delete">${commonUI.labels.generic.deleteButton}</button>
                    </div>
                </div>
            </div>
        </li>
    `);
}
