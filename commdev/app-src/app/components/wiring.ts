import { timelineActivityV2 } from "./timelineActivityV2/timelineActivityV2";
import { jsonMagicFrom } from "./jsonMagic/jsonMagic";
import { htmlRenderFrom } from "./htmlRender/htmlRender";

export function componentWiring() {
    skuid.events.subscribe("ats.component.timelineActivityV2.hasLoaded", timelineActivityV2);
    skuid.events.subscribe("ats.component.jsonMagic.hasLoaded", jsonMagicFrom);
    skuid.events.subscribe("ats.component.htmlRender.hasLoaded", htmlRenderFrom);
}
