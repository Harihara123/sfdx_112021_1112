// Library
import * as core from "../../library/core";
import * as date from "../../library/date";

// Common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

// Helpers
import * as jqueryHelper from "../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

export function footerFromOver(view: commonUI.page.Page) {
    return function footerAs() {

        // Consider the page view
        commonUI.page.viaSomePage(view, {
            caseOfLandingPage: core.ignore,
            caseOfDetailsPage() {
                $("#year").text(date.currentYear());
            },
            caseOfNeither(reason) { core.fail(reason); }
        });
    }
}
