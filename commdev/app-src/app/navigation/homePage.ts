// Library
import * as core from "../../library/core";
import * as ui from "../../library/ui";

// Common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

// Helpers
import * as jqueryHelper from "../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

export function homePageFromOver(view: commonUI.page.Page) {
    return function homePageAs() {

        // Consider the page view
        commonUI.page.viaSomePage(view, {
            caseOfLandingPage: core.ignore,
            caseOfDetailsPage() {
              // Set the page title
              ui.setPageTitle(commonUI.labels.recruiter.recruiterHome);
            },
            caseOfNeither(reason) { core.fail(reason); }
        });
    }
}
