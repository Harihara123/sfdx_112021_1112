// Library
import * as core from "../../library/core";
import * as ui from "../../library/ui";

// Common
import * as commonModel from "../common/model";
import * as commonUI from "../common/ui";

// Helpers
import * as jqueryHelper from "../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../helpers/skuid/model";
import * as skuidUIHelpers from "../../helpers/skuid/ui";

export function masterTemplateFromOver(view: commonUI.page.Page) {
    return function masterTemplateAs() {

        // Consider the page view
        commonUI.page.viaSomePage(view, {
            caseOfLandingPage: core.ignore,
            caseOfDetailsPage() {
              //Hide Skuid "edit page" flyout tab (from the right)
              $('#skuidEditBtnContainer').hide();
            },
            caseOfNeither(reason) { core.fail(reason); }
        });
    }
}
