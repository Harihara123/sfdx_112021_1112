// Helpers
import { ajax } from "../helpers/jquery-helper";

const endpointsAs = () => ({
    geodata: {
        countries: (countryCode: string) => `https://api-dev.allegistest.com/v1/geodata/countries/${countryCode}`,
        states: (countryCode: string) => `https://api-dev.allegistest.com/v1/geodata/countries/${countryCode}/states`
    }
})

export const repositoryAs = () => ({
    geodata: <a>(countryCode: string) => {
        const endpoints = endpointsAs();
        const base = (url: string) => ({
            async: true,
            crossDomain: true,
            url: url,
            method: "GET",
            headers: { "authorization": "Bearer W24xwL9dEQ7yNjkyHUUrCIITH2PE" }
        })

        return {
            countries: () => ajax<a>(base(endpoints.geodata.countries(countryCode))),
            states: () => ajax<a>(base(endpoints.geodata.states(countryCode)))
        }
    }
})
