// Library
import * as array from "../../../library/array";
import * as core from "../../../library/core";
import * as optional from "../../../library/optional";
import * as text from "../../../library/text";
import * as ui from "../../../library/ui";
import * as utils from "../../../library/utils";
import { Event, watch } from "../../../library/events";

// Common
import * as commonModel from "../../common/model";
import * as commonUI from "../../common/ui";
import * as commonData from "../../common/data";
// Helpers
import * as jqueryHelper from "../../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../../helpers/skuid/model";
import * as skuidUIHelpers from "../../../helpers/skuid/ui";
import { ajax, valueOf } from "../../../helpers/jquery-helper";


export function candidateSearchFromOver() {
                    return function candidateSearchFrom() {
                        initCountryAndStatePicklists();
                    };
                }



const initCountryAndStatePicklists = () => {

    const countriesModel = optional.asSomeOrFail(skuidModelHelpers.getModelOpt("Countries"), text.emptyString);

    // Render country and state picklists
    let $countryPicklist = ui.render(
                        $(".candidate__search--country-wrapper"),
                        $("<select />", { class: "candidate__search--country" })
                    ).find(".candidate__search--country");
                    let $statePicklist = ui.render(
                        $(".candidate__search--state-wrapper"),
                        $("<select />", { class: "candidate__search--state" })
                    ).find(".candidate__search--state");

    watch($countryPicklist, Event.change, $countryPicklist => {

                        // Populate the state/province dropdown
                        populateStatePicklist(valueOf<string>($countryPicklist), $statePicklist);
    });

    $countryPicklist.select2({
                        data: array.map(
                            skuidModelHelpers.getRows<commonData.Country>(countriesModel),
                            country => ({ id: country.countryCode, text: country.countryName })
                        )
                    });

    let mailingCountryCode = '';

    // Set value of the $countryPicklist jqElement and trigger a change event
    $countryPicklist.val(mailingCountryCode).trigger("change");};




    const populateStatePicklist = (
        countryCodeOpt: optional.Optional<string>,
        $statePicklist: JQuery
    ): void => {
        optional.bind(countryCodeOpt, countryCode => {
            return optional.map(skuidModelHelpers.getModelOpt("States"), stModel => {
              // Set and enable URL merge condition
              optional.map(
                  skuidModelHelpers.getConditionOpt(stModel, "countryCode", false),
                  countryCodeCondition => {
                      skuidModelHelpers.deactivateConditions(stModel, array.fromOne(countryCodeCondition));
                      stModel.setCondition(countryCodeCondition, countryCode);
                      skuidModelHelpers.activateConditions(stModel, array.fromOne(countryCodeCondition));

                      // Update the States skuid model
                      skuidModelHelpers.updateData(stModel)
                          .then(stModel => {
                              return array.map<commonData.State, IdTextPair>(
                                skuidModelHelpers.getRows<commonData.State>(stModel),
                                state => ({ id: state.stateCode, text: state.stateName })
                              );
                          })
                          .then(statesPairs => {
                            $statePicklist.empty();
                            $statePicklist.select2({ data: statesPairs });

                            // Check if MailingState has an existing value
                            let mailingStateCode = '';

                            $statePicklist
                                .val(mailingStateCode)
                                .trigger("change");
                          })
                          .catch((error: string) => core.scheduleError<IdTextPair[]>(error));
                  }
              );

            })

        })
    };
