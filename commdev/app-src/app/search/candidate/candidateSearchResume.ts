import * as Moment from "moment";

import * as skuidModelHelpers from "../../../helpers/skuid/model";
import * as skuidUIHelpers from "../../../helpers/skuid/ui";
import * as utils from "../../../library/utils";


import {
    asSomeOrElse,
    asSomeOrFail,
    bind,
    map as optMap,
    map3 as optMap3,
    of as optOf,
    Optional,
    withSomeOfBothOrFail,
    withSomeOrElse,
    withSomeOrFail
} from "../../../library/optional";

import { alwaysEmptyString, emptyString, truncate, withSomeOrElse as textWithSomeOrElse } from "../../../library/text";

export function candidateSearchResumeFromOver() {
   return function candidateSearchResumeFrom() {

       displayCandidateResume();


   };
}

function  displayCandidateResume() {

    const resumeSearchModel = asSomeOrFail(skuidModelHelpers.getModelOpt("ResumeSearchModel"), emptyString);

    let talentDocumentId = utils.getQueryParamAtOpt("talentDocumentId");

    skuidModelHelpers.getConditionOpt(resumeSearchModel, "id", false),
            conditionResumeCandidateId => {
                    resumeSearchModel.setCondition(
                        conditionResumeCandidateId,
                        utils.getQueryParamAtOpt("id")
                    );
            }
    skuidModelHelpers.getConditionOpt(resumeSearchModel, "param", false),
                    conditionResumekeyword => {
                            resumeSearchModel.setCondition(
                                conditionResumekeyword,
                                utils.getQueryParamAtOpt("param")
                            );
                    }

    skuidModelHelpers.updateData(resumeSearchModel).then(resumeSearchModel => {
        let resumeArray = (resumeSearchModel  && resumeSearchModel.data && resumeSearchModel.data[0] &&
                                    resumeSearchModel.data[0].response && resumeSearchModel.data[0].response.hits
                        && resumeSearchModel.data[0].response.hits.hits &&  resumeSearchModel.data[0].response.hits.hits &&  resumeSearchModel.data[0].response.hits.hits[0]
                        &&   resumeSearchModel.data[0].response.hits.hits[0].highlight &&  resumeSearchModel.data[0].response.hits.hits[0].highlight.htmlsource
                          ?  resumeSearchModel.data[0].response.hits.hits[0].highlight.htmlsource : '' );
                var displableResume;
                if ( resumeArray !== '') {
                            var  lastUpdatedResume;
                            var  resumeLastModifiedDate;

                            $.each(resumeArray, function(index, resume) {
                               if (talentDocumentId && resume.search(talentDocumentId) >= 0 ) {
                                    displableResume = resume;
                               }
                               let modDate = $(resume).attr("modDate");
                               let lastModifiedDate  =   Moment(modDate, "YYYY-MM-DD HH:mm Z");

                               if (isNaN(resumeLastModifiedDate)) { resumeLastModifiedDate = lastModifiedDate; }
                               if (isNaN(lastUpdatedResume)) { lastUpdatedResume = resume; }

                               if (resumeLastModifiedDate < lastModifiedDate) {lastUpdatedResume = resume;}

                                   resumeLastModifiedDate = lastModifiedDate;
                            });

                            displableResume =  (displableResume ? displableResume : lastUpdatedResume);

                            if (displableResume && displableResume.length > 0) {
                                $("#templateId").append(displableResume);
                            }
                   }
     });

}
