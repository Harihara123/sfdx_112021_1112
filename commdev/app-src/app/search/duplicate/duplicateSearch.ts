// Library
import * as array from "../../../library/array";
import * as core from "../../../library/core";
import * as optional from "../../../library/optional";
import * as text from "../../../library/text";
import * as ui from "../../../library/ui";
import * as utils from "../../../library/utils";

// Common
import * as commonModel from "../../common/model";
import * as commonUI from "../../common/ui";


// Helpers
import * as jqueryHelper from "../../../helpers/jquery-helper";
import * as jqueryUIHelper from "../../../helpers/jquery-ui-helper";
import * as skuidModelHelpers from "../../../helpers/skuid/model";
import * as skuidUIHelpers from "../../../helpers/skuid/ui";




export function duplicateSearchFromOver() {
   return function duplicateSearchFrom() {
       skuid.events.subscribe("ats.duplicate.search.display.add.candidate.button", displayCandidateAddButton);
       skuid.events.subscribe("ats.duplicate.search.add.new", forwardToAddCandidate);

       $(document).keyup(function() {
         enableSearchButtonWhenRequiredFieldAvailable();
       });
   };
}

function forwardToAddCandidate() {
    let firstName =  $('#firstNameId').val();
    let lastName =   $('#lastNameId').val();
    let phone =  $('#phoneId').val();
    let email =   $('#emailId').val();

    let url = "/apex/c__Add_Edit_CandidateContact?fname=" + $('#firstNameId').val() + "&lname=" + $('#lastNameId').val()
            + "&phone=" + $('#phoneId').val() + "&email=" + $('#emailId').val();

    window.location.assign(url);
}

function displayCandidateAddButton() {
      $('#addCandidateButtonId').button('enable');
      $('#searchResultsArea').show();
      $('#addButtonArea').show();
}

//enable duplicate search button only if requird fields are provided
function enableSearchButtonWhenRequiredFieldAvailable() {
    let firstName =  $('#firstNameId').val();
    let lastName =   $('#lastNameId').val();
    let phone =  $('#phoneId').val();
    let email =   $('#emailId').val();

    let requiredFields =   (firstName && firstName.length > 1
                            && lastName && lastName.length > 1
                            &&
                            ( (phone && phone.length > 4)
                            ||
                              (email && email.length > 4)
                            )
                            ? true : false);

      if(requiredFields) {
          $('#duplicateSearchButtonId').button('enable');
      } else {
          $('#duplicateSearchButtonId').button('disable');
      }
}
