// Data
import * as commonUI from "./common/ui";

// Parts
import { candidateSummaryFromOver } from "./candidateSummary/candidateSummary";
import { candidateContactFromOver } from "./candidateContact/candidateContact";
import { candidateActivitiesFromOver } from "./candidateActivities/candidateActivities";
import { candidateQualificationsOver } from "./candidateQualifications/candidateQualifications";
import { candidateEmploymentFromOver } from "./candidateEmployment/candidateEmployment";
import { candidateFundamentalsFromOver } from "./candidateFundamentals/candidateFundamentals";
import { candidateReferenceFrom } from "./candidateReference/candidateReference";
import { candidateDocumentFromOver } from "./candidateDocument/candidateDocument";
import { duplicateSearchFromOver } from "./search/duplicate/duplicateSearch";
import { candidateSearchFromOver } from "./search/candidate/candidateSearch";
import { candidateSearchResumeFromOver } from "./search/candidate/candidateSearchResume";
import { footerFromOver } from "./navigation/footer";
import { homePageFromOver } from "./navigation/homePage";
import { masterTemplateFromOver } from "./navigation/masterTemplate";

export function partsWiring() {

    skuid.events.subscribe(
        "ats.candidateSummary.hasLoaded",
        candidateSummaryFromOver(commonUI.page.Page.Landing)
    );

    skuid.events.subscribe(
        "ats.candidateContact.hasLoaded",
        candidateContactFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.candidateActivities.hasLoaded",
        candidateActivitiesFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.candidateQualifications.hasLoaded",
        candidateQualificationsOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.candidateEmployment.hasLoaded",
        candidateEmploymentFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.candidateFundamentals.hasLoaded",
        candidateFundamentalsFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.footer.setCurrentYear",
        footerFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.homePage.hasLoaded",
        homePageFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.master.template.hasLoaded",
        masterTemplateFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.candidateReferences.hasLoaded",
        candidateReferenceFrom(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.candidateReferencesList.hasLoaded",
        candidateReferenceFrom(commonUI.page.Page.Landing)
    );

    skuid.events.subscribe(
        "ats.candidateDocuments.hasLoaded",
        candidateDocumentFromOver(commonUI.page.Page.Details)
    );

    skuid.events.subscribe(
        "ats.search.candidate.duplicate.check",
        duplicateSearchFromOver()
    );

    skuid.events.subscribe(
        "ats.search.candidate.check",
        candidateSearchFromOver()
    );

    skuid.events.subscribe(
        "ats.search.candidate.display.resume",
        candidateSearchResumeFromOver()
    );


}
