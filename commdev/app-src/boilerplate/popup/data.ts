export let popupXMLTemplate = `
    <popup title="Test" width="90%">
        <components>
            <basicfieldeditor showheader="true" showsavecancel="true" showerrorsinline="false" model="NewModel" buttonposition="" uniqueid="sk-1fq79s-489" mode="edit" layout="above">
                <columns layoutmode="responsive" columngutter="4px" rowgutter="4px">
                    <column ratio="1" minwidth="300px" behavior="flex">
                        <sections>
                            <section title="Section A" collapsible="no" showheader="false">
                                <fields>
                                    <field id="CreatedById"/>
                                    <field id="LastModifiedById"/>
                                </fields>
                            </section>
                        </sections>
                    </column>
                    <column ratio="1" minwidth="300px" behavior="flex">
                        <sections>
                            <section title="Section B" collapsible="no" showheader="false">
                                <fields>
                                    <field id="WhoId"/>
                                </fields>
                            </section>
                        </sections>
                    </column>
                </columns>
            </basicfieldeditor>
        </components>
    </popup>
`;

export let customButtonTemplate = `
    <div class="nx-header-right">
        <div style="display: inline-block;">
            <div class="nx-pagetitle-action ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only custom-button" role="button">
                <span class="ui-button-text">Custom Button</span>
            </div>
        </div>
    </div>
`;
