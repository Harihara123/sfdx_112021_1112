import * as core from "../../library/core";
import * as optional from "../../library/optional";
import * as text from "../../library/text";
import * as popupData from "./data";
import * as skuidModelHelpers from "../../helpers/skuid/model";

export function hasLoaded() {

    let $wrapper = $(".templateComponent");
    let xmlDoc = skuid.utils.makeXMLDoc(popupData.popupXMLTemplate);

    optional.withSomeOrFail(
        skuidModelHelpers.getModelOpt("NewModel"),
        model => optional.withSomeOrFail(
            skuidModelHelpers.getRowOpt(model, "00TJ000000cbg3GMAQ"),
            row => {
                $wrapper.append(
                    skuidModelHelpers.merge(
                        "model",
                        popupData.customButtonTemplate,
                        { nl2br: true, createFields: true },
                        model,
                        row
                    )
                );
            },
            text.emptyString
        ),
        text.emptyString
    );

    $(".custom-button").on("click", () => {
        skuid.utils.createPopupFromPopupXML($(xmlDoc));
    });

}
