import { expect } from "../../library/testing";

let add = (firstNumber: number) => {
    return (secondNumber: number) => {
        return firstNumber + secondNumber;
    }
};

let addV2 = (firstNumber: number) => (secondNumber: number) => firstNumber + secondNumber;

let addOne = add(1);
let addTwo = add(2);

console.log(expect(() => add(2)(2)).toBe(4));
console.log(expect(() => addOne(2)).toBe(3));
console.log(expect(() => addTwo(4)).toBe(6));
