// Library
import { promise } from "../library/async";
import * as optional from "../library/optional";

/**
 * Performs an AJAX request that returns a chainable Promise.
 *
 * If the AJAX request fails, the reject handler will be called, if the AJAX request succeeds, the resolve handler will
 * be called with the result wrapped in an Optional.
 */
export const ajax = <a>(options: JQueryAjaxSettings) => {
    return promise<a, JQueryPromise<a>>((resolve, reject) => {
        return $.ajax(options).fail(reject).done(result => resolve(optional.of<a>(result)));
    });
};

// Returns the value of an attribute on a JQuery element contained in an Optional
export const attributeFrom = <a>($element: JQuery, attributeName: string): optional.Optional<a> => {
    return optional.of<a>(<any>$element.attr(attributeName));
};

// Returns a Promise that resolves when the document is ready
export const documentReady = () => {
    return promise<JQuery, JQuery>((resolve, reject) => {
        return $(document).ready(() => { resolve($(document)); });
    });
};

// Returns the value of a JQuery element contained in an Optional
export const valueOf = <a>($jqElement: JQuery): optional.Optional<a> => {
    return optional.of<a>($jqElement.val());
};
