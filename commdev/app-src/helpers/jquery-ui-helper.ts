export module dialog {

    export interface Button {
        text: string;
        class: string;
        icons: { primary: string; };
        click: <a>(event: Event) => a;
        showText: boolean;
    }

    /**
     * Creates and returns a JQuery UI dialog with the passed properties, optionally opening the dialog on creation.
     */
    export function from<a>(
        xmlTemplate: string,
        buttons: dialog.Button[],
        onOpenEvent: string,
        onCloseEvent: string,
        isVisible: boolean
    ): JQuery {
        let $dialog = skuid.utils.createPopupFromPopupXML($(skuid.utils.makeXMLDoc(xmlTemplate)));

        $dialog.dialog({
            buttons: buttons,
            open: (event, ui) => {
                skuid.events.publish(onOpenEvent, [{ event: event, ui: ui }]);
            },
            close: (event, ui) => {
                event.stopImmediatePropagation();
                skuid.events.publish(onCloseEvent, [{ event: event, ui: ui }]);
            }
        });

        $dialog.closest(".ui-dialog")
            .find(".ui-button.slds-button")
            .removeClass("ui-button");

        return isVisible ? $dialog : $dialog.dialog("close");
    }

    export function close($dialog: JQuery): JQuery {
        return $dialog.dialog("close");
    }

    export function closeAll(): JQuery {
        return $(".ui-dialog-content").dialog("close");
    }

    export function open($dialog: JQuery): JQuery {
        return $dialog.dialog("open");
    }

    export function setTitle($dialog: JQuery, title: string): JQuery {
        return $dialog.dialog({ title: title });
    }

}
