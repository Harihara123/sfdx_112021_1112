export const select2DataObjectFrom = (id: string, text: string): IdTextPair => {
    return { id, text };
}

export const from = (jqElem: JQuery, options: Select2Options): JQuery => {
    return jqElem.select2(options);
}

export const noOptions = (jqElem: JQuery) => from(jqElem, {});
export const noSearch = (jqElem: JQuery) => from(jqElem, { minimumResultsForSearch: Infinity });
export const multiple = (jqElem: JQuery) => from(jqElem, { multiple: true });
export const tokenized = (jqElem: JQuery) => from(jqElem, { multiple: true, tags: true, tokenSeparators: [','] });
