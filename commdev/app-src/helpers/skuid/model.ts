import * as array from "../../library/array";
import * as core from "../../library/core";
import * as optional from "../../library/optional";
import * as text from "../../library/text";
import * as ui from "../../library/ui";

// 3rd party
import { curry } from "ramda";


/**
 * Activate multiple conditions on the same skuid.model.Model
 */
export function activateConditions(model: skuid.model.Model, conditions: skuid.model.Condition[]) {
    conditions.map(condition => model.activateCondition(condition));
}

/**
 * Deactivate multiple conditions on the same skuid.model.Model
 */
export function deactivateConditions(model: skuid.model.Model, conditions: skuid.model.Condition[]) {
    conditions.map(condition => model.deactivateCondition(condition));
}

/**
 * Returns an Optional wrapping either a skuid model or an error message.
 */
export function getModelOpt(modelName: string): optional.Optional<skuid.model.Model> {
    return core.nullable.outOf<skuid.model.Model, optional.Optional<skuid.model.Model>>(
        skuid.model.getModel(modelName),
        model => optional.fromSome(model),
        msg => optional.fromNoneOf<skuid.model.Model>(`${msg}. No model found at the key of "${modelName}"`)
    );
}

/**
 * Get specific field value passing model reference and field name.
 */
export function getFieldValueOfFirstRow<r>(model: skuid.model.Model, fieldName: string): r {
    return model.getFirstRow()[fieldName];
}

/**
 * Get the first row of data from the given skuid model
 */
export function getFirstRow<r>(model: skuid.model.Model): r {
    return model.getFirstRow<r>();
}

/**
 * Returns an Optional wrapping either a skuid row or an error message.
 */
export function getRowOpt<r>(model: skuid.model.Model, rowId: string): optional.Optional<r> {
    let row = model.getRowById<r>(rowId);
    if (typeof row !== "boolean") return optional.fromSome(row);
    else return optional.fromNoneOf<r>(`No row could be found with the id "${rowId}"`);
}

/**
 * Returns an array of skuid.Row for the given skuid.model.Model.
 */
export function getRows<r>(model: skuid.model.Model): r[] {
    return model.getRows<r>();
}

/**
 * Returns an Optional wrapping either a skuid model condition or an error message.
 */
export function getConditionOpt(
    model: skuid.model.Model,
    conditionName: string,
    searchSubconditions: boolean
): optional.Optional<skuid.model.Condition> {
    let condition = model.getConditionByName(conditionName, searchSubconditions);
    if (typeof condition !== "boolean") return optional.fromSome(condition);
    else return optional.fromNoneOf<skuid.model.Condition>(`No condition could be found with the name "${conditionName}"`);
}

/**
 * Merges an HTML template with a skuid model and row.
 */
export function merge<a>(
    mergeType: string,
    htmlTemplate: string,
    mergeOptions: { nl2br: boolean, createFields: boolean },
    model: skuid.model.Model,
    row: a
): JQuery {
    return skuid.utils.merge(
        mergeType,
        htmlTemplate.replace(/\r?\n|\r/g, ""),
        mergeOptions,
        model,
        row
    );
}

export interface ModelProperties {
    sObjectName?: string;
    id: string;
    recordsLimit?: number;
    fields: skuid.metadata.Field[];
    conditions: skuid.metadata.Conditions[];
    doQuery?: boolean;
    createRowIfNoneFound?: boolean;
}

export function modelFrom(modelProperties: ModelProperties) {
    let model = new skuid.model.Model();

    model.objectName = modelProperties.sObjectName || text.emptyString;
    model.id = modelProperties.id;
    model.recordsLimit = modelProperties.recordsLimit || 20;
    model.fields = modelProperties.fields || array.of<skuid.metadata.Field>();
    model.conditions = modelProperties.conditions || array.of<skuid.metadata.Conditions>();
    model.doQuery = modelProperties.doQuery || true;
    model.createRowIfNoneFound = modelProperties.createRowIfNoneFound || true;

    model.initialize().register();

    return model;
}

export function modelFromXML(xmlString: string) {
    let model = new skuid.model.Model(skuid.utils.makeXMLDoc(xmlString));
    model.initialize().register();
    return model;
}

export function save<r>(model: skuid.model.Model, options?: skuid.model.SaveOptions): Promise<skuid.Result> {
    return new Promise<skuid.Result>(
        (resolve: (result: skuid.Result) => r, reject: (error: string) => r) => {
            return model.save(options).then(
                (result) => {
                    if (!result.totalsuccess) return reject(array.headOf(result.messages));
                    else return resolve(result);
                },
                (error) => reject(`${error}`)
            );
        }
    );
}

export function saveAll<r>(models: skuid.model.Model[], options?: skuid.model.SaveOptions): Promise<skuid.Result> {
    return new Promise<skuid.Result>(
        (resolve: (result: skuid.Result) => r, reject: (error: string) => r) => {
            return skuid.model.save(models, options).then(
                (result) => {
                    if (!result.totalsuccess) return reject(array.headOf(result.messages));
                    else return resolve(result);
                },
                (error) => reject(`${error}`)
            );
        }
    );
}

export function updateData<r>(model: skuid.model.Model): Promise<skuid.model.Model> {
    return new Promise<skuid.model.Model>(
        (resolve: (result: skuid.model.Model) => r, reject: (error: string) => r) => {
            skuid.model.updateData(array.fromOne(model)).then(
                (result) => resolve(model),
                (error) => reject(`${error}`)
            );
        }
    );
}

export function updateDataOfAll<r>(models: skuid.model.Model[]): Promise<skuid.model.Model[]> {
    return new Promise<skuid.model.Model[]>(
        (resolve: (result: skuid.model.Model[]) => r, reject: (error: string) => r) => {
            skuid.model.updateData(models).then(
                (result) => resolve(models),
                (error) => reject(`${error}`)
            );
        }
    );
}

export const updateRow = <a, b>(model: skuid.model.Model, row: a, updates: b): a => {
    return model.updateRow<a>(row, updates);
};

/**
 * Get the first row of data from the given skuid model and pass it to a callback
 */
export const withFirstRow = curry(<a, b>(withRow: (row: a) => b, model: skuid.model.Model): optional.Optional<b> => {
    return optional.map(optional.of(model.getFirstRow<a>()), withRow);
});

/**
 * Get the first row of data from the given skuid model and pass it to a callback
 */
export const withFirstRowOrElse = curry(
    <a, b>(withRow: (row: a) => b, orElse: () => b, model: skuid.model.Model): b => {
        return optional.withSomeOrElse(optional.of(model.getFirstRow<a>()), withRow, orElse);
    }
);
