import * as array from "../../library/array";
import * as core from "../../library/core";
import * as text from "../../library/text";
import * as ui from "../../library/ui";

/**
 * Removes skuid error messages from the DOM
 */
export function removeErrors(): void {
    ui.removeElements(array.fromOne(".nx-error"));
}

/**
 * Removes inline (field-correlated) skuid error messages from the DOM.
 */
export function removeErrorsInline(): void {
    ui.removeElements(array.fromOne(".nx-error-inline"));
}

/**
 * Passing the editor, custom label key, and severity; display an error
 */
export function displayEditorMessage(editor: skuid.ui.Editor, labelName: string, severity: string) {
    editor.clearMessages();
    editor.handleMessages([{
        message: skuid.label.read(labelName, ""),
        severity: severity
    }]);
}

/**
 * Passing the editor, custom label key, dynamic content, and severity; display an error
 */
export function displayEditorMessageDynamic(editor: skuid.ui.Editor, labelName: string, dynamicContent: string, severity: string) {
    editor.clearMessages();
    editor.handleMessages([{
        message: skuid.label.read(labelName, "") + " " + dynamicContent,
        severity: severity
    }]);
}

/**
 * Returns a skuid editor associated with a jQuery element or throws an error if no editor is associated with the
 * given element
 */
export function editorFromOrFail($element: JQuery): skuid.ui.Editor {
    if ($element.data("object").hasOwnProperty("editor")) {
        return $element.data("object").editor;
    } else {
        core.fail(`No editor found attached to the element: ${$element}`);
    }
}

export function editorOf($element: JQuery, showSaveCancel: boolean): skuid.ui.Editor {
    return new skuid.ui.Editor($element, { showSaveCancel: showSaveCancel });
}


/**
 * Returns a skuid label matching the label name or throws an error if none found
 */
export function labelFromOrFail(labelName: string): string {
    let label = text.trim(skuid.label.read(labelName, text.emptyString));

    if (text.isNotEmpty(label)) {
        return label;
    } else {
        core.fail(`No label found at the key ${labelName}`);
    }
}

/**
 * Toggles between different fieldEditor and change the mode
 */
export function toggleFieldEditorMode(fieldEditorId: string, mode: string) {
    var component: skuid.component.Component;

    $(".nx-basicfieldeditor, .nx-skootable")
        .toArray()
        .map((componentElem: HTMLElement) => {
            component = $(componentElem).data("component");
            if (component.getId() === fieldEditorId) {
                component.element.mode = mode;
                component.element.list.render({
                    doNotCache: true,
                    refreshFields: false,
                    resetPagination: false
                });
            }
        });
}

/**
 * Returns a skuid editor as an argument to a callback or returns an error message to another callback if no editor
 * exists on the given jQuery element
 */
export function withEditorOrElse<r>(
    $element: JQuery,
    haveEditor: (editor: skuid.ui.Editor) => r,
    haveNone: (msg: string) => r
): r {
    return core.nullable.outOf<{ editor?: skuid.ui.Editor }, r>(
        $element.data("object"),
        data => {
            return "editor" in data ? haveEditor(data.editor) : haveNone(`No editor found attached to the selector: ${$element.selector}`);
        },
        none => {
            return haveNone(`No editor found attached to the selector: ${$element.selector}`);
        }
    );
}

export function withLabelOrElse<r>(
    labelName: string,
    haveLabel: (label: string) => r,
    haveNone: (msg: string) => r
): r {
    let label = text.trim(skuid.label.read(labelName, text.emptyString));

    if (text.isNotEmpty(label)) {
        return haveLabel(label);
    } else {
        return haveNone(`No label found at the key ${labelName}`);
    }
}

export module popup {

    export function from(title: string, width: string) {
        return `<popup title="${title}" width="${width}">`;
    }

    export function close() {
        return "</popup>";
    }

}
