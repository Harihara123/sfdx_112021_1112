// Library
import { of as optOf, Optional } from "./optional";

interface ViaTwoArrays<a, b, c> {
    caseOfBoth: (first: a[], second: b[]) => c;
    caseOfFirst: (first: a[]) => c;
    caseOfSecond: (second: b[]) => c;
    caseOfNeither: (msg: string) => c;
}

/**
 * Returns a concatenated array from two arrays of the same type
 */
export function concat<a>(one: a[], another: a[]): a[] {
    return one.concat(another);
}

/**
 * Returns a new array derived from applying the filterBy function on the passed array
 */
export function filter<a>(someArray: a[], filterBy: (someValue: a) => boolean): a[] {
    return someArray.filter(filterBy);
}

/**
 * Flattens an array of N depth.
 */
export function flatten<a>(nestedArray: NArray<a>): any {
    return nestedArray.reduce((flat: a[], toFlatten: a | NArray<a>) => {
        return flat.concat((Array.isArray(toFlatten) ? flatten<a>(toFlatten) : toFlatten));
    }, of<a>());
}

export function forEach<a>(someArray: a[], forEach: (value: a, index: number) => void): void {
    someArray.forEach(forEach);
}

/**
 * Returns an array with the passed value
 */
export function fromOne<a>(one: a): a[] {
    return [one];
}

/**
 * Returns an array with the passed values
 */
export function fromTwo<a>(one: a, another: a): a[] {
    return [one, another];
}

export function fromThree<a>(first: a, second: a, third: a): a[] {
    return [first, second, third];
}

export function fromFour<a>(first: a, second: a, third: a, fourth: a): a[] {
    return [first, second, third, fourth];
}

/**
 * Returns true if the passed array contains any elements, else false
 */
export function hasAny<a>(someArray: a[]): boolean {
    return someArray.length > 0;
}

/**
 * Returns true if the passed array has just one value, else false
 */
export function hasJustOne<a>(someArray: a[]): boolean {
    return someArray.length === 1;
}

/**
 * Returns true if all arrays in the passed array have values, else false
 */
export function haveAny<a>(someArrays: a[][]): boolean {
    return someArrays.every(hasAny);
}

/**
 * Returns the head (first element) of the passed array WITHOUT mutating the given array
 */
export function headOf<a>(someArray: a[]): a {
    return someArray.slice().shift();
}

export const safeHeadOf = <a>(someArray: a[]): Optional<a> => {
    return optOf<a>(someArray.slice().shift());
}

export const join = <a>(glue: string, someArray: a[]): string => {
    return someArray.join(glue);
}

/**
 * Returns the length of the longest value in an array of values.
 */
export function longestOf(someArray: string[]): number {
    return someArray.reduce((maxLength, value) => {
        return value.length > maxLength ? value.length : maxLength;
    }, 0);
}

/**
 * Returns an array of <b> from an array of <a>, via a map transformation function
 */
export function map<a, b>(someArray: a[], map: (someValue: a) => b): b[] {
    return someArray.map(someValue => map(someValue));
}

/**
 * Creates a key/value pair object from the zipped string array
 */
export function objectFrom<a, r>(zippedArray: [string, a][]): r {
    let start: { [key: string]: a } = {};
    return reduce(zippedArray, (accumulator, pair: [string, a]) => {
        accumulator[pair[0]] = pair[1];
        return accumulator;
    }, start);
}

/**
 * Returns an empty array of type <a>
 */
export function of<a>(): a[] {
    return [];
}

export function reduce<a, b>(
    someArray: a[],
    reducer: (prev: a|b, next: a, index: number, array: a[]) => a,
    initialValue?: b
): a|b {
    return initialValue ? someArray.reduce(reducer, initialValue) : someArray.reduce(reducer);
}

/**
 * Returns the array(s) that pass a given test as an argument to a given callback. If neither array passes the test,
 * the final callback is executed and passed an error message that can be handled by the caller.
 */
export function viaTwoArrays<a, b, c>(
    one: a[],
    another: b[],
    test: (someArray: a[] | b[]) => boolean,
    via: ViaTwoArrays<a, b, c>
) {
    if (test(one) && test(another)) return via.caseOfBoth(one, another);
    else if (test(one)) return via.caseOfFirst(one);
    else if (test(another)) return via.caseOfSecond(another);
    else return via.caseOfNeither("Neither array passes the test!");
}

/**
 * If non-empty returns a callback "haveSome" with the passed array, else returns a callback "haveNone" with an error
 * message
 */
export function withSomeOrElse<a, b>(
    someArray: a[],
    haveSome: (someArray: a[]) => b,
    haveNone: (msg: string) => b
): b {
    if (hasAny(someArray)) return haveSome(someArray);
    else return haveNone("The array is empty.");
}

/**
 * Zips two arrays of the same type into a single array, matching the value of the first array and the value of the
 * second array at the same index.
 */
export function zip<a, b>(one: a[], another: b[]): [a, b][] {
    return one.map((value, index) => {
        return <any>[one[index], another[index]];
    });
}

export module curried {

    /**
     * Returns a concatenated array from two arrays of the same type
     */
    export function concat<a>(one: a[]) {
        return function(another: a[]) {
            return one.concat(another);
        };
    }

    /**
     * Returns a new array derived from applying the filterBy function on the passed array
     */
    export function filter<a>(someArray: a[]) {
        return function(filterBy: (someValue: a) => boolean) {
            return someArray.filter(filterBy);
        };
    }

}

type NArray<a> = ((a | a[])[] | a)[];
