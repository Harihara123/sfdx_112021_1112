export type Resolve = <a>(resolveWith: a) => Promise<a>;
export type Reject = <a>(reason: string) => Promise<a>;

export const promise = <a, b>(callback: (resolve: Resolve, reject: Reject) => b): Promise<a> => {
    return new Promise<a>(callback);
}
