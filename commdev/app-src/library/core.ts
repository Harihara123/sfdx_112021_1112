// 3rd party
import { curry } from "ramda";

export let nothing: void;

/**
 * Returns a property of a passed object or throws an error if that property does not exist.
 */
export module accessors {

    export function idOf<a>(someObject: a): string {
        if (someObject.hasOwnProperty("id")) return someObject["id"];
        else raise(`The "id" property does not exist on ${someObject}`);
    }

    export function valueOf<a>(someObject: a): string {
        if (someObject.hasOwnProperty("value")) return someObject["value"];
        else raise(`The "value" property does not exist on ${someObject}`);
    }

}

export const booleanFrom = curry((asTrue: string, value: string): boolean => {
    return value === asTrue ? true : false;
});

export const outOfBoolean = curry((asTrue: string, asFalse: string, value: boolean): string => {
    return value === true ? asTrue : asFalse;
});

/**
 * Returns the passed value.
 */
export function bounce<a>(value: a): a { return value; }

/**
 * Throw an error with the passed message
 */
export function fail<r>(reason: string): r {
    console.error(reason);
    return raise<r>(reason);
}

export const hasKey = <a>(key: string, someObject: a): boolean => {
    return key in someObject;
};

export function keyAt<a, r>(key: string, someObject: a): r {
    return key in someObject ? someObject[key] : fail(`The key ${key} was not found on ${someObject}`);
}

/**
 * NOTE: For internal use only
 *
 * Used by fail() to allow for additional steps to happen before throwing
 */
function raise<r>(reason: string): r {
    throw new Error(reason);
}

/**
* For code stubs that have no continuation.
*/
export function ignore(): void {}


export function schedule<a, r>(toBeRun: (params: any) => r, params: a[]): r {
    setTimeout(() => toBeRun.apply(null, params));
    return <r>{};
}

export function scheduleError<r>(error: string): r {
    return schedule<string, r>(fail, [error]);
}

export const withKeyAt = curry(<a, b, r>(
    key: string,
    haveSome: (some: b) => r,
    haveNone: (reason: string) => r,
    someObject: a
): r => {
    return key in someObject ? haveSome(someObject[key]) : haveNone(`The key ${key} was not found on ${someObject}`);
});

export module use {

    /**
     * Takes a value and callback, passing the value to the callback and returning the result.
     */
    export function one<a, r>(value: a, use: (value: a) => r): r {
        return use(value);
    }

    export function two<a, b, r>(one: a, another: b, use: (one: a, another: b) => r): r {
        return use(one, another);
    }

    /**
    * Takes two callbacks, passing the first into the second, and returning the result.
    */
    export function withNone<a, b>(toUse: () => a, useIn: (value: a) => b): b {
        return useIn(toUse());
    }

    export function withOne<a, b, c>(param: a, toUse: (param: a) => b, useIn: (value: b)=> c): c {
        return useIn(toUse(param));
    }

    export function withTwo<a, b, c, d>(
        paramOne: a,
        paramTwo: b,
        toUse: (one: a, two: b) => c,
        useIn: (value: c)=> d
    ): d {
        return useIn(toUse(paramOne, paramTwo));
    }
}


/**
 * Deals with nullable values to ensure proper handling
 */
export module nullable {

    /**
     * Returns the value if non-null or throws an error.
     */
    export function asSomeOrFail<a>(value: a, failure: string): a {
        if ((value !== void 0) && (value !== null)) return value;
        else return fail<a>(failure);
    }

    /**
     * Returns the value if non-null or another passed value of the same type.
     */
    export function asSomeOrElse<a>(value: a, orElse: a): a {
        if ((value !== void 0) && (value !== null)) return value;
        else return orElse;
    }

    /**
     * Returns the value as a parameter to the first callback if non-null, or an error message to the second callback.
     */
    export function outOf<a, r>(value: any, haveSome: (some: a) => r, haveNone: (msg: string) => r): r {
        if ((value !== void 0) && (value !== null)) return haveSome(value);
        else return haveNone("The value is undefined!");
    }

    interface ViaTwo<a, b, r> {
        caseOfBoth: (one: a, another: b) => r;
        caseOfOne: (one: a) => r;
        caseOfAnother: (another: b) => r;
        caseOfNeither: (msg: string) => r;
    }

    export function viaTwo<a, b, r>(oneValue: a, anotherValue: b, via: ViaTwo<a, b, r>): r {
        if ((oneValue !== void 0) && (oneValue !== null)) {
            if ((anotherValue !== void 0) && (anotherValue !== null)) {
                return via.caseOfBoth(oneValue, anotherValue);
            } else {
                return via.caseOfOne(oneValue);
            }
        } else if ((anotherValue !== void 0) && (anotherValue !== null)) {
            return via.caseOfAnother(anotherValue);
        } else {
            return via.caseOfNeither("Both values are undefined!");
        }
    }

    interface ViaThree<a, b, c, r> {
        caseOfAll: (first: a, second: b, third: c) => r;
        caseOfFirstAndSecond: (first: a, second: b) => r;
        caseOfFirstAndThird: (first: a, third: c) => r;
        caseOfSecondAndThird: (second: b, third: c) => r;
        caseOfFirst: (first: a) => r;
        caseOfSecond: (second: b) => r;
        caseOfThird: (third: c) => r;
        caseOfNone: (msg: string) => r;
    }

    export function viaThree<a, b, c, r>(first: a, second: b, third: c, via: ViaThree<a, b, c, r>): r {
        if ((first !== void 0) && (first !== null)) {
            if ((second !== void 0) && (second !== null)) {
                if ((third !== void 0) && (third !== null)) {
                    return via.caseOfAll(first, second, third);
                } else {
                    return via.caseOfFirstAndSecond(first, second);
                }
            } else if ((third !== void 0) && (third !== null)) {
                return via.caseOfFirstAndThird(first, third);
            } else {
                return via.caseOfFirst(first);
            }
        } else if ((second !== void 0) && (second !== null)) {
            if ((third !== void 0) && (third !== null)) {
                return via.caseOfSecondAndThird(second, third);
            } else {
                return via.caseOfSecond(second);
            }
        } else if ((third !== void 0) && (third !== null)) {
            return via.caseOfThird(third);
        } else {
            return via.caseOfNone("All values are undefined!");
        }
    }

}
