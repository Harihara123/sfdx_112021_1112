// Returns the current year
export const currentYear = () => new Date().getFullYear();

// Returns the current point in time
export const now = () => new Date();

// Returns a JS Date from a well-formed string
export const from = (dateString: string) => new Date(dateString);

// Static value that is always the current point in time
export const alwaysNow = now();
