/**
 * Casts an Element to a HTMLSelectElement
 */
export const selectElementFrom = function(element: Element): HTMLSelectElement {
    return <HTMLSelectElement>element;
}
