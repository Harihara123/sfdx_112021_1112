/**
 * Throw an error with the passed message
 */
export function raise<r>(reason: string): r {
    throw new Error(reason);
}
