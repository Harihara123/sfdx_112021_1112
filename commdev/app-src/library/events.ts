// Library
import { fail } from "./core";

/**
 * Enum of events
 */
export enum Event {
    change,
    click
}

/**
 * INTERNAL USE ONLY
 *
 * Converts a given event to its string equivalent
 */
const outOfEvent = (event: Event): string => {
    switch(event) {
        case Event.change: return "change";
        case Event.click: return "click";
        default: return fail<string>(`The event ${event} does not exist!`);
    }
}

/**
 * Watch a given jqElement for a given event
 */
export const watch = <r>($jqElement: JQuery, event: Event, onEvent: ($jqElement: JQuery) => r): void => {
    $jqElement.on(outOfEvent(event), () => { onEvent($jqElement) });
};
