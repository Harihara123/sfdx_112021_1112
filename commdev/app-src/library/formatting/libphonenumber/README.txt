*************** 03/04/2016 - Mark Tucker **************

Use of the Google i18n library is based on the Github project found here:
https://github.com/googlei18n/libphonenumber/tree/master/javascript

To modify/compile .js file used in ATS (i18nPhoneFormatter.js):
- Follow setup info found in the URL above, to setup your local environment.
- Modify i18nPhoneFormatter_raw.js (in this directory) as needed.
- drop build.xml and i18nPhoneFormatter_raw.js in <path to downloaded library>/libphonenumber/javascript/i18n
- Follow the "How to Compile" instructions from the URL above, using the custom compile-ats task.
- Copy compiled i18nPhoneFormatter.js back to this location, and deploy to app.
