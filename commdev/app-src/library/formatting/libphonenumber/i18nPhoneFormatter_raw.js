/**
 * @license
 * Copyright (C) 2010 The Libphonenumber Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview  Phone Number Parser Demo.
 *
 * @author Nikolaos Trogkanis
 */

goog.require('goog.dom');
goog.require('goog.json');
goog.require('goog.proto2.ObjectSerializer');
goog.require('goog.string.StringBuffer');
goog.require('i18n.phonenumbers.AsYouTypeFormatter');
goog.require('i18n.phonenumbers.PhoneNumberFormat');
goog.require('i18n.phonenumbers.PhoneNumberType');
goog.require('i18n.phonenumbers.PhoneNumberUtil');
goog.require('i18n.phonenumbers.PhoneNumberUtil.ValidationResult');

function formatPhoneNumber(phoneNumber, regionCode) {
  var output = new goog.string.StringBuffer();
  try {
    var phoneUtil = i18n.phonenumbers.PhoneNumberUtil.getInstance();
    var number = phoneUtil.parseAndKeepRawInput(phoneNumber, regionCode);
    var isNumberValid = phoneUtil.isValidNumber(number);
    var PNF = i18n.phonenumbers.PhoneNumberFormat;
    output.append(isNumberValid ?
                  phoneUtil.format(number, PNF.INTERNATIONAL) :
                  number.getRawInput());
  } catch (e) {
    console.log('\n' + e);
  }
  return output.toString();
}

function isValidPhoneNumber(phoneNumber, regionCode) {
  var output = new goog.string.StringBuffer();
  var isNumberValid = false;
  try {
    var phoneUtil = i18n.phonenumbers.PhoneNumberUtil.getInstance();
    var number = phoneUtil.parseAndKeepRawInput(phoneNumber, regionCode);
    //console.log(number);
    isNumberValid = phoneUtil.isValidNumber(number);
  } catch (e) {
    output.append('\n' + e);
  }
  return isNumberValid;
}

goog.exportSymbol('formatPhoneNumber', formatPhoneNumber);
goog.exportSymbol('isValidPhoneNumber', isValidPhoneNumber);
