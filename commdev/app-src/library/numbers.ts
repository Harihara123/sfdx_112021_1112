// Library
import * as core from "./core";

export function integerFromOrFail(someValue: string, fail: string): number {
    let parsed = parseInt(someValue, 10);
    if (!isNaN(parsed)) return parsed;
    else return core.fail<number>(`${fail} The value could not be parsed as an integer!`);
}
