// 3rd party
import { curry } from "ramda";

// Library
import * as array from "./array";
import * as core from "./core";

export type Optional<a> = Some<a> | None;

export interface None { none: string; }

export interface Some<a> { some: a; }

function noneFrom(reason: string): None {
    return {
        none: reason
    };
}

function someFrom<a>(value: a): Some<a> {
    return {
        some: value
    };
}

export function of<a>(value: a): Optional<a> {
    return (value !== undefined && value !== null)
        ? someFrom(value)
        : noneFrom("The value is either undefined or null");
}

export function fromSome<a>(value: a): Optional<a> {
    return someFrom(value);
}

export function fromNoneOf<a>(reason: string): Optional<a> {
    return noneFrom(reason);
}

export function isSome<a>(value: Optional<a>): value is Some<a> {
    return "some" in value;
}

export function allAreSome<a>(values: Optional<a>[]): values is Some<a>[] {
    return values.every(isSome);
}

export function map<a, b>(
    value: Optional<a>,
    mapping: (value: a) => b
): Optional<b> {
    return isSome(value) ? fromSome(mapping(value.some)) : value;
}

export const map2 = curry(<a, b, c>(
    mapping: (one: a, another: b) => c,
    oneOpt: Optional<a>,
    anotherOpt: Optional<b>
): Optional<c> => {
    return (isSome(oneOpt) && isSome(anotherOpt))
        ? fromSome(mapping(oneOpt.some, anotherOpt.some))
        : fromNoneOf<c>("At least one of the Optionals were None");
});

export const map3 = curry(<a, b, c, d>(
    mapping: (first: a, second: b, third: c) => d,
    firstOpt: Optional<a>,
    secondOpt: Optional<b>,
    thirdOpt: Optional<c>
): Optional<d> => {
    return (isSome(firstOpt) && isSome(secondOpt) && isSome(thirdOpt))
        ? fromSome(mapping(firstOpt.some, secondOpt.some, thirdOpt.some))
        : fromNoneOf<d>("At least one of the Optionals were None")
});

export function bind<a, b>(
    value: Optional<a>,
    binding: (value: a) => Optional<b>
): Optional<b> {
    return isSome(value) ? binding(value.some) : value;
}

export function isNone<a>(value: Optional<a>): value is None {
    return 'none' in value;
}

export function asSomeOrFail<a>(value: Optional<a>, failure: string): a {
    return isSome(value) ? value.some : core.fail<a>(`${failure} ${value.none}`);
}

export function asSomeOrThat<a>(value: Optional<a>, that: a): a {
    return isSome(value) ? value.some : that;
}

export function asSomeOrElse<a>(value: Optional<a>, toElsewhat: () => a): a {
    return isSome(value) ? value.some : toElsewhat();
}

export function asNoneOrFail<a>(value: Optional<a>, failure: string): string {
    return isSome(value) ? core.fail<string>(`${failure} No value is expected, but there is a value.`) : value.none;
}

export function withSomeOrFail<a, r>(value: Optional<a>, haveSome: (value: a) => r, failure: string): r {
    return isSome(value) ? haveSome(value.some) : core.fail<r>(`${failure} ${value.none}`);
}

export function withSomeOrThat<a, r>(value: Optional<a>, haveSome: (value: a) => r, that: r): r {
    return isSome(value) ? haveSome(value.some) : that;
}

export function withSomeOrElse<a, r>(
    value: Optional<a>,
    haveSome: (value: a) => r,
    haveNone: (value: string) => r
): r {
    return isSome(value) ? haveSome(value.some) : haveNone(value.none);
}

export function withSomeOfBothOrFail<a, r>(
    one: Optional<a>,
    another: Optional<a>,
    haveSomeOfBoth: (one: a, another: a) => r,
    failure: string
): r {
    if (isSome(one)) {
        if (isSome(another)) {
            return haveSomeOfBoth(one.some, another.some);
        } else {
            return core.fail<r>(`${failure} ${another.none}`);
        }
    } else {
        return core.fail<r>(`${failure} ${one.none}`);
    }
}

export function withSomeOfEitherOrElse<a, r>(
    one: Optional<a>,
    another: Optional<a>,
    haveSomeOfOne: (one: a) => r,
    haveSomeOfAnother: (another: a) => r,
    haveNeither: (reason: string) => r
) {
    return isSome(one)
        ? haveSomeOfOne(one.some)
        : isSome(another)
            ? haveSomeOfAnother(another.some)
            : haveNeither(`${one.none} ${another.none}`);
}

export function withSomeOfAllOrFail<a, r>(
    someOptionals: Optional<a>[],
    haveSomeOfAll: (values: a[]) => r,
    failure: string
): r {
    if (allAreSome(someOptionals)) {
        return haveSomeOfAll(array.map(someOptionals, someOptional => someOptional.some))
    } else {
        return core.fail<r>(`${failure}`);
    }
}

export function transformNone<a>(value: Optional<a>, transform: (reason: string) => string): Optional<a> {
    return isSome(value) ? value : noneFrom(transform(value.none));
}

export module one {
    export function withSomeOrFail<a, One, r>(value: Optional<a>, one: One, haveSome: (value: a, one: One) => r, failure: string): r {
        return isSome(value) ? haveSome(value.some, one) : core.fail<r>(failure + ' ' + value.none);
    }

    export function withSomeOrThat<a, One, r>(value: Optional<a>, one: One, haveSome: (value: a, one: One) => r, that: r): r {
        return isSome(value) ? haveSome(value.some, one) : that;
    }

    export function withSomeOrElse<a, One, r>(value: Optional<a>, one: One, haveSome: (value: a, one: One) => r, toElsewhat: (one: One) => r): r {
        return isSome(value) ? haveSome(value.some, one) : toElsewhat(one);
    }
}

export module two {
    export function withSomeOrFail<a, One, Two, r>(value: Optional<a>, one: One, two: Two, haveSome: (value: a, one: One, two: Two) => r, failure: string): r {
        return isSome(value) ? haveSome(value.some, one, two) : core.fail<r>(failure + ' ' + value.none);
    }

    export function withSomeOrThat<a, One, Two, r>(value: Optional<a>, one: One, two: Two, haveSome: (value: a, one: One, two: Two) => r, that: r): r {
        return isSome(value) ? haveSome(value.some, one, two) : that;
    }

    export function withSomeOrElse<a, One, Two, r>(value: Optional<a>, one: One, two: Two, haveSome: (value: a, one: One, two: Two) => r, toElsewhat: (one: One, two: Two) => r): r {
        return isSome(value) ? haveSome(value.some, one, two) : toElsewhat(one, two);
    }
}
