import * as la from "./array";
import * as le from "./error";
import * as lt from "./text";
import * as chalk from "chalk";
import { Spec } from "../specs/specs";

export interface Test {
    name: string;
    run: () => boolean;
}

export function expect<a>(callback: () => a | a[]) {
    return {

        toBe: (value: a): boolean => {
            return callback() === value;
        },

        toBeLike: (value: a): boolean => {
            return callback() == value;
        },

        toBeEmpty: (): boolean => {
            let results = callback();
            if (Array.isArray(results)) return results.length === 0;
            else return false;
        },

        toContain: (value: a): boolean => {
            let results = callback();
            if (Array.isArray(results)) return results.some(result => result === value);
            else return false;
        },

        toContainAllOf: (values: a[]): boolean => {
            let results = callback();
            if (Array.isArray(results)) return results.every(result => values.indexOf(result) > -1);
            else return false;
        }

    };
}

function prettyPrint(value: boolean): Chalk.ChalkChain {
    if (value) return chalk.bgGreen("PASS");
    else return chalk.bgRed("FAIL");
}

export function testAll(tests: Test[]): boolean {
    return tests.every(test => test.run());
}

function logTest(test: Test, longestLength: number): string {
    return `${test.name} ${lt.charactersByCount(" ", longestLength - test.name.length)}${prettyPrint(test.run())}`;
}

export function logSpecs(specs: Spec[]): string {
    let longestLength = la.longestOf(specs
        .map(spec => spec.tests.map(test => test.name))
        .reduce((one, another) => one.concat(another)
    ));

    return specs.reduce((formatted: string[], spec: Spec, index: number, specArray: Spec[]) => {
        return formatted.concat("", `${spec.section}`, `${lt.charactersByCount("=", spec.section.length)}`, spec.tests.map(test => logTest(test, longestLength)).join("\n"));
    }, la.of<string>()).join("\n");
}
