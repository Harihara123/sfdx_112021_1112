import { curry } from "ramda";

export let emptyString = "";

export const alwaysEmptyString = (): string => emptyString;

export const capitalizeFirst = (value: string): string => {
    return `${value.charAt(0).toUpperCase()}${value.substr(1)}`;
};

export const charactersByCount = (char: string, count: number): string => {
    return Array.prototype.slice.call(Array(count + 1)).map((x, i) => i).join(char);
}

export const cleanse = (value: string): string => {
    return value !== undefined ? value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-') : emptyString;
}

export const isNotEmpty = (value: string): boolean => {
    return value !== emptyString;
}

export const trim = (value: string): string => {
    return value.trim();
}

export const truncate = (value: string, start: number, end: number): string => {
    return value.substring(start, end);
}

// Takes a negative index and a string, returning index # of chars from the end of the string
export const right = curry((index: number, value: string) => value.substr(index));

export const withSomeOrElse = <r>(
    value: string,
    haveSome: (value: string) => r,
    haveNone: (reason: string) => r
): r => {
    return isNotEmpty(value) ? haveSome(value) : haveNone(`The value ${value} is empty.`);
}
