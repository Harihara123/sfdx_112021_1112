// Library
import { fromNoneOf, of as optOf } from "./optional";
import * as lt from "./text";

interface SelectedInputElements {
    selectedElements: HTMLInputElement[],
    areAllSelected: boolean
}

export enum SelectorType { Class, Id }

interface ViaSomeSelectorType<a> {
    caseOFClass: () => a;
    caseOfId: () => a;
    caseOfNeither: (msg: string) => a;
}

const viaSomeSelectorType = <a>(selectorType: SelectorType, via: ViaSomeSelectorType<a>) => {
    switch(selectorType) {
        case SelectorType.Class: return via.caseOFClass();
        case SelectorType.Id: return via.caseOfId();
        default: return via.caseOfNeither(`Unexpected case of selectorType: ${selectorType}`);
    }
};

export const getElementSafe = (selectorType: SelectorType, selector: string) => {
    return viaSomeSelectorType(selectorType, {
        caseOFClass: () => optOf(<HTMLElement>document.querySelector(selector)),
        caseOfId: () => optOf(document.getElementById(selector)),
        caseOfNeither: () => fromNoneOf<HTMLElement>(`No element matching "${selector}" found in the DOM.`)
    });
};

/**
 * Return an array of an element's children
 */
export function getChildElementsOf(
    $jqElement: JQuery,
    elementType: string
): HTMLElement[] {
    return $jqElement.find(elementType).toArray();
}

// TODO: Ask Farid if this is something he wrote
export function getSelectedCheckboxValues(fieldType: string): SelectedInputElements {
    let checkBoxesName = `filtercheckBoxName_${fieldType}`;
    let $checkBoxElements = $(`input[name="${checkBoxesName}"]`);
    let $selectedCheckboxElements = $(`input[name="${checkBoxesName}"]:checked`);

    return {
        selectedElements: $selectedCheckboxElements.toArray(),
        areAllSelected: $checkBoxElements.length === $selectedCheckboxElements.length
    };
}

/**
 * Hides element(s) from the ui
 */
export function hideElements(selectors: string[]) {
    selectors.map(selector => {
        $(selector).hide();
    });
}

/**
 * Removes element(s) from the ui; also cleans up event listeners
 */
export function removeElements(selectors: string[]): void {
    selectors.map(selector => {
        $(selector).remove();
    });
}

/**
 * Append a new child jqElement to a parent jqElement, returning the parent
 */
export const render = ($parent: JQuery, $newChild: JQuery): JQuery => {
    return $parent.append($newChild);
};

/**
 * Replace an element with its text
 */
export function replaceElementWithText($jqElement: JQuery) {
    $jqElement.after($("<span />").text($jqElement.text()));
    $jqElement.remove();
}

/**
 * Set the page title using a custom Salesforce label
 */
export function setPageTitle(title: string) {
    document.title = title;
}

/**
 * Shows element(s) from the ui
 */
export function showElements(selectors: string[]) {
    selectors.map(selector => {
        $(selector).show();
    });
}
