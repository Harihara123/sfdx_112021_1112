import * as array from "./array";
import * as core from "./core";
import * as optional from "./optional";

interface QueryParam {
    key: string;
    value: string;
}

export function addQueryParam(queryParam: QueryParam): void {
    window.location.search += `&${queryParam.key}=${queryParam.value}`;
}

export function getQueryParamAtOrFail(key: string): QueryParam {
    let filtered = array.filter(getQueryParams(), param => param.key === key);

    if (array.hasJustOne(filtered)) return array.headOf(filtered);
    else core.fail(`The key ${key} is not present in the query params.`);
}

export function getQueryParamAtOpt(key: string): optional.Optional<QueryParam> {
    let filtered = array.filter(getQueryParams(), param => param.key === key);

    if (array.hasJustOne(filtered)) return optional.fromSome(array.headOf(filtered));
    else return optional.fromNoneOf<QueryParam>(`The key ${key} is not present in the query params.`);
}

export function getQueryParams(): QueryParam[] {
    return window.location.search
        .substr(1)
        .split("&")
        .map(param => {
            let split = param.split("=");
            return {
                key: split[0],
                value: split[1]
            };
        });
}

export function haveQueryParam(queryParams: QueryParam[], key: string): boolean {
    return array.hasJustOne(array.filter(queryParams, param => param.key === key));
}
