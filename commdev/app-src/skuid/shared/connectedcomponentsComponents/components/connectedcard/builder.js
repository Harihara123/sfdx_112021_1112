(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__connectedcard",
		name: "Connected Card",
		icon: "sk-icon-contact",
		description: "This builds wrapper for card view",
		componentRenderer: function (component) {
			//console.log(arguments);

	        var r = skuid.builder.getBuilders().wrapper.componentRenderer;
	        //console.log(r);
	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },	
		propertiesRenderer: function (propertiesObj, component) {
	        
	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('Card Properties');


	        var propsList = [
				{
				    id: "mobileColSize",
				    type: "picklist",
				    label: "Column size for mobile",
				    picklistEntries: [
                        { value: 'slds-size--1-of-4', label: '3 column' },
                        { value: 'slds-size--1-of-3', label: '4 column' },
                        { value: 'slds-size--1-of-2', label: '6 column' },
                        { value: 'slds-size--1-of-1', label: '12 column' }
				    ],
				    defaultValue: 'slds-size--1-of-1',
				    helptext: "Select a column size for mobile device",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "tabletColSize",
				    type: "picklist",
				    label: "Column size for Tablet",
				    picklistEntries: [
                        { value: 'slds-medium-size--1-of-4', label: '3 column' },
                        { value: 'slds-medium-size--1-of-3', label: '4 column' },
                        { value: 'slds-medium-size--1-of-2', label: '6 column' },
                        { value: 'slds-medium-size--1-of-1', label: '12 column' }
				    ],
				    defaultValue: 'slds-medium-size--1-of-1',
				    helptext: "Select a column size for tablet device",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "desktopColSize",
				    type: "picklist",
				    label: "Column size for Desktop",
				    picklistEntries: [
                        { value: 'slds-large-size--1-of-4', label: '3 column' },
                        { value: 'slds-large-size--1-of-3', label: '4 column' },
                        { value: 'slds-large-size--1-of-2', label: '6 column' },
                        { value: 'slds-large-size--1-of-1', label: '12 column' }
				    ],
				     defaultValue: 'slds-large-size--1-of-1',
				    helptext: "Select a column size for desktop device",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "modelForCard",
				    type: "model",
				    label: "Select the model to be displayed in card",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "nubmerOfCards",
				    type: "string",
				    label: "Default Number of Cards",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "enableLoadMore",
				    type: "boolean",
				    label: "Enable Load More",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "header",
				    type: "template",
					modelprop : "modelForCard",
				    label: "Header Template",
					location: "attribute",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "body",
				    type: "template",
					modelprop : "modelForCard",
				    label: "Body Template",
					location: "attribute",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "footer",
				    type: "template",
					modelprop : "modelForCard",
				    label: "Footer Template",
					location: "attribute",
				    onChange: function () {
				        component.refresh();
				    }
				}				
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propsList,
	        });
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);

	        
	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__connectedcard"));
	    }
		
	}));

		
})(skuid);