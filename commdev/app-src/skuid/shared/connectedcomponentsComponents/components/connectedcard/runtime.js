(function(skuid){
	var $ = skuid.$;
	skuid.componentType.register("connectedcomponents__connectedcard",function(domElement, xmlConfig, component){

		var mobileClass = xmlConfig.attr('mobileColSize');
		var tabletClass = xmlConfig.attr('tabletColSize');
		var desktopClass = xmlConfig.attr('desktopColSize');
		var modelName = xmlConfig.attr('modelForCard');
		var nubmerOfCards = xmlConfig.attr('nubmerOfCards');
		
		
		var model = skuid.model.getModel(modelName);

		var cardResponsiveDivClass = 'slds-align-middle slds-col--padded ' + mobileClass + ' ' + tabletClass + ' ' +  desktopClass;

		var cardOuterDiv = $('<div class="slds-card" />');

        domElement.empty();	
		domElement.addClass('slds-grid slds-wrap');
		
		var loopCount = nubmerOfCards;
		var hasMoreRecords = true;
		if (nubmerOfCards > model.data.length) {
			loopCount = model.data.length;
			hasMoreRecords = false;
		}
		var divisionId = xmlConfig.attr('uniqueid');
		//var modelRows = model.getRows();
		for(var i=0; i<loopCount; i ++) {
			
			//var cardTest = $('<div class="slds-card" />').html(skuid.utils.merge( 'row', header, null, model, modelRows[i]));
			var cardSized = buildCard(xmlConfig, model, i, cardResponsiveDivClass);
			cardSized.appendTo(domElement);
		}
		
		/**if (hasMoreRecords) {
			domElement.append($('<div/>').addClass('slds-col--padded slds-size--1-of-4 slds-medium-size--1-of-1 slds-large-size--1-of-1')
				.html('<a href="#">Load More..</a>'));
		}*/
		if (hasMoreRecords) {
			//domElement.append($("<div id=\"\infiniteData-" + divisionId + "\" />"));
			domElement.append($('<div/>').addClass('slds-align-middle slds-col--padded slds-size--1-of-4 slds-medium-size--1-of-1 slds-large-size--1-of-1')
				.append($("<span id=\"moreRecordsLink-" + divisionId + "\" href=\"#\">Load More Data..</span>"))
				.click(function(){
					var currentDataSize = model.data.length;
					model.loadNextOffsetPage(function() {
						if (!model.canRetrieveMoreRows) {
							$("#moreRecordsLink-" + divisionId).remove();
						}						
						for (var i = currentDataSize; i < model.data.length; i++) {
							buildCard(xmlConfig, model, i, cardResponsiveDivClass).insertBefore($("#moreRecordsLink-" + divisionId).parent());
						}									
					});
				})
			);
		}
                
	});
})(skuid);


function buildCard(xmlConfig, model, i, cardResponsiveDivClass) {
	var $ = skuid.$;
	var modelRows = model.getRows();
	var cardTest = $('<div class="slds-card" />').append(buildHeader(xmlConfig.attr('header'), model, modelRows[i]))
								.append(buildBody(xmlConfig.attr('body'), model, modelRows[i]))
								.append(buildFooter(xmlConfig.attr('footer'), model, modelRows[i]));
	var cardSized = $('<div/>').addClass(cardResponsiveDivClass).append(cardTest);
	//var cardSpacer = $('<div class="' + cardResponsiveDivClass + '"/>');
	//cardSized.insertBefore(cardSpacer);
	return cardSized;
}

function buildHeader(headerTemplate, model, row) {
	var $ = skuid.$;
	var cardHeader = $('<header/>').addClass('slds-card__header slds-grid');
	var hLevel2 = $('<div/>').addClass('slds-media slds-media--center slds-has-flexi-truncate');
	var hLevel3 = $('<div/>').addClass('slds-media__body');
	var hLevel4 = $('<h3/>').addClass('slds-text-heading--small slds-truncate');
	hLevel4.html(skuid.utils.merge( 'row', headerTemplate, null, model, row));
	return cardHeader.append(hLevel2.append(hLevel3.append(hLevel4)));
}

function buildFooter(footerTemplate, model, row) {
	var $ = skuid.$;
	var cardFooter = $('<footer/>').addClass('slds-card__footer');
	return cardFooter.html(skuid.utils.merge( 'row', footerTemplate, null, model, row)); 
}

function buildBody(bodyTemplate, model, row) {
	var $ = skuid.$;
	var cardBody = $('<section/>').addClass('slds-card__body');
	return cardBody.html(skuid.utils.merge( 'row', bodyTemplate, null, model, row)); 
}