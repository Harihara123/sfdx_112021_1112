(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__connectedgrid",
		name: "Connected Grid",
		icon: "sk-icon-contact",
		description: "This component acts as container for material look and feel type grid",
		componentRenderer: function (component) {
			var r = skuid.builder.getBuilders().wrapper.componentRenderer;
	        //console.log(r);
	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },	
		propertiesRenderer: function (propertiesObj, component) {
			console.log('Grid : ' + propertiesObj);
	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('MDL Grid Properties');
	        //console.log(propCategories);
	        propCategories.splice(0,1);
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);
	        

	        
	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__connectedgrid"));
	    }
		
	}));

		
})(skuid);