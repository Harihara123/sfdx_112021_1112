(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__contextmenu",
		name: "Context Menu",
		icon: "sk-icon-contact",
		description: "This component build context menu",
		propertiesRenderer: function (propertiesObj, component) {
			console.log('Menu : ' + propertiesObj);
			var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var state = component.state;
			var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('Menu Properties');
	        var propertyList = [				
				{
				    id: "menucontextname",
				    type: "string",
				    label: "Context Name",
					defaultValue: 'test123',
				    helptext: "Provide a context name {{{$User.name}}}",
				    onChange: function () {
				        component.refresh();
				    }
				}				
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propertyList,
	        });	
			//console.log(state.attr('menucontextname'));
	        propertiesObj.applyPropsWithCategories(propCategories, state);
	    },
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>Build your context menu!</div>"
			);
		},
		defaultStateGenerator : function() {
			console.log(propertiesObj);
			return skuid.utils.makeXMLDoc("<connectedcomponents__contextmenu />");
		}
	
	    
	}));

		
})(skuid);