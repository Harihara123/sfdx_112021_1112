(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__filter",
		name: "Filter",
		icon: "sk-icon-page-edit",
		description: "The Filter Componets",
		componentRenderer: function (component) {
			

	        var r = skuid.builder.getBuilders().wrapper.componentRenderer;
	        
	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },	
		propertiesRenderer: function (propertiesObj, component) {
	        
	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('Filter Properties');


	       var propsList = [
				{
				    id: "filterModelId",
				    type: "model",
				    label: "Filter Model",
					required : true,
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "filterTypeId",
				    type: "string",
				    label: "First Filter in the List",
				    onChange: function () {
				        component.refresh();
				    }
				}
				
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propsList,
	        });
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);

	        
	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__filter"));
	    }
		
	}));

		
})(skuid);