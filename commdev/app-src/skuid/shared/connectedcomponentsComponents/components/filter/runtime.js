var $ = skuid.$;

    (function(skuid){

    	skuid.componentType.register("connectedcomponents__filter",function(domElement, xmlConfig, component){

    	var filterModelNameId = xmlConfig.attr('filterModelId');
        var filterType = xmlConfig.attr('filterTypeId');


        filterType = skuid.utils.mergeAsText("global","{{$Label.".concat(filterType.concat("}}")))

        //render open activity wait for it to finish rendering and then render past activity
        renderFilterComponent(filterModelNameId, filterType, domElement, true);

         $( document ).ready(function() {

              var checkBoxIdHeader = "#".concat(filterType).concat("CheckboxId");
              var checkBoxIdName = "input[name='filtercheckBoxName_".concat(filterType).concat("']");
              var checkBoxId     =   "#".concat('filtercheckBoxId').concat(filterType);
              var checkBoxesName = "filtercheckBoxName_".concat(filterType);

              //check/uncheck All checkBoxes
             $(checkBoxIdHeader).on('change', function() {
                 $(checkBoxIdName).prop('checked', $(this).prop("checked"));
             });


              //check if one of selected checkboxes are selected unselect the header checkbox
              $(".checkBoxClass").click(function(){
                 if ( $("input[name='".concat(checkBoxesName).concat("']")).length ==  $("input[name='".concat(checkBoxesName).concat("']:checked")).length) {
                      $("input[name='" +  filterType.concat("CheckBoxName") + "']").prop('checked', true);
                 } else {
                      $("input[name='" +  filterType.concat("CheckBoxName") + "']").prop('checked', false);
                 }
             });

            //reset filter - check all selected checkboxex
            var resetType = 'resetFilter'.concat(filterType);
            skuid.events.subscribe(resetType,function(results){
                 $("input[name='" +  filterType.concat("CheckBoxName") + "']").prop('checked', true);
                 $("input[name='" +  "filtercheckBoxName_".concat(filterType) + "']").prop('checked', true);
            });
        });

    	});

    })(skuid);



    function renderFilterComponent(filterModelNameId, filterType, domElement, initalLoad) {

       var model = skuid.model.getModel(filterModelNameId);
       var varList = skuid.model.getModel(filterModelNameId).fields[0].picklistEntries;

       var divContainer = $('<div/>');

       var divType = $('<div/>').addClass("filterHeaderClass");

       var checkboxHeaderLabel = skuid.utils.mergeAsText("global","{{$Label.ATS_SELECT_ALL}}").concat(" ").concat(filterType);

       divType.append(getCheckBoxDiv(checkboxHeaderLabel, filterType, initalLoad, filterType.concat("CheckBoxName"), filterType.concat("CheckboxId"), 'checkBoxHeaderClass'));

       divContainer.append(divType);

       var parentDiv = $('<div/>').addClass('slds-grid slds-wrap');

       for (i = 0; i < varList.length; i++) {

             var col1 = $("<div/>").addClass('slds-col slds-size--1-of-2 slds-small-order--1 slds-medium-order--3');
             var col2 = $("<div/>").addClass('slds-col slds-size--1-of-2 slds-small-order--1 slds-medium-order--2');

            if ((i % 2) != 1) {
                     col1.append(getCheckBoxDiv(varList[i].label, varList[i].value, initalLoad, "filtercheckBoxName_".concat(filterType), 'filtercheckBoxId'.concat(filterType).concat(i), 'checkBoxClass'));
                      parentDiv.append(col1);
             } else {
                     col2.append(getCheckBoxDiv(varList[i].label, varList[i].value, initalLoad, "filtercheckBoxName_".concat(filterType), 'filtercheckBoxId'.concat(filterType).concat(i), 'checkBoxClass'));

                      parentDiv.append(col2);
             }

             parentDiv.append(parentDiv);
       }



       divContainer.append(parentDiv);

       domElement.html(divContainer.html());

         //console.log("divContainer: " + divContainer.html());
    }



    function getCheckBoxDiv(strLabel, strValue, initalLoad, checkBoxName, filtercheckBoxId, checkBoxClassName) {

            var divCheckBox = $('<div/>').addClass("filterNameClass");

            var checkBox = $('<input/>').attr("type", "checkBox");
            $(checkBox).attr("name", checkBoxName);
            $(checkBox).attr("id", filtercheckBoxId);
             $(checkBox).attr("class", checkBoxClassName);
            $(checkBox).attr("value", strValue);

            if (initalLoad) {
                $(checkBox).attr('checked', true);
            }

            var label = $('<label/>').attr("for", filtercheckBoxId);
             // var label = $('<label/>');
            $(label).attr("class", "filterLableClass")
            $(label).append(strLabel);

            checkBox.append(label);

            divCheckBox.append(checkBox);
            divCheckBox.append(label);

            return divCheckBox;
    }


    function resetFilter(eventName) {
          skuid.events.publish(eventName);
    }


    function getSelectedCheckboxValues(fieldType) {

        var selectedValues = '';
        var checkBoxesName = "filtercheckBoxName_".concat(fieldType);
        var allCheckboxesSelected = false;

        //get all selected values
        var CheckBoxesArr = $("input[name='".concat(checkBoxesName).concat("']:checked")).map(function(){
                   return $(this).val();
        }).get();

        //create an string from selected values
        for (i = 0; i < CheckBoxesArr.length; i++) {
                    selectedValues   =    selectedValues.concat(CheckBoxesArr[i].concat(","));
        }

       //if all selection are selected there would not be any query filter
         if ( $("input[name='".concat(checkBoxesName).concat("']")).length ==  $("input[name='".concat(checkBoxesName).concat("']:checked")).length) {
             allCheckboxesSelected = true;
         }

          //remove the lasts ', ' from sub query filter
        if (selectedValues.length > 1)  {
             selectedValues = selectedValues.substring(0, (selectedValues.length-1));
          }

        var selectedMap = new Object();

        selectedMap["selectedValues"] = selectedValues;
        selectedMap["allValuesSelected"] = allCheckboxesSelected;

         return selectedMap;
    }
