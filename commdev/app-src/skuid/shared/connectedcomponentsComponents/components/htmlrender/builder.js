(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__htmlrender",
		name: "HTML Renderer",
		icon: "sk-icon-contact",
		description: "This component renders an HTML string",
		componentRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div>HTML will be rendered here</div>"
			);
		},
		mobileRenderer: function(component) {
			component.setTitle(component.builder.name);
			component.body.html(
				"<div class='hello-content'>Hello " + component.state.attr("person") + "!</div>"
			);
		},
		propertiesRenderer: function (propertiesObj,component) {
			propertiesObj.setTitle("HTML Render Component Properties");
			var state = component.state;
			var propCategories = [];
			var propsList = [
				{
					id: "htmlstring",
					type: "template",
					label: "HTML",
					defaultValue: "",
					helptext: "HTML",
					onChange: function(){
						component.refresh();
					}
				}
			];
			propCategories.push({
				name: "",
				props: propsList,
			});
			if(skuid.mobile) propCategories.push({ name : "Remove", props : [{ type : "remove" }] });
			propertiesObj.applyPropsWithCategories(propCategories,state);
		},
		defaultStateGenerator : function() {
			return skuid.utils.makeXMLDoc("<connectedcomponents__htmlrender/>");
		}
	}));
})(skuid);
