var $ = skuid.$;
(function(skuid){
	skuid.componentType.register("connectedcomponents__htmlrender", function(element,xmlDef) {
		var properties = {
			$root: element,
			htmlString: xmlDef.find('htmlstring').text(),
		};

		skuid.events.publish("ats.component.htmlRender.hasLoaded", [properties]);
	});
})(skuid);
