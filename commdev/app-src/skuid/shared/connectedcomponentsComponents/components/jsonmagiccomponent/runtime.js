var $ = skuid.$;
(function(skuid){

	skuid.componentType.register("connectedcomponents__jsonmagiccomponent",function(element,xmlDef){

		var properties = {
			$element: element,
			schema: xmlDef.find('schema').text(),
			uidef: xmlDef.find('uidef').text(),
			modelName: xmlDef.attr('modelId'),
			jsonFieldName: xmlDef.attr('fieldProp'),
			mode: xmlDef.attr('mode')
		};

		skuid.events.publish("ats.component.jsonMagic.hasLoaded", [properties]);
	});
})(skuid);
