(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__search",
		name: "Search",
		icon: "sk-icon-page-edit",
		description: "The Search Componets",
		componentRenderer: function (component) {


	        var r = skuid.builder.getBuilders().wrapper.componentRenderer;

	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },
		propertiesRenderer: function (propertiesObj, component) {

	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('Search  Properties');


	       var propsList = [
				{
				    id: "searchModelId",
				    type: "model",
				    label: "Search Model",
					required : true,
				    onChange: function () {
				        component.refresh();
				    }
				},
                {
				    id: "searchResumeModelId",
				    type: "model",
				    label: "Resume Search Model",
					required : true,
				    onChange: function () {
				        component.refresh();
				    }
				},
                {
                    id: "SearchTypeId",
                    type: "picklist",
                    label: "Search Type",
                    picklistEntries: [{
                                          value: "candidateSearch",
                                          label: "CandidateSearch"
                                      },
                                       {
                                           value: "duplicateSearch",
                                           label: "DuplicateSearch"
                                       }],
                                        defaultValue: 'candidateSearch',
                                        onChange: function(){
                                                component.refresh();
                                        }
                },
				{
				    id: "templateParamsjsonId",
				    type: "template",
					modelprop : "timeLineModelId",
					required : true,
				    label: "Search json format",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
                {
				    id: "searchEventId",
				    type: "string",
					required : true,
				    label: "Search Event Name",
				    helptext: "You will call this event name when you perform a search",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
                {
				    id: "hideTitleId",
				    type: "boolean",
					required : true,
				    label: "Inlucde Header Title",
				    helptext: "This would indicate you will render Header Title",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
                {
				    id: "includePaginationId",
				    type: "boolean",
					required : true,
				    label: "Inlucde Pagination",
				    helptext: "Indicate Whether the search rsults includes pagination",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
                {
				    id: "resultsPerPageId",
				    type: "string",
					required : true,
				    label: "Results Per Page",
				    helptext: "How many records display per page",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				}
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propsList,
	        });
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);


	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__search"));
	    }

	}));


})(skuid);
