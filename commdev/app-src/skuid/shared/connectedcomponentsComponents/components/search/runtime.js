    var $ = skuid.$;

    (function(skuid){

        skuid.componentType.register("connectedcomponents__search",function(domElement, xmlConfig, component){

            var searchModelName =           xmlConfig.attr('searchModelId');
            var resumeSearchModelName =     xmlConfig.attr('searchResumeModelId');
            var resultLayout =              xmlConfig.attr('templateParamsjsonId');
            var searchType =                xmlConfig.attr('SearchTypeId');
            var performSearchEventName =    xmlConfig.attr('searchEventId');
            var displayTitleFlag       =    xmlConfig.attr('hideTitleId');


            //pagination param
            var includePaginationFlag     =    xmlConfig.attr('includePaginationId');
            var recordPerPage             = getSearchResultRecordsPerPage(xmlConfig.attr('resultsPerPageId'));

            var parsedResultLayout = JSON.parse(resultLayout);

             //populate this array with the list of fields not to display for view/hide user selection
             var fieldsToHideList =  ["HideField1"];

             var model = skuid.model.getModel(searchModelName);

             skuid.events.subscribe(performSearchEventName,function(pageOffset, selectedFacets){


                pageOffset = (isNaN(pageOffset) ? 1 : pageOffset);

                performSearchAndRenderResults(  model,
                                                resumeSearchModelName,
                                                parsedResultLayout.resultLayout.modelcondition,
                                                parsedResultLayout,
                                                searchType,
                                                domElement,
                                                fieldsToHideList,
                                                displayTitleFlag,
                                                includePaginationFlag,
                                                recordPerPage,
                                                pageOffset,
                                                performSearchEventName,
                                                selectedFacets);
             });
    });

    })(skuid);


    function performSearchAndRenderResults(model, resumeSearchModelName, modelCondition, parsedResultLayout,
                                                        searchType, domElement,
                                                        fieldsToHideList, displayTitleFlag, includePaginationFlag, recordsPerPage,
                                                        pageOffset, performSearchEventName, selectedFacets) {
        var paginationParamMap;

        switch (searchType) {

            case "candidateSearch":

                    //reset any model older values
                    candidateSearchDeactivateConditions(model, modelCondition, []);

                    var candidateSearchParams = getCandidateSearchTermParamMap(pageOffset, recordsPerPage);

                    if (candidateSearchParams["hasValidKeywordSearchTerms"] &&
                        candidateSearchParams["hasValidLocationSearchTerms"])  {

                        setCandidateSearchModelCondition(model,
                                                        getCandidateSearchModelConditionsArray(modelCondition,
                                                                                        candidateSearchParams));
                        model.updateData(function() {

                            removePreviousSearchResultsAndMessages();

                            isSubmitedSearchQueryIncludedValidGeoLocation(model.data, candidateSearchParams);

                            $('#searchResultsWrapperId').prepend(
                                        displayInvalidSearchGeoLoctionIfAny(candidateSearchParams));

                            paginationParamMap =
                                getSearchPaginationParameter(includePaginationFlag,
                                                            recordsPerPage, pageOffset,
                                                            getSearchResultRecords(searchType, 'totalresult',   model.data),
                                                            performSearchEventName);
                            renderSearchResult( model,
                                                resumeSearchModelName,
                                                parsedResultLayout,
                                                searchType,
                                                domElement,
                                                fieldsToHideList,
                                                getSearchResultRecords(searchType, 'records',       model.data),
                                                getSearchResultRecords(searchType, 'facets',        model.data),
                                                getSearchResultRecords(searchType, 'totalresult',   model.data),
                                                displayTitleFlag,
                                                paginationParamMap,
                                                performSearchEventName,
                                                modelCondition,
                                                candidateSearchParams,
                                                selectedFacets);
                                            });
                    } else {
                        displayMissingRequiredSearchFields(domElement, candidateSearchParams);
                    }
                    break;

            case "duplicateSearch":

                    if (isDuplicateSearchRequiredFieldsProvided()) {

                            performDuplicateSearch().then(function callbackMethod(records){

                                paginationParamMap =
                                    getSearchPaginationParameter(includePaginationFlag,
                                                                recordsPerPage,
                                                                pageOffset,
                                                                getSearchResultRecords(searchType,
                                                                                        'totalresult', records),
                                                                performSearchEventName);

                                renderSearchResult( model,
                                                    '',
                                                    parsedResultLayout,
                                                    searchType,
                                                    domElement,
                                                    fieldsToHideList,
                                                    getSearchResultRecords(searchType, 'records', records),
                                                    '',
                                                    '',
                                                    displayTitleFlag,
                                                    paginationParamMap,
                                                    performSearchEventName,
                                                    modelCondition,
                                                    '',
                                                    selectedFacets);
                            });
                    } else {
                        displayMissingRequiredSearchFields(domElement);
                    }
                    break;
        }

    }


function renderSearchResult(model, resumeSearchModelName, parsedResultLayout, searchType, domElement,
                            fieldsToHideList, results, facets, totalResult, displayTitleFlag,
                            paginationParamMap, performSearchEventName, modelCondition,
                             searchParams, selectedFacets) {

    var divParent = $("<div/>");

    if (doesResultHaveRecords(results)) {

        var divPlaceHolder = $("<div/>");

        var divGridWrapper  = $('<div/>').addClass("slds-grid slds-wrap slds-grid--pull-padded");

        var facetsExistFlag = generateSearchResultFacets(divGridWrapper,
                                                            searchType, facets,
                                                            performSearchEventName, totalResult, selectedFacets);

        divParent.append(getSearchResultsTotalRecordHeaderTag(totalResult, searchType));

        handleSearchResultsTitleHeaders(divPlaceHolder, displayTitleFlag,
                                                        parsedResultLayout,fieldsToHideList);

        $.each(results, function(index, record) {
                var divRecord = $('<div/>').addClass("container__list-item");
                divPlaceHolder.append(divRecord.append(getSearchResultsLayout(record,
                                                                                parsedResultLayout,
                                                                                fieldsToHideList,searchType,
                                                                                model, resumeSearchModelName, modelCondition,
                                                                                searchParams)
                                                                        ));
        });


        handleSearchResultPaginationHtmlTag(divPlaceHolder, paginationParamMap);

        var searchResultDivGridColVal = (facetsExistFlag ? "10" : "12");

        var divResults  = $('<div/>')
        .addClass("slds-col--padded slds-size--1-of-2 slds-medium-size--4-of-6 slds-large-size--" +
                    searchResultDivGridColVal  + "-of-12");

        divResults.append(divPlaceHolder);
        divGridWrapper.append(divResults);
        divParent.append(divGridWrapper);

        domElement.html(divParent.html());

        handleSearchResultsPaginationEvent(paginationParamMap);

    } else {displayNoSearchResultsFound(domElement, divParent);}

    handleDuplicateSearchAddCandidateButtonEnable(searchType);

}


function getSearchResultsLayout(record, fieldLayout, fieldsToHideList, searchType,
                                model, resumeSearchModelName,  modelCondition, searchParamsMap) {

        var divWrapper  = $('<div/>');
        //the layout rule (from Json file) would be excuted for each row of results
        $(fieldLayout.resultLayout.rows).each(function(i, rows) {
                var parentDiv  = $('<div/>');
                //determining the numbre of rows of layout to display for each search results
                //each row of layout will have a set of fiels Specified  in layout json rule
                $(rows).each(function(k, row) {
                    divWrapper.append(constructSearchRowColumns(record,
                                                                row,
                                                                parentDiv,
                                                                fieldsToHideList,
                                                                searchType, model, resumeSearchModelName, modelCondition, searchParamsMap));
                });
        });
        return divWrapper.html();
    }



    function constructSearchRowColumns(record, row, parentDiv, fieldsToHideList,
                                    searchType, model, resumeSearchModelName, modelCondition, searchParamsMap) {

        var divWrapper = $('<div/>').addClass("slds-grid slds-wrap slds-grid--pull-padded");
        //each row will have a set of fields from a single search result
        $.each(row.columns, function(b, fieldArray) {
                var divColumn = $('<div/>').addClass('slds-no-flex slds-col--padded');
                getSearchColumnFieldDivElement(record,
                                                fieldArray,
                                                fieldsToHideList,
                                                parentDiv,
                                                divWrapper,
                                                divColumn,
                                                searchType, model, resumeSearchModelName, modelCondition,
                                                 searchParamsMap);
        });
        return parentDiv.html();
    }

function getSearchColumnFieldDivElement(record, fieldArray,
                          fieldsToHideList, parentDiv, divWrapper, divColumn,
                           searchType, model, resumeSearchModelName, modelCondition, searchParamsMap) {

        var fieldGridSize      =   '1'; //default size grid column
        var hyperLink          =   $('<a/>'); //incase of a field hyperlink
        var isHyperLink        =   false;
        var divFields          =   $('<div/>');
        var isFieldDisplayable =   false;
        var fieldSeparatorFlag =   false;

        //there could be composite fields like first/last name  or address
        $(fieldArray.fields).each(function(n, field) {
            if (isSearchResultFieldDisplayable(fieldsToHideList, field.name)) {

                isHyperLink  = isSearchFieldHyperLink(n, searchType, record, field, hyperLink, isHyperLink);
                var fieldValue = getSearchColumnRecordFieldValue(searchType, record, field.name);

                divFields.append(n > 0  && fieldSeparatorFlag && field.tagelement
                    && field.tagelement == "span" && fieldValue && fieldValue.length > 0
                                                    ? ", " : "");

                divFields.append(getSearchResultFieldElementTag(field, fieldValue, record,
                       searchType, model, resumeSearchModelName,  modelCondition, searchParamsMap["keyword"]));

                fieldGridSize = (field.gridsize ? field.gridsize.toString() : "1");
                isFieldDisplayable = true;
                if (fieldValue && fieldValue.length > 0) {fieldSeparatorFlag = true; }
            }
        });
        if (isFieldDisplayable) {
                divColumn.append(isHyperLink ? hyperLink.append(divFields.html()) : divFields.html());
                divColumn.addClass('slds-large-size--' + fieldGridSize + '-of-12');
                parentDiv.append(divWrapper.append(divColumn));
       }

    }


function getSearchResultFieldElementTag(field, value, record, searchType, model, resumeSearchModelName,
                                        modelCondition, keyword) {

    var tagElement;

    if (field.tagelement && field.tagelement === "button") {
            tagElement = getSearchResultsActionButtonTag(field, record, searchType,
                                                        resumeSearchModelName, modelCondition, keyword);
    } else {
            tagElement  = (field.tagelement && field.tagelement === "span" ? $('<span/>') : $('<div/>') )

            if (field.includepreferedflag && field.includepreferedflag.name) {
                var preferedValue = getSearchColumnRecordFieldValue(searchType, record,
                                                                    field.includepreferedflag.name);

                if (searchType === 'candidateSearch') {
                    var fieldValue = getSearchColumnRecordFieldValue(searchType, record, field.name);

                    if (preferedValue && fieldValue && fieldValue === preferedValue) {
                         $(tagElement).addClass("text__preferred");
                    }

                } else if (searchType === 'duplicateSearch')  {

                      if (preferedValue === field.includepreferedflag.value) {
                          $(tagElement).addClass("text__preferred");
                      }
                }
            }

            if(value && value.length > 0) {
                tagElement.append(field.inlinelabel && field.inlinelabel.length > 0 ? field.inlinelabel : '');
                tagElement.append(field.type ? getSearchResultsFieldType(value, field) : value);
                tagElement.addClass(field.classname);
            }

    }
    return tagElement;
}

function getSearchResultsFieldType(value, field) {
    switch (field.type) {
        case "date":
                if (value && value.length) {
                    value = skuid.time.formatDate("M dd, yy", skuid.time.parseSFDate(value));
                }
                return value;
                break;
        case "button":
                break;
    }
}

function isSearchFieldHyperLink(index, searchType, record, field, hyperLink, isHyperLink) {
    if (index == 0 && field.hyperlink && field.hyperlink.link) {
        var link = field.hyperlink.link;
        var params = constructHyperParmNameValuesOnField(searchType, record, field.hyperlink);
        link = link.concat(params);
        hyperLink.attr('href', link);
        isHyperLink = true;
    }
    return isHyperLink;
}


    function isSearchResultFieldDisplayable(fieldsToHideList, fieldName) {
        return (fieldName in fieldsToHideList ? false : true);
    }


    // in case there is a different betweeen data model return schema/Json format
    function getSearchColumnRecordFieldValue(searchType, record, fieldProperty) {
        switch (searchType) {
            case "candidateSearch":
                    if (fieldProperty === "highlight.body") {
                        var highlight = (record.highlight && record.highlight.body ?
                        record.highlight.body.toString() : record._source['teaser']);
                        return highlight;

                    } else if (fieldProperty === "DegreeTypeCode") {
                        var degreeCode  = (record._source && record._source.education_history && record._source.education_history.EducationOrganizationAttendance &&
                            record._source.education_history.EducationOrganizationAttendance[0] &&
                        record._source.education_history.EducationOrganizationAttendance[0].EducationDegree &&
                        record._source.education_history.EducationOrganizationAttendance[0]
                        .EducationDegree.DegreeTypeCode
                         ?
                        record._source.education_history.EducationOrganizationAttendance[0]
                        .EducationDegree.DegreeTypeCode : '');
                        return degreeCode;
                    } else {
                        return  (record._source[fieldProperty] ? record._source[fieldProperty] : "");

                    }
                    break;
            case "duplicateSearch":
                        return (record[fieldProperty] ? record[fieldProperty] : "");
                    break;
        }
    }

    function constructHyperParmNameValuesOnField(searchType, record, fieldProperties) {
            var paramStr = '';
            if (fieldProperties && fieldProperties.linkparam) {
                $.each(fieldProperties.linkparam, function(index, param) {
                        paramStr = paramStr.concat(index > 0 ? "&" : "");
                        paramStr = paramStr.concat(param.paramname).concat("=");
                        var parmValue =  param.paramvalue;
                        if (parmValue.search("{{") > -1) {
                            parmValue = (parmValue.replace("{{", "")).replace("}}", "");
                            parmValue =  getSearchColumnRecordFieldValue(searchType, record, parmValue);
                        }
                        paramStr = paramStr.concat(parmValue);
                });
            }
            return paramStr;
    }

    function setCandidateSearchModelCondition(model, modelConditionNameValueArray) {

        $(modelConditionNameValueArray).each(function(index, condition) {
            model.setCondition(model.getConditionByName(condition.conditionName),
                               condition.conditionValue, false);
        });

    }

    //candidate search only - the list could grow
    function getCandidateSearchModelConditionsArray(conditionArray, searchParamsMap) {

        var keyword = searchParamsMap["keyword"];

        var lastActivityDateFacet =  (searchParamsMap && searchParamsMap["facetLastActivityDate"]
                                            ?  searchParamsMap["facetLastActivityDate"] : '');
        var educationFacet        =  (searchParamsMap && searchParamsMap["facetEducation"]
                                            ? searchParamsMap["facetEducation"] : '');

        var country     = searchParamsMap["country"];
        var geoLocation = searchParamsMap["geoLocation"];
        var radiusUnit  = searchParamsMap["radiusUnit"];
        var radius      = searchParamsMap["radius"];

        var pageOffset  = searchParamsMap["pageOffset"];
        var recordsPerPage = searchParamsMap["recordsPerPage"];

        var geoSearchFlagOn = false;

        var modelConditionNameValueArray = [];
        var indexCounter = -1;  //can't user the index from eachloop since some condition are not included
        $(conditionArray).each(function(index, condition) {
                if (condition.name === 'param') {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                                            condition.name, keyword);
                } else if (condition.name === 'size') {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                                condition.name, recordsPerPage);
                } else if (condition.name === 'from') {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                condition.name,
                                                getSearchResultRecordsPageOffSet(pageOffset, recordsPerPage));
                } else if (condition.name === 'geoCntry' && searchParameterNotEmpty(country)) {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                         condition.name,country);
                } else if (condition.name === 'geo' && searchParameterNotEmpty(geoLocation)) {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                         condition.name, geoLocation);
                        geoSearchFlagOn = true;
                } else if (condition.name === 'geoUnitOfMeasure' && geoSearchFlagOn) {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                            condition.name, radiusUnit);
                } else if (condition.name === 'geoRadius' && geoSearchFlagOn) {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                             condition.name, radius);
                } else if (condition.name === 'facetlastactivity'
                                        && lastActivityDateFacet && lastActivityDateFacet.length > 0 ) {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                             condition.name, lastActivityDateFacet);
                } else if (condition.name === 'faceteducation'
                                        && educationFacet && educationFacet.length > 0 ) {
                        setSearchModelConditionNameValue(modelConditionNameValueArray,
                                                             condition.name, educationFacet.toString());
                }
        });
        return modelConditionNameValueArray;
    }


    function setSearchModelConditionNameValue(modelConditionNameValueArray, name, value) {
            modelConditionNameValueArray.push(
                                                {
                                                    conditionName  : name,
                                                    conditionValue : value
                                                }

                                            );
    }


    //place holder need to modify when the fields are created
    function getDuplicateSearchSearchFieldArray(model) {
        var searchFieldArray = [];
        searchFieldArray[0]  = ''; // need to know what the duplicate field values are
        return searchFieldArray;
    }

    //place holder need to modify when the conditions are created
    function  getDuplicateSearchSearchModelConditionArray() {

    var duplicateSearchConditions = [];

    var conditionArray = [];
    var arrayCounter = -1;

    var phone = $('#phoneId').val();
    var email = $('#emailId').val();

    if (phone && phone.length > 5) {
        conditionArray[++arrayCounter] =
                    constructDuplicateSearchCondition( "Phone",     $('#phoneId').val());
        conditionArray[++arrayCounter] =
                    constructDuplicateSearchCondition("MobilePhone", $('#phoneId').val());
        conditionArray[++arrayCounter] =
                    constructDuplicateSearchCondition("HomePhone",   $('#phoneId').val());
    }


    if (email && email.length > 5) {
        arrayCounter =  (arrayCounter > 0 ? arrayCounter : -1);
        conditionArray[++arrayCounter] =
                        constructDuplicateSearchCondition("Email",          $('#emailId').val());
        conditionArray[++arrayCounter] =
                        constructDuplicateSearchCondition("Other_Email__c",  $('#emailId').val());
    }

    duplicateSearchConditions[0] = conditionArray;
    duplicateSearchConditions[1] =  constructDuplicateSearchConditionLogicOrder(++arrayCounter);

    return duplicateSearchConditions;

    }


    //place holder need to modify when the fields are created
    function constructDuplicateCheckQuery() {
        var queryStr = '';

        var AND_OPERATOR = " AND ";
        var WILDCARD_ASKTERISK = "*";

        var firstName =  $('#firstNameId').val();
        var lastName =   $('#lastNameId').val();

        return queryStr.concat(firstName).concat(WILDCARD_ASKTERISK)
        .concat(AND_OPERATOR).concat(lastName);

    }


    function isDuplicateSearchRequiredFieldsProvided() {
        var firstName =  $('#firstNameId').val();
        var lastName =   $('#lastNameId').val();
        var phone =  $('#phoneId').val();
        var email =   $('#emailId').val();

        return  (firstName && firstName.length > 1 && lastName && lastName.length > 1 &&
            ((phone && phone.length > 4) || (email && email.length > 4)) ? true : false);
    }



function performDuplicateSearch() {

    var duplicateSearchReults;

    var firstLastNameQueryStr   = constructDuplicateCheckQuery();
    var conditionArray          = getDuplicateSearchSearchModelConditionArray();

    var searchCompleted = $.when(skuid.sfdc.search({
            query: firstLastNameQueryStr,
            searching: "NAME FIELDS",
            returning: [
                        { "objectName": "Contact",
                            "fields":   [
                                            "FirstName", "LastName", "AccountId", "Talent_Id__c",
                                            "Phone", "MobilePhone", "HomePhone", 'Preferred_Phone__c',
                                            "Email", "Other_Email__c", "MailingCity", "Preferred_Email__c",
                                            "MailingCountry", "MailingPostalCode", "MailingState"
                                        ],
                            "conditions"        : conditionArray[0],
                            "conditionLogic"    : conditionArray[1]
                        }
                    ]
    })).done(function(searchResult){
        duplicateSearchReults =   searchResult.results[0].records;
        console.log("searchResult.sosl =>" + searchResult.sosl);
    }).fail(function(searchResult){
        console.log('Search failed: ' + searchResult.error);
        duplicateSearchReults =  '';
    });
    return searchCompleted;
}


function appendDoublequotes(strValue) {
  return  "\"" + strValue + "\"";
}


function constructDuplicateSearchCondition(fieldName, fieldValue) {

        return  new skuid.model.Condition({
                                            field: fieldName,
                                            value: fieldValue,
                                            operator: "="
                                        });
}

function constructDuplicateSearchConditionLogicOrder(numberOfCondition) {

    var conditionLogic = '';
    for (var i = 1; i <= numberOfCondition; i++) {
        conditionLogic = conditionLogic.concat((i > 1 ? " OR " : ""));
        conditionLogic = conditionLogic.concat(i.toString());
    };
    return  conditionLogic;
}

function handleSearchResultsTitleHeaders(divPlaceHolder, displayTitleFlag, parsedResultLayout,
                                                                            fieldsToHideList) {

    if(displayTitleFlag === "true") {
            var titleHeaderArray = parsedResultLayout.resultLayout.rows[0].columns;
            var divParent = $('<div/>');
            var divWrapper = $('<div/>').addClass("slds-grid slds-wrap slds-grid--pull-padded  slds-text-heading--label");
            $(titleHeaderArray).each(function(index, field) {
                    $(field.fields).each(function(index, record) {
                        if (record.title) {
                            if (isSearchResultFieldDisplayable(fieldsToHideList, record.title)) {
                                var divCol =  $('<div/>').addClass('slds-col--padded slds-large-size--'
                                   +  (record.gridsize ? record.gridsize.toString() : '1')  +   '-of-12');
                                divCol.append(skuid.utils.mergeAsText("global",record.title));
                                divWrapper.append(divCol);
                            }
                        }
                    });
            });
            divParent.append(divWrapper);
            divPlaceHolder.append(divParent.html());
    }
}

function displayNoSearchResultsFound(domElement, divPlaceHolder) {
    divPlaceHolder.append("No results match your search criteria");
    domElement.html(divPlaceHolder.html());
}


function handleSearchResultPaginationHtmlTag(divPlaceHolder, paginationParamMap) {
            divPlaceHolder.append(
                paginationParamMap["includePaginationFlag"]=== 'true' ?
                                ("<ul id=\"pagination-demo\" class=\"pagination-sm\"></ul>") : '');
}

//http://esimakin.github.io/twbs-pagination/#intro pagination date
function setupSearchResultsPaginationEvent(paginationParamMap) {
    $('#pagination-demo').twbsPagination({
            totalPages: paginationParamMap["totalResultPages"],
            visiblePages: 10,
            initiateStartPageClick : false,
            startPage :  paginationParamMap["pageOffset"],
            onPageClick: function (event, page) {
                    skuid.events.publish(paginationParamMap["performSearchEventCall"],
                      [page, getCandidateSearchResultsSelectedFacetsArray()]);
            }
    });
}


function getSearchPaginationParameter(includePaginationFlag, recordsPerPage, pageOffset, totalResults,
                                                                performSearchEvent) {

        var paginationParamMap = new Object();

        totalResults = (isNaN(totalResults) ? 1 : totalResults);

        paginationParamMap["includePaginationFlag"]     = includePaginationFlag;
        paginationParamMap["recordsPerPage"]            = recordsPerPage;
        paginationParamMap["pageOffset"]                = (pageOffset ? pageOffset : 1);
        paginationParamMap["totalResults"]              = totalResults;
        paginationParamMap["totalResultPages"]      = Math.ceil(getPaginationTotalNumberOfPages(totalResults,
                                                                                        recordsPerPage));
        paginationParamMap["performSearchEventCall"]    = performSearchEvent;

        return paginationParamMap;
}


function getPaginationTotalNumberOfPages(totalRecord, resultsPerPage) {

    if (totalRecord &&  resultsPerPage) {
        return (totalRecord <= resultsPerPage ? 1 : (totalRecord / resultsPerPage) )
    }  else {
        return 1;
    }

}

function getSearchResultRecordsPageOffSet(paginationOffSet, resultRecordPerPage) {
     return  (isNaN(paginationOffSet) || paginationOffSet == 1 ? 0
                                    : ((paginationOffSet -1) * resultRecordPerPage));
}


function getSearchResultRecordsPerPage(recordPerPage) {
    return  (isNaN(recordPerPage)  ? 25 : parseInt(recordPerPage));
}

function handleSearchResultsPaginationEvent(paginationParamMap) {
    if (paginationParamMap["includePaginationFlag"] === 'true' ) {
        setupSearchResultsPaginationEvent(paginationParamMap);
    }
}

function handleDuplicateSearchAddCandidateButtonEnable(searchType) {
    if(searchType === "duplicateSearch") {
                skuid.events.publish("ats.duplicate.search.display.add.candidate.button");
    }
}

function doesResultHaveRecords(results) {
    return (
            (results && results !== ''
               && results !== 'undefined' &&  results !== undefined
               && results.length && results.length > 0)
            ? true
            : false
            )
}


function getSearchResultRecords(searchType, resultType, result) {
    switch (searchType) {
        case "candidateSearch":
                switch (resultType) {
                    case "records":
                            return (result && result[0] && result[0].response && result[0].response.hits
                                                ? result[0].response.hits.hits :  '');
                            break;
                    case "totalresult":
                                    return (result && result[0] && result[0].response && result[0].response.hits
                                                        ? result[0].response.hits.total :  '');
                                    break;
                    case "facets":
                            return (result && result[0] && result[0].response && result[0].response.aggregations
                                                ? result[0].response.aggregations :  '');
                            break;
                }
                break;
        case "duplicateSearch":
                    switch (resultType) {
                        case "records":
                                    return (result && result.results && result.results[0].records
                                    ? result.results[0].records : '');
                                    break;
                        case "totalresult":
                                    return (result && result.results && result.results[0].records
                                                        ? result.results[0].records.length : 0);
                                    break;
                    }
                break;
    }
}


function createEditDeleteButtonforSearchResults(field) {
    var button = $('<button/>').addClass(field.classname);
    $(button).attr('onclick', field.hyperlink);
    button.append(field.inlinelabel);
    return button;
}


function getCandidateSearchTermParamMap(pageOffset, recordsPerPage) {

    var searchParamsMap = new Object();

        var keyword         = $('#searchKeyWordId').val();
        var country         = $('#locationSearchCountryFieldId option:selected').text();
        var state           = $('#locationSearchStateFieldId option:selected').text();
        var city            = $('#cityinputid').val();
        var postalCode      = $('#postalcodeinputid').val();
        var radiusUnit      = $('#locationSearchRadiusUnitFieldId option:selected').val();
        var radius          = $('#locationSearchRadiusFieldId option:selected').val();
        var geoLocation     = getCandidateSearchGeoLocationValue(state, city, postalCode);

        searchParamsMap["hasValidKeywordSearchTerms"] = searchParameterNotEmpty(keyword);

        searchParamsMap["hasValidLocationSearchTerms"] =
                    isCandidateSearchLocationParamValid(country, state, city, postalCode, geoLocation);

        searchParamsMap["keyword"] = keyword;
        searchParamsMap["country"] = country;
        searchParamsMap["geoLocation"] = geoLocation;

        searchParamsMap["radiusUnit"]   =  (radiusUnit    ? radiusUnit    : "mi");
        searchParamsMap["radius"]       =  (radius        ? radius        : "25")
        //pagination
        searchParamsMap["pageOffset"] = pageOffset;
        searchParamsMap["recordsPerPage"] = recordsPerPage;

        selectedFacets = getCandidateSearchResultsSelectedFacetsArray();
        searchParamsMap["facetLastActivityDate"] = selectedFacets["lastActivityFacets"];
        searchParamsMap["facetEducation"] = selectedFacets["eductionFacets"];

    return  searchParamsMap;

}

function getCandidateSearchGeoLocationValue(state, city, postalCode) {
    var geoLocation = '';
    if (city && city.length > 2 && state) {
        geoLocation = city.concat(", ").concat(state);
    } else if (state) {
        geoLocation = state;
    } else if (postalCode && postalCode.length > 2) {
        geoLocation = postalCode;
    }  else {
        geoLocation = '';
    }
    return geoLocation;
}


function isCandidateSearchLocationParamValid(country, state, city, postalCode, geoLocation) {

    if (geoLocation === '' && (searchParameterNotEmpty(state) || searchParameterNotEmpty(city))) {
        return false;
    } else if (searchParameterNotEmpty(geoLocation) && (!country || country.length < 1)) {
        return false
    } else {
        return true;
    }

}

function searchParameterNotEmpty(field) {
    return (field && field.length > 1 ? true : false);
}


function generateSearchResultFacets(divGridWrapper, searchType, facets, performSearchEventName,
                            totalResult, selectedFacets) {

    var divFacets  = $('<div/>');

    var facetsExistFlag = false;

    if (searchType !== 'duplicateSearch') {

        divFacets
            .addClass("slds-col--padded slds-size--1-of-2 slds-medium-size--2-of-6  slds-large-size--2-of-12");

        if (searchType === 'candidateSearch') {
                    divFacets.append(getCandidateSearchFacets(facets, performSearchEventName,
                                                totalResult, selectedFacets));
        }

        facetsExistFlag = (divFacets.find( "li" ).length > 0  ? true : false);
    }

    divGridWrapper.append(divFacets);

    return facetsExistFlag;

}



function getCandidateSearchFacets(facets, performSearchEventName, totalResults, selectedFacets) {

        var divfacet  = $('<div/>');

        divfacet.append(getSearchResultsFacetsRecentActivity(facets.qualifications_last_activity_date,
                                                               performSearchEventName, totalResults, selectedFacets)
                       );
        divfacet.append(getSearchResultsFacetsEduction(facets.education_degree_name_fct,
                                                                performSearchEventName, totalResults,
                                                                                        selectedFacets)
                        );
        return divfacet;

}

function getSearchResultsFacetsEduction(education_degree_name_fct,
                                                    performSearchEventName, totalResults, selectedFacets) {


    if (education_degree_name_fct && education_degree_name_fct.buckets &&
        education_degree_name_fct.buckets.length > 0 ) {

            var divParent  = $('<div/>');

            var degreeLabelDiv = $('<div/>').addClass("slds-m-bottom--small");
            var degreFacetLabel = $('<span/>').addClass("slds-text-heading--label");
            degreFacetLabel.append("Degree");
            degreeLabelDiv.append(degreFacetLabel);

            divParent.append(degreeLabelDiv);

            var ulEle  = $('<ul/>');

            var selectedEducationFacets = ( selectedFacets && selectedFacets["eductionFacets"]
                                                ? selectedFacets["eductionFacets"] : [] );

            $(education_degree_name_fct.buckets).each(function(index, degree) {

                var onClickAtr = "onClickEductionFacetCheckBox('".concat(performSearchEventName).concat("');");

                var label = $('<label/>').addClass("slds-checkbox");
                var inputCheckbox = $("<input/>").attr("type", "checkbox").attr("value", degree.key);
                $(inputCheckbox).attr("id", "eductionFacetid_".concat(index));
                $(inputCheckbox).attr("class", "eductionFacetidClass");
                $(inputCheckbox).attr("name", "eductionFacetName");
                $(inputCheckbox).attr("onclick", onClickAtr);

                 if (($.inArray(degree.key, selectedEducationFacets) >= 0 ) ) {
                     $(inputCheckbox).attr("checked", "checked");
                 }

                label.append(inputCheckbox);

                var spanCheckBox = $('<span/>').addClass("slds-checkbox--faux");
                label.append(spanCheckBox);

                var spanLabel = $('<span/>').addClass("slds-form-element__label");;
                spanLabel.append(degree.key);
                label.append(spanLabel);

                var spanCount = $('<span/>').addClass("slds-text-body--small");
                spanCount.append("(".concat(degree.doc_count).concat(")"));
                label.append(spanCount);

                var liEle  = $('<li/>');

                var divForm =  $('<div/>').addClass("slds-form-element");
                var divFormEle = $('<div/>').addClass("slds-form-element__control");

                divFormEle.append(label)
                divForm.append(divFormEle);

                liEle.append(divForm);
                ulEle.append(liEle);
            });
            divParent.append(ulEle);
            return divParent;
        }
        return '';
}


function getSearchResultsFacetsRecentActivity(qualifications_last_activity_date,
                                                    performSearchEventName, totalResults, selectedFacets) {

        if (qualifications_last_activity_date && qualifications_last_activity_date.buckets &&
                                              qualifications_last_activity_date.buckets.length > 0) {
                var divParent  = $('<div/>').addClass("slds-m-bottom--small");
                var activityDateRangeArray = [];
                activityDateRangeArray.length = 4;
                var selectedLastActivityFacet  =    (selectedFacets && selectedFacets["lastActivityFacets"]
                                                            ? selectedFacets["lastActivityFacets"] : '');
                $(qualifications_last_activity_date.buckets).each(function(index, lastActivity) {
                            getLastActivityDateRange(activityDateRangeArray, lastActivity,
                                                    performSearchEventName, totalResults, selectedLastActivityFacet);
                });
                if (activityDateRangeArray.length > 0) {
                    var spanFacetTitle = $('<span/>').addClass("slds-text-heading--label");
                    spanFacetTitle.append("Recent Activity (days)");
                    divParent.append(spanFacetTitle);
                    divParent.append(constructSearchFacetList(activityDateRangeArray));
                }
                return divParent;
        }
        return '';
}

function constructSearchFacetList(listArray) {
    var ulEle  = $('<ul/>');
    $(listArray).each(function(index, navigator) {
            if(navigator) {
                var liDiv  = $('<li/>');
                liDiv.append(navigator.html());
                ulEle.append(liDiv);
            }
    });
    return ulEle;
}


//pastNumberOfDays 30, 60, 90 days
//use the http://momentjs.com/docs/#/parsing/string/ for moment.js reference
function getLastActivityDateRange(activityArrayPos, lastActivity, performSearchEventName,
                            totalResults, selectedLastActivityFacet) {

    var dateFrom    =    moment(lastActivity.from_as_string,  'MM/DD/YY');
    var dateTo      =    moment(lastActivity.to_as_string,    'MM/DD/YY');

    var days = dateTo.diff(dateFrom, 'days');

    var div         =  $('<div/>');

    var isLastActivitySelected = (selectedLastActivityFacet && selectedLastActivityFacet.length > 0
                                                    ? true : false);

    if (days > 90) {
        selectedLastActivityFacet =  (isLastActivitySelected
                                ? selectedLastActivityFacet : lastActivity.key);
        setFacetLastActivityDateRangeDocCount(div, "All", activityArrayPos, 3,
         lastActivity.key, performSearchEventName, totalResults,  selectedLastActivityFacet);

    } else if (days <= 30 && isLastActivityFacetSelectedExcludeOthers(
                                    isLastActivitySelected, lastActivity.key, selectedLastActivityFacet)) {
        setFacetLastActivityDateRangeDocCount(div, "30", activityArrayPos, 0,
        lastActivity.key, performSearchEventName, lastActivity.doc_count, selectedLastActivityFacet);

    } else if ( (days > 30 && days <= 60)  && isLastActivityFacetSelectedExcludeOthers(
                                    isLastActivitySelected, lastActivity.key, selectedLastActivityFacet)) {
        setFacetLastActivityDateRangeDocCount(div, "60", activityArrayPos, 1,
         lastActivity.key, performSearchEventName, lastActivity.doc_count, selectedLastActivityFacet);

    } else if ((days > 60 && days <= 90) && isLastActivityFacetSelectedExcludeOthers(
                                    isLastActivitySelected, lastActivity.key, selectedLastActivityFacet)) {
        setFacetLastActivityDateRangeDocCount(div, "90", activityArrayPos, 2,
         lastActivity.key, performSearchEventName, lastActivity.doc_count, selectedLastActivityFacet);

    }
}


function setFacetLastActivityDateRangeDocCount(div, label, activityArrayPos, index, key,
                                                        performSearchEventName, count, selectedLastActivityFacet) {

   var onClickSearchEventMethod =  "onClickLastActivityFacetRadioButton('" + performSearchEventName + "');";

   var radio = $('<input/>').attr("type", "radio").attr("value", key).attr("name" , "lastactivityfacetradbt");
   $(radio).attr("onclick", onClickSearchEventMethod);

   if (selectedLastActivityFacet && selectedLastActivityFacet  === key) {
            $(radio).attr("checked", "checked");
   }
   if (label === "All") { $(radio).attr("value", ''); }

   var spanRadio = $('<span/>').append(radio);

    var spanRange   = $('<span/>').addClass("slds-form-element__label");
    spanRange.append(label);

    if (label !== "All") {
            var spanCount  = $('<span/>');
            spanCount.append("(".concat(count).concat(")"));
    }

    var spanRadio = $('<span/>').addClass("slds-radio--faux");

    var labelEle =  $('<label/>').addClass("slds-radio");
    labelEle.append(radio);
    labelEle.append(spanRadio);
    labelEle.append(spanRange) ;
    if (label !== "All") {
            var spanCount  = $('<span/>').addClass("slds-text-body--small");
            spanCount.append("(".concat(count).concat(")"));
            labelEle.append(spanCount);
    }

    var activityDiv = $('<div/>').addClass("slds-form-element__control");

    activityDiv.append(labelEle);

    div.prepend(activityDiv);

    activityArrayPos[index] = div;
}


function getSearchReultsFacetLastActivityDateQueryParam() {
    return  $('input:radio[name=lastactivityfacetradbt]:checked').val();
}

function onClickLastActivityFacetRadioButton(eventName) {
     skuid.events.publish(eventName, [1, getCandidateSearchResultsSelectedFacetsArray()]);
}


function candidateSearchDeactivateConditions(model, conditons, exceptionListArray) {


    var desireConditions = removeConditionsFromSearchModel(conditons, exceptionListArray);

    $.each(desireConditions, function(index, condition) {
        model.deactivateCondition(model.getConditionByName(condition.name), false);
    });

}

function getSearchResultsActionButtonTag(field, record, searchType, model, modelCondition, keyword) {

    var functionParams = getSearchResultsActionButtonParamValues(field.functionparam, record, searchType);

    var conditionArray = [modelCondition];

    var buttonId = "viewResumeButtonId_".concat(record._id);

    var params = "('".concat(functionParams).concat("','").concat(model).concat("','").concat(buttonId).concat("','")
    .concat(record._id).concat("','").concat(keyword).concat("');");

    var functionName = field.functionname.concat(params);

    var button = $('<button/>').addClass(field.classname);
    $(button).attr('onclick', functionName);
    $(button).attr('id', buttonId);
    button.append(field.buttonname);

    return button;
}


function getSearchResultsActionButtonParamValues(paramArray, record, searchType) {

    var paramStr = '';
    $.each(paramArray, function(index, paramItem) {

        var param = paramItem.name;

        paramStr = paramStr.concat(index > 0 ? "," : "");

        if (param.search("{{") > -1) {
            param = (param.replace("{{", "")).replace("}}", "");
            param =  getSearchColumnRecordFieldValue(searchType, record, param);
        }
        paramStr = paramStr.concat(param);
    });
    return paramStr;
}


function loadCandidateSearchDiplayResume(recordId, resumeSearchModel, buttonElementId, candidateId, keyword){

    var displableResume;

    var talentDocumentModel =   skuid.model.getModel('TalentDocument');
    var resumeSearchModel        =   skuid.model.getModel(resumeSearchModel);

    talentDocumentModel.setCondition(talentDocumentModel.getConditionByName('Talent__r.Id'), recordId, false);

    talentDocumentModel.updateData(function() {

        var talentDocumentId = (talentDocumentModel && talentDocumentModel.data
                                    && talentDocumentModel.data[0] && talentDocumentModel.data[0].Id
                                    ?  talentDocumentModel.data[0].Id  : '')

        if (talentDocumentId && talentDocumentId.length > 0) {
            var path = '/apex/c__DisplayCandidateResume?param=' + keyword + "&id=" + candidateId
                                                                + "&talentDocumentId=" + talentDocumentId;
            if(sforce && sforce.console && sforce.console.isInConsole()) {
                sforce.console.openPrimaryTab(null, path, true, 'View Resume', null);
            } else {
                    window.location.replace(path);
            }
        } else  {
                    var buttonRef = "#".concat(buttonElementId);
                    $(buttonRef).text("No resume available");
                    $(buttonRef).prop("disabled", true);
        }
    });
}


function getSearchResultsTotalRecordHeaderTag(totalResult, searchType) {

    if (searchType === 'candidateSearch') {
        var divRecordTotalTag = $('<div/>');
        var divTotal =  $('<div/>').addClass("slds-text-body--small slds-m-bottom--small");
        divTotal.append("Total Results: ".concat(totalResult));
        divRecordTotalTag.append(divTotal);

        return  divRecordTotalTag;
    }
    return '';
}


function getCandidateSearchResultsSelectedFacetsArray() {

    var selectedFacets = new Object();

     var selectedLastActivityFacet =  $('input:radio[name=lastactivityfacetradbt]:checked').val();

     selectedFacets["lastActivityFacets"] = selectedLastActivityFacet;

    var values = $("input[name='eductionFacetName']:checked").map(function(){
                                return $(this).val();
                        }).get();

     selectedFacets["eductionFacets"] = values;

     return selectedFacets;
}

function onClickEductionFacetCheckBox(eventName) {
     skuid.events.publish(eventName, [1, getCandidateSearchResultsSelectedFacetsArray()]);
}


function removeConditionsFromSearchModel(conditionArray, exceptionConditionNames) {

    var desireConditionArray = [];

        $.each(conditionArray, function(index, condition) {
             if (($.inArray(condition.sourceParam, exceptionConditionNames) < 0 ) ) {
                 desireConditionArray.push(condition);
             }
        });

    return desireConditionArray;
}


function isLastActivityFacetSelectedExcludeOthers(isLastActivitySelected, key, selectedLastActivityFacet) {

    if (!isLastActivitySelected || (isLastActivitySelected && key === selectedLastActivityFacet)) {
         return true;
    } else {
        return false;
    }
}

//in case user include a geo location however search query noting having a geo location
//returned in the search results it means the geo location was invalid
function isSubmitedSearchQueryIncludedValidGeoLocation(queryParam, searchParamsMap){


    if (searchParamsMap && searchParamsMap["geoLocation"] && searchParamsMap["geoLocation"].length > 0) {
        var geoLocation = (queryParam && queryParam[0] && queryParam[0].header &&
                                queryParam[0].header.request && queryParam[0].header.request.params &&  queryParam[0].header.request.params.geo
                            ? queryParam[0].header.request.params.geo : '');
        searchParamsMap["queryResultsHasInValidGeoLocation"]  =
                                    (geoLocation && geoLocation.length > 1 ? false : true);
    }

}

function displayInvalidSearchGeoLoctionIfAny(searchParamsMap) {

    if (searchParamsMap && searchParamsMap["queryResultsHasInValidGeoLocation"]) {
        var div = $('<div/>').addClass("").attr("id", "invalidQueryGeoLocationId")
        div.append("The City does not match the State/Province selected, so location was not used in the                    search. Please update the city information and search again.");
        return div;
    }
       return '';

}


function removePreviousSearchResultsAndMessages() {
    $('#invalidQueryGeoLocationId').empty();
    $('#searchResultsWrapperId').show();
}


function displayMissingRequiredSearchFields(domElement, candidateSearchParams) {

    var errorMessage = '';

    if (candidateSearchParams) {
        if (!candidateSearchParams["hasValidKeywordSearchTerms"]) {
            errorMessage = "Keyword term is missing. ";
        }
        if (!candidateSearchParams["hasValidLocationSearchTerms"])
            errorMessage = errorMessage.concat("City State do not match. ")
    } else {
        errorMessage = errorMessage.concat("Required Search Terms are missing.");
    }

    removePreviousSearchResultsAndMessages()
     var div = $('<div/>').addClass("");
    div.append(errorMessage);
    domElement.html($(div)[0].outerHTML);
    }
