(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__simplecard",
		name: "Simple Card",
		icon: "sk-icon-contact",
		description: "This builds a basic card with no footer.",
		componentRenderer: function (component) {
			//console.log(arguments);

	        var r = skuid.builder.getBuilders().wrapper.componentRenderer;
	        //console.log(r);
	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },	
		propertiesRenderer: function (propertiesObj, component) {
	        
	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('Card Properties');


	        var propsList = [
				{
				    id: "modelforcard",
				    type: "model",
				    label: "Select the model to be displayed in card",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "title",
				    type: "template",
					modelprop : "modelforcard",
				    label: "Title Template",
					location: "attribute",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "titlecss",
				    type: "string",
				    label: "CSS class for title",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "subtitle",
				    type: "template",
					modelprop : "modelforcard",
				    label: "Subtitle Template",
					location: "attribute",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "subtitlecss",
				    type: "string",
				    label: "CSS class for subtitle",
				    onChange: function () {
				        component.refresh();
				    }
				}				
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propsList,
	        });
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);

	        
	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__simplecard"));
	    }
		
	}));

		
})(skuid);