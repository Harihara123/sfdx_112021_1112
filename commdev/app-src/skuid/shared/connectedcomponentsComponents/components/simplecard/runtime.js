var $ = skuid.$;

(function(skuid) {
    var $ = skuid.$;
    skuid.componentType.register("connectedcomponents__simplecard", function(domElement, xmlConfig, component) {
        
        var $xml = skuid.utils.makeXMLDoc;
        var xmlDefinition = $xml(loadXml());

        //pass in parameters (see if uniqueid created & passed by builder)
        //replace params in xml
            //mode
            //static resource path
            //JSON field for model token/id pairs OR 5 pairs of modeltoken1/modelid1

        domElement.empty();
        skuid.component.factory({
            element: domElement,
            xmlDefinition: xmlDefinition
        });
    });
})(skuid);

function loadXml() {
    var fileInfo;
    $.ajax({
    url: "/resource/1443811189000/mark", // path to file. IMPORTANT: This changes for every new upload of static resource!
    dataType: 'text', // type of file (text, json, xml, etc)
    success: function(data) { // callback for successful completion
      fileInfo = data;
    },
      async: false,
    error: function() { // callback if there's an error
      alert("error");
    }
  }); 
  return fileInfo;  
}