var $ = skuid.$;

(function(skuid) {
    var $ = skuid.$;
    skuid.componentType.register("connectedcomponents__simplecard", function(domElement, xmlConfig, component) {
        var myXML = skuid.utils.makeXMLDoc(xmlConfig[0].outerHTML.replace("connectedcomponents__simplecard", "wrapper").replace("/connectedcomponents__simplecard", "/wrapper"));
        domElement.empty();
        skuid.component.factory({
            element: domElement,
            xmlDefinition: myXML
        });
        var fullCard = buildTitle(xmlConfig);
        domElement.prepend(fullCard);
        //domElement first-child prepend(fullCard)
        //domElement.html('Simple Card is rendered!');
    });
})(skuid);

function buildTitle(xmlConfig) {
    var title = xmlConfig.attr('title');
    var titleCss = xmlConfig.attr('titlecss');
    var subtitle = xmlConfig.attr('subtitle');
    var subTitleCss = xmlConfig.attr('subtitlecss');
    var modelForCard = xmlConfig.attr('modelforcard');
    var model = skuid.model.getModel(modelForCard);
    //var name = model.getFieldValue(model.getFirstRow(), 'Name')

    var cardGrandParent = $('<div/>').addClass("slds-card");
    var cardParent = $('<div/>').addClass("slds-card__header");
    var cardChild = $('<div/>').addClass("slds-card__header slds-grid");
    
    var titleVal = validVal(skuid.utils.merge('row', title, null, model, model.getFirstRow())); 
    var subtitleVal = validVal(skuid.utils.merge('row', subtitle, null, model, model.getFirstRow())); 
    var cardTitle = $('<div/>').addClass("slds-text-heading--small slds-truncate").addClass(titleCss).html(titleVal);
    var cardSubTitle = $('<div/>').addClass("slds-text-heading--xx-small slds-truncate").addClass(subTitleCss).html(subtitleVal);

    var cardGrandChild1 = $('<div/>').addClass("slds-has-flexi-truncate").append(cardTitle);
    var cardGrandChild2 = $('<div/>').addClass("slds-no-flex").append(cardSubTitle);

    var finalMarkup = cardGrandParent.append((cardParent.append((cardChild.append(cardGrandChild1).append(cardGrandChild2)))));

    return finalMarkup;
}

function validVal(val) {
    if (val) {
        return val;
    } else {
        return "";
    }
}

function buildCard(xmlConfig, model, i, cardResponsiveDivClass) {
    var modelRows = model.getRows();
    var cardTest = $('<div class="slds-card" />').append(buildHeader(xmlConfig.attr('header'), model, modelRows[i])).append(buildBody(xmlConfig.attr('body'), model, modelRows[i])).append(buildFooter(xmlConfig.attr('footer'), model, modelRows[i]));
    var cardSized = $('<div/>').addClass(cardResponsiveDivClass).append(cardTest);
    //var cardSpacer = $('<div class="' + cardResponsiveDivClass + '"/>');
    //cardSized.insertBefore(cardSpacer);
    return cardSized;
}

function buildHeader(headerTemplate, model, row) {
    var $ = skuid.$;
    var cardHeader = $('<header/>').addClass('slds-card__header slds-grid');
    var hLevel2 = $('<div/>').addClass('slds-media slds-media--center slds-has-flexi-truncate');
    var hLevel3 = $('<div/>').addClass('slds-media__body');
    var hLevel4 = $('<h3/>').addClass('slds-text-heading--small slds-truncate');
    hLevel4.html(skuid.utils.merge('row', headerTemplate, null, model, row));
    return cardHeader.append(hLevel2.append(hLevel3.append(hLevel4)));
}

function buildFooter(footerTemplate, model, row) {
    var $ = skuid.$;
    var cardFooter = $('<footer/>').addClass('slds-card__footer');
    return cardFooter.html(skuid.utils.merge('row', footerTemplate, null, model, row));
}

function buildBody(bodyTemplate, model, row) {
    var $ = skuid.$;
    var cardBody = $('<section/>').addClass('slds-card__body');
    return cardBody.html(skuid.utils.merge('row', bodyTemplate, null, model, row));
}