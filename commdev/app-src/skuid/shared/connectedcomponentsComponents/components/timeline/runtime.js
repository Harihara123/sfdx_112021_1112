var $ = skuid.$;


(function(skuid){

	skuid.componentType.register("connectedcomponents__timeline",function(domElement, xmlConfig, component){

		var modelName = xmlConfig.attr('timeLineModelId');
		var templateParamsjson =  xmlConfig.attr('templateParamsjsonId');
		var loadMoreOption     =  xmlConfig.attr('loadMoreOptionId');
		var renderTimeLineEvent     =  xmlConfig.attr('renderTimeLineEventId');
		var loadMoreEventId     =  xmlConfig.attr('loadMoreEventId');

        svg4everybody();

		var model = skuid.model.getModel(modelName);

		var ui =  skuid.utils.generateGUID();



		renderTimeLine(model, templateParamsjson, domElement, loadMoreOption, loadMoreEventId, ui);

		 //will get invoked when click on "More"
		skuid.events.subscribe(loadMoreEventId,function(loadedResult){
		 	 loadMoreTimeLine(model,
                                templateParamsjson,
                                domElement,
                                loadMoreOption,
                                loadMoreEventId,
                                ui);
        });

	    //will get invoked when there is a model save/update/delete
	    skuid.events.subscribe(renderTimeLineEvent,function(saveResult){
                    renderTimeLine(model,
                                    templateParamsjson,
                                    domElement,
                                    loadMoreOption,
                                    loadMoreEventId,
                                    ui);
        });

	});

})(skuid);


function renderTimeLine(model, templateParamsjson, domElement, loadMoreOption, loadMoreEventId, ui) {

         var jsonVariables  =  convertJsonToArray(templateParamsjson);

	 	 var displayList =  jsonVariables["displayList"];
	     var ParentClassName =  jsonVariables["ParentClassName"];
	     var addInfoButton =  jsonVariables["AddInfoButton"];
	     var editButtonRequired =   jsonVariables["EditButtonRequired"];
	     var deleteButtonRequired =   jsonVariables["DeleteButtonRequired"];

		var docText = $('<div/>').addClass("outerDivContainer");

		if (model && model.data && model.data.length > 0) {

            var divContainer = $('<div/>');
            var parentUL =    $('<ul/>').addClass("slds-timeline");
            if (ParentClassName && ParentClassName !== '') {
                $(divContainer).attr("class", ParentClassName);
            }

		  $.each(model.data,function(i,row){

				var parentLi =    $('<li/>').addClass("slds-timeline__item");
				var div1 =        $('<div/>').addClass("slds-media");
				var div2 =        $('<div/>').addClass("slds-media__body");
				var div3 =        $('<div/>').addClass("slds-media slds-media--timeline slds-timeline__media--email");
				var div4 =        $('<div/>').addClass("slds-media__figure");
				var svg =         $('<svg/>').addClass("slds-icon slds-icon-text-default");
				$(svg).attr("aria-hidden", true);
				var use =         $('<use/>');
				$(use).attr("xlink:href", "/resource/LightningDesignSystem/assets/icons/utility-sprite/svg/symbols.svg#record");
				var div5 =        $('<div/>').addClass("slds-media__body");

				var childUL =    $('<ul/>').addClass("slds-tile__detail");


				var liStr =  getListRowTimeLine(model, row, i, displayList, addInfoButton);

                div4.append(svg.append(use));
                div5.append(childUL.append(liStr));

                div3.append(div4);
                div3.append(div5);

                div2.append(div3);
                div1.append(div2);

                parentLi.append(div1);

				docText = docText.append(divContainer.append(parentUL.append(parentLi)));

				div1.append((editButtonRequired || deleteButtonRequired) ?  addEditDeleteButtonTimeline(editButtonRequired, deleteButtonRequired, model, row) : "");


		});
                 docText = docText.append(renderLoadMore(loadMoreOption, model, loadMoreEventId, ui));
      } else {
		         docText = docText.append(skuid.utils.mergeAsText("global","{{$Label.ATS_THERE_NO_MORE_RECORDS}}"));
	}

	domElement.html(docText.html());

 }



function  convertJsonToArray(json) {

    var jsonVariables = new Object();
    var arr = [];
	var parsed = JSON.parse(json);

	for(var x in parsed) {
		if(startsWithTimeline(x.toLowerCase(), "line")) {
		   arr.push(parsed[x]);
		}
		else if(startsWithTimeline(x.toLowerCase(), "parentclassname")) {
		    jsonVariables["ParentClassName"] = parsed[x];
	    } else if(startsWithTimeline(x.toLowerCase(), "addinfobutton")) {
		    jsonVariables["AddInfoButton"] = parsed[x];
	    } else if(startsWithTimeline(x.toLowerCase(), "editbutton")) {
		    jsonVariables["EditButtonRequired"] = parsed[x];
	    } else if(startsWithTimeline(x.toLowerCase(), "deletebutton")) {
		    jsonVariables["DeleteButtonRequired"] = parsed[x];
	    }
	}

    jsonVariables["displayList"] = arr;

	return jsonVariables;
 }

 function startsWithTimeline(string, pattern) {
 	return string.slice(0, pattern.length) === pattern;
 }


 function createEditDeleteButtonforTimeLineV1(buttonProp, model, row){
   var divButton = $('<div/>').addClass("divbuttonClass");;
   var button = $('<button/>').addClass(buttonProp.classname);
   $(button).attr(buttonProp.attributeName,  skuid.utils.mergeAsText('row',buttonProp.attributeValue,{},model, row));
   button.append(skuid.utils.mergeAsText("global",buttonProp.labelname));
   divButton.append(button);
   return divButton;
}


function loadMoreTimeLine(model, templateParamsjson, domElement, loadMoreOption, loadMoreEventId, ui) {

	if (model.canRetrieveMoreRows) {
	        model.loadNextOffsetPage(function () {
		         if (model.canRetrieveMoreRows) {
		                 $("#hasMoreDivId".concat(ui)).show();
		         } else {
		                 //hide the link - here is not more data to load
		                $("#hasMoreDivId".concat(ui)).hide();
		         }
	        });
	} else {
	     //hide the link - here is not more data to load
	     $("#hasMoreDivId".concat(ui)).hide();
	}
}



function renderLoadMore(loadMoreOption, model, loadMoreEventId, ui) {

    var hasMoreDiv =   $('<div/>');

	if(loadMoreOption === 'true' &&  model.canRetrieveMoreRows) {

		var onMoreDivId = "hasMoreDivId".concat(ui);
			$(hasMoreDiv).attr("id", onMoreDivId);

        var hrefLink = "javascript:callRegisterEventTimeLine('"  + loadMoreEventId +  "')";
		var onMoreLink = $("<a/>");
			$(onMoreLink).attr("href", hrefLink);
			$(onMoreLink).append(skuid.utils.mergeAsText("global","{{$Label.ATS_LOAD_MORE}}"));
        hasMoreDiv.append(onMoreLink);
	}

	return hasMoreDiv;
}




function callRegisterEventTimeLine(eventName) {
   skuid.events.publish(eventName);
 }


 function isNotEmpty(strVal) {
 	return (strVal ? true : false);
 }


 function getListRowTimeLine(model, row, i,  displayList, addInfoButton) {

                var liStr =  $('<div/>');

                $.each(displayList, function(k, lineinfo) {

                        var isRequiredFieldMissing = false;
                        var listItemVal = lineinfo.textline;

                     if(lineinfo.fieldproperty) {
                        //in case of any rules to be applied (ex: maxLength, required field),
                	    $.each(lineinfo.fieldproperty, function(n,field){

							var fieldName = "{{{".concat(field.fieldname).concat("}}}");
							var fieldValue = eval("model.data[i]." + field.fieldname);

                            //if field is in the textline
                            if (listItemVal.search(fieldName) >= 0) {
                            		//required rule
                            		if (isPropertyExistAndTrue(field.isrequired) && (!isNotEmpty(fieldValue))) {
                            			  var missingFieldDiv = $('<div/>').append(($('<span/>').addClass("missingFieldClassName")).append(field.requirefieldlabel));
		                                  listItemVal = listItemVal.replace(fieldName, missingFieldDiv.html());
		                                  isRequiredFieldMissing = true;
                            		}
                            		//maxlength rule
                            		if (field.maxlength && fieldValue && (fieldValue > parseInt(field.maxlength))) {
                            			   listItemVal = listItemVal.replace(fieldName, fieldValue.toString().substring(0, parseInt(field.maxlength)));
                            		}
                            }
                        });
					}

                        liStr.append($('<li/>').append( skuid.utils.merge('row',listItemVal,{},model, row).html()));

                        if (addInfoButton && isRequiredFieldMissing) {
                	 		  liStr.prepend(createEditDeleteButtonforTimeLineV1(addInfoButton, model, row));
               			 }
                });

				/**
				 * HACK: Forces the content into a single line, because skuid decided to give us extra HTML we didn't
				 * ask for
				 */
				liStr.children("li").children("div.titleLink.itemList").children("div").css("display", "inline-block");

	           return liStr;
 }

function isPropertyExistAndTrue(property) {

  return ((property && property === 'true')	? true : false);

 }


 function addEditDeleteButtonTimeline(editButtonRequired, deleteButtonRequired, model, row) {

	    var div = $('<div/>').addClass("slds-timeline__actions");

	     div.append(editButtonRequired   ? createEditDeleteButtonforTimeLineV1(editButtonRequired, model, row) : "");
         //div.append("<br/><br/>");
	     div.append(deleteButtonRequired ? createEditDeleteButtonforTimeLineV1(deleteButtonRequired, model, row) : "");

	    return  ($('<div/>').addClass("slds-media__figure slds-media__figure--reverse")).append(div);

 }
