(function() {
    skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
        id: "connectedcomponents__timelineactivityv2",
        name: "Activity Timeline V2",
        icon: "sk-icon-contact",
        description: "The Activity Timeline Component",
        componentRenderer: function(component) {
            component.setTitle(component.builder.name);
        },
        propertiesRenderer: function(propertiesObj, component) {
            propertiesObj.setTitle(component.builder.name);
            var defaultProps = [
                {
                    id: "nextStepsModelName",
                    type: "model",
                    label: "NextSteps Model",
                    required: true,
                    onChange: function() {
                        component.refresh();
                    }
                },
                {
                    id: "pastActivityModelName",
                    type: "model",
                    label: "PastActivity Model",
                    required: true,
                    onChange: function() {
                        component.refresh();
                    }
                },
                {
                    id: "includeButtons",
                    type: "boolean",
                    label: "Include Edit and Delete buttons",
                    required: false,
                    defaultValue: true,
                    onChange: function() {
                        component.refresh();
                    }
                },
                {
                    id: "includeLoadMore",
                    type: "boolean",
                    label: "Include a 'Load More' option",
                    required: false,
                    defaultValue: true,
                    onChange: function() {
                        component.refresh();
                    }
                },
                {
                    id: "loadMoreCount",
                    type: "picklist",
                    label: "Number of activities to load via 'Load More'",
                    picklistEntries: [
                        { value: "1", label: "1" },
                        { value: "2", label: "2" },
                        { value: "3", label: "3" },
                        { value: "4", label: "4" },
                        { value: "5", label: "5" },
                        { value: "6", label: "6" },
                        { value: "7", label: "7" },
                        { value: "8", label: "8" },
                        { value: "9", label: "9" },
                        { value: "10", label: "10" }
                    ],
                    defaultValue: "1",
                    required: false,
                    onChange: function() {
                        component.refresh();
                    }
                }
            ];
            var eventProps = [
                {
                    id: "onMarkCompleteEvent",
                    type: "text",
                    label: "Event to publish on mark complete request",
                    required: true,
                    defaultValue: "ats.activityTimeline.wishCouldMarkComplete",
                    onChange: function() {
                        component.refresh();
                    }
                },
                {
                    id: "onEditEvent",
                    type: "text",
                    label: "Event to publish on edit request",
                    required: true,
                    defaultValue: "ats.activityTimeline.wishCouldEdit",
                    onChange: function() {
                        component.refresh();
                    }
                },
                {
                    id: "onDeleteEvent",
                    type: "text",
                    label: "Event to publish on delete request",
                    required: true,
                    defaultValue: "ats.activityTimeline.wishCouldDelete",
                    onChange: function() {
                        component.refresh();
                    }
                }
            ];
            var propCategories = [
                { name: "Properties", props: defaultProps },
                { name: "Events", props: eventProps }
            ];

            propertiesObj.applyPropsWithCategories(propCategories, component.state);
        },
        defaultStateGenerator: function() {
            return skuid.utils.makeXMLDoc(`<connectedcomponents__timelineactivityv2 includeButtons="true" includeLoadMore="true" onMarkCompleteEvent="wishCouldMarkComplete" onEditEvent="wishCouldEdit" onDeleteEvent="wishCouldDelete" />`);
        }
    }));
})(skuid);
