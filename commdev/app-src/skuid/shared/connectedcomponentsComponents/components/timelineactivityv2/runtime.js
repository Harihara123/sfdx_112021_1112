(function(skuid){
	skuid.componentType.register("connectedcomponents__timelineactivityv2", function(element, xmlDef) {
		var properties = {
			elementId: "#" + xmlDef.attr("uniqueid"),
			nextStepsModelName: xmlDef.attr("nextStepsModelName"),
			pastActivityModelName: xmlDef.attr("pastActivityModelName"),
		    includeButtons: stringToBoolean(xmlDef.attr("includeButtons")),
		    includeLoadMore: stringToBoolean(xmlDef.attr("includeLoadMore")),
		    loadMoreCount: parseInt(xmlDef.attr("loadMoreCount"), 10),
			events: {
				wishCouldMarkComplete: xmlDef.attr("onMarkCompleteEvent"),
				wishCouldEdit: xmlDef.attr("onEditEvent"),
			    wishCouldDelete: xmlDef.attr("onDeleteEvent")
			}
		};
		skuid.events.publish("ats.component.timelineActivityV2.hasLoaded", [properties]);
	});

	function stringToBoolean(value) {
		if (value === "true") return true;
		else return false;
	}
})(skuid);
