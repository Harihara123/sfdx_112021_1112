(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__timelineactivityv3",
		name: "Activity Time Line V3",
		icon: "sk-icon-page-edit",
		description: "The Time Line Componets V3 including Apex class call",
		componentRenderer: function (component) {


	        var r = skuid.builder.getBuilders().wrapper.componentRenderer;

	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },
		propertiesRenderer: function (propertiesObj, component) {

	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('Activity Time Line Properties');


	       var propsList = [
				{
				    id: "activityOpenModelId",
				    type: "model",
				    label: "Time Line Model Activities Open",
					required : true,
				    onChange: function () {
				        component.refresh();
				    }
				},
                {
				    id: "activityHistoryModelId",
				    type: "model",
				    label: "Time Line Model Activities Hisotry",
					required : true,
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "templateParamsjsonId",
				    type: "template",
					modelprop : "timeLineModelId",
					required : true,
				    label: "Time Line list Item key/value json format",
					helptext: "{\"line1\":\"{{{duration}}}\",\"line2\":\"<div class=\"titleLink itemList\"><a href='javascript: loadDataTimeLine(\"OneEmploymentDetail\", \"{{{Id}}}\", \"Id\", \"displayEmploymentDetailSectionId\")' id=\"titleLinkId\" >{{{Title__c}}}</a></div>\", \"line3\":\"<div class=\"itemList\">{{{Organization_Name__c}}}</div>\",\"PDC\":\"ParentDiveClass\",\"excludebuttons\":\"true\"}",

					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
				{
                    id: "MorePicklistPropId",
                    type: "picklist",
                    label: "'More' on load value",
                    picklistEntries: [{
                                          value: "1",
                                          label: "1"
                                       },
                                       {
                                           value: "3",
                                           label: "3"
                                        },
                                        {
                                            value: "5",
                                            label: "5"
                                         },
                                         {
                                            value: "7",
                                            label: "7"
                                         },
                                         {
                                            value: "10",
                                            label: "10"
                                         }],
                                        defaultValue: '3',
                                        onChange: function(){
                                                component.refresh();
                                        }
                },
                {
				    id: "loadMoreOptionId",
				    type: "boolean",
					required : true,
				    label: "Include a Load 'More..' option ",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				}
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propsList,
	        });
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);


	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__timelineactivityv3"));
	    }

	}));


})(skuid);
