var $ = skuid.$;

(function(skuid){

    skuid.componentType.register("connectedcomponents__timelineactivityv3",function(domElement, xmlConfig, component){
        // 2 models Name :  acitvity model  open and history
        var activityModelOpen =     xmlConfig.attr('activityOpenModelId');
        var activityModelHistory =  xmlConfig.attr('activityHistoryModelId');
        //the json param for the timeline
        var templateParamsjson =    xmlConfig.attr('templateParamsjsonId');
        //load more constant 1, 3 , 5, 10
        var loadMoreConstant =      xmlConfig.attr('MorePicklistPropId');
        //boolean (checkbox) whether to show the loadmore option on the timeline
        var loadMoreOption =        xmlConfig.attr('loadMoreOptionId');

         svg4everybody();

        //need to create div container where we write the activities inside of it
        creatContainerActivity(domElement);

        activeModelCondtionsActivityTimeLine(activityModelOpen, templateParamsjson);
        activeModelCondtionsActivityTimeLine(activityModelHistory, templateParamsjson);



        //render open activity wait for it to finish rendering and then render past activity
        renderActivity(
            activityModelOpen,
            templateParamsjson,
            'OpenActivities',
            parseInt(loadMoreConstant),
            'openActivityDivId',
            getQueryParamActivity('', "ASC"),
            loadMoreOption
        ).then(function() {
            renderActivity(
                activityModelHistory,
                templateParamsjson,
                'ActivityHistories',
                parseInt(loadMoreConstant),
                'PastActivityDivId',
                getQueryParamActivity('', "DESC"),
                loadMoreOption
            );
        });

        //handles open activitiy
        skuid.events.subscribe('OpenActivities',function(queryParamloadMore){
            queryParamloadMore[1] = "ASC";

            var constantLoadMoreOA =  getQueryLimitActivity(skuid.model.getModel(activityModelOpen).data.length, parseInt(loadMoreConstant), false);
            renderActivity(activityModelOpen, templateParamsjson, 'OpenActivities', constantLoadMoreOA, 'openActivityDivId', queryParamloadMore,loadMoreOption);
        });

        skuid.events.subscribe('ActivityHistories',function(queryParamloadMore){
            queryParamloadMore[1] = "DESC";
            var constantLoadMorePA =  getQueryLimitActivity(skuid.model.getModel(activityModelHistory).data.length, parseInt(loadMoreConstant), false);
            renderActivity(activityModelHistory, templateParamsjson, 'ActivityHistories', constantLoadMorePA, 'PastActivityDivId', queryParamloadMore,loadMoreOption);
        });

        skuid.events.subscribe('filterOnActivity',function(queryParam){

            var constantLoadMorePA =  getQueryLimitActivity(skuid.model.getModel(activityModelHistory).data.length, parseInt(loadMoreConstant), true);
            var constantLoadMoreOA =  getQueryLimitActivity(skuid.model.getModel(activityModelOpen).data.length, parseInt(loadMoreConstant), true);
            queryParam[1] = "ASC";
            renderActivity(activityModelOpen, templateParamsjson, 'OpenActivities', constantLoadMoreOA, 'openActivityDivId', queryParam,loadMoreOption);
            queryParam[1] = "DESC";
            renderActivity(activityModelHistory, templateParamsjson, 'ActivityHistories', constantLoadMorePA, 'PastActivityDivId', queryParam,loadMoreOption);

        });

        skuid.events.subscribe('editAddActivityQueryModel',function(queryParam){

            var constantLoadMorePA =  getQueryLimitActivity(skuid.model.getModel(activityModelHistory).data.length, parseInt(loadMoreConstant), true);
            var constantLoadMoreOA =  getQueryLimitActivity(skuid.model.getModel(activityModelOpen).data.length, parseInt(loadMoreConstant), true);

            queryParam[1] = "ASC";
            renderActivity(activityModelOpen, templateParamsjson, 'OpenActivities', constantLoadMoreOA, 'openActivityDivId', queryParam,loadMoreOption);
            queryParam[1] = "DESC";
            renderActivity(activityModelHistory, templateParamsjson, 'ActivityHistories', constantLoadMorePA, 'PastActivityDivId', queryParam,loadMoreOption);

        });

        skuid.events.subscribe('onSaveCheckBoxPopupApplyfilter', function(){
            renderTimelineonCheckboxUpdatePopup();
        });

        var jsonVariables  =  convertJsonToArrayActivity(templateParamsjson);
        var ephemeralModel = jsonVariables["ephemeral_model"];

        skuid.events.subscribe('onSaveCheckBoxPopupEventValidateFields', function(){
            onCheckboxPopupSaveModels(ephemeralModel.event.modelName, 'Event');
        });
        skuid.events.subscribe('onSaveCheckBoxPopupTaskValidateFields', function(){
            onCheckboxPopupSaveModels(ephemeralModel.task.modelName, 'Task');
        });

    });
})(skuid);



function renderActivity(modelName, templateParamsjson, dataObject, loadMoreConstantValue, divSectioinId, queryParam, loadMoreOption) {

    console.log("Activity TimeLine renders ! ");

    var model = skuid.model.getModel(modelName);
    if (model) {
        model.emptyData();
    }

    setModelConditionActivity(model, dataObject, loadMoreConstantValue, queryParam, templateParamsjson);

    return model.updateData(function() {
         renderHtmlTagsActivity(model,
                                templateParamsjson,
                                divSectioinId,
                                dataObject,
                                loadMoreConstantValue,
                                queryParam,
                                loadMoreOption
                               );
    });
}



function setModelConditionActivity(model, dataObject, loadMoreConstantValue, queryParam, templateParamsjson) {

    var jsonVar  =  convertJsonToArrayActivity(templateParamsjson);
    var conditionNames =  jsonVar["presistence_model"];

    //set condition or the query parameters
    model.setCondition(model.getConditionByName(conditionNames.conditionname.recordid), eval('skuid.page.params.' + conditionNames.accountidparamname));
    model.setCondition(model.getConditionByName(conditionNames.conditionname.dataobject), dataObject);
    model.setCondition( model.getConditionByName(conditionNames.conditionname.loadMore),  loadMoreConstantValue + 1);
    model.setCondition(model.getConditionByName(conditionNames.conditionname.sortorder),  queryParam[1]);
    model.setCondition(model.getConditionByName(conditionNames.conditionname.queryfilter), getModelConditionQueryFilterActivity(queryParam[0]));

}



function getModelConditionQueryFilterActivity(filterStr) {

    var filterQuery = '';
    if (filterStr  && filterStr.length > 1) {
        filterQuery = " ActivityType in ( " + addSingleQtoSubQueryActivity(filterStr) +  " ) ";
    }
    //console.log("filterQuery: " + filterQuery);
    return filterQuery;
}



function renderHtmlTagsActivity(model, templateParamsjson, divSectioinId, dataObject, loadMoreConstantValue, queryParam, loadMoreOption) {

    var docText = $('<div/>').addClass("outerDivContainer");


    if (!model.data || (model.data.length == 0)) {
        if (queryParam && queryParam[0].length > 1) {
            docText.append(skuid.utils.mergeAsText("global","{{$Label.ATS_NO_ACTIVITIES_MATCH_RECORD}}"));
        } else if (dataObject === 'OpenActivities') {
            docText.append(skuid.utils.mergeAsText("global","{{$Label.ATS_NO_NEXT_STEP_NO_ACTIVITY}}"));
        } else {
            docText.append(skuid.utils.mergeAsText("global","{{$Label.ATS_NO_PAST_ACTIVITY}}"));
        }

    } else {
        renderModelDataActivity(model, templateParamsjson, divSectioinId, dataObject, loadMoreConstantValue, docText, queryParam, loadMoreOption);
    }

    if(dataObject === 'OpenActivities') {
        var hr = $('<hr/>');
        docText.append(hr);
    }

    var divSectin =  '#' + divSectioinId;
    $(divSectin).html(docText.html());

    //console.log("html cont: " + docText.html());
}


function renderModelDataActivity(model, templateParamsjson, divSectioinId, dataObject, loadMoreConstantValue, docText, queryParam, loadMoreOption) {


    var jsonVariables  =  convertJsonToArrayActivity(templateParamsjson);

    var displayListEvent =  jsonVariables["displayListEvent"];
    var displayListTask =   jsonVariables["displayListTask"];

    var itemListParentDivClassName =  jsonVariables["itemListParentDivClassName"];

    var excludeButtons =  jsonVariables["excludebuttons"];

    var ephemeralModel = jsonVariables["ephemeral_model"];

    var objectData = model.data;

    var hasMoreActivityData = false;

    if(hasMoreDataActivity(model.data.length, loadMoreConstantValue)) {
        hasMoreActivityData = true;
        //to find if we hae added mroe data we added one record to query limit so here we need to remove the last record
        objectData = objectData.slice(0, (objectData.length -1));
    }


    var parentUL =    $('<ul/>').addClass("slds-timeline");
    $.each(objectData,function(i,row){

        var iconSprite = getIconNameActivity(model.data[i].IsTask, model.data[i].ActivityType);
        var sprite =  iconSprite.split('.')[0];
        var icon = iconSprite.split('.')[1];
        var iconTimeLineColor = iconSprite.split('.')[2];
        var lineColor = iconSprite.split('.')[3];

        var divContainer = $('<div/>');

        var timeLineLi =  $('<li/>').addClass("slds-timeline__item");
        var div1 =        $('<div/>').addClass("slds-media timeline__container slds-grid slds-wrap");
        var div2 =        $('<div/>').addClass("slds-media__body timeline__middle-column slds-small-order--2 slds-medium-order--1 slds-large-order--1 timeline--width slds-max-small-order--2");
        var div3 =        $('<div/>').addClass("slds-media slds-media--timeline slds-timeline__media--".concat(lineColor));

        var div4 =        $('<div/>').addClass("slds-media__figure");

        var svg =         $('<svg/>').addClass("slds-icon ".concat(iconTimeLineColor).concat(" slds-timeline__icon"));
        $(svg).attr("aria-hidden", true);
        var use =         $('<use/>');
        $(use).attr("xlink:href", "/resource/LightningDesignSystem/assets/icons/".concat(sprite).concat("-sprite/svg/symbols.svg#".concat(icon)));


        svg.append(use);
        div4.append(svg);

        div3.append(div4);

        if (itemListParentDivClassName !== '') {
            $(divContainer).attr("class", itemListParentDivClassName);
        }

        var liStr = $('<div/>');

        if (model.data[i].IsTask) {
            div3.append(renderActivityType(model, row, displayListTask, model.data[i], ephemeralModel, dataObject));
        } else {
            div3.append(renderActivityType(model, row, displayListEvent, model.data[i], ephemeralModel, dataObject));
        }

        div2.append(div3);
        div1.append(div2);

        div1.append(renderTimeEditSectionActivity(model.data[i], excludeButtons));
        timeLineLi.append(div1);
        parentUL.append(timeLineLi);
        docText = docText.append(parentUL);
    });

    //console.log("docText: " + docText.html());

    //has more.. code , loadMoreOption
    if (loadMoreOption  === 'true' && hasMoreActivityData) {
        var hasMoreDiv =   $('<div/>');
        $(hasMoreDiv).attr("id", "hasMoreDivId");

        var loadonMoreParam =  model.id + "','" + dataObject + "','" +   queryParam[0] + "','" + queryParam[1]

        var  loadMoreText = "<a href=\"javascript:loadMoreActivity('" + loadonMoreParam +  "')\" >&nbsp&nbsp&nbspLoad " + skuid.utils.mergeAsText("global","{{$Label.ATS_MORE}}") + "...</a>";

        hasMoreDiv.append(loadMoreText);

        docText = docText.append(hasMoreDiv);

    }

}

function renderActivityType(model, row, activityList, modelData, ephemeralModel, dataObject) {

    var div5 =  $('<div/>').addClass("slds-media__body");
    var div6 =  $('<div/>').addClass("slds-media slds-tile slds-media--small");

    var div7 =  $('<div/>').addClass("slds-media__figure");

    var label = $('<label/>').addClass('slds-checkbox');

    var checkBoxIdValue = "mark-complete_".concat(modelData.Id);

    var input = $('<input/>');
    $(input).attr("name", "checkbox");
    $(input).attr("type", "checkbox");
    $(input).attr("id", checkBoxIdValue);

    var activityType = (modelData.IsTask ? "Task" : "Event");
    //need the name of the model that will get updated (not the tmeline model that is presistant)
    var ephemeralModelName  =  (modelData.IsTask ? ephemeralModel.task : ephemeralModel.event);
    //activty timeline checkbox method and params
    $(input).attr("onClick", "handleActivityCheckBox('".concat(ephemeralModelName.modelName).concat("','").concat(ephemeralModelName.conditionName).concat("','").concat(activityType).concat("','").concat(modelData.Id).concat("','").concat(checkBoxIdValue).concat("')"));


    var span2 =  $('<span/>').addClass("slds-checkbox--faux");
    var span3 =  $('<span/>').addClass("slds-form-element__label slds-assistive-text");
    span3.append("mark-complete");

    label.append(input);
    label.append(span2);
    label.append(span3);
    div7.append(label);

    div6.append(div7);

    var divList =       $('<div/>').addClass("slds-media__body");
    var pTag = $('<p/>').addClass("slds-tile__title slds-truncate");
    var childUL =    $('<ul/>').addClass("slds-tile__detail");

    var liStr =  $('<div/>');
    for (i = 0; i < activityList.length; i++) {

        var listItem  =  skuid.utils.merge('row',activityList[i].text,{},model, row).html();

        //title (subjet) is on the above UI/LI list not the first item of the list
        if (activityList[i].text.search("title") > -1) {
            pTag.append(listItem.split("title")[1]);
            listItem = '';
        }

        if(dataObject === 'OpenActivities' && modelData.IsTask
                   && (activityList[i].text.search("{{Completed__c}}") > -1) ) {
                    listItem = '';
        }

        //input jason data having requiresDateFormat means the date needs to be formated first
        if (activityList[i].text.search("requiresDateFormat") > -1) {
            listItem =  formateDateActivity(listItem, listItem.split("requiresDateFormat")[1]);
        }

        //if the json input date includde a text limit
        if (activityList[i].text.search("limitText") > -1) {
            var splits = listItem.split("limitText");
            var text  = limitStringLengthActivity(splits[1], splits[2], modelData.Id);

            var p = $('<p/>');

            var limitDiv = $('<div/>');
            limitDiv.attr("id", "limited" + modelData.Id);
            limitDiv.attr("style", "display: block");
            limitDiv.html(splits[0].concat(text));
            var fullDiv = $('<div/>');
            fullDiv.attr("id", "full" + modelData.Id);
            fullDiv.attr("style", "display: none");
            fullDiv.html(splits[0].concat(splits[1]));

            p.append(limitDiv).append(fullDiv);

            listItem = p.html();
        }

        //handles is all day events
        if (activityList[i].text.search("{{IsAllDayEvent}}") > -1) {
            if (modelData.IsTask || !modelData.IsAllDayEvent) {
                listItem = '';
            } else if (modelData.IsAllDayEvent) {
                listItem  = skuid.utils.mergeAsText("global","{{$Label.ATS_ALL_DAY_EVENT}}");
            }
        }

        if (listItem && listItem.length > 0) {
            var liTag =   $('<li/>');
            if (activityList[i].liClass && activityList[i].liClass.length > 0) {

                $(liTag).attr("class", activityList[i].liClass);
            }
            liTag.append(listItem);
        }
        liStr.append(liTag);
    }
    childUL.append(liStr);
    divList.append(pTag);
    divList.append(childUL);
    div6.append(divList);
    div5.append(div6);

    return div5;
}


function renderTimeEditSectionActivity(modelData, excludeButtons) {

    var div1Time = $('<div/>').addClass("slds-media__figure slds-media__figure--reverse timeline__right-column slds-small-order--1 slds-medium-order--2 slds-large-order--2 timeline--width slds-max-small-order--1");
    var div2Time = $('<div/>').addClass("slds-timeline__actions");
    var pTagTime = $('<p/>').addClass("slds-tile__title");


    div2Time.append(pTagTime);


    var activityDate;
    if (modelData.IsTask) {
        activityDate = modelData.ActivityDate;
    } else {
        activityDate = modelData.StartDateTime;
    }

    pTagTime.append(skuid.time.formatDate("M dd, yy", skuid.time.parseSFDate(activityDate)));

    if(!modelData.IsTask) {
        var hourMinVar = skuid.time.formatTime("h:mm", activityDate);
        var pmamVar = skuid.time.formatTime("a", activityDate);

        var pTagTimeHour = $('<p/>').addClass("slds-tile__title timeMonthDayYear");
        pTagTimeHour.append(hourMinVar).append(" ").append(pmamVar);

        //var pTagTimeAMPM = $('<p/>').addClass("slds-tile__title");
        //pTagTimeAMPM.append(pmamVar);

        div2Time.append(pTagTimeHour);
        //div2Time.append(pTagTimeAMPM);
    }

    if ((!excludeButtons) || (  excludeButtons && (excludeButtons !==  "true")))  {
        div2Time.append(createEditDeleteButtonforTimeLine(skuid.utils.mergeAsText("global","{{$Label.ATS_EDIT}}"), "slds-button slds-button--brand", modelData, 'editActivityFromTimeLine'));
        div2Time.append(createEditDeleteButtonforTimeLine(skuid.utils.mergeAsText("global","{{$Label.ATS_DELETE_BUTTON}}"), "slds-button slds-button--neutral", modelData,  'deleteActivityFromTimeLine'));
    }


    div1Time.append(div2Time);


    return div1Time;

}

function createEditDeleteButtonforTimeLine(actionName, className, modelData, fName){

    var button = $('<button/>').addClass(className);

    var functionName = fName.concat("('");
    if (modelData.IsTask) {
        functionName = functionName.concat("task");
    } else {
        functionName = functionName.concat("event");
    }
    functionName = functionName.concat("','").concat(modelData.Id).concat("')");

    $(button).attr('onclick', functionName);

    button.append(actionName);

    return button;
}


function formateDateActivity(fieldStr, dateStr) {

    if (dateStr) {
        return  fieldStr.split("requiresDateFormat")[0].concat(skuid.time.formatDate(" M dd, yy ", dateStr).concat(skuid.time.formatTime(" h:mm a", dateStr)));
    } else {
        return fieldStr.split("requiresDateFormat")[0];
    }
}



function limitStringLengthActivity(strValue, len, id) {

    if (strValue && strValue.length > len) {
        return   strValue.substring(0, (len - 1)) + " ... <a onclick=\"document.getElementById('limited" + id + "').style.display = 'none'; document.getElementById('full" + id + "').style.display = 'block'; \">READ MORE</a>";
    }
    return strValue;

}


function getIconNameActivity(isTask, activity) {

    var iconName = '';

    if (isTask) {
        switch (activity) {
            case "Attempted Contact":
            iconName= 'custom.custom6.slds-icon-standard-task.task';
            break;
            case "BizDev  Call":
            iconName= 'standard.call.slds-icon-standard-log-a-call.call';
            break;
            case "Maintenance Call":
            iconName= 'standard.call.slds-icon-standard-log-a-call.call';
            break;
            case "Tickler Call":
            iconName= 'standard.call.slds-icon-standard-log-a-call.call';
            break;
            case "Correspondence":
            iconName= 'custom.custom18.slds-icon-standard-task.task';
            break;
            case "Email":
            iconName= 'standard.email.slds-icon-standard-email.email';
            break;
            case "To Do":
            iconName= 'action.new_task.slds-icon-standard-task.task';
            break;
            default:
            iconName= 'standard.task.slds-icon-standard-task.task';
        }
    } else {
        switch (activity) {
            case "Meeting":
            iconName = 'standard.event.slds-icon-standard-event.event';
            break;
            case "Breakfast":
            iconName = 'custom.custom51.slds-icon-standard-event.event';
            break;
            case "Lunch":
            iconName = 'custom.custom51.slds-icon-standard-event.event';
            break;
            case "Dinner":
            iconName = 'custom.custom51.slds-icon-standard-event.event';
            break;
            case "Out of Office":
            iconName = 'custom.custom24.slds-icon-standard-event.event';
            break;
            case "Networking":
            iconName = 'standard.social.slds-icon-standard-event.event';
            break;
            case "Contractor Meeting":
            iconName = 'custom.custom14.slds-icon-standard-event.event';
            break;
            case "Other":
            iconName = 'action.log_event.slds-icon-standard-event.event';
            break;
            default:
            iconName = 'standard.event.slds-icon-standard-event.event';
        }
    }
    return iconName;

}


function getQueryActivity(queryType) {
    var queryStr;
    switch (queryType) {
        case "activity":
        queryStr =       ' SELECT  ( SELECT Id, ActivityDate, StartDateTime, EndDateTime, Pre_Meeting_Notes__C, ActivityType, Description, IsAllDayEvent, ' +
        ' IsClosed, IsTask, Location, Priority, Status, Subject, Completed__c FROM dataObjectToken ' +
        ' ORDER BY ActivityDate sortOrderToken NULLS LAST, LastModifiedDate DESC LIMIT loadMoreToken  ) ' +
        'FROM Account WHERE id = ';
        break;

        case "activityFilter":
        queryStr =           ' SELECT  ( SELECT Id, ActivityDate, StartDateTime, EndDateTime, Pre_Meeting_Notes__C, ActivityType, Description, IsAllDayEvent, ' +
        ' IsClosed, IsTask, Location, Priority, Status, Subject, Completed__c FROM dataObjectToken where subQueryfilterByToken ' +
        ' ORDER BY ActivityDate sortOrderToken NULLS LAST, LastModifiedDate DESC LIMIT loadMoreToken  ) ' +
        'FROM Account WHERE id = ';
        break;

        default:
        queryStr =      ' SELECT  ( SELECT Id, ActivityDate, StartDateTime, EndDateTime, Pre_Meeting_Notes__C, ActivityType, Description, IsAllDayEvent, ' +
        ' IsClosed, IsTask, Location, Priority, Status, Subject, Completed__c FROM dataObjectToken ' +
        ' ORDER BY ActivityDate sortOrderToken NULLS LAST, LastModifiedDate DESC LIMIT loadMoreToken  ) ' +
        'FROM Account WHERE id = ';
    }
    return queryStr;
}

function startsWith(string, pattern) {
	return string.slice(0, pattern.length) === pattern;
}

function  convertJsonToArrayActivity(json) {

    var jsonVariables = new Object();

    var arrEvent = [];
    var arrTask  = [];

    var parsed = JSON.parse(json);

    for(var x in parsed){

        if(startsWith(x, "event")) {
            arrEvent.push(parsed[x]);

        } else if(startsWith(x, "task")) {
            arrTask.push(parsed[x]);

        } else if(startsWith(x, "PDC")) {
            jsonVariables["itemListParentDivClassName"] = parsed[x];

        } else if(startsWith(x, "excludebuttons")) {
            jsonVariables["excludebuttons"] = parsed[x];

        }  else if(startsWith(x, "ephemeral_model")) {
            jsonVariables["ephemeral_model"] = parsed[x];

        } else if(startsWith(x, "presistence_model")) {
            jsonVariables["presistence_model"] = parsed[x];
        }

    }

    jsonVariables["displayListEvent"] = arrEvent;
    jsonVariables["displayListTask"]  = arrTask;

    return jsonVariables;
}



function loadMoreActivity(modelName, activityType, queryFilter, querySortOrder) {

    var queryData = [getQueryParamActivity(queryFilter, querySortOrder)];

    if (activityType === 'ActivityHistories') {
        skuid.events.publish("ActivityHistories", queryData);
    } else {
        skuid.events.publish("OpenActivities", queryData);
    }
}


function filterOnActivities(eventName, queryFilter, querySortOrder) {

    var arrayData = [getQueryParamActivity(queryFilter, querySortOrder)];

    skuid.events.publish(eventName, arrayData);

}

function onAddEditActivityQueryModeles(eventName, queryFilter, querySortOrder) {

    var arrayData = [getQueryParamActivity(queryFilter, querySortOrder)];

    skuid.events.publish(eventName, arrayData);

}

function creatContainerActivity(domElement) {

    var parentDiv = $('<div/>');

    var openActivityDiv = $("<div/>").addClass('slds-text-heading--small slds-m-top--medium slds-m-bottom—medium');
    openActivityDiv.append(skuid.utils.mergeAsText("global","{{$Label.ATS_NEXT_STEPS}}"));

    var openActivityTimeline = $("<div/>");
    $(openActivityTimeline).attr("id", "openActivityDivId");

    parentDiv.append(openActivityDiv);
    parentDiv.append(openActivityTimeline);

    var PastctivityDiv = $("<div/>").addClass('slds-text-heading--small slds-m-top--medium slds-m-bottom—medium');
    PastctivityDiv.append(skuid.utils.mergeAsText("global","{{$Label.ATS_PAST_ACTIVITY}}"));

    var pastctivityDivTimeline = $("<div/>");
    $(pastctivityDivTimeline).attr("id", "PastActivityDivId");

    parentDiv.append(PastctivityDiv);
    parentDiv.append(pastctivityDivTimeline);

    domElement.html(parentDiv.html());

}


function hasMoreDataActivity(int1, int2) {

    if (int2 > int1 || int2 == int1) {
        return false;
    } else {
        if ((int2 == 1) && (int1 > int2)) {
            return true;
        } else if ((int1 % int2) != 0) {
            return true;
        } else {
            return false;
        }
    }

}


//do not earse static resrouce used in page Snippet
function getQueryParamActivity(queryFilter, querySortOrder) {
    return  [queryFilter, querySortOrder];
}

function addSingleQtoSubQueryActivity(subQuery) {

    var queryTerms = subQuery.split(',');

    var subQ = '';

    for (i=0; i < queryTerms.length; i++) {

        subQ = subQ +   "'".concat(queryTerms[i].concat("', "));

    }

    if (subQ.length > 1)  {
        subQ = subQ.substring(0, (subQ.length-2));
    }

    return subQ;


}

function getQueryLimitActivity(modelLength, constantValue, notOnLoadMore) {

    var queryLimit = 1;

    if (modelLength <= constantValue) {
        queryLimit = constantValue;
    } else {
        if (notOnLoadMore) {
            queryLimit = constantValue;
        } else {
            queryLimit = (modelLength -1) + constantValue;
        }
    }
    return queryLimit;

}


function callEditDeletePopupActivity(modelName, modelConditionName, recordId, popupXMLString) {

    var popupXML = skuid.utils.makeXMLDoc(popupXMLString);

    var activityModel = skuid.model.getModel(modelName);

    activityModel.emptyData();

    var condition = activityModel.getConditionByName(modelConditionName);
    activityModel.cancel();
    activityModel.deactivateCondition(condition);
    activityModel.setCondition(condition, recordId);
    activityModel.activateCondition(condition);

    activityModel.updateData(function() {

        var row = activityModel.getFirstRow();

        var context = {
            row: activityModel
        };
        var popup = skuid.utils.createPopupFromPopupXML(popupXML,context);

        // Replace ui-button class with slds-button class
        var $buttons = $(popup).find(".ui-button");
        $($buttons[0]).removeClass("ui-button").addClass("slds-button slds-button--neutral");
        $($buttons[1]).removeClass("ui-button").addClass("slds-button slds-button--brand");
    });

}

function handleActivityCheckBox(modelName, conditionName, activityType, recordId, elementId) {

    var popupStr = getActivityCheckBoxPopStr(activityType);

    var popupXML = skuid.utils.makeXMLDoc(popupStr.replace(/ActivityModeReplacelToken/g, modelName));

    var CasesModel = skuid.model.getModel(modelName);

    var condition = CasesModel.getConditionByName(conditionName);
    CasesModel.cancel();
    CasesModel.deactivateCondition(condition);
    CasesModel.setCondition(condition, recordId);
    CasesModel.activateCondition(condition);

    CasesModel.updateData(function() {
        var row = CasesModel.getRowById(recordId);
        var context = {
            row: CasesModel
        };
        var popup = skuid.utils.createPopupFromPopupXML(popupXML,context);

        // Replace ui-button class with slds-button class
        var $buttons = $(popup).find(".ui-button");
        $($buttons[0]).removeClass("ui-button").addClass("slds-button slds-button--neutral");
        $($buttons[1]).removeClass("ui-button").addClass("slds-button slds-button--brand");

        toggleCheckBoxValue(elementId, false);
    });


}


function toggleCheckBoxValue(elementId, checkBoxValue) {
     var  taskElementId = "#".concat(elementId);
     $(taskElementId).prop('checked', checkBoxValue); // Unchecks it
}


function getActivityCheckBoxPopStr(type) {

    if (type == "Task") {
       return   "<popup title=\"{{$Label.ACTIVITY_NOTES_ATS}}\" width=\"90%\">"
                                  + "<components>"
                                     + "<wrapper uniqueid=\"sk-286dZJ-572-activityCheckBoxPopup\" cssclass=\"checkBoxPopupWrapperClassName\">"
                                        + "<components>"
                                        + "<template multiple=\"false\" uniqueid=\"sk-YoeqQ-382\" allowhtml=\"true\"  cssclass=\"checkBoxPopupTemplateClassName\">"
                                             + "<contents>{{$Label.ATS_LEAVE_COMMENTS_TO_COMPELETE_TASK}}</contents>"
                                           + "</template>"
                                           + "<basicfieldeditor showheader=\"true\" showsavecancel=\"false\" showerrorsinline=\"false\" model=\"ActivityModeReplacelToken\" buttonposition=\"\" uniqueid=\"sk-287ANm-580-activityCheckBoxPopup\" mode=\"edit\" layout=\"above\">"
                                              + "<columns layoutmode=\"responsive\" columngutter=\"1px\" rowgutter=\"1px\">"
                                                 + "<column behavior=\"flex\" ratio=\"1\" minwidth=\"100%\" verticalalign=\"top\">"
                                                    + "<sections>"
                                                       + "<section title=\"New Section\" collapsible=\"no\" showheader=\"false\">"
                                                          + "<fields>"
                                                             + "<field id=\"Status\" valuehalign=\"\" type=\"\">"
                                                                + "<label>{{$Label.ATS_STATUS}}</label>"
                                                             + "</field>"
                                                          + "</fields>"
                                                       + "</section>"
                                                       + "<section title=\"New Section\" collapsible=\"no\" showheader=\"false\">"
                                                          + "<fields/>"
                                                       + "</section>"
                                                    + "</sections>"
                                                 + "</column>"
                                                 + "<column behavior=\"flex\" ratio=\"1\" minwidth=\"100%\" verticalalign=\"top\">"
                                                    + "<sections>"
                                                       + "<section title=\"New Section\" collapsible=\"no\" showheader=\"false\">"
                                                          + "<fields>"
                                                             + "<field id=\"Description\" valuehalign=\"\" type=\"\" displayrows=\"4\">"
                                                                + "<label>{{$Label.ATS_COMMENT}}</label>"
                                                             + "</field>"
                                                          + "</fields>"
                                                       + "</section>"
                                                    + "</sections>"
                                                 + "</column>"
                                              + "</columns>"
                                           + "</basicfieldeditor>"
                                           + "<wrapper uniqueid=\"sk-28IwWN-366-activityCheckBoxPopup\">"
                                              + "<components>"
                                                 + "<pagetitle uniqueid=\"sk-28I-Ia-378-activityCheckBoxPopup\" model=\"ActivityModeReplacelToken\">"
                                                    + "<actions>"
                                                       + "<action type=\"multi\"  label=\"{{$Label.ATS_CANCEL_BUTTON}}\" cssclass=\"slds-button slds-button--neutral\">"
                                                          + "<actions>"
                                                             + "<action type=\"cancel\">"
                                                                + "<models>"
                                                                   + "<model>ActivityModeReplacelToken</model>"
                                                                + "</models>"
                                                             + "</action>"
                                                             + "<action type=\"emptyModelData\">"
                                                                + "<models>"
                                                                   + "<model>ActivityModeReplacelToken</model>"
                                                                + "</models>"
                                                             + "</action>"
                                                             + "<action type=\"closeAllPopups\"/>"
                                                          + "</actions>"
                                                       + "</action>"
                                                        + "<action type=\"multi\" label=\"{{$Label.ATS_BUTTON_SAVE}}\" cssclass=\"slds-button slds-button--brand\" uniqueid=\"saveActivityModeReplacelTokenButtonDomId\">"
                                                               + "<actions>"
                                                                   + "<action type=\"publish\" event=\"onSaveCheckBoxPopupTaskValidateFields\"/>"
                                                               + "</actions>"
                                                               + "<renderconditions logictype=\"and\"/>"
                                                               + "<enableconditions/>"
                                                               + "<hotkeys/>"
														+ "</action>"
                                                    + "</actions>"
                                                 + "</pagetitle>"
                                              + "</components>"
                                              + "<styles>"
                                                 + "<styleitem type=\"background\"/>"
                                                 + "<styleitem type=\"border\"/>"
                                                 + "<styleitem type=\"size\"/>"
                                              + "</styles>"
                                           + "</wrapper>"
                                        + "</components>"
                                        + "<styles>"
                                           + "<styleitem type=\"background\"/>"
                                           + "<styleitem type=\"border\"/>"
                                           + "<styleitem type=\"size\"/>"
                                        + "</styles>"
                                     + "</wrapper>"
                                     + "<wrapper uniqueid=\"trashbin-popupCheckbox\">"
                                           + "<components/>"
                                           + "<styles>"
                                              + "<styleitem type=\"background\"/>"
                                              + "<styleitem type=\"border\"/>"
                                              + "<styleitem type=\"size\"/>"
                                           + "</styles>"
                                           + "<renderconditions logictype=\"and\"/>"
                                        + "</wrapper>"
                                  + "</components>"
                               + "</popup>";
    } else {

        return  "<popup title=\"{{$Label.ACTIVITY_NOTES_ATS}}\" width=\"90%\">"
                                       + "<components>"
                                          + "<wrapper uniqueid=\"sk-2HHR9R-384-TaskCheckboxPopup\">"
                                             + "<components>"
                                                + "<template multiple=\"false\" uniqueid=\"sk-n_RrT-473-TaskCheckboxPopup\">"
                                                   + "<contents>{{$Label.ATS_LEAVE_COMMENTS_TO_COMPELETE_EVENT}}</contents>"
                                                + "</template>"
                                                + "<wrapper uniqueid=\"sk-n_G0g-456-TaskCheckboxPopup\">"
                                                   + "<components>"
                                                      + "<basicfieldeditor showheader=\"true\" showsavecancel=\"false\" showerrorsinline=\"false\" model=\"ActivityModeReplacelToken\" buttonposition=\"\" uniqueid=\"sk-2HHToo-392-TaskCheckboxPopup\" mode=\"edit\" layout=\"above\">"
                                                         + "<columns layoutmode=\"responsive\" columngutter=\"4px\" rowgutter=\"4px\">"
                                                            + "<column ratio=\"1\" minwidth=\"100%\" behavior=\"flex\" verticalalign=\"top\">"
                                                               + "<sections>"
                                                                  + "<section title=\"New Section\" collapsible=\"no\" showheader=\"false\">"
                                                                     + "<fields>"
                                                                        + "<field id=\"Description\" valuehalign=\"\" type=\"\" displayrows=\"4\">"
                                                                           + "<label>{{$Label.ATS_POST_MEETING}}</label>"
                                                                        + "</field>"
                                                                     + "</fields>"
                                                                  + "</section>"
                                                               + "</sections>"
                                                            + "</column>"
                                                         + "</columns>"
                                                      + "</basicfieldeditor>"
                                                      + "<pagetitle uniqueid=\"sk-2HKWXa-473-TaskCheckboxPopup\" model=\"ActivityModeReplacelToken\">"
                                                         + "<actions>"
                                                            + "<action type=\"multi\" label=\"{{$Label.ATS_CANCEL_BUTTON}}\" cssclass=\"slds-button slds-button--neutral\">"
                                                               + "<actions>"
                                                                  + "<action type=\"cancel\">"
                                                                     + "<models>"
                                                                        + "<model>ActivityModeReplacelToken</model>"
                                                                     + "</models>"
                                                                  + "</action>"
                                                                  + "<action type=\"emptyModelData\">"
                                                                     + "<models>"
                                                                        + "<model>ActivityModeReplacelToken</model>"
                                                                     + "</models>"
                                                                  + "</action>"
                                                                  + "<action type=\"closeAllPopups\"/>"
                                                               + "</actions>"
                                                            + "</action>"
                                                            + "<action type=\"multi\"  label=\"{{$Label.ATS_BUTTON_SAVE}}\" cssclass=\"slds-button slds-button--brand\" uniqueid=\"saveEventButtonDomId\">"
                                                               + "<actions>"
                                                                  + "<action type=\"publish\" event=\"onSaveCheckBoxPopupEventValidateFields\"/>"
                                                               + "</actions>"
                                                            + "</action>"
                                                         + "</actions>"
                                                      + "</pagetitle>"
                                                   + "</components>"
                                                   + "<styles>"
                                                      + "<styleitem type=\"background\"/>"
                                                      + "<styleitem type=\"border\"/>"
                                                      + "<styleitem type=\"size\"/>"
                                                   + "</styles>"
                                                + "</wrapper>"
                                             + "</components>"
                                             + "<styles>"
                                                + "<styleitem type=\"background\"/>"
                                                + "<styleitem type=\"border\"/>"
                                                + "<styleitem type=\"size\"/>"
                                             + "</styles>"
                                          + "</wrapper>"
                                          + "<wrapper uniqueid=\"trashbin-popupCheckbox\">"
                                                + "<components/>"
                                                + "<styles>"
                                                   + "<styleitem type=\"background\"/>"
                                                   + "<styleitem type=\"border\"/>"
                                                   + "<styleitem type=\"size\"/>"
                                                + "</styles>"
                                                + "<renderconditions logictype=\"and\"/>"
                                             + "</wrapper>"
                                       + "</components>"
                                    + "</popup>";
    }

}


function getQueryFilterTimeLineActivity() {
    var selectedMapTask = getSelectedCheckboxValues("tasks");
    var selectedMapEvent = getSelectedCheckboxValues("events");

    var selectedValues = '';

    if (selectedMapTask["allValuesSelected"] && selectedMapEvent["allValuesSelected"]) {
      selectedValues = '';
    } else {
      if ((selectedMapTask["selectedValues"].length > 0) && (selectedMapEvent["selectedValues"].length > 0)) {
           selectedValues =  selectedMapTask["selectedValues"].concat(",").concat(selectedMapEvent["selectedValues"]);
      } else if (selectedMapTask["selectedValues"].length > 0) {
          selectedValues =  selectedMapTask["selectedValues"];
      } else if (selectedMapEvent["selectedValues"].length > 0) {
          selectedValues =  selectedMapEvent["selectedValues"];
      }
    }
  console.log("selectedValues: " + selectedValues);
  return selectedValues;
}


function renderTimelineonCheckboxUpdatePopup() {
    filterOnActivities('filterOnActivity',  getQueryFilterTimeLineActivity(), 'DESC');
}


function onCheckboxPopupSaveModels(modelName, activityType) {

    var CasesModel = skuid.model.getModel(modelName);

    //use a "trashbin-popupCheckbox" element to suppress standard errors
    var editor = new skuid.ui.Editor($("#trashbin-popupCheckbox"));
    editor.registerModel(CasesModel);
    var editorId = editor.id();

    if (activityType == 'Task' && CasesModel.data &&  CasesModel.originals[CasesModel.data[0].Id]) {
        var originalStatus =  CasesModel.originals[CasesModel.data[0].Id].Status;
        var newStatus =  CasesModel.data[0].Status;
        if (originalStatus === "Completed" && newStatus !== 'Completed') {
            var row =  CasesModel.getRowById(CasesModel.data[0].Id);
             CasesModel.updateRow(row, {Completed_Flag__C: false});
        }
    }

    skuid.model.save([CasesModel],{callback: function(result){
        if (result.totalsuccess){
            filterOnActivities('filterOnActivity',  getQueryFilterTimeLineActivity(), 'DESC');
            CasesModel.abandonAllRows();
            skuid.$('.ui-dialog-content').last().dialog('close');
        }
    }, initiatorId: editorId

    });
}

function activeModelCondtionsActivityTimeLine(modelName, jqueryParam) {

    var jsonVar  =  convertJsonToArrayActivity(jqueryParam);
    var conditionNames =  jsonVar["presistence_model"];

    var model = skuid.model.getModel(modelName);

    //set condition or the query parameters
    model.activateCondition(model.getConditionByName(conditionNames.conditionname.recordid));
    model.activateCondition(model.getConditionByName(conditionNames.conditionname.dataobject));
    model.activateCondition( model.getConditionByName(conditionNames.conditionname.loadMore));
    model.activateCondition(model.getConditionByName(conditionNames.conditionname.sortorder));
    model.activateCondition(model.getConditionByName(conditionNames.conditionname.queryfilter));
}
