(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__timelinev2",
		name: "Time_Line v2",
		icon: "sk-icon-page-edit",
		description: "Time_Line Componet v2",
		componentRenderer: function (component) {

	        var r = skuid.builder.getBuilders().wrapper.componentRenderer;

	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },
		propertiesRenderer: function (propertiesObj, component) {

	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('Time Line Properties');


	       var propsList = [
				{
				    id: "timeLineModelId",
				    type: "model",
				    label: "Time Line Modle",
					required : true,
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "templateParamsjsonId",
				    type: "template",
					modelprop : "timeLineModelId",
					required : true,
				    label: "Time Line list Item key/value json format",
					helptext: '{\"line1\":{\"textline\":\"{{{Title__c}}}|{{{Salary__c}}}|{{{Organization_Name__c}}}\",\"fieldproperty\":{\"field1\":{\"fieldname\":\"Salary__c\",\"isrequired\":\"true\",\"requirefieldlabel\":\"Salary\",\"maxlength\":\"5\"}}},\"addinfobutton\":{\"classname\":\"edittimelineclassName\",\"methodname\":\"editTimeLineMethod\",\"methodparam\":\"123\",\"labelname\":\"AddInfo\"},\"editbutton\":{\"classname\":\"edittimelineclassName\",\"methodname\":\"editTimeLineMethod\",\"methodparam\":\"123\",\"labelname\":\"Edit\"},\"deletebutton\":{\"classname\":\"edittimelineclassName\",\"methodname\":\"editTimeLineMethod\",\"methodparam\":\"123\",\"labelname\":\"Delete\"}}',

					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
				{
				    id: "renderTimeLineEventId",
				    type: "string",
					required : true,
				    label: "Re-render model event call",
				    helptext: "Provide a UID so you can call the publish event in case to Re-render the TimeLine for save/edit/delete, example: callRegisterEventTimeLine('renderTimeLineEvent-UID')",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
				{
				    id: "loadMoreEventId",
				    type: "string",
					required : true,
				    label: "Load More event call",
				    helptext: "Provide a UID so you can call the publish event in case to have a load more for each TimeLine",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				},
				{
				    id: "loadMoreOptionId",
				    type: "boolean",
					required : true,
				    label: "Include a Load 'More..' option ",
					onChange: function(){
                           component.refresh();
                    },
                    location: "attribute"
				}
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propsList,
	        });
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);


	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__timelinev2"));
	    }

	}));


})(skuid);
