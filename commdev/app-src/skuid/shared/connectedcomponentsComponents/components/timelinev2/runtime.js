var $ = skuid.$;

(function(skuid){

	skuid.componentType.register("connectedcomponents__timelinev2",function(domElement, xmlConfig, component){

		var modelName = xmlConfig.attr('timeLineModelId');
		var templateParamsjson =  xmlConfig.attr('templateParamsjsonId');
		var loadMoreOption     =  xmlConfig.attr('loadMoreOptionId');
		var renderTimeLineEvent     =  xmlConfig.attr('renderTimeLineEventId');
		var loadMoreEventId     =  xmlConfig.attr('loadMoreEventId');

        svg4everybody();

		var model = skuid.model.getModel(modelName);

		var ui =  skuid.utils.generateGUID();



		renderTimeLineV2(model, templateParamsjson, domElement, loadMoreOption, loadMoreEventId, ui);

		 //will get invoked when click on "More"
		skuid.events.subscribe(loadMoreEventId,function(loadedResult){
            if (model.canRetrieveMoreRows) {
        	        model.loadNextOffsetPage(function () {
                         skuid.events.publish(renderTimeLineEvent);
                    });
            }
        });

	    //will get invoked when there is a model save/update/delete
	    skuid.events.subscribe(renderTimeLineEvent,function(saveResult){
                    var eventName = renderTimeLineEvent;
                    renderTimeLineV2(model,
                                    templateParamsjson,
                                    domElement,
                                    loadMoreOption,
                                    loadMoreEventId,
                                    ui);
        });

	});

})(skuid);


function renderTimeLineV2(model, templateParamsjson, domElement, loadMoreOption, loadMoreEventId, ui) {

    console.log("TimeLine renders ! ");

         var jsonVariables  =  convertJsonToArrayV2(templateParamsjson);

	 	 var displayList =  jsonVariables["displayList"];
	     var ParentClassName =  jsonVariables["ParentClassName"];
	     var addInfoButton =  jsonVariables["AddInfoButton"];
	     var editButtonRequired =   jsonVariables["EditButtonRequired"];
	     var deleteButtonRequired =   jsonVariables["DeleteButtonRequired"];

		var docText = $('<div/>').addClass("outerDivContainer");

		if (model && model.data && model.data.length > 0) {
            var divContainer = $('<div/>');
            var parentUL =    $('<ul/>').addClass("slds-timeline");
            if (ParentClassName !== '') {
                $(divContainer).attr("class", ParentClassName);
            }

          $.each(model.data,function(i,row){

                var parentLi =    $('<li/>').addClass("slds-timeline__item");
                var div1 =        $('<div/>').addClass("slds-media timeline__container slds-grid slds-wrap");
                var div2 =        $('<div/>').addClass("slds-media__body timeline__middle-column slds-small-order--2 slds-medium-order--1 slds-large-order--1 timeline--width slds-max-small-order--2");
                var div3 =        $('<div/>').addClass("slds-media slds-media--timeline slds-timeline__media--email");
                var div4 =        $('<div/>').addClass("slds-media__figure");
                var svg =         $('<svg/>').addClass("slds-icon slds-icon-text-default");
                $(svg).attr("aria-hidden", true);
                var use =         $('<use/>');
                $(use).attr("xlink:href", "/resource/LightningDesignSystem/assets/icons/utility-sprite/svg/symbols.svg#record");
                var div5 =        $('<div/>').addClass("slds-media__body timeline__text");

                var childUL =    $('<ul/>').addClass("slds-tile__detail");

                var liStr =  getListRowTimeLineV2(model, row, i, displayList, addInfoButton);

                div4.append(svg.append(use));
                div5.append(childUL.append(liStr));

                div3.append(div4);
                div3.append(div5);

                div2.append(div3);
                div1.append(div2);

                parentLi.append(div1);

                docText = docText.append(divContainer.append(parentUL.append(parentLi)));

                div1.append((editButtonRequired || deleteButtonRequired) ?  addEditDeleteButtonTimelineV2(editButtonRequired, deleteButtonRequired, model, row) : "");


        });
                 docText = docText.append(renderLoadMoreV2(loadMoreOption, model, loadMoreEventId, ui));
        } else {
                 docText = docText.append(skuid.utils.mergeAsText("global","{{$Label.ATS_THERE_NO_MORE_RECORDS}}"));
        }

        domElement.html(docText.html());

        }



function  convertJsonToArrayV2(json) {

    var jsonVariables = new Object();
    var arr = [];
	var parsed = JSON.parse(json);

	for(var x in parsed) {
		if(startsWithTimelineV2(x.toLowerCase(), "line")) {
		   arr.push(parsed[x]);
		}
		else if(startsWithTimelineV2(x.toLowerCase(), "parentclassname")) {
		    jsonVariables["ParentClassName"] = parsed[x];
	    } else if(startsWithTimelineV2(x.toLowerCase(), "addinfobutton")) {
		    jsonVariables["AddInfoButton"] = parsed[x];
	    } else if(startsWithTimelineV2(x.toLowerCase(), "editbutton")) {
		    jsonVariables["EditButtonRequired"] = parsed[x];
	    } else if(startsWithTimelineV2(x.toLowerCase(), "deletebutton")) {
		    jsonVariables["DeleteButtonRequired"] = parsed[x];
	    }
	}

    jsonVariables["displayList"] = arr;

	return jsonVariables;
 }

 function startsWithTimelineV2(string, pattern) {
 	return string.slice(0, pattern.length) === pattern;
 }


 function createEditDeleteButtonforTimeLineV2(buttonProp, model, row){
   var divButton = $('<div/>').addClass("divbuttonClass");;
   var button = $('<button/>').addClass(buttonProp.classname);
   $(button).attr(buttonProp.attributeName,  skuid.utils.mergeAsText('row',buttonProp.attributeValue,{},model, row));
   button.append(skuid.utils.mergeAsText("global",buttonProp.labelname));
   divButton.append(button);
   return divButton;
}





function renderLoadMoreV2(loadMoreOption, model, loadMoreEventId, ui) {

    var hasMoreDiv =   $('<div/>');

	if(loadMoreOption === 'true' &&  model.canRetrieveMoreRows) {

		var onMoreDivId = "hasMoreDivId".concat(ui);
			$(hasMoreDiv).attr("id", onMoreDivId);

        var hrefLink = "javascript:callRegisterEventTimeLine('"  + loadMoreEventId +  "')";
		var onMoreLink = $("<a/>");
			$(onMoreLink).attr("href", hrefLink);
			$(onMoreLink).append(skuid.utils.mergeAsText("global","{{$Label.ATS_LOAD_MORE}}"));
        hasMoreDiv.append(onMoreLink);
	}

	return hasMoreDiv;
}




function callRegisterEventTimeLine(eventName) {
   skuid.events.publish(eventName);
 }


 function isNotEmpty(strVal) {
 	return (strVal ? true : false);
 }


 function getListRowTimeLineV2(model, row, i,  displayList, addInfoButton) {

                var liStr =  $('<div/>');

                $.each(displayList, function(k, lineinfo) {

                        var isRequiredFieldMissing = false;
                        var listItemVal = lineinfo.textline;

                     if(lineinfo.fieldproperty) {
                        //in case of any rules to be applied (ex: maxLength, required field),
                	    $.each(lineinfo.fieldproperty, function(n,field){

							var fieldName = "{{{".concat(field.fieldname).concat("}}}");
							var fieldValue = eval("model.data[i]." + field.fieldname);

                            //if field is in the textline
                            if (listItemVal.search(fieldName) >= 0) {
                            		//required rule
                            		if (isPropertyExistAndTrue(field.isrequired) && (!isNotEmpty(fieldValue))) {
                            			  var missingFieldDiv = $('<div/>').append(($('<span/>').addClass("missingFieldClassName")).append(field.requirefieldlabel));
		                                  listItemVal = listItemVal.replace(fieldName, missingFieldDiv.html());
		                                  isRequiredFieldMissing = true;
                            		}
                            		//maxlength rule
                            		if (field.maxlength && fieldValue && (fieldValue > parseInt(field.maxlength))) {
                            			   listItemVal = listItemVal.replace(fieldName, fieldValue.toString().substring(0, parseInt(field.maxlength)));
                            		}
                            }
                        });
					}

                        liStr.append($('<li/>').append( skuid.utils.merge('row',listItemVal,{},model, row).html()));

                        if (addInfoButton && isRequiredFieldMissing) {
                	 		  liStr.prepend(createEditDeleteButtonforTimeLineV2(addInfoButton, model, row));
               			 }
                });

				/**
				 * HACK: Forces the content into a single line, because skuid decided to give us extra HTML we didn't
				 * ask for
				 */
				liStr.children("li").children("div.titleLink.itemList").children("div").css("display", "inline-block");

	           return liStr;
 }

function isPropertyExistAndTrue(property) {

  return ((property && property === 'true')	? true : false);

 }


 function addEditDeleteButtonTimelineV2(editButtonRequired, deleteButtonRequired, model, row) {

	    var div = $('<div/>').addClass("slds-timeline__actions");

	     div.append(editButtonRequired   ? createEditDeleteButtonforTimeLineV2(editButtonRequired, model, row) : "");
         //div.append("<br/><br/>");
	     div.append(deleteButtonRequired ? createEditDeleteButtonforTimeLineV2(deleteButtonRequired, model, row) : "");

	    return  ($('<div/>').addClass("slds-media__figure slds-media__figure--reverse timeline__right-column slds-small-order--1 slds-medium-order--2 slds-large-order--2 timeline--width slds-max-small-order--1")).append(div);

 }
