(function($) {
	skuid.builder.core.registerBuilder(new skuid.builder.core.Builder({
		id: "connectedcomponents__xmlinclude",
		name: "XML Include",
		icon: "sk-icon-page-edit",
		description: "XML Include allows the reuse of a page or partial page by loading Skuid XML from a static resource.",
		componentRenderer: function (component) {
			//console.log(arguments);

	        var r = skuid.builder.getBuilders().wrapper.componentRenderer;
	        //console.log(r);
	        r.apply(this,arguments);
	        var self = component;
	        component.header.html(component.builder.name);
	    },	
		propertiesRenderer: function (propertiesObj, component) {
	        
	        var r = skuid.builder.getBuilders().wrapper.propertiesRenderer;
	        r.apply(this, arguments);
	        var propCategories = propertiesObj.catObj;
	        propertiesObj.setHeaderText('XML Include Properties');


	        var propsList = [
				{
				    id: "resourcepath",
				    type: "string",
				    label: "XML Static Resource Path",
				    helptext: "Relative path to the static resource containing the Skuid XML. (e.g. /resource/1443811189000/xmlFile)" + 
				    "\nIMPORTANT: This changes for every new upload of static resource!",
				    onChange: function () {
				        component.refresh();
				    }
				},
				{
				    id: "paramsjson",
				    type: "string",
				    label: "Parameters JSON",
				    helptext: "JSON object containing pairs of Tokens & Values to be replaced in Skuid XML.",
				    onChange: function () {
				        component.refresh();
				    }
				}			
	        ];
	        propCategories.push({
	            name: "Custom Properties",
	            props: propsList,
	        });
	        var state = component.state;
	        propertiesObj.applyPropsWithCategories(propCategories, state);
	        
	    },
	    defaultStateGenerator: function () {
	        return skuid.$(skuid.builder.getBuilders().wrapper.defaultStateGenerator()[0].outerHTML.replace("wrapper", "connectedcomponents__xmlinclude"));
	    }
		
	}));

		
})(skuid);