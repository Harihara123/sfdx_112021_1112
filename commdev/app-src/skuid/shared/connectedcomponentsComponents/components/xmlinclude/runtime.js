var $ = skuid.$;

(function(skuid) {
    var $ = skuid.$;
    skuid.componentType.register("connectedcomponents__xmlinclude", function(domElement, xmlConfig, component) {
        
        var resourcePath = xmlConfig.attr('resourcepath');
        var paramsJSON = xmlConfig.attr('paramsjson');

        var $xml = skuid.utils.makeXMLDoc;
        var finalXML;

        if (null != paramsJSON && paramsJSON.length > 0) { //Check if any JSON parameters were passed.
            //Replace tokens with values from json
            finalXML = replaceTokens(JSON.parse(paramsJSON), loadXml(resourcePath));
        } else {
            //No json was passed, so load xml as is.
            finalXML = loadXml(resourcePath);
        }
        var xmlDefinition = $xml(finalXML);

        domElement.empty();
        skuid.component.factory({
            element: domElement,
            xmlDefinition: xmlDefinition
        });
    });
})(skuid);

function replaceTokens(obj, xml) {
    for ( var i = 0; i < obj.params.length; i++) {
        //xml = xml.replace(obj.params[i].token, obj.params[i].value);
        xml = xml.replace(new RegExp(escapeRegExp(obj.params[i].token), 'g'), obj.params[i].value);
    }
    return xml;
}

function loadXml(staticResPath) {
    var fileInfo;
    $.ajax({
        url: staticResPath, // path to file (/resource/1443811189000/mark).
        dataType: 'text', // type of file (text, json, xml, etc)
        success: function(data) { // callback for successful completion
          fileInfo = data;
        },
          async: false,
        error: function() { // callback if there's an error
          alert("There was an error trying to load resource '" + staticResPath + "'.");
        }
      }); 
  return fileInfo;  
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}