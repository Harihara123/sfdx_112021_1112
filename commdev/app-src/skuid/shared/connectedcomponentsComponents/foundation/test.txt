      <wrapper uniqueid="sk-S4xGY-100">
         <components>
            <basicfieldeditor showheader="true" showsavecancel="false" showerrorsinline="true" model="@@contact@@" buttonposition="" uniqueid="sk-S4yyv-108" mode="@@mode@@">
               <columns>
                  <column width="33.3%">
                     <sections>
                        <section title="Section A" collapsible="no" showheader="false">
                           <fields>
                              <field id="FirstName" valuehalign="" type="" cssclass="uft_first_name"/>
                              <field id="LastName"/>
                              <field id="Title"/>
                           </fields>
                        </section>
                     </sections>
                  </column>
                  <column width="33.3%">
                     <sections>
                        <section title="Section B" collapsible="no" showheader="false">
                           <fields>
                              <field id="MailingAddress" valuehalign="" type=""/>
                              <field id="MailingCity"/>
                              <field id="MailingState"/>
                              <field id="MailingPostalCode"/>
                              <field id="MailingCountry"/>
                           </fields>
                        </section>
                     </sections>
                  </column>
                  <column width="33.3%">
                     <sections>
                        <section title="New Section" collapsible="no" showheader="false">
                           <fields>
                              <field id="Phone"/>
                           </fields>
                        </section>
                     </sections>
                  </column>
               </columns>
            </basicfieldeditor>
         </components>
         <styles>
            <styleitem type="background"/>
            <styleitem type="border"/>
            <styleitem type="size"/>
         </styles>
      </wrapper>