import * as la from "../../library/array";
import { expect } from "../../library/testing";
import { Spec } from "../specs";

module arrayOf {
    export function works(): boolean {
        return expect(() => la.of<number>()).toBeEmpty();
    }
}

module concat {
    export function works(): boolean {
        let result = la.concat([1,2,3],[4,5,6]);
        return expect(() => result).toContainAllOf([1,2,3,4,5,6]);
    }
    export function worksCurried(): boolean {
        let result = la.curried.concat([1,2,3])([4,5,6]);
        return expect(() => result).toContainAllOf([1,2,3,4,5,6]);
    }
}

module filter {
    export function works(): boolean {
        let result = la.filter([1,2,3,4,5], value => value % 2 === 0);
        return expect(() => result).toContainAllOf([2,4]);
    }
    export function worksCurried(): boolean {
        let result = la.curried.filter([1,2,3,4,5])(value => value % 2 === 0);
        return expect(() => result).toContainAllOf([2,4]);
    }
}

module fromOne {
    export function worksWithNumber(): boolean {
        return expect(() => la.fromOne(2)).toContain(2);
    }
    export function worksWithString(): boolean {
        return expect(() => la.fromOne("value")).toContain("value");
    }
}

module fromTwo {
    export function worksWithNumbers(): boolean {
        let values = la.fromTwo(1, 2);
        return expect(() => values).toContain(1) && expect(() => values).toContain(2);
    }
    export function worksWithStrings(): boolean {
        let values = la.fromTwo("one", "two");
        return expect(() => values).toContainAllOf(["one", "two"]);
    }
}

module hasAny {
    let values = [1, 2, 3];

    export function works(): boolean {
        return expect(() => la.hasAny(values)).toBe(true);
    }
}

module longestOf {
    let values = ["1", "123", "12", "1", "1234"];

    export function works(): boolean {
        return expect(() => la.longestOf(values)).toBe(4);
    }
}

module flatten {
    export function worksWithTwoNested(): boolean {
        return expect(() => la.flatten<number>([[1,2],[3,4]])).toContainAllOf([1,2,3,4]);
    }
    export function worksWithThreeNested(): boolean {
        return expect(() => la.flatten<number>([[1,2],[3,4],[5,6]])).toContainAllOf([1,2,3,4,5,6]);
    }
    export function worksWithNestedArrayN(): boolean {
        return expect(() => la.flatten([[1,[2,3]],4])).toContainAllOf([1,2,3,4]);
    }
}

module viaTwoArrays {
    let oneArray = [];
    let anotherArray = [1, 2, 3];
    let result = la.viaTwoArrays(oneArray, anotherArray, la.hasAny, {
        caseOfBoth() { return false; },
        caseOfFirst() { return false; },
        caseOfSecond() { return true; },
        caseOfNeither() { return false; }
    });

    export function worksWithHasAny(): boolean {
        return expect(() => result).toBe(true);
    }
}

module withSomeOrElse {
    export function worksWithSome(): boolean {
        let result = la.withSomeOrElse([1, 2, 3], some => true, msg => false);
        return expect(() => result).toBe(true);
    }

    export function worksWithNone(): boolean {
        let result = la.withSomeOrElse([], some => true, msg => false);
        return expect(() => result).toBe(false);
    }
}

export function specs(): Spec[] {
    return [
        { section: "arrayOf", tests: [ { name: "works", run: arrayOf.works } ] },
        { section: "concat", tests: [
            { name: "works", run: concat.works },
            { name: "worksCurried", run: concat.worksCurried }
        ]},
        { section: "filter", tests: [
            { name: "works", run: filter.works },
            { name: "worksCurried", run: filter.worksCurried }
        ]},
        { section: "fromOne", tests: [
            { name: "worksWithNumber", run: fromOne.worksWithNumber },
            { name: "worksWithString", run: fromOne.worksWithString }
        ]},
        { section: "fromTwo", tests: [
            { name: "worksWithNumbers", run: fromTwo.worksWithNumbers },
            { name: "worksWithStrings", run: fromTwo.worksWithStrings }
        ]},
        { section: "hasAny", tests: [ { name: "works", run: hasAny.works } ] },
        { section: "longestOf", tests: [ { name: "works", run: longestOf.works } ] },
        { section: "fold", tests: [
            { name: "worksWithTwoNested", run: flatten.worksWithTwoNested },
            { name: "worksWithThreeNested", run: flatten.worksWithThreeNested },
            { name: "worksWithNestedArrayN", run: flatten.worksWithNestedArrayN }
        ]},
        { section: "viaTwoArrays", tests: [ { name: "works", run: viaTwoArrays.worksWithHasAny } ] },
        { section: "withSomeOrElse", tests: [
            { name: "worksWithSome", run: withSomeOrElse.worksWithSome },
            { name: "worksWithNone", run: withSomeOrElse.worksWithNone }
        ]}
    ];
}
