import * as ts from "../library/testing";
import { specs as arraySpecs } from "./library/array-spec";
import * as chalk from "chalk";

export interface Spec {
    section: string;
    tests: ts.Test[]
}

if (ts.testAll(arraySpecs().map(spec => spec.tests).reduce((one, another) => one.concat(another)))) {
    console.log("");
    console.log("RESULTS: ", chalk.bgGreen("ALL TESTS PASSED"));
} else {
    console.log(ts.logSpecs(arraySpecs()));
    console.log("");
    console.log("RESULTS: ", chalk.bgRed("SOME TEST(S) FAILED"));
}
