//This typing is used as an interface for /library/formatting/libphonenumber/i18nPhoneFormatter.js
//It is imported in app.ts.

// Return a boolean to determine if the phone number is valid.
declare const isValidPhoneNumber: (

    // The name of the phoneNumber
    phoneNumber: string,

    // the default regionCode
    regionCode: string

) => boolean;

// Return an i18n formatted phone number, or if invalid, return the raw number.
declare const formatPhoneNumber: (

    // The name of the phoneNumber
    phoneNumber: string,

    // the default regionCode
    regionCode: string

) => boolean;
