interface ObjectConstructor {
    assign<a>(target: a, ...sources: any[]): a;
}
