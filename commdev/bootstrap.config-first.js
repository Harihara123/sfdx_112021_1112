'use strict';

/**
 * Bootstrap config for the "first" (pre-login) pages. 
 * For available elements, have a look at http://bline.github.io/bootstrap-webpack-example/
 */
module.exports = {

    scripts: {
    	"modal": true
    }, 

    styles: {
    	"buttons": true,
    	"close": true,
    	"mixins": true, 
    	"modals": true
    }
}
