'use strict';

/**
 * Bootstrap config for the post-login pages. 
 * For available elements, have a look at http://bline.github.io/bootstrap-webpack-example/
 */
module.exports = {

    scripts: {
    	"popover": true, 
		"tooltip": true
    }, 

    styles: {
    	"buttons": true,
    	"close": true,
    	"mixins": true, 
    	"popovers": true, 
  		"tooltip": true
    }
}