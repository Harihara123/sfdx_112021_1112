// #region Vars
// Gulp
const { dest, parallel, series, src, watch } = require('gulp');

const gulpZip     = require('gulp-zip');
const header      = require('gulp-header');
const less        = require('gulp-less');
const log         = require('fancy-log'); 
const plumber     = require('gulp-plumber'); 
const imagemin    = require('gulp-imagemin');
const pkg         = require('./package.json');

//Scripts
var scriptBundle = 'tc_script';
var scriptSourceRoot = '../app-src-com/script';

//RIP Skuid
//var skuidComponentsBundle = 'tc_skuid_components';
//var skuidComponentsSourceRoot = '../app-src-com/skuid/components';

//Style
var stylingSourceRoot = '../app-src-com/style';
var stylingDest = '../app-src-com/style/build/styles';
var stylingBundle = 'tc_style';

//Lightning Styles
var stylingDestLightning = '../app-src-com/style/build/lightning-styles';

//Images
var imagesSourceRoot = '../app-src-com/images';
var imageBundle = 'tc_images';

// Final distribution directory for all built Salesforce-bound static resources.
var distDir = '../../sfdc/declarative/staticresources/';

var srcPaths = {
  // Add opco sylesheet paths here...
  lessRootFiles: [
    stylingSourceRoot + '/styles/aerotek-styles.less',
    stylingSourceRoot + '/styles/aerotekemea-styles.less',
    stylingSourceRoot + '/styles/astoncarter-styles.less',
    stylingSourceRoot + '/styles/astoncarteremea-styles.less',
    stylingSourceRoot + '/styles/easi-styles.less',
    stylingSourceRoot + '/styles/newco-styles.less',
    stylingSourceRoot + '/styles/teksystems-styles.less',
    stylingSourceRoot + '/styles/teksystemsemea-styles.less'
  ],

  lessRootFilesLightning: [
    stylingSourceRoot + '/lightning-styles/aerotek-styles.less',
    stylingSourceRoot + '/lightning-styles/astoncarter-styles.less',
	stylingSourceRoot + '/lightning-styles/astoncarteremea-styles.less',
    stylingSourceRoot + '/lightning-styles/aerotekemea-styles.less',
    stylingSourceRoot + '/lightning-styles/newco-styles.less',
    stylingSourceRoot + '/lightning-styles/teksystems-styles.less',
    stylingSourceRoot + '/lightning-styles/teksystemsemea-styles.less',
    stylingSourceRoot + '/lightning-styles/easi-styles.less'
  ],

  imageSource: imagesSourceRoot + '/**',

  // Styling files to watch for changes, triggering a LESS compile.
  lessSource:  [stylingSourceRoot + '/styles/**/*.less'],
  lessLightningSource:  [stylingSourceRoot + '/lightning-styles/**/*.less'],
  // Skuid component source files to watch for changes, triggering a (zip) "build".
  //skuidComponentsSource: [skuidComponentsSourceRoot + '/**'],
  // Webpack-compiled source files to watch for changes, triggering a (zip) "build".
  webpackBuilt: scriptSourceRoot + '/built/app-src-com/*'
};
// #endregion

// #region tc_style
// Build Classic Style
function buildClassicStyle(cb) {
  src(srcPaths.lessRootFiles)
      .pipe(plumber({errorHandler: onError}))
      .pipe(less())
      .pipe(header(compiledBanner, { pkg : pkg } ))
      .pipe(dest(stylingDest));

  cb();
};

// Build Lightning Style
function buildLightningStyle(cb) {
  src(srcPaths.lessRootFilesLightning)
      .pipe(plumber({errorHandler: onError}))
      .pipe(less())
      .pipe(header(compiledBanner, { pkg : pkg } ))
      .pipe(dest(stylingDestLightning));
console.log("in the buildLightningStyle");
  cb();
};

// Zip Style (tc_style)
function zipStyleResource(cb) {
  src('../app-src-com/style/**','../app-src-com/style/build/**')
      //.on('error', onError("Error: <%= error.message %>"))
      .pipe(gulpZip(stylingBundle + '.resource'))
      .pipe(dest(distDir));
	console.log("in the zipStyleResource");
  cb();
};

// Zip Images (tc_image)
function zipImageResource(cb) {
  src(imagesSourceRoot + '/**')
      //.on('error', onError("Error: <%= error.message %>"))
      .pipe(imagemin())
      .pipe(gulpZip(imageBundle + '.resource'))
      .pipe(dest(distDir));

  cb();
};
// #endregion

// #region tc_script
// Build Javascript
function buildScript(cb) {
  src(srcPaths.webpackBuilt)
      .pipe(gulpZip(scriptBundle + '.resource'))
      .pipe(dest(distDir));

  cb();
};
// #endregion

// #region Metadata Files
function copyMeta(cb) {
  src('./metadata/tc_style.resource-meta.xml').pipe(dest(distDir));
  src('./metadata/tc_script.resource-meta.xml').pipe(dest(distDir));
  src('./metadata/tc_images.resource-meta.xml').pipe(dest(distDir));

  cb();
}
// #endregion

// #region Utils
function watchAll(cb) {
  watch(srcPaths.lessSource, series(buildClassicStyle, zipStyleResource));
  watch(srcPaths.lessLightningSource, series(buildLightningStyle, zipStyleResource));
  watch(srcPaths.imageSource, zipImageResource);
  watch(srcPaths.webpackBuilt, buildScript);

  cb();
}

// Error Handler
var onError = function(err) {
  log('Gulp Failure! Error: ' + err);
};

// Header
var compiledBanner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version <%= pkg.version %>',
  ' * @author <%= pkg.author %>',
  ' *',
  ' * COMPILED FILE DO NOT DIRECTLY EDIT',
  ' */',
  ''].join('\n');
// #endregion

//exports.once = series(buildClassicStyle, buildLightningStyle, zipStyleResource);
exports.once = parallel(series(buildClassicStyle, buildLightningStyle, zipImageResource, buildScript, zipStyleResource), copyMeta);
exports.default = parallel(series(buildClassicStyle, buildLightningStyle, zipStyleResource), zipImageResource, buildScript, copyMeta, watchAll);