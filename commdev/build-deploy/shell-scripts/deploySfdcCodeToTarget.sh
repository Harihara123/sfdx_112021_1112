#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_sfdc.xml deploy -DenvName=$1  -Dpackage.deploy=sfdc_package.xml
else
    echo "You must provide a target org name as a parameter, e.g.: ./deploySfdcCodeToTarget.sh atsci"
fi
