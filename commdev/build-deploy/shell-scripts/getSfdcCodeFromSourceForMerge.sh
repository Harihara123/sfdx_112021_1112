#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_sfdc.xml retrieveUnpackaged  -DenvName=$1 -Dpackage.retrieve=sfdc_package_ats.xml
else
    echo "You must provide a source org name as a parameter, e.g.: ./getSfdcCodeFromSource.sh atsdev1"
fi
