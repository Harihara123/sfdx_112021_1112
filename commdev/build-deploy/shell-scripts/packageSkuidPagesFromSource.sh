#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_skuid.xml packagePagesInSourceOrg -DenvName=$1
else
    echo "You must provide a source org name as a parameter, e.g.: ./packageSkuidPagesFromSource.sh atsdev1"
fi
