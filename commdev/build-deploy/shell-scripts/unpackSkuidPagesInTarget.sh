#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_skuid.xml unpackPagesInTargetOrg -DenvName=$1
else
    echo "You must provide a target org name as a parameter, e.g.: ./unpackSkuidPagesInTarget.sh atsci"
fi
