#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_sfdc.xml uploadUncompressedResource -DenvName=$1 -DresourceName=ats_bundle -DsourcePath=../built/app/ -DsourceFileName=ats_bundle.js -DmimeType=application/javascript
else
    echo "You must provide a target org name as a parameter, e.g.: ./uploadAtsBundle.sh atsdev1"
fi
