#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_sfdc.xml uploadUncompressedResource -DenvName=$1 -DresourceName=ats_stylesheet -DsourcePath=../app-src/styles/ -DsourceFileName=ats_stylesheet.css -DmimeType=text/css
else
    echo "You must provide a target org name as a parameter, e.g.: ./uploadAtsStylesheet.sh atsdev1"
fi
