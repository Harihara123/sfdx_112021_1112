@ECHO OFF

if [%1]==[] (
	ECHO "You must provide a target org name as a parameter, e.g.: ./uploadConnectedComponents.sh atsdev1"
) else (
	ant -f ../build_sfdc.xml uploadCompressedResource -DenvName=%1 -DresourceName=connectedcomponentsComponents -DsourcePath=../app-src/skuid/shared/ -DmimeType=application/x-zip-compressed
)
