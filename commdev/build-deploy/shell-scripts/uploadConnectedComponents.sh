#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_sfdc.xml uploadCompressedResource -DenvName=$1 -DresourceName=connectedcomponentsComponents -DsourcePath=../app-src/skuid/shared/ -DmimeType=application/x-zip-compressed
else
    echo "You must provide a target org name as a parameter, e.g.: ./uploadConnectedComponents.sh atsdev1"
fi
