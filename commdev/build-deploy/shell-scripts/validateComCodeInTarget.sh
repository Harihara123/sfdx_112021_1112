#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    ant -f ../build_sfdc.xml validateDeploy -DenvName=$1 -Dpackage.deploy=com_package.xml
else
    echo "You must provide a target org name as a parameter, e.g.: ./validateSfdcCodeInTarget.sh atsci"
fi
