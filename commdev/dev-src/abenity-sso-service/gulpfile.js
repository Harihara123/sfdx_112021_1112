var del = require('del');
var gulp = require('gulp');
var merge = require('merge-stream');
var notify = require('gulp-notify');
var runSequence = require('run-sequence');

// Path for the local Apcahe server's htdocs directory.
var apacheDocsDir = 'C:/Apache24/htdocs/';

/**
 * Copy PHP code from the source directory to the appropriate Apache directory.
 */
gulp.task('copyPhpCode', function() {
    return gulp.src(['web/**/*'], {base:"web"})
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest(apacheDocsDir + 'web'))
        .pipe(notify({
            title: 'Gulp',
            subtitle: 'Success',
            message: 'Code files copied'
        }));
});

/**
 * Copy PHP config files from the source directory to the appropriate Apache directory.
 */
gulp.task('copyPhpConfig', function() {
    var stream1 = gulp.src(['composer.json', 'composer.lock'], {base:"."})
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest(apacheDocsDir))
        .pipe(notify({
            title: 'Gulp',
            subtitle: 'Success',
            message: 'Top-level config files copied', 
            onLast: true
        }));

    var stream2 = gulp.src(['web/.htaccess'], {base:"web"})
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest(apacheDocsDir + 'web'))
        .pipe(notify({
            title: 'Gulp',
            subtitle: 'Success',
            message: 'Web config files copied', 
            onLast: true
        }));

    return merge(stream1, stream2);
});

/**
 * Copy 3rd-party Composer-managed PHP libraries from the source directory to the appropriate Apache directory.
 */
gulp.task('copyPhpLibraries', function() {
    return gulp.src(['vendor/**/*'], {base:"vendor"})
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest(apacheDocsDir + 'vendor'))
        .pipe(notify({
            title: 'Gulp',
            subtitle: 'Success',
            message: 'Libraries copied', 
            onLast: true
        }));
});

gulp.task('clean', function() {
    del([apacheDocsDir + 'vendor']);
});

/**
 * Copy all necessary PHP source assets from the source directories to the appropriate Apache directories.
 */
gulp.task('copyAll', ['clean', 'copyPhpCode', 'copyPhpConfig', 'copyPhpLibraries'], function() {
    return gulp.src(['.'])
        .pipe(notify({
            title: 'Gulp',
            subtitle: 'Success',
            message: 'All files copied'
        }));
});

// gulp.task('copyAll', function(callback) {
//     runSequence([
//         'copyPhpCode', 
//         'copyPhpConfig', 
//         'copyPhpLibraries'], 
//         //'notifyOnce', 
//         'watch', 
//         callback
//     );
// });

/**
 * Watch for changes and build (copy) on-change.
 */
gulp.task('watch', function() {
    // Set up fine-grained watches because copying the libraries can be slow.
    gulp.watch(['web/*'], ['copyPhpCode']);
    gulp.watch(['composer.json', 'composer.lock', 'web/.htaccess'], ['copyPhpConfig']);
    gulp.watch(['vendor/**'], ['copyPhpLibraries']);
});

/**
 * Execute once and exit.
 */
gulp.task('once', ['copyAll']);

/**
 * The default task (called when you run 'gulp' from the command line).
 */
gulp.task('default', ['copyAll', 'watch']);

