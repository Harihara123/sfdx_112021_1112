<?php

require('../vendor/autoload.php');

$app = new Silex\Application();
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Use a customized subclass of the Abenity-provided ApiClient class.
require 'AbenityApiClient.php';

$helper = new AbenitySsoAppHelper($app);

//$app['debug'] = true;

$abenityLogFilePath = $helper->calcLogFilePath();
file_put_contents('php://stderr', print_r('Setting log file path to ' . $abenityLogFilePath, TRUE));

$app->register(new \Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => $abenityLogFilePath, 
    'monolog.level' => \Monolog\Logger::DEBUG,
));

/**
 * Set a before application middleware that allows us to tweak the Request before the controller 
 * is executed.
 */
$app->before(function (Request $request) use($app, $helper) {
	$contentType = $request->headers->get('Content-Type');
	$app['monolog']->debug("contentType = " . print_r($contentType, TRUE));
	/*
	 * If passed JSON, parse and set back (unencoded) into the request.
	 */
    if (0 === strpos($contentType, 'application/json')) {
        $requestData = json_decode($request->getContent(), true);
        //$app['monolog']->debug("requestData =  . print_r($requestData, TRUE));
        $request->request->replace(is_array($requestData) ? $requestData : array());
        //$app['monolog']->debug("$request->request = " . print_r($request->request, TRUE));
    }
});

/**
 * Sign-on a user, given the ID and details data for that user, returning back a temporary URL that 
 * can be used/followed by the user to complete the sign-in.
 */
$app->post('/sso', function(Request $request) use($app, $helper) {
	// TODO Add tripwire to force a time-out.

	$memberProps = array(
		/*
		 * An immutable, unique ID for the user within Allegis' system. A hash of this value is 
		 * stored by Abenity to determine if the member is new or returning. This is how the user 
		 * is uniquely identified by Abenity.
		 */
	    'client_user_id' => $request->get('user-id'), 
	    /*
	     * The user's email address. This will also be used for the user's Abenity username, if 
	     * possible.
	     */
	    'email' => $request->get('user-email-address'), 
	    'firstname' => $request->get('user-first-name'), 
	    'lastname' => $request->get('user-last-name'), 
	    'address' => $request->get('user-address-street'), 
	    'city' => $request->get('user-address-city'), 
	    'state' => $request->get('user-address-state'), 
	    'zip' => $request->get('user-address-postal-code'), 
	    'country' => $request->get('user-address-country')
	);
	$memberProps['creation_time'] = date('c');
	$memberProps['salt'] = rand(0,100000);
	$suppressWelcomeEmail = 0;
	$sendWelcomeEmail = 1;
	$memberProps['send_welcome_email'] = $suppressWelcomeEmail;

	//$username = $helper->buildUsername($memberProps);
	//$app['monolog']->debug("username = " . $username);
	//$memberProps['username'] = $username;

	$app['monolog']->debug("memberProps = " . json_encode($memberProps));

	$privateKeyStr = $helper->readInPrivateKey();

	$apiVersion = 1;
	// In seconds.
	$apiTimeout = 30;
	$apiUrl = $helper->calcAbenityApiUrl();
	$apiUsername = $helper->calcAbenityApiUsername();
	$apiPassword = $helper->calcAbenityApiPassword();
	$apiKey = $helper->calcAbenityApiKey();
	$app['monolog']->debug("apiUrl = " . $apiUrl);
	//$app['monolog']->debug("apiUsername = " . $apiUsername);
	//$app['monolog']->debug("apiPassword = " . $apiPassword);
	//$app['monolog']->debug("apiKey = " . $apiKey);
	$abenity = new AbenityApiClient($apiUsername, $apiPassword, $apiKey, $apiVersion, $apiTimeout, $apiUrl);
	$app['monolog']->debug("Attempting to sign-on user " . $memberProps['email'] . ' with ID ' . $memberProps['client_user_id']);
	$abenity_response = $abenity->ssoMember($memberProps, $privateKeyStr);
	//$app['monolog']->debug("API client debug = " . json_encode($abenity->debug));

	//phpinfo();
	$responseCode = 200;

	if (isset($abenity_response->status)) {

	    //$app['monolog']->debug(json_encode($abenity));

	    // Test verifiction
	    if ($abenity_response->status == 'ok') {

	        // Display the login link
	        //$result = $abenity_response->data->token_url;
	        $result = array('success' => $abenity_response->data->token_url);

	    } else {
	    	$responseCode = 500;
			$result = array('error' => $abenity_response->error);
	    }

	}

	/*
	 * Add a property to the response to let the client know whether or not the request was made 
	 * against the Abenity sandbox environment. If we're pointed at the Abenity sanbox, the returned 
	 * URL won't actually work.
	 */
	$result['isSandbox'] = $helper->calcAbenityApiUrlIsSandbox();

	$app['monolog']->debug("Result = " . json_encode($result));
	return $app->json($result);
});

// $app->error(function (\Exception $e, Request $request, $code) {
//     switch ($code) {
//         case 404:
//             $message = 'The requested page could not be found.';
//             break;
//         default:
//             $message = 'We are sorry, but something went terribly wrong.';
//     }

//     return new Response($message);
// });

$app->run();

class AbenitySsoAppHelper {

	private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

	/**
	 * Determine and return the filepath of the log file. 
	 *
	 * Using php://stderr works fine on Heroku, but not on a local Apache server. (The file, error.log, only 
	 * gets written to when Apache is stopped.) Locally, add the following to your php.ini file (with the path 
	 * set appropriately for your system): 
	 * 	ABENITY_SSO_LOG = C:\Apache24\logs\abenity-sso.log
	 * This requires Apache to be restarted to take effect.
	 */
    public function calcLogFilePath() {
		$abenityLogFilePath = get_cfg_var('ABENITY_SSO_LOG');
		if (!isset($abenityLogFilePath) || $abenityLogFilePath == FALSE) {
			$abenityLogFilePath = 'php://stderr';
		}
		return $abenityLogFilePath;
    }

	/**
	 * Determine and return the base URL for the Abenity API. This will be for either the Abenity 
	 * production environment or a sandbox. 
	 *
	 * Locally, add the following to your php.ini file (with the path set appropriately for the target env, 
	 * https://api.abenity.com for Abenity PROD and https://sandbox.abenity.com for Abenity non-PROD): 
	 * 	ABENITY_API_URL = https://sandbox.abenity.com
	 * This requires Apache to be restarted to take effect.
	 */
    public function calcAbenityApiUrl() {
    	return $this->retrieveConfigValue('ABENITY_API_URL');
    }

	/**
	 * Determine and return the authorized username for accessing the Abenity API. This may be found in the 
	 * Abenity Back Office site (My Account -> API Integration)
	 *
	 * Locally, add the following to your php.ini file: 
	 * 	ABENITY_API_USERNAME = [username]
	 * This requires Apache to be restarted to take effect.
	 */
    public function calcAbenityApiUsername() {
    	return $this->retrieveConfigValue('ABENITY_API_USERNAME');
    }

	/**
	 * Determine and return the authorized password for accessing the Abenity API. This may be found in the 
	 * Abenity Back Office site (My Account -> API Integration)
	 *
	 * Locally, add the following to your php.ini file: 
	 * 	ABENITY_API_PASSWORD = [password]
	 * This requires Apache to be restarted to take effect.
	 */
    public function calcAbenityApiPassword() {
    	return $this->retrieveConfigValue('ABENITY_API_PASSWORD');
    }

	/**
	 * Determine and return the authorized key for accessing the Abenity API. This may be found in the 
	 * Abenity Back Office site (My Account -> API Integration)
	 *
	 * Locally, add the following to your php.ini file: 
	 * 	ABENITY_API_KEY = [key]
	 * This requires Apache to be restarted to take effect.
	 */
    public function calcAbenityApiKey() {
    	return $this->retrieveConfigValue('ABENITY_API_KEY');
    }

	/**
	 * On a locally-installed Apache, assume that entries in the php.ini file are the only available 
	 * method for setting environmental/config variables (even though it's less than ideal). As such, use 
	 * get_cfg_var(). Conversely, on Heroku, config variables are accessible via getenv().
	 */
    private function retrieveConfigValue($configVarName) {
		$configValue = get_cfg_var($configVarName);
		if (!isset($configValue) || $configValue == FALSE) {
			$configValue = getenv($configVarName);
		}
		return $configValue;
    }

	/**
	 * Return true if the context Abenity API URL is for a sandbox (non-production) environment, false 
	 * otherwise. The Abenity sandbox environment is useful for testing the SSO integration, but not 
	 * for end-to-end testing, as the returned URL's don't work and we have no visibility into the 
	 * user accounts in the sandbox.
	 */
    public function calcAbenityApiUrlIsSandbox() {
  		$isSandbox = false;
		$abenityApiUrl = $this->calcAbenityApiUrl();
		if (strpos($abenityApiUrl, 'api.abenity') == FALSE) {
			$isSandbox = true;
		}
		return $isSandbox;
    }

    /**
     * Read in the Allegis-Abenity private key and return as a string.
     *
     * Generated with the following command: 
     *	ssh-keygen -t rsa -b 2048
     */
    public function readInPrivateKey() {
    	$privateKeyStr = file_get_contents('./allegis-abenity-private.key');
    	return $privateKeyStr;
    }

	/**
	 * Optional. The user's Abenity username may be set to override the default setting of using the 
	 * user's email address as their username. The Abenity username must be unique among all Abenity 
	 * users (not just within the Allegis program).
	 */
    public function buildUsername($memberProps) {
		$idx = strrpos($memberProps['email'], '@');
		//$this->app['monolog']->debug("client user id = " . print_r($memberProps['client_user_id'], TRUE));
		$username = substr_replace($memberProps['email'], '-' . $memberProps['client_user_id'], $idx, 0);
		return $username;
    }

}

