package com.allegis.sfdc.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class FileIO {

	public Map<String, List<Case>> readCaseFile(String path) throws ParseException, FileNotFoundException {
		List<File> userReportFiles = parseDirectory(path, "Case");
		path = userReportFiles.get(0).getAbsolutePath();
		SimpleDateFormat sf = new SimpleDateFormat("M/d/yyyy HH:mm aaa");
		Map<String, List<Case>> caseMap = new HashMap<String, List<Case>>();
		List<Case> caseList;
		Calendar c = Calendar.getInstance();
		try {

			CsvReader caseFile = new CsvReader(path);

			caseFile.readHeaders();

			while (caseFile.readRecord()) {
				if (caseFile.getColumnCount() > 1) {
					Case caseRecord = new Case();
					caseRecord.setCaseNumber(caseFile.get("Case Number"));
					caseRecord.setContactPSId(caseFile.get("Peoplesoft ID"));
					caseRecord.setDate(sf.parse(caseFile.get("Date/Time Opened")));
					caseRecord.setDaysToEndDate(caseFile.get("Days_To_EndDate"));
					caseRecord.setPositionId(caseFile.get("Talent JobRequisitionNumber"));
					
					c.setTime(caseRecord.getDate()); 
					if ((caseRecord.getDaysToEndDate() > 0) && !StringUtils.isBlank(String.valueOf(caseRecord.getDaysToEndDate()))) {
						c.add(Calendar.DATE, caseRecord.getDaysToEndDate()); 
						caseRecord.setOldEndDate(c.getTime());
					}
					caseRecord.setSubject(caseFile.get("Subject"));
					if (caseMap.get(caseFile.get("Peoplesoft ID")) == null) {
						caseList = new ArrayList<Case>();
						caseList.add(caseRecord);
					} else {
						caseList = caseMap.get(caseFile.get("Peoplesoft ID"));
						caseList.add(caseRecord);
					}
					caseMap.put(caseFile.get("Peoplesoft ID"), caseList);
				}
				

			}

			caseFile.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return caseMap;
	}

	public Map<String, User> readUserFile(String path) throws FileNotFoundException {
		Map<String, User> userMap = new HashMap<String, User>();
		List<File> userReportFiles = parseDirectory(path, "User");
		path = userReportFiles.get(0).getAbsolutePath();
		try {

			CsvReader userFile = new CsvReader(path);

			userFile.readHeaders();

			while (userFile.readRecord()) {
				if (userFile.getColumnCount() > 1) {
					User userRecord = new User();
					userRecord.setPsId(userFile.get("Peoplesoft Id"));
					userRecord.setId(userFile.get("User Id"));
					userRecord.setName(userFile.get("Full Name"));
					userRecord.setOffice(userFile.get("Office"));
					if (StringUtils.isBlank(userFile.get("Office Code"))) {
						continue;
					}
					userRecord.setOfficeCode(userFile.get("Office Code"));
					userRecord.setRegion(userFile.get("Region"));
					userRecord.setRegionCode(userFile.get("Region Code"));
					userRecord.setLastLogin(userFile.get("Last Login"));
					userRecord.setProfileName(userFile.get("Profile"));
					userRecord.setLinkedInToken(userFile.get("TC Linkedin User Token"));
					userRecord.setContactId(userFile.get("Contact ID"));
					userMap.put(userFile.get("Peoplesoft Id"), userRecord);
				}
				
			}

			userFile.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userMap;
	}

	public void writeReportFile(String fileName, List<Report> reportData, String cutOffDate) throws FileNotFoundException {

		try {

			CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName, false), ',');
			csvOutput.write("Region");
			csvOutput.write("Office");
			csvOutput.write("Users");
			csvOutput.write("Full Name");
			csvOutput.write("Contact Id");
			csvOutput.write("Users Logged In");
			csvOutput.write("Users Logged In After " + cutOffDate);
			csvOutput.write("Number Of End Date Cases/Job Application After " + cutOffDate);
			csvOutput.write("Users Requested End Date/Job Application After " + cutOffDate);
			// csvOutput.write("Case Number");
			csvOutput.write("Request Date");
			csvOutput.write("Users Granted Extension After " + cutOffDate);
			csvOutput.write("Extension Days After " + cutOffDate);
			csvOutput.write("Users Imported LinkedIn Profile");
			

			csvOutput.endRecord();

			for (Report reportRecord : reportData) {
				csvOutput.write(reportRecord.getRegion());
				csvOutput.write(reportRecord.getOffice());
				csvOutput.write(reportRecord.getPsId());
				csvOutput.write(reportRecord.getName());
				csvOutput.write(reportRecord.getContactId());
				csvOutput.write(reportRecord.getHasLoggedIn());
				csvOutput.write(reportRecord.getHasLoggedInAfterCutOffDate());
				csvOutput.write(reportRecord.getNumberOfEndDateCases());
				csvOutput.write(reportRecord.getHasrequestedEndDateReview());
				// csvOutput.write(reportRecord.getCaseNumber());
				csvOutput.write(reportRecord.getEndDateCaseRequestDate());
				csvOutput.write(reportRecord.getExtensionGranted());
				csvOutput.write(reportRecord.getExtensionDays());
				csvOutput.write(reportRecord.getHasImportedLinkedinData());
				csvOutput.endRecord();

			}

			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<File> parseDirectory(String path, String objectType) throws FileNotFoundException {
		List<File> successFiles = new ArrayList<File>();
		//System.out.println(path);
		File dir = new File(path);
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (File f : files) {
				//System.out.println(f.getName());
					if(f.getName().endsWith(".csv")) {
						if (objectType.equals("User")) {
							if (isUserReportFile(f)) {
								System.out.println("Reading User Report File " + f.getName());
								successFiles.add(f);
								break;
							}
						} else if (objectType.equals("Case")) {
							if (isCaseReportFile(f)) {
								System.out.println("Reading Case Report File " + f.getName());
								successFiles.add(f);
								break;
							}
						} else if (objectType.equals("TalentWorkHistory")) {
							if (isWorkHistoryReportFile(f)) {
								System.out.println("Reading Work History Report File " + f.getName());
								successFiles.add(f);
								break;
							}
						}
					}
					
				
			}
		}
		return successFiles;
	}

	public boolean isCaseReportFile(File f) throws FileNotFoundException {
		boolean match = false;
		try {
			CsvReader successFile = new CsvReader(f.getAbsolutePath());
			successFile.readHeaders();

			while (successFile.readRecord()) {

				if (!StringUtils.isBlank(successFile.get("Case ID"))) {
					match = true;
				}

				break;
			}
			successFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return match;
	}
	
	public boolean isUserReportFile(File f) throws FileNotFoundException {
		boolean match = false;
		try {
			CsvReader successFile = new CsvReader(f.getAbsolutePath());
			successFile.readHeaders();

			while (successFile.readRecord()) {

				if (!StringUtils.isBlank(successFile.get("User ID"))) {
					match = true;
				}

				break;
			}
			successFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return match;
	}
	
	public boolean isWorkHistoryReportFile(File f) throws FileNotFoundException {
		boolean match = false;
		try {
			CsvReader successFile = new CsvReader(f.getAbsolutePath());
			successFile.readHeaders();

			while (successFile.readRecord()) {

				if (!StringUtils.isBlank(successFile.get("Talent Work History: ID"))) {
					match = true;
				}

				break;
			}
			successFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return match;
	}
	
	public Map<String, List<TalentWorkHistory>> readWorkHistoryFile(String path) throws ParseException, FileNotFoundException {
		List<File> workHistoryReportFiles = parseDirectory(path, "TalentWorkHistory");
		path = workHistoryReportFiles.get(0).getAbsolutePath();
		SimpleDateFormat sf = new SimpleDateFormat("M/d/yyyy");
		Map<String, List<TalentWorkHistory>> workHistoryMap = new HashMap<String, List<TalentWorkHistory>>();
		List<TalentWorkHistory> workHistoryList;
		try {

			CsvReader workHistoryFile = new CsvReader(path);

			workHistoryFile.readHeaders();

			while (workHistoryFile.readRecord()) {
				if(workHistoryFile.getColumnCount() > 1 ) {
					if(!StringUtils.isBlank(workHistoryFile.get("Start Date")) &&
							!StringUtils.isBlank(workHistoryFile.get("End Date")) &&
							!StringUtils.isBlank(workHistoryFile.get("JobRequisitionNumber"))) {
						TalentWorkHistory workHistoryRecord = new TalentWorkHistory();
						workHistoryRecord.setTalentName(workHistoryFile.get("Account Name"));
						workHistoryRecord.setPsId(workHistoryFile.get("Peoplesoft ID"));
						workHistoryRecord.setStartDate(sf.parse(workHistoryFile.get("Start Date")));
						workHistoryRecord.setEndDate(sf.parse(workHistoryFile.get("End Date")));
						workHistoryRecord.setPositionId(workHistoryFile.get("JobRequisitionNumber"));	
						workHistoryRecord.setSourceId(workHistoryFile.get("SourceId"));
						
						if (workHistoryMap.get(workHistoryFile.get("Peoplesoft ID")) == null) {
							workHistoryList = new ArrayList<TalentWorkHistory>();
							workHistoryList.add(workHistoryRecord);
						} else {
							workHistoryList = workHistoryMap.get(workHistoryFile.get("Peoplesoft ID"));
							workHistoryList.add(workHistoryRecord);
						}
						workHistoryMap.put(workHistoryFile.get("Peoplesoft ID"), workHistoryList);
					}
				}
				
			}

			workHistoryFile.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return workHistoryMap;
	}

}
