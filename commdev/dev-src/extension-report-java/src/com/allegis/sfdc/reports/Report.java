package com.allegis.sfdc.reports;

public class Report {
	String id;
	String psId;
	String hasLoggedInAfterCutOffDate;
	public String getHasLoggedInAfterCutOffDate() {
		return hasLoggedInAfterCutOffDate;
	}
	public void setHasLoggedInAfterCutOffDate(String hasLoggedInAfterCutOffDate) {
		this.hasLoggedInAfterCutOffDate = hasLoggedInAfterCutOffDate;
	}
	String hasLoggedIn;
	String hasrequestedEndDateReview;
	String numberOfEndDateCases;
	String hasJobSearchCase;
	String numberOfJobSearchCases;	
	String endDateCaseRequestDate;
	String hasImportedLinkedinData;
	String caseNumber;
	String extensionGranted;
	String extensionDays;
	String opco;
	String contactId;
	
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public String getNumberOfEndDateCases() {
		return numberOfEndDateCases;
	}
	public void setNumberOfEndDateCases(String numberOfEndDateCases) {
		this.numberOfEndDateCases = numberOfEndDateCases;
	}
	public String getHasJobSearchCase() {
		return hasJobSearchCase;
	}
	public void setHasJobSearchCase(String hasJobSearchCase) {
		this.hasJobSearchCase = hasJobSearchCase;
	}
	public String getNumberOfJobSearchCases() {
		return numberOfJobSearchCases;
	}
	public void setNumberOfJobSearchCases(String numberOfJobSearchCases) {
		this.numberOfJobSearchCases = numberOfJobSearchCases;
	}
	public String getOpco() {
		return opco;
	}
	public void setOpco(String opco) {
		this.opco = opco;
	}
	public String getExtensionGranted() {
		return extensionGranted;
	}
	public void setExtensionGranted(String extensionGranted) {
		this.extensionGranted = extensionGranted;
	}
	public String getExtensionDays() {
		return extensionDays;
	}
	public void setExtensionDays(String extensionDays) {
		this.extensionDays = extensionDays;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getHasImportedLinkedinData() {
		return hasImportedLinkedinData;
	}
	public void setHasImportedLinkedinData(String hasImportedLinkedinData) {
		this.hasImportedLinkedinData = hasImportedLinkedinData;
	}
	public String getEndDateCaseRequestDate() {
		return endDateCaseRequestDate;
	}
	public void setEndDateCaseRequestDate(String endDateCaseRequestDate) {
		this.endDateCaseRequestDate = endDateCaseRequestDate;
	}
	
	public String getHasLoggedIn() {
		return hasLoggedIn;
	}
	public void setHasLoggedIn(String hasLoggedIn) {
		this.hasLoggedIn = hasLoggedIn;
	}
	public String getHasrequestedEndDateReview() {
		return hasrequestedEndDateReview;
	}
	public void setHasrequestedEndDateReview(String hasrequestedEndDateReview) {
		this.hasrequestedEndDateReview = hasrequestedEndDateReview;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPsId() {
		return psId;
	}
	public void setPsId(String psId) {
		this.psId = psId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getOfficeCode() {
		return officeCode;
	}
	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	String name;
	String office;
	String officeCode;
	String region;
	String regionCode;
	String lastLogin;
	
	
	
}
