package com.allegis.sfdc.reports;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class Case implements Comparable<Case>{
	String id;
	String subject;
	String type;
	Date date;
	String contactPSId;
	String ownerId;
	String caseNumber;
	int daysToEndDate;
	
	String positionId;
	Date oldEndDate;
	
	public int getDaysToEndDate() {
		return daysToEndDate;
	}
	public void setDaysToEndDate(String daysToEndDate) {
		if (StringUtils.isBlank(daysToEndDate)) {
			this.daysToEndDate = 0;
		} else {
			this.daysToEndDate = Integer.parseInt(daysToEndDate);
		}
	}
	
	public String getPositionId() {
		return positionId;
	}
	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}
	public Date getOldEndDate() {
		return oldEndDate;
	}
	public void setOldEndDate(Date oldEndDate) {
		this.oldEndDate = oldEndDate;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getContactPSId() {
		return contactPSId;
	}
	public void setContactPSId(String contactPSId) {
		this.contactPSId = contactPSId;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	@Override
	public int compareTo(Case o) {
		if(this.getDate().compareTo(o.getDate()) > 0) {
			return 1;
		}
		return 0;
	}
}
