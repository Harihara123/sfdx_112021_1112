package com.allegis.sfdc.reports;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;

public class ReportUtil {


	public static void main(String[] args) throws ParseException, IOException {
		String rootDir = "C://Users//rchakrab//Documents//Work//Communities//Reports";
		SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("M/d/yyyy HH:mm aaa");

		Date TEK_CUT_OFF_DATE = sf.parse("06/03/2017");
		Date AEROTEK_CUT_OFF_DATE = sf.parse("07/21/2017");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do {
			System.out.print("Enter the working directory: (e.g. C://Users//rchakrab//Documents//Work//Communities//Reports) ");
			rootDir = br.readLine();			
		} while (StringUtils.isBlank(rootDir));

		
		List<Report> tekReportData = new ArrayList<Report>();
		List<Report> tekSubVendorReportData = new ArrayList<Report>();
		List<Report> aerotekReportData = new ArrayList<Report>();
		List<Report> aerotekCanadaReportData = new ArrayList<Report>();
		List<Report> astonCarterData = new ArrayList<Report>();
		List<Report> tgsW2ReportData = new ArrayList<Report>();
		List<Report> tgsSubVendorReportData = new ArrayList<Report>();



		FileIO fileIO = new FileIO();
		Map<String, List<TalentWorkHistory>> workHistoryMap = fileIO.readWorkHistoryFile(rootDir);
		Map<String, User> userMap = fileIO.readUserFile(rootDir);
		Map<String, List<Case>> caseMap = fileIO.readCaseFile(rootDir);

		List<Case> caseList;
		List<TalentWorkHistory> twhList;
		Set<String> casePositionNumbers = new HashSet<String>();
		long extensionDays = 0;
		int endDateCaseCount = 0;
		int jobSearchCaseCount = 0;
		boolean endDateCaseFound = false, jobSearchCaseFound = false;
		String sourceId;

		System.out.println(userMap.keySet().size() + " user records");

		System.out.println(caseMap.keySet().size() + " users with case records");

		System.out.println(workHistoryMap.keySet().size() + " users with position records");

		//Core logic to generate the report
		
		//Iterate over each user record
		for(String psId: userMap.keySet()) {
			extensionDays = 0;
			endDateCaseCount = 0;
			jobSearchCaseCount = 0;
			endDateCaseFound = false;
			jobSearchCaseFound = false;
			casePositionNumbers.clear();

			Report reportRecord = new Report();
			//Add each user to the report
			reportRecord.setPsId(psId);
			/** If the user has logged in set login flag to true. TEK and Aerotek has a special cut-off
			 * date logic, which is taken into account to make sure the percentages are calculated based on 
			 * data, when the appropriate fields were added to generate this report.
			 */
			if (!StringUtils.isBlank(userMap.get(psId).getLastLogin())) {
				if((userMap.get(psId).getProfileName().equalsIgnoreCase("Aerotek Community User") 
						&& dateTimeFormat.parse(userMap.get(psId).getLastLogin()).after(AEROTEK_CUT_OFF_DATE)) || 
						(userMap.get(psId).getProfileName().equalsIgnoreCase("Teksystem Community User") 
								&& dateTimeFormat.parse(userMap.get(psId).getLastLogin()).after(TEK_CUT_OFF_DATE)) ||
						(userMap.get(psId).getProfileName().equalsIgnoreCase("AstonCarter Community User")) ||
						(userMap.get(psId).getProfileName().equalsIgnoreCase("Aerotek Canada Community User")) ||
						(userMap.get(psId).getProfileName().equalsIgnoreCase("TekSystem Subvendor Community User")) ||
						(userMap.get(psId).getProfileName().equalsIgnoreCase("TGS W2 Community User")) ||
						(userMap.get(psId).getProfileName().equalsIgnoreCase("TGS SubVendor Community User"))) {
					reportRecord.setHasLoggedInAfterCutOffDate("Yes");
				}
			}
			//Set user data points to the report 
			reportRecord.setName(userMap.get(psId).getName());
			reportRecord.setContactId(userMap.get(psId).getContactId());
			reportRecord.setOfficeCode(userMap.get(psId).getOfficeCode());
			reportRecord.setOffice(userMap.get(psId).getOffice());
			reportRecord.setRegionCode(userMap.get(psId).getRegionCode());
			reportRecord.setRegion(userMap.get(psId).getRegion());
			if (!StringUtils.isBlank(userMap.get(psId).getLastLogin())) {
				reportRecord.setHasLoggedIn("Yes");
			} 		
			/**
			 * Loop through cases for each talent.			 * 
			 */
			if (caseMap.get(psId) != null) {
				caseList = caseMap.get(psId);
				Collections.sort(caseList);
				for (Case c: caseMap.get(psId)) {
					if ((userMap.get(psId).getProfileName().equalsIgnoreCase("Aerotek Community User") 
							&& AEROTEK_CUT_OFF_DATE.before(c.getDate())) || 
							(userMap.get(psId).getProfileName().equalsIgnoreCase("Teksystem Community User") 
									&& TEK_CUT_OFF_DATE.before(c.getDate())) ||
							(userMap.get(psId).getProfileName().equalsIgnoreCase("AstonCarter Community User")) ||
							(userMap.get(psId).getProfileName().equalsIgnoreCase("Aerotek Canada Community User")) ||
							(userMap.get(psId).getProfileName().equalsIgnoreCase("TekSystem Subvendor Community User")) ||
							(userMap.get(psId).getProfileName().equalsIgnoreCase("TGS W2 Community User")) ||
							(userMap.get(psId).getProfileName().equalsIgnoreCase("TGS SubVendor Community User"))) {
						//Apply condition to filter only end date and job search cases
						if((c.getSubject().toLowerCase().indexOf("end date") >= 0) || (c.getSubject().toLowerCase().indexOf("job") >= 0)) {
							endDateCaseCount++;

							reportRecord.setHasrequestedEndDateReview("Yes");
							reportRecord.setEndDateCaseRequestDate(sf.format(c.getDate()));
							reportRecord.setCaseNumber(c.getCaseNumber());
							/**
							 * Check if the position number is captured on the case. Otherwise the mapping with 
							 * talent work history cannot be done
							 */
							if(!StringUtils.isBlank(c.getPositionId())) {
								twhList = workHistoryMap.get(psId);
								if (twhList != null) {
									/**
									 * Loop over all position history records for the user
									 */
									for (TalentWorkHistory twh: twhList) {
										sourceId = psId + c.getPositionId();
										if(c.getPositionId().equals(twh.getPositionId()) && !casePositionNumbers.contains(sourceId) && twh.getSourceId().equals(sourceId)) {
											/**
											 * Add the position number to a list, so that it does not reflect more than once
											 * in calculating the extension days. That means if a person a created multiple 
											 * end date and job access cases the extension for the position is calculated only 
											 * once for all types of cases.
											 */
											casePositionNumbers.add(sourceId);
											//System.out.println("Calculating extension days between " + c.getOldEndDate() + " and " + twh.getEndDate());
											if (c.getOldEndDate() != null) {
												extensionDays = getDateDiff(c.getOldEndDate(), twh.getEndDate(), TimeUnit.DAYS);
												//System.out.println("Result is  " + extensionDays);
												if (extensionDays > 0) {
													reportRecord.setExtensionGranted("Yes");
													if (!StringUtils.isBlank(reportRecord.getExtensionDays())) {
														extensionDays += Long.parseLong(reportRecord.getExtensionDays());
														reportRecord.setExtensionDays(String.valueOf(extensionDays));
													} else {
														reportRecord.setExtensionDays(String.valueOf(extensionDays));
													}
												}
											}

										}
									}
								}

							}

						}
					}

				}
				if(endDateCaseCount > 0) {
					reportRecord.setNumberOfEndDateCases(String.valueOf(endDateCaseCount));
				}

			}


			if (!StringUtils.isBlank(userMap.get(psId).getLinkedInToken())) {
				reportRecord.setHasImportedLinkedinData("Yes");
			} 
			if(userMap.get(psId).getProfileName().equalsIgnoreCase("Aerotek Community User")) {
				reportRecord.setOpco("ONS");
				aerotekReportData.add(reportRecord);
			} else if (userMap.get(psId).getProfileName().equalsIgnoreCase("Teksystem Community User")) {
				reportRecord.setOpco("TEK");
				tekReportData.add(reportRecord);
			} else if (userMap.get(psId).getProfileName().equalsIgnoreCase("TekSystem Subvendor Community User")) {
				reportRecord.setOpco("TEKSUB");
				tekSubVendorReportData.add(reportRecord);
			} else if (userMap.get(psId).getProfileName().equalsIgnoreCase("Aerotek Canada Community User")) {
				reportRecord.setOpco("ONSCA");
				aerotekCanadaReportData.add(reportRecord);
			} else if (userMap.get(psId).getProfileName().equalsIgnoreCase("AstonCarter Community User")) {
				reportRecord.setOpco("AC");
				astonCarterData.add(reportRecord);
			} else if (userMap.get(psId).getProfileName().equalsIgnoreCase("TGS W2 Community User")) {
				reportRecord.setOpco("TGSW2");
				tgsW2ReportData.add(reportRecord);
			} else if (userMap.get(psId).getProfileName().equalsIgnoreCase("TGS SubVendor Community User")) {
				reportRecord.setOpco("TGSSUB");
				tgsSubVendorReportData.add(reportRecord);
			}

		}

		fileIO.writeReportFile(rootDir + "//TEKReport.csv", tekReportData, "June 3rd");
		fileIO.writeReportFile(rootDir + "//AerotekReport.csv", aerotekReportData, "July 21st");
		fileIO.writeReportFile(rootDir + "//AstonCarterReport.csv", astonCarterData, "");
		fileIO.writeReportFile(rootDir + "//AerotekCanadaReport.csv", aerotekCanadaReportData, "");
		fileIO.writeReportFile(rootDir + "//TEKSubVendorReport.csv", tekSubVendorReportData, "");
		fileIO.writeReportFile(rootDir + "//TGSW2.csv", tgsW2ReportData, "");
		fileIO.writeReportFile(rootDir + "//TGSSubVendor.csv", tgsSubVendorReportData, "");
	}

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}

}
