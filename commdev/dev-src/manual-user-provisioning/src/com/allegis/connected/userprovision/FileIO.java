package com.allegis.connected.userprovision;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class FileIO {
	
	static final boolean sample = false;
	
	static List<String[]> amRecList = new ArrayList<String[]>();
	
	Set<String> invalidDivisionIds = new HashSet<String>();
	Set<String> invalidTalentOfficeCodes = new HashSet<String>();
	Set<String> invalidRecruiterOfficeCodes = new HashSet<String>();
	Set<String> internalUserIds = new HashSet<String>();
	Set<String> exceptionIds = new HashSet<String>();
	Set<String> formerTalents = new HashSet<String>();
	Set<String> invalidPSCustId = new HashSet<String>();
	Set<String> invalidPlacementTypes = new HashSet<String>();
	Set<String> invalidSegment = new HashSet<String>();
	Set<String> blacklistedUsers = new HashSet<String>();
	Set<String> existingUsers = new HashSet<String>();
	Set<String> missingName = new HashSet<String>();
	Set<String> missingEmail = new HashSet<String>();
	Set<String> missingRWSId = new HashSet<String>();
	Set<String> duplicateEmail = new HashSet<String>();
	
	
	Map<String, Map<String, Organization>> officeMap = new HashMap<String, Map<String, Organization>>();
	Map<String, Map<String, String>> regionMap = new HashMap<String, Map<String, String>>();
	
	public Map<String, SObject> readInternalUserFile(String fileName) {
		Map<String, SObject> userMap = new HashMap<String, SObject>();
		try {

			CsvReader internalUser = new CsvReader(fileName);

			internalUser.readHeaders();

			while (internalUser.readRecord()) {
				String psId = internalUser.get("#RECR_ID");
				InternalUser user = new InternalUser();
				user.setPeoplesoftId(psId);
				userMap.put(psId, user);

				// perform program logic here
				//System.out.println(psId + " : " + accntMgrId + " : " + recId);
			}

			internalUser.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userMap;
	}
	
	public Map<String, SObject> readTalentFile(String fileName) {
		Map<String, SObject> talentMap = new HashMap<String, SObject>();
		try {

			CsvReader talentUser = new CsvReader(fileName);

			talentUser.readHeaders();

			while (talentUser.readRecord()) {
				String psId = talentUser.get("#PS_EMPLID");
				String accntMgrId = talentUser.get("ACCT_MGR_ID");
				String recId = talentUser.get("RECRUITER_ID");
				
				TalentAccount talentAccount = new TalentAccount();
				talentAccount.setPeoplesoftId(psId);
				talentAccount.setAccountManagerPSId(accntMgrId);
				talentAccount.setRecruiterPSId(recId);
				talentMap.put(psId, talentAccount);

				// perform program logic here
				System.out.println(psId + " : " + accntMgrId + " : " + recId);
			}

			talentUser.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return talentMap;
	}
	
	
	public Map<String, SObject> readRawTalentFile(String fileName, Map<String, TalentUser> talentUserExportMap) {
		Map<String, SObject> talentFileMap = new HashMap<String, SObject>();
		String duplicatePSIds = "";
		String rwsId;
		Set<String> existingEmail = new HashSet<String>();
		Set<String> existingPsId = new HashSet<String>();
		
		for(TalentUser exisintUser : talentUserExportMap.values()) {
			existingEmail.add(exisintUser.getEmail());
			existingPsId.add(exisintUser.getPeoplesoftId());
		}
		
		try {

			CsvReader talentUser = new CsvReader(fileName);

			talentUser.readHeaders();
			long recordCount = 1;
			long loopCount = 0;
			long skippedRecordCount = 0;
			
			if (GenerateDataLoadFiles.massUserLoad) {
				loadAMRecMappingForMassUpload();
			}

			while (talentUser.readRecord()) {
				String psId = StringUtils.leftPad(talentUser.get("#PS_EMPLID"), 8, '0').trim();				
				TalentFile fileRecord = new TalentFile();
				fileRecord.setPsId(psId);
				fileRecord.setPsCustId(talentUser.get("PS_CUST_ID"));
				fileRecord.setPsCustName(talentUser.get("PS_CUST_NAME"));
				if (talentUser.get("RWS_CANDIDATE_ID").indexOf(".") > 0) {
					rwsId = talentUser.get("RWS_CANDIDATE_ID");
					fileRecord.setRwscandidateid(rwsId.substring(0, rwsId.indexOf(".")));
				} else {
					fileRecord.setRwscandidateid(talentUser.get("RWS_CANDIDATE_ID"));
				}				
				fileRecord.setContrpaystubtype(talentUser.get("CONTR_PAYSTUB_TYPE"));
				fileRecord.setContrtimecapturetype(talentUser.get("CONTR_TIME_CAPTURE_TYPE"));
				fileRecord.setControfficeid(StringUtils.leftPad(talentUser.get("REQ_OFFICE_ID"),5,"0"));
				fileRecord.setControfficename(talentUser.get("CONTR_OFFICE_NAME"));
				fileRecord.setContrlastname(talentUser.get("CONTR_LAST_NAME"));
				fileRecord.setContrfirstname(talentUser.get("CONTR_FIRST_NAME"));
				fileRecord.setContrmiddlename(talentUser.get("CONTR_MIDDLE_NAME"));
				fileRecord.setContrnameprefix(talentUser.get("CONTR_NAME_PREFIX"));
				fileRecord.setContrnamesuffix(talentUser.get("NAME_SUFFIX").trim());
				fileRecord.setEmpstatus(talentUser.get("EMP_STATUS"));
				fileRecord.setEmplClassCategory(talentUser.get("EMPL_CLASS_CTGRY"));
				fileRecord.setJobreqno(talentUser.get("JOB_REQ_NO"));
				fileRecord.setDivisionNumber(talentUser.get("DIVISION_NUMBER"));
				fileRecord.setDivisionName(talentUser.get("DIVISION_NAME"));
				fileRecord.setJobreqjobcode(talentUser.get("JOB_REQ_JOBCODE"));
				fileRecord.setJobreqjobcodedesc(talentUser.get("JOB_REQ_JOB_CODE_DESC"));
				fileRecord.setClientjobtitle(talentUser.get("CLIENT_JOB_TITLE"));
				fileRecord.setPlacementType(talentUser.get("PLACEMENT_TYPE"));
				fileRecord.setStartdate(talentUser.get("START_DATE"));
				fileRecord.setEnddate(talentUser.get("END_DATE"));
				if (!StringUtils.isBlank(talentUser.get("EMAIL_ADDR"))) {
					fileRecord.setEmailaddr(talentUser.get("EMAIL_ADDR").trim());
				}				
				fileRecord.setWorkphnum(talentUser.get("WORK_PH_NUM"));
				fileRecord.setMobilephnum(talentUser.get("MOBILE_PH_NUM"));
				fileRecord.setHomephnum(talentUser.get("HOME_PH_NUM"));
				fileRecord.setAddrline1(talentUser.get("ADDR_LINE1"));
				fileRecord.setAddrline2(talentUser.get("ADDR_LINE2"));
				fileRecord.setAddrline3(talentUser.get("ADDR_LINE3"));
				fileRecord.setAddrline4(talentUser.get("ADDR_LINE4"));
				fileRecord.setCity(talentUser.get("CITY"));
				fileRecord.setStateprov(talentUser.get("STATE_PROV"));
				fileRecord.setCountry(talentUser.get("COUNTRY"));
				fileRecord.setPostalcode(StringUtils.leftPad(talentUser.get("POSTAL_CODE").trim(), 5, '0'));
				if (GenerateDataLoadFiles.massUserLoad) {
					String[] amRec = getAMRec(loopCount);
					fileRecord.setAcctmgrid(StringUtils.leftPad(amRec[0], 8, '0'));
					fileRecord.setAcctmgrname("");
					fileRecord.setRecruiterid(StringUtils.leftPad(amRec[1], 8, '0'));
					fileRecord.setRecruitername("");
				} else {
					fileRecord.setAcctmgrid(StringUtils.leftPad(talentUser.get("ACCT_MGR_ID"), 8, '0'));
					fileRecord.setAcctmgrname(talentUser.get("ACCT_MGR_NAME"));
					fileRecord.setRecruiterid(StringUtils.leftPad(talentUser.get("RECRUITER_ID"), 8, '0'));
					fileRecord.setRecruitername(talentUser.get("RECRUITER_NAME"));
				}
				fileRecord.setRecruiteroffnum(StringUtils.leftPad(talentUser.get("RECRUITER_OFF_NUM"), 5, "0"));
				fileRecord.setSegment(talentUser.get("SEGMENT"));
				loopCount++;
				if((talentFileMap.get(fileRecord.getPsId()) != null)) {
					duplicatePSIds += fileRecord.getPsId() + ", ";
				}
				
				if (doesSatisfyfilterV2(fileRecord, existingPsId, existingEmail)) {
					talentFileMap.put(psId, fileRecord);
				} else {
					skippedRecordCount++;
				}
				
				
			}
			System.out.println("Total users in talent file " + loopCount);
			GenerateDataLoadFiles.uploadReport.append("Total users in talent file " + loopCount).append("\n");
			System.out.println("Skipped " + skippedRecordCount +" users in talent file due to filter and/or they already exist");
			GenerateDataLoadFiles.uploadReport.append("Skipped " + skippedRecordCount +" users in talent file due to filter and/or they already exist").append("\n");
			System.out.println("Duplicate talents in source file : " + duplicatePSIds);
			GenerateDataLoadFiles.uploadReport.append("Duplicate talents in source file : " + duplicatePSIds).append("\n");
			talentUser.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(invalidTalentOfficeCodes.size() > 0) {
			System.out.println(invalidTalentOfficeCodes.size() + " talents are filtered for invalid talent office codes");
			System.out.println(convertSetToString(invalidTalentOfficeCodes));
			GenerateDataLoadFiles.uploadReport.append(invalidTalentOfficeCodes.size() + " talents are filtered for invalid talent office codes").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(invalidTalentOfficeCodes)).append("\n");
		}
		
		if(invalidDivisionIds.size() > 0) {
			System.out.println(invalidDivisionIds.size() + " talents are filtered for invalid division");
			System.out.println(convertSetToString(invalidDivisionIds));
			GenerateDataLoadFiles.uploadReport.append(invalidDivisionIds.size() + " talents are filtered for invalid division").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(invalidDivisionIds)).append("\n");
		}
		
		if(internalUserIds.size() > 0) {
			System.out.println(internalUserIds.size() + " talents are filtered as they are internal");
			System.out.println(convertSetToString(internalUserIds));
			GenerateDataLoadFiles.uploadReport.append(internalUserIds.size() + " talents are filtered as they are internal").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(internalUserIds)).append("\n");
		}
		
		if(exceptionIds.size() > 0) {
			System.out.println(exceptionIds.size() + " talents are filtered due to error");
			System.out.println(convertSetToString(exceptionIds));
			GenerateDataLoadFiles.uploadReport.append(exceptionIds.size() + " talents are filtered due to error").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(exceptionIds)).append("\n");
		}
		
		if(invalidPlacementTypes.size() > 0) {
			System.out.println(invalidPlacementTypes.size() + " talents are filtered for invalid placement types");
			System.out.println(convertSetToString(invalidPlacementTypes));
			GenerateDataLoadFiles.uploadReport.append(invalidPlacementTypes.size() + " talents are filtered for invalid placement types").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(invalidPlacementTypes)).append("\n");
		}
		if(invalidRecruiterOfficeCodes.size() > 0) {
			System.out.println(invalidRecruiterOfficeCodes.size() + " talents are filtered for invalid recruiter office codes");
			System.out.println(convertSetToString(invalidRecruiterOfficeCodes));
			GenerateDataLoadFiles.uploadReport.append(invalidRecruiterOfficeCodes.size() + " talents are filtered for invalid recruiter office codes").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(invalidRecruiterOfficeCodes)).append("\n");
		}
		if(invalidPSCustId.size() > 0) {
			System.out.println(invalidPSCustId.size() + " talents are filtered for invalid PS Customer Ids");
			System.out.println(convertSetToString(invalidPSCustId));
			GenerateDataLoadFiles.uploadReport.append(invalidPSCustId.size() + " talents are filtered for invalid PS Customer Ids").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(invalidPSCustId)).append("\n");
		}
		if(formerTalents.size() > 0) {
			System.out.println(formerTalents.size() + " talents are filtered because they are formers");
			System.out.println(convertSetToString(formerTalents));
			GenerateDataLoadFiles.uploadReport.append(formerTalents.size() + " talents are filtered because they are formers").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(formerTalents)).append("\n");
		}
		if(existingUsers.size() > 0) {
			System.out.println(existingUsers.size() + " talents are filtered because they already exists");
			System.out.println(convertSetToString(existingUsers));
			GenerateDataLoadFiles.uploadReport.append(existingUsers.size() + " talents are filtered because they already exists").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(existingUsers)).append("\n");
		}
		if(duplicateEmail.size() > 0) {
			System.out.println(duplicateEmail.size() + " talents are filtered because their email already exists");
			System.out.println(convertSetToString(duplicateEmail));
			GenerateDataLoadFiles.uploadReport.append(duplicateEmail.size() + " talents are filtered because their email already exists").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(duplicateEmail)).append("\n");
		}
		if(missingName.size() > 0) {
			System.out.println(missingName.size() + " talents are filtered because they dont have name");
			System.out.println(convertSetToString(missingName));
			GenerateDataLoadFiles.uploadReport.append(missingName.size() + " talents are filtered because they dont have name").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(missingName)).append("\n");
			GenerateDataLoadFiles.errorReport.append(missingName.size() + " talents are filtered because they dont have name").append("\n");
			GenerateDataLoadFiles.errorReport.append(convertSetToString(missingName)).append("\n");
		}
		if(missingEmail.size() > 0) {
			System.out.println(missingEmail.size() + " talents are filtered because they dont have email");
			System.out.println(convertSetToString(missingEmail));
			GenerateDataLoadFiles.uploadReport.append(missingEmail.size() + " talents are filtered because they dont have email").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(missingEmail)).append("\n");
			GenerateDataLoadFiles.errorReport.append(missingEmail.size() + " talents are filtered because they dont have email").append("\n");
			GenerateDataLoadFiles.errorReport.append(convertSetToString(missingEmail)).append("\n");
		}
		if(invalidSegment.size() > 0) {
			System.out.println(invalidSegment.size() + " talents are filtered because of invalid segment");
			System.out.println(convertSetToString(invalidSegment));
			GenerateDataLoadFiles.uploadReport.append(invalidSegment.size() + " talents are filtered because of invalid segment").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(invalidSegment)).append("\n");
			
		}
		if(blacklistedUsers.size() > 0) {
			System.out.println(blacklistedUsers.size() + " talents are filtered because of blacklisted Ids");
			System.out.println(convertSetToString(blacklistedUsers));
			GenerateDataLoadFiles.uploadReport.append(blacklistedUsers.size() + " talents are filtered because of blacklisted Ids").append("\n");
			GenerateDataLoadFiles.uploadReport.append(convertSetToString(blacklistedUsers)).append("\n");
			
		}
		if(missingName.size() > 0 || missingEmail.size() > 0) {			
			GenerateDataLoadFiles.errorReport.append("Re-Export the talents with following Peoplesoft Ids").append("\n");
			GenerateDataLoadFiles.errorReport.append(convertSetToString(missingName)).append(",");
			GenerateDataLoadFiles.errorReport.append(convertSetToString(missingEmail)).append("\n");
		}		
		return talentFileMap;
	}
	
	public Map<String, SObject> readInternalUserExportFile(String fileName) {
		Map<String, SObject> userMap = new HashMap<String, SObject>();
		try {

			CsvReader userFile = new CsvReader(fileName);

			userFile.readHeaders();

			while (userFile.readRecord()) {
				String id = userFile.get("Id");
				String isActive = userFile.get("IsActive");
				String psId = StringUtils.leftPad(userFile.get("Peoplesoft_Id__c"), 8, '0');
				InternalUser internalUser = new InternalUser();
				
				internalUser.setId(id);
				internalUser.setIsActive(isActive);
				internalUser.setPeoplesoftId(psId);
				userMap.put(psId, internalUser);
			}

			userFile.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userMap;
	}
	
	
	public Map<String, TalentUser> readTalentUserExportFile(String fileName) {
		Map<String, TalentUser> userMap = new HashMap<String, TalentUser>();
		try {

			CsvReader userFile = new CsvReader(fileName);

			userFile.readHeaders();

			while (userFile.readRecord()) {
				String psId = StringUtils.leftPad(userFile.get("Peoplesoft Id"), 8, '0');
				TalentUser talentUser = new TalentUser();
				
				
				talentUser.setPeoplesoftId(psId);
				talentUser.setEmail(userFile.get("Email"));
				userMap.put(psId, talentUser);
			}

			userFile.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userMap;
	}
	
	
	
	public void writeInternalUserToFile(String fileName, Set<String> psIds) {
		
			
		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName, false), ',');
			
			
			csvOutput.write("PeoplesoftId");
			csvOutput.endRecord();
			
			for (String psId: psIds) {
				csvOutput.write(psId);
				csvOutput.endRecord();
			}
			
			
			
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeAccountUploadFile(String fileName, List<TalentAccount> accountUpload) {
		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName, false), ',');
			
			
			csvOutput.write("Name");
			csvOutput.write("OwnerId");
			csvOutput.write("RecordTypeId");
			csvOutput.write("Talent_Recruiter__c");
			csvOutput.write("Talent_Account_Manager__c");
			csvOutput.write("Peoplesoft_Id__c");
			csvOutput.write("Source_System_Id__c");
			csvOutput.write("Talent_Ownership__c");
			csvOutput.write("Talent_Visibility__c");
			csvOutput.write("Talent_Personalization__c");			
			
			csvOutput.endRecord();
			
			for (TalentAccount account: accountUpload) {
				csvOutput.write(account.getAccountName());
				csvOutput.write(account.getOwnerId());
				csvOutput.write(account.getRecordTypeId());
				csvOutput.write(account.getRecruiterId());
				csvOutput.write(account.getAccountManagerId());
				csvOutput.write(account.getPeoplesoftId());
				csvOutput.write(account.getSourceSystemId());
				csvOutput.write(account.getOwnership());
				csvOutput.write(account.getVisibility());
				csvOutput.write(account.getTalentPreferences());				
				csvOutput.endRecord();
				if(sample) {
					break;
				}
			}
			
			
			
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeContactUploadFile(String fileName, List<TalentContact> contactUpload) {
		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName, false), ',');
			
			csvOutput.write("AccountId");
			csvOutput.write("FirstName");
			csvOutput.write("LastName");
			csvOutput.write("Suffix");
			csvOutput.write("Peoplesoft_Id__c");
			csvOutput.write("Source_System_Id__c");
			csvOutput.write("RecordTypeId");
			csvOutput.write("Phone");
			csvOutput.write("Email");			
			csvOutput.write("Title");
			csvOutput.write("MailingStreet");
			csvOutput.write("MailingCity");
			csvOutput.write("MailingState");
			csvOutput.write("MailingPostalCode");
			csvOutput.write("MailingCountry");
			
			csvOutput.endRecord();
			
			for (TalentContact contact: contactUpload) {
				csvOutput.write(contact.getAccountId());
				csvOutput.write(contact.getFirstName());
				csvOutput.write(contact.getLastName());
				csvOutput.write(contact.getSuffix());
				csvOutput.write(contact.getPeoplesoftId());
				csvOutput.write(contact.getSrcSystemId());
				csvOutput.write(contact.getRecordTypeId());
				csvOutput.write(contact.getPhone());
				csvOutput.write(contact.getEmail());
				
				csvOutput.write(contact.getTitle());
				csvOutput.write(contact.getAddress());
				csvOutput.write(contact.getCity());
				csvOutput.write(contact.getState());
				csvOutput.write(contact.getZip());
				csvOutput.write(contact.getCountry());
				csvOutput.endRecord();
				if(sample) {
					break;
				}
			}
			
			
			
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeUserUploadFile(String fileName, List<TalentUser> userUpload) throws FileNotFoundException {
		Organization org;
		readOrgFile();
		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName, false), ',');
			
			csvOutput.write("ContactId");
			csvOutput.write("FirstName");
			csvOutput.write("LastName");
			csvOutput.write("UserName");
			csvOutput.write("Email");
			csvOutput.write("ProfileId");
			csvOutput.write("Peoplesoft_Id__c");
			csvOutput.write("FederationIdentifier");
			csvOutput.write("Alias");
			csvOutput.write("Nickname");
			csvOutput.write("Title");			
			csvOutput.write("Phone");
			csvOutput.write("Street");
			csvOutput.write("City");
			csvOutput.write("State");
			csvOutput.write("PostalCode");
			csvOutput.write("Country");
			csvOutput.write("Office__c");
			csvOutput.write("Office_Code__c");
			csvOutput.write("Region__c");
			csvOutput.write("Region_Code__c");
			csvOutput.write("Division__c");
			csvOutput.write("Division");
			csvOutput.write("Opco");
			csvOutput.write("Position_Service_finished__c");
			csvOutput.write("Employee_service_Finished__c");
			csvOutput.write("Azure_Service_Finished__c");
			csvOutput.write("First_Time_Login__c");
			
			csvOutput.write("User_Application__c");
			csvOutput.write("TimeZoneSidKey");
			csvOutput.write("LocaleSidKey");
			csvOutput.write("EmailEncodingKey");
			csvOutput.write("LanguageLocaleKey");
			csvOutput.write("UserPreferencesDisableAllFeedsEmail");
			csvOutput.write("UserPreferencesDisableBookmarkEmail");
			csvOutput.write("UserPreferencesDisableChangeCommentEmail");
			csvOutput.write("UserPreferencesDisableFollowersEmail");
			csvOutput.write("UserPreferencesDisableLaterCommentEmail");
			csvOutput.write("UserPreferencesDisableLikeEmail");
			csvOutput.write("UserPreferencesDisableMentionsPostEmail");
			csvOutput.write("UserPreferencesDisableMessageEmail");
			csvOutput.write("UserPreferencesDisableProfilePostEmail");
			csvOutput.write("UserPreferencesDisableSharePostEmail");
			csvOutput.write("UserPreferencesDisCommentAfterLikeEmail");
			csvOutput.write("UserPreferencesDisMentionsCommentEmail");
			csvOutput.write("UserPreferencesDisProfPostCommentEmail");
			
			csvOutput.endRecord();
			
			for (TalentUser user: userUpload) {
				csvOutput.write(user.getContactId());
				csvOutput.write(user.getFirstName());
				csvOutput.write(user.getLastName());
				csvOutput.write(user.getUserName());
				csvOutput.write(user.getEmail());
				csvOutput.write(user.getProfileId());
				csvOutput.write(user.getPeoplesoftId());
				csvOutput.write(user.getFederationId());
				csvOutput.write(user.getAlias());
				csvOutput.write(user.getNickName());
				csvOutput.write(user.getTitle());
				csvOutput.write(user.getPhone());
				csvOutput.write(user.getStreet());
				csvOutput.write(user.getCity());
				csvOutput.write(user.getState());
				csvOutput.write(user.getZip());
				csvOutput.write(user.getCountry());
				org = officeMap.get(user.getOpco()).get(user.getOfficeCode());
				
				if(org != null) {
					csvOutput.write(user.getOpco() + "-" + org.getOfficeName() + "-" + org.getOfficeCode());
					csvOutput.write(org.getOfficeCode());
					csvOutput.write(org.getRegionName());
					csvOutput.write(org.getRegionCode());
				} else{
					csvOutput.write("");
					csvOutput.write("");
					csvOutput.write("");
					csvOutput.write("");
				}
				
				csvOutput.write(user.getDivisionName());
				csvOutput.write(user.getDivisionNumber());
				csvOutput.write(user.getOpco());
				csvOutput.write(user.getPositionServiceFinished());
				csvOutput.write(user.getEmployeeServiceFinished());
				csvOutput.write(user.getAzureServiceFinished());
				csvOutput.write(user.getFirstTimeLogin());
				
				csvOutput.write(user.getUserApplication());			
				csvOutput.write(user.getTimeZoneSidKey());
				csvOutput.write(user.getLocaleSidKey());
				csvOutput.write(user.getEmailEncodingKey());
				csvOutput.write(user.getLanguageLocaleKey());
				csvOutput.write(user.getUserPreferencesDisableAllFeedsEmail());
				csvOutput.write(user.getUserPreferencesDisableBookmarkEmail());
				csvOutput.write(user.getUserPreferencesDisableChangeCommentEmail());
				csvOutput.write(user.getUserPreferencesDisableFollowersEmail());
				csvOutput.write(user.getUserPreferencesDisableLaterCommentEmail());
				csvOutput.write(user.getUserPreferencesDisableLikeEmail());
				csvOutput.write(user.getUserPreferencesDisableMentionsPostEmail());
				csvOutput.write(user.getUserPreferencesDisableMessageEmail());
				csvOutput.write(user.getUserPreferencesDisableProfilePostEmail());
				csvOutput.write(user.getUserPreferencesDisableSharePostEmail());
				csvOutput.write(user.getUserPreferencesDisCommentAfterLikeEmail());
				csvOutput.write(user.getUserPreferencesDisMentionsCommentEmail());
				csvOutput.write(user.getUserPreferencesDisProfPostCommentEmail());
				
				
				csvOutput.endRecord();
				if(sample) {
					break;
				}
			}
			
			
			
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeWorkhistoryUploadFile(String fileName, List<TalentWorkHistory> workhistoryUpload) {
		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName, false), ',');
			
			csvOutput.write("Talent");
			csvOutput.write("Start_Date__c");
			csvOutput.write("End_Date__c");
			
			
			csvOutput.endRecord();
			
			for (TalentWorkHistory work: workhistoryUpload) {
				csvOutput.write(work.getAccountId());
				csvOutput.write(work.getStartDate());
				csvOutput.write(work.getEndDate());
				csvOutput.endRecord();
				
				if(sample) {
					break;
				}
			}
			
			
			
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**public Map<String, SuccessFile> readSuccessFile(String fileName) throws FileNotFoundException {
		Map<String, SuccessFile> successFileMap = new HashMap<String, SuccessFile>();
		try {

			CsvReader successFile = new CsvReader(fileName);

			successFile.readHeaders();

			while (successFile.readRecord()) {
				SuccessFile rec = new SuccessFile();
				rec.setSfdcId(successFile.get("ID"));
				rec.setPsId(successFile.get("PEOPLESOFT_ID__C"));
				successFileMap.put(successFile.get("PEOPLESOFT_ID__C"), rec);
			}

			successFile.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return successFileMap;
	}*/
	
	public Map<String, SuccessFile> readSuccessFile(String path, String objectType) throws FileNotFoundException {
		Map<String, SuccessFile> successFileMap = new HashMap<String, SuccessFile>();
		List<File> files = parseDirectory(path, objectType);
		for(File f: files) {
			try {

				CsvReader successFile = new CsvReader(f.getAbsolutePath());

				successFile.readHeaders();

				while (successFile.readRecord()) {
					SuccessFile rec = new SuccessFile();								
					rec.setSfdcId(successFile.get("ID"));
					rec.setPsId(successFile.get("PEOPLESOFT_ID__C"));
					successFileMap.put(successFile.get("PEOPLESOFT_ID__C"), rec);
				}

				successFile.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw e;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return successFileMap;
	}
	
	public List<File> parseDirectory(String path, String objectType) throws FileNotFoundException {
		List<File> successFiles = new ArrayList<File>();
		System.out.println(path);
		File dir = new File(path + "\\Reports");
		if(dir.isDirectory()) {
			File[] files = dir.listFiles();
			for(File f: files) {
				System.out.println(f.getName());
				if(f.getName().startsWith("success")) {					
					if(objectType.equals("Account")) {
						if(isSuccessFileForObjectType(f, "001")) {
							System.out.println("Reading Account Success File " + f.getName());
							successFiles.add(f);
						}
					} else if (objectType.equals("Contact")) {
						if(isSuccessFileForObjectType(f, "003")) {
							System.out.println("Reading Contact Success File " +f.getName());
							successFiles.add(f);
						}
					}
				}
			}
		}		
		return successFiles;
	}
	
	public boolean isSuccessFileForObjectType(File f, String objectId) throws FileNotFoundException {
		boolean match = false;
		try {
			CsvReader successFile = new CsvReader(f.getAbsolutePath());
			successFile.readHeaders();
			String fileObjectId;
			while (successFile.readRecord()) {				
				fileObjectId = successFile.get("ID");
				if (fileObjectId.startsWith(objectId)) {
					match = true;
				}
				break;
			}
			successFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return match;
	}
	
	public Properties loadProperties(String fileName) throws Exception {
		Properties prop = new Properties();
		InputStream input = null;

		try {
			//input = this.getClass().getClassLoader().getResourceAsStream(fileName);
			prop.load(new FileInputStream(fileName));

			// get the property value and print it out
			if(prop.getProperty("approved_Placement_Type") == null) {
				throw new Exception("Placement Type Filter is not defined in properties file");
			}
			if(prop.getProperty("approved_Talent_Office_List") == null) {
				throw new Exception("Talent Office Filter is not defined in properties file");
			}
			if(prop.getProperty("approved_SRC_Office_List") == null) {
				throw new Exception("SRC Office Filter is not defined in properties file");
			}
			if(prop.getProperty("approved_Talent_Division") == null) {
				throw new Exception("Talent Division is not defined in properties file");
			}
			if(prop.getProperty("recruiter_Office_In_Pilot") == null) {
				throw new Exception("Recruiter Office enabled is not defined in properties file");
			}
			if(prop.getProperty("work_Directory") == null) {
				throw new Exception("Work Directory is not defined in properties file");
			}
			if(prop.getProperty("timeZone") == null) {
				throw new Exception("Timezone is not defined in properties file");
			}
			if(prop.getProperty("exclude_PS_Cust_Id") == null) {
				throw new Exception("Exclude PS CUST Id is not defined in properties file");
			}
			if(prop.getProperty("include_Formers") == null) {
				throw new Exception("Exclude formers is not defined in properties file");
			}
			if(prop.getProperty("exclude_Segments") == null) {
				throw new Exception("Exclude segments is not defined in properties file");
			}
			if(prop.getProperty("exclude_Talents") == null) {
				throw new Exception("Exclude talents is not defined in properties file");
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;

	}
	
	public Properties createAnnonymousApexFile(String fileName, Set<String> recIds, Set<String> amIds, String opco, String baseDir, String aliases) {
		Properties prop = new Properties();
		InputStream input = null;
		StringBuilder recIdsStr = new StringBuilder();
		StringBuilder amIdsStr = new StringBuilder();
		StringBuilder output = new StringBuilder();
		String profileId = "";
		
		String communityAccessPermSet = null, internalUserGroup = null;
		
		if (opco.equalsIgnoreCase("TEK")) {
			communityAccessPermSet = "Community_Access_TEKsystems";
			internalUserGroup = "Tek Community Internal Users";
			profileId = GenerateDataLoadFiles.tekCommunityUserProfileId;
		} else if (opco.equalsIgnoreCase("ONS")) {
			communityAccessPermSet = "Community_Access_Aerotek";
			internalUserGroup = "Aerotek Community Internal Users";
			profileId = GenerateDataLoadFiles.aerotekCommunityUserProfileId;
		}
		
		for(String id : recIds) {
			recIdsStr.append("'").append(id).append("',");
		}
		if (recIdsStr.length() > 1) {
			recIdsStr.deleteCharAt(recIdsStr.length()-1);
		}
		
		for(String id : amIds) {
			amIdsStr.append("'").append(id).append("',");
		}
		if (amIdsStr.length() > 1) {
			amIdsStr.deleteCharAt(amIdsStr.length()-1);
		}

		try {
			input = this.getClass().getClassLoader().getResourceAsStream(fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(input));
			String line = br.readLine();
			while (line != null) {
				
				if(line.indexOf("{{@RECRUITERPSID}}") != -1) {
					line = line.replace("{{@RECRUITERPSID}}", recIdsStr.toString());
				}
				if(line.indexOf("{{@AMPSID}}") != -1) {
					line = line.replace("{{@AMPSID}}", amIdsStr.toString());
				}
				if(line.indexOf("{{@COMMUNITYACCESSPERMSET}}") != -1) {
					line = line.replace("{{@COMMUNITYACCESSPERMSET}}", communityAccessPermSet);
				}
				if(line.indexOf("{{@INTERNALGROUP}}") != -1) {
					line = line.replace("{{@INTERNALGROUP}}", internalUserGroup);
				}
				if(line.indexOf("{{@COUNT}}") != -1) {
					line = line.replace("{{@COUNT}}", String.valueOf(recIds.size() + amIds.size()));
				}
				if(line.indexOf("{{@RECRUITERCOUNT}}") != -1) {
					line = line.replace("{{@RECRUITERCOUNT}}", String.valueOf(recIds.size()));
				}
				if(line.indexOf("{{@AMCOUNT}}") != -1) {
					line = line.replace("{{@AMCOUNT}}", String.valueOf(amIds.size()));
				}
				if(line.indexOf("{{@PROFILEID}}") != -1) {
					line = line.replace("{{@PROFILEID}}", profileId);
				}
				if(line.indexOf("{{@ALIASES}}") != -1) {
					line = line.replace("{{@ALIASES}}", aliases);
				}
				output.append(line).append("\n");
				line = br.readLine();
			}
			
			Writer fw = new FileWriter(baseDir + "\\AnnonmousApex.txt");
			fw.write(output.toString());
			fw.close();
			

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;

	}
	
	public void writeIdToFile(String fileName, Set<String> sfdcIds) {
		
		
		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName, false), ',');
			
			
			csvOutput.write("Id");
			csvOutput.endRecord();
			
			for (String psId: sfdcIds) {
				csvOutput.write(psId);
				csvOutput.endRecord();
			}
			
			
			
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadAMRecMappingForMassUpload() throws FileNotFoundException {
		String rec,am;
		if (GenerateDataLoadFiles.massUserLoad) {
			try {

				CsvReader overrideFile = new CsvReader(GenerateDataLoadFiles.baseDir + "\\overrideInternalUserExport.csv");

				overrideFile.readHeaders();

				while (overrideFile.readRecord()) {
					am = overrideFile.get("AM");
					rec = overrideFile.get("REC");
					amRecList.add(new String[]{am,rec});
				}
				overrideFile.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw e;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String[] getAMRec(long count) {
		int index = (int) (count % amRecList.size());
		return amRecList.get(index);
	}
	
	
	
	boolean doesSatisfyfilterV2(TalentFile fileRecord, Set<String> existingPsId, Set<String> existingEmail) {
		boolean result = false, evaluateNextFilter = true;
		
		try {
			if(existingPsId.contains(fileRecord.getPsId())) {
				existingUsers.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			
			if(evaluateNextFilter && existingEmail.contains(fileRecord.getEmailaddr())) {
				duplicateEmail.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			
			if(evaluateNextFilter && (StringUtils.isBlank(fileRecord.getContrfirstname()) 
					|| StringUtils.isBlank(fileRecord.getContrlastname()))) {
				missingName.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			
			if(evaluateNextFilter && (StringUtils.isBlank(fileRecord.getEmailaddr()))) {
				missingEmail.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			
			if(evaluateNextFilter && GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getInvalidSegments().contains(fileRecord.getSegment())) {
				invalidSegment.add(fileRecord.getPsId());
				evaluateNextFilter = false;				
			}
			
			if(evaluateNextFilter && GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getBlacklistedTalents().contains(fileRecord.getPsId())) {
				blacklistedUsers.add(fileRecord.getPsId());
				evaluateNextFilter = false;				
			}
			
			if (evaluateNextFilter && !GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getOfficeCodes().contains(fileRecord.getControfficeid())) {			
				if(!isRecruiterCenterTalent(fileRecord)) {
					invalidTalentOfficeCodes.add(fileRecord.getPsId());
					evaluateNextFilter = false;
				}
			}
			
			if (evaluateNextFilter && !GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getDivisionCodes().contains(fileRecord.getDivisionNumber())) {
				invalidDivisionIds.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			
			if (evaluateNextFilter && !GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getPlacementTypes().contains(fileRecord.getPlacementType())) {
				invalidPlacementTypes.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			
			if (evaluateNextFilter && GenerateDataLoadFiles.internalEmployeeClass.contains(fileRecord.getEmplClassCategory())) {
				internalUserIds.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			
			if (evaluateNextFilter && !isRecruiterCenterTalent(fileRecord) && GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).isRecruiterOfficeInPilot()  && 
					(!GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getOfficeCodes().contains(fileRecord.getRecruiteroffnum()))) {
				invalidRecruiterOfficeCodes.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			if (evaluateNextFilter && GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getPsCustIds().contains(fileRecord.getPsCustId())) {
				invalidPSCustId.add(fileRecord.getPsId());
				evaluateNextFilter = false;
			}
			if (evaluateNextFilter && !GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).isIncludeFormer()) {
				if(!isFormer(fileRecord.getEnddate())) {
					evaluateNextFilter = true;
				} else {
					formerTalents.add(fileRecord.getPsId());
					evaluateNextFilter = false;
				}
			}
			
			if(evaluateNextFilter) {
				result = true;
			}
		} catch (Exception ex) {
			exceptionIds.add(fileRecord.getPsId());
			result = false;
		}
		
		return result;
	}
	
	String convertSetToString(Set<String> setOfIds) {
		StringBuilder sb = new StringBuilder();
		for (String s : setOfIds) {
			sb.append(s).append(",");
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}
	
	public void readOrgFile() throws FileNotFoundException {
		String opcoCode, officeCode, officeName, regionCode, regionName;
		
		Map<String, Organization> opcoOfficeMap, opcoRegionMap;
		File orgFile = new File("OrgDataMaster.csv");
		try {

			CsvReader orgCSVFile = new CsvReader(orgFile.getAbsolutePath());

			orgCSVFile.readHeaders();

			while (orgCSVFile.readRecord()) {
				opcoCode = orgCSVFile.get("OpCo_Code__c");				
				officeCode= orgCSVFile.get("Office_Code__c");
				officeName = orgCSVFile.get("Office_Name__c");				
				regionCode = orgCSVFile.get("Region_Code__c");
				regionName = orgCSVFile.get("Region_Name__c");
				
				if(officeMap.get(opcoCode) != null) {
					opcoOfficeMap = officeMap.get(opcoCode);
				} else {
					opcoOfficeMap = new HashMap<String, Organization>();					
				}
				Organization org = new Organization();
				org.setOfficeCode(officeCode);
				org.setOfficeName(officeName);
				org.setRegionCode(regionCode);
				org.setRegionName(regionName);
				opcoOfficeMap.put(officeCode, org);
				officeMap.put(opcoCode, opcoOfficeMap);
				
			}
			orgCSVFile.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private boolean isFormer(String date) {
		boolean result = false;
		SimpleDateFormat sf = new SimpleDateFormat("M/d/yyyy");
		if (date.indexOf(" ") != -1) {
			date = date.substring(0, date.indexOf(" "));
		}
		try {
			Date endDate = sf.parse(date);
			Date todayDate = new Date();
			if (todayDate.after(endDate)) {
				result = true;
			} 
		} catch (Exception ex) {
			
		}
		
		return result;
	}
	
	
	public void writeStringToFile(String name, StringBuilder sb) {
		try {
			File f = new File(name);
			Writer fw = new FileWriter(f);
			fw.write(sb.toString());
			fw.close();
			

		} catch (IOException ex) {
			ex.printStackTrace();
		} 
	}
	
	private boolean isRecruiterCenterTalent(TalentFile fileRecord) {
		boolean result = false;
		if(!StringUtils.isBlank(fileRecord.getRecruiteroffnum()) && GenerateDataLoadFiles.opcoConfig.get(GenerateDataLoadFiles.getOpco(fileRecord)).getSrcOfficeCodes().contains(fileRecord.getRecruiteroffnum())) {
			result = true;
		}
		return result;
	}
}
