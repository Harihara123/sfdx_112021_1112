package com.allegis.connected.userprovision;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class GenerateDataLoadFiles {

	static final String accountOwnerId = "005240000038Gjf";
	static final String accountTalentRecordTypeId = "01224000000FFqQ";
	static final String contactTalentRecordTypeId = "01224000000FFqV";
	static final String aerotekCommunityUserProfileId = "00e24000000JLa0";
	static final String aerotekCanadaCommunityUserProfileId = "00e24000000FRiq";
	static final String tekCommunityUserProfileId = "00e24000000JLa2";
	static final String tekSubVendorCommunityUserProfileId = "00e24000000NXQE";
	static final String tekGlobalServicesW2CommunityUserProfileId = "00e24000001EIOe";
	static final String tekGlobalServicesSUBCommunityUserProfileId = "00e24000001EIOZ";
	static final String astonCarterCommunityUserProfileId = "00e24000001Ae2s";
	static final boolean activeRecruiterOnly = false;
	static final Set<String> internalEmployeeClass = new HashSet<String>(Arrays.asList("INTERNAL","INTCONTR"));
	static String baseDir;
	
	static Map<String, OpcoFilterConfig> opcoConfig = new HashMap<String, OpcoFilterConfig>();
	
	
	static StringBuilder uploadReport = new StringBuilder();
	static StringBuilder errorReport = new StringBuilder();

	static String nonProdEmail = "rchakrab1@allegisgroup.com";

	/**
	 * UPDATE THESE PARAMETERS BASED ON OPCO AND SANDBOX
	 */
	//static String opco = "";
	//static String timeZone = "";
	static boolean prodMode = false;

	static boolean massUserLoad = false;
	

	public static void main(String[] args) throws Exception {

		/*
		 * boolean generateAccountFile = false; boolean generateContactFile =
		 * false; boolean generateUserFile = false;
		 */
		Set<String> tallentWithMissingAM = new HashSet<String>();
		FileIO fileIO = new FileIO();
		File configFile;
		Properties prop;
		// Properties prop = fileIO.loadProperties("config.properties");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Is this running in Production Mode? (y/n)\n");
		String s = br.readLine();
		if (s.equalsIgnoreCase("y")) {
			prodMode = true;
		} else {
			prodMode = false;
		}
		

		/*do {
			 System.out.print("Select OPCO -- Aerotek/TEKSystems/RWS ? (a/t/r)\n ");
			s = br.readLine();

			if (s.equalsIgnoreCase("a")) {
				opco = "ONS";
			} else if (s.equalsIgnoreCase("t")) {
				opco = "TEK";
			} else if (s.equalsIgnoreCase("r")) {
				opco = "RWS";
			}
		} while (!(s.equalsIgnoreCase("a") || s.equalsIgnoreCase("t") || s.equalsIgnoreCase("r")));*/

		System.err.println("\nLoad TEKSystems Properties\n");
		configFile = new File("TEK.properties");
		prop = fileIO.loadProperties(configFile.getAbsolutePath());
		updateOpcoConfig(prop, "TEK");
		
		System.err.println("\nLoad is Aerotek Properties\n");
		configFile = new File("AEROTEK.properties");
		prop = fileIO.loadProperties(configFile.getAbsolutePath());
		updateOpcoConfig(prop, "ONS");
		
		System.err.println("\nLoad is AstonCarter Properties\n");
		configFile = new File("ASTONCARTER.properties");
		prop = fileIO.loadProperties(configFile.getAbsolutePath());
		updateOpcoConfig(prop, "AC");
		
		System.err.println("\nLoad is Aerotek Canada Properties\n");
		configFile = new File("AEROTEKCANADA.properties");
		prop = fileIO.loadProperties(configFile.getAbsolutePath());
		updateOpcoConfig(prop, "ONSCA");
		
		System.err.println("\nLoad is TEK Sub Vendor Properties\n");
		configFile = new File("TEKSUB.properties");
		prop = fileIO.loadProperties(configFile.getAbsolutePath());
		updateOpcoConfig(prop, "TEKSUB");
		
		System.err.println("\nLoad is TEK Global Services W2 Properties\n");
		configFile = new File("TGSW2.properties");
		prop = fileIO.loadProperties(configFile.getAbsolutePath());
		updateOpcoConfig(prop, "TGSW2");
		
		System.err.println("\nLoad is TEK Global Services Sub Properties\n");
		configFile = new File("TGSSUB.properties");
		prop = fileIO.loadProperties(configFile.getAbsolutePath());
		updateOpcoConfig(prop, "TGSSUB");

		
		
		/*
		 * for (String s1 : placementTypes) { System.out.println(s1); } for
		 * (String s1 : officeCodes) { System.out.println(s1); } for (String s1
		 * : divisionCodes) { System.out.println(s1); }
		 */
		/**
		 * do { System.out.print("Select User TimeZone - EST/MST ? (e/m)\n "); s
		 * = br.readLine();
		 * 
		 * if (s.equalsIgnoreCase("m")) { timeZone = "America/Phoenix"; } else
		 * if (s.equalsIgnoreCase("e")) { timeZone = "America/New_York"; } else
		 * { timeZone = "America/New_York"; } } while (!(s.equalsIgnoreCase("e")
		 * || s.equalsIgnoreCase("m")));
		 */

		if (!prodMode) {
			do {
				System.out.print("Do you want to override AM/Rec? (y/n)\n ");
				s = br.readLine();

				if (s.equalsIgnoreCase("y") && !prodMode) {
					massUserLoad = true;
				}
			} while (!(s.equalsIgnoreCase("y") || s.equalsIgnoreCase("n")));

			System.out.print("Enter the talent email?\n ");
			nonProdEmail = br.readLine();

		}
		baseDir = prop.getProperty("work_Directory");
		
		
		/*s = prop.getProperty("timeZone");
		if (s.equalsIgnoreCase("m")) {
			timeZone = "America/Phoenix";
		} else if (s.equalsIgnoreCase("e")) {
			timeZone = "America/New_York";
		} else {
			timeZone = "America/New_York";
		}*/

		if (prodMode) {
			System.err.println("\nprogram is running in ____PRODUCTION____ mode\n");
		} else {
			System.err.println("\nprogram is running in ___TEST___ mode\n");
		}

		if (massUserLoad) {
			System.err.println("\nThe Data Is Generated For Mass User Load in SANDBOX\n");
		}

		System.err.println("\nFiles are read from " + baseDir + "\n\n");

		Map<String, SObject> internalUserMap = new HashMap<String, SObject>();
		Map<String, SObject> talentUserMap = new HashMap<String, SObject>();
		Map<String, SObject> talentUserUpload = new HashMap<String, SObject>();
		Map<String, SuccessFile> successRecordsMap = new HashMap<String, SuccessFile>();
		// Read User and Talent files

		Map<String, TalentUser> talentUserExportMap = fileIO.readTalentUserExportFile(baseDir + "\\talentExport.csv");
		talentUserMap = fileIO.readRawTalentFile(baseDir + "\\talent.csv", talentUserExportMap);
		if ((args != null) && (args.length > 0) && !args[0].equalsIgnoreCase("validate")) {
			// internalUserMap = fileIO.readInternalUserExportFile(baseDir +
			// "\\internalUserExport.csv");
		}

		// System.out.println("Talent File has " + talentUserMap.size() + "
		// records");
		// System.out.println("Internal User file has " + internalUserMap.size()
		// + " records");
		String recruiterPSId, accountMgrId;
		InternalUser recruiter = null, accountMgr = null;

		for (String talentPsId : talentUserMap.keySet()) {
			accountMgrId = ((TalentFile) talentUserMap.get(talentPsId)).getAcctmgrid();
			recruiterPSId = ((TalentFile) talentUserMap.get(talentPsId)).getRecruiterid();

			// accountMgr = (InternalUser) internalUserMap.get(accountMgrId);
			// recruiter = (InternalUser) internalUserMap.get(recruiterPSId);

			if (activeRecruiterOnly && (args != null) && (args.length > 0) && !args[0].equalsIgnoreCase("validate")) {
				if (recruiter != null && recruiter.getIsActive().equalsIgnoreCase("true")) {
					TalentFile file = (TalentFile) talentUserMap.get(talentPsId);
					if (accountMgr != null) {
						file.setAcctmgrsfdcid(accountMgr.getId());
					} else {
						tallentWithMissingAM.add(talentPsId);
					}
					file.setRecruitersfdcid(recruiter.getId());
					talentUserUpload.put(talentPsId, talentUserMap.get(talentPsId));
				} else {

				}
			} else {
				TalentFile file = (TalentFile) talentUserMap.get(talentPsId);

				if (accountMgr != null) {
					file.setAcctmgrsfdcid(accountMgr.getId());
				}
				if (recruiter != null) {
					file.setRecruitersfdcid(recruiter.getId());
				}
				talentUserUpload.put(talentPsId, talentUserMap.get(talentPsId));
			}
		}

		// System.out.println(tallentWithMissingAM.size() + " will be uploaded
		// without AM details");

		// System.out.println(tallentWithMissingAM.toArray());

		// System.out.println(talentUserUpload.size() + " records will be ready
		// for upload");

		if ((args != null) && (args.length > 0) && args[0].equalsIgnoreCase("validate")) {
			generateSOQL(talentUserUpload);
		}

		if ((args != null) && (args.length > 0) && args[0].equalsIgnoreCase("generateAccount")) {
			generateAccountFile(talentUserUpload);
			fileIO.writeStringToFile(baseDir + "\\report.txt", uploadReport);
			fileIO.writeStringToFile(baseDir + "\\error.txt", errorReport);

		}

		if ((args != null) && (args.length > 0) && args[0].equalsIgnoreCase("generateContact")) {
			// successRecordsMap = fileIO.readSuccessFile(baseDir +
			// "\\accountSuccess.csv");
			successRecordsMap = fileIO.readSuccessFile(baseDir, "Account");
			generateContactFile(talentUserUpload, successRecordsMap);
			// generateWorkHistoryFile(talentUserUpload, successRecordsMap);
			fileIO.writeStringToFile(baseDir + "\\report.txt", uploadReport);
			fileIO.writeStringToFile(baseDir + "\\error.txt", errorReport);

		}

		if ((args != null) && (args.length > 0) && args[0].equalsIgnoreCase("generateUser")) {
			// successRecordsMap = fileIO.readSuccessFile(baseDir +
			// "\\contactSuccess.csv");
			successRecordsMap = fileIO.readSuccessFile(baseDir, "Contact");
			generateUserFile(talentUserUpload, successRecordsMap);
			fileIO.writeStringToFile(baseDir + "\\report.txt", uploadReport);
			fileIO.writeStringToFile(baseDir + "\\error.txt", errorReport);
		}

	}

	static void generateAccountFile(Map<String, SObject> talentUserUpload) throws Exception {
		List<TalentAccount> accountUpload = new ArrayList<TalentAccount>();

		TalentFile talentFile;
		for (String accountPSId : talentUserUpload.keySet()) {
			talentFile = (TalentFile) talentUserUpload.get(accountPSId);
			TalentAccount account = new TalentAccount();

			account.setAccountName(talentFile.getContrfirstname() + " " + talentFile.getContrlastname());
			account.setOwnerId(accountOwnerId);
			account.setRecordTypeId(accountTalentRecordTypeId);

			/**
			 * Updated By AUP Position Service
			 */
			// account.setRecruiterId(talentFile.getRecruitersfdcid());
			// account.setAccountManagerId(talentFile.getAcctmgrsfdcid());

			
			if (talentFile.getPsId().startsWith("S")) {
				throw new Exception("Peoplesoft Id is null");
			}
			account.setPeoplesoftId(talentFile.getPsId());
			account.setSourceSystemId("COM." + talentFile.getRwscandidateid());
			

			if (getOpco(talentFile).equalsIgnoreCase("ONS") || getOpco(talentFile).equalsIgnoreCase("ONSCA")) {
				account.setOwnership("Aerotek Community");
			} else if (getOpco(talentFile).equalsIgnoreCase("TEK") || getOpco(talentFile).equalsIgnoreCase("TGSW2") ||
					getOpco(talentFile).equalsIgnoreCase("TEKSUB") || getOpco(talentFile).equalsIgnoreCase("TGSSUB")) {
				account.setOwnership("Teksystems Community");
			} else if (getOpco(talentFile).equalsIgnoreCase("AC")) {
				account.setOwnership("AstonCarter Community");
			} else {
				throw new Exception("Opco Is Unknown");
			}

			account.setVisibility("Private");

			account.setTalentPreferences("{'paystubType':'" + talentFile.getContrpaystubtype() + "','timecaptureType':'"
					+ talentFile.getContrtimecapturetype() + "'}");

			accountUpload.add(account);

		}
		FileIO fileIO = new FileIO();
		fileIO.writeAccountUploadFile(baseDir + "\\accountUpload.csv", accountUpload);
		System.out.println(accountUpload.size() + " records will be ready for upload");
	}

	static void generateContactFile(Map<String, SObject> talentUserUpload, Map<String, SuccessFile> successRecordsMap)
			throws Exception {
		List<TalentContact> contactUpload = new ArrayList<TalentContact>();

		TalentFile talentFile;
		for (String talentPSId : talentUserUpload.keySet()) {
			talentFile = (TalentFile) talentUserUpload.get(talentPSId);
			TalentContact contact = new TalentContact();

			contact.setFirstName(talentFile.getContrfirstname());
			contact.setLastName(talentFile.getContrlastname());
			contact.setSuffix(talentFile.getContrnamesuffix());
			
			contact.setPeoplesoftId(talentFile.getPsId());
			contact.setSrcSystemId("COM." + talentFile.getRwscandidateid());

			contact.setRecordTypeId(contactTalentRecordTypeId);

			if (!StringUtils.isBlank(talentFile.getMobilephnum())) {
				contact.setPhone(talentFile.getMobilephnum());
			} else {
				contact.setPhone(talentFile.getHomephnum());
			}

			if (prodMode) {
				contact.setEmail(talentFile.getEmailaddr());
			} else {
				contact.setEmail(nonProdEmail);
			}
			/**
			 * Updated By AUP Position Service
			 */
			// contact.setTitle(talentFile.getClientjobtitle());

			contact.setAddress(talentFile.getAddrline1() + talentFile.getAddrline2() + talentFile.getAddrline3()
					+ talentFile.getAddrline4());
			contact.setCity(talentFile.getCity());
			contact.setState(talentFile.getStateprov());
			contact.setZip(talentFile.getPostalcode());
			contact.setCountry(talentFile.getCountry());

			
			if (successRecordsMap.get(talentPSId) != null) {
				contact.setAccountId(successRecordsMap.get(talentPSId).getSfdcId());
				contactUpload.add(contact);
			}

			

		}
		FileIO fileIO = new FileIO();
		fileIO.writeContactUploadFile(baseDir + "\\contactUpload.csv", contactUpload);
		System.out.println(contactUpload.size() + " records will be ready for upload");
	}

	static void generateUserFile(Map<String, SObject> talentUserUpload, Map<String, SuccessFile> successRecordsMap)
			throws Exception {
		List<TalentUser> userUpload = new ArrayList<TalentUser>();

		TalentFile talentFile;
		for (String talentPSId : talentUserUpload.keySet()) {
			talentFile = (TalentFile) talentUserUpload.get(talentPSId);
			TalentUser user = new TalentUser();

			user.setFirstName(talentFile.getContrfirstname());
			user.setLastName(talentFile.getContrlastname());
			user.setUserName(talentFile.getEmailaddr() + ".talent");
			user.setPeoplesoftId(talentFile.getPsId());

			if (getOpco(talentFile).equalsIgnoreCase("ONS")) {
				user.setProfileId(aerotekCommunityUserProfileId);
			} else if (getOpco(talentFile).equalsIgnoreCase("TEK")) {
				user.setProfileId(tekCommunityUserProfileId);
			} else if (getOpco(talentFile).equalsIgnoreCase("ONSCA")) {
				user.setProfileId(aerotekCanadaCommunityUserProfileId);
			} else if (getOpco(talentFile).equalsIgnoreCase("AC")) {
				user.setProfileId(astonCarterCommunityUserProfileId);
			} else if (getOpco(talentFile).equalsIgnoreCase("TEKSUB")) {
				user.setProfileId(tekSubVendorCommunityUserProfileId);
			} else if (getOpco(talentFile).equalsIgnoreCase("TGSW2")) {
				user.setProfileId(tekGlobalServicesW2CommunityUserProfileId);
			} else if (getOpco(talentFile).equalsIgnoreCase("TGSSUB")) {
				user.setProfileId(tekGlobalServicesSUBCommunityUserProfileId);
			} else {
				throw new Exception("Opco Is Unknown");
			}

			if (talentFile.getMobilephnum() != null) {
				user.setPhone(talentFile.getMobilephnum());
			} else {
				user.setPhone(talentFile.getHomephnum());
			}
			if (prodMode) {
				user.setEmail(talentFile.getEmailaddr());
			} else {
				user.setEmail(nonProdEmail);
			}
			/**
			 * Updated By AUP Position Service
			 */
			// user.setTitle(talentFile.getClientjobtitle());
			user.setFederationId(talentFile.getPsId());
			String alias = generateAlias(talentFile.getContrfirstname(), talentFile.getContrlastname());
			user.setAlias(alias);
			user.setNickName(talentFile.getEmailaddr());
			user.setTimeZoneSidKey(opcoConfig.get(getOpco(talentFile)).getTimeZone());

			user.setStreet(talentFile.getAddrline1() + talentFile.getAddrline2() + talentFile.getAddrline3()
					+ talentFile.getAddrline4());
			user.setCity(talentFile.getCity());
			user.setState(talentFile.getStateprov());
			user.setZip(talentFile.getPostalcode());
			user.setCountry(talentFile.getCountry());
			user.setOfficeName(talentFile.getControfficename());
			user.setOfficeCode(talentFile.getControfficeid());
			user.setDivisionName(talentFile.getDivisionName());
			user.setDivisionNumber(talentFile.getDivisionNumber());
			/**
			 * Updated By AUP Position Service
			 */
			user.setPositionServiceFinished("FALSE");
			user.setAzureServiceFinished("TRUE");
			user.setEmployeeServiceFinished("TRUE");
			user.setFirstTimeLogin("FALSE");

			
			if (getOpco(talentFile).equalsIgnoreCase("ONS") || getOpco(talentFile).equalsIgnoreCase("ONSCA")) {
				user.setOpco("ONS");
			} else if (getOpco(talentFile).equalsIgnoreCase("TEK") || getOpco(talentFile).equalsIgnoreCase("TGSW2") ||
					getOpco(talentFile).equalsIgnoreCase("TEKSUB") || getOpco(talentFile).equalsIgnoreCase("TGSSUB")) {
				user.setOpco("TEK");
			} else if (getOpco(talentFile).equalsIgnoreCase("AC")) {
				user.setOpco("ONS");
			} else {
				throw new Exception("Opco Is Unknown");
			}

			if (successRecordsMap.get(talentPSId) != null) {
				user.setContactId(successRecordsMap.get(talentPSId).getSfdcId());
				userUpload.add(user);
			}

		}
		FileIO fileIO = new FileIO();
		fileIO.writeUserUploadFile(baseDir + "\\userUpload.csv", userUpload);
		System.out.println(userUpload.size() + " records will be ready for upload");
	}

	static void generateWorkHistoryFile(Map<String, SObject> talentUserUpload,
			Map<String, SuccessFile> successRecordsMap) {
		List<TalentWorkHistory> workhistoryUpload = new ArrayList<TalentWorkHistory>();

		TalentFile talentFile;
		for (String accountPSId : talentUserUpload.keySet()) {
			talentFile = (TalentFile) talentUserUpload.get(accountPSId);
			TalentWorkHistory work = new TalentWorkHistory();

			work.setStartDate(talentFile.getStartdate());
			work.setEndDate(talentFile.getEnddate());

			if (successRecordsMap.get(accountPSId) != null) {
				work.setAccountId(successRecordsMap.get(accountPSId).getSfdcId());
				workhistoryUpload.add(work);
			}

		}
		FileIO fileIO = new FileIO();
		fileIO.writeWorkhistoryUploadFile(baseDir + "\\workhistoryUpload.csv", workhistoryUpload);
	}

	static void generateSOQL(Map<String, SObject> talentUserUpload) throws Exception {

		Set<String> amPSIds = new HashSet<String>();
		Set<String> recPSIds = new HashSet<String>();
		StringBuilder aliases = new StringBuilder();

		TalentFile talentFile;
		for (String accountPSId : talentUserUpload.keySet()) {
			talentFile = (TalentFile) talentUserUpload.get(accountPSId);
			if (talentFile.getPsId().length() < 8) {
				throw new Exception("Invalid PS Id for Talent " + talentFile.getPsId());
			}
			if (talentFile.getAcctmgrid().length() < 8) {
				throw new Exception(
						"Account Manager Peoplesoft ID is invalid for Talent with PS ID " + talentFile.getPsId());
			}
			if (talentFile.getRecruiterid().length() < 8) {
				throw new Exception("Recruiter Peoplesoft ID is invalid for Talent with PS ID " + talentFile.getPsId());
			}
			aliases.append("'" + generateAlias(talentFile.getContrfirstname(), talentFile.getContrlastname()) + "',");

			/*
			 * System.out.println(talentFile.getRecruiterid());
			 * System.out.println(talentFile.getAcctmgrid());
			 */
			recPSIds.add(talentFile.getRecruiterid());
			amPSIds.add(talentFile.getAcctmgrid());

		}
		aliases.deleteCharAt(aliases.length() - 1);

		FileIO fileIO = new FileIO();
		/*if (!opco.equalsIgnoreCase("RWS")) {
			fileIO.createAnnonymousApexFile("___apex.template", recPSIds, amPSIds, opco, baseDir, aliases.toString());
		}*/
		

	}

	static String generateAlias(String firstName, String lastName) {
		String alias = "";

		switch (firstName.length()) {
		case 0:
			alias += "";
			break;
		case 1:
			alias += firstName;
			break;
		case 2:
			alias += firstName.substring(0, 2);
			break;
		case 3:
			alias += firstName.substring(0, 3);
			break;
		default:
			alias += firstName.substring(0, 3);
			break;

		}

		switch (lastName.length()) {
		case 0:
			alias += "";
			break;
		case 1:
			alias += lastName;
			break;
		case 2:
			alias += lastName.substring(0, 2);
			break;
		case 3:
			alias += lastName.substring(0, 3);
			break;
		default:
			alias += lastName.substring(0, 3);
			break;

		}

		alias = alias.replace("'", "").toLowerCase();
		return alias;
	}

	public static String getOpco(TalentFile user) {
		String opco = "";
		Set<String> divisionMatch = new HashSet<String>();
		Set<String> officeMatch = new HashSet<String>();
		for (String opcoKey : opcoConfig.keySet()) {
			if (opcoConfig.get(opcoKey).getDivisionCodes().contains(user.getDivisionNumber())) {
				divisionMatch.add(opcoKey);
			}
		}
		
		for (String opcoKey : divisionMatch) {
			if (opcoConfig.get(opcoKey).getOfficeCodes().contains(user.getControfficeid())) {
				officeMatch.add(opcoKey);
			}
		}
		
		for (String opcoKey : officeMatch) {
			if (opcoConfig.get(opcoKey).getPlacementTypes().contains(user.getPlacementType())) {
				opco = opcoKey;
				break;
			}
		}
		return opco;
	}
	
	
	public static void updateOpcoConfig (Properties opcoProp, String opcoName) {		
		OpcoFilterConfig opcoFilterConfig = new OpcoFilterConfig();
		Set<String> placementTypes = new HashSet<String>(Arrays.asList(opcoProp.getProperty("approved_Placement_Type").split(",")));
		opcoFilterConfig.setPlacementTypes(placementTypes);
		
		Set<String> officeCodes = new HashSet<String>(Arrays.asList(opcoProp.getProperty("approved_Talent_Office_List").split(",")));
		officeCodes.add("00000");
		opcoFilterConfig.setOfficeCodes(officeCodes);
		
		Set<String> srcOfficeCodes = new HashSet<String>(Arrays.asList(opcoProp.getProperty("approved_SRC_Office_List").split(",")));
		opcoFilterConfig.setSrcOfficeCodes(srcOfficeCodes);
		
		Set<String> divisionCodes = new HashSet<String>(Arrays.asList(opcoProp.getProperty("approved_Talent_Division").split(",")));
		opcoFilterConfig.setDivisionCodes(divisionCodes);
		
		Set<String> psCustIds = new HashSet<String>(Arrays.asList(opcoProp.getProperty("exclude_PS_Cust_Id").split(",")));
		opcoFilterConfig.setPsCustIds(psCustIds);
		
		Set<String> psinvalidSegments = new HashSet<String>(Arrays.asList(opcoProp.getProperty("exclude_Segments").split(",")));
		opcoFilterConfig.setInvalidSegments(psinvalidSegments);
		
		Set<String> blacklistedUsers = new HashSet<String>(Arrays.asList(opcoProp.getProperty("exclude_Talents").split(",")));
		opcoFilterConfig.setBlacklistedTalents(blacklistedUsers);
		
		boolean includeFormer = (opcoProp.getProperty("include_Formers") == null) ? false
				: Boolean.valueOf(opcoProp.getProperty("include_Formers"));
		opcoFilterConfig.setIncludeFormer(includeFormer);
		
		boolean recruiterOfficeInPilot = (opcoProp.getProperty("recruiter_Office_In_Pilot") == null) ? false
				: Boolean.valueOf(opcoProp.getProperty("recruiter_Office_In_Pilot"));		
		opcoFilterConfig.setRecruiterOfficeInPilot(recruiterOfficeInPilot);
		
		String timeZone = opcoProp.getProperty("timeZone");
		if (timeZone.equalsIgnoreCase("m")) {
			timeZone = "America/Phoenix";
		} else if (timeZone.equalsIgnoreCase("e")) {
			timeZone = "America/New_York";
		} else {
			timeZone = "America/New_York";
		}
		opcoFilterConfig.setTimeZone(timeZone);
		
		opcoConfig.put(opcoName, opcoFilterConfig);
		
	}

}
