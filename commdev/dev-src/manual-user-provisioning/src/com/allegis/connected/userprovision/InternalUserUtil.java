package com.allegis.connected.userprovision;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;


public class InternalUserUtil {

	public static void main(String[] args) {
		String baseDir = "C:\\Users\\rchakrab\\Documents\\Communities\\UserProvisioning\\OfficeIdUpdate";
		Map<String, SObject> internalUserExportMap = new HashMap<String, SObject>();
		Map<String, SObject> internalUserUpdateMap = new HashMap<String, SObject>();
		//Read User and Talent files
		FileIO fileIO = new FileIO();
		internalUserExportMap = fileIO.readInternalUserExportFile(baseDir + "\\userExport.csv");
		
		internalUserUpdateMap = fileIO.readInternalUserExportFile(baseDir + "\\UserUpdate.csv");
		
		int count=0;
		for (String psId : internalUserUpdateMap.keySet()) {
			//System.out.println(psId);
			if (internalUserExportMap.get(psId) == null) {
				System.out.println("User Not Found " + psId);
				count++;
			}
		}
		System.out.println(count);

	}

}
