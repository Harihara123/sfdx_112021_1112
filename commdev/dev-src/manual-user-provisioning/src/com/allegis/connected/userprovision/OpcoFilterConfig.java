package com.allegis.connected.userprovision;

import java.util.Set;

public class OpcoFilterConfig {	
	Set<String> placementTypes;
	Set<String> officeCodes;
	Set<String> srcOfficeCodes;
	Set<String> divisionCodes;
	Set<String> psCustIds;
	Set<String> invalidSegments;
	Set<String> blacklistedTalents;	
	String timeZone;
	
	boolean includeFormer;
	boolean recruiterOfficeInPilot;
	
	public Set<String> getPlacementTypes() {
		return placementTypes;
	}
	public void setPlacementTypes(Set<String> placementTypes) {
		this.placementTypes = placementTypes;
	}
	public Set<String> getOfficeCodes() {
		return officeCodes;
	}
	public void setOfficeCodes(Set<String> officeCodes) {
		this.officeCodes = officeCodes;
	}
	public Set<String> getSrcOfficeCodes() {
		return srcOfficeCodes;
	}
	public void setSrcOfficeCodes(Set<String> srcOfficeCodes) {
		this.srcOfficeCodes = srcOfficeCodes;
	}
	public Set<String> getDivisionCodes() {
		return divisionCodes;
	}
	public void setDivisionCodes(Set<String> divisionCodes) {
		this.divisionCodes = divisionCodes;
	}
	public Set<String> getPsCustIds() {
		return psCustIds;
	}
	public void setPsCustIds(Set<String> psCustIds) {
		this.psCustIds = psCustIds;
	}
	public boolean isIncludeFormer() {
		return includeFormer;
	}
	public void setIncludeFormer(boolean includeFormer) {
		this.includeFormer = includeFormer;
	}
	public boolean isRecruiterOfficeInPilot() {
		return recruiterOfficeInPilot;
	}
	public void setRecruiterOfficeInPilot(boolean recruiterOfficeInPilot) {
		this.recruiterOfficeInPilot = recruiterOfficeInPilot;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public Set<String> getInvalidSegments() {
		return invalidSegments;
	}
	public void setInvalidSegments(Set<String> invalidSegments) {
		this.invalidSegments = invalidSegments;
	}
	public Set<String> getBlacklistedTalents() {
		return blacklistedTalents;
	}
	public void setBlacklistedTalents(Set<String> blacklistedTalents) {
		this.blacklistedTalents = blacklistedTalents;
	}
}
