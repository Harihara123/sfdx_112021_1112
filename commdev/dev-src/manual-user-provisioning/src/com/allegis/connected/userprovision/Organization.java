package com.allegis.connected.userprovision;

public class Organization {
	private boolean isActive;
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getOpcoCode() {
		return opcoCode;
	}
	public void setOpcoCode(String opcoCode) {
		this.opcoCode = opcoCode;
	}
	public String getOfficeCode() {
		return officeCode;
	}
	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}
	public String getOfficeName() {
		return officeName;
	}
	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	
	public String toString() {
		return (this.officeCode + "--" +this.officeName + "--" + this.regionCode + "--" + this.getRegionName());
	}
	private String opcoCode;
	private String officeCode;
	private String officeName;
	private String regionCode;
	private String regionName;
	
}
