package com.allegis.connected.userprovision;

public class TalentAccount implements SObject {
	String id;
	String recordTypeId;
	String visibility;
	String ownership;
	String sourceSystemId;
	String peoplesoftId;
	String recruiterId;
	String accountManagerId;
	String talentPreferences;
	String recruiterPSId;
	String accountManagerPSId;
	String ownerId;
	String accountName;
	String createdById;
	
	public String printLine() {
		return 
			"\"" + this.id + "\"," + 
			"\"" + this.recordTypeId + "\"," +
			"\"" + this.visibility + "\"," +
			"\"" + this.ownership + "\"," +
			"\"" + this.sourceSystemId + "\"";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRecordTypeId() {
		return recordTypeId;
	}

	public void setRecordTypeId(String recordTypeId) {
		this.recordTypeId = recordTypeId;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getOwnership() {
		return ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getSourceSystemId() {
		return sourceSystemId;
	}

	public void setSourceSystemId(String sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}

	public String getPeoplesoftId() {
		return peoplesoftId;
	}

	public void setPeoplesoftId(String peoplesoftId) {
		this.peoplesoftId = peoplesoftId;
	}

	public String getRecruiterId() {
		return recruiterId;
	}

	public void setRecruiterId(String recruiterId) {
		this.recruiterId = recruiterId;
	}

	public String getAccountManagerId() {
		return accountManagerId;
	}

	public void setAccountManagerId(String accountManagerId) {
		this.accountManagerId = accountManagerId;
	}

	public String getTalentPreferences() {
		return talentPreferences;
	}

	public void setTalentPreferences(String talentPreferences) {
		this.talentPreferences = talentPreferences;
	}

	public String getRecruiterPSId() {
		return recruiterPSId;
	}

	public void setRecruiterPSId(String recruiterPSId) {
		this.recruiterPSId = recruiterPSId;
	}

	public String getAccountManagerPSId() {
		return accountManagerPSId;
	}

	public void setAccountManagerPSId(String accountManagerPSId) {
		this.accountManagerPSId = accountManagerPSId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getCreatedById() {
		return createdById;
	}

	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}
	
}
