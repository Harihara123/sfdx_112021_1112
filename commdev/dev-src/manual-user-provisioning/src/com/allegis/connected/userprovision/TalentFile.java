package com.allegis.connected.userprovision;

public class TalentFile implements SObject {
	String psId;
	String psCustId;
	String psCustName;
	String rwscandidateid; 
	String carrierbuilderid; 
	String contrpersonalemailid; 
	String contrpaystubtype; 
	String contrtimecapturetype; 
	String controfficeid;
	String controfficename;
	String contrlastname;
	String contrfirstname;
	String contrmiddlename;
	String contrnameprefix;
	String contrnamesuffix;
	String empstatus;
	String jobreqno;
	String jobreqjobcode;
	String divisionNumber;
	String divisionName;
	String jobreqjobcodedesc;
	String clientjobtitle;
	String placementType;
	String startdate;
	String enddate;
	String emailaddr;
	String workphnum;
	String mobilephnum;
	String homephnum;
	String addrline1;
	String addrline2;
	String addrline3;
	String addrline4;
	String city;
	String stateprov;
	String country;
	String postalcode;
	String acctmgrid;
	String acctmgrsfdcid;
	String acctmgrname;
	String acctmgroffnum;
	String acctmgroffname;
	String recruiterid;
	String recruitersfdcid;
	String recruitername;
	String recruiteroffnum;
	String recruiteroffname;
	String spreadType;
	String segment;
	String emplClassCategory;
	
	public String getContrnamesuffix() {
		return contrnamesuffix;
	}
	public void setContrnamesuffix(String contrnamesuffix) {
		this.contrnamesuffix = contrnamesuffix;
	}
	public String getEmplClassCategory() {
		return emplClassCategory;
	}
	public void setEmplClassCategory(String emplClassCategory) {
		this.emplClassCategory = emplClassCategory;
	}
	public String getSegment() {
		return segment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public String getSpreadType() {
		return spreadType;
	}
	public void setSpreadType(String spreadType) {
		this.spreadType = spreadType;
	}
	public String getPsId() {
		return psId;
	}
	public void setPsId(String psId) {
		this.psId = psId;
	}
	public String getRwscandidateid() {
		return rwscandidateid;
	}
	public void setRwscandidateid(String rwscandidateid) {
		this.rwscandidateid = rwscandidateid;
	}
	public String getCarrierbuilderid() {
		return carrierbuilderid;
	}
	public void setCarrierbuilderid(String carrierbuilderid) {
		this.carrierbuilderid = carrierbuilderid;
	}
	public String getContrpersonalemailid() {
		return contrpersonalemailid;
	}
	public void setContrpersonalemailid(String contrpersonalemailid) {
		this.contrpersonalemailid = contrpersonalemailid;
	}
	public String getContrpaystubtype() {
		return contrpaystubtype;
	}
	public void setContrpaystubtype(String contrpaystubtype) {
		this.contrpaystubtype = contrpaystubtype;
	}
	public String getContrtimecapturetype() {
		return contrtimecapturetype;
	}
	public void setContrtimecapturetype(String contrtimecapturetype) {
		this.contrtimecapturetype = contrtimecapturetype;
	}
	public String getControfficeid() {
		return controfficeid;
	}
	public void setControfficeid(String controfficeid) {
		this.controfficeid = controfficeid;
	}
	public String getControfficename() {
		return controfficename;
	}
	public void setControfficename(String controfficename) {
		this.controfficename = controfficename;
	}
	public String getContrlastname() {
		return contrlastname;
	}
	public void setContrlastname(String contrlastname) {
		this.contrlastname = contrlastname;
	}
	public String getContrfirstname() {
		return contrfirstname;
	}
	public void setContrfirstname(String contrfirstname) {
		this.contrfirstname = contrfirstname;
	}
	public String getContrmiddlename() {
		return contrmiddlename;
	}
	public void setContrmiddlename(String contrmiddlename) {
		this.contrmiddlename = contrmiddlename;
	}
	public String getContrnameprefix() {
		return contrnameprefix;
	}
	public void setContrnameprefix(String contrnameprefix) {
		this.contrnameprefix = contrnameprefix;
	}
	public String getEmpstatus() {
		return empstatus;
	}
	public void setEmpstatus(String empstatus) {
		this.empstatus = empstatus;
	}
	public String getJobreqno() {
		return jobreqno;
	}
	public void setJobreqno(String jobreqno) {
		this.jobreqno = jobreqno;
	}
	public String getJobreqjobcode() {
		return jobreqjobcode;
	}
	public void setJobreqjobcode(String jobreqjobcode) {
		this.jobreqjobcode = jobreqjobcode;
	}
	public String getJobreqjobcodedesc() {
		return jobreqjobcodedesc;
	}
	public void setJobreqjobcodedesc(String jobreqjobcodedesc) {
		this.jobreqjobcodedesc = jobreqjobcodedesc;
	}
	public String getClientjobtitle() {
		return clientjobtitle;
	}
	public void setClientjobtitle(String clientjobtitle) {
		this.clientjobtitle = clientjobtitle;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getEmailaddr() {
		return emailaddr;
	}
	public void setEmailaddr(String emailaddr) {
		this.emailaddr = emailaddr;
	}
	public String getWorkphnum() {
		return workphnum;
	}
	public void setWorkphnum(String workphnum) {
		this.workphnum = workphnum;
	}
	public String getMobilephnum() {
		return mobilephnum;
	}
	public void setMobilephnum(String mobilephnum) {
		this.mobilephnum = mobilephnum;
	}
	public String getHomephnum() {
		return homephnum;
	}
	public void setHomephnum(String homephnum) {
		this.homephnum = homephnum;
	}
	public String getAddrline1() {
		return addrline1;
	}
	public void setAddrline1(String addrline1) {
		this.addrline1 = addrline1;
	}
	public String getAddrline2() {
		return addrline2;
	}
	public void setAddrline2(String addrline2) {
		this.addrline2 = addrline2;
	}
	public String getAddrline3() {
		return addrline3;
	}
	public void setAddrline3(String addrline3) {
		this.addrline3 = addrline3;
	}
	public String getAddrline4() {
		return addrline4;
	}
	public void setAddrline4(String addrline4) {
		this.addrline4 = addrline4;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateprov() {
		return stateprov;
	}
	public void setStateprov(String stateprov) {
		this.stateprov = stateprov;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public String getAcctmgrid() {
		return acctmgrid;
	}
	public void setAcctmgrid(String acctmgrid) {
		this.acctmgrid = acctmgrid;
	}
	public String getAcctmgrname() {
		return acctmgrname;
	}
	public void setAcctmgrname(String acctmgrname) {
		this.acctmgrname = acctmgrname;
	}
	public String getAcctmgroffnum() {
		return acctmgroffnum;
	}
	public void setAcctmgroffnum(String acctmgroffnum) {
		this.acctmgroffnum = acctmgroffnum;
	}
	public String getAcctmgroffname() {
		return acctmgroffname;
	}
	public void setAcctmgroffname(String acctmgroffname) {
		this.acctmgroffname = acctmgroffname;
	}
	public String getRecruiterid() {
		return recruiterid;
	}
	public void setRecruiterid(String recruiterid) {
		this.recruiterid = recruiterid;
	}
	public String getRecruitername() {
		return recruitername;
	}
	public void setRecruitername(String recruitername) {
		this.recruitername = recruitername;
	}
	public String getRecruiteroffnum() {
		return recruiteroffnum;
	}
	public void setRecruiteroffnum(String recruiteroffnum) {
		this.recruiteroffnum = recruiteroffnum;
	}
	public String getRecruiteroffname() {
		return recruiteroffname;
	}
	public void setRecruiteroffname(String recruiteroffname) {
		this.recruiteroffname = recruiteroffname;
	}
	public String getAcctmgrsfdcid() {
		return acctmgrsfdcid;
	}
	public void setAcctmgrsfdcid(String acctmgrsfdcid) {
		this.acctmgrsfdcid = acctmgrsfdcid;
	}
	public String getRecruitersfdcid() {
		return recruitersfdcid;
	}
	public void setRecruitersfdcid(String recruitersfdcid) {
		this.recruitersfdcid = recruitersfdcid;
	}
	public String getPsCustId() {
		return psCustId;
	}
	public void setPsCustId(String psCustId) {
		this.psCustId = psCustId;
	}
	public String getPsCustName() {
		return psCustName;
	}
	public void setPsCustName(String psCustName) {
		this.psCustName = psCustName;
	}
	public String getDivisionNumber() {
		return divisionNumber;
	}
	public void setDivisionNumber(String divisionNumber) {
		this.divisionNumber = divisionNumber;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public String getPlacementType() {
		return placementType;
	}
	public void setPlacementType(String placementType) {
		this.placementType = placementType;
	}
}
