package com.allegis.connected.userprovision;
public class TalentUser {
	String firstName;
	String lastName;
	String userName;
	String email;
	String nickName;
	String alias;
	String profileId;
	String peoplesoftId;
	String federationId;
	String officeCode;
	String officeName;
	String divisionName;
	String divisionNumber;
	public String getDivisionNumber() {
		return divisionNumber;
	}
	public void setDivisionNumber(String divisionNumber) {
		this.divisionNumber = divisionNumber;
	}
	String title;
	String phone;
	String mobile;
	String street;
	String city;
	String state;
	String zip;
	String country;
	String opco;
	String contactId;
	String azureServiceFinished;
	String employeeServiceFinished;
	String positionServiceFinished;
	String firstTimeLogin;
	
	
	public String getAzureServiceFinished() {
		return azureServiceFinished;
	}
	public void setAzureServiceFinished(String azureServiceFinished) {
		this.azureServiceFinished = azureServiceFinished;
	}
	public String getEmployeeServiceFinished() {
		return employeeServiceFinished;
	}
	public void setEmployeeServiceFinished(String employeeServiceFinished) {
		this.employeeServiceFinished = employeeServiceFinished;
	}
	public String getPositionServiceFinished() {
		return positionServiceFinished;
	}
	public void setPositionServiceFinished(String positionServiceFinished) {
		this.positionServiceFinished = positionServiceFinished;
	}
	String userApplication = "Communities";
	String timeZoneSidKey;
	String localeSidKey = "en_US";
	String emailEncodingKey = "UTF-8";
	String languageLocaleKey = "en_US";
	String userPreferencesDisableAllFeedsEmail = "true";
	String userPreferencesDisableBookmarkEmail = "true";
	String userPreferencesDisableChangeCommentEmail = "true";
	String userPreferencesDisableFollowersEmail = "true";
	String userPreferencesDisableLaterCommentEmail = "true";
	String userPreferencesDisableLikeEmail = "true";
	String userPreferencesDisableMentionsPostEmail = "true";
	String userPreferencesDisableMessageEmail = "true";
	String userPreferencesDisableProfilePostEmail = "true";
	String userPreferencesDisableSharePostEmail = "true";
	String userPreferencesDisCommentAfterLikeEmail = "true";
	String userPreferencesDisMentionsCommentEmail = "true";
	String userPreferencesDisProfPostCommentEmail = "true";
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getPeoplesoftId() {
		return peoplesoftId;
	}
	public void setPeoplesoftId(String peoplesoftId) {
		this.peoplesoftId = peoplesoftId;
	}
	public String getFederationId() {
		return federationId;
	}
	public void setFederationId(String federationId) {
		this.federationId = federationId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getTimeZoneSidKey() {
		return timeZoneSidKey;
	}
	public String getLocaleSidKey() {
		return localeSidKey;
	}
	public String getEmailEncodingKey() {
		return emailEncodingKey;
	}
	public String getLanguageLocaleKey() {
		return languageLocaleKey;
	}
	public String getUserPreferencesDisableAllFeedsEmail() {
		return userPreferencesDisableAllFeedsEmail;
	}
	public String getUserPreferencesDisableBookmarkEmail() {
		return userPreferencesDisableBookmarkEmail;
	}
	public String getUserPreferencesDisableChangeCommentEmail() {
		return userPreferencesDisableChangeCommentEmail;
	}
	public String getUserPreferencesDisableFollowersEmail() {
		return userPreferencesDisableFollowersEmail;
	}
	public String getUserPreferencesDisableLaterCommentEmail() {
		return userPreferencesDisableLaterCommentEmail;
	}
	public String getUserPreferencesDisableLikeEmail() {
		return userPreferencesDisableLikeEmail;
	}
	public String getUserPreferencesDisableMentionsPostEmail() {
		return userPreferencesDisableMentionsPostEmail;
	}
	public String getUserPreferencesDisableMessageEmail() {
		return userPreferencesDisableMessageEmail;
	}
	public String getUserPreferencesDisableProfilePostEmail() {
		return userPreferencesDisableProfilePostEmail;
	}
	public String getUserPreferencesDisableSharePostEmail() {
		return userPreferencesDisableSharePostEmail;
	}
	public String getUserPreferencesDisCommentAfterLikeEmail() {
		return userPreferencesDisCommentAfterLikeEmail;
	}
	public String getUserPreferencesDisMentionsCommentEmail() {
		return userPreferencesDisMentionsCommentEmail;
	}
	public String getUserPreferencesDisProfPostCommentEmail() {
		return userPreferencesDisProfPostCommentEmail;
	}
	public String getOpco() {
		return opco;
	}
	public void setOpco(String opco) {
		this.opco = opco;
	}
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public String getOfficeCode() {
		return officeCode;
	}
	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}
	public String getOfficeName() {
		return officeName;
	}
	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public String getUserApplication() {
		return userApplication;
	}
	public void setUserApplication(String userApplication) {
		this.userApplication = userApplication;
	}
	public void setTimeZoneSidKey(String timeZoneSidKey) {
		this.timeZoneSidKey = timeZoneSidKey;
	}
	public String getFirstTimeLogin() {
		return firstTimeLogin;
	}
	public void setFirstTimeLogin(String firstTimeLogin) {
		this.firstTimeLogin = firstTimeLogin;
	}
}
