package com.allegis.connected.userprovision;

public class TalentWorkHistory implements Comparable<TalentWorkHistory>{
	String accountId;
	String startDate;
	String endDate;
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public int compareTo(TalentWorkHistory twh) {
		if(this.getEndDate().compareTo(twh.getEndDate()) > 0) {
			return 1;
		}
		return 0;
	}
}
