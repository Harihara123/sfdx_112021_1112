#!/usr/bin/env node

'use strict';

var commander = require('commander');

var sag = require('./sag');

/**
 * Command line interface for Shared Asset Gatekeeper, which detects changes made to Salesforce assets that should be protected.
 * More info - http://nextgen.team.allegisgroup.com:8090/display/COM/Development+-+Build+and+Deployment#Development-BuildandDeployment-SalesforceAssetGatekeeper
 */

/*
 * Formatting constants.
 */
var INDENT = '  ';
var INDENT_EXTRA = '                               ';

commander
	.description('Shared Asset Gatekeeper - Detects changes made to Salesforce assets that should be protected.')
	.version('0.0.1')
	.usage('[options]')
	.option('-p, --package [file]', 'Relative path to- and name of the XML-formatted package.xml file. \n' + 
		INDENT_EXTRA + 'Identifies files/assets that are being modified.')
	.option('-w, --whitelist [file]', 'Relative path to- and name of the XML-formatted whitelist file. \n' + 
		INDENT_EXTRA + 'Identifies files/assets that should not be protected. \n' + 
		INDENT_EXTRA + 'Not allowed when protect-mode (--protect-mode) is set to ' + sag.PROTECT_RECENTLY_RELEASED + '.')
	.option('-b, --blacklist [file]', 'Relative path to- and name of the XML-formatted blacklist file. \n' + 
		INDENT_EXTRA + 'Identifies files/assets that should be protected. \n' + 
		INDENT_EXTRA + 'Required when protect-mode (--protect-mode) is set to ' + sag.PROTECT_RECENTLY_RELEASED + '.')
	.option('-a, --acknowledgements [file]', 'Relative path to- and name of the XML-formatted acknowledgements file. \n' + 
		INDENT_EXTRA + 'Allows for modifications to be made to protected files/assets.')
	.option('-r, --protect-mode [option]', 'Set to \'' + sag.PROTECT_SHARED + '\' if shared assets should be protected, \n' + 
		INDENT_EXTRA + '\'' + sag.PROTECT_RECENTLY_RELEASED + '\' if assets recently released to PROD should be protected, \n' + 
		INDENT_EXTRA + 'or \'' + sag.PROTECT_SIMULTANEOUS + '\' if assets should be protected from simultanous changes by multiple streams.')
  	.parse(process.argv);

// console.log('commander.package = ' + commander.package);
// console.log('commander.whitelist = ' + commander.whitelist);
// console.log('commander.blacklist = ' + commander.blacklist);
// console.log('commander.acknowledgements = ' + commander.acknowledgements);

var protectMode = null;
var packageFileStr = null;
var whitelistFileStr = null;
var blacklistFileStr = null;
var acknowledgementFileStr = null;

var errorsFound = [];

if (commander.protectMode !== sag.PROTECT_SHARED && commander.protectMode !== sag.PROTECT_RECENTLY_RELEASED && commander.protectMode !== sag.PROTECT_SIMULTANEOUS) {
	errorsFound.push('Legal values for protect mode are \'' + sag.PROTECT_SHARED + '\', \'' + sag.PROTECT_RECENTLY_RELEASED + '\', or \'' + sag.PROTECT_SIMULTANEOUS + '\'. ' + 
		'Please consult the help (--help) for details.');
} 

if (typeof commander.package === 'undefined') {
	errorsFound.push('The package option (--package) must be specified. Please consult the help (--help) for details.');
}

if (commander.protectMode === sag.PROTECT_SHARED) {
	if (typeof commander.whitelist === 'undefined' && typeof commander.blacklist === 'undefined') {
		errorsFound.push('Either the whitelist option (--whitelist) or blacklist option (--blacklist) must be specified. ' + 
		'Please consult the help (--help) for details.');
	}
	if (typeof commander.whitelist !== 'undefined' && typeof commander.blacklist !== 'undefined') {
		errorsFound.push('Both the whitelist (--whitelist) and blacklist (--blacklist) options have been specified. Please only specify one. ' + 
		'Please consult the help (--help) for details.');
	}
} else if (commander.protectMode === sag.PROTECT_RECENTLY_RELEASED) {
	if (typeof commander.whitelist !== 'undefined') {
		errorsFound.push('The whitelist option (--whitelist) may not be specified when protect-mode (--protect-mode) is set to ' + sag.PROTECT_RECENTLY_RELEASED + '. ' + 
		'Please consult the help (--help) for details.');
	}
	if (typeof commander.blacklist === 'undefined') {
		errorsFound.push('The blacklist option (--blacklist) must be specified when protect-mode (--protect-mode) is set to ' + sag.PROTECT_RECENTLY_RELEASED + '. ' + 
		'Please consult the help (--help) for details.');
	}
}

if (typeof commander.acknowledgements === 'undefined') {
	errorsFound.push('The acknowledgements option (--acknowledgements) must be specified. Please consult the help (--help) for details.');
}

if (errorsFound.length === 0) {
	protectMode = commander.protectMode;
	/*
	 * Resolve the file arguments relative to the current working directory, NOT relative to the 
	 *	location of the program.
	 */
	packageFileStr = process.cwd() + '/' + commander.package;
	if (typeof commander.whitelist !== 'undefined') {
		whitelistFileStr = process.cwd() + '/' + commander.whitelist;
	} else if (typeof commander.blacklist !== 'undefined') {
		blacklistFileStr = process.cwd() + '/' + commander.blacklist;
	}
	acknowledgementFileStr = process.cwd() + '/' + commander.acknowledgements;
} else {
	console.error('One or more errors found:');
	errorsFound.forEach(function(errorFound) {
	    console.error(INDENT + '- ' + errorFound);
	});
	process.exit(1);
}

sag.enforceProtections(protectMode, packageFileStr, whitelistFileStr, blacklistFileStr, acknowledgementFileStr, function(err, result) {
	/*
	 * The result is an object representation of any unacknowledged shared assets that are found. If null, 
	 *	none were found (which indicates success).
	 */
	if (err !== null) {
		console.error(err);
		process.exit(1);
	} else if (result === null) {
		process.exit(0);
	} else {
		process.exit(1);
	}

	/*
	 * In order to see the exit code from the shell, execute the following after invoking the sag-cli program: 
	 *	echo $?
	 */
});


