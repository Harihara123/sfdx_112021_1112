'use strict';

var fs = require('fs');
var util = require('util');

var _ = require('lodash');
var xml2js = require('xml2js');

/**
 * Shared Asset Gatekeeper - Detects changes made to Salesforce assets that should be protected.
 * More info - http://nextgen.team.allegisgroup.com:8090/display/COM/Development+-+Build+and+Deployment#Development-BuildandDeployment-SalesforceAssetGatekeeper
 */
module.exports = {

	/**
	 * Indicates that shared assets should be protected.
	 */
	PROTECT_SHARED: 'shared', 

	/**
	 * Indicates that assets recently released to PROD should be protected.
	 */
	PROTECT_RECENTLY_RELEASED: 'recently-released', 

	/**
	 * Indicates that assets should be protected from simultanous changes by multiple streams.
	 */
	PROTECT_SIMULTANEOUS: 'simultaneously-changed', 

	/**
	 * Enforce the protections provided by the Shared Asset Gatekeeper, namely, prevent uninformed/unvetted changes 
	 *	to Salesforce assets that are shared by multiple project streams or are just plain sensitive.
	 *
	 * @param protectMode - One of PROTECT_SHARED, PROTECT_RECENTLY_RELEASED, or PROTECT_SIMULTANEOUS.
	 * @param packageFileStr - Path to- and name of the XML-formatted package.xml file. This is the Salesforce-standard 
	 *	migration tool index file. Required.
	 * @param whitelistFileStr - Path to- and name of the XML-formatted whitelist file. The structure of this file 
	 *	should mimic the package.xml file. This file identifies those files/assets that are excluded from protection. 
	 *	One of either whitelistFileStr or blacklistFileStr are required.
	 * @param blacklistFileStr - Path to- and name of the XML-formatted blacklist file. The structure of this file 
	 *	should mimic the package.xml file. This file identifies those files/assets that are protected. One of either 
	 *	whitelistFileStr or blacklistFileStr are required.
	 * @param acknowledgementFileStr - Path to- and name of the XML-formatted acknowledgements file. Provides a 
	 *	mechanism for allowing (informed/vetted) modifications to protected files/assets. Required.
	 * @param callback - On completion, invoked with an object representation of any unacknowledged shared assets if 
	 *	any are found; else, invoked with null.
	 */
	enforceProtections: function(protectMode, packageFileStr, whitelistFileStr, blacklistFileStr, acknowledgementFileStr, callback) {
		var result = null;
		var moduleScope = this;

		_validateArguments(protectMode, packageFileStr, whitelistFileStr, blacklistFileStr, acknowledgementFileStr);

		var maskListCallback = function(err, maskList) {
			if (err !== null) {
				callback(err, null);
			} else {
				_processPackageFile(packageFileStr, maskList, protectMode, function(err, sharedAssetsFound) {
					if (err !== null) {
						callback(err, null);
					} else {
				    	//console.log('Matches:');
						//console.log(sharedAssetsFound);

						if (_.isEmpty(sharedAssetsFound)) {
							if (protectMode !== moduleScope.PROTECT_SIMULTANEOUS) {
								console.log('No unacknowledged ' + protectMode + ' asset(s) found.');
							}
							callback(err, result);
						} else {
							_processAcknowledgementFile(acknowledgementFileStr, sharedAssetsFound, protectMode, function(err, unackSharedAssetsFound) {
								if (err !== null) {
									callback(err, null);
								} else {
									if (!_.isEmpty(unackSharedAssetsFound)) {
										if (protectMode !== moduleScope.PROTECT_SIMULTANEOUS) {
											console.error('One or more unacknowledged ' + protectMode + ' asset(s) found: ');
											_.forEach(unackSharedAssetsFound, function(membersForType, type, coll1) {
												console.error('\t' + type);
												_.forEach(membersForType, function(member, idx, coll2) {
													console.error('\t\t' + member);
												});
											});
											console.error('');
											if (protectMode === moduleScope.PROTECT_SHARED) {
												console.error('You must acknowledge (in the acknowledgements file) that the changes to the above assets ' + 
													'have been properly analyzed, vetted, and socialized.');
												console.error('If appropriate, you may (alternatively) account for the above assets in the whitelist/blacklist file.');
											} else if (protectMode === moduleScope.PROTECT_RECENTLY_RELEASED) {
												console.error('You must acknowledge (in the acknowledgements file) that the changes to the above assets ' + 
													'have properly accounted for the changes that were recently released to PROD.');
											} 
										}
							    		result = unackSharedAssetsFound;
									} else {
										if (protectMode !== moduleScope.PROTECT_SIMULTANEOUS) {
											console.log('No unacknowledged ' + protectMode + ' asset(s) found.');
										}
									}

									callback(err, result);
								}
							});
						}
					}
				});
			}
		};
		
		if (whitelistFileStr !== null) {
			var whitelist = new Whitelist();
			_processMasklistFile(whitelistFileStr, whitelist, maskListCallback);
		} else if (blacklistFileStr !== null) {
			var blacklist = new Blacklist();
			_processMasklistFile(blacklistFileStr, blacklist, maskListCallback);
		}
	}

}

var _validateArguments = function(protectMode, packageFileStr, whitelistFileStr, blacklistFileStr, acknowledgementFileStr) {
	if (protectMode === null || typeof protectMode === 'undefined') {
		throw new Error('protectMode must be specified.');
	}

	if (protectMode !== module.exports.PROTECT_SHARED 
		&& protectMode !== module.exports.PROTECT_RECENTLY_RELEASED 
		&& protectMode !== module.exports.PROTECT_SIMULTANEOUS) 
	{
		throw new Error('Legal values for protectMode are \'' + module.exports.PROTECT_SHARED + '\', ' + 
			'\'' + module.exports.PROTECT_RECENTLY_RELEASED + '\', ' + 
			'or \'' + module.exports.PROTECT_SIMULTANEOUS + '\'.');
	}

	if (packageFileStr === null || typeof packageFileStr === 'undefined') {
		throw new Error('Path to- and name of the package.xml file must be provided.');
	}

	if ((whitelistFileStr === null || typeof whitelistFileStr === 'undefined') 
		&& (blacklistFileStr === null || typeof blacklistFileStr === 'undefined')) 
	{
		throw new Error('Path to- and name of either the whitelist or blacklist file must be provided.');
	}

	if ((whitelistFileStr !== null && blacklistFileStr !== null)) {
		throw new Error('Only the whitelist OR blacklist file may be provided, not both.');
	}

	if (acknowledgementFileStr === null || typeof acknowledgementFileStr === 'undefined') {
		throw new Error('Path to- and name of the acknowledgement file must be provided.');
	}

	if (protectMode === module.exports.PROTECT_RECENTLY_RELEASED && blacklistFileStr === null) 
	{
		throw new Error('When protectMode is set to ' + module.exports.PROTECT_RECENTLY_RELEASED + 
			', the blacklist file must be provided.');
	}

	if (protectMode === module.exports.PROTECT_SIMULTANEOUS && blacklistFileStr === null) 
	{
		throw new Error('When protectMode is set to ' + module.exports.PROTECT_SIMULTANEOUS + 
			', the blacklist file must be provided.');
	}
};

/**
 * Superclass of Blacklist and Whitelist.
 */
function Masklist() {
	// Currently, no inherited functionality/characteristics.
};

function Blacklist() {
	Masklist.call(this);
};
Blacklist.prototype = Object.create(Masklist.prototype);
Blacklist.prototype.constructor = Blacklist;

function Whitelist() {
	Masklist.call(this);
};
Whitelist.prototype = Object.create(Masklist.prototype);
Whitelist.prototype.constructor = Whitelist;

/**
 * Build and return an object to keep track of state as the XML data structures are being parsed.
 */
var _buildXmlParseState = function() {
	return {
		// The name of the current XML element. For example, "types", "members", "name", etc.
		currentEleName: null, 

		/*
		 * The values of the <members> elements belonging to the current <types> element. A structure akin to the 
		 *	following is assumed:
		 *	<types>
		 *	    <members>BypassMethods__c</members>
		 *	    <members>UserProvisioningMapping__mdt</members>
		 *	    <members>User_Settings__mdt</members>
		 *	    <name>CustomObject</name>
		 *	</types>		 
		 */
		currentTypeMembersList: []
	};
};

/**
 * Parse the masklist file located at the given filesystem path/location.
 *
 * @param masklistFileStr - Path to- and name of the XML-formatted masklist file.
 * @param callback - On success, invoked with the constructed masklist.
 */
var _processMasklistFile = function(masklistFileStr, masklist, callback) {
	fs.readFile(masklistFileStr, function(err, data) {
		if (err !== null) {
			var errMsg = 'Error raised reading masklist file - ' + err;
			callback(errMsg);
		} else {
			var xmlParseState = _buildXmlParseState();

			// See https://github.com/Leonidas-from-XIV/node-xml2js for more config info.
			var masklistXmlParser = new xml2js.Parser({
				tagNameProcessors: [function(eleName) {
					_processTagName(eleName, xmlParseState);
				}],
				valueProcessors: [function(eleValue) {
					_processElementValueMasklist(eleValue, masklist, xmlParseState)
				}]
			});

			/*
			 * Allow the masklist to contain commented-out elements (using agreed-upon 
			 *	commenting conventions) that will still be evaluated. This allows for 
			 *	the detection of simultaneous changes, where the blacklist is another 
			 *	project's package.xml file.
			 */
			var dataStr = _removeIndexFileComments(data.toString());
		    masklistXmlParser.parseString(
		    	dataStr, function (err, result) {
		    		if (err !== null) {
		    			var errMsg = 'Error raised parsing masklist file content - ' + err;
		    			callback(errMsg);
		    		} else {
			    		callback(null, masklist);
		    		}
		    	}
		    );
		}
	});
};

/**
 * Handle each XML element name found in the relevant XML.
 *
 * @param eleName - Name of the current XML element.
 * @param xmlParseState - Used to keep track of state as the XML data structure is being parsed.
 */
var _processTagName = function(eleName, xmlParseState){
    xmlParseState.currentEleName = eleName;

    // Whenever a new <types> element is reached, re-init the list of "members" for that type.
    if (xmlParseState.currentEleName === 'types') {
    	xmlParseState.currentTypeMembersList = [];
    }
};

/**
 * Handle each XML element value found in the relevant  masklist XML.
 *
 * @param eleValue - Value of the current XML element.
 * @param masklist - Context blacklist or whitelist that is to be populated from the XML.
 * @param xmlParseState - Used to keep track of state as the XML data structure is being parsed.
 */
var _processElementValueMasklist = function(eleValue, masklist, xmlParseState) {
	if (xmlParseState.currentEleName === 'members') {
		// Add the value of each <members> element to the current list of members.
	    xmlParseState.currentTypeMembersList.push(eleValue);
	} else if (xmlParseState.currentEleName === 'name') {
    	/*
    	 * Whenever a new <name> element is reached, add the current list of members to the context 
    	 *	mask list, keyed by type (<types>).
    	 */
	    masklist[eleValue] = xmlParseState.currentTypeMembersList;
	}
};

/**
 * Parse the package(.xml) file located at the given filesystem path/location.
 *
 * @param packageFileStr - Path to- and name of the XML-formatted package file.
 * @param masklist - Context blacklist or whitelist that is used to identify shared assets.
 * @param protectMode - One of PROTECT_SHARED, PROTECT_RECENTLY_RELEASED, or PROTECT_SIMULTANEOUS.
 * @param callback - On success, invoked with a collection of shared assets that are included in the 
 *	specified package file.
 */
var _processPackageFile = function(packageFileStr, maskList, protectMode, callback) {
	var xmlParseState = _buildXmlParseState();

	// Shared assets found in the specified package file.
	var sharedAssetsFound = {};

	// See https://github.com/Leonidas-from-XIV/node-xml2js for more config info.
	var packageXmlParser = new xml2js.Parser({
		tagNameProcessors: [function(eleName) {
			_processTagName(eleName, xmlParseState);
		}],
		valueProcessors: [function(eleValue) {
			_processElementValuePackage(eleValue, maskList, protectMode, xmlParseState, sharedAssetsFound)
		}]
	});

	fs.readFile(packageFileStr, function(err, data) {
		if (err !== null) {
			var errMsg = 'Error raised reading package file - ' + err;
			callback(errMsg);
		} else {
			var dataStr = _removeIndexFileComments(data.toString());
		    packageXmlParser.parseString(
		    	dataStr, function (err, result) {
		    		if (err !== null) {
		    			var errMsg = 'Error raised parsing package file content - ' + err;
		    			callback(errMsg);
		    		} else {
			        	//console.dir(result);
			        	//console.log(util.inspect(result, false, null));
			        	callback(null, sharedAssetsFound);
		    		}
		    	}
		    );
		}
	});
};

/**
 * Remove XML comments from the given string that conform to agreed-upon commenting conventions, 
 *	found here: 
 *	http://nextgen.team.allegisgroup.com:8090/display/COM/Development+-+Build+and+Deployment#Development-BuildandDeployment-IndexFileComments
 *
 * The commented-out elements are made active by this logic, not removed. For example: 
 * BEFORE
 *  <types>
 *  	<!-- MANUAL, COM-2015 <members>Communties_Update_PeopleSoft_ID-6</members>
 * 	 	 -->
 *      <!-- NON-CI, COM-2015 <members>Communties_Update_Foo-8</members> -->
 *      <name>Flow</name>
 *  </types> 
 *      <types>
 *      <!-- NON-REPEATABLE, COM-2015
 *      <members>Communties_Update_PeopleSoft_ID</members>
 *      -->
 *      <name>FlowDefinition</name>
 *  </types>
 * 
 * AFTER
 *  <types>
 *      <members>Communties_Update_PeopleSoft_ID-6</members>
 * 
 *      <members>Communties_Update_Foo-8</members>
 *    <name>Flow</name>
 *  </types>
 *      <types>
 *      <members>Communties_Update_PeopleSoft_ID</members>
 * 
 *      <name>FlowDefinition</name>
 *  </types>
 *
 * @param indexFileContentsStr - String to be processed, but left unmodified.
 * @return the given string, but with the previously-commented elements left uncommented.
 */
var _removeIndexFileComments = function(indexFileContentsStr) {
	//console.log(indexFileContentsStr);
	/*
	 * <!-- matches the characters <!-- literally (case sensitive)
	 * .* matches any character (except for line terminators)
	 * (MANUAL|NON-REPEATABLE|NON-CI) matches the characters MANUAL or NON-REPEATABLE or NON-CI literally (case sensitive)
	 * [^]*? matches any character, including newline
	 * *? Quantifier — Matches between zero and unlimited times, as few times as possible, expanding as needed (lazy)
	 * < matches the character < literally (case sensitive)
	 * g modifier: global. All matches (don't return after first match)
	 *
	 * Paste into https://www.regex101.com/ for a full explanation.
	 */
	//var regExp1 = new RegExp(/<!--.*(MANUAL|NON-REPEATABLE|NON-CI)[^]*?</g);

	/*
	 * [^] matches any character, including newline
	 * --> matches the characters --> literally (case sensitive)
	 * g modifier: global. All matches (don't return after first match)
	 *
	 * Paste into https://www.regex101.com/ for a full explanation.
	 */
	//var regExp2 = new RegExp(/[^]-->/g);

	//indexFileContentsStr = indexFileContentsStr.replace(regExp1, '$2');
	//indexFileContentsStr = indexFileContentsStr.replace(regExp2, '');

	/*
	 * <!-- matches the characters <!-- literally (case sensitive)
	 * .* matches any character (except for line terminators)
	 *   * Quantifier — Matches between zero and unlimited times, as many times as possible, giving back as needed (greedy)
	 * Non-capturing group (?:MANUAL|NON-REPEATABLE|NON-CI)
	 *   1st Alternative MANUAL
	 *     MANUAL matches the characters MANUAL literally (case sensitive)
	 *   2nd Alternative NON-REPEATABLE
	 *     NON-REPEATABLE matches the characters NON-REPEATABLE literally (case sensitive)
	 *   3rd Alternative NON-CI
	 *     NON-CI matches the characters NON-CI literally (case sensitive)
	 * [^]*? matches any character, including newline
	 *   *? Quantifier — Matches between zero and unlimited times, as few times as possible, expanding as needed (lazy)
	 * 1st Capturing Group (<[^]*?)
	 *   < matches the character < literally (case sensitive)
	 *   [^]*? matches any character, including newline
	 *     *? Quantifier — Matches between zero and unlimited times, as few times as possible, expanding as needed (lazy)
	 * --> matches the characters --> literally (case sensitive)
	 * Global pattern flags
	 *   g modifier: global. All matches (don't return after first match)
	 *
	 * Paste into https://www.regex101.com/ for a full explanation.
	 */
	var regExp = new RegExp(/<!--.*(?:MANUAL|NON-REPEATABLE|NON-CI)[^]*?(<[^]*?)-->/g);

	// Replace the matched expression using the contents of the first (only) capture group.
	indexFileContentsStr = indexFileContentsStr.replace(regExp, '$1');
	//console.log(indexFileContentsStr);

	return indexFileContentsStr;
};

/**
 * Handle each XML element value found in the relevant package XML.
 *
 * @param eleValue - Value of the current XML element.
 * @param masklist - Context blacklist or whitelist that is used to identify shared assets.
 * @param protectMode - One of PROTECT_SHARED, PROTECT_RECENTLY_RELEASED, or PROTECT_SIMULTANEOUS.
 * @param xmlParseState - Used to keep track of state as the XML data structure is being parsed.
 * @param sharedAssetsFound - Accumulated collection of shared assets found in the context package XML.
 */
var _processElementValuePackage = function(eleValue, maskList, protectMode, xmlParseState, sharedAssetsFound){
	if (xmlParseState.currentEleName === 'members') {
		// Add the value of each <members> element to the current list of members.
	    xmlParseState.currentTypeMembersList.push(eleValue);
	} else if (xmlParseState.currentEleName === 'name') {
    	/*
    	 * Whenever a new <name> element is reached, loop through the current list of members, checking 
    	 *	to see if each is shared.
    	 */
		var typeName = eleValue;
		_.forEach(xmlParseState.currentTypeMembersList, function(memberName, idx, coll) {
			var isSharedAsset = false;

			// Items included in the blacklist ARE shared, whereas items in the whitelist ARE NOT shared.
			if (maskList instanceof Blacklist) {
				//console.log('black');
				isSharedAsset = _listContains(maskList, typeName, memberName, protectMode);
			} else if (maskList instanceof Whitelist) {
				//console.log('white');
				isSharedAsset = !(_listContains(maskList, typeName, memberName, protectMode));
			}

			// If the current member is a shared asset, add it to the collection, keyed by type (<type>).
			if (isSharedAsset) {
				//console.log(typeName + '.' + memberName);
				var sharedAssetsFoundForType = sharedAssetsFound[typeName];
				if (sharedAssetsFoundForType === null || (typeof sharedAssetsFoundForType === 'undefined')) {
					sharedAssetsFoundForType = [];
					sharedAssetsFound[typeName] = sharedAssetsFoundForType;
				}
				sharedAssetsFoundForType.push(memberName);
			}
		});	
	}
};

/**
 * Return true if the given masklist contains a member of the specified type, else false.
 *
 * @param protectMode - One of PROTECT_SHARED, PROTECT_RECENTLY_RELEASED, or PROTECT_SIMULTANEOUS.
 */
var _listContains = function(masklist, typeName, memberName, protectMode) {
	var result = false;
	var masklistTypeMembers = masklist[typeName];
	var idx = _.findIndex(masklistTypeMembers, function(oneMemberName, idx, coll) {
		/*
		 * In modes PROTECT_RECENTLY_RELEASED or PROTECT_SIMULTANEOUS, Flows need to be matched irrespective 
		 *	of version number. This is because, in these modes, the blacklist is another package.xml, which 
		 *	cannot contain wildcards for flows.
		 * For example: 
		 * <members>Communties_Update_PeopleSoft_ID-6</members>
		 */
		if (typeName === 'Flow') {
			if (protectMode === module.exports.PROTECT_RECENTLY_RELEASED || protectMode === module.exports.PROTECT_SIMULTANEOUS) {
				/*
				 * - matches the character - literally (case sensitive)
				 * \d+ matches a digit (equal to [0-9])
				 *   + Quantifier — Matches between one and unlimited times, as many times as possible, giving back as needed (greedy)
				 * $ asserts position at the end of the string
				 * 
				 * Paste into https://www.regex101.com/ for a full explanation.
				 */
				var regExp = new RegExp(/-\d+$/);
				/*
				 * Replace any trailing digits with a match pattern for those digits. For example: 
				 *	OwnershipFlowInvoked-2
				 * becomes...
				 *	OwnershipFlowInvoked-\d+$
				 */
				oneMemberName = oneMemberName.replace(regExp, '-\\d+$');
				//console.log('oneMemberName = ' + oneMemberName);
			}
		} else {
			/*
			 * $ asserts position at the end of the string
			 *	This prevents "TC_CustomSettingControllerTest" from being matched by "TC_CustomSettingController".
			 */
			oneMemberName = oneMemberName + '$';
		}

		// The member name in the masklist could actually be a regular expression.
		var matchPattern = oneMemberName;
		var regExp = new RegExp(matchPattern, 'i');
		// We have a match if a hit is made starting at the first character.
		return (memberName.search(regExp) === 0);
	});	

	if (idx > -1) {
		// The given member name matches at least one entry in the masklist.
		result = true;
	}
	//console.log('typeName = ' + typeName + ', memberName = ' + memberName + ', result = ' + result);
	return result;
};

/**
 * Parse the acknowledgement file located at the given filesystem path/location.
 *
 * @param acknowledgementFileStr - Path to- and name of the XML-formatted acknowledgement file.
 * @param sharedAssetsFound - Accumulated collection of shared assets found.
 * @param protectMode - Indicates which entries in the specified acks are relevant.
 * @param callback - On success, invoked with a collection of shared assets that are NOT found in the 
 *	specified acknowledgements file.
 */
var _processAcknowledgementFile = function(acknowledgementFileStr, sharedAssetsFound, protectMode, callback) {
	fs.readFile(acknowledgementFileStr, function(err, data) {
		if (err !== null) {
			var errMsg = 'Error raised reading acknowledgements file - ' + err;
			callback(errMsg);
		} else {
			var acksXmlParser = new xml2js.Parser({

			});

		    acksXmlParser.parseString(
		    	data, function (err, result) {
		    		if (err !== null) {
		    			var errMsg = 'Error raised parsing acknowledgements file content - ' + err;
		    			callback(errMsg);
		    		} else {
		    			var ackColl = _buildAcknowledgementCollection(JSON.parse(JSON.stringify(result)), protectMode);

		    			var unackSharedAssetsFound = null;
		    			if (_.isEmpty(ackColl)) {
		    				unackSharedAssetsFound = sharedAssetsFound;
		    			} else {
			    			unackSharedAssetsFound = _filterAcknowledgedMatches(sharedAssetsFound, ackColl);
		    			}
		    			callback(null, unackSharedAssetsFound);
		    		}
		    	}
		    );
		}
	});
};

/**
 * Build and return an object (that is not specific to node-xml2js) encapsulating the contents of the 
 *	given acknowledgements object (which is specific to node-xml2js).
 *
 * @param protectMode - Indicates which entries in the given acksXmlObj are relevant.
 */
var _buildAcknowledgementCollection = function(acksXmlObj, protectMode) {
	var ackColl = [];
	
	/*
	 * The acks are categorized by protection mode. Only grab the acks for the context mode.
	 */
	var acksXmlTypeSpecificSection = null;
	if (protectMode === module.exports.PROTECT_SHARED) {
		acksXmlTypeSpecificSection = acksXmlObj.acknowledgements['shared-changes'];
	} else if (protectMode === module.exports.PROTECT_RECENTLY_RELEASED) {
		acksXmlTypeSpecificSection = acksXmlObj.acknowledgements['recently-released'];
	} else if (protectMode === module.exports.PROTECT_SIMULTANEOUS) {
		acksXmlTypeSpecificSection = acksXmlObj.acknowledgements['simultaneous-changes'];
	}

	var acksXmlObjArr = null;
	if (typeof acksXmlTypeSpecificSection !== 'undefined' && acksXmlTypeSpecificSection.length > 0) {
		acksXmlObjArr = acksXmlTypeSpecificSection[0].acknowledgement;
	}

	//console.log('acksXmlObjArr = ' + JSON.stringify(acksXmlObjArr));
	_.forEach(acksXmlObjArr, function(ackXmlObj, idx, coll) {
		var ack = {};
		// {"_":"\r\n        Foo\r\n    ","$":{"type":"CustomObject","member":"BypassMethods__c","acknowledger":"Michael Scepaniak","ack-date":"20161122"}}
		ack.type = ackXmlObj.$.type;
		ack.member = ackXmlObj.$.member;
		ack.acknowledger = ackXmlObj.$.acknowledger;
		ack.ackDate = ackXmlObj.$['ack-date'];
		ack.whyOk = ackXmlObj._;
		if (ack.type.trim() === '' 
			|| ack.member.trim() === '' 
			|| ack.acknowledger.trim() === '' 
			|| ack.ackDate.trim() === '' 
			|| ack.whyOk.trim() === '') 
		{
			console.warn('Incomplete acknowledgement found. Ignoring - ' + JSON.stringify(ack));
		} else {
			ackColl.push(ack);
		}
	});	
	return ackColl;
};

/**
 * Identiy (and return) from the given collection of found shared assets those assets that are NOT properly 
 *	acknowledged (as safe) in the the given collection of acknowledgements.
 */
var _filterAcknowledgedMatches = function(sharedAssetsFound, ackColl) {
	var result = {};

	var typeToMembersMap = sharedAssetsFound;

	// Loop through each type (<types>).
	_.forEach(typeToMembersMap, function(membersForType, type, coll1) {
		/*
		 * Loop through each member (<members>) for one type (<types>), removing those members that are 
		 * 	properly acknowledged.
		 */
		var membersFiltered = _.reject(membersForType, function(member) {
			// In practice, there should only be one ack per member.
			var acksForMember = _.filter(ackColl, function(oneAck, idx, coll4) { 
				var result = false;
				if (oneAck.type === type) {
					// The member name in the ack could actually be a regular expression.
					var matchPattern = oneAck.member;
					var regExp = new RegExp(matchPattern, 'i');
					// We have a match if a hit is made starting at the first character.
					result = (member.search(regExp) === 0);
					//console.log('matchPattern = ' + matchPattern + ', result = ' + result);
				}
				return result;
			});
			return acksForMember.length > 0;
		});

		result[type] = membersFiltered;
		if (_.isEmpty(result[type])) {
			delete result[type];
		}
	});	

	return result;
};






