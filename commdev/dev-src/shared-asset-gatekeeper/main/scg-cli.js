#!/usr/bin/env node

'use strict';

var commander = require('commander');
var _ = require('lodash');

var scg = require('./scg');

/**
 * Command line interface for Simultaneous Changes Gatekeeper, which detects changes made to the same 
 *	Salesforce assets.
 * More info - http://nextgen.team.allegisgroup.com:8090/display/COM/Development+-+Build+and+Deployment#Development-BuildandDeployment-SalesforceAssetGatekeeper
 */

/*
 * Formatting constants.
 */
var INDENT = '  ';
var INDENT_EXTRA = '                               ';

// Holds the value passed in for the <dir> argument.
var primaryAssetDir = null;

// Holds the value(s) passed in the for the [otherDirs...] argument(s).
var otherAssetDirs = [];

commander
	.description('Simultaneous Changes Gatekeeper - Detects changes made to the same Salesforce assets.')
	.version('0.0.1')
	.usage('[dir] [otherDirs...]')
	.arguments('<dir> [otherDirs...]')
	.action(function (dir, otherDirs) {
		primaryAssetDir = dir;
		otherAssetDirs = otherDirs;
	})
	.on('--help', function() {
        console.log(INDENT + '<dir> is the primary asset directory, whereas [otherDirs...] is one or more \n' + 
        	INDENT + 'other assets directories to which the primary is to be compared. All given directories \n' + 
        	INDENT + 'are assumed to contain a target-release.xml file and src sub-directory that contains a \n' + 
        	INDENT + 'package.xml file and sag-acknowledgements.xml file.');
        console.log();
    })
  	.parse(process.argv);

//console.log('primaryAssetDir = ' + primaryAssetDir);
//console.log('otherAssetDirs = ' + otherAssetDirs);

var errorsFound = [];

if (primaryAssetDir === null) {
	errorsFound.push('The primary asset directory must be specified. Please consult the help (--help) for details.');
}
if (otherAssetDirs.length <= 0) {
	errorsFound.push('At least one "other" asset directory must be specified. Please consult the help (--help) for details.');
}

if (errorsFound.length === 0) {
	/*
	 * Resolve the directory arguments relative to the current working directory, NOT relative to the 
	 *	location of the program.
	 */
	primaryAssetDir = process.cwd() + '/' + primaryAssetDir;
	_.forEach(otherAssetDirs, function(otherAssetDir, idx, coll) {
		otherAssetDirs[idx] = process.cwd() + '/' + otherAssetDir;
	});	
} else {
	console.error('One or more errors found:');
	errorsFound.forEach(function(errorFound) {
	    console.error(INDENT + '- ' + errorFound);
	});
	process.exit(1);
}

scg.enforceProtections(primaryAssetDir, otherAssetDirs, function(err, result) {
	/*
	 * The result is an object representation of any unacknowledged simultaneously changed assets that are found. If null, 
	 *	none were found (which indicates success).
	 */
	if (err !== null) {
		console.error(err);
		process.exit(1);
	} else if (result === null) {
		process.exit(0);
	} else {
		process.exit(1);
	}

	/*
	 * In order to see the exit code from the shell, execute the following after invoking the sag-cli program: 
	 *	echo $?
	 */
});


