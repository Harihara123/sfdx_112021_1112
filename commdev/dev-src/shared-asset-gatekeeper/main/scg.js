'use strict';

var fs = require('fs');
var util = require('util');

var _ = require('lodash');
var asyncEach = require ('async/each');
var asyncFilter = require ('async/filter');
var moment = require('moment');
var xml2js = require('xml2js');

var sag = require('./sag');

/**
 * Simultaneous Changes Gatekeeper - Detects simultaneous changes made to the same Salesforce assets.
 * More info - http://nextgen.team.allegisgroup.com:8090/display/COM/Development+-+Build+and+Deployment#Development-BuildandDeployment-SalesforceAssetGatekeeper
 */
module.exports = {

	nowMoment: moment(),

	/**
	 * Enforce the protections provided by the Simultaneous Changes Gatekeeper, namely, prevent uncoordinated 
	 *	simultaneous changes to the same Salesforce assets.
	 *
	 * @param primaryAssetRoot - Path to the directory that contains a release config file (target-release.xml) 
	 *	and Salesforce-standard migration tool index file (src/package.xml). The index file identifies those 
	 *	files/assets that are being changed. Required.
	 * @param otherAssetRoots - Array of paths to one or more directories that contain a release config file 
	 *	(target-release.xml) and Salesforce-standard migration tool index file (src/package.xml). The index file 
	 *	identifies those files/assets that are being changed. Required.
	 * @param callback - On completion, invoked with an object representation of any unacknowledged simultaneous 
	 *	changed assets if any are found; else, invoked with null.
	 */
	enforceProtections: function(primaryAssetRoot, otherAssetRoots, callback) {
		var result = null;

		var moduleScope = module.exports;
		_validateArguments(primaryAssetRoot, otherAssetRoots);

		_retrieveReleaseMomentForAssetDir(primaryAssetRoot, function(err, releaseMoment) {
			if (err !== null) {
				callback(err, result);
			} else {
				if (releaseMoment === null) {
					console.warn('No target release date found for %s', primaryAssetRoot);
					callback(err, result);
				} else if (!releaseMoment.isValid()) {
					callback('Unparseable target release date found for ' + primaryAssetRoot, null);
				} else if (releaseMoment.isBefore(moduleScope.nowMoment)) {
					callback('Past target release date found for ' + primaryAssetRoot, null);
				} else {
					_retrieveOverlappingReleases(otherAssetRoots, releaseMoment, function(err, otherAssetRootsWithReleaseOverlap) {
						if (err !== null) {
							callback(err, result);
						} else {
							_compareToOverlappingReleases(primaryAssetRoot, otherAssetRootsWithReleaseOverlap, function(err, compareResults) {
								if (err !== null) {
									callback(err, result);
								} else {
									if (_.size(compareResults) > 0) {
										console.error('One or more unacknowledged simultaneous changes found in one or more asset roots:');
										_.forEach(compareResults, function(sagResult, otherAssetRoot, coll1) {
											console.error(otherAssetRoot);
											_.forEach(sagResult, function(membersForType, type, coll1) {
												console.error('\t' + type);
												_.forEach(membersForType, function(member, idx, coll2) {
													console.error('\t\t' + member);
												});
											});
										});
										console.error('');
										console.error('Another developer in a different stream is making changes to the same asset. ' + 
											'Go find out who it is and make sure that you all are on the same page. Once that is done, ' + 
											'you must acknowledge (in the acknowledgements file) that the changes to the above assets ' + 
											'have been properly analyzed, vetted, and socialized.');
										result = compareResults;
									} else {
										console.log('No unacknowledged simultaneously changed asset(s) found.');
									}

									callback(null, result);
								}
							});
						}
					})
				}
			}
		});
	}
}

var _validateArguments = function(primaryAssetRoot, otherAssetRoots) {
	if (primaryAssetRoot === null || typeof primaryAssetRoot === 'undefined') {
		throw new Error('Path to the primary asset root directory that contains the target-release.xml ' + 
			'file and src/package.xml file must be provided.');
	}

	if ((otherAssetRoots === null || typeof otherAssetRoots === 'undefined' || otherAssetRoots.length <= 0)) 
	{
		throw new Error('One or more paths to other asset root directories must be provided.');
	}
};

/**
 * Resolve and return the target release Moment object for the given directory.
 *
 * @param assetRoot - Path to a directory that contains a release config file named target-release.xml.
 * @param callback - On success, invoked with a Moment object.
 */
var _retrieveReleaseMomentForAssetDir = function(assetRoot, callback) {
	fs.readFile(assetRoot + '/target-release.xml', function(err, data) {
		if (err !== null) {
			var errMsg = 'Error raised reading release config file - ' + err;
			callback(errMsg);
		} else {
			// See https://github.com/Leonidas-from-XIV/node-xml2js for more config info.
			var releaseXmlParser = new xml2js.Parser({

			});

		    releaseXmlParser.parseString(
		    	data, 
		    	function (err, result) {
		    		if (err !== null) {
		    			var errMsg = 'Error raised parsing release config file content - ' + err;
		    			callback(errMsg);
		    		} else {
	    				var releaseMoment = _retrieveReleaseMomentFromXml(JSON.parse(JSON.stringify(result)));
	    				callback(null, releaseMoment);
		    		}
		    	}
		    );
		}
	});
};

/**
 * Resolve and return the target release Moment object for the date contained in the 
 *	given release config object (which is specific to node-xml2js).
 */
var _retrieveReleaseMomentFromXml = function(releaseXmlObj) {
	var result = null;
	if (releaseXmlObj !== null && 
		typeof releaseXmlObj.release !== 'undefined' && 
		typeof releaseXmlObj.release.date !== 'undefined') 
	{
		var datesXmlObjArr = releaseXmlObj.release.date;

		// We are only expecting one <date> element.
		if (datesXmlObjArr.length > 0) {
			var dateXmlObj = datesXmlObjArr[0];
			// Retrieve the value of the element.
			var dateStr = dateXmlObj._;
			if (dateStr !== null && typeof dateStr !== 'undefined' && dateStr.length > 0) {
				// The date format should be provided in the XML along with the date, itself.
				var dateFormat = dateXmlObj.$.format;
				result = moment(dateStr, dateFormat);
			}
		}
	}

	return result;
};

/**
 * Determine and provide which of the given "other" asset roots have a target release date that 
 *	that overlaps with the given target release Moment object.
 *
 * @param otherAssetRoots - Array of paths to directories that contain a release config file named 
 *	target-release.xml.
 * @param primaryReleaseMoment - Moment object.
 */
var _retrieveOverlappingReleases = function(otherAssetRoots, primaryReleaseMoment, callback) {
	var moduleScope = module.exports;

	/*
	 * Start the overlap period 4 days prior to- and 3 days after the given Moment.
	 */
	var primaryReleaseOverlapStartMoment = primaryReleaseMoment.clone().subtract(4, 'days');
	var primaryReleaseOverlapEndMoment = primaryReleaseMoment.clone().add(3, 'days');

	/*
	 * Filter the given collection (array) of "other" asset roots by overlapping release date. 
	 *	Do the filtering asynchronously, receiving the aggregated results in the second callback.
	 */
	asyncFilter(otherAssetRoots, function(otherAssetRoot, callback1) {
		_retrieveReleaseMomentForAssetDir(otherAssetRoot, function(err, otherAssetReleaseMoment) {
			if (err !== null) {
				callback1(err, false);
			} else {
				if (otherAssetReleaseMoment === null) {
					console.warn('No target release date found for %s', otherAssetRoot);
					callback1(null, false);
				} else if (!otherAssetReleaseMoment.isValid()) {
					callback1('Unparseable target release date found for ' + otherAssetRoot, false);
				} else if (otherAssetReleaseMoment.isBefore(primaryReleaseOverlapStartMoment)) {
					console.warn('Past target release date (outside of the overlap window) found for ' + otherAssetRoot);
					callback1(null, false);
				} else {
					/* 
					 * Determine if the release date for the "other" asset root is included (inclusively, 
					 *	due to the "[]") in the overlap period.
					 */
					var overlapExists = moment(otherAssetReleaseMoment)
						.isBetween(primaryReleaseOverlapStartMoment, primaryReleaseOverlapEndMoment, null, '[]');
					if (overlapExists) {
						callback1(null, true);
					} else {
						callback1(null, false);
					}
				}
			}
		});
	}, function(err, otherAssetRootsWithReleaseOverlap) {
		if (err !== null) {
			callback(err);
		} else {
			callback(null, otherAssetRootsWithReleaseOverlap);
		}
	});
};

/**
 * Compare the package.xml for the given primary asset root to the package.xml files for 
 *	the given "other" asset roots, flagging for any common assets found.
 *
 * @param primaryAssetRoot - Path to a directory that contains a package.xml file and
 *	sag-acknowledgements.xml file in a src sub-directory.
 * @param otherAssetRoots - Array of paths to directories that contain a package.xml file and 
 *	sag-acknowledgements.xml file in a src sub-directory.
 */
var _compareToOverlappingReleases = function(primaryAssetRoot, otherAssetRoots, callback) {
	var sagResults = {};

	var primaryPackageFileStr = primaryAssetRoot + '/src/package.xml';
	var acknowledgementFileStr = primaryAssetRoot + '/src/sag-acknowledgements.xml';

	// For each "other" asset root, enforce protections (asynchronously).
	asyncEach(otherAssetRoots, function(otherAssetRoot, callback1) {
		// The "other" package.xml file serves as a blacklist.
		var otherPackageFileStr = otherAssetRoot + '/src/package.xml';
		sag.enforceProtections(sag.PROTECT_SIMULTANEOUS, primaryPackageFileStr, null, otherPackageFileStr, acknowledgementFileStr, function(err, result) {
			/*
			 * The result is an object representation of any unacknowledged simultaneously changed assets that are found. 
			 *	If null, none were found (which indicates success).
			 */
			if (err !== null) {
				console.error(err);
				callback1(err);
			} else if (result === null) {
				callback1(null);
			} else {
				// Aggregate the results for communication at the end.
				sagResults[otherAssetRoot] = result;
				callback1(null);
			}
		});
	}, function(err) {
		if (err !== null) {
			callback(err);
		} else {
			callback(null, sagResults);
		}
	});
};

