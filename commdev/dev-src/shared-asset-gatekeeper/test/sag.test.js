import test from 'ava';

import sag from '../main/sag';

test('no protect mode arg', async function(t) {
	t.plan(1);

	t.throws(function() {
		sag.enforceProtections();
	}, new RegExp('protectMode must be specified', 'i'));
});

test('illegal protect mode arg', async function(t) {
	t.plan(1);

	t.throws(function() {
		sag.enforceProtections('foo');
	}, new RegExp('Legal values for protectMode are', 'i'));
});

test('no package file arg', async function(t) {
	t.plan(1);

	t.throws(function() {
		sag.enforceProtections(sag.PROTECT_SHARED);
	}, new RegExp('package.xml file must be provided', 'i'));
});

test('no masklist file args', async function(t) {
	t.plan(1);

	t.throws(function() {
		sag.enforceProtections(sag.PROTECT_SHARED, 'package.xml');
	}, new RegExp('whitelist or blacklist file must be provided', 'i'));
});

test('no ack file arg', async function(t) {
	t.plan(1);

	t.throws(function() {
		sag.enforceProtections(sag.PROTECT_SHARED, 'package.xml', 'whitelist.xml', null);
	}, new RegExp('acknowledgement file must be provided', 'i'));
});

test('both masklist file args', async function(t) {
	t.plan(1);

	t.throws(function() {
		sag.enforceProtections(sag.PROTECT_SHARED, 'package.xml', 'whitelist.xml', 'blacklist.xml');
	}, new RegExp('whitelist OR blacklist file may be provided, not both', 'i'));
});

test('protect mode rr and no blacklist file arg', async function(t) {
	t.plan(1);

	t.throws(function() {
		sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'package.xml', 'whitelist.xml', null, 'acknowledgements.xml');
	}, new RegExp('When protectMode is set to ' + sag.PROTECT_RECENTLY_RELEASED + ', the blacklist file must be provided', 'i'));
});

test.cb('non-existent masklist file', function(t) {
	t.plan(1);

	sag.enforceProtections(sag.PROTECT_SHARED, 'package.xml', 'whitelist.does-not-exist.xml', null, 'acknowledgements.xml', function(err, result) {
		t.regex(err, new RegExp('Error raised reading masklist file', 'i'));
		t.end();
	});
});

test.cb('non-existent package file', function(t) {
	t.plan(1);

	sag.enforceProtections(sag.PROTECT_SHARED, 'package.does-not-exist.xml', 'test/fixtures/whitelist.empty.xml', null, 'acknowledgements.xml', function(err, result) {
		t.regex(err, new RegExp('Error raised reading package file', 'i'));
		t.end();
	});
});

test.cb('empty package and masklist files', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.empty.xml', 'test/fixtures/whitelist.empty.xml', null, 'acknowledgements.xml', function(err, result) {
		t.is(err, null);
		t.is(result, null);
		t.end();
	});
});

test.cb('minimal masklist file', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.empty.xml', 'test/fixtures/whitelist.minimal.xml', null, 'acknowledgements.xml', function(err, result) {
		t.is(err, null);
		t.is(result, null);
		t.end();
	});
});

test.cb('minimal package and masklist files', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.minimal.xml', 'test/fixtures/whitelist.minimal.xml', null, 'acknowledgements.xml', function(err, result) {
		t.is(err, null);
		t.is(result, null);
		t.end();
	});
});

test.cb('non-existent acks file', function(t) {
	t.plan(1);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', 'test/fixtures/whitelist.minimal.xml', null, 'acknowledgements.does-not-exist.xml', function(err, result) {
		t.regex(err, new RegExp('Error raised reading acknowledgements file', 'i'));
		t.end();
	});
});

test.cb('malformed masklist file', function(t) {
	t.plan(1);

	sag.enforceProtections(sag.PROTECT_SHARED, 'package.xml', 'test/fixtures/whitelist.malformed.xml', null, 'acknowledgements.xml', function(err, result) {
		t.regex(err, new RegExp('Error raised parsing masklist file', 'i'));
		t.end();
	});
});

test.cb('malformed package file', function(t) {
	t.plan(1);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.malformed.xml', 'test/fixtures/whitelist.empty.xml', null, 'acknowledgements.xml', function(err, result) {
		t.regex(err, new RegExp('Error raised parsing package file', 'i'));
		t.end();
	});
});

test.cb('malformed acks file', function(t) {
	t.plan(1);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', 'test/fixtures/whitelist.minimal.xml', null, 'test/fixtures/acknowledgements.malformed.xml', function(err, result) {
		t.regex(err, new RegExp('Error raised parsing acknowledgements file', 'i'));
		t.end();
	});
});

test.cb('package #1 and minimal whitelist', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', 'test/fixtures/whitelist.minimal.xml', null, 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the whitelist file is empty, the element in the package is flagged.
		 */
		t.deepEqual(result, {
			StaticResource: [
				"tc_script"
			]
		});
		t.end();
	});
});

test.cb('package #1 and whitelist #1', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', 'test/fixtures/whitelist.1.xml', null, 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the whitelist file covers the content of the package, no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('package #1, minimal whitelist, and acks #1', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', 'test/fixtures/whitelist.minimal.xml', null, 'test/fixtures/acknowledgements.1.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the acks file covers the content of the package, no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('package #1 and blacklist #1', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', null, 'test/fixtures/blacklist.1.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the blacklist covers the content of the package, the element in the package is flagged.
		 */
		t.deepEqual(result, {
			StaticResource: [
				"tc_script"
			]
		});
		t.end();
	});
});

test.cb('package #1, blacklist #1, and acks #1', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', null, 'test/fixtures/blacklist.1.xml', 'test/fixtures/acknowledgements.1.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the acks file covers the content of the package, no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('package #1, blacklist #1, and acks #1 incomplete', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', null, 'test/fixtures/blacklist.1.xml', 'test/fixtures/acknowledgements.1.incomplete.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the ack in the acks file is incomplete, the element in the package is flagged.
		 */
		t.deepEqual(result, {
			StaticResource: [
				"tc_script"
			]
		});
		t.end();
	});
});

test.cb('package #1 and whitelist #1 regex', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', 'test/fixtures/whitelist.1.regex.xml', null, 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the whitelist file covers the content of the package (via regex), no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('package #1, blacklist #1, and acks #1 regex', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', null, 'test/fixtures/blacklist.1.xml', 'test/fixtures/acknowledgements.1.regex.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the acks file covers the content of the package (via regex), no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('package #1 and blacklist #1 regex', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.1.xml', null, 'test/fixtures/blacklist.1.regex.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the blacklist covers the content of the package (via regex), the element in the package is flagged.
		 */
		t.deepEqual(result, {
			StaticResource: [
				"tc_script"
			]
		});
		t.end();
	});
});

test.cb('package with comments and TC whitelist', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.comments.xml', 'test/fixtures/whitelist.tc.xml', null, 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Properly-commented elements are enabled for validation, which are then flagged as 
		 *	not covered by the whitelist.
		 */
		t.deepEqual(result, {
			Flow: [
				"Communties_Update_PeopleSoft_ID-6", 
				"Communties_Update_Foo-8"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('package #2 and TC whitelist', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.2.xml', 'test/fixtures/whitelist.tc.xml', null, 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the whitelist and acks files do not cover everything in the package, several 
		 *	element in the package are flagged.
		 */
		t.deepEqual(result, {
			ApexClass: [
				"UserProvisioningHTTPRequestMock", 
				"UserTriggerHandler", 
				"ReqsTriggerHandler", 
				"BypassLogic"
			], CustomField: [
				"User.User_Provisioning_Status__c", 
				"BypassMethods__c.Key__c", 
				"User.Corporate_Email__c", 
				"BypassMethods__c.Disable__c", 
				"UserProvisioningMapping__mdt.Object__c", 
				"UserProvisioningMapping__mdt.Object_Fields__c", 
				"UserProvisioningMapping__mdt.VariablesKey__c", 
				"Account.RecentWorkHistory__c", 
				"Contact.Talent_Ownership__c"
			], CustomMetadata: [
				"*"
			], CustomObject: [
				"BypassMethods__c", 
				"UserProvisioningMapping__mdt", 
				"User_Settings__mdt"
			], ApexTrigger: [
				"UserTrigger"
			], 
			Flow: [
				"Communties_Update_PeopleSoft_ID-6"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('package #2, TC whitelist, and acks #2 apex', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.2.xml', 'test/fixtures/whitelist.tc.xml', null, 'test/fixtures/acknowledgements.2.apex.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the whitelist and acks files do not cover everything in the package, several 
		 *	element in the package are flagged. Because the acks file covers every Apex file, 
		 *	that entire element is removed from the result.
		 */
		t.deepEqual(result, {
			CustomField: [
				"User.User_Provisioning_Status__c", 
				"BypassMethods__c.Key__c", 
				"User.Corporate_Email__c", 
				"BypassMethods__c.Disable__c", 
				"UserProvisioningMapping__mdt.Object__c", 
				"UserProvisioningMapping__mdt.Object_Fields__c", 
				"UserProvisioningMapping__mdt.VariablesKey__c", 
				"Account.RecentWorkHistory__c", 
				"Contact.Talent_Ownership__c"
			], CustomMetadata: [
				"*"
			], CustomObject: [
				"BypassMethods__c", 
				"UserProvisioningMapping__mdt", 
				"User_Settings__mdt"
			], ApexTrigger: [
				"UserTrigger"
			], 
			Flow: [
				"Communties_Update_PeopleSoft_ID-6"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('package #2, TC whitelist, and acks #2', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SHARED, 'test/fixtures/package.2.xml', 'test/fixtures/whitelist.tc.xml', null, 'test/fixtures/acknowledgements.2.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the acks file covers the content of the package, no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('protect mode rr, package #2, blacklist flows, and acks #2 (shared)', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'test/fixtures/package.2.xml', null, 'test/fixtures/blacklist.flows.xml', 'test/fixtures/acknowledgements.2.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the acks file covers the blacklisted elements, but NOT using the correct 
		 *	mode element ("shared-changes" instead of "recently-released", given that we're in 
		 *	PROTECT_RECENTLY_RELEASED), the elements are flagged.
		 */
		t.deepEqual(result, {
			Flow: [
				"Communties_Update_PeopleSoft_ID-6"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('protect mode rr, package #2, blacklist flows, and acks #2 (rr)', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'test/fixtures/package.2.xml', null, 'test/fixtures/blacklist.flows.xml', 'test/fixtures/acknowledgements.2.rr.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the acks file covers the content of the package using the correct 
		 *	mode element ("recently-released"), no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('protect mode rr, package #2, blacklist flows, and acks minimal', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'test/fixtures/package.2.xml', null, 'test/fixtures/blacklist.flows.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the blacklist covers v7 of the flow, and we're in PROTECT_RECENTLY_RELEASED, 
		 *	and the acks don't cover the flow, v6 of the flow is flagged.
		 */
		t.deepEqual(result, {
			Flow: [
				"Communties_Update_PeopleSoft_ID-6"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('protect mode rr, package #2, blacklist flows with mismatch, and acks minimal', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'test/fixtures/package.2.xml', null, 'test/fixtures/blacklist.flows-mismatch.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the blacklist covers Communties_Update_PeopleSoft_I-6 instead of 
		 * Communties_Update_PeopleSoft_ID-6 (note the last 'D'), the flow is not flagged.
		 */
		t.deepEqual(result, {
			FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('protect mode rr, package with comments, blacklist flows, and acks minimal', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'test/fixtures/package.2.xml', null, 'test/fixtures/blacklist.flows.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * Because the blacklist covers v7 of the flow, and we're in PROTECT_RECENTLY_RELEASED, 
		 *	and the acks don't cover the flow, v6 of the flow is flagged.
		 */
		t.deepEqual(result, {
			Flow: [
				"Communties_Update_PeopleSoft_ID-6"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('protect mode rr, package flows, blacklist flows, and acks minimal', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'test/fixtures/package.flows.xml', null, 'test/fixtures/blacklist.flows.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * All of the flows get flagged, despite version mismatches or versions not being 
		 *	specified in the blacklist.
		 */
		t.deepEqual(result, {
			Flow: [
				"Communties_Update_PeopleSoft_ID-6", 
				"Update_Default_Resume-3", 
				"Update_Talent_Account_for_Document-2", 
				"Update_Talent_Account_for-1_Preferences-16"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('protect mode sc, package #2, blacklist with comments, and acks minimal', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_SIMULTANEOUS, 'test/fixtures/package.2.xml', null, 'test/fixtures/blacklist.comments.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * The flow get flagged, despite elements in the blacklist being commented.
		 */
		t.deepEqual(result, {
			Flow: [
				"Communties_Update_PeopleSoft_ID-6"
			], FlowDefinition: [
				"Communties_Update_PeopleSoft_ID"
			]
		});
		t.end();
	});
});

test.cb('package tests, blacklist non-tests, and acks minimal', function(t) {
	t.plan(2);

	sag.enforceProtections(sag.PROTECT_RECENTLY_RELEASED, 'test/fixtures/package.tests.xml', null, 'test/fixtures/blacklist.tests.xml', 'test/fixtures/acknowledgements.minimal.xml', function(err, result) {
		t.is(err, null);
		/*
		 * The non-test classes being in the blacklist should not cause the test classes in the package to be flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

