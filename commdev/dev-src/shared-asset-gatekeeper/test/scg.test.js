import test from 'ava';

import moment from 'moment';

import scg from '../main/scg';

test('no primary directory arg', async function(t) {
	t.plan(1);

	t.throws(function() {
		scg.enforceProtections();
	}, new RegExp('Path to the primary asset root directory.*must be provided.', 'i'));
});

test('no other directories arg', async function(t) {
	t.plan(1);

	t.throws(function() {
		scg.enforceProtections('primary');
	}, new RegExp('One or more paths to other asset root directories must be provided.', 'i'));
});

test('empty other directories arg', function(t) {
	t.plan(1);

	t.throws(function() {
		scg.enforceProtections('primary', []);
	}, new RegExp('One or more paths to other asset root directories must be provided.', 'i'));
});

test.cb('non-existent primary directory', function(t) {
	t.plan(1);

	scg.enforceProtections('primary-does-not-exist', ['other'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading release config file', 'i'));
		t.end();
	});
});

test.cb('empty primary directory', function(t) {
	t.plan(1);

	scg.enforceProtections('test/fixtures/scg-primary-empty', ['other'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading release config file', 'i'));
		t.end();
	});
});

test.cb('primary target release file empty', function(t) {
	t.plan(2);

	scg.enforceProtections('test/fixtures/scg-primary-release-empty', ['other'], function(err, result) {
		t.is(err, null);
		/*
		 * Because the primary target release config has no date, no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('primary target release file malformed', function(t) {
	t.plan(1);

	scg.enforceProtections('test/fixtures/scg-primary-release-malformed', ['other'], function(err, result) {
		t.regex(err, new RegExp('Error raised parsing release config file', 'i'));
		t.end();
	});
});

test.cb('primary target release file with bad date', function(t) {
	t.plan(1);

	scg.enforceProtections('test/fixtures/scg-primary-release-bad-date', ['other'], function(err, result) {
		t.regex(err, new RegExp('Unparseable target release date found', 'i'));
		t.end();
	});
});

test.cb('primary target release file with past date', function(t) {
	t.plan(1);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-release-past-date', ['other'], function(err, result) {
		t.regex(err, new RegExp('Past target release date found', 'i'));
		t.end();
	});
});

test.cb('non-existent other directory', function(t) {
	t.plan(1);

	scg.enforceProtections('primary-does-not-exist', ['other'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading release config file', 'i'));
		t.end();
	});
});

test.cb('empty other directory', function(t) {
	t.plan(1);

	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other1-empty'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading release config file', 'i'));
		t.end();
	});
});

test.cb('other target release file empty', function(t) {
	t.plan(2);

	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other-release-empty'], function(err, result) {
		t.is(err, null);
		/*
		 * Because the other target release config has no date, no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('other target release file malformed', function(t) {
	t.plan(1);

	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other-release-malformed'], function(err, result) {
		t.regex(err, new RegExp('Error raised parsing release config file', 'i'));
		t.end();
	});
});

test.cb('other target release file with bad date', function(t) {
	t.plan(1);

	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other-release-bad-date'], function(err, result) {
		t.regex(err, new RegExp('Unparseable target release date found', 'i'));
		t.end();
	});
});

test.cb('other target release file with past date', function(t) {
	t.plan(2);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other-release-past-date'], function(err, result) {
		t.is(err, null);
		/*
		 * Other target release config dates that are in the past (outside of the overlap
		 *	window) are simply ignored.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('no overlaps', function(t) {
	t.plan(2);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other-release-no-overlap'], function(err, result) {
		t.is(err, null);
		/*
		 * Because the other target release config dates do not overlap, no elements are flagged.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('both packages non-existent', function(t) {
	t.plan(1);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other-package-missing'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading masklist file.*scg-other-package-missing', 'i'));
		t.end();
	});
});

test.cb('other package non-existent', function(t) {
	t.plan(1);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other-package-missing'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading masklist file.*scg-other-package-missing', 'i'));
		t.end();
	});
});

test.cb('primary package non-existent', function(t) {
	t.plan(1);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-package-missing', ['test/fixtures/scg-other1'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading package file.*scg-primary-package-missing', 'i'));
		t.end();
	});
});

test.cb('primary acks non-existent', function(t) {
	t.plan(1);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-acks-missing', ['test/fixtures/scg-other1'], function(err, result) {
		t.regex(err, new RegExp('Error raised reading acknowledgements file.*scg-primary-acks-missing', 'i'));
		t.end();
	});
});

test.cb('trigger overlap no ack', function(t) {
	t.plan(2);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary1', ['test/fixtures/scg-other1'], function(err, result) {
		t.is(err, null);
		/*
		 * UserTrigger is in both, with no ack, so flag.
		 */
		t.deepEqual(result, {
			"test/fixtures/scg-other1": {
				ApexTrigger: [
					"UserTrigger"
				]
			}
		});
		t.end();
	});
});

test.cb('trigger overlap ack mistyped', function(t) {
	t.plan(2);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary-acks-mistyped', ['test/fixtures/scg-other1'], function(err, result) {
		t.is(err, null);
		/*
		 * UserTrigger has an ack, but NOT using the correct mode element ("shared-changes" instead 
		 *	of "simultaneous-changes", given that we're in PROTECT_SIMULTANEOUS), so the entry 
		 *	remains flagged.
		 */
		t.deepEqual(result, {
			"test/fixtures/scg-other1": {
				ApexTrigger: [
					"UserTrigger"
				]
			}
		});
		t.end();
	});
});

test.cb('trigger overlap ack', function(t) {
	t.plan(2);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	scg.enforceProtections('test/fixtures/scg-primary2', ['test/fixtures/scg-other1'], function(err, result) {
		t.is(err, null);
		/*
		 * UserTrigger has an ack, so no flag.
		 */
		t.is(result, null);
		t.end();
	});
});

test.cb('tc, ats, and crm overlaps #1', function(t) {
	t.plan(2);

	scg.nowMoment = moment('20161203', 'YYYYMMDD');
	/*
	 * Validate against two branches each for ATS and CRM, one for dev and the other for release.
	 */
	scg.enforceProtections('test/fixtures/scg-primary-tc1', [
			'test/fixtures/scg-other-ats1', 
			'test/fixtures/scg-other-ats2', 
			'test/fixtures/scg-other-crm1', 
			'test/fixtures/scg-other-crm2'
		], function(err, result) {
		t.is(err, null);

		/*
		 * The target release config date for ats2 is too far in the future and, for crm1, too 
		 *	far in the past. So, nothing is flagged for those two. The package.xml for ats1 
		 *	contains Flow.Update_Talent_Account_for_Document-10 (commented-out), which causes 
		 *	Flow.Update_Talent_Account_for_Document-9 (also commented-out) to be flagged.
		 */
		t.deepEqual(result, {
			"test/fixtures/scg-other-crm2": {
				ApexClass: [
					"UserTriggerHandler"
				]
			}, 
			"test/fixtures/scg-other-ats1": {
				Flow: [
					"Update_Talent_Account_for_Document-9"
				], 
				FlowDefinition: [
					"Update_Talent_Account_for_Document"
				]
			}
		});
		t.end();
	});
});
