metricData = {};

// 9/28
var reportsuites = {
    "reportsuite": [
        //NON-PROD
        { "domain": "aerotek.com.dev.allegisgroup.com", "reportsuite": "allegisgrpaerotekexternaldev,allegisgrpthingamajobglobalint", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "aerotek.com.int.allegisgroup.com", "reportsuite": "allegisgrpaerotekexternalint,allegisgrpthingamajobglobalint", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "aerotek.com.load.allegisgroup.com", "reportsuite": "allegisgrpaerotekexternalint,allegisgrpthingamajobglobalint", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "emea.aerotek.com.branch.allegisgroup.com", "reportsuite": "allegisgrpaerotekexternalemeabranch,allegisgrpthingamajobglobalemeabranch", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "mlaglobal.com.dev.allegisgroup.com", "reportsuite": "allegisgrpalleigsgrpmlaglobaldev", "siteid": "major lindsay africa", "sitegroup": "major lindsay africa" },
        { "domain": "mlaglobal.com.int.allegisgroup.com", "reportsuite": "allegisgrpalleigsgrpmlaglobalint", "siteid": "major lindsay africa", "sitegroup": "major lindsay africa" },
        { "domain": "mlaglobal.com.load.allegisgroup.com", "reportsuite": "allegisgrpalleigsgrpmlaglobalint", "siteid": "major lindsay africa", "sitegroup": "major lindsay africa" },
        { "domain": "allegisglobalsolutions.com.dev.allegisgroup.com", "reportsuite": "allegisgrpallegisglobalsolutionsint", "siteid": "allegis global solutions", "sitegroup": "allegis global solutions" },
        { "domain": "allegisglobalsolutions.com.int.allegisgroup.com", "reportsuite": "allegisgrpallegisglobalsolutionsint", "siteid": "allegis global solutions", "sitegroup": "allegis global solutions" },
        { "domain": "allegisglobalsolutions.com.load.allegisgroup.com", "reportsuite": "allegisgrpallegisglobalsolutionsint", "siteid": "allegis global solutions", "sitegroup": "allegis global solutions" },
        { "domain": "populusgroup.com.dev.allegisgroup.com", "reportsuite": "allegisgrppopulusprodint", "siteid": "populus group", "sitegroup": "populus group" },
        { "domain": "populusgroup.com.int.allegisgroup.com", "reportsuite": "allegisgrppopulusprodint", "siteid": "populus group", "sitegroup": "populus group" },
        { "domain": "populusgroup.com.load.allegisgroup.com", "reportsuite": "allegisgrppopulusprodint", "siteid": "populus group", "sitegroup": "populus group" },
        { "domain": "allegis-partners.com.dev.allegisgroup.com", "reportsuite": "allegisgrpallegispartnersint,allegisgrpthingamajobglobalint", "siteid": "allegis partners", "sitegroup": "allegis group services" },
        { "domain": "allegis-partners.com.int.allegisgroup.com", "reportsuite": "allegisgrpallegispartnersint,allegisgrpthingamajobglobalint", "siteid": "allegis partners", "sitegroup": "allegis group services" },
        { "domain": "allegis-partners.com.load.allegisgroup.com", "reportsuite": "allegisgrpallegispartnersint,allegisgrpthingamajobglobalint", "siteid": "allegis partners", "sitegroup": "allegis group services" },
        { "domain": "allegisgroup.com.dev.allegisgroup.com", "reportsuite": "allegisgrpallegisgroupexternalint", "siteid": "allegis group corp external", "sitegroup": "allegis group corporate" },
        { "domain": "allegisgroup.com.int.allegisgroup.com", "reportsuite": "allegisgrpallegisgroupexternalint", "siteid": "allegis group corp external", "sitegroup": "allegis group corporate" },
        { "domain": "allegisgroup.com.load.allegisgroup.com", "reportsuite": "allegisgrpallegisgroupexternalint", "siteid": "allegis group corp external", "sitegroup": "allegis group corporate" },
        { "domain": "astoncarter.com.dev.allegisgroup.com", "reportsuite": "allegisgrpastoncarterint", "siteid": "aston carter external", "sitegroup": "aston carter" },
        { "domain": "astoncarter.com.int.allegisgroup.com", "reportsuite": "allegisgrpastoncarterint", "siteid": "aston carter external", "sitegroup": "aston carter" },
        { "domain": "astoncarter.com.load.allegisgroup.com", "reportsuite": "allegisgrpastoncarterint", "siteid": "aston carter external", "sitegroup": "aston carter" },
        { "domain": "easi.com.dev.allegisgroup.com", "reportsuite": "allegisgrpeasiint", "siteid": "aerotek easi", "sitegroup": "aerotek" },
        { "domain": "easi.com.int.allegisgroup.com", "reportsuite": "allegisgrpeasiint", "siteid": "aerotek easi", "sitegroup": "aerotek" },
        { "domain": "easi.com.load.allegisgroup.com", "reportsuite": "allegisgrpeasiint", "siteid": "aerotek easi", "sitegroup": "aerotek" },
        { "domain": "europe.teksystemsxp-aut.allegisgroup.com", "reportsuite": "allegisgrpteksystemseuropedev", "siteid": "Teksystem europe external", "sitegroup": "teksystems" },
        { "domain": "asia.teksystemsxp-aut.allegisgroup.com", "reportsuite": "Allegisgrpteksystemsasiadev", "siteid": "Teksystem asia external", "sitegroup": "teksystems" },

        /* CONNECTED COMMUNITY SITES */
            { "domain": "^comdev-allegisconnected\\.cs.*\\.force\\.com\\/teksystems", "reportsuite": "allegisgrpteksystemsconnectedcomdev", "siteid": "TEKsystems Connected COMDEV", "sitegroup": "teksystems" },
            { "domain": "^comdev-allegisconnected\\.cs.*\\.force\\.com\\/aerotek", "reportsuite": "allegisgrpaerotekconnectedcomdev", "siteid": "Aerotek Connected COMDEV", "sitegroup": "aerotek" },
            { "domain": "^comdev2-connected\\.cs.*\\.force\\.com\\/teksystems", "reportsuite": "allegisgrpteksystemsconnectedcomdev2", "siteid": "TEKsystems Connected COMDEV2", "sitegroup": "teksystems" },
            { "domain": "^comdev2-connected\\.cs.*\\.force\\.com\\/aerotek", "reportsuite": "allegisgrpaerotekconnectedcomdev2", "siteid": "Aerotek Connected COMDEV2", "sitegroup": "aerotek" },
            { "domain": "^comci-allegisconnected\\.cs.*\\.force\\.com\\/teksystems", "reportsuite": "allegisgrpteksystemsconnectedcomci", "siteid": "TEKsystems Connected COMCI", "sitegroup": "teksystems" },
            { "domain": "^comci-allegisconnected\\.cs.*\\.force\\.com\\/aerotek", "reportsuite": "allegisgrpaerotekconnectedcomci", "siteid": "Aerotek Connected COMCI", "sitegroup": "aerotek" },

            { "domain": "^dev-allegisconnected\\.cs.*\\.force\\.com\\/teksystems", "reportsuite": "allegisgrpteksystemsconnecteddev", "siteid": "TEKsystems Connected Dev", "sitegroup": "teksystems" },
            { "domain": "^dev-allegisconnected\\.cs.*\\.force\\.com\\/aerotek", "reportsuite": "allegisgrpaerotekconnecteddev", "siteid": "Aerotek Connected Dev", "sitegroup": "aerotek" },
            { "domain": "^test-allegisconnected\\.cs.*\\.force\\.com\\/teksystems", "reportsuite": "allegisgrpteksystemsconnectedtest", "siteid": "TEKsystems Connected Test", "sitegroup": "teksystems" },
            { "domain": "^test-allegisconnected\\.cs.*\\.force\\.com\\/aerotek", "reportsuite": "allegisgrpaerotekconnectedtest", "siteid": "Aerotek Connected Test", "sitegroup": "aerotek" },
            { "domain": "^load-allegisconnected\\.cs.*\\.force\\.com\\/teksystems", "reportsuite": "allegisgrpteksystemsconnectedload", "siteid": "TEKsystems Connected Load", "sitegroup": "teksystems" },
            { "domain": "^load-allegisconnected\\.cs.*\\.force\\.com\\/aerotek", "reportsuite": "allegisgrpaerotekconnectedload", "siteid": "Aerotek Connected Load", "sitegroup": "aerotek" },

        /* BOND NON-PROD SITES */
          { "domain": "rws11.apac.allegisgroup.com/aerotek", "reportsuite": "allegisgrpaerotekexternalint", "siteid": "aerotek external", "sitegroup": "aerotek" },
          { "domain": "rws11.apac.allegisgroup.com/astoncarter", "reportsuite": "allegisgrpastoncarterint", "siteid": "aston carter external", "sitegroup": "aston carter" },
        { "domain": "rws11.apac.allegisgroup.com/teksystems", "reportsuite": "allegisgrpteksystemsexternalint", "siteid": "teksystems external", "sitegroup": "teksystems" },

        { "domain": "aerotekdesktop.gosnaphop.com", "reportsuite": "allegisgrpaerotekexternalint", "siteid": "aerotek external", "sitegroup": "aerotek" },
          { "domain": "astoncarterdesktop.gosnaphop.com", "reportsuite": "allegisgrpastoncarterint", "siteid": "aston carter external", "sitegroup": "aston carter" },
          { "domain": "astoncarterdemoau.gosnaphop.com", "reportsuite": "allegisgrpastoncarterint", "siteid": "aston carter external", "sitegroup": "aston carter" },
        { "domain": "teksystemsdesktop.gosnaphop.com", "reportsuite": "allegisgrpteksystemsexternalint", "siteid": "teksystems external", "sitegroup": "teksystems" },
       
        /* PLEASE ADD NEW NON-PROD DOMAINS ABOVE THIS LINE */
        { "domain": "dev.allegisgroup.com", "reportsuite": "allegisgrpallegisgroupexternalint", "siteid": "allegis group corp external", "sitegroup": "allegis group corporate" },
        { "domain": "int.allegisgroup.com", "reportsuite": "allegisgrpallegisgroupexternalint", "siteid": "allegis group corp external", "sitegroup": "allegis group corporate" },
        { "domain": "load.allegisgroup.com", "reportsuite": "allegisgrpallegisgroupexternalint", "siteid": "allegis group corp external", "sitegroup": "allegis group corporate" },

        //PROD

        /* CONNECTED COMMUNITY SITES */
        { "domain": "connect.aerotek.com", "reportsuite": "allegisgrpaerotekconnected", "siteid": "Aerotek Connected", "sitegroup": "aerotek" },
        { "domain": "connect.teksystems.com", "reportsuite": "allegisgrpteksystemsconnected", "siteid": "TEKsystems Connected", "sitegroup": "teksystems" },

        /* CAREER BUILDER TALENT NETWORK */
        { "domain": "cbtalentnetwork.com", "reportsuite": "allegisgrpcareersitedev", "siteid": "careersite dev", "sitegroup": "all" },

        /* BOND PROD SITES */
        { "domain": "online.apac.allegisgroup.com/aerotek", "reportsuite": "allegisgrpaerotekexternal", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "online.apac.allegisgroup.com/astoncarter", "reportsuite": "allegisgrpastoncarter", "siteid": "aston carter external", "sitegroup": "aston carter" },
        { "domain": "online.apac.allegisgroup.com/teksystems", "reportsuite": "allegisgrpteksystemsexternal", "siteid": "teksystems external", "sitegroup": "teksystems" },

        /* Allegis/TEK Websites */
        { "domain": "aerotek.com", "reportsuite": "allegisgrpaerotekexternal,allegisgrpthingamajobglobal", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "aerotekphoenix.com", "reportsuite": "allegisgrpaerotekexternal,allegisgrpthingamajobglobal", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "aerotekcatseguin.com", "reportsuite": "allegisgrpaerotekexternal,allegisgrpthingamajobglobal", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "keepyourdooropen.com", "reportsuite": "allegisgrpaerotekexternal,allegisgrpthingamajobglobal", "siteid": "aerotek external", "sitegroup": "aerotek" },
        { "domain": "aerotekcareers.com", "reportsuite": "allegisgrpaerotekcareers,allegisgrpthingamajobglobal", "siteid": "aerotek careers", "sitegroup": "aerotek" },
        { "domain": "easi.com", "reportsuite": "allegisgrpeasi,allegisgrpthingamajobglobal", "siteid": "aerotek easi", "sitegroup": "aerotek" },
        { "domain": "aerotekcanada.ca", "reportsuite": "allegisgrpaerotekcanada,allegisgrpthingamajobglobal", "siteid": "aerotek canada", "sitegroup": "aerotek" },
        { "domain": "aerotek.nl", "reportsuite": "allegisgrpaeroteknetherlands,allegisgrpthingamajobglobal", "siteid": "aerotek netherlands", "sitegroup": "aerotek" },
        { "domain": "aerotek.de", "reportsuite": "allegisgrpaerotekgermany,allegisgrpthingamajobglobal", "siteid": "aerotek germany", "sitegroup": "aerotek" },
        { "domain": "aerotek.co.uk", "reportsuite": "allegisgrpallegisgrpaerotekuk,allegisgrpthingamajobglobal", "siteid": "aerotek uk", "sitegroup": "aerotek" },
        { "domain": "findarchitectjobs.com", "reportsuite": "allegisgrparchitectexternal,allegisgrpfindarchtjprod,allegisgrpthingamajobglobal", "siteid": "aerotek find architect", "sitegroup": "aerotek" },
        { "domain": "aerotekcareers.co.uk", "reportsuite": "allegisgrpaerotekcareersuk,allegisgrpthingamajobglobal", "siteid": "aerotek careers uk", "sitegroup": "aerotek" },
        { "domain": "aerotek.allegisgroup.com", "reportsuite": "allegisgrpaerotekinternal1", "siteid": "aerotek internal", "sitegroup": "aerotek" },
        { "domain": "<somefacebookURL>", "reportsuite": "allegisgrpaerotekfacebook,allegisgrpthingamajobglobal", "siteid": "aerotek facebook", "sitegroup": "aerotek" },
        { "domain": "corp.allegisgroup.com", "reportsuite": "allegisgrpallegisgroupintranet", "siteid": "allegis group corp intranet", "sitegroup": "allegis group corporate" },
        { "domain": "allegisgroupservices.com", "reportsuite": "allegisgrpagsexternal", "siteid": "allegis group services external", "sitegroup": "allegis group services" },
        { "domain": "allegisglobalsolutions.com", "reportsuite": "allegisgrpallegisglobalsolutionsexternal", "siteid": "allegis global solutions", "sitegroup": "allegis global solutions" },
        { "domain": "assetmanagementresources.com", "reportsuite": "allegisgrpamrexternal", "siteid": "asset management resources", "sitegroup": "asset management resources" },
        { "domain": "beltwaycapital.com", "reportsuite": "allegisgrpbeltwaycapitalexternal", "siteid": "beltway capital", "sitegroup": "beltway capital" },
        { "domain": "insideedgelegal.com", "reportsuite": "allegisgrpinsideedgelegal", "siteid": "inside edge legal", "sitegroup": "inside edge legal" },
        { "domain": "mlaglobal.com", "reportsuite": "allegisgrpalleigsgrpmlaglobal", "siteid": "major lindsay africa", "sitegroup": "major lindsay africa" },
        { "domain": "marketsource.com", "reportsuite": "allegisgrpmktsource", "siteid": "marketsource external", "sitegroup": "marketsource" },
        { "domain": "partnerlogix.allegisgroup.com", "reportsuite": "allegisgrppartnerlogix", "siteid": "marketsource partner logix", "sitegroup": "marketsource" },
        { "domain": "parkwaycapital.com/", "reportsuite": "allegisgrpparkwaycapital", "siteid": "parkway capital", "sitegroup": "parkway capital" },
        { "domain": "populusgroup.com", "reportsuite": "allegisgrppopulusprod", "siteid": "populus group", "sitegroup": "populus group" },
        { "domain": "qualisysmed.com", "reportsuite": "allegisgrpqualisysexternal", "siteid": "qualisys", "sitegroup": "qualisys" },
        { "domain": "stephenjames.com", "reportsuite": "allegisgrpstephenjamesexternal,allegisgrpthingamajobglobal", "siteid": "stephenjames", "sitegroup": "stephenjames" },
        { "domain": "teksystemscareers.com", "reportsuite": "allegisgrpteksystemscareers,allegisgrpthingamajobglobal", "siteid": "teksystems careers", "sitegroup": "teksystems" },
        { "domain": "europe.teksystems.com", "reportsuite": "allegisgrpteksystemsexternaleurope", "siteid": "Teksystem europe external", "sitegroup": "teksystems" },
        { "domain": "asia.teksystems.com", "reportsuite": "allegisgrpteksystemsexternalasia", "siteid": "Teksystem asia external", "sitegroup": "teksystems" },
        { "domain": "teksystems.com", "reportsuite": "allegisgrpteksystemsexternal,allegisgrpthingamajobglobal", "siteid": "teksystems external", "sitegroup": "teksystems" },
        { "domain": "training.teksystems.com", "reportsuite": "allegisgrpteksystemseducationsvc,allegisgrpthingamajobglobal", "siteid": "teksystems education services", "sitegroup": "teksystems" },
        { "domain": "vsc.teksystems.com/", "reportsuite": "allegisgrpteksystemsvsc,allegisgrpthingamajobglobal", "siteid": "teksystems vsc", "sitegroup": "teksystems" },
        { "domain": "teksystems.ca/", "reportsuite": "allegisgrpteksystemscanada,allegisgrpthingamajobglobal", "siteid": "teksystems canada", "sitegroup": "teksystems" },
        { "domain": "teksystems.nl", "reportsuite": "allegisgrpteksystemsnetherlands,allegisgrpthingamajobglobal", "siteid": "teksystems netherlands", "sitegroup": "teksystems" },
        { "domain": "teksystems.de", "reportsuite": "allegisgrpteksystemsgermany,allegisgrpthingamajobglobal", "siteid": "teksystems germany", "sitegroup": "teksystems" },
        { "domain": "teksystems.allegisgroup.com", "reportsuite": "allegisgrpteksystemsintranet", "siteid": "teksystems intranet", "sitegroup": "teksystems" },
        { "domain": "teksystems.co.uk", "reportsuite": "allegisgrpallegisgrpteksystemsuk,allegisgrpthingamajobglobal", "siteid": "teksystems uk", "sitegroup": "teksystems" },
        { "domain": "teksystemscareers.co.uk", "reportsuite": "teksystemscareersuk,allegisgrpthingamajobglobal", "siteid": "teksystems careers uk", "sitegroup": "teksystems" },
        { "domain": "thingamajob.com", "reportsuite": "allegisgrpthingamajob,allegisgrpthingamajobglobal", "siteid": "thingamajob", "sitegroup": "thingamajob" },
        { "domain": "frontoffice.allegisgroup.com", "reportsuite": "allegisgrpthingamajobfrontoffice,allegisgrpthingamajobglobal", "siteid": "", "sitegroup": "thingamajob" },
        { "domain": "aerotek.com", "reportsuite": "allegisgrpaerotekextjprod,allegisgrpthingamajobglobal", "siteid": "", "sitegroup": "" },
        { "domain": "stephenjames.com", "reportsuite": "allegisgrpsjamesextjprod,allegisgrpthingamajobglobal", "siteid": "", "sitegroup": "" },
        { "domain": "teksystems.com", "reportsuite": "allegisgrptekextjprod,allegisgrpthingamajobglobal", "siteid": "", "sitegroup": "" },
        { "domain": "wexfordequitites.com", "reportsuite": "allegisgrpwexfordequities", "siteid": "wexford equities", "sitegroup": "wexford equities" },
        { "domain": "allegis-partners.com", "reportsuite": "allegisgrpallegispartners,allegisgrpthingamajobglobal", "siteid": "allegis partners", "sitegroup": "allegis group services" },
        { "domain": "astoncarter.com", "reportsuite": "allegisgrpastoncarter", "siteid": "aston carter external", "sitegroup": "aston carter" },
        /* PLEASE ADD NEW PROD DOMAINS ABOVE THIS LINE */

        //Last entry - don't add anything below it
        { "domain": "allegisgroup.com", "reportsuite": "allegisgrpallegisgroupexternal", "siteid": "allegis group corp external", "sitegroup": "allegis group corporate" }
    ],
    "base": [
          { "domain": "", "reportsuite": "", "siteid": "", "sitegroup": "" }
    ]
};

metricData.getReportSuiteInfo = function () {
    var currentDomain = location.hostname.toLowerCase();
    var tjDomain = "thingamajob.com";
    var connectedDomains = ["force.com", "salesforce.com"];
    var bondDomains = ["rws11.apac.allegisgroup.com", "online.apac.allegisgroup.com"];
    var sReferrer = document.referrer.toLowerCase();
    var bFound = 0;
    var pathName = location.pathname != null && location.pathname != '' ? location.pathname.toLowerCase() : '';

    //Logic for Connected community sites
    var bFoundConnectedDomain = false;
    for (d = 0; !bFoundConnectedDomain && d < connectedDomains.length; d++) {
        if (currentDomain.indexOf(connectedDomains[d]) >= 0) {
            if (/^\/aerotek/i.test(pathName)) {
                currentDomain += '/aerotek';
                bFoundConnectedDomain = true;
            }
            else if (/^\/teksystems/i.test(pathName)) {
                currentDomain += '/teksystems';
                bFoundConnectedDomain = true;
            }
        }
    }

    //Logic for bond sites
    var bFoundBondDomain = false;
    for (b = 0; !bFoundBondDomain && b < bondDomains.length; b++) {
        if (currentDomain.indexOf(bondDomains[b]) >= 0) {
            if (/^\/aerotek/i.test(pathName)){
                currentDomain += '/aerotek';
                bFoundBondDomain = true;
            }
            else if (/^\/teksystems/i.test(pathName)) {
                currentDomain += '/teksystems';
                bFoundBondDomain = true;
            }
            else if (/^\/astoncarter/i.test(pathName)) {
                currentDomain += '/astoncarter';
                bFoundBondDomain = true;
            }
        }
    }

    //Set the rs, siteid, and sitegroup values for the page
    for (i = 0; i < reportsuites.reportsuite.length; i++) {
        //Set reportsuiteid, siteid, and group id by domain name
        var regex = new RegExp(reportsuites.reportsuite[i].domain)
        if (regex.test(currentDomain)) {
          metricData.reportSuiteId = reportsuites.reportsuite[i].reportsuite;
         metricData.site_id = reportsuites.reportsuite[i].siteid;
         metricData.site_group = reportsuites.reportsuite[i].sitegroup;
         bFound = 1;
         break;
      }
    }

    //The domain wasn't found.
    if (!bFound) {
        metricData.reportSuiteId = "No Value";
        metricData.site_id = "No Value";
        metricData.site_group = "No Value";
    }

    //If TJ, then set prop33 to the sitegroup of the referring domain
    if (currentDomain.indexOf(tjDomain)) {
        for (i = 0; i < reportsuites.reportsuite.length; i++) {
            //Is the referrer in the list of domains above and is the referrer NOT TJ
            if ((sReferrer.indexOf(reportsuites.reportsuite[i].domain) >= 0) && (sReferrer.indexOf(tjDomain) == -1)) {
                metricData.site_group = reportsuites.reportsuite[i].sitegroup;
                metricData.setTJCookie("refSG", reportsuites.reportsuite[i].sitegroup);
                break;
            }
        }
        //Has the cookie already been set?
        if (metricData.getCookie("refSG")) {
            metricData.site_group = metricData.getCookie("refSG");
        }
    }

    return metricData;
}



metricData.setTJCookie = function (name, value) {
    path = "/"
    expires = "";
    domain = ".thingamajob.com";
    secure = "";

    var expires_date = new Date();

    document.cookie = name + "=" + escape(value) +
        ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
        ((path) ? ";path=" + path : "") +
        ((domain) ? ";domain=" + domain : "") +
        ((secure) ? ";secure" : "");
}

metricData.getCookie = function (c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}