import test from 'ava';

import reportSuites from '../main/reportsuites';

test('getReportSuiteInfo comdev teksystems', async function(t) {
	t.plan(3);

	location = {
		hostname: 'comdev-allegisconnected.cs83.force.com', 
		pathname: '/teksystems/tc_talent_gateway_change_password'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpteksystemsconnectedcomdev');
	t.is(result.site_id, 'TEKsystems Connected COMDEV');
	t.is(result.site_group, 'teksystems');
});

test('getReportSuiteInfo comdev aerotek', async function(t) {
	t.plan(3);

	location = {
		hostname: 'comdev-allegisconnected.cs83.force.com', 
		pathname: '/aerotek/tc_talent_gateway_change_password'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekconnectedcomdev');
	t.is(result.site_id, 'Aerotek Connected COMDEV');
	t.is(result.site_group, 'aerotek');
});

test('getReportSuiteInfo comci teksystems', async function(t) {
	t.plan(3);

	location = {
		hostname: 'comci-allegisconnected.cs88.force.com', 
		pathname: '/teksystems/tc_jobs'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpteksystemsconnectedcomci');
	t.is(result.site_id, 'TEKsystems Connected COMCI');
	t.is(result.site_group, 'teksystems');
});

test('getReportSuiteInfo comci aerotek', async function(t) {
	t.plan(3);

	location = {
		hostname: 'comci-allegisconnected.cs88.force.com', 
		pathname: '/aerotek/tc_jobs'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekconnectedcomci');
	t.is(result.site_id, 'Aerotek Connected COMCI');
	t.is(result.site_group, 'aerotek');
});

test('getReportSuiteInfo dev teksystems', async function(t) {
	t.plan(3);

	location = {
		hostname: 'dev-allegisconnected.cs88.force.com', 
		pathname: '/teksystems/tc_login_aerotek?retURL=tc_login_teksystems'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpteksystemsconnecteddev');
	t.is(result.site_id, 'TEKsystems Connected Dev');
	t.is(result.site_group, 'teksystems');
});

test('getReportSuiteInfo dev aerotek', async function(t) {
	t.plan(3);

	location = {
		hostname: 'dev-allegisconnected.cs88.force.com', 
		pathname: '/aerotek/tc_login_aerotek?retURL=tc_login_aerotek'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekconnecteddev');
	t.is(result.site_id, 'Aerotek Connected Dev');
	t.is(result.site_group, 'aerotek');
});

test('getReportSuiteInfo test teksystems', async function(t) {
	t.plan(3);

	location = {
		hostname: 'test-allegisconnected.cs88.force.com', 
		pathname: '/teksystems/tc_dashboard'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpteksystemsconnectedtest');
	t.is(result.site_id, 'TEKsystems Connected Test');
	t.is(result.site_group, 'teksystems');
});

test('getReportSuiteInfo test aerotek', async function(t) {
	t.plan(3);

	location = {
		hostname: 'test-allegisconnected.cs88.force.com', 
		pathname: '/aerotek/tc_dashboard'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekconnectedtest');
	t.is(result.site_id, 'Aerotek Connected Test');
	t.is(result.site_group, 'aerotek');
});

test('getReportSuiteInfo load teksystems', async function(t) {
	t.plan(3);

	location = {
		hostname: 'load-allegisconnected.cs80.force.com', 
		pathname: '/teksystems/tc_forgot_password_teksystems'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpteksystemsconnectedload');
	t.is(result.site_id, 'TEKsystems Connected Load');
	t.is(result.site_group, 'teksystems');
});

test('getReportSuiteInfo load aerotek', async function(t) {
	t.plan(3);

	location = {
		hostname: 'load-allegisconnected.cs80.force.com', 
		pathname: '/aerotek/tc_forgot_password_aerotek'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekconnectedload');
	t.is(result.site_id, 'Aerotek Connected Load');
	t.is(result.site_group, 'aerotek');
});

test('getReportSuiteInfo prod teksystems', async function(t) {
	t.plan(3);

	location = {
		hostname: 'connect.teksystems.com', 
		pathname: '/tc_login_teksystems?retURL=tc_login_teksystems'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpteksystemsconnected');
	t.is(result.site_id, 'TEKsystems Connected');
	t.is(result.site_group, 'teksystems');
});

test('getReportSuiteInfo prod aerotek', async function(t) {
	t.plan(3);

	location = {
		hostname: 'connect.aerotek.com', 
		pathname: '/tc_login_aerotek?retURL=tc_login_aerotek'
	}

	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekconnected');
	t.is(result.site_id, 'Aerotek Connected');
	t.is(result.site_group, 'aerotek');
});

test('getReportSuiteInfo no match env', async function(t) {
	t.plan(3);

	location = {
		hostname: 'unknown-allegisconnected.cs83.force.com', 
		pathname: '/aerotek/tc_talent_gateway_change_password'
	}

	var result = metricData.getReportSuiteInfo();
	// No values returned for unmatched URL pattern.
	t.is(result.reportSuiteId, 'No Value', 'No report suite ID should be returned when the env portion of the URL is unmatched.');
	t.is(result.site_id, 'No Value', 'No site ID should be returned when the env portion of the URL is unmatched.');
	t.is(result.site_group, 'No Value', 'No site group should be returned when the env portion of the URL is unmatched.');
});

test('getReportSuiteInfo no match account', async function(t) {
	t.plan(3);

	location = {
		hostname: 'comdev-allegisfoo.cs83.force.com', 
		pathname: '/aerotek/tc_talent_gateway_change_password'
	}

	var result = metricData.getReportSuiteInfo();
	// No values returned for unmatched URL pattern.
	t.is(result.reportSuiteId, 'No Value', 'No report suite ID should be returned when the account portion of the URL is unmatched.');
	t.is(result.site_id, 'No Value', 'No site ID should be returned when the account portion of the URL is unmatched.');
	t.is(result.site_group, 'No Value', 'No site group should be returned when the account portion of the URL is unmatched.');
});

test('getReportSuiteInfo no match domain', async function(t) {
	t.plan(3);

	location = {
		hostname: 'test-allegisconnected.cs83.enforce.com', 
		pathname: '/aerotek/tc_talent_gateway_change_password'
	}

	var result = metricData.getReportSuiteInfo();
	// No values returned for unmatched URL pattern.
	t.is(result.reportSuiteId, 'No Value', 'No report suite ID should be returned when the domain portion of the URL is unmatched.');
	t.is(result.site_id, 'No Value', 'No site ID should be returned when the domain portion of the URL is unmatched.');
	t.is(result.site_group, 'No Value', 'No site group should be returned when the domain portion of the URL is unmatched.');
});

test('getReportSuiteInfo no match tld', async function(t) {
	t.plan(3);

	location = {
		hostname: 'load-allegisconnected.cs83.force.org', 
		pathname: '/aerotek/tc_talent_gateway_change_password'
	}

	var result = metricData.getReportSuiteInfo();
	// No values returned for unmatched URL pattern.
	t.is(result.reportSuiteId, 'No Value', 'No report suite ID should be returned when the tld portion of the URL is unmatched.');
	t.is(result.site_id, 'No Value', 'No site ID should be returned when the tld portion of the URL is unmatched.');
	t.is(result.site_group, 'No Value', 'No site group should be returned when the tld portion of the URL is unmatched.');
});

test('getReportSuiteInfo non-connected', async function(t) {
	t.plan(15);

	location = {
		hostname: 'aerotek.com.dev.allegisgroup.com', 
		pathname: '/index.htm'
	}
	var result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekexternaldev,allegisgrpthingamajobglobalint');
	t.is(result.site_id, 'aerotek external');
	t.is(result.site_group, 'aerotek');

	location = {
		hostname: 'allegisglobalsolutions.com.int.allegisgroup.com', 
		pathname: '/index.htm'
	}
	result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpallegisglobalsolutionsint');
	t.is(result.site_id, 'allegis global solutions');
	t.is(result.site_group, 'allegis global solutions');

	location = {
		hostname: 'aerotek.com', 
		pathname: '/index.htm'
	}
	result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpaerotekexternal,allegisgrpthingamajobglobal');
	t.is(result.site_id, 'aerotek external');
	t.is(result.site_group, 'aerotek');

	location = {
		hostname: 'allegisgroupservices.com', 
		pathname: '/index.htm'
	}
	result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpagsexternal');
	t.is(result.site_id, 'allegis group services external');
	t.is(result.site_group, 'allegis group services');

	location = {
		hostname: 'teksystems.com', 
		pathname: '/index.htm'
	}
	result = metricData.getReportSuiteInfo();
	// Values as hard-coded in reportsuites.js
	t.is(result.reportSuiteId, 'allegisgrpteksystemsexternal,allegisgrpthingamajobglobal');
	t.is(result.site_id, 'teksystems external');
	t.is(result.site_group, 'teksystems');
});

