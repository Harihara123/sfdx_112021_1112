/** 
 * Controller that provides context branding data, which is assumed to be aligned 
 * by OpCo (Operating Company, e.g., TEKsystems, Aerotek, etc.).
 *
 * @author mscepani
 */
public class TC_BrandingController {

	@TestVisible
	private TC_BrandingService brandingService;

	/**
	 * The ID of the context user.
	 */
	public String contextUserId {get;set;}

	/**
	 * The profile ID of the context user.
	 */
	public String contextProfileId {get;set;}

	/**
	 * The URL (string) for the context page/request.
	 */
	public String contextUrlStr {get;set;}

	private String brandingKey;
	
	public TC_BrandingController() {
		this.brandingService = new TC_BrandingService();
	}

	/**
	 * Accessor for a key that uniquely and tersely identifies the branding. Assumes that either 
	 * 	the contextUserId or contextUrlStr field have been set.
	 */
	public String getBrandingKey() {
        if (this.brandingKey == null) {
            this.brandingKey = retrieveBrandingKey();
        }

        return this.brandingKey;
	}

	private String retrieveBrandingKey() {
		String result = null;
		if (this.contextUrlStr == null) {
			/*
			 * Returns the base URL of the current site that references and links should use. Includes the path prefix. 
			 * 	I believe this can be seen in the Salesforce config as Setup -> Build -> Develop -> Sites as the value of 
			 *	the "Site URL". For example: 
			 *		http://comci-allegisconnected.cs87.force.com/aerotek
			 *	In PROD, I would think that this value would be something akin to "https://connect.teksystems.com", but 
			 *	it may actually be more akin to the pre-PROD environments. Regardless, either should work here.
			 */
			this.contextUrlStr = Site.getBaseUrl();

			// Hack to mock a result from Site.getBaseUrl() when executing as a unit test.
			if (System.Test.isRunningTest()) {
				this.contextUrlStr = 'https://test-allegisconnected.cs81.force.com/aerotek';
			}

			System.debug(System.LoggingLevel.INFO, 'this.contextUrlStr = ' + this.contextUrlStr);
		}

		if (this.contextProfileId == null && this.contextUserId != null) {
			result = this.brandingService.calcBrandingKey(this.contextUserId, this.contextUrlStr);
		} else if (this.contextProfileId != null) {
			result = this.brandingService.calcBrandingKey(this.contextProfileId, this.contextUrlStr);
		} else {
			result = this.brandingService.calcBrandingKey(null, this.contextUrlStr);
		}
		
		return result;
	}

}