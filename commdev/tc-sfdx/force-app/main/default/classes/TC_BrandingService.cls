/**
 * Service that provides context branding data, which is assumed to be aligned 
 * by OpCo (Operating Company, e.g., TEKsystems, Aerotek, etc.).
 *
 * @author mscepani
 */
public class TC_BrandingService {

	/**
	 * Allow the branding-key to be specified via a URL query parameter.
	 */
	public static final String BRANDING_KEY_PARAM = 'branding-key';

	/**
	 * Calculate and return a keyable string identifying the context branding, primarily based 
	 * on the given user, secondarily based on the given URL/host.
	 * 
	 * @param userOrProfileId - ID of the user (or the ID of their profile) for which the applicable 
	 * 	branding should be "calculated". Either may be passed, depending on how "specific" a result 
	 *	is desired.
	 * @param url - URL object instance for the context page/request, which may or may not apply 
	 *	(as this may be invoked from a VisualForce email template, for example).
	 */
	public String calcBrandingKey(String userOrProfileId, String urlStr) {
		String result = null;

		result = calcBrandingKeyFromUserProfile(userOrProfileId);

		if (String.isEmpty(result)) {
			System.debug(System.LoggingLevel.WARN, 'Unable to calc the branding key from the user/profile ID. Trying to calc using the URL.');
			result = calcBrandingKeyFromRequest(urlStr);
		}

		System.debug(System.LoggingLevel.INFO, 'result = ' + result);
		return result;
	}

	/**
	 * Return true if the context environment, as derived using the context Salesforce "Site", is 
	 * production, else false.
	 *
	 * TODO Move into a more appropriate class, such as a new TC_EnvUtils.
	 */
	public Boolean calcIsProdSite() {
		/*
		 * Returns the base URL of the current site that references and links should use. Includes the path prefix. 
		 * 	I believe this can be seen in the Salesforce config as Setup -> Build -> Develop -> Sites as the value of 
		 *	the "Site URL". For example: 
		 *		http://comci-allegisconnected.cs87.force.com/aerotek
		 *	In PROD, I would think that this value would be something akin to "https://connect.teksystems.com", but 
		 *	it may actually be more akin to the pre-PROD environments. Regardless, either should work here.
		 */
		String siteUrlStr = Site.getBaseUrl();

		// Hack to mock a result from Site.getBaseUrl() when executing as a unit test.
		if (System.Test.isRunningTest()) {
			siteUrlStr = 'https://connect.aerotek.com';
		}

		URL siteUrl = new Url(siteUrlStr);
		String siteUrlHost = siteUrl.getHost();

		Boolean isProd = this.calcIsProd(siteUrlHost);
		return isProd;
	}

	/**
	 * Return true if the context environment, as communicated by the given URL host, is production, 
	 * else false.
	 *
	 * @param urlHost - For example, https://connect.teksystems.com
	 *
	 * TODO Move into a more appropriate class, such as a new TC_EnvUtils.
	 */
	public Boolean calcIsProd(String urlHost) {
		// Play it safe and assume PROD.
		Boolean isProd = true;

		// For example, https://com073016-allegisconnected.cs83.force.com/teksystems/LoginT...
		if (urlHost != null) {
			if (urlHost.endsWithIgnoreCase('.force.com') || urlHost.endsWithIgnoreCase('.salesforce.com')) {
				isProd = false;
			}
		}
		return isProd;
	}

	private String calcBrandingKeyFromUserProfile(String userOrProfileId) {
		String result = null;

        if (userOrProfileId != null) {
            /*
             * TC_Opco_Mappings__c is a hierarchical (user/profile-aware) custom setting.
             */
            TC_Opco_Mappings__c customSetting = TC_Opco_Mappings__c.getInstance(userOrProfileId);
            if (customSetting != null) {
                String profileSlug = customSetting.Profile_Slug__c;
                if (!String.isEmpty(profileSlug)) {
                    result = profileSlug;
                }
            }
        }

		return result;
	}

	private String calcBrandingKeyFromRequest(String urlStr) {
		String result = null;

		if (!String.isEmpty(urlStr)) {
			Url url = null;
			try {
				url = new Url(urlStr);
			} catch (StringException ex) {
				System.debug(System.LoggingLevel.ERROR, 'Unable to parse the given string [' + urlStr + '] as a URL.');
				System.debug(System.LoggingLevel.ERROR, ex.getStackTraceString());
			}

			if (url != null) {
				String brandingKey = calcBrandingKeyFromUrl(url);

				if (!String.isEmpty(brandingKey)) {
					result = brandingKey;
				} else {
					System.debug(System.LoggingLevel.WARN, 'Unable to calculate a branding key for URL ' + urlStr);
				}
			}
		}

		return result;
	}

	/**
	 * Calculate the context OpCo, represented by an identifying slug. Current known values: 
	 *	- teksystems
	 *	- aerotek
	 */
	private String calcBrandingKeyFromUrl(Url url) {
		String opCoSlug = null;

		String urlHost = url.getHost();
		Boolean isProd = calcIsProd(urlHost);
		System.debug(System.LoggingLevel.INFO, 'isProd = ' + isProd);
		if (isProd) {
			// For example, https://connect.teksystems.com
			List<String> hostTokensArr = urlHost.split('\\.');
			// Pluck out the token preceding the TLD.
			opCoSlug = hostTokensArr[hostTokensArr.size() - 2];
		} else {
			// For example, url https://com073016-allegisconnected.cs83.force.com/teksystems/LoginT...
			// For example, path /teksystems/tc_login_teksystems
			String urlPath = url.getPath();
			List<String> pathTokensArr = urlPath.split('/');
			// Be prepared to handle paths both with and without a leading slash.
			opCoSlug = pathTokensArr[0];
			if (opCoSlug == null || opCoSlug.length() < 1) {
				opCoSlug = pathTokensArr[1];
			}
		}
		return opCoSlug;
	}

}