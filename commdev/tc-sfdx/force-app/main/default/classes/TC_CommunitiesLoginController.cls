/**
* An apex page controller that exposes the site login functionality
*/
global with sharing class TC_CommunitiesLoginController {
  
    @TestVisible
    private SiteWrapper siteWrapper;

    public String username{get;set{username = value == null ? value : value.trim();}}
    public String password{get;set;}
    public UserLogin userLogHistory {get;set;}
    public User loggedinUser{get;set;}
    public boolean inactiveuser{get;set;}
    public boolean userlocked{get;set;}
    public String userProfileName{get;set;}
    
    public TC_CommunitiesLoginController() {
        this.siteWrapper = new SiteWrapper();
        this.inactiveuser = false;
    }
      public String profileSlug {
        get {
            return TC_Utils.getProfileSlug();
        } set;
    }
     
    public pageReference forwardToStartPage() {
        try {
            inactiveuser=false;
            if((username == null || username =='' || password == null || password == '')){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.Error,'Your login attempt has failed.Make sure the username and password are correct.');
                ApexPages.addMessage(msg);
            } else {
                loggedInUser = [select Id,Username,IsActive, Social_Media_Policy_Accepted__c, First_Time_Login__c, Profile_Name__c from user where username = :username+ '.talent' LIMIT 1];
               
                //Set profile from user for pre-auth custom settings
                if (loggedInUser.Profile_Name__c != '') {
                    userProfileName = loggedInUser.Profile_Name__c.toLowerCase();
                }

                if(loggedInUser.IsActive == false){
                    inactiveuser=true;
                    // ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.Error,'Inactiveuser' + inactiveuser);
                    //ApexPages.addMessage(msg);
                }else{
                    UserLoghistory =  [SELECT Id,IsFrozen,IsPasswordLocked,LastModifiedById,LastModifiedDate,UserId FROM UserLogin WHERE UserId =: loggedInUser.Id];
                    if(UserLoghistory.IsPasswordLocked == true){
                        userlocked=UserLoghistory.IsPasswordLocked;
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.Error,'Your account is temporarily locked due to too many unsuccessful login attempts. Try again in 30 minutes.');
                        ApexPages.addMessage(msg);
                    } else {
                        String postAuthLandingPageStr = TC_Utils.calcPostAuthLandingPage(loggedInUser);
                        return this.siteWrapper.login(username + '.talent',password, postAuthLandingPageStr);
                    }
                }
            }
        } catch(Exception e) {
            system.debug('EXCEPTION'+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Invalid Username'));
        }
        return null;
    }
    
}