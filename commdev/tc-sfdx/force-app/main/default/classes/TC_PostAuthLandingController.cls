/**
 * This controller's corresponding VF page (tc_post_auth_landing.page) is configured as the 
 *  "Active Site Home Page" for each OpCo-specific talent community (Site). This is for the sole 
 *  purpose of enabling the "login as customer/talent user" functionality. For talent users, that 
 *  out-of-the-box Salesforce functionality uses the value of the "Active Site Home Page" field as 
 *  the post-login destination page/path. If that field were set to tc_login, the user would end 
 *  up on the login page, even though they had actually been logged-in. (They would be able to get 
 *  to tc_dashboard if they entered that into the URL.)
 *
 * So, to minimize confusion for internal users, we configure tc_post_auth_landing.page as the 
 *  "Active Site Home Page". However, there is a drawback to this. If an unauthenticated talent 
 *  user attempts to access a post-auth page or a page that does not exist, that user will be 
 *  directed to the value of "Active Site Home Page". Without doing a proper check for whether or 
 *  not they are logged-in, they would get directed to tc_profile_wizard. Security would prevent 
 *  them from accessing that page, and then they'd get redirected (server-side) to the login page. 
 *  However, the resultant URL would read tc_profile_wizard, which is awkward and misleading.
 *
 * So, to minimize that confusion, here, we first do a check for whether or not the user is 
 *  logged-in. If not, they get taken to the login page (tc_login).
 *
 * The login and change password flows do _not_ make use of this controller (and corresponding 
 *  page), as it is unnecessary and wasteful (redirects and a repetitive query against User).
 */
public class TC_PostAuthLandingController{

    public TC_PostAuthLandingController(){
    }

    public PageReference calcLandingPage() {
        // Assume the user is not logged-in, which would make their default page the login page.
        PageReference result = new PageReference('/tc_login');

        String currentUserTypeStr = userinfo.getuserType();
        /* 
         * If the user type is not "Guest", they are logged-into the Community. As such, send 
         *  them to the correct post-auth landing page. This is for the "login as customer/talent 
         *  user" scenario.
         */
        if (currentUserTypeStr != 'Guest') {
            User loggedInUser = [select Id,Username, First_Time_Login__c, TC_Mobile_Download_Ack__c from user where ID = :userinfo.getUserId() LIMIT 1];
            String postAuthLandingPageStr = TC_Utils.calcPostAuthLandingPage(loggedInUser);
            result = new PageReference(postAuthLandingPageStr);
        }
        return result;
    }
  
}