public with sharing class TC_TemplateController {

    public TC_TemplateController() {

    }

    public String UserType { get {
      return [select UserType from user where Id = :userinfo.getUserId()][0].UserType;
    } set; }  

    public PageReference getRedirtoFilenotfound() {
      String currentUserType = userinfo.getuserType();
      // If the user type is not "Guest", they are logged-into the Community.
      if (currentUserType != 'Guest') {
        /*
         * The request for tc_file_not_found is already in-flight. Allow it to complete. This assumes that 
         * tc_file_not_found.page is the only VF page making use of the getRedirtoFilenotfound action.
         */
        return null;
      } else {
        /*
         * If the user type is "Guest", they haven't yet logged into the Community. As such, just send them to
         * the login screen.
         */
        PageReference pageRef = new PageReference('/tc_login');
        return pageRef;
      }
    }
    
    /**
     * Calculate and return a short identifier for the profile of the context user. For example, "teksystems" 
     * or "aerotek". Works for both logged-in and non-logged-in users. For a non-logged-in user, the profile 
     * used will be of the context community guest user. For example, "TEKsystems Talent Community Site Guest User".
     */
    public String profileSlug {
        get {
            return TC_Utils.getProfileSlug();
        } set;
    }

    /**
     * Return the path of the CSS file that is appropriate for the profile of the context user. The path is 
     * relative to the root of the tc_style static resource bundle. Works for both logged-in and non-logged-in 
     * users.
     */
    public String getProfileCssPath() {
        String result = TC_Utils.getProfileCssPath();
        return result;
    }

}