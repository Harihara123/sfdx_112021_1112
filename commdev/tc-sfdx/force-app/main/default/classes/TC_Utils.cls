public with sharing class TC_Utils {
/**
	 * This method will return the profile slug of the running user. This value
	 * is stored in a Custom Setting named TC_Opco_Mappings__c
	 * @return String - profile slug; used to load opco specific skuid pages / stylesheets
	 */
	public static String getProfileSlug() {
		
		// Get running users profile id
		Id profileId = UserInfo.getProfileId();
		
		TC_Opco_Mappings__c customSetting = TC_Opco_Mappings__c.getInstance(profileId);
		String profileSlug = customSetting.Profile_Slug__c;

		return profileSlug;
	}

	public static String getProfileCssPath(){
		if(TC_Utils.getProfileSlug() != null){
			return 'build/styles/' + TC_Utils.getProfileSlug() + '-styles.css';	
		} else {
			// Default stylesheet
			return 'build/styles/teksystems-styles.css';
		}
	}
	public static Date assignmentEndDate;
	public static void calculateAssignmentEndDate(Id accountId) {
	    String calculatedEndDate = '';
	    List<Talent_Work_History__c> workHistories = [Select Start_Date__c, End_Date__c from Talent_Work_History__c 
	        where Talent__c = :accountId];
	    Date today =  Date.today();
	    
	    Boolean firstIteration = true; 
	    for (Talent_Work_History__c twh: workHistories) {
	        if ((twh.Start_Date__c <= today) && (twh.End_Date__c >= today)) {
	            if (firstIteration) {
	                assignmentEndDate = twh.End_Date__c;
	                firstIteration = false;
	            } else if (assignmentEndDate < twh.End_Date__c) {
	                assignmentEndDate  = twh.End_Date__c;
	            }                
	        }
	    }
	}
	public static String getAssignmentEndDate(Id accountId) {
	    calculateAssignmentEndDate(accountId);
	    if (assignmentEndDate != null) {
	        return assignmentEndDate.format();
	    } else {
	        return 'No date on file';
	    }
	}

	/**
	 * Return the path for the appropriate page to which the given user should be redirected after 
	 *	a successful login, which might be via a password change.
	 */
    public static String calcPostAuthLandingPage(User loggedInUser) {
        Boolean First_Time_Login = loggedInUser.First_Time_Login__c;
        if (First_Time_Login == true) {
            return '/tc_profile_wizard';
        } else {
            return '/tc_dashboard';
        }
    }
    /**
     * Returns consolidated message from list of of errors
     */
    public static String generateErrorMessage(List<Database.Error> errors) {
        List<Log__c> logs =new List<Log__c>();
        String errorMessage = '';
        for (Database.Error error : errors) {
            errorMessage += error.getMessage(); 
        }        
        return errorMessage;
    }
    
    /**
     * Calculated governor limits for the current transaction
     */
    public static String getAvailableLimit() {
        Decimal cpuLimit = Limits.getLimitCpuTime();
        Integer usedCpu = Limits.getCpuTime();
        Decimal cpuPct = (cpuLimit - usedCpu)*100;
        
        Decimal heapLimit = Limits.getLimitHeapSize();
        Integer usedHeap = Limits.getHeapSize();
        Decimal heapPct = (heapLimit - usedHeap)*100;        
        
        Decimal queryLimit = Limits.getLimitQueryRows();
        Integer usedQuery = Limits.getQueryRows();
        Decimal queryPct = (queryLimit - usedQuery)*100;
        
        Decimal dmlLimit = Limits.getLimitDMLRows();
        Integer usedDML = Limits.getDMLRows();
        Decimal dmlPct = (dmlLimit - usedDML)*100;
        
        Integer dmlRows = Limits.getLimitDMLRows() - Limits.getDMLRows();
        return ('Cpu Availability=' + cpuPct.divide(cpuLimit,2) + '%, Heap Availability=' + heapPct.divide(heapLimit,2) + 
                '%, Query Availability=' + queryPct.divide(queryLimit, 2) + '%, DML Availability=' + dmlPct.divide(dmlLimit, 2) + '%');
    }
}