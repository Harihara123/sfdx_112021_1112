<apex:page docType="html-5.0" showHeader="false" sidebar="false" applyBodyTag="false" applyHtmlTag="false" standardStylesheets="false" controller="TC_CommunitiesLoginController">
    <apex:composition template="tc_template_first">
        <apex:stylesheet value="{!URLFOR($Resource.tc_3rdparty_font_awesome, 'css/font-awesome.min.css')}" />
        <apex:define name="titleContent">
            <c:TC_OutputForBranding brandingKeyToEvalIn="aerotek">Aerotek Login Page</c:TC_OutputForBranding>
            <c:TC_OutputForBranding brandingKeyToEvalIn="teksystems">TEKsystems Login Page</c:TC_OutputForBranding>
            <c:TC_OutputForBranding brandingKeyToEvalIn="astoncarter">Aston Carter Login Page</c:TC_OutputForBranding>
        </apex:define>

        <apex:define name="head">
            <script type="text/javascript">
                var userProfileName = '{!userProfileName}';
            </script>
        </apex:define>

        <apex:define name="pageSlugDeclaration">
            var pageSlug = 'login';
        </apex:define>
<apex:define name="javaScriptRuntimePopulation">
<apex:variable var="msg" value="TC_login_disabled_{!ProfileSlug}"/>
    sfdcContext.label.map['{!msg}'] = '{!$Label[msg]}';
</apex:define>
        <apex:define name="body">
            <div class="container">
                <div class="row">
                    <!-- Left -->
                    <div class="col-md-6">
                        <div class="u-login-container">

                            <div style="flex: 1;">
                                <div style="text-align: center">
                                 
                                    <apex:variable var="logo" value="tc_login_styles/{!ProfileSlug}_logo.png"/>
                                    <apex:image value="{!URLFOR($Resource.tc_login_styles, logo)}" />   
                                  
                                </div>

                                <div style="flex: 1;">
                                    <div class="col-sm-12 col-lg-8 col-lg-push-2">

                                        <dl class="u-accordion visible-sm">
                                                <dt>
                                                    <apex:variable var="topMessage" value="TC_Success_Powered_by_{!ProfileSlug}"/>
                                                    <span class="u-chevron right"></span> {!$Label[topMessage]}
                                                </dt>
                                                <dd>
                                                <apex:variable var="bulletLabel1" value="TC_Login_Bullet_1_{!ProfileSlug}"/>
                                                <apex:variable var="bulletLabel2" value="TC_Login_Bullet_2_{!ProfileSlug}"/>
                                                <apex:variable var="bulletLabel3" value="TC_Login_Bullet_3_{!ProfileSlug}"/>
                                                    <p class="u-lead u-margin-bottom-10">{!$Label[bulletLabel1]}</p>
                                                    <p class="u-lead u-margin-bottom-10">{!$Label[bulletLabel2]}</p>
                                                    <p class="u-lead u-margin-bottom-10">{!$Label[bulletLabel3]}</p>
                                                    <c:TC_OutputForBranding brandingKeyToEvalIn="teksystems">
                                                        <p class="u-lead u-margin-bottom-10">{!$Label.TC_Login_Bullet_4_teksystems}</p>
                                                    </c:TC_OutputForBranding>
                                                </dd>
                                        </dl>

                                        <div class="c-card">
                                            <apex:form styleClass="c-login-form">
                                                <div class="c-custom-sf-message">
                                                    <apex:messages styleClass="errorMsg qa-login-messages" style="color:red"/>
                                                    <br />
                                                </div>
                                                <div class="c-form-group" >
                                                    <label for="username">Username:</label>
                                                    <apex:inputtext value="{!username}" styleclass="c-form-group__text-input c-form-group__text-input--pronounced c-form-group__text-input--username username qa-username-input" id="username" html-placeholder="Email"/>
                                                </div>
                                                <div class="c-form-group">
                                                    <label for="password">Password:</label>
                                                    <apex:inputsecret value="{!password}" id="password" styleclass="c-form-group__text-input c-form-group__text-input--pronounced c-form-group__text-input--password password qa-password-input" html-placeholder="Password"/>
                                                </div>
                                                <div class="c-form-group">
                                                    <p>
                                                        <input type="checkbox" name="remember" id="remember" class="rememberme qa-remember-me-input" />&nbsp;
                                                        Remember me
                                                    </p>
                                                </div>

                                                <div class="c-form-group">
                                                    <p><apex:commandbutton styleClass="u-btn u-btn--block qa-login-button" value="LOGIN" action="{!forwardToStartPage}" id="login"/></p>
                                                </div>
                                                <div class="c-form-group">
                                                    <p><a href="{!$Site.Prefix}/tc_forgot_password" class="qa-forgot-password-link">Forgot your password?</a></p>
                                                </div>
												<div class="flags" style="display:none;">
                                                    <apex:inputCheckbox selected="{!inactiveuser}" styleclass="js-inactive-user-flag" />
                                                </div>
                                            </apex:form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Right -->
             <div class="u-halfscreen-container u-halfscreen-container--text u-halfscreen-container--right hidden-xs hidden-sm">
                <div class="u-halfscreen-container__text-container">
                        <apex:variable var="topMessage" value="TC_Success_Powered_by_{!ProfileSlug}"/>
                        <h2 class="u-header--lg u-header--margin">{!$Label[topMessage]}</h2>
                        <apex:variable var="bulletLabel1" value="TC_Login_Bullet_1_{!ProfileSlug}"/>
                        <apex:variable var="bulletLabel2" value="TC_Login_Bullet_2_{!ProfileSlug}"/>
                        <apex:variable var="bulletLabel3" value="TC_Login_Bullet_3_{!ProfileSlug}"/>                          
                        <ul class="u-bullet-list">
                            <li><p class="u-lead">{!$Label[bulletLabel1]}</p></li>
                            <li><p class="u-lead">{!$Label[bulletLabel2]}</p></li>
                            <li><p class="u-lead">{!$Label[bulletLabel3]}</p></li>
                            <c:TC_OutputForBranding brandingKeyToEvalIn="teksystems">
                                <li><p class="u-lead">{!$Label.TC_Login_Bullet_4_teksystems}</p></li>
                            </c:TC_OutputForBranding>
                        </ul>
                </div>
            </div>
        </apex:define>

    </apex:composition>
</apex:page>