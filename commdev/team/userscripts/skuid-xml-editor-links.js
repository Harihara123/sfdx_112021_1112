// ==UserScript==
// @name         Skuid XML editor links
// @namespace    http://allegisgroup.com/
// @version      0.1
// @description  Add direct links to the XML editor for Skuid pages (from the "all pages" page).
// @author       Michael Harry Scepaniak
// @include      https://*.force.com/apex/skuid__PageList
// @include      https://*.force.com/apex/PageList
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    /**
     * Add direct links to the XML editor for Skuid pages (from the "all pages" page).
     */
    var _addXmlLinks = function() {
        var pagesTable = document.getElementsByClassName('nx-list-contents');

        // Wait for the table of Skuid pages to populate.
        if (!pagesTable || pagesTable.length === 0) {
            setTimeout(function(){
                _addXmlLinks();
            }, 200);
        } else {
            var pagesTableRowArr = pagesTable[0].getElementsByTagName('tr');
            var limit = pagesTableRowArr.length;
            // Loop through each row of the table.
            for (var i = 0; i < limit; i++) {
                // Get all of the text-containing cells in the row.
                var pagesTableRowCellArr = pagesTableRowArr[i].getElementsByClassName('nx-fieldtext');
                // Get the first text-containing column in the row, which should be the link to the Skuid page.
                var pageLinkBox = pagesTableRowCellArr[0];
                // Get the link for the page. For example, 'https://allegisgroup--comdev2--skuid.cs20.visual.force.com/a1Em0000000m7qMEAQ'
                var pageLinkHref = pageLinkBox.getElementsByTagName('a')[0].href;
                // Parse out the ID for the Skuid page.
                var idx = pageLinkHref.lastIndexOf('/');
                var pageId = pageLinkHref.substring(idx + 1);
                // Build the direct link to the XML editor for the Skuid page.
                var xmlHref = '/apex/skuid__PageXMLEdit?id=' + pageId;
                // Build and insert a link to the XML editor for the Skuid page.
                var newLink = document.createElement('a');
                newLink.href = xmlHref;
                var newLinkText = document.createTextNode('[xml]');
                newLink.appendChild(newLinkText);
                pageLinkBox.appendChild(document.createTextNode('  '));
                pageLinkBox.appendChild(newLink);
            }
        }
    };

    _addXmlLinks();

})();