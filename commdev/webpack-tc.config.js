'use strict';

var path = require('path');

var webpack = require('webpack');
var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

/**
 * Webpack configuration for Communities.
 */
module.exports = {
    entry: {
        // app-first is for the "first" pages - login, change password, forgot password, etc.
        "app-first": "./app-src-com/script/app/app-first.js", 
        // app is for everything post-login.
        "app": "./app-src-com/script/app/app.js"
    },

    /*
     * Resolve accurate filenames. More info - http://stackoverflow.com/questions/25553868/current-file-path-in-webpack#25565222
     */
    context: __dirname,
    node: {
        __filename: true
    }, 

    output: {
        path: path.resolve(__dirname, 'app-src-com/script/built/app-com'), 
        filename: '[name].js', 
        // Handles the creation of each non-entry chunk, the result of our code-splitting. Search the codebase for require.ensure.
        chunkFilename: "[name].js"
    },
    //devtool: 'inline-source-map', 
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".js"]
    },
    externals: {
        /*
         * Wherever jquery is required/referenced, resolve the dependency from the global jQuery, which we are assuming 
         * is provided by Skuid. On non-Skuid pages, jQuery should be explicitly required using the expose loader: 
         *  require('expose?$!expose?jQuery!jquery');
         * The expose loader works despite this externals config. For example, see app-first.js.
         */ 
        "jquery": "jQuery"
    }, 
    plugins: [
        new webpack.optimize.UglifyJsPlugin(), 
        new LodashModuleReplacementPlugin({
            // Available feature sets: https://github.com/lodash/lodash-webpack-plugin#feature-sets
        })
    ],
    module: { 
        preLoaders: [ 
            /*
             * Exclude needed to avoid the following build-time error: 
             * Error: Cannot find module '...Front-Office\node_modules\source-map-loader\index.js!...Front-Office\bootstrap.config.js'
             */
            //{ test: /\.js$/, exclude: /bootstrap.config/, loader: "source-map-loader" }
        ], loaders: [ 
            { test: /\.ts$/, loader: "ts-loader" }, 
            { test: /\.css$/, loader: "style-loader!css-loader" }, 

            // Provide access to the jQuery object for each bootstrap js file required by bootstrap-webpack.
            { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
            // Needed for the css-loader when bootstrap-webpack loads bootstrap's css.
            { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&minetype=application/font-woff" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=application/octet-stream" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=image/svg+xml" }, 
            
            { test: /\.hbs$/, loader: "handlebars-loader",
                // Config options: https://github.com/pcardune/handlebars-loader#details
                query: { 
                    //debug: true, 
                    helperDirs: __dirname + "/app-src-com/script/app/templates/helpers", 
                    /*
                     * Work-around for Handlebars no-conflict.js debugger issue. Mor info: 
                     * https://github.com/wycats/handlebars.js/issues/1134
                     */
                    runtime: "handlebars/dist/handlebars.runtime.js"
                } 
            }
        ]
    }
}
