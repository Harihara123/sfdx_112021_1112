'use strict';

var util = require('util');

var _remove = require('lodash/remove');
var baseWebpackConfig = require('./webpack-tc.config');

/**
 * Webpack configuration meant for usage by unit tests driven by mocha-webpack.
 */

// mocha-webpack does not work well with uglified code.
console.log('Removing UglifyJsPlugin from the list of Webpack plugins.');
_remove(baseWebpackConfig.plugins, function(ele) {
	var inspectOutput = util.inspect(ele);
	return inspectOutput.startsWith('UglifyJsPlugin');
});

/*
 * Compile for usage in a node.js-like environment. This allows us to use the fs 
 *	module to read in fixture files from unit tests (when desired).
 */
baseWebpackConfig.target = "node";

module.exports = baseWebpackConfig;
