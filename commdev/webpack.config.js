var webpack = require('webpack');

module.exports = {
    entry: "./app-src/app/app.ts",
    output: {
        filename: "./built/app/ats_bundle.js"
    },
    //devtool: 'source-map',
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".js"]
    },
    // plugins: [
    //     new webpack.optimize.UglifyJsPlugin()
    // ],
    module: {
        loaders: [{
            test: /\.ts$/,
            loader: "ts-loader"
        }]
    }
}
