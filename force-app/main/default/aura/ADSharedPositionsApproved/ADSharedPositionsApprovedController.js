({
    
    doInit : function(component, event, helper) {
        helper.verifyQueueMember(component, event);
    },
    
    confirmApproval : function(component, event, helper) {
        if(component.get( "v.is_editable" ) ){
            helper.approvePositions(component, event);   
        }else{
            helper.showToast("Only queue members are authorized to approve.", "error", "Error");
        }
        
    },
    
    
})