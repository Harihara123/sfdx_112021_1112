({
    
    verifyQueueMember : function(component, event ) {
		
        var action = component.get('c.isQueueMember');
		action.setParams({
            "recordId" : component.get("v.recordId")
        });

        action.setCallback(this, $A.getCallback( function (response) {
            
            if( response.getState() === "SUCCESS") {
                component.set('v.is_editable', response.getReturnValue() );
            }
           
        }));
        
        $A.enqueueAction(action);
	},
    
    
    approvePositions : function(component, event ) {
		
        var action = component.get('c.updateApprovedPositions');
		action.setParams({
            "approvedPositions": component.get("v.ADPositionsApproved"),
            "recordId" : component.get("v.recordId")
        });

        action.setCallback(this, $A.getCallback( function (response) {
            
            if( response.getState() === "SUCCESS") {
                this.showToast(component.get("v.ADPositionsApproved") +" Positions Approved Successfully..", "success", "Success");
                $A.get('e.force:refreshView').fire();
            }else{
                this.showToast("Approval failed, Please contact Administrator.", "error", "Error");
            }
        }));
        
        $A.enqueueAction(action);
	},
    
    
	showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },
})