({  
    resetFileUploadComponent : function(component, event, helper) {
        var uniqId = component.get("v.uniqueId");
       // console.log ("Reset File Upload Event Fired ---- " + uniqId);
        //var fileInput = component.find(uniqId).getElement();
        var fileInput = document.getElementById("f-" + uniqId);
        fileInput.value = ''; 
        component.set("v.fileDetails", "");
        component.set("v.validationMsg", '');
    },
    
    handleFilesChange : function(component, event, helper) {
        var uniqId = component.get("v.uniqueId");
        // var fileInput = component.find("file-upload-input-01").getElement();
        //var fileInput = component.find(uniqId).getElement();
        var fileInput = document.getElementById("f-" + uniqId);
    	var file = fileInput.files[0];
        //Set the name of the file in the invoking component on selection
        var ucEvent = component.getEvent("fileSelected");
        if (file) {
            console.log("Handle filechange controller: " + file);
            component.set("v.fileDetails", file.name + ", " + helper.upsizeUnit(file.size));
            ucEvent.setParams({ "fileName" : file.name});
        }else{
            component.set("v.fileDetails", "No File Selected");
        }   
        ucEvent.fire();
        
        component.set("v.validationMsg", '');
    },
    
    save : function(component, event, helper) {        
        var params = event.getParam('arguments');
        if (params){
            var Id = params.objectId;
            component.set("v.parentId", Id);
            helper.save(component);
        }
        else {
            //throw error
        }            
    },
})