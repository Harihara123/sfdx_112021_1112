({
    // This is called on component initialization. It parses the URL for a parameter
    // "jobId" which is the opportunityId of the job posting.
    doInit: function(component, event) {
        var urlRegex = /\?jobId=(.*)&?/g;
        var sourceUrl = window.location.href;
        var matches = urlRegex.exec(sourceUrl);
        if (matches != null) {
            component.set("v.jobId", matches[1]);
        }
        
        component.set("v.source", document.referrer);
        
    },
    
    // This hides the spinner when the component receives a fileUploadComplete event. This event is fired
    // by the file upload component when the file upload is complete.
    HideSpinner: function(component, event) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-hide');
        var action2 = component.get("c.populateTD");
        action2.setParams({"cId":  component.get("v.recordId")});        
        action2.setCallback(this, function(response2) {
            console.log(response2);
            
        });
        $A.enqueueAction(action2);
        var address = '/thankyou/';
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :true
        });
        urlEvent.fire();
    },
    
    checkFilename: function(component, event) {
        
        if(typeof event.getParam('fileName') != 'undefined'){
            component.set("v.filename", event.getParam('fileName'));
            
        }else{
             component.set("v.filename", 'none');
        }
    },
    
    rerenderApply: function(component, event) {
        var agree = component.find("agreeId").get("v.value");
        if(agree){
            component.find("applyId").set("v.disabled", false); 
        }else{
            component.find("applyId").set("v.disabled", true); 
        }
    },
    
    
    // When the user clicks the apply button...
    //     * Create a case and get the caseId
    //     * transfer the selected file and attach it to caseId
    applyForJob: function(component, event, helper) {
        console.log("------- APPLY for JOB CALLED ------------");
        // Show the spinner (gets hidden by fileUploadComplete event)
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-hide');
        
        helper.validateForm(component);
        
    }
})