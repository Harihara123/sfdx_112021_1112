({
    validateForm: function(component) {
        // Simplistic error checking
        var validCase = true;
         var spinner = component.find('spinner');
        
        // FirstName must not be blank
        var fnameField = component.find("fnameId");
        var fname = fnameField.get("v.value");
        if ($A.util.isEmpty(fname)){
            validCase = false;
            fnameField.set("v.errors", [{message:"First Name cannot be blank."}]);
        }
        else {
            if ( fname.length > 35 ){
                validCase = false;
                fnameField.set("v.errors", [{message:"First Name cannot be more than 35 characters."}]);
            }
            else {
                fnameField.set("v.errors", null);
            }
        }
        
        // LastName must not be blank
        var lnameField = component.find("lnameId");
        var lname = lnameField.get("v.value");
        if ($A.util.isEmpty(lname)){
            validCase = false;
            lnameField.set("v.errors", [{message:"Last Name cannot be blank."}]);
        }
        else {
            if ( lname.length > 35 ){
                validCase = false;
                lnameField.set("v.errors", [{message:"Last Name cannot be more than 35 characters."}]);
            }
            else {
                lnameField.set("v.errors", null);
            }
        }
        
        // Amount must be set, must be a positive number
        var phoneField = component.find("phoneId");
        var phone = phoneField.get("v.value");
        
        var emailField = component.find("emailId");
        var email = emailField.get("v.value");
        
        if ($A.util.isEmpty(phone) && $A.util.isEmpty(email)  ){
            validCase = false;
            phoneField.set("v.errors", [{message:"Enter an email address or a phone number."}]);
            emailField.set("v.errors", [{message:"Enter an email address or a phone number."}]);
        }
        else {
            emailField.set("v.errors", null);
            phoneField.set("v.errors", null);
                
            if ($A.util.isEmpty(phone) === false){
                
                if(isNaN(phone)){
                    validCase = false;
                    phoneField.set("v.errors", [{message:"Enter a valid phone number. Only numbers are allowed."}]); 
                    
                }else if(phone.length < 10 || phone.length > 15 ){
                    validCase = false;
                    phoneField.set("v.errors", [{message:"Enter a valid phone number. Phone numbers can be between 10 to 15 digits."}]); 
                    
                }//else{
                   // phoneField.set("v.errors", null);
                //}
            }
            
            var re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if ($A.util.isEmpty(email) === false){
                if(re.test(email) === false){
                    validCase = false;
                    emailField.set("v.errors", [{message:"Enter a valid email address."}]); 
                }
                //else{
                  //  emailField.set("v.errors", null);
                //}
            }
            
            
        }
        
        
        var agreeField = component.find("agreeId");
        var agree = agreeField.get("v.value");
        if(!agree){
            validCase = false;
            agreeField.set("v.errors", [{message:"please Check the checkbox."}]);
        }else{
            agreeField.set("v.errors", null);  
        }
        
        var filename = component.get("v.filename");
        if(filename == "none"){
            validCase = false; 
            component.set("v.fileerror",true);
            component.set("v.fileerrormsg","Please Upload a file.");
            $A.util.addClass(spinner, 'slds-hide');
            
        }else{
            component.set("v.fileerror",false);
                
            var action = component.get("c.validateFileType"); 
                action.setParams({"namefile": filename});
                action.setCallback(this, function(responsefile) {
                    
                    if (responsefile.getState() === "SUCCESS") {
                        var validFileType = responsefile.getReturnValue();
                        if (!validFileType) {
                            validCase = false;
                            component.set("v.fileerror",true);
                            component.set("v.fileerrormsg", "Please select a valid file type.");
                            $A.util.addClass(spinner, 'slds-hide');
                        }else{
                                component.set("v.fileerror",false); 
                                
                                // Create the case 
                                if(validCase){
                                    var actioncase = component.get("c.createJobApplicationCase");        
                                    actioncase.setParams({"fn":  component.get("v.firstName"),
                                                          "ln": component.get("v.lastName"),
                                                          "ph": component.get("v.phone"),
                                                          "email": component.get("v.email"),
                                                          "jobId": component.get("v.jobId"),
                                                          "source": component.get("v.source"),
                                                          "optin":component.get("v.optIn")});
                                    
                                    console.log("-------------- Parameters:" + actioncase.getParams());
                                    actioncase.setCallback(this, function(response) {
                                        var caseId = response.getReturnValue();
                                        // If case successfully created, then upload the file and attach it to the case.
                                        if (response.getState() === "SUCCESS") {
                                            component.set("v.recordId", caseId);
                                            var uploader = component.find("fileUploader");
                                            uploader.uploadFile(caseId);
                                            $A.util.addClass(spinner, 'slds-hide');
                                        }
                                        
                                        
                                    });
                                    
                                    $A.enqueueAction(actioncase); 
                                    
                                }
                        }
                        
                    }else{
                        $A.util.addClass(spinner, 'slds-hide');
                        validCase = false;
                        component.set("v.fileerror",true);
                        component.set("v.fileerrormsg", "Error while uploading the file.");
                    }
                    
                })
                
                $A.enqueueAction(action);
                 
        }
        
        
        return(validCase);
    }
})