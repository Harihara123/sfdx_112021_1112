({
	doInit : function(component, event, helper){
		var recId = component.get("v.recordId");
        var action = component.get("c.getAccountCurrentsJSON");
        component.set('v.talentcolumns', [
           		{label: 'Division', fieldName: 'division', type: 'text'},
                {label: 'Owner', fieldName: 'ownerId', type: 'url', typeAttributes: {label: { fieldName: 'ownerName' }, target: '_blank'}},
                {label: 'Hiring Manager', fieldName: 'hiringmanagerId', type: 'url', typeAttributes: {label: { fieldName: 'hiringmanager' }, target: '_blank'}},
                {label: 'Talent Name', fieldName: 'candidateId', type: 'url', typeAttributes: {label: { fieldName: 'candidatename' }, target: '_blank'}},
            	{label: 'Job Title', fieldName: 'opportunityId', type: 'url', typeAttributes: {label: {fieldName: 'jobtitle' }, target: '_blank'}},
                {label: 'Start Date', fieldName: 'startdate', type: 'text '},
                {label: 'End Date', fieldName: 'enddate', type: 'text '},
                {label: 'Bill Rate', fieldName: 'billrate', type: 'text '}
            ]);
        action.setParams({"acctId":  recId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log('===='+dataObj);
                var res; 
                var talentList = [];
                var talentFormerList = [];
                var talentPlacedList = [];
                var currentTalents = [];
                var formerTalents = [];
                var placedTalents = [];
                res = JSON.parse(dataObj);
                currentTalents = res.currentTalents;
                formerTalents = res.formerTalents;
                placedTalents = res.placedTalents;
                if(currentTalents !== null && currentTalents.length > 0){
                     for (var i = 0; i < currentTalents.length; i++){
                         talentList.push(currentTalents[i]);
                     }
                     talentList.forEach(function(talent){
                          talent.ownerId = '/'+talent.ownerId;
                          talent.opportunityId = '/'+talent.opportunityId;
                          talent.hiringmanagerId = '/'+talent.hiringmanagerId;
                          talent.candidateId = '/'+talent.candidateId;
                     });
                    component.set("v.displayedCurrents","showcurrent");
                    component.set("v.currents",talentList);
                }
                if(formerTalents !== null && formerTalents.length > 0){
                    for (var i = 0; i < formerTalents.length; i++){
                         talentFormerList.push(formerTalents[i]);
                     }
                     talentFormerList.forEach(function(talent){
                          talent.ownerId = '/'+talent.ownerId;
                          talent.opportunityId = '/'+talent.opportunityId;
                          talent.hiringmanagerId = '/'+talent.hiringmanagerId;
                          talent.candidateId = '/'+talent.candidateId;
                     });
                    component.set("v.displayedFormers","showformer");
                    component.set("v.formers",talentFormerList);
                }
                
                if(placedTalents !== null && placedTalents.length > 0){
                    for (var i = 0; i < placedTalents.length; i++){
                        talentPlacedList.push(placedTalents[i]);
                    }
                    talentPlacedList.forEach(function(talent){
                        talent.ownerId = '/'+talent.ownerId;
                        talent.opportunityId = '/'+talent.opportunityId;
                        talent.hiringmanagerId = '/'+talent.hiringmanagerId;
                        talent.candidateId = '/'+talent.candidateId;
                    });
                    component.set("v.displayedPlaced","showPlaced");
                    component.set("v.placedCandidates",talentPlacedList);
                }
               component.set("v.IsSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    
    viewDetails : function(component, event, helper) {
        var accId = component.get("v.recordId");
        if(accId.startsWith('003')){
            var hiringCurrent = $A.get("{!$Label.c.CRM_Report_HiringManager_Current}");
        	var urllink = '/lightning/r/Report/'+hiringCurrent+'/view?t=1479844235107&fv1='+accId;
        	window.open(urllink, '_blank');
        }
        else{
            var accountCurrent = $A.get("{!$Label.c.CRM_Report_Account_Current}");
        	var urllink = '/lightning/r/Report/'+accountCurrent+'/view?t=1479844235107&fv1='+accId;
        	window.open(urllink, '_blank');            
        }
	},
    viewFormerDetails : function(component, event, helper) {
		var accId = component.get("v.recordId");
        if(accId.startsWith('003')){
            var hiringFormer = $A.get("{!$Label.c.CRM_Report_HiringManager_Former}");
        	var urllink = '/lightning/r/Report/'+hiringFormer+'/view?t=1479844235107&fv1='+accId;
        	window.open(urllink, '_blank');
        }        
        else{
            var accountFormer = $A.get("{!$Label.c.CRM_Report_Account_Former}");
        	var urllink = '/lightning/r/Report/'+accountFormer+'/view?t=1479844235107&fv1='+accId;
        	window.open(urllink, '_blank');            
        }

	},
    viewPlacedDetails : function(component, event, helper) {
        var accId = component.get("v.recordId");
        if(accId.startsWith('003')){
            var hiringPlaced = $A.get("{!$Label.c.CRM_Report_HiringManager_Placed}");
            var urllink = '/lightning/r/Report/'+hiringPlaced+'/view?t=1479844235107&fv0='+accId;
            window.open(urllink, '_blank');
        }        
        else{
            var accountPlaced = $A.get("{!$Label.c.CRM_Report_Account_Placed}");
            var urllink = '/lightning/r/Report/'+accountPlaced+'/view?t=1479844235107&fv0='+accId;
            window.open(urllink, '_blank');            
        }
    }
})