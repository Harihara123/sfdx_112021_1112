({
    drawNewHighChart : function(component){
        var action = component.get("c.getHeader");
        action.setParams({
            "accountId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var self = this;
            var val = response.getReturnValue();
            console.log(state);
            if(state === 'SUCCESS') { 
               var resp = JSON.parse(val);
                console.log(resp);
                // Start Zone
               var series = [];
               var gasfinal = [];
               var ser = [];
               var sec = [];
               var fin = [];
               
               var gas;
               var ops;
               var spread;
               var test;
                if(resp.length > 0){
                    gas = resp[0].GlobalAccountSpread;
                    ops = resp[0].OpCoSpread; // Aerotek
                    spread = gas - ops; //Tek
                    test = 'pass';
                }else{
                    gas = null;
                    ops = null;
                    test = 'fail';
                }
                console.log(gas + ''+ops + ''+test);
               if(gas === null && ops === null){
                    fin = [];
               }else{
                    ser.push("TEK",ops);
                    sec.push("Aerotek",spread);
                    fin.push(ser,sec);
               }
               
               var defaultVal = '';
                
                if(gas === null && ops === null){
                    defaultVal = null;
                }else if(ops > spread){
                    defaultVal = '<span style="font-size: 24px;text-align:center;">'+'$'+ops.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</span>'+ '<br/>'+'<span style="font-size: 10px;text-align:center;">TEK</span>'; 
                }else{
                    defaultVal = '<span style="font-size: 24px;text-align:center;">'+'$'+spread.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</span>'+ '<br/>'+'<span style="font-size: 10px;text-align:center;">Aerotek</span>';
                }
                        
        var doTitle = defaultVal;
        console.log(fin);
        var cmpTarget = component.find("hychart").getElement();
        var chart = new Highcharts.chart(cmpTarget,{
                            chart: {
                                type: 'pie',
                                height: 430,
                                width: 281,
                                borderRadius: 0,
                                marginBottom: 140, //-200
                                //marginTop: 02,
                                marginRight: 10
                            },
                             title: {
                                text: doTitle,
                                align: 'center',
                                verticalAlign: 'middle',
                                floating: true,
                                 y: -20,
                                style: {
                                            color: '#4572A7',
                                            fontSize: '24px'
                                        }  
                            },
                            tooltip: {
                                       formatter: function() {
                                           if(this.y === ops){
                                               return  'TEK' +'<br/>'+'$' + this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                                           }else{
                                               return  'Aerotek' +'<br/>'+'$' + this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                                           }
                                       }
                                   },
                            credits: {
                                       enabled: false
                            },
                            exporting: { enabled: false }, 
                            yAxis: {
                                title: {
                                    text: 'Total Spread'
                                }
                            },
                            plotOptions: {
                               pie: {
                                        startAngle: 90,
                                        innerSize: '74%',
                                        size: '100%',
                                        dataLabels: false,
                                        stickyTracking: false,
                                        states: {
                                            hover: {
                                                enabled: true
                                            }
                                   },
                                    
                point: {
                    events: {
                        mouseOver: function () {
                            chart.setTitle({text: ''});
                            var stext = '<span style="color:'+this.color+';">';
                            this.series.chart.innerText.attr({
                                text: stext + '</span>'+ '<br/>' +'<span style="font-size: 24px;color:'+this.color+';">'+'$'+fin[this.x][1].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</span>'+ '<br/>'+ '<span style="font-size: 10px;color:'+this.color+';">'+fin[this.x][0]+'</span>'
                            });
                        },
                        mouseOut: function () {
                            this.series.chart.innerText.attr({
                                text: defaultVal
                            });
                        },
                         legendItemClick: function() {
                            return false;
                        }
                    }
                }
            }
                            },
                            legend: {
                                        layout: 'vertical',
                                        align: 'center',
                                        symbolHeight: 12,
                                        symbolRadius: 2,
                                        verticalAlign: 'bottom',
                                        symbolPadding: 10,
                                        padding: 40,
                                        itemDistance: -24,
                                        itemMarginTop: 6,//no
                                        itemMarginBottom: 6,//20
                                        x: 0,
                                        y: 0
                                    },
                            lang: {
                                     noData: "No Data to Display"
                                  },
                            noData: {
                                style: {
                                    fontSize: '12px'
                                }
                            },
                             series: [{
										name: 'Spread',
										data: fin,
										size: '92%',
										innerSize: '70%',
										showInLegend:true,
										dataLabels: {
											enabled: false
										},
                                        color: '#4f81bd'
                                     }]
                           },
     function (chart) { // on complete
        var xpos = '50%';
        var ypos = '53%';
        var circleradius = 102;
        var boundingBox;
        var series = chart.series[0];
         
        // Render the text 
         chart.innerText = chart.renderer.text('      ', 0, 0).add();
         boundingBox = chart.innerText.getBBox();
        
        chart.innerText.css({
            width: circleradius * 2,
            fontSize: '30px',
            color: '#4f81bd',
            textAlign: 'center'
        }).attr({
            // why doesn't zIndex get the text in front of the chart?
            x: series.center[0]/1.45 - boundingBox.width / 2 + chart.plotLeft / 2,
            y: series.center[1]/1.3  + 1.5 * boundingBox.height + 4 * chart.plotTop,
            zIndex: 999
        }).add();    
    });
               //End If
            } else {
                alert('Error');
                console.log(response.getError());
            }

             
  });
        $A.enqueueAction(action);  
        //End
  }
})