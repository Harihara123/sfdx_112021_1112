({
    recordtypeInit : function(component, event, helper) {
        helper.getOfficeDetails(component, event, helper);
        var action = component.get("c.getOpportunityRecord");
        var recrdId = component.get("v.recordId");
        action.setParams({"recordId":  recrdId});
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
               var model = data.getReturnValue();
               console.log("ererere");
               console.log(model.RecordType.Name);
               component.set('v.recordtypeName',model.RecordType.Name); 
               if(component.get("v.recordtypeName") == 'Req' ){
                   if(component.get("v.companyName") != 'AG_EMEA' && component.get("v.companyName") != 'TEKsystems, Inc.') {
                       helper.notAeroTekEasi(component, event, helper);
                   }
                   		 
              } 
                else if (component.get("v.recordtypeName") != 'Aerotek EASi') {
                     helper.notAeroTekEasi(component, event, helper);
                } 
            }
          });
       $A.enqueueAction(action); 
    },
    
    loadOptions: function (component, event, helper) {
       	
        var opts = [
            { value: "Edit", label: "Read/Write" },
            { value: "Read", label: "Read Only" }
        ];
        component.set("v.options", opts);
    },
    createRecord : function (component, event, helper) {
        
        var action = component.get("c.insertOpportunityTeamMember");
        var recrdId = component.get("v.recordId");
        var OpportunityAccess = component.get("v.selectedValue");
        var userRoleName = component.find("TeamMemberRoleID").get("v.value");
        
        if(userRoleName != null && userRoleName != ''){
            
            action.setParams({
                                "recordId":  recrdId,
                                "OpportunityAccessLevel" : OpportunityAccess,
                                "TeamMemberRole" : userRoleName
                             }); 
            
            action.setCallback(this, function(response) {
                var data = response;
                
                if (data.getState() == 'SUCCESS') {
                   var model = data.getReturnValue();
                    if(model.isMemberAlreadyAdded){
                       var msg = 'You are already added to the Opportunity team ';
                        helper.showErrorToast(msg);  
                      }else{
                        var msg = 'You have been successfully added to the Opportunity team';
                        helper.showSuccessToast(msg);
                      }
                        var strtUrl;
                        strtUrl ="/lightning/r/Opportunity/"+model.recId+"/view";
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                           "url": strtUrl,
                           "isredirect":true
                        });
                       urlEvent.fire();
                    
                }
              });
            
           $A.enqueueAction(action);
        }    
    },
    CancelAction : function(component, event, helper) {
		var recrdId = component.get("v.recordId");
        var strtUrl;
        strtUrl ="/lightning/r/Opportunity/"+recrdId+"/view";
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": strtUrl,
            "isredirect":true
        });
        urlEvent.fire();
	}
})