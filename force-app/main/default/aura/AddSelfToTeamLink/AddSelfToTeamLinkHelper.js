({
	notAeroTekEasi: function(component, event, helper) {
        
        // Prepare a new record from template
        
        var action = component.get("c.createOpportunityTeamMember");
        var recrdId = component.get("v.recordId");
        
        action.setParams({"recordId":  recrdId});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
               var model = data.getReturnValue();
                if(model.isMemberAlreadyAdded){
                   var msg = 'You are already added to the Opportunity team ';
                    helper.showErrorToast(msg);  
                  }else{
				    var msg = 'You have been successfully added to the Opportunity team';
                    helper.showSuccessToast(msg);
				  }
                    var strtUrl;
                    strtUrl ="/lightning/r/Opportunity/"+model.recId+"/view";
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                       "url": strtUrl,
                       "isredirect":true
                    });
                   urlEvent.fire();
                
            }
          });
        
       $A.enqueueAction(action);
        
    },
    showErrorToast : function(message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Team Member Status',
            message: message,
            messageTemplate: '',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },
	showSuccessToast : function(message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Team Member Status',
            message: message,
            messageTemplate: '',
            duration:' 5000',
            key: 'info_alt',
            type: 'Success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    getOfficeDetails : function(component, event, helper) {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') { 
                var model = data.getReturnValue();
                component.set("v.companyName",model.CompanyName);
            }else if (data.getState() == 'ERROR'){
                
            }
        });
        $A.enqueueAction(action);
    }
})