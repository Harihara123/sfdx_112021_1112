({
	doInit : function(component, event, helper) {
		helper.initialize(component);
	},
    closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
      var urlre='https://'+window.location.hostname+'/lightning/o/Opportunity/list?filterName=Recent';
       window.location=urlre;
   }
})