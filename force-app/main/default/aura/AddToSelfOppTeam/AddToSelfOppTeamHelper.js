({
	initialize : function(component) {
		    var action=component.get("c.addSelfToOppTeam");
            var oppIds=component.get("v.selectedRecords");
            console.log('Select Opptys '+oppIds);
            action.setParams({"recIdList":  oppIds});
            action.setCallback(this, function(a) {
                var state = a.getState();
                if (state === "SUCCESS") {
                  var model = a.getReturnValue();
                    console.log('value returned'+model.isMultipleSelection);
                    component.set("v.isOpen", true);
                    if(model.isNoSelection == true){
                        component.set("v.statusMessage",'Please select an Opportunity');
                    }else if(model.isMemberAlreadyAdded == true){
                        component.set("v.statusMessage",'You\'re already an Opportunity Team Member');
                    }else if(model.isMultipleSelection == true){
                        component.set("v.statusMessage",'Please select only one Opportunity');
                    }else{
                        component.set("v.statusMessage",'You have been successfully added as an Opportunity Team Member');
                    }
                    
                }else{
                    console.log('state'+state);
                }
                
            });
            $A.enqueueAction(action);
      },
    
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'sticky',
            title: title,
            message: errorMessage,
            duration:' 5000',
            type: 'error'
        });
        toastEvent.fire();
    },
    showSuccessToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Opportunity Team Member Added',
            message: 'Opportunity Team Member Added Successfully',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'sticky'
        });
        toastEvent.fire();
    }
      
})