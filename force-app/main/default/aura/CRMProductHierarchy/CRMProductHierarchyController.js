({
	doInit : function(component, event, helper) {
         
		 helper.initProductHierarchy(component,event,helper,null);	
        component.set("v.isSkillsSave","false");
	},
     onOpcoChange : function(component, event, helper) {
       helper.loadBusinessUnitValues(component,event,'None');
	},
    onBusinessunitChange : function(component, event, helper) {
       helper.EnterpriseReqSegmentModal(component,event);
	},
     onSegmentChange : function(component, event, helper) {
       helper.EnterpriseReqJobcodeModal(component,event,helper,null);
	},
    onJobCodeChange : function(component, event, helper) {
       helper.EnterpriseReqCategoryModal(component,event,helper,null);
	},
    onCategoryChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillModal(component,event,helper,null);
	},
    onMainSkillChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillid(component,event);
	},
    onProductPicklistChange : function(component, event, helper) { 
       //helper.EnterpriseProductName(component,event); 
	},
    events : function(component, event, helper){
    console.log('HANDLER');
	},
    handleEditClick : function(component, event, helper){
        
        helper.EditProductHierarchy(component,event,helper);
        //helper.initProductHierarchy(component,event,helper,null);	
		//component.set("v.isSkillsSave","true");
        /*
        component.set("v.prod.Job_Code__c","");
        component.set("v.prod.Category__c","");
        component.set("v.prod.Skill__c","");
        component.set("v.productHireValMsg","");
       */
    },
    handleCancelClick :function(component, event, helper){
        component.set("v.isSkillsPresent", "true");
        component.set("v.isSkillsSave","false");
        component.set("v.productHireValMsg","");
        
                               
    },
    handleSaveClick : function(component, event, helper){
        console.log('save');     
		helper.saveEnterpriseReq(component, event,helper);   
        var opptyId = component.get("v.recordId");
        console.log('saving updating');
        console.log(opptyId);
    }
})