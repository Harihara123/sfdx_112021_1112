({
   	 EditProductHierarchy : function(component,event,helper){
      	component.set("v.isSkillsSave","true");
        var action = component.get("c.getEnterpriseReqPicklists");
        action.setParams({"recId":  component.get("v.recordId")});
         action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                this.EnterpriseReqSegmentModal(component,event,helper,model);
                component.set("v.req.Req_Division__c",model.division);
                component.set("v.prod.Segment__c",model.prod.Segment__c);
                this.EnterpriseReqJobcodeModal(component,event,helper,model);
                component.set("v.prod.Job_Code__c",model.prod.Job_Code__c);
                this.EnterpriseReqCategoryModal(component,event,helper,model);
                component.set("v.prod.Category__c",model.prod.Category__c);
                this.EnterpriseReqMainSkillModal(component,event,helper,model);
                component.set("v.prod.Skill__c",model.prod.Skill__c);
                component.set("v.req.Product__c",model.productName);
            }else if (data.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }  
        }); 
        
       $A.enqueueAction(action);
      },  
	 initProductHierarchy : function(component,event,helper){
        console.log('InitProdHie');
        var action = component.get("c.getEnterpriseReqPicklists");
        var UrlQuery = window.location.href;
        console.log(UrlQuery);
        //var accntId = component.get("v.recordId");
        var accntId = this.getQueryString('accId',UrlQuery);
        var contId = this.getQueryString('conId',UrlQuery);
        console.log('account last'+accntId);
        console.log('contact id'+contId);
        //component.set("v.isSkillsSave","false");
        var objectName=component.get("v.sObjectName");
        var recordId=component.get("v.recordId"); 
         
         
        if(objectName == 'Contact'){
            contId=recordId;
            component.set("v.isCreatedFromContact",true);
        }
        action.setParams({"recId":  recordId});
       
         action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                
                var opcoListArray = [];
                opcoListArray.push({value:'--None--', key:'--None--'}); 
                var opcoListMap = model.opcoMappings;
                for ( var key in opcoListMap) {
                    opcoListArray.push({value:opcoListMap[key], key:key});
                }
				var productPicklistArray = [];
                    var productPicklistInfo = model.productName; 
                    var productPicklistMap = model.productPicklistMappings;
                    
                    productPicklistArray.push({value:'--None--', key:'--None--'});  
                    for ( var key in productPicklistMap) {
                            productPicklistArray.push({value:productPicklistMap[key], key:key,selected: key ===productPicklistInfo});
                        }
                    component.set("v.productListPicklist", productPicklistArray);
                    
				var opcoValList = opcoListArray;
                    opcoValList.sort(function compare(a,b){
                                  if (a.key > b.key)
                                    return -1;
							      if (a.key < b.key)
                                    return 1;
                                  return 0;
                                });

				 opcoValList.reverse();

                component.set("v.opcoList", opcoValList);
                
                 var opcoDivisionArray=[];
                var opcoDivsionMap=model.opcoDivsionDepdentMappings;
                for(var key in opcoDivsionMap){
                  opcoDivisionArray.push({value:opcoDivsionMap[key], key:key});
                }
                
                component.set("v.opcoDivisionList",opcoDivisionArray);
                
                console.log(model.act);
                component.set("v.account",model.act);
                component.set("v.contact",model.cnt);
                console.log(model.office);
                component.set("v.organization",model.office);
                //component.set("v.isSkillsSave","false");
                component.set("v.newOpportunityId",model.objRecId);
                
               // component.set("v.businessUnitList", businessUnitArray);
               // component.set("v.businessList", busArray);
                
               //component.set("v.req.Req_Total_Positions__c",1);
              // component.set("v.req.Req_Include_Bonus__c",true);
               //component.set("v.req.OpCo__c",model.opco);
              // model.opco='AG_EMEA';
               component.set("v.recordType","Req");
                component.set("v.model",model);
                this.loadBusinessUnitValues(component,event,model.opco);	
               component.set("v.req.Req_Division__c",model.division); 
               component.set("v.opcoReName",model.opco);
               component.set("v.divisionReName",model.division);
              // component.find("segmentId").set("v.value",model.prod.Segment__c); 
               
                this.EnterpriseReqSegmentModal(component,event,helper,model);
                
                //component.set("v.prod.Segment__c",model.prod.Segment__c);
                //component.set("v.prod.Job_Code__c",model.prod.Job_Code__c);
               // component.set("v.prod.Category__c",model.prod.Category__c);
               if(model.prod!=null){
                 component.set("v.prodPicklistVarName",model.productName);
                component.set("v.prodSkillVarName",model.prod.Skill__c);
                component.set("v.segmentReName",model.prod.Segment__c);
                component.set("v.jobcodeReName",model.prod.Job_Code__c);
                component.set("v.categoryReName",model.prod.Category__c);
              
               }
                component.set("v.req.OpCo__c",model.opco);
               component.set("v.req.Req_Division__c",model.division); 
              
            }else if (data.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }  
        }); 
        
       $A.enqueueAction(action);  
       
     },
    getQueryString : function(field,url){
        var href = url ? url : window.location.href;
        var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        var string = reg.exec(href);
        return string ? string[1] : null;
    },
    validateEnterpriseReq : function (component, event, helper) {
          var opco=component.get("v.req.OpCo__c");
          var opcoValVar=component.get("v.opcoVarName");
          var businessUnit=component.get("v.req.Req_Division__c");
          var businessUnitValVar=component.get("v.req.businessUnitVarName");
          var descriptionId=component.find("inputSkillDescription");
          var mainskill=component.find("MainSkillId");
         console.log('OpCo value--->'+(typeof opco == "undefined"));
       	 var validated=true;
         if(opco=='--None--' || typeof opco == "undefined"){
            validated=false;
            component.set("v.opcoVarName","OpCo cannot be blank.");
        }else{
             component.set("v.opcoVarName","");
        }

        console.log('businessUnit value--->'+(typeof businessUnit == "undefined"));

        if(businessUnit=='--None--' || typeof businessUnit == "undefined"){
            validated=false;
            component.set("v.businessUnitVarName","Division cannot be blank.");
        }else{
             component.set("v.businessUnitVarName","");
        }
		if(opco=='TEKsystems, Inc.'){
			var productPicklistUnit=component.get("v.req.Product__c");        
			if(productPicklistUnit=='--None--' || typeof productPicklistUnit == "undefined"){
				validated=false;
				component.set("v.ProductPicklistUnitVarName","Product cannot be blank.");
			}else{
				 component.set("v.ProductPicklistUnitVarName",""); 
			}
        }
        var segVar=component.find("segmentId");
        var jobVar=component.find("JobcodeId");
        var catVar=component.find("CategoryId");
        var criteriaValidate=true;
		if(validated    && 
           (opco=='TEKsystems, Inc.' || opco=='Aerotek, Inc') && (
             (businessUnit=='--None--' || typeof businessUnit == "undefined") || (mainskill!=null && 
                          (mainskill.get("v.value")=='' || mainskill.get("v.value")=='--None--' || typeof mainskill.get("v.value")=="undefined" ) )
           || ( segVar!=null && (segVar.get("v.value")=='' || segVar.get("v.value")=='--None--' || typeof segVar.get("v.value") == "undefined" )) || 
            (jobVar!=null && (jobVar.get("v.value")=='' || jobVar.get("v.value")=='--None--' || typeof jobVar.get("v.value") == "undefined")) 
          || (catVar!=null && (catVar.get("v.value")=='--None--' || catVar.get("v.value")=='' || typeof catVar.get("v.value") == "undefined"))                                                                                                    
            )){
            console.log('Req_Standard_Burden_Required  validation rule.');
            component.set("v.productHireValMsg","Please complete product hierarchy.");
            criteriaValidate=false;
        }else{
            component.set("v.productHireValMsg","");
            //criteriaValidate=true;
        }
       return (validated && criteriaValidate);
    },     
    EnterpriseReqSegmentModal : function(component,event,helper,model){
        
        var action = component.get("c.getsegmentReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
       if(component.find("opcoId")!=null && component.find("businessUnitId")!=null){
        action.setParams({"Opco": component.find("opcoId").get("v.value"),"Division": component.find("businessUnitId").get("v.value")});
       }else if(model!=null){
         action.setParams({"Opco": model.opco,"Division": model.division});
       } 
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var segmentmodel = data.getReturnValue();
                
                var segmentarray = [];
                //segmentarray.push({label:'--None--', value:'--None--'}); 
                if(model != null){    
                    if(model.prod.Segment__c != null){
                        segmentarray.push({value:model.prod.Segment__c, key:model.prod.Segment__c});
                    }
                    else{ 
                      segmentarray.push({value:'--None--', key:'--None--'});  
                      component.set("v.prod.Job_Code__c","");  
                    }
                }else{
                    segmentarray.push({value:'--None--', key:'--None--'}); 
                    component.set("v.prod.Job_Code__c","");
                }   
                
                var segmentmap = segmentmodel.SegmentMappings;
                console.log('Size of Segment Mappings 1--->'+segmentmap.length);
                if(segmentmap != undefined){
                    for ( var key in segmentmap) { 
                        if(key != 'VMS' || (key == 'VMS' && component.get('v.model').oppStageName == 'Staging'))
                        {
                            segmentarray.push({label:segmentmap[key], value:key});
                        }
                   }
               }
                
                component.set("v.segmentList", segmentarray);
            }
            });
        $A.enqueueAction(action);
        
    },
    EnterpriseReqJobcodeModal : function(component,event,helper,model){
        var action = component.get("c.getJobcodeReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
       if(component.find("opcoId")!=null && component.find("businessUnitId")!=null && component.find("segmentId")!=null)
        action.setParams({"Opco": component.find("opcoId").get("v.value"),"Division": component.find("businessUnitId").get("v.value"),"segment": component.find("segmentId").get("v.value") });
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var jobcodemodel = data.getReturnValue();
                
                var jobcodearray = [];
                if(model != null){    
                    if(model.prod.Job_Code__c != null){
                        jobcodearray.push({value:model.prod.Job_Code__c, key:model.prod.Job_Code__c});
                    }
                    else{ 
                      jobcodearray.push({value:'--None--', key:'--None--'});
                      component.set("v.prod.Category__c","");   
                    }
                }else{
                    jobcodearray.push({value:'--None--', key:'--None--'});
                    component.set("v.prod.Category__c","");   
                }   
                
                var jobcodemap = jobcodemodel.JobCodeMappings;
                console.log('jobcodemap'+jobcodemap);
                if(jobcodemap != undefined){
                   for ( var key in jobcodemap) {
                    jobcodearray.push({value:jobcodemap[key], key:key});  
                  } 
                }
                console.log('harry'+jobcodearray);
                component.set("v.jobcodeList", jobcodearray);
                
            }
            });
        this.EnterpriseReqReSet(component,event,helper,model);
      
        $A.enqueueAction(action);
        
    },
    
    EnterpriseReqCategoryModal : function(component,event,helper,model){
        var action = component.get("c.getCategoryReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
        if(component.find("opcoId")!=null && component.find("businessUnitId")!=null && component.find("segmentId")!=null && component.find("JobcodeId")!=null)
        action.setParams({"Opco": component.find("opcoId").get("v.value"),"Division": component.find("businessUnitId").get("v.value"),"segment": component.find("segmentId").get("v.value"),"Jobcode" :component.find("JobcodeId").get("v.value")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var categorymodel = data.getReturnValue();
                
                var categoryarray = [];
                if(model != null){    
                	if(model.prod.Category__c != null){
                        categoryarray.push({value:model.prod.Category__c, key:model.prod.Category__c});
                    }
                    else{
                      categoryarray.push({value:'--None--', key:'--None--'});
                      component.set("v.prod.Skill__c","");  
                    }
                }else{
                	categoryarray.push({value:'--None--', key:'--None--'});    
                    component.set("v.prod.Skill__c","");
                }
                var categorymap = categorymodel.CategoryMappings;
                console.log('categorymap'+categorymap);
                if(categorymap != undefined){
                    for ( var key in categorymap) {
                    categoryarray.push({value:categorymap[key], key:key});
                  }
                }
                
                console.log('harry'+categoryarray);
                component.set("v.CategoryList", categoryarray);
                
            }
            });
        this.EnterpriseReqReSet(component,event,helper,model);
        $A.enqueueAction(action);
    },
    
    EnterpriseReqMainSkillModal : function(component,event,helper,model){
        var action = component.get("c.getMainskillReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
       if(component.find("opcoId")!=null && component.find("businessUnitId")!=null && component.find("segmentId")!=null && component.find("JobcodeId")!=null && component.find("CategoryId"))
        action.setParams({"Opco": component.find("opcoId").get("v.value"),"Division": component.find("businessUnitId").get("v.value"),"segment": component.find("segmentId").get("v.value"),"Jobcode" :component.find("JobcodeId").get("v.value"),"Category":component.find("CategoryId").get("v.value")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var MainSkillmodel = data.getReturnValue();
                
                var MainSkillarray = [];
                if(model != null){    
                	if(model.prod.Skill__c != null){
                        MainSkillarray.push({value:model.prod.Skill__c, key:model.prod.Skill__c});
                    }
                    else{
                      MainSkillarray.push({value:'--None--', key:'--None--'});  
                    }
                }else{
                	MainSkillarray.push({value:'--None--', key:'--None--'});    
                }
                
                var MainSkillmap = MainSkillmodel.MainSkillMappings;
                console.log('MainSkillmap'+MainSkillmap);
                for (var key in MainSkillmap) {
                    MainSkillarray.push({value:MainSkillmap[key], key:key});
                }
                console.log('harry'+MainSkillarray);
                component.set("v.MainSkillList", MainSkillarray);
                
            }
            });
        this.EnterpriseReqReSet(component,event,helper,model);
        $A.enqueueAction(action);
    },
    
     redirectToOpportunityURL : function (component, event, helper) {
          var optyUrl ="/one/one.app?source=aloha#/sObject/"+ component.get("v.newOpportunityId");
           console.log(optyUrl);
           var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": optyUrl
            });
            urlEvent.fire();
     },
    
    
    EnterpriseReqMainSkillid : function(component,event,helper,model){
        var action = component.get("c.getMainskillid");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
        action.setParams({"Opco": component.find("opcoId").get("v.value"),"Division": component.find("businessUnitId").get("v.value"),"segment": component.find("segmentId").get("v.value"),"Jobcode" :component.find("JobcodeId").get("v.value"),"Category":component.find("CategoryId").get("v.value"),"MainSkill":component.find("MainSkillId").get("v.value")});
        
            action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var MainSkillid = data.getReturnValue();
                console.log('mainskillid'+ MainSkillid)
            }
          });
        this.EnterpriseReqReSet(component,event,helper,model);
        $A.enqueueAction(action);
    }, 
   
    EnterpriseReqReSet : function (component,event,helper,model){
        
        if(component.find("businessUnitId")!=null && component.find("businessUnitId").get("v.value")=='--None--'){
          	  console.log('Value of business unit id '+(component.find("businessUnitId").get("v.value")));
              component.find("segmentId").set("v.value",'--None--');
            
        }
        
        if(component.find("segmentId")!=null && component.find("segmentId").get("v.value")=='--None--'){
          	  console.log('Value of segmentId id '+(component.find("segmentId").get("v.value")));
              component.find("JobcodeId").set("v.value",'--None--');
            
        }
        
        if(component.find("JobcodeId")!=null && component.find("JobcodeId").get("v.value")=='--None--'){
          	  console.log('Value of JobcodeId id '+(component.find("JobcodeId").get("v.value")));
              component.find("CategoryId").set("v.value",'--None--');
        }
        
         if(component.find("CategoryId")!=null && component.find("CategoryId").get("v.value")=='--None--'){
          	  console.log('Value of CategoryId id '+(component.find("CategoryId").get("v.value")));
              component.find("MainSkillId").set("v.value",'--None--');
        }
        
        if(component.find("MainSkillId")!=null && component.find("MainSkillId").get("v.value")=='--None--'){
          	  console.log('Value of MainSkillId id '+(component.find("MainSkillId").get("v.value")));
              //component.find("MainSkillId").set("v.value",'--None--');
        }
        
    },
     saveEnterpriseReq : function(component,event,helper){
        component.set("v.isDisabled", true);
        var oppReq = component.get("v.req");
           
        var recordId=component.get("v.recordId");
        
        oppReq.id=recordId;
        var oppty = JSON.stringify(oppReq);
         
        var prod=component.get("v.prod");
        var prody=JSON.stringify(prod);
     
        
        var flagval=this.validateEnterpriseReq(component, event);
        console.log('flagval--->'+flagval);
        //Validation for Enterprise Req call.
        if(flagval){  
       
        var officeInfoId = component.get("v.organization.Id");
        //job title
        var jtitle = component.get("v.newJobTitle");
         console.log("new job"+jtitle);
        //Create Enterprise Req
                
        console.log(component.get("v.oppSkills"));
        console.log('passed');
        //console.log(finalSkills);
        //var optyStr = component.get("v.oppSkills");
            
        var opco=component.get("v.req.OpCo__c");
        var opcoValVar=component.get("v.opcoVarName");

        var businessUnit=component.get("v.req.Req_Division__c");
        var businessUnitValVar=component.get("v.req.businessUnitVarName");
        
        var descriptionId=component.find("inputSkillDescription");
        var mainskill=component.find("MainSkillId");
                 
        var actionopp = component.get("c.saveEnterpriseReq");
            actionopp.setParams({"oppStr":  oppty,"prodStr": prody
                                 });
                                    
        actionopp.setCallback(this, function(response) {
                  var oppId = response.getReturnValue();
                  component.set("v.isDisabled", true);
                            // Opp successfully created.
                  if (response.getState() === "SUCCESS") {
                      console.log('Oppid--->'+oppId);
                      var errors = response.getError();
                      console.log(errors);
                      var opptyId = response.getReturnValue();
                      component.set("v.newOpportunityId",opptyId);
                      
                      /*
                                  $A.util.addClass(component.find("MainModel"), "slds-modal slds-fade-in-hide");
                                  $A.util.removeClass(component.find("MainModel"), "slds-modal slds-fade-in-open");        
                                  $A.util.addClass(component.find("modalBackdrop"),  "slds-backdrop slds-backdrop--hide"); 
                                  $A.util.removeClass(component.find("modalBackdrop"),  "slds-backdrop slds-backdrop--open");
                                */
                   //   this.redirectToOpportunityURL(component, event, helper);
                   //$A.get('e.force:refreshView').fire();
                  this.initProductHierarchy (component,event,helper);
                   // $A.get('e.force:refreshView').fire();
                       component.set("v.isSkillsSave","false");
                   }else if (response.getState() === "ERROR") {
                       
                       component.set('v.isDisabled', false);
                       var errors = response.getError();
                       if (errors) {
                           if (errors[0] && errors[0].message) {
                               this.showError(errors[0].message);
                           }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                               this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                           }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                               this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                           }else{
                               this.showError('An unexpected error has occured. Please Contact System Administrator!');
                           }
                       }
                   }
        				});
            component.set('v.isDisabled', true);
            $A.enqueueAction(actionopp);         
        }
                                
       component.set('v.isDisabled', false);
    },
    loadBusinessUnitValues : function(component,event,opcoValue){
        var model=component.get("v.model");
        var opcoVal;
        
        
        
        if(opcoValue=='None'){
            opcoVal=component.get("v.req.OpCo__c");
        }else{
            opcoVal=opcoValue;
        }
        
        var aerotekArray = [];
        var tekArray = [];
        var allegisArray = [];
        var agsArray = [];
        var apArray = [];
        var mlaArray = [];
        
        component.set("v.MainSkillProductid",null);
        
        
        var busUnitMap = [];
        if(opcoVal=='Aerotek, Inc'|| opcoVal=='TEKsystems, Inc.'){
            component.set("v.MainskillOpco",true);
        }else{
            component.set("v.MainskillOpco",false);
        }
        
        
        var opArray=component.get("v.opcoDivisionList");
        
        console.log('Load Business Unit Values--->'+opArray);
        
        for (var i=0; i<opArray.length; i++) {
            
            console.log('Values from dependent picklists-->'+opArray[i].value);
            console.log('Values from dependent picklists-->'+opArray[i].key);
            console.log('OPCO String--->'+opcoVal);
            console.log('opcoVal-->'+opArray[i].key ==opcoVal);
            
            if(opcoVal=='AG_EMEA'){
                opcoVal='AG EMEA';
            }
            
            if(opArray[i].key ==opcoVal){
                var res=opArray[i].value;
                var spltArry=res.toString().split(",");
                console.log('Split Result is--->'+res);
                console.log('Split array is--->'+spltArry);
                for(var j=0;j<spltArry.length;j++){
                    if(res[j] != 'VMS' || (res[j] == 'VMS' && component.get('v.model').oppStageName == 'Staging'))
                    {
                        busUnitMap.push(res[j]);
                    }
                }
                
            }
          //  break;
        }
        
        var busUnitArray = [];
        busUnitArray.push({value:'--None--', key:'--None--'}); 
        for ( var key in busUnitMap) {
            console.log('busUnitMap[key]-------->'+(typeof busUnitMap[key]!='undefined'));
            if(busUnitMap[key]!=='' || typeof busUnitMap[key]!=='undefined')
                busUnitArray.push({value: busUnitMap[key], key:key});
        }
        console.log('busienss array');
        console.log(busUnitArray);

		var oppBusinessUnitList = busUnitArray;
                     oppBusinessUnitList.sort(function compare(a,b){
                                  if (a.value > b.value)
                                    return -1;
							      if (a.value < b.value)
                                    return 1;
                                  return 0;
                                });
         
         oppBusinessUnitList.reverse();
        component.set("v.businessUnitList", oppBusinessUnitList);
        if(component.find("businessUnitId")!=null)
        component.find("businessUnitId").set("v.value", '--None--');
        
    }
})