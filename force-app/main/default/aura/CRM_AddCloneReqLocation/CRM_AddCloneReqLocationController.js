({	
	doInit : function(component,event,helper) {
        
       var createReqFlag=component.get("v.createReqFlag");
       component.set("v.workAddressFlag",false);
            if(component.get("v.countriesMap") === null){
                var action = component.get("c.getCountryMappingsWithAbbr");
                action.setStorable();
                action.setCallback(this,function(response) {
                    let countryState=response.getReturnValue();
                    component.set("v.countriesMap",countryState.countriesMap);
                    component.set("v.stateMap",countryState.globalLovStateMap);
                    var oppRecord=component.get("v.oppRecordFields");
                    
                    var createReqFlag=component.get("v.createReqFlag");
                     
                     if(createReqFlag==true){
                       
                         var req=component.get("v.req");
                         var recAccount=req.account;
                         var recContact=req.contact;
                         var reqInit=JSON.parse(JSON.stringify(req));
                         component.set("v.reqInit",reqInit);
                         component.set("v.glookupFlag",false);
                         component.set("v.presetLocation",req.Req_Worksite_Street__c);
                         let countryMdt=helper.getKeyByValue(component.get("v.countriesMap"),req.Req_Worksite_Country__c);
                         if(typeof countryMdt!='undefined' && countryMdt!=null && countryMdt!=''){
                         	component.set("v.MailingCountryKey",countryMdt.Label);
                            req.Req_Worksite_Country__c=countryMdt.CountryName__c;
                            let stateGlov=helper.getStateByKey(component.get("v.stateMap"),countryMdt.Label+'.'+req.Req_Worksite_State__c);
                            if(typeof stateGlov!='undefined' && stateGlov!=null && stateGlov!=''){
                                component.set("v.MailingStateKey",stateGlov.Text_Value_3__c);
                                req.Req_Worksite_State__c=stateGlov.Text_Value_3__c;
                            }  
                         }    
                    component.set("v.req",req);
                    }
                    
                    
                    component.set("v.eventSourceFlag",false); 
                    component.set("v.workAddressFlag",true);
                    
               });
                $A.enqueueAction(action);
            }
       
    },
    //S-65378 - Added by KK - Update Mailing fields on location selection
    handleLocationSelection : function(component,event, helper) {
		component.set("v.isGoogleLocation", true);
        component.set("v.eventSourceFlag",true);   
        var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode,countrymap,displayText;
		mailingstreet = '';
		streetnum = '';
		streetname = '';
		city = '';
		state = '';
		country = '';
		countrycode = '';
		postcode = '';
		displayText = '';
		var addr = event.getParam("addrComponents");
		var latlong = event.getParam("latlong");     
		countrymap = component.get("v.countriesMap"); 
		displayText = event.getParam("displayText");
        //console.log('address component'+JSON.stringify(addr));
        for (var i=0; i<addr.length; i++) {
            var addTypes=addr[i].types;
            if (addr[i].types.indexOf("street_number") >= 0) {
                streetnum = addr[i].long_name;
            }
            if (addr[i].types.indexOf("route") >= 0) {
                streetname = addr[i].long_name;
            }        
            if (addr[i].types.indexOf("postal_code") >= 0) {
                postcode = addr[i].long_name;
            }
            if (addr[i].types.indexOf("locality") >= 0) {
                city = addr[i].long_name;
            }
            if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
                state = addr[i].long_name;
            }
            if (addr[i].types.indexOf("country") >= 0) {
                country = addr[i].long_name;
                countrycode = countrymap[addr[i].short_name];
            }        
        }
        //console.log('teststreet',streetnum,streetname);
        if(streetnum) {
            mailingstreet = streetnum;
        } 
        if(streetnum && streetname) {
            mailingstreet = mailingstreet + ' ';
        }
        if(streetname) {
            if(mailingstreet != ''){
                mailingstreet = mailingstreet + streetname;        
            } else {
                mailingstreet = streetname;
            }
        }
        //console.log(mailingstreet,city,state,countrycode,country,postcode,displaytext);
       
       // component.set("v.addinsertFlag",false);
        var oppRecord=component.get("v.req");
        if(typeof oppRecord!='undefined' && oppRecord!=null){
            //
           // component.set("v.workSiteStreet",mailingstreet);
            oppRecord.Req_Worksite_Street__c=mailingstreet;
            oppRecord.Req_Worksite_City__c=city;
            oppRecord.Req_Worksite_State__c=state;
            oppRecord.Req_Worksite_Country__c=country;
            oppRecord.Req_Worksite_Postal_Code__c=postcode;
            oppRecord.StreetAddress2__c='';
            //oppRecord.StreetAddress2__c=component.get("v.Opportunity.StreetAddress2__c");
            component.set("v.req",oppRecord); 
        }
            
       
        
		//var str = displayText.substr(0,displayText.indexOf(',')); 
        component.set("v.presetLocation",mailingstreet);
		//component.set("v.streetAddress2","");
		//S-96255 - Update MailLat, MailLong fields on Contact
		if(latlong && (mailingstreet || city || postcode)){
			component.set("v.req.Req_GeoLocation__Latitude__s",latlong.lat);
			component.set("v.req.Req_GeoLocation__Longitude__s",latlong.lng);
             
       } else {
			component.set("v.req.Req_GeoLocation__Latitude__s",null);
			component.set("v.req.Req_GeoLocation__Longitude__s",null);
       }
        component.set("v.glookupFlag",true);
        const tReq=JSON.parse(JSON.stringify(component.get("v.req")));
        component.set("v.reqInit",tReq);
        helper.compareAddress(component,event,helper); 
         //helper.validateAddress(component, event);
		//component.set("v.contact.LatLongSource__c","Google");
		component.set("v.viewState","EDIT");
		component.set("v.parentInitialLoad",true);
		var countryCode = helper.getKeyByValue(component.get("v.countriesMap"),countrycode);
		component.set("v.MailingCountryKey",countryCode);
    },
    updateMailingCountryCode: function(component, event, helper) {
		helper.updateMailingCountryCode(component, event);
	},
    enableInlineEdit :function(component, event, helper) {
       console.log('inline edit pressed');
       component.set("v.invokeInlineEdit",!component.get("v.invokeInlineEdit"));
       component.set("v.editMode",!component.get("v.editMode"));
    },
    presetLocationValueChange :function(component, event, helper) {
        var searchText=component.get("v.presetLocation");
     	console.log('presetLocationValueChange---searchText-->'+searchText);
        var oppRecord=component.get("v.req");
        if(typeof oppRecord!='undefined' && oppRecord!=null){
            oppRecord.Req_Worksite_Street__c=searchText;
        	component.set("v.req",oppRecord);
            helper.compareAddress(component, event, helper);
        }
    },
	addFieldValidationChange:function(component, event, helper) {
        var testmap=component.get("v.addFieldMessagedMap");
        console.log('addFieldMessagedMap----->'+testmap);
    },
	onRender:function(component, event, helper) {
    //component.set("v.workAddressFlag",true);
   },
   onChangeCompareAddress:function(component,event,helper){
   	 helper.compareAddress(component,event,helper);     
   } 
})