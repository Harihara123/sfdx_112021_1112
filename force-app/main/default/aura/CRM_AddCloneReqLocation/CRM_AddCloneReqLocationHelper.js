({
   getKeyByValue : function(map, searchValue) {
		for(var key in map) {
			if (map[key].CountryCode__c === searchValue || map[key].CountryName__c  === searchValue || map[key].Label === searchValue) {
				return map[key];
			}
		}
	},
    getStateByKey : function(map, searchValue) {
		for(var key in map) {
			if (map[key].Text_Value_2__c === searchValue) {
				return map[key];
			}
		}
	},
    updateMailingCountryCode: function(component, event) {
        var conKey = component.get('v.MailingCountryKey');
        var countrymap = component.get("v.countriesMap");
        var countrycode ;
        if (conKey !== null && typeof conKey !== 'undefined' && countrymap.hasOwnProperty(conKey)) {
            countrycode = countrymap[conKey].Label;
            if(typeof countrycode!='undefined' && countrycode!=null && (countrycode=='US' || countrycode=='CA')){
                component.set("v.isStateMandFlag",true); 
            }else{
                component.set("v.isStateMandFlag",false); 
            }   
        }
    },
    validateAddress:function(cmp,event){
         var oppAddress=cmp.get("v.Opportunity");
         var fieldMessages=cmp.get("v.addFieldMessage");
         if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
             cmp.set("v.streetErrorVarName","Street cannot be blank.");
             fieldMessages.push("Street cannot be blank.");
           }else{
             cmp.set("v.streetErrorVarName","");
          }
        
         
         if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
             cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
             fieldMessages.push("City cannot be blank.");
           }else{
             cmp.set("v.cityTypeErrorVarName","");
          }
        
         if(oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == ''){
             cmp.set("v.stateErrorVarName","State cannot be blank.");
             fieldMessages.push("State cannot be blank.");
           }else{
             cmp.set("v.stateErrorVarName","");
          }
        
         if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
            cmp.set("v.countryVarName","Country cannot be blank.");
            fieldMessages.push("Country cannot be blank.");
           }else{
             cmp.set("v.countryVarName","");
          }
        
         if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
             cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
             fieldMessages.push("Postal Code cannot be blank.");
          }else{
             cmp.set("v.zipErrorVarName","");
          }
        
        
        if(typeof fieldMessages!='undefined' && fieldMessages!=null && fieldMessages.length>0){
            cmp.set("v.insertFlag",true);
            cmp.set("v.addFieldMessage",fieldMessages);
        }else{
             cmp.set("v.insertFlag",false);
        }
        
    },
    isStateMandatory : function(component,event,helper) {
        let opp=component.get("v.req");
    	let countryVar=opp.Req_Worksite_Country__c;
        let mandFlag=false;
        
        if(countryVar.includes('United States') || countryVar.includes('USA') ){
            mandFlag=true;
        }else if(countryVar.includes('Canada')){
            mandFlag=true;
        }else{
            mandFlag=false;
        }
        component.set("v.isStateMandFlag",mandFlag); 
    },
    compareAddress :function(component,event,helper) {
       var originalReq=component.get("v.reqInit");
       var currentReq=component.get("v.req");
      /* if(event.target!=null){
       	var acId=event.target.value;
       }*/
     
        var oworksiteStreet= originalReq.Req_Worksite_Street__c;
        var oworksiteCity=originalReq.Req_Worksite_City__c;
        var oworksiteState=originalReq.Req_Worksite_State__c;
        var oworksiteCountry=originalReq.Req_Worksite_Country__c;
        var oworksitePostalCode=originalReq.Req_Worksite_Postal_Code__c;
        var oworksiteStreet2=originalReq.StreetAddress2__c;  
        
      	
        var cworksiteStreet=currentReq.Req_Worksite_Street__c;
        var cworksiteCity=currentReq.Req_Worksite_City__c;
        var cworksiteState=currentReq.Req_Worksite_State__c;
        var cworksiteCountry=currentReq.Req_Worksite_Country__c;
        var cworksitePostalCode=currentReq.Req_Worksite_Postal_Code__c;
        var cworksiteStreet2=currentReq.StreetAddress2__c;
        
        var streetFlag=(oworksiteStreet!=cworksiteStreet);
        var siteFlag=(oworksiteCity!=cworksiteCity);
        var stateFlag=(oworksiteState!=cworksiteState);
        var pcodeFlag=(oworksitePostalCode!=cworksitePostalCode);
        var countryFlag=(oworksiteCountry!=cworksiteCountry);
      //  let street2Flag=(oworksiteStreet2!=cworksiteStreet2);
		
        if(streetFlag || siteFlag || stateFlag || pcodeFlag ||countryFlag){
             var olat=originalReq.Req_GeoLocation__Latitude__s;
             var olong=originalReq.Req_GeoLocation__Longitude__s;
             var clat=component.get("v.req.Req_GeoLocation__Latitude__s");
             var clong=component.get("v.req.Req_GeoLocation__Longitude__s");
            if(clat!=null && clong!=null && olat!=null && olong!=null){
                if(clat==olat && clong==olong){
    				component.set("v.req.Req_GeoLocation__Latitude__s",null);
                    component.set("v.req.Req_GeoLocation__Longitude__s",null);
                }    
            }
           component.set("v.isDirtyFlag",true);
           console.log('Original Lat ------>'+olat);
           console.log('Original Long ------>'+olong);
           console.log('Current Lat ------>'+clat);
           console.log('Current Long ------>'+clong);  
        }else{
             var olat=originalReq.Req_GeoLocation__Latitude__s;
             var olong=originalReq.Req_GeoLocation__Longitude__s;
             var clat=component.get("v.req.Req_GeoLocation__Latitude__s");
             var clong=component.get("v.req.Req_GeoLocation__Longitude__s");
            if(!streetFlag && !siteFlag && !stateFlag && !pcodeFlag && !countryFlag){
                if(olat!=null && olong!=null){
                        component.set("v.req.Req_GeoLocation__Latitude__s",olat);
                        component.set("v.req.Req_GeoLocation__Longitude__s",olong);
              }
            }
           component.set("v.isDirtyFlag",false);
           console.log('Original Lat ------>'+olat);
           console.log('Original Long ------>'+olong);
           console.log('Current Lat ------>'+clat);
           console.log('Current Long ------>'+clong);   
        }
       
       var clat=component.get("v.req.Req_GeoLocation__Latitude__s");
       var clong=component.get("v.req.Req_GeoLocation__Longitude__s"); 
       
       console.log('Final lat ---->'+clat+'----and long is--->'+clong); 
       console.log('change flag is---->'+component.get("v.isDirtyFlag"));
    
   }     
})