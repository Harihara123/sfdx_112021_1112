({
    doInit :function(cmp, event, helper) {
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true);
        
    },
	recordLoadedView :function(cmp, event, helper) {
       
        var accountInfoVar = document.getElementById("accountInfo")
                if(accountInfoVar != null){
                    var yPosition = document.getElementById("accountInfo").offsetTop;
                    window.scrollTo(0,yPosition-50);
                    cmp.set("v.isFocus1", true);
                    
                }
        //Andy: S-174826 setting variables for Show More/Less component.
        const {recordUi} = event.getParams();
        try {
            let showMore = {};
            helper.SHOWMORE_FIELDS.forEach(field => {
                const value = (recordUi.record.fields[field] || {}).value || '';
                showMore[field] = value;
            })
            cmp.set('v.showMore', showMore);
			/*
        	//Fetch Related titles on load
			const relatedTitles = recordUi.record.fields.Suggested_Job_Titles__c.value;
			if(relatedTitles) {
				const parsedTitles = JSON.parse(relatedTitles);
				cmp.set("v.initialSuggTitles",parsedTitles);
				let suggTitles = [];
				parsedTitles.forEach(title => {
					if (!title.removed) {
						suggTitles.push(title.term);
					}
				})
				cmp.set("v.suggJobTitles",suggTitles);
				cmp.set("v.initialJobTitle",recordUi.record.fields.Req_Job_Title__c.value);
			} */
            /*
			//End of Fetch related titles on view
            const skills = recordUi.record.fields.EnterpriseReqSkills__c.value;
            if (skills) {
                const parsedSkills = JSON.parse(skills);
                cmp.set('v.skills', parsedSkills);
                let topSkills = [];
                let secondarySkills = []
                parsedSkills.forEach(skill => {
                    if (skill.favorite) {
                        topSkills.push(skill.name)
                    } else {
                        secondarySkills.push(skill.name)
                    }
                })
                cmp.set('v.topSkills', topSkills);
				cmp.set('v.topSkillsOnCancel', topSkills);
                cmp.set('v.secondarySkills', secondarySkills);
				cmp.set('v.secondarySkillsOnCancel', secondarySkills);
            } */

            //console.log('%cShow More fields','background-color: black; color: aqua;',showMore);

        } catch (err) {
    	//console.log(err)
		}

        if(recordUi.record.fields.Req_RVT_Occupation_Code__c && recordUi.record.fields.RVT_Occupation_Group__c &&
            recordUi.record.fields.Req_Job_Level__c && recordUi.record.fields.Metro__c && 
            recordUi.record.fields.Req_Worksite_Country__c) {
            var ocCode = recordUi.record.fields.Req_RVT_Occupation_Code__c.value;
            var ocGroup = recordUi.record.fields.RVT_Occupation_Group__c.value;
            var jobLevel = recordUi.record.fields.Req_Job_Level__c.value;
            var metro = recordUi.record.fields.Metro__c.value;
            var country = recordUi.record.fields.Req_Worksite_Country__c.value;
            var lcountry;
            if(country != null && country != undefined) {
                lcountry = country.toLowerCase();
            }
            var fcountry;
            if(lcountry=='united states' || lcountry=='usa' || lcountry=='us'){
                fcountry = 'USA';
            }
            else if(lcountry='canada' || lcountry=='can' || lcountry=='ca'){
                fcountry = 'CAN';
            }               
            
            if((ocCode!=null && ocCode!=0) && (ocGroup!=null && ocGroup!=0) && (jobLevel!=null && jobLevel!=0) && metro != null && fcountry != null){
                var baseURL = $A.get("$Label.c.RVT_PROD_URL")
                var link = baseURL+'&parent_hcs_description='+ocGroup+'&Hcs%20Group%20Display='+ocCode+'&Hcs%20Group%20Level%20Display='+jobLevel+'&Country%20Code='+fcountry+'&Ag%20Geog%20Short%20Name%20Orig='+metro;            
                cmp.set("v.RVTLink",link);
            }
        }

        //console.log('record view 5');
        var isOnLoad = cmp.get("v.isInit");
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var skillAction = cmp.get("c.getAccessDetailsForSkillSet");
        skillAction.setParams({"currentUserId": userId});
        skillAction.setCallback(this, function(response) {
            if (response.getState() == 'SUCCESS') 
            {
                var skillAccess = response.getReturnValue();
                //alert('skillAccess::'+skillAccess);
                cmp.set("v.suggestedSkillsFlag",skillAccess);
            }
        });
        $A.enqueueAction(skillAction);
        try {
           var flag=cmp.get("v.viewLoaded");
            cmp.set("v.skillRefreshFlag",true);
            cmp.set("v.jobTitleRefreshFlag",true);
            cmp.set("v.addinsertFlag",true);
           if(flag==false){
               cmp.set("v.viewLoaded",true);
               if(isOnLoad===false){
                   cmp.set("v.editFlag",false);
                   cmp.set("v.viewFlag",true);  
               }
			 
               //Focus on top section after save
              /* var accountInfoVar = document.getElementById("accountInfo");
                if(accountInfoVar != null){
                    var yPosition = document.getElementById("accountInfo").offsetTop;
                    window.scrollTo(0,yPosition-50);
                    cmp.set("v.isFocus1", true);
                    
                } */
               
             //Fetch Related titles on load
			const relatedTitles = recordUi.record.fields.Suggested_Job_Titles__c.value;
			if(relatedTitles) {
				const parsedTitles = JSON.parse(relatedTitles);
				cmp.set("v.initialSuggTitles",parsedTitles);
				let suggTitles = [];
				parsedTitles.forEach(title => {
					if (!title.removed) {
						suggTitles.push(title.term);
					}
				})
				cmp.set("v.suggJobTitles",suggTitles);
				cmp.set("v.initialJobTitle",recordUi.record.fields.Req_Job_Title__c.value);
			} 
			//End of Fetch related titles on view
            const skills = recordUi.record.fields.EnterpriseReqSkills__c.value;
            if (skills) {
                const parsedSkills = JSON.parse(skills);
                cmp.set('v.skills', parsedSkills);
                let topSkills = [];
                let secondarySkills = []
                parsedSkills.forEach(skill => {
                    if (skill.favorite) {
                        topSkills.push(skill.name)
                    } else {
                        secondarySkills.push(skill.name)
                    }
                })
                cmp.set('v.topSkills', topSkills);
				cmp.set('v.topSkillsOnCancel', topSkills);
                cmp.set('v.secondarySkills', secondarySkills);
				cmp.set('v.secondarySkillsOnCancel', secondarySkills);
            }
               
			   var payval = event.getParams().recordUi.record.fields.Req_Payroll__c.value;
               if(payval == true){
                   cmp.set("v.payrollVal", true);
               }
			   
			   var exclusive = event.getParams().recordUi.record.fields.Req_Exclusive__c.value;
               if(exclusive == true){
                   cmp.set("v.exclusiveVal", true);
               }   
               
                var recordU=event.getParam("recordUi");
                var prodval=event.getParams().recordUi.record.fields.Req_Product__c.value;
                //console.log('prodtype val rU'+prodval);
				helper.getAddressData(cmp, event, helper);
                //helper.getOpcoSectionData(cmp, event, helper);

                let payload = {};
                payload["req_job_title__c"] = recordUi.record.fields.Req_Job_Title__c.value;
                payload["req_job_description__c"] = recordUi.record.fields.Req_Job_Description__c.value;
                cmp.set("v.payload", payload);   
                if(event.getParams().recordUi.record.fields.Req_Manually_Updated_Fields__c !=null){
                    let ManualUpdatedFields=event.getParams().recordUi.record.fields.Req_Manually_Updated_Fields__c.value;
                    cmp.set("v.req.Req_Manually_Updated_Fields__c",ManualUpdatedFields);
                }
            
               var actSectsList = cmp.get("v.activeSections"); 
                    // F cth and SI -- Perm
                 if(prodval=='Permanent' ){
                         cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",false);
                     cmp.set("v.contractFlag",false);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("F");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                 }else if(prodval=='Contract to Hire'){
                     cmp.set("v.permFlag",false);
                          cmp.set("v.cthFlag",true);
                     cmp.set("v.contractFlag",false);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("F");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         }
                 }else{
                          cmp.set("v.cthFlag",false);
                          cmp.set("v.permFlag",false);
                     cmp.set("v.contractFlag",true);
                          
                         if(actSectsList != undefined){
                              //console.log("act sec"+actSectsList);
                              actSectsList.push("F");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                              //console.log("after F sec"+actSectsList);
                         } 
                     }
             
             //Adding w.r.t REDZONE
             var redZone =event.getParams().recordUi.record.fields.Req_Red_Zone__c.value;
             cmp.set("v.redZoneReq",redZone);
             //Adding w.r.t Qualyfying sate
             var stgName=event.getParams().recordUi.record.fields.Req_Qualifying_Stage__c.value;                                       
             var stageArray = [];
                    stageArray.push({value:'--None--', key:'--None--'});
             		stageArray.push({value:'Draft', key:'Draft',selected: 'Draft' === stgName });
                    stageArray.push({value:'Qualified', key:'Qualified',selected: 'Qualified' === stgName});
               		stageArray.push({value:'RedZone', key:'RedZone',selected: 'RedZone' === stgName});
                    cmp.set("v.stageList", stageArray);
               cmp.set("v.req_stage",stgName);
               if(stgName == "RedZone"){
                   cmp.set("v.StageName","Qualified");                   
               }else{
                   cmp.set("v.StageName",stgName);                   
               }        	
             helper.clientWorkingOnReqHelper(cmp, event, helper);       
                    //Adding w.r.t TGS Requirement
            var TGSReq =event.getParams().recordUi.record.fields.Req_TGS_Requirement__c.value;
			cmp.set("v.req.Req_TGS_Requirement__c",TGSReq);			            
             var TGSReqArray = [];
             TGSReqArray.push({value:'--None--', key:'--None--'});
             TGSReqArray.push({value:'Yes', key:'Yes',selected: 'Yes' === TGSReq });
             TGSReqArray.push({value:'No', key:'No',selected: 'No' === TGSReq}); 
             cmp.set("v.isTGSReqList", TGSReqArray);
            cmp.set("v.IsTGSReq",TGSReq=='Yes'?true:false);
            
               helper.fieldsToPopulateOnPageLoad(cmp, event, helper);
               
               
               //helper.clientWorkingOnReqHelper(cmp, event, helper);     
               helper.getOpcoSectionData(cmp, event, helper);
                    event.setParam("recordUi",recordU);

                  }
            //Adding w.r.t S-238087 O-Number on Manual Req From VMS Req at merge
            let mergedReqName = event.getParams().recordUi.record.fields.Merged_Req_Name__c.value;
            let mergedReqId = event.getParams().recordUi.record.fields.Originating_Opportunity__c.value;
            cmp.set("v.mergedReqId", mergedReqId);
            cmp.set("v.mergedReqName", mergedReqName);
            //End of Adding w.r.t S-238087 O-Number on Manual Req From VMS Req at merge
        }
        catch(err) {
            //console.log('errors '+err);
        } 
        finally {
            var drugTestCheckbox = event.getParams().recordUi.record.fields.Req_Drug_Test_Required__c.value;
            var OFCCPCheckbox = event.getParams().recordUi.record.fields.Req_OFCCP_Required__c.value;
            var backgroundCheckbox = event.getParams().recordUi.record.fields.Req_Background_Check_Required__c.value;
            var governmentCheckbox = event.getParams().recordUi.record.fields.Req_Government__c.value;			   			             
            
            var drugTest= drugTestCheckbox ? 'Drug Test Required; ' : '';
            var OFCCP = OFCCPCheckbox ? 'OFCCP Required; ' : '' ; 
            var background = backgroundCheckbox ? 'Background Check Required; ' : '' ;
            var government = governmentCheckbox ? 'Government ' : '' ;
            
            var complainceVal = (drugTest + OFCCP + background).trim();
            if((complainceVal.charAt(complainceVal.length-1))==";"){
               complainceVal=complainceVal.substring(0,complainceVal.length-1);
            }
            cmp.set("v.ComplainceVar",complainceVal); 
            
            var additionalReqConcatenate='';           
            var subVendorCheckbox = event.getParams().recordUi.record.fields.Req_Can_Use_Approved_Sub_Vendor__c.value;
            var subVendor =subVendorCheckbox ? 'Can Use Approved Sub-vendor; ' : '';
            var startCheckbox = event.getParams().recordUi.record.fields.Immediate_Start__c.value;
            var startCheck = startCheckbox ? 'Go To Work; ' : '' ;            
            var exclusiveCheckbox = event.getParams().recordUi.record.fields.Req_Exclusive__c.value;
            var exclusiveCheck = exclusiveCheckbox ? 'Exclusive; ' : '' ;
			var payRollCheckbox = event.getParams().recordUi.record.fields.Req_Payroll__c.value;
            var payRoll =payRollCheckbox ? 'Payroll; ' : '';
            
            additionalReqConcatenate = (subVendor + startCheck + exclusiveCheck + payRoll).trim();            
            if((additionalReqConcatenate.charAt(additionalReqConcatenate.length-1))==";"){
               additionalReqConcatenate=additionalReqConcatenate.substring(0,additionalReqConcatenate.length-1);
            }
            cmp.set("v.AdditionalVar",additionalReqConcatenate);
            
            var reqprod =event.getParams().recordUi.record.fields.Req_Product__c.value;            
            cmp.set("v.placementType",reqprod);            
            if(sgn == 'Qualified'){
			   //cmp.set("v.stgQual",true);
			}
            var statusLDSOnLoad;
            var prevStageOnLoad;
            var sgn=event.getParams().recordUi.record.fields.StageName.value;
			cmp.set("v.initialStageName",sgn);
            
            
            
            if(event.getParams().recordUi.record.fields.LDS_Status__c != null){
                statusLDSOnLoad = event.getParams().recordUi.record.fields.LDS_Status__c.value;
                cmp.set("v.statusLDS",statusLDSOnLoad);
            }
            if(event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c != null){
                prevStageOnLoad = event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c.value;
                cmp.set("v.prevStage",prevStageOnLoad);
            }            
            if(statusLDSOnLoad == 'Open'){
                if(sgn == 'Draft'){
                    cmp.set("v.StageName",'Draft');
                }else{
                    cmp.set("v.StageName",'Qualified');
                }
            }else if(statusLDSOnLoad == 'Closed'){
                if(prevStageOnLoad == 'Draft' && sgn.substring(0,6) == 'Closed'){
                    cmp.set("v.StageName",'Draft');
                }/*else if(prevStageOnLoad == 'Staging' && sgn.substring(0,6) == 'Closed'){
                    cmp.set("v.StageName",'Staging');
                }*/else{
                    cmp.set("v.StageName",'Qualified');
                }
                
            }       
        }
    },
	
    recordLoadedEdit :function(cmp, event, helper) {
        //console.log('edit pressed 25');

        var btn = event.getSource();
        var isOnLoad = cmp.get("v.isInit");
        //console.log('btn pressed '+btn); 
        var editLoadedFlag=cmp.get("v.editLoaded");
         if(editLoadedFlag==false){
             if(isOnLoad===false){
                 cmp.set("v.editFlag",false);
                 cmp.set("v.viewFlag",true);  
             }
             
             //var jobDescription;
             //var addnlQualify;
			 //var extDesc;
             var IntervInformation;
             //var TopSkillDet;
			 
             if(cmp.find("stageIdReason") != undefined && cmp.find("stageIdReason").get("v.value") == '--None--'){
                cmp.find("stageIdReason").set("v.value","");
             }
             if(cmp.find("workRemoteId") != undefined && cmp.find("workRemoteId").get("v.value") == '--None--'){
                cmp.find("workRemoteId").set("v.value","");
             }
			
			 helper.fieldsToPopulateOnPageLoad(cmp, event, helper);         
             
             //Client Working On Req
             helper.clientWorkingOnReqHelper(cmp, event, helper);     
             
			 if(event.getParams().recordUi.record.fields.Req_Opportunity_Owner__c!=null){
                 setTimeout(
                     $A.getCallback(function() {
                         var emeaOwnerEditVar = cmp.find("emeaOwneridE");
                         if(emeaOwnerEditVar != null){
                             cmp.find("emeaOwneridE").set("v.value",event.getParams().recordUi.record.fields.OwnerId.value);  
                         }
                     }),1000
                 ); 
             }
             
             var sgn;
             var statusLDS;
             var prevStage;
             if(event.getParams().recordUi.record.fields.StageName != null){
             	sgn=event.getParams().recordUi.record.fields.StageName.value;
             }    
            //New Qualifying Stage Changes
             if(event.getParams().recordUi.record.fields.LDS_Status__c != null){
             	statusLDS = event.getParams().recordUi.record.fields.LDS_Status__c.value;
             }
             if(event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c != null){
            	prevStage = event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c.value;
             } 
            if(event.getParams().recordUi.record.fields.Req_Product__c != null){
				cmp.set("v.placementtypeLoadValue",event.getParams().recordUi.record.fields.Req_Product__c.value);
            }
            
             if(cmp.find("acctId") != undefined){
                var aeroAcc = cmp.find("acctId");
                var flg=Array.isArray(aeroAcc);
                var acctId = event.getParams().recordUi.record.fields.AccountId.value;
                //console.log("Acc id"+acctId);
                if(flg){
                     //console.log("acc is array");
                     aeroAcc[0].set("v.value",acctId);
                    }else{
                      //console.log("not an array");
                      aeroAcc.set("v.value",acctId);
                   }
             }
         }
    },
    handleError : function(cmp,event,helper){
        //console.log('error reached '); 
        cmp.set("v.editDisable",false);
        var params = event.getParams();
        var Validation = false;
        var validationMessages=[];
        
         if(params.output!=null && params.output.fieldErrors!=null ){
           
            var stgErrors=params.output.fieldErrors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      var checkFlag=Array.isArray(value);
                      //console.log(key + ':' + value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                Validation = true;
                });
         
        }
        
        if(params.output!=null && params.output.errors!=null ){
           
            var stgErrors=params.output.errors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      //console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                Validation = true;
                });
         
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
        var fieldErrors=event.getParams().error.data.output.fieldErrors;
        Object.keys(fieldErrors).forEach(function(key){
  				      var value = fieldErrors[key];
   					  //console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
            
            		  validationMessages.push(fmessage);
            Validation = true;
            
			});
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {
            
            var fieldErrors=event.getParams().error.data.output.errors;
            
            Object.keys(fieldErrors).forEach(function(key){
  				  var value = fieldErrors[key];
   					 //console.log(key + ':' + value);
                   var checkFlag=Array.isArray(value);
            	   var fmessage=(checkFlag==true)?value[0].message:value.message;
            		  validationMessages.push(fmessage);
                Validation = true;
            
			});
            
        }
        //console.log('handleError----------->'+validationMessages);	
        	
        //console.log("ValidationMessage Retrieve----->"+cmp.get("v.validationMessagesList"));
        
    },
    
    onSubmit  : function(cmp, event, helper) {
        cmp.set("v.showSpinner", true); 
        //console.log('on submit click ');
        event.preventDefault();
        var error_count; 
        var Validation = true;
        cmp.set("v.editDisable",true);
        var eventFields = event.getParam("fields");
        
        var fieldMessages=[];
        var validated = true;
        helper.removeActiveSection(cmp,event,helper);
        //helper.validateAddress(cmp,event);
        eventFields=helper.validateReqAddr(cmp,event,helper,eventFields);
        
        var addressErrors=cmp.get("v.addFieldMessagedMap");
        if(typeof addressErrors!='undefined' && addressErrors!=null && Object.keys(addressErrors).length>0){
            validated=false;
        }
        
        var workSiteOpp=cmp.get("v.Opportunity");
        var addFieldMessages=cmp.get("v.addFieldMessage");
        
        if(typeof addFieldMessages!='undefined' && addFieldMessages!=null && addFieldMessages.length>0){
            fieldMessages=fieldMessages.concat(addFieldMessages);
            validated=false;
        }else{
            fieldMessages=fieldMessages.concat(addFieldMessages)
            
        }
        
     // validated=helper.validateProdHierarchy(cmp,event,helper);
        if(validated){
            eventFields=helper.productHierarchySection(cmp,event,helper,eventFields); 
        }
     // eventFields.Req_TGS_Requirement__c= cmp.get("v.req.Req_TGS_Requirement__c");
        if(cmp.get("v.req.Req_TGS_Requirement__c")=="Yes"){            
            eventFields=helper.setTGSSectionData(cmp,event,helper,eventFields);
        }        
        
       //Related Titles
		var prepareRelatedTitles = cmp.find("relatedTitles");
		prepareRelatedTitles.prepareRelatedTerms();
		let relatedTitles = cmp.get("v.finalRelatedTitlesList");
		if(relatedTitles.length > 0){
			eventFields.Suggested_Job_Titles__c = JSON.stringify(relatedTitles);
		} else {
			eventFields.Suggested_Job_Titles__c = null;
		}

        //helper.prepareSkills(cmp, event);
        cmp.set("v.loopFlag",false);
        //var finalSkillList=cmp.get("v.finalskillList");
        var jsonSkills=cmp.get("v.stringSkills");
        var eventFields = event.getParam("fields");
        if (jsonSkills) {
            eventFields.EnterpriseReqSkills__c=jsonSkills;
        }
        
        //Job Description
        var desc = cmp.get("v.editJobdescription");
        eventFields.Req_Job_Description__c = desc ;

        if(eventFields.Req_Job_Description__c != null)
        {
            eventFields.Description=eventFields.Req_Job_Description__c;
        }
        
         //Top Skills Details
        var topSkillDet = cmp.get("v.editTopSkillDetls");
        eventFields.Req_Skill_Details__c = topSkillDet ; 
		
		 //Client Opened Position
        var clientOpenedPos = cmp.get("v.clientOpenPositionDate");
        eventFields.Req_Date_Client_Opened_Position__c = clientOpenedPos ; 
		
		//Expected Start Date
        var startDate = cmp.get("v.reqStartDate");
        eventFields.Start_Date__c = startDate ; 
		
		//Expected Interview Date
        var interviewDate = cmp.get("v.reqInterviewDate");
        eventFields.Req_Interview_Date__c = interviewDate ;       
       
        //Additional Qualifications
        var quali = cmp.get("v.addQualification");
		
        eventFields.Req_Qualification__c = quali ;      
            
        
		//ExtDesc
        var descExt = cmp.get("v.externalCommJobDescriptionText");
        eventFields.Req_External_Job_Description__c = descExt ;  
		
        
        if(cmp.get("v.req_stage")) {
            //Adding w.r.t REDZONE
            var qualifyingStage=cmp.get("v.req_stage");
            if(qualifyingStage == "RedZone"){
                eventFields.Req_Red_Zone__c=true;
                if(cmp.get("v.initialStageName") == "Draft")
                	eventFields.StageName="Qualified";                
                if(cmp.get("v.initialStageName") == "Qualified")
                	eventFields.Req_Prev_Stage_Name_c__c=cmp.get("v.initialStageName");
            }else{
                eventFields.Req_Red_Zone__c=false;
                if(cmp.get("v.initialStageName") == "Draft" || cmp.get("v.initialStageName") == "Qualified"){
                    eventFields.StageName = cmp.get("v.req_stage");
                    eventFields.Req_Prev_Stage_Name_c__c=cmp.get("v.initialStageName");
                }
            }            
        }
                       
        if(cmp.find("experienceLevelId").get("v.value") != null && typeof cmp.find("experienceLevelId").get("v.value") != undefined) {
            eventFields.Req_Job_Level__c = cmp.find("experienceLevelId").get("v.value");
        }        
        eventFields.Req_Skill_Specialty__c = cmp.get("v.req.Req_Skill_Specialty__c");
        eventFields.Product__c = cmp.get("v.req.Product__c")=='--None--'?'':cmp.get("v.req.Product__c");
        eventFields.Functional_Non_Functional__c = cmp.get("v.req.Functional_Non_Functional__c");
        if(cmp.get("v.SSApiModel") != null && cmp.get("v.SSApiModel") != "" && typeof cmp.get("v.SSApiModel") != undefined)
            eventFields.Req_RVT_Occupation_Code__c = cmp.get("v.SSApiModel").RVT.Occupation_Code;
        eventFields.Req_ONet_SOC_Description__c = cmp.get("v.req.Req_ONet_SOC_Description__c");
        eventFields.Soc_onet__c = cmp.get("v.req.Soc_onet__c");
        eventFields.Req_RVT_Premium_Skills__c = cmp.get("v.req.Req_RVT_Premium_Skills__c");
        eventFields.Skill_Set__c = cmp.get("v.req.Skill_Set__c");
        //S-234531 end
        //Adding w.r.t Story S-234527
        eventFields.Req_Manually_Updated_Fields__c = cmp.get("v.req.Req_Manually_Updated_Fields__c");
        //End of Adding w.r.t Story S-234527


		eventFields.OwnerId = eventFields.Req_Opportunity_Owner__c; 
		var jobtitle = cmp.get("v.jobtitle[0].name");
        if(typeof(jobtitle) != "undefined" && jobtitle !=null)
        eventFields.Req_Job_Title__c = jobtitle;       
        cmp.set("v.descriptionVarName", "");
		cmp.set("v.OpptyOwnerVarName","");
		cmp.set("v.flatFeeValMsg", "");
        eventFields.AccountId=cmp.find("acctId").get("v.value");
		//eventFields.AccountId=cmp.find("acctId").get("v.value");
        var stgName = cmp.find("tekdetailStage").get("v.value");
        cmp.set("v.StageName",stgName);
        if(stgName == "RedZone"){
            stgName="Qualified";
        }
        var placeType = cmp.find("productId").get("v.value");
        if( placeType == "Permanent"){
            //Remove value from CTH and Contract fields
            eventFields.Req_Duration__c = null ;
            eventFields.Req_Duration_Unit__c = "" ;
            eventFields.Req_Bill_Rate_Min__c = null ;
            eventFields.Req_Bill_Rate_Max__c = null ;
            eventFields.Req_Pay_Rate_Min__c = null ;
            eventFields.Req_Pay_Rate_Max__c = null ;
            eventFields.Req_Standard_Burden__c = null ; 
            eventFields.OT_Multiplier__c = "" ;
        } else {
            eventFields.Req_Bonus__c = null ;
            eventFields.Req_Fee_Percent__c = null ;
            eventFields.Req_Salary_Min__c = null ;
            eventFields.Req_Salary_Max__c = null ;
            eventFields.Req_Other_Compensations__c = "" ;
            eventFields.Req_Flat_Fee__c = null ;
        }
        cmp.set("v.placementType",placeType);        
        var reqValidator = cmp.find('validator');
        //var validations = reqValidator.validateRequiredFields(cmp.find("tekdetailStage").get("v.value"),cmp.find("productId").get("v.value"),cmp.find("isTGSReqId").get("v.value"));
        var validations = reqValidator.validateRequiredFields(stgName,cmp.find("productId").get("v.value"),'No');
        //var validations = {'isvalid': true , 'error_count':0};
        cmp.set("v.validationResponse", validations );
        var validatedcheck = cmp.get("v.validationResponse.isvalid");
        //var validatedcheck = true ;
        if(validatedcheck == false){
            validated = validatedcheck;
            
            if(validated == false){
                error_count = cmp.get("v.validationResponse.error_count");
                if(typeof(cmp.get("v.focus_id")) != "undefined" && cmp.get("v.focus_id") !=null){
                	document.getElementById( cmp.get("v.focus_id") ).scrollIntoView({ behavior: 'instant', block: 'center' });
                    cmp.set("v.focus_id",undefined);
                	//error_count = cmp.get("v.validationResponse.error_count");
                }
                /*else if(typeof(cmp.find("tgsId")) != "undefined" && cmp.find("tgsId") !=null){
                    if(typeof(cmp.find("tgsId").get("v.focus_id")) != "undefined" && cmp.find("tgsId").get("v.focus_id") !=null)
					document.getElementById( cmp.find("tgsId").get("v.focus_id") ).scrollIntoView({ behavior: 'instant', block: 'center' });
                	//error_count = cmp.get("v.validationResponse.error_count");
                }*/
                else if(typeof(cmp.find("prodId")) != "undefined" && cmp.find("prodId") !=null){
                    if(typeof(cmp.find("prodId").get("v.focus_id")) != "undefined" && cmp.find("prodId").get("v.focus_id") !=null)
					document.getElementById( cmp.find("prodId").get("v.focus_id") ).scrollIntoView({ behavior: 'instant', block: 'center' });
                	//error_count = cmp.get("v.validationResponse.error_count");
                }           	
            }
        }else if(validatedcheck == true){
            error_count = 0;
            var jobDescription=cmp.find("jobDescription").get("v.value");
            if(cmp.get("v.StageName") != 'Draft' && typeof(jobDescription) !="undefined" && jobDescription !=null && jobDescription !='' && jobDescription.length < 25){
                cmp.set("v.descriptionVarName", "Job Description should have at least 25 characters.");
                validated = false;
                error_count++;
                if(error_count == 1){
                	document.getElementById('label-description').scrollIntoView({ behavior: 'instant', block: 'center' });
            	}
            }
            var OpptyOwner = cmp.find("emeaOwneridE").get("v.value");
            if(typeof(OpptyOwner) != "undefined" && (OpptyOwner == null || OpptyOwner == '')){
                cmp.set("v.OpptyOwnerVarName","Opportunity Owner is required");
                validated = false;
                error_count++;
            }
			if((cmp.get("v.StageName") =='Qualified' || cmp.get("v.StageName") =='RedZone')){                
                if(cmp.get("v.placementType") == 'Permanent'){
                    var percentaFeeValue = cmp.find("inputFeePerc").get("v.value");
                	var flatFeeValue = cmp.find("inputFlatFee").get("v.value");
                    if((typeof percentaFeeValue == "undefined" || percentaFeeValue == null || percentaFeeValue == "") && (typeof flatFeeValue == "undefined" || flatFeeValue == null || flatFeeValue == "")){
                    	cmp.set("v.flatFeeValMsg", "Fee % or Flat Fee is required.");
                    	validated=false;
                    	error_count++;
                    	if(error_count == 1){
                			document.getElementById('label-flatfee').scrollIntoView({ behavior: 'instant', block: 'center' });
            			}
                	}else if((typeof flatFeeValue == "undefined" || flatFeeValue == null || flatFeeValue == "") && (typeof percentaFeeValue == "undefined" || percentaFeeValue == null || percentaFeeValue == "")){
                    	cmp.set("v.flatFeeValMsg", "Fee % or Flat Fee is required.");
                    	validated=false;
                    	error_count++;
                    	if(error_count == 1){
                			document.getElementById('label-flatfee').scrollIntoView({ behavior: 'instant', block: 'center' });
            			}
                	}
                }                
                if(error_count == 0){
                    let tmp_error =helper.validatePlacementTypeFields(cmp, event, helper, validated, eventFields);
                    if(tmp_error > 0) {
                        validated = false;
                        error_count = error_count + tmp_error;
                        document.getElementById('label-productId').scrollIntoView({ behavior: 'instant', block: 'center' });
                    }
                }                
            }        
        }
        event.setParam("fields", eventFields);
        
        if(validated){
           cmp.set("v.insertFlag",true);
           cmp.set("v.addinsertFlag",false);
            cmp.set("v.viewLoaded",false);
           cmp.find('emeaEditForm').submit(eventFields); 
            //cmp.set("v.skillRefreshFlag",false);
            //cmp.set("v.jobTitleRefreshFlag",false);
        }else{
            Validation = true;
            //console.log('count '+fieldMessages.length);
            if(error_count > 0){
                var stgName=cmp.get("v.StageName");
                if(stgName != "Draft"){
                    stgName="Qualified/RedZone";
                }
                //var message = 'There are ' + error_count + ' errors that need to be addressed before creating this req in a ' + cmp.get("v.StageName") + ' state.';
                var message = 'There are ' + error_count + ' errors that need to be addressed before creating this req in a ' + stgName + ' state.';
                helper.showToast(message, 'Req Validation Error', 'error');
            }
            cmp.set("v.editDisable",false);
            //cmp.set("v.validationMessagesList",fieldMessages); 
            if(typeof(error_count) !="undefined" && error_count == 0)
            cmp.set("v.Validation",Validation);
        }
        cmp.set("v.showSpinner", false); 
    },
    handleOpportunitySaved :function(cmp, event,helper){
        
    	var params = event.getParams();
 		
        cmp.set("v.validationMessagesList",null);
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true); 
        cmp.set("v.cancelFlag",true);
        
        var positionRefereshEvt = $A.get('e.c:E_RefreshPositionCard');
        positionRefereshEvt.fire();
   
    },
    preventProcessing : function (cmp,event,helper){
        event.preventDefault();
        event.stopPropagation();
        //console.log('Prevent method.....');
    },
    onAccountChange :function (cmp,event,helper){
       var acId=event.getParams().value;
       cmp.find("acctId").set("v.value",acId);
    },
    onProductChange :  function (cmp,event,helper){
      var eventFields = event.getParams();
			 var prodval=eventFields.value;
         if(prodval=='Permanent' ){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",false);
             cmp.set("v.contractFlag",false);
         }else if(prodval=='Contract to Hire'){
             cmp.set("v.permFlag",false);
              cmp.set("v.cthFlag",true);
             cmp.set("v.contractFlag",false);
         }else{
             cmp.set("v.cthFlag",false);
              cmp.set("v.permFlag",false);
             cmp.set("v.contractFlag",true);
			 
         }
		cmp.set("v.placementType",prodval);
        var stgName = cmp.find("tekdetailStage").get("v.value");
		cmp.set("v.StageName",stgName);
        if(stgName == "RedZone"){
            stgName="Qualified";
        }
		var validatefalg = cmp.find("validator");
//validatefalg.validateRequiredEditFields(cmp.find("tekdetailStage").get("v.value"),cmp.find("productId").get("v.value"),cmp.find("isTGSReqId").get("v.value"));
      validatefalg.validateRequiredEditFields(stgName,cmp.find("productId").get("v.value"),'No');
    },
    handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');
        
		//alert(openSections.length);
        cmp.set("v.activeSections",openSections);
        
        if (openSections.length === 0) {
            //cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            //cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    navigateToError: function(component, event, helper){
        var elmnt = document.getElementById("editpageid");
        window.scrollTo(0,200);
	}, 
    enterHandler: function(component, event, helper) {
     component.set("v.lookupJTFlag",!component.get("v.lookupJTFlag"));
     component.set("v.loopFlag",true);
    },
    changeJobTitle :function (component, event, helper){
        
        var globalLovId=event.getParams().value;
        var tempRec = component.find("recordLoader");
        tempRec.set("v.recordId",globalLovId[0]);
        tempRec.reloadRecord();
    },
    recordUpdated:function (component, event, helper){
        helper.recordUpdated(component, event, helper);
    },
    buttonHandleCancel : function (cmp,event,helper){
		cmp.set("v.validationMessagesList",null);
		cmp.set("v.stageErrorVarName","");
		cmp.set("v.jobTitleErrorVarName","");
		cmp.set("v.hiringManagerErrorVarName","");
		cmp.set("v.accountErrorVarName","");
		cmp.set("v.draftReasonVarName","");
		cmp.set("v.officeErrorVarName","");
		cmp.set("v.plmtTypeErrorVarName","");
		cmp.set("v.streetErrorVarName","");
		cmp.set("v.cityTypeErrorVarName","");
		cmp.set("v.stateErrorVarName","");
		cmp.set("v.countryVarName","");
		cmp.set("v.zipErrorVarName","");
		cmp.set("v.currencyErrorVarName","");
		cmp.set("v.skillPillValidationError","");
        cmp.set("v.totalPositionsVarName","");
		cmp.set("v.Validation",false);
        cmp.set("v.editDisable",false);
        cmp.set("v.cancelJTFlag",true);
        cmp.set("v.addFieldMessage",null);
        cmp.set("v.addFieldMessagedMap",null);
		cmp.set("v.descriptionVarName", "");
        cmp.set("v.OpptyOwnerVarName","");
		cmp.set("v.flatFeeValMsg", "");
        helper.onCancelSetFieldAttribute(cmp,event);
        helper.setDefaultAddressData(cmp,event,helper);
		cmp.set("v.stringSkills","");
        cmp.set("v.skills","");
        //cmp.set("v.jobtitle",null);
        cmp.set("v.finalRelatedTitlesList",[]);
        cmp.set("v.isUpdatedRelatedTitles",false);
		cmp.set("v.updatedSuggestedJobTitles",null);
        
		//var topSkillCancel = cmp.get("v.topSkillsOnCancel");
		//var secondarySkillCancel = cmp.get("v.secondarySkillsOnCancel");
		//cmp.set('v.topSkills', topSkillCancel);		
        //cmp.set('v.secondarySkills', secondarySkillCancel);       
		var reqValidator = cmp.find('validator');
        var validations = reqValidator.clearRequiredFieldsOnCancel();
		if(cmp.find("productId") !="undefined"){
            if(cmp.get("v.placementtypeLoadValue") == 'Contract'){
                cmp.set("v.contractFlag",true);
                cmp.set("v.cthFlag",false);
                cmp.set("v.permFlag",false);
            }else if(cmp.get("v.placementtypeLoadValue") == 'Permanent'){
                cmp.set("v.permFlag",true);
                cmp.set("v.cthFlag",false);
                cmp.set("v.contractFlag",false);
            }else if(cmp.get("v.placementtypeLoadValue") == 'Contract to Hire'){
                cmp.set("v.cthFlag",true);
                cmp.set("v.permFlag",false);
                cmp.set("v.contractFlag",false);
            }
        }        
        cmp.set("v.viewLoaded",false);
        cmp.set("v.editFlag",false);
		cmp.set("v.viewFlag",true);  
		cmp.set("v.cancelFlag",true);
		event.preventDefault();
    },
    changeJobTitleHandler :function(cmp,event,helper){
        let payload = cmp.get("v.payload");
        let jbTitle=cmp.get("v.jobtitle");
        if(jbTitle!=null && jbTitle.length>0){
			let jobtitle = jbTitle[0].name;
            if(payload != null) {
                payload["req_job_title__c"] =  jobtitle;
            } else {
                payload = {};
                payload["req_job_title__c"] =  jobtitle;
            }            
            cmp.set("v.payload", payload);
        	cmp.set("v.suggestedjobTitle",jobtitle);
			if(!cmp.get("v.viewFlag")) {
				helper.fetchRelatedJobTitles(cmp,event,jobtitle);
			}
        }
        helper.skillSpecialtyClassifierCall(cmp, event);    
    },
    
    showEditModal : function(component, event, helper) {
        var appEvent = $A.get('e.c:E_PositionModal');
        appEvent.fire();
    },
    
     refereshPositionData : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    handleEditClick : function(component, event, helper){
        
       //alert('Hii'+event.getSource().get("v.name"));
       var buttonClick = event.getSource().get("v.name");
       component.set('v.clickedSection', buttonClick);
	   component.set("v.activeSections",[]);
            var sectionVar = ['A','B','C','D','E','F','G','H'];
			component.set("v.activeSections",sectionVar);

       //console.log(component.get("v.activeSections"));
       
       let editFormWidth = component.find("emeaFormContainer").getElement().clientWidth;
       component.set("v.containerWidth", editFormWidth);
       component.set("v.editFlag",true);
       component.set("v.viewFlag",false);
       component.set("v.editDisable",false);
       helper.skillSpecialtyClassifierCall(component, event);
          setTimeout(
                $A.getCallback(function() {
                    if(component.find("tekdetailStage") != undefined){	
                    	var stgName=component.find("tekdetailStage").get("v.value");
                    
                        
                    if(stgName == "RedZone"){
           				stgName="Qualified";
        			}
                    var validatefalg = component.find("validator");					
                    validatefalg.validateRequiredEditFields(stgName,component.find("productId").get("v.value"),'No'); 
                    }    
                    //var activeStatusVar = component.get("v.activeSections");
                    //activeStatusVar.includes('A');
                    if(buttonClick == 'A'){
                        var accountInfoeditVar = document.getElementById("accountInfoEdit")
                        if(accountInfoeditVar != null){
                            var yPosition = document.getElementById("accountInfoEdit").offsetTop;
                            window.scrollTo(0,yPosition-50);
                            component.set("v.isFocus1", true);
    
                        }
                        
                    }else if(buttonClick == 'B'){
                        var overviewCriteriaEditVar = document.getElementById("overviewCriteriaEdit")
                        if(overviewCriteriaEditVar != null){
                            var yPosition = document.getElementById("overviewCriteriaEdit").offsetTop;
                            window.scrollTo(0,yPosition-50);
                            component.set("v.isFocus2", true);
                        }
                       
                    }else if(buttonClick == 'C'){
                        var positionSummaryEditVar = document.getElementById("positionSummaryEdit");
                        if(positionSummaryEditVar != null){
                            var yPosition = document.getElementById("positionSummaryEdit").offsetTop;
                            window.scrollTo(0,yPosition-50);
                            component.set("v.isFocus3", true);
                         }
                        
                    }else if(buttonClick == 'D'){
                        var businessQualificationEditVar = document.getElementById("businessQualificationEdit");
                        if(businessQualificationEditVar != null){
                            var yPosition = document.getElementById("businessQualificationEdit").offsetTop;
                            window.scrollTo(0,yPosition-50);
							component.set("v.isFocus4", true);
                        }
                        
                    }else if(buttonClick == 'E'){
                        var systemInfoEditVar = document.getElementById("systemInfoEdit");
                        if(systemInfoEditVar != null){
                            var yPosition = document.getElementById("systemInfoEdit").offsetTop;
                            window.scrollTo(0,yPosition);
                            component.set("v.isFocus5", true); 
                        }
                    }else if(buttonClick == 'G'){
                        var deliveryEditVar = document.getElementById("deliveryEdit");
                        if(deliveryEditVar != null){
                            var yPosition = document.getElementById("deliveryEdit").offsetTop;
                            window.scrollTo(0,yPosition);
                            component.set("v.isFocus6", true); 
                        }
                    }
					else if(buttonClick == 'H'){
                        var vmsEditVar = document.getElementById("vmsEdit");
                        if(vmsEditVar != null){
                            var yPosition = document.getElementById("vmsEdit").offsetTop;
                            window.scrollTo(0,yPosition);
                            component.set("v.isFocus7", true); 
                        }
                    }else if(buttonClick == 'F'){
                        var overviewEditVar = document.getElementById("overviewInfoEdit")
                        if(overviewEditVar != null){
                            var yPosition = document.getElementById("overviewInfoEdit").offsetTop;
                            window.scrollTo(0,yPosition+50);
                            component.set("v.isFocus8", true);
                        }
                       
                    }
					var opts;                      
                    if(component.get("v.statusLDS") == 'Open'){                        
                        if(component.get("v.StageName") == 'Draft'){
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                            ];
                        }  
                        else{
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                            ];
                        }
                    }
                    //Closed Req
                    if(component.get("v.statusLDS") == 'Closed'){          
                        if(component.get("v.prevStage") =='Draft' && component.get("v.StageName").substring(0,6) == 'Closed'){
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                            ];
                        }
                        else if(component.get("v.prevStage") == 'Staging' && component.get("v.StageName").substring(0,6)=='Closed'){
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" },
                                { "class": "slds-input slds-combobox__input ", label: "Staging", value: "Staging" ,selected: "true"}
                            ];
                        }                
                            else{
                                opts = [
                                    { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                                    { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                                ];
                            }
                    }                    
                    if(component.find("tekdetailStage") != undefined){
                        var stgVw = component.find("tekdetailStage");
                        var flag=Array.isArray(stgVw);
                        if(flag){
                            //console.log("stageV array");
                            stgVw[0].set("v.options", opts);
                        }else{
                            //console.log("not array");
                            //component.find("tekdetailStage").set("v.options", opts);
                            //component.find("tekdetailStage").set("v.value", stgVw.get("v.value"));
                        } 
                    }                  
                }), 4000
            );
        	
            setTimeout(
                $A.getCallback(function() {
                    component.set('v.isFocus1', false);
                    component.set('v.isFocus2', false);
                    component.set('v.isFocus3', false);
                    component.set('v.isFocus4', false);
                    component.set('v.isFocus5', false);  
                    component.set('v.isFocus8', false);  
                }),20000
            );
        
    },
     //Call by aura:waiting event  
    handleShowSpinner: function(component, event, helper) {
        component.set("v.showSpinner", true); 
    },
    
    //Call by aura:doneWaiting event 
    handleHideSpinner : function(component,event,helper){
        component.set("v.showSpinner", false);
    },
     onStageChange : function(component,event,helper){
		var stgName = component.find("tekdetailStage").get("v.value");
		component.set("v.StageName",stgName);
         if(stgName == "RedZone"){
             stgName="Qualified";
         }
		var placeType = component.find("productId").get("v.value");
		component.set("v.placementType",placeType);
		var validatefalg = component.find("validator");         
		//validatefalg.validateRequiredEditFields(component.find("tekdetailStage").get("v.value"),component.find("productId").get("v.value"),component.find("isTGSReqId").get("v.value"));
     	validatefalg.validateRequiredEditFields(stgName,component.find("productId").get("v.value"),'No');
	},
    handleTopSkillsChange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.topSkills', data.skills)
      const secSkills = cmp.get('v.secondarySkills');
      const dupes = data.skills.filter(skill => secSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.topSkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
      cmp.set('v.topSkills', data.skills)
      h.constructFinalSkills(cmp);          
    },
    handleSecSkillsChange: function(cmp, e, h) {
        const data = e.getParams();
        const topSkills = cmp.get('v.topSkills');
        const dupes = data.skills.filter(skill => topSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                const suggestedSkills = cmp.get('v.simpleSuggestedSkills')
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.secondarySkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
        cmp.set('v.secondarySkills', data.skills)
        h.constructFinalSkills(cmp)
    },
    handlesuggestedskillschange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.simpleSuggestedSkills', data.pills);
    },/*
    setSuggestedSkills: function(cmp, e, h) {
        const suggestedSkills = cmp.get('v.suggestedSkills');
        if (suggestedSkills) {
            const suggestedSkillsList = suggestedSkills.map(skill => skill.name);
            cmp.set('v.simpleSuggestedSkills', suggestedSkillsList);
        }
    },*/
        setSuggestedSkills: function(cmp, e, h) {
        const suggestedSkills = cmp.get('v.initialSuggestedSkills');
        if (suggestedSkills) {
            cmp.set('v.simpleSuggestedSkills', suggestedSkills);
        }
    },
    handleDupeMsg: function(cmp, e, h) {
        h.showToast(`${e.getParams().val} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')        
    },
    //Adding w.r.t S-179672 Page Refresh
    destroyComp: function(component, event, helper) {        
    	component.destroy();          
    },
	handleSuggestedTitlesChange:function(cmp,event,helper) {
		
		const data = event.getParams();
		//const data = event.getParam("skills");
		cmp.set("v.isUpdatedRelatedTitles",true);
		cmp.set("v.updatedSuggestedJobTitles",data.skills);
		//call suggested skills api
		if(!$A.util.isUndefined(data.skills) && !$A.util.isEmpty(data.skills)) {
			cmp.set("v.suggTitlesForSkills",data.skills.join());
		}
	},
	relatedTitleHandler: function(cmp,event,helper) {
		console.log('suggJobTitles'+cmp.get('v.suggJobTitles'));
		let titles = cmp.get('v.suggJobTitles')
		if(titles != []) {
			
			cmp.set("v.suggTitlesForSkills",titles.join());
			
		}
	},
        setSkills : function(cmp,event,helper) {
        	const allSkills = cmp.get('v.skills');
        	cmp.set('v.allSelectedSkills', allSkills);
    },
        onTGSChange:function(component,event,helper) { 
            var TGSReqValue = component.find("isTGSReqId").get("v.value");                        
            var OpCoValue = component.get("v.req.OpCo__c");
            component.set("v.req.Req_TGS_Requirement__c",TGSReqValue);
            component.set("v.placementType",component.find("productId").get("v.value"));
        if(OpCoValue == 'TEKsystems, Inc.' && TGSReqValue == 'Yes'){
            component.set("v.IsTGSReq", true);
            var actSectsList = component.get("v.activeSections");
            
            if(actSectsList != undefined){
                 actSectsList.push("F");                              
                             setTimeout($A.getCallback(
                                    () => component.set("v.activeSections", actSectsList)
                                ));
                         }
            helper.setRequiredFieldsOnTGSChange(component,event);
        }else if(OpCoValue == 'TEKsystems, Inc.' && (TGSReqValue == 'No' || TGSReqValue == '--None--')){			
			component.set("v.req.Practice_Engagement__c","--None--");
            component.set("v.req.GlobalServices_Location__c","--None--");
            component.set("v.req.Employment_Alignment__c","--None--");
            component.set("v.req.Final_Decision_Maker__c","--None--");
            component.set("v.req.Is_International_Travel_Required__c","--None--");
            component.set("v.req.National__c", false);
            component.set("v.req.Backfill__c", false);
            component.set("v.req.Internal_Hire__c","--None--");
			component.set("v.isTGSPEName","");
            component.set("v.isTGSLocationName","");
            component.set("v.isTGSEmpAlignName","");
            component.set("v.isFinaDecMakName","");
            component.set("v.isIntNationalName","");
            component.set("v.isInternalName","");
            component.set("v.isTGSTrue", "false");
            component.set("v.IsTGSReq", false); 
            helper.setRequiredFieldsOnTGSChange(component,event);
        }
        
            
        var tgsReqWorksiteLoc 	= component.find('locationId');
    	tgsReqWorksiteLoc.TGSReqForTek();
        },
	onJobDescChange : function(component, event, helper){
		//var jobTitle = component.find("jobtitleid").get("v.value");
        if(component.get("v.editJobdescription")) {
            var jobDescTrimmed = component.get("v.editJobdescription").trim();		
            component.find("jobDescription").set("v.value", jobDescTrimmed);   
            let payload = component.get("v.payload");
            if(payload != null) {
                payload["req_job_description__c"] = jobDescTrimmed;
            } else {
                payload = {};
                payload["req_job_description__c"] = jobDescTrimmed;
            }            
            component.set("v.payload, payload");              
            helper.skillSpecialtyClassifierCall(component, event);        
        }
    },        
    onAdditionalQualChange : function(component, event, helper){
        if(component.get("v.addQualification"))  {
            var addnQualTrimmed = component.get("v.addQualification").trim();		
            component.find("qualificationId").set("v.value", addnQualTrimmed);            
        }
    },            
    extCommunitiesChange : function(component, event, helper){
        if(component.get("v.externalCommJobDescriptionText")) {
            var extCommunitiesTrimmed = component.get("v.externalCommJobDescriptionText").trim();		
            component.find("extCommunitiesId").set("v.value", extCommunitiesTrimmed);
        }
    },
    EXPblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Req_Job_Level__c");        
    },
    //Adding w.r.t S-238087 O-Number on Manual Req From VMS Req at merge
    navigateToVMSReq: function(component, event, helper) {
        let mergedReqId = component.get("v.mergedReqId");
        if(mergedReqId != "" && mergedReqId != null && typeof mergedReqId != undefined) {
            var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
            var dURL;                
            dURL = ligtningNewURL+"/r/Opportunity/" + mergedReqId +"/view";
		
            window.open(dURL, '_blank');            
        }
    }
    //End of Adding w.r.t S-238087 O-Number on Manual Req From VMS Req at merge
})