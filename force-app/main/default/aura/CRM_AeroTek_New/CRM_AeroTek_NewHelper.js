({
    SHOWMORE_FIELDS: [
        'Req_Job_Description__c',
        'Req_Qualification__c',
        'Req_External_Job_Description__c',
        'Req_Business_Challenge__c',
        'Req_EVP__c',
        'Req_Interview_Information__c',
        'Req_RemoteWork_Education_Info__c',
        'Req_Additional_Information__c',
        'Req_Approval_Process__c',
        'Req_Project_Stage_Lifecycle_Info__c',
        'Req_Competition_Info__c',
        'Req_Compliance__c',
        'Req_Impact_to_Internal_External_Customer__c',
        'Req_Internal_External_Customer__c',
        'Req_Work_Environment__c',
        'Req_Skill_Details__c',
        'Req_Performance_Expectations__c'
    ],
    prepareSkills: function(cmp, event) {
        var skillsList = cmp.get("v.skills");
        var finalSkillList = [];
        var i;
        for (i = 0; i < skillsList.length; i++) {
            var skillObj = skillsList[i];
            var newSkill = {
                "name": skillObj.name,
                "favorite": skillObj.favorite
            };
            finalSkillList.push(newSkill);
        }
        cmp.set("v.finalskillList", finalSkillList);
    },
    recordUpdated: function(component, event, helper) {

        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {
            /* handle error; do this first! */ } else if (changeType === "LOADED") {
            var sRec = component.get("v.simpleRecord");
            component.set("v.lookupJobTitle", sRec.Name);

        }

    },
    validateAddress: function(cmp, event) {
        var oppAddress = cmp.get("v.Opportunity");
        var fieldMessages = [];
        var addFielMessagedMap = {};


        if (typeof fieldMessages != 'undefined' && fieldMessages != null && fieldMessages.length > 0) {
            cmp.set("v.insertFlag", true);
            cmp.set("v.addFieldMessage", fieldMessages);
            cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
            var testmap = cmp.get("v.addFieldMessagedMap");
            console.log('Field Map--->' + testmap);
        } else {
            cmp.set("v.insertFlag", false);
            cmp.set("v.addFieldMessage", fieldMessages);
            cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
        }

    },
    validateReqAddr: function(cmp, event, helper, eventFields) {
        var oppAddress = cmp.get("v.req");
        var addFielMessagedMap = {};
        var TGSReqValue = cmp.get("v.req.Req_TGS_Requirement__c");
        if (TGSReqValue == 'No' || TGSReqValue == '--None--') {
            if (oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == '') {
                addFielMessagedMap['streetErrorVarName'] = "Street is required.";
            } else {
                cmp.set("v.streetErrorVarName", "");
            }
            if (oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == '') {
                addFielMessagedMap['cityTypeErrorVarName'] = "City is required.";
            } else {
                cmp.set("v.cityTypeErrorVarName", "");
            }
            if (oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == '') {
                addFielMessagedMap['countryVarName'] = "Country is required.";
            } else {
                cmp.set("v.countryVarName", "");
            }
            //Adding w.r.t Story S-179373
            if ((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && (oppAddress.Req_Worksite_Country__c == 'United States' || oppAddress.Req_Worksite_Country__c == 'Canada')) {
                //if(oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') {
                addFielMessagedMap['stateErrorVarName'] = "State is required.";
            } else {
                cmp.set("v.stateErrorVarName", "");
            }
            if (oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == '') {
                addFielMessagedMap['zipErrorVarName'] = "Postal Code is required.";
            } else {
                cmp.set("v.zipErrorVarName", "");
            }
            if (typeof addFielMessagedMap != 'undefined' && addFielMessagedMap != null && Object.keys(addFielMessagedMap).length > 0) {
                cmp.set("v.insertFlag", true);
                addFielMessagedMap['locationErrorVarName'] = "Worksite Location not found you need to add it manually or lookup another location.";
                cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
                var testmap = cmp.get("v.addFieldMessagedMap");
                return eventFields;
            } else {
                cmp.set("v.insertFlag", false);
                cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
                eventFields = this.saveAddressData(cmp, event, helper, eventFields);
                return eventFields;
            }
        } else {
            cmp.set("v.insertFlag", false);
            cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
            eventFields = this.saveAddressData(cmp, event, helper, eventFields);
            return eventFields;
        }
    },
    saveAddressData: function(cmp, event, helper, eventFields) {
        eventFields.Req_Worksite_Street__c = cmp.get("v.req.Req_Worksite_Street__c");
        eventFields.Req_Worksite_City__c = cmp.get("v.req.Req_Worksite_City__c");
        eventFields.Req_Worksite_State__c = cmp.get("v.req.Req_Worksite_State__c");
        eventFields.Req_Worksite_Postal_Code__c = cmp.get("v.req.Req_Worksite_Postal_Code__c");
        eventFields.Req_Worksite_Country__c = cmp.get("v.req.Req_Worksite_Country__c");
        eventFields.StreetAddress2__c = cmp.get("v.req.StreetAddress2__c");
        //for default address data
        cmp.set("v.reqDefaultAddress.Req_Worksite_Street__c", cmp.get("v.req.Req_Worksite_Street__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_City__c", cmp.get("v.req.Req_Worksite_City__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_State__c", cmp.get("v.req.Req_Worksite_State__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_Postal_Code__c", cmp.get("v.req.Req_Worksite_Postal_Code__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_Country__c", cmp.get("v.req.Req_Worksite_Country__c"));
        cmp.set("v.reqDefaultAddress.StreetAddress2__c", cmp.get("v.req.StreetAddress2__c"));
        return eventFields;
    },
    getAddressData: function(cmp, event, helper) {
        cmp.set("v.req.Req_Worksite_Street__c", event.getParams().recordUi.record.fields.Req_Worksite_Street__c.value);
        cmp.set("v.req.Req_Worksite_City__c", event.getParams().recordUi.record.fields.Req_Worksite_City__c.value);
        cmp.set("v.req.Req_Worksite_State__c", event.getParams().recordUi.record.fields.Req_Worksite_State__c.value);
        cmp.set("v.req.Req_Worksite_Postal_Code__c", event.getParams().recordUi.record.fields.Req_Worksite_Postal_Code__c.value);
        cmp.set("v.req.Req_Worksite_Country__c", event.getParams().recordUi.record.fields.Req_Worksite_Country__c.value);
        //for default address data        
        cmp.set("v.reqDefaultAddress.Req_Worksite_Street__c", event.getParams().recordUi.record.fields.Req_Worksite_Street__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_City__c", event.getParams().recordUi.record.fields.Req_Worksite_City__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_State__c", event.getParams().recordUi.record.fields.Req_Worksite_State__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_Postal_Code__c", event.getParams().recordUi.record.fields.Req_Worksite_Postal_Code__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_Country__c", event.getParams().recordUi.record.fields.Req_Worksite_Country__c.value);

        if (event.getParams().recordUi.record.fields.StreetAddress2__c.value != undefined) {
            cmp.set("v.req.StreetAddress2__c", event.getParams().recordUi.record.fields.StreetAddress2__c.value);
            cmp.set("v.reqDefaultAddress.StreetAddress2__c", event.getParams().recordUi.record.fields.StreetAddress2__c.value);
        }
        this.displayLocation(cmp, event, helper);
    },
    setDefaultAddressData: function(cmp, event, helper) {
        cmp.set("v.req.Req_Worksite_Street__c", cmp.get("v.reqDefaultAddress.Req_Worksite_Street__c"));
        cmp.set("v.req.Req_Worksite_City__c", cmp.get("v.reqDefaultAddress.Req_Worksite_City__c"));
        cmp.set("v.req.Req_Worksite_State__c", cmp.get("v.reqDefaultAddress.Req_Worksite_State__c"));
        cmp.set("v.req.Req_Worksite_Postal_Code__c", cmp.get("v.reqDefaultAddress.Req_Worksite_Postal_Code__c"));
        cmp.set("v.req.Req_Worksite_Country__c", cmp.get("v.reqDefaultAddress.Req_Worksite_Country__c"));

        if (cmp.get("v.reqDefaultAddress.StreetAddress2__c") != undefined) {
            cmp.set("v.req.StreetAddress2__c", cmp.get("v.reqDefaultAddress.StreetAddress2__c"));
        }
    },
    displayLocation: function(component, event, helper) {
        var req = component.get("v.req");
        var displayAddr = "";
        if (req.Req_Worksite_Street__c != null && req.Req_Worksite_Street__c != "") {
            displayAddr = req.Req_Worksite_Street__c;
        }
        if (req.Req_Worksite_City__c != null && req.Req_Worksite_City__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_City__c : req.Req_Worksite_City__c;
        }
        if (req.Req_Worksite_State__c != null && req.Req_Worksite_State__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_State__c : req.Req_Worksite_State__c;
        }
        if (req.Req_Worksite_Postal_Code__c != null && req.Req_Worksite_Postal_Code__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_Postal_Code__c : req.Req_Worksite_Postal_Code__c;
        }
        if (req.Req_Worksite_Country__c != null && req.Req_Worksite_Country__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_Country__c : req.Req_Worksite_Country__c;
        }
        component.set("v.presetLocation", displayAddr);
    },
    constructFinalSkills: function(cmp) {
        const topSkills = cmp.get('v.topSkills');
        const topSuggestedSkills = cmp.find('crmSkillV').getElement().inUseSuggestedSkills; //lwcReqSkills stores suggested skills in this variable
        const formattedTopSkills = topSkills.map((skill, i) => { //iterating over top skills and checking against suggested skills to mark them true
            const normalizedSuggested = topSuggestedSkills.map(s => s.toLowerCase());
            return {
                name: skill,
                favorite: true,
                index: i,
                suggestedSkill: normalizedSuggested.includes(skill.toLowerCase())
            }

        })

        const secSkills = cmp.get('v.secondarySkills');
        const secSuggestedSkills = cmp.find('lwcSecSkills').getElement().inUseSuggestedSkills;
        const formattedSecSkills = secSkills.map((skill, i) => {
            const normalizedSuggested = secSuggestedSkills.map(s => s.toLowerCase());
            return {
                name: skill,
                favorite: false,
                index: topSkills.length + i,
                suggestedSkill: normalizedSuggested.includes(skill.toLowerCase())
            }

        })

        const formattedSkills = [...formattedTopSkills, ...formattedSecSkills]
        cmp.set('v.stringSkills', JSON.stringify(formattedSkills));
        //set skills to make sure selected skills are excluded
        cmp.set('v.skills', formattedSkills);
    },
    showToast: function(message, title, toastType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },
    fetchRelatedJobTitles: function(cmp, event, jobTitle) {
        //Related titles
        if (jobTitle != null) {
            var relatedTitles = cmp.find("relatedTitles");
            relatedTitles.fetchRelatedTerms(jobTitle, "AG_EMEA");
        }
    },
    getOpcoSectionData: function(cmp, event, helper) {
        //cmp.set("v.req.Req_Draft_Reason__c",event.getParams().recordUi.record.fields.Req_Draft_Reason__c.value);
        cmp.set("v.req.REQ_Opportunity_driven_mostly_by_LOB__c", event.getParams().recordUi.record.fields.REQ_Opportunity_driven_mostly_by_LOB__c.value);
        //cmp.set("v.req.Req_TGS_Requirement__c",event.getParams().recordUi.record.fields.Req_TGS_Requirement__c.value);
        cmp.set("v.req.Req_Client_working_on_Req__c", event.getParams().recordUi.record.fields.Req_Client_working_on_Req__c.value);
        cmp.set("v.req.Req_Division__c", event.getParams().recordUi.record.fields.Req_Division__c.value);
        cmp.set("v.req.Product__c", event.getParams().recordUi.record.fields.Product__c.value);
        if (cmp.get("v.req.Req_TGS_Requirement__c") == "Yes")
            this.getTGSSectionData(cmp, event, helper);
    },
    getTGSSectionData: function(cmp, event, helper) {
        cmp.set("v.IsTGSReq", true);
        cmp.set("v.req.Practice_Engagement__c", event.getParams().recordUi.record.fields.Practice_Engagement__c.value);
        cmp.set("v.req.Req_Client_working_on_Req__c", event.getParams().recordUi.record.fields.Req_Client_working_on_Req__c.value);
        cmp.set("v.req.Is_International_Travel_Required__c", event.getParams().recordUi.record.fields.Is_International_Travel_Required__c.value);
        cmp.set("v.req.Internal_Hire__c", event.getParams().recordUi.record.fields.Internal_Hire__c.value);
        cmp.set("v.req.Employment_Alignment__c", event.getParams().recordUi.record.fields.Employment_Alignment__c.value);
        cmp.set("v.req.Final_Decision_Maker__c", event.getParams().recordUi.record.fields.Final_Decision_Maker__c.value);
        cmp.set("v.req.GlobalServices_Location__c", event.getParams().recordUi.record.fields.GlobalServices_Location__c.value);
        cmp.set("v.req.National__c", event.getParams().recordUi.record.fields.National__c.value);
        cmp.set("v.req.Backfill__c", event.getParams().recordUi.record.fields.Backfill__c.value);
    },
    setTGSSectionData: function(cmp, event, helper, eventFields) {
        eventFields.Practice_Engagement__c = cmp.get("v.req.Practice_Engagement__c");
        eventFields.Req_Client_working_on_Req__c = cmp.get("v.req.Req_Client_working_on_Req__c");
        eventFields.Is_International_Travel_Required__c = cmp.get("v.req.Is_International_Travel_Required__c");
        eventFields.Internal_Hire__c = cmp.get("v.req.Internal_Hire__c");
        eventFields.Employment_Alignment__c = cmp.get("v.req.Employment_Alignment__c");
        eventFields.Final_Decision_Maker__c = cmp.get("v.req.Final_Decision_Maker__c");
        eventFields.GlobalServices_Location__c = cmp.get("v.req.GlobalServices_Location__c");
        eventFields.National__c = cmp.get("v.req.National__c");
        eventFields.Backfill__c = cmp.get("v.req.Backfill__c");
        cmp.set("v.IsTGSReq", true);
        return eventFields;
    },
    productHierarchySection: function(component, event, helper, eventFields) {
        if (component.get("v.req.Apex_Context__c") != undefined) {
            eventFields.Apex_Context__c = component.get("v.req.Apex_Context__c");
        }
        if (component.get("v.req.Legacy_Product__c") != undefined) {
            eventFields.Legacy_Product__c = component.get("v.req.Legacy_Product__c");
        }
        if (component.get("v.req.RecordTypeId") != undefined) {
            eventFields.RecordTypeId = component.get("v.req.RecordTypeId");
        }
        /*if(component.get("v.req.Req_TGS_Requirement__c")!=undefined){
            eventFields.Req_TGS_Requirement__c= component.get("v.req.Req_TGS_Requirement__c");
        }*/
        if (component.get("v.req.Req_Client_working_on_Req__c") != undefined) {
            eventFields.Req_Client_working_on_Req__c = component.get("v.req.Req_Client_working_on_Req__c");
        }
        if (component.get("v.req.Req_Division__c") != undefined) {
            var division = component.get("v.req.Req_Division__c");
            eventFields.Req_Division__c = division;
        }
        eventFields.Product__c = component.get("v.req.Product__c");
        return eventFields;
    },
    validateProdHierarchy: function(cmp, event, helper) {
        var validated = true;
        var opco = cmp.get("v.req.OpCo__c");
        var businessUnit = cmp.get("v.req.Req_Division__c");
        //var mainskill=cmp.find("MainSkillId");
        var mainskill = cmp.get("v.prod.Skill__c");
        var segVar = cmp.get("v.prod.Segment__c");
        var jobVar = cmp.get("v.prod.Job_Code__c");
        var catVar = cmp.get("v.prod.Category__c");
        var criteriaValidate = true;


        if (opco == '--None--' || typeof opco == "undefined") {
            validated = false;
            cmp.set("v.opcoVarName", "OpCo cannot be blank.");
        } else {
            cmp.set("v.opcoVarName", "");
        }
        if (businessUnit == '--None--' || typeof businessUnit == "undefined") {
            validated = false;
            cmp.set("v.businessUnitVarName", "Division cannot be blank.");
        } else {
            cmp.set("v.businessUnitVarName", "");
        }
        if (opco == 'TEKsystems, Inc.') {
            var productPicklistUnit = cmp.get("v.req.Product__c");
            if (productPicklistUnit == '--None--' || typeof productPicklistUnit == "undefined") {
                validated = false;
                cmp.set("v.ProductPicklistUnitVarName", "Product cannot be blank.");
            } else {
                cmp.set("v.ProductPicklistUnitVarName", "");
            }
        }
        if (validated &&
            (opco == 'TEKsystems, Inc.' || opco == 'Aerotek, Inc') && (
                (businessUnit == '--None--' || typeof businessUnit == "undefined") || (mainskill != null &&
                    (mainskill == '' || mainskill == '--None--' || mainskill == "undefined")) ||
                (segVar != null && (segVar == '' || segVar == '--None--' || segVar == "undefined")) ||
                (jobVar != null && (jobVar == '' || jobVar == '--None--' || jobVar == "undefined")) ||
                (catVar != null && (catVar == '--None--' || catVar == '' || catVar == "undefined"))
            )) {
            console.log('Req_Standard_Burden_Required  validation rule.');
            cmp.set("v.productHireValMsg", "Please complete product hierarchy.");
            criteriaValidate = false;
        } else {
            cmp.set("v.productHireValMsg", "");
            //criteriaValidate=true;
        }
        return (validated && criteriaValidate);
    },
    compliance: function(cmp, event, helper) {
        //Compliance Checkbox            

        var drugTestCheckbox = event.getParams().recordUi.record.fields.Req_Drug_Test_Required__c.value;
        var OFCCPCheckbox = event.getParams().recordUi.record.fields.Req_OFCCP_Required__c.value;
        var backgroundCheckbox = event.getParams().recordUi.record.fields.Req_Background_Check_Required__c.value;
        var governmentCheckbox = event.getParams().recordUi.record.fields.Req_Government__c.value;

        var drugTest = drugTestCheckbox ? 'Drug Test Required; ' : '';
        var OFCCP = OFCCPCheckbox ? 'OFCCP Required; ' : '';
        var background = backgroundCheckbox ? 'Background Check Required; ' : '';
        var government = governmentCheckbox ? 'Government ' : '';

        var complainceVal = drugTest + OFCCP + background + government;
        cmp.set("v.ComplainceVar", complainceVal);
    },

    additionReqInfo: function(cmp, event, helper) {
        var subVendorCheckbox = event.getParams().recordUi.record.fields.Req_Can_Use_Approved_Sub_Vendor__c.value;
        var startCheckbox = event.getParams().recordUi.record.fields.Immediate_Start__c.value;
        var exclusiveCheckbox = event.getParams().recordUi.record.fields.Req_Exclusive__c.value;

        var subvendor = subVendorCheckbox ? 'Can Use Approved Sub-vendor; ' : '';
        var startChk = startCheckbox ? 'Go To Work; ' : '';
        var exclusiveChk = exclusiveCheckbox ? 'Exclusive ' : '';


        var AddnReqInfoVal = subvendor + startChk + exclusiveChk;
        cmp.set("v.AdditionalVar", AddnReqInfoVal);
    },
    removeActiveSection: function(cmp, event, helper) {
        var value = "E";
        var actSectListArr = cmp.get("v.activeSections");
        actSectListArr = actSectListArr.filter(function(item) {
            return item !== value;
        })

        setTimeout($A.getCallback(
            () => cmp.set("v.activeSections", actSectListArr)
        ));

    },

    clientWorkingOnReqHelper: function(cmp, event, helper) {

        var ClientReq;
        if (cmp.get("v.req.Req_Client_working_on_Req__c") == undefined) {
            ClientReq = event.getParams().recordUi.record.fields.Req_Client_working_on_Req__c.value;
            cmp.set("v.req.Req_Client_working_on_Req__c", ClientReq);
        } else {
            ClientReq = cmp.get("v.req.Req_Client_working_on_Req__c");
        }


        var ClientReqArray = [];
        ClientReqArray.push({
            value: '--None--',
            key: '--None--'
        });
        ClientReqArray.push({
            value: 'Yes',
            key: 'Yes',
            selected: 'Yes' === ClientReq
        });
        ClientReqArray.push({
            value: 'No',
            key: 'No',
            selected: 'No' === ClientReq
        });
        cmp.set("v.clientworkOnReqList", ClientReqArray);
    },
    fieldsToPopulateOnPageLoad: function(cmp, event, helper) {
        var jobDescription;
        var clientOpenPosition;
        var expectedStartDate;
        var expectedInterviewDate;
        var addnlQualify;
        var extDesc;
        var TopSkillDet;


        //Job Description
        if (cmp.get("v.editJobdescription") == undefined) {
            if (event.getParams().recordUi.record.fields.Req_Job_Description__c != null) {
                jobDescription = event.getParams().recordUi.record.fields.Req_Job_Description__c.value;
                cmp.set("v.editJobdescription", jobDescription);
            }
        }


        //Additional Qualification
        if (cmp.get("v.addQualification") == undefined) {
            if (event.getParams().recordUi.record.fields.Req_Qualification__c != null) {
                addnlQualify = event.getParams().recordUi.record.fields.Req_Qualification__c.value;
                cmp.set("v.addQualification", addnlQualify);
            }
        }

        //Client Opened Position Date
        if (cmp.get("v.clientOpenPositionDate") == undefined) {
            if (event.getParams().recordUi.record.fields.Req_Date_Client_Opened_Position__c != null) {
                clientOpenPosition = event.getParams().recordUi.record.fields.Req_Date_Client_Opened_Position__c.value;
                cmp.set("v.clientOpenPositionDate", clientOpenPosition);
            }
        }
        //Expected start date
        if (cmp.get("v.reqStartDate") == undefined) {
            if (event.getParams().recordUi.record.fields.Start_Date__c != null) {
                expectedStartDate = event.getParams().recordUi.record.fields.Start_Date__c.value;
                cmp.set("v.reqStartDate", expectedStartDate);
            }
        }

        //Expected interview date
        if (cmp.get("v.reqInterviewDate") == undefined) {
            if (event.getParams().recordUi.record.fields.Req_Interview_Date__c != null) {
                expectedInterviewDate = event.getParams().recordUi.record.fields.Req_Interview_Date__c.value;
                cmp.set("v.reqInterviewDate", expectedInterviewDate);
            }
        }

        //External Community job Description
        if (cmp.get("v.externalCommJobDescriptionText") == undefined) {
            if (event.getParams().recordUi.record.fields.Req_External_Job_Description__c != null) {
                extDesc = event.getParams().recordUi.record.fields.Req_External_Job_Description__c.value;
                cmp.set("v.externalCommJobDescriptionText", extDesc);
            }
        }

        //Top Skills Details
        if (cmp.get("v.editTopSkillDetls") == undefined) {
            if (event.getParams().recordUi.record.fields.Req_Skill_Details__c != null) {
                TopSkillDet = event.getParams().recordUi.record.fields.Req_Skill_Details__c.value;
                cmp.set("v.editTopSkillDetls", TopSkillDet);
            }
        }


    },
    setRequiredFieldsOnTGSChange: function(component, event) {
        window.setTimeout(
            $A.getCallback(function() {
                var stgName = component.find("tekdetailStage").get("v.value");
                if (stgName == "RedZone") {
                    stgName = "Qualified";
                }
                //Req Validator after TGS Change    
                var validatefalg = component.find("validator");
                validatefalg.validateRequiredEditFields(stgName, component.find("productId").get("v.value"), 'No');
            }), 500
        );
    },
    onCancelSetFieldAttribute: function(cmp, event) {

        //var extComm = cmp.get("v.req.Req_External_Job_Description__c");
        cmp.set("v.editJobdescription", cmp.get("v.req.Req_Job_Description__c"));
        cmp.set("v.addQualification", cmp.get("v.req.Req_Qualification__c"));
        cmp.set("v.clientOpenPositionDate", cmp.get("v.req.Req_Date_Client_Opened_Position__c"));
        cmp.set("v.reqStartDate", cmp.get("v.req.Start_Date__c"));
        cmp.set("v.reqInterviewDate", cmp.get("v.req.Req_Interview_Date__c"));
        cmp.set("v.externalCommJobDescriptionText", cmp.get("v.req.Req_External_Job_Description__c"));
        cmp.set("v.editTopSkillDetls", cmp.get("v.req.Req_Skill_Details__c"));
    },
    validatePlacementTypeFields: function(component, event, helper, validated, eventFields) {
        let error_count = 0;
        var salaryMinValue = eventFields.Req_Salary_Min__c;
        var salaryMaxValue = eventFields.Req_Salary_Max__c;
        var requiredBillRateMax = eventFields.Req_Bill_Rate_Max__c;
        var requiredBillRateMin = eventFields.Req_Bill_Rate_Min__c;
        var flatFee = eventFields.Req_Flat_Fee__c;
        var Fee = eventFields.Req_Fee_Percent__c;
        var payrateMax = eventFields.Req_Pay_Rate_Max__c;
        var payrateMin = eventFields.Req_Pay_Rate_Min__c;
        var reqStandardBurden = eventFields.Req_Standard_Burden__c;

        if (requiredBillRateMin != null && requiredBillRateMin >= 10000000000) {
            component.set("v.billRateMinValMsg", "Bill Rate Min is very large.");
            validated = false;
            error_count++;
        } else {
            component.set("v.billRateMinValMsg", "");
        }

        if (requiredBillRateMax != null && requiredBillRateMax >= 10000000000) {
            component.set("v.billRateMaxValMsg", "Bill Rate Max is very large.");
            validated = false;
            error_count++;
        } else {
            component.set("v.billRateMaxValMsg", "");
        }

        if (payrateMin != null && payrateMin >= 10000000000) {
            component.set("v.payRateMinValMsg", "Pay Rate Min is very large.");
            validated = false;
            error_count++;
        } else {
            component.set("v.payRateMinValMsg", "");
        }

        if (payrateMax != null && payrateMax >= 10000000000) {
            component.set("v.payRateMaxValMsg", "Pay Rate Max is very large.");
            validated = false;
            error_count++;
        } else {
            component.set("v.payRateMaxValMsg", "");
        }

        if (reqStandardBurden != null && reqStandardBurden >= 100000) {
            component.set("v.standardBurdenValMsg", "Total Burden is very large.");
            validated = false;
            error_count++;
        } else {
            component.set("v.standardBurdenValMsg", "");
        }

        if (salaryMinValue != null && salaryMinValue >= 10000000000) {
            component.set("v.salaryMinValMsg", "Salary Min is very large.");
            validated = false;
            error_count++;
        } else {
            component.set("v.salaryMinValMsg", "");
        }

        if (flatFee != null && Fee != null && Fee >= 10000000000 && flatFee >= 10000000000) {
            component.set("v.flatFeeValMsg", "Flat Fee & Fee% is very large.");
            validated = false;
            error_count++;
        } else {
            if (flatFee != null && flatFee >= 10000000000) {
                component.set("v.flatFeeValMsg", "Flat Fee is very large.");
                validated = false;
                error_count++;
            }
            if (Fee != null && Fee >= 10000000000) {
                component.set("v.flatFeeValMsg", "Fee% is very large.");
                validated = false;
                error_count++;
            }
        }

        if (salaryMaxValue != null && salaryMaxValue >= 10000000000) {
            component.set("v.salaryMaxValMsg", "Salary Max is very large.");
            validated = false;
            error_count++;
        } else {
            component.set("v.salaryMaxValMsg", "");
        }

        return error_count;
    },
    skillSpecialtyClassifierCall: function(component, event) {
        console.log(component.get("v.payload"));
        var callAPI = component.get("v.SSCAPIinProgress") == true ? false : true && component.get("v.editFlag");
        let payload = component.get("v.payload");

        if (
            callAPI &&
            component.get("v.callSkillSpecialtyAPI") == true &&
            payload != null &&
            payload["req_job_title__c"] != null && typeof payload["req_job_title__c"] != undefined &&
            payload["req_job_description__c"] != null && typeof payload["req_job_description__c"] != undefined
        ) {
            var action = component.get("c.fetchSkillSpecialtyWrapper");
            console.log(payload);
            action.setParams({
                "payload": JSON.stringify(payload)
            });
            component.set("v.SSCAPIinProgress", true);

            action.setCallback(this, function(response) {
                var data = response;
                if (data.getState() == 'SUCCESS') {
                    var model = data.getReturnValue();
                    component.set("v.SSApiModel", model);
                    console.log(model);
                    this.updateFieldsFromWrapper(component, event, model);
                    var updatedFields = component.get("v.req.Req_Manually_Updated_Fields__c");
                    if (updatedFields == undefined || !updatedFields.includes('Skill_Set__c')) {
                        this.updateSkillSet(component, event, model);
                    }
					this.setReqMetricsData(component, event);
                }
                component.set("v.SSCAPIinProgress", false);
            });
            $A.enqueueAction(action);
        }
    },
    updateFieldsFromWrapper: function(component, event, model) {
        //Adding w.r.t Story S-234527
        var updatedFields = component.get("v.req.Req_Manually_Updated_Fields__c");

        if (updatedFields == undefined || !updatedFields.includes('Req_Skill_Specialty__c')) {
            if (model.skill_specialty.label) {
                component.set("v.req.Req_Skill_Specialty__c", model.skill_specialty.label);
            }
        }
        if (updatedFields == undefined || !updatedFields.includes('Functional_Non_Functional__c')) {
            if (model.functional.functional) {
                if (model.functional.functional == 'Functional') {
                    component.set("v.req.Functional_Non_Functional__c", "Functional");
                } else if (model.functional.functional == 'Not-Functional') {
                    component.set("v.req.Functional_Non_Functional__c", "Non-Functional");
                }
            }
        }
        if (updatedFields == undefined || !updatedFields.includes('Req_Job_Level__c')) {
            if (model.RVT.Experience_level) {
                if (model.RVT.Experience_level == '0') {
                    component.find("experienceLevelId").set("v.value", 'Entry Level');
                } else if (model.RVT.Experience_level == '1') {
                    component.find("experienceLevelId").set("v.value", 'Intermediate Level');
                } else if (model.RVT.Experience_level == '2') {
                    component.find("experienceLevelId").set("v.value", 'Expert Level');
                } else {
                    component.find("experienceLevelId").set("v.value", model.RVT.Experience_level);
                }
            }
        }
        //End of Adding w.r.t Story S-234527
        if (model.onet_job_code) {
            component.set("v.req.Req_ONet_SOC_Description__c", model.onet_job_code.onet_soc_description);
            component.set("v.req.Soc_onet__c", model.onet_job_code.onet_soc_code);    
        }
        if (model.RVT && model.RVT.RVT_detail){
            component.set("v.req.Req_RVT_Premium_Skills__c", model.RVT.RVT_detail.premium_fields);
            component.set("v.req.Req_RVT_Occupation_Code__c",model.RVT.Occupation_Code);
            component.set("v.req.Req_RVT_Occupation_Code_Confidence__c",model.RVT.Occupation_confidence_value);
            component.set("v.req.Req_RVT_Experience_Level_Confidence__c",model.RVT.Experience_level_confidence_value);
        }        
    },
    updateSkillSet: function(component, event, model) {
        let skillSetModel = model.skillset.skillset_classifier.maxSkillsetClassifier;
        if (skillSetModel) {
            component.set("v.req.Skill_Set__c", skillSetModel);
        }
    },
    ManuallyUpdatedFields: function(component, event, helper, APIName) {
        var updatedFields = component.get("v.req.Req_Manually_Updated_Fields__c");
        if (updatedFields != undefined && updatedFields != null && !updatedFields.includes(APIName)) {
            updatedFields = updatedFields + ',' + APIName;
        }
        if (updatedFields == undefined || updatedFields == null || updatedFields == '') {
            updatedFields = APIName;
        }
        component.set("v.req.Req_Manually_Updated_Fields__c", updatedFields);
	},
	setReqMetricsData: function(component, event) {
		//Adding w.r.t Story S-234527
		var model = component.get("v.SSApiModel");
		let ExpLevel;
		if(model.RVT.Experience_level) {
			if(model.RVT.Experience_level == '0') {
				ExpLevel = 'Entry Level';
			} else if(model.RVT.Experience_level == '1') {
				ExpLevel = 'Intermediate Level';
			} else if(model.RVT.Experience_level == '2') {
				ExpLevel = 'Expert Level';
			} else {
				ExpLevel = model.RVT.Experience_level;
			}
		}
		//End of Adding w.r.t Story S-234527
		component.set("v.reqMetrics.Skill_Speciality_From_Model__c", model.skill_specialty.label);
		component.set("v.reqMetrics.F_NF_From_Model__c", model.functional.functional == 'Functional' ? 'Functional' : 'Non-Functional');
		component.set("v.reqMetrics.Skillset_From_Model__c", model.skillset.skillset_classifier.maxSkillsetClassifier);
		component.set("v.reqMetrics.Experience_Level_From_Model__c", ExpLevel);
		component.set("v.reqMetrics.Job_Title__c", component.get("v.jobtitle")[0].name);
		component.set("v.reqMetrics.Job_Description__c", component.find("jobDescription").get("v.value"));
		component.set("v.reqMetrics.OpptyId__c", component.get("v.recordId"));
		var reqMetrics = component.find('reqMetrics');
		reqMetrics.saveMetricDataOnCreate();
	},
})