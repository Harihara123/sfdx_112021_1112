({
    afterRender: function (component, helper) {
        this.superAfterRender();
        //alert('After Render Hii');
        // interact with the DOM here
        // var interval = window.setInterval(
        //    $A.getCallback(function() {
		//		 component.set('v.isFocus1', false);
        //    }), 10000
        //); 
        if (window.location.href.indexOf("view?sv=") > -1 && component.find("container")) {
        	$A.util.addClass(component.find("container"), 'container');
        }        
    },
    rerender : function(component, helper) {
        this.superRerender();
        const sectionEl = component.get('v.clickedSection');
        const editMode = component.get('v.editFlag')

        //const divs = document.querySelectorAll('div');
        if (editMode && sectionEl) {

            const selector = 'div.section-'+sectionEl;
            const el = document.querySelector(selector)
            //console.log('%c Edit section button','background-color: black;color: aqua;',sectionEl+ ' ' +selector, el)
            if (el) {
                const timeoutRef = component.get('v.timeoutRef');
                if (timeoutRef) {
                    clearTimeout(timeoutRef)
                }
                const timeout = setTimeout(() => {
                    el.style = 'scroll-margin: 20rem'
                    el.scrollIntoView(true)
                    component.set('v.clickedSection', null)
                }, 500)
                component.set('v.timeoutRef', timeout);
            }
        }
    },
})