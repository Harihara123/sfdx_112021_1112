({
	recordLoadedView :function(cmp, event, helper) {
        console.log('record view 5');
        try {
           var flag=cmp.get("v.viewLoaded");
           cmp.set("v.skillRefreshFlag",true);
           cmp.set("v.jobTitleRefreshFlag",true);
           cmp.set("v.addinsertFlag",true);
           if(flag==false){
                 cmp.set("v.viewLoaded",true);
                 cmp.set("v.editFlag",false);
                 cmp.set("v.viewFlag",true);
                
                 
                var recordU=event.getParam("recordUi");
                var prodval=event.getParams().recordUi.record.fields.Req_Product__c.value;
                console.log('prodtype val rU'+prodval);

               var ocCode = event.getParams().recordUi.record.fields.Req_RVT_Occupation_Code__c.value;
               var ocGroup = event.getParams().recordUi.record.fields.RVT_Occupation_Group__c.value;
               var jobLevel = event.getParams().recordUi.record.fields.Req_Job_Level__c.value;
               var metro = event.getParams().recordUi.record.fields.Metro__c.value;
               var country = event.getParams().recordUi.record.fields.Req_Worksite_Country__c.value;
               var lcountry = country.toLowerCase();
               var fcountry;
               if(lcountry=='united states' || lcountry=='usa' || lcountry=='us'){
                   fcountry = 'USA';
               }
               else if(lcountry='canada' || lcountry=='can' || lcountry=='ca'){
                   fcountry = 'CAN';
               }
               
               
               if((ocCode!=null && ocCode!=0) && (ocGroup!=null && ocGroup!=0) && (jobLevel!=null && jobLevel!=0) && metro != null && fcountry != null){
                 var baseURL = $A.get("$Label.c.RVT_PROD_URL")
                 var link = baseURL+'&parent_hcs_description='+ocGroup+'&Hcs%20Group%20Display='+ocCode+'&Hcs%20Group%20Level%20Display='+jobLevel+'&Country%20Code='+fcountry+'&Ag%20Geog%20Short%20Name%20Orig='+metro;
                 cmp.set("v.RVTVisibility",true);
                 cmp.set("v.RVTLink",link);
               }

               var actSectsList = cmp.get("v.activeSections"); 
                    // F cth and SI -- Perm
                 if(prodval=='Permanent' ){
                         cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",false);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                 }else if(prodval=='Contract to Hire'){
                     cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",true);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                     
                 }else{
                          cmp.set("v.cthFlag",true);
                          cmp.set("v.permFlag",false);
                          
                         if(actSectsList != undefined){
                              console.log("act sec"+actSectsList);
                             actSectsList.push("F");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                              console.log("after F sec"+actSectsList);
                         } 
                     }
                    
                    
                    event.setParam("recordUi",recordU);
                  }
            // let aeroTekFormContainer = cmp.find("aeroTekFormView");
            // if(!$A.util.isEmpty(aeroTekFormContainer)){
            //     let viewHeight = aeroTekFormContainer.getElement().scrollHeight;
            //     cmp.set("v.containerHeight.viewHeight", viewHeight);
            //     console.log(`The view height is ===> ${viewHeight}`);
            // }
            
        }
        catch(err) {
            console.log('errors '+err);
        } 
        finally {
			const relatedTitles = event.getParam("recordUi").record.fields.Suggested_Job_Titles__c.value;
			if(relatedTitles) {
				const parsedTitles = JSON.parse(relatedTitles);
				cmp.set("v.initialSuggTitles",parsedTitles);
				let suggTitles = [];
				parsedTitles.forEach(title => {
					if (!title.removed) {
						suggTitles.push(title.term);
					}
				})
				cmp.set("v.suggJobTitles",suggTitles);
			}
            
            var sgn=event.getParams().recordUi.record.fields.StageName.value;
            //New Qualifying Stage Changes
            var statusLDS = event.getParams().recordUi.record.fields.LDS_Status__c.value;
            var prevStage = event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c.value;
             
            console.log('stg val rU'+sgn);
            console.log('Status SK '+statusLDS);
            console.log('stg val Prev'+prevStage);
            
            cmp.set("v.req_stage",sgn);
            
            var opts;          
            
            //Open Req
            if(statusLDS == 'Open'){
               if(sgn == 'Qualified'){
                      cmp.set("v.stgQual",true);
                   }           
               if(sgn == 'Draft'){
                   opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                       ];
                      }  
                else{
                      opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                       ];
                    }
            }
            //Closed Req
            if(statusLDS == 'Closed'){          
               if(prevStage=='Draft' && sgn.substring(0,6) == 'Closed'){
                   opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                       ];
                      }
               else if(prevStage == 'Staging' && sgn.substring(0,6)=='Closed'){
                   opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" },
                       	   { "class": "slds-input slds-combobox__input ", label: "Staging", value: "Staging" ,selected: "true"}
                       ];
                      }                
                else{
                      opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                       ];
                    }
            }            
              
                    
                    if(cmp.find("detailViewStageNameV") != undefined){
                         var stgVw = cmp.find("detailViewStageNameV");
                         var flag=Array.isArray(stgVw);
                         if(flag){
                             console.log("stageV array");
                             stgVw[0].set("v.options", opts);
                            }else{
                              console.log("not array");
                              cmp.find("detailViewStageNameV").set("v.options", opts);
                            } 
                    }
        }
       
    },
    enableInlineEdit :function(cmp, event, helper) {
        console.log('inline edit pressd 3');  
        cmp.set("v.editFlag",true);
        cmp.set("v.viewFlag",false);
        
        var stgName = cmp.find("detailViewStageName");
        console.log('stage name '+stgName);
        
         var opts = [
                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
        	];
        
        cmp.find("detailViewStageName").set("v.options", opts);

        

        if(cmp.get("v.editFlag")){
            // set coordinates of fixed save/cancel bar
            let editFormWidth = cmp.find("aeroTekFormContainer").getElement().clientWidth;

            cmp.set("v.containerWidth", editFormWidth);
           
        }
        
        cmp.set("v.skillRefreshFlag",true);
        cmp.set("v.jobTitleRefreshFlag",true);
        if(event.getSource().getLocalId()!=null || typeof event.getSource().getLocalId()!='undefined'){
            var sId=event.getSource().getLocalId();

           
          
            
            
            var id=sId.substring(0,sId.length-1);

            cmp.set("v.scrollId",id);

            var scrollId = cmp.get("v.scrollId");

            let cmpTarget = cmp.find(scrollId);
            
           //  let rect = event.target.getBoundingClientRect();

           //  var mouseX = Math.round(event.clientX * rect.top);

           //  let aeroTekFormContainerEdit = cmp.find("aeroTekForm");
           //  if(!$A.util.isEmpty(aeroTekFormContainerEdit)){

           //      let viewHeight = aeroTekFormContainerEdit.getElement().scrollHeight;
           //      cmp.set("v.containerHeight.editHeight", viewHeight);
           //      console.log(`The edit height is ===> ${viewHeight}`);
           //  }

           //  let containerDiff = Math.round(cmp.get("v.containerHeight.viewHeight") - cmp.get("v.containerHeight.editHeight")) + 600;


           // if(scrollId != undefined){
           //   // window.scrollTo(0, screenY);
           //  window.scrollTo({
           //      top: mouseX - containerDiff,
           //      behavior: 'smooth'
           //   });
           //  }
            
        }
        
    },
    onStageChange: function(cmp) {
        var stgSelect = cmp.find("detailViewStageName"); 

		cmp.set("v.req_stage",stgSelect.get("v.value"));
        if(stgSelect.get("v.value") == 'Draft'){
        	cmp.find("aerobusinessRedZone").set("v.value", false);
        }
        
        
        if(stgSelect.get("v.value") == 'Qualified'){
            cmp.set("v.stgQual",true);
        }
	 },
    recordLoadedEdit :function(cmp, event, helper) {
        console.log('edit pressed 25'); 
        var btn = event.getSource();
        console.log('btn pressed '+btn); 
        var editLoadedFlag=cmp.get("v.editLoaded");
         if(editLoadedFlag==false){
             cmp.set("v.editFlag",true);
             cmp.set("v.viewFlag",false);
             
             var jobDescription;
             var topSkillDetails;
             var addnlSkills;
             var perfExpects;
             var workEnv;
             var evp;
             var addnlInfo;
             var comp;
             var extCommJobdesc;

			
             //suggestedSkills
		 cmp.set("v.reqOpco",event.getParams().recordUi.record.fields.OpCo__c.value);
		 cmp.set("v.isCloneReq",true);
           // cmp.set("v.reqOpco",event.getParams().recordUi.record.fields.OpCo__c.value);
            var acId=event.getParams().recordUi.record.fields.AccountId.value;
            if(typeof acId!='undefined' && acId!=null){
                    var fcmp=cmp.find('accountLoader');
                    fcmp.set("v.recordId",acId);
                    fcmp.reloadRecord();
            }
             
             
         /*    if(event.getParams().recordUi.record.fields.Req_Job_Title__c!=null){
                 if(!cmp.get("v.loopFlag")){
                     var jobAryId=cmp.find("aerodetailReqJobTitle");
                     var jobTitle=event.getParams().recordUi.record.fields.Req_Job_Title__c.value;
                     var flg=Array.isArray(jobAryId);
                     cmp.set("v.lookupJTFlag",false);
                     if(flg){
                         console.log("jobAryId is array");
                         jobAryId[0].set("v.value",jobTitle);
                        }else{
                          console.log("not an array");
                          jobAryId.set("v.value",jobTitle);
                       }
                }
             }*/
             
             if(event.getParams().recordUi.record.fields.Req_Job_Description__c!=null){
                 jobDescription=event.getParams().recordUi.record.fields.Req_Job_Description__c.value;
                 cmp.set("v.editJobdescription",jobDescription);
             }
              
             if(event.getParams().recordUi.record.fields.Req_Skill_Details__c !=null){
                 topSkillDetails=event.getParams().recordUi.record.fields.Req_Skill_Details__c.value;
                 cmp.set("v.editTopSkillDetls",topSkillDetails);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Qualification__c !=null){
                 addnlSkills=event.getParams().recordUi.record.fields.Req_Qualification__c.value;
                 cmp.set("v.editAddnlSkillsQuals",addnlSkills);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Performance_Expectations__c !=null){
                perfExpects =event.getParams().recordUi.record.fields.Req_Performance_Expectations__c.value;
                 cmp.set("v.editPerfExpects",perfExpects);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Work_Environment__c !=null){
                 workEnv =event.getParams().recordUi.record.fields.Req_Work_Environment__c.value;
                 cmp.set("v.editWorkEnv",workEnv);
             }
             
             if(event.getParams().recordUi.record.fields.Req_EVP__c !=null){
                 evp =event.getParams().recordUi.record.fields.Req_EVP__c.value;
                 cmp.set("v.editEvp",evp);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Additional_Information__c !=null){
                 addnlInfo =event.getParams().recordUi.record.fields.Req_Additional_Information__c.value;
                 cmp.set("v.editAddnlInfo",addnlInfo);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Compliance__c !=null){
                 comp =event.getParams().recordUi.record.fields.Req_Compliance__c.value;
                 cmp.set("v.editCompliance",comp);
             }
             
             if(event.getParams().recordUi.record.fields.Req_External_Job_Description__c !=null){
                 extCommJobdesc =event.getParams().recordUi.record.fields.Req_External_Job_Description__c.value;
                 cmp.set("v.editExtCommJobDesc",extCommJobdesc);
             }
             
            var stgName =event.getParams().recordUi.record.fields.StageName.value;
            
             if(stgName == 'Qualified'){
                   cmp.set("v.stgQual",true);
                }
             
             if(cmp.find("detailViewStageName") != undefined){
                 console.log('stg name1 '+stgName);
                 
                 var stgd = cmp.find("detailViewStageName");
                 var flag=Array.isArray(stgd);
                 if(flag){
                     console.log("stage is array");
                     stgName = (stgName == 'Draft') ? 'Draft' : 'Qualified';
                     stgd[0].set("v.value",stgName);
                    }else{
                      console.log("not an array");
                      stgName = (stgName == 'Draft') ? 'Draft' : 'Qualified';
                      cmp.find("detailViewStageName").set("v.value",stgName);
                    } 
             }
             if(cmp.find("aerodetailAccountId") != undefined){
                var aeroAcc = cmp.find("aerodetailAccountId");
                var flg=Array.isArray(aeroAcc);
                var acctId = event.getParams().recordUi.record.fields.AccountId.value;
                console.log("Acc id"+acctId);
                if(flg){
                     console.log("acc is array");
                     aeroAcc[0].set("v.value",acctId);
                    }else{
                      console.log("not an array");
                      aeroAcc.set("v.value",acctId);
                   }
             }
         }
		 if(cmp.get("v.editDisable")) {
			cmp.set("v.editDisable",false);
		 }
    },
    handleError : function(cmp,event,helper){
        console.log('error reached '); 
        var params = event.getParams();
        cmp.set("v.editDisable",false);
        var Validation = false;
        var validationMessages=[];
        //cmp.set("v.skillRefreshFlag",true);
 		//cmp.set("v.jobTitleRefreshFlag",true);
        
         if(params.output!=null && params.output.fieldErrors!=null ){
           
            var stgErrors=params.output.fieldErrors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      var checkFlag=Array.isArray(value);
                      console.log(key + ':' + value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                		Validation = true;
                });
         
        }
        
        if(params.output!=null && params.output.errors!=null ){
           
            var stgErrors=params.output.errors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                		Validation = true;
                });
         
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
        var fieldErrors=event.getParams().error.data.output.fieldErrors;
        Object.keys(fieldErrors).forEach(function(key){
  				      var value = fieldErrors[key];
   					  console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;            
            		  validationMessages.push(fmessage);
            			Validation = true;
            
			});
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {
            
            var fieldErrors=event.getParams().error.data.output.errors;
            
            Object.keys(fieldErrors).forEach(function(key){
  				  var value = fieldErrors[key];
   					 console.log(key + ':' + value);
                   var checkFlag=Array.isArray(value);
            	   var fmessage=(checkFlag==true)?value[0].message:value.message;
            		  validationMessages.push(fmessage);
                		Validation = true;
            
			});
            
        }
        cmp.set("v.Validation",Validation);
        cmp.set("v.validationMessagesList",validationMessages);
 		console.log('handleError----------->'+validationMessages);
        
        console.log("ValidationMessage Retrieve----->"+cmp.get("v.validationMessagesList"));
    },
    handleCancel : function (cmp,event,helper){
        event.preventDefault();
         /*cmp.set("v.validationMessagesList",null);
         cmp.set("v.editFlag",false);
         cmp.set("v.viewFlag",true);  
         cmp.set("v.cancelFlag",true);*/
        /*
           cmp.set("v.stageErrorVarName","");
            cmp.set("v.jobTitleErrorVarName","");
            cmp.set("v.hiringManagerErrorVarName","");
            cmp.set("v.accountErrorVarName","");
            cmp.set("v.draftReasonVarName","");
            cmp.set("v.officeErrorVarName","");
            cmp.set("v.plmtTypeErrorVarName","");
            cmp.set("v.streetErrorVarName","");
            cmp.set("v.cityTypeErrorVarName","");
            cmp.set("v.stateErrorVarName","");
            cmp.set("v.countryVarName","");
            cmp.set("v.zipErrorVarName","");
            cmp.set("v.currencyErrorVarName","");
            cmp.set("v.skillPillValidationError","");
         	event.preventDefault();
        	cmp.set("v.Validation",false);*/
    },
    onSubmit  : function(cmp, event, helper) {
       console.log('on submit click ');
        cmp.set("v.editDisable",true);
        event.preventDefault();
        var eventFields = event.getParam("fields");
        var Validation = false;
        var fieldMessages=[];
        helper.validateAddress(cmp,event);
        var workSiteOpp=cmp.get("v.Opportunity");
        var addFieldMessages=cmp.get("v.addFieldMessage");
        var validated = true;
        if(typeof addFieldMessages!='undefined' && addFieldMessages!=null && addFieldMessages.length>0){
            fieldMessages=fieldMessages.concat(addFieldMessages);
            validated=false;
        }else{
            fieldMessages=fieldMessages.concat(addFieldMessages)
            eventFields.Req_Worksite_Street__c=workSiteOpp.Req_Worksite_Street__c;
			eventFields.Req_Worksite_City__c=workSiteOpp.Req_Worksite_City__c;
			eventFields.Req_Worksite_State__c=workSiteOpp.Req_Worksite_State__c;
			eventFields.Req_Worksite_Country__c=workSiteOpp.Req_Worksite_Country__c;
            eventFields.StreetAddress2__c=workSiteOpp.StreetAddress2__c;
            eventFields.Req_Worksite_Postal_Code__c=workSiteOpp.Req_Worksite_Postal_Code__c;
            eventFields.Req_GeoLocation__Latitude__s=workSiteOpp.Req_GeoLocation__Latitude__s;
            eventFields.Req_GeoLocation__Longitude__s=workSiteOpp.Req_GeoLocation__Longitude__s;
        }
        
        
        
        helper.prepareSkills(cmp, event);
				
		//Related Titles
		var prepareRelatedTitles = cmp.find("relatedTitles");
		prepareRelatedTitles.prepareRelatedTerms();
		let relatedTitles = cmp.get("v.finalRelatedTitlesList");
		if(relatedTitles.length > 0){
			eventFields.Suggested_Job_Titles__c = JSON.stringify(relatedTitles);
		}
        cmp.set("v.loopFlag",false);
        var Validation = false;
        var finalSkillList=cmp.get("v.finalskillList");
        var jsonSkills=JSON.stringify(finalSkillList);
        eventFields.EnterpriseReqSkills__c=jsonSkills;
         if(cmp.get("v.editJobdescription")!=null)
          eventFields.Req_Job_Description__c=cmp.get("v.editJobdescription");
        //processing addnl
         if(eventFields.Req_Job_Description__c != null)
             eventFields.Description=eventFields.Req_Job_Description__c;
        
        if(cmp.get("v.editTopSkillDetls") != null){
            eventFields.Req_Skill_Details__c =cmp.get("v.editTopSkillDetls");
        }
        
        if(cmp.get("v.editAddnlSkillsQuals") != null){
            eventFields.Req_Qualification__c =cmp.get("v.editAddnlSkillsQuals");
        }
        
         if(cmp.get("v.editPerfExpects") != null){
            eventFields.Req_Performance_Expectations__c =cmp.get("v.editPerfExpects");
        }
        
        if(cmp.get("v.editWorkEnv") != null){
            eventFields.Req_Work_Environment__c =cmp.get("v.editWorkEnv");
        }
        
        if(cmp.get("v.editEvp") != null){
            eventFields.Req_EVP__c =cmp.get("v.editEvp");
        }
        
        if(cmp.get("v.editAddnlInfo") != null){
            eventFields.Req_Additional_Information__c =cmp.get("v.editAddnlInfo");
        }
        
        if(cmp.get("v.editCompliance") != null){
            eventFields.Req_Compliance__c =cmp.get("v.editCompliance");
        }
        
        if(cmp.get("v.editExtCommJobDesc") != null){
            eventFields.Req_External_Job_Description__c =cmp.get("v.editExtCommJobDesc");
        }
        
         var dsn=cmp.find('detailViewStageName');
         var stgName =dsn.get("v.value");
         
        if(eventFields.StageName =='Draft' || eventFields.StageName =='Qualified'){
           eventFields.StageName =stgName;
        }
        
        //validation
         //var validated = true;
         //var fieldMessages=[];
        
        if(eventFields.StageName == null || eventFields.StageName == ''){
            validated=false;
            cmp.set("v.stageErrorVarName","Stage Name cannot be blank.");
            fieldMessages.push("Stage Name cannot be blank.");
           }else{
             cmp.set("v.stageErrorVarName","");
          }
         
        //var appName = cmp.get("v.editJobdescription");
        var inputDesc = cmp.find("aeroreqSummaryJobDescription");
         if (((inputDesc.get("v.value") == null) || (inputDesc.get("v.value") == '')) && eventFields.StageName == 'Qualified' ) {
             validated=false; 
             //cmp.set("v.aeroreqSmyJobDescError","Description is required.");
             inputDesc.set("v.errors", [{message:"Description is required."}]);
             fieldMessages.push("Description is required.");
         }
        else if(inputDesc.get("v.value") != null && inputDesc.get("v.value") != '' && inputDesc.get("v.value").length < 25 && eventFields.StageName == 'Qualified' ) {
             validated=false; 
             //cmp.set("v.aeroreqSmyJobDescError","Description is required.");
             inputDesc.set("v.errors", [{message:"Job Description should have at least 25 characters."}]);
             fieldMessages.push("Job Description should have at least 25 characters.");
         }
            else{
             inputDesc.set("v.errors",null);
             cmp.set("v.aeroreqSmyJobDescError","");
         }
        
         var jobtitleAry = cmp.get("v.jobtitle");
         if(jobtitleAry==undefined || jobtitleAry==null || jobtitleAry.length<=0){
            validated=false;
            cmp.set("v.jobTitleErrorVarName","Job Title cannot be blank.");
            var tempAr=[];
            cmp.set("v.jobtitle",tempAr);
           //  cmp.set("v.jobTitleRefreshFlag",true);
           // cmp.set("v.skillRefreshFlag",true);
            fieldMessages.push("Job Title cannot be blank.");
           }else{
               if((jobtitleAry!=null && jobtitleAry!=undefined && jobtitleAry[0]  != null && jobtitleAry[0]  != '')){
                   eventFields.Req_Job_Title__c=jobtitleAry[0].name;
               }
             cmp.set("v.jobTitleErrorVarName","");
          }
        
         if(eventFields.Req_Hiring_Manager__c == null || eventFields.Req_Hiring_Manager__c == ''){
            validated=false;
            cmp.set("v.hiringManagerErrorVarName","Hiring Manager cannot be blank.");
            fieldMessages.push("Hiring Manager cannot be blank.");
           }else{
             cmp.set("v.hiringManagerErrorVarName","");
          }
        
        if(eventFields.Req_Total_Positions__c == null || eventFields.Req_Total_Positions__c == ''){
            validated=false;
            cmp.set("v.totalPositionsVarName", "Total Positions cannot be blank.");
            fieldMessages.push("Total Positions cannot be blank.");
        }else{
            cmp.set("v.totalPositionsVarName", "");
        }
        
       
         if(eventFields.LDS_Account_Name__c == null || eventFields.LDS_Account_Name__c == ''){
            validated=false;
            cmp.set("v.accountErrorVarName","Account Name cannot be blank.");
            fieldMessages.push("Account Name cannot be blank.");
           }else{
             cmp.set("v.accountErrorVarName","");
          }
        
		//var quals = eventFields.Req_Qualification__c; 
		var inputQual = cmp.find("aeroreqSummaryQualification");
        if(((inputQual.get("v.value") == null) || (inputQual.get("v.value") == '')) && eventFields.StageName == 'Qualified'){
			validated=false;
			//cmp.set("v.aeroreqQualEditErr","Additional Skills & Qualifications is required.");
			inputQual.set("v.errors", [{message:"Additional Skills & Qualifications is required."}]);
			fieldMessages.push("Additional Skills & Qualifications is required.");
		}else{
             inputQual.set("v.errors",null);
             cmp.set("v.aeroreqQualEditErr","");
          }
         
         var drftRsn = eventFields.Req_Draft_Reason__c;
         if(((drftRsn == null) || (drftRsn == '')) && eventFields.StageName == 'Draft') {
            validated=false;
            cmp.set("v.draftReasonVarName","Draft Reason cannot be blank.");
            fieldMessages.push("Draft Reason cannot be blank.");
           }else{
             cmp.set("v.draftReasonVarName","");
          }
        
         if(eventFields.Organization_Office__c == null || eventFields.Organization_Office__c == ''){
            validated=false;
            cmp.set("v.officeErrorVarName","Office cannot be blank.");
            fieldMessages.push("Office cannot be blank.");
           }else{
             cmp.set("v.officeErrorVarName","");
          }
        
         if(eventFields.Req_Product__c == null || eventFields.Req_Product__c == ''){
            validated=false;
            cmp.set("v.plmtTypeErrorVarName","Product cannot be blank.");
            fieldMessages.push("Product cannot be blank.");
           }else{
             cmp.set("v.plmtTypeErrorVarName","");
          }
        var mspName = cmp.find("aeroMSP").get("v.value");
        if(eventFields.StageName =='Qualified' && 
          (mspName ==null || mspName =='' || typeof mspName == "undefined" || mspName== '--None--')){
             cmp.set("v.mspNameValMsg","Please complete this field.");
             fieldMessages.push("Please complete this field.");
            validated=false;
        }else{
             cmp.set("v.mspNameValMsg","");
        } 
        var governmentWorkName = cmp.find("governmentWorkId").get("v.value");
        if(eventFields.StageName =='Qualified' && 
          (governmentWorkName ==null || governmentWorkName =='' || typeof governmentWorkName == "undefined" || governmentWorkName== '--None--')){
             cmp.set("v.governmentWorkNameValMsg","Please complete this field.");
             fieldMessages.push("Please complete this field.");
            validated=false;
        }else{
             cmp.set("v.governmentWorkNameValMsg","");
        } 
        /* if(eventFields.Req_Worksite_Street__c == null || eventFields.Req_Worksite_Street__c == ''){
            validated=false;
            cmp.set("v.streetErrorVarName","Street cannot be blank.");
             fieldMessages.push("Street cannot be blank.");
           }else{
             cmp.set("v.streetErrorVarName","");
          }
        
         
         if(eventFields.Req_Worksite_City__c == null || eventFields.Req_Worksite_City__c == ''){
            validated=false;
            cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
             fieldMessages.push("City cannot be blank.");
           }else{
             cmp.set("v.cityTypeErrorVarName","");
          }
        
         if(eventFields.Req_Worksite_State__c == null || eventFields.Req_Worksite_State__c == ''){
            validated=false;
            cmp.set("v.stateErrorVarName","State cannot be blank.");
             fieldMessages.push("State cannot be blank.");
           }else{
             cmp.set("v.stateErrorVarName","");
          }
        
         if(eventFields.Req_Worksite_Country__c == null || eventFields.Req_Worksite_Country__c == ''){
            validated=false;
            cmp.set("v.countryVarName","Country cannot be blank.");
            fieldMessages.push("Country cannot be blank.");
           }else{
             cmp.set("v.countryVarName","");
          }
        
         if(eventFields.Req_Worksite_Postal_Code__c == null || eventFields.Req_Worksite_Postal_Code__c == ''){
            validated=false;
            cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
             fieldMessages.push("Postal Code cannot be blank.");
           }else{
             cmp.set("v.zipErrorVarName","");
          }
        */
         
         if(eventFields.Currency__c == null || eventFields.Currency__c == ''){
            validated=false;
            cmp.set("v.currencyErrorVarName","Currency cannot be blank.");
            fieldMessages.push("Currency cannot be blank.");
           }else{
             cmp.set("v.currencyErrorVarName","");
          }
        
        var pillData=cmp.get("v.finalskillList"); 
       if(stgName=='Qualified' && (pillData==null || pillData=='' || typeof pillData == "undefined")){
            cmp.set("v.skillPillValidationError","At least one skill is required before moving to the 'Qualified' stage or above.");
           // cmp.set("v.skillRefreshFlag",true);
           // cmp.set("v.jobTitleRefreshFlag",true);
            validated=false;
            fieldMessages.push("At least one skill is required before moving to the 'Qualified' stage or above.");
       }else{
            cmp.set("v.skillPillValidationError","");
       }
        
        eventFields.AccountId=cmp.find("aerodetailAccountId").get("v.value");
        event.setParam("fields", eventFields);
        
        if(validated){
          /* cmp.set("v.skillRefreshFlag",false);
           cmp.set("v.jobTitleRefreshFlag",false);*/
           cmp.set("v.insertFlag",true);
           cmp.set("v.addinsertFlag",false);
           cmp.find('aeroTekEditForm').submit(eventFields); 
        }else{
            cmp.set("v.editDisable",false);
            console.log('count '+fieldMessages.length);
            cmp.set("v.validationMessagesList",fieldMessages); 
            Validation = true;
			cmp.set("v.Validation",Validation);
        }
    },
    handleOpportunitySaved :function(cmp, event,helper){
        
    	var params = event.getParams();
 		console.log('handleopty Saved----------->'+JSON.stringify(params));
        cmp.set("v.validationMessagesList",null);
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true); 
        cmp.set("v.cancelFlag",true);
        
        var positionRefereshEvt = $A.get('e.c:E_RefreshPositionCard');
        positionRefereshEvt.fire();
		var appEvent = $A.get("e.c:RefreshEinsteinCard");
        appEvent.setParams({
            "message" : "Opportunity Updated" });
        appEvent.fire();
   
		var skillRefreshEvt = $A.get('e.c:E_OpportunitySkillsRefresh');
		skillRefreshEvt.fire();
    },
    preventProcessing : function (cmp,event,helper){
        event.preventDefault();
        event.stopPropagation();
        console.log('Prevent method.....');
    },
    onAccountChange :function (cmp,event,helper){
       var acId=event.getParams().value;
       cmp.find("aerodetailAccountId").set("v.value",acId);
    },
    onProductChange :  function (cmp,event,helper){
      var eventFields = event.getParams();
			 var prodval=eventFields.value;
         if(prodval=='Permanent' ){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",false);
         }else if(prodval=='Contract to Hire'){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",true);
         }else{
             cmp.set("v.cthFlag",true);
              cmp.set("v.permFlag",false);
         }
    },
    handleSectionToggle: function (cmp, event) {
        
    },
    enterHandler: function(component, event, helper) {
     component.set("v.lookupJTFlag",!component.get("v.lookupJTFlag"));
     component.set("v.loopFlag",true);
    },
    changeJobTitle :function (component, event, helper){
        
        var globalLovId=event.getParams().value;
        var tempRec = component.find("recordLoader");
        tempRec.set("v.recordId",globalLovId[0]);
        tempRec.reloadRecord();
    },
    recordUpdated:function (component, event, helper){
        helper.recordUpdated(component, event, helper);
    },
    onRender:function (component, event, helper){
      //  var elmnt = document.getElementById("extIdopp");
      //  elmnt.scrollIntoView();
    },
	navigateToError: function(component, event, helper){
    	var elmnt = document.getElementById("editpageid");
        window.scrollTo(0,200);
	},
    buttonHandleCancel : function (cmp,event,helper){
         cmp.set("v.validationMessagesList",null);
         cmp.set("v.editFlag",false);
         cmp.set("v.viewFlag",true);  
         cmp.set("v.cancelFlag",true);
		 //set skills to original skill & title data
		 cmp.set("v.skills",[]);
        
           cmp.set("v.stageErrorVarName","");
            cmp.set("v.jobTitleErrorVarName","");
            cmp.set("v.hiringManagerErrorVarName","");
            cmp.set("v.accountErrorVarName","");
            cmp.set("v.draftReasonVarName","");
            cmp.set("v.officeErrorVarName","");
            cmp.set("v.plmtTypeErrorVarName","");
        	cmp.set("v.mspNameValMsg","");
       		cmp.set("v.governmentWorkNameValMsg","");
            cmp.set("v.streetErrorVarName","");
            cmp.set("v.cityTypeErrorVarName","");
            cmp.set("v.stateErrorVarName","");
            cmp.set("v.countryVarName","");
            cmp.set("v.zipErrorVarName","");
            cmp.set("v.currencyErrorVarName","");
            cmp.set("v.skillPillValidationError","");
            cmp.set("v.totalPositionsVarName","");
            cmp.set("v.cancelJTFlag",true);
        	cmp.set("v.addFieldMessage",null);
            cmp.set("v.addFieldMessagedMap",null);
         	event.preventDefault();
        	cmp.set("v.Validation",false);
    },
    accountUpdated:function (component, event, helper){
        helper.accountUpdated(component, event, helper);
    },
    
    showEditModal : function(component, event, helper) {
        var appEvent = $A.get('e.c:E_PositionModal');
        appEvent.fire();
    },
    
     refereshPositionData : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
	
    changeJobTitleHandler :function(cmp,event,helper){//Suggested skills for aerotek
        let jbTitle=cmp.get("v.jobtitle");
        if(jbTitle!=null && jbTitle.length>0){
        	cmp.set("v.suggestedjobTitle",jbTitle[0].name);
        } else {
			cmp.set("v.suggestedjobTitle","");
		} 
    },
	handleSuggestedTitlesChange:function(cmp,event,helper) {
		
		const data = event.getParams();
		cmp.set("v.isUpdatedRelatedTitles",true);
		cmp.set("v.updatedSuggestedJobTitles",data.skills);
		if(!$A.util.isUndefined(data.skills) || !$A.util.isEmpty(data.skills)) {
             console.log('test')
			cmp.set("v.suggTitlesForSkills",data.skills.join());
		}

	},
	changeSuggestedTitleHandler : function(cmp,event,helper) {
		if(!cmp.get("v.viewFlag")) {
			//Fetch Suggested Job titles
			let jobTitle = cmp.get("v.suggestedjobTitle");
			if(jobTitle != null) {
				//Related titles
				var relatedTitles = cmp.find("relatedTitles");
				relatedTitles.fetchRelatedTerms(jobTitle,"ONS");
			}
		}
	},
	relatedTitleHandler: function(cmp,event,helper) {
		console.log('suggJobTitles'+cmp.get('v.suggJobTitles'));
		let titles = cmp.get('v.suggJobTitles')
		if(titles != []) {
			
			cmp.set("v.suggTitlesForSkills",titles.join());
			
		}
	}
    
})