({
    afterScriptsLoaded : function(component, event, helper) 
    {
        //var getDataAction = component.get("c.CRM_AllegisInsightsEnterpriseBarChartTest");
        var getDataAction = component.get("c.CRM_AllegisInsightsEnterpriseBarChartMethod");
        getDataAction.setParams({
            //"accId":  '0011k00000Wx1seAAB',
            "accId" : component.get("v.accountRecordId"),
            "whereId":'Enterprise'
        });
        getDataAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") 
            {
                //alert("SUCCESS");
                var dataObj = response.getReturnValue();
                
                var companyNameList = dataObj.companyNameList;
                var companyNameDataMap = dataObj.resultCountMap;
                
                var categoriesToPass = new Array(); 
                if(companyNameList != null && companyNameList != undefined && companyNameList.length > 0)
                {
                    for(var i=0; i < companyNameList.length; i++)
                    {
                        categoriesToPass[i] = companyNameList[i];
                    }
                }
                var seriesHelper;
                if(companyNameDataMap != null && companyNameDataMap != undefined)
                {
                    for(var key in companyNameDataMap)
                    {
                        if(seriesHelper)
                        {                    
                            seriesHelper = seriesHelper + ',{"name":"'+ key + '","data":[' + companyNameDataMap[key] + ']}';
                        }
                        else
                        {
                            seriesHelper = '{"name":"'+ key + '","data":[' + companyNameDataMap[key] + ']}';
                        }
                    }
                    console.log("seriesHelper:"+seriesHelper);
                    seriesHelper = '[' + seriesHelper + ']';
                }
                console.log("seriesHelper1:"+seriesHelper);
                var seriesData = '[{"name": "Aerotek, Inc","data": [4,0,0,6,1,2,2,0,1]}, {"name": "Allegis Group, Inc.","data": [3,3,0,3,1,2,4,1,1]},{"name": "Major, Lindsey & Africa, LLC","data": [0,0,16,0,0,0,0,0,0]}]';
                
                if(companyNameList != null && companyNameList != undefined && companyNameList.length > 0 &&
                   companyNameDataMap != null && companyNameDataMap != undefined)
                {
                    Highcharts.chart({
                        chart: {
                            renderTo: component.find("container").getElement(),
                            type: 'bar'
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            categories : categoriesToPass
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Values'
                            }
                        },
                        legend: {
                            reversed: true
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        series: JSON.parse(seriesHelper)
                    });
                }
                else
                {
                    component.set("v.msgDisplay",true);
                    component.set("v.msg","No data to display");
                }
            }
        });
        $A.enqueueAction(getDataAction);      
    },
    afterScriptsLoadedDynamic : function(component, event, helper) 
    {
        var mainList = [];
        mainList.push('Apples');
        mainList.push('Oranges');
        mainList.push('Pears');
        mainList.push('Grapes');
        mainList.push('Bananas');
        var categories = '';
        var categoriesToPass = new Array();
        var seriesData = '[{"name": "John","data": [5, 3, 4, 7, 0]}, {"name": "Jane","data": [2, 2, 3, 2, 1]},{"name": "Joe","data": [3, 4, 4, 2, 5]}]';
        for(var i=0; i < mainList.length; i++)
        {
            categoriesToPass[i] = mainList[i];
            /*if(categories == '')
            {
                categories =  mainList[i];
            }
            else
            {
                categories = categories + ',' + mainList[i];
            }*/
        }
        //alert('categories:'+categoriesToPass);
        //var categoriesVar = '['+ JSON.stringify(mainList) + ']';
        //var categoriesVar = JSON.stringify(mainList);
        //alert('categories:'+categoriesVar);
        //var categoriesVarTest = '"Apples", "Oranges", "Pears", "Grapes", "Bananas"';
        //alert('categoriesVarTest:'+categoriesVarTest);
        //var testValue = 'categories: ["Apples","Oranges","Pears", "Grapes", "Bananas"]';
        Highcharts.chart({
            chart: {
                renderTo: component.find("container").getElement(),
                type: 'bar'
            },
            title: {
                text: ''
            },
            xAxis: {
                //testValue
                //categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                //categories: [JSON.stringify(mainList)]
                //categories : [categoriesVarTest]
                //categories: ["Apples","Oranges","Pears", "Grapes", "Bananas"]
                categories : categoriesToPass
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: JSON.parse(seriesData)
            /*series: [
                {
                    name: 'John',
                    data: [5, 3, 4, 7, 2]
                }, 
                {
                    name: 'Jane',
                    data: [2, 2, 3, 2, 1]
                }, 
                {
                    name: 'Joe',
                    data: [3, 4, 4, 2, 5]
                }
            ]*/
        });
    },
    afterScriptsLoadedBackup : function(component, event, helper) 
    {
        Highcharts.chart({
            chart: {
                renderTo: component.find("container").getElement(),
                type: 'bar'
            },
           
            xAxis: {
                categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: [
                {
                    name: 'John',
                    data: [5, 3, 4, 7, 2]
                }, 
                {
                    name: 'Jane',
                    data: [2, 2, 3, 2, 1]
                }, 
                {
                    name: 'Joe',
                    data: [3, 4, 4, 2, 5]
                }
            ]
        });
    }        
})