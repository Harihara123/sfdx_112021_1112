({
    doInit : function(component, event, helper) 
    {
		helper.loadDonutChart(component, event, helper);
    },
    loadDonutChart : function(component, event, helper){
        var action = component.get("c.getAccountFinanceData");
        
        var recId = component.get("v.recordId");
        action.setParams({
            "accId":  recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
               // alert(dataObj);
                //console.log('dataObj:'+dataObj);
                var res = [];
                var outterData = [];
                var outterMapData = [];
                var innerData = [];
                var subinnerData = [];
                res = JSON.parse(dataObj);
                outterData = res.OutterData;
                outterMapData = res.OutterDataMap;
                innerData = res.InnerMapData;
                subinnerData = res.InnerData;
                //console.log(outterData.length);
                if(outterData !=null && outterData.length > 0){                    
                    var outArray = [];
                    for(var i = 0; i < outterData.length; i++){
                        outArray.push(outterData[i]);
                    }
                    var innerMapData = [];
                    for(var key in innerData) {
                        if (innerData.hasOwnProperty(key)) {
                            innerMapData.push({data:innerData[key]});
                        }
                    }
                    var st='';
                    for(var i=0; i <innerMapData.length; i++){
                        var t = innerMapData[i].data[0].name.split(':')[0];
                        var y = innerMapData[i].data[0].name.split(':')[1];
                        if(innerMapData[i].data.length > 1){
                            for(var x = 0; x < innerMapData[i].data.length; x++){
                                innerMapData[i].data[x].name = innerMapData[i].data[x].drilldown ;
                                innerMapData[i].data[x].drilldown = '';
                            }
                        }else{
                            innerMapData[i].data[0].name = innerMapData[i].data[0].drilldown ;
                            innerMapData[i].data[0].drilldown = '';
                        }
                        if (st) {
                            st += ',"'+t+'": '+JSON.stringify(innerMapData[i]);
                        } else {
                            st = '"'+t+'": '+JSON.stringify(innerMapData[i]);
                        }
                    }
                    component.set("v.outterDatas",JSON.stringify(outArray));                    
                    var finaloutput = '';
                    finaloutput = '{'+st+'}';
                    component.set("v.subinnerDatas",finaloutput);
                    //console.log('finaloutput--'+finaloutput);                        
                    helper.donutchart(component,event,helper);                                             
                }else{
                    component.set("v.msgDisplay",true);
                    component.set("v.msg","No data to display");
                }
            }
        });
        $A.enqueueAction(action);
    },
    donutchart : function(component,event,helper) {
        var finaloutterData = component.get("v.outterDatas");
        //console.log('finaloutterData'+finaloutterData);
        var finalinnerData = component.get("v.subinnerDatas");
       // console.log('finalinnerData'+finalinnerData);
        Highcharts.setOptions({
            lang: {
                drillUpText: 'Back',
                thousandsSep: ','
            }
        });
        new Highcharts.Chart({
            chart: {
                renderTo: component.find("container").getElement(),
                type: 'pie',                
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    labels: {
                        formatter: function() {
                            if (this.value >= 1E6) {
                                return (this.value / 1000000).toFixed(2) + 'M';
                            }
                            return this.value / 1000 + 'k';
                        }
                    }
                },                
                events: {
                    drilldown: function (e){
                        console.log('-e-'+e);
                        if (!e.seriesOptions) {                            
                            var chart = this,
                                //drilldowns = JSON.parse('{"TEKsystems, Inc.":{"data":[{"y":6,"showInLegend":true,"name":"Applications","drilldown":"Applications"},{"y":4,"showInLegend":true,"name":"Communications","drilldown":"Communications"},{"y":1,"showInLegend":true,"name":"Network Infrastructure","drilldown":"Network Infrastructure"}]},"AG_EMEA": {"data":[{"y":1,"showInLegend":true,"name":"Aerotek","drilldown":"Aerotek"}]},"Aerotek":{"data":[{"name":"Draft","y":1,"showInLegend":true}]},"Applications":{"data":[{"name":"Draft","y":2,"showInLegend":true},{"name":"Qualified","y":4,"showInLegend":true}]},"Communications":{"data":[{"name":"Qualified","y":3,"showInLegend":true},{"name":"Presenting","y":1,"showInLegend":true}]},"Network Infrastructure":{"data":[{"name":"Draft","y":1,"showInLegend":true}]}}'),
                                //drilldowns = JSON.parse('{"TEKsystems, Inc.": {"data":[{"y":6,"showInLegend":true,"name":"Applications","drilldown":"Applications"},{"y":4,"showInLegend":true,"name":"Communications","drilldown":"Communications"},{"y":1,"showInLegend":true,"name":"Network Infrastructure","drilldown":"Network Infrastructure"}]},"AG_EMEA": {"data":[{"y":1,"showInLegend":true,"name":"Aerotek","drilldown":"Aerotek"}]},"Aerotek":{"data":[{"name":"Draft","y":1,"showInLegend":true}]},"Applications":{"data":[{"name":"Draft","y":2,"showInLegend":true},{"name":"Qualified","y":4,"showInLegend":true}]},"Communications":{"data":[{"name":"Qualified","y":3,"showInLegend":true},{"name":"Presenting","y":1,"showInLegend":true}]},"Network Infrastructure":{"data":[{"name":"Draft","y":1,"showInLegend":true}]}}'),
                                //drilldowns = JSON.parse('[{"id":"TEKsystems, Inc.-2010","data":[["TEKsystems",4],["TEKsystems New",5]]},{"id":"Aerotek, Inc-2010","data":[["Aerotek",6],["Aerotek New",7]]},{"id":"TEKsystems, Inc.-2014","data":[["TEKsystems 1",2],["TEKsystems New",3]]},{"id":"Aerotek, Inc-2014","data":[["Aerotek 1",4],["Aerotek New",5]]}]');
                                drilldowns = JSON.parse(finalinnerData),
                                series = drilldowns[e.point.name];
                          /*  console.log('-e.point.name-'+JSON.stringify(e.point.name));
                            console.log('-finalinnerData-'+JSON.stringify(finalinnerData));
                            console.log('-drilldowns-'+JSON.stringify(drilldowns));
                            console.log('-series-'+JSON.stringify(series));*/
                            
                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 100);
                        }                        
                    },
                    load: function () {
                        //var chart = this.series[0].chart.userOptions.series[0].data[0].y;
                        var chart = this.series[0].chart;
                        const num = Highcharts.numberFormat(chart.userOptions.series[0].data[0].y,0, null, ',');
                        const nameVal = chart.userOptions.series[0].data[0].name;
                        const styles = (el) => {
                            const container = `width: 170px; color: ${this.options.colors[0]}; text-align: center; position: absolute; top: 144px; left: 67px;`;
                            const number = `font-size: 30px;`;
                            const styles = {container, number};                                        
                            	return styles[el];
                            }
                        const text = `<div style="${styles('container')}"><p style="${styles('number')}">${num}</p><p class="slds-truncate" title="${nameVal}">${nameVal}</p></div>`;
                        if (!chart.lbl) {
                         	chart.lbl = chart.renderer.label(text, null, null, null, null, null, true).add();
                        }
                        chart.lbl
                        .show()
                        .attr({
                         	text
                        });
                    },
                    drillup: function () {
                        //var chart = this.series[0].chart.userOptions.series[0].data[0].y;
                        var chart = this.series[0].chart;
                        const num = Highcharts.numberFormat(chart.userOptions.series[0].data[0].y,0, null, ',');
                        const nameVal = chart.userOptions.series[0].data[0].name;
                        const styles = (el) => {
                            const container = `width: 170px; color: ${this.options.colors[0]}; text-align: center; position: absolute; top: 144px; left: 67px;`;
                            const number = `font-size: 30px;`;
                            const styles = {container, number};                                        
                            	return styles[el];
                            }
                        const text = `<div style="${styles('container')}"><p style="${styles('number')}">${num}</p><p class="slds-truncate" title="${nameVal}">${nameVal}</p></div>`;
                        if (!chart.lbl) {
                         	chart.lbl = chart.renderer.label(text, null, null, null, null, null, true).add();
                        }
                        chart.lbl
                        .show()
                        .attr({
                         	text
                        });
                    }
                }
            },
            legend: {
                layout: 'horizontal'
            },
            plotOptions: {
                "column": {
                "stacking": "normal"
            },series: {
                    borderWidth: 6,
                    innerSize: '65%',  
                    startAngle: 90,
                    size: '100%',
                    shadow: true,
                    dataLabels: false,
                    showInLegend: true,
                    point: {
                        events: 
                        {
                            mouseOver: function () 
                            {
                                var chart = this.series.chart;
                                const num = Highcharts.numberFormat(this.y,0, null, ',');
                                const styles = (el) => 
                                    {
                                    const container = `width: 170px; color: ${this.color}; text-align: center; position: absolute; top: 144px; left: 67px;`;
                                    const number = `font-size: 30px;`;
                                    const styles = {container, number};

                                    return styles[el];
                                }
                                const text = `<div style="${styles('container')}"><p style="${styles('number')}">${num}</p><p class="slds-truncate" title="${this.name}">${this.name}</p></div>`;
                                if (!chart.lbl) 
                                                    {
                                    chart.lbl = chart.renderer.label(text, null, null, null, null, null, true).add();
                                }

                                chart.lbl
                                .show()
                                .attr({
                                    //text: this.y +'.00 '+ this.name
                                    // text: this.y +' '+ this.name,
                                    text
                                });
                            }
                        }
                    }
                }
            },
            series: [{
                name: '',
                //data: JSON.parse('[{"y":11,"showInLegend":true,"name":"TEKsystems, Inc.","drilldown":"Yes"},{"y":2,"showInLegend":true,"name":"AG_EMEA","drilldown":"Yes"}]')
                //data: JSON.parse('[{"AG_EMEA":{"y":1,"showInLegend":true,"name":"AG_EMEA","drilldown":"Yes"},"TEKsystems, Inc.":{"y":11,"showInLegend":true,"name":"TEKsystems, Inc.","drilldown":"Yes"}}]')
                //data: JSON.parse('[{"name":"TEKsystems, Inc.","data":[{"name": "2010","y":9,"drilldown":"TEKsystems, Inc.-2010"},{"name":"2014","y":5,"drilldown":"TEKsystems, Inc.-2014"}]},{"name":"Aerotek, Inc","data":[{"name":"2010","y":13,"drilldown":"Aerotek, Inc-2010"},{"name":"2014","y":9,"drilldown":"Aerotek, Inc-2014"}]}]')
                data: JSON.parse(finaloutterData)
            }],            
            drilldown: 
                                                    {
                series: []
            },
            title:{
                text:''
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>' 
            },
            credits: {
                enabled: false
            }
        });
    }
})