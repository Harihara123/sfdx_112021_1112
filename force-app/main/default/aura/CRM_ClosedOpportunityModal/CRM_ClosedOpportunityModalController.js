({
	//Method to closed modal
    closeModal: function(component, event, helper){
        var setNull;
        component.set("v.isOpen", false);
        component.set("v.enableOtherDetails", false);
        component.set("v.closedReasonCategory", setNull);
    },
	//Method to control visibility of Closed Other Details    
    enableOtherDetailField: function(component, event, helper){
        var selectedClosedCategory = String(component.find("selectedCategory").get("v.value"));
        if(selectedClosedCategory.trim() === 'Other'){
        	component.set("v.enableOtherDetails", true); 
        }
        else{
            component.set("v.enableOtherDetails", false);  
        }
    },
    // Method to validate and close opportunity.
    closeOpportunity: function(component, event, helper){           
        component.set("v.disableOnClick",true);
        helper.validate(component, event, helper);
    },
    callPicklistMapping : function(component, event, helper){	     
        helper.getPicklistMapping(component, event, helper);
    },
    callclosedReasonCategory : function(component, event, helper){
        // get selected controller field value
        var controllerValueKey = component.find("closedReason").get("v.value"); 
        var depnedentFieldMap = component.get("v.depnedentClosedReasonMap");
        
        if (controllerValueKey != '--- None ---') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            
            if(ListOfDependentFields.length > 0){
                component.set("v.bDisabledDependentFld" , false);  
                helper.fetchDepValues(component, ListOfDependentFields, false);    
            }else{
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['--- None ---']);
            }  
            
        } else {
            component.set("v.listDependingValues", ['--- None ---']);
            component.set("v.bDisabledDependentFld" , true);
        }
    }
})