({
	validate : function(component, event, helper) {
        var validate = true;
        var closedReason = component.find("closedReason").get("v.value");
        var closedCategory = component.find("selectedCategory").get("v.value");    
        var closedReasonField = component.find("closedReason");
        var isclosedReasonEmpty = $A.util.isEmpty(component.find("closedReason").get("v.value"));
        //validate Closed Reason
        closedReasonField.set("v.errors", null);
        if(isclosedReasonEmpty || closedReason === "--- None ---"){
            validate = false;
            closedReasonField.set("v.errors", [{message: "Please select a value."}]);   
        }
        //Validate Closed Reason Category
        var closedCategoryField = component.find("selectedCategory");
        var isclosedCategoryEmpty = $A.util.isEmpty(component.find("selectedCategory").get("v.value"));
        closedCategoryField.set("v.errors", null);
        if(isclosedCategoryEmpty  || closedCategory === "--- None ---"){
            validate = false;
            closedCategoryField.set("v.errors", [{message: "Please select a value."}]);   
        }
        //validate  Closed other details
        if(closedCategory === "Other"){    
            var otherDetails = component.find("otherDetails").get("v.value");
            var otherDetailsField = component.find("otherDetails");
            var isotherDetailsEmpty = $A.util.isEmpty(component.find("otherDetails").get("v.value"));
            otherDetailsField.set("v.errors", null);
            if(isotherDetailsEmpty){
                validate = false;
                otherDetailsField.set("v.errors", [{message: "Please enter details."}]);   
            }
        }
        // Is validataion is successful
        if(validate){
            if(closedReason === "Won"){
                //component.set("v.isOpen",false);
                var p = component.get("v.parent");
    			p.validatePostBid();
                helper.validateTGSOppSubmit(component, event, helper);
            }
            else{
                helper.closeTheOpportunity(component, event, helper);
            }            
        }
        else{
            component.set("v.disableOnClick",false);
        }
    },
    closeTheOpportunity : function(component, event, helper){
        var tgsOpp = component.get("v.OpptyRec");
        var closedReason = component.find("closedReason").get("v.value");
        var closedCategory = component.find("selectedCategory").get("v.value");    
        var otherDetails = ""
        if(closedCategory === "Other"){    
            otherDetails = component.find("otherDetails").get("v.value");
        }
        var action = component.get("c.closeOppty");
        action.setParams({"oppty": tgsOpp,
                          "closedReason": closedReason,
                          "closedCategory" : closedCategory,
                          "otherDetails": otherDetails
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var theResponse = response.getReturnValue();
                if(theResponse === "Success"){
                    
                    component.set("v.isOpen",false);
                    helper.showToastMessages('dismissible','success!','', 'Opportunity update is successful.', 'success');
                    $A.get('e.force:refreshView').fire();
                }else{
                    helper.showToastMessages('dismissible','Error!','', theResponse, 'error'); 
                    component.set("v.isOpen",false);
                }
                
            }else if(state === "ERROR"){
                
            }
        });        
        $A.enqueueAction(action);  
    },
    getPicklistMapping : function(component, event, helper){	
        var controllingFieldAPI = component.get("v.controllingFieldAPI");
        var dependingFieldAPI = component.get("v.dependingFieldAPI");
        var objDetails = component.get("v.objDetail");
        var tgsOpp = component.get("v.OpptyRec");
        
        var action = component.get("c.createDependentMap");
        action.setParams({
            'objDetail' : objDetails 
        });
        //set callback   
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var StoreResponse = response.getReturnValue();
                // once set #StoreResponse to depnedentFieldMap attribute 
                component.set("v.depnedentStageMap",StoreResponse[0]);
                component.set("v.depnedentClosedReasonMap",StoreResponse[1]);
                // create a empty array for store map keys(@@--->which is controller picklist values) 
                var listOfkeys = []; // for store all map keys (controller picklist values)
                var ControllerField = []; // for store controller picklist value to set on lightning:select. 
                
                // play a for loop on Return map 
                // and fill the all map key on listOfkeys variable.
                for (var singlekey in StoreResponse) {
                    listOfkeys.push(singlekey);
                }
                
                //set the controller field value for lightning:select
                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push('--- None ---');
                }
                
                for (var i = 0; i < listOfkeys.length; i++) {
                    ControllerField.push(listOfkeys[i]);
                }
                var controllerValueKey = tgsOpp.StageName; // get selected controller field value
                var depnedentFieldMap = component.get("v.depnedentStageMap");
                
                if (controllerValueKey != '--- None ---') {
                    var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
                    
                    if(ListOfDependentFields.length > 0){
                        component.set("v.bDisabledDependentFld" , false);  
                        helper.fetchDepValues(component, ListOfDependentFields, true);    
                    }else{
                        component.set("v.bDisabledDependentFld" , true);
                    }  
                    
                } 
                // set the ControllerField variable values to country(controller picklist field)
                component.set("v.listControllingValues", ControllerField);
                
            }else{
                //alert('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
    },
    fetchDepValues: function(component, ListOfDependentFields, isStage) {
        // create a empty array var for store dependent picklist values for controller field  
        var dependentFields = [];
        dependentFields.push('--- None ---');
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push(ListOfDependentFields[i]);
        }
        // set the dependentFields variable values to store(dependent picklist field) on lightning:select
        if(isStage === true)
        	component.set("v.listClosedReason", dependentFields);
        else
            component.set("v.closedReasonCategory", dependentFields);
        
    },
    getclosedReasonCategory : function(component, event, helper){
    	var selectedClosedReason = component.find("closedReason").get("v.value");
        var closedCategoryMap = [];
        if(selectedClosedReason !== null && selectedClosedReason !== ""){
            
            var closedCategoryString = "";
            for(var key in component.get("v.closedReasonAndCategory")){
                if(component.get("v.closedReasonAndCategory")[key].key === selectedClosedReason){
                	closedCategoryString = String(component.get("v.closedReasonAndCategory")[key].value);
                }
            }
            var lstCat = closedCategoryString.split(",");
            for(var val in lstCat){
                closedCategoryMap.push(lstCat[val]);
            }
         }
        component.set("v.closedReasonCategory", closedCategoryMap);
    },
    validateTGSOppSubmit : function(component, event, helper){
        
        component.set("v.OppList",component.get("v.OpptyRec"));
        var tgsOpp = JSON.parse(JSON.stringify(component.get("v.OppList")));
        var action = component.get("c.stageValidationMessages");
        action.setParams({"opptyNewList": tgsOpp
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var acc = response.getReturnValue();
                console.log(acc);
                if(acc.length > 0){
                    component.set("v.isOpen",false);
                }
                else{
                    helper.closeTheOpportunity(component, event, helper);
                }
            }else if(state === "ERROR"){
                var errors = response.getError();
            }
        });        
        $A.enqueueAction(action);        
    },
    showToastMessages : function(sticky,title, duration, message, type ){
       // message = message.replaceAll('<br/>', '\n');
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "mode": sticky,
            "title": title,
            "duration":duration,
            "message": message,
            "type": type
        });
        toastEvent.fire();
        
    }
})