({
	doInit : function(component, event, helper) {
        var navCmp = component.find("navService");
        var recId =component.get("v.recordId");
        var redirectComponent = 'c__CRM_Compensation_Form';
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": redirectComponent
            }, 
            "state": {
                "c__redirectId" : recId
            }                        
        };
        component.set("v.pageReference", pageReference);
        var navCmp = component.find("navCmp");
        var pageReference = component.get("v.pageReference");
        navCmp.navigate(pageReference);
	}
})