({
    doInit: function(component, event, helper) {
        var UrlQuery = window.location.href; 
        var myPageRef = component.get("v.pageReference");
        var tgsId ='';
        var oppId;
        if(myPageRef){
            oppId = myPageRef.state.c__redirectId;
        }
        if(oppId !== null && oppId !== undefined){
            component.set("v.recordId", oppId);
        }
        component.find("requestRecordCreator").getNewRecord(
            "Support_Request__c",
            "01224000000gLMzAAM",
            false,
            $A.getCallback(function() {
                var rec = component.get("v.request");                
                var error = component.get("v.newRequestError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }                
            })
        );        
        var action = component.get("c.isOpportunityClosed");
        action.setParams({"recordId": component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var theResponse = response.getReturnValue();
                if(theResponse === true){
                    component.set("v.requestId",true);
                }else{
                    component.set("v.requestId",false);
                }                
            }else if(state === "ERROR"){
                
            }
        });        
        $A.enqueueAction(action);
    },
    handleSaveRecord: function(component, event, helper) {
        component.set("v.btnDisable",true);
        var validity = helper.isFormValid(component, event, helper); 
        if(validity){
            component.find("requestRecordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {  
                    helper.showToastMessages('Success!', 'Compensation form saved successfully.', 'success');
                    helper.redirectToStartURL(component, event, helper);
                } else if (saveResult.state === "INCOMPLETE") {
                    // handle the incomplete state
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    // handle the error state
                    console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            });   
            component.set("v.btnDisable",false);
        }else{
            component.set("v.btnDisable",false);
        }
    },    
    GoToParent : function (component,event,helper) {
        helper.redirectToStartURL(component, event, helper);
    },
    reInit : function(component, event, helper) {
        helper.reInit(component, event, helper); 
    },
    handleLoad: function (component, event, helper) {
        var action = component.get("c.getOpptyData");
        action.setParams({"recordId": component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var theResponse = response.getReturnValue();
                for(var i=0; i < theResponse.length; i++){                    
                    if(typeof(theResponse[i].Location__c) !="undefined" && theResponse[i].Location__c !=null){
                        var location = theResponse[i].Location__c;
                        component.find("locationId").set("v.value", location);
                    }
                    if(typeof(theResponse[i].Impacted_Regions__c) !="undefined" && theResponse[i].Impacted_Regions__c !=null){
                        var impactedRegion = theResponse[i].Impacted_Regions__c;
                        component.find("regionsId").set("v.value", impactedRegion);
                    }
                    if(typeof(theResponse[i].Division__c) !="undefined" && theResponse[i].Division__c !=null){
                        var opptyDivision = theResponse[i].Division__c;
                        component.find("divisionId").set("v.value", opptyDivision);
                    }
                }
            }else if(state === "ERROR"){
                
            }
        });        
        $A.enqueueAction(action);
    }
})