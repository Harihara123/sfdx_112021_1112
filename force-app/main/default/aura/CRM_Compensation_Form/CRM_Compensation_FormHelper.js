({
    isFormValid: function (component, event, helper) {
        var valid = true;        
        if(valid){
            if(component.find("seniorDirectorId").get("v.value") == "" || component.find("seniorDirectorId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "seniorDirectorId", "seniorDirectorMessage");    
            if(component.find("opportunityId").get("v.value") == "" || component.find("opportunityId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "opportunityId", "opportunityMessage");
            if(component.find("accountTypeId").get("v.value") == "" || component.find("accountTypeId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "accountTypeId", "accountTypeMessage"); 
            if(component.find("effectiveId").get("v.value") == "" || component.find("effectiveId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "effectiveId", "effectiveMessage");
            if(component.find("programId").get("v.value") == "" || component.find("programId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "programId", "programIdMessage");
            if(component.find("winId").get("v.value") == "" || component.find("winId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "winId", "winMessage");
            if(component.find("yearSpendId").get("v.value") == "" || component.find("yearSpendId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "yearSpendId", "yearSpendIdMessage");
            if(component.find("spreadDateId").get("v.value") == "" || component.find("spreadDateId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "spreadDateId", "spreadDateMessage");
            if(component.find("stepsoppId").get("v.value") == "" || component.find("stepsoppId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "stepsoppId", "stepsoppIdMessage");
            if(component.find("vendorsId").get("v.value") == "" || component.find("vendorsId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "vendorsId", "VendorsMessage");
            if(component.find("summaryoppId").get("v.value") == "" || component.find("summaryoppId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "summaryoppId", "summaryoppIdMessage");
            if(component.find("regionsId").get("v.value") == "" || component.find("regionsId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "regionsId", "RegionsMessage");
            if(component.find("alternativeId").get("v.value") == "" || component.find("alternativeId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "alternativeId", "alternativeIdMessage");
            if(component.find("locationId").get("v.value") == "" || component.find("locationId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "locationId", "LocationMessage");
            if(component.find("supplyId").get("v.value") == "" || component.find("supplyId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "supplyId", "supplyIdMessage");
            if(component.find("divisionId").get("v.value") == "" || component.find("divisionId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "divisionId", "DivisionMessage");
            if(component.find("winningId").get("v.value") == "" || component.find("winningId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "winningId", "MarkupsMessage");
            if(component.find("InternalId").get("v.value") == "" || component.find("InternalId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "InternalId", "InternalMessage");
            if(component.find("lengthId").get("v.value") == "" || component.find("lengthId").get("v.value") == null)
                valid = helper.checkValidity(component, event, helper, "lengthId", "ContractMessage");			
        }
        return valid;
    },
    checkValidity :  function (component, event, helper, fieldName, errorFieldName) {
        var inputCmp = component.find(fieldName);
        var errorMessage = component.find(errorFieldName);
        var value = inputCmp.get("v.value");
        var valid = false;
        if (value == "" || value == null) {
            valid = false;
            $A.util.addClass(inputCmp, 'slds-visible slds-has-error');
            $A.util.removeClass(errorMessage, 'slds-hidden');
            $A.util.addClass(errorMessage, 'slds-visible slds-text-color--error');
        } else {
            valid = true;
            $A.util.removeClass(inputCmp, 'slds-visible slds-has-error');
            $A.util.removeClass(errorMessage, 'slds-visible slds-text-color--error');
            $A.util.addClass(errorMessage, 'slds-hidden');
        }
        return valid;
    },
    redirectToStartURL : function (component, event, helper){      
        var strtUrl ="/lightning/r/Opportunity/"+component.get("v.recordId")+ "/view";        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": strtUrl,
            "isredirect":true
        });
        urlEvent.fire();        
    },
    reInit : function(component, event, helper){
        $A.get('e.force:refreshView').fire();
    },
    showToastMessages : function( title, message, type ) {
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });        
        toastEvent.fire();
    }
})