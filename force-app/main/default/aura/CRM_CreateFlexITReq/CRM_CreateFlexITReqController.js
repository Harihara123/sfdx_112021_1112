({
    doInit: function(component, event, helper) {
        //helper.reInit(component, event, helper);
        var UrlQuery = window.location.href;        
        var myPageRef = component.get("v.pageReference");
        var tgsId ='';
        var pId ='';
        var oppId;
		var accName = '';
        if(myPageRef){
            oppId  = myPageRef.state.c__recId;
            tgsId = myPageRef.state.c__tgsOppId;
            pId = myPageRef.state.c__pId;
			accName = myPageRef.state.c__accName;
            if(oppId !== null && oppId !== undefined){
                component.set('v.AccountVarName',oppId); 
                component.set("v.recordId", oppId);
                component.set("v.parentrecordId", pId);
                //component.set("v.flexITVal", tgsId);
                component.set("v.account.Id",oppId);
				component.set("v.account.Name",accName);
                if(tgsId == 'Flexible Capacity'){                    
                    component.find("DeliveryModelId").set("v.value", 'Flexible Capacity');
                    //component.set("v.showTWS",true);
                }else{
                    component.find("DeliveryModelId").set("v.value", '');
                    //component.set("v.showTWS",false);
                }
				var Interest = $A.get("$Label.c.FC_Interest");
				var Qualifying = $A.get("$Label.c.FC_Qualifying");
                var opts = [
                    { "class": "slds-input slds-combobox__input ", label: Interest, value: "Interest", selected: "true" },
                    { "class": "slds-input slds-combobox__input ", label: Qualifying, value: "Qualifying" }
                ];                
                component.find("StageID").set("v.options", opts);            
            }
        } 
    },
    recordUpdate : function(component, event, helper) {
        var CmpName = component.get("v.record.CompanyName");
        //var ProfileName = component.get("v.record.Profile.Name");
		component.find("controllerFld").set("v.value", CmpName);
		if(component.find("DeliveryModelId").get("v.value") == 'Flexible Capacity' && CmpName == 'TEKsystems, Inc.'){
		    component.find("BusinessUnitID").set("v.value", 'TEK Global Services');  
			component.set("v.showTWS",true);
        }
        //var reqProfileNames = $A.get("$Label.c.DisplayProductProfiles");
        //console.log('reqProfileNames:'+reqProfileNames);
        //if(reqProfileNames.includes(ProfileName))
        if(CmpName == 'TEKsystems, Inc.' || CmpName == 'Aerotek, Inc')
        {
            component.set("v.showProduct",true);            
        }
    },
    onPicklistChange : function(component, event, helper){        
        var StageVal = component.find("StageID").get("v.value");
        component.set("v.StageValue",StageVal);
        /*if(component.get("v.StageValue") == 'Qualifying'){
            component.set("v.ProbValue",10); 
        }else{
            component.set("v.ProbValue",0); 
        } */
    },
    saveValidation : function(component,event,helper){
        var SaveBtVal = event.getSource().getLocalId();
        //alert(SaveBtVal);
        component.set("v.BtnVal",SaveBtVal);        
    },
    onSubmit : function(component,event,helper){ 
		event.preventDefault();
		component.set("v.isDisabled", true);        
        var fieldMessages=[];
        var FlexOpp = event.getParam("fields"); //component.get("v.objDetail");
        //var accStg = component.get("v.account");  
        //alert(accStg);
        var AccName = component.get("v.AccountVarName");
		
        if(AccName == null || AccName ==''){            
            component.set("v.accVarName","Account Name cannot be blank.");
            fieldMessages.push("Account Name cannot be blank.");
        }else{
            FlexOpp.AccountId = AccName;
            component.set("v.accVarName","");
        }
        
        var OppNameVal = component.find("inputoppName").get("v.value");
        if(OppNameVal !=null && OppNameVal !=''){
            FlexOpp.Name = OppNameVal;
            component.set("v.oppVarName","");
        }else{
            component.set("v.oppVarName","Opportunity Name cannot be blank.");
            fieldMessages.push("Opportunity Name cannot be blank.");
        }
        
        var OppStage = component.get("v.StageValue");//component.find("StageID").get("v.value");
        if(OppStage !=null && OppStage !=''){
            if(OppStage = 'Interest'){
                FlexOpp.StageName = component.get("v.StageValue");
                //FlexOpp.Probability = component.get("v.ProbValue");
            }else{
                FlexOpp.StageName = component.get("v.StageValue"); 
                //FlexOpp.Probability = component.get("v.ProbValue");
            }
            component.set("v.stageVarName",""); 
        }else{
            component.set("v.stageVarName","Stage cannot be blank.");
            fieldMessages.push("Stage cannot be blank.");
        }
        
        var OppRespType = component.find("RespType").get("v.value");
		var RespTypeErr = $A.get("$Label.c.FC_Response_Type_Err");
        if(OppRespType !=null && OppRespType !='' && OppRespType != '--- None ---'){
            FlexOpp.Response_Type__c = OppRespType;
            component.set("v.respVarName","");
        }else{
            component.set("v.respVarName",RespTypeErr);
            fieldMessages.push(RespTypeErr);
        }  
        
        if(component.get("v.showProduct") == true)
        {
            console.log('test0');
            var OppProduct = component.find("ProductID").get("v.value");
            var CmpName = component.get("v.record.CompanyName");
            var ProductErr = $A.get("$Label.c.FC_Product_Err");
            if(OppProduct !=null && OppProduct !='' && OppProduct != '--- None ---' || CmpName == 'Aerotek, Inc'){
                console.log('test');
                FlexOpp.Product__c = OppProduct;
                component.set("v.prdVarName","");
            }else{
                console.log('test1');
                component.set("v.prdVarName",ProductErr);
                fieldMessages.push(ProductErr);
            } 
        }
        
        var OppOpCo = component.find("controllerFld").get("v.value");
		var OpCoErr = $A.get("$Label.c.FC_OpCo_Err");
        if(OppOpCo !=null && OppOpCo !='' && OppOpCo != '--- None ---'){
            FlexOpp.OpCo__c = OppOpCo;
            component.set("v.OpCoVal","");
        }else{
            component.set("v.OpCoVal",OpCoErr);
            fieldMessages.push(OpCoErr);
        }   
        
        var OppBU = component.find("BusinessUnitID").get("v.value");
		var BusinessUnitErr = $A.get("$Label.c.FC_Business_Unit_Error");
        //if(component.get("v.bDisabledDependentFld") == false){
        if(OppBU !=null && OppBU !='' && OppBU != '--- None ---'){
            FlexOpp.BusinessUnit__c = OppBU;
            component.set("v.BuVal","");
        }else{
            component.set("v.BuVal",BusinessUnitErr);
            fieldMessages.push(BusinessUnitErr);
        } 
        //}
        /*var OppPT = component.find("ProductTeamID").get("v.value");
        if(OppBU =='EASI' || OppBU =='Government' || OppBU =='Strategic Delivery Solutions' || OppBU =='TEK Staffing' || OppBU =='Vertical Sales' || OppBU =='EASi Engineering' || OppBU =='EASi Sciences' || OppBU =='AG EMEA'){
            if(OppPT !=null && OppPT !=''){
                FlexOpp.Product_Team__c = OppPT;
                component.set("v.prodTeamVal","");
            }else{
                component.set("v.prodTeamVal","Product Team cannot be blank.");
                fieldMessages.push("Product Team cannot be blank.");
            } 
        }*/
        
        /*var OppDiv = component.find("DivisionID").get("v.value");
        //if(component.get("v.bDisabledDependentFld") == false){
        if(OppDiv !=null && OppDiv !='' && OppDiv != '--- None ---'){
            FlexOpp.Division__c = OppDiv;
            component.set("v.divisionVal","");
        }else{
            component.set("v.divisionVal","Division cannot be blank.");
            fieldMessages.push("Division cannot be blank.");
        } 
        //}
        var OppDM = component.find("DeliveryModelId").get("v.value");
        if(OppDM !=null && OppDM !=''){
            FlexOpp.DeliveryModel__c = OppDM;
            component.set("v.dMVal","");
        }else{
            component.set("v.dMVal","Delivery Model cannot be blank.");
            fieldMessages.push("Delivery Model cannot be blank.");
        }*/
        if(component.find("DeliveryModelId").get("v.value") == 'Flexible Capacity' && component.find("controllerFld").get("v.value") == 'TEKsystems, Inc.'){
        	FlexOpp.Segment_TEK__c = 'Flexible Capacity';
        }
        if(component.get("v.StageValue") == 'Qualifying'){
            /*var OppIR = component.find("ImpactedRegionID").get("v.value");
            if(OppIR !=null && OppIR !='' && OppIR != '--- None ---'){
                FlexOpp.Impacted_Regions__c = OppIR;
                component.set("v.impactedRegionVal","");
            }else{
                component.set("v.impactedRegionVal","Impacted Region cannot be blank.");
                fieldMessages.push("Impacted Region cannot be blank.");
            }*/
            
            var OppNRP = component.find("NumID").get("v.value");
            if(OppNRP !=null && OppNRP !=''){
                FlexOpp.Number_of_Resources__c = OppNRP;
                component.set("v.nRPVal","");
            }else{
                component.set("v.nRPVal","Number of Resources / Positions cannot be blank.");
                fieldMessages.push("Number of Resources / Positions cannot be blank.");
            }
            
            var OppRev = component.find("RevenueID").get("v.value");
            if(OppRev !=null && OppRev !=''){
                FlexOpp.Amount	= OppRev;
                component.set("v.revVal","");
            }else{
                component.set("v.revVal","Revenue cannot be blank.");
                fieldMessages.push("Revenue cannot be blank.");
            }
        }else{
            var OppIR = component.find("ImpactedRegionID").get("v.value");
            FlexOpp.Impacted_Regions__c = OppIR;
            component.set("v.impactedRegionVal","");
            
            var OppNRP = component.find("NumID").get("v.value");
            FlexOpp.Number_of_Resources__c = OppNRP;
            component.set("v.nRPVal","");
            
            var OppRev = component.find("RevenueID").get("v.value");
            FlexOpp.Amount	= OppRev;
            component.set("v.revVal","");
        }
        
        var OppDescp = component.find("DescriptionID").get("v.value");
		var DescErr = $A.get("$Label.c.FC_Desc_Error");
        if(OppDescp !=null && OppDescp !=''){
            FlexOpp.Description	= OppDescp;
            component.set("v.descpVal","");
        }else{
            component.set("v.descpVal",DescErr);
            fieldMessages.push(DescErr);
        }
        
        var OppcloseDate = component.find("closeDateVal").get("v.value");
        if(OppcloseDate !=null && OppcloseDate !=''){
            FlexOpp.CloseDate = OppcloseDate;
            component.set("v.closedDateVal","");
        }else{
            component.set("v.closedDateVal","Close date cannot be blank.");
            fieldMessages.push("Close date cannot be blank.");
        }
        //alert(fieldMessages.length); 
        if(fieldMessages.length > 0){
            component.set("v.isDisabled", false);
            component.set("v.validationMessagesList",fieldMessages);
        } else {  
            //alert('-1-'+component.get("v.BtnVal"));            
            component.find('FlexITOppID').submit(FlexOpp);
        }
    },
    handleSuccess : function(component, event, helper) { 
        var newOppId = event.getParams().response;
        console.log(newOppId.id);
        //alert('-2-'+component.get("v.BtnVal"));
        if(newOppId !=null){
            if(component.get("v.BtnVal") == 'saveButton'){
                helper.showToastMessages('Success!', 'Opportunity saved successfully.', 'success');
                component.set("v.newOpportunityId",newOppId.id);
                helper.redirectToOpportunityURL(component, event, helper);
            }else{
                helper.showToastMessages('Success!', 'Opportunity saved successfully.', 'success');
                $A.get('e.force:refreshView').fire();
            }
        }
    },
    handleError : function(cmp,event,helper){
        console.log('error reached '); 
        var params = event.getParams();
        var validationMessages=[];
        
        if(params.output!=null && params.output.fieldErrors!=null ){            
            var stgErrors=params.output.fieldErrors;            
            Object.keys(stgErrors).forEach(function(key){
                var value = stgErrors[key];
                var checkFlag=Array.isArray(value);
                console.log(key + ':' + value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;
                validationMessages.push(fmessage);                
            });            
        }
        
        if(params.output!=null && params.output.errors!=null ){            
            var stgErrors=params.output.errors;            
            Object.keys(stgErrors).forEach(function(key){
                var value = stgErrors[key];
                console.log(key + ':' + value);
                var checkFlag=Array.isArray(value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;
                validationMessages.push(fmessage);                
            });            
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
            var fieldErrors=event.getParams().error.data.output.fieldErrors;
            Object.keys(fieldErrors).forEach(function(key){
                var value = fieldErrors[key];
                console.log(key + ':' + value);
                var checkFlag=Array.isArray(value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;                
                validationMessages.push(fmessage);                                
            });
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {            
            var fieldErrors=event.getParams().error.data.output.errors;            
            Object.keys(fieldErrors).forEach(function(key){
                var value = fieldErrors[key];
                console.log(key + ':' + value);
                var checkFlag=Array.isArray(value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;
                validationMessages.push(fmessage);                                
            });            
        }
        cmp.set("v.validationMessagesList",validationMessages);
        console.log('handleError----------->'+validationMessages);        
        console.log("ValidationMessage Retrieve----->"+cmp.get("v.validationMessagesList"));
    },
    cancelAction : function(component, event, helper){
        component.set("v.accVarName","");
        component.set("v.oppVarName","");
        component.set("v.stageVarName","");
        component.set("v.respVarName","");
        component.set("v.OpCoVal","");
        component.set("v.prdVarName","");
        component.set("v.BuVal","");
        component.set("v.prodTeamVal","");
        component.set("v.divisionVal","");
        component.set("v.dMVal","");
        component.set("v.impactedRegionVal","");
        component.set("v.nRPVal","");
        component.set("v.revVal","");
        component.set("v.descpVal","");
        component.set("v.closedDateVal","");
        component.set("v.validationMessagesList",null);
        event.preventDefault();
        helper.redirectToStartURL(component, event, helper);
    },
    reInit : function(component, event, helper) {
        helper.reInit(component, event, helper); 
    },
    deliveryModelVal: function(component, event, helper){        
        var DMVal = component.find("DeliveryModelId").get("v.value");
        var OpVal = component.find("controllerFld").get("v.value");
        if(DMVal == 'Flexible Capacity' && OpVal == 'TEKsystems, Inc.'){
            component.set("v.showTWS",true);
        }else{
            component.set("v.showTWS",false);
        }
    }
})