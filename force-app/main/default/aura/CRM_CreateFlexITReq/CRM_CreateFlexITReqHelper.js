({
    redirectToOpportunityURL : function (component, event, helper) {
        var optyUrl = "/lightning/r/Opportunity/"+component.get("v.newOpportunityId")+"/view";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": optyUrl,
            "isredirect": true
        });
        urlEvent.fire();
    },
    redirectToStartURL : function (component, event, helper){      
        var strtUrl ="/lightning/r/Account/"+component.get("v.parentrecordId")+ "/view";        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": strtUrl,
            "isredirect":true
        });
        urlEvent.fire();        
    },
    reInit : function(component, event, helper){
        /*var UrlQuery = window.location.href;        
        var myPageRef = component.get("v.pageReference");
        var tgsId ='';
        var oppId;
        if(myPageRef){
            oppId  = myPageRef.state.c__recId;
            tgsId = myPageRef.state.tgsOppId;			   
            if(oppId !== null && oppId !== undefined){
                component.set('v.AccountVarName',oppId); 
                component.set("v.recordId", oppId);
                //component.set("v.flexITVal", tgsId);
                component.set("v.account.Id",oppId);
                if(tgsId == 'FlexIT'){                    
                    component.find("DeliveryModelId").set("v.value", 'Flex IT');
                }else{
                    component.find("DeliveryModelId").set("v.value", '');
                }
                var opts = [
                    { "class": "slds-input slds-combobox__input ", label: "Interest", value: "Interest", selected: "true" },
                    { "class": "slds-input slds-combobox__input ", label: "Qualifying", value: "Qualifying" }
                ];                
                component.find("StageID").set("v.options", opts);
                component.set("v.inputoppName","");
                component.set("v.RespType","");
                component.set("v.DeliveryModelId","");
                component.set("v.closeDateVal","");
                component.set("v.BusinessUnitID","");
                component.set("v.DivisionID","");
                component.set("v.ImpactedRegionID","");
                component.set("v.ProductTeamID","");
                component.set("v.NumID","");
                component.set("v.RevenueID","");
                component.set("v.DescriptionID","");
                component.set("v.validationMessagesList",null);
            }
        }*/
        $A.get('e.force:refreshView').fire();
    },
    showToastMessages : function( title, message, type ) {
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });        
        toastEvent.fire();
    }
})