({
doInit: function(component, event, helper){
    
    	var action = component.get("c.getActivityDetail");
    	var actId = component.get("v.recordId");
    
    	console.log("Activity Id",actId);
    
    	action.setParams({
        	"activityId":actId
    	})
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.task", response.getReturnValue());
                console.log("Success",response.getReturnValue());
                
                component.set("v.task.Status","Open");
                component.set("v.task.Description","this is a test");                
                component.set("v.task.Type","Call");

            }
        });        
        
        
    	$A.enqueueAction(action);
	},
saveTask:function(component, event, helper){  
        helper.saveTask(component,event, helper); 
	},
cancelTask : function(component, event, helper){
        component.set("v.errorMessageVal","");
    }
})