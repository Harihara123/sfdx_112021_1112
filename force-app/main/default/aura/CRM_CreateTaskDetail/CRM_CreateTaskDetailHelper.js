({ 
    validateFieldLengths : function(component, event) {
        var validForm = true;

        var limits = [{"field" : "Subject", "limit" : 255, "errorFieldId" : "subjTooLong"}];
        for (var i=0; i<limits.length; i++) {
            var limitedField = component.find(limits[i].field);

            var fieldValue = limitedField.get("v.value");
            var valueLength = (typeof fieldValue !== "undefined") ? fieldValue.length : 0;

            if (valueLength > limits[i].limit) {
                var warningSpanId = component.find(limits[i].errorFieldId);
                $A.util.removeClass(warningSpanId, "toggle");
                validForm = false;
            } else {
                var warningSpanId = component.find(limits[i].errorFieldId);
                $A.util.addClass(warningSpanId, "toggle");
            }
        }

        return validForm;
    },

    validateDateFormats : function (component, task) {
        var valid = true;
        var errText = "Valid date format is " + this.getAcceptedDateFormat();

        if (task.ActivityDate === false) {
            var warningSpanId = component.find("badActivityDate");
            warningSpanId.set("v.value", errText);
            $A.util.removeClass(warningSpanId, "toggle");
            valid = false;
        } else {
            var warningSpanId = component.find("badActivityDate");
            $A.util.addClass(warningSpanId, "toggle");
        }

        return valid;
    },

    validateRequiredFields : function (component, event) {
        var valid = true;
        var requiredFields = [component.find('Subject'), component.find('ActivityDate')];

        if(requiredFields !== undefined){
         for(var i=0; i<requiredFields.length; i++){
           
           if(requiredFields[i] === component.find('Subject') && $A.util.isEmpty(requiredFields[i].get("v.value"))){
              requiredFields[i].set("v.errors", [{message: "Subject is required."}]);
              valid = false;
            } else if (requiredFields[i] === component.find('Subject') && !($A.util.isEmpty(requiredFields[i].get("v.value")))) {
                requiredFields[i].set("v.errors", []);
            }
            if(requiredFields[i] === component.find('ActivityDate') && $A.util.isEmpty(requiredFields[i].get("v.value"))){
              requiredFields[i].set("v.errors", [{message: "Date is required."}]);
              valid = false;
            } else if(requiredFields[i] === component.find('ActivityDate') && !($A.util.isEmpty(requiredFields[i].get("v.value")))){
               requiredFields[i].set("v.errors", []); 
            }

         }
           // S - 32431 - Added by akshay on 10/10/17 - start
            var interval = component.find("RecurrenceInterval");
            var type = component.get("v.task.RecurrenceRegeneratedType");
            if (typeof type !== "undefined" && type !== "") {
                if($A.util.isEmpty(interval.get("v.value"))){
                    valid = false;
                    interval.set("v.errors", [{message: "Recurrence Interval is required."}]); 
                }else if(isNaN(interval.get("v.value")) ){
                    valid = false;
                    interval.set("v.errors", [{message: "Recurrence Interval is invalid."}]); 
                }else{
                    interval.set("v.errors", null);  
                }  
            }
          
          /*if(interval.get('v.validity').valueMissing){
              valid = false;
              interval.showHelpMessageIfInvalid();
          } else {
              interval.set("v.errors", null);
              $A.util.removeClass(interval, "slds-has-error"); // remove red border
              $A.util.addClass(interval, "hide-error-message"); // hide error message
              
          }*/
           // S - 32431 - Added by akshay on 10/10/17 - end  
            
        }
        return valid;

    },
    
    validateRelatedToField : function(component, event){
        var valid = true;

        var relatedToFieldText, 
            oppLkpCmp = component.find("taskOppLookup");
        if (oppLkpCmp) {
            relatedToFieldText = oppLkpCmp.get("v.currentTextVal"); 
        }
        //document.getElementById("WhatId").value;
        var warningSpanId = component.find("relatedFieldNotValid");
        if (relatedToFieldText != null && relatedToFieldText != undefined && relatedToFieldText.length > 0) {
            var task = component.get("v.task");     
            if (task !== null && typeof task.What !== 'undefined' && typeof task.What.Name !== 'undefined') {        
                if (task.What.Name === relatedToFieldText) {
                        $A.util.addClass(warningSpanId, "toggle");
                } else {
                    	valid = false; 
                        $A.util.removeClass(warningSpanId, "toggle");  
                }
            } else {
                valid = false;
                $A.util.removeClass(warningSpanId, "toggle");
            } 
        } else {
        	$A.util.addClass(warningSpanId, "toggle");
        }
        return valid; 
    },
    
    saveTask:function(component, event, helper){  
        
        if (!this.validateFieldLengths(component, event)) {
            return;
        }

        if(!this.validateRequiredFields(component, event)){
            return;
        }
        
        if (!this.validateRelatedToField(component, event)) {
            return;
        } 
        
        component.set("v.shouldDisabled", true);
        var task = component.get("v.task");     
        var saveAction = component.get("c.saveTask");
    
    	saveAction.setParams({
        	"newTask": JSON.stringify(task)
    	})		
        saveAction.setCallback(this, function(response){
            var state = response.getState();
            console.log('state--'+state);;            
            if (state === "SUCCESS") {
                console.log("Inserted");
            }
        });
        $A.enqueueAction(saveAction);
                    
 
    },
    formatDate:function(inputDate){
        var input = inputDate;
        if(input){
             // Typed in dates only work for "/" as delimiter.
            var arr = input.split("/");
            if(arr.length === 3){
                // Pad "0" for single digit date / month
                if(arr[0].length === 1){
                   arr[0] = '0' + arr[0];
                }
                if(arr[1].length === 1){
                   arr[1] = '0' + arr[1];
                }

                // Convert to "yyyy-mm-dd" based on user locale.
                var language = window.navigator.userLanguage || window.navigator.language;
                if(language === 'en-US'){
                    input = arr[2] + '-' + arr[0] + '-' + arr[1];
                }else{
                    input = arr[2] + '-' + arr[1] + '-' + arr[0];
                }
            } 

            // Adjust for timezone since the new Date() constructor sets to UTC midnight.
            var now = this.adjustDateForTimezone(input);
            if (now === false || isNaN(now.getTime())) {
                return false;
            } else {
                return now.getFullYear()+"-"+(now.getMonth() + 1) +"-"+now.getDate();
            } 
        }else{
            return '';
        }
       
    }
    ,formatDateTime:function(inputDateTime){
        var output = inputDateTime;

        if(inputDateTime){

            // Split the input string on space
            var arr = inputDateTime.split(' ');
           

            // Works only with the standard date format. Spaces in date will break this.
            if(arr.length >= 2){
                // Call function to format date part of the string.
                var sDate = this.formatDate(arr[0]);

                // Split time part of the string by colon. 
                var hhmm = arr[1].split(":");
                var hrs, mins, ampm;
                ampm = '';
                if (hhmm.length >= 2) {
                    // Pad single digit hours with "0".
                    hrs = hhmm[0].length === 1 ? "0" + hhmm[0] : hhmm[0];
                    // If am/pm is not separated from time by space, use substring.
                    mins = hhmm[1].substring(0, 2);
                    if (hhmm[1].length > 2) {
                        ampm = hhmm[1].substring(2, hhmm[1].length);
                    }
                }
                // If am/pm separated from time by space
                if (arr.length === 3) {
                    ampm = arr[2];
                }
                // Adjust to 24 hour clock
                if (ampm.toLowerCase() === "pm" && hrs !== "12") {
                    hrs = parseInt(hrs) + 12;
                } else if (ampm.toLowerCase() === "am" && hrs === "12") {
                    hrs = "00";
                }

                // Initialize Date object from generated date and time strings.
                var sTime = " " + hrs + ":" + mins + ":00.000Z";

                // Initialize date and readjust for timezone because previous parsing steps set it to UTC
                var tDate = new Date(sDate + sTime);
                var theDate = new Date(tDate.getTime() + (tDate.getTimezoneOffset() * 60 * 1000));

                // Check for invalid date format. Return boolean false if invalid.
                if (!isNaN(theDate.getTime())) {
                    output = theDate.toISOString();
                } else {
                    output = false;
                }
            } else {
                // Datepicker was used, so input is ISO string and passed right back as output.
                output = inputDateTime;
            }
        }
        return output;
    },
    adjustDateForTimezone : function (inputDate) {
        var theDate = new Date(inputDate);
        if (!isNaN(theDate.getTime())) {
            // Use same date to get the timezone offset so daylight savings is factored in.
            var output = new Date(theDate.getTime() + (theDate.getTimezoneOffset() * 60 * 1000));
            return output;
        } else {
            return false;
        }
    },
        getAcceptedDateFormat : function () {
        var language = window.navigator.userLanguage || window.navigator.language;
        return language === "en-US" ? "mm/dd/yyyy" : "dd/mm/yyyy";
    },

    getAcceptedTimeFormat : function () {
        return "hh:mm AM/PM";
    },

    actvitySaved:function(cmp,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The activity has been saved.",
            "type": "success"
        });
        toastEvent.fire();
    
        var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
        updateLMD.fire();
    }
   
 })