({
    doInit :function(cmp, event, helper) {        
        //Adding w.r.t. S-235972
        var action = cmp.get("c.isGroupMember");
        action.setParams({});
        action.setCallback(this, function(response) {
            if (response.getState() == 'SUCCESS') {                
                cmp.set("v.isPGMember",response.getReturnValue());
                cmp.set("v.editFlag",false);
                cmp.set("v.viewFlag",true);
            }
        });
        $A.enqueueAction(action);
        //End of Adding w.r.t. S-235972
    },
	recordLoadedView :function(cmp, event, helper) {
        //Andy: S-174826 setting variables for Show More/Less component.
        const {recordUi} = event.getParams();
        try {
            let showMore = {};
            helper.SHOWMORE_FIELDS.forEach(field => {
                const value = (recordUi.record.fields[field] || {}).value || '';
                showMore[field] = value;
            })
            cmp.set('v.showMore', showMore);
			//Fetch Related titles on load
			const relatedTitles = recordUi.record.fields.Suggested_Job_Titles__c.value;
			if(relatedTitles) {
				const parsedTitles = JSON.parse(relatedTitles);
				cmp.set("v.initialSuggTitles",parsedTitles);
				let suggTitles = [];
				parsedTitles.forEach(title => {
					if (!title.removed) {
						suggTitles.push(title.term);
					}
				})
				cmp.set("v.suggJobTitles",suggTitles);
				cmp.set("v.initialJobTitle",recordUi.record.fields.Req_Job_Title__c.value);
			}
			//End of Fetch related titles on view
            const skills = recordUi.record.fields.EnterpriseReqSkills__c.value;
            if (skills) {
                const parsedSkills = JSON.parse(skills);
                cmp.set('v.skills', parsedSkills);
                let topSkills = [];
                let secondarySkills = []
                parsedSkills.forEach(skill => {
                    if (skill.favorite) {
                        topSkills.push(skill.name)
                    } else {
                        secondarySkills.push(skill.name)
                    }
                })
                cmp.set('v.topSkills', topSkills);
                cmp.set('v.secondarySkills', secondarySkills);
            }
            //console.log('%cShow More fields','background-color: black; color: aqua;',showMore);

        } catch (err) {
    	//console.log(err)
		}

        //console.log('record view 5');
        var isOnLoad = cmp.get("v.isInit");
        
        try {
			
			var isViewFlag = cmp.get("v.viewFlag");
            //let payload = cmp.get("v.payload");
            if(isViewFlag){
			
				const skills = recordUi.record.fields.EnterpriseReqSkills__c.value;
				if (skills) {
					const parsedSkills = JSON.parse(skills);
					cmp.set('v.skills', parsedSkills);
					let topSkills = [];
					let secondarySkills = []
					parsedSkills.forEach(skill => {
						if (skill.favorite) {
							topSkills.push(skill.name)
						} else {
							secondarySkills.push(skill.name)
						}
					})
					cmp.set('v.topSkills', topSkills);
					cmp.set('v.secondarySkills', secondarySkills);
				}
				
				var userId = $A.get("$SObjectType.CurrentUser.Id");
				var skillAction = cmp.get("c.getAccessDetailsForSkillSet");
				skillAction.setParams({"currentUserId": userId});
				skillAction.setCallback(this, function(response) {
					if (response.getState() == 'SUCCESS') {
						var skillAccess = response.getReturnValue();
						//alert('skillAccess::'+skillAccess);
						cmp.set("v.suggestedSkillsFlag",skillAccess);
					}
				});
				$A.enqueueAction(skillAction);
			
                var jobDescription;
                var addnlQualify;
                var extDesc
                if(event.getParams().recordUi.record.fields.Req_Job_Description__c!=null){
                    jobDescription=event.getParams().recordUi.record.fields.Req_Job_Description__c.value;
                    cmp.set("v.editJobdescription",jobDescription);                    
                }
                
                if(event.getParams().recordUi.record.fields.Req_Qualification__c !=null){
                    addnlQualify=event.getParams().recordUi.record.fields.Req_Qualification__c.value;
                    cmp.set("v.addQualification",addnlQualify);                    
                }
                
                if(event.getParams().recordUi.record.fields.Req_External_Job_Description__c !=null){
                    extDesc=event.getParams().recordUi.record.fields.Req_External_Job_Description__c.value;
                    cmp.set("v.externalCommJobDescriptionText",extDesc);
                }

                if(event.getParams().recordUi.record.fields.Req_Division__c !=null){
                    let division=event.getParams().recordUi.record.fields.Req_Division__c.value;
                    cmp.set("v.req.Req_Division__c",division);
                }
				//Adding w.r.t S-235321 PrimaryBuyer dependent on division
				if(event.getParams().recordUi.record.fields.Product__c !=null){
                    let primaryBuyer=event.getParams().recordUi.record.fields.Product__c.value;
                    cmp.set("v.req.Product__c",primaryBuyer);
                }
				//End of Adding w.r.t S-235321
				//Adding w.r.t Story S-234527
               if(event.getParams().recordUi.record.fields.Req_Manually_Updated_Fields__c !=null){
                    let ManualUpdatedFields=event.getParams().recordUi.record.fields.Req_Manually_Updated_Fields__c.value;
                    cmp.set("v.req.Req_Manually_Updated_Fields__c",ManualUpdatedFields);
                }                  
               //End of Adding w.r.t Story S-234527
				              
            }                       
            var flag=cmp.get("v.viewLoaded");
            cmp.set("v.skillRefreshFlag",true);
            cmp.set("v.jobTitleRefreshFlag",true);
            cmp.set("v.addinsertFlag",true);
           if(flag==false){
               cmp.set("v.viewLoaded",true);
               if(isOnLoad===false){
                   cmp.set("v.editFlag",false);
                   cmp.set("v.viewFlag",true);  
               }
			   
			   var payval = event.getParams().recordUi.record.fields.Req_Payroll__c.value;
               if(payval == true){
                   cmp.set("v.payrollVal", true);
               }
			   
			   var exclusive = event.getParams().recordUi.record.fields.Req_Exclusive__c.value;
               if(exclusive == true){
                   cmp.set("v.exclusiveVal", true);
               }
                              
                let payload = {};
                payload["req_job_title__c"] = recordUi.record.fields.Req_Job_Title__c.value;
                payload["suggested_job_titles__c"] = recordUi.record.fields.Suggested_Job_Titles__c.value;
                payload["opco__c"] = recordUi.record.fields.OpCo__c.value;
                let enterprisereqskills__c = JSON.parse(recordUi.record.fields.EnterpriseReqSkills__c.value);
                let payloadSkills="";
                if(enterprisereqskills__c != null) {
                    for(let x=0; x < enterprisereqskills__c.length; x++) {
                        if(typeof enterprisereqskills__c[x] != undefined && typeof enterprisereqskills__c[x].name != undefined) {
                            payloadSkills += enterprisereqskills__c[x].name + ",";
                        }
                    }
                }
                if(payloadSkills.length > 1)
                    payload["enterprisereqskills__c"] = payloadSkills.substring(0, payloadSkills.length-1);             
                payload["req_job_description__c"] = recordUi.record.fields.Req_Job_Description__c.value;
                payload["req_qualification__c"] = recordUi.record.fields.Req_Qualification__c.value;
                cmp.set("v.payload", payload);   
                if(event.getParams().recordUi.record.fields.Req_Skill_Specialty__c.value != null) {
                    cmp.set("v.req.Req_Skill_Specialty__c", event.getParams().recordUi.record.fields.Req_Skill_Specialty__c.value);
                }
                if(event.getParams().recordUi.record.fields.Functional_Non_Functional__c.value != null) {
                    cmp.set("v.req.Functional_Non_Functional__c", event.getParams().recordUi.record.fields.Functional_Non_Functional__c.value);
                }                
               //Adding w.r.t Story S-234527
                if(event.getParams().recordUi.record.fields.Req_Manually_Updated_Fields__c !=null){
                    let ManualUpdatedFields=event.getParams().recordUi.record.fields.Req_Manually_Updated_Fields__c.value;
                    cmp.set("v.req.Req_Manually_Updated_Fields__c",ManualUpdatedFields);
                }                  
               //End of Adding w.r.t Story S-234527
                
                var recordU=event.getParam("recordUi");
                var prodval=event.getParams().recordUi.record.fields.Req_Product__c.value;
                //console.log('prodtype val rU'+prodval);
				helper.getAddressData(cmp, event, helper);
            
               var actSectsList = cmp.get("v.activeSections"); 
                    // F cth and SI -- Perm
                 if(prodval=='Permanent' ){
                         cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",false);
                     cmp.set("v.contractFlag",false);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                 }else if(prodval=='Contract to Hire'){
                     cmp.set("v.permFlag",false);
                          cmp.set("v.cthFlag",true);
                     cmp.set("v.contractFlag",false);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         }
                 }else{
                          cmp.set("v.cthFlag",false);
                          cmp.set("v.permFlag",false);
                     cmp.set("v.contractFlag",true);
                          
                         if(actSectsList != undefined){
                              //console.log("act sec"+actSectsList);
                              actSectsList.push("F");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                              //console.log("after F sec"+actSectsList);
                         } 
                     }
                    
                    
                    event.setParam("recordUi",recordU);
                  }
        }
        catch(err) {
            //console.log('errors '+err);
        } 
        finally {
            var reqprod =event.getParams().recordUi.record.fields.Req_Product__c.value;            
            cmp.set("v.placementType",reqprod);            
            if(sgn == 'Qualified'){
			   //cmp.set("v.stgQual",true);
			}
            var statusLDSOnLoad;
            var prevStageOnLoad;
            var sgn=event.getParams().recordUi.record.fields.StageName.value;
            if(event.getParams().recordUi.record.fields.LDS_Status__c != null){
                statusLDSOnLoad = event.getParams().recordUi.record.fields.LDS_Status__c.value;
                cmp.set("v.statusLDS",statusLDSOnLoad);
            }
            if(event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c != null){
                prevStageOnLoad = event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c.value;
                cmp.set("v.prevStage",prevStageOnLoad);
            }            
            if(statusLDSOnLoad == 'Open'){
                if(sgn == 'Draft'){
                    cmp.set("v.StageName",'Draft');
                }else{
                    cmp.set("v.StageName",'Qualified');
                }
            }else if(statusLDSOnLoad == 'Closed'){
                if(prevStageOnLoad == 'Draft' && sgn.substring(0,6) == 'Closed'){
                    cmp.set("v.StageName",'Draft');
                }/*else if(prevStageOnLoad == 'Staging' && sgn.substring(0,6) == 'Closed'){
                    cmp.set("v.StageName",'Staging');
                }*/else{
                    cmp.set("v.StageName",'Qualified');
                }
                
            }       
        }
    },
	
    recordLoadedEdit :function(cmp, event, helper) {
        //console.log('edit pressed 25'); 
        var btn = event.getSource();
        var isOnLoad = cmp.get("v.isInit");
        //console.log('btn pressed '+btn); 
        var editLoadedFlag=cmp.get("v.editLoaded");
         if(editLoadedFlag==false){
             if(isOnLoad===false){
                 cmp.set("v.editFlag",false);
                 cmp.set("v.viewFlag",true);  
             }
             
             //alert('Record edit Load');
             
             
          
             
             if(event.getParams().recordUi.record.fields.Req_Terms_of_engagement__c != null){
                 var opts = [];
                 //alert('Inside Check'+event.getParams().recordUi.record.fields.Req_Terms_of_engagement__c.value.trim().length);
                 var termVal = event.getParams().recordUi.record.fields.Req_Terms_of_engagement__c.value;
                 //alert(termVal);
                 if(termVal == null || termVal === "" || typeof(termVal) == "undefined"){ //(termVal !== null && termVal.trim().length===0)){
                     opts.push( { value: "", label: "--None--",selected:true });
                     opts.push( { value: "Contingent", label: "Contingent" });
                     opts.push( { value: "Retained", label: "Retained" });
                 }else if(termVal == 'Retained'){
                     opts.push( { value: "", label: "--None--" });
                     opts.push( { value: "Contingent", label: "Contingent" });
                     opts.push( { value: "Retained", label: "Retained",selected:true });
                 }else if(termVal == 'Contingent'){
                     opts.push( { value: "", label: "--None--" });
                     opts.push( { value: "Contingent", label: "Contingent",selected:true });
                     opts.push( { value: "Retained", label: "Retained" });
                 }else if(termVal != 'Retained' && termVal != 'Contingent' ){
                     opts.push( { value: "", label: "--None--" });
                     opts.push( { value: "Contingent", label: "Contingent" });
                     opts.push( { value: "Retained", label: "Retained" });
                     opts.push({ value: termVal, label: termVal,selected:true });
                 }
                 cmp.set("v.options", opts);
             }
             
			 if(event.getParams().recordUi.record.fields.Req_Opportunity_Owner__c!=null){
                 setTimeout(
                     $A.getCallback(function() {
                         var emeaOwnerEditVar = cmp.find("emeaOwneridE");
                         if(emeaOwnerEditVar != null){
                             cmp.find("emeaOwneridE").set("v.value",event.getParams().recordUi.record.fields.OwnerId.value);  
                         }
                     }),1000
                 ); 
             }
             
             var sgn;
             var statusLDS;
             var prevStage;
             if(event.getParams().recordUi.record.fields.StageName != null){
             	sgn=event.getParams().recordUi.record.fields.StageName.value;
             }    
            //New Qualifying Stage Changes
             if(event.getParams().recordUi.record.fields.LDS_Status__c != null){
             	statusLDS = event.getParams().recordUi.record.fields.LDS_Status__c.value;
             }
             if(event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c != null){
            	prevStage = event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c.value;
             } 
            if(event.getParams().recordUi.record.fields.Req_Product__c != null){
				cmp.set("v.placementtypeLoadValue",event.getParams().recordUi.record.fields.Req_Product__c.value);
            }
            
             if(cmp.find("acctId") != undefined){
                var aeroAcc = cmp.find("acctId");
                var flg=Array.isArray(aeroAcc);
                var acctId = event.getParams().recordUi.record.fields.AccountId.value;
                //console.log("Acc id"+acctId);
                if(flg){
                     //console.log("acc is array");
                     aeroAcc[0].set("v.value",acctId);
                    }else{
                      //console.log("not an array");
                      aeroAcc.set("v.value",acctId);
                   }
             }
         }
    },
    handleError : function(cmp,event,helper){
        //console.log('error reached '); 
        cmp.set("v.editDisable",false);
        var params = event.getParams();
        var Validation = false;
        var validationMessages=[];
        
         if(params.output!=null && params.output.fieldErrors!=null ){
           
            var stgErrors=params.output.fieldErrors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      var checkFlag=Array.isArray(value);
                      //console.log(key + ':' + value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                Validation = true;
                });
         
        }
        
        if(params.output!=null && params.output.errors!=null ){
           
            var stgErrors=params.output.errors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      //console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                Validation = true;
                });
         
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
        var fieldErrors=event.getParams().error.data.output.fieldErrors;
        Object.keys(fieldErrors).forEach(function(key){
  				      var value = fieldErrors[key];
   					  //console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
            
            		  validationMessages.push(fmessage);
            Validation = true;
            
			});
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {
            
            var fieldErrors=event.getParams().error.data.output.errors;
            
            Object.keys(fieldErrors).forEach(function(key){
  				  var value = fieldErrors[key];
   					 //console.log(key + ':' + value);
                   var checkFlag=Array.isArray(value);
            	   var fmessage=(checkFlag==true)?value[0].message:value.message;
            		  validationMessages.push(fmessage);
                Validation = true;
            
			});
            
        }
        //console.log('handleError----------->'+validationMessages);	
        	
        //console.log("ValidationMessage Retrieve----->"+cmp.get("v.validationMessagesList"));
        
    },
    
    onSubmit  : function(cmp, event, helper) {
        cmp.set("v.showSpinner", true); 
        //console.log('on submit click ');
        event.preventDefault();
        var error_count; 
        var Validation = true;
        cmp.set("v.editDisable",true);
        var eventFields = event.getParam("fields");
        
        var fieldMessages=[];
        var validated = true;
        //helper.validateAddress(cmp,event);
		eventFields=helper.validateReqAddr(cmp,event,helper,eventFields);
        var addressErrors=cmp.get("v.addFieldMessagedMap");
        if(typeof addressErrors!='undefined' && addressErrors!=null && Object.keys(addressErrors).length>0){
            validated=false;
        }
        
        var workSiteOpp=cmp.get("v.Opportunity");
        var addFieldMessages=cmp.get("v.addFieldMessage");
        
        if(typeof addFieldMessages!='undefined' && addFieldMessages!=null && addFieldMessages.length>0){
            fieldMessages=fieldMessages.concat(addFieldMessages);
            validated=false;
        }else{
            fieldMessages=fieldMessages.concat(addFieldMessages)
            //eventFields.Req_Worksite_Street__c=workSiteOpp.Req_Worksite_Street__c;
			//eventFields.Req_Worksite_City__c=workSiteOpp.Req_Worksite_City__c;
			//eventFields.Req_Worksite_State__c=workSiteOpp.Req_Worksite_State__c;
			//eventFields.Req_Worksite_Country__c=workSiteOpp.Req_Worksite_Country__c;
            //eventFields.StreetAddress2__c=workSiteOpp.StreetAddress2__c;
            //eventFields.Req_Worksite_Postal_Code__c=workSiteOpp.Req_Worksite_Postal_Code__c;
            //eventFields.Req_GeoLocation__Latitude__s=workSiteOpp.Req_GeoLocation__Latitude__s;
            //eventFields.Req_GeoLocation__Longitude__s=workSiteOpp.Req_GeoLocation__Longitude__s;
        }
       //Related Titles
		var prepareRelatedTitles = cmp.find("relatedTitles");
		prepareRelatedTitles.prepareRelatedTerms();
		let relatedTitles = cmp.get("v.finalRelatedTitlesList");
		if(relatedTitles.length > 0){
			eventFields.Suggested_Job_Titles__c = JSON.stringify(relatedTitles);
		} else {
			eventFields.Suggested_Job_Titles__c = null;
		}

        //helper.prepareSkills(cmp, event);
        cmp.set("v.loopFlag",false);
        //var finalSkillList=cmp.get("v.finalskillList");
        var jsonSkills=cmp.get("v.stringSkills");
        var eventFields = event.getParam("fields");
        if (jsonSkills) {
            eventFields.EnterpriseReqSkills__c=jsonSkills;
        }
        
        //Job Description
        var desc = cmp.get("v.editJobdescription");
        eventFields.Req_Job_Description__c = desc ;

        if(eventFields.Req_Job_Description__c != null)
        {
            eventFields.Description=eventFields.Req_Job_Description__c;
        } 
       
        //Additional Qualifications
        var quali = cmp.get("v.addQualification");
		
        eventFields.Req_Qualification__c = quali ;
        
		//ExtDesc
        var descExt = cmp.get("v.externalCommJobDescriptionText");
        eventFields.Req_External_Job_Description__c = descExt ;
		
        //Stage
        var dsn=cmp.find('tekdetailStage');
        var stgName =dsn.get("v.value");
        if(eventFields.StageName =='Draft' || eventFields.StageName =='Qualified'){
            eventFields.StageName =stgName;
        }
        
		var terms = cmp.get("v.selectedValue");
        eventFields.Req_Terms_of_engagement__c = terms ;
		
        if(eventFields.Req_Payroll__c == true){
            cmp.set("v.payrollVal", true); 
        }else{
            cmp.set("v.payrollVal", false);
        }
        
        if(eventFields.Req_Exclusive__c == true){
            cmp.set("v.exclusiveVal", true); 
        }else{
            cmp.set("v.exclusiveVal", false);
        }

        //S-234531 start        
        if(cmp.find("experienceLevelId").get("v.value") != null && typeof cmp.find("experienceLevelId").get("v.value") != undefined) {
            eventFields.Req_Job_Level__c = cmp.find("experienceLevelId").get("v.value");
        }        
        eventFields.Req_Skill_Specialty__c = cmp.get("v.req.Req_Skill_Specialty__c");
        eventFields.Product__c = cmp.get("v.req.Product__c")=='--None--'?'':cmp.get("v.req.Product__c");
        eventFields.Functional_Non_Functional__c = cmp.get("v.req.Functional_Non_Functional__c");
        if(cmp.get("v.SSApiModel") != null && cmp.get("v.SSApiModel") != "" && typeof cmp.get("v.SSApiModel") != undefined)
            eventFields.Req_RVT_Occupation_Code__c = cmp.get("v.SSApiModel").RVT.Occupation_Code;
        eventFields.Req_ONet_SOC_Description__c = cmp.get("v.req.Req_ONet_SOC_Description__c");
        eventFields.Soc_onet__c = cmp.get("v.req.Soc_onet__c");
        eventFields.Req_RVT_Premium_Skills__c = cmp.get("v.req.Req_RVT_Premium_Skills__c");
        eventFields.Skill_Set__c = cmp.get("v.skillset");
        //S-234531 end
        //Adding w.r.t Story S-234527
        eventFields.Req_Manually_Updated_Fields__c = cmp.get("v.req.Req_Manually_Updated_Fields__c");
        //End of Adding w.r.t Story S-234527


		eventFields.OwnerId = eventFields.Req_Opportunity_Owner__c; 
		var jobtitle = cmp.get("v.jobtitle[0].name");
        if(typeof(jobtitle) != "undefined" && jobtitle !=null)
        eventFields.Req_Job_Title__c = jobtitle;         
        cmp.set("v.descriptionVarName", "");
		cmp.set("v.OpptyOwnerVarName","");
		cmp.set("v.flatFeeValMsg", "");
        eventFields.AccountId=cmp.find("acctId").get("v.value");
		//eventFields.AccountId=cmp.find("acctId").get("v.value");
        var stgName = cmp.find("tekdetailStage").get("v.value");
        cmp.set("v.StageName",stgName); 
        var placeType = cmp.find("productId").get("v.value");
        cmp.set("v.placementType",placeType);
        var reqValidator = cmp.find('validator');
        var validations = reqValidator.validateRequiredFields(cmp.get("v.StageName"),cmp.get("v.placeType"));
        cmp.set("v.validationResponse", validations );
        var validatedcheck = cmp.get("v.validationResponse.isvalid");
        if(validatedcheck == false){
            validated = validatedcheck;
            if(validated == false && typeof(cmp.get("v.focus_id")) != "undefined" && cmp.get("v.focus_id") !=null){
                document.getElementById( cmp.get("v.focus_id") ).scrollIntoView({ behavior: 'instant', block: 'center' });
                error_count = cmp.get("v.validationResponse.error_count");       
            }			
        }else if(validatedcheck == true){
            error_count = 0;
            var jobDescription=cmp.find("jobDescription").get("v.value");
            if(cmp.get("v.StageName") != "" && cmp.get("v.StageName") != null && typeof cmp.get("v.StageName") != undefined &&
                cmp.get("v.StageName") != 'Draft' && typeof(jobDescription) !="undefined" && 
                jobDescription !=null && jobDescription !='' && jobDescription.length < 25){
                cmp.set("v.descriptionVarName", "Job Description should have at least 25 characters.");
                validated = false;
                error_count++;
            }
            var OpptyOwner = cmp.find("emeaOwneridE").get("v.value");
            if(typeof(OpptyOwner) != "undefined" && (OpptyOwner == null || OpptyOwner == '')){
                cmp.set("v.OpptyOwnerVarName","Opportunity Owner is required");
                validated = false;
                error_count++;
            }
			if(cmp.get("v.StageName") =='Qualified' && cmp.get("v.placementType") == 'Permanent'){ 
                var percentaFeeValue = cmp.find("inputFeePerc").get("v.value");
                var flatFeeValue = cmp.find("inputFlatFee").get("v.value");
                if((typeof percentaFeeValue == "undefined" || percentaFeeValue == null || percentaFeeValue == "") && (typeof flatFeeValue == "undefined" || flatFeeValue == null || flatFeeValue == "")){
                    cmp.set("v.flatFeeValMsg", "Fee % or Flat Fee is required.");
                    validated=false;
                    error_count++;
                }else if((typeof flatFeeValue == "undefined" || flatFeeValue == null || flatFeeValue == "") && (typeof percentaFeeValue == "undefined" || percentaFeeValue == null || percentaFeeValue == "")){
                    cmp.set("v.flatFeeValMsg", "Fee % or Flat Fee is required.");
                    validated=false;
                    error_count++;
                }            
            }
        }
        event.setParam("fields", eventFields);
        
        if(validated){
           cmp.set("v.insertFlag",true);
           cmp.set("v.addinsertFlag",false);
           cmp.find('emeaEditForm').submit(eventFields); 
            //cmp.set("v.skillRefreshFlag",false);
            //cmp.set("v.jobTitleRefreshFlag",false);
        }else{
            Validation = true;
            //console.log('count '+fieldMessages.length);
            if(error_count > 0){
                var message = 'There are ' + error_count + ' errors that need to be addressed before creating this req in a ' + cmp.get("v.StageName") + ' state.';
                helper.showToast(message, 'Req Validation Error', 'error');
            }
            cmp.set("v.editDisable",false);
            //cmp.set("v.validationMessagesList",fieldMessages); 
            if(typeof(error_count) !="undefined" && error_count == 0)
            cmp.set("v.Validation",Validation);
        }
        cmp.set("v.showSpinner", false); 
    },
    handleOpportunitySaved :function(cmp, event,helper){
        
    	var params = event.getParams();
 		
        cmp.set("v.validationMessagesList",null);
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true); 
        cmp.set("v.cancelFlag",true);
        
        var positionRefereshEvt = $A.get('e.c:E_RefreshPositionCard');
        positionRefereshEvt.fire();
   
    },
    preventProcessing : function (cmp,event,helper){
        event.preventDefault();
        event.stopPropagation();
        //console.log('Prevent method.....');
    },
    onAccountChange :function (cmp,event,helper){
       var acId=event.getParams().value;
       cmp.find("acctId").set("v.value",acId);
    },
    onProductChange :  function (cmp,event,helper){
      var eventFields = event.getParams();
			 var prodval=eventFields.value;
         if(prodval=='Permanent' ){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",false);
             cmp.set("v.contractFlag",false);
         }else if(prodval=='Contract to Hire'){
             cmp.set("v.permFlag",false);
              cmp.set("v.cthFlag",true);
             cmp.set("v.contractFlag",false);
         }else{
             cmp.set("v.cthFlag",false);
              cmp.set("v.permFlag",false);
             cmp.set("v.contractFlag",true);
         }
		cmp.set("v.placementType",prodval);
        var stgName = cmp.find("tekdetailStage").get("v.value");
		cmp.set("v.StageName",stgName);
		var validatefalg = cmp.find("validator");
		validatefalg.validateRequiredEditFields(cmp.get("v.StageName"),cmp.get("v.placeType"));
    },
    handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');
        
		//alert(openSections.length);
        cmp.set("v.activeSections",openSections);
        //alert(cmp.get("v.activeSections")+'Section Togle'+openSections);
        //component.set("v.activeSections",openSections);
        //console.log(component.get("v.activeSections")+'#openSections#'+openSections[0]);
        if (openSections.length === 0) {
            //cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            //cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    navigateToError: function(component, event, helper){
        var elmnt = document.getElementById("editpageid");
        window.scrollTo(0,200);
	}, 
    enterHandler: function(component, event, helper) {
     component.set("v.lookupJTFlag",!component.get("v.lookupJTFlag"));
     component.set("v.loopFlag",true);
    },
    changeJobTitle :function (component, event, helper){
        
        var globalLovId=event.getParams().value;
        var tempRec = component.find("recordLoader");
        tempRec.set("v.recordId",globalLovId[0]);
        tempRec.reloadRecord();
    },
    recordUpdated:function (component, event, helper){
        helper.recordUpdated(component, event, helper);
    },
    buttonHandleCancel : function (cmp,event,helper){
		cmp.set("v.validationMessagesList",null);
		cmp.set("v.stageErrorVarName","");
		cmp.set("v.jobTitleErrorVarName","");
		cmp.set("v.hiringManagerErrorVarName","");
		cmp.set("v.accountErrorVarName","");
		cmp.set("v.draftReasonVarName","");
		cmp.set("v.officeErrorVarName","");
		cmp.set("v.plmtTypeErrorVarName","");
		cmp.set("v.streetErrorVarName","");
		cmp.set("v.cityTypeErrorVarName","");
		cmp.set("v.stateErrorVarName","");
		cmp.set("v.countryVarName","");
		cmp.set("v.zipErrorVarName","");
		cmp.set("v.currencyErrorVarName","");
		cmp.set("v.skillPillValidationError","");
        cmp.set("v.totalPositionsVarName","");
		cmp.set("v.Validation",false);
        cmp.set("v.editDisable",false);
        cmp.set("v.cancelJTFlag",true);
        cmp.set("v.addFieldMessage",null);
        cmp.set("v.addFieldMessagedMap",null);
		cmp.set("v.descriptionVarName", "");
        cmp.set("v.OpptyOwnerVarName","");
		cmp.set("v.flatFeeValMsg", "");
        helper.setDefaultAddressData(cmp,event,helper);
		var reqValidator = cmp.find('validator');
        var validations = reqValidator.clearRequiredFieldsOnCancel();
		if(cmp.find("productId") !="undefined"){
            if(cmp.get("v.placementtypeLoadValue") == 'Contract'){
                cmp.set("v.contractFlag",true);
                cmp.set("v.cthFlag",false);
                cmp.set("v.permFlag",false);
            }else if(cmp.get("v.placementtypeLoadValue") == 'Permanent'){
                cmp.set("v.permFlag",true);
                cmp.set("v.cthFlag",false);
                cmp.set("v.contractFlag",false);
            }else if(cmp.get("v.placementtypeLoadValue") == 'Contract to Hire'){
                cmp.set("v.cthFlag",true);
                cmp.set("v.permFlag",false);
                cmp.set("v.contractFlag",false);
            }
        }
        cmp.set("v.editFlag",false);
		cmp.set("v.viewFlag",true);  
		cmp.set("v.cancelFlag",true);
		event.preventDefault();
    },
    changeJobTitleHandler :function(cmp,event,helper){
        let payload = cmp.get("v.payload");
        let jbTitle=cmp.get("v.jobtitle");
        if(jbTitle!=null && jbTitle.length>0){
			let jobtitle = jbTitle[0].name;
            if(payload != null) {
                payload["req_job_title__c"] =  jobtitle;
            } else {
                payload = {};
                payload["req_job_title__c"] =  jobtitle;
            }            
            cmp.set("v.payload", payload);
        	cmp.set("v.suggestedjobTitle",jobtitle);
			if(!cmp.get("v.viewFlag")) {
				helper.fetchRelatedJobTitles(cmp,event,jobtitle);
			}
            helper.skillSpecialtyClassifierCall(cmp, event);
        }    
    },
    
    showEditModal : function(component, event, helper) {
        var appEvent = $A.get('e.c:E_PositionModal');
        appEvent.fire();
    },
    
     refereshPositionData : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    handleEditClick : function(component, event, helper){        
       //alert('Hii'+event.getSource().get("v.name"));
       var buttonClick = event.getSource().get("v.name");
       component.set('v.clickedSection', buttonClick);
	   component.set("v.activeSections",[]);
            var sectionVar = ['A','B','C','D','E','G'];
			component.set("v.activeSections",sectionVar);

       //console.log(component.get("v.activeSections"));
       var model = component.get("v.EnterpriseReqInitializedModel");
       if(!model || typeof model == undefined || model == null) {
            helper.getSkillSpecialtyOptions(component, event);
            helper.getFNFOptions(component, event);
       }
       
       let editFormWidth = component.find("emeaFormContainer").getElement().clientWidth;
       component.set("v.containerWidth", editFormWidth);
       component.set("v.editFlag",true);
       component.set("v.viewFlag",false);
        component.set("v.editDisable",false);
        helper.skillSpecialtyClassifierCall(component, event);
          setTimeout(
                $A.getCallback(function() {
                    //var activeStatusVar = component.get("v.activeSections");
                    //activeStatusVar.includes('A');
                    if(buttonClick == 'A'){
                        var accountInfoeditVar = document.getElementById("accountInfoEdit")
                        if(accountInfoeditVar != null){
                            var yPosition = document.getElementById("accountInfoEdit").offsetTop;
                            window.scrollTo(0,yPosition-50);
                            component.set("v.isFocus1", true);
    
                        }
                        //var accountIdVar = component.find("emeadetailAcctId");
                        //$A.util.addClass(accountIdVar, "custom_focus");
                    }else if(buttonClick == 'B'){
                        var overviewCriteriaEditVar = document.getElementById("overviewCriteriaEdit")
                        if(overviewCriteriaEditVar != null){
                            var yPosition = document.getElementById("overviewCriteriaEdit").offsetTop;
                            window.scrollTo(0,yPosition-50);
                            component.set("v.isFocus2", true);
                        }
                       
                    }else if(buttonClick == 'C'){
                        var positionSummaryEditVar = document.getElementById("positionSummaryEdit");
                        if(positionSummaryEditVar != null){
                            var yPosition = document.getElementById("positionSummaryEdit").offsetTop;
                            window.scrollTo(0,yPosition-50);
                            component.set("v.isFocus3", true);
                         }
                        //var prodTypeVar = component.find("aerodetailReqProductType");
                        //$A.util.addClass(prodTypeVar, "custom_focus");
                        //$A.util.removeClass(prodTypeVar, 'custom_focus');
                    }else if(buttonClick == 'D'){
                        var businessQualificationEditVar = document.getElementById("businessQualificationEdit");
                        if(businessQualificationEditVar != null){
                            var yPosition = document.getElementById("businessQualificationEdit").offsetTop;
                            window.scrollTo(0,yPosition);
                        }
                        //textAreaVar.focus();
                        //component.set("v.isFocus4", true);
                    }else if(buttonClick == 'E'){
                        var systemInfoEditVar = document.getElementById("systemInfoEdit");
                        if(systemInfoEditVar != null){
                            var yPosition = document.getElementById("systemInfoEdit").offsetTop;
                            window.scrollTo(0,yPosition);
                            component.set("v.isFocus5", true); 
                        }
                    }
					var opts;                      
                    if(component.get("v.statusLDS") == 'Open'){                        
                        if(component.get("v.StageName") == 'Draft'){
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                            ];
                        }  
                        else{
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                            ];
                        }
                    }
                    //Closed Req
                    if(component.get("v.statusLDS") == 'Closed'){          
                        if(component.get("v.prevStage") =='Draft' && component.get("v.StageName").substring(0,6) == 'Closed'){
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                            ];
                        }
                        else if(component.get("v.prevStage") == 'Staging' && component.get("v.StageName").substring(0,6)=='Closed'){
                            opts = [
                                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" },
                                { "class": "slds-input slds-combobox__input ", label: "Staging", value: "Staging" ,selected: "true"}
                            ];
                        }                
                            else{
                                opts = [
                                    { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                                    { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                                ];
                            }
                    }                    
                    if(component.find("tekdetailStage") != undefined){
                        var stgVw = component.find("tekdetailStage");
                        var flag=Array.isArray(stgVw);
                        if(flag){
                            //console.log("stageV array");
                            stgVw[0].set("v.options", opts);
                        }else{
                            //console.log("not array");
                            //component.find("tekdetailStage").set("v.options", opts);
                            //component.find("tekdetailStage").set("v.value", stgVw.get("v.value"));
                            component.set("v.stageList", opts);
                            component.set("v.qualifyStage", stgVw.get("v.value"))
                        } 
                    }                  
                }), 4000
            );
        
            setTimeout(
                $A.getCallback(function() {
                    component.set('v.isFocus1', false);
                    component.set('v.isFocus2', false);
                    component.set('v.isFocus3', false);
                    component.set('v.isFocus4', false);
                    component.set('v.isFocus5', false);
                }),20000
            );
        if(window.location.href.indexOf("view?sv=") > -1) {
            if(component.find("label-dateOpnPos")) {
               $A.util.removeClass(component.find("label-dateOpnPos"), "nowrap-whitespace"); 
            }            
        }
        else {
            if(component.find("label-dateOpnPos")) {
               $A.util.addClass(component.find("label-dateOpnPos"), "nowrap-whitespace"); 
            }
        }
    },
     //Call by aura:waiting event  
    handleShowSpinner: function(component, event, helper) {
        component.set("v.showSpinner", true); 
    },
    
    //Call by aura:doneWaiting event 
    handleHideSpinner : function(component,event,helper){
        component.set("v.showSpinner", false);
    },
     onStageChange : function(component,event,helper){
		var stgName = component.find("tekdetailStage").get("v.value");
		component.set("v.StageName",stgName); 
		var placeType = component.find("productId").get("v.value");
		component.set("v.placementType",placeType);
		var validatefalg = component.find("validator");
		validatefalg.validateRequiredEditFields(component.get("v.StageName"),component.get("v.placeType"));
	},
    handleTopSkillsChange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.topSkills', data.skills)
      const secSkills = cmp.get('v.secondarySkills');
      const dupes = data.skills.filter(skill => secSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.topSkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
      cmp.set('v.topSkills', data.skills);
      h.constructFinalSkills(cmp);
      let payload = cmp.get("v.payload");
      let enterprisereqskills__c = cmp.get("v.topSkills");
      let payloadSkills="";
      for(let x=0; x < enterprisereqskills__c.length; x++) {
        if(typeof enterprisereqskills__c[x] != undefined) {
            payloadSkills += enterprisereqskills__c[x] + ",";
        }
      }
      if(payloadSkills.length > 1) {
        if(payload != null) {
            payload["enterprisereqskills__c"] = payloadSkills.substring(0, payloadSkills.length-1);
        } else {
            payload = {};
            payload["enterprisereqskills__c"] = payloadSkills.substring(0, payloadSkills.length-1);
        }
      }               
      cmp.set("v.payload, payload");
      h.skillSpecialtyClassifierCall(cmp,e);
    },
    handleSecSkillsChange: function(cmp, e, h) {
        const data = e.getParams();
        const topSkills = cmp.get('v.topSkills');
        const dupes = data.skills.filter(skill => topSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                const suggestedSkills = cmp.get('v.simpleSuggestedSkills')
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.secondarySkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
        cmp.set('v.secondarySkills', data.skills)
        h.constructFinalSkills(cmp)
    },
    handlesuggestedskillschange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.simpleSuggestedSkills', data.pills);
    },
    setSuggestedSkills: function(cmp, e, h) {
        const suggestedSkills = cmp.get('v.initialSuggestedSkills');
        if (suggestedSkills) {
            cmp.set('v.simpleSuggestedSkills', suggestedSkills);
        }
    },
    handleDupeMsg: function(cmp, e, h) {
        h.showToast(`${e.getParams().val} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')        
    },
    //Adding w.r.t S-179672 Page Refresh
    destroyComp: function(component, event, helper) {
        component.destroy();
    },
	handleSuggestedTitlesChange:function(cmp,event,helper) {
		
		const data = event.getParams();
		cmp.set("v.isUpdatedRelatedTitles",true);
		cmp.set("v.updatedSuggestedJobTitles",data.skills);
		//call suggested skills api
		if(!$A.util.isUndefined(data.skills) || !$A.util.isEmpty(data.skills)) {
			cmp.set("v.suggTitlesForSkills",data.skills.join());            
		}        
	},
	relatedTitleHandler: function(cmp,event,helper) {
		console.log('suggJobTitles'+cmp.get('v.suggJobTitles'));
		let titles = cmp.get('v.suggJobTitles')
		if(titles != []) {			
			cmp.set("v.suggTitlesForSkills",titles.join());
            let payload = cmp.get("v.payload"); 
            if(payload != null) {
                payload["suggested_job_titles__c"] = cmp.get("v.suggTitlesForSkills");
            } else {
                payload = {};
                payload["suggested_job_titles__c"] = cmp.get("v.suggTitlesForSkills");
            }                    
            cmp.set("v.payload, payload");	
            helper.skillSpecialtyClassifierCall(cmp, event);		
		}        
    },
    setSkills : function(cmp,event,helper) {
        const allSkills = cmp.get('v.skills');
        cmp.set('v.allSelectedSkills', allSkills);
    },
    onJobDescChange : function(component, event, helper){		
        let jbDesc=component.find("jobDescription").get("v.value");
        if(jbDesc!=null && jbDesc!='undefined' && jbDesc!='' ){	
            let payload = component.get("v.payload");
            if(payload != null) {
                payload["req_job_description__c"] = jbDesc;
            } else {
                payload = {};
                payload["req_job_description__c"] = jbDesc;
            }            
            component.set("v.payload, payload");
			helper.skillSpecialtyClassifierCall(component, event);
		}
	},
    onAddQualificationChange : function(component, event, helper){		
        let addQual=component.find("qualiText").get("v.value");
        if(addQual!=null && addQual!='undefined' && addQual!='' ){	
            let payload = component.get("v.payload");
            if(payload != null) {
                payload["req_qualification__c"] = addQual;
            } else {
                payload = {};
                payload["req_qualification__c"] = addQual;
            }            
            component.set("v.payload, payload");
			helper.skillSpecialtyClassifierCall(component, event);
		}
	},
    //Adding w.r.t Story S-234527
    SSblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Req_Skill_Specialty__c");        
    },
    FNFblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Functional_Non_Functional__c");        
    },
    EXPblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Req_Job_Level__c");        
    },
    SkillSetblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Skill_Set__c");
    },
	//End of Adding w.r.t Story S-234527
	onDivisionChange: function(component, event, helper){
        var eventFields = event.getParams();
		var divValue=eventFields.value;
        component.set("v.req.Req_Division__c",divValue);
        var model=component.get("v.EnterpriseReqInitializedModel");
        component.set("v.req.Product__c", "");
        if(model != null && model != undefined){
            helper.PrimaryBuyerDependsDivision(component, event, model);
            helper.skillSpecValues(component, event, model.skillSpecialtyDivisionDepdentMappings);
        } else {
            helper.getSkillSpecialtyOptions(component, event);
        }     
    }
})