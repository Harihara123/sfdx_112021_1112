({
    SHOWMORE_FIELDS: [
        'Req_Job_Description__c',
        'Req_Qualification__c',
        'Req_External_Job_Description__c',
        'Req_Business_Challenge__c',
        'Req_EVP__c',
        'Req_Interview_Information__c',
        'Req_RemoteWork_Education_Info__c'
    ],
    prepareSkills: function(cmp, event) {
        var skillsList = cmp.get("v.skills");
        var finalSkillList = [];
        var i;
        for (i = 0; i < skillsList.length; i++) {
            var skillObj = skillsList[i];
            var newSkill = {
                "name": skillObj.name,
                "favorite": skillObj.favorite
            };
            finalSkillList.push(newSkill);
        }
        cmp.set("v.finalskillList", finalSkillList);
    },
    recordUpdated: function(component, event, helper) {

        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {
            /* handle error; do this first! */ } else if (changeType === "LOADED") {
            var sRec = component.get("v.simpleRecord");
            component.set("v.lookupJobTitle", sRec.Name);

        }

    },
    validateAddress: function(cmp, event) {
        var oppAddress = cmp.get("v.Opportunity");
        var fieldMessages = [];
        var addFielMessagedMap = {};
        /* Mani Commented 4/9/2020
     if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
        // cmp.set("v.streetErrorVarName","Street cannot be blank.");
         fieldMessages.push("Street cannot be blank.");
         addFielMessagedMap['streetErrorVarName']="Street cannot be blank.";
       }else{
         cmp.set("v.streetErrorVarName","");
      }
    
     
     if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
        // cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
         fieldMessages.push("City cannot be blank.");
         addFielMessagedMap['cityTypeErrorVarName']="City cannot be blank.";
       }else{
         cmp.set("v.cityTypeErrorVarName","");
      } Mani Commented 4/9/2020  */

        /*   if(oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == ''){
               //cmp.set("v.stateErrorVarName","State cannot be blank.");
               fieldMessages.push("State cannot be blank.");
               addFielMessagedMap['stateErrorVarName']="State cannot be blank.";
             }else{
               cmp.set("v.stateErrorVarName","");
            }
         */

        /* Mani Commented 4/9/2020
     if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
        //cmp.set("v.countryVarName","Country cannot be blank.");
        fieldMessages.push("Country cannot be blank.");
        //addFielMessagedMap.set("countryVarName","Country cannot be blank.");
        addFielMessagedMap['countryVarName']="Country cannot be blank.";
       }else{
         cmp.set("v.countryVarName","");
      }
     
      if((typeof oppAddress.Req_Worksite_State__c == 'undefined' || oppAddress.Req_Worksite_State__c == null ||
        oppAddress.Req_Worksite_State__c == '') && 
        
       (oppAddress.Req_Worksite_Country__c == 'United States' || oppAddress.Req_Worksite_Country__c == 'Canada'))
        {
        //alert('In If');
        fieldMessages.push("State cannot be blank.");
        addFielMessagedMap['stateErrorVarName']="State cannot be blank.";
            }
        else{
        cmp.set("v.stateErrorVarName","");
        }
    
     if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
         //cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
         fieldMessages.push("Postal Code cannot be blank.");
         //addFielMessagedMap.set("zipErrorVarName","Postal Code cannot be blank.");
         addFielMessagedMap['zipErrorVarName']="Postal Code cannot be blank.";
      }else{
         cmp.set("v.zipErrorVarName","");
      }
      Mani Commented 4/9/2020  */

        if (typeof fieldMessages != 'undefined' && fieldMessages != null && fieldMessages.length > 0) {
            cmp.set("v.insertFlag", true);
            cmp.set("v.addFieldMessage", fieldMessages);
            cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
            var testmap = cmp.get("v.addFieldMessagedMap");
            console.log('Field Map--->' + testmap);
        } else {
            cmp.set("v.insertFlag", false);
            cmp.set("v.addFieldMessage", fieldMessages);
            cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
        }

    },
    validateReqAddr: function(cmp, event, helper, eventFields) {
        var oppAddress = cmp.get("v.req");
        var addFielMessagedMap = {};
        if (oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == '') {
            addFielMessagedMap['streetErrorVarName'] = "Street is required.";
        } else {
            cmp.set("v.streetErrorVarName", "");
        }
        if (oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == '') {
            addFielMessagedMap['cityTypeErrorVarName'] = "City is required.";
        } else {
            cmp.set("v.cityTypeErrorVarName", "");
        }
        if (oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == '') {
            addFielMessagedMap['countryVarName'] = "Country is required.";
        } else {
            cmp.set("v.countryVarName", "");
        }
        //Adding w.r.t Story S-179373
        if ((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && (oppAddress.Req_Worksite_Country__c == 'United States' || oppAddress.Req_Worksite_Country__c == 'Canada')) {
            //if(oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') {
            addFielMessagedMap['stateErrorVarName'] = "State is required.";
        } else {
            cmp.set("v.stateErrorVarName", "");
        }
        if (oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == '') {
            addFielMessagedMap['zipErrorVarName'] = "Postal Code is required.";
        } else {
            cmp.set("v.zipErrorVarName", "");
        }
        if (typeof addFielMessagedMap != 'undefined' && addFielMessagedMap != null && Object.keys(addFielMessagedMap).length > 0) {
            cmp.set("v.insertFlag", true);
            addFielMessagedMap['locationErrorVarName'] = "Location not found you need to add it manually or lookup another location.";
            cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
            var testmap = cmp.get("v.addFieldMessagedMap");
            return eventFields;
        } else {
            cmp.set("v.insertFlag", false);
            cmp.set("v.addFieldMessagedMap", addFielMessagedMap);
            eventFields = this.saveAddressData(cmp, event, helper, eventFields);
            return eventFields;
        }
    },
    saveAddressData: function(cmp, event, helper, eventFields) {
        eventFields.Req_Worksite_Street__c = cmp.get("v.req.Req_Worksite_Street__c");
        eventFields.Req_Worksite_City__c = cmp.get("v.req.Req_Worksite_City__c");
        eventFields.Req_Worksite_State__c = cmp.get("v.req.Req_Worksite_State__c");
        eventFields.Req_Worksite_Postal_Code__c = cmp.get("v.req.Req_Worksite_Postal_Code__c");
        eventFields.Req_Worksite_Country__c = cmp.get("v.req.Req_Worksite_Country__c");
        eventFields.StreetAddress2__c = cmp.get("v.req.StreetAddress2__c");
        //for default address data
        cmp.set("v.reqDefaultAddress.Req_Worksite_Street__c", cmp.get("v.req.Req_Worksite_Street__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_City__c", cmp.get("v.req.Req_Worksite_City__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_State__c", cmp.get("v.req.Req_Worksite_State__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_Postal_Code__c", cmp.get("v.req.Req_Worksite_Postal_Code__c"));
        cmp.set("v.reqDefaultAddress.Req_Worksite_Country__c", cmp.get("v.req.Req_Worksite_Country__c"));
        cmp.set("v.reqDefaultAddress.StreetAddress2__c", cmp.get("v.req.StreetAddress2__c"));
        return eventFields;
    },
    getAddressData: function(cmp, event, helper) {
        cmp.set("v.req.Req_Worksite_Street__c", event.getParams().recordUi.record.fields.Req_Worksite_Street__c.value);
        cmp.set("v.req.Req_Worksite_City__c", event.getParams().recordUi.record.fields.Req_Worksite_City__c.value);
        cmp.set("v.req.Req_Worksite_State__c", event.getParams().recordUi.record.fields.Req_Worksite_State__c.value);
        cmp.set("v.req.Req_Worksite_Postal_Code__c", event.getParams().recordUi.record.fields.Req_Worksite_Postal_Code__c.value);
        cmp.set("v.req.Req_Worksite_Country__c", event.getParams().recordUi.record.fields.Req_Worksite_Country__c.value);
        //for default address data        
        cmp.set("v.reqDefaultAddress.Req_Worksite_Street__c", event.getParams().recordUi.record.fields.Req_Worksite_Street__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_City__c", event.getParams().recordUi.record.fields.Req_Worksite_City__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_State__c", event.getParams().recordUi.record.fields.Req_Worksite_State__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_Postal_Code__c", event.getParams().recordUi.record.fields.Req_Worksite_Postal_Code__c.value);
        cmp.set("v.reqDefaultAddress.Req_Worksite_Country__c", event.getParams().recordUi.record.fields.Req_Worksite_Country__c.value);

        if (event.getParams().recordUi.record.fields.StreetAddress2__c.value != undefined) {
            cmp.set("v.req.StreetAddress2__c", event.getParams().recordUi.record.fields.StreetAddress2__c.value);
            cmp.set("v.reqDefaultAddress.StreetAddress2__c", event.getParams().recordUi.record.fields.StreetAddress2__c.value);
        }
        this.displayLocation(cmp, event, helper);
    },
    setDefaultAddressData: function(cmp, event, helper) {
        cmp.set("v.req.Req_Worksite_Street__c", cmp.get("v.reqDefaultAddress.Req_Worksite_Street__c"));
        cmp.set("v.req.Req_Worksite_City__c", cmp.get("v.reqDefaultAddress.Req_Worksite_City__c"));
        cmp.set("v.req.Req_Worksite_State__c", cmp.get("v.reqDefaultAddress.Req_Worksite_State__c"));
        cmp.set("v.req.Req_Worksite_Postal_Code__c", cmp.get("v.reqDefaultAddress.Req_Worksite_Postal_Code__c"));
        cmp.set("v.req.Req_Worksite_Country__c", cmp.get("v.reqDefaultAddress.Req_Worksite_Country__c"));

        if (cmp.get("v.reqDefaultAddress.StreetAddress2__c") != undefined) {
            cmp.set("v.req.StreetAddress2__c", cmp.get("v.reqDefaultAddress.StreetAddress2__c"));
        }
    },
    displayLocation: function(component, event, helper) {
        var req = component.get("v.req");
        var displayAddr = "";
        if (req.Req_Worksite_Street__c != null && req.Req_Worksite_Street__c != "") {
            displayAddr = req.Req_Worksite_Street__c;
        }
        if (req.Req_Worksite_City__c != null && req.Req_Worksite_City__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_City__c : req.Req_Worksite_City__c;
        }
        if (req.Req_Worksite_State__c != null && req.Req_Worksite_State__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_State__c : req.Req_Worksite_State__c;
        }
        if (req.Req_Worksite_Postal_Code__c != null && req.Req_Worksite_Postal_Code__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_Postal_Code__c : req.Req_Worksite_Postal_Code__c;
        }
        if (req.Req_Worksite_Country__c != null && req.Req_Worksite_Country__c != "") {
            displayAddr = displayAddr != "" ? displayAddr + ", " + req.Req_Worksite_Country__c : req.Req_Worksite_Country__c;
        }
        component.set("v.presetLocation", displayAddr);
    },
    constructFinalSkills: function(cmp) {
        const topSkills = cmp.get('v.topSkills');
        const topSuggestedSkills = cmp.find('crmSkillV').getElement().inUseSuggestedSkills; //lwcReqSkills stores suggested skills in this variable
        const formattedTopSkills = topSkills.map((skill, i) => { //iterating over top skills and checking against suggested skills to mark them true
            const normalizedSuggested = topSuggestedSkills.map(s => s.toLowerCase());
            return {
                name: skill,
                favorite: true,
                index: i,
                suggestedSkill: normalizedSuggested.includes(skill.toLowerCase())
            }
            // if (normalizedSuggested.includes(skill.toLowerCase())) {
            //     //{"name":skillName,"favorite":false,"index":i,"suggestedSkill":true};
            //     return {name: skill, favorite: true, index: i, suggestedSkill: true }
            // }
            // return {name: skill, favorite: true, index: i, suggestedSkill: false}
        })

        const secSkills = cmp.get('v.secondarySkills');
        const secSuggestedSkills = cmp.find('lwcSecSkills').getElement().inUseSuggestedSkills;
        const formattedSecSkills = secSkills.map((skill, i) => {
            const normalizedSuggested = secSuggestedSkills.map(s => s.toLowerCase());
            return {
                name: skill,
                favorite: false,
                index: topSkills.length + i,
                suggestedSkill: normalizedSuggested.includes(skill.toLowerCase())
            }
            // if (normalizedSuggested.includes(skill.toLowerCase())) {
            //     //{"name":skillName,"favorite":false,"index":i,"suggestedSkill":true};
            //     return {name: skill, favorite: false, index: topSkills.length + i, suggestedSkill: true }
            // }
            // return {name: skill, favorite: false, index: topSkills.length + i, suggestedSkill: false}
        })

        const formattedSkills = [...formattedTopSkills, ...formattedSecSkills]
        cmp.set('v.stringSkills', JSON.stringify(formattedSkills));
        //set skills to make sure selected skills are excluded
        cmp.set('v.skills', formattedSkills);
    },
    showToast: function(message, title, toastType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },
    fetchRelatedJobTitles: function(cmp, event, jobTitle) {
        //Related titles
        if (jobTitle != null) {
            var relatedTitles = cmp.find("relatedTitles");
            relatedTitles.fetchRelatedTerms(jobTitle, "AG_EMEA");
        }
    },
    skillSpecialtyClassifierCall: function(component, event) {
        console.log(component.get("v.payload"));
        var callAPI = component.get("v.SSCAPIinProgress") == true ? false : true && component.get("v.editFlag");
        let payload = component.get("v.payload");
        if (component.get("v.callSkillSpecialtyAPI") == true && callAPI && payload != null &&
            payload["req_job_title__c"] != null && typeof payload["req_job_title__c"] != undefined &&
            payload["req_job_description__c"] != null && typeof payload["req_job_description__c"] != undefined &&
            payload["enterprisereqskills__c"] != null && typeof payload["enterprisereqskills__c"] != undefined) {
            var action = component.get("c.fetchSkillSpecialtyWrapper");
            console.log(payload);
            action.setParams({
                "payload": JSON.stringify(payload)
            });
            component.set("v.SSCAPIinProgress", true);
            action.setCallback(this, function(response) {
                var data = response;
                if (data.getState() == 'SUCCESS') {
                    var model = data.getReturnValue();
                    component.set("v.SSApiModel", model);
                    console.log(model);
                    this.updateFieldsFromWrapper(component, event, model);
                    //Adding w.r.t Story S-234527
                    var updatedFields = component.get("v.req.Req_Manually_Updated_Fields__c");
                    if (updatedFields == undefined || !updatedFields.includes('Skill_Set__c')) {
                        this.updateSkillSet(component, event, model);
                    }
                    //End of Adding w.r.t Story S-234527
                    //Adding w.r.t S-228904
                    this.setReqMetricsData(component, event);
                    //End of Adding w.r.t S-228904
                }
                component.set("v.SSCAPIinProgress", false);
            });
            $A.enqueueAction(action);
        }
    },
    skillSpecValues: function(component, event, model) {
        let division = component.get("v.req.Req_Division__c");
        var skillArray = [];
        var skillMap = model[division];
        var skillSpecialty = component.get("v.req.Req_Skill_Specialty__c");
        let APIFlag = true;

        if (skillSpecialty != "" && skillSpecialty != null && typeof skillSpecialty != undefined) {
            skillArray.push({
                label: '--None--',
                value: '',
                selected: false
            });
        } else {
            skillArray.push({
                label: '--None--',
                value: '',
                selected: true
            });
        }
        for (let key in skillMap) {
            skillArray.push({
                value: skillMap[key],
                label: skillMap[key],
                selected: skillMap[key] == skillSpecialty
            });
            if (skillMap[key] == skillSpecialty) {
                APIFlag = false;
            }
        }
        if (APIFlag && skillSpecialty != "" && skillSpecialty != null && typeof skillSpecialty != undefined) {
            skillArray.push({
                value: skillSpecialty,
                label: skillSpecialty,
                selected: true
            });
        }

        component.set("v.specialityList", skillArray);
    },
    getSkillSpecialtyOptions: function(component, event) {
        var action = component.get("c.getSkillSpecialtyPicklists");
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue().skillSpecialtyDivisionDepdentMappings;
                //Adding w.r.t S-235321 PrimaryBuyer dependent on division
                //let EnterpriseReqInitializedModel=data.getReturnValue();
                component.set("v.EnterpriseReqInitializedModel", data.getReturnValue());
                this.PrimaryBuyerDependsDivision(component, event, component.get("v.EnterpriseReqInitializedModel"));
                //End of Adding w.r.t S-235321
                this.skillSpecValues(component, event, model);
            }
        });
        $A.enqueueAction(action);
    },
    getFNFOptions: function(component, event) {
        var funcPositionArray = [];
        var fnf = component.get("v.req.Functional_Non_Functional__c");
        funcPositionArray.push({
            value: '',
            key: '--None--',
            selected: true
        });
        funcPositionArray.push({
            value: 'Functional',
            key: 'Functional',
            selected: 'Functional' === fnf
        });
        funcPositionArray.push({
            value: 'Non-Functional',
            key: 'Non-Functional',
            selected: 'Non-Functional' === fnf
        });
        component.set("v.funcPositionList", funcPositionArray);
    },
    updateFieldsFromWrapper: function(component, event, model) {
        //Adding w.r.t Story S-234527
        var updatedFields = component.get("v.req.Req_Manually_Updated_Fields__c");

        if (updatedFields == undefined || !updatedFields.includes('Req_Skill_Specialty__c')) {
            if (model.skill_specialty.label) {
                component.set("v.req.Req_Skill_Specialty__c", model.skill_specialty.label);
                this.getSkillSpecialtyOptions(component, event);
            }
        }
        if (updatedFields == undefined || !updatedFields.includes('Functional_Non_Functional__c')) {
            if (model.functional.functional) {
                if (model.functional.functional == 'Functional') {
                    component.set("v.req.Functional_Non_Functional__c", "Functional");
                } else if (model.functional.functional == 'Not-Functional') {
                    component.set("v.req.Functional_Non_Functional__c", "Non-Functional");
                }
                this.getFNFOptions(component, event);
            }
        }
        if (updatedFields == undefined || !updatedFields.includes('Req_Job_Level__c')) {
            if (model.RVT.Experience_level) {
                if (model.RVT.Experience_level == '0') {
                    component.find("experienceLevelId").set("v.value", 'Entry Level');
                } else if (model.RVT.Experience_level == '1') {
                    component.find("experienceLevelId").set("v.value", 'Intermediate Level');
                } else if (model.RVT.Experience_level == '2') {
                    component.find("experienceLevelId").set("v.value", 'Expert Level');
                } else {
                    component.find("experienceLevelId").set("v.value", model.RVT.Experience_level);
                }
            }
        }
        //End of Adding w.r.t Story S-234527
        if (model.onet_job_code) {
            component.set("v.req.Req_ONet_SOC_Description__c", model.onet_job_code.onet_soc_description);
            component.set("v.req.Soc_onet__c", model.onet_job_code.onet_soc_code);    
        }
        if (model.RVT && model.RVT.RVT_detail){
            component.set("v.req.Req_RVT_Premium_Skills__c", model.RVT.RVT_detail.premium_fields);
            component.set("v.req.Req_RVT_Occupation_Code__c",model.RVT.Occupation_Code);
            component.set("v.req.Req_RVT_Occupation_Code_Confidence__c",model.RVT.Occupation_confidence_value);
            component.set("v.req.Req_RVT_Experience_Level_Confidence__c",model.RVT.Experience_level_confidence_value);
        }
    },
    updateSkillSet: function(component, event, model) {
        let skillSetModel = model.skillset.skillset_classifier.maxSkillsetClassifier;
        if (skillSetModel && component.find("skillSetEdit")) {
            component.find("skillSetEdit").set("v.value", skillSetModel);
            component.set("v.skillset", skillSetModel);
        }
        component.set("v.req.Skill_Set__c",skillSetModel);
    },
    //Adding w.r.t S-228904
    setReqMetricsData: function(component, event) {
        //Adding w.r.t Story S-234527
        var model = component.get("v.SSApiModel");
        let ExpLevel;
        if (model.RVT.Experience_level) {
            if (model.RVT.Experience_level == '0') {
                ExpLevel = 'Entry Level';
            } else if (model.RVT.Experience_level == '1') {
                ExpLevel = 'Intermediate Level';
            } else if (model.RVT.Experience_level == '2') {
                ExpLevel = 'Expert Level';
            } else {
                ExpLevel = model.RVT.Experience_level;
            }
        }
        //End of Adding w.r.t Story S-234527
        component.set("v.reqMetrics.Skill_Speciality_From_Model__c", model.skill_specialty.label);
        component.set("v.reqMetrics.F_NF_From_Model__c", model.functional.functional == 'Functional' ? 'Functional' : 'Non-Functional');
        component.set("v.reqMetrics.Skillset_From_Model__c", model.skillset.skillset_classifier.maxSkillsetClassifier);
        component.set("v.reqMetrics.Experience_Level_From_Model__c", ExpLevel);
        component.set("v.reqMetrics.Job_Title__c", component.get("v.jobtitle")[0].name);
        component.set("v.reqMetrics.Job_Description__c", component.find("jobDescription").get("v.value"));
        component.set("v.reqMetrics.OpptyId__c", component.get("v.recordId"));
        var reqMetrics = component.find('reqMetrics');
        reqMetrics.saveMetricDataOnCreate();
    },
    //End of Adding w.r.t S-228904
    //Adding w.r.t Story S-234527  
    ManuallyUpdatedFields: function(component, event, helper, APIName) {
        var updatedFields = component.get("v.req.Req_Manually_Updated_Fields__c");
        if (updatedFields != undefined && updatedFields != null && !updatedFields.includes(APIName)) {
            updatedFields = updatedFields + ',' + APIName;
        }
        if (updatedFields == undefined || updatedFields == null || updatedFields == '') {
            updatedFields = APIName;
        }
        component.set("v.req.Req_Manually_Updated_Fields__c", updatedFields);
    },
    //End of Adding w.r.t Story S-234527
    //Adding w.r.t S-235321 PrimaryBuyer dependent on division
    PrimaryBuyerDependsDivision: function(component, event, model) {
        var productPicklistArray = [];
        var productPicklistInfo;
        if (component.get("v.req.Product__c") == null || component.get("v.req.Product__c") == undefined) {
            productPicklistInfo = "";
        } else {
            productPicklistInfo = component.get("v.req.Product__c");
        }
        productPicklistArray.push({
            value: '--None--',
            key: '--None--',
            selected: true
        });
        if (component.get("v.req.Req_Division__c") != '--None--' && component.get("v.req.Req_Division__c") != undefined) {
            let productPicklistMap = model.PrimaryBuyerMapping;
            for (var key in productPicklistMap[component.get("v.req.Req_Division__c")]) {
                productPicklistArray.push({
                    value: productPicklistMap[component.get("v.req.Req_Division__c")][key],
                    key: productPicklistMap[component.get("v.req.Req_Division__c")][key],
                    selected: productPicklistMap[component.get("v.req.Req_Division__c")][key] === productPicklistInfo
                });
            }
        } else {
            productPicklistInfo = "";
        }
        component.set("v.productListPicklist", productPicklistArray);
        component.set("v.req.Product__c", productPicklistInfo);
    }
    //End of Adding w.r.t S-235321
})