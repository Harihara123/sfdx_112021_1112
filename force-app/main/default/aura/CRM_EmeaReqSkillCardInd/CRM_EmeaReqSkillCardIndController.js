({
	doInit : function(component, event, helper) {
        var fcmp=component.find('recordLoader');
        fcmp.set("v.recordId", component.get("v.recordId"));
        component.set("v.displaySkill",true);
        let emtyMap={};
        component.set("v.skillsMap",emtyMap);
        component.set("v.duplicateSkillError","");          
   },
    onFavourite : function(component, event, helper) {        
        console.log('IN onFavourite');
        var ctarget = event.currentTarget;
    	var pillId = ctarget.id;
        var skillsList=component.get("v.skills");
        let skillsMap=component.get("v.skillsMap");
        var i;
        component.set("v.duplicateSkillError","");
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==pillId){
              skillObj.favorite=!skillObj.favorite;
        	}
            skillsList[i]=skillObj;
            skillsMap[skillObj.name.toUpperCase()]=skillObj;
              
       }
        component.set("v.skills",skillsList);
        component.set("v.skillsMap",skillsMap);
        event.preventDefault();  
     },
     removePill: function (component, event, helper) {
         console.log('IN changeHandler');
        var skillIndex=event.getSource().get("v.name");
        var skillsList=component.get("v.skills");
        let skillsMap=component.get("v.skillsMap");
        var i;
        component.set("v.duplicateSkillError","");
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==skillIndex){
             if(skillsMap.hasOwnProperty(skillObj.name.toUpperCase())==true){
                delete skillsMap[skillObj.name.toUpperCase()];
                console.log('SkillsMap after delete---->'+JSON.stringify(skillsMap));
                skillsList.splice(i,1);
                 if(skillObj.suggestedSkill==true){ 
                	let txObj={"name":skillObj.name, "favorite":false,"index":skillIndex,"suggestedSkill":true,"action":'remove'};
                	component.set("v.txSkill",txObj);
                 }     
              }
        	}
  	    }
        component.set("v.skills",skillsList);
        component.set("v.skillsMap",skillsMap);
        event.preventDefault();              
    },
    recordUpdated : function(component, event, helper) {	
		var changeType = event.getParams().changeType;
		//Neel - Not a good practice to change the default error to forcefully loaded. 
		//But no other good option. Don't know why component getting data using LDS even if skills can be pass from CreateEnterpriseReq.cmp.
		//Now skills value has been passed from CreateEnterpriseReq.cmp for creating opportunity from Proactive Submittal(TLP) .  
		if(component.get("v.psSkills") !== undefined && component.get("v.psSkills") !== null && component.get("v.psSkills") !== '[]'  && component.get("v.psSkills").length !== 0) {  //Neel- psSkills attribute is populating while Opportunity is creating from Proactive Submittal
				changeType = 'LOADED';
		}
        if (changeType === "ERROR") { /* handle error; do this first! */ }
        else if (changeType === "LOADED" || changeType === "CHANGED") {           
           var sRec=component.get("v.oppRecord");
           var indexVl=0;
		   var skillsT;
		if(component.get("v.psSkills") !== undefined && component.get("v.psSkills") !== null && component.get("v.psSkills") !== '[]' &&  component.get("v.psSkills").length !== 0) {  
			skillsT =component.get("v.psSkills");
		}
		else {
				skillsT=sRec.EnterpriseReqSkills__c;
		}
        
        //Adding w.r.t D-12783
        helper.validateTopAndSecondarySkills(component, event, helper, skillsT);
           
         /*  let skillsMap=component.get("v.skillsMap");
    
        if(typeof skillsT!='undefined' && skillsT!=null && skillsT!='[]' ){
           var skillsList=JSON.parse(skillsT);
           var dbList=[];
            if( typeof skillsList!='undefined' && skillsList != null ){
               var i;
                for(i=0;i<skillsList.length;i++){
                    var dbobj=skillsList[i];
                   if(skillsMap.hasOwnProperty(dbobj.name.toUpperCase())==false){
                      skillsMap[dbobj.name.toUpperCase()]=skillObj;
                      var skillObj = {"name":dbobj.name, "favorite":dbobj.favorite,"index":i,"suggestedSkill":false};
                      dbList.push(skillObj);
                   	  component.set("v.duplicateSkillError","");
                   }else{
                      component.set("v.duplicateSkillError","Skill already exists");
                      setTimeout(function(){ component.set("v.duplicateSkillError",""); }, 2000);
                   } 
                }
                 component.set("v.skills",dbList);
                 component.set("v.skillsMap",skillsMap);
            }
            }*/
      	console.log("Record is loaded successfully.");
        }
	   
    },  

	 topSkillUpdateHandler: function(component, event, helper) {
        var topSkills=component.get("v.TopSkills");
        var skillsList=component.get("v.skills");
		var skillsSize=component.get("v.skills").length-component.get("v.SecondarySkills").length;
        var tsize=topSkills.length;
		var favorite=true;
        //var Lsize=skillsList.length;
        if(tsize > skillsSize && tsize > 0){
            if(topSkills[tsize-1]!=null && topSkills[tsize-1].value!=undefined && topSkills[tsize-1].value!=""){
        		//helper.addTopSkill(component, event, helper);
				helper.addSkill(component, event, helper, favorite);
            }            
        }
        if(tsize < skillsSize && tsize >= 0){
            helper.removeSkill(component, event, helper, favorite);
        }
     },

	 SecondarySkillUpdateHandler: function(component, event, helper) {
        var SecondarySkills=component.get("v.SecondarySkills");
        var skillsList=component.get("v.skills");
		var skillsSize=component.get("v.skills").length-component.get("v.TopSkills").length;
        var secSkillSize=SecondarySkills.length;
        var Lsize=skillsList.length;
		var favorite=false;
        if(secSkillSize > skillsSize && secSkillSize > 0){
            if(SecondarySkills[secSkillSize-1]!=null && SecondarySkills[secSkillSize-1].value!=undefined && SecondarySkills[secSkillSize-1].value!=""){
        		helper.addSkill(component, event, helper, favorite);
            }            
        }
        if(secSkillSize < skillsSize && secSkillSize >= 0){
            helper.removeSkill(component, event, helper, favorite);
        }
     },

    keyPressHandler: function(component, event, helper) {
         var glov=component.get("v.glov");
         if(glov!=null && glov.Text_Value__c!=undefined && glov.Text_Value__c!=""){
                 console.log('On click Value selectd--->'+glov.Text_Value__c);
                 var indexVl=0;
                 var skillsList=component.get("v.skills");
                 let skillsMap=component.get("v.skillsMap");
                 if(skillsList == null || typeof skillsList=='undefined'){
                        skillsList=[];
                        indexVl=0;
                 }else{
                     //indexVl=skillsList.length+1;
                     let mapLength=Object.keys(skillsMap).length;
                     let valuesArray=Object.values(skillsMap);
                     if(valuesArray.length>0){
                        indexVl=valuesArray[valuesArray.length-1].index+1; 
                     }
                 }
                 
                 if(skillsMap.hasOwnProperty(glov.Text_Value__c.toUpperCase())==false){
                   let skillObj = {"name":glov.Text_Value__c,"favorite":false,"index":indexVl,"suggestedSkill":false};
                   skillsList.push(skillObj);
                   skillsMap[skillObj.name.toUpperCase()]=skillObj;
                   component.set("v.duplicateSkillError","");
                 }else{
                    component.set("v.duplicateSkillError","Skill already exists");
                    setTimeout(function(){ component.set("v.duplicateSkillError",""); }, 2000);
                 }
                 component.set("v.skills",Array.from(skillsList));
                 component.set("v.skillsMap",skillsMap);  
                 console.log('Skills After set in keypresshandler---->'+component.get("v.skills"));
                 component.set("v.createPill",false);
                 //component.set("v.glov.Text_Value__c",null);
        }
     },
    txSkillsHandler :function(component,event,helper){
        let txObj=component.get("v.txSkill");
        console.log('TxSkillsHandler----->'+txObj);
        if(txObj!=null && txObj!=undefined && txObj.name!='' && txObj.action=='add'){
                 var indexVl=0;
                 var skillsList=component.get("v.skills");
                 let skillsMap=component.get("v.skillsMap");
                 if(skillsList == null || typeof skillsList=='undefined'){
                        skillsList=[];
                        indexVl=0;
                 }else{
                     //indexVl=skillsList.length+1;
                     let mapLength=Object.keys(skillsMap).length;
                     let valuesArray=Object.values(skillsMap);
                     if(valuesArray.length>0){
                        indexVl=valuesArray[valuesArray.length-1].index+1; 
                     }
                 }            	 
           		//Adding w.r.t S-168146       		
            	var getSkills = component.get("v.skills");
            	var skillFound = false;            
            	if(getSkills != null && typeof getSkills !='undefined'){                
                    for(var i=0; i<getSkills.length; i++){
                        if(getSkills[i].name == txObj.name){
                            skillFound=true;
                            break;
                        }
                    }
                }
            	var updatedSkills= txObj.favorite == true?component.get("v.TopSkills"):component.get("v.SecondarySkills");
            	var updatedSkillSize=updatedSkills.length;
            	if(skillFound==false){ 
                    component.set("v.isSuggestedSkill",txObj.suggestedSkill);
                    
            		updatedSkills.push({"key":updatedSkillSize,
                           "value":txObj.name});                    
                    if(txObj.favorite == true){
                		component.set("v.TopSkills",updatedSkills);
            		}else{
                		component.set("v.SecondarySkills",updatedSkills);
            		} 
                  	component.set("v.duplicateSkillError","");
                  }else{
                    component.set("v.duplicateSkillError","Skill already exists");
                    setTimeout(function(){ component.set("v.duplicateSkillError",""); }, 2000);
                  }            	          
            	//End of Adding w.r.t S-168146               
        }    
    }	
})