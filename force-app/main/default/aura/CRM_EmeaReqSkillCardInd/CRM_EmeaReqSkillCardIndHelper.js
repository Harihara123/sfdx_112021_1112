({
	prepareSkills: function(cmp,event){
        var skillsList=cmp.get("v.skills");
        var finalSkillList=[];
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            var newSkill = {"name":skillObj.name, "favorite":skillObj.favorite};
            finalSkillList.push(newSkill);
        }
        cmp.set("v.finalskillList",finalSkillList);
    },
    isExistingSkill: function(cmp,event){
        let skillsList=cmp.get("v.skills");
        
    },    
	addSkill:function(component, event, helper, favorite){
    	var updatedSkills= favorite == true?component.get("v.TopSkills"):component.get("v.SecondarySkills");
		var skillsList=component.get("v.skills");
    	var updatedSkillSize=updatedSkills.length;
        var Lsize=skillsList.length;
        
       			var indexVl=0;
                 var skillsList=component.get("v.skills");
                 let skillsMap=component.get("v.skillsMap");
                 if(skillsList == null || typeof skillsList=='undefined'){
                        skillsList=[];
                        indexVl=0;
                 }else{
                     //indexVl=skillsList.length+1;
                     let mapLength=Object.keys(skillsMap).length;
                     let valuesArray=Object.values(skillsMap);
                     if(valuesArray.length>0){
                        indexVl=valuesArray[valuesArray.length-1].index+1; 
                     }
                 }
                 
                 if(skillsMap.hasOwnProperty(updatedSkills[updatedSkillSize-1].value.toUpperCase())==false){
                   //let skillObj = {"name":updatedSkills[updatedSkillSize-1].value,"favorite":favorite,"index":indexVl,"suggestedSkill":false};
                   let skillObj = {"name":updatedSkills[updatedSkillSize-1].value,"favorite":favorite,"index":indexVl,"suggestedSkill":component.get("v.isSuggestedSkill")};
                   skillsList.push(skillObj);
                   skillsMap[skillObj.name.toUpperCase()]=skillObj;
                   component.set("v.duplicateSkillError","");
                   component.set("v.skills",Array.from(skillsList));
                   component.set("v.skillsMap",skillsMap);
                 }else{
                    component.set("v.duplicateSkillError","Skill already exists");
					var duplicateSkill=updatedSkills[updatedSkillSize-1].value;	
                     var duplicateSkillKey=updatedSkills[updatedSkillSize-1].key;
					delete updatedSkills[updatedSkillSize-1];
					updatedSkills.splice(updatedSkillSize-1,1);
					var test= updatedSkills;
					if(favorite == true){							
						component.set("v.TopSkills",updatedSkills);                        
                        duplicateSkill=duplicateSkill+' '+$A.get("$Label.c.ATS_ALREADY_EXISTS");
					}
					else{						
						component.set("v.SecondarySkills",updatedSkills);                                                
                        duplicateSkill=duplicateSkill+' '+$A.get("$Label.c.ATS_ALREADY_EXISTS");
					}
									
					this.showToastError(component, event, duplicateSkill);
                    //setTimeout(function(){ component.set("v.duplicateSkillError",""); }, 2000);
                 }                   
                 console.log('Skills After set in keypresshandler---->'+component.get("v.skills"));
                 component.set("v.createPill",false);
        		 component.set("v.isSuggestedSkill",false);       	
	},
    removeSkill:function(component, event, helper, favorite){
        console.log('IN changeHandler');
        var skillIndex;
		var updatedSkills= favorite == true?component.get("v.TopSkills"):component.get("v.SecondarySkills");
		var otherSkills= favorite == true?component.get("v.SecondarySkills"):component.get("v.TopSkills");
        var skillsList=component.get("v.skills");
        let skillsMap=component.get("v.skillsMap");
        var i;
        component.set("v.duplicateSkillError","");
        for ( i = 0; i < skillsList.length; i++) {
			var skillToRemove=true;
            var temp=skillsList[i].name;
            for (var t = 0; t < updatedSkills.length; t++) {
				if(updatedSkills[t].value == skillsList[i].name){
					skillToRemove = false;
					break;
				}
			}
			if(skillToRemove == true){
				for (var t = 0; t < otherSkills.length; t++) {
					if(otherSkills[t].value == skillsList[i].name){
						skillToRemove = false;
						break;
					}
				}			
			}

			if(skillToRemove == true){
				skillIndex=skillsList[i].index;
				var skillObj=skillsList[i];
				if(skillObj.index==skillIndex){
				//if(skillObj.name.contains(skillIndex){
					if(skillsMap.hasOwnProperty(skillObj.name.toUpperCase())==true){
						delete skillsMap[skillObj.name.toUpperCase()];
						console.log('SkillsMap after delete---->'+JSON.stringify(skillsMap));
						skillsList.splice(i,1);
                        component.set("v.skills",skillsList);
        				component.set("v.skillsMap",skillsMap);
						if(skillObj.suggestedSkill==true){                            
                			let txObj={"name":skillObj.name, "favorite":favorite,"index":component.get("v.suggestedSkills").length,"suggestedSkill":true,"action":'remove'};
                			component.set("v.txSkill",txObj);
						}     
					}
        		}
			}
        }        
    },
	showToastError : function(component,event,duplicateSkill) {
        var toastEvent = $A.get("e.force:showToast");
	    toastEvent.setParams({
			message: duplicateSkill,
	        type:'error',
	        //title:'Duplicate Value'
			title:$A.get("$Label.c.ATS_Duplicate_Value")
	    });
	    toastEvent.fire(); 
	},
    
    //Adding w.r.t D-12783
    validateTopAndSecondarySkills:function(component, event, helper, skillsT){
    	if(typeof skillsT!='undefined' && skillsT!=null && skillsT!='[]' ){
           var skillsList=JSON.parse(skillsT);
           var dbListTop=[];
           var dbListSec=[];
           var finalSkills=component.get("v.skills"); 
           if( typeof skillsList!='undefined' && skillsList != null && finalSkills.length == 0){
				var i;
            	for(i=0;i<skillsList.length;i++){
                    var dbobj=skillsList[i];
                    //var skillObj = {"name":dbobj.name, "favorite":dbobj.favorite,"index":i,"suggestedSkill":false};                   
                    if(dbobj.favorite == true){
                        dbListTop.push({"key":i,"value":dbobj.name});
                        component.set("v.TopSkills",dbListTop);
                    }
                    else{
                        dbListSec.push({"key":i,"value":dbobj.name});
                        component.set("v.SecondarySkills",dbListSec);
                    }                  
                }
                component.set("v.duplicateSkillError","");          
            }
        }
    }    
})