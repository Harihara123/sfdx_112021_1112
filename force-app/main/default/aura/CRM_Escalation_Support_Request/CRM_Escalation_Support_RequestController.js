({
    doInit: function(component, event, helper) {
        component.find("requestRecordCreator").getNewRecord(
            "Support_Request__c",
            "01224000000gLN0AAM",
            false,
            $A.getCallback(function() {
                var rec = component.get("v.request");
                var error = component.get("v.newRequestError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                
            })
        );
        
        var action = component.get("c.isEscalationRequestCreated");
        action.setParams({"recordId": component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var theResponse = response.getReturnValue();
                if(theResponse === true){
                    component.set("v.requestId",false);
                }else{
                    component.set("v.requestId",true);
                }
                
            }else if(state === "ERROR"){
                
            }
        });        
        $A.enqueueAction(action);
    },
    handleSaveRecord: function(component, event, helper) {
        component.set("v.btnDisable",true);
        var validity = helper.isFormValid(component, event, helper); 
        if(validity){
            component.find("recordLoader").saveRecord($A.getCallback(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    // handle component related logic in event handler
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            }));
            component.set('v.newRequest.Opportunity__c',component.get("v.recordId"));             
            component.find("requestRecordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    helper.showToastMessages('Success!', 'Escalation Support Request saved successfully.', 'success');
                    $A.get('e.force:refreshView').fire();
                    $A.get("e.force:closeQuickAction").fire();
                    // helper.handleOnSuccess(component, event, helper);
                } else if (saveResult.state === "INCOMPLETE") {
                    // handle the incomplete state
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    // handle the error state
                    console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
             });
            setTimeout(
            $A.getCallback(function() {
                component.set("v.btnDisable",false);
            }), 3000);
            
        }else{
            setTimeout(
            $A.getCallback(function() {
                component.set("v.btnDisable",false);
            }), 3000);
        }
    },    
    GoToParent : function (component,event,helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})