({
	initHandler : function(component, event, helper) {
		component.set("v.textboxName", Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1));
	},
   

	focusHandler : function(component, event, helper) {
        component.set("v.comboBoxContainerStyle", "input-focus");
		helper.getMyLocation(component);
		// Start request queue processor
		helper.startRequestQProcessor(component);
        var input = helper.getInputTextValue(component);
		// If use cache is true, ignore input and make server call. 
		// Otherwise validate input length before call.
		if (input.length >= component.get("v.minSearchChars")) {
            
			helper.search(component);
		}

	},

	/**
	 * Handler on keydown only for the TAB key since the other key events don't catch this.
	 */
	tabHandler : function(component, event, helper) {
        
		var key = 'which' in event ? event.which : event.keyCode;
		//window.event.preventDefault();
		// console.log("key up handler --- " + key + " - "  + component.get("v.data.length") + " - " + component.get("v.lookupList.length")+ " - "  + component.get("v.focusTracker"));

		// On TAB, select the item in focus and hide combobox
		if (key == 9) {
            helper.setValueAndHideDropdown(component);
        } 
        // console.log("after tab handler --- " + component.get("v.focusTracker"));
	},

	keystrokeHandler : function(component, event, helper) {
        
		var key = 'which' in event ? event.which : event.keyCode;
		event.preventDefault();
		// console.log("kup handler --- " + key + " - "  + component.get("v.data.length") + " - " + component.get("v.lookupList.length")+ " - "  + component.get("v.focusTracker"));
		var optionPrefix = component.get("v.lookupId") + "-listbox-option-unique-id--";

		if (key == 40 || key == 38) {
            // On UP and DOWN keys, defocus old selection, update focusTracker, and focus new selection.
            var trackIndex = component.get("v.focusTracker");
            if (trackIndex === undefined) {
            	trackIndex = 0;
            } 
            helper.defocusItem(optionPrefix + trackIndex);

            if (key == 40) {
            	// focusTracker does not increment more than length of list.
            	var optionsLength = component.get("v.lookupList.length");
            	if (trackIndex < optionsLength - 1) {
            		trackIndex++;
            	}
            } else {
            	// focusTracker does not decrement below zero. -1 (default) implies arrow keys not used yet.
            	if (trackIndex > 0) {
            		trackIndex--;
            	}
            }
            helper.focusItem(optionPrefix + trackIndex);
            helper.scrollToKeyboardFocus(component);
            
            component.set("v.focusTracker", trackIndex);
        } else if (key == 13) {
        	// On ENTER, select item in focus and hide combobox
        	if(component.get("v.focusTracker") === -1){
               helper.detectValidTypedInValue(component); 
            }else{
              helper.setValueAndHideDropdown(component);  
            }
            // component.set("v.showError", false);
            // console.log("keyboard show error FALSE");
        } else if (key == 27) {
        	// On ESC, clear selection and hide combobox.
        	helper.clearValueAndHideDropdown(component);
        } else {
    		var input = helper.getInputTextValue(component);
			if (input.length >= component.get("v.minSearchChars")) {
				helper.search(component);

			}	
        }
        
        // console.log("value ------ " + component.get("v.comboValue"));
        // console.log("after kye handler --- " + component.get("v.focusTracker"));
	},

	blurHandler : function(component, event, helper) {
        // HACK alert - delay 250 msec for the option onclick to register and set value.
		setTimeout(function() {
			helper.resetFocusTracker(component);
			helper.hideDropdown(component);
			helper.stopRequestQProcessor(component);
            component.set("v.comboBoxContainerStyle", " ");
            // component.set("v.showError", false);
            
		}, 250);
	},
	mouseoverHandler : function(component, event, helper) {
		// Reset keyboard option focus when the mouse is moved over the options.
		helper.resetFocusTracker(component);
	},

    changeHandler : function(component, event, helper) {
        // Detect if the input text is a valid option
    	// helper.detectValidTypedInValue(component);
    	var input = helper.getInputTextValue(component);
       
    	if (input === "") {
            helper.clearLookupList(component);
    		helper.fireClearedEvent(component);
    	}
        
		component.set("v.presetLocation",input);
		
    },

    selectLookupValue : function(component, event, helper) {
    	// the recordid on the option has the index of the selection in the lookup list
        var dataVal = event.currentTarget.dataset.recordid;
        var list = component.get("v.lookupList");
        helper.selectLookupValue(component, list[dataVal]);
	},

	presetLocation : function(component, event, helper) {
		var input = component.get("v.presetLocation");
		component.set("v.searchText", input);
        if (input === undefined || input === null || input === "") {
            // Preset location value been cleared. Backup and clear out the input box.
            helper.backupValue(component);
            helper.clearValue(component);
        }
        // console.log("Location preset to "+ component.get("v.searchText") + " on --- " + component.get("v.lookupId"));
        // helper.redrawPills(component);
		// helper.sendRequestToServer(component, input, true);
	},

    restoreBak : function(component, event, helper) {
        if (component.get("v.restore")) {
            // console.log("Restoring ... ");
            helper.restoreValue(component);
        }
    }

    
})