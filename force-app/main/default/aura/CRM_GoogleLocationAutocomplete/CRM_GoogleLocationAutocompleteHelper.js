({
    getMyLocation : function (component) {
    	navigator.geolocation.getCurrentPosition(function(position) {
    		// console.log("User located!!! ");
    		component.set("v.myLocation", position.coords.latitude + "," + position.coords.longitude);
    	});
    },

	search : function(component) {
		this.processRequest(component);
	},

    /*
     * Add to request Q
     */
    processRequest : function(component) {
        var requestQ = component.get("v.requestQ");
        requestQ.push({
            "input" : this.getInputTextValue(component)
        });
        component.set("v.requestQ", requestQ);
    },

    /*
     * Start the request Q processor.
     */
    startRequestQProcessor : function(component) {
        var self = this;
        // Set interval timer (500 msec) to process server requests.
        var qProcessorId = setInterval($A.getCallback(function() {
        	// console.log("Interbal running! --- " + component.get("v.requestQ.length"));
            var requestQ = component.get("v.requestQ");
            if (requestQ === undefined) {
                // requestQ undefined implies the component is no longer valid. Clear the interval function.
                clearInterval(qProcessorId);
            } else if (requestQ.length > 0) {
                // If there are queued requests, send the last one to server and clear queue.
                var lastReq = requestQ[requestQ.length - 1];
                self.sendRequestToServer(component, lastReq.input, false, false);
                // requestQ = [];
                component.set("v.requestQ", []);
            }

        }), 500);
        component.set("v.qProcessorId", qProcessorId);
    },

    /*
     * Stop the request Q processor.
     */
    stopRequestQProcessor : function(component) {
        // clearInterval(this.qProcessorId);
        clearInterval(component.get("v.qProcessorId"));
    },

    /*
     * Send the request to server and set callback to process response
     */
    sendRequestToServer : function(component, input, isPreset) {
        var self = this;

        // console.log("Searching for  -- " + input);
		var action = component.get("c.autocomplete");

		action.setParams({
			"input" : input,
			"myLocation" : component.get("v.myLocation"),
			"getAddress" : component.get("v.getAddress")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				// console.log(response.getReturnValue());
	            // Reset focus
	            self.resetFocusTracker(component);
	            // Process response data
	            self.processResponse(component, response.getReturnValue(), isPreset);
			}
		});

		$A.enqueueAction(action);
    },

    processResponse : function(component, response, isPreset) {
        // Update the dropdown list
        // console.log(response);
        // console.log("-----------------=======================");
        var lookup = JSON.parse(response);

       	// console.log(lookup.predictions);
       	component.set("v.lookupList", lookup.predictions);

       	if (!isPreset) {
	        this.showDropdown(component);
			this.resetFocusTracker(component);
			component.set("v.invalidLocation", false);
       	} else if (lookup.predictions && lookup.predictions.length > 0) {
       		this.getPlaceDetails(component, lookup.predictions[0]);
       		component.set("v.invalidLocation", false);
       	}

       	// if(lookup.status === "ZERO_RESULTS" || lookup.predictions.length === 0){
       	// 	component.set("v.showError", true);
       	// } else {
       	// 	component.set("v.showError", false);
       		
       	// }
       	
    },

	setValueAndHideDropdown : function(component) {
        var trackIndex = component.get("v.focusTracker");
        var optionPrefix = component.get("v.lookupId") + "-listbox-option-unique-id--";

        var val = this.getItemValue(optionPrefix + trackIndex);
        if (val !== "") {
            var list = component.get("v.lookupList");
            this.getPlaceDetails(component, list[val]);
        }

    	this.hideDropdown(component);
	},

	clearValueAndHideDropdown : function(component) {
    	this.clearValue(component);
    	this.hideDropdown(component);
	},

	clearValue : function(component) {
    	component.set("v.latlong", {});
    	component.set("v.comboValue", "");
        // Explicitly clearing the input value since it it was inconsistent when only clearing the bound attribute.
        var el = document.getElementById(component.get("v.lookupId"));
        if (el !== null) {
        	el.value = "";
        }
        this.fireClearedEvent(component);
	},

	backupValue : function(component) {
		var bak = {};
		bak.l = component.get("v.latlong");
		bak.cv = component.get("v.comboValue");
		var el = document.getElementById(component.get("v.lookupId"));
        if (el !== null) {
        	bak.v = el.value;
        }
        component.set("v.bak", bak);
	},

	restoreValue : function(component) {
		var bak = component.set("v.bak");
		if (bak) {
	    	component.set("v.latlong", bak.l);
	    	component.set("v.comboValue", bak.cv);
	        // Explicitly clearing the input value since it it was inconsistent when only clearing the bound attribute.
	        var el = document.getElementById(component.get("v.lookupId"));
	        if (el !== null) {
	        	el.value = bak.v;
	        }
		}
	},

	clearLookupList : function(component) {
		// component.set("v.data", []);
		component.set("v.lookupList", []);

	},

	hideDropdown : function(component) {
		component.set("v.comboBoxStyle", "slds-is-hide");
	},

	showDropdown : function(component) {
		component.set("v.comboBoxStyle", "slds-is-open");
	},

	resetFocusTracker : function(component) {
		// Remove current selection and update focusTracker to -1 (no selection).
		this.defocusItem(component.get("v.lookupId") + "-listbox-option-unique-id--" + component.get("v.focusTracker"));
		component.set("v.focusTracker", -1);
	},

	focusItem : function(elid) {
		var el = document.getElementById(elid);
		if (el !== null) {
			el.className = el.className + " has-keyboard-focus";
		}
	},

	scrollToKeyboardFocus : function(component) {
		var ft = component.get("v.focusTracker");
		var dropdownEl = document.getElementById(component.get("v.lookupId") + "-ul-id");
		var listLength = component.get("v.lookupList.length");

		// Set the dropdown scroll position. (dropdownEl.scrollHeight / listLength) is the pixel height 
		// of each option. This works assuming the height of the dropdown is set to accommodate 5 rows. 
		// Will require adjustment if that changes.
		dropdownEl.scrollTop = (ft - 3) * (dropdownEl.scrollHeight / listLength);
	},

	defocusItem : function(elid) {
		var el = document.getElementById(elid);
		if (el !== null) {
			el.className = el.className.replace(" has-keyboard-focus", "");
		}
	},

	getItemValue : function(elid) {
		var el = document.getElementById(elid);
		return el !== null ? el.getAttribute("data-recordid") : "";
	},

	forceSetValue : function(component, val) {
        component.set('v.latlong', val.latlong);
        component.set('v.viewport', val.viewport);
        component.set('v.placeTypes', val.placeTypes);
		component.set("v.searchText", val.value);
		component.set("v.addrComponents", val.components);
		component.set("v.autoRadiusKm", this.getViewportRadius(component));
        
		// Explicitly setting the input value since it it was inconsistent when only setting the bound attribute.
		if (component.get('v.treatAsStreetAddress')) {
			var str = val.value.substr(0,val.value.indexOf(','));
			document.getElementById(component.get("v.lookupId")).value = str;
		} else {
			document.getElementById(component.get("v.lookupId")).value = val.value;
		}
		document.getElementById(component.get("v.lookupId")).blur();
		
		this.fireSelectedEvent(component);
	},

	/**
	 * getInputTextValue - returns the current value of the input text box
	 * @return value of input text box if available. "" if not.
	 */
	getInputTextValue : function(component) {
        var inputEl = document.getElementById(component.get("v.lookupId"));
        return inputEl && inputEl.value ? inputEl.value : "";
	},

	selectLookupValue : function(component, dataValue) {
        var elementId = document.getElementById(component.get("v.lookupId")).getAttribute("id");
         
		this.getPlaceDetails(component, dataValue);
        /*if(elementId !==null || elementId !== undefined){
           document.getElementById(elementId).focus();            
        }*/

        this.hideDropdown(component);
	},

	getPlaceDetails : function(component, prediction) {
        var self = this;

		var action = component.get("c.getPlaceDetails");
		action.setParams({
			"placeId" : prediction.place_id
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				// console.log(response.getReturnValue());
				var result = JSON.parse(response.getReturnValue());
				// console.log(result);
				var viewport = result.result.geometry.viewport;
				var place = {
					"value" : result.result.formatted_address,
					"latlong" : result.result.geometry.location,
					"viewport" : viewport,
					"placeTypes" : result.result.types,
					"components" : result.result.address_components
				};
				this.forceSetValue(component, place);
			}
		});

		$A.enqueueAction(action);
	},

	/**
	 * Compare input value to the lookup list entries to confirm that the typed in value is legitimate.
	 */
	detectValidTypedInValue : function(component) {
        var val = this.getInputTextValue(component);
    	var list = component.get("v.lookupList");

    	var found = false;
        for (var i=0; i<list.length; i++) {
            if (val.toLowerCase() === list[i].description.toLowerCase()) {
                found = list[i];
            }
        }

		if (found) {
    		this.getPlaceDetails(component, found);
    	} else {
    		this.clearValue(component);
    	}

	},

    /** Haversine function. Code referenced from https://rosettacode.org/wiki/Haversine_formula#JavaScript 
    * Usage:  "viewportDiag" : this.calculateViewportDiagonal(viewport.southwest.lat,
    *                                                  viewport.southwest.lng,
    *                                                  viewport.northeast.lat,
    *                                                  viewport.northeast.lng)
    */
    calculateDiagonal : function(viewport) {
        var radians = Array.prototype.map.call(arguments, function(deg) { return deg/180.0 * Math.PI; });
        var lat1 = radians[0], lon1 = radians[1], lat2 = radians[2], lon2 = radians[3];
        var R = 6372.8; // km
        var dLat = lat2 - lat1;
        var dLon = lon2 - lon1;
        var a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLon / 2) * Math.sin(dLon /2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    },

    getViewportRadius : function(component) {
        var viewport = component.get("v.viewport");
        var ne = viewport.northeast;
        var sw = viewport.southwest;
        return this.calculateDiagonal(sw.lat, sw.lng, ne.lat, ne.lng) / 2;
    },

	fireSelectedEvent : function(component) {
        var selectedEvt = component.getEvent("placeSelected");
		selectedEvt.setParams({
			"lookupId" : component.get("v.lookupId"),
			"displayText" : component.get("v.searchText"),
			"viewport" : component.get("v.viewport"),
			"latlong" : component.get("v.latlong"),
			"placeTypes" : component.get("v.placeTypes"),
			"addrComponents" : component.get("v.addrComponents"),
			"autoRadiusKm" : component.get("v.autoRadiusKm")
		});
        selectedEvt.fire();
	},

	fireClearedEvent : function(component) {
		var clearedEvt = component.getEvent("placeCleared");
		clearedEvt.setParams({
			"lookupId" : component.get("v.lookupId")
		});
		clearedEvt.fire();
	}

})