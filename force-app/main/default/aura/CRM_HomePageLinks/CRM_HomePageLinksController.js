({
    doInit: function (component, event, helper) {
        //helper.initialize(component);
    },
    getUserRecord : function(component, event, helper){
        var userInfo = component.get("v.userRecord");
        var action = component.get("c.getHomePageLinks");
        action.setParams({"OpCo": userInfo.CompanyName,
                          "OfficeCode": userInfo.Office_Code__c,
                          "ProfileName": userInfo.Profile.Name
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var acc = response.getReturnValue();
                component.set("v.homePageLinkObj", acc);
            }
        });        
        $A.enqueueAction(action);
    }
})