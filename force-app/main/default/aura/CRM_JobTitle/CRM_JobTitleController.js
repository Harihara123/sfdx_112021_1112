({
	doInit : function(component, event, helper) {
            var usercmp=component.find('userLoader');
            usercmp.set("v.recordId",$A.get("$SObjectType.CurrentUser.Id"));
            usercmp.reloadRecord();
            var fcmp=component.find('recordLoader');
            fcmp.set("v.recordId", component.get("v.recordId"));
            fcmp.reloadRecord();
            component.set("v.displaySkill",true);
            //component.set("v.enteredSkills",'lkupskill');
   },
    handleEditClick : function(component, event, helper){
    },
    handleCancelClick :function(component, event, helper){
        component.set("v.isSkillsPresent", "true");
        component.set("v.isSkillsSave","false");
                               
    },
    handleSaveClick : function(component, event, helper){
       
    },
    loadHandler: function(component, event, helper) {
        
        var indom=document.getElementsByTagName("input");
            indom.focus();
            console.log('skilldom----->'+skilldom);
        
    // var skillVal=component.find("eskillInput").get("v.value");
       
    } ,
     changeHandler: function(component, event, helper) {
       
        console.log('IN changeHandler');
        var globalLovId=event.getParams().value;
         
        var tempRec = component.find("recordLoader");
        tempRec.set("v.recordId",globalLovId[0]);
        tempRec.reloadRecord();
        
    } ,
   enterHandler: function(component, event, helper) {
     var tempval=component.find("eskillLookup");

     component.set("v.enteredSkills",'none');
     component.set("v.displayAdd",'fillskill');
    },
   enterHandler1: function(component, event, helper) {
     var tempval=component.find("eskillLookup");
	 component.set("v.enteredSkills",'lkupskill');
     component.set("v.displayAdd",'none');	
    }
   , 
   recordLoadedEdit :function(component, event, helper) {
    
    var skillsT;
    var validationMsg=component.get("v.jobTitleErrorVarName");
    var validationFlag=false;
    
    if(validationMsg!=null || validationMsg!=undefined || typeof validationMsg !='undefined'){
           validationFlag=true;
    }
       
   
        if(typeof event.getParams().recordUi.record.fields.Req_Job_Title__c !='undefined' && event.getParams().recordUi.record.fields.Req_Job_Title__c !=undefined)
        skillsT=event.getParams().recordUi.record.fields.Req_Job_Title__c.value;
        var loadOnce=component.get("v.loadOnce");
        var uiSkillList=component.get("v.jobtitle");
        
        
            if(typeof skillsT!='undefined' && skillsT!=null && skillsT!='' && (uiSkillList.length==0 )){
               var skillsList=[skillsT];
               var dbList=[];
                if( typeof skillsList!='undefined' && skillsList != null ){
                   var i;
                    for(i=0;i<skillsList.length;i++){
                        var dbobj=skillsList[i];
                        var skillObj = {"name":dbobj, "favorite":false,"index":i};
                        dbList.push(skillObj);
                    }
                     component.set("v.jobtitle",dbList);
                }        
            }
    

   },
     onSubmit : function(component, event, helper) {
        event.preventDefault();
      /*   var eventFields = event.getParam("fields");
         var skillVal=component.find("eskillInput")==null?component.find("eskillInput1").get("v.value"):component.find("eskillInput").get("v.value");
         if(skillVal!=null && skillVal!=undefined && typeof skillVal!='undefined'){
             var indexVl;
             var skillsList=component.get("v.skills");
             if(skillsList == null || typeof skillsList=='undefined'){
                    skillsList=[];
                    indexVl=0;
             }else{
                 indexVl=skillsList.length+1;
             }
             if(skillVal!='' && skillVal!=null && skillVal!=undefined){
              var skillObj = {"name":skillVal, "favorite":false,"index":indexVl};
              skillsList.push(skillObj);
             }
             component.set("v.skills",skillsList);
        }
        component.set("v.displayAdd",'none');
        component.set("v.displaySkill",'none');
        component.set("v.enteredSkills",'lkupskill');
       
        var embedded=component.find('embeddedSkill');
        if(!embedded){
            
            var vsmForm=component.find('skillForm');
            helper.prepareSkills(component, event);
            event.preventDefault();
            var finalSkillList=component.get("v.finalskillList");
            var jsonSkills=JSON.stringify(finalSkillList);
            eventFields.EnterpriseReqSkills__c=jsonSkills;
            eventFields.LDS_VMS__c=true;
            event.setParam("fields", eventFields);
            vsmForm.submit(eventFields);
      
        }
        */
     },
    onFavourite : function(component, event, helper) {
        
        console.log('IN onFavourite');
        var ctarget = event.currentTarget;
    	var pillId = ctarget.id;
        var skillsList=component.get("v.jobtitle");
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==pillId){
              skillObj.favorite=!skillObj.favorite;
        	}
  			skillsList[i]=skillObj;
       }
        component.set("v.jobtitle",skillsList);
        event.preventDefault();  
     },
     removePill: function (component, event, helper) {
        console.log('IN changeHandler');
        var skillIndex=event.getSource().get("v.name");
        var skillsList=component.get("v.jobtitle");
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==skillIndex){
              skillsList.splice(i,1);
        	}
  	    }
        component.set("v.jobtitle",skillsList);
        
     if(skillsList.length==0){
             
       }
        event.preventDefault();
        
    },
    recordUpdated : function(component, event, helper) {
        var changeType = event.getParams().changeType;
        var insertFlag=component.get("v.insertFlag");
        var viewModeFlag=component.get("v.viewMode");
        var cancelJTFlag=component.get("v.cancelJTFlag");
        
        if (changeType === "ERROR") { /* handle error; do this first! */ }
        else if ((((typeof insertFlag=='undefined' || insertFlag==false ) || (viewModeFlag==true && cancelJTFlag==true)) && changeType === "LOADED") || (insertFlag==true && changeType === "CHANGED")) {
            
           
           var sRec=component.get("v.oppRecord");
           var indexVl=0;
          var skillsT=sRec.Req_Job_Title__c;
    
        if(typeof skillsT!='undefined' && skillsT!=null && skillsT!='[]' ){
           var skillsList=[skillsT];
           var dbList=[];
            if( typeof skillsList!='undefined' && skillsList != null ){
               var i;
                for(i=0;i<skillsList.length;i++){
                    var dbobj=skillsList[i];
                    var skillObj = {"name":dbobj, "favorite":false,"index":i};
                    dbList.push(skillObj);
                }
                 component.set("v.jobtitle",dbList);
            }        
           
        }		
            	console.log("Insert Flag JT record--->."+component.get("v.insertFlag"));  
                console.log("JT Record is loaded successfully---->."+changeType);  
       
        //$A.get('e.force:refreshView').fire();
       
			
	    }
       
    },
    handleError : function(cmp,event,helper){
        var params = event.getParams();
        console.log('Params ---->'+params);
    },
    onRender : function(cmp,event,helper){
      /* console.log('On Render---->');
       var emptyList=[];
       
       if(cmp.find("eskillLookupd")!=null){
            var lgnInput=document.querySelector('input');
            var jt=document.getElementsByTagNameNS('lightning','input');
            //jt.focus();
           
            var sklkupd=cmp.find("eskillLookupd");
            //cmp.find("eskillLookup").focus();
            
            var skilldom=document.getElementById("eskillLookupdom");
            //skilldom.style.borderColor = "red";
           
            var indom=document.getElementsByTagName("input");
            indom[0].focus();
            console.log('skilldom----->'+skilldom);
           
           	skilldom.addEventListener('focusout', function doThing(){
   			 console.log('Horray! Someone wrote "' + this.value + '"!');
			});
       
       
          	skilldom.addEventListener('focusout', function doThing(){
                var jt=document.getElementsByTagNameNS('lightning','input');
   				alert('Horray! Someone wrote "' + this.value + '"!');
			});
       }*/
       
    /*   if(cmp.find("eskillLookup")!=null){
        var outerDiv =cmp.find("eskillLookup").getElement();
        var value;
        
        

       }
       */
        //cmp.set("v.freeTextList",emptyList);
     	/*document.getElementById("lkid").addEventListener('keydown', function(e){
               	console.log(e.which);
                console.log(e.code);
               emptyList[emptyList.length+1]=e.code;
              console.log('final string --->'+emptyList.join(""))
		});
        */
        

       /* let form = document.querySelector("form");
        console.log("Saving value", form.elements);
        form.addEventListener("submit", event => {
            console.log("Saving value", form.elements);
            console.log("Saving value", form.elements);
            console.log("Saving value", form.elements);
            event.preventDefault();
        });*/
    },
    keyPressHandler: function(component, event, helper) {
         var glov=component.get("v.glov");
         if(glov!=null && glov.Text_Value__c!=undefined && glov.Text_Value__c!=""){
             if(component.get("v.createPill")){
         		console.log('On click Value selectd--->'+glov.Text_Value__c);
                 var indexVl=0;
                 var skillsList=component.get("v.jobtitle");
                 if(skillsList == null || typeof skillsList=='undefined'){
                        skillsList=[];
                        indexVl=0;
                 }else{
                     indexVl=skillsList.length+1;
                 }
                 var skillObj = {"name":glov.Text_Value__c,"favorite":false,"index":indexVl};
                 skillsList.push(skillObj);
                 component.set("v.jobtitle",Array.from(skillsList));  
                 component.set("v.createPill",false);
                 component.set("v.glov.Text_Value__c",null);
                 
             }else{
                  console.log('ELSE Value selectd--->'+glov.Text_Value__c);
                 if(glov.Text_Value__c!=undefined){
                     component.set("v.glov.Text_Value__c",null);
                 }
                  component.set("v.createPill",true);
             }
         }
     },
    userRecordUpdated : function(component, event, helper) {
	
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") { /* handle error; do this first! */ }
        else if (changeType === "LOADED" || changeType === "CHANGED") { 
           var sRec=component.get("v.simpleRecord");
           var companyName=sRec.CompanyName;
           component.set("v.companyName",companyName);
        	console.log("Record is loaded successfully.");  
	    }
       
    }
})