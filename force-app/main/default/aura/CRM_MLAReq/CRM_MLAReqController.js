({
	recordLoadedView :function(cmp, event, helper) {
        console.log('record view 5');
        try {
           var flag=cmp.get("v.viewLoaded");
            cmp.set("v.skillRefreshFlag",true);
            cmp.set("v.jobTitleRefreshFlag",true);
            cmp.set("v.addinsertFlag",true);
           if(flag==false){
                 cmp.set("v.viewLoaded",true);
                 cmp.set("v.editFlag",false);
                 cmp.set("v.viewFlag",true);
                
                var recordU=event.getParam("recordUi");
                var prodval=event.getParams().recordUi.record.fields.Req_Product__c.value;
                console.log('prodtype val rU'+prodval);
            
               var actSectsList = cmp.get("v.activeSections"); 
                    // F cth and SI -- Perm
                 if(prodval=='Permanent' ){
                         cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",false);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                 }else if(prodval=='Contract to Hire'){
                     cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",true);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                 }else{
                          cmp.set("v.cthFlag",true);
                          cmp.set("v.permFlag",false);
                          
                         if(actSectsList != undefined){
                              console.log("act sec"+actSectsList);
                             actSectsList.push("F");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                              console.log("after F sec"+actSectsList);
                         } 
                     }
                    
                    
                    event.setParam("recordUi",recordU);
                  }
        }
        catch(err) {
            console.log('errors '+err);
        }
        
        var sgn=event.getParams().recordUi.record.fields.StageName.value;
        cmp.set("v.req_stage",sgn);
       
    },
    enableInlineEdit :function(cmp, event, helper) {
        console.log('inline edit pressd 3');  
        cmp.set("v.editFlag",true);
        cmp.set("v.viewFlag",false);
        

        if(cmp.get("v.editFlag")){
            // set coordinates of fixed save/cancel bar
            let editFormWidth = cmp.find("mlaTekFormContainer").getElement().clientWidth;
           

            cmp.set("v.containerWidth", editFormWidth);
           
        }
        cmp.set("v.skillRefreshFlag",true);
         cmp.set("v.jobTitleRefreshFlag",true);
        if(event.getSource().getLocalId()!=null || typeof event.getSource().getLocalId()!='undefined'){
            var sId=event.getSource().getLocalId();
            
            var id=sId.substring(0,sId.length-1);
            cmp.set("v.scrollId",id);
            console.log("scrll id "+id);
            
            var scrollId = cmp.get("v.scrollId");

        }
        
    },
    onStageChange: function(cmp) {
        var stgSelect = cmp.find("detailViewStageName"); 
        
        cmp.set("v.req_stage",stgSelect.get("v.value"));
        if(stgSelect.get("v.value") == 'Draft'){
        	cmp.find("mlaRedZone").set("v.value", false);
        }
        
        
        if(stgSelect.get("v.value") == 'Qualified'){
            cmp.set("v.stgQual",true);
        }
	 },
    recordLoadedEdit :function(cmp, event, helper) {
        console.log('edit pressed 25'); 
        var btn = event.getSource();
        console.log('btn pressed '+btn); 
        var editLoadedFlag=cmp.get("v.editLoaded");
         if(editLoadedFlag==false){
             cmp.set("v.editFlag",true);
             cmp.set("v.viewFlag",false);
             
             var jobDescription;
             var topSkillDetails;
             var addnlSkills;
             var perfExpects;
             var workEnv;
             var evp;
             var addnlInfo;
             var comp;
             var extCommJobdesc;
             var othCompdetails;
             
             /*if(event.getParams().recordUi.record.fields.Req_Job_Title__c!=null){
                 if(!cmp.get("v.loopFlag")){
                     var jobAryId=cmp.find("aerodetailReqJobTitle");
                     var jobTitle=event.getParams().recordUi.record.fields.Req_Job_Title__c.value;
                     var flg=Array.isArray(jobAryId);
                     cmp.set("v.lookupJTFlag",false);
                     if(flg){
                         console.log("jobAryId is array");
                         jobAryId[0].set("v.value",jobTitle);
                        }else{
                          console.log("not an array");
                          jobAryId.set("v.value",jobTitle);
                       }
                }
             }*/
             
             
             if(event.getParams().recordUi.record.fields.Req_Job_Description__c!=null){
                 jobDescription=event.getParams().recordUi.record.fields.Req_Job_Description__c.value;
                 cmp.set("v.editJobdescription",jobDescription);
             }
              
             if(event.getParams().recordUi.record.fields.Req_Skill_Details__c !=null){
                 topSkillDetails=event.getParams().recordUi.record.fields.Req_Skill_Details__c.value;
                 cmp.set("v.editTopSkillDetls",topSkillDetails);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Qualification__c !=null){
                 addnlSkills=event.getParams().recordUi.record.fields.Req_Qualification__c.value;
                 cmp.set("v.editAddnlSkillsQuals",addnlSkills);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Performance_Expectations__c !=null){
                perfExpects =event.getParams().recordUi.record.fields.Req_Performance_Expectations__c.value;
                 cmp.set("v.editPerfExpects",perfExpects);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Work_Environment__c !=null){
                 workEnv =event.getParams().recordUi.record.fields.Req_Work_Environment__c.value;
                 cmp.set("v.editWorkEnv",workEnv);
             }
             
             if(event.getParams().recordUi.record.fields.Req_EVP__c !=null){
                 evp =event.getParams().recordUi.record.fields.Req_EVP__c.value;
                 cmp.set("v.editEvp",evp);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Additional_Information__c !=null){
                 addnlInfo =event.getParams().recordUi.record.fields.Req_Additional_Information__c.value;
                 cmp.set("v.editAddnlInfo",addnlInfo);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Compliance__c !=null){
                 comp =event.getParams().recordUi.record.fields.Req_Compliance__c.value;
                 cmp.set("v.editCompliance",comp);
             }
             
             if(event.getParams().recordUi.record.fields.Req_External_Job_Description__c !=null){
                 extCommJobdesc =event.getParams().recordUi.record.fields.Req_External_Job_Description__c.value;
                 cmp.set("v.editExtCommJobDesc",extCommJobdesc);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Other_Compensations__c !=null){
                 othCompdetails =event.getParams().recordUi.record.fields.Req_Other_Compensations__c.value;
                 cmp.set("v.editOthCompDetails",othCompdetails);
             }
             
            var stgName =event.getParams().recordUi.record.fields.StageName.value;
            
             if(stgName == 'Qualified'){
                   cmp.set("v.stgQual",true);
                }
             
             if(cmp.find("detailViewStageName") != undefined){
                 var stgd = cmp.find("detailViewStageName");
                 var flag=Array.isArray(stgd);
                 if(flag){
                     console.log("stage is array");
                     stgd[0].set("v.value",stgName);
                    }else{
                      console.log("not an array");
                      cmp.find("detailViewStageName").set("v.value",stgName);
                    } 
             }
             if(cmp.find("mladetailAccountId") != undefined){
                var mlaAcc = cmp.find("mladetailAccountId");
                var flg=Array.isArray(mlaAcc);
                var acctId = event.getParams().recordUi.record.fields.AccountId.value;
                console.log("Acc id"+acctId);
                if(flg){
                     console.log("acc is array");
                     mlaAcc[0].set("v.value",acctId);
                    }else{
                      console.log("not an array");
                      mlaAcc.set("v.value",acctId);
                   }
             }
             if(cmp.find("mlaownr") != undefined){
                var mlaOwr = cmp.find("mlaownr");
                var flg=Array.isArray(mlaOwr);
                var owrId = event.getParams().recordUi.record.fields.OwnerId.value;
                console.log("ownr id"+owrId);
                if(flg){
                     console.log("owr is array");
                     mlaOwr[0].set("v.value",owrId);
                    }else{
                      console.log("not an array");
                      mlaOwr.set("v.value",owrId);
                   }
             }
         }
    },
    handleError : function(cmp,event,helper){
        console.log('error reached '); 
        var params = event.getParams();
        cmp.set("v.editDisable",false);
        var Validation = false;
        var validationMessages=[];
        
         if(params.output!=null && params.output.fieldErrors!=null ){
           
            var stgErrors=params.output.fieldErrors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      var checkFlag=Array.isArray(value);
                      console.log(key + ':' + value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                	  Validation = true;
                });
         
        }
        
        if(params.output!=null && params.output.errors!=null ){
           
            var stgErrors=params.output.errors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                	  Validation = true;
                });
         
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
        var fieldErrors=event.getParams().error.data.output.fieldErrors;
        Object.keys(fieldErrors).forEach(function(key){
  				      var value = fieldErrors[key];
   					  console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
            
            		  validationMessages.push(fmessage);
            		  Validation = true;
            
			});
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {
            
            var fieldErrors=event.getParams().error.data.output.errors;
            
            Object.keys(fieldErrors).forEach(function(key){
  				  var value = fieldErrors[key];
   					 console.log(key + ':' + value);
                   var checkFlag=Array.isArray(value);
            	   var fmessage=(checkFlag==true)?value[0].message:value.message;
            		  validationMessages.push(fmessage);
                	  Validation = true;
            
			});
            
        }
        //alert(Validation);
        cmp.set("v.Validation",Validation);
        cmp.set("v.validationMessagesList",validationMessages);
 		console.log('handleError----------->'+validationMessages);
        
        console.log("ValidationMessage Retrieve----->"+cmp.get("v.validationMessagesList"));
    },
    handleCancel : function (cmp,event,helper){
		/*cmp.set("v.validationMessagesList",null);
		cmp.set("v.editFlag",false);
		cmp.set("v.viewFlag",true);  
		cmp.set("v.cancelFlag",true);

		cmp.set("v.stageErrorVarName","");
		cmp.set("v.jobTitleErrorVarName","");
		cmp.set("v.hiringManagerErrorVarName","");
		cmp.set("v.accountErrorVarName","");
		cmp.set("v.draftReasonVarName","");
		cmp.set("v.officeErrorVarName","");
		cmp.set("v.plmtTypeErrorVarName","");
		cmp.set("v.streetErrorVarName","");
		cmp.set("v.cityTypeErrorVarName","");
		cmp.set("v.stateErrorVarName","");
		cmp.set("v.countryVarName","");
		cmp.set("v.zipErrorVarName","");
		cmp.set("v.currencyErrorVarName","");
		cmp.set("v.skillPillValidationError","");		
		cmp.set("v.Validation",false);*/
		event.preventDefault();
    },
    
    onSubmit  : function(cmp, event, helper) {
       console.log('on submit click ');
        cmp.set("v.editDisable",true);
        event.preventDefault();
        //cmp.set("v.skillRefreshFlag",true);
        //cmp.set("v.jobTitleRefreshFlag",false);
        var eventFields = event.getParam("fields");
        var fieldMessages=[];
        helper.validateAddress(cmp,event);
        var workSiteOpp=cmp.get("v.Opportunity");
        var addFieldMessages=cmp.get("v.addFieldMessage");
        var validated = true;
        if(typeof addFieldMessages!='undefined' && addFieldMessages!=null && addFieldMessages.length>0){
            fieldMessages=fieldMessages.concat(addFieldMessages);
            validated=false;
        }else{
            fieldMessages=fieldMessages.concat(addFieldMessages)
            eventFields.Req_Worksite_Street__c=workSiteOpp.Req_Worksite_Street__c;
			eventFields.Req_Worksite_City__c=workSiteOpp.Req_Worksite_City__c;
			eventFields.Req_Worksite_State__c=workSiteOpp.Req_Worksite_State__c;
			eventFields.Req_Worksite_Country__c=workSiteOpp.Req_Worksite_Country__c;
            eventFields.StreetAddress2__c=workSiteOpp.StreetAddress2__c;
            eventFields.Req_Worksite_Postal_Code__c=workSiteOpp.Req_Worksite_Postal_Code__c;
            eventFields.Req_GeoLocation__Latitude__s=workSiteOpp.Req_GeoLocation__Latitude__s;
            eventFields.Req_GeoLocation__Longitude__s=workSiteOpp.Req_GeoLocation__Longitude__s;
        }

        helper.prepareSkills(cmp, event);
        cmp.set("v.loopFlag",false);
        var finalSkillList=cmp.get("v.finalskillList");
        var jsonSkills=JSON.stringify(finalSkillList);
        var eventFields = event.getParam("fields");
        eventFields.EnterpriseReqSkills__c=jsonSkills;
        
        var Validation = false;
         if(cmp.get("v.editJobdescription")!=null)
          eventFields.Req_Job_Description__c=cmp.get("v.editJobdescription");
        //processing addnl
         if(eventFields.Req_Job_Description__c != null)
             eventFields.Description=eventFields.Req_Job_Description__c;
        
        if(cmp.get("v.editTopSkillDetls") != null){
            eventFields.Req_Skill_Details__c =cmp.get("v.editTopSkillDetls");
        }
        
        if(cmp.get("v.editAddnlSkillsQuals") != null){
            eventFields.Req_Qualification__c =cmp.get("v.editAddnlSkillsQuals");
        }
        
         if(cmp.get("v.editPerfExpects") != null){
            eventFields.Req_Performance_Expectations__c =cmp.get("v.editPerfExpects");
        }
        
        if(cmp.get("v.editWorkEnv") != null){
            eventFields.Req_Work_Environment__c =cmp.get("v.editWorkEnv");
        }
        
        if(cmp.get("v.editEvp") != null){
            eventFields.Req_EVP__c =cmp.get("v.editEvp");
        }
        
        if(cmp.get("v.editAddnlInfo") != null){
            eventFields.Req_Additional_Information__c =cmp.get("v.editAddnlInfo");
        }
        
        if(cmp.get("v.editCompliance") != null){
            eventFields.Req_Compliance__c =cmp.get("v.editCompliance");
        }
        
        if(cmp.get("v.editExtCommJobDesc") != null){
            eventFields.Req_External_Job_Description__c =cmp.get("v.editExtCommJobDesc");
        }
        
         var dsn=cmp.find('detailViewStageName');
         var stgName =dsn.get("v.value");
         eventFields.StageName =stgName;
        
        //validation
        //var validated = true;
        // var fieldMessages=[];
        
        if(eventFields.StageName == null || eventFields.StageName == ''){
            validated=false;
            cmp.set("v.stageErrorVarName","Stage Name cannot be blank.");
            fieldMessages.push("Stage Name cannot be blank.");
           }else{
             cmp.set("v.stageErrorVarName","");
          }
         
        var appName = cmp.find("mlareqSummaryJobDescription");
         if (((appName.get("v.value") == null) || (appName.get("v.value") == '')) && eventFields.StageName == 'Qualified' ) {
             validated=false; 
             //var inputDesc = cmp.find("mlareqSummaryJobDescription");
             //cmp.set("v.mlareqSmyJobDescError","Description is required.");
             appName.set("v.errors", [{message:"Description is required."}]);
             fieldMessages.push("Description is required.");
         }else{
             appName.set("v.errors", null);
             cmp.set("v.mlareqSmyJobDescError","");
         }
        
        var jobtitleAry = cmp.get("v.jobtitle");
        if(jobtitleAry==undefined || jobtitleAry==null || jobtitleAry.length<=0){
            validated=false;
            cmp.set("v.jobTitleErrorVarName","Job Title cannot be blank.");
            //cmp.set("v.jobTitleRefreshFlag",true);
            //cmp.set("v.skillRefreshFlag",true);
            var tempAr=[];
            cmp.set("v.jobtitle",tempAr);
            fieldMessages.push("Job Title cannot be blank.");
        }else{
            if((jobtitleAry!=null && jobtitleAry!=undefined && jobtitleAry[0]  != null && jobtitleAry[0]  != '')){
                eventFields.Req_Job_Title__c=jobtitleAry[0].name;
            }
            cmp.set("v.jobTitleErrorVarName","");
        }
        
        
         /*if(eventFields.Req_Job_Title__c == null || eventFields.Req_Job_Title__c == ''){
            validated=false;
            cmp.set("v.jobTitleErrorVarName","Job Title cannot be blank.");
            fieldMessages.push("Job Title cannot be blank.");
           }else{
             cmp.set("v.jobTitleErrorVarName","");
          }*/
         
        
         if(eventFields.Req_Hiring_Manager__c == null || eventFields.Req_Hiring_Manager__c == ''){
            validated=false;
            cmp.set("v.hiringManagerErrorVarName","Hiring Manager cannot be blank.");
            fieldMessages.push("Hiring Manager cannot be blank.");
           }else{
             cmp.set("v.hiringManagerErrorVarName","");
          }
        
        if(eventFields.Req_Total_Positions__c == null || eventFields.Req_Total_Positions__c == ''){
            validated=false;
            cmp.set("v.totalPositionsVarName", "Total Positions cannot be blank.");
            fieldMessages.push("Total Positions cannot be blank.");
        }else{
            cmp.set("v.totalPositionsVarName", "");
        }

        
         if(eventFields.LDS_Account_Name__c == null || eventFields.LDS_Account_Name__c == ''){
            validated=false;
            cmp.set("v.accountErrorVarName","Account Name cannot be blank.");
            fieldMessages.push("Account Name cannot be blank.");
           }else{
             cmp.set("v.accountErrorVarName","");
          }
        
        var quals = cmp.find("mladetailQuali");
        if(((quals.get("v.value") == null) || (quals.get("v.value") == '')) && eventFields.StageName == 'Qualified'){
            validated=false;
            //var inputQual = cmp.find("mladetailQuali");
            //cmp.set("v.mlareqQualEditErr","Additional Skills & Qualifications is required.");
            quals.set("v.errors", [{message:"Additional Skills & Qualifications is required."}]);
            fieldMessages.push("Additional Skills & Qualifications is required.");
        }else{
            quals.set("v.errors", null);
            cmp.set("v.mlareqQualEditErr","");
        }
         
         if(eventFields.Organization_Office__c == null || eventFields.Organization_Office__c == ''){
            validated=false;
            cmp.set("v.officeErrorVarName","Office cannot be blank.");
            fieldMessages.push("Office cannot be blank.");
           }else{
             cmp.set("v.officeErrorVarName","");
          }
        
         if(eventFields.Req_Product__c == null || eventFields.Req_Product__c == ''){
            validated=false;
            cmp.set("v.plmtTypeErrorVarName","Product cannot be blank.");
            fieldMessages.push("Product cannot be blank.");
           }else{
             cmp.set("v.plmtTypeErrorVarName","");
          }
        
          if(eventFields.Req_Division__c == null || eventFields.Req_Division__c == ''){
            validated=false;
            cmp.set("v.mlaDivisionVar","Division cannot be blank.");
            fieldMessages.push("Division cannot be blank.");
           }else{
             cmp.set("v.mlaDivisionVar","");
          }
         /*
         if(eventFields.Req_Worksite_Street__c == null || eventFields.Req_Worksite_Street__c == ''){
            validated=false;
            cmp.set("v.streetErrorVarName","Street cannot be blank.");
             fieldMessages.push("Street cannot be blank.");
           }else{
             cmp.set("v.streetErrorVarName","");
          }
        
         
         if(eventFields.Req_Worksite_City__c == null || eventFields.Req_Worksite_City__c == ''){
            validated=false;
            cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
             fieldMessages.push("City cannot be blank.");
           }else{
             cmp.set("v.cityTypeErrorVarName","");
          }
        
         if(eventFields.Req_Worksite_Country__c == null || eventFields.Req_Worksite_Country__c == ''){
            validated=false;
            cmp.set("v.countryVarName","Country cannot be blank.");
            fieldMessages.push("Country cannot be blank.");
           }else{
             cmp.set("v.countryVarName","");
          }
        
         if(eventFields.Req_Worksite_Postal_Code__c == null || eventFields.Req_Worksite_Postal_Code__c == ''){
            validated=false;
            cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
             fieldMessages.push("Postal Code cannot be blank.");
           }else{
             cmp.set("v.zipErrorVarName","");
          }
         */ 
         
         if(eventFields.Currency__c == null || eventFields.Currency__c == ''){
            validated=false;
            cmp.set("v.currencyErrorVarName","Currency cannot be blank.");
            fieldMessages.push("Currency cannot be blank.");
           }else{
             cmp.set("v.currencyErrorVarName","");
          }
        
         if(eventFields.Req_Terms_of_engagement__c == null || eventFields.Req_Terms_of_engagement__c == ''){
            validated=false;
            cmp.set("v.mlaToEngErr","Terms of Engagement cannot be blank.");
            fieldMessages.push("Terms of Engagement cannot be blank.");
           }else{
             cmp.set("v.mlaToEngErr","");
          }
        
        eventFields.AccountId=cmp.find("mladetailAccountId").get("v.value");
        eventFields.OwnerId =cmp.find("mlaownr").get("v.value");
        event.setParam("fields", eventFields);
        
        var pillData=cmp.get("v.finalskillList"); 
        if((stgName=='Qualified' || stgName=='Presenting') && (pillData==null || pillData=='' || typeof pillData == "undefined")){
            cmp.set("v.skillPillValidationError","At least one skill is required before moving to the 'Qualified' stage or above.");
            cmp.set("v.skillRefreshFlag",true);
            validated=false;
            fieldMessages.push("At least one skill is required before moving to the 'Qualified' stage or above.");
        }else{
            cmp.set("v.skillPillValidationError","");
        } 
        
        if(validated){
            cmp.set("v.insertFlag",true);
            cmp.set("v.addinsertFlag",false);
            cmp.find('mlaEditForm').submit(eventFields); 
            //cmp.set("v.skillRefreshFlag",false);
            //cmp.set("v.jobTitleRefreshFlag",false);
            
        }else{
            Validation = true;
            cmp.set("v.editDisable",false);
            console.log('count '+fieldMessages.length);
            cmp.set("v.validationMessagesList",fieldMessages); 
            cmp.set("v.Validation",Validation);
        }
    },
    handleOpportunitySaved :function(cmp, event,helper){
        
        var params = event.getParams();
 		console.log('handleopty Saved----------->'+JSON.stringify(params));
        cmp.set("v.validationMessagesList",null);
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true); 
        cmp.set("v.cancelFlag",true);
        
        var positionRefereshEvt = $A.get('e.c:E_RefreshPositionCard');
        positionRefereshEvt.fire();
   
    },
    preventProcessing : function (cmp,event,helper){
        event.preventDefault();
        event.stopPropagation();
        console.log('Prevent method.....');
    },
    onAccountChange :function (cmp,event,helper){
       var acId=event.getParams().value;
       cmp.find("mladetailAccountId").set("v.value",acId);
    },
    onOwnerChange : function (cmp,event,helper){
       var owrId=event.getParams().value;
       cmp.find("mlaownr").set("v.value",owrId);
    },
    onProductChange :  function (cmp,event,helper){
      var eventFields = event.getParams();
			 var prodval=eventFields.value;
         if(prodval=='Permanent' ){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",false);
         }else if(prodval=='Contract to Hire'){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",true);
         }else{
             cmp.set("v.cthFlag",true);
              cmp.set("v.permFlag",false);
         }
    },
    handleSectionToggle: function (cmp, event) {
        
    },
    navigateToError: function(component, event, helper){
        var elmnt = document.getElementById("editpageid");
        window.scrollTo(0,200);
    },
     enterHandler: function(component, event, helper) {
     component.set("v.lookupJTFlag",!component.get("v.lookupJTFlag"));
     component.set("v.loopFlag",true);
    },
    changeJobTitle :function (component, event, helper){
        
        var globalLovId=event.getParams().value;
        var tempRec = component.find("recordLoader");
        tempRec.set("v.recordId",globalLovId[0]);
        tempRec.reloadRecord();
    },
    recordUpdated:function (component, event, helper){
        helper.recordUpdated(component, event, helper);
    },
    buttonHandleCancel : function (cmp,event,helper){
		cmp.set("v.validationMessagesList",null);
		cmp.set("v.editFlag",false);
		cmp.set("v.viewFlag",true);  
		cmp.set("v.cancelFlag",true);
		cmp.set("v.stageErrorVarName","");
		cmp.set("v.jobTitleErrorVarName","");
		cmp.set("v.hiringManagerErrorVarName","");
		cmp.set("v.accountErrorVarName","");
		cmp.set("v.draftReasonVarName","");
		cmp.set("v.officeErrorVarName","");
		cmp.set("v.plmtTypeErrorVarName","");
		cmp.set("v.streetErrorVarName","");
		cmp.set("v.cityTypeErrorVarName","");
		cmp.set("v.stateErrorVarName","");
		cmp.set("v.countryVarName","");
		cmp.set("v.zipErrorVarName","");
		cmp.set("v.currencyErrorVarName","");
		cmp.set("v.skillPillValidationError","");		
        cmp.set("v.totalPositionsVarName","");
		cmp.set("v.Validation",false);
        cmp.set("v.cancelJTFlag",true);
        cmp.set("v.addFieldMessage",null);
        cmp.set("v.addFieldMessagedMap",null);
		event.preventDefault();
    },
    
    showEditModal : function(component, event, helper) {
        var appEvent = $A.get('e.c:E_PositionModal');
        appEvent.fire();
    },
    
    refereshPositionData : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    
})