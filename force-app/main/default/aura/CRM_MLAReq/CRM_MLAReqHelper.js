({
	 prepareSkills: function(cmp,event){
        var skillsList=cmp.get("v.skills");
        var finalSkillList=[];
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            var newSkill = {"name":skillObj.name, "favorite":skillObj.favorite};
            finalSkillList.push(newSkill);
        }
        cmp.set("v.finalskillList",finalSkillList);
    },
    recordUpdated : function(component, event, helper) {
	
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") { /* handle error; do this first! */ }
        else if (changeType === "LOADED") { 
           var sRec=component.get("v.simpleRecord");
           component.set("v.lookupJobTitle",sRec.Name);
           
	    }
       
    },
    validateAddress:function(cmp,event){
         var oppAddress=cmp.get("v.Opportunity");
         var fieldMessages=[];
        var addFielMessagedMap={};
         if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
            // cmp.set("v.streetErrorVarName","Street cannot be blank.");
             fieldMessages.push("Street cannot be blank.");
             addFielMessagedMap['streetErrorVarName']="Street cannot be blank.";
           }else{
             cmp.set("v.streetErrorVarName","");
          }
        
         
         if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
            // cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
             fieldMessages.push("City cannot be blank.");
             addFielMessagedMap['cityTypeErrorVarName']="City cannot be blank.";
           }else{
             cmp.set("v.cityTypeErrorVarName","");
          }
        
       /*  if(oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == ''){
             //cmp.set("v.stateErrorVarName","State cannot be blank.");
             fieldMessages.push("State cannot be blank.");
             addFielMessagedMap['stateErrorVarName']="State cannot be blank.";
           }else{
             cmp.set("v.stateErrorVarName","");
          }*/
        
         if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
            //cmp.set("v.countryVarName","Country cannot be blank.");
            fieldMessages.push("Country cannot be blank.");
            //addFielMessagedMap.set("countryVarName","Country cannot be blank.");
            addFielMessagedMap['countryVarName']="Country cannot be blank.";
           }else{
             cmp.set("v.countryVarName","");
          }
        
         if((typeof oppAddress.Req_Worksite_State__c == 'undefined' || oppAddress.Req_Worksite_State__c == null ||
            oppAddress.Req_Worksite_State__c == '') && 
            
           (oppAddress.Req_Worksite_Country__c == 'United States' || oppAddress.Req_Worksite_Country__c == 'Canada'))
        	{
            //alert('In If');
            fieldMessages.push("State cannot be blank.");
            addFielMessagedMap['stateErrorVarName']="State cannot be blank.";
       	 	}
        	else{
            cmp.set("v.stateErrorVarName","");
        	}
        
         if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
             //cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
             fieldMessages.push("Postal Code cannot be blank.");
             //addFielMessagedMap.set("zipErrorVarName","Postal Code cannot be blank.");
             addFielMessagedMap['zipErrorVarName']="Postal Code cannot be blank.";
          }else{
             cmp.set("v.zipErrorVarName","");
          }
        
        
        if(typeof fieldMessages!='undefined' && fieldMessages!=null && fieldMessages.length>0){
            cmp.set("v.insertFlag",true);
            cmp.set("v.addFieldMessage",fieldMessages);
            cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
            var testmap=cmp.get("v.addFieldMessagedMap");
            console.log('Field Map--->'+testmap);
        }else{
             cmp.set("v.insertFlag",false);
             cmp.set("v.addFieldMessage",fieldMessages);
             cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
        }
        
    }
    
})