({
    doInit : function(component, event, helper){
        var eventMsg = event.getParams().message;
        if(typeof(eventMsg) !="undefined"){
            if(eventMsg == '# of Seats by Location was saved.'){
            	helper.loadModalAfterListviewRecordUpdated(component, event, helper);    
            }else if(eventMsg.includes("Strong to Weak")){
                $A.get('e.force:refreshView').fire();
            }
        }else{
            helper.getNumSeatsByLocationData(component, event, helper);
        }
    },
    saveUpdatedSeats : function(component, event, helper){
        var NumSL = component.get("v.numOfSeatObj"); 
        console.log('--NumSL--'+NumSL);
        var saveValidate1 = 'False';
        var saveValidate2 = 'False';
        for(var i=0; i < NumSL.length; i++){
            console.log('--Stage--'+NumSL[i].Opportunity__r.StageName);
            if(NumSL[i].Opportunity__r.StageName == 'Solutioning' || NumSL[i].Opportunity__r.StageName == 'Proposing' || NumSL[i].Opportunity__r.StageName == 'Negotiating'){
                if(parseFloat(NumSL[i].Num_of_Seats__c) <= '0' || NumSL[i].Num_of_Seats__c == '' || typeof(NumSL[i].Num_of_Seats__c) == "undefined"){
                    saveValidate1 = 'True';
                }
            }else if(parseFloat(NumSL[i].Num_of_Seats__c) <= '-1' || typeof(NumSL[i].Num_of_Seats__c) == "undefined"){
                saveValidate2 = 'True';
            }
        }
        if(saveValidate1 == 'False' && saveValidate2 == 'False'){
            var action = component.get("c.saveUpdatedSeatsbyLocations");
            action.setParams({"NumSL": NumSL
                             });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS") {
                    component.find("forceRecord").reloadRecord();
                    component.set("v.loadCmp", false);                    
                    $A.get('e.force:refreshView').fire();
                    helper.showToastMessages('Success!', '# of Seats by Location save successfully.', 'success');
                }else if(state === "ERROR"){
                    var errors = response.getError();
                }                  
            });
            $A.enqueueAction(action);
            helper.getNumSeatsByLocationData(component, event, helper);    
        }else{
            if(saveValidate1 == 'True'){
                helper.showToastMessages('Error: ', 'Please indicate the number of seats for each location.', 'error' );
            }else if(saveValidate2 == 'True'){
            	helper.showToastMessages('Error: ', 'Please enter a valid number.', 'error' );    
            }            
        }        
    },
    recordUpdated : function(component, event, helper){
        var changeType = event.getParams().changeType;
        console.log('--changeType---'+changeType);
        var eventParams = event.getParams();
        if (changeType === "CHANGED"){ 
            var changedFields = eventParams.changedFields; 
            console.log('Fields that are changed: ' + JSON.stringify(changedFields));
            Object.keys(JSON.parse(JSON.stringify(changedFields))).forEach(function(key){
                console.log('Key : ' + key);
                if(key == 'Location__c'){
                    helper.getNumSeatsByLocationData(component, event, helper);
                    $A.get('e.force:refreshView').fire(); 
                    console.table('Value : ' + JSON.parse(JSON.stringify(changedFields))[key]);
                    if(typeof(JSON.parse(JSON.stringify(changedFields))[key].value) != "undefined" && JSON.parse(JSON.stringify(changedFields))[key].value != null && typeof(JSON.parse(JSON.stringify(changedFields))[key].oldValue) !="undefined")
                    component.set("v.loadCmp", true);
                }
                if(key == 'Number_of_Competitors__c'){
                    console.table('Old Value : ' + JSON.parse(JSON.stringify(changedFields))[key]);                    
                    if(typeof(JSON.parse(JSON.stringify(changedFields))[key].oldValue) != "undefined" && JSON.parse(JSON.stringify(changedFields))[key].oldValue == null){
                        helper.getNumOfCompetitorData(component, event, helper);
                        component.set("v.loadCompetitorsCmp", true);
                    }
                }
            })
        }
    },
    openModel: function(component, event, helper){
        component.set("v.loadCmp", true);
    },
    closeModel: function(component, event, helper){
        var NumSL = component.get("v.numOfSeatObj");
        var saveValidate = 'False';
        for(var i=0; i < NumSL.length; i++){
            console.log('--Stage--'+NumSL[i].Opportunity__r.StageName);
            if(NumSL[i].Opportunity__r.StageName == 'Solutioning' || NumSL[i].Opportunity__r.StageName == 'Proposing' || NumSL[i].Opportunity__r.StageName == 'Negotiating'){
                if(parseFloat(NumSL[i].Num_of_Seats__c) <= '0' || NumSL[i].Num_of_Seats__c == '' || typeof(NumSL[i].Num_of_Seats__c) == "undefined"){
                    saveValidate = 'True';
                }
            }
        }
        if(saveValidate == 'True'){
                helper.showToastMessages('Error: ', 'Please indicate the number of seats for each location.', 'error' );
        }else{
            helper.getNumSeatsByLocationData(component, event, helper);
            component.set("v.loadCmp", false);
            component.set("v.loadCompetitorsCmp", false);
        }
    },
    saveCompetitorDecisionDifferentiation : function(component, event, helper){
        component.set("v.disableBtn",true);
        var OppId = component.get("v.recordId");
        var action = component.get("c.saveCompetitorObj");
        action.setParams({"CSW": component.get("v.numOfCompetitorObj"),
                          "DSO": component.get("v.decisionRows"),
                          "TDO": component.get("v.differentiationRows"),
                          "recordId" : OppId
                         });	
        action.setCallback(this, function(response){            
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                helper.showToastMessages('Success!', 'Competitor Information save successfully.', 'success'); 
                $A.get('e.force:refreshView').fire();
                component.set("v.loadCompetitorsCmp", false);
                component.set("v.disableBtn",false);
            }else if(state === "ERROR"){
                var errors = response.getError();
                component.set("v.disableBtn",false);
            }                
        });
    	$A.enqueueAction(action);
	} 
})