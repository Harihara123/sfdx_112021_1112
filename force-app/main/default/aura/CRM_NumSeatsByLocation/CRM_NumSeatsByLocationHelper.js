({
    getNumSeatsByLocationData : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.getNumSeatsByLocations");
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var acc = response.getReturnValue();
                console.log(acc);
                var sum = 0;
                for(var i=0; i < acc.length; i++){                    
                    if(typeof(acc[i].Num_of_Seats__c) !="undefined"){
                        sum += parseInt(acc[i].Num_of_Seats__c);         
                    }                 
                }
                component.set("v.numOfSeatObj", acc);
                component.set("v.totalSeats", sum);
                component.set("v.totalCount", acc.length);
            }
        });        
        $A.enqueueAction(action);        
    },
    loadModalAfterListviewRecordUpdated : function(component, event, helper){
        var eventMsg = event.getParams().message;
        if(eventMsg == '# of Seats by Location was saved.'){
            helper.getNumSeatsByLocationData(component, event, helper);
            component.find("forceRecord").reloadRecord();
        }
    },    
    getNumOfCompetitorData : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.getNumCompetitors");
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var acc = response.getReturnValue();  
                var numofRows =  component.get("v.rowCounter"); 
                var decisionRowItemList = component.get("v.decisionRows");
                var differentiationRowItemList = component.get("v.differentiationRows");
                for(var i = 0 ; i < numofRows ; i++){
                    decisionRowItemList.push({
                        'Decision_Criteria__c': '',
                        'Strongest_to_Weakest__c': i+1
                    });
                    differentiationRowItemList.push({
                        'TGS_Differentiation__c': '',
                        'Strongest_to_Weakest__c': i+1
                    });
                }
                console.log('--decisionRowItemList--'+decisionRowItemList+'--differentiationRowItemList--'+differentiationRowItemList);
                component.set("v.numOfCompetitorObj", acc);
                component.set("v.decisionRows",decisionRowItemList);
                component.set("v.differentiationRows",differentiationRowItemList);                
            }
        });        
        $A.enqueueAction(action);    
    },
    showToastMessages : function( title, message, type ){
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})