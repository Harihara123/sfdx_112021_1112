({
	doInit : function(cmp, event, helper) {
		console.log("Record Id");
        console.log(cmp.get("v.recordId"));
        var params = {'recordId':cmp.get("v.recordId")};
 	var bdata = cmp.find("bdhelper");
			bdata.callServer(cmp,'','','c.getCurrentUser',function(response){
				cmp.set("v.runningUser", response);
			
			},params,false);
			
		bdata.callServer(cmp,'','','c.getCurrentUserWithOwnership',function(response){
				cmp.set("v.userOwnership", response);
			
			},params,false); 
	},
	handleTest: function(component, e, h) {
		console.log('firing TEST')
		const detail = e.getParams();
		console.log(JSON.stringify(detail));
		// var modalBody;
		// $A.createComponent("c:lwcPagination", {},
		// 	function(content, status) {
		// 		if (status === "SUCCESS") {
		// 			modalBody = content;
		// 			component.find('overlayLib').showCustomModal({
		// 				header: "Application Confirmation",
		// 				body: modalBody,
		// 				showCloseButton: true,
		// 				cssClass: "mymodal",
		// 				closeCallback: function() {
		// 					alert('You closed the alert!');
		// 				}
		// 			})
		// 		}
		// 	});
	},

	handleChangeCancel: function (cmp){
		const lwc = cmp.get('v.lwcUpdate');
				if (lwc) {
					lwc.lwcStatusUpdate(lwc.prevStatus, lwc.submittalId);
					cmp.set('v.lwcUpdate', null);
				}
        const lwcSub = cmp.find('lwcSubmittals');
        lwcSub.clearSelections();
		cmp.find('lwcSubmittals').toggleSpinner(false);
	},
    
	handleChangeSave : function(cmp, event, helper) {
		// console.log('handling change save')
		// const lwcSub = cmp.find('lwcSubmittals');
		// lwcSub.refreshData();
        const lwc = cmp.get('v.lwcUpdate');
        if(lwc){
            if(lwc.status =='Remove Link' || lwc.status =='Remove Applicant'){
                cmp.find('lwcSubmittals').removeSubmittalRecord(lwc.submittalId);
                cmp.set('v.lwcUpdate', null);
            }
            // if (lwc.status === 'Add New Interview') {
				cmp.find('lwcSubmittals').refreshSubmittalHistory()
			// }
        }
		cmp.set('v.lwcUpdate', null);
		cmp.find('lwcSubmittals').refreshSubmittals()
        cmp.set("v.toggleHelper",false); 
    },
    
	statusChange: function(cmp, e) {
        cmp.set("v.toggleHelper",true);
		cmp.find('lwcSubmittals').toggleSpinner(true);
		console.log('firing statusChange')
		const detail = e.getParams();
		console.log(JSON.stringify(detail));
		cmp.set('v.lwcUpdate', {
			lwcStatusUpdate: cmp.find('lwcSubmittals').updateSubmittalStatus,
			toggleSpinner: cmp.find('lwcSubmittals').toggleSpinner,
			showToast: cmp.find('lwcSubmittals').showToast,
			refreshSubmittals: cmp.find('lwcSubmittals').refreshSubmittals,
			status: detail.status,
			prevStatus: detail.prevStatus,
			submittalId: detail.submittal.Id
		})

		//let self = this;
		let type = detail.status;
		if (['Interviewing','Add New Interview','Edit Interview'].includes(type) ) {
			var modalBody;
			$A.createComponent("c:C_SubmittalInterviewModal",
				{
					submittal: detail.submittal,
					modalPromise: cmp.getReference('v.modalPromise'),
					editMode: type === 'Edit Interview',
					targetStatus:type,
					preselectedEvent: (detail.preselect? detail.preselect : null),
					updateLwcStatus: cmp.find('lwcSubmittals') ? cmp.getReference('v.lwcUpdate') : null
				},
				function(content, status) {
					if (status === "SUCCESS") {
						modalBody = content;
						let modalPromise = cmp.find('overlayLib').showCustomModal({
							header: $A.get("$Label.c.ATS_INTERVIEW_MANAGEMENT"), //$Label.c.ATS_INTERVIEW_MANAGEMENT
							body: modalBody,
							showCloseButton: true,
							cssClass: "slds-modal_medium",
							closeCallback: function(data) {
								//do this when closing
								console.log('modal closed')
								const lwcUpdate = cmp.get('v.lwcUpdate');
								cmp.find('lwcSubmittals').toggleSpinner(false);
								if(lwcUpdate) {
									lwcUpdate.lwcStatusUpdate(lwcUpdate.prevStatus, lwcUpdate.submittalId)
								}
                                $A.get('e.force:refreshView').fire();
							}
						})
						cmp.set('v.modalPromise', modalPromise);
                       cmp.set("v.toggleHelper",false); 
					}
				});
		} else {
			var sHelp = cmp.find("submittalHelper");
			sHelp.selectStatus(function(response) {
					cmp.find('lwcSubmittals').toggleSpinner(false);
				}, //component.get("v.editId"), component.get("v.newStatus"), component.get("v.submittal")
				detail.submittal.Id, detail.status, detail.submittal, false
			);
            
            if (!["Interviewing","Add New Interview","Edit Interview","Offer Accepted","Remove Applicant","Remove Link","Not Proceeding","Edit Not Proceeding"].includes(type) ) {
                if((detail.submittal.Opportunity.OpCo__c == "AG_EMEA"  && type == "Started") || type != "Started"){
                sHelp.updateStatus(function(response) {
					cmp.find('lwcSubmittals').toggleSpinner(false);
					cmp.find('lwcSubmittals').showToast('success', 'Success', 'Submittal Updated Successfully.');
                    $A.get('e.force:refreshView').fire();
                    cmp.find('lwcSubmittals').refreshSubmittals();
					cmp.set("v.toggleHelper",false); 
                }, detail.submittal.Id, detail.status, "", "",""
            );
            }       
        	}
        }
		
    },
    
    massStatusChange: function(cmp, event) {
		//cmp.find('lwcSubmittals').toggleSpinner(true);
		cmp.set("v.toggleHelper",true);
        const detail = event.getParams();
        var sHelp = cmp.find("submittalHelper");
                    sHelp.massUpdateStatus(function(response) {
						//cmp.find('lwcSubmittals').toggleSpinner(false);
                        }, detail.subIds
                );
        
    },
    handleMassChange : function(cmp, event, helper) {
		console.log('mass change');
        const lwc = cmp.find('lwcSubmittals');
        lwc.refreshSubmittals();
        //lwc.clearSelections();
		// var params = event.getParams();
        // if(params.subIds){
        //     for (let i = 0; i < params.subIds.length; i++) {
        //     	lwc.updateSubmittalStatus(params.newStatus,params.subIds[i]);
        //     }
        // }
	},
	openModalConvertToOpp : function(component, event, helper) {
			component.set("v.toggleHelper",true);
			component.find("overlayLib").notifyClose();
			var sHelp = component.find("submittalHelper");
			//var self = this;
			sHelp.selectStatus(function(response) {
				}, event.getParam("submittalId"), event.getParam("targetStatus"), event.getParam("submittal"), component.get("v.isApplicationCard")
			);

	},
    refreshSubmittals : function(cmp,event,helper){
    	cmp.find('lwcSubmittals').refreshSubmittals();
	}
})