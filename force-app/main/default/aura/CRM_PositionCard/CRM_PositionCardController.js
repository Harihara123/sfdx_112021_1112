({
    doInit : function(component, event, helper) {
        helper.getOpportunityPositions(component);
        helper.getReasonPicklist(component);
    },
    
    showEditModal : function(component, event, helper) {
        helper.showEditOverlay(component, event );
    },
    
    refreshPositionCard : function(component, event, helper) {
        helper.getOpportunityPositions(component);
    },
    
    handleSave: function (component, event, helper) {
        
        var selectedStatus = component.get("v.position_obj.PositionStatus__c");
        
        if( !($A.util.isUndefined(selectedStatus) || $A.util.isEmpty( selectedStatus ) ) ){
            if( selectedStatus == 'wash' || selectedStatus == 'loss' ){
                helper.changePositionStatus(component, 'open',  selectedStatus );
            }else if( selectedStatus == 'unwash' ){
                helper.changePositionStatus(component, 'wash', 'open' );
            }else if( selectedStatus == 'unloss' ){
                helper.changePositionStatus(component, 'loss', 'open' );
            }
        }else{
            component.set("v.has_error", true);
            if( $A.util.isUndefined( component.get("v.error_message") ) || $A.util.isEmpty( component.get("v.error_message") ) ){
            	component.set("v.error_message", 'Please verify the input data or contact your Administrator.!');
            }
            helper.stopSpinner(component);
        }
    },
    
    handleSaveAndClose: function (component, event , helper) {
        
        let closeModal = true;
        var selectedStatus = component.get("v.position_obj.PositionStatus__c");
        
        if( !($A.util.isUndefined(selectedStatus) || $A.util.isEmpty( selectedStatus ) ) ){
            if( selectedStatus == 'wash' || selectedStatus == 'loss' ){
                helper.changePositionStatus(component, 'open',  selectedStatus, closeModal);
            }else if( selectedStatus == 'unwash' ){
                helper.changePositionStatus(component, 'wash', 'open', closeModal);
            }else if( selectedStatus == 'unloss' ){
                helper.changePositionStatus(component, 'loss', 'open', closeModal );
            }
        }else{
            component.set("v.has_error", true);
            if( $A.util.isUndefined( component.get("v.error_message") ) || $A.util.isEmpty( component.get("v.error_message") ) ){
            	component.set("v.error_message", 'Something went wrong, Please try again..');
            }
            helper.stopSpinner(component);
            
        }
    },
    
    handleClose: function (component, event, helper) {
        helper.closeOverlay(component);
    },    
    recordUpdated : function(component, event, helper){	
        var changeType = event.getParams().changeType;
        if (changeType === "CHANGED"){
            helper.getOpportunityPositions(component);
        }
    }
})