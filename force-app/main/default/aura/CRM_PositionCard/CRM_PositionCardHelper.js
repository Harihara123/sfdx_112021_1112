({
    getOpportunityPositions : function(component, closeModal=true ) {
        
        var action = component.get('c.getOpportunityPositionData');
        action.setParams({
            "opportunityId": component.get("v.recordId")
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var positions = response.getReturnValue();
                if(positions!==null){
                    var arrStatus = new Object();
                    arrStatus["open"]   = 0;
                    arrStatus["fill"]   = 0;
                    arrStatus["loss"]   = 0;
                    arrStatus["wash"]   = 0;
                    
                    for (var i = 0; i < positions.length; i++){
                        switch( positions[i]['PositionStatus__c'] ) {
                            case 'Open':
                                arrStatus['open']++;
                                break;
                            case 'Fill':
                                arrStatus['fill']++;
                                break;
                            case 'Loss':
                                arrStatus['loss']++;
                                break;
                            case 'Wash':
                                arrStatus['wash']++;
                                break;
                            default:
                                
                        }
                    }
                    
                    var IsClosed = positions[0]['Opportunity__r']['IsClosed'];
                    
                    if(IsClosed){
                        component.set("v.closedReq", true);                 
                    }else{
                        component.set("v.closedReq", false);  
                    }
                    
                    component.set("v.position_data", positions );
                    component.set("v.position_count", positions.length );
                    component.set("v.statuses_count", arrStatus );
                    
                    if(!closeModal){
                        this.closeOverlay(component);
                        this.showEditOverlay(component);
                    }
                }
                this.stopSpinner(component);
                
                } else if (state === "ERROR") {
                this.handleError(response);
            }

        }));
        
        $A.enqueueAction(action);
        
    },
    
    getReasonPicklist : function(component){
        var action = component.get("c.getReasonPicklist");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.reasons_picklist", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    showEditOverlay : function(component) {
        
        component.set("v.is_disabled", true);
        component.set("v.has_error", false);
        $A.createComponents([
            ["c:CRM_PositionEditModal",{
                position_data: component.getReference("v.position_data"),
                position_count: component.getReference("v.position_count"),
                statuses_count: component.getReference("v.statuses_count"),
                reasons_picklist: component.get("v.reasons_picklist"),
                position_obj: component.getReference("v.position_obj"),
                recordId: component.get("v.recordId"),
                has_error: component.getReference("v.has_error"),
                error_message: component.getReference("v.error_message"),
                spinner: component.getReference("v.spinner"),
                is_disabled : component.getReference("v.is_disabled"),
                closedReq: component.getReference("v.closedReq"),   
				radioLabel: component.getReference("v.radioLabel"),
            }],
            ["c:CRM_PositionEditModalFooter", {
                handleSave: component.getReference("c.handleSave"),
                handleSaveAndClose: component.getReference("c.handleSaveAndClose"),
                handleClose: component.getReference("c.handleClose"),
                is_disabled : component.getReference("v.is_disabled"),
            }]
        ],
                            
                            function(components, status){
                                if (status === "SUCCESS") {
                                    
                                    let modalBody = components[0];
                                    let modalFooter = components[1]
                                    let overlayPromise = component.find('edit_overlay').showCustomModal({
                                        header: $A.get("$Label.c.ATS_Update_Opportunity_Status"),
                                        body: modalBody,
                                        footer: modalFooter,
                                        showCloseButton: true,
                                        cssClass: "slds-modal_large",
                                        closeCallback: function() {
                                            
                                        }
                                    });
                                    
                                    component.set("v.overlayPromise", overlayPromise );
                                    
                                }else if (status === "ERROR") {
                                    this.showToast(component, $A.get("$Label.c.ATS_Position_Card_Error"), 'error','error');
                                } 
                            }
                           );
        
    },
    
    changePositionStatus : function( component, fromStatus, toStatus, closeModal=false ){
        
        this.startSpinner(component);
        
        var action = component.get("c.editPositionStatuses");
        action.setParams({
            "opportunityId" : component.get("v.recordId"),
            "fromStatus": fromStatus,
            "toStatus": toStatus,
            "numberOfPositions": component.get("v.position_obj.Number"),
            "reason": component.get("v.position_obj.ReasonforChange__c"),
			"radioLabel" : component.get("v.radioLabel"),
        });
        
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                
                if(a.getReturnValue()){
                    this.getOpportunityPositions(component, closeModal); 
                    if(closeModal){
                        this.closeOverlay(component);
                        this.stopSpinner(component);
						if(a.getReturnValue()[0] == 'batchUpdate')
                        this.showToastMessages('Information!', $A.get("$label.c.ATS_Get_Opportunity_Postions_Informatin_Message"), 'info');
                    }
                    
                    var appEvent = $A.get('e.c:E_OpportunityPositionRefresh');
        			appEvent.fire();
                    $A.get('e.force:refreshView').fire();
                
                }else{
                    component.set("v.has_error", true);
                    component.set("v.error_message", $A.get("$Label.c.ATS_Position_Card_Error_Message1"));
                    this.stopSpinner(component);
                }
                
                component.set("v.position_data", null); 
                component.set("v.statuses_count", null); 
                component.set("v.position_obj", null); 
                component.set("v.position_count", 0); 
                
            }else{
                component.set("v.has_error", true);
                component.set("v.error_message", $A.get("$Label.c.ATS_Position_Card_Error_Message2"));
                this.stopSpinner(component);
            }
            
        });
        
        $A.enqueueAction(action);
    },
    
    changeWashOrLossStatusToOpen : function(component){
        var action = component.get("c.changeWashOrLossToOpen");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.reasons_picklist", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    closeOverlay: function (component) {
        component.get("v.overlayPromise").then(
            function (modal) {
                modal.close();
            }
        );
    },
    
    handleError : function(response){
        // Use error callback if available
        if (typeof errCallback !== "undefined") {
            errCallback.call(this, response.getReturnValue());
            // return;
        }
        // Fall back to generic error handler
        var errors = response.getError();
        if (errors) {
            console.log("Errors", errors);
            if (errors[0] && errors[0].message) {
                this.showError(errors[0].message);
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,$A.get("Label.c.ATS_An_error_occured"));
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,$A.get("$Label.c.ATS_An_error_occured"));
            }else{
                this.showError($A.get("$Label.c.ATS_Unexpected_Error_Occured"));
            }
        } else {
            this.showError(errors[0].message);
            //throw new Error("Unknown Error");
        }
    },            
    
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    
    startSpinner: function(component){
        component.set('v.spinner', true);
    },
    
    stopSpinner: function(component){
        component.set('v.spinner', false);
        setTimeout(function() {
            component.set("v.has_error", false); 
        }, 10000);
        
    },
    showToastMessages : function( title, message, type ){
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }    
})