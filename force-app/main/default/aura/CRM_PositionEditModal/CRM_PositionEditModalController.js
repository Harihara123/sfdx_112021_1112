({
    doInit : function(component, event, helper) {
        helper.startSpinner(component);
        helper.doInitialize(component);
    },
    
    statusChanged : function(component, event, helper) {
        
        let selectedStatus = event.getSource().get("v.value");
        component.find("postions_updating").set("v.value", '' );
        component.find("select-reason").set("v.value", '' );
        component.set("v.position_error", false);
        
        var closedReq = component.get("v.closedReq");
        var reqOpenDisclaimer = component.get("v.reqOpenDisclaimer");
        
        if(($A.util.isUndefined( selectedStatus ) || $A.util.isEmpty( selectedStatus )) &&  closedReq==false){
            component.set("v.is_disabled", true);
            component.set("v.error_message",  'You should select either LOSS or WASH as the status.' );
            component.set("v.position_obj", null );
            
        }
        else if((selectedStatus=="unwash" || selectedStatus=="unloss") && closedReq==true){
            component.set("v.reqOpenDisclaimer", true);
            component.set("v.error_message",  null );
            helper.doValidation(component, event);            
        }
        else{            
            component.set("v.is_disabled", false);
            component.set("v.error_message",  null );
            helper.doValidation(component, event);
			
        }
	},
    
    reqOpenChange : function(component, event, helper){
        var selected = event.getSource().get("v.name");
        var reqOpenCheck = component.get("v.reqOpenCheck");
        if(reqOpenCheck){
        	component.set("v.is_disabled", false);
            component.set("v.radioLabel", selected);
        }else{
            component.set("v.is_disabled", true);
            component.set("v.radioLabel", "");
        }
    },
    
    validatePositions  : function(component, event, helper) {
        helper.doValidation(component, event);             
	},
    onRadio1Change : function(component, event, helper) {
		var selected = event.getSource().get("v.name");
		component.set("v.radioLabel", selected);
		var checkCmp = component.find("radio2");
		checkCmp.set("v.checked", false);
        component.set("v.is_disabled", false);
        helper.doValidation(component, event);
	},	
	onRadio2Change : function(component, event, helper) {
		var selected = event.getSource().get("v.name");
		component.set("v.radioLabel", selected);
		var checkCmp = component.find("radio1");
		checkCmp.set("v.checked", false);
        component.set("v.is_disabled", false);
        helper.doValidation(component, event);
	}
})