({
    
    doInitialize : function( component ) {
        
        var action = component.get('c.allowUnlossUnwash');
        action.setParams({
            "opportunityId": component.get("v.recordId")
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.allow_access", response.getReturnValue() );
            } 
            this.stopSpinner(component);
            
        }));
        
        $A.enqueueAction(action);
        
    },
    
    
    doValidation  : function(component, event) {
        
        component.set("v.is_nagative", false);
        component.set("v.position_error", false);
        component.set("v.position_obj", {} );
        
        let selectedStatus = component.find("select-status").get("v.value");
        let selectedReason = component.find("select-reason").get("v.value");
        let positions = component.find("postions_updating").get("v.value");
        component.set("v.selected_status",  selectedStatus );
		var totalfill = component.get("v.statuses_count.loss") + parseInt(positions) + component.get("v.statuses_count.wash") + component.get("v.statuses_count.fill")
        if( $A.util.isUndefined( positions ) || $A.util.isEmpty( positions ) ){
            component.set("v.error_message",  $A.get("$Label.c.ATS_Number_of_Positions_Empty"));
            return;
        }
        
        if( positions < 0 ){
            component.set("v.is_nagative", true);
            component.set("v.error_message",   $A.get("$Label.c.ATS_Number_of_Positions_Negative"));
            return;
        }else if(positions == 0 ){
            component.set("v.error_message",   $A.get("$Label.c.ATS_Number_of_Positions_Zero"));
            return;
        }
        
        if( $A.util.isUndefined( selectedStatus ) || $A.util.isEmpty( selectedStatus ) ){
            component.set("v.error_message",  $A.get("$Label.c.ATS_Select_Status_Loss_or_Wash") );
            component.set("v.reqOpenNotProceedingDisclaimer", false);
            component.set("v.radioLabel","");
        }else if( ( selectedStatus == 'wash' || selectedStatus == 'loss' ) && ( $A.util.isUndefined( selectedReason ) || $A.util.isEmpty( selectedReason ) ) ){
            component.set("v.error_message",  $A.get("$Label.c.ATS_Loss_or_Wash_reason_required") );
            component.set("v.reqOpenNotProceedingDisclaimer", false);
            component.set("v.radioLabel","");
        }else if( ( selectedStatus == 'wash' || selectedStatus == 'loss' ) && positions > component.get("v.statuses_count.open") ){
            component.set("v.max_availible_ positions", component.get("v.statuses_count.open") );
            component.set("v.position_error", true);
            component.set("v.error_message", $A.get("$Label.c.ATS_Updated_positions_cannot_exceed_open_positions") );
            component.set("v.reqOpenNotProceedingDisclaimer", false);
            component.set("v.radioLabel","");
        }else if( selectedStatus == 'unwash' && positions > component.get("v.statuses_count.wash") ){
            component.set("v.max_availible_positions", component.get("v.statuses_count.wash") );
            component.set("v.position_error", true);
            component.set("v.reqOpenNotProceedingDisclaimer", false);
            component.set("v.radioLabel","");
            component.set("v.error_message",  $A.get("$Label.c.ATS_Updated_positions_cannot_be_more_than_Washed_positions")	 );
        }else if(selectedStatus == 'unloss' && positions > component.get("v.statuses_count.loss") ){
            component.set("v.max_availible_positions", component.get("v.statuses_count.loss") );
            component.set("v.position_error", true);
            component.set("v.error_message",  $A.get("$Label.c.ATS_Updated_positions_cannot_be_more_than_Lost_positions")  );
            component.set("v.reqOpenNotProceedingDisclaimer", false);
			if(component.get("v.radioLabel") != 'reqOpenDisclaimerBox')
            component.set("v.radioLabel","");
        }else if(totalfill == component.get("v.position_count") && selectedStatus != 'unloss' && selectedStatus != 'unwash' && ($A.util.isUndefined( component.get("v.radioLabel") ) || $A.util.isEmpty( component.get("v.radioLabel") ))){
            component.set("v.is_disabled", true);
            component.set("v.reqOpenNotProceedingDisclaimer", true);            
        }else{            
            if(totalfill != component.get("v.position_count")){
				if(component.get("v.radioLabel") != 'reqOpenDisclaimerBox')
                component.set("v.radioLabel","");
                component.set("v.reqOpenNotProceedingDisclaimer", false);
            }
            var positionObj = {};
            positionObj.PositionStatus__c	= selectedStatus;
            positionObj.ReasonforChange__c	= selectedReason;
            positionObj.Number				= positions;
            
            component.set("v.is_nagative", false);
            component.set("v.position_error", false);
            component.set("v.position_obj", positionObj );
            
        }   
        
    },
    
    startSpinner: function(component){
        component.set('v.spinner', true);
    },
    
    stopSpinner: function(component){
        component.set('v.spinner', false);
    },
    
    
    
})