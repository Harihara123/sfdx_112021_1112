({
    recordUpdated : function(component, event, helper){	
        var changeType = event.getParams().changeType;
        if (changeType === "LOADED" || changeType === "CHANGED"){ 
            var tgsOpp = component.get("v.simpleRecord");
            component.set("v.OppStageValue",tgsOpp.StageName);
        }       
    },
    closeModel: function(component, event, helper){
        component.set("v.isOpen", false);
    },    
    setPercentage: function(component, event, helper){           
        helper.approveOppProcess(component, event, helper);
    }
})