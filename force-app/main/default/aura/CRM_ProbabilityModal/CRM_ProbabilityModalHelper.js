({
	approveOppProcess : function(component, event, helper){
        component.set("v.disableOnClick",true);
        var buttonValue = event.getSource().getLocalId();
		var recordId = component.get("v.recordId");
        var setpercentValue = component.find("makeId").get("v.value");
        if(setpercentValue == ''){
            helper.showToastMessages('Error: ', 'Please select Probability percent value.', 'error');
            component.set("v.disableOnClick",false);
        }else{
            var action = component.get("c.ApproveOrRejectTGSOpportunity");
            action.setParams({
                "oppId": recordId,
                "approvalAction":buttonValue,
                "oppProbabilityValue":setpercentValue
            });
            action.setCallback(this, function(response){            
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    component.set("v.disableOnClick",false);
                    component.set("v.isOpen", false);
                    component.set("v.showApproveBtn",false);
                    if(component.get("v.OppStageValue") == 'Proposing'){
                    	component.set("v.hideSubmitBtn",true);    
                    }else{
                        component.set("v.hideSubmitBtn",false);
                    }                    
                    component.set("v.hideCloseBtn",false);
                    if(component.get("v.OppStageValue") == 'Solutioning'){
                        component.set("v.btnLabel",'Approve');
                    }
                    $A.get('e.force:refreshView').fire();                
                }else if(state === "ERROR"){
                    var errors = response.getError();
                    component.set("v.disableOnClick",false);
                }                
            });
            $A.enqueueAction(action);
        }
	},
    showToastMessages : function(title, message, type){
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})