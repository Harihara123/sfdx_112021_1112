({
	doInit : function(component, event, helper) {
         
		 helper.initProductHierarchy(component,event,helper,null);	        
	},
     onOpcoChange : function(component, event, helper) {
       helper.loadBusinessUnitValues(component,event,'None');
	},
    onBusinessunitChange : function(component, event, helper) {
        component.find("segmentId").set("v.value","--None--");
        component.find("JobcodeId").set("v.value","--None--");
        component.find("CategoryId").set("v.value","--None--");
        component.find("MainSkillId").set("v.value","--None--");
        /*if(component.find("businessUnitId").get("v.value")=='--None--'){
            helper.EnterpriseReqReSet(component,event,helper,null);
        }else{*/
			helper.EnterpriseReqSegmentModal(component,event,helper,null);            
        //}       
	},
     onSegmentChange : function(component, event, helper) {
       component.find("JobcodeId").set("v.value","--None--");
       component.find("CategoryId").set("v.value","--None--");
       component.find("MainSkillId").set("v.value","--None--");  
       helper.EnterpriseReqJobcodeModal(component,event,helper,null);
	},
    onJobCodeChange : function(component, event, helper) {
       component.find("CategoryId").set("v.value","--None--");
       component.find("MainSkillId").set("v.value","--None--");  
       helper.EnterpriseReqCategoryModal(component,event,helper,null);
	},
    onCategoryChange : function(component, event, helper) {
       component.find("MainSkillId").set("v.value","--None--");  
       helper.EnterpriseReqMainSkillModal(component,event,helper,null);
	},
    onMainSkillChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillid(component,event);
        //Adding w.r.t Product hierarchy for Tek systems new form 
        var opcoVal=component.get("v.req.OpCo__c");       
        //if(component.get("v.isSkillsSave")&& component.get("v.isTekUser")){
        if(component.get("v.isSkillsSave")&& (opcoVal=='TEKsystems, Inc.' || opcoVal=='Aerotek, Inc')){
            var validated=helper.validateEnterpriseReq(component, event, helper);
            if(validated){
                helper.getReqProductdata(component, event, helper);
            }
        }
	},    
    onProductPicklistChange : function(component, event, helper) { 
       //helper.EnterpriseProductName(component,event); 
	},
    events : function(component, event, helper){
    console.log('HANDLER');
	},
    handleEditClick : function(component, event, helper){
        
        helper.EditProductHierarchy(component,event,helper);
        //helper.initProductHierarchy(component,event,helper,null);	
		//component.set("v.isSkillsSave","true");
        /*
        component.set("v.prod.Job_Code__c","");
        component.set("v.prod.Category__c","");
        component.set("v.prod.Skill__c","");
        component.set("v.productHireValMsg","");
       */
    },
    handleCancelClick :function(component, event, helper){
        component.set("v.isSkillsPresent", "true");
        component.set("v.isSkillsSave","false");
        component.set("v.productHireValMsg","");
        
                               
    },
    handleSaveClick : function(component, event, helper){
        console.log('save');     
		helper.saveEnterpriseReq(component, event,helper);   
        var opptyId = component.get("v.recordId");
        console.log('saving updating');
        console.log(opptyId);
    }
    /*onTGSChange: function(component, event, helper) { 
        var OpCoValue = component.get("v.req.OpCo__c");
        var TGSReqValue = component.find("isTGSReqId").get("v.value");
        if(OpCoValue == 'TEKsystems, Inc.' && TGSReqValue == 'Yes'){
            component.set("v.IsTGSReq", true);            
            //component.set("v.isTGSTrue", "true");
        }else if(OpCoValue == 'TEKsystems, Inc.' && (TGSReqValue == 'No' || TGSReqValue == '--None--')){			
			component.set("v.req.Practice_Engagement__c","--None--");
            component.set("v.req.GlobalServices_Location__c","--None--");
            component.set("v.req.Employment_Alignment__c","--None--");
            component.set("v.req.Final_Decision_Maker__c","--None--");
            component.set("v.req.Is_International_Travel_Required__c","--None--");
            component.set("v.req.National__c", false);
            component.set("v.req.Backfill__c", false);
            component.set("v.req.Internal_Hire__c","--None--");
			component.set("v.isTGSPEName","");
            component.set("v.isTGSLocationName","");
            component.set("v.isTGSEmpAlignName","");
            component.set("v.isFinaDecMakName","");
            component.set("v.isIntNationalName","");
            component.set("v.isInternalName","");
            component.set("v.isTGSTrue", "false");
            component.set("v.IsTGSReq", false); 
        } 
    }*/
})