({
	isFormValid: function (component, event, helper) {
        var valid = true;
        if(valid){
        	valid = helper.checkValidity(component, event, helper, "revenueId", "revenueMessage");    
        }
        if(valid){
        	valid = helper.checkValidity(component, event, helper, "positionId", "positionMessage");
        }
        if(valid){
        	valid = helper.checkValidity(component, event, helper, "deliveryId", "deliveryMessage");    
        }
        if(valid){
            valid = helper.checkValidity(component, event, helper, "priorityId", "PriorityMessage");
        }
        return valid;
    },
    checkValidity :  function (component, event, helper, fieldName, errorFieldName) {
        var inputCmp = component.find(fieldName);
        var errorMessage = component.find(errorFieldName);
        var value = inputCmp.get("v.value");
        var valid = false;
         if (value == null || value == "") {
             valid = false;
             $A.util.addClass(inputCmp, 'slds-visible slds-has-error');
             $A.util.removeClass(errorMessage, 'slds-hidden');
             $A.util.addClass(errorMessage, 'slds-visible slds-text-color--error');
        } else {
            valid = true;
             $A.util.removeClass(inputCmp, 'slds-visible slds-has-error');
             $A.util.removeClass(errorMessage, 'slds-visible slds-text-color--error');
             $A.util.addClass(errorMessage, 'slds-hidden');
        }
        return valid;
    },
    showToastMessages : function( title, message, type ) {
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });        
        toastEvent.fire();
    }
})