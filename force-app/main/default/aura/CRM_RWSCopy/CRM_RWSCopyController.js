({
	doInit : function(component, event, helper) {
		
        var fReqId=component.get("v.recordId")==null?component.get("v.reqId"):component.get("v.recordId");
        component.set("v.fReqId",fReqId);
     //  component.find('recordLoader').reloadRecord(true);
        
        var fcmp=component.find('recordLoader');
        fcmp.set("v.fReqId", component.get("v.fReqId"));
       
		fcmp.reloadRecord();
		
    },
    onChange : function(component, event, helper) {
           var labelVar=event.getSource().getLocalId();
           component.set("v.fvalue",labelVar);        
    },
    onCheck : function(component, event, helper) {
         var flag=component.get("v.acknowledgeVar");
         component.set("v.acknowledgeVar",!flag);
         var button = component.find('okbuttonid');
         button.set('v.disabled',flag);
    },
    
    ofccpCheck : function(component, event, helper) {
    },
    
    handleClick : function(component, event, helper){
        if(component.get("v.switchFlag") === 'RWS') { 
			//var recStage=component.get("v.simpleRecord").StageName;
			var validationFlag=true;
        
			var ack=component.get("v.acknowledgeVar");
			var fVar=component.get("v.fvalue");
      	
			var postingId=component.get("v.simpleRecord").Source_System_id__c;
        
        
			if(postingId=='' && postingId==null && typeof postingId =='undefined'){
				 var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						title : 'Error',
						message: 'Posting Id can be not blank',
						messageTemplate: '',
						duration:' 5000',
						key: 'info_alt',
						type: 'error',
						mode: 'sticky'
					});
					toastEvent.fire();
			}
			else{
        
				var rwsPostingId;
				if(postingId) {
					rwsPostingId = postingId.replace('R.','');
				}
        
        
				component.set("v.isModal",false);
				var reqId=component.get("v.isShow")=='SHOWREQ'?component.get("v.recordId"):component.get("v.reqId");
				component.set("v.isShow",'HIDEREQ');
				component.set("v.isReqSearch",true);
				var dismissActionPanel = $A.get("e.force:closeQuickAction");
				dismissActionPanel.fire();
        
				// console.log("Public OFCCP ",component.get("v.publicPost"));
				// console.log("Private OFCCP ",component.get("v.privatePost"));
				// console.log("OFCCP ",component.get("v.isOFCCP"));
				//S-184498 changes are in else bloack- Neel-
			
				var rwsURL = $A.get("$Label.c.RWS_Job_Posting_URL");
			
				if(component.get("v.acknowledgeVar")==true && validationFlag && component.get("v.isOFCCP")==false){
					window.open(rwsURL+'/RWS/posting/savePreBBDataAndInitiatePosting.action?postingTO.ofccpFlg=false&postingTO.privateFlg=false&source=ATS&&postingTO.id='+rwsPostingId,'_blank');
				}
				else if(component.get("v.acknowledgeVar")==true && validationFlag && component.get("v.isOFCCP")==true && fVar=='public'){
					window.open(rwsURL+'/RWS/posting/savePreBBDataAndInitiatePosting.action?postingTO.ofccpFlg=true&postingTO.privateFlg=false&source=ATS&&postingTO.id='+rwsPostingId,'_blank');
				}
				else if(component.get("v.acknowledgeVar")==true && validationFlag && component.get("v.isOFCCP")==true && fVar=='private'){
					window.open(rwsURL+'/RWS/posting/savePreBBDataAndInitiatePosting.action?postingTO.ofccpFlg=true&postingTO.privateFlg=true&source=ATS&&postingTO.id='+rwsPostingId,'_blank');
				}	
			
				var compEvents = component.getEvent("jobPostingDisableEvent");
				compEvents.setParams({ "disableFlag" : true });
				compEvents.fire();
			}
		}	
		else {
			helper.handleSFDCJobPosting(component, event);
		}
    }
    ,     
    handleClickMandatory: function(component, event, helper){
    	component.set("v.isModal",true);
       // var flag=component.get("v.acknowledgeVar");
        component.set("v.acknowledgeVar",false);
        var button = component.find('okbuttonid');
        button.set('v.disabled',true);
        component.set("v.publicPost","public");
    },
    openModal:function(component, event, helper){
        component.set("v.isModal",false);
	},
    handleCancel: function(component, event, helper){
        component.set("v.isModal",false);
        component.set("v.isShow",'HIDEREQ');
        component.set("v.isReqSearch",true);
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
    	dismissActionPanel.fire();
        var compEvents = component.getEvent("jobPostingDisableEvent");
        compEvents.setParams({ "disableFlag" : true });
        compEvents.fire();
    },
    doneRendering: function(cmp, event, helper) {
        // var dismissActionPanel = $A.get("e.force:closeQuickAction");
    	 // dismissActionPanel.fire();
   },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
			helper.jobPostingSwitch(component,event);
            var ofccpFlag=component.get("v.simpleRecord").OFCCP_Flag__c;
            console.log('OFCCP flag ',ofccpFlag);
            if(ofccpFlag){
               component.set("v.isOFCCP",true);
            } 
            console.log("Record is loaded successfully.");
        } else if(eventParams.changeType === "ERROR") {
            var errorMessage=component.get("v.recordError");
             var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: errorMessage,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
             component.set("v.isModal",false);
            var reqId=component.get("v.isShow")=='SHOWREQ'?component.get("v.recordId"):component.get("v.reqId");
            component.set("v.isShow",'HIDEREQ');
            component.set("v.isReqSearch",true);
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire(); 
            var compEvents = component.getEvent("jobPostingDisableEvent");
            compEvents.setParams({ "disableFlag" : true });
            compEvents.fire();
            // there’s an error while loading, saving, or deleting the record
        }
    }
})