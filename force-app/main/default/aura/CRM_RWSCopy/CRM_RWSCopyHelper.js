({
	jobPostingSwitch : function(cmp, event) {
		let action = cmp.get("c.switchToSFDC");
		action.setCallback(this, function(resp){
			cmp.set("v.isLoading", false);
			if(resp.getState() == 'SUCCESS') {
				let jpMap = resp.getReturnValue();
				//Start - Cross copy checking
				cmp.set("v.switchFlag", jpMap.SFDC_SWITCH);
				var srcId = cmp.get("v.simpleRecord").Source_System_id__c;
				if(jpMap.SFDC_SWITCH === 'RWS' && !srcId) {
					cmp.set("v.ccFlag", true);
					cmp.set("v.ccMsg", "Copy Posting functionality is not allowed on this Posting. Please select a different Posting.");
				}
				else if(jpMap.SFDC_SWITCH === 'SFDC' && srcId){
					cmp.set("v.ccFlag", true);
					cmp.set("v.ccMsg", "Legacy Posting/s Cannot be Copied into Connected. Please perform Copy Posting action from non-legacy Posting.");
				}
				else {
					cmp.set("v.ccFlag", false);
				}
				//End - Cross copy checking
				if(jpMap.limit === 'over') {
					cmp.set("v.jobPostingLimit", true);
				}
				else {
					cmp.set("v.jobPostingLimit", false);
				}
				cmp.set("v.opcoLimit", jpMap.OPCO_LIMIT);
			}
		});
		$A.enqueueAction(action);	 
	},
	handleSFDCJobPosting : function(cmp, event) {
		let pageReference = {
			 				type: 'standard__component',
			 				attributes: {
			 					"componentName" : "c__C_JobPostingRedirectToLWC"
			 				},
			 				state: {
			 					"c__jpId": cmp.get("v.recordId")
			 				}
			 			};
		event.preventDefault();
		let  navService = cmp.find("navService");
		navService.navigate(pageReference);
		this.delayedRefresh(100);
	},
	delayedRefresh : function(milliseconds){
		let ms = milliseconds || 100;
		window.setTimeout($A.getCallback(function(){
			$A.get('e.force:refreshView').fire();
		}),ms);
	}

})