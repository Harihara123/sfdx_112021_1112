({
	refreshPosting : function(component, event, helper) {
        
	   var returnAction=component.get("c.inActivateJobPostings");
       var jobPosting=component.get("v.recordId");
       var postingList=[];
       postingList.push(jobPosting);
       returnAction.setParams({"jobPostingIdList":  postingList});
       returnAction.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                var postingList=model.postingIdList;
                var responseMsg=model.responseMsg;
                var flag=false;
                if(responseMsg=='SUCCESS'){
                  this.showToastMessage('Inactivation has been initiated for the posting/s. Please check the Posting Progression icon for current status.','Inactivate','SUCCESS');
                }else if(responseMsg=='ERROR'){
                    this.showToastMessage(model.responseDetail,'ERROR','ERROR');
                }else if(responseMsg=='PARTIAL'){
                    if(model.postingIdList!=null && model.postingIdList.length>0){
                    	this.showToastMessage(model.postingIdList[0].message,'Inactivate',model.postingIdList[0].code);    
                    }
                    
                }else if(responseMsg==null || responseMsg=='' || typeof responseMsg=='undefined'){
                    this.showToastMessage('Job Posting InActivate in RWS returned with ERROR.','ERROR','ERROR');
                }
            }else if(data.getState() == 'ERROR'){
                console.log('$A.log("Errors", a.getError())------>'+data.getError());
                this.showToastMessage('Error occured while inactivating Job Posting in RWS','ERROR','ERROR');
            }
        });
		$A.enqueueAction(returnAction);

	},
	inactivateJobPosting : function(cmp, event) {
		let jpId = cmp.get("v.recordId");
		let equestId = cmp.get("v.simpleRecord.eQuest_Job_Id__c");
		let action = cmp.get("c.inactivateJobPosting");
		action.setParams({"jpId" : jpId});

		action.setCallback(this, function(response){
			if(response.getState() === 'SUCCESS') {
				this.showToastMessage('Inactivation Successful.','SUCCESS','SUCCESS');
			}
			else if(data.getState() == 'ERROR'){
                this.showToastMessage('Error occured while inactivating Job Posting','ERROR','ERROR');
            }
		});
		$A.enqueueAction(action);
	},
    showToastMessage : function (message,title,type){
	     var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : title,
                    message: message,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: type,
                    mode: 'pester'
                });
                toastEvent.fire();
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire(); 
  }
})