({
     doInit : function(component, event, helper) {
         console.log('REcord id in conrtoller'+component.get('v.recordId'));
		 helper.inactivatePosting(component, event, helper);		 
	},
    goToMyView : function(component, event, helper) {
    	var action = component.get("c.getListViewId");
    	action.setCallback(this, function(response){
        	var state = response.getState();
        	if (state === "SUCCESS") {
            	var listviewId = response.getReturnValue();
            	sforce.one.navigateToURL("/lightning/o/Job_Posting__c/list?filterName="+listviewId);
        	}
    	});
    	$A.enqueueAction(action);
	}
})