({
	inactivatePosting : function(component, event, helper) {
        
	   var returnAction=component.get("c.massinActivateJobPostings");
       var jobPosting=component.get("v.selectedRecords");
       console.log('Selected contacts in Lightning Component'+jobPosting);
        
       var partsOfStr = jobPosting.split(',');
       var strList=[];
       var x;
       for(x in partsOfStr){
           var y=String(partsOfStr[x]);
           strList.push(y);
        }
        
       returnAction.setParams({"jobPostingIdList":  strList});
       returnAction.setCallback(this, function(response) {
            var data = response;
            var spinner = component.find("rwsSpinner");
            var model = data.getReturnValue();
            var successList=[];
            var errorList=[];
            $A.util.addClass(spinner, "slds-hide");
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                component.set("v.viewId",model.viewId);
                var postingList=model.postingIdList;
                var responseMsg=model.responseMsg;
                var flag=false;
				//console.log('***********');
				//console.log(JSON.stringify(model));
				if(model.sfdcMsg=='SUCCESS'){                    
                     if(model.postingIdList!=null && model.postingIdList.length>0){                      
                         var x=0;
                         for(x in model.postingIdList){
                             if(model.postingIdList[x].postingId.includes('JP') ) {
								if(model.postingIdList[x].message === 'SUCCESS'){
									successList.push(model.postingIdList[x].postingId+'  Inactivation Successful.'); 
								}
								else if(model.postingIdList[x].message === 'DIFF_OWNER') {
									 errorList.push(model.postingIdList[x].postingId+' Inactivation Failed: Posting not owned by You.'); 
								}
								else if(model.postingIdList[x].message === 'ALREADY_INACTIVE'){
									 errorList.push(model.postingIdList[x].postingId+' Inactivation Failed: Posting is Inactive.'); 
								}
							 }
                         }
                     }
                }
                if(responseMsg=='SUCCESS'){                    
                     if(model.postingIdList!=null && model.postingIdList.length>0){                      
                         var x=0;
                         for(x in model.postingIdList){
                             if(!model.postingIdList[x].postingId.includes('JP')) {
								successList.push(model.postingIdList[x].postingId+':-'+model.postingIdList[x].message);
							 } 
                         }
                     }
                }else if(responseMsg=='ERROR'){
                    errorList.push(model.responseDetail);
                }else if(responseMsg=='PARTIAL'){
                    if(model.postingIdList!=null && model.postingIdList.length>0){
                        if(model.postingIdList!=null && model.postingIdList.length>0){                      
                         var x=0;
                         for(x in model.postingIdList){
                             if(!model.postingIdList[x].postingId.includes('JP')) {
								 if(model.postingIdList[x].code=='SUCCESS'){
                             		successList.push(model.postingIdList[x].postingId+':-'+model.postingIdList[x].message); 
								 }else if(model.postingIdList[x].code=='ERROR'){
									 errorList.push(model.postingIdList[x].postingId+':-'+model.postingIdList[x].message); 
								 }
							 }
                           }
                       }
                    }
                }else if(!(responseMsg || model.sfdcMsg)){
                    errorList.push('Job Posting Inactivate in RWS returned with ERROR or you did not select job posting.');
                }
            }else if(data.getState() == 'ERROR'){
                errorList.push('Error occured while inactivating Job Posting in RWS');
            }           
           component.set("v.successList",successList);
           component.set("v.errorList",errorList);
        });
		$A.enqueueAction(returnAction);

	},
    showToastMessage : function (component,message,title,type){
         component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Something has gone wrong!",
            "message": "Unfortunately, there was a problem updating the record.",
            closeCallback: function() {
                alert('You closed the alert!');
            }
        });
            
  }
})