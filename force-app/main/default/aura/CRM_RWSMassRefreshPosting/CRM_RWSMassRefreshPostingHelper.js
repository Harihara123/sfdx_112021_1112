({
	refreshPosting : function(component, event, helper) {
        
	   var returnAction=component.get("c.massrefreshJobPostings");
       var jobPosting=component.get("v.selectedRecords");
       console.log('Selected contacts in Lightning Component'+jobPosting);
        
       var partsOfStr = jobPosting.split(',');
       var strList=[];
       console.log('Job Posting --partsOfStr---->'+partsOfStr);
       var x;
       for(x in partsOfStr){
           var y=String(partsOfStr[x]);
           strList.push(y);
        }
        
       console.log('Job Posting --strList---->'+strList);
        
      console.log('Job Posting --strList---->'+strList.length);

       console.log('Type of jobposting is array---->'+Array.isArray(jobPosting));
       returnAction.setParams({"jobPostingIdList": strList});
       returnAction.setCallback(this, function(response) {
            var data = response;
            var spinner = component.find("rwsSpinner");
            var model = data.getReturnValue();
            var successList=[];
            var errorList=[];
            console.log('Inside refreshmass CALBACCK##$$$$--->'+model);
           // sforce.one.navigateToURL("/lightning/o/Job_Posting__c/list?filterName="+model);
        	$A.util.addClass(spinner, "slds-hide");
            console.log('INside callback of REfresh**&*&*'+data.getState());
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                console.log('Returned model--->'+JSON.stringify(model));
                component.set("v.viewId",model.viewId);
                var postingList=model.postingIdList;
                var responseMsg=model.responseMsg;
                var flag=false;
                if(responseMsg=='SUCCESS'){
                    
                     if(model.postingIdList!=null && model.postingIdList.length>0){
                      
                         var x=0;
                         for(x in model.postingIdList){
                             
                             successList.push(model.postingIdList[x].postingId+':-'+model.postingIdList[x].message); 
                         }
                     }
                    
                 // this.showToastMessage(component,'Job Posting Refreshed successfully in RWS, It will be sync with Connected.','Refreshed','SUCCESS');
                }else if(responseMsg=='ERROR'){
                    errorList.push(model.responseDetail);
                    //this.showToastMessage(component,model.responseDetail,'ERROR','ERROR');
                }else if(responseMsg=='PARTIAL'){
                    if(model.postingIdList!=null && model.postingIdList.length>0){
                        if(model.postingIdList!=null && model.postingIdList.length>0){
                      
                         var x=0;
                         for(x in model.postingIdList){
                             if(model.postingIdList[x].code=='SUCCESS'){
                             	successList.push(model.postingIdList[x].postingId+':-'+model.postingIdList[x].message); 
                             }else if(model.postingIdList[x].code=='ERROR'){
                                 console.log('INside partial ERROR IF---postingid---->'+model.postingIdList[x].postingId);
                                 console.log('INside partial ERROR IF---message---->'+model.postingIdList[x].message);
                                 errorList.push(model.postingIdList[x].postingId+':-'+model.postingIdList[x].message); 
                             }
                           }
                       }
                    	//this.showToastMessage(component,model.postingIdList[0].message,'Refreshed',model.postingIdList[0].code);    
                    }
                }else if(responseMsg==null || responseMsg=='' || typeof responseMsg=="undefined"){
                    errorList.push('Job Posting Refresh in RWS returned with ERROR or You did not select Job Posting.');
                    //this.showToastMessage(component,'Job Posting Refresh in RWS returned with ERROR.','ERROR','ERROR');
                }
            }else if(data.getState() == 'ERROR'){
                console.log('$A.log("Errors", a.getError())------>'+data.getError());
                errorList.push('Error occured while refreshing Job Posting in RWS');
                //this.showToastMessage(component,'Error occured while refreshing Job Posting in RWS','ERROR','ERROR');
            }
           
           component.set("v.successList",successList);
           component.set("v.errorList",errorList);
        });
		$A.enqueueAction(returnAction);

	},
    showToastMessage : function (component,message,title,type){
         component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Something has gone wrong!",
            "message": "Unfortunately, there was a problem updating the record.",
            closeCallback: function() {
                alert('You closed the alert!');
            }
        });
            
  }
})