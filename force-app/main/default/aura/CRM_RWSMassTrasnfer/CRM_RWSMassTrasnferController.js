({
	onSubmit : function (cmp,event,helper){
        
        console.log('Inside ObSubmit');
        var eventFields = event.getParam("fields");
        var transferId=eventFields.Owner_To_Transfer__c;
        var selectedRecords=cmp.get("v.selectedRecords");
        
        var func = cmp.get('v.callback');
        if (func){ 
            func({ownerId:transferId});
        }
        
        console.log('Selected User Id for transfer'+transferId);
        event.preventDefault();
       // cmp.find('userEditForm').cancel(eventFields);
       
   
    },
    cancel : function (cmp,event,helper){
        console.log('Inside cancel action ');
        var listViewId = $A.get("$Label.c.RWS_List_View");
        sforce.one.navigateToURL('/lightning/o/Job_Posting__c/list?filterName='+listViewId);
        event.preventDefault();
    }
    
    
})