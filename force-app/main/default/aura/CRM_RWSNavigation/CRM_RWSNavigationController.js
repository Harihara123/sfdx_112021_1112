({
	doInit : function(component, event, helper) {
        var fReqId=component.get("v.recordId")==null?component.get("v.reqId"):component.get("v.recordId");
        console.log('Req Id----->'+fReqId);
        component.set("v.fReqId",fReqId);
     //  component.find('recordLoader').reloadRecord(true);
        
         var fcmp=component.find('recordLoader');
        fcmp.set("v.fReqId", component.get("v.fReqId"));
       
		fcmp.reloadRecord();	  
    },
    pressCheck : function(component, event, helper) {
         	var opts = document.querySelector('input[name="options"]:checked');
         	console.log('Option selected --> ',opts);
            try{
            var opts = document.querySelector('input[name="options"]:checked').id
         	if(opts=="pubPost"){
            	component.set("v.publicPost",true);
         	}
         	else if(opts=="privPost"){
            	component.set("v.privatePost",true);
            }}
            catch(e){
            }
        console.log("OPTS -->"+opts);
        console.log("Public Post pressCheck "+component.get("v.publicPost"));
        console.log("Private Post pressCheck "+component.get("v.privatePost"));
        
        
    },
    
    onCheck : function(component, event, helper) {
         var flag=component.get("v.acknowledgeVar");
         component.set("v.acknowledgeVar",!flag);
         var button = component.find('okbuttonid');
         button.set('v.disabled',flag);
    },
    
    ofccpCheck : function(component, event, helper) {
    },
    
    handleClick : function(component, event, helper){
        var recStage=component.get("v.simpleRecord").StageName;
        var OriginationReq = component.get("v.simpleRecord").Req_origination_System_Id__c;
        var PartSystemId = component.get("v.simpleRecord").Req_Origination_Partner_System_Id__c;
        
        console.log('Reqorg Id----->'+OriginationReq);
        console.log('stagename----->'+recStage);
        var validationFlag=true;
        
        if(recStage!='Draft' && recStage!='Qualified' && recStage!='Presenting' && recStage!='Interviewing'){
            
            validationFlag=false;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message: 'Job posting can be created only from Draft/Qualified Req Opportunities',
                messageTemplate: '',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
        }else{
            //Balaji Changes
            if(component.get("v.switchFlag") === 'RWS') { 
                
                if(component.get("v.isOFCCP")==true){
                    var opts = document.querySelector('input[name="options"]:checked');
                    console.log('Option selected --> ',opts);
                    try{
                        var opts = document.querySelector('input[name="options"]:checked').id
                        if(opts.id=="pubPost"){
                            component.set("v.publicPost",true);
                        }
                        else{
                            component.set("v.privatePost",true);
                        }}
                    catch(e){
                        validationFlag=false;
                        var ofccpRadio = component.find("ofccp-radio");
                        $A.util.addClass(ofccpRadio, 'slds-has-error');
                    }
                }
                if(validationFlag){       
                    component.set("v.isModal",false);
                    var reqId=component.get("v.isShow")=='SHOWREQ'?component.get("v.recordId"):component.get("v.reqId");
                    component.set("v.isShow",'HIDEREQ');
                    component.set("v.isReqSearch",true);
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                }else{
                    component.set("v.validationError",'Select a posting category');
                }
                
                var rwsURL = $A.get("$Label.c.RWS_Job_Posting_URL");
                
                if(component.get("v.acknowledgeVar")==true && validationFlag && component.get("v.isOFCCP")==false){
                    window.open(rwsURL+'/RWS/posting/savePreBBDataAndInitiatePosting.action?partnerREQID='+OriginationReq+'&partnerReqSystemID='+PartSystemId+'&ofccpFlg=false&privateFlg=false&source=ATS','_blank');
                }
                else if(component.get("v.acknowledgeVar")==true && validationFlag && component.get("v.isOFCCP")==true && component.get("v.publicPost")==true){
                    window.open(rwsURL+'/RWS/posting/savePreBBDataAndInitiatePosting.action?partnerREQID='+OriginationReq+'&partnerReqSystemID='+PartSystemId+'&ofccpFlg=true&privateFlg=false&source=ATS','_blank');
                }
                    else if(component.get("v.acknowledgeVar")==true && validationFlag && component.get("v.isOFCCP")==true && component.get("v.privatePost")==true){
                        window.open(rwsURL+'/RWS/posting/savePreBBDataAndInitiatePosting.action?partnerREQID='+OriginationReq+'&partnerReqSystemID='+PartSystemId+'&ofccpFlg=true&privateFlg=true&source=ATS','_blank');
                    }
                var compEvents = component.getEvent("jobPostingDisableEvent");      
                compEvents.setParams({ "disableFlag" : true });
                compEvents.fire();
            }
            else {
                //Balaji Changes
                helper.handleSFDCJobPosting(component, event);
            } 
        }
        
    },     
    handleClickMandatory: function(component, event, helper){
    	component.set("v.isModal",true); 
        component.set("v.acknowledgeVar",false); 
    },
    openModal:function(component, event, helper){
        component.set("v.isModal",false);
	},
    handleCancel: function(component, event, helper){
        component.set("v.isModal",false);
        component.set("v.isShow",'HIDEREQ');
        component.set("v.isReqSearch",true);
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
    	dismissActionPanel.fire();
        var compEvents = component.getEvent("jobPostingDisableEvent");
        compEvents.setParams({ "disableFlag" : true });
        compEvents.fire();
    },
    doneRendering: function(cmp, event, helper) {
        // var dismissActionPanel = $A.get("e.force:closeQuickAction");
    	 // dismissActionPanel.fire();
   },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var ofccpFlag=component.get("v.simpleRecord").Req_OFCCP_Required__c;
            console.log('OFCCP flag ',ofccpFlag);
            if(ofccpFlag){
               component.set("v.isOFCCP",true);
            } 
            console.log("Record is loaded successfully.");
        } else if(eventParams.changeType === "ERROR") {
            var errorMessage=component.get("v.recordError");
             var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: errorMessage,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
             component.set("v.isModal",false);
            var reqId=component.get("v.isShow")=='SHOWREQ'?component.get("v.recordId"):component.get("v.reqId");
            component.set("v.isShow",'HIDEREQ');
            component.set("v.isReqSearch",true);
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire(); 
            var compEvents = component.getEvent("jobPostingDisableEvent");
            compEvents.setParams({ "disableFlag" : true });
            compEvents.fire();
            // there’s an error while loading, saving, or deleting the record
        }
		helper.jobPostingSwitch(component,event);
    }
})