({
    //Balaji Changes
	jobPostingSwitch : function(cmp, event) {
		let action = cmp.get("c.switchToSFDC");
		action.setCallback(this, function(resp){
			cmp.set("v.isLoading", false);
			if(resp.getState() == 'SUCCESS') {
				let jpMap = resp.getReturnValue();

				cmp.set("v.switchFlag", jpMap.SFDC_SWITCH);
				if(jpMap.limit === 'over') {
					cmp.set("v.jobPostingLimit", true);
				}
				else {
					cmp.set("v.jobPostingLimit", false);
				}
				cmp.set("v.opcoLimit", jpMap.OPCO_LIMIT);
			}
		});
		$A.enqueueAction(action);	 
	},
    //Balaji Changes
    handleSFDCJobPosting : function(cmp, event) {
		let pageReference = {
			 				type: 'standard__component',
			 				attributes: {
			 					"componentName" : "c__C_JobPostingRedirectToLWC"
			 				},
			 				state: {
			 					"c__oppId": cmp.get("v.recordId")
			 				}
			 			};
		event.preventDefault();
		let  navService = cmp.find("navService");
		navService.navigate(pageReference);
		this.delayedRefresh(100);
	},
    //Balaji Changes
	delayedRefresh : function(milliseconds){
		let ms = milliseconds || 100;
		window.setTimeout($A.getCallback(function(){
			$A.get('e.force:refreshView').fire();
		}),ms);
	}
})