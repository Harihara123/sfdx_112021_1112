({
	onRender : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire(); 

     },
    
     handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
        
           helper.refreshPosting(component, event, helper);
            
            console.log("Record is loaded successfully.");
        } else if(eventParams.changeType === "ERROR") {
            var errorMessage=component.get("v.recordError");
             var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: errorMessage,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
              var dismissActionPanel = $A.get("e.force:closeQuickAction");
             dismissActionPanel.fire(); 
        }
    }
})