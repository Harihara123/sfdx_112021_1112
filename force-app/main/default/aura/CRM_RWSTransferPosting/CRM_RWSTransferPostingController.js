({
	onRender : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire(); 

     },
    onSubmit : function (component,event,helper){
        
        console.log('Inside ObSubmit');
        var eventFields = event.getParam("fields");
        var transferId=eventFields.Owner_To_Transfer__c;
        
        component.set("v.transferId",transferId);
        
        helper.transferPosting(component,event,helper);
        
        console.log('Selected User Id for transfer'+transferId);
        event.preventDefault();
       // cmp.find('userEditForm').cancel(eventFields);
       
   
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            
         var ownerId=component.get("v.simpleRecord.OwnerId");  
         var userId = $A.get("$SObjectType.CurrentUser.Id");

            if(ownerId==userId){
                component.set("v.ownerSelected",true);
           		
            }else{
                component.set("v.ownerSelected",false);
                helper.showToastMessage('Job Posting can only be transferred by its owner.','ERROR','ERROR');
            }
            
            console.log("Record is loaded successfully.");
        } else if(eventParams.changeType === "ERROR") {
            var errorMessage=component.get("v.recordError");
             var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: errorMessage,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
              var dismissActionPanel = $A.get("e.force:closeQuickAction");
             dismissActionPanel.fire(); 
        }
    }, 
    handleCancel : function (component,event,helper){
         event.preventDefault();
          var dismissActionPanel = $A.get("e.force:closeQuickAction");
           dismissActionPanel.fire(); 
         /*var jobPosting=component.get("v.recordId");
         var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
         "url": '/lightning/r/Job_Posting__c/'+jobPosting+'/view'
         });
         urlEvent.fire();*/
         

    }
})