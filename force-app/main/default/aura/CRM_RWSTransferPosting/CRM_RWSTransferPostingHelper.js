({
	transferPosting : function(component, event, helper) {
        
	   var returnAction=component.get("c.transferJobPostings");
       var jobPosting=component.get("v.recordId");
       var postingList=[];
       postingList.push(jobPosting);
       var transferId=component.get("v.transferId");
       returnAction.setParams({"ownerId":transferId,"jobPostingIdList":  postingList});
       returnAction.setCallback(this, function(response) {
            var data = response;
            var spinner = component.find("rwsSpinner");
        	$A.util.addClass(spinner, "slds-hide");
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                var postingList=model.postingIdList;
                var responseMsg=model.responseMsg;
                var flag=false;
                if(responseMsg=='SUCCESS'){
                  this.showToastMessage('Transfer has been initiated for the posting/s.Please check the Posting Progress icon for current status.','Transfer','Transfer');
                }else if(responseMsg=='ERROR'){
                    this.showToastMessage(model.responseDetail,'ERROR','ERROR');
                }else if(responseMsg=='PARTIAL'){
                    if(model.postingIdList!=null && model.postingIdList.length>0){
                    	this.showToastMessage(model.postingIdList[0].message,'Transfer',model.postingIdList[0].code);    
                    }
                }else if(responseMsg==null || responseMsg=='' || typeof responseMsg=="undefined"){
                    this.showToastMessage('Job Posting Transfer in RWS returned with ERROR.','ERROR','ERROR');
                }
            }else if(data.getState() == 'ERROR'){
                console.log('$A.log("Errors", a.getError())------>'+data.getError());
                this.showToastMessage('Error occured while transferring Job Posting in RWS','ERROR','ERROR');
            }
           
        });
		$A.enqueueAction(returnAction);

	},
    showToastMessage : function (message,title,type){
	     var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : title,
                    message: message,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: type,
                    mode: 'sticky'
                });
                toastEvent.fire();
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire(); 
  }
})