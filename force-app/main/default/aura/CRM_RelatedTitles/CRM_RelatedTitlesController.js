({
	
	fetchJobTitles : function(cmp,event,helper) {
		if(event) {
			var callback;
			var params = event.getParam('arguments');		

			if(params) {
				let jobTitle = params.jobTitle;
				let opco = params.opco;
				helper.fetchJobTitlesRelated(cmp,event, jobTitle,opco);
			}
		}
	},
	prepareRelatedTitles : function(cmp,event,helper) {
		helper.prepareTitlesJSON(cmp,event);
	}
})