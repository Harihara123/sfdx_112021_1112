({
	fetchJobTitlesRelated: function (cmp,event, jobTitle,opco) {
		let parentCmp = cmp.get("v.parent");
		var action = cmp.get("c.fetchRelatedJobTitles");
		
		action.setParams({
        	"text" : jobTitle,
			"opco" : opco
		});

		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
            	if (response.getReturnValue() !== "") {
					parentCmp.set("v.updatedSuggestedJobTitles",[]);
					parentCmp.set("v.isUpdatedRelatedTitles",false);
					const parsedJobTitles = JSON.parse(response.getReturnValue());
					if(parsedJobTitles != null) {
						let relatedTerms = parsedJobTitles.suggestedTitles;
						if(relatedTerms != null) {
							let relatedTitles;
							let suggJobTitles = [];
							let initialSuggTitles = [];
							relatedTitles = relatedTerms.split(";");
							relatedTitles.forEach(title => {
								let titleDetails,initialSuggTitle;
								titleDetails = title.split(":");
								initialSuggTitle = {"term":titleDetails[0], "removed":false, "weight":titleDetails[1]};
								suggJobTitles.push(titleDetails[0]);
								initialSuggTitles.push(initialSuggTitle);
							})
							parentCmp.set("v.initialSuggTitles",initialSuggTitles);
							parentCmp.set('v.suggJobTitles', suggJobTitles);
						}else {
							parentCmp.set('v.suggJobTitles', []);
							parentCmp.set("v.initialSuggTitles",null);
						}
					} else {
						parentCmp.set('v.suggJobTitles', []);
						parentCmp.set("v.initialSuggTitles",null);
					}
            	}
			} else {
				parentCmp.set('v.suggJobTitles', []);
				parentCmp.set("v.initialSuggTitles",null);
			}
		});
		$A.enqueueAction(action);
	},
	prepareTitlesJSON : function(cmp,event) {
		
		let parentCmp = cmp.get("v.parent");
		let initialRelatedTitles = parentCmp.get("v.initialSuggTitles");
		let updatedRelatedTitles = parentCmp.get("v.updatedSuggestedJobTitles");
		let isUpdatedTitles = parentCmp.get("v.isUpdatedRelatedTitles");
		let relatedTitleArr = [];
		if(initialRelatedTitles != null && isUpdatedTitles) {
			//Create Related titles object comparing initial suggested titles with Updated titles
			for(let i=0;i<initialRelatedTitles.length;i++) {
				let initialRelatedTitle = initialRelatedTitles[i];
				let newRelatedTitle;
				if(updatedRelatedTitles.includes(initialRelatedTitle.term)) {
					newRelatedTitle = {"term":initialRelatedTitle.term, "removed":false, "weight":initialRelatedTitle.weight};
					relatedTitleArr.push(newRelatedTitle);
				} else{
					newRelatedTitle = {"term":initialRelatedTitle.term, "removed":true, "weight":initialRelatedTitle.weight};
					relatedTitleArr.push(newRelatedTitle);
				}
			
			}

			//Create Related titles object comparing Updated titles with initial suggested titles which user explicitly entered
			for(let i=0;i<updatedRelatedTitles.length;i++) {
				let newRelatedTitle;
				let initialTitles = parentCmp.get("v.suggJobTitles");			
				if(!initialTitles.includes(updatedRelatedTitles[i])) {
					newRelatedTitle = {"term":updatedRelatedTitles[i], "removed":false, "weight":0};
					relatedTitleArr.push(newRelatedTitle);
				}
			} 
		} else {
			if(initialRelatedTitles != null) {
				for(let i=0;i<initialRelatedTitles.length;i++) {
					let newRelatedTitle;
					newRelatedTitle = {"term":initialRelatedTitles[i].term, "removed":false, "weight":initialRelatedTitles[i].weight};
					relatedTitleArr.push(newRelatedTitle);
				}
			} else {
				if(isUpdatedTitles) { // If no related titles populated and user enters a title from lookup
					for(let i=0;i<updatedRelatedTitles.length;i++) {
						let newRelatedTitle;
						newRelatedTitle = {"term":updatedRelatedTitles[i], "removed":false, "weight":0};
						relatedTitleArr.push(newRelatedTitle);
					}
				}
			}
		}
		if(relatedTitleArr.length > 0)
			parentCmp.set("v.finalRelatedTitlesList",relatedTitleArr);
	}
	
})