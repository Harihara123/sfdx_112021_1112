({	
	loadOppRecord: function(component,event,helper) {
        
        if(component.get("v.countriesMap") === null){
			var action = component.get("c.getCountryMappingsWithAbbr");
			action.setStorable();
			action.setCallback(this,function(response) {
				let countryState=response.getReturnValue();
                component.set("v.countriesMap",countryState.countriesMap);
                var oppRecord=component.get("v.oppRecordFields");
                if(typeof oppRecord!='undefined' && oppRecord!=null){
                    if(oppRecord.Req_OpCo_Code__c!='AP' && oppRecord.Req_OpCo_Code__c!='MLA' 
                       && oppRecord.Req_OpCo_Code__c!='AG_EMEA' && oppRecord.Req_OpCo_Code__c!='RPO'){
                    	component.set("v.mandStateFlag",true);    
                    }
               
                let countryMdt=helper.getKeyByValue(component.get("v.countriesMap"),oppRecord.Req_Worksite_Country__c);
                if(typeof countryMdt!='undefined' && countryMdt!=null){
                	component.set("v.MailingCountryKey",countryMdt.Label);
                	oppRecord.Req_Worksite_Country__c=countryMdt.CountryName__c;
                }
                component.set("v.Opportunity",oppRecord);
                component.set("v.presetLocation",oppRecord.Req_Worksite_Street__c);
                component.set("v.MailingCityKey",oppRecord.Req_Worksite_City__c)
                component.set("v.MailingPostalCode",oppRecord.Req_Worksite_Postal_Code__c);
                }     
                component.set("v.eventSourceFlag",false); 
           });
			$A.enqueueAction(action);
		}
        
        console.log('Inside Load Opp Record');
		
	},
    doInit : function(component,event,helper) {
       
    },
    //S-65378 - Added by KK - Update Mailing fields on location selection
    handleLocationSelection : function(component,event, helper) {
		component.set("v.isGoogleLocation", true);
        component.set("v.eventSourceFlag",true);   
        var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode,countrymap,displayText;
		mailingstreet = '';
		streetnum = '';
		streetname = '';
		city = '';
		state = '';
		country = '';
		countrycode = '';
		postcode = '';
		displayText = '';
		var addr = event.getParam("addrComponents");
		var latlong = event.getParam("latlong");     
		countrymap = component.get("v.countriesMap"); 
		displayText = event.getParam("displayText");
        //console.log('address component'+JSON.stringify(addr));
        for (var i=0; i<addr.length; i++) {
            var addTypes=addr[i].types;
            if (addr[i].types.indexOf("street_number") >= 0) {
                streetnum = addr[i].long_name;
            }
            if (addr[i].types.indexOf("route") >= 0) {
                streetname = addr[i].long_name;
            }        
            if (addr[i].types.indexOf("postal_code") >= 0) {
                postcode = addr[i].long_name;
            }
            if (addr[i].types.indexOf("locality") >= 0) {
                city = addr[i].long_name;
            }
            if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
                state = addr[i].long_name;
            }
            if (addr[i].types.indexOf("country") >= 0) {
                country = addr[i].long_name;
                countrycode = countrymap[addr[i].short_name];
            }        
        }
        //console.log('teststreet',streetnum,streetname);
        if(streetnum) {
            mailingstreet = streetnum;
        } 
        if(streetnum && streetname) {
            mailingstreet = mailingstreet + ' ';
        }
        if(streetname) {
            if(mailingstreet != ''){
                mailingstreet = mailingstreet + streetname;        
            } else {
                mailingstreet = streetname;
            }
        }
        //console.log(mailingstreet,city,state,countrycode,country,postcode,displaytext);
       
       // component.set("v.addinsertFlag",false);
        component.set("v.Opportunity.Req_Worksite_Street__c",mailingstreet);
        component.set("v.Opportunity.Req_Worksite_City__c",city);
        component.set("v.Opportunity.Req_Worksite_State__c",state);
        component.set("v.Opportunity.Req_Worksite_Country__c",country);
        component.set("v.Opportunity.Req_Worksite_Postal_Code__c",postcode);
        component.set("v.Opportunity.StreetAddress2__c","");
       
        
		//var str = displayText.substr(0,displayText.indexOf(',')); 
        component.set("v.presetLocation",mailingstreet);
		//component.set("v.streetAddress2","");
		//S-96255 - Update MailLat, MailLong fields on Contact
		if(latlong && (mailingstreet || city || postcode)){
			component.set("v.Opportunity.Req_GeoLocation__Latitude__s",latlong.lat);
			component.set("v.Opportunity.Req_GeoLocation__Longitude__s",latlong.lng);				
		} else {
			component.set("v.Opportunity.Req_GeoLocation__Latitude__s",null);
			component.set("v.Opportunity.Req_GeoLocation__Longitude__s",null);				
		}
         helper.validateAddress(component, event);
		//component.set("v.contact.LatLongSource__c","Google");
		component.set("v.viewState","EDIT");
		component.set("v.parentInitialLoad",true);
		var countryCode = helper.getKeyByValue(component.get("v.countriesMap"),countrycode);
		component.set("v.MailingCountryKey",countryCode);
    },
    updateMailingCountryCode: function(component, event, helper) {
		helper.updateMailingCountryCode(component, event);
	},
    enableInlineEdit :function(component, event, helper) {
       console.log('inline edit pressed');
       component.set("v.invokeInlineEdit",!component.get("v.invokeInlineEdit"));
       component.set("v.editMode",!component.get("v.editMode"));
    },
    presetLocationValueChange :function(component, event, helper) {
        
    	console.log('presetLocationValueChange----->');
        if(component.get("v.eventSourceFlag")!=true){
	       var opp = component.get("v.Opportunity");
           opp.Req_Worksite_Street__c=component.get("v.presetLocation");
           component.set("v.Opportunity",opp);
        }
     	component.set("v.eventSourceFlag",false);  
    },
	addFieldValidationChange:function(component, event, helper) {
        
        var testmap=component.get("v.addFieldMessagedMap");
        console.log('addFieldMessagedMap----->'+testmap);
    }   
})