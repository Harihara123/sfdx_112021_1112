({
   getKeyByValue : function(map, searchValue) {
		for(var key in map) {
			if (map[key].CountryCode__c === searchValue || map[key].CountryName__c  === searchValue || map[key].Label === searchValue) {
				return map[key];
			}
		}
	},
    updateMailingCountryCode: function(component, event) {
        var conKey = component.get('v.MailingCountryKey');
        var countrymap = component.get("v.countriesMap");
        var countrycode ;
        if (conKey !== null && typeof conKey !== 'undefined') {
            countrycode = countrymap[conKey];
            if(countrycode !== null && typeof countrycode !== 'undefined'){
                component.set("v.oppRecordFields.MailingCountry",countrycode);
            } else {
				component.set("v.oppRecordFields.MailingCountry","");
			}
        }
		/*if (component.get('v.initialLoad')) {			
			component.set("v.initialLoad",false);
		} else {
			component.set("v.parentInitialLoad",false);
		}*/
    },
    validateAddress:function(cmp,event){
         var oppAddress=cmp.get("v.Opportunity");
         var fieldMessages=cmp.get("v.addFieldMessage");
         if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
             cmp.set("v.streetErrorVarName","Street cannot be blank.");
             fieldMessages.push("Street cannot be blank.");
           }else{
             cmp.set("v.streetErrorVarName","");
          }
        
         
         if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
             cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
             fieldMessages.push("City cannot be blank.");
           }else{
             cmp.set("v.cityTypeErrorVarName","");
          }
        
         if(oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == ''){
             cmp.set("v.stateErrorVarName","State cannot be blank.");
             fieldMessages.push("State cannot be blank.");
           }else{
             cmp.set("v.stateErrorVarName","");
          }
        
         if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
            cmp.set("v.countryVarName","Country cannot be blank.");
            fieldMessages.push("Country cannot be blank.");
           }else{
             cmp.set("v.countryVarName","");
          }
        
         if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
             cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
             fieldMessages.push("Postal Code cannot be blank.");
          }else{
             cmp.set("v.zipErrorVarName","");
          }
        
        
        if(typeof fieldMessages!='undefined' && fieldMessages!=null && fieldMessages.length>0){
            cmp.set("v.insertFlag",true);
            cmp.set("v.addFieldMessage",fieldMessages);
        }else{
             cmp.set("v.insertFlag",false);
        }
        
    }
	
})