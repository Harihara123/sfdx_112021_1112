({
	saveMetricData : function(component, event, helper) {
        var ParentComponent = component.get("v.parent");
    	component.set("v.reqMetrics.Origination_Page__c",ParentComponent.getName());
        var metObj=component.get("v.reqMetrics");        
		var action = component.get("c.saveReqMetrics");
        action.setParams({"reqMetricsData": JSON.stringify(metObj)});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var recCreated='true';
            }
        });
        $A.enqueueAction(action);
	},
    mapVMSReqData : function(component, event, helper) {
        var ParentComponent = component.get("v.parent");
        component.set("v.reqMetrics.Skill_Speciality_From_Model__c", ParentComponent.get("v.Req_Skill_Specialty__c"));
        component.set("v.reqMetrics.F_NF_From_Model__c", ParentComponent.get("v.Functional_Non_Functional__c"));
        component.set("v.reqMetrics.SOCONET__c", ParentComponent.get("v.Soc_onet__c"));        
        component.set("v.reqMetrics.Job_Title__c",ParentComponent.get("v.jobtitle"));
        component.set("v.reqMetrics.Job_Description__c", ParentComponent.get("v.jobDescription"));
        component.set("v.reqMetrics.OpptyId__c",ParentComponent.get("v.recordId"));
    }
})