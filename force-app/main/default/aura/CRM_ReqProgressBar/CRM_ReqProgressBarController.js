({
    /* Get the opco based required fields from custom settings */
    doInit : function(component, event, helper){
        helper.initilizeProgressBar(component, event );
    },
    
    /*wait for the parent component to load on the DOM */
    onRender : function(component, event, helper){
        helper.doProgress(component);
    },
    
    getStageRequiredFieds : function(component, event, helper) {
        if( component.get("v.stage") == '--None--' ){
            component.set("v.stage", 'Draft');
        }        
        helper.initilizeProgressBar(component, event );
	},
    
})