({
	initilizeProgressBar : function(component, event) {
        
		var req_stage;
        var action = component.get("c.getProgressBarDetails");
        
        if( $A.util.isUndefined(component.get("v.TGS")) || $A.util.isEmpty( component.get("v.TGS") ) ) {
            component.set("v.TGS", "No");
        }
        if( $A.util.isUndefined(component.get("v.stage")) || $A.util.isEmpty( component.get("v.stage") ) ) {
            req_stage = 'Draft';
        }else{
            //Adding w.r.t REDZONE
            if(component.get("v.stage")=="RedZone"){
                req_stage = 'Qualified';
            }else{
                req_stage = component.get("v.stage");
            }          
        }
        /*if( $A.util.isUndefined(component.get("v.stage")) || $A.util.isEmpty( component.get("v.stage") ) ) {
            component.set("v.stage", 'Draft');
        }*/
        if( $A.util.isUndefined(component.get("v.placementType")) || $A.util.isEmpty( component.get("v.placementType") ) ) {
            component.set("v.placementType", 'Contract');
        } 
        action.setParams({"stage": req_stage,
                          "placementType": component.get("v.placementType"),
                          "TGS": component.get("v.TGS")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fields= response.getReturnValue();
                component.set("v.requiredFields", fields );
            }
        });
        $A.enqueueAction(action);		
	},
    
    doProgress : function( component) {
        
        var createEnterpriseReqCmp = component.get("v.parent");
        var requiredFields = component.get("v.requiredFields");
        if(requiredFields != undefined && requiredFields.length > 0){
            var progress = 0;  
        	var progressLength = 100/requiredFields.length;
        
        /* generic lightning inputs */
        var reqInputs = createEnterpriseReqCmp.find("container").find({instancesOf : "lightning:input"});
        for (var j=0;j<reqInputs.length;j++) {
            for(var i = 0; i < requiredFields.length; i++) {
                if( requiredFields[i].aura_id__c == reqInputs[j].getLocalId() && 
                   (!( $A.util.isUndefined( reqInputs[j].get('v.value') ) || $A.util.isEmpty( reqInputs[j].get('v.value') ) ) ) ){
                    progress = progress + progressLength;
                }
            }
        }
        
        /* ltng lookup inputs */
        var reqlookups = createEnterpriseReqCmp.find("container").find({instancesOf : "c:C_LookupLtng"});
        for (var k=0; k<reqlookups.length;k++) {
            for(var i = 0; i < requiredFields.length; i++) {
                if( requiredFields[i].aura_id__c == reqlookups[k].getLocalId() &&
                   (!( $A.util.isUndefined( reqlookups[k].get('v.comboValue') ) || $A.util.isEmpty( reqlookups[k].get('v.comboValue') ) ) ) ){
                    progress = progress + progressLength;
                }
            }
        }
        
        /* lightning select */
        var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "lightning:select"});
        for (var j=0;j<reqSelects.length;j++) {
            for(var i = 0; i < requiredFields.length; i++) {
                if( requiredFields[i].aura_id__c == reqSelects[j].getLocalId() && 
                   (!( $A.util.isUndefined( reqSelects[j].get('v.value') ) || $A.util.isEmpty( reqSelects[j].get('v.value') ) ) ) 
                   && reqSelects[j].get('v.value') != '--None--' ){
                    	progress = progress + progressLength;
                }
            }
        }
        
        /* lightning radio */
        var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "lightning:radioGroup"});
        for (var j=0;j<reqSelects.length;j++) {
            for(var i = 0; i < requiredFields.length; i++) {
                if( requiredFields[i].aura_id__c == reqSelects[j].getLocalId() && 
                   (!( $A.util.isUndefined( reqSelects[j].get('v.value') ) || $A.util.isEmpty( reqSelects[j].get('v.value') ) ) ) 
                   && reqSelects[j].get('v.value') != '--None--' ){
                    	progress = progress + progressLength;
                }
            }
        }
        
        /* lightning textarea */
        var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "lightning:textarea"});
        for (var j=0;j<reqSelects.length;j++) {
            for(var i = 0; i < requiredFields.length; i++) {
                if( requiredFields[i].aura_id__c == reqSelects[j].getLocalId() && 
                   (!( $A.util.isUndefined( reqSelects[j].get('v.value') ) || $A.util.isEmpty( reqSelects[j].get('v.value') ) ) ) 
                   && reqSelects[j].get('v.value') != '--None--' ){
                    	progress = progress + progressLength;
                }
            }
        }
        
        
        /* CRM_EmeaReqSkillCardInd lookup inputs  */
        var reqlookups = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_EmeaReqSkillCardInd"});
        for (var k=0; k<reqlookups.length;k++) {
            for(var i = 0; i < requiredFields.length; i++) {
                if( requiredFields[i].aura_id__c == reqlookups[k].getLocalId() &&
                   (!( $A.util.isUndefined( reqlookups[k].get('v.skills') ) || $A.util.isEmpty( reqlookups[k].get('v.skills') ) ) ) ){
                    progress = progress + progressLength;
                }
            }
        }
        
        /* CRM_EmeaReqLocation lookup inputs  */
        var reqLocInput = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_EmeaReqLocation"});
        for (var k=0; k<reqLocInput.length;k++) {
            for(var i = 0; i < requiredFields.length; i++) {
                if( requiredFields[i].aura_id__c == reqLocInput[k].getLocalId() &&
                   (!( $A.util.isUndefined( reqLocInput[k].get('v.locationProgress') ) || $A.util.isEmpty( reqLocInput[k].get('v.locationProgress') ) ) ) ){
                    progress = progress + progressLength;
                }
            }
        }

             /* lwcPillsLookup lookup inputs  */
            var reqlookups = createEnterpriseReqCmp.find("container").find({instancesOf : "c:lwcPillsLookup"});
            for (var k=0; k<reqlookups.length;k++) {
                for(var i = 0; i < requiredFields.length; i++) {
                    if( requiredFields[i].aura_id__c == reqlookups[k].getLocalId() &&
                       (!( $A.util.isUndefined( reqlookups[k].get('v.skills') ) || $A.util.isEmpty( reqlookups[k].get('v.skills') ) ) ) ){
                        progress = progress + progressLength;
                    }
                }
            }
            
            /*TGS section data*/
            var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_TGS"});
            for (var j=0;j<reqSelects.length;j++) {
                var TotalTGSRequiredFields=0;
                var TGSFieldsProgress=0;
                for(var i = 0; i < requiredFields.length; i++) {                    
                    if(createEnterpriseReqCmp.find(reqSelects[j].getLocalId()).find(requiredFields[i].aura_id__c)!=undefined){
                        if(createEnterpriseReqCmp.find(reqSelects[j].getLocalId()).find(requiredFields[i].aura_id__c).get('v.value')!=undefined &&
                          createEnterpriseReqCmp.find(reqSelects[j].getLocalId()).find(requiredFields[i].aura_id__c).get('v.value')!= '--None--' ){
                            TGSFieldsProgress=TGSFieldsProgress++;
                            //progress = progress + progressLength;
                        }
                    TotalTGSRequiredFields=TotalTGSRequiredFields++;
                    }                    
                }
                if(TGSFieldsProgress>0){
                    progress = progress + (progressLength*TGSFieldsProgress);
                }                
            }
            
            component.set('v.progress', progress );
        }        
    }
    
    
})