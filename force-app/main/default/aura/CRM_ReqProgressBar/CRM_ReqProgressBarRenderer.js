({
    // Your renderer method overrides go here
    afterRender: function (component, helper) {
        
        this.superAfterRender();
        
        let clikedFunction = (ev) => {
            helper.doProgress(component);
        }
        
        let keyupFunction = (ev) => {
            if (ev.keyCode == 9) {
                helper.doProgress(component);
            }
        }
        
        let req_container = document.getElementById("container");
    	req_container.addEventListener('click', clikedFunction);
        req_container.addEventListener('keyup', keyupFunction);
        
    },
            
               
})