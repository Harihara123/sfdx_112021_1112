({
	/* Get the opco based required fields from custom settings */
    doInit : function(component, event, helper){
        helper.getAllRequiredFields(component, event );
    },
    getStageRequiredFieds : function(component, event, helper) {
        helper.clearRequiredFields(component, event );
        helper.getAllRequiredFields(component, event );
    },
    getStageRequiredFiedsforPlacementType : function(component, event, helper) {
        if(component.get("v.cmpType") == 'Create'){
            helper.clearRequiredFields(component, event );
            helper.getAllRequiredFields(component, event );
        }
    },
    validateFields : function(component, event, helper) {
        /*if(component.get("v.cmpType") == 'Create'){
            helper.clearRequiredFields(component, event );
            helper.getAllReqFields(component, event, helper);
        }else if(component.get("v.cmpType") == 'Edit'){*/
        var isvalid = true;
        var requiredFields = component.get("v.requiredFields");
        var createEnterpriseReqCmp = component.get("v.parent");
        var field_api, error_var, field_value, error_count=0;
        var cmptype = component.get("v.cmpType");
        for (var i=0; i<requiredFields.length; i++) {
            if(cmptype == 'Create'){
                field_api =  requiredFields[i].field_api__c;    
            }else if(cmptype == 'Edit'){
                if(requiredFields[i].Field_API_Edit__c == 'prodId'){
                    field_api =  'prodId';
                }else if(requiredFields[i].Field_API_Edit__c == 'TGS'){
                     field_api = 'TGS';   
                }else{
                field_api =  requiredFields[i].aura_id__c; 
                	if(field_api == 'locationId'){
                    	field_api =  requiredFields[i].field_api__c; 
                	}else if(field_api == 'jobtitleid'){
                    	field_api =  requiredFields[i].Field_API_Edit__c;
                	}else if(field_api == 'crmSkillV'){
                    	field_api =  requiredFields[i].Field_API_Edit__c;
                	}
              	}      
            }
            error_var = requiredFields[i].error_var__c;
            console.log('--field_api--'+field_api);
            if(cmptype == 'Create'){
                if(field_api == 'presetLocation'){
                    //field_value = createEnterpriseReqCmp.get("v."+field_api);
                    field_value = createEnterpriseReqCmp.get("v.locationProgress");                    
                }
                else if(field_api == 'prod'){
                    field_value = createEnterpriseReqCmp.find(requiredFields[i].aura_id__c).get("v.value"); 
                }else if(field_api == 'TGS'){
                    //var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_TGS"});
                    field_value=createEnterpriseReqCmp.find("tgsId").find(requiredFields[i].aura_id__c).get('v.value');
                    
                    //field_value = createEnterpriseReqCmp.find(requiredFields[i].aura_id__c).get("v.value"); 
                }else{
                    field_value = createEnterpriseReqCmp.get("v."+field_api);
                }                
            }else if(cmptype == 'Edit'){
                if(field_api == 'presetLocation'){
                    //field_value = createEnterpriseReqCmp.get("v."+field_api);
                    field_value = createEnterpriseReqCmp.get("v.locationProgress");                    
                }else if(field_api == 'jobtitle[0].name'){
                    field_value = createEnterpriseReqCmp.get("v."+field_api);                    
                }else if(field_api == 'topSkills'){
                    field_value = createEnterpriseReqCmp.get("v."+field_api);                    
                }else if(field_api == 'prodId'){
                    if(requiredFields[i].aura_id__c == 'productIdPicklist'){
                    	field_value=createEnterpriseReqCmp.get("v.req.Product__c"); 
                    }else{
                        if(createEnterpriseReqCmp.find(field_api) != undefined)
                        field_value=createEnterpriseReqCmp.find(field_api).find(requiredFields[i].aura_id__c).get('v.value'); 
                    }
                        
                        //field_value = createEnterpriseReqCmp.find(requiredFields[i].aura_id__c).get("v.value");
                    /*var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_ProductHierarchy_New"});
                    if(requiredFields[i].aura_id__c == 'productIdPicklist'){
                        field_value=createEnterpriseReqCmp.get("v.req.Product__c");
                    }else{
                        field_value=createEnterpriseReqCmp.find(reqSelects[0].getLocalId()).find(requiredFields[i].aura_id__c).get('v.value'); 
                    }*/
                    
                }else if(field_api == 'TGS'){
                    if(createEnterpriseReqCmp.find("tgsId") != undefined)
                    field_value=createEnterpriseReqCmp.find("tgsId").find(requiredFields[i].aura_id__c).get('v.value');                   
                }else{
                    field_value = createEnterpriseReqCmp.find(field_api).get("v.value");
                }                     
            }
            
            console.log('--field_value--'+field_value); 
            if( $A.util.isUndefined( field_value ) || $A.util.isEmpty( field_value ) || field_value == '--None--' ){
                if(isvalid == true && ( requiredFields[i].label_Id__c != null || requiredFields[i].label_Id__c!="" || requiredFields[i].label_Id__c!= undefined)){
                    console.log('error - focus - field = ' + requiredFields[i].label_Id__c );
                    if(field_api == 'TGS'){
                        //var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_TGS"});
                        //createEnterpriseReqCmp.find(reqSelects[0].getLocalId()).set("v.focus_id", requiredFields[i].label_Id__c);
						createEnterpriseReqCmp.find("tgsId").set("v.focus_id", requiredFields[i].label_Id__c);                                                
                        //createEnterpriseReqCmp.find(reqSelects[0].getLocalId()).find(requiredFields[i].aura_id__c).set("v.focus_id", requiredFields[i].label_Id__c);
                    }else if(field_api == 'prodId'){
                    	createEnterpriseReqCmp.find(field_api).set("v.focus_id", requiredFields[i].label_Id__c);
                	}else{
                        createEnterpriseReqCmp.set("v.focus_id", requiredFields[i].label_Id__c);
                    }                   
                }
                isvalid = false;
                //createEnterpriseReqCmp.set("v."+error_var, requiredFields[i].Name + " is required.");
                if(field_api == 'TGS'){
                    //var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_TGS"});
                    //createEnterpriseReqCmp.find(reqSelects[0].getLocalId()).set("v."+error_var, requiredFields[i].Full_Name__c + " is required.");
                    createEnterpriseReqCmp.find("tgsId").set("v."+error_var, requiredFields[i].Full_Name__c + " is required.");
                }else if(field_api == 'prodId'){
                    createEnterpriseReqCmp.find(field_api).set("v."+error_var, requiredFields[i].Full_Name__c + " is required.");
                }else{
                    createEnterpriseReqCmp.set("v."+error_var, requiredFields[i].Full_Name__c + " is required.");
                }                
                //alert( error_var +  ' ----> ' + requiredFields[i].Name + " cannot be blank.");
                error_count++;
            }else{
                if(field_api == 'TGS'){
                    //var reqSelects = createEnterpriseReqCmp.find("container").find({instancesOf : "c:CRM_TGS"});
                    //createEnterpriseReqCmp.find(reqSelects[0].getLocalId()).set("v."+error_var, "");
                    if(createEnterpriseReqCmp.find("tgsId") != undefined)
                    createEnterpriseReqCmp.find("tgsId").set("v."+error_var, "");
                }else if(field_api == 'prodId'){
                    createEnterpriseReqCmp.find(field_api).set("v."+error_var,"");
                }else{
                    createEnterpriseReqCmp.set("v."+error_var, "");
                }                
            }
        }
        
        return {'isvalid': isvalid , 'error_count':error_count};        
    },
    validateEditFields : function(component, event,helper){
        helper.clearRequiredFields(component, event );
        helper.getrequiredfieldonEdit(component, event,helper);
    },
    clearReqFieldsOnCancel : function(component, event,helper){
        helper.clearRequiredFields(component, event );
    }         
})