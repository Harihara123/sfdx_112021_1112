({
    getAllRequiredFields :  function(component, event ) {
        
        var req_stage;
		var req_placement;
        var TGS;
        
        if( $A.util.isUndefined(component.get("v.TGS")) || $A.util.isEmpty( component.get("v.TGS") ) ) {
            TGS = 'No';
        }else{
            TGS = component.get("v.TGS");
        }
               
        if( $A.util.isUndefined(component.get("v.stage")) || $A.util.isEmpty( component.get("v.stage") ) ) {
            req_stage = 'Draft';
        }else{
            req_stage = component.get("v.stage");
        }
        if( $A.util.isUndefined(component.get("v.placementType")) || $A.util.isEmpty( component.get("v.placementType") ) ) {
            req_placement = 'Contract';
        }else{
            req_placement = component.get("v.placementType");
        }
        var action = component.get("c.getProgressBarDetails");
        action.setParams({"stage": req_stage,
                          "placementType": req_placement,
                          "TGS": TGS
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fields= response.getReturnValue();
                component.set("v.requiredFields", fields );
                if(component.get("v.firstload") == false ){
                    this.setRequiredFields(component);    
                }else{
                    component.set("v.firstload", false );
                }
                
            }
        });
        $A.enqueueAction(action);		
    },
    
    clearRequiredFields :  function(component, event) {
        var requiredFields = component.get("v.requiredFields");
        
        for (var i=0; i<requiredFields.length; i++) {
            var element = document.getElementById( requiredFields[i].label_Id__c );
            
            if( !($A.util.isUndefined( element ) || $A.util.isEmpty( element )) ){
                element.classList.remove("required-field");    
            }
			/* Clear all the error validations on changing req stage */            
            if(requiredFields[i].aura_id__c == 'locationId'){
                createEnterpriseReqCmp.set("v.addFielMessagedMap",{});
            }else{                
            	var createEnterpriseReqCmp = component.get("v.parent");
            	createEnterpriseReqCmp.set("v."+requiredFields[i].error_var__c, "");
            }           
        }
    },
    
    setRequiredFields: function( component) {
        
        var requiredFields = component.get("v.requiredFields");
        var createEnterpriseReqCmp = component.get("v.parent");
        
        if(typeof requiredFields !== "undefined") {
        
        for (var i=0; i<requiredFields.length; i++) {
            
            var element = document.getElementById( requiredFields[i].label_Id__c );
            
            if(  !($A.util.isUndefined( element ) || $A.util.isEmpty( element )) ){
                /* Adding asterisk to all the requited field labels */
                element.classList.add("required-field");
            }
            
        }
      }     
    },    
    getrequiredfieldonEdit : function(component, event, helper){
        var req_stage;
        var req_placement;
        var TGS;
        
        if( $A.util.isUndefined(component.get("v.TGS")) || $A.util.isEmpty( component.get("v.TGS") ) ) {
            TGS = 'No';
        }else{
            TGS = component.get("v.TGS");
        }
        if( $A.util.isUndefined(component.get("v.stage")) || $A.util.isEmpty( component.get("v.stage") ) ) {
            req_stage = 'Draft';
        }else{
            req_stage = component.get("v.stage");
        }
        if( $A.util.isUndefined(component.get("v.placementType")) || $A.util.isEmpty( component.get("v.placementType") ) ) {
            req_placement = 'Contract';
        }else{
            req_placement = component.get("v.placementType");
        }
        var action = component.get("c.getProgressBarDetails");
        action.setParams({"stage": req_stage,
                          "placementType": req_placement,
                          "TGS": TGS
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fields= response.getReturnValue();
                component.set("v.requiredFields", fields );
                this.setEditRequiredFields(component, event, helper);
            }
        });
        $A.enqueueAction(action);	
    },
    setEditRequiredFields: function(component, event, helper) {
        var requiredFields = component.get("v.requiredFields");
        var createEnterpriseReqCmp = component.get("v.parent");        
        if(typeof requiredFields !== "undefined") {            
            for (var i=0; i<requiredFields.length; i++) {            
                var element = document.getElementById( requiredFields[i].label_Id__c );            
                if(  !($A.util.isUndefined( element ) || $A.util.isEmpty( element )) ){
                    /* Adding asterisk to all the requited field labels */
                    if(component.get("v.firstload") == false ){
                        element.classList.add("required-field");    
                    }else{
                        component.set("v.firstload", false );
                    }                
                }            
            }
        }     
    },
    /*getAllReqFields : function(component, event, helper){
		var req_stage;
        var req_placement;
        if( $A.util.isUndefined(component.get("v.stage")) || $A.util.isEmpty( component.get("v.stage") ) ) {
            req_stage = 'Draft';
        }else{
            req_stage = component.get("v.stage");
        }
        if( $A.util.isUndefined(component.get("v.placementType")) || $A.util.isEmpty( component.get("v.placementType") ) ) {
            req_placement = 'Contract';
        }else{
            req_placement = component.get("v.placementType");
        }
        var action = component.get("c.getProgressBarDetails");
        action.setParams({"stage": req_stage,
                          "placementType": req_placement
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
				if (state === "SUCCESS") {
					var fields= response.getReturnValue();
					component.set("v.requiredFields", fields );
					var isvalid = true;
					//var requiredFields = component.get("v.requiredFields");
					var createEnterpriseReqCmp = component.get("v.parent");
					var field_api, error_var, field_value, error_count=0;
					var cmptype = component.get("v.cmpType");
					for (var i=0; i<fields.length; i++) {
						if(cmptype == 'Create'){
							field_api =  fields[i].field_api__c;    
						}else if(cmptype == 'Edit'){
							field_api =  fields[i].aura_id__c; 
							if(field_api == 'locationId'){
								field_api =  fields[i].field_api__c; 
							}else if(field_api == 'jobtitleid'){
								field_api =  fields[i].Field_API_Edit__c;
							}else if(field_api == 'crmSkillV'){
								field_api =  fields[i].Field_API_Edit__c;
							}
						}
						error_var = fields[i].error_var__c;
						console.log('--field_api--'+field_api);
						if(cmptype == 'Create'){
							if(field_api == 'presetLocation'){
								//field_value = createEnterpriseReqCmp.get("v."+field_api);
								field_value = createEnterpriseReqCmp.get("v.locationProgress");                    
							}else{
								field_value = createEnterpriseReqCmp.get("v."+field_api);
							}                
						}else if(cmptype == 'Edit'){
							if(field_api == 'presetLocation'){
								//field_value = createEnterpriseReqCmp.get("v."+field_api);
								field_value = createEnterpriseReqCmp.get("v.locationProgress");                    
							}else if(field_api == 'jobtitle[0].name'){
								field_value = createEnterpriseReqCmp.get("v."+field_api);                    
							}else if(field_api == 'topSkills'){
								field_value = createEnterpriseReqCmp.get("v."+field_api);                    
							}else{
								field_value = createEnterpriseReqCmp.find(field_api).get("v.value");  
							}
						}
						console.log('--field_value--'+field_value); 
						if( $A.util.isUndefined( field_value ) || $A.util.isEmpty( field_value ) || field_value == '--None--' ){
							if(isvalid == true && ( fields[i].label_Id__c != null || fields[i].label_Id__c!="" || fields[i].label_Id__c!= undefined)){
								console.log('error - focus - field = ' + fields[i].label_Id__c );
								createEnterpriseReqCmp.set("v.focus_id", fields[i].label_Id__c);
							}
							isvalid = false;
							//createEnterpriseReqCmp.set("v."+error_var, fields[i].Name + " is required.");
							createEnterpriseReqCmp.set("v."+error_var, fields[i].Full_Name__c + " is required.");
							//alert( error_var +  ' ----> ' + fields[i].Name + " cannot be blank.");
							error_count++;
						}else{
							createEnterpriseReqCmp.set("v."+error_var, "");
						}
					}
                    //component.set("v.isvalid",isvalid);
                    //component.set("v.error_count",error_count);
					return {'isvalid': isvalid , 'error_count':error_count};
				}
			});
        $A.enqueueAction(action);        
    }
    getAllReqFields : function(component, event, helper){
		var req_stage;
        var req_placement;
        if( $A.util.isUndefined(component.get("v.stage")) || $A.util.isEmpty( component.get("v.stage") ) ) {
            req_stage = 'Draft';
        }else{
            req_stage = component.get("v.stage");
        }
        if( $A.util.isUndefined(component.get("v.placementType")) || $A.util.isEmpty( component.get("v.placementType") ) ) {
            req_placement = 'Contract';
        }else{
            req_placement = component.get("v.placementType");
        }
        var action = component.get("c.getProgressBarDetails");
        action.setParams({"stage": req_stage,
                          "placementType": req_placement
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fields= response.getReturnValue();
                component.set("v.requiredFields", fields );
                this.validateFields(component, event, helper);
            }
        });
        $A.enqueueAction(action);        
    },
    validateFields : function(component, event, helper) {        
        var isvalid = true;
        var requiredFields = component.get("v.requiredFields");
        var createEnterpriseReqCmp = component.get("v.parent");
        var field_api, error_var, field_value, error_count=0;
        var cmptype = component.get("v.cmpType");
        for (var i=0; i<requiredFields.length; i++) {
            if(cmptype == 'Create'){
            	field_api =  requiredFields[i].field_api__c;    
            }else if(cmptype == 'Edit'){
                field_api =  requiredFields[i].aura_id__c; 
                if(field_api == 'locationId'){
                    field_api =  requiredFields[i].field_api__c; 
                }
            }
            error_var = requiredFields[i].error_var__c;
            console.log('--field_api--'+field_api);
            if(cmptype == 'Create'){
                field_value = createEnterpriseReqCmp.get("v."+field_api);
            }else if(cmptype == 'Edit'){
                if(field_api == 'presetLocation'){
                    field_value = createEnterpriseReqCmp.get("v."+field_api);
                }else{
                    field_value = createEnterpriseReqCmp.find(field_api).get("v.value");  
                }
            }
            console.log('--field_value--'+field_value); 
            if( $A.util.isUndefined( field_value ) || $A.util.isEmpty( field_value ) || field_value == '--None--' ){
                if(isvalid == true && ( requiredFields[i].label_Id__c != null || requiredFields[i].label_Id__c!="" || requiredFields[i].label_Id__c!= undefined)){
                    console.log('error - focus - field = ' + requiredFields[i].label_Id__c );
                    createEnterpriseReqCmp.set("v.focus_id", requiredFields[i].label_Id__c);
                }
                isvalid = false;
           		//createEnterpriseReqCmp.set("v."+error_var, requiredFields[i].Name + " is required.");
				createEnterpriseReqCmp.set("v."+error_var, requiredFields[i].Full_Name__c + " is required.");
                //alert( error_var +  ' ----> ' + requiredFields[i].Name + " cannot be blank.");
                error_count++;
            }else{
                createEnterpriseReqCmp.set("v."+error_var, "");
            }
        }
        
        return {'isvalid': isvalid , 'error_count':error_count};
    }
    */
})