({
	doInit : function(component, event, helper) {
        component.set("v.displaySkill",'none');
        component.set("v.enteredSkills",'lkupskill');
   },
    handleEditClick : function(component, event, helper){
    },
    handleCancelClick :function(component, event, helper){
        component.set("v.isSkillsPresent", "true");
        component.set("v.isSkillsSave","false");
                               
    },
    handleSaveClick : function(component, event, helper){
       
    },
    changeHandler1: function(component, event, helper) {
        
    // var skillVal=component.find("eskillInput").get("v.value");
       
    } ,
     changeHandler: function(component, event, helper) {
       
        console.log('IN changeHandler');
        var globalLovId=event.getParams().value;
         
        var tempRec = component.find("recordLoader");
        tempRec.set("v.recordId",globalLovId[0]);
        tempRec.reloadRecord();
        
    } ,
   enterHandler: function(component, event, helper) {
     var tempval=component.find("eskillLookup");

     component.set("v.enteredSkills",'none');
     component.set("v.displayAdd",'fillskill');
    },
   enterHandler1: function(component, event, helper) {
     var tempval=component.find("eskillLookup");
	 component.set("v.enteredSkills",'lkupskill');
     component.set("v.displayAdd",'none');	
    }
   , 
   recordLoadedEdit :function(component, event, helper) {
    
    var skillsT;
    if(typeof event.getParams().recordUi.record.fields.EnterpriseReqSkills__c!='undefined' && event.getParams().recordUi.record.fields.EnterpriseReqSkills__c!=undefined)
    skillsT=event.getParams().recordUi.record.fields.EnterpriseReqSkills__c.value;
    var loadOnce=component.get("v.loadOnce");
    var uiSkillList=component.get("v.skills");
    
    
    
        if(typeof skillsT!='undefined' && skillsT!=null && skillsT!='[]' && (uiSkillList.length==0 )){
           var skillsList=JSON.parse(skillsT);
           var dbList=[];
            if( typeof skillsList!='undefined' && skillsList != null ){
               var i;
                for(i=0;i<skillsList.length;i++){
                    var dbobj=skillsList[i];
                    var skillObj = {"name":dbobj.name, "favorite":dbobj.favorite,"index":i};
                    dbList.push(skillObj);
                }
                 component.set("v.skills",dbList);
            }        
           
        }
    
           
   },
     onSubmit : function(component, event, helper) {
        event.preventDefault();
         var eventFields = event.getParam("fields");
         var skillVal=component.find("eskillInput")==null?component.find("eskillInput1").get("v.value"):component.find("eskillInput").get("v.value");
         if(skillVal!=null && skillVal!=undefined && typeof skillVal!='undefined'){
             var indexVl;
             var skillsList=component.get("v.skills");
             if(skillsList == null || typeof skillsList=='undefined'){
                    skillsList=[];
                    indexVl=0;
             }else{
                 indexVl=skillsList.length+1;
             }
             if(skillVal!='' && skillVal!=null && skillVal!=undefined){
              var skillObj = {"name":skillVal, "favorite":false,"index":indexVl};
              skillsList.push(skillObj);
             }
             component.set("v.skills",skillsList);
        }
        component.set("v.displayAdd",'none');
        component.set("v.displaySkill",'none');
        component.set("v.enteredSkills",'lkupskill');
       
        var embedded=component.find('embeddedSkill');
        if(!embedded){
            
            var vsmForm=component.find('skillForm');
            helper.prepareSkills(component, event);
            event.preventDefault();
            var finalSkillList=component.get("v.finalskillList");
            var jsonSkills=JSON.stringify(finalSkillList);
            eventFields.EnterpriseReqSkills__c=jsonSkills;
            eventFields.LDS_VMS__c=true;
            event.setParam("fields", eventFields);
            vsmForm.submit(eventFields);
      
        }
        
     },
    onFavourite : function(component, event, helper) {
        
        console.log('IN onFavourite');
        var ctarget = event.currentTarget;
    	var pillId = ctarget.id;
        var skillsList=component.get("v.skills");
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==pillId){
              skillObj.favorite=!skillObj.favorite;
        	}
  			skillsList[i]=skillObj;
       }
        component.set("v.skills",skillsList);
        event.preventDefault();  
     },
     removePill: function (component, event, helper) {
        console.log('IN changeHandler');
        var skillIndex=event.getSource().get("v.name");
        var skillsList=component.get("v.skills");
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==skillIndex){
              skillsList.splice(i,1);
        	}
  	    }
        component.set("v.skills",skillsList);
        event.preventDefault();
        
    },
    recordUpdated : function(component, event, helper) {
	
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") { /* handle error; do this first! */ }
        else if (changeType === "LOADED") { 
           var sRec=component.get("v.simpleRecord");
           var indexVl=0;
           var skillsList=component.get("v.skills");
             if(skillsList == null || typeof skillsList=='undefined'){
                    skillsList=[];
                    indexVl=0;
             }else{
                 indexVl=skillsList.length+1;
             }
            var skillObj = {"name":sRec.Name,"favorite":false,"index":indexVl};
            skillsList.push(skillObj);
            component.set("v.skills",Array.from(skillsList));   
            
           var flag=component.get("v.enteredSkills");
            component.set("v.enteredSkills",component.get("v.enteredSkills")=='refreshlkupskill'?'lkupskill':'refreshlkupskill');     
	    }
       
    },
    handleError : function(cmp,event,helper){
        var params = event.getParams();
        console.log('Params ---->'+params);
    }     
})