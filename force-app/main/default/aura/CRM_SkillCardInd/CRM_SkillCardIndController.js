({
	doInit : function(component, event, helper) {
        var fcmp=component.find('recordLoader');
        fcmp.set("v.recordId", component.get("v.recordId"));
        fcmp.reloadRecord();
        component.set("v.displaySkill",true);
        component.set("v.duplicateSkillError","");
   },
    onFavourite : function(component, event, helper) {
        
        console.log('IN onFavourite');
        var ctarget = event.currentTarget;
    	var pillId = ctarget.id;
        var skillsList=component.get("v.skills");
        let skillsMap=component.get("v.skillsMap");
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==pillId){
              skillObj.favorite=!skillObj.favorite;
        	}
  			skillsList[i]=skillObj;
            skillsMap[skillObj.name.toUpperCase()]=skillObj;
       }
        component.set("v.skills",skillsList);
        component.set("v.skillsMap",skillsMap);
        event.preventDefault();  
     },
     removePill: function (component, event, helper) {
        console.log('IN changeHandler');
        var skillIndex=event.getSource().get("v.name");
        var skillsList=component.get("v.skills");
        let skillsMap=component.get("v.skillsMap");
        var i;
        
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==skillIndex){
             if(skillsMap.hasOwnProperty(skillObj.name.toUpperCase())==true){
                delete skillsMap[skillObj.name.toUpperCase()];
                console.log('SkillsMap after delete---->'+JSON.stringify(skillsMap));
                skillsList.splice(i,1);
                let txObj={"name":skillObj.name, "favorite":false,"index":-1,"suggestedSkill":true,"action":'remove'};
                component.set("v.txSkill",txObj);
              }
        	}
  	    }
        component.set("v.skills",skillsList);
        component.set("v.skillsMap",skillsMap);
        event.preventDefault();
        
    },
    recordUpdated : function(component, event, helper) {
		var skillsMap ={};
	    component.set("v.duplicateSkillError","");
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") { /* handle error; do this first! */ }
        else if (changeType === "LOADED" || changeType === "CHANGED") {
          
           var sRec=component.get("v.oppRecord");
           var indexVl=0;
          var skillsT=sRec.EnterpriseReqSkills__c;
    
        if(typeof skillsT!='undefined' && skillsT!=null && skillsT!='[]' ){
           var skillsList=JSON.parse(skillsT);
           var dbList=[];
            if( typeof skillsList!='undefined' && skillsList != null ){
               var i;
                for(i=0;i<skillsList.length;i++){
                    var dbobj=skillsList[i];
                    var skillObj = {"name":dbobj.name, "favorite":dbobj.favorite,"index":i};
                    dbList.push(skillObj);
					//populate skills Map
					if(skillsMap.hasOwnProperty(skillsList[i].name.toUpperCase())==false){
						  let skillObj = {"name":skillsList[i].name,"favorite":false,"index":i};
						  //skillsList.push(skillObj);
						  skillsMap[skillsList[i].name.toUpperCase()]=skillObj;
              			  component.set("v.duplicateSkillError","");
						  console.log("skills:"+JSON.stringify(skillObj));
					 }
                }
				  component.set("v.skillsMap",skillsMap);
                 component.set("v.skills",dbList);
				
            }        
           
        } else {
			component.set("v.skills",[]);
		}
        //$A.get('e.force:refreshView').fire();
       
			console.log("Record is loaded successfully.");
            }
	   
    },
    keyPressHandler: function(component, event, helper) {
         var glov=component.get("v.glov");
         if(glov!=null && glov.Text_Value__c!=undefined && glov.Text_Value__c!=""){
             //if(component.get("v.createPill")){
         		console.log('On click Value selectd--->'+glov.Text_Value__c);
                 var indexVl=0;
                 var skillsList=component.get("v.skills");
                 let skillsMap=component.get("v.skillsMap");
                 if(skillsList == null || typeof skillsList=='undefined'){
                        skillsList=[];
                        indexVl=0;
                 }else{
                     //indexVl=skillsList.length+1;
                     let mapLength=Object.keys(skillsMap).length;
                     let valuesArray=Object.values(skillsMap);
                     if(valuesArray.length>0){
                        indexVl=valuesArray[valuesArray.length-1].index+1; 
                     }
                 }
             
              	if(skillsMap.hasOwnProperty(glov.Text_Value__c.toUpperCase())==false){	
                  let skillName=glov.Text_Value__c.charAt(0).toUpperCase() + glov.Text_Value__c.slice(1);
                  let skillObj = {"name":skillName,"favorite":false,"index":indexVl};
				   skillsMap[skillObj.name.toUpperCase()]=skillObj;
                  component.set("v.duplicateSkillError","");
                  skillsList.push(skillObj);
                 
                }else{
                    component.set("v.duplicateSkillError","Skill already exists");
                    setTimeout(function(){ component.set("v.duplicateSkillError",""); }, 2000);
                }    
                 component.set("v.skills",Array.from(skillsList));
                 component.set("v.skillsMap",skillsMap); 
                 component.set("v.createPill",false);
                 component.set("v.glov.Text_Value__c",null);
         }
     },
    changeTxSkillHandler: function(component,event,helper){
	
	 	let txObj=component.get("v.txSkill");
        console.log('TxSkillsHandler----->'+txObj);
        if(txObj!=null && txObj!=undefined && txObj.name!='' && txObj.action=='add'){
                 var indexVl=0;
                 var skillsList=component.get("v.skills");
                 let skillsMap=component.get("v.skillsMap");
                 if(skillsList == null || typeof skillsList=='undefined'){
                        skillsList=[];
                        indexVl=0;
                 }else{
                     //indexVl=skillsList.length+1;
                     let mapLength=Object.keys(skillsMap).length;
                     let valuesArray=Object.values(skillsMap);
                     if(valuesArray.length>0){
                        indexVl=valuesArray[valuesArray.length-1].index+1; 
                     }
                 }
                 if(skillsMap.hasOwnProperty(txObj.name.toUpperCase())==false){
                    let skillObj = {"name":txObj.name,"favorite":false,"index":indexVl};
                    skillsList.push(skillObj);
                    skillsMap[txObj.name.toUpperCase()]=skillObj;
                    component.set("v.duplicateSkillError","");
                  }else{
                    component.set("v.duplicateSkillError","Skill already exists");
                  }
				  component.set("v.skillsMap",skillsMap);
                  component.set("v.skills",Array.from(skillsList));
                       
                 
        }   	        
  
    },
    changeSkillsHandler: function(component,event,helper){
        let skillsList=component.get("v.skills");
        let skillsMap=component.get("v.skillsMap");
        let mapLength=Object.keys(skillsMap).length;
        if(skillsList!=null && skillsList.length>0 && skillsList.length!=mapLength){		
       	  var i;
          for ( i = 0; i < skillsList.length; i++) { 
              if(skillsMap.hasOwnProperty(skillsList[i].name.toUpperCase())==false){
                  let skillObj = {"name":skillsList[i].name,"favorite":false,"index":i};
                  skillsList.push(skillObj);
                  skillsMap[skillsList[i].name.toUpperCase()]=skillObj;
              	  component.set("v.duplicateSkillError","");
              }else{
                  component.set("v.duplicateSkillError","Skill already exists");
              }  
          }
           component.set("v.skillsMap",skillsMap);       
		   //console.log("skills Map:"+JSON.stringify(component.get("v.skillsMap")));
        }
       //} 
    },
	refreshSkillData: function(cmp,event,helper) {
		var rcmp=cmp.find('recordLoader');
        rcmp.set("v.recordId", cmp.get("v.recordId"));
        rcmp.reloadRecord(true);
	}  
})