({
    doInit : function(component, event, helper) {
        
    },
    
    
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
           // record is loaded (render other component which needs record data value)
            console.log("Record is loaded successfully.");
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },

    
    createRecord: function(component, event, helper) {
       
       $A.get("e.force:closeQuickAction").fire();
      	var createRecordEvent = $A.get("e.force:createRecord");
       	var oppid=component.get("v.recordId");
      	  
        var  accid=component.get("v.simpleNewOpp.AccountId"); 
        var startdate= component.get("v.simpleNewOpp.Start_Date__c");
        
        console.log(oppid);
        console.log(accid);
        console.log(startdate);
        
       	
   		
      //helper.getOfficeDetails(component, event, helper); 
      
	  
	  var selectedRadioOption = component.find("r3");
	  var isTgsOpp = selectedRadioOption.get("v.checked");
       var recType = "012U0000000DWpK";
       if(isTgsOpp === true){
           recType = "012U0000000DWpL";
       }
	  createRecordEvent.setParams({
         "entityApiName": "Solution_Development__c", // using account standard object for this sample
          "recordTypeId": recType, // Optionally Specify Record Type Id
          
          "defaultFieldValues": {
                'Account__c' : accid,
              'Opportunity__c' : oppid,
              'Start_Date__c': startdate
              
              
          }
          
      });
      createRecordEvent.fire();
      component.destroy(); 
       
   }
})