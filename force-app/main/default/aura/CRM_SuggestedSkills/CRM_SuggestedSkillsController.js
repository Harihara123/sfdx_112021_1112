({
    initSuggestedSkill : function(component, event, helper){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        let isCloneReq=component.get("v.isCloneReq");
        helper.fetchOpcoMappings(component,event);
        /*if(isCloneReq){
                helper.handleJobTitleHandler(component, event, helper);
            }*/
        //helper.fetchOpcoMappings(component,event);
    },
    handleAddSkillHandler : function(component, event, helper){
      helper.addToSkills(component, event, helper);
    },
    handleFavoriteHandler : function(component, event, helper){
      console.log('Favorite Handler ');
    },
    handleCloseHandler : function(component, event, helper){
      console.log('Close Handler ');
      var skillsList = [];
      component.set("v.suggestedSkills",skillsList);
    },
    handleJobTitleHandler : function(component, event, helper){        
        helper.handleJobTitleHandler(component, event, helper);        
    },
    
    handlenormalizedDataHandler : function(component, event, helper){
 
    },
    txSkillsHandler :function(component,event,helper){
        let initSkills = component.get('v.initialSuggestedSkills');
        let selectedSkills = component.get('v.skills');
        let txObj = component.get("v.txSkill");

        if(typeof txObj != 'undefined' && txObj != null && txObj.suggestedSkill == true && txObj.action == 'remove'){
            // Alex Thomas - Updated everything in here to reflect proper positioning when a suggested skill is removed

            // Put selected skills (after remove) into a list of names
			let selectedSkillsList = [];
            selectedSkills.forEach(function(selectedSkill) {
				selectedSkillsList.push(selectedSkill.name.toLowerCase());
            });

            // Create list of skills not including selected skills
            let newSuggestedSkills = [];
            for (let i = 0; i < initSkills.length; i++) {
                if (!selectedSkillsList.includes(initSkills[i].toLowerCase())) {
                    var newSkill = {"name":initSkills[i],"favorite":false,"index":i,"suggestedSkill":true};
                    newSuggestedSkills.push(newSkill);
                }
            };
            
            component.set("v.suggestedSkills", newSuggestedSkills);
        }
    }
})