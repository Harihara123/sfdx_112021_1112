({
	addToSkills: function(component, event, helper) {
        var skillIndex=event.getSource().get("v.name");       
        var finalSkillList=component.get('v.suggestedSkills');
        let newFinalSkillList=[];
        let txObj;
        //var skillName;
        //var dupeCheck=false;
		if(typeof finalSkillList!='undefined' && finalSkillList!=null){
          var i;
          for( i = 0; i < finalSkillList.length; i++) {
              var skillObj=finalSkillList[i];
              if(skillObj.index==skillIndex){
                  //skillName=skillObj.name;
                  //dupeCheck=this.dupeCheck(component, event, helper, skillName,dupeCheck);
                  //if(dupeCheck==false){
                  finalSkillList.splice(i,1);
				  //Adding w.r.t S-168146 'favorite dynamically'
                  txObj={"name":skillObj.name, "favorite":component.get('v.suggestedTopSkills'),"index":skillObj.index,"suggestedSkill":true,"action":'add'};              
                  //}
                  /*else{
                      this.showToastError(component, event, skillName);
                  }*/
               }              
          }
          //Adding w.r.t S-168146
          //To make array and index of suggested skills same
          //after splicing suggested skill
          //if(dupeCheck==false){
          for( i = 0; i < finalSkillList.length; i++) {
          	 if(finalSkillList[i].index !== i){
                  finalSkillList[i].index=i;
             }
          } 
          //}
          component.set('v.suggestedSkills',finalSkillList);
          component.set("v.txSkill",txObj);                      
        }       
    },
    
    dupeCheck: function(component, event, helper, skillName,dupeCheck){
        var addedSkills=component.get("v.skills");
        for(var i=0;i<addedSkills.length;i++){
            if(addedSkills[i].name==skillName)
                dupeCheck=true;
            return dupeCheck;
        }
    },
    showToastError : function(component,event,skillName) {
        var toastEvent = $A.get("e.force:showToast");
	    toastEvent.setParams({
			message: skillName,
	        type:'error',
	        //title:'Duplicate Value'
			title:$A.get("$Label.c.ATS_Duplicate_Value")
	    });
	    toastEvent.fire(); 
	},
    
    addSuggestedSkill: function(component, event, helper,skill){
	    var finalSkillList=component.set('v.suggestedSkills');
		if(typeof skillsT!='undefined' && skillsT!=null){
            var skillObj=skillsList[i];
            var newSkill = {"name":skill, "favorite":false,"index":skillsList.length+1};
            finalSkillList.push(newSkill);
            component.set("v.suggestedSkills",finalSkillList);
       }
   },
   removeSuggestedSkill: function(component, event, helper,skillIndex){
        var finalSkillList=component.set('v.suggestedSkills');
        var i;
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            if(skillObj.index==skillIndex){
            	finalSkillList.splice(i,1);    
            }
        }
        component.set("v.suggestedSkills",finalSkillList);
   },
    prepareSuggestedSkills: function(component,event,skillList,initialSkillsList){
        var finalSkillList=[];
        var i;
        var skillsList=skillList.slice(0,50);
        for ( i = 0; i < skillsList.length; i++) { 
            var skillObj=skillsList[i];
            let skillName=skillObj.charAt(0).toUpperCase() + skillObj.slice(1);
            var newSkill = {"name":skillName,"favorite":false,"index":i,"suggestedSkill":true};
            finalSkillList.push(newSkill);
        }
        component.set("v.suggestedSkills", finalSkillList);
        component.set("v.initialSuggestedSkills", initialSkillsList);
    },
    handleJobTitleHandler : function(component, event, helper){        
        
        var oppOpco = component.get("v.selectedOpCo");
        //End of Adding w.r.t D-12783
        if(component.get("v.opcoMapping")) {
			let opcoMapping = component.get("v.opcoMapping");
			let opco = '';
			for(let i=0; i<opcoMapping.length;i++) {
				if(opcoMapping[i].MasterLabel === oppOpco) {
					opco = opcoMapping[i].Translated_Value__c;
					this.fetchSuggestedSkills(component,event,helper,opco);	
				}
			}
			if(opco === ''){
				opco = oppOpco;
				this.fetchSuggestedSkills(component,event,helper,opco);	
			}
		}
        
		//fetch translated opco
		/*var fetchOpcoMapping = component.get("c.fetchOpcoMapping");
		fetchOpcoMapping.setParams({'opcoText':oppOpco});

		fetchOpcoMapping.setCallback(this, function(response) {
			let state = response.getState();            
            if (state === "SUCCESS") {
				let respObj = response.getReturnValue();
				if(respObj != null) {
					let respParsed = JSON.parse(JSON.stringify(respObj));	
					console.log('respParsed:'+respParsed);
					let opco = respParsed.Translated_Value__c;
					this.fetchSuggestedSkills(component,event,helper,opco);				
				}else {
					this.fetchSuggestedSkills(component,event,helper,oppOpco);	
				}
			} else {
				this.fetchSuggestedSkills(component,event,helper,oppOpco);	
			}
		});

		$A.enqueueAction(fetchOpcoMapping); */

               
    },
    fetchSuggestedSkills : function(component, event, helper,opco){
		var title=component.get("v.suggestedjobTitle");
		if(title !== '') {
			console.log('@@Title'+  title)
			var skillsList = component.get("v.skills");
			 console.log('@@skillsList'+   skillsList)
			var skillsCSV = '';       
		
			//Adding w.r.t D-12783
			skillsCSV=this.excludedSuggestedSkills(component, event, helper);
            let selectedSkillsList = skillsCSV.toLowerCase().split(',');

			//var body=encodeURI("?terms="+title+"&domain="+opco+"&max=10&appId=CRM_SkillSearch&exclude="+skillsCSV);  
			var body=encodeURI("?terms=")+encodeURIComponent(title)+encodeURI("&domain="+opco+"&max=50&appId=CRM_SkillSearch");
        
			console.log('SK params '+body);      
			// var body=  "{\"query\":{\"term\": {\"titlevector\": \""+title.toLowerCase()+"\"}},\"aggs\":{\"skill_counts\":{\"terms\":{\"script\":{\"inline\": \"doc['skillsvector'].values\"},\"size\": 100}}}}";
			var action = component.get("c.Skills"); 
			action.setParams({
				'httpRequestType': "GET",
				'requestBody' : body
			});
			action.setCallback(this, function(response) {
				console.log('---'+ response);
				var state = response.getState();
				if (state === "SUCCESS") {
					var storeResponse = response.getReturnValue();
					var responseParsed = JSON.parse(storeResponse);
					var responseWithErrors=responseParsed.hasOwnProperty("errors");
                
					var skillsList = [];
                    var skillsMapList = [];

                    var initialSkillsList = [];
                    var initialSkillsMapList = [];

					if(responseWithErrors==false){
						for(var key in responseParsed){
							responseParsed[key].forEach(function(item, index) {
                                // Push all to initial list
                                initialSkillsMapList.push(item);

                                // Push unselected to other list
                                if (!selectedSkillsList.includes(item.term.toLowerCase()))
                                    skillsMapList.push(item);
								//skillsList.push(item.term);
							});
                		}
					}
                
					let finalist = skillsMapList.sort(this.GetSortOrder("similarity"));    
					finalist.forEach(function(item, index) {
                        skillsList.push(item.term)
                    });
                    
					let finalInitList = initialSkillsMapList.sort(this.GetSortOrder("similarity"));    
					finalInitList.forEach(function(item, index) {
                        initialSkillsList.push(item.term)
                    });
                    
					//Remove duplicates
					let uniqueSkills = [...new Set(skillsList)];
					let uniqueInitSkills = [...new Set(initialSkillsList)];
				       
					if(uniqueSkills.length>0){
						this.prepareSuggestedSkills(component,event,uniqueSkills,uniqueInitSkills);
					} else {
						component.set("v.suggestedSkills",[]);
                        component.set("v.initialSuggestedSkills",[]);
					}
				}
			});
			$A.enqueueAction(action);
		} else {
			component.set("v.suggestedSkills",[]);
			component.set("v.initialSuggestedSkills",[]);
		} 
	},
    //Adding w.r.t D-12783
    excludedSuggestedSkills : function(component, event, helper){        
        var excludedSkills='';
        var skillCSV='';       
		if(component.get("v.isCloneReq") == "true" && component.get("v.skills").length == 0){
		//if(component.get("v.isCloneReq") && component.get("v.skills").length == 0){
            var reqSkills=component.get("v.req.EnterpriseReqSkills__c");
            if(typeof reqSkills!='undefined' && reqSkills!=null){
                excludedSkills=JSON.parse(reqSkills);
            }            
        }else{
            excludedSkills=component.get("v.skills");            
        }
        for (var i = 0; i < excludedSkills.length; i++) {
        	skillCSV += excludedSkills[i].name+',';
        }
        skillCSV = skillCSV.slice(0,-1);
        var test=skillCSV;
        return skillCSV;
    },
    //End of Adding w.r.t D-12783
    fetchOpcoMappings:function(cmp,event) {
		
        var fetchOpcoMapping = cmp.get("c.fetchOpcoMappings");
        fetchOpcoMapping.setCallback(this, function(response) {
            let state = response.getState();            
            if (state === "SUCCESS") {
                let respObj = response.getReturnValue();
                if(respObj != null) {
                    console.log("respObj:"+JSON.stringify(respObj));
                    cmp.set("v.opcoMapping",respObj); 
                    let isCloneReq=cmp.get("v.isCloneReq");
                    if(isCloneReq){
                        this.handleJobTitleHandler(cmp, event);
                    }    
                }
            }
     	});
        $A.enqueueAction(fetchOpcoMapping);
     },
	 GetSortOrder:function(prop) {    
		return function(a, b) {    
			if (a[prop] > b[prop]) {    
				return -1;    
			} else if (a[prop] < b[prop]) {    
				return 1;    
			}    
			return 0;    
		}    
	}  
})