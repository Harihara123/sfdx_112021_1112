({
	doInit : function(component, event, helper){
        
        component.set("v.inProcess", true );
        var recId = component.get("v.recordId");
        var action = component.get("c.syncToDrz");
		action.setParams({"opportunityId":  recId});   
        
        action.setCallback(this,function(response) {
            
            component.set("v.inProcess", false );
            $A.get("e.force:closeQuickAction").fire();
            var message;
            if(response.getReturnValue()){
                message = 'The Req has been successfully synced to DRZ';
                helper.showToast(message, 'Success');  
            }else{
                message = 'The Req failed to sync with DRZ, Please contact your Administrator.';
            	helper.showToast(message, 'Error');      
            }
            
            		
		});
        
        $A.enqueueAction(action);
               
	},
    
    
})