({
    
    showToast : function(message, status ) {
        //status value has to be Error or Success
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Sync to DRZ',
            message: message,
            type: status
        });
        toastEvent.fire();
    }
})