({
	initializeTGS : function(component, event, helper) {
		var action = component.get("c.getEnterpriseReqPicklists");
        var UrlQuery = window.location.href;
        
        var myPageRef = component.get("v.pageReference");
        var tgsId ='';
		var oppId;
        
         if(myPageRef){
             oppId  = myPageRef.state.c__recId;
			 tgsId = myPageRef.state.c__tgsOppId;
             // Code freeze fix
             var paSubStatus = myPageRef.state.c__proactiveSubmittalStatus;
             if(paSubStatus!=undefined){
                 component.set("v.proactiveSubmittalStatus",paSubStatus);
             } // Code freeze fix
             if(oppId !== null && oppId !== undefined){
				component.set("v.recordId", oppId);
             }
			 if(myPageRef.state.c__source !== null && myPageRef.state.c__source !== undefined) {
				component.set("v.source", myPageRef.state.c__source);
			 }
         }
        var recrdId = component.get("v.recordId");
        action.setParams({"recId":recrdId,"tgsOppId":tgsId});
        action.setCallback(this, function(response) {
            var data = response;            
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                component.set("v.account",model.act);
                component.set("v.contact",model.cnt);
                component.set("v.organization",model.office);
                component.set("v.sobjType",model.sobjType);                
                //if(model.opp.OpCo__c == 'TEKsystems, Inc.'){
                if(model.opco == 'TEKsystems, Inc.' || model.opp.OpCo__c == 'TEKsystems, Inc.'){
                    var TGSReq;
                    var TGSPracEng;
                    var TGSIntTravel;
                    var TGSInternalHire;
                    var TGSEmpAlign;
                    var TGSFnlDecMkr;
                    var TGSLocation;
                    var ReqDisposition;
                    if(model.opp != null && model.opp!= undefined && component.get("v.isCreateFlag")==false){
                        TGSReq = model.opp.Req_TGS_Requirement__c!= undefined && model.opp.Req_TGS_Requirement__c != null ? model.opp.Req_TGS_Requirement__c : '';
                        TGSPracEng = model.opp.Practice_Engagement__c!= undefined && model.opp.Practice_Engagement__c != null ? model.opp.Practice_Engagement__c : '';     
                        component.set("v.req.Practice_Engagement__c",TGSPracEng);
                        TGSIntTravel = model.opp.Is_International_Travel_Required__c!= undefined && model.opp.Is_International_Travel_Required__c != null ? model.opp.Is_International_Travel_Required__c : ''; 
                        component.set("v.req.Is_International_Travel_Required__c",TGSIntTravel);
                        TGSInternalHire = model.opp.Internal_Hire__c!= undefined && model.opp.Internal_Hire__c != null ? model.opp.Internal_Hire__c : ''; 
                        component.set("v.req.Internal_Hire__c",TGSInternalHire);
                        TGSEmpAlign = model.opp.Employment_Alignment__c!= undefined && model.opp.Employment_Alignment__c != null ? model.opp.Employment_Alignment__c : ''; 
                        component.set("v.req.Employment_Alignment__c",TGSEmpAlign);
                        TGSFnlDecMkr = model.opp.Final_Decision_Maker__c!= undefined && model.opp.Final_Decision_Maker__c != null ? model.opp.Final_Decision_Maker__c : ''; 
                        component.set("v.req.Final_Decision_Maker__c",TGSFnlDecMkr);
                        TGSLocation = model.opp.GlobalServices_Location__c!= undefined && model.opp.GlobalServices_Location__c != null ? model.opp.GlobalServices_Location__c : '';
                        component.set("v.req.GlobalServices_Location__c",TGSLocation);
                        component.set("v.req.National__c", model.opp.National__c);
                        component.set("v.req.Backfill__c", model.opp.Backfill__c);
                        ReqDisposition = model.opp.Req_Disposition__c!= undefined && model.opp.Req_Disposition__c != null ? model.opp.Req_Disposition__c : '';
                        component.set("v.req.Req_Disposition__c",ReqDisposition);
                        this.setAdditionalTGSInfo(component, event, helper);
                    }else{
                        this.defaultValues(component, event, helper);
                    }
                    
                    /*var IsTGSReqArray = [];
                        var IsTGSMap = model.TGSReqListMappings;                    	
                        IsTGSReqArray.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSReqArray.push({value:IsTGSMap[key], key:key, selected :IsTGSMap[key] ===TGSReq });
                        }
                        component.set("v.isTGSReqList", IsTGSReqArray);*/
                        
                        var IsTGSMap;
                    
                    	var IsTGSPracEng = [];
                        IsTGSMap = model.PracEngListMappings;                    	
                        IsTGSPracEng.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSPracEng.push({value:IsTGSMap[key], key:key, selected :IsTGSMap[key] ===TGSPracEng});
                        }
                        component.set("v.IsTGSPracEngList", IsTGSPracEng);
                        
                        var IsTGSIntTravel = [];
                        IsTGSMap = model.IsIntTravelListMappings;                    	
                        IsTGSIntTravel.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSIntTravel.push({value:IsTGSMap[key], key:key, selected :IsTGSMap[key] ===TGSIntTravel});
                        }
                        component.set("v.IsTGSIntTravelList", IsTGSIntTravel);
                        
                        var IsTGSInternalHire = [];
                        IsTGSMap = model.InternalHireMappings;                    	
                        IsTGSInternalHire.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSInternalHire.push({value:IsTGSMap[key], key:key, selected :IsTGSMap[key] ===TGSInternalHire});
                        }
                        component.set("v.IsTGSInternalHireList", IsTGSInternalHire);
                        
                        var IsTGSEmpAlign = [];
                        IsTGSMap = model.EmploymentAlignmentMappings;                    	
                        IsTGSEmpAlign.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSEmpAlign.push({value:IsTGSMap[key], key:key, selected :IsTGSMap[key] ===TGSEmpAlign});
                        }
                        component.set("v.IsTGSEmpAlignList", IsTGSEmpAlign);
                        
                        var IsTGSFnlDecMkr = [];
                        IsTGSMap = model.FinalDecisionMakerMappings;                    	
                        IsTGSFnlDecMkr.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSFnlDecMkr.push({value:IsTGSMap[key], key:key, selected :IsTGSMap[key] ===TGSFnlDecMkr});
                        }
                        component.set("v.IsTGSFnlDecMkrList", IsTGSFnlDecMkr);
                        
                        var IsTGSLocation = [];
                        IsTGSMap = model.GlobalServiceLocationMappings;                    	
                        IsTGSLocation.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSLocation.push({value:IsTGSMap[key], key:key, selected :IsTGSMap[key] ===TGSLocation});
                        }
                        component.set("v.IsTGSLocationList", IsTGSLocation);
                    
                    	var dispositionArray = [];
						var dispositionMap = model.dispositionMappings;
						dispositionArray.push({value:'--None--', key:'--None--'});  
						for (var key in dispositionMap) {
                            if(key != "Requirement Lost" && key != "Requirement Washed") {
								dispositionArray.push({value:dispositionMap[key], key:key, selected :dispositionMap[key] ===ReqDisposition});                                
							}
                        }
						component.set("v.dispositionList", dispositionArray);
                }
            }
    	});
         $A.enqueueAction(action);
	},
    defaultValues : function(component, event, helper) {
        component.set("v.req.Practice_Engagement__c","--None--");            
        component.set("v.req.GlobalServices_Location__c","--None--");            
        component.set("v.req.Employment_Alignment__c","--None--");            
        component.set("v.req.Final_Decision_Maker__c","--None--");            
        component.set("v.req.Is_International_Travel_Required__c","--None--");            
        component.set("v.req.Internal_Hire__c","--None--");
        //component.find("isBackfillId").set("v.checked",false);
        //component.find("isNationalId").set("v.checked",false);        
        component.set("v.req.Backfill__c",false);
        component.set("v.req.National__c",false);
        component.set("v.req.Req_Disposition__c","--None--");
    },
    
    setAdditionalTGSInfo: function(component, event, helper) {
    		var additionalReqConcatenate='';           
            var NationalCheckbox = component.get("v.req.National__c");
            var nationalCheck = NationalCheckbox ? 'National; ' : '';
            var BackfillCheckbox = component.get("v.req.Backfill__c");
            var backfillCheck = BackfillCheckbox ? 'Backfill; ' : '' ;            
            
            additionalReqConcatenate = (nationalCheck + backfillCheck).trim();            
            if((additionalReqConcatenate.charAt(additionalReqConcatenate.length-1))==";"){
               additionalReqConcatenate=additionalReqConcatenate.substring(0,additionalReqConcatenate.length-1);
            }
            component.set("v.TGSRequirementVar",additionalReqConcatenate);
    }
})