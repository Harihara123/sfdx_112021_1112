({
	afterRender: function (component, helper) {
        this.superAfterRender();
        if (window.location.href.indexOf("view?sv=") > -1) {
            if(component.find("NationalId")) {            
                $A.util.addClass(component.find("NationalId"), 'slds-m-left_medium');
                $A.util.removeClass(component.find("NationalId"), 'slds-m-left_large');            
            }
            if(component.find("BackfillId")) {            
                $A.util.addClass(component.find("BackfillId"), 'slds-m-left_medium');
                $A.util.removeClass(component.find("BackfillId"), 'slds-m-left_large');            
            }
        }
    }
})