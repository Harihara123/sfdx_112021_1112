({
    handleLoad: function(component, event, helper){
        console.log('--recordId--'+component.get("v.recordId"));
    },
    handleSubmit: function(component, event, helper){
        event.preventDefault();
		var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        var tgsOpp = JSON.parse(JSON.stringify(component.get("v.OppList")));
        var tgsCloseOpp = event.getParam("fields");                
        var validate = true;
        var closedReason = component.find("closedReasonId").get("v.value");  
        var closedCategory = component.find("closedReasonCatId").get("v.value");
        var isclosedCategoryEmpty = $A.util.isEmpty(component.find("closedReasonCatId").get("v.value"));
        if(closedReason === "Won" && isclosedCategoryEmpty == false && closedCategory != '--None--' && closedCategory != ''){
            var otherDetailCheck = true;
            component.set("v.btnDisable",true);
            if(closedCategory === "Other"){
                var otherDetails = component.find("closedDetailsId").get("v.value");
                var isotherDetailsEmpty = $A.util.isEmpty(component.find("closedDetailsId").get("v.value"));
                component.set("v.closeReasonDetailError", "");
                if(isotherDetailsEmpty){
                    otherDetailCheck = false;
                    component.set("v.closeReasonDetailError", "Please enter details.");
                    component.set("v.btnDisable",false);
                }else{
                    otherDetailCheck = true;
                }
            }
            if(otherDetailCheck){
                component.set("v.validate",false);
                var p = component.get("v.parent");
                p.validatePostBid();
                var action = component.get("c.stageValidationMessages");
                action.setParams({"opptyNewList": tgsOpp
                                 });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS"){
                        var acc = response.getReturnValue();
                        if(acc.length > 0){ 
                            component.set("v.btnDisable",false);
                            component.set("v.closeReasonError", "");
                            component.set("v.closeReasonCategoryError", "");
                            component.set("v.isOpen",false);
                        }else{
                            tgsCloseOpp.Customer_Signature_Date__c = today;
                            tgsCloseOpp.StageName = closedReason;
							tgsCloseOpp.CloseDate = today;
                            if(closedCategory === "Other")
                                tgsCloseOpp.Closed_Other_Details__c = otherDetails;
								tgsCloseOpp.Apex_Context__c = true;
                            //component.set("v.btnDisable",false);
                            component.find('tgsCloseOppId').submit(tgsCloseOpp);
                        }
                    }else if(state === "ERROR"){
                        var errors = response.getError();
                    }
                });        
                $A.enqueueAction(action); 
            }
        }else{
            component.set("v.validate",true);
        }
        //validate Closed Reason
        if(component.get("v.validate")== true){
            component.set("v.btnDisable",true);
            var validate1 = true;            
            var isclosedReasonEmpty = $A.util.isEmpty(component.find("closedReasonId").get("v.value"));            
            component.set("v.closeReasonError", "");
            if(isclosedReasonEmpty || closedReason === "--None--"){
                validate1 = false;
                component.set("v.closeReasonError", "Please select a value.");
            }
            //Validate Closed Reason Category            
            component.set("v.closeReasonCategoryError", "");
            if(isclosedCategoryEmpty  || closedCategory === "--None--"){
                validate1 = false;
                component.set("v.closeReasonCategoryError", "Please select a value.");
            }
            //validate  Closed other details
            if(closedCategory === "Other"){
                var otherDetails = component.find("closedDetailsId").get("v.value");
                var isotherDetailsEmpty = $A.util.isEmpty(component.find("closedDetailsId").get("v.value"));
                component.set("v.closeReasonDetailError", "");
                if(isotherDetailsEmpty){
                    validate1 = false;
                    component.set("v.closeReasonDetailError", "Please enter details.");
                }else{
                    tgsCloseOpp.Closed_Other_Details__c = otherDetails;
                }
            }   
            var oldProbValue ;
            for(var i=0; i < tgsOpp.length; i++){
                if(typeof(tgsOpp[i].Probability) !="undefined" && tgsOpp[i].Probability !=''){
                    oldProbValue = tgsOpp[i].Probability;
                }
            }
            component.set("v.validate1",validate);
            if(validate1){
                tgsCloseOpp.Probability_new__c = oldProbValue;
				tgsCloseOpp.CloseDate = today;
                tgsCloseOpp.StageName = closedReason;
				tgsCloseOpp.Apex_Context__c = true;
                //component.set("v.btnDisable",false);                
                component.find('tgsCloseOppId').submit(tgsCloseOpp);            
            }else{
                component.set("v.btnDisable",false);
            }
        }
    },
    handleError: function(component, event, helper){
        var params = event.getParams();
        console.log('error reached '+params);
    },
    handleSuccess: function(component, event, helper){
        if(component.get("v.validate1") == true){
            component.set("v.closeReasonError", "");
            component.set("v.closeReasonCategoryError", "");
            component.set("v.closeReasonDetailError", "");
            component.set("v.showClosedOthers", false);
            component.set("v.btnDisable",false);
            component.set("v.isOpen", false);
            $A.get('e.force:refreshView').fire();
        }
    },
    closeModal: function(component, event, helper){
        component.set("v.closeReasonError", "");
        component.set("v.closeReasonCategoryError", "");
        /*component.set("v.closeReasonDetailError", "");
        component.set("v.showClosedOthers", false);*/
        component.set("v.btnDisable",false);
        component.set("v.isOpen", false);
    },
    closeOpportunity : function(component, event, helper){
    },
    showOther : function(component, event, helper){
        var ClosedReasonVal = component.find("closedReasonCatId").get("v.value");
        if(ClosedReasonVal == 'Other'){
            component.set("v.showClosedOthers", true);
        }else{
            component.set("v.showClosedOthers", false);
            component.set("v.closeReasonDetailError", "");
        }
    }
})