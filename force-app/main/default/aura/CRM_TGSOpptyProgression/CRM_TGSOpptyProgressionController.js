({
    doInit : function(component, event, helper){  
         helper.isTGSEMEADirectorGrpMember(component,event,helper);
        console.log(component.get("v.recordId"));
        var eventMsg = event.getParams().message;
        if(typeof(eventMsg) !="undefined"){
            if(eventMsg.includes("Opportunity was approved.")){
                helper.getLoggedInUserRecord(component, event, helper);
            }
        }        
    },
    recordUpdated:function (component, event, helper){
        helper.recordUpdated(component, event, helper);
    },    
    getUserRecord : function(component, event, helper){
        helper.getLoggedInUserRecord(component, event, helper);
    },
    validateForPostBid : function(component, event, helper){
        component.set("v.showValidation", false);
        component.set("v.isPostBidStage", true);
        helper.validateTGSOppSubmit(component, event, helper);
    },
    validateTGSOpp : function(component, event, helper){
        component.set("v.showValidation", false);
        helper.validateTGSOppSubmit(component, event, helper);
    },
    openApproveModel: function(component, event, helper) {
        component.set("v.isOpen", true);
    },
    openClosedOpptyModal: function(component, event, helper) {
        component.set("v.isOpenClosedOpptyModal", true);
    },
    rejectOpp : function(component, event, helper){
        helper.rejectOppProcess(component, event, helper);
    },
    sendBidNotification : function(component, event, helper){
        if(component.get("v.simpleRecord").Start_Date__c != null && component.get("v.simpleRecord").Project_Commencement_Date__c != null){
        	helper.sendTGSBidNotification(component, event, helper);        
        }   
        else{
            var theMessage = '';
            var startDate = component.get("v.simpleRecord").Start_Date__c;
            var kickOff = component.get("v.simpleRecord").Project_Commencement_Date__c;
            var oppType = component.get("v.simpleRecord").Global_Services_Opportunity_Type__c;
            var responseType = component.get("v.simpleRecord").Response_Type__c;
            if(startDate == null && kickOff != null){
            	theMessage = 'Start Date:Please enter the date field';
            }
            if(startDate != null && kickOff == null){
                if(oppType =='Renewal' && responseType=='PCR(Renewal)'){
                    helper.sendTGSBidNotification(component, event, helper);
                }else{
            	theMessage = 'Customer Kick-Off Date:Please enter the date field';
                }
            }
            if(startDate == null && kickOff == null){
            	theMessage = 'Customer Kick-Off Date & Start Date:Please enter the date fields';
            }
            helper.showToastMessages('dismissible','Error!','', theMessage, 'Error');                
        }
    }
})