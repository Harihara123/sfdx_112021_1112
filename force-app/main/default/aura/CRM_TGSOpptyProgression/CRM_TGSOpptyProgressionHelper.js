({
    recordUpdated : function(component, event, helper){	
        var changeType = event.getParams().changeType;
        if (changeType === "LOADED" || changeType === "CHANGED"){ 
            var tgsOpp = component.get("v.simpleRecord");
            console.log('---tgsOpp---'+tgsOpp.StageName);            
            if(tgsOpp.StageName == 'Proposing' && tgsOpp.TGS_Bid_Notification_Date__c == null && tgsOpp.TGS_Bid_Notification_User__c == null){
                component.set("v.showBidBtn",true);
            }
			if(tgsOpp.StageName == 'Negotiating'){
               component.set("v.closeBtnVariant",'brand');
            }
            component.set("v.OppStageValue",tgsOpp.StageName);
            component.set("v.ResponseType",tgsOpp.Response_Type__c);
            component.set("v.OppType",tgsOpp.Global_Services_Opportunity_Type__c);
            component.set("v.OppList",tgsOpp); 
			component.set("v.showValidation", false);
            var currentUser = $A.get("$SObjectType.CurrentUser.Id");
            console.log('Current User is' +currentUser);
            console.log('Regional Director  is' +tgsOpp.Regional_Director__c);
            if(tgsOpp.Regional_Director__c == currentUser)
            {
            component.set("v.isUserRegionalDirector",true);
            }
            var action = component.get("c.getStageInstruction");
            action.setParams({"stageValue": tgsOpp.StageName,
                              "oppId": component.get("v.recordId")
                             });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS"){
                    var acc = response.getReturnValue();
                    component.set("v.instruction",acc);
                    
                }else if(state === "ERROR"){
                    var errors = response.getError();
                }
            });        
            $A.enqueueAction(action);   
        }        
    },
    validateTGSOppSubmit : function(component, event, helper){
		var TGSDeliveryProfile = $A.get("$Label.c.TGS_Delivery_Profile"); 
        var TGSManagerProfile = $A.get("$Label.c.TGS_Manager_Profile"); 
        var TGSExecutiveProfile = $A.get("$Label.c.TGS_Executive_Profile");
        var TGSEMEAProfile = $A.get("$Label.c.TGS_EMEA_Profile");
        var profileName = component.get("v.userRecord");
        this.getTGSOpptyApprovalStatus(component, event, helper);
		component.set("v.disableOnClick",true);
        var tgsOpp = JSON.parse(JSON.stringify(component.get("v.OppList")));
        var action = component.get("c.stageValidationMessages");
        action.setParams({"opptyNewList": tgsOpp
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var acc = response.getReturnValue();
                var oppStage = component.get("v.OppStageValue");
				component.set("v.disableOnClick",false);
                var tgsOpp = component.get("v.simpleRecord");
                console.log(acc);
                if(acc.length > 0){
                    var toastmsg = acc.trim();//.replace(/,/g, '\n');
                    component.set("v.validationErrorMessages", toastmsg);
                    component.set("v.showValidation", toastmsg);
                    component.set("v.instructions", oppStage);
                    helper.showToastMessages('dismissible','Error: ',' 5000','Please complete the required fields.', 'error' );
                    console.log(toastmsg);
                }else if(component.get("v.opptyProcessInstance") == 'Pending'){
                     if(profileName.Profile.Name == TGSExecutiveProfile  || component.get("v.isTGSEMEADirectorsGroupMember") == true || component.get("v.isUserRegionalDirector") == true){
                        component.set("v.isOpen", true);
                    }else{
                        this.showToastMessages('dismissible','Error!','', 'Opportunity is locked. Please contact your Regional Director for Approval', 'error');
                    }
                }else if(oppStage == 'Interest'){                    
                    helper.updateTGSOppStage(component, tgsOpp.Probability_TGS__c);
                }else if((oppStage == 'Qualifying' || oppStage == 'Solutioning')){
                    if (!(component.get("v.ResponseType") == 'PCR(Renewal)' && component.get("v.OppType") == 'Renewal')) {
                        if(confirm("Submitting the Opportunity for Approval will lock the record for editing until your Regional Director takes action on the Opportunity.\n\nClick Cancel if you need to make any additional updates to the opportunity.\n\nOnly click OK if you do not have further edits and are ready to send the opportunity for approval.") == true){
                            helper.updateTGSOppStage(component, tgsOpp.Probability_TGS__c);
                        }else{
                            return false;
                        }
                    } else {
                        helper.updateTGSOppStage(component, '100%');
                    }
                }else if(oppStage != 'Interest' || oppStage != 'Qualifying' || oppStage != 'Solutioning'){
                    if(oppStage == 'Proposing' && component.get("v.showBidBtn") == true){
                    	helper.showToastMessages('','Error: ','', 'Bid must be sent to the Client.', 'error' );    
                    }else if(component.get("v.isPostBidStage") == false){                        
                       /* if(profileName.Profile.Name == TGSDeliveryProfile || profileName.Profile.Name == TGSManagerProfile){
                            //if(confirm("Send notification to Regional Director to Advance to Negotiating?") == true){
							helper.sendNegotiatingNotification(component, event, helper);
                            //}else{
                            //    return false;
                            //}
                        }
                        else{
                            component.set("v.isOpen", true);
                        }*/
                        component.set("v.isOpen", true);
                    }
                }
            }else if(state === "ERROR"){
				component.set("v.disableOnClick",false);
                var errors = response.getError();
            }
        });        
        $A.enqueueAction(action);        
    },
    updateTGSOppStage : function(component, probability){
        component.set("v.disableOnClick",true);
		var tgsOpp = component.get("v.simpleRecord");
        var oppId = component.get("v.recordId");
        var action = component.get("c.UpdateTGSOppStage");
        action.setParams({"TGSOppId": oppId,
                          "probValue":probability
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var oppStage = component.get("v.OppStageValue");
                component.set("v.disableOnClick",false);
                if(oppStage == 'Qualifying' || oppStage == 'Solutioning') {
                    if (!(component.get("v.ResponseType") == 'PCR(Renewal)' && component.get("v.OppType") == 'Renewal') || oppStage == 'Solutioning') {
                        component.set("v.hideSubmitBtn",true);
                        component.set("v.hideCloseBtn",true);
                    }
                    component.set("v.showApproveBtn", false); 
                }
                $A.get('e.force:refreshView').fire();
            }else if(state === "ERROR"){
				component.set("v.disableOnClick",false);
                var errors = response.getError();
			}
        });        
        $A.enqueueAction(action);
    },
    rejectOppProcess : function(component, event, helper){
        var buttonValue = event.getSource().getLocalId();
		var recordId = component.get("v.recordId");
        var action = component.get("c.ApproveOrRejectTGSOpportunity");
        action.setParams({
            "oppId": recordId,
            "approvalAction":buttonValue,
            "oppProbabilityValue":''
        });
        action.setCallback(this, function(response){            
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                component.set("v.showApproveBtn",false);
                component.set("v.hideSubmitBtn",false);
                component.set("v.hideCloseBtn",false);
                $A.get('e.force:refreshView').fire();
            }else if(state === "ERROR"){
                var errors = response.getError();
            }                
        });
        $A.enqueueAction(action);
    },
    sendTGSBidNotification : function(component, event, helper){
        component.set("v.disableOnClick",true);
        var recordId = component.get("v.recordId");
        var action = component.get("c.updateOppBidNotifiction");
        action.setParams({"oppId": recordId
                         });
        action.setCallback(this, function(response){            
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                component.set("v.showBidBtn",false);
                component.set("v.disableOnClick",false);
                helper.showToastMessages('dismissible','Success!','', 'Bid notification sent.', 'success');
				$A.get('e.force:refreshView').fire();                
            }else if(state === "ERROR"){
                var errors = response.getError();
                component.set("v.disableOnClick",false);
            }                
        });
        $A.enqueueAction(action);
    },
    sendNegotiatingNotification : function(component, event, helper){
    	var recordId = component.get("v.recordId");
        var action = component.get("c.emailNegotiatingNotification");
        action.setParams({"oppId": recordId
                         });
        action.setCallback(this, function(response){            
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                //component.set("v.disableOnClick",false);
                helper.showToastMessages('dismissible','Success!','', 'Notification has been sent to Regional Director.', 'success');                
            }else if(state === "ERROR"){
                var errors = response.getError();
                //component.set("v.disableOnClick",false);
            }                
        });
        $A.enqueueAction(action);
	},
    showToastMessages : function(sticky,title, duration, message, type ){
       // message = message.replaceAll('<br/>', '\n');
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "mode": sticky,
            "title": title,
            "duration":duration,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
    getTGSOpptyApprovalStatus : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.getTGSOppProcessInstance");
        action.setParams({"oppId": recordId
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var processInstance = response.getReturnValue();
                component.set("v.opptyProcessInstance",processInstance);
            }else if(state === "ERROR"){
                var errors = response.getError();
            }
        });        
        $A.enqueueAction(action);
    },
    getLoggedInUserRecord : function(component, event, helper){
        var profileName = component.get("v.userRecord");
        console.log('--Profile Name--'+profileName.Profile.Name);
        var recordId = component.get("v.recordId");
        var TGSExecutiveProfile = $A.get("$Label.c.TGS_Executive_Profile"); 
        helper.isTGSEMEADirectorGrpMember(component,event,helper);
        var action = component.get("c.getTGSOppProcessInstance");
        action.setParams({"oppId": recordId
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var processInstance = response.getReturnValue();
                component.set("v.opptyProcessInstance",processInstance);
                if(component.get("v.OppStageValue") == 'Interest' || component.get("v.OppStageValue") == 'Qualifying' || component.get("v.OppStageValue") == 'Solutioning'){
                    //component.set("v.btnLabel",'Submit');
                    if(processInstance == 'Pending' && (profileName.Profile.Name == TGSExecutiveProfile || profileName.Profile.Name == 'System Administrator' || component.get("v.isTGSEMEADirectorsGroupMember") == true || component.get("v.isUserRegionalDirector") == true )){
                        component.set("v.showApproveBtn", true);
                        component.set("v.hideSubmitBtn",true);
                        component.set("v.hideCloseBtn",true);
                    }else if(processInstance == '' && (profileName.Profile.Name == TGSExecutiveProfile || profileName.Profile.Name == 'System Administrator' || component.get("v.isTGSEMEADirectorsGroupMember") == true || component.get("v.isUserRegionalDirector") == true)){
                        component.set("v.showApproveBtn", false);   
                    }else if(processInstance == 'Pending' && (profileName.Profile.Name != TGSExecutiveProfile || component.get("v.isTGSEMEADirectorsGroupMember") != true || component.get("v.isUserRegionalDirector") != true)){
                        component.set("v.hideSubmitBtn",true);
                        component.set("v.hideCloseBtn",true);
                        component.set("v.showApproveBtn", false);    
                    }else if(processInstance == '' && (profileName.Profile.Name != TGSExecutiveProfile || component.get("v.isTGSEMEADirectorsGroupMember") != true || component.get("v.isUserRegionalDirector") != true) && component.get("v.OppStageValue") == 'Solutioning'){                        
                        component.set("v.hideCloseBtn",false);                        
                    }
                }else if((component.get("v.OppStageValue") != 'Interest' || component.get("v.OppStageValue") != 'Qualifying' || component.get("v.OppStageValue") != 'Solutioning') && (profileName.Profile.Name == TGSExecutiveProfile || profileName.Profile.Name == 'System Administrator' || component.get("v.isTGSEMEADirectorsGroupMember") == true || component.get("v.isUserRegionalDirector") == true)){
                    component.set("v.btnLabel",'Approve');
                    if(component.get("v.OppStageValue") == 'Proposing' && component.get("v.showBidBtn") == true){
                        component.set("v.showBidBtn",true);
                    }if(component.get("v.OppStageValue") == 'Negotiating'){
                        component.set("v.hideSubmitBtn",true);
                    }
                }else if(profileName.Profile.Name != TGSExecutiveProfile || component.get("v.isTGSEMEADirectorsGroupMember") != true || component.get("v.isUserRegionalDirector") != true){
					//component.set("v.btnLabel",'Submit');
                    //component.set("v.hideSubmitBtn",true);                    
                    if(component.get("v.OppStageValue") == 'Negotiating'){
                        component.set("v.hideCloseBtn",false);
						component.set("v.hideSubmitBtn",true);
                    }else{
                        component.set("v.btnLabel",'Approve');
                        component.set("v.hideCloseBtn",false);
                    }
                    component.set("v.showApproveBtn", false); 
                    //component.set("v.showBidBtn",false);
                }
            }else if(state === "ERROR"){
                var errors = response.getError();
            }
			component.set("v.showCmp", true);
        });        
        $A.enqueueAction(action);
        $A.get('e.force:refreshView').fire();
    },
    isTGSEMEADirectorGrpMember : function(component,event,helper)
    {
        var action = component.get("c.isTGSEMEADirectorGroupMember");
        action.setCallback(this,function(response){
            var resState = response.getState();
            if(resState == 'SUCCESS')
            {
                var responseVal = response.getReturnValue();
                component.set("v.isTGSEMEADirectorsGroupMember",responseVal);
                }
        });
        $A.enqueueAction(action);
    }
})