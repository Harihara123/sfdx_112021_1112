({
	recordLoadedView :function(cmp, event, helper) {
        console.log('record view');
        try {
           var flag=cmp.get("v.viewLoaded");
            cmp.set("v.skillRefreshFlag",true);
            cmp.set("v.jobTitleRefreshFlag",true);
            cmp.set("v.addinsertFlag",true);
           if(flag==false){
                 cmp.set("v.viewLoaded",true);
                 cmp.set("v.editFlag",false);
                 cmp.set("v.viewFlag",true);
                
                var recordU=event.getParam("recordUi");
                var prodval=event.getParams().recordUi.record.fields.Req_Product__c.value;
                console.log('prodtype val rU'+prodval);
                
               var ocCode = event.getParams().recordUi.record.fields.Req_RVT_Occupation_Code__c.value;
               var ocGroup = event.getParams().recordUi.record.fields.RVT_Occupation_Group__c.value;
               var jobLevel = event.getParams().recordUi.record.fields.Req_Job_Level__c.value;
               var metro = event.getParams().recordUi.record.fields.Metro__c.value;
               var country = event.getParams().recordUi.record.fields.Req_Worksite_Country__c.value;
               var lcountry = country.toLowerCase();
               var fcountry;
               if(lcountry=='united states' || lcountry=='usa' || lcountry=='us'){
                   fcountry = 'USA';
               }
               else if(lcountry='canada' || lcountry=='can' || lcountry=='ca'){
                   fcountry = 'CAN';
               }               
               
               if((ocCode!=null && ocCode!=0) && (ocGroup!=null && ocGroup!=0) && (jobLevel!=null && jobLevel!=0) && metro != null && fcountry != null){
                 var baseURL = $A.get("$Label.c.RVT_PROD_URL")
                 var link = baseURL+'&parent_hcs_description='+ocGroup+'&Hcs%20Group%20Display='+ocCode+'&Hcs%20Group%20Level%20Display='+jobLevel+'&Country%20Code='+fcountry+'&Ag%20Geog%20Short%20Name%20Orig='+metro;
                 cmp.set("v.RVTVisibility",true);
                 cmp.set("v.RVTLink",link);
               }
                
				/*var actSectsList = cmp.get("v.activeSections"); 
                    // F cth and SI -- Perm
                 if(prodval=='Permanent' ){
                         cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",false);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                 }else if(prodval=='Contract to Hire'){
                     cmp.set("v.permFlag",true);
                          cmp.set("v.cthFlag",true);
                       
                         if(actSectsList != undefined){
                             actSectsList.push("SI");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                         } 
                     
                 }else{
                          cmp.set("v.cthFlag",true);
                          cmp.set("v.permFlag",false);
                          
                         if(actSectsList != undefined){
                              console.log("act sec"+actSectsList);
                             actSectsList.push("F");
                             //cmp.set("v.activeSections",actSectsList); 
                             setTimeout($A.getCallback(
                                    () => cmp.set("v.activeSections", actSectsList)
                                ));
                              console.log("after F sec"+actSectsList);
                         } 
                     }
                    
                    */
                    event.setParam("recordUi",recordU);
                  }
        }
        catch(err) {
            console.log('errors '+err);
        } 
        finally {
            
            var sgn=event.getParams().recordUi.record.fields.StageName.value;
            //New Qualifying Stage Changes
            var statusLDS = event.getParams().recordUi.record.fields.LDS_Status__c.value;
            var prevStage = event.getParams().recordUi.record.fields.Req_Prev_Stage_Name_c__c.value;
             
            console.log('stg val rU'+sgn);
            console.log('Status SK '+statusLDS);
            console.log('stg val Prev'+prevStage);
            var opts;          
            
            
            cmp.set("v.req_stage",sgn);
            
            //Open Req
            if(statusLDS == 'Open'){
               if(sgn == 'Qualified'){
                      cmp.set("v.stgQual",true);
                   }           
               if(sgn == 'Draft'){
                   opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                       ];
                      }  
                else{
                      opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                       ];
                    }
            }
            //Closed Req
            if(statusLDS == 'Closed'){          
               if(prevStage=='Draft' && sgn.substring(0,6) == 'Closed'){
                   opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft", selected: "true" },
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
                       ];
                      }
               else if(prevStage == 'Staging' && sgn.substring(0,6)=='Closed'){
                   opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" },
                       	   { "class": "slds-input slds-combobox__input ", label: "Staging", value: "Staging" ,selected: "true"}
                       ];
                      }                
                else{
                      opts = [
                           { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft"},
                           { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" ,selected: "true"}
                       ];
                    }
            } 
              
                    
                    if(cmp.find("tekdetailStageV") != undefined){
                         var stgVw = cmp.find("tekdetailStageV");
                         var flag=Array.isArray(stgVw);
                         if(flag){
                             console.log("stageV array");
                             stgVw[0].set("v.options", opts);
                            }else{
                              console.log("not array");
                              cmp.find("tekdetailStageV").set("v.options", opts);
                            } 
                    } 
			var actSectsList = cmp.get("v.activeSections");
            var tgsValue = event.getParams().recordUi.record.fields.Req_TGS_Requirement__c.value;            
            if(tgsValue == 'Yes'){
                actSectsList.push("K");
                cmp.set("v.isTGSTrue", "true");
                setTimeout($A.getCallback(
                    () => cmp.set("v.activeSections", actSectsList)
                ));
            }
			var PlacementType = event.getParams().recordUi.record.fields.Req_Product__c.value;            
            if(PlacementType == 'Permanent'){
                actSectsList.push("SI");
                cmp.set("v.permFlag",true);
                cmp.set("v.cthFlag",false);
                setTimeout($A.getCallback(
                    () => cmp.set("v.activeSections", actSectsList)
                ));
            }else if(PlacementType == 'Contract' || PlacementType == 'Contract to Hire'){
                actSectsList.push("F");
                cmp.set("v.permFlag",false);
                cmp.set("v.cthFlag",true);
                setTimeout($A.getCallback(
                    () => cmp.set("v.activeSections", actSectsList)
                ));
            }
            helper.loadPicklistValues(cmp, event, helper);
        }
    },
    enableInlineEdit :function(cmp, event, helper) {
        console.log('inline edit pressd');  
        cmp.set("v.editFlag",true);
        cmp.set("v.viewFlag",false);
        cmp.set("v.editDisable",false);
        
         var opts = [
                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
        	];
        
       cmp.find("tekdetailStage").set("v.options", opts);

       if(cmp.get("v.editFlag")){
            // set coordinates of fixed save/cancel bar
            let editFormWidth = cmp.find("tekFormContainer").getElement().clientWidth;
           

            cmp.set("v.containerWidth", editFormWidth);
           
        }
        cmp.set("v.skillRefreshFlag",true);
        cmp.set("v.jobTitleRefreshFlag",true);
        if(event.getSource().getLocalId()!=null || typeof event.getSource().getLocalId()!='undefined'){
            var sId=event.getSource().getLocalId();
            
            var id=sId.substring(0,sId.length-1);
            cmp.set("v.tekscrollId",id);
            
        }
        
    },
	onStageChange: function(cmp) {
        
        var stgSelect = cmp.find("tekdetailStage"); 
        
        cmp.set("v.req_stage",stgSelect.get("v.value"));
        if(stgSelect.get("v.value") == 'Draft'){
        	cmp.find("tekSumRdZn").set("v.value", false);
        }
        
        if(stgSelect.get("v.value") == 'Qualified'){
            cmp.set("v.stgQual",true);
        }
        
        
	 },
    recordLoadedEdit :function(cmp, event, helper) {
        console.log('edit pressed'); 
        
        var editLoadedFlag=cmp.get("v.editLoaded");
         if(editLoadedFlag==false){
             cmp.set("v.editFlag",true);
             cmp.set("v.viewFlag",false);
             
             var jobDescription;
             var topSkillDetails;
             var busQuals;
             var IntExts;
             var busChall;
             var ImpExts;
             var workEnv;
             var evp;
             var addnlInfo;
             var comp;
             var extCommJobdesc;
             var intInf;
             var compInfs;
             var reqApprvs;
             var whyPosnOpen;
            
			var acId=event.getParams().recordUi.record.fields.AccountId.value;
			if(typeof acId!='undefined' && acId!=null){
				var fcmp=cmp.find('accountLoader');
				fcmp.set("v.recordId",acId);
				fcmp.reloadRecord();			   
			} 
             
            /*if(event.getParams().recordUi.record.fields.Req_Job_Title__c!=null){
                 if(!cmp.get("v.loopFlag")){
                     var jobAryId=cmp.find("aerodetailReqJobTitle");
                     var jobTitle=event.getParams().recordUi.record.fields.Req_Job_Title__c.value;
                     var flg=Array.isArray(jobAryId);
                     cmp.set("v.lookupJTFlag",false);
                     if(flg){
                         console.log("jobAryId is array");
                         jobAryId[0].set("v.value",jobTitle);
                        }else{
                          console.log("not an array");
                          jobAryId.set("v.value",jobTitle);
                       }
                }
             }*/
             
             if(event.getParams().recordUi.record.fields.Req_Job_Description__c!=null){
                 jobDescription=event.getParams().recordUi.record.fields.Req_Job_Description__c.value;
                 cmp.set("v.editJobdescription",jobDescription);
             }
              
             if(event.getParams().recordUi.record.fields.Req_Skill_Details__c !=null){
                 topSkillDetails=event.getParams().recordUi.record.fields.Req_Skill_Details__c.value;
                 cmp.set("v.editTopSkillDetls",topSkillDetails);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Qualification__c !=null){
                 busQuals=event.getParams().recordUi.record.fields.Req_Qualification__c.value;
                 cmp.set("v.editQuals",busQuals);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Internal_External_Customer__c !=null){
                IntExts =event.getParams().recordUi.record.fields.Req_Internal_External_Customer__c.value;
                 cmp.set("v.editIntExts",IntExts);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Business_Challenge__c !=null){
                busChall =event.getParams().recordUi.record.fields.Req_Business_Challenge__c.value;
                 cmp.set("v.editBusinessChals",busChall);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Impact_to_Internal_External_Customer__c !=null){
                ImpExts =event.getParams().recordUi.record.fields.Req_Impact_to_Internal_External_Customer__c.value;
                 cmp.set("v.editImpctExts",ImpExts);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Work_Environment__c !=null){
                 workEnv =event.getParams().recordUi.record.fields.Req_Work_Environment__c.value;
                 cmp.set("v.editWorkEnv",workEnv);
             }
             
             if(event.getParams().recordUi.record.fields.Req_EVP__c !=null){
                 evp =event.getParams().recordUi.record.fields.Req_EVP__c.value;
                 cmp.set("v.editTekEvp",evp);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Additional_Information__c !=null){
                 addnlInfo =event.getParams().recordUi.record.fields.Req_Additional_Information__c.value;
                 cmp.set("v.editAddnlInfo",addnlInfo);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Compliance__c !=null){
                 comp =event.getParams().recordUi.record.fields.Req_Compliance__c.value;
                 cmp.set("v.editCompliance",comp);
             }
             
             if(event.getParams().recordUi.record.fields.Req_External_Job_Description__c !=null){
                 extCommJobdesc =event.getParams().recordUi.record.fields.Req_External_Job_Description__c.value;
                 cmp.set("v.editExtCommJobDesc",extCommJobdesc);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Interview_Information__c != null){
                 intInf = event.getParams().recordUi.record.fields.Req_Interview_Information__c.value;
                 cmp.set("v.editIntInfoE",intInf);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Competition_Info__c != null){
                 compInfs = event.getParams().recordUi.record.fields.Req_Competition_Info__c.value;
                 cmp.set("v.editCompInfos",compInfs);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Approval_Process__c != null){
                 reqApprvs = event.getParams().recordUi.record.fields.Req_Approval_Process__c.value;
                  cmp.set("v.editReqApprvs",compInfs);
             }
             
             if(event.getParams().recordUi.record.fields.Req_Why_Position_Open_Details__c != null){
                 whyPosnOpen = event.getParams().recordUi.record.fields.Req_Why_Position_Open_Details__c.value;
                 cmp.set("v.editWhyPosnOpens",whyPosnOpen);
             }
             
            var stgName =event.getParams().recordUi.record.fields.StageName.value;
            
             if(cmp.find("tekdetailStage") != undefined){
                 var stgde = cmp.find("tekdetailStage");
                 var flag=Array.isArray(stgde);
                 if(flag ){
                     console.log("stage is array");
                     stgName = (stgName == 'Draft') ? 'Draft' : 'Qualified';
                     stgde[0].set("v.value",stgName);
                    }else{
                      console.log("not an array");
                      stgName = (stgName == 'Draft') ? 'Draft' : 'Qualified';
                      cmp.find("tekdetailStage").set("v.value",stgName);
                    }
                 
             }
             if(cmp.find("tekdetailAcctId") != undefined){
                 var tekAcc = cmp.find("tekdetailAcctId");
                 var flg=Array.isArray(tekAcc);
                if(flg){
                     console.log("acc is array");
                     tekAcc[0].set("v.value",event.getParams().recordUi.record.fields.AccountId.value);
                    }else{
                      console.log("not an array");
                      cmp.find("tekdetailAcctId").set("v.value",event.getParams().recordUi.record.fields.AccountId.value);
                   }
             }
             
             if(cmp.find("tekOwn") != undefined){
                var tekOwr = cmp.find("tekOwn");
                var flg=Array.isArray(tekOwr);
                var owrId = event.getParams().recordUi.record.fields.OwnerId.value;
                console.log("ownr id"+owrId);
                if(flg){
                     console.log("owr is array");
                     tekOwr[0].set("v.value",owrId);
                    }else{
                      console.log("not an array");
                      tekOwr.set("v.value",owrId);
                   }
             }
         }
    },
    handleError : function(cmp,event,helper){
        console.log('error reached '); 
         cmp.set("v.editDisable",false);
        var params = event.getParams();
        var Validation = false;
        var validationMessages=[];
        
         if(params.output!=null && params.output.fieldErrors!=null ){
           
            var stgErrors=params.output.fieldErrors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      var checkFlag=Array.isArray(value);
                      console.log(key + ':' + value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
					  Validation = true;
                });
         
        }
        
        if(params.output!=null && params.output.errors!=null ){
           
            var stgErrors=params.output.errors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
					  Validation = true;
                });
         
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
        var fieldErrors=event.getParams().error.data.output.fieldErrors;
        Object.keys(fieldErrors).forEach(function(key){
  				      var value = fieldErrors[key];
   					  console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
            
            		  validationMessages.push(fmessage);
					  Validation = true;
            
			});
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {
            
            var fieldErrors=event.getParams().error.data.output.errors;
            
            Object.keys(fieldErrors).forEach(function(key){
  				  var value = fieldErrors[key];
   					 console.log(key + ':' + value);
                   var checkFlag=Array.isArray(value);
            	   var fmessage=(checkFlag==true)?value[0].message:value.message;
            		  validationMessages.push(fmessage);
					  Validation = true;
            
			});
            
        }
        cmp.set("v.Validation",Validation);
        cmp.set("v.validationMessagesList",validationMessages);
 		console.log('handleError----------->'+validationMessages);
        
        console.log("ValidationMessage Retrieve----->"+cmp.get("v.validationMessagesList"));
    },
    handleCancel : function (cmp,event,helper){
		/*cmp.set("v.validationMessagesList",null);
		cmp.set("v.editFlag",false);
		cmp.set("v.viewFlag",true);  
		cmp.set("v.cancelFlag",true);
		cmp.set("v.editDisable",false);		        
		cmp.set("v.stageErrorVarName","");
		cmp.set("v.jobTitleErrorVarName","");
		cmp.set("v.hiringManagerErrorVarName","");
		cmp.set("v.accountErrorVarName","");
		cmp.set("v.draftReasonVarName","");
		cmp.set("v.officeErrorVarName","");
		cmp.set("v.plmtTypeErrorVarName","");
		cmp.set("v.streetErrorVarName","");
		cmp.set("v.cityTypeErrorVarName","");
		cmp.set("v.stateErrorVarName","");
		cmp.set("v.countryVarName","");
		cmp.set("v.zipErrorVarName","");
		cmp.set("v.currencyErrorVarName","");
		cmp.set("v.aeroreqExtComJobDescE","");
		cmp.set("v.skillPillValidationError","");
		cmp.set("v.Validation",false);*/
		event.preventDefault();
    },
    
    onSubmit  : function(cmp, event, helper) {
       console.log('on submit click 1');
        cmp.set("v.editDisable",true);
	   var Validation = false;
        event.preventDefault();
        //cmp.set("v.skillRefreshFlag",true);
        //cmp.set("v.jobTitleRefreshFlag",false);
        var fieldMessages=[];
        var eventFields = event.getParam("fields");
        
        
        helper.validateAddress(cmp,event);
        var workSiteOpp=cmp.get("v.Opportunity");
        var addFieldMessages=cmp.get("v.addFieldMessage");
        var validated = true;
        if(typeof addFieldMessages!='undefined' && addFieldMessages!=null && addFieldMessages.length>0){
            fieldMessages=fieldMessages.concat(addFieldMessages);
            validated=false;
        }else{
            fieldMessages=fieldMessages.concat(addFieldMessages)
            eventFields.Req_Worksite_Street__c=workSiteOpp.Req_Worksite_Street__c;
			eventFields.Req_Worksite_City__c=workSiteOpp.Req_Worksite_City__c;
			eventFields.Req_Worksite_State__c=workSiteOpp.Req_Worksite_State__c;
			eventFields.Req_Worksite_Country__c=workSiteOpp.Req_Worksite_Country__c;
            eventFields.StreetAddress2__c=workSiteOpp.StreetAddress2__c;
            eventFields.Req_Worksite_Postal_Code__c=workSiteOpp.Req_Worksite_Postal_Code__c;
            eventFields.Req_GeoLocation__Latitude__s=workSiteOpp.Req_GeoLocation__Latitude__s;
            eventFields.Req_GeoLocation__Longitude__s=workSiteOpp.Req_GeoLocation__Longitude__s;
        }
        
        helper.prepareSkills(cmp, event);
        cmp.set("v.loopFlag",false);
        var finalSkillList=cmp.get("v.finalskillList");
        var jsonSkills=JSON.stringify(finalSkillList);
        var eventFields = event.getParam("fields");
        eventFields.EnterpriseReqSkills__c=jsonSkills;
       
        
        if(eventFields.StageName == null || eventFields.StageName == ''){
            validated=false;
            cmp.set("v.stageErrorVarName","Stage Name cannot be blank.");
            fieldMessages.push("Stage Name cannot be blank.");
           }else{
             cmp.set("v.stageErrorVarName","");
          }
        
		if(eventFields.Req_Skill_Specialty__c == null || eventFields.Req_Skill_Specialty__c == '' || eventFields.Req_Skill_Specialty__c == '--None--'){
            validated=false;
            cmp.set("v.skillSpecialtyVal","Skill Specialty cannot be blank.");
            fieldMessages.push("Skill Specialty cannot be blank.");
           }else{
             cmp.set("v.skillSpecialtyVal","");
          }
		  
         if(cmp.get("v.editJobdescription")!=null)
          eventFields.Req_Job_Description__c=cmp.get("v.editJobdescription");
        //processing addnl
         if(eventFields.Req_Job_Description__c != null)
             eventFields.Description=eventFields.Req_Job_Description__c;
        
        
       
         var appName = cmp.find("tekreqSummaryJobDescription");
		 var input = cmp.find("tekdetailStage");
         if ((appName.get("v.value") == null || appName.get("v.value") == '') && input.get("v.value") == 'Qualified') {
             validated=false; 
             //var inputDesc = cmp.find("tekreqSummaryJobDescription");
             //cmp.set("v.aeroreqSmyJobDescError","Description is required.");
             appName.set("v.errors", [{message:"Description is required."}]);
             fieldMessages.push("Description is required.");
         }
         else if(appName.get("v.value") != null && appName.get("v.value") != '' && appName.get("v.value").length < 25 && eventFields.StageName == 'Qualified' ) {
             validated=false; 
             //cmp.set("v.aeroreqSmyJobDescError","Description is required.");
             appName.set("v.errors", [{message:"Job Description should have at least 25 characters."}]);
             fieldMessages.push("Job Description should have at least 25 characters.");
         }
        else{
             appName.set("v.errors", null);
             cmp.set("v.mlareqSmyJobDescError","");
         }
        
        var quals = cmp.find("tekreqSummaryQualification");
        if(((quals.get("v.value") == null) || (quals.get("v.value") == '')) && input.get("v.value") == 'Qualified'){
            validated=false;
            //var inputQual = cmp.find("tekreqSummaryQualification");
            //cmp.set("v.tekreqSummaryQualification","Additional Skills & Qualifications is required.");
            quals.set("v.errors", [{message:"Additional Skills & Qualifications is required."}]);
            fieldMessages.push("Additional Skills & Qualifications is required.");
        }else{
            quals.set("v.errors", null);
            cmp.set("v.tekAddnlQualsError","");
        }
        
        var jobtitleAry = cmp.get("v.jobtitle");
        if(jobtitleAry==undefined || jobtitleAry==null || jobtitleAry.length<=0){
            validated=false;
            cmp.set("v.jobTitleErrorVarName","Job Title cannot be blank.");
            //cmp.set("v.jobTitleRefreshFlag",true);
             //cmp.set("v.skillRefreshFlag",true);
            var tempAr=[];
            cmp.set("v.jobtitle",tempAr);
            fieldMessages.push("Job Title cannot be blank.");
        }else{
            if((jobtitleAry!=null && jobtitleAry!=undefined && jobtitleAry[0]  != null && jobtitleAry[0]  != '')){
                eventFields.Req_Job_Title__c=jobtitleAry[0].name;
            }
            cmp.set("v.jobTitleErrorVarName","");
        }
        
         
        if(cmp.get("v.editTopSkillDetls") != null){
            eventFields.Req_Skill_Details__c =cmp.get("v.editTopSkillDetls");
        }
        
        if(cmp.get("v.editQuals") != null){
            eventFields.Req_Qualification__c =cmp.get("v.editQuals");
        }
        
        if(cmp.get("v.editIntExts") != null){
            eventFields.Req_Internal_External_Customer__c =cmp.get("v.editIntExts");
        }
        
        if(cmp.get("v.editBusinessChals") != null){
            eventFields.Req_Business_Challenge__c =cmp.get("v.editBusinessChals");
        }
        
        if(cmp.get("v.editWhyPosnOpens") != null){
            eventFields.Req_Why_Position_Open_Details__c =cmp.get("v.editWhyPosnOpens");
        }
        
        if(cmp.get("v.editImpctExts") != null){
            eventFields.Req_Impact_to_Internal_External_Customer__c =cmp.get("v.editImpctExts");
        }
        
        if(cmp.get("v.editWorkEnv") != null){
            eventFields.Req_Work_Environment__c =cmp.get("v.editWorkEnv");
        }
        
        if(cmp.get("v.editTekEvp") != null){
            eventFields.Req_EVP__c =cmp.get("v.editTekEvp");
        }
        
        if(cmp.get("v.editAddnlInfo") != null){
            eventFields.Req_Additional_Information__c =cmp.get("v.editAddnlInfo");
        }
        
        if(cmp.get("v.editCompliance") != null){
            eventFields.Req_Compliance__c =cmp.get("v.editCompliance");
        }
        
        if(cmp.get("v.editExtCommJobDesc") != null){
            eventFields.Req_External_Job_Description__c = cmp.get("v.editExtCommJobDesc");
        }
        
        if(cmp.get("v.editIntInfoE") != null){
            eventFields.Req_Interview_Information__c = cmp.get("v.editIntInfoE");
        }
        
        if(cmp.get("v.editCompInfos") != null){
            eventFields.Req_Competition_Info__c = cmp.get("v.editCompInfos");
        }
        
        if(cmp.get("v.editReqApprvs") != null){
            eventFields.Req_Approval_Process__c = cmp.get("v.editReqApprvs");
        }
        
         var dsn=cmp.find('tekdetailStage');
         var stgName =dsn.get("v.value");
        if(eventFields.StageName =='Draft' || eventFields.StageName =='Qualified'){
           eventFields.StageName =stgName;
        }
        //validation
         
         
         /*if(eventFields.Req_Job_Title__c == null || eventFields.Req_Job_Title__c == ''){
            validated=false;
            cmp.set("v.jobTitleErrorVarName","Job Title cannot be blank.");
            fieldMessages.push("Job Title cannot be blank.");
           }else{
             cmp.set("v.jobTitleErrorVarName","");
          }*/
         
        
         if(eventFields.Req_Hiring_Manager__c == null || eventFields.Req_Hiring_Manager__c == ''){
            validated=false;
            cmp.set("v.hiringManagerErrorVarName","Hiring Manager cannot be blank.");
            fieldMessages.push("Hiring Manager cannot be blank.");
           }else{
             cmp.set("v.hiringManagerErrorVarName","");
          }
        
        if(eventFields.Req_Total_Positions__c == null || eventFields.Req_Total_Positions__c == ''){
            validated=false;
            cmp.set("v.totalPositionsVarName", "Total Positions cannot be blank.");
            fieldMessages.push("Total Positions cannot be blank.");
        }else{
            cmp.set("v.totalPositionsVarName", "");
        }
       
         if(eventFields.LDS_Account_Name__c == null || eventFields.LDS_Account_Name__c == ''){
            validated=false;
            cmp.set("v.accountErrorVarName","Account Name cannot be blank.");
            fieldMessages.push("Account Name cannot be blank.");
           }else{
             cmp.set("v.accountErrorVarName","");
          }
        
         if((eventFields.Req_Draft_Reason__c == null) || (eventFields.Req_Draft_Reason__c == '') && eventFields.StageName == 'Draft'){
            validated=false;
            cmp.set("v.draftReasonVarName","Draft Reason cannot be blank.");
            fieldMessages.push("Draft Reason cannot be blank.");
           }else{
             cmp.set("v.draftReasonVarName","");
          }
        
         if(eventFields.Organization_Office__c == null || eventFields.Organization_Office__c == ''){
            validated=false;
            cmp.set("v.officeErrorVarName","Office cannot be blank.");
            fieldMessages.push("Office cannot be blank.");
           }else{
             cmp.set("v.officeErrorVarName","");
          }
        
         if(eventFields.Req_Product__c == null || eventFields.Req_Product__c == ''){
            validated=false;
            cmp.set("v.plmtTypeErrorVarName","Product cannot be blank.");
            fieldMessages.push("Product cannot be blank.");
           }else{
             cmp.set("v.plmtTypeErrorVarName","");
          }
        
        /*
         if(eventFields.Req_Worksite_Street__c == null || eventFields.Req_Worksite_Street__c == ''){
            validated=false;
            cmp.set("v.streetErrorVarName","Street cannot be blank.");
             fieldMessages.push("Street cannot be blank.");
           }else{
             cmp.set("v.streetErrorVarName","");
          }
        
         
         if(eventFields.Req_Worksite_City__c == null || eventFields.Req_Worksite_City__c == ''){
            validated=false;
            cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
             fieldMessages.push("City cannot be blank.");
           }else{
             cmp.set("v.cityTypeErrorVarName","");
          }
        
         if(eventFields.Req_Worksite_State__c == null || eventFields.Req_Worksite_State__c == ''){
            validated=false;
            cmp.set("v.stateErrorVarName","State cannot be blank.");
             fieldMessages.push("State cannot be blank.");
           }else{
             cmp.set("v.stateErrorVarName","");
          }
        
         if(eventFields.Req_Worksite_Country__c == null || eventFields.Req_Worksite_Country__c == ''){
            validated=false;
            cmp.set("v.countryVarName","Country cannot be blank.");
            fieldMessages.push("Country cannot be blank.");
           }else{
             cmp.set("v.countryVarName","");
          }
        
         if(eventFields.Req_Worksite_Postal_Code__c == null || eventFields.Req_Worksite_Postal_Code__c == ''){
            validated=false;
            cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
             fieldMessages.push("Postal Code cannot be blank.");
           }else{
             cmp.set("v.zipErrorVarName","");
          }
          */
         
		if(eventFields.Currency__c == null || eventFields.Currency__c == ''){
			validated=false;
			cmp.set("v.currencyErrorVarName","Currency cannot be blank.");
			fieldMessages.push("Currency cannot be blank.");
		}else{
			cmp.set("v.currencyErrorVarName","");
		}
        if(eventFields.Product__c == null || eventFields.Product__c == ''){
            validated=false;
            cmp.set("v.productErrorVarName","Primary Buyer (NextGen Division) cannot be blank.");
            fieldMessages.push("Primary Buyer (NextGen Division) cannot be blank.");
        }else{
            cmp.set("v.productErrorVarName","");
        }
         if(((eventFields.Req_External_Job_Description__c == null) || (eventFields.Req_External_Job_Description__c == '')) && eventFields.StageName == 'Qualified'){
            validated=false;
            cmp.set("v.extCommJbDescVarName","External Job description cannot be blank.");
            fieldMessages.push("External Job description cannot be blank.");
           }else{
             cmp.set("v.extCommJbDescVarName","");
          }
        
        var pillData=cmp.get("v.finalskillList"); 
        if(stgName=='Qualified' && (pillData==null || pillData=='' || typeof pillData == "undefined")){
            cmp.set("v.skillPillValidationError","At least one skill is required before moving to the 'Qualified' stage or above.");
            cmp.set("v.skillRefreshFlag",true);
            validated=false;
            fieldMessages.push("At least one skill is required before moving to the 'Qualified' stage or above.");
        }else{
            cmp.set("v.skillPillValidationError","");
        }
        
		if(eventFields.Req_TGS_Requirement__c == null || eventFields.Req_TGS_Requirement__c == '' || eventFields.Req_TGS_Requirement__c == '--None--'){
            validated=false;
            cmp.set("v.tekTGSReqVal","You must indicate if this is a TGS Requirement.");
            fieldMessages.push("You must indicate if this is a TGS Requirement.");
        }else if(eventFields.Req_TGS_Requirement__c == 'No'){
            eventFields.Legacy_Record_Type__c = '';
            cmp.set("v.tekTGSReqVal","");
            cmp.set("v.isTGSTrue", "true");
            eventFields.Practice_Engagement__c = '';
            eventFields.GlobalServices_Location__c = '';
            eventFields.Employment_Alignment__c = '';
            eventFields.Final_Decision_Maker__c = '';
            eventFields.Is_International_Travel_Required__c = '';
            eventFields.Internal_Hire__c = '';
            eventFields.National__c = false;
            eventFields.Backfill__c = false;
			eventFields.Req_Disposition__c = '';
            cmp.set("v.isTGSPEName","");
            cmp.set("v.isTGSLocationName","");
            cmp.set("v.isTGSEmpAlignName","");
            cmp.set("v.isFinaDecMakName","");
            cmp.set("v.isIntNationalName","");
            cmp.set("v.isInternalName","");
            cmp.set("v.isTGSTrue", "false");
        }else if(eventFields.Req_TGS_Requirement__c == 'Yes'){
            eventFields.Legacy_Record_Type__c = 'Global Services Req';
            cmp.set("v.tekTGSReqVal","");       
            if((typeof input.get("v.value") == "undefined" || input.get("v.value") == '--None--' || input.get("v.value") != 'Draft') && (typeof cmp.get("v.req.Practice_Engagement__c") == "undefined" || cmp.get("v.req.Practice_Engagement__c") == '' || cmp.get("v.req.Practice_Engagement__c") == '--None--')){
                validated = false;
                cmp.set("v.isTGSPEName","Select a Practice Engagement value.");
                fieldMessages.push("Select a Practice Engagement value.");
            }else{
                eventFields.Practice_Engagement__c = cmp.find("isTGSPEId").get("v.value");
                cmp.set("v.isTGSPEName","");
            }
            if(typeof input.get("v.value") == "undefined" || input.get("v.value") == '--None--' || input.get("v.value") !=''){
                if(eventFields.GlobalServices_Location__c == '' || eventFields.GlobalServices_Location__c == '--None--'){
                    validated = false;
                    cmp.set("v.isTGSLocationName","Select a Location value.");
                    fieldMessages.push("Select a Location value.");
                }else{
                    cmp.set("v.isTGSLocationName","");
                }
                if(eventFields.Employment_Alignment__c == '' || eventFields.Employment_Alignment__c == '--None--'){
                    validated = false;
                    cmp.set("v.isTGSEmpAlignName","Select an Employment Alignment value.");
                    fieldMessages.push("Select an Employment Alignment value.");
                }else{
                    cmp.set("v.isTGSEmpAlignName","");
                }
                if(eventFields.Final_Decision_Maker__c == '' || eventFields.Final_Decision_Maker__c == '--None--'){
                    validated = false;
                    cmp.set("v.isFinaDecMakName","Select a Final Decision Maker value.");
                    fieldMessages.push("Select a Final Decision Maker value.");
                }else{
                    cmp.set("v.isFinaDecMakName","");
                }
                if(eventFields.Is_International_Travel_Required__c == '' || eventFields.Is_International_Travel_Required__c == '--None--'){
                    validated = false;
                    cmp.set("v.isIntNationalName","Indicate if International Travel is required.");
                    fieldMessages.push("Indicate if International Travel is required.");
                }else{
                    cmp.set("v.isIntNationalName","");
                }
                if(eventFields.Internal_Hire__c == '' || eventFields.Internal_Hire__c == '--None--'){
                    validated = false;
                    cmp.set("v.isInternalName","Indicate if this is an Internal Hire.");
                    fieldMessages.push("Indicate if this is an Internal Hire.");
                }else{
                    cmp.set("v.isInternalName","");
                }
            }
        }
		
        eventFields.AccountId=cmp.find("tekdetailAcctId").get("v.value");
        eventFields.OwnerId =cmp.find("tekOwn").get("v.value");
        event.setParam("fields", eventFields);
        
        if(validated){
            //cmp.set("v.skillRefreshFlag",false);
            //cmp.set("v.jobTitleRefreshFlag",false);
            cmp.set("v.insertFlag",true);
            cmp.set("v.addinsertFlag",false);
            cmp.find('TekEditForm').submit(eventFields); 
            
        }else{
			Validation = true;
            console.log('count '+fieldMessages.length);
            cmp.set("v.editDisable",false);
            cmp.set("v.validationMessagesList",fieldMessages); 
			cmp.set("v.Validation",Validation);
        }
    },
    handleOpportunitySaved :function(cmp, event,helper){
        
        var params = event.getParams();
 		console.log('handleopty Saved----------->'+JSON.stringify(params));
        cmp.set("v.validationMessagesList",null);
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true); 
        cmp.set("v.cancelFlag",true);
        
        var positionRefereshEvt = $A.get('e.c:E_RefreshPositionCard');
        positionRefereshEvt.fire();
        
        
        console.log('opp changed fired');
            var appEvent = $A.get("e.c:RefreshEinsteinCard");
            appEvent.setParams({
              "message" : "Opportunity Updated" });
               appEvent.fire();
   
    },
    preventProcessing : function (cmp,event,helper){
        event.preventDefault();
        event.stopPropagation();
        console.log('Prevent method.....');
    },
    onAccountChange :function (cmp,event,helper){
       var acId=event.getParams().value;
       cmp.find("tekdetailAcctId").set("v.value",acId);
    },
    onOwnerChange : function (cmp,event,helper){
       var owrId=event.getParams().value;
       cmp.find("tekOwn").set("v.value",owrId);
    },
    onProductChange :  function (cmp,event,helper){
      var eventFields = event.getParams();
			 var prodval=eventFields.value;
         if(prodval=='Permanent' ){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",false);
         }else if(prodval=='Contract to Hire'){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",true);
         }else{
             cmp.set("v.cthFlag",true);
              cmp.set("v.permFlag",false);
         }
    },
    handleSectionToggle: function (cmp, event) {
        
    },
	navigateToError: function(component, event, helper){
    	var elmnt = document.getElementById("editpageid");
        window.scrollTo(0,200);
	},
    enterHandler: function(component, event, helper) {
     component.set("v.lookupJTFlag",!component.get("v.lookupJTFlag"));
     component.set("v.loopFlag",true);
    },
    changeJobTitle :function (component, event, helper){
        
        var globalLovId=event.getParams().value;
        var tempRec = component.find("recordLoader");
        tempRec.set("v.recordId",globalLovId[0]);
        tempRec.reloadRecord();
    },
    recordUpdated:function (component, event, helper){
        helper.recordUpdated(component, event, helper);
    },
    buttonHandleCancel : function (cmp,event,helper){
		cmp.set("v.validationMessagesList",null);
		cmp.set("v.editFlag",false);
		cmp.set("v.viewFlag",true);  
		cmp.set("v.cancelFlag",true);
		cmp.set("v.editDisable",false);		        
		cmp.set("v.stageErrorVarName","");
		cmp.set("v.jobTitleErrorVarName","");
		cmp.set("v.hiringManagerErrorVarName","");
		cmp.set("v.accountErrorVarName","");
		cmp.set("v.draftReasonVarName","");
		cmp.set("v.officeErrorVarName","");
		cmp.set("v.plmtTypeErrorVarName","");
		cmp.set("v.streetErrorVarName","");
		cmp.set("v.cityTypeErrorVarName","");
		cmp.set("v.stateErrorVarName","");
		cmp.set("v.countryVarName","");
		cmp.set("v.zipErrorVarName","");
		cmp.set("v.currencyErrorVarName","");
		cmp.set("v.extCommJbDescVarName","");
		cmp.set("v.skillPillValidationError","");
        cmp.set("v.totalPositionsVarName","");
		cmp.set("v.Validation",false);
		cmp.set("v.cancelJTFlag",true);
        cmp.set("v.addFieldMessage",null);
        cmp.set("v.addFieldMessagedMap",null);
		cmp.set("v.tekTGSReqVal",""); 
		event.preventDefault();
    },
    accountUpdated:function (component, event, helper){
        helper.accountUpdated(component, event, helper);
    },
    onTGSChange : function(component, event, helper){
        var TGSReqValue = component.find("tekTGSReq").get("v.value");
        if(TGSReqValue == 'Yes'){
            component.set("v.isTGSTrue", "true");
        }if(TGSReqValue == 'No' || TGSReqValue == '--None--'){
            component.set("v.isTGSTrue", "false");
        }
    },
    doInit : function(component, event, helper) {
	   helper.loadPicklistValues(component, event, helper);
	},
    
    showEditModal : function(component, event, helper) {
        var appEvent = $A.get('e.c:E_PositionModal');
        appEvent.fire();
    },
    
    refereshPositionData : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    
    
})