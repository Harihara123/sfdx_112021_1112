({
	enableInlineEdit :function(cmp, event, helper) {
        cmp.set("v.editFlag",true);
        cmp.set("v.viewFlag",false);  
         var opts = [
                { "class": "slds-input slds-combobox__input ", label: "Staging", value: "Staging", selected: "true" },
                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
        	];
        
        //cmp.find("detailStageName").set("v.options", opts);
        cmp.set("v.stageList", opts);
        
      
        if(event.getSource().getLocalId()!=null || typeof event.getSource().getLocalId()!='undefined'){
            var sId=event.getSource().getLocalId();
            
            var id=sId.substring(0,sId.length-1);
            cmp.set("v.scrollId",id);
             
            var prodType=cmp.find('detailReqPlacementType');
            if(prodType!=null || prodType!='undefined')
            var prodV=prodType.get("v.value");
            
        }
    },
    disableInlineEdit :function(cmp, event, helper) {
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true); 
    },
    recordLoadedEdit :function(cmp, event, helper) {
        console.log('recordLoadedEdit----------->');
        var editLoadedFlag=cmp.get("v.editLoaded");
         var jobDescription;
        if(event.getParams().recordUi.record.fields.Req_Job_Description__c!=null)
        jobDescription=event.getParams().recordUi.record.fields.Req_Job_Description__c.value;
        cmp.set("v.editJobdescription",jobDescription);
        
        var reqVMSAddInfo=event.getParams().recordUi.record.fields.Req_Additional_Information__c.value;
         cmp.set("v.editVMSAdditionalInfo",reqVMSAddInfo);
        
        if(editLoadedFlag==false){
            var legProdVal=event.getParams().recordUi.record.fields.Legacy_Product__c.value;
            cmp.set("v.legacyProductStr",legProdVal);
			
            
            cmp.find("detailAccountId").set("v.value",event.getParams().recordUi.record.fields.AccountId.value);
      		
            //Adding w.r.t Story S-206574
            var oppOpco=event.getParams().recordUi.record.fields.OpCo__c.value;
            if(oppOpco != undefined && oppOpco != null){
				cmp.set("v.oppOpco",oppOpco);
            }
            //End of Adding w.r.t Story S-206574
            
            var jobDescription=event.getParams().recordUi.record.fields.Req_Job_Description__c.value;
            cmp.set("v.editJobdescription",jobDescription);
            
             var prodval=event.getParams().recordUi.record.fields.Req_Product__c.value;
             if(prodval=='Permanent' ){
                 cmp.set("v.permFlag",true);
                  cmp.set("v.cthFlag",false);
             }else{
                 cmp.set("v.cthFlag",true);
                  cmp.set("v.permFlag",false);
             }
             cmp.set("v.editLoaded",true);
            
            
             var opts = [
                { "class": "slds-input slds-combobox__input ", label: "Staging", value: "Staging", selected: "true" },
                { "class": "slds-input slds-combobox__input ", label: "Draft", value: "Draft" },
                { "class": "slds-input slds-combobox__input ", label: "Qualified", value: "Qualified" }
        	];
        
        //cmp.find("detailStageName").set("v.options", opts);
        cmp.set("v.stageList", opts);
            
        }else if(cmp.get("v.cancelFlag")!=null && cmp.get("v.cancelFlag")==true){
             cmp.find("detailAccountId").set("v.value",event.getParams().recordUi.record.fields.AccountId.value);
            cmp.set("v.cancelFlag",false);
        }
        
        
    },
    handleAccountSaved :function(cmp, event,helper){
        
    	var params = event.getParams();
 		console.log('handleAccountSaved----------->'+JSON.stringify(params));
        cmp.set("v.validationMessagesLIST",null);
        cmp.set("v.editFlag",false);
        cmp.set("v.viewFlag",true); 
        cmp.set("v.cancelFlag",true);
   
    },
    handleError : function(cmp,event,helper){
		var params = event.getParams();
        
        var validationMessages=[];
        
         if(params.output!=null && params.output.fieldErrors!=null ){
           
            var stgErrors=params.output.fieldErrors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      var checkFlag=Array.isArray(value);
                      console.log(key + ':' + value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                });
         
        }
        
        if(params.output!=null && params.output.errors!=null ){
           
            var stgErrors=params.output.errors;
            
            Object.keys(stgErrors).forEach(function(key){
                      var value = stgErrors[key];
                      console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
                      validationMessages.push(fmessage);
                });
         
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
        var fieldErrors=event.getParams().error.data.output.fieldErrors;
        Object.keys(fieldErrors).forEach(function(key){
  				      var value = fieldErrors[key];
   					  console.log(key + ':' + value);
                      var checkFlag=Array.isArray(value);
                      var fmessage=(checkFlag==true)?value[0].message:value.message;
            
            		  validationMessages.push(fmessage);
            
			});
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {
            
            var fieldErrors=event.getParams().error.data.output.errors;
            
            Object.keys(fieldErrors).forEach(function(key){
  				  var value = fieldErrors[key];
   					 console.log(key + ':' + value);
                   var checkFlag=Array.isArray(value);
            	   var fmessage=(checkFlag==true)?value[0].message:value.message;
            		  validationMessages.push(fmessage);
            
			});
            
        }
        
        
      
        cmp.set("v.validationMessagesLIST",validationMessages);
 		console.log('handleError----------->'+validationMessages);
        
        console.log("ValidationMessage Retrieve----->"+cmp.get("v.validationMessagesLIST"));
    },
    recordUpdated  : function(cmp,event,helper){
        
        
    },
    recordLoadedView :function(cmp, event, helper) {
        var flag=cmp.get("v.viewLoaded");
        if(flag==false){
            cmp.set("v.viewLoaded",true);
             cmp.set("v.editFlag",false);
       		 cmp.set("v.viewFlag",true); 
             //if(event.getParam("recordUi").record.fields!=null)
             var recordU=event.getParam("recordUi");
             var accId=event.getParam("recordUi").record.fields.AccountId;
             cmp.set("v.accountParam",accId.value);
            
            
             var jobdescription=event.getParam("recordUi").record.fields.Req_Job_Description__c;
             cmp.set("v.editJobdescriptionCancel",jobdescription);
            
            
             var reqVMSAddInfo=event.getParams().recordUi.record.fields.Req_Additional_Information__c.value;
             cmp.set("v.editVMSAdditionalInfo",reqVMSAddInfo);
            
            
            
            var prodval=event.getParams().recordUi.record.fields.Req_Product__c.value;
             if(prodval=='Permanent' ){
                 cmp.set("v.permFlag",true);
                  cmp.set("v.cthFlag",false);
             }else{
                 cmp.set("v.cthFlag",true);
                  cmp.set("v.permFlag",false);
             }
           
            if(recordU.record.fields.LDS_Account_Name__c!=null)
            recordU.record.fields.LDS_Account_Name__c.value=accId.value;
            event.setParam("recordUi",recordU);
            
        }
       
    },
    handleCancel : function (cmp,event,helper){
         cmp.set("v.validationMessagesLIST",null);
         cmp.set("v.editFlag",false);
         cmp.set("v.viewFlag",true);  
         cmp.set("v.cancelFlag",true);
		 cmp.set("v.extDescpErrorVarName","");
         event.preventDefault();
   
    },
    onSubmit  : function(cmp, event, helper) {
   		event.preventDefault();
        var eventFields = event.getParam("fields");
        var prodval=cmp.get("v.legacyProductStr");
        
		var dcmp=cmp.find('detailStageName');
		var plcType=dcmp.get("v.value");
        
        var fieldMessages=[];
        var flag=false;
        eventFields.StageName =plcType;
        eventFields.LDS_VMS__c=true;
        eventFields.AccountId=cmp.find("detailAccountId").get("v.value");
        if(cmp.get("v.editJobdescription")!=null)
        eventFields.Req_Job_Description__c=cmp.get("v.editJobdescription");
        
        
        if(cmp.get("v.editVMSAdditionalInfo")!=null)
        eventFields.Req_Additional_Information__c=cmp.get("v.editVMSAdditionalInfo");

		//Adding w.r.t Story S-206574
		cmp.set("v.jobtitle",eventFields.Req_Job_Title__c);
		cmp.set("v.jobDescription",eventFields.Req_Job_Description__c);
		/*if(cmp.get("v.jobtitle") != null && cmp.get("v.jobDescription") != null && eventFields.StageName != 'Staging' && cmp.get("v.oppOpco") == 'TEKsystems, Inc.'){
			helper.skillSpecialtyClassifierCall(cmp,event);
            if(cmp.get("v.Req_Skill_Specialty__c") != undefined && cmp.get("v.Req_Skill_Specialty__c") != null){
                eventFields.Req_Skill_Specialty__c=cmp.get("v.Req_Skill_Specialty__c");
            }
            if(cmp.get("v.Functional_Non_Functional__c") != undefined && cmp.get("v.Functional_Non_Functional__c") != null){
                eventFields.Functional_Non_Functional__c=cmp.get("v.Functional_Non_Functional__c");
            }
            if(cmp.get("v.Soc_onet__c") != undefined && cmp.get("v.Soc_onet__c") != null){
                eventFields.Soc_onet__c=cmp.get("v.Soc_onet__c");
            }
		}*/
		//End of Adding w.r.t Story S-206574

        var VmsDescription = cmp.find("reqSummaryExtJobDesc");
        if(VmsDescription.get("v.value") != null && VmsDescription.get("v.value") != '' && VmsDescription.get("v.value").length > 500){
            flag = true
            fieldMessages.push("External Communities Job Description cannot be greater than 500 characters.");
            cmp.set("v.extDescpErrorVarName","External Communities Job Description cannot be greater than 500 characters.");
        }else{
            cmp.set("v.extDescpErrorVarName","");
        }
        if(flag){
            cmp.set("v.validationMessagesLIST",fieldMessages);
        }else{
            if(cmp.get("v.jobtitle") != null && cmp.get("v.jobDescription") != null && eventFields.StageName != 'Staging' && cmp.get("v.oppOpco") == 'TEKsystems, Inc.'){
                helper.skillSpecialtyClassifierCall(cmp,event,eventFields);                
            }else{
                event.setParam("fields", eventFields);
            	cmp.find('vmsEditForm').submit(eventFields);
            }            
        }     
    },
     onPlacementTypeChange : function (cmp,event,helper){
    		 var eventFields = event.getParams();
			 var prodval=eventFields.value;
         if(prodval=='Permanent' ){
             cmp.set("v.permFlag",true);
              cmp.set("v.cthFlag",false);
         }else{
             cmp.set("v.cthFlag",true);
              cmp.set("v.permFlag",false);
         }
    },
    dummyMethod : function (cmp,event,helper){
        event.preventDefault();
        event.stopPropagation();
        console.log('dummy method.....');
    },
    onAccountChange :function (cmp,event,helper){
       var acId=event.getParams().value;
       cmp.find("detailAccountId").set("v.value",acId);
    }
    
    
})