({
	showErrorToast : function(cmp,event) {
        console.log('showErrorToast Function');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
                title : 'Error.',
                message: 'Please complete product hierarchy',
                messageTemplate: '',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            
            toastEvent.fire();
            cmp.set("v.editFlag",false);
            cmp.set("v.viewFlag",true);  
            event.preventDefault();
        
    },
	//Adding w.r.t Story S-206574
	skillSpecialtyClassifierCall : function(component,event,eventFields){
		var action = component.get("c.fetchSkillSpecialtyClassifier");
		action.setParams({"jobTitle":component.get("v.jobtitle"),"jobDescription":component.get("v.jobDescription")});
        action.setCallback(this, function(response) {
			var data = response;			
            if (data.getState() == 'SUCCESS'){
				var model = data.getReturnValue();
				this.updateSkillSpecialtyList(component, event, model);
                this.updateFunctionalAndSoconet(component, event, model);
                if(component.get("v.Req_Skill_Specialty__c") != undefined && component.get("v.Req_Skill_Specialty__c") != null){
                	eventFields.Req_Skill_Specialty__c=component.get("v.Req_Skill_Specialty__c");
            	}
            	if(component.get("v.Functional_Non_Functional__c") != undefined && component.get("v.Functional_Non_Functional__c") != null){
                	eventFields.Functional_Non_Functional__c=component.get("v.Functional_Non_Functional__c");
            	}
            	if(component.get("v.Soc_onet__c") != undefined && component.get("v.Soc_onet__c") != null){
                	eventFields.Soc_onet__c=component.get("v.Soc_onet__c");
            	}
				//Adding w.r.t S-208297
                var reqMetrics = component.find('reqMetrics');
        		reqMetrics.mapVMSMetricData();
                reqMetrics.saveMetricDataOnCreate();
                //End of Adding w.r.t S-208297
                
                event.setParam("fields", eventFields);
            	component.find('vmsEditForm').submit(eventFields);
            }
            else{
                event.setParam("fields", eventFields);
            	component.find('vmsEditForm').submit(eventFields);
            }
        });
		$A.enqueueAction(action);
	},
    updateSkillSpecialtyList : function(component, event,model){
		
        var SSCModel = model[3];       
		var parsedSSC = JSON.parse(SSCModel);
		let SSCAPI;
        var SSValuespair={};
        
		parsedSSC.forEach(title => {            
            SSCAPI = title.split(":");
			SSValuespair[SSCAPI[0]]=SSCAPI[1];            
			})
        
        var payloadModel = model[0];
        const parsedSkillSpecialty = JSON.parse(payloadModel);
        let payloadData = parsedSkillSpecialty.payload;
        var skillSpecialtyAPI = [];
        var maxScore=null;
        var maxScoreValue=null;
        var SSApiValues={};
        payloadData.forEach(title => {
            
            SSApiValues[title.tables.score]=title.tables.value;
            if(maxScore==null){
            	maxScore=title.tables.score;
            	maxScoreValue=title.tables.value;
        	}else if(maxScore<=title.tables.score){
            		maxScore=title.tables.score;
            		maxScoreValue=title.tables.value;
        		}
            skillSpecialtyAPI.push({value:SSValuespair[title.tables.value], key:SSValuespair[title.tables.value],selected: title.tables.value ===maxScoreValue});
        	//skillSpecialtyAPI.push({value:title.tables.value, key:title.tables.value,selected: title.tables.value ===maxScoreValue});                 
            })
    	if(skillSpecialtyAPI != []){
			//component.set("v.skillSpecialtyAPIValues",SSApiValues);    	
			//component.set("v.specialityList", skillSpecialtyAPI);
    		component.set("v.Req_Skill_Specialty__c", SSValuespair[maxScoreValue]);
		}   
	},
 	updateFunctionalAndSoconet : function(component, event,model){		
        var payloadModel = model[1];
        const parsedFunctional = JSON.parse(payloadModel);
        let payloadData = parsedFunctional.payload;
        var maxScore=null;
        var maxScoreValue=null;
        var SSApiValues={};
		if(payloadData.length>0){
			payloadData.forEach(title => {            
				SSApiValues[title.tables.score]=title.tables.value;
				if(maxScore==null){
            		maxScore=title.tables.score;
            		maxScoreValue=title.tables.value;
        		}else if(maxScore<=title.tables.score){
            		maxScore=title.tables.score;
            		maxScoreValue=title.tables.value;
        		}
            })	 
			component.set("v.Soc_onet__c", maxScoreValue);    
    
			//FNF values w.r.t custom metadata
			var functionalData = model[2];
			var parsedSOC = JSON.parse(functionalData);
			component.set("v.Functional_Non_Functional__c","Non-Functional");			
			let ScoreValue;
            ScoreValue = maxScoreValue.split("~");
			parsedSOC.forEach(title => {            
            //if(maxScoreValue == title){
				if(ScoreValue[0] == title){
            		component.set("v.Functional_Non_Functional__c","Functional");            		
        		}            
			})
		}       
	}
	//End of Adding w.r.t Story S-206574
})