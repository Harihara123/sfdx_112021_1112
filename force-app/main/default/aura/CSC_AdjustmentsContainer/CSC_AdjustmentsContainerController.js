({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");	
        var action = component.get("c.isAdjustmentCheck");
        action.setParams({
            caseRecId : recordId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() == true){
                    component.set('v.showAdjustment',response.getReturnValue());
                }else{
                    let message = 'Payroll Form not applicable for non payroll related case types.';
                    helper.showToast(component, event, helper, message,'sticky','Info:','error');
                }
            }else if(state === "INCOMPLETE"){
                // do something
            }else if (state === "ERROR"){
                component.set('v.hideArticle', false);
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: " +errors[0].message);
                    }
                }else{
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})