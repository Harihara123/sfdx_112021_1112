({
	doInit: function(cmp, event, helper) {
        debugger;
        var myPageRef = cmp.get("v.pageReference");
        var caseId = myPageRef.state.c__caseId;
        cmp.set("v.caseId", caseId);
        var currentStatus = myPageRef.state.c__status;
        cmp.set("v.currentStatus", currentStatus);
        var caseOwnerId = myPageRef.state.c__ownerId;
        cmp.set("v.caseOwnerId", caseOwnerId);
        //alert(currentStatus);
        cmp.set('v.columns', [
            //{label: 'Case Number', fieldName: 'CaseNumber', type: 'text'},
         	{
                label: 'Case Number', 
                fieldName: 'URL',
                type: 'url', 
                typeAttributes: { 
                    label: {
                        fieldName: 'CaseNumber'
                    },
                    target: '_blank'
                }
            },
            {label: 'Contact Name', fieldName: 'ContactName', type: 'text'},
            {label: 'Peoplesoft Id', fieldName: 'Contact_s_Peoplesoft_ID__c', type: 'text'},
            {label: 'Center Name', fieldName: 'Center_Name__c', type: 'text'},
            {label: 'Owner Name', fieldName: 'OwnerName', type: 'text'},
            {label: 'Status', fieldName: 'Status', type: 'text'}
        ]);
		helper.fetchCurrentCaseStatus(cmp, event, helper,caseId);
        helper.fetchData(cmp, event, helper,caseId);
        helper.fetchPicklistOptions(cmp, event, helper, 'Case', 'Status');
        
    },
    
    updateSelectedText: function (cmp, event,helper) {
        debugger;
        var selectedRows = event.getParam('selectedRows');
        let selectedIds =[];
        cmp.set('v.selectedRowsCount', selectedRows.length);
        for(var i = 0; i < selectedRows.length; i++){
            selectedIds.push(selectedRows[i].Id);
        }
        cmp.set('v.selectedChildcaseIds', selectedIds);
    },
    
    handleStatusChange: function (cmp, event,helper) {
        // show required fields
        var selectedStatus = cmp.get("v.selectedStatus");
        //alert(selectedStatus);
        if(selectedStatus == 'Closed'){
            helper.fetchPicklistOptions(cmp, event, helper,'Case', 'Closed_Reason__c');
        }
        if(selectedStatus == 'On Hold'){
             helper.fetchPicklistOptions(cmp, event, helper,'Case', 'On_Hold_Reason__c');
        }
        helper.clearFieldValues(cmp, event, helper);
       
    },
    handleOnHoldReasonOnChange: function (cmp, event,helper) {
        
    },
    ReturnToRecord : function(component, event, helper) {
        var recordId = component.get("v.caseId");
        window.location.href = '/'+recordId;
    },
    updateCase : function(component, event, helper) {
        var selectedStatus = component.get("v.selectedStatus");
        var currentStatus = component.get("v.currentStatus");
        var selectedChildcaseIds = component.get("v.selectedChildcaseIds");
        var errormessage = false;
       if(selectedChildcaseIds.length == 0){
            helper.showToast(component, event, helper,'Error','Please select at least 1 child case!','sticky','error');
            errormessage = true;
        }else if(selectedStatus == ''){
            helper.showToast(component, event, helper,'Error','Please select a status!','sticky','error');
            errormessage = true;
        }else{
        
            if(selectedStatus == 'Closed'){
                var Comments = component.get("v.CaseResolutionComments");
                var selectClosedReason = component.get("v.selectClosedReason");
                var caseOwnerId = component.get("v.caseOwnerId");
                            
                if(Comments == undefined || (Comments != undefined && Comments.length < 20)){
                    helper.showToast(component, event, helper,'Error','Please provide minimum of 20 characters of comments!','sticky','error');
                    errormessage = true;
                }else if(selectClosedReason == undefined || selectClosedReason == '' || selectClosedReason == null ){
                    helper.showToast(component, event, helper,'Error','Please select why is the case being closed?','sticky','error');
                    errormessage = true;
                }/*else if(caseOwnerId != undefined && caseOwnerId.substring(0,3) == '00G'){
                    helper.showToast(component, event, helper,'Error','Case cannot be closed when case owner is a Queue!','sticky','error'); 
                    errormessage = true;
                }*/
            }
            if(selectedStatus == 'On Hold'){
                var selectedOnHoldReason = component.get("v.selectedOnHoldReason");
                
                if(selectedOnHoldReason == undefined || selectedOnHoldReason == ''){
                    helper.showToast(component, event, helper,'Error','Please select On Hold reason!','sticky','error');
                    errormessage = true;
                }else if(selectedOnHoldReason != undefined && selectedOnHoldReason != '' && selectedOnHoldReason == 'Future Dated Change'  ){
                    var futureDate = component.get("v.futureDate");
                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = today.getFullYear();
                    today = yyyy + '-' + mm + '-' + dd;
                    
                    if(futureDate == undefined || (futureDate != undefined && futureDate <= today)){
                        helper.showToast(component, event, helper,'Error','Please select a future date','sticky','error');
                        errormessage = true;
                    }
                }
             }
         }
        if(!errormessage){
            helper.updateChildCases(component, event, helper);
        }
        
    },
    
})