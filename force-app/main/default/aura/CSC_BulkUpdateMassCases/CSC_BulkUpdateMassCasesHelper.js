({
	fetchData: function (component, event, helper,caseId) {
        var action = component.get("c.getCaseList");
        action.setParams({ caseId : caseId
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                var rows = result.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Contact){
                        row.ContactName = row.Contact.Name;
                    } 
                    if (row.Owner){
                        row.OwnerName = row.Owner.Name;
                    }
                    if (row.CaseNumber){
                    	row['URL'] = '/lightning/r/Case/' + row.Id + '/view';
                    }
                }
                
                component.set("v.data",rows);  
                component.set("v.filteredData",rows);
                component.set('v.showSpinner', false);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchCurrentCaseStatus : function(component, event, helper, caseId){
        var action = component.get("c.getCurrentCaseStatus");
        action.setParams({ 
            caseId : caseId
        });
        action.setCallback(this, function(response){
        	var state = response.getState();
            if(state === "SUCCESS"){
                var row = response.getReturnValue();
                if(row[0]['Client_Account_Name__c'] == undefined){
                    component.set("v.isError", true);
                    component.set("v.errorMessage", 'Bulk Change for Mass Case functionality is only accessible within Parent Cases');
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPicklistOptions : function(component, event, helper,objectName,fieldName) {
        debugger;
        this.showSpinner(component, event, 'caseCloseSpinner');
		var action = component.get('c.getPicklistOptions');
        action.setParams({ objectApiName : objectName,fieldApiName : fieldName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var statusMap = [];
                for(var key in result){
                    statusMap.push({key: key, value: result[key]});
			    }
                if(fieldName == 'Closed_Reason__c'){
                    component.set("v.closedReasonMap", statusMap);
                }
                if(fieldName == 'Status'){
                    component.set("v.caseStatusMap", statusMap);
                }
                if(fieldName == 'On_Hold_Reason__c'){
                    component.set("v.onHoldReasonMap", statusMap);
                }
                
                debugger;
                this.hideSpinner(component, event, 'caseCloseSpinner');
            }
        });
        $A.enqueueAction(action);
	},
    clearFieldValues: function (component, event, helper) {
        component.set("v.selectClosedReason",'');
        component.set("v.CaseResolutionComments",'');
        component.set("v.selectedOnHoldReason",'');
        component.set("v.futureDate",null);
    },
    updateChildCases: function (component, event, helper) {
        this.showSpinner(component, event, 'caseCloseSpinner');
         var selectedChildcaseIds = component.get("v.selectedChildcaseIds");
        var action = component.get("c.saveChildCases");
        action.setParams({ parentCaseId : component.get("v.caseId"),
                          Status : component.get("v.selectedStatus"),
                          closeReason : component.get("v.selectClosedReason"),
                          closecomment : component.get("v.CaseResolutionComments"),
                          onHoldReason : component.get("v.selectedOnHoldReason"),
                          futureDate : component.get("v.futureDate"),
                          childCaseIds : JSON.stringify(selectedChildcaseIds)
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                var updateResult = result.getReturnValue();
                if(updateResult != undefined && updateResult.isSuccess){
                    this.showToast(component, event, helper,'Success', 'All '+selectedChildcaseIds.length + ' selected child cases updated successfully!','sticky','success');
                	 $A.get('e.force:refreshView').fire();
                }else{
                    this.showToast(component, event, helper,'Error',updateResult.errorMessage,'sticky','error');
                    if(updateResult.successCount != 0){
                        this.showToast(component, event, helper,'Success', updateResult.successCount+' of '+selectedChildcaseIds.length + ' child cases updated successfully!','sticky','success');
                    	 $A.get('e.force:refreshView').fire();
                    }
                    if(updateResult.successCount == 0){
                        this.showToast(component, event, helper,'Error','All selected child cases are failed to update','sticky','error');
                    }
                }
                this.hideSpinner(component, event, 'caseCloseSpinner');
               
            }
        });
        $A.enqueueAction(action);
    },
    
    hideSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-hide');
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-show');
            }), 0
        );
    },
    
    showSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-show');
        
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-hide');
            }), 0
        );
    },
    showToast : function(component, event, helper,title, message,mode,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "mode" : mode,
            "type" : type
        });
        toastEvent.fire();
    },
})