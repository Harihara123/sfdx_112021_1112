({
    
    doInit : function(component, event, helper) {
		//console.log(component.get("v.selectedRecordIds"));
        //alert(component.get("v.recordIdFromPage"));
        helper.fetchCaseStatus(component, event, helper);
        helper.fetchClosedReason(component, event, helper);
	},
    
    handleclosedReasonOnChange : function(component, event, helper) {
		//console.log(component.get("v.selectedRecordIds"));
        //alert(component.get("v.recordIdFromPage"));
        
        
	},
    
    CloseCase : function(component, event, helper) {
        var Comments = component.get("v.CaseResolutionComments");
        var selectClosedReason = component.get("v.selectClosedReason");
        var caseOwnerId = component.get("v.caseOwnerId");
        if(Comments == undefined || (Comments != undefined && Comments.length < 20)){
            component.set("v.isError", true);
            component.set("v.errorMessage", 'Please provide minimum of 20 characters of comments!');
        }else{
            if(selectClosedReason == undefined || selectClosedReason == '' || selectClosedReason == null ){
                component.set("v.isError", true);
           		component.set("v.errorMessage", 'Please select why is the case being closed?');
            }else{
                helper.callCloseTheCase(component, event, helper);
            }
            
        }
        
    },
    ReturnToRecord : function(component, event, helper) {
        var recordId = component.get("v.recordIdFromPage");
        window.location.href = '/'+recordId;
    }
    
})