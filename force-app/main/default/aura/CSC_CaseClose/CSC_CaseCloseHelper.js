({
	callCloseTheCase : function(component, event, helper) {
        debugger;
        this.showSpinner(component, event, 'caseCloseSpinner');
		var action = component.get('c.closeTheCase');
        action.setParams({  caseId : component.get("v.recordIdFromPage"),
                          Comments : component.get("v.CaseResolutionComments"),
                          Status : 'Closed',
                          closedReason : component.get("v.selectClosedReason")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result == 'success'){
                    component.set("v.isError", false);
            		component.set("v.errorMessage", 'Case Closed Successfully!');
                    var recordIdToRedirect = component.get("v.recordIdFromPage");
        			window.location.href = '/'+recordIdToRedirect;
                }else{
                    // perfrom logic
                    component.set("v.isError", true);
                    component.set("v.errorMessage", result);
                }
                this.hideSpinner(component, event, 'caseCloseSpinner');
            }
        });
        $A.enqueueAction(action);
	},
    
    fetchCaseStatus : function(component, event, helper) {
        debugger;
        this.showSpinner(component, event, 'caseCloseSpinner');
		var action = component.get('c.getCaseStatus');
        action.setParams({  caseId : component.get("v.recordIdFromPage")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var caseOwnerId = result[0]['OwnerId'];
                var allCasesClosed = result[0]['All_Child_Cases_Closed__c'];
                var clientAccName = result[0]['Client_Account_Name__c'];
                //alert(allCasesClosed);
                //alert(clientAccName);
                component.set("v.caseOwnerId", caseOwnerId);
                if(result[0]['Status'] == 'Closed' ){
                    component.set("v.isError", true);
                    component.set("v.hideForm", true);
            		component.set("v.errorMessage", 'Case is already closed!');
                }
                if(caseOwnerId.substring(0,3) == '00G' ){
                    component.set("v.isError", true);
                    component.set("v.hideForm", true);
            		component.set("v.errorMessage", 'Case cannot be closed when case owner is a Queue!');
                }else if(allCasesClosed == false && clientAccName != undefined){
                    component.set("v.isError", true);
                    component.set("v.hideForm", true);
            		component.set("v.errorMessage", 'Please resolve the related child cases prior to closing a Parent case');
                }
                this.hideSpinner(component, event, 'caseCloseSpinner');
            }
        });
        $A.enqueueAction(action);
	},
    fetchClosedReason : function(component, event, helper) {
        debugger;
        this.showSpinner(component, event, 'caseCloseSpinner');
		var action = component.get('c.getClosedReason');
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var closedReasonMap = [];
                for(var key in result){
                    closedReasonMap.push({key: key, value: result[key]});
                }
                component.set("v.closedReasonMap", closedReasonMap);
                debugger;
                this.hideSpinner(component, event, 'caseCloseSpinner');
            }
        });
        $A.enqueueAction(action);
	},
    hideSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-hide');
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-show');
            }), 0
        );
    },
    
    showSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-show');
        
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-hide');
            }), 0
        );
    },

})