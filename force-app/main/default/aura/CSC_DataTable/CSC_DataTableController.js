({
    init: function (cmp, event, helper) {
        var accId = cmp.get("v.accId");
        
        cmp.set("v.columns", [
            { label: "First Name", field: 'FirstName', sort: true },
            { label: "Last Name", field: 'LastName', sort: true},
            { label: "Physical Location", field: 'Contact_Location__c', sort: true },
            { label: "Phone", field: 'Phone', sort: true, style: 'width:10rem' },
            { label: "Email", field: 'Email', sort: true, style: 'width:20rem' },
            { label: "Peoplesoft ID", field: 'Peoplesoft_ID__c', sort: true, style: 'width:8rem' },
            { label: "Title", field: 'Title', sort: true },
            { label: "Candidate Status", field: 'Candidate_Status__c', sort: true, style: 'width:10rem' }
        ]);
       
        helper.fetchData(cmp, accId);

    },
    
    updateSelectedText: function (cmp, event,helper) {
        
        const selectedState = event.getParam('selected'),
            updatedRows = event.getParam('updatedRows');
        
        let selectedContactIds = cmp.get("v.selectedContactIds");
        if (selectedContactIds.length > 0){
            if (selectedState){
                // For adding pills/checks
                selectedContactIds = [...selectedContactIds, ...helper.getSelectedIds(cmp).filter(item => selectedContactIds.indexOf(item) < 0)];
            } else {
                // For removing pills/checks
                let currentIds = selectedContactIds;
                
                selectedContactIds = currentIds.filter(item => !updatedRows.includes(item));
                
            }
        } else {
            selectedContactIds = helper.getSelectedIds(cmp);
        }
        // Max selected row validation
        const maxAllowedRows = cmp.get('v.maxRowSelection');

        if(selectedContactIds.length > maxAllowedRows){
            //Show error when we reach the max limit
            const errorMsg = 'You can select maximum of '+maxAllowedRows+' talents';
            helper.showToast('Error', errorMsg, 'sticky', 'error');
            //get extra selected ids and uncheck those.
            let extraSelectedIds = [];
            extraSelectedIds = selectedContactIds.slice(maxAllowedRows,selectedContactIds.length);
            // console.log('---extraSelectedIds-----'+extraSelectedIds)
            for(let i =0; i<extraSelectedIds.length;i++){
                cmp.set("v.rowId", extraSelectedIds[i]);
            	cmp.find('dataTable').removeRow();
            }
            selectedContactIds =  selectedContactIds.slice(0,maxAllowedRows);
        }

        cmp.set("v.selectedContactIds", selectedContactIds);
        // cmp.set('v.selectedRowsCount', selectedContactIds.length);
        
        // Generate pills
        const data = cmp.get('v.data');
        const filteredData = data.filter(item => selectedContactIds.includes(item.Id));
        
        const generatePills = filteredData.map((item) => {
            const pill = {};
                                                
            pill.id = item.Id;
            pill.label = `${item.FirstName}, ${item.LastName}`;
            return pill;
        });
        
        cmp.set("v.selectedRowPills", generatePills);   
        var vx = cmp.get("v.method");
        $A.enqueueAction(vx);
    },
    
    onPillDelete: function(cmp, e, h){
        const newPillIds = e.getParam("pills").map(item => item.id);
        const selectedRows = cmp.get("v.selectedContactIds");
        
        const rowDiff = selectedRows.filter(item => !newPillIds.includes(item));
        
        cmp.set("v.rowId", rowDiff[0]);
        cmp.set("v.selectedContactIds", newPillIds);
        
        cmp.find('dataTable').removeRow();
        // cmp.set('v.selectedRowsCount', newPillIds.length);
    },
    
    updateRows: function(cmp, e, h) {
        const getPills = cmp.find("pillBar").getPills();
        const pillIds = getPills.map(item => item.id);
        cmp.set("v.selectedContactIds", pillIds);
    },
    
    filter: function (component, event, helper) {
        const data = component.get("v.data"),
            term = component.get("v.filter");

        helper.filterData(component, data, term)
    },

    filterStatus: function (cmp, e, h) {
        const selectedOption = e.getParam("value"),
            data = cmp.get("v.data");
        h.filterData(cmp, data, selectedOption);
    },

    fireSelectedTalentEvent: function(component, event, helper) {
            var cmpEvent = component.getEvent("dataTableCmpEvent");
            cmpEvent.setParams({
                "selectedRowIds" : component.get('v.selectedContactIds') 
            });
            cmpEvent.fire();    	
        }
    }
);