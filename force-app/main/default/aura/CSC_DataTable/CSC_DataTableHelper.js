({
   
    fetchData: function (component, accountId) {
        var action = component.get("c.getTalentList");
        action.setParams({ accountId : accountId
        });
        action.setCallback(this, function(result){
            var state = result.getState();
           
            if (component.isValid() && state === "SUCCESS"){
              
                var rows = result.getReturnValue();
                component.set("v.data", rows);

                const colSchema = component.get("v.columns");

                const newRows = rows.map(newRow => {
                    const cells = colSchema.map(col => ({value: newRow[col.field] || ''}))
                    
                    return {
                        id: newRow.Id,
                        cells
                    }
                });

                const filterOptions = [{
                    'label': 'All',
                    'value': 'All'
                    },{
                     'label': 'Current',
                     'value' : 'Current'
                    },{
                    'label': 'Former',
                    'value': 'Former'
                    }];

                component.set("v.options", filterOptions);
                
                component.set("v.filteredData", newRows)
                component.set("v.tableData", newRows);

                // component.set("v.data", rows);
                // component.set("v.filteredData", rows);
                component.set('v.showSpinner', false);
            } else {
                component.set('v.showSpinner', false);
                this.showToast('Error', 'Sorry and unexpected error occured, please contact your Admin.', 'dismissible', 'error');
            }
            
        });
        $A.enqueueAction(action);
        
    },

    getSelectedIds: function (cmp) {
        const selectedRows = cmp.find('dataTable').getSelected();
        let ids;

        if(selectedRows){
            ids = selectedRows.map(row => row.id);
        }

        return ids;
    },
    showToast : function(title, message, mode, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "mode" : mode,
            "type" : type
        });
        toastEvent.fire();
    },
    filterData: function(cmp, data, term) {
        let results = data, 
            regex;
        try{
            regex = new RegExp(term, "i");
        }catch(e) {
            //alert(e);
            return false;
        }
        // filter checks each row, constructs new array where function returns true
        // Added by Rajib for search------------------
        if(term !== 'All'){
            results = data.filter(row => regex.test(row.FirstName) || regex.test(row.LastName) || regex.test(row.Email) || regex.test(row.Peoplesoft_ID__c) || regex.test(row.Contact_Location__c) || regex.test(row.Phone) || regex.test(row.Title) || regex.test(row.Candidate_Status__c) || regex.test(row.FirstName +' '+row.LastName));
        }
        
        const colSchema = cmp.get("v.columns");
        const newRows = results.map(newRow => {
            const cells = colSchema.map(col => ({ value: newRow[col.field] || '' }));
            return {
                id: newRow.Id,
                cells
            }

        });

        cmp.set("v.filteredData", newRows);
    }
                        
});