({
	showToast : function(component, event, helper,title, message,mode,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "mode" : mode,
            "type" : type
        });
        toastEvent.fire();
    },
    showError : function(cmp, event, helper, fieldIdToShowerror, errorMsg){
        var fieldId = cmp.find(fieldIdToShowerror);
        $A.util.addClass(fieldId, 'slds-visible');
        helper.showToast(cmp, event, helper,'Error',errorMsg,'sticky','error');
        cmp.set('v.validationError', true);
    },
    fetchRecordTypeDetails : function(component, event, helper) {
        var action = component.get('c.getFsgRecordTypeId');
        action.setParams({  
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                // debugger;
                if(result != null && result != undefined){
                    //alert('CSC Case Page');
                    component.set("v.recordTypeId", result);
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    insertChildCases : function(component, event, helper) {
        var parentCaseId = component.get("v.recordId");
        var selectedTalentIds = component.get("v.selectedTalentIds");
        //alert(selectedTalentIds.length);
        if(selectedTalentIds.length < 2){
            component.set('v.showSpinner', false);
            helper.showError(component, event, helper,'caseDesc','Please select 2 or more talents to Create Mass Case');
        }else{
            component.set('v.showSpinner', true);
            debugger;
            var action = component.get('c.createChildCases');
            action.setParams({  parentCaseId : parentCaseId,
                              TalentIds : JSON.stringify(selectedTalentIds)
                             });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    debugger;
                    if(result != null && result != undefined && result == 'success'){
                        //alert('CSC Case Page');
                        component.set('v.saved', true);
                        component.set('v.showSpinner', false);
                    }else{
                        component.set('v.showSpinner', false);
                        this.showToast(component, event, helper,'Error',result,'sticky','error');
                    }
                }
            });
            $A.enqueueAction(action);
        }
	},
    
})