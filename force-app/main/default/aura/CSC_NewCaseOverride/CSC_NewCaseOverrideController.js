({
	doInit : function(component, event, helper) {
		var recordTypeId = component.get("v.pageReference").state.recordTypeId;
        //alert(component.get("v.recordId"));
        var additionalParams = component.get("v.pageReference").state.additionalParams;
        var contactIdFromUrl
        if(additionalParams != null && additionalParams != undefined){
            contactIdFromUrl = additionalParams.split("&")[0].split("=")[1];
        }
        //component.find("caseContact").set("v.value",'0031k00000T4biTAAR');
        //alert(contactIdFromUrl);
        component.set("v.recordTypeId",recordTypeId);
        helper.fetchRecordTypeDetails(component, event, helper,recordTypeId);
        //contactIdFromUrl = '0031k00000T4biTAAR';
        //alert(contactIdFromUrl);
        if(contactIdFromUrl != undefined){
            component.set("v.contactIdFromUrl", contactIdFromUrl);
            //helper.fetchContactDetails(component, event, helper,contactIdFromUrl);
            var action = component.get('c.getWorksiteAddress');
            action.setParams({  contactId : contactIdFromUrl
                             });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    //alert(result);
                    component.set("v.workSiteError",result);
                }
            });
            $A.enqueueAction(action);
        }
        
        
	},
    
    handleLoad: function(cmp, event, helper) {
        debugger;
        cmp.set('v.showSpinner', false);
        var contactIdFromUrl = cmp.get('v.contactIdFromUrl');
        if(contactIdFromUrl != undefined){
            cmp.find("caseContact").set("v.value",contactIdFromUrl);
        }
    },
    
    handleChange : function(component, event, helper) {        
        var lookupId = event.getParam("value")[0];
        var action = component.get('c.getWorksiteAddress');
        action.setParams({  contactId : lookupId
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.workSiteError",result);
            }
        });
        $A.enqueueAction(action);
    },

    handleSubmit: function(cmp, event, helper) {
        
        //cmp.set('v.disabled', true);
        cmp.set('v.showSpinner', true);
        cmp.set('v.validationError', false);
        /*event.preventDefault();
        var eventFields = event.getParam("fields");
        eventFields["ContactId"] = "0031k00000T4biTAAR";
        event.setParam("fields", eventFields);*/
        var conId = cmp.find("caseContact").get("v.value");
        if(conId == undefined || conId == null || conId == ''){
            helper.showError(cmp, event, helper,'caseContact','Please provide Contact Id');
        }
        var Subject = cmp.find("caseSubject").get("v.value");
        if(Subject == undefined || Subject == null || Subject == ''){
            helper.showError(cmp, event, helper,'caseSubject','Please provide Subject');
        }
        var caseType = cmp.find("caseType").get("v.value");
        if(caseType == undefined || caseType == null || caseType == ''){
            helper.showError(cmp, event, helper,'caseType','Please provide Case Type');
        }
        if(caseType == 'Started'){
            helper.showError(cmp, event, helper,'caseType','Manual \'Started\' case creation restricted, please review selected case types and sub types and make a different selection');
        }
        var caseSubType = cmp.find("caseSubType").get("v.value");
        if(caseSubType == undefined || caseSubType == null || caseSubType == ''){
            helper.showError(cmp, event, helper,'caseSubType','Please provide Sub Type');
        }
        var workSiteError = cmp.get("v.workSiteError");
        if(caseSubType != null && caseSubType != '' && caseSubType == 'Final Hours' && workSiteError){
            helper.showError(cmp, event, helper,'caseSubType','Final hours can only be generated for Talents in the following Worksite state (CA, HI, NV, CO, MO, MA, UT, MN, CT, DC, OR)');
        }
        var caseIssueDescription = cmp.find("caseDesc").get("v.value");
        //alert(caseIssueDescription);
        if(caseIssueDescription == undefined || caseIssueDescription == null || caseIssueDescription == ''){
            helper.showError(cmp, event, helper,'caseDesc','Please provide Case issue description');
        }
        var val1 = cmp.get("v.validation1");
        var val2 = cmp.get("v.validation2");
        var val3 = cmp.get("v.validation3");
        var val4 = cmp.get("v.validation4");
        if(val1){
            var caseOneX_Bill_Rate = cmp.find("caseOneX_Bill_Rate").get("v.value");
            if(caseOneX_Bill_Rate == undefined || caseOneX_Bill_Rate == null || caseOneX_Bill_Rate == ''){
                helper.showError(cmp, event, helper,'caseOneX_Bill_Rate','Please provide Regular Bill Rate');
            }
            var caseTwoX_Bill_Rate = cmp.find("caseTwoX_Bill_Rate").get("v.value");
            if(caseTwoX_Bill_Rate == undefined || caseTwoX_Bill_Rate == null || caseTwoX_Bill_Rate == ''){
                helper.showError(cmp, event, helper,'caseTwoX_Bill_Rate','Please provide Overtime Bill Rate');
            }
            /* Commented by rajib S-174374
             *
            var caseRequired_Date = cmp.find("caseRequired_Date").get("v.value");
            if(caseRequired_Date == undefined || caseRequired_Date == null || caseRequired_Date == ''){
                helper.showError(cmp, event, helper,'caseRequired_Date','Please provide Required Date');
            }*/
            var caseRequested_Date = cmp.find("caseRequested_Date").get("v.value");
            if(caseRequested_Date == undefined || caseRequested_Date == null || caseRequested_Date == ''){
                helper.showError(cmp, event, helper,'caseRequested_Date','Please provide Effective Date');
            }
        }
        if(val2){
            var caseOneX_Pay_Rate = cmp.find("caseOneX_Pay_Rate").get("v.value");
            if(caseOneX_Pay_Rate == undefined || caseOneX_Pay_Rate == null || caseOneX_Pay_Rate == ''){
                helper.showError(cmp, event, helper,'caseOneX_Pay_Rate','Please provide Regular Pay Rate');
            }
            var caseOneX_Bill_Rate2 = cmp.find("caseOneX_Bill_Rate2").get("v.value");
            if(caseOneX_Bill_Rate2 == undefined || caseOneX_Bill_Rate2 == null || caseOneX_Bill_Rate2 == ''){
                helper.showError(cmp, event, helper,'caseOneX_Bill_Rate2','Please provide Regular Bill Rate');
            }
            var caseTwoX_Bill_Rate2 = cmp.find("caseTwoX_Bill_Rate2").get("v.value");
            if(caseTwoX_Bill_Rate2 == undefined || caseTwoX_Bill_Rate2 == null || caseTwoX_Bill_Rate2 == ''){
                helper.showError(cmp, event, helper,'caseTwoX_Bill_Rate2','Please provide Overtime Bill Rate');
            }
            /* Commented by rajib S-174374
             *
            var caseRequired_Date = cmp.find("caseRequired_Date").get("v.value");
            if(caseRequired_Date == undefined || caseRequired_Date == null || caseRequired_Date == ''){
                helper.showError(cmp, event, helper,'caseRequired_Date','Please provide Required Date');
            }*/
            var caseRequested_Date2 = cmp.find("caseRequested_Date2").get("v.value");
            if(caseRequested_Date2 == undefined || caseRequested_Date2 == null || caseRequested_Date2 == ''){
                helper.showError(cmp, event, helper,'caseRequested_Date2','Please provide Effective Date');
            }
        }
        if(val3){
            var caseFinish_Reason = cmp.find("caseFinish_Reason").get("v.value");
            if(caseFinish_Reason == undefined || caseFinish_Reason == null || caseFinish_Reason == ''){
                helper.showError(cmp, event, helper,'caseFinish_Reason','Please Provide Finish Reason');
            }
        }
        if(val4){
            var caseOneX_Pay_Rate3 = cmp.find("caseOneX_Pay_Rate3").get("v.value");
            if(caseOneX_Pay_Rate3 == undefined || caseOneX_Pay_Rate3 == null || caseOneX_Pay_Rate3 == ''){
                helper.showError(cmp, event, helper,'caseOneX_Pay_Rate3','Please provide Regular Pay Rate');
            }
            var caseRequested_Date3 = cmp.find("caseRequested_Date3").get("v.value");
            if(caseRequested_Date3 == undefined || caseRequested_Date3 == null || caseRequested_Date3 == ''){
                helper.showError(cmp, event, helper,'caseRequested_Date3','Please provide Effective Date');
            }
        }
        //cmp.find('myform').submit(eventFields);
        var valerror = cmp.get('v.validationError');
        if(valerror){
            event.preventDefault();
            cmp.set('v.showSpinner', false);
        }
        
    },

    handleError: function(cmp, event, helper) {
        // errors are handled by lightning:inputField and lightning:nessages
        // so this just hides the spinnet
        cmp.set('v.showSpinner', false);
    },

    handleSuccess: function(cmp, event, helper) {
        cmp.find("caseContact").set("v.value",'');
        cmp.set('v.showSpinner', false);
        cmp.set('v.saved', true);
        var payload = event.getParams().response;
        //alert(payload.id);
        cmp.set("v.recordId",payload.id);
		

    },
    
    showRequireFields: function(cmp, event, helper) {
        //alert('change');
        var typeValue = cmp.find("caseType").get("v.value");
        var subTypeValue = cmp.find("caseSubType").get("v.value");
        if((typeValue == 'Billing/Invoices' && subTypeValue == 'Bill Rate Discrepancy') || (typeValue == 'Data Updates' && subTypeValue == 'Bill Rate Change')){
            cmp.set("v.validation1", true);
        }else{
            cmp.set("v.validation1", false);
        }
        if(typeValue == 'Data Updates' && subTypeValue == 'Pay Rate Change'){
            cmp.set("v.validation2", true);
            cmp.set("v.showFileUpload", true);
        }else{
            cmp.set("v.validation2", false);
            cmp.set("v.showFileUpload", false);
        }
        
        if(typeValue == 'Data Updates' && subTypeValue == 'Tax Changes'){
            cmp.set("v.showFileUpload", true);
        }else{
            cmp.set("v.showFileUpload", false);
        }
        
        if(typeValue == 'Payroll' && subTypeValue == 'Final Hours'){
            cmp.set("v.validation3", true);
        }else{
            cmp.set("v.validation3", false);
        }
        
        if(typeValue == 'Payroll' && subTypeValue == 'Pay Rate Error'){
            cmp.set("v.validation4", true);
        }else{
            cmp.set("v.validation4", false);
        }
        
    },
    navigateToRecord : function(component, event, helper) {
        /*component.set('v.saved', false); 
        
        var navEvent = $A.get("e.force:navigateToSObject");
         navEvent.setParams({
              recordId: component.get("v.recordId"),
              slideDevName: "detail"
         });
        navEvent.fire();*/ 
        window.location ='/'+component.get("v.recordId") ;
        
    },
    handleUploadFinished: function (cmp, event) {
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        //alert("Files uploaded : " + uploadedFiles.length);
    },
    
})