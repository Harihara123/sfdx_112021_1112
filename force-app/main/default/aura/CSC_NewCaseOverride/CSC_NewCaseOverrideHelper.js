({
	showToast : function(component, event, helper,title, message,mode,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "mode" : mode,
            "type" : type
        });
        toastEvent.fire();
    },
    showError : function(cmp, event, helper, fieldIdToShowerror, errorMsg){
        var fieldId = cmp.find(fieldIdToShowerror);
        $A.util.addClass(fieldId, 'slds-visible');
        helper.showToast(cmp, event, helper,'Error',errorMsg,'sticky','error');
        cmp.set('v.validationError', true);
    },
    fetchRecordTypeDetails : function(component, event, helper,recordTypeId) {
        var action = component.get('c.getRecordTypeDeatilsById');
        action.setParams({  recTypeId : recordTypeId
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                debugger;
                var recordTypeDeveloperName = '';
                if(result != undefined && result != null){
                    recordTypeDeveloperName = result.DeveloperName;
                }
                if(recordTypeDeveloperName == 'FSG'){
                    //alert('CSC Case Page');
                    component.set("v.isCscRecType", true);
                }else{
                    if(recordTypeDeveloperName == 'CSC_Read_Only'){
                        // stop creation of CSC Read Only Case
                        helper.showToast(component, event, helper,'Error','Please select "Collocated Services Center" Record Type to create a new case!','sticky','error');
                        
                    }else{
                        var createRecordEvent = $A.get("e.force:createRecord");
                        createRecordEvent.setParams({ 
                            "entityApiName": "Case",
                            "recordTypeId": recordTypeId
                        });
                        createRecordEvent.fire();
                    }
                }
            }
        });
        $A.enqueueAction(action);
	},
    
})