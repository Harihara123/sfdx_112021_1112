({
	fetchParentAttachments : function(component) {
		var recordId = component.get("v.recordId");
        var action = component.get("c.getAttachments");
        action.setParams({
            caseRecId : recordId
        });
        action.setCallback(this,function(response){
            //alert('test');
        	var state = response.getState();
            //alert(state);
            if(state === "SUCCESS"){
                //alert(response.getReturnValue());
                //component.set('v.contentDocList', response.getReturnValue());
                if(response.getReturnValue() != '' && response.getReturnValue().length > 0){
                    component.set('v.contentDocList', response.getReturnValue());
                }else
                    component.set('v.hideTable', false);
            }else if(state === "INCOMPLETE"){
                // do something
            }else if (state === "ERROR"){
                component.set('v.hideTable', false);
            	var errors = response.getError();
                 if(errors){
                 	if(errors[0] && errors[0].message){
                    	console.log("Error message: " +errors[0].message);
                    }
                 }else{
                        console.log("Unknown error");
                 }
            }
                
        });
        $A.enqueueAction(action);
	}
})