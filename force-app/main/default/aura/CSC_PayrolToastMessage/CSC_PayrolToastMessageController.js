({
    doInit : function(component, event, helper) {
        var action = component.get('c.fetchCaseInfo');
        //alert(component.get("v.recordId"));
        action.setParams({  processId : component.get("v.recordId")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                //alert(result);
                if(result == true){
                    let message = 'Additional Rejected Reason can be selected in the Case detail page.';
                    helper.showToast(component, event, helper, message,'sticky','Info:','info');
                }
            }
        });
        setTimeout(function() {
            $A.enqueueAction(action);
        }, 1500);
    }    
})