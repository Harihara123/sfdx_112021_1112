({
	doInit : function(component, event, helper) {
        //Check whether records are selected or not
		var selectedRec = component.get("v.selectedRecordIds");
        if(selectedRec.length == 0 ){
            component.set("v.isError", true);
            component.set("v.errorMessage", 'Please select at least one record!');
            //alert('Please select at leat one record!');
            helper.showToast(component, event, helper, 'Error', 'Sticky', 'Please select at leat one record!');
        }else{
            helper.valApprovalRec(component, event, helper);
        }
    }
})