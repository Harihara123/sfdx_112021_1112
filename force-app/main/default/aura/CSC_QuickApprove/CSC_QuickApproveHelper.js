({
    valApprovalRec : function(component, event, helper) {
    	this.showSpinner(component, event, 'quickApproveSpinner');
        var action = component.get('c.validateApprovalRec');
        action.setParams({  listOfselectedCaseIds : component.get("v.selectedRecordIds")  });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result){
                    component.set("v.isError", true);
                    component.set("v.errorMessage", 'One or multiple Records are not pending for approval cases');
                    
                }else{
                    // perfrom logic
                    this.approveCases(component, event,helper);
                }
                this.hideSpinner(component, event, 'quickApproveSpinner');
            }
        });
        $A.enqueueAction(action);
    },
    
    approveCases: function(component, event, helper) {
        debugger;
        this.showSpinner(component, event, 'quickcloseSpinner');
        var action = component.get('c.approveCases');
        action.setParams({ listOfselectedCaseIds : component.get("v.selectedRecordIds") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.successMessage", 'Cases Approved Successfully!');
                component.set("v.isSuccess", true);
            }else{
                component.set("v.isError", true);
                component.set("v.errorMessage", result);
            }
            this.hideSpinner(component, event, 'quickcloseSpinner');
        });
        $A.enqueueAction(action);
    },
    
	hideSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-hide');
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-show');
            }), 0
        );
    },
    showSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-show');
        
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-hide');
            }), 0
        );
    },
    
	showToast : function(component, event, helper, message, title, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "mode": mode,
            "message": message
        });
        toastEvent.fire();
    } 
})