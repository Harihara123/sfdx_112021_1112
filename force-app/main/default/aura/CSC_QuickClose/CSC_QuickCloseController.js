({
	doInit : function(component, event, helper) {
        helper.fetchClosedReason(component, event, helper);
        //Check whether records are selected or not
		var selectedRec = component.get("v.selectedRecordIds");
        if(selectedRec.length == 0 ){
            component.set("v.isError", true);
            component.set("v.hideForm", true);
            component.set("v.errorMessage", 'Please select at least one record!');
            //alert('Please select at leat one record!');
            helper.showToast(component, event, helper, 'Error', 'Sticky', 'Please select at leat one record!');
        }
        
        //get max limit from CVustom lavel
        var maxLimitOfBatch = $A.get("$Label.c.CSC_Max_Limit_for_Quick_Close");
        //validate Max limit
        if(selectedRec.length > parseInt(maxLimitOfBatch)){
            component.set("v.isError", true);
            component.set("v.hideForm", true);
            component.set("v.errorMessage", 'You can select up to '+maxLimitOfBatch + ' records!');
            
        }
        if(component.get("v.isError") == false){
           helper.validateLoggedInUser(component, event, helper); 
        }
	},
    
    CloseStartedCase : function(component, event, helper) {
        var Comments = component.get("v.CaseResolutionComments");
        var selectClosedReason = component.get("v.selectClosedReason");
        var caseOwnerId = component.get("v.caseOwnerId");
        if(Comments == undefined || (Comments != undefined && Comments.length < 20)){
            component.set("v.isError", true);
            component.set("v.hideForm", false);
            component.set("v.errorMessage", 'Please provide minimum of 20 characters of comments!');
        }else{
            if(selectClosedReason == undefined || selectClosedReason == '' || selectClosedReason == null ){
                component.set("v.isError", true);
                component.set("v.hideForm", false);
           		component.set("v.errorMessage", 'Please select why is the case being closed?');
            }else{
                helper.closeME(component, event, helper);
            }
        }
    },
    
    redirectToList : function(component, event, helper) {
        var listViewId = component.get("v.listViewId");
        window.location.href = '/lightning/o/Case/list?filterName='+listViewId;
    }
    
})