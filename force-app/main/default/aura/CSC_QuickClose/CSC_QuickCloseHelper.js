({
    validateLoggedInUser : function(component, event, helper) {
		// query and check whether the current user is part of CSC Case Transfer Group or not
		this.showSpinner(component, event, 'quickcloseSpinner');
        var action = component.get('c.fetchGroupMembers');
        action.setParams({  userId : component.get("v.currentUserId")  });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result){
                    component.set("v.isError", true);
                    component.set("v.hideForm", true);
            		component.set("v.errorMessage", 'You dont have access to perform this operation!');
                    
                }else{
                    // perfrom logic
                    component.set("v.isError", false);
                    //this.validateVSBRecord(component, event,helper);
                }
                this.hideSpinner(component, event, 'quickcloseSpinner');
            }
        });
        $A.enqueueAction(action);
	},
    
    validateVSBRecord : function(component, event, helper) {
    	this.showSpinner(component, event, 'quickcloseSpinner');
        var action = component.get('c.validateVSBRecords');
        action.setParams({  listOfselectedCaseIds : component.get("v.selectedRecordIds")  });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result){
                    component.set("v.isError", true);
                    component.set("v.hideForm", true);
            		component.set("v.errorMessage", 'One or multiple Records are not of Case type Started or Closed!');
                    
                }else{
                    // perfrom logic
                    component.set("v.isError", false);
                }
                this.hideSpinner(component, event, 'quickcloseSpinner');
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchClosedReason : function(component, event, helper) {
        debugger;
        this.showSpinner(component, event, 'quickcloseSpinner');
		var action = component.get('c.getClosedReason');
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var closedReasonMap = [];
                for(var key in result){
                    closedReasonMap.push({key: key, value: result[key]});
                }
                component.set("v.closedReasonMap", closedReasonMap);
                debugger;
                this.hideSpinner(component, event, 'quickcloseSpinner');
            }
        });
        $A.enqueueAction(action);
	},
    
    closeME: function(component, event, helper) {
        debugger;
        this.showSpinner(component, event, 'quickcloseSpinner');
        var action = component.get('c.closeTheCase');
        action.setParams({ listOfselectedCaseIds : component.get("v.selectedRecordIds"),
                          Comments : component.get("v.CaseResolutionComments"),
                          Status : 'Closed',
                          closedReason : component.get("v.selectClosedReason")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result == 'success'){
                    component.set("v.isError", false);
                    component.set("v.isSuccess", true);
                    component.set("v.hideForm", true);
                    component.set("v.successMessage", 'Cases Closed Successfully!');
                    
                }else{
                    // perfrom logic
                    component.set("v.isError", true);
                    component.set("v.errorMessage", result);
                }
                this.hideSpinner(component, event, 'quickcloseSpinner');
            }
        });
        $A.enqueueAction(action);
    },
    
    hideSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-hide');
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-show');
            }), 0
        );
    },
    
    showSpinner: function(component, event, spinnerComponentId) {
        var spinner = component.find(spinnerComponentId);
        $A.util.addClass(spinner, 'slds-show');
        
        window.setTimeout(
            $A.getCallback(function() {
                $A.util.removeClass(spinner, 'slds-hide');
            }), 0
        );
    },
    
	showToast : function(component, event, helper, message, title, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "mode": mode,
            "message": message
        });
        toastEvent.fire();
    } 
})