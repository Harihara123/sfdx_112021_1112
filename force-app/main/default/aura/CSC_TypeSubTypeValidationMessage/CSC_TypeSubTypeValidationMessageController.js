({
	doInit : function(component, event, helper) {
		var action = component.get('c.fetchCaseInfo');
        action.setParams({  caseId : component.get("v.recordId")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result != undefined && result['Type'] == 'Data Updates' && result['Sub_Type__c'] == 'Pay Rate Change'){
                    let message = 'This Case request requires that the Exhibit A document is attached as a file. Also please note, if this rate change requires a pay decrease or the contractor has moved positions, a new employment agreement will be required.';
                    helper.showToast(component, event, helper, message,'sticky','Info:','info');
                }
                if(result != undefined && result['Type'] == 'Data Updates' && result['Sub_Type__c'] == 'Tax Changes'){
                    let message = 'Please attach a W-4 or State Tax form. Ensure that this file does not include a digital signature or a PO Box in the address field.';
                    helper.showToast(component, event, helper, message,'sticky','Info:','info');
                }
                if(result != undefined && result['Type'] == 'Data Updates' && result['Sub_Type__c'] == 'Banking Information'){
                    let message = 'Please attach a statement that will display the new Routing and Account Number, such as Direct Deposit Form, Cash Pay Form, Voided Check or Bank Statement that shows Routing and Account Number.';
                    helper.showToast(component, event, helper, message,'sticky','Info:','info');
                }
                if(result != undefined && result['Type'] == 'Billing/Invoices' && result['Sub_Type__c'] == 'Bill Rate Discrepancy'){
                    let message = 'This Case request requires that the Exhibit A document is attached as a file. In addition, please include rates for all additional pay codes as needed.';
                    helper.showToast(component, event, helper, message,'sticky','Info:','info');
                }
                /*if(result != undefined && result['Is_CSC_Rejected_Case__c'] == true && result['Payroll_Rejected_Reason__c'] == undefined){
                    let message = 'Additional Rejected Reason can be selected in the Case detail page.';
                    helper.showToast(component, event, helper, message,'sticky','Info:','info'); 
                }*/
                if(result != undefined && result['Comments_Updated__c'] == true){
                    helper.updateCase(component, event, helper);
                }
                
            }
        });
       setTimeout(function() {
          $A.enqueueAction(action);
        }, 1500);
	},
    
    recordUpdated: function(component, event, helper) {
        
        var changeType = event.getParams().changeType;
        //alert(changeType);
        if (changeType === "LOADED" || changeType === "CHANGED") { 
            //alert('Changed now');
            var action = component.get('c.fetchCaseInfo');
            action.setParams({  caseId : component.get("v.recordId")
                             });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    if(result != undefined && result['Is_CSC_Rejected_Case__c'] == true && result['Payroll_Rejected_Reason__c'] == undefined){
                        let message = 'Additional Rejected Reason can be selected in the Case detail page.';
                        helper.showToast(component, event, helper, message,'sticky','Info:','info'); 
                    }                
                    $A.get('e.force:refreshView').fire(); 
                }
            });
            setTimeout(function() {
                $A.enqueueAction(action);
            }, 1500);
        }
        
    }
})