({
	showToast : function(component, event, helper, message,mode,title,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "mode" : mode,
            "type" : type
        });
        toastEvent.fire();
    },
    
    updateCase : function(component, event, helper) {
        //alert('hi');
        var action = component.get('c.updateCaseFlag');
        action.setParams({  caseId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
            }
        });
        $A.enqueueAction(action);
    },
})