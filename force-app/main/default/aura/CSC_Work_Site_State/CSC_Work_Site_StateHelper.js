({
	fetchWorkSiteStates : function(component) {
		var recordId = component.get("v.recordId");
        var action = component.get("c.fetchWorkSiteState");
        action.setParams({
            caseRecId : recordId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            var items;
            //alert(state);
            if(state === "SUCCESS"){
                //alert(response.getReturnValue());
                if(response.getReturnValue() != '' && response.getReturnValue().length > 0){
                    component.set('v.worksiteStateList', response.getReturnValue());
                    items = response.getReturnValue();
                    component.set('v.workSiteStateName', items[0].Name);
                }else
                    component.set('v.hideArticle', false);
            }else if(state === "INCOMPLETE"){
                // do something
            }else if (state === "ERROR"){
                component.set('v.hideArticle', false);
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: " +errors[0].message);
                    }
                }else{
                    console.log("Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
	}
})