({
    doInit : function(cmp,event,helper){
        var sPageURL = decodeURIComponent(window.location.href); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('?'); //Split by ? so that you get the key value pairs separately in a list
        sURLVariables = sURLVariables[1];// now the variables contains only the key and values
        sURLVariables = sURLVariables.split('&');
        var sParameterName;
        
        for ( var i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('='); //to split the key from the value.
              if (sParameterName[0] === 'talentId') { //lets say you are looking for param name - firstName
                sParameterName[1] === undefined ? 'Not found' : sParameterName[1];
                cmp.set("v.recordId",sParameterName[1]);
            }
            
            if (sParameterName[0] === 'mode') { //lets say you are looking for param name - firstName
                if(sParameterName[1] === 'edit'){ 
                   cmp.set("v.edit",true);
                }
            }
        }
        
        
        var apexFunction = '';     
        var soql = 'SELECT Id,Name,Talent_Preference_Internal__c FROM Account WHERE Id = %%%parentRecordId%%%'; 
        var recordType = 'HRXML';
        var recordID = cmp.get("v.recordId");
        var params = null;
        if(soql != ""){
                soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
                params = {"soql": soql};
            }
        
        var className = '';
        var subClassName = '';
        var methodName = 'getRecord';


        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordID};
            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }

            cmp.set("v.loadingData",false);
            helper.callServer(cmp,className,subClassName,methodName,function(response){
                if(recordType == "HRXML"){
                    var result = response;
                    
                    if(result){
                        if(result != ''){
                            cmp.set("v.tName",result.Name);
                            try{
                                var ipjson = JSON.parse(result.Talent_Preference_Internal__c);
                                cmp.set("v.record",ipjson );
                                helper.populatePickList(cmp);
                                helper.calculateTotal(cmp);
                                
                            }catch(ex){
                                helper.showError(ex);  
                            }
                            
                        }
                    }
                }else{
                    cmp.set("v.record", response);
                }
            },actionParams,false);
            
        }else{
            cmp.set("v.loading",false);
        }
        helper.callServer(cmp,'','',"getLanguages",function(response){
          if(cmp.isValid()){
            var opt = {};  
            var key = '';
            for(key in response) {     //this iterates over the returned map
                opt[key] = response[key];
            }
            cmp.set("v.languageList", opt);
            helper.populateLanguage(cmp);
            
          }
        },null,true);
        
    },
        rateTypeChange : function (cmp, event, helper) {
        
        cmp.set("v.record.employmentPrefs.desiredRate.rate",cmp.get("v.rateType")); 
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
   
    rateChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
    
    bonusPctChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
    
    bonusChange : function (cmp, event, helper) {
        helper.calculateBonusPct(cmp);
        helper.calculateTotal(cmp);
    },
    
    AddlCompChange : function (cmp, event, helper) {
        helper.calculateTotal(cmp);
    },
    
    editCandidateSummary :function(cmp, event, helper) {
       
       helper.editCandidateSummary(cmp, event);
    },
    viewCandidateSummary :function(cmp, event, helper) {
        helper.savePickList(cmp);
        helper.populateLanguageCode(cmp);
        
        helper.viewCandidateSummary(cmp, event);
    },
    backToTalent : function (cmp, event, helper) {
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": cmp.get("v.recordId")
    });
    navEvt.fire();
}
    
})