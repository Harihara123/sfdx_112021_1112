({  HOURLY_TO_ANNUAL_FACTOR : 2080,
    DAILY_TO_ANNUAL_FACTOR : 260,
    populateCurrencyType : function(component) {
        var params = null;
        this.callServer(component,'ATS','TalentEmploymentFunctions','getCurrencyList',function(response){
            if(component.isValid()){
                
            var desiredCurrency = component.get("v.record.employmentPrefs.desiredCurrency");
            component.set("v.desiredCurrency",desiredCurrency); 
                var CurrLevel = [];
                //CurrLevel.push({value:'', key:'--None--'}); 
                
                var CurrLevelMap = response;
                console.log(CurrLevelMap);
                for ( var key in CurrLevelMap ) {
                    CurrLevel.push({value:key, key:key,selected: key === desiredCurrency});
                    }
                component.set("v.CurrencyList", CurrLevel); 
            }
            
           
            //console.log(component.get("v.CurrencyList"));
            console.log(desiredCurrency +'Akshay'+component.get("v.desiredCurrency"));
            
        },params,false);
        
    },
    editCandidateSummary :function(cmp, event) {
        var recordID = cmp.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          //Neel summer 18 URL change-> "url": "/one/one.app#/n/Candidate_Summary?talentId="+recordID +"&mode=edit",
          "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Candidate_Summary?talentId="+recordID +"&mode=edit",
           "isredirect":true
        });
        urlEvent.fire();
	},
    viewCandidateSummary :function(cmp, event) {
        var recordID = cmp.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          //Neel summer 18 URL change-> "url": "/one/one.app#/n/Candidate_Summary?talentId="+recordID,
          "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Candidate_Summary?talentId="+recordID,
           "isredirect":true
        });
        urlEvent.fire();
	},
    calculateBonusPct:function(cmp) {
        var bonus = (cmp.get("v.record.employmentPrefs.desiredBonus") != null ? cmp.get("v.record.employmentPrefs.desiredBonus") : 0);
            bonus = parseFloat(bonus).toFixed(2);
            //cmp.set("v.record.employmentPrefs.desiredBonus",bonus);
        var paytype = cmp.get("v.record.employmentPrefs.desiredRate.rate");  
        var pay = (cmp.get("v.record.employmentPrefs.desiredRate.amount") != null ? cmp.get("v.record.employmentPrefs.desiredRate.amount") : 0);
        if(paytype != null){
            if(paytype == 'hourly'){
                pay = (pay * this.HOURLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'daily'){
                pay = (pay * this.DAILY_TO_ANNUAL_FACTOR); 
            }
            pay = (parseInt(bonus)/parseInt(pay))*100 ;
            pay = parseFloat(pay).toFixed(2);
            
            cmp.set("v.record.employmentPrefs.desiredBonusPercent",pay);  
        }
        
    },
    calculateBonusAmount:function(cmp) {
        var bonus = (cmp.get("v.record.employmentPrefs.desiredBonusPercent") != null ? cmp.get("v.record.employmentPrefs.desiredBonusPercent") : 0);
            bonus = parseFloat(bonus).toFixed(2);
        var paytype = cmp.get("v.record.employmentPrefs.desiredRate.rate");  
        var pay = (cmp.get("v.record.employmentPrefs.desiredRate.amount") != null ? cmp.get("v.record.employmentPrefs.desiredRate.amount") : 0);
        if(paytype != null){
            if(paytype == 'hourly'){
                pay = (pay * this.HOURLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'daily'){
                pay = (pay * this.DAILY_TO_ANNUAL_FACTOR); 
            }
            pay = (pay * bonus)/100 ;
            
            cmp.set("v.record.employmentPrefs.desiredBonusPercent",bonus);
            cmp.set("v.record.employmentPrefs.desiredBonus",pay);  
        }
        
    },
    calculateTotal:function(cmp) {
        var paytype = cmp.get("v.record.employmentPrefs.desiredRate.rate");  
        var pay = (cmp.get("v.record.employmentPrefs.desiredRate.amount") != null ? cmp.get("v.record.employmentPrefs.desiredRate.amount") : 0);
        var bonus = (cmp.get("v.record.employmentPrefs.desiredBonus") != null ? cmp.get("v.record.employmentPrefs.desiredBonus") : 0);
        var addl = (cmp.get("v.record.employmentPrefs.desiredAdditionalCompensation") != null ? cmp.get("v.record.employmentPrefs.desiredAdditionalCompensation") : 0);   
        if(paytype != null){
            if(paytype == 'hourly'){
                pay = pay * this.HOURLY_TO_ANNUAL_FACTOR; 
            }else if(paytype == 'daily'){
                pay = pay * this.DAILY_TO_ANNUAL_FACTOR; 
            }
            pay = parseFloat(pay) + parseFloat(bonus) + parseFloat(addl);
            
            
            cmp.set("v.totalComp",pay);  
        }
    },
  populateLanguage:function(cmp){
     var currentLang =  [];
     var cLangObj = [];
     var langMap = {};
      if(cmp.get("v.record.qualifications.languages").length > 0){
         currentLang = cmp.get("v.record.qualifications.languages"); 
         langMap = cmp.get("v.languageList"); 
          for(var i = 0;i<currentLang.length;i++){
              cLangObj.push({"key" : currentLang[i],"value" : langMap[currentLang[i]]});
          }
          cmp.set("v.currentLanguages",cLangObj);
      }
      
         
  },
  populateLanguageCode:function(cmp){
     var currentLang =  [];
     var codes = []; 
      if(cmp.get("v.currentLanguages").length > 0){
         currentLang = cmp.get("v.currentLanguages"); 
         for(var i = 0;i<currentLang.length;i++){
              codes.push(currentLang[i].key);
          }
          cmp.set("v.record.qualifications.languages",codes);
      }
      
         
  },
    populatePickList:function(cmp) {
        this.populateCurrencyType(cmp);
        var relocate = cmp.get("v.record.geographicPrefs.willingToRelocate");
        if(relocate  != null){
            if(relocate  == true){
                cmp.set("v.relocate","Yes"); 
            }else{
                cmp.set("v.relocate","No"); 
            }
        }
        var natOpp =  cmp.get("v.record.geographicPrefs.nationalOpportunities");
        if(natOpp  != null){
            if(natOpp  == true){
                cmp.set("v.nationalOpp","Yes"); 
            }else{
                cmp.set("v.nationalOpp","No"); 
            }
        }
        
        var reli = cmp.get("v.record.employabilityInformation.reliableTransportation");
        if(reli  != null){
            if(reli  == true){
                cmp.set("v.reliable","Yes"); 
            }else{
                cmp.set("v.reliable","No"); 
            }
        }
        
        var commuteType = cmp.get("v.record.geographicPrefs.commuteLength.unit");
        if(commuteType != null){
            cmp.set("v.commuteType",commuteType); 
          }
        
        var rateType = cmp.get("v.record.employmentPrefs.desiredRate.rate");
        if(rateType != null){
            cmp.set("v.rateType",rateType); 
          }
        
        var desiredPlacementType = cmp.get("v.record.employmentPrefs.desiredPlacementType");
        if(desiredPlacementType != null){
            cmp.set("v.desiredPlacementType",desiredPlacementType); 
          }
        
        var desiredScheduleType = cmp.get("v.record.employmentPrefs.desiredSchedule");
        if(desiredScheduleType != null){
            cmp.set("v.desiredScheduleType",desiredScheduleType); 
          }
        
        
        
	},
    savePickList:function(cmp) {
        var relocate = cmp.get("v.relocate");
        if(relocate  != null){
            if(relocate  == "Yes"){
                cmp.set("v.record.geographicPrefs.willingToRelocate",true); 
            }else if(relocate  == "No"){
                cmp.set("v.record.geographicPrefs.willingToRelocate",false); 
            }
        }
        var natOpp =  cmp.get("v.nationalOpp");
        if(natOpp  != null){
            if(natOpp == "Yes"){
                cmp.set("v.record.geographicPrefs.nationalOpportunities",true); 
            }else if(natOpp == "No"){
                cmp.set("v.record.geographicPrefs.nationalOpportunities",false); 
            }
        }
        
        var reli = cmp.get("v.reliable");
        if(reli  != null){
            if(reli  == "Yes"){
                cmp.set("v.record.employabilityInformation.reliableTransportation",true); 
            }else if(reli  == "No"){
                cmp.set("v.record.employabilityInformation.reliableTransportation",false); 
            }
        }
        
        var commuteType = cmp.get("v.commuteType");
        if(commuteType != ""){
           cmp.set("v.record.geographicPrefs.commuteLength.unit",commuteType); 
        }
        
        var rateType = cmp.get("v.rateType");
        if(rateType != ""){
            cmp.set("v.record.employmentPrefs.desiredRate.rate",rateType); 
          }
        
        var desiredCurrency = cmp.get("v.desiredCurrency");
        if(desiredCurrency != ""){
            cmp.set("v.record.employmentPrefs.desiredCurrency",desiredCurrency); 
          }
        
        var desiredPlacementType = cmp.get("v.desiredPlacementType");
        if(desiredPlacementType != ""){
            cmp.set("v.record.employmentPrefs.desiredPlacementType",desiredPlacementType); 
          }
        
        var desiredScheduleType = cmp.get("v.desiredScheduleType");
        if(desiredScheduleType != ""){
            cmp.set("v.record.employmentPrefs.desiredSchedule",desiredScheduleType); 
          }
        
	},
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    callServer : function(component, className, subClassName, methodName, callback, params, storable, errCallback) {
        var serverMethod = 'c.performServerCall';
        var actionParams = {'className':className,
                            'subClassName':subClassName,
                            'methodName':methodName.replace('c.',''),
                            'parameters': params};


        var action = component.get(serverMethod);
        action.setParams(actionParams);

        action.setBackground();
        
        if(storable){
            action.setStorable();
        }else{
            action.setStorable({"ignoreExisting":"true"});
        }
      
        action.setCallback(this,function(response) {
            var state = response.getState();
             if (state === "SUCCESS") { 
                // pass returned value to callback function
                callback.call(this, response.getReturnValue());   
                
            } else if (state === "ERROR") {
                // Use error callback if available
                if (typeof errCallback !== "undefined") {
                    errCallback.call(this, response.getReturnValue());
                    // return;
                }

                // Fall back to generic error handler
                var errors = response.getError();
                if (errors) {
                    console.log("Errors", errors);
                    if (errors[0] && errors[0].message) {
                        this.showError(errors[0].message);
                        //throw new Error("Error" + errors[0].message);
                    }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                        this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                    }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                        this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                    }else{
                        this.showError('An unexpected error has occured. Please try again!');
                    }
                } else {
                    this.showError(errors[0].message);
                    //throw new Error("Unknown Error");
                }
            }
           
        });
    
        $A.enqueueAction(action);
    }
    
})