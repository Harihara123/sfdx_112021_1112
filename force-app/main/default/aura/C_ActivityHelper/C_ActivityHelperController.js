({
    showModalFromLwc : function(component, event, helper) {
        var talentId = event.getParams('talentId').talentId;
        component.set('v.talentId', talentId);

        helper.showModal(component, helper);
    }
})
