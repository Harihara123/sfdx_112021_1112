({
    showModal : function(cmp, helper) {
        var params = {
            "recordId" : cmp.get("v.talentId"),
            "activityType" : cmp.get("v.activityType"),
            "eventOrigin" : "allocatedwidget"
        };
        
        helper.createComponent(cmp, 'c:C_TalentActivityAddEditModal', 'v.C_TalentActivityAddEditModal', params);
    },

    createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
			componentName,
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					cmp.set(targetAttributeName, newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			}
		);	
    },
})
