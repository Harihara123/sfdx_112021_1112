({
	doInit : function(component, event, helper) {
		 helper.initialize(component);
	},
    submitSearchAction : function (component, event, helper) {
        helper.addToCallSheet(component,event, helper);
    },
    closeModalBox : function (component, event, helper) {
        
        var accid=component.get("v.accountId");
        component.set("v.errorFlag",false); 
        component.set("v.successFlag",false);
        component.set("v.processFlag",false);
        var urlre='https://'+window.location.hostname+'/lightning/r/Account/'+accid+'/view';
        console.log('URL is---->'+urlre);
        window.location=urlre;
        //helper.closeModalBox(component);    
    }
})