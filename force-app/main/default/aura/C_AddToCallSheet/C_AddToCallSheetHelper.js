({
	initialize: function(component) {
        try {
            var action=component.get("c.initialize");
            var conIds=component.get("v.selectedRecords");
            action.setParams({"recIdList":  conIds});
            action.setCallback(this, function(a) {
                var state = a.getState();
                if (state === "SUCCESS") {
                var model = a.getReturnValue();
                
                component.set("v.contactListId",model.contactIdList);
                component.set("v.accountId",model.accountId);
                	
                var typeArray = [];
                var typeMap = model.typeMapping;
                for ( var key in typeMap) {
                    typeArray.push({value:typeMap[key], key:key});
                }
                component.set("v.typeList", typeArray);
                
                
                var statusArray = [];
                var statusMap = model.statusMapping;
                for ( var key in statusMap) {
                    statusArray.push({value:statusMap[key], key:key});
                }
                component.set("v.statusList",statusArray);
                
                
                
                var priorityArray = [];
                var priorityMap = model.priorityMapping;
                for ( var key in priorityMap) {
                    priorityArray.push({value:priorityMap[key], key:key});
                }
                component.set("v.priorityList",priorityArray);
                
                component.set("v.tsk",model.taskObj);
                if(component.get("v.searchKeyWord")!=null)
                component.set("v.tsk.subject",component.get("v.searchKeyWord"));
                
               
                component.set("v.tsk.subject",model.taskObj.Subject);
                    
                    
                component.set("v.tsk.ActivityDate",model.taskObj.ActivityDate);
                    
                component.set("v.tsk.Type",model.taskObj.Type);
                component.set("v.tsk.Status",model.taskObj.Status);
                component.set("v.tsk.Priority",model.taskObj.Priority);

                component.set("v.tsk.TransactionID__c",component.get("v.transactionId"));
                component.set("v.tsk.RequestID__c",component.get("v.requestId"));
                    
                                	
               }
                else if (state === "ERROR") {
                  
                }
            });
            $A.enqueueAction(action);
        }
        catch(e) {
            
        }
    }, 
    validate : function(component,tsk) {
     var sflag=true;
     var oflag=true;
     
       try {
    	   	var searchWord=component.get("v.tsk.subject");
    	   	if(searchWord==null || searchWord=='' ||typeof searchWord==undefined){
    	   		sflag=false;
    	   		component.set("v.subjectValMessage","Subject is required.");
    	   	}else{
    	   		component.set("v.subjectValMessage","");
    	   	}
           
            var dueDate=component.get("v.tsk.ActivityDate");
           	if(dueDate==null || dueDate=='' ||typeof dueDate==undefined){
    	   		sflag=false;
    	   		component.set("v.dueDateValMessage","Due Date is required.");
    	   	}else{
    	   		component.set("v.dueDateValMessage","");
    	   	}
           
          
          return (sflag);
        }catch(e) {
            
        }
    },
    addToCallSheet : function(component,event, helper) {
        try {
      
            var tsk=component.get("v.tsk");
            var flag=this.validate(component,tsk);
            var accid=component.get("v.accountId");
            if(flag){
              
                component.set("v.proccessFlag",true);
            	//this.doInfoCallSheetActions();
            	var rows=component.get("v.selectedRecords");
	            var action = component.get("c.saveCandidateToCallSheet");
	            var tsky = JSON.stringify(tsk);
	            action.setParams({ 
	                recordIdCSV : rows,
	                tasks :tsky
	            });
	         //   component.set("v.showModal",!component.get("v.showModal")); 
	            action.setCallback(this, function(a) {
	                var state = a.getState();
             
	                if (state === "SUCCESS") {
	                    this.doSuccessCallSheetActions(component);
                        var urlre='https://'+window.location.hostname+'/lightning/r/Account/'+accid+'/view';
						window.location=urlre;
	                }
	                else if (state === "ERROR") {
                
	                    this.doErrorCallSheetActions(a);
	                }
	            });
	            
	            $A.enqueueAction(action);
            }
        }
        catch(e) {
            
        }
    },
    doSuccessCallSheetActions : function(component) {
       
       component.set("v.successFlag",true);
       component.set("v.errorFlag",false);
        
	},
    doErrorCallSheetActions : function(a) {
        component.set("v.errorFlag",true);
        component.set("v.successFlag",false);
    },
    
    doInfoCallSheetActions : function() {
       component.set("v.proccessFlag",true);
       
    }
})