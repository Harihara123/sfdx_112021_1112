({
	doInit: function (component, event, helper) {
        var today = new Date().toISOString();
        component.set('v.dueDate', today);

        var subject = component.get('v.subject');

        if(!$A.util.isEmpty(subject)) {
            component.set('v.buttonDisabled', false);
        }


        helper.getModalValues(component);
	},
    closeModal: function (component, event, helper) {
        helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
        helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
		component.getEvent("statusCanceledEvt").fire();//added by akshay for job posting 11/9/2018  S-106132
    },
    addToCallSheet: function(component, event, helper) {
        helper.createCallSheetRecords(component, helper);
    },
    validateSubject: function (component, event, helper) {
        var buttonDisabled = component.get('v.buttonDisabled');
        var subject = component.get('v.subject');

        buttonDisabled = $A.util.isEmpty(subject);

        component.set('v.buttonDisabled', buttonDisabled);
    }
})