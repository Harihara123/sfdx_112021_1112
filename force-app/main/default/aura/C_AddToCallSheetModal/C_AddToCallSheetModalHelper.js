({
	toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    createCallSheetRecords: function(component, helper) {
        var dueDate = component.get('v.dueDate');

        var detailsMap = {
            "ActivityDate":dueDate,
            "Status":component.get('v.status'),
            "Priority":component.get('v.priority'),
            "Subject":component.get('v.subject'),
            "Type":component.get('v.type')
        };

        var contactList = component.get('v.contacts');

        var action = component.get('c.addContactsToCallSheet');
        action.setParams({contacts:contactList,callSheetDetails:detailsMap});

        action.setCallback(this, function(response) {
            var status = response.getState();

            var toastEvent = $A.get("e.force:showToast");

            helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
            helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');

            if(status === 'SUCCESS') {
                toastEvent.setParams({
                    "type":"success",
                    "title":"Success",
                    "message":"Contacts successfully added to Call Sheet."
                });             
            } else {
                toastEvent.setParams({
                    "type":"error",
                    "title":"error",
                    "message":"Unable to add Contacts to Call Sheet."
                });
            }
			
		    toastEvent.fire();
			component.getEvent("statusCanceledEvt").fire();//added by akshay for job posting 11/9/2018  S-106132
        });

        $A.enqueueAction(action);
    },
    getModalValues: function (component) {
        var action = component.get('c.getPicklistValues');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var model = response.getReturnValue();

                var typeArray = [];
                var typeMap = model.typeMapping;
                for ( var key in typeMap) {
                    typeArray.push({value:typeMap[key], key:key});
                }
                component.set("v.typeList", typeArray);
                
                
                var statusArray = [];
                var statusMap = model.statusMapping;
                for ( var key in statusMap) {
                    statusArray.push({value:statusMap[key], key:key});
                }
                component.set("v.statusList",statusArray);
                
                
                
                var priorityArray = [];
                var priorityMap = model.priorityMapping;
                for ( var key in priorityMap) {
                    priorityArray.push({value:priorityMap[key], key:key});
                }
                component.set("v.priorityList",priorityArray);
            }
        });

        $A.enqueueAction(action);
    }
})