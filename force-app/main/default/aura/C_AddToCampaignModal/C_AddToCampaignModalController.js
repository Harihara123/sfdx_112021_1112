({
	closeModal: function (component, event, helper) {
        helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
        helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
    },
    addToCampaign: function (component, event, helper) {
        helper.addToCampaign(component, event, helper);
    },
    addAllToCampaign: function (component, event, helper) {
        helper.addAllToCampaign(component, event, helper);
    }
})