({
	toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    addToCampaign: function(component,event,helper) {
        var campaignId = component.get('v.selectedCampaign');
        var contactList = component.get('v.contacts');
		var cType = component.get('v.cType');

        var action = component.get('c.addContactToCampaign');
        action.setParams({
            contacts:contactList,
            campaignId:campaignId
        });

        let ref = this;

        action.setCallback(this, function(response) {
            var status = response.getState();

            let toaster;

            helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
            helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');

            if(status === 'SUCCESS') {
                toaster = helper.setToast('success', 'Success', 'Contacts added to the Campaign.');   
                toaster.fire();  
				
				//fire an event to disable capmapign button on seachresultActions Component
				if(cType == 'C') {

					var disableButtonEvent = $A.get("e.c:E_DisableSearchActionButtons");
					disableButtonEvent.setParams({ "rowsSelectedCount" : 0 });
					disableButtonEvent.setParams({ "rowsSelected" : [] });
					//for unchecking checkboxes
					disableButtonEvent.setParams({ "contacts" : contactList });
				
					disableButtonEvent.fire();   
				
				}     
            } else {
                let errors = response.getError();
                let errorStatus = errors[0].pageErrors[0].statusCode;

                let toaster;

                if(errorStatus === 'DUPLICATE_VALUE') {
                    toaster = helper.setToast('warning','Duplicate Value','These Contacts have already been added to the campaign.');
                } else {
                    toaster = helper.setToast('error', 'Error', 'Unable to add Contacts to the campaign.');
                }

               
                toaster.fire();
            }

        });

        $A.enqueueAction(action);
    },
    addAllToCampaign: function(component,event,helper) {
        var campaignId = component.get('v.selectedCampaign');
       // var addAllIndicator = component.get('v.allAllIndicator');
		var cType = component.get('v.cType');
        var queryString = component.get('v.queryString');
        var action = component.get('c.addAllContactsToCampaign');
        action.setParams({
            campaignId:campaignId,
            queryString:queryString
        });

        let ref = this;

        action.setCallback(this, function(response) {
            var status = response.getState();

            let toaster;

            helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
            helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');

            if(status === 'SUCCESS') {
                toaster = helper.setToast('success', 'Success', 'Contacts added to the Campaign.');   
                toaster.fire();  
				
				//fire an event to disable capmapign button on seachresultActions Component
				/*if(cType == 'C') {

					var disableButtonEvent = $A.get("e.c:E_DisableSearchActionButtons");
					disableButtonEvent.setParams({ "rowsSelectedCount" : 0 });
					disableButtonEvent.setParams({ "rowsSelected" : [] });
					//for unchecking checkboxes
					disableButtonEvent.setParams({ "contacts" : contactList });
				
					disableButtonEvent.fire();   
				
				} */    
            } else {
                let errors = response.getError();
                let errorStatus = errors[0].pageErrors[0].statusCode;

                let toaster;

                if(errorStatus === 'DUPLICATE_VALUE') {
                    toaster = helper.setToast('warning','Duplicate Value','These Contacts have already been added to the campaign.');
                } else {
                    toaster = helper.setToast('error', 'Error', 'Unable to add Contacts to the campaign.');
                }

               
                toaster.fire();
            }

        });

        $A.enqueueAction(action);
    },
    setToast: function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "type":type,
            "title":title,
            "message":message
        });

        return toastEvent;
    }
})