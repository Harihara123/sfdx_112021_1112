({
    doInit : function(cmp,event,helper){
        var recordID = cmp.get("v.recordId");
		//console.log(recordID); 
		helper.populateRecordTypeName(cmp);
        if(typeof recordID !== 'undefined'){
            if(recordID.length == 18){
                helper.populateContactLists(cmp); 
                helper.populateCurrentUser(cmp); 
				helper.getTagsCount(cmp);                
            }else{
               helper.showError('Invalid Contact Id.','Error: '); 
            }
           
        }else{
			helper.showError('Invalid Contact Id.','Error: '); 
        }      
    },
    saveChanges :function(cmp, event, helper) {
		event.getSource().set("v.disabled",true);
		helper.saveAddtoList(cmp);      
    },
    killComponent :function(cmp, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();   
		helper.backToSource(cmp,'Client');      
    },
    cancelChanges :function(cmp, event, helper) {
	    var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();         
        helper.backToSource(cmp,'Client');      
    }
})