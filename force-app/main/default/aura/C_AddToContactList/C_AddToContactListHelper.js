({ 
	saveAddtoList :function(cmp) {
		var self = this;

        this.showSpinner(cmp);		
		var clist =  cmp.get("v.contactLists");
		var cInitMap = cmp.get("v.cMapInit");
		var recordType = cmp.get("v.recordType");
		var cMapNew = {};
		for(var i=0;i<clist.length;i++){
			if(typeof clist[i] !== "undefined"){
				if(typeof cInitMap[clist[i].key] === "undefined"){
					cMapNew[clist[i].key] = clist[i].value;
				} 
			}
		}
        
        
		if(Object.keys(cMapNew).length > 0){
			var params;  
			params = {
				'recordId' : cmp.get("v.recordId"),
				'clist' : cMapNew, 
                'requestId': cmp.get("v.requestId"),
                'transactionId' : cmp.get("v.transactionId")
                
			};
			this.callServer(cmp,'ATS','ContactListFunctions',"saveContactLists",function(response){
               
				if(cmp.isValid()){
					var opt = [];
					var opt2 = cmp.get("v.cListInit");
					var initMap = cmp.get("v.cMapInit");
					var key = '';
					for(var i=0; i<=response.length;i++) {     //this iterates over the returned map
						if(typeof response[i] !== "undefined"){
							opt.push( response[i].Name);
							opt2.push({"key":response[i].Id,
										"value":response[i].Name,
										"showPill":false});
							initMap[response[i].Id] = response[i].Name;
						}
                        
                       
					}
                    
					cmp.set("v.contactLists", opt2);//base list used as keys to populate map.
					cmp.set("v.cMapInit", initMap);//to send to backend to create new lists
					cmp.set("v.cListInit", opt2);//to populate existing badges, this is not bound with lookup pillbar
                    
					var toastEvent = $A.get("e.force:showToast");
					var message = "The talent has been added to: ";
					if(recordType === 'Client') {
						message = "The client has been added to: "
					}
					toastEvent.setParams({
						"type": "success",
						"title": "Success!",
						"message": message + opt 
					});
					toastEvent.fire();
					this.hideSpinner(cmp);
                    if(recordType === 'No record type name') {
                        self.backToSource(cmp,recordType);
                    } else {
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();                    
                        window.setTimeout(
                            $A.getCallback(function() {
                                self.backToSource(cmp,recordType);
                            }), 7000
                        );
                    }
				}
			},params,true); 

		}else{
			var toastEvent = $A.get("e.force:showToast");
			var message = "No new candidate lists have been added";
			if(recordType === 'Client') {
				message = "No new client lists have been added"
			}
			toastEvent.setParams({
				"type": "error",
				"title": "Error",
				"message": message
			});
			toastEvent.fire(); 
			this.hideSpinner(cmp);
			cmp.find("saveId").set("v.disabled",false);
		}
	},    
	backToSource : function (cmp,recordType ) {
		if(recordType === 'Talent' ) {  
			//events being fired from a talent record
            var recordId = cmp.get("v.recordId") ;        
			var transactionId = cmp.get("v.transactionId");
			var requestId = cmp.get("v.requestId");
            var refreshAppEvent = $A.get('e.c:E_RefreshChild');
			refreshAppEvent.fire();
            var params = {recordId: recordId};
            var createComponentEvent = cmp.getEvent('createCmpEventModal');
            createComponentEvent.setParams({
                    "componentName" : 'c:C_AddToContactListModal'
                    , "params":params
                    ,"targetAttributeName": 'v.C_AddToContactListModal' });
            createComponentEvent.fire();
			//events being fired from a search page 
			var createComponentEvent2 = cmp.getEvent('closeCmpEventContactList');
			var params2 = {recordId: recordId,                 
				  requestId: requestId,
                  transactionId: transactionId};
            createComponentEvent2.setParams({
                    "componentName" : 'c:C_AddToContactListModal'
                    , "params":params2
                    ,"targetAttributeName": 'v.C_AddToContactListModal' });
            createComponentEvent2.fire();
        } else if(recordType === 'Client'){
            var refreshAppEvent = $A.get('e.c:E_RefreshChild');
			refreshAppEvent.fire();
            $A.get('e.force:refreshView').fire();
        } 
	},    
	getTagsCount:function(cmp){
        var params;
        params = {};
        this.callServer(cmp,'ATS','ContactListFunctions',"getTagNamesCount",function(response){
            if(cmp.isValid()){
                cmp.set("v.tagNamesCount", response);				              
            }
        },params,true); 
    },
	populateRecordTypeName:function(cmp) {
		var params;
		var recordId = cmp.get("v.recordId");
		var objName = 'Contact';
		if(typeof recordId !== 'undefined' && recordId.substring(0, 3) === '001'){
			objName = 'Account';
		}
		params = {
					'recordId' : recordId,
					'objName' : objName          
				};
		this.callServer(cmp,'','',"getRecordTypeName",function(response){
			if(cmp.isValid()){
				if(response !== 'No record type name') {
					cmp.set("v.recordType",response);
				} else {
					cmp.set("v.recordType",'Talent');
				}				
			}
		},params,true);   
	}, 
	populateCurrentUser:function(cmp){
		var userId = $A.get("$SObjectType.CurrentUser.Id");
		cmp.set("v.userId",userId);   
	},
	populateContactLists:function(cmp){
		var params;
		params = {
				'recordId' : cmp.get("v.recordId")          
			};
		this.callServer(cmp,'ATS','ContactListFunctions',"getContactLists",function(response){
			if(cmp.isValid() && (response.length > 0) ){
			var opt = []; 
				var initMap = {};
				var key = '';
				for(var i=0; i<=response.length;i++) {     //this iterates over the returned map
					if(typeof response[i] !== "undefined"){ 
						opt.push({"key":response[i].Tag_Definition__r.Id,
								"value":response[i].Tag_Definition__r.Tag_Name__c,
								"displayName" : response[i].Tag_Definition__r.Tag_Name__c + ' (' +response[i].Tag_Definition__r.Candidate_Count__c + ')',
								"showPill":false});
						initMap[response[i].Tag_Definition__r.Id] = response[i].Tag_Definition__r.Tag_Name__c;
					}
                  
				}
				cmp.set("v.contactLists", opt);//base list used as keys to populate map.
				cmp.set("v.cMapInit", initMap);//to send to backend to create new lists
				cmp.set("v.cListInit", opt);//to populate existing badges, this is not bound with lookup pillbar
              
			}
		},params,false);  
	},
	showError : function(errorMessage, title){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			mode: 'dismissible',
			title: title,
			message: errorMessage,
			type: 'error'
		});
		toastEvent.fire();
	},
	callServer : function(component, className, subClassName, methodName, callback, params, storable, errCallback) {
		var serverMethod = 'c.performServerCall';
		var actionParams = {'className':className,
							'subClassName':subClassName,
							'methodName':methodName.replace('c.',''),
							'parameters': params};


		var action = component.get(serverMethod);
		action.setParams(actionParams);

		action.setBackground();
        
		if(storable){
			action.setStorable();
		}else{
			action.setStorable({"ignoreExisting":"true"});
		}
      
		action.setCallback(this,function(response) {
			var state = response.getState();
				if (state === "SUCCESS") { 
				// pass returned value to callback function
				callback.call(this, response.getReturnValue());   
                
			} else if (state === "ERROR") {
				// Use error callback if available
				if (typeof errCallback !== "undefined") {
					errCallback.call(this, response.getReturnValue());
					// return;
				}

				// Fall back to generic error handler
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						this.showError(errors[0].message);
						//throw new Error("Error" + errors[0].message);
					}else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
						this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
					}else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
						this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
					}else{
						this.showError('An unexpected error has occured. Please try again!');
					}
				} else {
					this.showError(errors[0].message);
					//throw new Error("Unknown Error");
				}
			}
           
		});
    
		$A.enqueueAction(action);
	},
	// automatically call when the component is done waiting for a response to a server request.  
    hideSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
     },
	// automatically call when the component is waiting for a response to a server request.
     showSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
     }

})
