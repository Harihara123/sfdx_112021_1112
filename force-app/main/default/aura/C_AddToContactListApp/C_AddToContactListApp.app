<aura:application access="GLOBAL" extends="ltng:outApp"> 

   <aura:dependency resource="c:C_MassAddToContactList"/>
   <aura:dependency resource="markup://force:showToast" type="EVENT"/>
   <aura:dependency resource="markup://force:navigateToList" type="EVENT"/>  
</aura:application>