({
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },	    
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },

    backToSource : function (cmp) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();      
        var recordId = cmp.get("v.recordId") ;      
		var transactionId = cmp.get("v.transactionId");
		var requestId = cmp.get("v.requestId");
        var params = {recordId: recordId};
        var createComponentEvent = cmp.getEvent('createCmpEventModal');
        createComponentEvent.setParams({
                "componentName" : 'c:C_AddToContactListModal'
                , "params":params
                ,"targetAttributeName": 'v.C_AddToContactListModal' });
        createComponentEvent.fire();
		var createComponentEvent2 = cmp.getEvent('closeCmpEventContactList');
			var params2 = {recordId: recordId,                 
				  requestId: requestId,
                  transactionId: transactionId};
            createComponentEvent2.setParams({
                    "componentName" : 'c:C_AddToContactListModal'
                    , "params":params2
                    ,"targetAttributeName": 'v.C_AddToContactListModal' });
            createComponentEvent2.fire();
    },
})