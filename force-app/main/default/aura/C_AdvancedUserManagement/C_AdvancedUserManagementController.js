({
    showModal : function(cmp, e, h) {
        const detail = e.getParams()       

        var modalBody;
        const lwc = cmp.find('lwc');        
        $A.createComponents([
            ["c:lwcUserDetails",{
                values: detail,
                modalPromise: cmp.getReference('v.modalPromise'),
                userMethod: lwc.handleUserAction,
                save: lwc.saveUser
            }]
        ],
        function(components, status){
            if (status === "SUCCESS") {
                modalBody = components[0];                
                let modalPromise = cmp.find('overlayLib').showCustomModal({
                   header: `Info for ${detail.name}`,
                   body: modalBody,
                   cssClass: 'slds-modal_medium',
                   showCloseButton: true,                   
                   closeCallback: function() {
                       //callback
                   }
               })
               cmp.set('v.modalPromise', modalPromise);
            }
        }
       );
    }
})