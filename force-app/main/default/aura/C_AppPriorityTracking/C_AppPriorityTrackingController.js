({
	init: function(component,event,helper) {
		var urlV = new URL(window.location.href);
		console.log('url params:'+JSON.stringify(urlV));
		component.set("v.urlParams",urlV.searchParams);
		helper.trackDetails(component,event);
	}
})