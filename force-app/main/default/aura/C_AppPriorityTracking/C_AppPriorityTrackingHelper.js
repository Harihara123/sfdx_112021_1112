({
	trackDetails: function (cmp,event) {
		let linktype = cmp.get("v.urlParams").get("c__type") ? cmp.get("v.urlParams").get("c__type") : '';
		let tftId = cmp.get("v.urlParams").get("c__tftId") ? cmp.get("v.urlParams").get("c__tftId"): '';
		let talentId = cmp.get("v.urlParams").get("c__talentId") ? cmp.get("v.urlParams").get("c__talentId"): '';
		let oppId = cmp.get("v.urlParams").get("c__oppId") ? cmp.get("v.urlParams").get("c__oppId") : '';

		let action = cmp.get("c.saveUserEmailActions");
		action.setParams({
            "linkType" : linktype,
            "tftId" : tftId, 
            "oppId" : oppId,
            "talentId" : talentId
        }); 
		this.showSpinner(cmp,event);
		action.setCallback(this, function(response){
			if(response.getState() === 'SUCCESS') {
				
				let linkType = cmp.get("v.urlParams").get("c__type") ? cmp.get("v.urlParams").get("c__type") : '';
				let pageType = 'standard__recordPage';

				if(linkType === 'Profile') {
					let objName = 'Contact';
					let recordId = cmp.get("v.urlParams").get("c__talentId");
					this.hideSpinner(cmp,event);
					this.redirectToSObject(cmp,event,objName,recordId,pageType);
				} else if(linkType === 'Applicant' || linkType === 'SuggestedReq') {
					let objName = 'Opportunity';
					let recordId = cmp.get("v.urlParams").get("c__oppId");
					this.hideSpinner(cmp,event);
					this.redirectToSObject(cmp,event,objName,recordId,pageType);

				} else if(linkType === 'AllMatches') {
					this.hideSpinner(cmp,event);
					this.redirectToMatchPage(cmp,event,linkType);

				}else if(linkType === 'JobPosting') {
					let objName = 'Job_Posting__c';
					let recordId = cmp.get("v.urlParams").get("c__JpId");
					this.hideSpinner(cmp,event);
					this.redirectToSObject(cmp,event,objName,recordId,pageType);
			   }
			}
			else {
				let err = response.getError();
				console.log('error:'+err);
				this.hideSpinner(cmp,event);
			}
		});

		$A.enqueueAction(action);
	},
	redirectToSObject: function(cmp,event,objName,recordId,pageType) {

		var navevt= cmp.find('nav');
        var pgRef = {    
           //"type": "standard__recordPage",
		   "type": pageType,
           "attributes": {
               "recordId": recordId,
               "objectApiName": objName,
               "actionName": "view"
            }
     	} 
        navevt.navigate(pgRef);
	},
	redirectToMatchPage: function(cmp,event,linkType) {
		let contactId = cmp.get("v.urlParams").get("c__talentId") ? cmp.get("v.urlParams").get("c__talentId"): '';
		let matchTalentId = cmp.get("v.urlParams").get("c__matchTalentId") ? cmp.get("v.urlParams").get("c__matchTalentId"): '';
		let srcName = cmp.get("v.urlParams").get("c__srcName") ? cmp.get("v.urlParams").get("c__srcName"): '';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": $A.get("$Label.c.LightningHost")
				+ "/lightning/n/Search_Reqs?c__matchTalentId=" + matchTalentId
				+ "&c__contactId=" + contactId
				+ "&c__srcName=" + srcName
				+"&c__noPush=true"
        });
        urlEvent.fire();
	},
	// function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // remove slds-hide class from mySpinner
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // add slds-hide class from mySpinner    
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    }
})