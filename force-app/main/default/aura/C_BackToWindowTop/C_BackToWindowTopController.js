({
	removeScrollHandler : function (component, event, handler) {
		window.clearInterval( component.get( "v.setIntervalId" ) );
	},
    
    backToTop : function (component, event, helper){
       
        var target = 0;

        var body = (document.documentElement || document.body.parentNode || document.body);

		var scrollTo = function(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop;
                var perTick = difference / duration * 10;
       
                setTimeout(function() {
                    element.scrollTop = element.scrollTop + perTick;
               
                    if (element.scrollTop === to) return;
                    scrollTo(element, to, duration - 10);
                }, 10);
            };
      
        scrollTo(body, target, 1000);
    }
})