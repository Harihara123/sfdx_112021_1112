({

    init: function(cmp) {
    },

    selectViewMenuItem : function(component, event, helper) {
        var triggerCmp = component.find("trigger");

        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            
            //annoying - you can only access the label right now, 
            //will have to parse array until iD works
            
            var views = component.get("v.viewList");
			var arrayLength = views.length;
            
			for (var i = 0; i < arrayLength; i++) {
    			
                if (views[i].label === label) {
                    component.set("v.viewListIndex",i);
                }
			}
          
        }
	}
})