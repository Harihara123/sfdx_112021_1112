({
	loadData: function(cmp, event, helper) {
        var apexFunction = cmp.get("v.apexFunction");     
        var soql = cmp.get("v.soql"); 
        var recordType = cmp.get("v.recordType");
        var recordID = cmp.get("v.recordId");
        var params = null;



        if(apexFunction == ""){
            if(soql != ""){
                soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");

                if(recordType == "HRXML"){
                    apexFunction = "getHRXMLRecord";
                }else{
                    apexFunction = "getRecord";
                }

                params = {"soql": soql};
            }
        }

        var className = '';
        var subClassName = '';
        var methodName = '';

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordID};
            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }

            cmp.set("v.loadingData",false);
            helper.callServer(cmp,className,subClassName,methodName,function(response){
                if(recordType == "HRXML"){
                    var result = response;
                    
                    if(result){
                        if(result != ''){
                             cmp.set("v.record", JSON.parse(result));
                        }
                    }
                }else{
                    cmp.set("v.record", response);
                }
            },actionParams,false);
            
        }else{
            cmp.set("v.loading",false);
        }
    },
    
    updateLoading: function(cmp, event, helper) {
        if(!cmp.get("v.loadingData")) {
            cmp.set("v.loading",false);
        }
    }
})