({
	loadData: function(cmp, event, helper) {
        cmp.set("v.loading",true);
        cmp.set("v.records",[]);
        cmp.set("v.recordCount",0);


        helper.getResults(cmp,helper);
        helper.getResultCount(cmp);
    }
    ,doInit: function(cmp, event, helper) {
        //.
        helper.getInitialViewListLoad(cmp,helper);  
    }
    ,updateLoading : function(cmp,event,helper){
        helper.updateLoading(cmp);
    }
    ,reloadData : function(cmp,event,helper) {
        var rd = cmp.get("v.reloadData");
        if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);

            helper.getResults(cmp,helper);
            helper.getResultCount(cmp);

            cmp.set("v.reloadData", false);
        }

        
    }
})