({
     getInitialViewListLoad : function(cmp,helper){
        var awaitResultFromViewList = false;
        var recordID = cmp.get("v.recordId");
       
        if (!recordID) {
            recordID = this.getParameterByName ("recordId");
            cmp.set("v.recordId", recordID);
        }
        var viewListFunction = cmp.get("v.viewListFunction");

        if(viewListFunction){
            if(viewListFunction != ''){
                awaitResultFromViewList = true;
            }
        }
      
        if(awaitResultFromViewList){
            this.getViewListItems(cmp, viewListFunction, recordID,helper);
        }else{
            this.getResults(cmp,helper);
            this.getResultCount(cmp);
        }
    }
    ,getViewFromApexFunction : function(cmp, apexFunction, recordID, params,helper){
        var className = '';
        var subClassName = '';
        var methodName = '';
        var self = this;

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordID, "maxRows": cmp.get("v.maxRows")};

            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }
            //. this.callServer(cmp,className,subClassName,methodName,function(response){
            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
               
                cmp.set("v.records", response);
                cmp.set("v.loadingData",false);
                var recordCount = 0;
                if(response){
                    if(response.length > 0){
                        if(response[0].totalItems){
                            recordCount = response[0].totalItems;
                        }

                        if (response[0].presentRecordCount !== "undefined") {
                            cmp.set ("v.presentRecordCount", response[0].presentRecordCount);
                            cmp.set ("v.pastRecordCount", response[0].pastRecordCount);
                            cmp.set ("v.hasNextSteps", response[0].hasNextSteps);
                            cmp.set ("v.hasPastActivity", response[0].hasPastActivity);
                        }
                    }
                }

                if(recordCount == 0){
                    self.getResultCount(cmp);
                }

                cmp.set("v.recordCount", recordCount); // returnVal.length);
                cmp.set("v.loadingCount",false);
            },actionParams,false);
        }

    }
    ,getViewListItems : function(cmp, viewListFunction, recordID,helper) {
       var self = this;
        cmp.set("v.loadingData",true);
        var actionParams = {'recordId':recordID};
        var className = '';
        var subClassName = '';
        var methodName = '';

        var arr = viewListFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }
           
            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
            cmp.set("v.loadingData",false);
            var returnVal = response;
            //Rajeesh Spring18 S-59499 - this component is not used anymore
            //but commented out eval function and replaced with json.parse.
            var newArr = JSON.parse(returnVal);
//            var newArr = (new Function("return [" + returnVal + "];")());
            cmp.set("v.viewList", newArr);
            cmp.set("v.loadingData",false);
            self.getResults(cmp, self);
            self.getResultCount(cmp, self);

        },actionParams,false);
    }
    ,getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");

        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var soql = listView[indexNo].soql;
			
            if(soql){
                if(soql != ''){
                    soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
  
                        var apexFunction = "c.getRows";

                        var params = {
                            "soql": soql,
                            "maxRows": cmp.get("v.maxRows")
                        };

                        cmp.set("v.loadingData",true);
                        var actionParams = {'recordId':recordID};
                        if(params){
                            if(params.length != 0){
                             Object.assign(actionParams,params);
                            }
                        }
                            var bdata = cmp.find("basedatahelper");
                            bdata.callServer(cmp,'','',apexFunction,function(response){
                            cmp.set("v.records", response);
                            cmp.set("v.loadingData",false);
                        
                            if(listView[indexNo].additional){
                                cmp.set("v.additional",listView[indexNo].additional);
                            }
                            
                        },actionParams,false);
                    }
                }else{
                    var apexFunc = listView[indexNo].apexFunction;
                    if(apexFunc){
                        var params = listView[indexNo].params;
                        this.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                    }
                }
            }
        }
    ,getResultCount : function(cmp){
        var listView = cmp.get("v.viewList");
        var bdata = cmp.find("basedatahelper");
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var countSoql = listView[indexNo].countSoql;    
            if(countSoql){
                 if(countSoql != ''){
                    countSoql = countSoql.replace("%%%parentRecordId%%%","'"+recordID+"'")
                    var params = {"soql": countSoql};
                    cmp.set("v.loadingCount",true);
                    
                    bdata.callServer(cmp,'','','getRowCount',function(response){
                            cmp.set("v.recordCount", response);
                            cmp.set("v.loadingCount",false);
                        
                    },params,false);

                 }else{
                    cmp.set("v.loadingCount",false);
                 }
            }
        }
        
    },

    updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },

    refreshList : function(cmp) {
        cmp.set("v.reloadData", true);
    },
        getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return ''; 
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
})