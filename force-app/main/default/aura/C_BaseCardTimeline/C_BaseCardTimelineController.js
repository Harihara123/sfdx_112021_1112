({
	shouldLoadMore : function(component, event, helper) {
        
        var childParamValue= event.getParam("shouldLoadMore");
        component.set ("v.displayLoadMore", childParamValue);
        
	}, 
    
    onSelectPresentChange : function(component, event, helper) {
        var spinner = component.find('spinnerLoadMore');
        $A.util.addClass(spinner, 'slds-show'); 

        var selected = parseInt(component.find("loadMorePresentDropdown").get("v.value")) + parseInt(component.get ("v.presentActivityCount"));   
        component.set ("v.presentActivityCount", selected);
        component.set("v.loadMorePresent",[0,5,10,25,50]);
        component.set("v.loadMorePresent",["Select","5","10","25","50"]);

        setTimeout(function() {
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
        }, 1500);

    },

    onSelectPastChange : function(component, event, helper) {

        var spinner = component.find('spinnerLoadMore');
        $A.util.addClass(spinner, 'slds-show'); 

        var selected = parseInt(component.find("loadMorePastDropdown").get("v.value")) + parseInt(component.get ("v.pastActivityCount"));   
        component.set ("v.pastActivityCount", selected);
        component.set("v.loadMorePast",[0,5,10,25,50]);
        component.set("v.loadMorePast",["Select","5","10","25","50"]);

        setTimeout(function() {
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
        }, 1500);
    },
    
    expandPastActivity : function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandPastActivity", true, "v.pastActivityExpanded");
	},
	collapsePastActivity: function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandPastActivity", false, "v.pastActivityExpanded");
	},
	expandNextSteps : function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandNextSteps", true, "v.nextStepsExpanded");
	},
	collapseNextSteps: function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandNextSteps", false, "v.nextStepsExpanded");
	}
})