({
	doInit : function(component, event, helper) {
       // var record = component.get("v.itemDetail");
       // console.log(JSON.stringify(record));
       // console.log(component.get("v.newTimeLine"));
	},
    handleOnItemEditEvent : function(component, event, helper) {
        
        var detail = event.getParam('eventData');
        var eventName = component.get("v.itemEditEventName");

        if(eventName){
            var itemType = detail.itemType;
            var itemID =  detail.itemID;
            var userevent = $A.get("e.c:" + eventName);
            userevent.setParams({"itemID":itemID, "itemType":itemType })
            userevent.fire();
        }
    },
    handleOnItemDeleteEvent : function(component, event, helper) {
        var detail = event.getParam('eventData');
        console.log("detail "+JSON.stringify(detail));
        var recordId = detail.recordId;
        if(recordId!=undefined){
            // Delete the record and Fire reload event
            helper.deleteTimelineItem(component, event,recordId);
        }
    }
})