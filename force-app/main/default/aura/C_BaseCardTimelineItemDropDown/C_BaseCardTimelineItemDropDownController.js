({
	editTimelineItem : function(component, event, helper) {
    var eventName = component.get("v.itemEditEventName");

		if(eventName){
			var itemType = component.get("v.itemType");
			var itemID =  component.get("v.itemID");
			var userevent = $A.get("e.c:" + eventName);
			userevent.setParams({"itemID":itemID, "itemType":itemType })
			userevent.fire();
		}
	},


    confirmDelete : function(component, event, helper) {

        if (component.get ("v.itemType") === 'education' 
            || component.get ("v.itemType") === 'certification'
            || component.get ("v.itemType") === 'event' 
            || component.get ("v.itemType") === 'task'
            || component.get ("v.itemType") === 'employment') {
                var theVal = component.get("v.showConfirmationModal")
                component.set("v.showConfirmationModal", !theVal);
       } else { 
            helper.deleteTimelineItem (component, event, helper);
       }
    },

	deleteTimelineItem :  function(component, event, helper) {
       if (component.get ("v.itemType") === 'education' 
            || component.get ("v.itemType") === 'certification'
          	|| component.get ("v.itemType") === 'event' 
            || component.get ("v.itemType") === 'task'
          	|| component.get ("v.itemType") === 'employment') {
            if (event.getParam('confirmationVal') === 'Y') {
                helper.deleteTimelineItem (component, event, helper);
            }
       }
    }	
})