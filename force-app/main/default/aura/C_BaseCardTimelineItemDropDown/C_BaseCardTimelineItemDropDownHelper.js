({
	deleteTimelineItem : function(component, event, helper) {
        var params = {"recordId": component.get("v.itemID")};
        var bdata = component.find("basedatahelper");
        bdata.callServer(component,'','',"deleteRecord",function(response){
            if (component.get("v.itemReloadEventName")) {
               if (component.get ("v.itemType") === 'education') {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.TC_Refer_A_Friend_Success"),
                        "message": $A.get("$Label.c.ATS_EDUCATION")+" "+$A.get("$Label.c.TC_Record_Delete_Success"),
                        "type": "success"
                    });
                    toastEvent.fire();
               } else if(component.get ("v.itemType") === 'employment'){
                   var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.TC_Refer_A_Friend_Success"),
                        "message": "Employment record deleted successfully.",
                        "type": "success"
                    });
                    toastEvent.fire();
               }else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.TC_Refer_A_Friend_Success"),
                        "message": $A.get("$Label.c.ATS_CERTIFICATION")+" "+$A.get("$Label.c.TC_Record_Delete_Success"),
                        "type": "success"
                    });
                    toastEvent.fire();
                }    
                var reloadEvt = $A.get("e.c:" + component.get("v.itemReloadEventName"));
                reloadEvt.fire();
            } else {
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.TC_Refer_A_Friend_Success"),
                        "message": "Activity deleted!",
                        "type": "success"
                    });
                    toastEvent.fire();
                var reloadEvt = $A.get("e.c:E_TalentActivityReload");
                reloadEvt.fire();
            }        
        },params,false);
	}
})