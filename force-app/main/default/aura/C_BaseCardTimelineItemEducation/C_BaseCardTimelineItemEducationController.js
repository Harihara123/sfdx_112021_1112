({
	displayEducationDetails : function(cmp, event, helper) {
        var recordID = cmp.get("v.itemDetail.ParentRecordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//Neel summer 18 URL change-"url": "/one/one.app#/n/Talent_Education_And_Cert_Details?recordId=" + recordID,
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Talent_Education_And_Cert_Details?recordId=" + recordID,
            "isredirect":true
        });
        urlEvent.fire();
	},
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");

        if(expanded){
            cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
        }
    },
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
        cmp.set("v.expanded", expanded);
    }
})