({
	displayDetails : function(component, event, helper) {
		var theid = event.target.id;
		var userevent = $A.get("e.c:E_TalentEmploymentAdd");
        userevent.setParams({ "Id":  theid});
        userevent.fire();
	},
    

    displayEmploymentDetails : function(component, event, helper) {
        var recordID = event.currentTarget.dataset.recordid;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//Neel summer 18 URL change-"url": "/one/one.app#/n/Employment_Details?recordId=" + recordID,
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Employment_Details?recordId=" + recordID,

            "isredirect":true
        });
        urlEvent.fire(); 
    },

    setupDisplayFields : function (cmp, event, helper) {
        cmp.set("v.expanded", cmp.get("v.expandAll"));
        cmp.set("v.employmentDateRange", helper.buildEmploymentDateRange(cmp.get("v.itemDetail")));
    },
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");

        if(expanded){
            cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
        }
    },
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
        cmp.set("v.expanded", expanded);
    }
})