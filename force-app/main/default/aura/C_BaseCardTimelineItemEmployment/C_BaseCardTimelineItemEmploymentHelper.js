({
	buildEmploymentDateRange : function(workExItem) {
		var output = "";
		var sDate = workExItem.EmploymentStartDate;
		var eDate = workExItem.EmploymentEndDate;

        if ((typeof sDate === "undefined" || sDate == null) 
        	&& (typeof eDate === "undefined" || eDate == null)) {
            output = "No Date Information";
        } else if (typeof sDate === "undefined" || sDate == null) {
            output = "No Start Date - " + $A.localizationService.formatDate(eDate);
        } else if (typeof eDate === "undefined" || eDate == null) {
        	output = $A.localizationService.formatDate(sDate) + " - No End Date";
        } else {
        	var sdt = new Date(sDate);
        	var edt = new Date(eDate);
            output = $A.localizationService.formatDate(sDate) + " - " + $A.localizationService.formatDate(eDate);

            var dur = $A.localizationService.duration(edt.getTime() - sdt.getTime());
            var yrs = $A.localizationService.getYearsInDuration(dur);
            var months = $A.localizationService.getMonthsInDuration(dur);

            var duration = ' (' 
                            + (yrs > 0 ? yrs + (yrs == 1 ? ' Year ' : ' Years ') : '') 
                            + (months > 0 ? months + (months == 1 ? ' Month' : ' Months') : '') 
                            + ')';
            if (duration != ' ()') {
                output += duration;
            }
            // console.log(sDate + " - " + eDate + " / " + output);
        }
        return output;
	}
})