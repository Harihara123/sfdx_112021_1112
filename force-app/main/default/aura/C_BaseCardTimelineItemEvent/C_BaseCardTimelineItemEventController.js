({
	
    doInit: function(component, event, helper) {
        /* Start for Story S-69509 */
        var ActivityType = component.get("v.itemDetail").ActivityType;
                                      
        if( ActivityType =='Submitted' || ActivityType =='Offer Accepted' || ActivityType =='Started' || ActivityType =='Interviewing' || ActivityType.substr(0,14) =='Not Proceeding'){
            component.set("v.showAssignedTo", false );
        }
        
        if(ActivityType.substr(0,14)=='Not Proceeding'){
            component.set("v.NotProceeding", true);
        }

        if(ActivityType =='Interviewing'){
            var description = component.get("v.itemDetail").SubmittalDescription;
            if (description) {
                description = description.replace(/(?:\r\n)/g, '<br>');
                // console.log('JSON STRING1'+description);
                if(description !='not defined'){
                    try{
                   	// console.log('Inside if condition :'+ component.get("v.itemDetail").Subject);
                   	var obj = JSON.parse(description);
                   
                   	// console.log('Candidate Comments' +obj.CandidateComments);
                   	// console.log('Client Comments' +obj.ClientComments);
                  	component.set("v.CandidateComments", obj.CandidateComments );
                 	component.set("v.ClientComments", obj.ClientComments); 
                    }
                    catch(ex){
                        console.log ('Error While Parsing Json - Display');
                    }
                }
            }    
       }
       
        /* End for Story S-69509*/
        var comment = component.get("v.itemDetail").PostMeetingNotes;
        component.set("v.expanded", component.get("v.expandAll")); 

        if(comment == "undefined" || comment == null)
            comment = "";
        else{
            if(parseInt(comment.length) > 1000){
                // console.log("comment");
                // console.log(comment);
                component.set("v.showMore", true);
                component.set("v.comment", comment.substring(0,1000));
                component.set("v.commentMore", comment);
            } else {
                component.set("v.showMore", false);
                component.set("v.comment", comment);
            }     
        }
    },


    displayMoreComments: function(component, event, helper) {
        var comment = component.get("v.commentMore");
        component.set("v.showMore", false);
        component.set("v.comment", comment);
    },

    completeTask:function(component,event,helper){
		var id = component.get("v.itemDetail").RecordId;
         var appEvent = $A.get("e.c:E_TalentActivityCloseEvent");
         if(appEvent){
            appEvent.setParams({ "recordId" :id,
                            "Enddate":component.get("v.itemDetail").EventEndDateTime,
                            "PostNotes":component.get("v.itemDetail").PostMeetingNotes});
            appEvent.fire();
         }
        
    
        var item = component.get("v.itemDetail");
        item.Status = item.Status;
        if(item.Status == 'Completed'){
            item.Complete = true;
        }else{
            item.Complete = false;
        }

        component.set("v.itemDetail", item);
	
	},
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");

        if(expanded){
            cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
        }
    },
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
        cmp.set("v.expanded", expanded);
    }
	
})