({
	doInit: function(component, event, helper) {
    	var comment = component.get("v.itemDetail").Detail;
        component.set("v.expanded", component.get("v.expandAll")); 
        
        if(comment == "undefined" || comment == null)
            comment = "";
        else{
            if(parseInt(comment.length) > 1000){
                // console.log("comment");
                // console.log(comment);
                component.set("v.showMore", true);
                component.set("v.comment", comment.substring(0,1000));
                component.set("v.commentMore", comment);
            } else {
                component.set("v.showMore", false);
                component.set("v.comment", comment);
            }     
        }
	},


    displayMoreComments: function(component, event, helper) {
        var comment = component.get("v.commentMore");
        component.set("v.showMore", false);
        component.set("v.comment", comment);
    },


    completeTask : function(component, event, helper) {
        
        var id = component.get("v.itemDetail").RecordId;
        var appEvent = $A.get("e.c:E_TalentActivityCloseTask");
        appEvent.setParams({ "recordId" :id,
                            "Status":component.get("v.itemDetail").Status,
                            "Description":component.get("v.itemDetail").Detail});
        appEvent.fire();


        var item = component.get("v.itemDetail");
        item.Status = item.Status;
        if(item.Status == 'Completed'){
            item.Complete = true;
        }else{
            item.Complete = false;
        }

        component.set("v.itemDetail", item);

	}, 
	showActivities : function(cmp, event, helper){
        var id = component.get("v.itemDetail").RecordId;
        var urlEvent = $A.get("e.force:navigateToURL");

        urlEvent.setParams({
          "url": "/apex/c__CandidateActivities?accountId=" + id
        });
        urlEvent.fire();
    },
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");

        if(expanded){
            cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
        }
    },
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
        cmp.set("v.expanded", expanded);
    }
})