({
	callServerHelper : function(component, className, subClassName, methodName, callback, params, storable, errCallback) {

        /* Spring19 Issue - Passing a list as a part of JSON (params) is causing this issue, expects all string value pairs - Showket  */
        if(params && params.hasOwnProperty('addlReturnFields')){
            params['addlReturnFields'] = JSON.stringify( params['addlReturnFields'] ) ;
        }
        
        //console.log('params '+JSON.stringify(params));

        var serverMethod = 'c.performServerCall';
        var actionParams = {'className':className,
                            'subClassName':subClassName,
                            'methodName':methodName.replace('c.',''),
                            'parameters': params};

    
        var action = component.get(serverMethod);
        action.setParams(actionParams);

        if(methodName === 'getWorkHistory' 
		|| methodName === 'getCurrentUserWithOwnership'
        || methodName === 'getDoNotRecruitAggregate'
        || methodName === 'getTalentResumeModel') {
        action.setBackground();
		}
        
        if(storable){
            action.setStorable();
        }else{
            action.setStorable({"ignoreExisting":"true"});
        }
      
        action.setCallback(this,function(response) {
           
            var state = response.getState();
            if (state === "SUCCESS") { 
                // pass returned value to callback function
                var resString = response.getReturnValue();
                if(resString === 'ATSApexException'){
                    //console.log('resString '+resString);
                    this.showError('An unexpected error has occured. Please try again!');
                    try{
                        callback.call(this, null);
                    }catch(e){
                        console.log('BaseDataHelper '+e);
                    }
                }else{
                    callback.call(this, resString);   
                }
            } else if (state === "ERROR") {
                /*
				console.log("Server returned ERROR ... ")
				console.log("className -> " + className);
				console.log("subClassName -> " + subClassName);
				console.log("methodName -> " + methodName);
				console.log("callback -> " + callback);
				console.log("params -> " + JSON.stringify(params));
                */
                // Use error callback if available
                if (errCallback) {
                    errCallback.call(this, response.getReturnValue());
                    // return;
                }

                // Fall back to generic error handler
                var errors = response.getError();
                if (errors) {
                    //console.log("Errors", errors);
                    if (errors[0] && errors[0].message) {
                        this.showError(errors[0].message);
                        //throw new Error("Error" + errors[0].message);
                    }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                        this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                    }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                        this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                    }else{
                        this.showError('An unexpected error has occured. Please try again!');
                    }
                } else {
                    this.showError(errors[0].message);
                    //throw new Error("Unknown Error");
                }
            }
           
        });
    
        $A.enqueueAction(action);
    }
    ,showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    }

    ,createComponent : function(cmp, componentName, targetAttributeName, params) {
         $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    }
    ,adjustDateForTimezone : function (inputDate) {
        var theDate = new Date(inputDate);
        if (!isNaN(theDate.getTime())) {
            // Use same date to get the timezone offset so daylight savings is factored in.
            var output = new Date(theDate.getTime() + (theDate.getTimezoneOffset() * 60 * 1000));
            return output;
        } else {
            return false;
        }
    }
    ,formatDate:function(inputDate){
        var input = inputDate;
        if(input){
             // Typed in dates only work for "/" as delimiter.
            var arr = input.split("/");
            if(arr.length === 3){
                // Pad "0" for single digit date / month
                if(arr[0].length === 1){
                   arr[0] = '0' + arr[0];
                }
                if(arr[1].length === 1){
                   arr[1] = '0' + arr[1];
                }

                // Convert to "yyyy-mm-dd" based on user locale.
                var language = window.navigator.userLanguage || window.navigator.language;
                if(language === 'en-US'){
                    input = arr[2] + '-' + arr[0] + '-' + arr[1];
                }else{
                    input = arr[2] + '-' + arr[1] + '-' + arr[0];
                }
            } 

            // Adjust for timezone since the new Date() constructor sets to UTC midnight.
            var now = this.adjustDateForTimezone(input);
            if (now === false || isNaN(now.getTime())) {
                return false;
            } else {
                return now.getFullYear()+"-"+(now.getMonth() + 1) +"-"+now.getDate();
            } 
        }else{
            return '';
        }
       
    }
    ,formatDateTime:function(inputDateTime){
        var output = inputDateTime;

        if(inputDateTime){

            // Split the input string on space
            var arr = inputDateTime.split(' ');
           

            // Works only with the standard date format. Spaces in date will break this.
            if(arr.length >= 2){
                // Call function to format date part of the string.
                var sDate = this.formatDate(arr[0]);

                // Split time part of the string by colon. 
                var hhmm = arr[1].split(":");
                var hrs, mins, ampm;
                ampm = '';
                if (hhmm.length >= 2) {
                    // Pad single digit hours with "0".
                    hrs = hhmm[0].length === 1 ? "0" + hhmm[0] : hhmm[0];
                    // If am/pm is not separated from time by space, use substring.
                    mins = hhmm[1].substring(0, 2);
                    if (hhmm[1].length > 2) {
                        ampm = hhmm[1].substring(2, hhmm[1].length);
                    }
                }
                // If am/pm separated from time by space
                if (arr.length === 3) {
                    ampm = arr[2];
                }
                // Adjust to 24 hour clock
                if (ampm.toLowerCase() === "pm" && hrs !== "12") {
                    hrs = parseInt(hrs) + 12;
                } else if (ampm.toLowerCase() === "am" && hrs === "12") {
                    hrs = "00";
                }

                // Initialize Date object from generated date and time strings.
                var sTime = " " + hrs + ":" + mins + ":00.000Z";

                // Initialize date and readjust for timezone because previous parsing steps set it to UTC
                var tDate = new Date(sDate + sTime);
                var theDate = new Date(tDate.getTime() + (tDate.getTimezoneOffset() * 60 * 1000));

                // Check for invalid date format. Return boolean false if invalid.
                if (!isNaN(theDate.getTime())) {
                    output = theDate.toISOString();
                } else {
                    output = false;
                }
            } else {
                // Datepicker was used, so input is ISO string and passed right back as output.
                output = inputDateTime;
            }
        }
        return output;
    },

    getAcceptedDateFormat : function () {
        var language = window.navigator.userLanguage || window.navigator.language;
        return language === "en-US" ? "mm/dd/yyyy" : "dd/mm/yyyy";
    },

    getAcceptedTimeFormat : function () {
        return "hh:mm AM/PM";
    },

    getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
})