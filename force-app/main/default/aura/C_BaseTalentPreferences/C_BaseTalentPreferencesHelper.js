({
	HOURLY_TO_ANNUAL_FACTOR : 2080,
    DAILY_TO_ANNUAL_FACTOR : 260,
    WEEKLY_TO_ANNUAL_FACTOR : 52,
    MONTHLY_TO_ANNUAL_FACTOR : 12,
    
    populateCurrencyType : function(component) {
        var params = null;
        this.callServer(component,'ATS','TalentEmploymentFunctions','getCurrencyList',function(response){
            if(component.isValid()){
                
            var desiredCurrency = component.get("v.talent.Desired_Currency__c");
            component.set("v.desiredCurrency",desiredCurrency); 
                var CurrLevel = [];
                var CurrLevelMap = response;
                for ( var key in CurrLevelMap ) {
                    CurrLevel.push({value:key, key:key,selected: key === desiredCurrency});
                    }
                component.set("v.CurrencyList", CurrLevel); 
            }
            
           
        },params,false);
        
    },
    //Neel summer 18 URL change->Remove hardcoded url Eventhough these method are not is use
    editCandidateSummary :function(cmp) {
        
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          //Neel summer 18 URL change->"url": "/one/one.app#/n/Candidate_Summary?talentId="+recordID +"&mode=edit" ,
          "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Candidate_Summary?talentId="+recordID +"&mode=edit" ,
           "isredirect":'true'
        });
        urlEvent.fire();
    },
    redirectCandidateSummary :function(cmp) {
       if(cmp.get("v.card")){
            $A.get('e.force:refreshView').fire();
			cmp.set("v.spinnerState", false);
            cmp.set("v.edit",false);
       }else{
        this.viewCandidateSummary(cmp);   
       }    
    },
    viewCandidateSummary :function(cmp) {
        
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        /*urlEvent.setParams({
                  "url": "/one/one.app#/sObject/" + recordID +"/view",
                  "isredirect":'true'
         });*/
        var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
        var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
        var sObj;
        var dURL;
        
        if(isFlagOn === "true") {
			if(recordID){
	            if(recordID.substring(0, 3) === '001'){
	               sObj = 'Account';
	                
	            }else if(recordID.substring(0, 3) === '003'){
	                sObj = 'Contact'; 
	            }
	        }
			dURL = ligtningNewURL+"/r/"+sObj+"/" + recordID +"/view";
		}
		else {
			dURL = ligtningNewURL+"/sObject/" + recordID +"/view";
		}
        
        urlEvent.fire();  
        
        
    },
    /* Compensation fields No longer calculated using rate frquency S-58916 

    calculateBonusPct:function(cmp) {
        var bonus = (cmp.find("desiredBonusId").get("v.value") != null ? cmp.find("desiredBonusId").get("v.value") : 0);
            bonus = parseFloat(bonus).toFixed(2);
        var paytype = cmp.find("ratetypeId").get("v.value"); 
        var pay = (cmp.find("desiredrateId").get("v.value") != null ? cmp.find("desiredrateId").get("v.value") : 0);
        
        
        if(paytype != null){
            if(paytype == 'hourly'){
                pay = (pay * this.HOURLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'daily'){
                pay = (pay * this.DAILY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'weekly'){
                pay = (pay * this.WEEKLY_TO_ANNUAL_FACTOR); 
            }
            else if(paytype == 'monthly'){
                pay = (pay * this.MONTHLY_TO_ANNUAL_FACTOR); 
            }
            pay = (pay != 0 ? (parseInt(bonus)/parseInt(pay))*100 : 0);
            pay = parseFloat(pay).toFixed(2);
            
            cmp.find("desiredBonusPercentId").set("v.value",pay) ;
        }
        
    },
    calculateBonusAmount:function(cmp) {
        var bonus = (cmp.find("desiredBonusPercentId").get("v.value") != null ? cmp.find("desiredBonusPercentId").get("v.value") : 0);
              bonus = parseFloat(bonus).toFixed(2);
        var paytype = cmp.find("ratetypeId").get("v.value");
        var pay  = (cmp.find("desiredrateId").get("v.value") != null ? cmp.find("desiredrateId").get("v.value") : 0);
              
        if(paytype != null){
            if(paytype == 'hourly'){
                pay = (pay * this.HOURLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'daily'){
                pay = (pay * this.DAILY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'weekly'){
                pay = (pay * this.WEEKLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'monthly'){
                pay = (pay * this.MONTHLY_TO_ANNUAL_FACTOR); 
            }
            pay = (pay * bonus)/100 ;
            
            cmp.find("desiredBonusPercentId").set("v.value",bonus);
            cmp.find("desiredBonusId").set("v.value",pay);
        }
        
    },

    calculateTotal:function(cmp) {
        if(cmp.get("v.edit")){
            var paytype = cmp.find("ratetypeId").get("v.value");
            var pay = (((cmp.find("desiredrateId").get("v.value") != null) && (cmp.find("desiredrateId").get("v.value") != "")) ? cmp.find("desiredrateId").get("v.value") : 0);
            var bonus = (((cmp.find("desiredBonusId").get("v.value") != null) && (cmp.find("desiredBonusId").get("v.value") != ""))  ? cmp.find("desiredBonusId").get("v.value") : 0);
            var addl = (((cmp.find("desiredAdditionalCompensationId").get("v.value") != null) && (cmp.find("desiredAdditionalCompensationId").get("v.value") != "")) ? cmp.find("desiredAdditionalCompensationId").get("v.value") : 0);  // adding this step to check for blank the addl bonus to prevent NaN issue. 
            
        }else{
            var paytype = cmp.find("readratetypeId").get("v.value");
            var pay = (((cmp.find("readdesiredrateId").get("v.value") != null) && (cmp.find("readdesiredrateId").get("v.value") != "")) ? cmp.find("readdesiredrateId").get("v.value") : 0);
            var bonus = (((cmp.find("readdesiredBonusId").get("v.value") != null) && (cmp.find("readdesiredBonusId").get("v.value") != ""))  ? cmp.find("readdesiredBonusId").get("v.value") : 0);
            var addl = (((cmp.find("readdesiredAdditionalCompensationId").get("v.value") != null) && (cmp.find("readdesiredAdditionalCompensationId").get("v.value") != "")) ? cmp.find("readdesiredAdditionalCompensationId").get("v.value") : 0);  // adding this step to check for blank the addl bonus to prevent NaN issue. 
            
        }
        
        if(paytype != null && paytype != "Select"){
            if(paytype == 'hourly'){
                pay = pay * this.HOURLY_TO_ANNUAL_FACTOR; 
            }else if(paytype == 'daily'){
                pay = pay * this.DAILY_TO_ANNUAL_FACTOR; 
            }else if(paytype == 'weekly'){
                pay = (pay * this.WEEKLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'monthly'){
                pay = (pay * this.MONTHLY_TO_ANNUAL_FACTOR); 
            }
            bonus = (isNaN(bonus)) ? 0 : bonus;
            addl = (isNaN(addl)) ? 0 : addl;
            pay = parseFloat(pay) + parseFloat(bonus) + parseFloat(addl);
            pay = parseFloat(pay).toFixed(2)
            cmp.set("v.totalComp",pay);  
        }
    },*/
  saveCandidateSummary:function(cmp){
    //debugger;
    //console.log('inside base talent preference'); 
    var g2Comments; 
    if(cmp.isValid()){
			  
              this.savePickList(cmp);
              this.populateLanguageCode(cmp);

            // console.log('talentxxx--->' +JSON.stringify( cmp.get("v.talent") ) );
            // console.log('talentyyy--->' +JSON.stringify( cmp.get("v.isG2Checked") ) );
            // console.log('talentzzz--->' +JSON.stringify( cmp.get("v.isG2DateToday") ) );

              
              //S-35422 Added by Karthik
              cmp.set("v.talent.Talent_Preference_Internal__c",JSON.stringify(cmp.get("v.record")));
              cmp.set("v.talent.Skills__c",JSON.stringify(cmp.get("v.currentSkills")));
        	  //S-38797 G2 changes - Added by Inshan
			  
			  if (cmp.get("v.isG2Checked")) {
					if (cmp.get("v.isG2DateToday")) {
		                  cmp.set("v.talent.G2_Completed__c", false);
					} else {
		                  cmp.set("v.talent.G2_Completed__c", true);
					}
			  } else {
                  cmp.set("v.talent.G2_Completed__c", false);
			  }
	          
        var params  = {"talent": cmp.get("v.talent"), "G2Comments": cmp.get("v.g2CommentsId") ? cmp.get("v.g2CommentsId") : null };
              this.callServer(cmp,'ATS','',"saveTalentPreferences",function(responsejson){
                if(cmp.isValid()){
                    var state = responsejson;
                    this.redirectCandidateSummary(cmp);
                    var updateLMD = $A.get("e.c:E_TalentActivityReload");
					updateLMD.fire();
                    var appEvent = $A.get("e.c:RefreshTalentHeader");
                    appEvent.fire();
                }  
             },params,false);
            
          } 
  },
  populateLanguage:function(cmp){
      var currentLang =  [];
      var cLangObj = [];
      var langMap = {};
      if(cmp.get("v.record") !== '') {
         if(cmp.get("v.record.languages").length > 0){
          currentLang = cmp.get("v.record.languages"); 
          langMap = cmp.get("v.languageList"); 
          for(var i = 0;i<currentLang.length;i++){
              cLangObj.push({"key" : currentLang[i],"value" : langMap[currentLang[i]]});
          }
      }
      cmp.set("v.currentLanguages",cLangObj);      
  }
      
         
  },
  populateLanguageCode:function(cmp){
     var currentLang =  [];
     var codes = []; 
      if(cmp.get("v.currentLanguages").length > 0){
         currentLang = cmp.get("v.currentLanguages"); 
         for(var i = 0;i<currentLang.length;i++){
              codes.push(currentLang[i].key);
          }
         
      }
       cmp.set("v.record.languages",codes);
         
  },
    populatePickList:function(cmp) {
        this.populateCurrencyType(cmp);
        var natOpp =  cmp.get("v.record.geographicPrefs.nationalOpportunities");
        if(natOpp  != null){
            if(natOpp  == true){
                cmp.set("v.nationalOpp","Yes"); 
            }else{
                cmp.set("v.nationalOpp","No"); 
            }
        }
        
        var reli = cmp.get("v.record.employabilityInformation.reliableTransportation");
        if(reli  != null){
            if(reli  == true){
                cmp.set("v.reliable","Yes"); 
            }else{
                cmp.set("v.reliable","No"); 
            }
        }
        
        var commuteType = cmp.get("v.record.geographicPrefs.commuteLength.unit");
        if(commuteType != null){
            cmp.set("v.commuteType",commuteType); 
          }
        
        
    },
    savePickList:function(cmp) {
     
      /*  var natOpp =  cmp.get("v.nationalOpp");
        if(natOpp  != null){
            if(natOpp == "Yes"  && natOpp != "Select"){
                cmp.set("v.record.geographicPrefs.nationalOpportunities",true); 
            }else if(natOpp == "No"){
                cmp.set("v.record.geographicPrefs.nationalOpportunities",false); 
            }
        }
        
        var reli = cmp.get("v.reliable");
        if(reli  != null && reli != "Select"){
            if(reli  == "Yes"  ){
                cmp.set("v.record.employabilityInformation.reliableTransportation",true); 
            }else if(reli  == "No"){
                cmp.set("v.record.employabilityInformation.reliableTransportation",false); 
            }
        } */

		var natOpp =  cmp.get("v.record.geographicPrefs.nationalOpportunities");
        if(natOpp  != null){
            if(natOpp == "Yes"  && natOpp != "Select"){
                cmp.set("v.record.geographicPrefs.nationalOpportunities",true); 
            }else if(natOpp == "No"){
                cmp.set("v.record.geographicPrefs.nationalOpportunities",false); 
            }
        }
        
        var reli = cmp.get("v.record.employabilityInformation.reliableTransportation");
        if(reli  != null && reli != "Select"){
            if(reli  == "Yes"  ){
                cmp.set("v.record.employabilityInformation.reliableTransportation",true); 
            }else if(reli  == "No"){
                cmp.set("v.record.employabilityInformation.reliableTransportation",false); 
            }
        }
        
        var commuteType = cmp.get("v.commuteType");
        
        if(commuteType != "" && commuteType != "Select"){
           cmp.set("v.record.geographicPrefs.commuteLength.unit",commuteType); 
        }
      
        
    },
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    callServer : function(component, className, subClassName, methodName, callback, params, storable, errCallback) {
        var serverMethod = 'c.performServerCall';
        var actionParams = {'className':className,
                            'subClassName':subClassName,
                            'methodName':methodName.replace('c.',''),
                            'parameters': params};


        var action = component.get(serverMethod);
        action.setParams(actionParams);

		if(methodName === 'getRecord'
            || methodName === 'getTalentOverviewModel'
			|| methodName === 'getCurrentUserWithOwnership'
			|| methodName === 'getTalentBaseFields'
			|| methodName === 'getLanguages'
			|| methodName === 'getCurrencyList'
			|| methodName === 'getDocumentListModal') {
        action.setBackground();
		}
        
        if(storable){
            action.setStorable();
        }else{
            action.setStorable({"ignoreExisting":"true"});
        }
      
        action.setCallback(this,function(response) {
            var state = response.getState();
             if (state === "SUCCESS") { 
                // pass returned value to callback function
                callback.call(this, response.getReturnValue());   
                
            } else if (state === "ERROR") {
                // Use error callback if available
                if (typeof errCallback !== "undefined") {
                    errCallback.call(this, response.getReturnValue());
                    // return;
                }

                // Fall back to generic error handler
                var errors = response.getError();
                if (errors) {
                    console.log("Errors", errors);
                    if (errors[0] && errors[0].message) {
                        this.showError(errors[0].message);
                        //throw new Error("Error" + errors[0].message);
                    }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                        this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                    }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                        this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                    }else{
                        this.showError('An unexpected error has occured. Please try again!');
                    }
                } else {
                    this.showError(errors[0].message);
                    //throw new Error("Unknown Error");
                }
            }
           
        });
    
        $A.enqueueAction(action);
    }, 
    g2Completed: function(cmp){
        // console.log("-- G2 completed ---"); 
        var g2CompletedVal = cmp.find("g2Checkbox").get("v.value"); 
        // console.log("g2Checkbox value: " + g2CompletedVal);
        if(g2CompletedVal === true){
            // console.log("--- G2 is Complete ---");
        var g2CompeletedDate = cmp.get("v.talent.G2_Completed_Date__c");
        // console.log("g2Checkbox value: " + g2CompletedVal);
        // console.log("g2CompeletedDate " + g2CompeletedDate);
        
        }
    },
	
	/*fetchCurrentUser : function(component) {
		var params = null;
        this.callServer(component,'','',"getCurrentUser",function(response){
            if(component.isValid()){
            	component.set ("v.runningUser", response); 
            }
        }, params,false);
    },*/
	//S-59001 Neel Kamal 03/14/2018 
	fetchCurrentUser : function(component) {
		var params = null;
        this.callServer(component,'','',"getCurrentUserWithOwnership",function(response){
            if(component.isValid()){
				var uoModal = response;
            	component.set ("v.runningUser", uoModal.usr);
				component.set ("v.usrOwnership", uoModal.userOwnership); 				
            }
        }, params,false); 
		       
    }, 
    calculateTotalComp:function(cmp) {
        if(cmp.get("v.edit")){
            var pay = (((cmp.find("desiredsalary") !== null) && (cmp.find("desiredsalary") !== undefined)) ? cmp.find("desiredsalary").get("v.value") : 0);
            var bonus = (((cmp.find("desiredBonusId") !== null) && (cmp.find("desiredBonusId") !== undefined))  ? cmp.find("desiredBonusId").get("v.value") : 0);
            var addl = (((cmp.find("desiredAdditionalCompensationId") !== null) && (cmp.find("desiredAdditionalCompensationId") !== undefined)) ? cmp.find("desiredAdditionalCompensationId").get("v.value") : 0);  // adding this step to check for blank the addl bonus to prevent NaN issue. 
            
        }else{
            var pay = (((cmp.find("readdesiredrsalary") !== null) && (cmp.find("readdesiredrsalary") !== undefined)) ? cmp.find("readdesiredrsalary").get("v.value") : 0);
            var bonus = (((cmp.find("readdesiredBonusId") !== null) && (cmp.find("readdesiredBonusId") !== undefined))  ? cmp.find("readdesiredBonusId").get("v.value") : 0);
            var addl = (((cmp.find("readdesiredAdditionalCompensationId") !== null) && (cmp.find("readdesiredAdditionalCompensationId") !== undefined)) ? cmp.find("readdesiredAdditionalCompensationId").get("v.value") : 0);  // adding this step to check for blank the addl bonus to prevent NaN issue. 
        }
        
      
            bonus = (isNaN(bonus) || bonus === null) ? 0 : bonus;
            addl = (isNaN(addl) || addl === null) ? 0 : addl;
            pay = (isNaN(pay) || pay === null) ? 0 : pay; 
            pay = parseFloat(pay) + parseFloat(bonus) + parseFloat(addl);
            pay = parseFloat(pay).toFixed(2)
            cmp.set("v.totalComp",pay);  

    },

    calculateBonusAmountOnSalary:function(cmp) {
        var bonus = (cmp.find("desiredBonusPercentId").get("v.value") != null ? cmp.find("desiredBonusPercentId").get("v.value") : 0);
              bonus = parseFloat(bonus).toFixed(2);
        var pay  = (cmp.find("desiredsalary").get("v.value") != null ? cmp.find("desiredsalary").get("v.value") : 0);
            pay = (pay * bonus)/100 ;    
            cmp.find("desiredBonusPercentId").set("v.value",bonus);
            cmp.find("desiredBonusId").set("v.value",pay);
        },
    
    calculateBonusPctOnSalary:function(cmp) {
        var bonus = (cmp.find("desiredBonusId").get("v.value") != null ? cmp.find("desiredBonusId").get("v.value") : 0);
            bonus = parseFloat(bonus).toFixed(2);
        var paytype = cmp.find("ratetypeId").get("v.value"); 
        var pay = (cmp.find("desiredsalary").get("v.value") != null ? cmp.find("desiredsalary").get("v.value") : 0);
        
            pay = (pay != 0 ? (parseInt(bonus)/parseInt(pay))*100 : 0);
            pay = parseFloat(pay).toFixed(2);
            
            cmp.find("desiredBonusPercentId").set("v.value",pay) ;
        
    }, 

    updateMaxSalary: function(component, event){

       var minSalaryField = component.find("desiredsalary"); 
       var maxSalaryField = component.find("desiredsalarymax");
       var minSalary = minSalaryField.get("v.value");
       var maxSalary = maxSalaryField.get("v.value");

       if($A.util.isEmpty(maxSalary)){
        maxSalaryField.set("v.value", minSalary);
       }
       if($A.util.isEmpty(minSalary) && !$A.util.isEmpty(maxSalary)){
        minSalaryField.set("v.errors", [{message:"Please enter Min. Salary"}]);
       }else{
        minSalaryField.set("v.errors", [{message:""}]);
       }


       if(minSalary > maxSalary && !$A.util.isEmpty(maxSalary)){
        maxSalaryField.set("v.errors", [{message:"Min salary cannot be greater than Max. Salary"}]);
       }else{
        maxSalaryField.set("v.errors", [{message:""}]);
       }
 

    },

    updateMax: function(component, event){

       var minField = component.find("desiredrateId"); 
       var maxField = component.find("desiredratemaxId");
       var minValue = minField.get("v.value");
       var maxValue = maxField.get("v.value");

       if($A.util.isEmpty(maxValue)){
        maxField.set("v.value", minValue);
       }
       if($A.util.isEmpty(minValue) && !$A.util.isEmpty(maxValue)){
        minField.set("v.errors", [{message: "Please enter Min. Rate"}]);
       }else{
        minField.set("v.errors", [{message: ""}]);
       }

       if(minValue > maxValue && !$A.util.isEmpty(maxValue)){
        maxField.set("v.errors", [{message:"Min Rate cannot be greater than Max. Rate"}]);
       }else{
        maxField.set("v.errors", [{message:""}]);
       }       

    }      
})