({
	handleMouseOver : function(component, event, helper) {        
		let toggle = {
			'slds-hide': 'slds-show',
			'slds-show': 'slds-hide'
		}
		setTimeout(()=>{
			component.set('v.popoverState', toggle[component.get('v.popoverState')]);
			event.stopPropagation();
		}, 200);
		
        
	},

	setKeyboardFocus:function(component, event, helper) {
		const toggle = (event.type == "focus" ? "slds-show" : (event.type == "blur" ? "slds-hide" : "" ));
		component.set('v.popoverState',toggle);

		setTimeout(()=>{
			component.set('v.popoverState', toggle);
			event.stopPropagation();
		}, 200);

	}
})