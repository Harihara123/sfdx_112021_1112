({
	diplayCandidateEditEndDateModal : function(cmp, event, helper) {
	  // console.log("Farid : init method");	
	}
    
    , showModal : function(cmp, event, helper) { 
          cmp.set("v.recordId", event.getParam("recordId"));        
          helper.initializeEndDateModal(cmp);
	},
    
	EnableDate : function(cmp, evt, helper) { 
         helper.EnableEndDate(cmp,evt);
	},
 
    hideModal : function(component, event, helper){
       helper.hideModal(component, event);   
    },
    saveNewEndDate  : function(component, event, helper){
	    if (helper.validateNewEndDate(component, event)) {
            helper.saveNewEndDate(component, event);
        }
        

    } 
})