({
	initializeEndDateModal : function(component) {
        
        var recordId = component.get("v.recordId"); 
        var action = component.get("c.getCandidateWorkExEndDateInfo");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {   
			    var workList =   response.getReturnValue();
				if (workList.length === 1){// added by akshay for  S-78569 4/18/18
				    workList[0].selected = true;
					component.set("v.SelectedId",workList[0].Id);
				}
                component.set("v.workExEndDateList",workList);
                 
                component.set("v.diplayCandidateEditEndDate",true);	
                component.set("v.modalTitle", "Change End Date");
                this.toggleClass(component,'backdropEditEndDate','slds-backdrop--');
                this.toggleClass(component,'modaldialogEditEndDate','slds-fade-in-');
            } 
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
		
	},
	EnableEndDate: function(cmp,evt){
	
	 cmp.set("v.SelectedId", evt.getSource().get("v.text"));
		   var list = cmp.get("v.workExEndDateList");
           //var index = evt.getSource().get("v.text");
		   for(var i=0;i < list.length;i++){
		       if(list[i].Id === cmp.get("v.SelectedId")){
			      list[i].disable = false;
				  list[i].selected = true; 
			   }else{
			      list[i].disable = true;
				  list[i].selected = false;
				  delete list[i].selecteddate;  
			   } 
		   }
		 cmp.set("v.workExEndDateList",list);  
	
	},
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
  }, 

    validateNewEndDate:function(component, event){
    var validdate = true;
	var selectedrec;
	var newEndDate;
	var list = component.get("v.workExEndDateList");
	for(var i=0;i < list.length;i++){
	      if(list[i].Id === component.get("v.SelectedId")){
		     selectedrec = list[i];
			 break; 
		  }
	    }

      if (typeof selectedrec === "undefined" || selectedrec === null) {
	      validdate = false;
		  this.showToast('error','Please Select a Work History');
         }
			else{
					if (typeof selectedrec.selecteddate !== "undefined") {
					var newEndDateStr = this.formatDate(selectedrec.selecteddate);
					var StDateStr = this.formatDate(selectedrec.Start_Date__c);
						if (newEndDateStr === false) {
							this.showToast('error','Valid date format is '+ this.getAcceptedDateFormat()); 
							validdate = false;
							}else{
							    newEndDate = new Date(newEndDateStr);
								var StartDate = new Date(StDateStr);
								var todayDate = new Date();
								if(newEndDate <= todayDate){
								   this.showToast('error','The new end date must be after today, otherwise please use the Finish Form.');
								   validdate = false;
								}else if(newEndDate <= StartDate){
								   this.showToast('error','The new end date must be after Start Date.');
								   validdate = false;
								}else if( Math.floor((newEndDate.getTime() - todayDate.getTime())/(86400000 * 365.25 )) >= 3){// number milliseconds in a day * number of days in a year .. added by akshay for  S-78569 4/18/18
								   this.showToast('error','End date cannot be more than three years from the current date.');
								   validdate = false;
								}else if (typeof selectedrec.PS_last_change_req_date__c !== "undefined" && selectedrec.PS_last_change_req_date__c !== null){
								    var LastReqDate = new Date(selectedrec.PS_last_change_req_date__c);
									if(Math.floor(parseInt(todayDate - LastReqDate)/parseInt($A.get("$Label.c.ATS_Peoplesoft_Change"))) === 0 ){
									   this.showToast('error','The End Date cannot be extended due to the ESF having been worked today.');
								       validdate = false;
									}
							        
								
								}
							
							} 
					}else{
							this.showToast('error','Please Enter a Date. ');
							validdate = false;
					}
			}
   if(validdate){
      component.set("v.SelectedDate",selectedrec.selecteddate);
   }
   return validdate; 
  },
  showToast : function(typ,ipmessage) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
	    type: typ,
        message: ipmessage
    });
    toastEvent.fire();
},
  formatDate:function(inputDate){
        var input = inputDate;
        if(input){
             // Typed in dates only work for "/" as delimiter.
            var arr = input.split("/");
            if(arr.length === 3){
                // Pad "0" for single digit date / month
                if(arr[0].length === 1){
                   arr[0] = '0' + arr[0];
                }
                if(arr[1].length === 1){
                   arr[1] = '0' + arr[1];
                }

                // Convert to "yyyy-mm-dd" based on user locale.
                var language = window.navigator.userLanguage || window.navigator.language;
                if(language === 'en-US'){
                    input = arr[2] + '-' + arr[0] + '-' + arr[1];
                }else{
                    input = arr[2] + '-' + arr[1] + '-' + arr[0];
                }
            } 

            // Adjust for timezone since the new Date() constructor sets to UTC midnight.
            var now = this.adjustDateForTimezone(input);
            if (now === false || isNaN(now.getTime())) {
                return false;
            } else {
                return now.getFullYear()+"-"+(now.getMonth() + 1) +"-"+now.getDate();
            } 
        }else{
            return '';
        }
       
    },

	adjustDateForTimezone : function (inputDate) {
        var theDate = new Date(inputDate);
        if (!isNaN(theDate.getTime())) {
            // Use same date to get the timezone offset so daylight savings is factored in.
            var output = new Date(theDate.getTime() + (theDate.getTimezoneOffset() * 60 * 1000));
            return output;
        } else {
            return false;
       }
    },

	getAcceptedDateFormat : function () {
        var language = window.navigator.userLanguage || window.navigator.language;
        return language === "en-US" ? "mm/dd/yyyy" : "dd/mm/yyyy";
    },

  saveNewEndDate: function(component, event){
        var newEndDate = component.get("v.SelectedDate");
		var twhId = component.get('v.SelectedId');
		var self = this; 
		var action = component.get('c.invokePSEndDateChangeRequest');
		action.setParams({ twhId : twhId,
						   endDate: newEndDate});
        action.setCallback(this, function(response) {
			var state = response.getState();
			var returnval = response.getReturnValue();
            if (state === "SUCCESS") { 
			if(returnval.startsWith("SUCCESS")){
			  this.showToast('success','The end date extension request has been sent successfully. Please expect a 24 hour delay before it updates the work history record.');
			  self.hideModal(component, event);
			}else{
			   this.showToast('error',response.getReturnValue());
			}
			
				//check response to see whether the request was sent successfully or not.
				/*
					This is the format of successful response that comes back.

					{
						"EndDateAutomationResponse": {
							"@MessageCode": 0,
							"@Message": "The end date extesion request has been sent successfully.",
							"TEXT": "NULL"
						}
					}

					Failures come back as
					'ERROR: Required fields missing. Request not sent to peoplesoft!'\
					or
					'Exception: Apigee call to Peoplesoft End Date Change failed!';
				*/
			}
			else{
			   this.showToast('error','Error while sending request to peoplesoft!');
				console.log('Error while sending request to peoplesoft!');
			}
		});
		$A.enqueueAction(action);
	
		
  }, 
      hideModal : function(component, event){
         //Toggle CSS styles for hiding Modal
		component.set("v.SelectedDate",null); 
		component.set('v.SelectedId',null);
		this.toggleClassInverse(component,'backdropEditEndDate','slds-backdrop--');
		this.toggleClassInverse(component,'modaldialogEditEndDate','slds-fade-in-');
		var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
    }
})