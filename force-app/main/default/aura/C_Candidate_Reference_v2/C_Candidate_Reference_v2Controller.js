({
    
    executeAction : function(cmp, event, helper) {
        // alert(event.getSource().getLocalId());
        // alert(event.getSource().get("v.name"));
        
        var action  = event.getSource().getLocalId();
        var row     = {'Id' : event.getSource().get("v.name") }
        cmp.set("v.row", row);
        
        switch (action) {
            case 'edit':
                // alert(cmp.get("v.recordId"));
                cmp.set("v.showReferenceModal", "true");
                var childCmp = cmp.find("referenceModal")
                childCmp.editMethod();
                break;
                
            case 'delete':
                var theVal = cmp.get("v.showConfirmationModal")
                cmp.set("v.showConfirmationModal", !theVal);
                break;
                
            case 'download':
                var rowIds = [];
                rowIds.push(row.Id);
                cmp.set("v.rowsSelected", rowIds);
                cmp.set('v.singleDownload', true);
                helper.loadFileData(cmp, event, helper);
                // helper.exportData(cmp, row.Id );
                break;
                
            case 'preview':
                var preview = true;
                
                var rowIds = [];
                rowIds.push(row.Id);
                cmp.set("v.rowsSelected", event.getSource().get("v.name") );
                
                helper.loadFileData(cmp, event, helper, preview);
                cmp.set("v.showPreview", true);
                break;
                
        }
        
    },
    handleMethod: function (cmp, e, h) {
        let Id = e.currentTarget.id;
        let action = e.currentTarget.getAttribute('data-action');

        cmp.set("v.row", {Id});

        console.log('handeling the method!');
        console.log(e.currentTarget.getAttribute('id'), e.currentTarget.getAttribute('data-action'));

        const actions = {

            edit: () => {
                cmp.set("v.showReferenceModal", "true");
                let childCmp = cmp.find("referenceModal")
                childCmp.editMethod();
            },
            preview: () => {
                let rowIds = [];
                rowIds.push({Id});
                cmp.set("v.rowsSelected", Id);

                h.loadFileData(cmp, e, h, true);
                cmp.set("v.showPreview", true);
            },
            delete: () => {
                cmp.set("v.showConfirmationModal", !cmp.get('v.showConfirmationModal'));
            },
            download: () => {
                let rowIds = [];
                rowIds.push(Id);

                cmp.set("v.rowsSelected", rowIds);
                cmp.set('v.singleDownload', true);
                h.loadFileData(cmp, e, h);
            },
            convert: () => {
                console.log(e.currentTarget.getAttribute('data-value'));

                cmp.set("v.showConvertModal",false);
                let convertTo = e.currentTarget.getAttribute('data-value');

                cmp.set("v.convertTo", convertTo);
                cmp.set("v.showConvertModal",true);

            },
            downloadAs: () => {
                console.log('downloading as', e.currentTarget.getAttribute('data-value'));
                cmp.set('v.singleDownload', true);

                cmp.set("v.opco", e.currentTarget.getAttribute('data-value'));

                let rowIds = [];
                rowIds.push(Id);
                cmp.set("v.rowsSelected", rowIds);
                h.loadFileData(cmp);
            }

        };

        actions[action]();
    },
    checkDone: function (cmp, e, h) {
        h.stopSpinner(cmp, e, h);
    },
    checkboxSelect: function(component, event, helper) {
        // get the selected checkbox value  
        // alert(event.getSource().get("v.value"));
        // alert(event.getSource().get("v.name"));
        
        component.find("select-all").set("v.value", false);
        
        var getCheckAllId = component.find("selection");
        var SelectAllflag = true;
        
        if( !Array.isArray(getCheckAllId)){
            if( component.find("selection").get("v.value") == false ){
                SelectAllflag = false;
            }
            
        }else{
            
            for (var i = 0; i < getCheckAllId.length; i++) {
                if( component.find("selection")[i].get("v.value") == false ){
                    SelectAllflag = false;
                    break;
                }
                
            }
            
        }
        
        
        component.find("select-all").set("v.value", SelectAllflag );
        
        var rowIds = [];
        rowIds = component.get("v.rowsSelected"); 
        
        if(event.getSource().get("v.value")){
            
            rowIds.push( event.getSource().get("v.name") );
            
            
        }else{
            
            var index = rowIds.indexOf( event.getSource().get("v.name") );
            if (index !== -1){
                rowIds.splice(index, 1);
            }
        }
        
        component.set("v.rowsSelected", rowIds);
        
        if( rowIds.length > 0 ){
            component.set("v.isDisabled", false);   
        }else{
            component.set("v.isDisabled", true);    
        }
        
    },
    
    handleClick: function (component, event, helper) {
        var theRecordId = event.currentTarget.getAttribute('data-id');
        window.open("/" + theRecordId);
    },
    selectAll: function (component, event, helper){
        // alert('select all');
        var rowIds = [];
        var slctCheck = event.getSource().get("v.value");
        var getCheckAllId = component.find("selection");
        
        if(!getCheckAllId){
            return;
        }
        
        if( !Array.isArray(getCheckAllId)){
            if (slctCheck == true) {
                component.find("selection").set("v.value", true);
                rowIds.push( component.find("selection").get("v.name") );
            } else {
                component.find("selection").set("v.value", false);
            }
            
        }
        
        if (slctCheck == true) {
            for (var i = 0; i < getCheckAllId.length; i++) {
                component.find("selection")[i].set("v.value", true);
                /* Add All from download */
                rowIds.push( component.find("selection")[i].get("v.name") );
                
            }
        } else {
            /* remove All from download */
            for (var i = 0; i < getCheckAllId.length; i++) {
                component.find("selection")[i].set("v.value", false);
                // rowIds = [];
            }
        }
        
        component.set("v.rowsSelected", rowIds);
        
        if( rowIds.length > 0 ){
            component.set("v.isDisabled", false);   
        }else{
            component.set("v.isDisabled", true);    
        }
        
    },
    
    displayReferenceTab : function(cmp, event, helper) {
        cmp.set("v.showConvertModal",false);        
        cmp.set("v.showReferncesTab",true);
        if (!cmp.get("v.userOwnership")) {
            helper.fetchCurrentUser(cmp);
        } else {
            helper.getReferenceData(cmp);
        }
    },
    
    
    
    addNewReference: function (cmp, event, helper){
        
        var searchParams = event.getParam('arguments');
        
        cmp.set("v.showReferenceModal", "true");
        var childCmp = cmp.find("referenceModal")
        childCmp.addMethod( searchParams );
		childCmp.focusCloseBtn();
    },
    
    /*
    handleRowActions: function (cmp, event, helper) {
        
        // console.log( 'yyyyyyyyyyyyyy' + JSON.stringify( event.getParam('action')));
        // console.log( 'yyyyyyyyyyyyyy' + JSON.stringify( event.getParam('row')));

        var action 	= event.getParam('action');
        var row 	= event.getParam('row');
        cmp.set("v.row", row);
        
        switch (action.value) {
            case 'edit':
                var childCmp = cmp.find("referenceModal")
 				childCmp.editMethod();
				break;
      
            case 'delete':
            	var theVal = cmp.get("v.showConfirmationModal")
                cmp.set("v.showConfirmationModal", !theVal);
                break;

            case 'download':
            	var rowIds = [];
            	rowIds.push(row.Id);
            	cmp.set("v.rowsSelected", rowIds);
            	helper.loadFileData(cmp, event, helper);
            	// helper.exportData(cmp, row.Id );
            	break;
        }
    },

    */
    
    /*
    UpdateSelectedRows : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');       
        var rowIds = [];
        for (var i = 0; i < selectedRows.length; i++){
            rowIds.push(selectedRows[i].Id);
        }

        if( rowIds.length > 0 ){
        	component.set("v.isDisabled", false);	
        }else{
        	component.set("v.isDisabled", true);	
        }

        component.set("v.rowsSelected", rowIds); // <-- Crashed datatable if "v.selectedRows"

    },

    */
    
    deleteReference : function(component, event, helper) {
        if (event.getParam('confirmationVal') === 'Y') {
            helper.deleteReference(component, component.get("v.row") );
        }    
    }, 
    
    downloadOpcoReference: function (cmp, event, helper) {
        cmp.set('v.singleDownload', true);
        // alert(event.getParam('itemId'));
        // alert(event.getParam('opco'));
        cmp.set("v.opco", event.getParam('opco') );
        // alert(cmp.get("v.opco"));
        var rowIds = [];
        rowIds.push(event.getParam('itemId'));
        cmp.set("v.rowsSelected", rowIds  );
        helper.loadFileData( cmp );
        
        
    },
    
    
    
    downloadData : function (cmp, event, helper) {
        cmp.set('v.singleDownload', false);
        helper.loadFileData( cmp );
    },
    
    downloadDataWithLogo : function (cmp, event, helper) {
        // alert(event.getSource().getLocalId());
        cmp.set('v.singleDownload', false);
        cmp.set("v.opco", event.getSource().getLocalId() );
        helper.loadFileData( cmp );
    },
    
    convertReferenceContact : function (cmp, event, helper) {
        cmp.set("v.showConvertModal",false);
        
        var row     = {'Id' : event.getSource().get("v.value") }
        var convertTo = event.getSource().getLocalId();
        cmp.set("v.row", row);
        cmp.set("v.convertTo", convertTo);
        cmp.set("v.showConvertModal",true);
        
    },
    
    reloadDatatable : function (cmp, event, helper) {
        helper.startSpinner(cmp, event, helper);
        cmp.set('v.reference_data', null );
        var loadingText = cmp.find('loading');
        $A.util.addClass( loadingText, 'show-loading');
        $A.util.removeClass(loadingText, 'hide-loading');
        helper.getReferenceData(cmp);
    },
    
    /*

    updateColumnSorting: function (cmp, event, helper) {
        var fieldName 		= event.getParam('fieldName');
        //var sortDirection 	= event.getParam('sortDirection');
        
        //cmp.set("v.sortedDirection", "asc" );
        // assign the latest attribute with the sorted column fieldName and sorted direction
        //cmp.set("v.sortedBy", fieldName);

        if(cmp.get("v.sortedDirection") === "asc" ){
        	cmp.set("v.sortedDirection", "desc");
        }else{
        	cmp.set("v.sortedDirection", "asc");
        }

        helper.sortData(cmp, fieldName, cmp.get("v.sortedDirection" ) );
    }

    */
    
    closeModel : function (cmp, event, helper) {
        cmp.set('v.showPreview', false );
    },
    
    editReference : function (cmp, event, helper) {
        cmp.set('v.showPreview', false );
        cmp.set("v.showReferenceModal", "true");
        var childCmp = cmp.find("referenceModal")
        childCmp.editMethod();
    },
    
    searchForExisting : function (component, event, helper) {
        component.set('v.showSearchPopup', true );
        var childCmp = component.find("search_existing")
        childCmp.togglePopup();    
    },

    reloadDataTable : function(component, event, helper) {
        helper.startSpinner(component, event, helper);
        var theRecordId = event.getParam("recordId");
        component.set("v.recordId", theRecordId); 
        component.set("v.showSearchPopup", false); 
        component.set("v.showConvertModal",false);        
        component.set('v.reference_data', null );
        // var loadingText = component.find('loading');
        // $A.util.addClass( loadingText, 'show-loading');
        // $A.util.removeClass(loadingText, 'hide-loading');
        helper.getReferenceData(component);
    },
	
	// Monika -- S-200794 -- Focus back on source after Add References modal is closed/cancel button is hit 
	focusonSource:function(component, event, helper) {
		const fieldId = event.getParam("fieldIdToGetFocus");
		if(fieldId == null) return;
		helper.setFocusToField(component, event, fieldId);
    }
    
})