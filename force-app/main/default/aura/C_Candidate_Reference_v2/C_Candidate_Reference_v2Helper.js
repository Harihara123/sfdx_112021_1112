({
	getReferenceData : function(cmp) {

		var action = cmp.get('c.getContactReferences');
		action.setParams({
            "contactId": cmp.get("v.recordId")
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var references = response.getReturnValue();
                
                var references_given    = references['given'];
                var references_recieved = references['recieved'];

                for (var i = 0; i < references_recieved.length; i++) {
                    let menuItems = [
                        {label: 'Edit', disabled: false, icon: 'utility:edit', action: 'edit'},
                        {label: 'Preview', disabled: false, icon: 'utility:preview', action: 'preview'}
                    ];
                    if (cmp.get('v.userOwnership') === 'AG_EMEA') {
                        menuItems.push(
                            {separator: true},
                            {label: 'Download as Aerotek', disabled: false, icon: 'utility:download', action: 'downloadAs', value: 'ONS'},
                            {label: 'Download as Aston Carter', disabled: false, icon: 'utility:download', action: 'downloadAs', value: 'AC'},
                            {label: 'Download as TEKsystems', disabled: false, icon: 'utility:download', action: 'downloadAs', value: 'TEK'},
                            /* This is newly introduced opcos for Aerotek(IND) and Aston Carter(SJA)
                            {label: 'Download as Aerotek', disabled: false, icon: 'utility:download', action: 'downloadAs', value: $A.get("$Label.c.Aerotek")},
                            {label: 'Download as Aston Carter', disabled: false, icon: 'utility:download', action: 'downloadAs', value: $A.get("$Label.c.Aston_Carter")},
                            {label: 'Download as Newco', disabled: false, icon: 'utility:download', action: 'downloadAs', value: $A.get("$Label.c.Newco")},
                            */
                            {separator: true}
                        )
                    } else {
                        menuItems.push({label: 'Download', disabled: false, icon: 'utility:download', action: 'download'},{separator: true})
                    }
                                        
                    if ( references_recieved[i].Recommendation_From__r ){
                        
                        references_recieved[i].FullName 	= references_recieved[i].Recommendation_From__r.FirstName + ' ' + references_recieved[i].Recommendation_From__r.LastName ;
                        references_recieved[i].Email 		= references_recieved[i].Recommendation_From__r.Email;
                        
                        if( references_recieved[i].Recommendation_From__r.RecordType && references_recieved[i].Recommendation_From__r.RecordType.DeveloperName ){
                            references_recieved[i].RecordType =  references_recieved[i].Recommendation_From__r.RecordType.DeveloperName;
                            if (references_recieved[i].RecordType === 'Reference') {
                                menuItems.push(
                                    {label: 'Convert to Talent Contact', disabled: false, icon: 'utility:change_owner', action: 'convert', value: 'Talent'},
                                    {label: 'Convert to Client Contact', disabled: false, icon: 'utility:change_owner', action: 'convert', value: 'Client'},
                                    {separator: true}
                                );

                            }
                        }
                        
                    }
                    else{
                        // if data from RWS
                        references_recieved[i].FullName 		= references_recieved[i].First_Name__c +' '+ references_recieved[i].Last_Name__c;
                        references_recieved[i].Email 		    = references_recieved[i].Email__c;
                        references_recieved[i].ReferRecommendation 		= true;
						references_recieved[i].RecordType =  ' ';
                        menuItems.push(
                            {label: 'Convert to Talent Contact', disabled: false, icon: 'utility:change_owner', action: 'convert', value: 'Talent'},
                            {label: 'Convert to Client Contact', disabled: false, icon: 'utility:change_owner', action: 'convert', value: 'Client'},
                            {label: 'Convert to Reference Contact', disabled: false, icon: 'utility:change_owner', action: 'convert', value: 'Reference'},
                            {separator: true}

                    );
                    }

                    references_recieved[i].CompanyName 	= references_recieved[i].Organization_Name__c;	
                    
	                if ( references_recieved[i].CreatedBy ){

	                	references_recieved[i].Created_By 	= references_recieved[i].CreatedBy.Name;
	                } 

                    if ( references_recieved[i].CreatedDate ){

                        $A.localizationService.formatDate(references_recieved[i].CreatedDate , "MMM YYYY");

                    }

                    if( references_recieved[i].Reference_Check_Completed__c ){
                        references_recieved[i].IsCompleted = 'utility:check';
                    }else{
                        references_recieved[i].IsCompleted = '';
                        menuItems.push({label: 'Delete', disabled: false, icon: 'utility:delete', action: 'delete'});
                    }
                    menuItems[menuItems.length-1].separator ? menuItems.splice(menuItems.length-1,1) : null;
                    references_recieved[i].menuItems = menuItems;
	            }
                console.log(references_recieved);
                cmp.set('v.counter_given', references_given.length );
                cmp.set('v.counter_recieved', references_recieved.length );

                cmp.set('v.references_given', references_given );
                cmp.set('v.references_recieved', references_recieved );


                if(references.length == 0 ){
                    cmp.set('v.noData', true );
                }else{
                    cmp.set('v.noData', false );
                }

                this.stopSpinner(cmp, event );

      //           var loadingText = cmp.find('loading');
			  	// $A.util.addClass( loadingText, 'hide-loading');
		    // 	$A.util.removeClass(loadingText, 'show-loading');

            } else if (state === "ERROR") {
                this.handleError(response);
                
            }
            this.stopSpinner(cmp);
        }));

        $A.enqueueAction(action);
    	
	},

	
	deleteReference : function(component, record ) {

		var action = component.get('c.deleteContactReference');
		action.setParams({
            "recordId": record.Id
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	
                component.set('v.references_given', null );
                var loadingText = component.find('loading');
                $A.util.addClass( loadingText, 'show-loading');
                $A.util.removeClass(loadingText, 'hide-loading');
                this.getReferenceData(component);

                var counter = component.get('v.counter');
                if( response.getReturnValue() ){
                    component.set('v.counter', counter - 1 );
                    this.showToast("Record has been Deleted", "success", "Success");
                }else{
                    this.showToast("Cannot delete the record, Please contact Administrator.", "error", "Error");
                }

                
                
		
            }else if (state === "ERROR") {
                this.handleError(response);
                this.stopSpinner(component);
                // this.showToast("Something went wrong, please try again", "Error", "Error");
            }

        }));


        $A.enqueueAction(action);

		
	},

	showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },

    /*

    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.table_data");
        var reverse = sortDirection !== 'asc';

        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.table_data", data);
    },

    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },

    */

    exportData : function (cmp, event, helper, data) {

        var docName = 'References for '

        if (data[0] && data[0].talent && data[0].talent.FirstName) {
            docName = docName + ' ' + data[0].talent.FirstName;
        } else{
            docName = "References"; 
        }

        if (data[0] && data[0].talent && data[0].talent.LastName) {
                docName = docName + ' ' + data[0].talent.LastName;
        }

       	this.exportToDoc(cmp, event, helper,'downloadable', docName);
        

    },


    
    exportToDoc : function (component, event, helper, element, filename) {

        // alert(component.get('v.singleDownload'));
        if( component.get('v.singleDownload') ){
            component.set('v.rowsSelected', [] ); 
        }

        window.setTimeout(
          $A.getCallback(function() {
			    var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><style>p.MsoNormal, li.MsoNormal, div.MsoNormal{margin-top:0in;margin-right:0in;margin-bottom:10.0pt;margin-left:0in;line-height:115%;font-size:11.0pt;font-family:'Calibri','sans-serif';}</style><title>Export HTML To Doc</title></head><body>";
			    var postHtml = "</body></html>";
			    
			    //var innerHTML = '<h1 style="color:red;"> Hello, My Name Is Showket </h1>';

			    var innerHTML = document.getElementById(element).innerHTML

			    var html = preHtml+ innerHTML +postHtml;

			    var blob = new Blob(['\ufeff', html]);
			    
			    // Specify link url
			    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
                // alert(url);
                // var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURI(html);
                // alert(url);
			    
			    // Specify file name
			    filename = filename?filename+'.doc':'document.doc';
			    
			    // Create download link element
			    var downloadLink = document.createElement("a");

			    document.body.appendChild(downloadLink);
			    
			    if(navigator.msSaveOrOpenBlob ){
			        navigator.msSaveOrOpenBlob(blob, filename);
			    }else{
			        // Create a link to the file
			        downloadLink.href = url;
			        
			        // Setting the file name
			        downloadLink.download = filename;
			        
			        //triggering the function
			        downloadLink.click();
			    }
			    
			    document.body.removeChild(downloadLink);
                var spinner = component.find('spinner');
                $A.util.removeClass(spinner, 'slds-show');
                $A.util.addClass(spinner, 'slds-hide');

                component.set('v.startDownload', false );

            }), 3000
        );
             
	},

    getLogoUrl : function (component) {

        // alert('Get Logo');
        // alert(component.get("v.opco"));

        var action = component.get('c.getLogoUrl');
        action.setParams({
            "name"  : component.get("v.opco"),
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state   = response.getState();
            
            if (state === "SUCCESS") {
                
                if( response.getReturnValue() != '#' ){
                    component.set("v.showLogo", true );
                }else{
                    component.set("v.showLogo", false );
                }
                
                component.set("v.logo", response.getReturnValue() );
            }else if (state === "ERROR") {
                this.handleError(response);
            }

        }));

        $A.enqueueAction(action);
    },

	loadFileData : function (component, event, helper, preview=false ) {

        component.set('v.startDownload', true );
        // var dynamicDiv = component.find('dynamic');
        // dynamicDiv.set("v.body", []);

        this.startSpinner(component, event, helper);
        this.getLogoUrl( component )

        var talentID = component.get("v.recordId");

		var action = component.get('c.loadDocumentData');
		action.setParams({
            "referenceIds"	: component.get("v.rowsSelected"),
            "talentId"		: talentID,
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state 	= response.getState();
            
            if (state === "SUCCESS") {
            	
            	var data 	= response.getReturnValue();

            	for (var i = 0, len = data.length; i < len; i++) {

            		/*Regrx removes all html tags except <p> and then replaces <p> with newline */
				  	if( data[i].reference.recommendation.hasOwnProperty('Talent_Job_Duties__c') ){
				  		data[i].reference.recommendation.Talent_Job_Duties__c = data[i].reference.recommendation.Talent_Job_Duties__c.replace(/<(?!p\s*\/?)[^>]+>/g, '');
				  		data[i].reference.recommendation.Talent_Job_Duties__c = data[i].reference.recommendation.Talent_Job_Duties__c.replace(/<p\s*[\/]?>/gi, "\n");
				  	}
				  	if( data[i].reference.recommendation.hasOwnProperty('Quality_Description__c') ){
            			data[i].reference.recommendation.Quality_Description__c = data[i].reference.recommendation.Quality_Description__c.replace(/<(?!p\s*\/?)[^>]+>/g, '');
            			data[i].reference.recommendation.Quality_Description__c = data[i].reference.recommendation.Quality_Description__c.replace(/<p\s*[\/]?>/gi, "\n");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('Competence_Description__c') ){
            			data[i].reference.recommendation.Competence_Description__c = data[i].reference.recommendation.Competence_Description__c.replace(/<(?!p\s*\/?)[^>]+>/g, '');
            			data[i].reference.recommendation.Competence_Description__c = data[i].reference.recommendation.Competence_Description__c.replace(/<p\s*[\/]?>/gi, "\n");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('Leadership_Description__c') ){
            			data[i].reference.recommendation.Leadership_Description__c = data[i].reference.recommendation.Leadership_Description__c.replace(/<(?!p\s*\/?)[^>]+>/g, '');
            			data[i].reference.recommendation.Leadership_Description__c = data[i].reference.recommendation.Leadership_Description__c.replace(/<p\s*[\/]?>/gi, "\n");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('Collaboration_Description__c') ){
            			data[i].reference.recommendation.Collaboration_Description__c = data[i].reference.recommendation.Collaboration_Description__c.replace(/<(?!p\s*\/?)[^>]+>/g, '');
            			data[i].reference.recommendation.Collaboration_Description__c = data[i].reference.recommendation.Collaboration_Description__c.replace(/<p\s*[\/]?>/gi, "\n");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('Trustworthy_Description__c') ){
            			data[i].reference.recommendation.Trustworthy_Description__c = data[i].reference.recommendation.Trustworthy_Description__c.replace(/<(?!p\s*\/?)[^>]+>/g, '');
            			data[i].reference.recommendation.Trustworthy_Description__c = data[i].reference.recommendation.Trustworthy_Description__c.replace(/<p\s*[\/]?>/gi, "\n");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('Workload_Description__c') ){
            			data[i].reference.recommendation.Workload_Description__c = data[i].reference.recommendation.Workload_Description__c.replace(/<(?!p\s*\/?)[^>]+>/g, '');
            			data[i].reference.recommendation.Workload_Description__c = data[i].reference.recommendation.Workload_Description__c.replace(/<p\s*[\/]?>/gi, "\n");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('Start_Date__c') ){
            			data[i].reference.recommendation.Start_Date__c = $A.localizationService.formatDate(data[i].reference.recommendation.Start_Date__c , "MMM YYYY");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('End_Date__c') ){
            			data[i].reference.recommendation.End_Date__c = $A.localizationService.formatDate(data[i].reference.recommendation.End_Date__c , "MMM YYYY");
            		}
            		if( data[i].reference.recommendation.hasOwnProperty('Start_Date__c') && !data[i].reference.recommendation.hasOwnProperty('End_Date__c') ){
            			data[i].reference.recommendation.End_Date__c = 'Present';
            		}


                    if(!data[i].reference.recommendation.hasOwnProperty('Recommendation_From__c')){

                        if( data[i].reference.recommendation.hasOwnProperty('Email__c') ){
                            data[i].reference.contact.Email = this.formatPhoneNumber(data[i].reference.recommendation.Email__c);
                        }


                        if( data[i].reference.recommendation.hasOwnProperty('Mobile_Phone__c') ){
                            data[i].reference.contact.MobilePhone = this.formatPhoneNumber(data[i].reference.recommendation.Mobile_Phone__c);
                        }

                        if( data[i].reference.recommendation.hasOwnProperty('Home_Phone__c') ){
                            data[i].reference.contact.HomePhone = this.formatPhoneNumber(data[i].reference.recommendation.Home_Phone__c);
                        }

                        if( data[i].reference.recommendation.hasOwnProperty('Work_Phone__c') ){
                            data[i].reference.contact.Phone = this.formatPhoneNumber(data[i].reference.recommendation.Work_Phone__c);
                        }

                        if( data[i].reference.recommendation.hasOwnProperty('Other_Phone__c') ){
                            data[i].reference.contact.OtherPhone = this.formatPhoneNumber(data[i].reference.recommendation.Other_Phone__c);
                        }

                        if( data[i].reference.recommendation.hasOwnProperty('First_Name__c') ){
                            data[i].reference.contact.FirstName     = data[i].reference.recommendation.First_Name__c;
                        }

                        if( data[i].reference.recommendation.hasOwnProperty('Last_Name__c') ){
                            data[i].reference.contact.LastName      = data[i].reference.recommendation.Last_Name__c;
                        }     

                    }else{


                        if( data[i].reference.contact.hasOwnProperty('MobilePhone') ){
                            data[i].reference.contact.MobilePhone = this.formatPhoneNumber(data[i].reference.contact.MobilePhone);
                        }
                        if( data[i].reference.contact.hasOwnProperty('Phone') ){
                            data[i].reference.contact.Phone = this.formatPhoneNumber(data[i].reference.contact.Phone);
                        }
                        if( data[i].reference.contact.hasOwnProperty('HomePhone') ){
                            data[i].reference.contact.HomePhone = this.formatPhoneNumber(data[i].reference.contact.HomePhone);
                        }
                        if(data[i].reference.contact.Title) {
                            let string = data[i].reference.contact.Title;

                            let newString = string.replace(/(\b[a-z](?!\s))/g, function(x){return x.toUpperCase();});

                            data[i].reference.contact.Title = newString;
                        }



                    }
                    

                    // if(component.get("v.opco") == 'TEK' ){
                    //         data[i].logo = $A.get('$Resource.logos') + '/TEKsystems_logotype_CMYK.png';
                    // }else if(component.get("v.opco") == 'ONS' ){
                    //         data[i].logo = 'https://cdn4.iconfinder.com/data/icons/iconset-addictive-flavour/png/sign_free_red2.png';
                    // }else if(component.get("v.opco") == 'AC' ){
                    //         data[i].logo = 'https://cdn4.iconfinder.com/data/icons/iconset-addictive-flavour/png/sign_free_red3.png';
                    // }else{
                    //         data[i].logo = 'https://cdn4.iconfinder.com/data/icons/iconset-addictive-flavour/png/sign_free_red.png';
                    // }

				}

				component.set('v.documentRecords', data );

                if(!preview){
                    //this.exportData(component, event, helper, data);
                    component.set('v.docxData', data );
                }else{
                    this.stopSpinner(component, event, helper);
                }
            	
            	
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
                this.showToast("Something went wrong, please try again", "Error", "Error");
            }

        }));

         $A.enqueueAction(action);
	},

	 formatPhoneNumber : function(s){
        if(s.length != 10 ){
            return s;
        }
	  	var s2 = (""+s).replace(/\D/g, '');
	  	var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
	  	return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
	},

    fetchCurrentUser : function(component) {
        var action = component.get('c.getCurrentUserWithOwnership');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.getReferenceData(component);
                component.set ("v.runningUser", response.getReturnValue().usr);
                component.set ("v.userOwnership", response.getReturnValue().userOwnership);                
            } else if (state === "ERROR") {
                this.handleError(response);
            }
        });
        $A.enqueueAction(action);
    }, 

    handleError : function(response){
       // Use error callback if available
        if (typeof errCallback !== "undefined") {
            errCallback.call(this, response.getReturnValue());
            // return;
        }
        // Fall back to generic error handler
        var errors = response.getError();
        if (errors) {
            console.log("Errors", errors);
            if (errors[0] && errors[0].message) {
                this.showError(errors[0].message);
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
            }else{
                this.showError('An unexpected error has occured. Please try again!');
            }
        } else {
            this.showError(errors[0].message);
            //throw new Error("Unknown Error");
        }
    },            

    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

    startSpinner: function(component, event, helper){
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show'); 
        $A.util.removeClass(spinner, 'slds-hide');  
    },

    stopSpinner: function(component, event, helper){
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');
    },


  

    /*
    getRowActions: function (cmp, row, doneCallback) {

        var Actions = [
                        {
                            'label': 'Edit',
                            'iconName': 'utility:edit',
                            'name': 'edit',
                            'value': 'edit'
                        },

                        {
                            'label': 'Delete',
                            'iconName': 'utility:delete',
                            'name': 'delete',
                            'value': 'delete'
                        },

                        {
                            'label': 'Download',
                            'iconName': 'utility:download',
                            'name': 'download',
                            'value': 'download'
                        }

                      ];

        // simulate a trip to the server
        setTimeout($A.getCallback(function () {
            doneCallback(Actions);
        }), 200);
    }
    */
	
	//Monika - Set focus to a particular field
	setFocusToField:function(component,event,cmpId){
		const fieldIdToGetFocus = document.getElementById("tlp-header-actions_AddReferences");
		if(fieldIdToGetFocus == null) return;
		setTimeout(function(){ fieldIdToGetFocus.focus(); }, 200);
	}
})