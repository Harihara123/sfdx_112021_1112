({
    doInit : function(cmp, evt, helper) {
        var contact = cmp.get("v.contact");
		var instanceURL = $A.get("$Label.c.Instance_URL");
        
		helper.loadTalentDetails(cmp);
        
        var vfOrigin = cmp.get("v.vfpage");
        var canvasUpdateEvent = $A.get("e.c:E_CanvasAppUpdateEvent");
        var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        window.addEventListener("message", function(event) {
            console.log('Integrat = '+event.origin);            

            // Handle the message
            var payLoadData = event.data;
            console.log('LT : payLoadData = '+JSON.stringify(payLoadData));

            if(payLoadData != undefined && payLoadData.payload != undefined && payLoadData.payload.status == "Close"){
                try {
                	helper.closeN2PClient(cmp);
                }
                catch(e){
                    console.log('JS E' + e);
                }
                
            } else if(payLoadData != undefined && payLoadData.payload != undefined){
                var parsedInfo = payLoadData.payload.parsedInfo
                var transactionId = payLoadData.payload.transactionId;
                var duration = payLoadData.payload.duration;
                console.log("integrator: " + duration);
                try{
                    urlEvent.setParams({
                    "recordId": cmp.get("v.recordId"), // should be Talent Id
                    "recId": cmp.get("v.recordId"),
                    "parsedInfo": parsedInfo,
                    "transactionId": transactionId,
                    "n2pduration": duration,
                    "source": "NotesParser"
                    });
                    helper.closeN2PClient(cmp);
                    urlEvent.fire();
                }
                catch(e){
                    console.log('JS E'+ e);
                }
            }
        }, false); 

    },
    canAppLoaded: function(cmp, evt, helper) {
        console.log('canvas Loading.2 ');
        Sfdc.canvas(function() {
            console.log('canvas Loaded. 2');
        }); 
    }
})