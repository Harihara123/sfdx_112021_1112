({
    raiseUpdateTalentEvent: function (cmp,parsedInfo) {

        var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        if(urlEvent != undefined){        
        var canvasUpdateEvent = $A.get("e.c:E_CanvasAppUpdateEvent");
        urlEvent.setParams({
            "recordId": cmp.get("v.recordId"),
            "recId": "",
            "parsedInfo": parsedInfo,
            "transactionId": transactionId,
            "n2pduration": duration,
            "source": "NotesParser"
        });
        urlEvent.fire();
        console.log('raiseUpdateTalentEvent '+JSON.stringify(parsedInfo));
     }
    },
    closeN2PClient:function(component){
        var parentCmp = component.get('v.parentCmp');
        console.log("HERE_________");
        console.log(parentCmp);
        parentCmp.set("v.enableN2PButton",true);
        component.set('v.parentCmp', null);
        try {
            $A.get('e.force:refreshView').fire();            
        } catch (e) {
            console.log('exception in close utility bar'+e.message);
        }
        component.destroy();
    },
    loadTalentDetails:function(cmp){
        var contactBaseFields=cmp.get("v.contactBaseFields");
        var accId = "";
        var city = "";
        var state = "";
        var country = "";
        var officeName = "";
        var officeCode = "";
        var userName = "";
        var userEmail = "";

        var userDetails = cmp.get("v.userDetails");
        var userObject = cmp.get("v.runningUser");
        var talentContId = cmp.get("v.recordId");
        var talentAccId = cmp.get("v.talentId");
        var userId = "";

        console.log(JSON.stringify(userDetails));

        if(userObject != undefined){
            userId = userObject.Id;
        }

        if(contactBaseFields != undefined){
            city = encodeURI(contactBaseFields.MailingCity != undefined ? contactBaseFields.MailingCity : "");
            state = encodeURI(contactBaseFields.MailingState != undefined ?contactBaseFields.MailingState : "");
            country = encodeURI(contactBaseFields.MailingCountry != undefined ? contactBaseFields.MailingCountry : "");            
        }

        if(userDetails != undefined){
            officeName = encodeURI(userDetails.Office__c != undefined ? userDetails.Office__c : "");
            officeCode = encodeURI(userDetails.Office_Code__c != undefined ? userDetails.Office_Code__c : "");
            userName = encodeURI(userDetails.Name != undefined ? userDetails.Name : "");
            userEmail = encodeURI(userDetails.Email != undefined ? userDetails.Email : "");
        }
        // Prod defect fix - D-14686
        if(!$A.util.isEmpty(userName)){
            if(userName.includes('\'')){ 
               var regex = new RegExp("'","g");
               // VF page unable to decode %27 so removing '
               userName = userName.replace(regex, "");
            }
            if(userName.includes('-')){
                const regex2 = /\-/g;
                userName = userName.replace(regex2, '%2D');
            }
        }
        cmp.set("v.talentCity",city);
        cmp.set("v.talentState",state);
        cmp.set("v.talentCountry",country);

        cmp.set('v.OfficeName', officeName);
        cmp.set('v.OfficeCode', officeCode);
        cmp.set('v.UserName', userName);
        cmp.set('v.UserEmail', userEmail);
        
        var hostURL = $A.get("$Label.c.ATS_INSTANCE_LIGHTNING_URL");//$A.get("$Label.c.ATS_INSTANCE_LIGHTNING_URL");
        var vfpage = hostURL;
        cmp.set("v.hostURL",hostURL);
        var vfpage = hostURL;
        var userLocation = cmp.get("v.userLocation");
        var parsingModal = cmp.get("v.parsingModal"); 
        vfpage = vfpage + '/apex/N2PCanvas?userLocation='+ userLocation +'&talentCity='+city + '&talentState=' + state + '&talentCountry=' + country+ '&hostURL='+hostURL+'&parsingModal='+parsingModal+'&talentContId='+talentContId+'&talentAccId='+talentAccId+'&userId='+userId+'&userName='+userName+'&userEmail='+userEmail+'&officeName='+officeName+'&officeCode='+officeCode;
        cmp.set("v.vfpage",vfpage);
    }
})