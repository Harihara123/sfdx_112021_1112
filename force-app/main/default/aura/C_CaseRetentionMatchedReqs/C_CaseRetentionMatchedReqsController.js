({
	handleRecordUpdated : function(cmp, event, helper) {
    	var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var caserecord = cmp.get("v.caseRecord");
            cmp.set("v.ContRecordId", caserecord.fields.ContactId.value);        
	   		var action = cmp.get("c.getCaseCurrentUserTalentObj");
			try {
        		action.setParams({"recId": cmp.get("v.ContRecordId")});
				action.setCallback(this, function(response) {
                	if (response.getState() === "SUCCESS"){
                    	var res = response.getReturnValue();
                        if(res){
                        	cmp.set('v.runningUser',res.UserModel.usr);
                            cmp.set('v.runningUserCategory', res.UserModel.userCategory);
                            cmp.set('v.usrOwnership', res.UserModel.userOwnership);
                            let contObj={"fields":
                            	            {
                                                "Id":{"value":cmp.get("v.ContRecordId")},
                                                "Name":{"value":res.ContactRec.Name},
                                                "AccountId":{"value":res.ContactRec.AccountId},
                                                "Lead_Talent__c":{"value":res.ContactRec.Lead_Talent__c}
                                             }                                            
                                         }
                             cmp.set("v.contactRecord",contObj);
                             helper.loadCmps(cmp);
						}
                 	}						
                });
        		$A.enqueueAction(action);
			}
			catch (err) {
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
							"title": "Error!",
							"message": "Error during Case load. Please refresh the page.",
							"type": "error"
						});
				toastEvent.fire();
			}	
        }
	}	
})