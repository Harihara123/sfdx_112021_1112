({
	createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){                
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    loadCmps : function(component){
        var recordId = component.get("v.ContRecordId") ;
        var contactRecord = component.get("v.contactRecord");
        var runningUserCategory = component.get("v.runningUserCategory");
        var usrOwnership = component.get("v.usrOwnership");
        var params = {recordId: recordId
                      , contactRecord :contactRecord
                      , runningUserCategory : runningUserCategory
                      , usrOwnership : usrOwnership
                      ,ParentName : "Case"}; 
        params['usrOpco'] = component.getReference('v.runningUser.OPCO__c');
        var componentName =   'c:C_TalentMatchedREQs';
        var targetAttributeName = 'v.C_TalentMatchedREQs';
        this.createComponent(component, componentName, targetAttributeName,params);
    }    
})