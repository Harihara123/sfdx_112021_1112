({
	doInit: function (component, event, helper) {
		console.log('recordId-- In Details do init'+component.get("v.recordId"));
		//component.find('recordLoader').reloadRecord(true);
		//console.log('MailingAddress value---'+component.get("v.contactRecordFields"));	
	},
	refreshData: function (component, event, helper) {		
		component.find('recordLoader').reloadRecord(true);
		//console.log('MailingAddress value---'+component.get("v.contactRecordFields.MailingCity"));	
	},
	sectionOne : function(component, event, helper) {
       helper.helperFun(component,event,'articleOne');	   
    },
    sectionTwo : function(component, event, helper) {
       helper.helperFun(component,event,'articleTwo');
    },
	sectionThree : function(component, event, helper) {
       helper.helperFun(component,event,'articleThree');
    },
	sectionFour : function(component, event, helper) {
       helper.helperFun(component,event,'articleFour');
    },
	sectionFive : function(component, event, helper) {
       helper.helperFun(component,event,'articleFive');
    },
	sectionSix : function(component, event, helper) {
       helper.helperFun(component,event,'articleSix');
    },
	sectionSeven : function(component, event, helper) {
       helper.helperFun(component,event,'articleSeven');
    },
	sectionEight : function(component, event, helper) {
       helper.helperFun(component,event,'articleEight');
    },
	navigateLinkedIn : function(component, event, helper) {
		//Find the text value of the component with aura:id set to "address"
		var firstName = component.get("v.contactRecordFields.FirstName");
		var lastName = component.get("v.contactRecordFields.LastName");
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
		  "url": 'http://www.linkedin.com/vsearch/p?firstName='+firstName+'&lastName='+lastName
		});
		urlEvent.fire();
	},
	linkedIn : function(component, event, helper) {
		//Find the text value of the component with aura:id set to "address"
		var lURL = component.get("v.contactRecordFields.LinkedIn_URL__c");
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
		  "url": lURL
		});
		urlEvent.fire();
	},
	originalReffer : function(component, event, helper) {
		//Find the text value of the component with aura:id set to "address"
		var oURL = component.get("v.contactRecordFields.Original_Referrer_URL__c");
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
		  "url": oURL
		});
		urlEvent.fire();
	},
	
	navigateToAccountRecord : function(component,event, helper){
        var recordID= component.get("v.contactRecordFields.AccountId");
        window.open('/' + recordID,'_blank');
    },
	navigateToCreatedByRecord : function (component, event, helper) {
		var recordId = component.get("v.contactRecordFields.AccountId");
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": recordId ,
		  "slideDevName": "detail"
		});
		navEvt.fire();
	},	
	navigateToModifiedByRecord : function (component, event, helper) {
		var recordId = component.get("v.contactRecordFields.LastModifiedById");
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": recordId ,
		  "slideDevName": "detail"
		});
		navEvt.fire();
	},
	navigateToOwnerRecord : function (component, event, helper) {
		var recordId = component.get("v.contactRecordFields.AccountId");
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": recordId ,
		  "slideDevName": "detail"
		});
		navEvt.fire();
	},
	
	navigateToReportsToRecord : function(component,event, helper){
        var recordID= component.get("v.contactRecordFields.ReportsToId");
        window.open('/' + recordID,'_blank');
    },
	navigateToLastModifiedByRecord : function (component, event, helper) {
		var recordId = component.get("v.contactRecordFields.AccountId");
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": recordId ,
		  "slideDevName": "detail"
		});
		navEvt.fire();
	},
	emailMain : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Email"));
    },
    
    emailAlt : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Other_Email__c"));
    },
	callMobilePhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.MobilePhone"));
    }, 
    callPhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.Phone"));
    },
    
    callOtherPhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.OtherPhone"));
    },
	formatData: function (cmp, e, h) {
		const data = cmp.get('v.contactRecordFields');		
		const addressFields = [
			'Street',
			'City',
			'State',
			'Country',
			'PostalCode'
		]
		let mailingAddress = [];
		let otherAddress = [];
		if(data != null){
			addressFields.forEach((field, i) => {
				mailingAddress.push(
					(data[`Mailing${field}`]?' '+data[`Mailing${field}`]:'')
				);
				otherAddress.push(
					(data[`Other${field}`]?' '+data[`Other${field}`]:'')
				);
			})
			if (mailingAddress.length) {
				mailingAddress = mailingAddress.join().trim();
				
				cmp.set('v.mailingAddress', mailingAddress);
			}
			if (otherAddress.length) {
				otherAddress = otherAddress.join().trim();
			
				cmp.set('v.otherAddress', otherAddress);
			}
		}
		
	},
	
})