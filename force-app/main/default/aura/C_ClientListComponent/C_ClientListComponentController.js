({
    doInit: function (component, event, helper) {
        helper.checkAllSelected(component);
    },
    sortByName: function (component, event, helper) {
         helper.sortRecordList(component, helper, 'Name');    
	},
    sortByAccountName: function (component, event, helper) {
         helper.sortRecordList(component, helper, 'Account.Name');
    },
    sortByStatus: function (component, event, helper) {
         helper.sortRecordList(component, helper, 'Status');
    },
    sortByTitle: function (component, event, helper) {
        helper.sortRecordList(component, helper, 'Title');
    },
    sortByDNC: function (component, event, helper) {
        helper.sortRecordList(component, helper, 'DNC');
    },
    sortByLastActivity: function (component, event, helper) {
        helper.sortRecordList(component, helper, 'Last Activity');
    },
    bringUpDeleteModal: function(cmp, event, helper) {
        helper.handleCreateComponent(cmp, event);
    },
    checkAll: function(cmp, event, helper) {
        var rowItems = cmp.find('clientRowItem');
        var selectedList = cmp.get('v.selectedList');

        selectedList = [];
        if(rowItems.length > 1) {
            for(var i = 0; i < rowItems.length; i++) {
                rowItems[i].set('v.isSelected', cmp.get('v.allSelected'));
                if(cmp.get('v.allSelected')) {
                    selectedList.push(rowItems[i].get('v.client'));
                }
            }
        } else {
            if(Array.isArray(rowItems)) {
                rowItems[0].set('v.isSelected', cmp.get('v.allSelected'));
                if(cmp.get('v.allSelected')) {
                    selectedList.push(rowItems[0].get('v.client'));
                }
            } else {
                rowItems.set('v.isSelected', cmp.get('v.allSelected'));
                if(cmp.get('v.allSelected')) {
                    selectedList.push(rowItems.get('v.client'));
                }
            }
        }

        cmp.set('v.selectedList', selectedList);
    },
    removeSelectedContact: function(component, event, helper) {
        helper.unselectRemovedContact(component, event);
    },
    checkAllSelected: function (component, event, helper) {
        helper.checkAllSelected(component);
    },
    noteEntered: function(component, event, helper) {
        component.set("v.noteText", event.target.value);
    },
    saveAndClose: function(component, event, helper) {
        component.set("v.showNotesModal", false);
        let tableData = component.get("v.tableData");
        let clientInModal = component.get("v.clientInModal");
        let clientIndex = tableData.findIndex(client => client.Id === clientInModal.Id);
        if (clientIndex != -1) {
            tableData[clientIndex].Notes = component.get("v.noteText");
            component.set("v.tableData", tableData);
			helper.setNotes(component,"update",clientInModal.Id);
        }        
    },
    cancelAndClose: function(component, event, helper) {
        component.set("v.showNotesModal", false);        
    },
    deleteNote: function(component, event, helper) {
        let c = confirm(`Deleting the note for ${component.get("v.clientInModal").Name} will delete all content and the note.  Are you sure?`);
	    if (c) {
    		component.set("v.showNotesModal", false);
        	let tableData = component.get("v.tableData");
	        let clientInModal = component.get("v.clientInModal");
    	    let clientIndex = tableData.findIndex(client => client.Id === clientInModal.Id);
       		if (clientIndex != -1) {
            	tableData[clientIndex].Notes = "";
            	component.set("v.tableData", tableData);
				helper.setNotes(component,"delete",clientInModal.Id);
        	}
		}                
    },
    previewFile: function(cmp, e, h) {
        const navService = cmp.find('navService');
        const doc = e.target.getAttribute('data-id')

        const docMap = {
            terms: $A.get("$Label.c.ATS_RESTRICTED_TERMS_DOC_ID"),
            eeo: $A.get("$Label.c.ATS_EEO_POLICY_DOC_ID")
        }

        e.preventDefault();
        if (doc && docMap[doc]) {
            navService.navigate({
                type: 'standard__namedPage',
                attributes: {
                    pageName:'filePreview'
                },
                state: {
                    recordIds: docMap[doc],
                    selectedRecordId: docMap[doc]
                }
            });   
        }        
    }
})