({
	sortRecordList: function (cmp, helper, sortField) {
        var recordList = cmp.get("v.tableData");
        var sortDirection = cmp.get("v.sortDirection");
        var newSortDirection = sortDirection === 'ASC' ? 'DESC' : 'ASC';

        var firstVal = newSortDirection === 'ASC' ? 1 : -1;
        var secondVal = newSortDirection === 'ASC' ? -1 : 1;

        recordList.sort(function(a,b) {
            
            var compareFields = helper.setSortFieldValues(a, b, sortField);

            var fieldA = compareFields[0];
            var fieldB = compareFields[1];

            if(fieldA < fieldB) {
                return secondVal;
            }

            if(fieldA > fieldB) {
                return firstVal;
            }

            return 0;
        });

        cmp.set("v.tableData", recordList);
        cmp.set("v.sortDirection", newSortDirection);
        cmp.set("v.sortColumn", sortField);
    },
    setSortFieldValues: function (a, b, sortField) {
        var fieldA, fieldB;
        var valA = a
        var valB = b;

        switch(sortField) {        
            case 'Account.Name':
                fieldA = valA['Account']['Name'] ? valA['Account']['Name'].toUpperCase() : '';
                fieldB = valB['Account']['Name'] ? valB['Account']['Name'].toUpperCase() : '';
                break;

            case 'Status':
                fieldA = valA['Customer_Status__c'] ? valA['Customer_Status__c'] : '';
                fieldB = valB['Customer_Status__c'] ? valB['Customer_Status__c'] : '';
                break;

            case 'Title':
                fieldA = valA['Title'] ? valA['Title'].toUpperCase() : '';
                fieldB = valB['Title'] ? valB['Title'].toUpperCase() : '';
                break;

            case 'DNC':
                fieldA = valA['Account']['Do_Not_Contact__c'] ? valA['Account']['Do_Not_Contact__c'] : false;
                fieldB = valB['Account']['Do_Not_Contact__c'] ? valB['Account']['Do_Not_Contact__c'] : false;
                break;

            case 'Last Activity':
                fieldA = valA['LastActivityDate'] ? valA['LastActivityDate'] : '';
                fieldB = valB['LastActivityDate'] ? valB['LastActivityDate'] : '';
                break;

            case 'Name':
            default:
                fieldA = valA['Name'] ? valA['Name'].toUpperCase() : '';
                fieldB = valB['Name'] ? valB['Name'].toUpperCase() : '';
                break;

                
        }

        return [fieldA, fieldB];
    },
    handleCreateComponent: function (cmp, event) {
        var logger = event.getParam('contact');
        console.log(logger.Id);
        console.log(cmp.get("v.list"));
        $A.createComponent(
            "c:C_RemoveContactModal",
            {"record" : event.getParam('contact'), "list" : cmp.get("v.list"), "selectedList" : cmp.get("v.selectedList")},
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    cmp.set('v.C_RemoveContactModal', newComponent);
                }
                else if (status === "INCOMPLETE") {
                }
                else if (status === "ERROR") {
                }
            }
        );
    },
    unselectRemovedContact: function (component, event) {
        let removedContact = event.getParam('contactId');
        let rowItems = component.find('clientRowItem');
        let selectedContacts = component.get('v.selectedList');

        if(rowItems.length > 1) {
            for(var i = 0; i < rowItems.length; i++) {
                if(rowItems[i].get('v.client').Id === removedContact) {
                    rowItems[i].set('v.isSelected', false);
                    break;
                }
            }
        } else {
            if(Array.isArray(rowItems)) {
                if(rowItems[0].get('v.client').Id === removedContact) {
                    rowItems[0].set('v.isSelected', false);
                }
            } else {
                if(rowItems.get('v.client').Id === removedContact) {
                    rowItems.set('v.isSelected', false);
                } 
            }
        }


        let tempList = selectedContacts.splice(selectedContacts.map(x => x.Id).indexOf(removedContact), 1);

        if(selectedContacts.length < rowItems.length || !Array.isArray(rowItems)) {
            component.set('v.allSelected', false);
        }

        component.set('v.selectedList', selectedContacts);
    },
    checkAllSelected: function (cmp) {
        var rowItems = cmp.find('clientRowItem');
        let selectedList = cmp.get('v.selectedList');
        let allSelected = false;

        if(Array.isArray(rowItems)) {
            allSelected = rowItems.length === selectedList.length;
        } else {
            allSelected = selectedList.length === 1;
        }

        cmp.set('v.allSelected', allSelected);
    },
    setNotes : function(component, operation, contactId) {
        if (operation === "delete") {
			component.set("v.noteText","");
		 }
		var action = component.get("c.setNotesInContactTag");
		action.setParams({
			notes : component.get("v.noteText"),
			tagId: component.get("v.list"),
			contactId : contactId
		});
		$A.enqueueAction(action);
   }
})