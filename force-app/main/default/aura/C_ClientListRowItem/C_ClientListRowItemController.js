({
    doInit : function(component, event, helper){
        helper.setSelected(component, event, helper);
    },
    bringUpDeleteModal: function(cmp, event, helper) {
        var record = cmp.get("v.client");
        var tagList = cmp.get("v.list");
        var cmpEvent = cmp.getEvent("removeContactEvent");
        cmpEvent.setParams({"contact":record});
        cmpEvent.fire();
    },
    selectClient: function (component, event, helper) {
        var isSelected = component.get('v.isSelected');
        var selectedList = component.get('v.selectedList');

        if(isSelected) {
            selectedList.push(component.get('v.client'));
            component.set('v.selectedList', selectedList);
        } else {
            var rowclient = component.get('v.client');
            selectedList = selectedList.filter(client => client.Id !== rowclient.Id);
            component.set('v.selectedList', selectedList);
        }

    },
    openNotesModal: function(component, event, helper) {
        helper.openNotesModal(component);
    }
})