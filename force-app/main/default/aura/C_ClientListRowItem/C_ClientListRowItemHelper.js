({
    setSelected : function(component, event, helper) {
        var rowClient = component.get('v.client');
        var clientList = component.get('v.selectedList');

        var isSelected = false;
        if(rowClient !== undefined && rowClient !== null) {
            isSelected = clientList.map(function(x) {return x.Id}).includes(rowClient.Id);
        }

        component.set('v.isSelected', isSelected);
    },
    openNotesModal: function(component) {
        component.set("v.showNotesModal", !component.get("v.showNotesModal"));
        component.set("v.clientInModal", component.get("v.client"));
    }
})