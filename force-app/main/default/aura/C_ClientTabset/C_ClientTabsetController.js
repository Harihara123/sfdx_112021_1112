({
	doInit : function (component, event, helper) {
		helper.setupActivities(component);	
	},
	refreshTab : function (component, event, helper) {
		component.set("v.enableActivitiesTab",false);
		component.set('v.selectedTab','detailsTab');
	},
	tabSelected : function (component, event, helper) {		
		if(component.get('v.selectedTab') === 'activitiesTab') {
			helper.setupActivities(component);  
			component.set("v.enableActivitiesTab",true);
		}
		else {
			component.set("v.enableActivitiesTab",false);
		}
	}
})