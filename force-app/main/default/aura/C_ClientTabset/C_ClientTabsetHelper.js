({
	setupActivities : function(component) {
		
		let accountId = component.get("v.accountId");
		console.log('accountId-----'+accountId+'---contactId---'+component.get("v.contactId"));
		if(accountId) {
			let contactRec = {"fields":{"AccountId":{"value":component.get("v.accountId")}, "id":{"value":component.get("v.contactId")}}};
			component.set("v.contactRecord", contactRec);
		}
	}
})