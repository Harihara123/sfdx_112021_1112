({
	doInit : function(component, event, helper) {
		 var action = component.get("c.communitiesPageRedirect");
        action.setParams({ AccId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result =  response.getReturnValue();
                //alert("From server: " + result);
                if(result !== null){
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": result
                    });
                    navEvt.fire();  
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Talent Not Available!!",
                        "message": "Could not find the appropriate Talent.",
                        "type":"error",
                        "mode":"pester"
                    });
                    toastEvent.fire();  
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	}
})