({
    doInit:function(component,event,helper){
		var dataBackupSetup =  $A.get("$Label.c.ATS_TLP_Card_Backup");
		var userId = $A.get("$SObjectType.CurrentUser.Id");
		/*
			Label contains comma seperated values of userids. (When the feature is piloting). Switch the label to GA to make it generally available for all users.
		*/
		if(dataBackupSetup.includes(userId)||dataBackupSetup.includes('GA')){
			component.set('v.usrHasAccess',true);
		}
		else{
			console.log('user doesnt have access to component data backup feature');
			return;
		}
		var hasBackup = component.getDataBackup();
		var trackObj = component.get('v.trackObj');
		if(!$A.util.isEmpty(trackObj)){
			component.set('v.hasBackup',true);
			//var trackObj = {};
			//component.getDataBackup();
			//trackObj = component.get('v.trackObj');
			//console.log('----Data backup cmp---abt to show recovery modal--');
			var trackObjStr = JSON.stringify(trackObj);
			var modalBody;
			$A.createComponents([["c:C_CompDataBackupModal",{"dataRetrieved":trackObjStr,"callingCard":component.get('v.callingCard'),"parentBackupCmp":component,"eraseBackup":component.get("v.eraseBackup")}]], 
				function(components, status){
					if (status === "SUCCESS") {
						modalBody = components[0];
						component.find('overlayLib').showCustomModal({
							header: "Information Recovery",
							body: modalBody,
							showCloseButton: false,
							closeCallback: function(liveParent) {
								//console.log('Data retrieval modal closed!!');
							}
						});
					}
				});

	  }
    },
    setDataBackup : function(component, event, helper) {
		var key;
        var contactId,cardNumber,trackObj;
        var params = event.getParam('arguments');
        if (params) {
            trackObj = params.trackObj;
        }
		component.set('v.trackObj',trackObj);
        key = component.get('v.callingCard').getName()+'_'+component.get('v.recordId');   
		//console.log('----Data backup cmp--set data backup---trackObj:'+JSON.stringify(trackObj));  
      	//rajeesh removing encoding as there are many characters that are not encodeable.
        //window.sessionStorage.setItem(key,window.btoa(JSON.stringify(trackObj)));
        window.sessionStorage.setItem(key,JSON.stringify(trackObj));
	},
    getDataBackup: function(component, event, helper){
		var decObj;
        var key =component.get('v.callingCard').getName()+'_'+component.get('v.recordId');
        var encStr = window.sessionStorage.getItem(key);
        if(!$A.util.isEmpty(encStr)){
            //decObj = JSON.parse(window.atob(encStr));
            decObj = JSON.parse(encStr);
			//console.log('----Data backup cmp--get data backup:decObj:'+window.atob(encStr));  
        }

		component.set('v.trackObj',decObj);
    },
	clearDataBackup: function(component,event,helper){
		var key = component.get('v.callingCard').getName()+'_'+component.get('v.recordId');
		window.sessionStorage.removeItem(key);
		//console.log('----Data backup cmp--clear data backup---');  
	}
})