({
	doInit:function(component,event,helper){
		//console.log('inside data retrieval modal');
	},
	retrieveAndUpdateCard : function(component, event, helper) {
		var parent = component.get('v.callingCard');
		var dataStr = component.get('v.dataRetrieved');
		var data = JSON.parse(dataStr);
		//parent.set('v.edit',true);
		for(var key in data){
			//get keys of first level object- those are variables from calling card. Put it back on the instance.
			var k = 'v.'+key;
			//console.log('key'+key+' :val'+data[key]+' k'+k);
			parent.set(k,data[key]);
		}
		//Retain the backup while recovery.
		//parent.set("v.eraseBackup",true);
		//component.get('v.parentBackupCmp').clearDataBackup();
		component.set('v.callingCard',null);//will this de refer the original reference. before closing the modal?
		component.set('v.parentBackupCmp',null);//dereference immediate parent too.
		//component.set("v.eraseBackup",true);
		var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Information recovery successful.\n Please save the record",
            "type": "success"
        });
        toastEvent.fire();
		component.find('overlayLib').notifyClose();
	},
	cancelModal: function(component,event,helper){
		
		component.get('v.parentBackupCmp').clearDataBackup();//clear data backup
		component.set("v.eraseBackup",true);

		component.set('v.callingCard',null);//will this de refer the original reference. before closing the modal?
		component.set('v.parentBackupCmp',null);
		component.find('overlayLib').notifyClose();
	}
})