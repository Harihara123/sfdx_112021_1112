({
	handleConfirm : function(component, event, helper) {
		var cmpEvent = component.getEvent("onConfirm");
		cmpEvent.fire();
        //console.log("handleConfirme evnt fired");
	},
    handleCancel : function(component, event, helper) {
		var cmpEvent = component.getEvent("onCancel");
		cmpEvent.fire();	
		helper.closeDailog(component,event);        
	},
    openDailog: function(component, event, helper){
        var params  = event.getParam('arguments');
        if(params){
        //console.log('openDailog called...');
        //console.log('header ',params.header);
        //console.log('content ',params.content);
        component.set("v.dailogHeader",params.header);
        component.set("v.dailogContent",params.content); 
        }
        
        helper.openDailog(component,event);
    },
    closeDailog: function(component, event, helper){
        helper.closeDailog(component,event);
    }
})