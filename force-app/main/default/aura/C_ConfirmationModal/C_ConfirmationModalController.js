({
	showConfirmationModal : function (component, event, helper) {
		let source = component.get('v.isMerge') ? 'slds-modal_medium slds-fade-in-' : 'slds-modal_small slds-fade-in-';
			
        helper.toggleClass(component,'backdrop','slds-backdrop--');
        helper.toggleClass(component,'modaldialog', source);
    },
    
    openRecord : function (component, event, helper) {
        
        var talentId = event.target.id;
		let recordId;
        
        if( component.get("v.data[0].masterId") == talentId ){
			recordId = component.get("v.talentIds.masterRecId");
        }else{
            recordId = component.get("v.talentIds.duplicateRecId");
        }
        
        window.open('/lightning/r/Contact/' + recordId +'/view','_Blank');
    },

	confirmedYes : function (component, event, helper) {
		helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');

        var modalCloseEvt = component.getEvent("closeConfirmationModalEvent");
        modalCloseEvt.setParams({ "confirmationVal" : "Y"});

        modalCloseEvt.fire();
    },

	confirmedNo : function (component, event, helper) {
		helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');

        var modalCloseEvt = component.getEvent("closeConfirmationModalEvent");
        modalCloseEvt.setParams({ "confirmationVal" : "N"});

        modalCloseEvt.fire();
    },

    closeModal : function (component, event, helper) {
        helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
        helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');
    },
    closeMergeModal : function (component, event, helper) {
        helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
        helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');
        
        var modalCloseEvt = component.getEvent("closeConfirmationModalEvent");
        modalCloseEvt.setParams({ "confirmationVal" : "N"});

        modalCloseEvt.fire();
    }

})