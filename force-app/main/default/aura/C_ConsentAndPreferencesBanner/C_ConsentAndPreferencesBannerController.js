({
    doInit: function (component, event, helper) {
        helper.getBannerMsg(component);
    },
    handleRecordUpdated : function(component, event, helper){
    	helper.getBannerMsg(component);
    },
    //Sandeep: send consent email//
    sendConsentEmail: function(component, event, helper){
        console.log('sendConsentEmail called....');
         var dailog =component.find("confirmationdailog");
         dailog.closeDailog();
         if(component.get("v.contactRecordFields.Email") /*|| 
           component.get("v.contactRecordFields.Other_Email__c") || 
           component.get("v.contactRecordFields.Work_Email__c")*/){
        var action =component.get("c.sendConsentPreferenceEmail");
        var recordId = component.get("v.recordId");
        action.setParams({"conId" : recordId, "consentType": "Email"});
        action.setCallback(this, function(response){
            var state = response.getState();  
            if(state === 'SUCCESS'){ 
                 component.set("v.bannerMsg.Allow_Resend__c",false);
               
                helper.showSuccessToast(component,'Consent and Preferences Request has been sent to this contact.');//sandeep
            }else if(state === 'ERROR'){
                helper.showErrorToast(component,'Failed to send Email.');//sandeep
            }  
        });
        $A.enqueueAction(action); 
         }else{
             helper.showErrorToast(component,'Talent must have Primary Email to Initiate Consent.');
         }
    },
    closeToast : function(component, event){
        var toastcmp = component.find('toastcmp');
        $A.util.addClass(toastcmp, "slds-hide");
    },
    /*Sandeep */
    showConfirmationDailog: function(component, event,helper){
              if(component.get("v.contactRecordFields.Email")){
        //console.log('showConfirmationDailog called');
        var dailog =component.find("confirmationdailog");
        //dailog.addEventHandler("onConfirm", component.getReference("c.sendConsentEmail"));
        //console.log('event handler added ..');
        dailog.openDailog("Request Consent Confirmation ","You are requesting consent for communication from the contact. It may take up to 15 minutes for this email to arrive. Click CONFIRM to continue or CANCEL to go back.");
        //console.log('openDailog called ..');
         }else{
            helper.showErrorToast(component,'Talent must have Primary Email to Initiate Consent.');
        }
    }
})