({
    getBannerMsg:function(component){
        var action =component.get("c.getConsentPreferencesBannerProperties");
        var bannerMsg = {};
        var recordId = component.get("v.recordId");
        action.setParams({"contactID" : recordId});
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(!$A.util.isEmpty(response.getReturnValue())){ 
                component.set('v.recordType', bannerMsg.is_Talent__c ? 'Talent': 'Client');
				component.set('v.bannerMsg',response.getReturnValue());
                    
                    }
                }
        });
        $A.enqueueAction(action);
    },
    showSuccessToast : function(component,message){
        var toastParams = {'type':'success','message':message,'title':'Success','mode':'dismissible'};
        component.set("v.toastParams",toastParams);  
        component.set("v.showToast",true);
    },
    showErrorToast : function(component,message){
        var toastParams = {'type':'error','message':message,'title':'Success','mode':'dismissible'};
        component.set("v.toastParams",toastParams);  
        component.set("v.showToast",true);
    }
})