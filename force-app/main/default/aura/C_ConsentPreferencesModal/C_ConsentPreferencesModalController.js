({
    doInit: function (component, event, helper) {
        var contactID = component.get("v.contactID");
        if(!$A.util.isEmpty(contactID)){
            component.set("v.recordId", contactID);
        }
        helper.populateConsentPref(component);
		
    },
    
    saveCP : function(component, event, helper){
        helper.saveConsentPref(component);
    },
    cancelCPChanges : function(component, event, helper){
        //helper.cancelChanges(component);
		helper.closeModal(component);
    },
    
    onEmailConsentChange : function(component, event, helper) {
        var ucpList = component.get("v.cpList");
        if(ucpList[1].fieldValue === 'No') {
            ucpList[3].fieldValue = '';
        }
        component.set("v.cpList", ucpList);
    },	
    onMobileConsentChange : function(component, event, helper) {
        var ucpList = component.get("v.cpList");
        if(ucpList[2].fieldValue === 'No') {
            ucpList[4].fieldValue = '';
        }
        component.set("v.cpList", ucpList);
    },	
    closeModal : function(component, event, helper) {
        helper.closeModal(component);
    },
    //Sandeep: send consent email//
    sendConsentEmail: function(component, event, helper){
        helper.toggleModalDisplay(component);
        // component.find("overlayLib").notifyClose();
        var dailog =component.find("confirmationdailog");
        dailog.closeDailog();
		let consentType = component.get("v.requestedConsent");

        var action =component.get("c.sendConsentPreferenceEmail");
        var recordId = component.get("v.recordId");
        action.setParams({"conId" : recordId, "consentType": consentType});
        action.setCallback(this, function(response){
            var state = response.getState();  
            if(state === 'SUCCESS'){ 
                component.set("v.enableSendConsent",false);
                var refreshBanner = $A.get("e.c:E_RecordUpdatedAppEvent");
                refreshBanner.fire();
                //helper.showSaveToastStd('Consent and Preferences Request has been sent to this contact.','');//sandeep
				helper.showSaveToastStd($A.get("$Label.c.ATS_CONSENT_PREFERENCES_SENDCONTACT"),'');//sandeep
                //helper.showSuccessToast(component,'Consent and Preferences Request has been sent to this contact.');
                helper.closeModal(component);
            }else if(state === 'ERROR'){
                //helper.showErrorToast(component,'Failed to send Email.');//sandeep
                helper.showErrorToastStd($A.get("$Label.c.ATS_FAILED_TO_SEND_EMAIL"),'');//sandeep
                helper.closeModal(component);
            } 
                
        });
            
        $A.enqueueAction(action); 
        
    },
    closeToast : function(component, event){
        var toastcmp = component.find('toastcmp');
        $A.util.addClass(toastcmp, "slds-hide");
    },
    handleClick: function(component, event, helper) {
        helper.handleClick(component, event);				
    },
    recordUpdated: function(component, event, helper) {
        //console.log('record updated');				
    },
    /*Sandeep */
    showConfirmationDailog: function(component, event,helper){
        //helper.closeModal(component);
        //Sandeep:Hide consent modal
		component.set("v.requestedConsent", "Email");
        if(component.get("v.contactRecordFields.Email")){
            helper.toggleModalDisplay(component);
            //sandeep:show dialog
            var dailog =component.find("confirmationdailog");
            //dailog.addEventHandler("onConfirm", component.getReference("c.sendConsentEmail"));
            //console.log('event handler added ..');
           // dailog.openDailog("Request Consent Confirmation","You are requesting consent for communication from the contact. It may take up to 15 minutes for this email to arrive. Click CONFIRM to continue or CANCEL to go back.");
		   dailog.openDailog($A.get("$Label.c.ATS_CONSENT_PREFERENCE_MODALMESSAGE"),$A.get("$Label.c.ATS_CONSENT_PREFERENCE_COMM_CONTACT"));
            //console.log('openDailog called ..');
			
        }else{
            helper.showErrorToast(component,$A.get("$Label.c.ATS_CONSENT_PREFERENCES_WARNING"));
        }
    },
	
	showConfirmationDailogText : function(component, event, helper) {
		component.set("v.requestedConsent", "Text");	
		 if(component.get("v.contactRecordFields.Email") && component.get("v.contactRecordFields.MobilePhone")){
            helper.toggleModalDisplay(component);
            var dailog =component.find("confirmationdailog");
			
            dailog.openDailog($A.get("$Label.c.ATS_CONSENT_PREFERENCE_MODALMESSAGE"),'Consent form to be sent to: '+component.get('v.contactRecordFields.Email') +'.'+
															 '<br/><br/> '+$A.get("$Label.c.ATS_CONSENT_PREFERENCE_COMM_CONTACT"));            
        }else{
            helper.showErrorToast(component,$A.get("$Label.c.ATS_REQUEST_CONSENT_WARNIN_PHONE"));
        }
	},
    onCancel : function(component, event,helper){
        helper.toggleModalDisplay(component);
    }
    
})