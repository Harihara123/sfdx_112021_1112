({	
    //<Andy>
    Icons: {
        toggleColors: {
            NA: {
                orange: 'red',
                red: 'orange'
            },
            Yes: {
                green: 'red',
                red: 'green'
            }
        },
        initColors: {
            NA: {
                'utility:warning': 'orange',
                'utility:ban': 'red'
            },
            Yes: {
                'utility:success': 'green',
                'utility:ban': 'red'
            },
            No: {
                'utility:ban': 'red'
            }            
        },
        toggleIcons: {
            NA: {
                'utility:warning': 'utility:ban',
                'utility:ban': 'utility:warning'
            },
            Yes: {
                'utility:success': 'utility:ban',
                'utility:ban': 'utility:success'
            }  
        },
        initIcons: {
            NA: 'utility:warning',
            Yes: 'utility:success',
            No: 'utility:ban'
        }
    },
    FooterInit: {
        NA: false,
        Yes: false,
        No: true
    },
    populateConsentPref: function (component) {
        var action =component.get("c.getConsentPreferencesModelProperties");
        var recordId = component.get("v.recordId");
        action.setParams({"contactID" : recordId});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var PreferenceDetails = response.getReturnValue();
                //console.log('model properties ',PreferenceDetails);
                var lcpList =PreferenceDetails.consentPreferencesFieldList
                component.set("v.cpList", PreferenceDetails.consentPreferencesFieldList);
                component.set("v.enableSendConsent",PreferenceDetails.showSendConsentButton);
				if(PreferenceDetails.isRequestTextConsentAllowed && component.get('v.modalOpeningSource') === 'TalentLandingPage') {
					component.set("v.userTextingConsentAllowed",true);
				}
				else {
					component.set("v.userTextingConsentAllowed",false);
				}
				
				
                // <Andy's stuff>
                let items = [];
                let showRequestFooterEmail;
                let showRequestFooterText;
                lcpList.map((item) => {
                    if (item.fieldLabel.includes('Emails')) {
                    item.fieldName = 'cpemail';
                    item.checkboxState = false;
                    item.fDisplay = item.fieldDisplay;
                    showRequestFooterEmail = this.FooterInit[item.fieldValue];
                    component.set(`v.cpemailIcon`, {icon: this.Icons.initIcons[item.fieldValue], type: item.fieldValue, color: this.Icons.initColors[item.fieldValue][this.Icons.initIcons[item.fieldValue]]})
                items.push(item)
            }
            if (item.fieldLabel.includes('Texts')) {
                item.fieldName = 'cptext';
                item.checkboxState = false;
                item.fDisplay = item.fieldDisplay;
                showRequestFooterText = this.FooterInit[item.fieldValue];
                component.set(`v.cptextIcon`, {icon: this.Icons.initIcons[item.fieldValue], type: item.fieldValue, color: this.Icons.initColors[item.fieldValue][this.Icons.initIcons[item.fieldValue]]})
                items.push(item)
            }
        });
        //console.log(items);
        component.set('v.showRequestFooter', showRequestFooterEmail && showRequestFooterText ? true : false);
        component.set('v.items', items);
        // </Andy stuff>
    }
});
$A.enqueueAction(action);
},
    //Andy
    handleClick: function(component, event) {
        
        let items = component.get('v.items');
        let color = this.Icons.toggleColors[component.get(`v.${event.currentTarget.id}Icon.type`)][component.get(`v.${event.currentTarget.id}Icon.color`)];
        let icon = this.Icons.toggleIcons[component.get(`v.${event.currentTarget.id}Icon.type`)][component.get(`v.${event.currentTarget.id}Icon.icon`)];		        
        
        component.set(`v.${event.currentTarget.id}Icon.color`, color);
        component.set(`v.${event.currentTarget.id}Icon.icon`, icon);
        
        items[event.currentTarget.name].checkboxState = !items[event.target.name].checkboxState
        
        component.set('v.items', items);
        
        //Enable-disable save button.
        let emailFlag=false;
        let textFlag=false;
        for(let i=0; i<items.length; i++) {
            if(items[i].fieldName === 'cpemail' && items[i].checkboxState === true) {
                emailFlag = true;
            }
            else if(items[i].fieldName === 'cptext' && items[i].checkboxState === true) {
                textFlag = true;
            }
        }
        
        if(emailFlag===false && textFlag===false ){
            component.set("v.isSaveDisabled", true);
        }
        else {
            component.set("v.isSaveDisabled", false);
        }
    },
        
        saveConsentPref : function(component){
            let items = component.get('v.items');
            let cpList = component.get("v.cpList");
            for(let j=0 ; j<cpList.length; j++) {
                let cp = cpList[j];
                if (cp.fieldLabel.includes('Emails')) {
                    for(let i=0; i<items.length; i++) {
                        if(items[i].fieldName === 'cpemail' && items[i].checkboxState === true) {
                            cp.fieldValue = 'No'
                        }
                    }
                }
                else if (cp.fieldLabel.includes('Texts')) {
                    for(let i=0; i<items.length; i++) {
                        if(items[i].fieldName === 'cptext' && items[i].checkboxState === true) {
                            cp.fieldValue = 'No'
                        }
                    }
                }
            }
            
            let recordId = component.get("v.recordId");
            let action = component.get("c.saveConsentPref");
            action.setParams({"consPrefDTOJSON" : JSON.stringify(cpList), "contactID" : recordId});
            
            action.setCallback(this, function(response) {
                let state = response.getState();
                if(state === 'SUCCESS'){
                    var refreshBanner = $A.get("e.c:E_RecordUpdatedAppEvent");
                    refreshBanner.fire();
                    this.showSaveToastStd('Consent & Preferences updated successfully.',''); 
                    this.closeModal(component);
                }
                else if(state === 'ERROR') {	
                    this.showErrorToastStd($A.get("$Label.c.Error"),'');			
                }
            });
            $A.enqueueAction(action);
        },
            
            showErrorToastStd : function(errorMessage, title){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    title: title,
                    duration:7000,
                    message: errorMessage,
                    type: 'error'
                });
                toastEvent.fire();
            },
                showSaveToastStd : function(msg, title){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'success',
                        title: title,
                        message: msg,
                        duration:7000,
                        type: 'success'
                    });
                    toastEvent.fire();
                },
                    
                    //sandeep
                    showSuccessToast : function(component,message){
                        var toastParams = {'type':'success','message':message,'title':'Success','mode':'dismissible'};
                        component.set("v.toastParams",toastParams);  
                        component.set("v.showToast",true);
                    },
                        //sandeep
                        showErrorToast : function(component,message){
                            var toastParams = {'type':'error','message':message,'title':'Success','mode':'dismissible'};
                            component.set("v.toastParams",toastParams);  
                            component.set("v.showToast",true);
                        },
                            closeModal : function(component) {
                                
                                let source = component.get("v.modalOpeningSource");
                                if(source === 'ClientContact') {
                                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                                    dismissActionPanel.fire();
                                }
                                else {
                                    
                                    component.find("overlayLib").notifyClose();
                                }
                                
                                //component.destroy();
                            },
                                toggleModalDisplay: function(cmp){
                                    var modal = cmp.find("consentModal");
                                    $A.util.toggleClass(modal, "slds-hide");
                                    var toastEvent = $A.get("e.force:showToast");
                                    toastEvent.fire();
                                    
                                }   
})