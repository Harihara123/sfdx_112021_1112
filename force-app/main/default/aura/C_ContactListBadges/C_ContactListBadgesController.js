({
    doInit : function(cmp,event,helper){
        var recordID = cmp.get("v.recordId"); 
        if(recordID !== null && recordID !== undefined){
            if(typeof recordID !== 'undefined'){
                if(recordID.length == 18){
                    if(!cmp.get("v.passInList")) {
                        helper.populateContactLists(cmp);              
                    }
                }else{
                   helper.showError('Invalid Contact Id.','Error: '); 
                }
               
            }else{
                helper.showError('Invalid Contact Id.','Error: '); 
            }      
        }
        
    },

    updateBadges : function(cmp, event, helper) {
        helper.populateContactLists(cmp);
    },
	// 5/17/2018 S-82783/S-81190: Function to toggle between sliced and full array. -Andy Satraitis
	toggleList: function(cmp) {
		var slicedArray = cmp.get("v.showCList");
		var initArray = cmp.get("v.cListInit");
		var text = cmp.get("v.toggleViewMore");
						
		var toggleText = {
			"View More": "View Less",
			"View Less": "View More"
		}
		
		if(slicedArray.length === 5) {
			cmp.set("v.showCList", initArray);
			cmp.set("v.toggleViewMore", toggleText[text]);
		} else {
			cmp.set("v.showCList", initArray.slice(0,5));
			cmp.set("v.toggleViewMore", toggleText[text]);
		}
	}
})