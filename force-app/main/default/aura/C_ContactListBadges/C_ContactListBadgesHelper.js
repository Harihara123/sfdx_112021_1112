({
	populateContactLists:function(cmp){
		var params;
		params = {
				'recordId' : cmp.get("v.recordId")          
			};
        var bdata = cmp.find("bdhelper");
		bdata.callServer(cmp,'ATS','ContactListFunctions',"getContactLists",function(response){
			if(cmp.isValid() && (response.length > 0) ){
			var opt = []; 
				var key = '';
				for(var i=0; i<=response.length;i++) {     //this iterates over the returned map
					if(typeof response[i] !== "undefined"){
						opt.push({"key":response[i].Tag_Definition__r.Id,
								"value":response[i].Tag_Definition__r.Tag_Name__c + ' (' +response[i].Tag_Definition__r.Candidate_Count__c + ')',
								"showPill":false});
					}
                  
				}
				// 5/17/2018 S-82783/S-81190: Checking if array.length is greater than 6. -Andy Satraitis
					if(opt.length > 5) {
						var array = opt.slice(0,5); // this new array to be used to itterate inside the component.
						cmp.set("v.cListInit", opt); // Contains original array.
						cmp.set("v.showCList", array); // Contains sliced array; to be itterate in the component.
						cmp.set("v.toggleViewMore", "View More"); //only show "View More" only when more than 5 lists.
						
					} else {								
						cmp.set("v.cListInit", opt); //to populate existing badges  
						cmp.set("v.showCList", opt); 					
					}						   
			}
		},params,false);  
	},
    /*
	callServer : function(component, className, subClassName, methodName, callback, params, storable, errCallback) {
		var serverMethod = 'c.performServerCall';
		var actionParams = {'className':className,
							'subClassName':subClassName,
							'methodName':methodName.replace('c.',''),
							'parameters': params};


		var action = component.get(serverMethod);
		action.setParams(actionParams);

		if(storable){
			action.setStorable();
		}else{
			action.setStorable({"ignoreExisting":"true"});
		}
      
		action.setCallback(this,function(response) {
			var state = response.getState();
				if (state === "SUCCESS") { 
				// pass returned value to callback function
				callback.call(this, response.getReturnValue());   
                
			} else if (state === "ERROR") {
				// Use error callback if available
				if (typeof errCallback !== "undefined") {
					errCallback.call(this, response.getReturnValue());
					// return;
				}

				// Fall back to generic error handler
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						this.showError(errors[0].message);
						//throw new Error("Error" + errors[0].message);
					}else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
						this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
					}else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
						this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
					}else{
						this.showError('An unexpected error has occured. Please try again!');
					}
				} else {
					this.showError(errors[0].message);
					//throw new Error("Unknown Error");
				}
			}
           
		});
    
		$A.enqueueAction(action);
	},*/
	showError : function(errorMessage, title){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			mode: 'dismissible',
			title: title,
			message: errorMessage,
			type: 'error'
		});
		toastEvent.fire();
	},
})