({
	doInit: function (cmp, event, helper) {
        helper.getTableRows(cmp);
       //helper.checkSMS360Permission(cmp);
	},
    showToast: function(cmp, event, helper) {
        helper.removeRowFromList(cmp, event);
    },
    showReqModal: function(cmp, event, helper) {
	    var params = {  "recordId":event.getParam("contactId")
                        ,"talentId":event.getParam("talentId")
                        ,"record":event.getParam("record")
                        ,"runningUserOPCO":event.getParam("category")};
		helper.createComponent(cmp, 'c:C_TalentReqSearchModal', 'v.C_TalentReqSearchModal',params);
    },
    bringUpCallSheetModal: function(cmp, event, helper) {
        helper.handleCreateComponent(cmp, event, 'callSheet');
    },
    bringUpCampaignModal: function(cmp, event, helper) {
        helper.handleCreateComponent(cmp, event, 'campaign');
    },
    refreshListView : function(cmp, event, helper){
        console.log('refreshing the view');
    },
    filterContacts: function(cmp, event, helper) {
        helper.filterContactLists(cmp, event, helper);
    },
    removeSelectedTalent: function(cmp, event, helper) {
        if(cmp.get('v.currentTalentList').length > 0) {
            var cmpEvent = $A.get("e.c:E_RemoveSelectedContact");
            cmpEvent.setParams({"contactId":event.getSource().get("v.name")});
            cmpEvent.fire();
        } else {
            let selectedList = cmp.get('v.selectedTalents');
            selectedList.splice(selectedList.map(x => x.Id).indexOf(event.getSource().get('v.name')), 1);
            cmp.set('v.selectedTalents', selectedList);
        }

    },
    removeSelectedClient: function(cmp, event, helper) {
        if(cmp.get('v.currentClientList').length > 0) {
            var cmpEvent = $A.get("e.c:E_RemoveSelectedClient");
            cmpEvent.setParams({"contactId":event.getSource().get("v.name")});
            cmpEvent.fire();
        } else {
            let selectedList = cmp.get('v.selectedClients');
            selectedList.splice(selectedList.map(x => x.Id).indexOf(event.getSource().get('v.name')), 1);
            cmp.set('v.selectedClients', selectedList);
        }
    },
	deleteContactTags: function(component, event, helper) {
		helper.handleCreateComponent(component, event, 'deleteContactTags');
	},
	showToastforSelectedContacts: function(component, event, helper) {
		helper.removeSelectedRowsFromList(component, event);
	},  


    /*SendSMSList : function(cmp,event,helper){
     try{
			let selectedListTalent = [];
			var optedInIds = ''; 
			selectedListTalent = cmp.get('v.selectedTalents');
			if(selectedListTalent.length > 0){
				for(var i=0;i<selectedListTalent.length;i++){
					if(selectedListTalent[i].PrefCentre_Aerotek_OptOut_Mobile__c==='No') {
						if($A.util.isEmpty(optedInIds)) {
							optedInIds = selectedListTalent[i].Id;
						}
						else {
							optedInIds = optedInIds + "," + selectedListTalent[i].Id;
						}
					}
				}
				
				if($A.util.isEmpty(optedInIds)) {
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						type: 'error',
						message: $A.get("$Label.c.SMS_NO_OPTIN_SELECTED")
					});
					toastEvent.fire();
				}
				else {
					var returl = window.parent.location.origin+'/lightning/r/Tag_Definition__c/'+cmp.get('v.recordId')+'/view';
					var pageName = 'tdc_tsw__SendBulkSMS_SLDS?ids';
					var modalBody;
					
					$A.createComponent("c:SendSMSContactComp", {"recordIdd": optedInIds, "returnURL": returl,"pageName": pageName},
						function(content, status) {
							if (status === "SUCCESS") {
								modalBody = content;
								cmp.find('SMSoverlayLib').showCustomModal({
								   body: modalBody, 
								   showCloseButton: true,
								   cssClass: "mymodal",
							   })
						   }                               
					});
				}
			}        
		}catch(e){}
    } */ 
})