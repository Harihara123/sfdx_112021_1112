({
	getTableRows: function (component) {
        var recordId = component.get('v.recordId');

        var action = component.get("c.getContactsForList");
        action.setParams({
            tagId: recordId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                var returnMap = response.getReturnValue();
                component.set('v.clientList', returnMap.contactMap.Client);
                component.set('v.talentList', returnMap.contactMap.Talent);
                component.set('v.talentSubmittals', returnMap.talentsWithAtLeastOneSubmittal);
                component.set('v.currentClientList', returnMap.contactMap.Client);
                component.set('v.currentTalentList', returnMap.contactMap.Talent);
                component.set('v.profileName', returnMap.uoModal.usr.Profile.Name);
                component.set('v.userAlias', returnMap.uoModal.usr.Alias);
				component.set('v.userPplSoftID', returnMap.uoModal.usr.Peoplesoft_Id__c);
                component.set('v.userCategory', returnMap.uoModal.userCategory);
                component.set('v.userId', returnMap.uoModal.usr.Id);
				//component.set('v.hasUserSMS360Access', returnMap.hasUserSMSAccess);
                component.set('v.isLoaded', true);
				//marketing user attribute
				component.set('v.isMarketingUser',returnMap.uoModal.usr.UserPermissionsMarketingUser);
				// Uncomment below line for 14.0 release.
				this.setContactNotes(component,returnMap.contactNotes);
                if(!returnMap.contactMap.Talent.length && returnMap.contactMap.Client.length) {
                    component.set('v.selectedTab', 'clientList');
                } else if(returnMap.contactMap.Talent.length && !returnMap.contactMap.Client.length) {
                    component.set('v.selectedTab', 'talentList');
                }
				//show campaign button attribute
				if (['Single Desk 1','Single Desk 2','System Administrator','Lightning Marketing'].includes(returnMap.uoModal.usr.Profile.Name)) 
					component.set("v.isCampaignEnabled",true);
				/*else if(['Lightning Marketing'].includes(returnMap.uoModal.usr.Profile.Name) && returnMap.uoModal.usr.UserPermissionsMarketingUser)
					component.set("v.isCampaignEnabled",true);*/
				
            } else {
                console.log('error');
            }
        });

        $A.enqueueAction(action);
	},
    removeRowFromList: function(component, event) {
        var removedContact = event.getParam("contact");

        var contacts;
        var currentContacts;
        var selectedContacts;
        var otherCurrentList;

        if(removedContact.RecordType.Name == "Talent") {
            contacts = component.get("v.talentList");
            otherCurrentList = component.get("v.currentClientList");
            currentContacts = component.get("v.currentTalentList");
            selectedContacts = component.get("v.selectedTalents");
        } else if(removedContact.RecordType.Name == "Client") {
            contacts = component.get("v.clientList");
            otherCurrentList = component.get("v.currentTalentList");
            currentContacts = component.get("v.currentClientList");
            selectedContacts = component.get("v.selectedClients");
        }

        var removalSuccess = event.getParam("requestStatus");

        var toastEvent = $A.get("e.force:showToast");

        if(removalSuccess) {
            toastEvent.setParams({
                "type":"success",
                "title":"Success",
                "message":"The Contact was successfully removed."
            });

            var newList = contacts.filter(function(el) {
                return el.Id !== removedContact.Id;
            });

            var currentList = currentContacts.filter(function(el) {
                return el.Id !== removedContact.Id;
            });

            var selectedList = selectedContacts.filter(function(el) {
                return el.Id !== removedContact.Id;
            });

            if(!currentList.length && otherCurrentList.length) {
                var correctTab = removedContact.RecordType.Name === "Talent" ? 'clientList' : 'talentList';
                console.log(correctTab);
                component.set('v.selectedTab', correctTab);
            }

            if(removedContact.RecordType.Name == "Talent") {
                contacts = component.set("v.talentList", newList);
                component.set("v.currentTalentList", currentList);
                component.set("v.selectedTalents", selectedList);
            } else if(removedContact.RecordType.Name == "Client") {
                contacts = component.set("v.clientList", newList);
                component.set("v.currentClientList", currentList);
                component.set("v.selectedClients", selectedList);
            }
        } else {
            toastEvent.setParams({
                "type": "error",
                "title": "Error",
                "message": "Unable to remove the Contact from the list."
            });
        }

        toastEvent.fire();
    },
	removeSelectedRowsFromList: function(component, event) {
        var removedContacts = event.getParam("contacts");
		var removedTalentContactIds = [];
		var removedClientContactIds = [];

		var removedTalentContacts = removedContacts.filter(function(el) {
            return el.RecordType.Name == "Talent";
        });

		var removedClientContacts = removedContacts.filter(function(el) {
            return el.RecordType.Name == "Client";
        });

		for(var i=0; i<removedContacts.length; i++) {
			removedContacts[i].RecordType.Name == "Talent" ? removedTalentContactIds.push(removedContacts[i].Id) : removedClientContactIds.push(removedContacts[i].Id);
		}

        var talentList = component.get("v.talentList");
		var clientList = component.get("v.clientList");
		var currentTalentList = component.get("v.currentTalentList");
        var currentClientList = component.get("v.currentClientList");

        var removalSuccess = event.getParam("requestStatus");

        var toastEvent = $A.get("e.force:showToast");
        if(removalSuccess) {
            toastEvent.setParams({
                "type":"success",
                "title":"Success",
                "message":"The Contact was successfully removed."
            });

            var newTalentList = talentList.filter(function(el) {
                return this.indexOf(el.Id) < 0;
            }, removedTalentContactIds);

            var newClientList = clientList.filter(function(el) {
                return this.indexOf(el.Id) < 0;
            }, removedClientContactIds);

			var currentNewTalentList = currentTalentList.filter(function(el) {
                return this.indexOf(el.Id) < 0;
            }, removedTalentContactIds);

			var currentNewClientList = currentClientList.filter(function(el) {
                return this.indexOf(el.Id) < 0;
            }, removedClientContactIds);

            var correctTab = !currentNewTalentList.length && currentNewClientList.length ? 'clientList' : (currentNewTalentList.length && !currentNewClientList.length ? 'talentList' : component.get('v.selectedTab'));
            component.set('v.selectedTab', correctTab);

            component.set("v.talentList", newTalentList);
			component.set("v.clientList", newClientList);

            component.set("v.currentTalentList", currentNewTalentList);
            component.set("v.currentClientList", currentNewClientList);

			component.set("v.selectedTalents", []);
            component.set("v.selectedClients", []);
        } else {
            toastEvent.setParams({
                "type": "error",
                "title": "Error",
                "message": "Unable to remove selected Contacts from the list."
            });
        }

        toastEvent.fire();

	},
    handleCreateComponent: function (component, event, modal) {
        var talents = component.get('v.selectedTalents');
        var clients = component.get('v.selectedClients');
		var list = component.get("v.recordId");
        var typeList = component.get('v.typeList');
        var priorityList = component.get('v.priorityList');
        var statusList = component.get('v.statusList');

        var listName = component.get('v.simpleRecord');

        talents = talents.concat(clients);

        if(modal === 'callSheet') {
            $A.createComponent(
                "c:C_AddToCallSheetModal",
                {
                "contacts" : talents, 
                "typeList" : typeList,
                "priorityList" : priorityList,
                "statusList" : statusList,
                "subject" : listName.Tag_Name__c
            },

            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    component.set('v.C_AddToCallSheetModal', newComponent);
                }
                else if (status === "INCOMPLETE") {
                }
                else if (status === "ERROR") {
                    }
                }
            );
        } else if(modal === 'campaign'){
			var cType = "L"; 
            $A.createComponent(
                "c:C_AddToCampaignModal",
                {
                    "contacts" : talents,
					"cType" : cType
				},

                function(newComponent, status, errorMessage){
                    if (status === "SUCCESS") {
                        component.set('v.C_AddToCampaignModal', newComponent);
                    }
                    else if (status === "INCOMPLETE") {
                    }
                    else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
        } else if(modal === 'deleteContactTags') {
			$A.createComponent(
                "c:C_RemoveContactModal",
                {
                    "contacts" : talents, 
					"numberOfRecords" : talents.length,
					"showListMessage" : true,
					"list" : list
                },

                function(newComponent, status, errorMessage){
                    if (status === "SUCCESS") {
                        component.set('v.C_RemoveContactModal', newComponent);
                    }
                    else if (status === "INCOMPLETE") {
                    }
                    else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
		}
    },

	createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
			componentName,
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					cmp.set(targetAttributeName, newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			}
		);
		
    },
    filterContactLists : function(cmp, event, helper) {
        //get parent lists
        var clientList = cmp.get('v.clientList');
        var talentList = cmp.get('v.talentList');

        //get search term
        var filterText = cmp.get('v.filterText');

        if(filterText) {
            filterText = filterText.toUpperCase();
        } else {
            //reset lists on empty search
            cmp.set('v.currentClientList', clientList);
            cmp.set('v.currentTalentList', talentList);

            return;
        }

        //refactor to function notation

        var filteredClientList = clientList.filter(function(client) {
            if(helper.checkIfContactContainsSearchTerm(client, filterText, helper)) {
                return client;
            }
        });

        //refactor to function notation

        var filteredTalentList = talentList.filter(function(talent)  {
            if(helper.checkIfContactContainsSearchTerm(talent, filterText, helper)) {
                return talent;
            }
        });

        //set filtered lists

        if(filteredClientList.length && !filteredTalentList.length) {
            cmp.set('v.selectedTab', 'clientList');
        } else if(!filteredClientList.length && filteredTalentList.length) {
            cmp.set('v.selectedTab', 'talentList');
        }


        cmp.set('v.currentClientList', filteredClientList);
        cmp.set('v.currentTalentList', filteredTalentList);


    },
    checkIfContactContainsSearchTerm : function(contact, searchTerm, helper) {

        if(contact.Name.toUpperCase().includes(searchTerm)) {
            return true;
        }

        if(contact.Talent_Id__c && contact.RecordType.Name === 'Talent'
            && contact.Talent_Id__c.toUpperCase().includes(searchTerm)) {
            return true
        }

        if(contact.Email && contact.Email.toUpperCase().includes(searchTerm)) {
            return true;
        }

        if(contact.Other_Email__c && contact.Other_Email__c.toUpperCase().includes(searchTerm)) {
            return true;
        }

        if(contact.Work_Email__c && contact.Work_Email__c.toUpperCase().includes(searchTerm)) {
            return true;
        }

        if(helper.checkPhoneNumbers(contact, searchTerm)) {
            return true;
        }

        if(contact.Title && contact.Title.toUpperCase().includes(searchTerm)) {
            return true;
        }

        if(contact.Account.Talent_Current_Employer_Formula__c 
            && contact.Account.Talent_Current_Employer_Formula__c.toUpperCase().includes(searchTerm)) {
            return true;
        }

        if(contact.Mailing_Address_Formatted__c && contact.RecordType.Name === 'Talent'
            && contact.Mailing_Address_Formatted__c.toUpperCase().includes(searchTerm)) {
            return true;
        }

        if(contact.Account.Talent_Latest_Submittal_Status__c
            && contact.Account.Talent_Latest_Submittal_Status__c.toUpperCase().includes(searchTerm)) {
            return true;
        }
		
		if(contact.Account.Name && contact.Account.Name.toUpperCase().includes(searchTerm)) {
            return true;
        }

        return false;

    },
    checkPhoneNumbers: function(contact, searchTerm) {
        var onlyDigits = /\D/g;

        var scrubbedSearch = searchTerm.replace(onlyDigits, '');

        var phone = contact.Phone ? contact.Phone.replace(onlyDigits, '') : '';
        var homePhone = contact.HomePhone ? contact.HomePhone.replace(onlyDigits, '') : '';
        var mobilePhone = contact.MobilePhone ? contact.MobilePhone.replace(onlyDigits, '') : '';
        var otherPhone = contact.OtherPhone ? contact.OtherPhone.replace(onlyDigits, '') : '';

        return ((phone.includes(scrubbedSearch) || homePhone.includes(scrubbedSearch) || mobilePhone.includes(scrubbedSearch) || otherPhone.includes(scrubbedSearch)) && scrubbedSearch !== '');
    },
    removeContact : function(cmp, event, recordType) {
        var selectedRecordTypeAttribute = recordType === 'talent' ? 'v.selectedTalents' : 'v.selectedClients';
        var selectedList = cmp.get(selectedRecordTypeAttribute);

        var removedId = event.getSource().get("v.name");

        var filteredList = selectedList.filter(function(contact) {
            return contact.Id !== removedId;
        });

        var rowItems = cmp.find('rowItem');
        if(rowItems) {
            if(rowItems.length > 1) {
                for(var i = 0; i < rowItems.length; i++) {
                    rowItems[i].set('v.isSelected', selectedList.includes(rowItems[i].get('v.talent')));
                }
            } else {
                if(Array.isArray(rowItems)) {
                    rowItems[0].set('v.isSelected', selectedList.includes(rowItems[0].get('v.talent')));
                } else {
                    rowItems.set('v.isSelected', false);
                }
            }
        }

        cmp.set(selectedRecordTypeAttribute, filteredList);
    },
    getModalValues: function (component) {
        var action = component.get('c.getPicklistValues');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var model = response.getReturnValue();

                var typeArray = [];
                var typeMap = model.typeMapping;
                for ( var key in typeMap) {
                    typeArray.push({value:typeMap[key], key:key});
                }
                component.set("v.typeList", typeArray);
                
                
                var statusArray = [];
                var statusMap = model.statusMapping;
                for ( var key in statusMap) {
                    statusArray.push({value:statusMap[key], key:key});
                }
                component.set("v.statusList",statusArray);
                
                
                
                var priorityArray = [];
                var priorityMap = model.priorityMapping;
                for ( var key in priorityMap) {
                    priorityArray.push({value:priorityMap[key], key:key});
                }
                component.set("v.priorityList",priorityArray);
            }
        });
         
        $A.enqueueAction(action);
    },

	setContactNotes : function (component,notesMap) {
		var talentList = component.get('v.currentTalentList');
		var clientList = component.get('v.currentClientList');
		
		for (var note in notesMap) {
			let talentIndex = talentList.findIndex(talent => talent.Id === note);
			if (talentIndex !== -1) {
				talentList[talentIndex].Notes = notesMap[note];
			}

			let clientIndex = clientList.findIndex(talent => talent.Id === note);
			if (clientIndex !== -1) {
				clientList[clientIndex].Notes = notesMap[note];
			}
		}
		component.set('v.currentTalentList',talentList);  
		component.set('v.currentClientList',clientList);
	},

   /* checkSMS360Permission:function(component){
        var action = component.get('c.userHasAccessPermission');
         action.setParams({
            permissionName: 'SMS'
        });
        action.setCallback(this,function(response){
            if(response.getState()=="SUCCESS")
            {
                var hasAccess = response.getReturnValue();
                console.log('user has access SMS 360 App' + hasAccess);
                if(hasAccess){
                    component.set('v.userHasSMS360Access',true);
                }
            }
            else{
                console.log('something unexpected happened---'+response.getState());
            }
        });
         $A.enqueueAction(action);
    } */
})