({
	init : function(cmp, event, helper) {
        cmp.set('v.customActions', [
            { label: 'Custom action', name: 'custom_action' }
        ]) ;      
		cmp.set('v.accConAssColumns', [
            { label: 'Account Name', fieldName: 'LinkName', type: 'url', typeAttributes: {label: { fieldName: 'Account__r_Name' }, target: '_blank'} },
            { label: 'Status', fieldName: 'Status__c', type: 'Picklist' },
            { label: 'Date Moved', fieldName: 'Contact_Moved_Date__c', type:"date-local", typeAttributes:{month:"2-digit", day:"2-digit"}},
            { label: 'Is Primary Account', fieldName: 'Is_Primary_Account__c', type:"Checkbox" }
        ])	;

		cmp.set('v.vocColumns', [
            { label: 'NPS Score', fieldName: 'NPS_Score__c', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: 'Survey Date', fieldName: 'Survey_Date__c', type: 'date' },
            { label: 'Survey Source', fieldName: 'Survey_Source__c', type: 'picklist'},
            { label: 'Record Type', fieldName: 'RecordType.name', type:"Record Type" }
        ])	; 
		 
        cmp.set('v.opptyColumns', [
            { label: 'OPPORTUNITY  Name', fieldName: 'LinkName', type: 'url', typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'} },
            { label: 'Stage', fieldName: 'StageName', type: 'text' },
            { label: 'Amount', fieldName: 'Amount', type: 'currency', cellAttributes: { alignment: 'left' } },
            { label: 'Close Date', fieldName: 'CloseDate', type:"date-local"}
        ])	; 	
		
		cmp.set('v.conIntelHistoryColumns', [
            { label: 'Created Date', fieldName: 'CreatedDate', type:"date"},
            { label: 'User',  fieldName: 'LinkName', type: 'url', typeAttributes: {label: { fieldName: 'User__r_Name' }, target: '_top'}},
            { label: 'OPCO', fieldName: 'opco__c', type:"text"},
            { label: 'FIELD', fieldName: 'Field__c', type:"text" }
        ])	;
    },
    customHandler : function(cmp, event, helper) {
        alert("It's custom action!")
    },
    
    refreshView : function (component, event, helper) {
		component.set("v.displayRelatedList", true);
    }    
})