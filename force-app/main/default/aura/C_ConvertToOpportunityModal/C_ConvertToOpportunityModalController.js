({
	doInit:function(component,event,helper){
		var submittalId = component.get("v.submittalId");
		var submittal = component.get("v.submittal");

		component.set("v.talentId",submittal.ShipToContactId);
		component.set("v.submittalId",submittalId);
		component.set("v.submittal",submittal);
		console.log('talentId----CTOM'+component.get("v.talentId"));
		console.log('submittal----CTOM'+component.get("v.submittal"));
				
    },
	cancelChange: function(component, event, helper) {
		//console.log('Status:'+component.get("v.targetStatus"));
		//if (['Interviewing','Add New Interview','Edit Interview'].includes(component.get("v.targetStatus"))) {
			var reloadEvt = $A.get("e.c:E_TalentActivityReload");
			reloadEvt.fire();
		//}
        helper.fireCanceledEvt(component,event);
		
    },
	showError : function(component, message, messageType, theTitle){
        var title = theTitle;
        var errorMessage = message;
        var theType = messageType;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: theType
        });
        toastEvent.fire();
    },
	 linkCandidateToJob : function(cmp, event, helper) {	
	 console.log('Before close of modal:'+cmp.get("v.submittal"));
	  var recordId = cmp.get("v.submittal").Id;
	  var proactiveSubmittalStatus = cmp.get("v.statusText");
	  var modalPromise = cmp.getReference('v.modalPromise');
	  var submittal = cmp.get("v.submittal");
	  cmp.set("v.editId",submittal.Id);
	  //Closing The existing popup		
       // helper.fireCanceledEvt(cmp,event);
		//Open another Component
		console.log('after cclose of modal:'+cmp.get("v.submittal"));
		
		if( submittal.AccountId == null ){ //submittal.Hiring_Manager__c == null || submittal.Hiring_Manager__c =='' ||
			helper.fireToastError();
			//Closing The existing popup		
			helper.fireCanceledEvt(cmp,event);
			// Open New modal from Talent Submittals
			/*var params = { "submittal" :submittal
                        ,"reqOpco":"Proactive"
						,"targetStatus":proactiveSubmittalStatus};
			var handleNewModal = $A.get("e.c:E_CreateProactiveOpp");
			handleNewModal.setParams(params);
			handleNewModal.fire();*/
		}else{
		//Closing The existing popup		
			helper.fireCanceledEvt(cmp,event);
			var params = { "submittal" :submittal
                        ,"reqOpco":"Proactive"
						,"targetStatus":proactiveSubmittalStatus
						,"oppModalFlag": true};						
			var handleNewModal = $A.get("e.c:E_CreateProactiveOpp");
			handleNewModal.setParams(params);
			handleNewModal.fire();	
					
		}		    
				
    }
})