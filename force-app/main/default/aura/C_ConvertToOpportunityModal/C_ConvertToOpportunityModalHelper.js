({
	fireCanceledEvt : function(component,event) {
		/*let promise = component.get('v.modalPromise')
		promise.then(
			function (modal) {
				var evt = component.getEvent("statusCanceledEvt");
				evt.fire();
				modal.close();
			}
		);*/
		//this.toggleClassInverse(component,'backdropModal','slds-backdrop_');
		//this.toggleClassInverse(component,'mainModal','slds-fade-in-');
		//component.destroy();
		
		//var closeEvent = $A.get("e.force:closeQuickAction");
		//closeEvent.fire(); 

		var evt = component.getEvent("statusCanceledEvt");
		evt.fire();
	},
	closeModal: function(component) {
		this.toggleClassInverse(component,'backdropModal','slds-backdrop_');
		this.toggleClassInverse(component,'mainModal','slds-fade-in-');
    },
	toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },
	fireToastError : function(component, event, helper) {  
     var toastEvent = $A.get("e.force:showToast");  
     toastEvent.setParams({  
       "title": "Failed !!!",  
       "message": "Account Info Missing in Submittal. Please Update !!!",  
       "type": "ERROR"  
     });  
     toastEvent.fire();  
   } 
})