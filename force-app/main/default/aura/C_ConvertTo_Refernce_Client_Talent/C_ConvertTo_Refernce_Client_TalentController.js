({
    doInit : function (component,event,helper){
        
        helper.toggleClass(component,'backdropAddDocument','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
        helper.populateFields(component);
       
        var convertTo = component.get("v.convertTo");
        if(convertTo === "Talent" && (component.get("v.runningUser.OPCO__c") === 'ONS'  
                                              || component.get("v.runningUser.OPCO__c") === 'TEK'
                                              ||component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aerotek")
                                              ||component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aston_Carter")
                                              ||component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Newco"))){
             helper.getCountryMappings(component);
            component.set("v.showLocation","true");
        }
        if(convertTo === "Client"){
            component.set("v.displayClientLookup","true");
            helper.getClientAccountDetails(component);
        }
    },
    
    
    validateconvertContact : function (component,event,helper){
        
        helper.validateContact(component);  
    },
    
    closeModal1 : function(component,event,helper){
        
        helper.closeModal1(component); 
    },
    updateMailingCountryCode: function(cmp, event, helper) {
        helper.updateMailingCountryCode(cmp,event);
    },
    
    handleLocationSelection : function(component,event, helper) {
        component.set("v.isGoogleLocation", true);
        var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode,countrymap,displayText;
        mailingstreet = '';
        streetnum = '';
        streetname = '';
        city = '';
        state = '';
        country = '';
        countrycode = '';
        postcode = '';
        displayText = '';
        var addr = event.getParam("addrComponents");
        var latlong = event.getParam("latlong");    //S-96255 - Update MailLat, MailLong fields on Contact - Karthik    
        countrymap = component.get("v.countriesMap"); 
        displayText = event.getParam("displayText");
        //console.log('address component'+JSON.stringify(addr));
        for (var i=0; i<addr.length; i++) {
            if (addr[i].types.indexOf("street_number") >= 0) {
                streetnum = addr[i].long_name;
            }
            if (addr[i].types.indexOf("route") >= 0) {
                streetname = addr[i].long_name;
            }        
            if (addr[i].types.indexOf("postal_code") >= 0) {
                postcode = addr[i].long_name;
            }
            if (addr[i].types.indexOf("locality") >= 0) {
                city = addr[i].long_name;
            }
            if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
                state = addr[i].long_name;
            }
            if (addr[i].types.indexOf("country") >= 0) {
                country = addr[i].long_name;
                countrycode = countrymap[addr[i].short_name];
            }        
        }
        //console.log('teststreet',streetnum,streetname);
        if(streetnum) {
            mailingstreet = streetnum;
        } 
        if(streetnum && streetname) {
            mailingstreet = mailingstreet + ' ';
        }
        if(streetname) {
            if(mailingstreet != ''){
                mailingstreet = mailingstreet + streetname;        
            } else {
                mailingstreet = streetname;
            }
        }
        //console.log(mailingstreet,city,state,countrycode,country,postcode,displaytext);
        component.set("v.contact.MailingStreet",mailingstreet);
        component.set("v.contact.MailingCity",city);
        component.set("v.contact.MailingState",state);
        component.set("v.contact.MailingCountry",countrycode);
        component.set("v.contact.Talent_Country_Text__c",country);
        component.set("v.contact.MailingPostalCode",postcode);
        //var str = displayText.substr(0,displayText.indexOf(',')); // added to display only street address - Manish
        component.set("v.presetLocation",mailingstreet);
        component.set("v.streetAddress2","");
        //S-96255 - Update MailLat, MailLong fields on Contact
        if(latlong && (mailingstreet || city || postcode)){
            //component.set("v.contact.GoogleMailingLatLong__Latitude__s",latlong.lat);
            //component.set("v.contact.GoogleMailingLatLong__Longitude__s",latlong.lng);
            component.set("v.contact.MailingLatitude",latlong.lat);
            component.set("v.contact.MailingLongitude",latlong.lng);                
        } else {
            component.set("v.contact.MailingLatitude",null);
            component.set("v.contact.MailingLongitude",null);               
        }
        component.set("v.contact.LatLongSource__c","Google");
        component.set("v.viewState","EDIT");
        component.set("v.parentInitialLoad",true);
        var countryCode = helper.getKeyByValue(component.get("v.countriesMap"),countrycode);
        component.set("v.MailingCountryKey",countryCode);
    },


    handleSelectedContact: function (component, event, helper) {

        if( $A.util.isUndefined( component.get("v.selectedRecord") ) || $A.util.isEmpty(component.get("v.selectedRecord" ) ) ){
            helper.showToast(component, "Please select one record...!", "Error", "error");
            return;
        }
        helper.use_selected_contact(component);
    },

    handleCurrentContact: function (component, event , helper) {
        helper.convertContact(component); 
        helper.closeDedupeOverlay( component );
    },


    handleCloseModal: function (component, event, helper) {
      	helper.closeDedupeOverlay(component);
        helper.toggleClass(component,'backdropAddDocument','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
        
    },

   
    

})