({
    
    
    getClientAccountDetails : function(component){
        
        var recordId = component.get("v.recordId").Id;
        var action = component.get('c.getClientAccountDetails');
        action.setParams({
            "referenceId": recordId
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state 	  		= response.getState();
            var reference_data 	= response.getReturnValue();
            
            if (state === "SUCCESS") {
                if( Object.keys(reference_data).length ){
                    component.set("v.clientAccount", response.getReturnValue() );
                    if(component.get("v.clientAccount.Id")){
                        component.set("v.disableAccountLookup","true");
                    }
                 }                
            }
            
        }));        
        
        $A.enqueueAction(action);
        
    },
    populateFields : function(component){
        
        component.set("v.Spinner", true );
        var recordId = component.get("v.recordId").Id;
        var action = component.get('c.getReferenceContactDetails');
        action.setParams({
            "referenceId": recordId
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state 	  		= response.getState();
            var reference_data 	= response.getReturnValue();
            
            if (state === "SUCCESS") {
                component.set("v.Spinner", false );
                if( Object.keys(reference_data).length ){
                    component.set("v.referenceContact", response.getReturnValue() );
                }                
            }
            
            
        }));        
        
        $A.enqueueAction(action);
        
    },
    
    convertContact : function(component) {
        
        this.startSpinner(component);
        
        var action ;
        if(component.get("v.convertTo") =="Client"){
            action = component.get('c.convertReferenceToClient');
            
        }else if(component.get("v.convertTo") =="Reference"){
            action = component.get('c.convertReferenceToReference');     
        }else if(component.get("v.convertTo") =="Talent"){
            action = component.get('c.convertReferenceToTalent'); 
            component.get("v.referenceContact").MailingCountry = component.get("v.contact").MailingCountry;
            component.get("v.referenceContact").MailingStreet = component.get("v.contact").MailingStreet;
            component.get("v.referenceContact").MailingCity = component.get("v.contact").MailingCity;
            component.get("v.referenceContact").MailingState = component.get("v.contact").MailingState;
            component.get("v.referenceContact").MailingPostalCode = component.get("v.contact").MailingPostalCode;
            component.get("v.referenceContact").Talent_Country_Text__c = component.get("v.contact").Talent_Country_Text__c;
            component.get("v.referenceContact").talent_Ownership__c = component.get("v.userOwnership");
        }
        
        action.setParams({
            "contact_obj": component.get("v.referenceContact"),
             "referenceId" : component.get("v.recordId.Id"),
            "account_obj" : component.get("v.clientAccount")            
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            
            if (response.getState() === "SUCCESS" && response.getReturnValue() ) {

                //this.closeDedupeOverlay(component)
                this.showToast(component, "Converted Successfully", "success", "success", true);
                this.closeModal1(component);
            }else{
                this.showToast(component, "Conversion Failed, Please Contact Administrator", "error", "error");
                this.closeModal1(component);
            }
            
            this.stopSpinner( component );
            
        }));
        
        $A.enqueueAction(action);
    },
    
    validateContact : function(component){
        
        var isValid = true;
        isValid = this.validateTitle(component, isValid );
        isValid = this.validateFields(component, isValid, "firstName", "First Name") ;
        isValid = this.validateFields(component, isValid, "lastName", "Last Name") ;
        isValid = this.validatePhoneOrEmail(component, isValid) ;
        if(component.get("v.showLocation")){
            isValid = this.validateStateCountry(component, isValid);
        }
        if(component.get("v.convertTo") =="Client"){
            isValid = this.validateAccountLookup(component, isValid);
        }
        
        if(isValid){
            this.checkForDuplicates(component);
            // this.convertContact(component);
        }
        
    },
    
    validateAccountLookup :function(component,isValid){
        
        if(!component.get("v.clientAccount.Id")){
            isValid = false;
            this.showToast(component, "Please Select valid Client Account.","error","error", true);
            component.set("v.accountLookupError", true);
        } else {
            component.set("v.accountLookupError", false);
        }
      return  isValid;
    },
    
    validateFields : function(component, isValid, componentId, fieldName) {
        var currentFieldName = component.find(componentId);
        if(currentFieldName !== undefined){
            var validateField = currentFieldName.get("v.value");
            if (!validateField || validateField === "") {
                isValid = false;
                this.showToast(component, fieldName + " is Required.","error","error", true);
                currentFieldName.set("v.errors", [{message: fieldName + " is Required."}]);
                currentFieldName.set("v.isError",true);
                $A.util.addClass(currentFieldName,"slds-has-error show-error-message");
            } else {
                currentFieldName.set("v.errors", []);
                currentFieldName.set("v.isError", false);
                $A.util.removeClass(currentFieldName,"slds-has-error show-error-message");
            }
        }    
        return isValid;
    },
    
    validateTitle : function(component, isValid) {
        var title = component.get("v.referenceContact.Title");
        if (!title || title === "") {
            isValid = false;
            component.set("v.titleError", true);
            this.showToast(component, "Job title field is empty.","error","error", true);
        } else {
            component.set("v.titleError", false);
        }
        return isValid;
    },
    
    validatePhoneOrEmail : function(component, isValid) {
        
        var phoneFieldName      = component.find("phone");
        var mobileFieldName     = component.find("mobile");
        var homephoneFieldName  = component.find("homephone");
        var emailFieldName      = component.find("email");
        
        
        var validatePhone = phoneFieldName.get("v.value");
        var validateEmail = emailFieldName.get("v.value");
        var validateHomephone 	= homephoneFieldName.get("v.value");
        var validateMobile 		= mobileFieldName.get("v.value");
        
        if ( ( !validatePhone || validatePhone === "") && ( !validateEmail || validateEmail === "") &&
            ( !validateHomephone || validateHomephone === "") && ( !validateMobile || validateMobile === "" ) ) {

            isValid = false;
            this.showToast(component, "Phone Number or Email Address is Required.","error","error", true);
            phoneFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            phoneFieldName.set("v.isError",true);
            $A.util.addClass(phoneFieldName,"slds-has-error show-error-message");
            
            mobileFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            mobileFieldName.set("v.isError",true);
            $A.util.addClass(mobileFieldName,"slds-has-error show-error-message");
            
            homephoneFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            homephoneFieldName.set("v.isError",true);
            $A.util.addClass(homephoneFieldName,"slds-has-error show-error-message");
            
            emailFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            emailFieldName.set("v.isError",true);
            $A.util.addClass(emailFieldName,"slds-has-error show-error-message");
        }else {
            phoneFieldName.set("v.errors", []);
            phoneFieldName.set("v.isError", false);
            $A.util.removeClass(phoneFieldName,"slds-has-error show-error-message");
            
            emailFieldName.set("v.errors", []);
            emailFieldName.set("v.isError", false);
            $A.util.removeClass(emailFieldName,"slds-has-error show-error-message");
            
            homephoneFieldName.set("v.errors", []);
            homephoneFieldName.set("v.isError", false);
            $A.util.removeClass(homephoneFieldName,"slds-has-error show-error-message");
            
            mobileFieldName.set("v.errors", []);
            mobileFieldName.set("v.isError", false);
            $A.util.removeClass(mobileFieldName,"slds-has-error show-error-message");
        }
        
        return isValid;
    },
    
    validateStateCountry : function(component, validTalent) {
        
        var state    =      component.get("v.contact.MailingState");
        var country  =      component.get("v.contact.Talent_Country_Text__c");
        
        var mailingCountry = component.get("v.contact.MailingCountry");
        
        if ( $A.util.isEmpty(state) || $A.util.isEmpty(country) ) {
            validTalent = false; 
            this.showToast(component, "Please enter a valid Country and State.","error","error");
        } else if (mailingCountry === '' ) {
            var countryMap = component.get("v.countriesMap");
            var countryCode = component.get("v.MailingCountryKey");
            if (countryMap && countryCode && countryCode.length > 0) {
                try {
                    var countryValue = countryMap[countryCode];
                    if (countryValue && countryValue.length > 0 ) {
                        component.set("v.contact.MailingCountry", countryValue);
                    } else {
                        validTalent = false; 
                        this.showToast(component, "Please enter a valid Country and State.","error","error");
                    }
                } catch(error) {
                    console.error(error);
                    validTalent = false;
                    this.showToast(component, "Please enter a valid Country and State.","error","error");
                }
            } else {
                validTalent = false;
                this.showToast(component, "Please enter a valid Country and State.","error","error");
            }
        } 
        return validTalent;        
    },
    closeModal1: function(component) {
        var parentComponent = component.get("v.parent");
        parentComponent.reloadDatatable();
        this.toggleClassInverse(component,'backdropAddDocument','slds-backdrop--');
        this.toggleClassInverse(component,'modaldialogAddDocument','slds-fade-in-');
        
    },
    
    showToast : function(component, message, title, toastType, place){
        var toastEvent = $A.get("e.force:showToast");
        console.log(component.get('v.overlayPromise'))

        let toastParams = {
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        };

        if (component.get('v.overlayPromise')) {
            component.set('v.toast', toastParams);
            component.set('v.showToast', true);
        }
        if (place || !component.get('v.overlayPromise')) {
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }

    },
    
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    toggleClassInverse: function(component,componentId,className) {
        
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    
    startSpinner: function(component, event, helper){
        component.set("v.Spinner", true);
        var spinner = component.find('spinnerId');
        $A.util.addClass(spinner, 'slds-show');   
    },
    
    stopSpinner: function(component ){
        var spinner = component.find('spinnerId');
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');
        component.set("v.Spinner", false);   
    },
    getCountryMappings : function(component) {   
        var action = component.get("c.getCountryMappings");
        
        action.setCallback(this,function(response) {
            component.set("v.countriesMap",response.getReturnValue());
            if(component.get("v.contact.Talent_Country_Text__c").length == 0 && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
                component.set("v.contact.Talent_Country_Text__c",'United States');
                component.set("v.MailingCountryKey",'US');	
            }   
        });
        $A.enqueueAction(action);
    },
    
    updateMailingCountryCode: function(cmp, event) {
        var conKey = cmp.get('v.MailingCountryKey');
        var countrymap = cmp.get("v.countriesMap");
        var countrycode ;
        if (conKey !== null && typeof conKey !== 'undefined') {
            countrycode = countrymap[conKey];
            if(countrycode !== null && typeof countrycode !== 'undefined'){
                cmp.set("v.contact.MailingCountry",countrycode);
            }
        }
    },
    
    getKeyByValue : function(map, searchValue) {
        for(var key in map) {
            if (map[key] === searchValue) {
                return key;
            }
        }
    },
    
    setLatLongAddress : function (component) {
        var contact = component.get("v.contact");
        if(!component.get("v.isGoogleLocation")){
            component.set("v.contact.MailingLatitude",null);
            component.set("v.contact.MailingLongitude",null);
            if (contact.LatLongSource__c != null && contact.MailingCity == null && contact.MailingPostalCode == null) {
                component.set("v.contact.LatLongSource__c","");
            }	
        }
        var address = component.get("v.presetLocation");
        if (component.get("v.streetAddress2")) {
            address = address  +', '+component.get("v.streetAddress2");			
        }       
        component.set("v.contact.MailingStreet",(address != undefined ? address : ""));
    },

    checkForDuplicates : function(component, event ){

        this.startSpinner(component);

        var phoneList = [];

        if( component.get("v.referenceContact.Phone") ){
            phoneList.push( component.get("v.referenceContact.Phone") );
        }

        if( component.get("v.referenceContact.MobilePhone") ){
            phoneList.push( component.get("v.referenceContact.MobilePhone") );
        }

        if( component.get("v.referenceContact.HomePhone") ){
            phoneList.push( component.get("v.referenceContact.HomePhone") );
        }

        var action = component.get('c.callDedupeService');

        action.setParams({
            "fullName"  : component.get("v.referenceContact.FirstName") + ' ' + component.get("v.referenceContact.LastName"),
            "email"     : [component.get("v.referenceContact.Email")] ,
            "phones"    : phoneList,
            "recordType" : component.get("v.convertTo")
        });


        action.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                this.stopSpinner(component);
                var searchResults = JSON.parse( response.getReturnValue() );
        
                if( searchResults != null ){
                    component.set("v.dedupeResults", this.sortReferenceData(searchResults, 'Record_Type_Name__c')); 
        
                    this.toggleClassInverse(component,'backdropAddDocument','slds-backdrop--');
                    this.toggleClassInverse(component,'modaldialogAddDocument','slds-fade-in-');
                    this.displayResultOverlay(component, event );
                    
                    
                }else{
                    component.set("v.dedupeResults", null );
                    this.convertContact(component); 
                }
                
                
            }else if (state === "ERROR") {
                this.showToast(component, "There is some issue with Dedupe service, Please contact your Administrator for details.", "error", "error");
                this.stopSpinner(component);
            }
           

        });
        $A.enqueueAction(action);
        
    },

    sortReferenceData : function( referenceData , sortByKey) {
        
        var sortedReferences =  referenceData.sort(function(a, b) {
            var x = a[sortByKey]; var y = b[sortByKey];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
        
        /* Return in decending order Talent -> Reference -> Client  */
        return sortedReferences.reverse();
        
    }, 

                
    
    displayResultOverlay : function(component, event ){

        $A.createComponents([
                ["c:C_DedupeResultModal",{
                    dedupeResults: component.get("v.dedupeResults"),
                    selectedRecord: component.getReference("v.selectedRecord")}],
                ["c:C_DedupeResultFooter", {
                    	recordType: component.get("v.convertTo"),
                        handleSelectedContact: component.getReference("c.handleSelectedContact"),
                        handleCurrentContact: component.getReference("c.handleCurrentContact"),
                        handleCloseModal: component.getReference("c.handleCloseModal"),
                        toast: component.getReference("v.toast"),
                        showToast: component.getReference("v.showToast")
                    } 
                ]
            ],
            function(components, status){
                if (status === "SUCCESS") {
                    let modalBody = components[0];
                    let modalFooter = components[1];
                    let overlayPromise = component.find('dedupe_overlay').showCustomModal({
                        header: "Duplicate " + component.get("v.convertTo") + ' Contacts',
                        body: modalBody,
                        footer: modalFooter,
                        showCloseButton: false,
                        cssClass: "slds-modal_large",
                        closeCallback: function() {
                            
                        }
                    });

                    component.set("v.overlayPromise", overlayPromise );

                }else if (status === "ERROR") {
                    this.showToast(component, 'Some Error Occurred, Please Contact Your Administrator.!', 'error','error');
                }
            }
        );
        
    },

    use_selected_contact : function(component) {   
        
        this.startSpinner( component );

        var action = component.get("c.useSelectedContact");
        action.setParams({
            "selectedContactId"     : component.get("v.selectedRecord"),
            "existingContact_obj"   : component.get("v.referenceContact"),
            "referenceId"           : component.get("v.recordId").Id

        });
        
        action.setCallback(this,function(response) {
            
            var state           = response.getState();
            var reference_data  = response.getReturnValue();

            if (state === "SUCCESS") {
                this.closeModal1(component);
                this.closeDedupeOverlay(component)
                this.showToast(component, "Converted Successfully", "success", "success", true);

            }else{
                this.showToast(component,  response.getError() , "Error", "error");
            }

            this.stopSpinner( component );

            
        });

        $A.enqueueAction(action);
    },


    closeDedupeOverlay: function (component) {
        component.get("v.overlayPromise").then(
            function (modal) {
                modal.close();
                component.set("v.dedupeResults", null );
                component.set("v.selectedRecord", '');
            }
        );
    },
    
  
    
    
})