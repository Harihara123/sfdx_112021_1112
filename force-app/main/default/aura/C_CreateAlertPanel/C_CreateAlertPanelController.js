({
    init: function (cmp, e, h) {
       let randomAlertId = Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
       let randomFreqId = Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
       let randomEmailId = Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
       cmp.set('v.randomAlertId', randomAlertId);
       cmp.set('v.randomFreqId', randomFreqId);
       cmp.set('v.randomEmailId', randomEmailId);
    },
})