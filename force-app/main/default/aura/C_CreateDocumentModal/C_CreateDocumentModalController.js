({
	close : function(component, event, helper) {
        var docModalCloseEvt = component.getEvent("documentModalCloseEvent");
        docModalCloseEvt.fire();
        component.destroy();
    },

    doInit : function(component, event, helper) {
		//if(component.get("v.source") === 'OTHER') {
		if(component.get("v.source") != 'Find_Add_Talent' ) {
			var action = component.get('c.getResumeListToReplace');
			action.setParams({'talentAccountId' : component.get('v.contactId')});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if(state === 'SUCCESS') {
					component.set('v.resumeReplaceList', response.getReturnValue());
					var replaceList = component.get('v.resumeReplaceList');
					if(replaceList.length > 4) {
						var toast = {'title' : 'warning'
							, 'message' : $A.get('$Label.c.FIND_ADD_TALENT_MAXED_RESUMES_REPLACE_MSG')
							, 'type' : 'warning'
							, 'mode' : 'dismissible'};
						component.set('v.toast', toast);
						component.set('v.showToast', true);
					}
				}
				else if(state === 'ERROR') {
					var errors = response.getError();
					if(errors) {
						if(errors[0] && errors[0].message) {
							console.log('error message: ' + errors[0].message);
						}
						else {
							console.log('unknown error');
						}
					}
				}
			});
			$A.enqueueAction(action);
		}
    },

    saveDisabled : function(component, event, helper) {
        component.set('v.saveDisabled', true);
    },

    setSaveDisabled : function(component, event, helper) {
        component.set('v.saveDisabled', helper.determineSaveDisabled(component));
    },

    onReplaceResumeChange : function(component, event, helper) {
        var selectedResume = event.getSource().get("v.text");
        component.set("v.replaceResumeId" , selectedResume); 
        component.set('v.saveDisabled', helper.determineSaveDisabled(component));
    },

    saveText : function(component, event, helper) {
        component.set('v.showSpinner', true);
		helper.saveText(component);		
    },
	toggleResults: function (cmp, e, h) {
		h.showDupeResults(cmp, e);
	},
	invokeMerge: function (cmp, e, h) {
		h.invokeMerge(cmp,e)
	},
	closeResults: function (cmp, e, h) {
		h.closeDupeResults(cmp, e);
	},
})