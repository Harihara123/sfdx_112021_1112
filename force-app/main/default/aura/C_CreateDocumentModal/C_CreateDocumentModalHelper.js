({
    callSearchDedupe : function(component,talentDocId,contact){
        console.log('Inside call Search Dedupe');
        var action = component.get("c.callSearchDedupeService");
		action.setParams({
            "talentDocID": talentDocId,
            "Source" : component.get('v.source'),
            "contactDetails" : JSON.stringify(contact),
			"transactionId" : component.get('v.transactionId')
		});

		action.setCallback(this, function(response) {
            var source = component.get('v.source');
            //console.log(response);
			var state = response.getState();
			if (state === "SUCCESS" ) {
				console.log('success');
                if(response.getReturnValue()){
                    //console.log('Data Quality Service response: '+response.getReturnValue());

                    if (source==="Find_Add_Talent") {
                       if(response.getReturnValue() !== 'No_Result_Found'){
                        	this.performPostUploadAndCallingService(component,contact, JSON.parse(response.getReturnValue()));    
                        }else{
                            //console.log("when status code 200 but no duplicate result");
                            this.performPostUploadAndCallingService(component,contact, response.getReturnValue());    
                        }
                    } else {
                        if(response.getReturnValue() !== 'No_Result_Found'){
                        	this.showDupeResults(component, JSON.parse(response.getReturnValue()));
                        }else{
                            //console.log("when status code 200 but no duplicate result");
                            this.performPostUploadAndCallingService(component,contact,null);
                            
                        }
                    }
                  
                }
                else{
                    //console.log('No Duplicate Found');  
                    this.performPostUploadAndCallingService(component,contact,null);
                }
                
			} else {
				//console.log("ERROR .... ");
               this.performPostUploadAndCallingService(component,contact,null);
			}
			component.set("v.showSpinner", false);
		});
        $A.enqueueAction(action);
    },
    
    
    performPostUploadAndCallingService :function(component,contact,dupeResults){
        var source = component.get('v.source');
		var duplicateResult;
        if(dupeResults && dupeResults !==  'No_Result_Found'){
            duplicateResult = this.formatDupeResults(dupeResults);
        }else{
            duplicateResult = dupeResults;
        }
        if(source==="Find_Add_Talent") {
            var uploadParseCompleteEvt = $A.get("e.c:E_TalentUploadParseComplete");
            uploadParseCompleteEvt.setParams({
                "contact" : contact,
                "accountId" : component.get("v.accountId"),
                "dupeResults" : duplicateResult
            });
            uploadParseCompleteEvt.fire();
            
            component.destroy();
        }
        else {
            component.set('v.showSpinner', false);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                title: 'Success',
                message: 'Resume Uploaded Successfully!',
                type: 'success'
            });
            toastEvent.fire();
            this.refreshResumePreviewCard(component,component.get('v.talentDocumentId'));
            
            component.destroy();
        }
    },
    refreshResumePreviewCard: function (component,talentDocumentId) {
        
        var appEvent = $A.get("e.c:E_TalentDocumentsRefresh");
        appEvent.setParams({"isUploadCompare" : false});
        appEvent.setParams({"uploadedDocID" : talentDocumentId});
        appEvent.setParams({"source" : component.get("v.source")});
        appEvent.fire();
        
    },
    
    determineSaveDisabled : function(component) {
        var documentTitle = component.get('v.documentTitle');
        var richText = component.get('v.richText');
        var resumeReplaceList = component.get('v.resumeReplaceList');
        var replaceResumeId = component.get('v.replaceResumeId');
        if(!documentTitle || documentTitle.length === 0 || !documentTitle.trim()) {
            return true;
        }
        if(!richText || richText.length === 0 || !richText.trim()) {
            return true;
        }
        if(resumeReplaceList.length > 4 && !replaceResumeId) {
            return true;
        }
        return false;
    },
    saveText : function(component) {
        var source = component.get('v.source');
        var text = component.get('v.richText');
        var talentId = component.get('v.contactId')
        if(source==="Find_Add_Talent") { //Find/Add Talent fucntionality first creating the temp Account.
            talentId = component.get('v.accountId')
        }
        
        var params = {
            'talentId' : talentId
            , 'fileName' : component.get('v.documentTitle')
            , 'encodedURIText' : encodeURIComponent(text)
            , 'replaceResumeId' : component.get('v.replaceResumeId')
            , 'source' : source
        };
        
        var action = component.get('c.saveCopiedPasteContent');
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                
                if(source==="Find_Add_Talent") {
                    this.parseAndCreateTalent(component, response.getReturnValue());
                }
                else {
                    component.set("v.talentDocumentId",response.getReturnValue());
                   this.callSearchDedupe(component,response.getReturnValue(),null);
           //        this.performPostUploadAndCallingService(component,null,null);
                   
                }
            }
            else if(state === 'ERROR') {
                component.set('v.showSpinner', false);
                var toast = {'title' : 'Error'
							, 'message' : $A.get('$Label.c.ADD_NEW_TALENT_COPY_RESUME_PARSING_ERROR')
							, 'type' : 'error'
							, 'mode' : 'dismissible'};
				/*
				var toast = {'title' : 'Error'
							, 'message' : 'Error occured while parsing the resume.Please contact support.'
							, 'type' : 'error'
							, 'mode' : 'dismissible'};
							*/
						component.set('v.toast', toast);
						component.set('v.showToast', true);
                /*var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log('error message: ' + errors[0].message);
                    }
                    else {
                        console.log('unknown error');
                    }
                }*/
            }
        });
        $A.enqueueAction(action);
    },
    
    parseAndCreateTalent : function(component, talentDocumentId) {
        var params = {
            "talentDocumentId": talentDocumentId
        };
        
        var action = component.get('c.parseContentsAndCreateTalent');
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set('v.showSpinner', false);
            console.log('state-->>',state);
            console.log('response from resume parser-->'+response);
            if(state === 'SUCCESS') {
                var contact = null;
                component.set('v.showSpinner', false);
                if (response !== "errorparsingresume") {
                  
                    if(response.getReturnValue() != 'Unknown error' && response.getReturnValue() != 'errorparsingresume') {
                        
                        contact = this.getContactFromParserResponse(response.getReturnValue());
                        if(contact){
                            console.log('contact-->>',contact);
                        	component.set('v.contact', contact)
                        	this.callSearchDedupe(component,talentDocumentId,contact);
                        	//this.performPostUploadAndCallingService(component,contact,null);
                        }else{
                            var toast = {'title' : 'Error'
							, 'message' : $A.get('$Label.c.ADD_NEW_TALENT_COPY_RESUME_PARSING_ERROR')
							, 'type' : 'error'
							, 'mode' : ''};
							component.set('v.toast', toast);
							component.set('v.showToast', true);
                        }
                        
                    }else{
                        var toast = {'title' : 'Error'
							, 'message' : $A.get('$Label.c.ADD_NEW_TALENT_COPY_RESUME_PARSING_ERROR')
							, 'type' : 'error'
							, 'mode' : ''};
						component.set('v.toast', toast);
						component.set('v.showToast', true);
                    }
                }
                
            }
            else if(state === 'ERROR') {
                //var errors = response.getError();
               //Error occurred while parsing the resume due to an image.  Please remove any images and try again or use the "add talent" option to add talent."
			    var toast = {'title' : 'Error'
							, 'message' : $A.get('$Label.c.ADD_NEW_TALENT_COPY_RESUME_PARSING_ERROR')
							, 'type' : 'error'
							, 'mode' : ''};
						component.set('v.toast', toast);
						component.set('v.showToast', true);
               
			   /* var toast = {'title' : 'Error'
							, 'message' : 'Error occured while parsing the resume.Please contact support.'
							, 'type' : 'error'
							, 'mode' : ''};
						component.set('v.toast', toast);
						component.set('v.showToast', true);*/
                /* if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log('error message: ' + errors[0].message);
                    }
                    else {
                        console.log('unknown error');
                    }
                }*/
            }
        });
        $A.enqueueAction(action);
    },
    getContactFromParserResponse : function(parserResponse) {
        // Response should a one entry map indexed by attachment ID
        console.log('Inside getContactFromParserResponse');
        var data = JSON.parse(parserResponse);
        console.log('Inside getContactFromParserResponse');
        console.log(data);
        var keys = Object.keys(data);
        // Return the first entry in the map
        if (keys !== undefined && keys !== null && keys.length > 0) {
            return data[keys[0]];
        }
    },
    showDupeResults: function (cmp, dupeResults) {

        //console.log('logic for showing new comp with results from Search.');
        if (dupeResults.length > 1) {
            cmp.set('v.showDupeResults', !cmp.get('v.showDupeResults'));
            const modal = cmp.find('RelateToClientModal');
            if (cmp.get('v.showDupeResults')) {
                $A.util.addClass(modal, 'slds-modal_large');
                //console.log(dupeResults);
                cmp.set('v.dupeResults', this.formatDupeResults(dupeResults));
            } else {
                $A.util.removeClass(modal, 'slds-modal_large');
            }
        } else {this.performPostUploadAndCallingService(cmp, cmp.get('v.contact'),null);}
    },
    closeDupeResults: function (cmp, e) {
        cmp.set('v.showDupeResults', false);
        const modal = cmp.find('RelateToClientModal');
        $A.util.removeClass(modal, 'slds-modal_large');

        this.performPostUploadAndCallingService(cmp, cmp.get('v.contact'),null);
    },
    invokeMerge: function (cmp, e) {
        const dupeResults = cmp.find('tduperesults');
        console.log(dupeResults);
        dupeResults.merge();
    },
    formatDupeResults: function (dupeResults) {
        let formattedResults = [];
        const schema = ['talentId','firstName','lastName','fullName', 'city', 'contactId','candidateId','currentEmployerName', 'candidateStatus', 'employmentPositionTitle', 'resumeModifiedDate', 'qualificationsLastActivityDate'];
        const emailSchema = ['email', 'workEmail', 'personalEmail', 'otherEmail']
        const phoneSchema = [
            ['mobilePhone', '(M)'],
            ['homePhone', '(H)'],
            ['workPhone', '(W)'],
            ['otherPhone', '(O)']
        ]
        const USLocSchema = ['city', 'state', 'zipCode']
        const locSchema = ['country', 'city', 'zipCode']
        const locationHoverSchema = ['street', 'city', 'zipCode', 'country']
        dupeResults.forEach((dupe) => {
            let newObj = {};
            newObj.similarity = dupe.similarity.toFixed(2);
        	newObj.duplicateMatchFlags = dupe.duplicateMatchFlags[0];
            schema.forEach(key => newObj[key] = dupe.duplicateTalent[key])
			
            if(dupe.duplicateTalent.candidateStatus){
            	newObj.candidateStatus = dupe.duplicateTalent.candidateStatus.toUpperCase();    
            }
            
            let emails = [];
            emailSchema.forEach(key => {
                if (dupe.duplicateTalent[key]) {
                    emails.push({key: key, value: dupe.duplicateTalent[key]})
                }
            })
            newObj.emails = emails;

            let phones = [];
            phoneSchema.forEach(key => {
                if (dupe.duplicateTalent[key[0]]) {
                    phones.push({label: key[1], value: dupe.duplicateTalent[key[0]]})
                }
            })
            newObj.phones = phones;

            let location = [];
            if (dupe.duplicateTalent.country && dupe.duplicateTalent.country.toUpperCase() === 'UNITED STATES') {
                USLocSchema.forEach(key => {
                    if (dupe.duplicateTalent[key]) {
                        location.push(dupe.duplicateTalent[key])
                    }
                })
            } else {
                locSchema.forEach(key => {
                    if (dupe.duplicateTalent[key]) {
                        location.push(dupe.duplicateTalent[key])
                    }
                })
            }
            newObj.location = location;


            let locationHover = '';
            locationHoverSchema.forEach((key, i) => {
                if (dupe.duplicateTalent[key]) {
                    locationHover += locationHover += (i===0?'':',')+' '+dupe.duplicateTalent[key]
                }
            })
            newObj.locationHover = locationHover.trim();

            formattedResults.push(newObj);
        })

        return formattedResults;
    },
})