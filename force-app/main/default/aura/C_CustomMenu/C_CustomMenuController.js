({
    showMenu: function (cmp, e, h) {
        
            var trackingEvent = $A.get("e.c:TrackingEvent");
            trackingEvent.setParam('clickTarget', 'talent_actions--' + cmp.get('v.id'));
            trackingEvent.fire();

        const toggleClass = {
            'slds-show': 'slds-hide',
            'slds-hide': 'slds-show'
        };

        let container = document.getElementById(`container-${cmp.get('v.id')}`);

        cmp.set('v.menuState', toggleClass[cmp.get('v.menuState')]);

        setTimeout(() => {

            // D-17657: Updating position logic to show if drop down is within scrollable region.
            // If the bounds of the drop down are below the results-container div, show above instead.
            // This mitigates an issue with small resolution screens.
            let containerHeight = container.getBoundingClientRect().height;
            let containerBottom = 0;

            if (container.closest('.cC_SearchResultsTalentCard')) {
                containerBottom = container.closest('.cC_SearchResultsTalentCard').offsetTop + container.closest('.cC_SearchResultsTalentCard').offsetHeight + containerHeight + 20;
            }
            else if (container.closest('.cC_SearchResultsReqsNew')) {
                containerBottom = container.closest('.cC_SearchResultsReqsNew').offsetTop + container.closest('.cC_SearchResultsReqsNew').offsetHeight + containerHeight + 20;
            }

    		let parentHeight = container.closest('.results-container') ? container.closest('.results-container').scrollHeight : null;

            if (containerBottom > parentHeight) {
                container.className = container.className.replace('slds-dropdown_right', 'slds-dropdown_right slds-dropdown_bottom');
                container.style.visibility = 'visible';
            } else {
                container.style.visibility = 'visible';
            }
        }, 5);

    },

    closeMenu: function (cmp, e, h) {
        let container = document.getElementById(`container-${cmp.get('v.id')}`);

        if (cmp.get('v.id')) {
            container.style.visibility = 'hidden';
        }

        setTimeout(() => {
            cmp.set('v.menuState', 'slds-hide');
        }, 5);
    },

    disabledMenuItem: function (cmp, e, h) {
        e.preventDefault();
        e.stopPropagation();
    }

})