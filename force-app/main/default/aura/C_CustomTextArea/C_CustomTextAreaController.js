({
	change: function(component, event, helper) {
		//console.log("Change value --- " + document.getElementById(component.get('v.randomID')).value);
	},
    
    containerClick: function(component, event, helper) {
        let textarea = document.getElementById(component.get('v.randomID'));
        if (textarea) {
            textarea.focus();
        }
    },

    onFocus : function(component, event, helper) {
        if (event.type === 'keyup') {
            event.preventDefault();
        }
        if (event.type === 'keydown' && event.keyCode == 8) {
            helper.onCut(component);
            return;
        }

        let textarea = document.getElementById(component.get('v.randomID'));
        let rows = textarea.getAttribute('rows');
        
        while (textarea.scrollHeight > textarea.offsetHeight) {

            rows = +rows + 1;
            if (rows > component.get('v.maxRows')) {
                textarea.style.overflowY = 'scroll';
                rows = component.get('v.maxRows');
                textarea.setAttribute('rows', rows);
                break;
            }
            textarea.setAttribute('rows', rows);
        }
        let container = document.getElementById(component.get('v.data-id'));
        let truncatedClass = container.classList.contains('truncated-container');
        if (truncatedClass) {
            container.classList.remove('truncated-container')
        }
        let truncatedSM = container.classList.contains('truncated-sm');
        if (truncatedClass) {
            container.classList.remove('truncated-sm')
        }
        let focusedClass = container.classList.contains('focused-div-container');
        if (!focusedClass) {
            container.classList.add('focused-div-container')
        }
        let txtcontainer = document.getElementById(component.get('v.randomID')).parentNode;
        let truncatedContainerClass = txtcontainer.classList.contains('truncated-container');
        if (truncatedContainerClass) {
            txtcontainer.classList.remove('truncated-container')
            let truncatedSM = container.classList.contains('truncated-sm');
        if (truncatedClass) {
            container.classList.remove('truncated-sm')
        }
        }
		 let newRows = textarea.getAttribute('rows');  
		 component.set('v.value', textarea.value);
		 textarea.setAttribute('rows', newRows);
        if (event.type === 'paste' || event.type === 'drop') {
            setTimeout(() => {
                component.set('v.value', textarea.value);
                textarea.setAttribute('rows', newRows);
                textarea.scrollTop = textarea.scrollHeight;

            }, 0)
        }
        
    },

    onBlur : function(component, event, helper) {
        let textarea = document.getElementById(component.get('v.randomID'));
        textarea.style.height = 'auto';
        textarea.scrollTop = 0;
        textarea.scrollLeft = 0;
        textarea.style.overflowY = 'hidden';
        textarea.setAttribute('rows', 1);

        let txtcontainer = document.getElementById(component.get('v.randomID')).parentNode;
        let rows = textarea.getAttribute('rows');
        while (textarea.scrollHeight > textarea.offsetHeight) {
            rows = +rows + 1;
            if (rows > component.get('v.minRows')) {
                textarea.style.overflowY = 'hidden';

                rows = component.get('v.minRows');
                txtcontainer.classList.add('truncated-container');
                if (component.get('v.searchMatchField')) {
                    txtcontainer.classList.add('truncated-sm');
                }
                
                textarea.setAttribute('rows', rows);
                break;
            }
            textarea.setAttribute('rows', rows);
        }
        document.getElementById(component.get('v.data-id')).classList.remove('focused-div-container');
    },

    onCut: function(component, event, helper) {
        helper.onCut(component);
    },

    minChanged: function(cmp, event, helper) {
        let textarea = document.getElementById(cmp.get('v.randomID'));
        let rows = textarea.getAttribute('rows');
        while (textarea.scrollHeight > textarea.offsetHeight) {
            rows = +rows + 1;
            if (rows > cmp.get('v.minRows')) {
                textarea.style.overflowY = 'scroll';
                rows = cmp.get('v.minRows');
                let txtcontainer = document.getElementById(cmp.get('v.randomID')).parentNode;
                txtcontainer.classList.add('truncated-container');
                if (component.get('v.searchMatchField')) {
                    txtcontainer.classList.add('truncated-sm');
                }
                

                textarea.setAttribute('rows', rows);
                break;
            }
            textarea.setAttribute('rows', rows);
        }
    },

    valChange: function (component, event) {
        
        let textarea = document.getElementById(component.get('v.randomID'));

        if (textarea && component.get('v.value')) {
            textarea.value = component.get('v.value');
        }
        if (textarea) {
            let txtcontainer = document.getElementById(component.get('v.randomID')).parentNode;
            let rows = textarea.getAttribute('rows');
            while (textarea.scrollHeight > textarea.offsetHeight) {
                rows = +rows + 1;
                if (rows > component.get('v.minRows') && document.activeElement !== textarea) {
                    textarea.style.overflowY = 'hidden';
                    rows = component.get('v.minRows');
                    txtcontainer.classList.add('truncated-container');
                    if (component.get('v.searchMatchField')) {
                        txtcontainer.classList.add('truncated-sm');
                    }
                    
                    textarea.setAttribute('rows', rows);
                    break;
                }
                textarea.setAttribute('rows', rows);
            }
        }
    },
    clearValue: function(component) {
        let textarea = document.getElementById(component.get('v.randomID'));
        setTimeout(() => {
            textarea.value = '';
            textarea.setAttribute('rows', 1);

            if (document.getElementById(component.get('v.randomID')) && document.getElementById(component.get('v.randomID')).parentNode) {
                let txtcontainer = document.getElementById(component.get('v.randomID')).parentNode;
                if (txtcontainer.classList.contains('truncated-container')) {
                    txtcontainer.classList.remove('truncated-container');
                    txtcontainer.classList.remove('truncated-sm');
                }
            }
        }, 0)
    },
        init: function(component) {
        let randomID = 'textArea-' + Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
        component.set('v.randomID', randomID);
            let textarea = document.getElementById(component.get('v.randomID'));
    },

    setValue: function (cmp, e, h) {
        let randomId = cmp.get('v.randomID');
        let textarea = document.getElementById(cmp.get('v.randomID'));
		if(textarea){
           cmp.set('v.value', textarea.value);
		}
    },
})