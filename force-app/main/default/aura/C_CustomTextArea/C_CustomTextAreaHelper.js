({
    onCut : function(cmp) {
        setTimeout(() => {
            let textarea = document.getElementById(cmp.get('v.randomID'));
            textarea.style.overflow = 'hidden';
            textarea.setAttribute('rows', 1);

            let txtcontainer = document.querySelectorAll('[data-parent="text-area-container-' + cmp.get('v.randomID') + '"]')[0];
            let rows = textarea.getAttribute('rows');
            while (textarea.scrollHeight > textarea.offsetHeight) {
                rows = +rows + 1;
                if (rows > 10) {
                    textarea.style.overflowY = 'scroll';
                    
                    rows = 10;
//                    txtcontainer.classList.add('truncated-container');
                    textarea.setAttribute('rows', rows);
                    break;
                }
                textarea.setAttribute('rows', rows);
            }
        }, 0);
    }
})