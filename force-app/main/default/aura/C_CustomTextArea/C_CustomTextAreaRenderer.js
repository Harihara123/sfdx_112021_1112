({
    afterRender: function(cmp, helper) {
       // For pre-loaded values
        let textarea = document.getElementById(cmp.get('v.randomID'));
        if (cmp.get('v.value')) {
            textarea.value = cmp.get('v.value');
        }

        if (cmp.get('v.searchMode')) {
            textarea.style.resize = 'none';
        }

        let rows = textarea.getAttribute('rows');
        while (textarea.scrollHeight > textarea.offsetHeight) {
            rows = +rows + 1;
            if (rows > cmp.get('v.minRows')) {
                //textarea.style.overflowY = 'scroll';
                rows = cmp.get('v.minRows');
                let txtcontainer = document.getElementById(cmp.get('v.randomID')).parentNode;
                if (!cmp.get('v.searchMode')) {
                    txtcontainer.classList.add('truncated-container');
                } else {
                    txtcontainer.classList.add('truncated-search');
                }

                textarea.setAttribute('rows', rows);
                break;
            }
            textarea.setAttribute('rows', rows);
        }
        this.superAfterRender();
    }
})