({
	selectRecord : function(component, event, helper) {
		var checkboxes = component.find("select-contact");
        var clickedElement = event.getSource().get("v.name");
        
        if( Array.isArray(checkboxes) ){
            for (var i = 0; i < checkboxes.length; i++){
                if( component.find("select-contact")[i].get("v.name") == clickedElement){
                    if( component.find("select-contact")[i].get("v.value") ){
                        component.set('v.selectedRecord', clickedElement );
                    }else{
                        component.set('v.selectedRecord', null );
                    }
                    continue;
                }
                component.find("select-contact")[i].set("v.value", false);         
            }
            
        }else{
            if(component.find("select-contact").get("v.value")){
                component.set('v.selectedRecord', component.find("select-contact").get("v.name") );  
            }else{
                component.set('v.selectedRecord', null );
            }
            
        }
        
	}
    
    
})