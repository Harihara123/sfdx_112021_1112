({
	doInit : function(component, event, helper) {
		// alert(component.get('v.isactivity'));
		if(component.get('v.isactivity') ){
			component.set("v.popupId", component.get('v.RecordId') + '_activity_tab' );
		}else{
			component.set("v.popupId", component.get('v.RecordId') + '_detail_tab' );
		}
		var title = component.get('v.title');
		var type = component.get('v.type');
        var value = component.get('v.value');
		title = title.replace(' ','&nbsp;');

		if(title != ''){
			title = title + ':'
		}
		component.set("v.title", title);
        if(type === 'Percent'){
            if(value){
              var tempVal = parseFloat(value).toFixed(2);
              component.set('v.value',tempVal.toString());  
            }
            
       	}
     
	},

	showRecruiterPopup: function(component, event, helper) {
		
		var id = helper.getElementId(component, event);
		var domElement = document.getElementById(id);
		domElement.style.display = "block";
        var serviceResponse = "";
		if( component.get("v.userDetails") == null ){
			
            
			var action = component.get("c.getUserDetails");
			action.setParams({
	            "userId": component.get("v.UserId")
	        });

			action.setCallback(this, function(response){
                try{
    	            var state = response.getState();
                    serviceResponse = state;
    	            if (state === "SUCCESS") {
    	            	var userDetails =  response.getReturnValue().user;
    	            	var link 	=  response.getReturnValue().userLink;
    	                component.set("v.userDetails", userDetails );
    	                component.set("v.userLink", link );
    	            }

                }catch(ex){
                var exName = ex.name;
                var mesage = ex.message;
                var trackTrace = ex.stackTrace;
                var customMessage = "inputs to the apex, userId = " + component.get("v.UserId") + ". And serviceResponse = " + serviceResponse + " isComponent Valid " + (component != undefined && component.isValid());



                var errAction = component.get("c.saveErrorLog");
                errAction.setParams({
                    "errMsg": mesage,
                    "errStackTrace": trackTrace,
                    "errType" : exName,
                    "customException" : customMessage
                });
                errAction.setCallback(this,function(response){
                    if(response.getState() !=="SUCCESS")    {
                     console.log(response);   
                    }
                });
                $A.enqueueAction(errAction);

                //domElement.style.display = "none";
            }
			});            
			// Popover automaticlly closes after 5 seconds
			$A.enqueueAction(action); 
		}

		//setTimeout(function(){
		//	domElement.style.display = "none";
		//}, 5000);
	},

	closePopoup: function(component, event, helper) {
		var id = event.target.id;
		var domElement = document.getElementById(id);
		domElement.style.display = "none";
	},

	clickedRecruiter: function(component, event, helper) {
		// var url = 'lightning/setup/ManageUsers/page?address=%2F' +  + '%3Fnoredirect%3D1%26';
		// window.open(url,'_blank');
		
		if(component.get("v.userLink")){
			var urlEvent = $A.get("e.force:navigateToURL");
	         urlEvent.setParams({
	         "url": component.get("v.userLink")
	        });
	       	urlEvent.fire();	
		}
		
	},

})