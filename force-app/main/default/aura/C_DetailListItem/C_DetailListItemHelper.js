({
	getElementId : function(component, event) {
		var id = component.get('v.RecordId');

		if(component.get('v.isactivity') ){
			id = id + '_activity_tab';
		} else {
			id = id + '_detail_tab';
		}
		// console.log('xxxx' + id );
		return id;
	}
})