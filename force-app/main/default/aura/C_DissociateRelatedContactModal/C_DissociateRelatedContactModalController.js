({
	doInit: function (component, event, helper) {
		console.log('inside init');
         var params = {"soql": "select Name,Phone,MobilePhone,HomePhone,Email,Work_Email__c,Other_Email__c,Title from Contact where Related_Contact__c = '" + component.get("v.recordId") + "'"};
         var bdata = component.find("basedatahelper");         
         bdata.callServer(component,'','',"getRecord",function(response){
             component.set("v.contactRecord", response);
			 component.set('v.showSpinner',false);
         },params,false);
         
	},
    closeModal: function(component,event,helper){
        /*  Item decides what is getting delinked. For ex. if it is Client, it is from talent
			and vice versa.
		*/
		if(component.get('v.item')=="Client"){
			component.find('overlayLib').notifyClose();
		}
		else{
			$A.get("e.force:closeQuickAction").fire();
		}
    },
    removeAssociatedContact:function(component,event,helper){
        var contactId = component.get('v.recordId');
        var updateRecord = {
            'Id': contactId,
            'Related_Contact__c': null
        };
        var action = component.get('c.updateRecord');
        action.setParams({'record':updateRecord});
        component.set('v.showSpinner', true);
        component.set('v.buttonDisabled', true);
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"success",
                    "title":"Success",
                    "message":$A.get("$Label.c.ATS_RELATED")+" "+component.get('v.item')+" "+$A.get("$Label.c.ATS_REMOVE_RELATED_SUCCESS")
                });

            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"error",
                    "title":"Error",
                    "message":$A.get("$Label.c.ATS_RELATED")+" "+component.get('v.item')+" "+$A.get("$Label.c.ATS_TOAST_REMOVE_RELATED")
                });
            }
			/*  Item decides what is getting delinked. For ex. if it is Client, it is from talent
				and vice versa.
			*/
			if(component.get('v.item')=="Client"){
				component.find('overlayLib').notifyClose();
			}
			else{
				$A.get("e.force:closeQuickAction").fire();
			}
			
            toastEvent.fire();
        });
        $A.enqueueAction(action);
    }
    
})