({
    init: function (cmp, e) {
        const {Do_Not_Use__c, DNU_Reason__c, DNU_Expiration_Date__c} = cmp.get("v.contactObject");
        cmp.set('v.state', {Do_Not_Use__c, DNU_Reason__c, DNU_Expiration_Date__c});
        this.getMinDate(cmp)
        this.getAllPicklist(cmp, "DNU_Reason__c");
    },
    getMinDate: function (cmp, e, h) {
        let now = new Date();
        let minDate = now.toISOString().split('T')[0];

        cmp.set('v.minDate', minDate);
    },
    handleChange: function (cmp, e) {
        let state = cmp.get('v.state');
        if (e.getParam('value') === 'true') {
            state.Do_Not_Use__c = true;
        } else {
            state.Do_Not_Use__c = false;
            state.DNU_Reason__c = cmp.get('v.reasons')[0].value ;
            state.DNU_Expiration_Date__c = null;
        }
        cmp.set('v.state', state);
    },
    closeModal: function (cmp, e) {
        const modalPromise = cmp.get('v.dnuModalPromise');
        modalPromise.then(modal => modal.close());

        modalPromise.then(function(modal) { modal.close()});
    },
    saveData: function (cmp, e) {
        if(!this.validateDate(cmp,e)) {
            return;
        }
        const state = cmp.get('v.state');
        let conObj = cmp.get('v.contactObject');

        Object.keys(state).forEach(key => conObj[key] = state[key])

        cmp.set('v.loaded', false);
        var action = cmp.get("c.saveDNUDetails");
        action.setParams({conObj});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.loaded', true);

                var appEvent = $A.get("e.c:RefreshTalentHeader");
                appEvent.fire();
                this.showToast({type: 'success', message: 'Successfully Saved Do Not Use Preferences.'})
                this.closeModal(cmp, e);
            }
            else if (state === "INCOMPLETE") {
                cmp.set('v.loaded', true);

                this.showToast({type: 'error', message: $A.get("$Label.c.Error")})
                this.closeModal(cmp, e);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                cmp.set('v.loaded', true);

                this.showToast({type: 'error', message: $A.get("$Label.c.Error")})
                this.closeModal(cmp, e);
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function (params) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(params);
        toastEvent.fire();
    },
    getAllPicklist: function(component, fieldName) {
        var serverMethod = 'c.getAllPicklistValues';
        var params = {
            "objectStr": "Contact",
            "fld": fieldName
        };
        var action = component.get(serverMethod);
        action.setParams(params);

        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                const picklist = allValues["Contact.DNU_Reason__c"];
                this.setPicklistValues(component, picklist);
                component.set('v.loaded', true);
            }
        });
        $A.enqueueAction(action);
    },
    
    setPicklistValues : function(component, picklist){
        let reasons = [];
        picklist.forEach(list => reasons.push({label:list, value:list}));
        component.set('v.reasons', reasons);
    },
    validateDate: function (cmp, e) {
        const dateField = cmp.find("expDateInput");
        if(dateField) {
            const validity = dateField.reportValidity();
            if (!validity) {
                this.showToast({type: 'error', message: 'Expiration date is not correct.'});
                return validity;
            }
        }
        return true;
    },
})