({
    doInit: function(component, event, helper) {        
        if (component.get("v.sortableFields")) {   
			var fields;        
			if (typeof component.get("v.sortableFields") === "object") {
				fields = component.get("v.sortableFields");
			} else { 
				fields = JSON.parse(component.get('v.sortableFields'));
			}
            /*for (let z = 0; z < fields.length; z++) {
                if (fields[z].key === 'relevancyV2|desc') {
                fields[z].value = 'Relevance-Interest';
            }                
            }*/
			let runningUser = component.get("v.runningUser");             
            // var type= component.get("v.type");
			// if (type === 'C') {
                /*fields = fields.filter(val => {
                    let key = val.key.split('|');
                    return key[0] !== 'relevancyV2';
                }); */
            /*}
			if (type === 'R2C' && runningUser.companyName === 'AG_EMEA') {
                fields = fields.filter(val => {
                    let key = val.key.split('|');
                    return key[0] !== '';
                });
            } else {
                 fields = fields.filter(val => {
                    let key = val.key.split('|');
                    return key[0] !== 'relevancyV2';
                });
            }*/
            component.set('v.sortableFields', fields);
            //let sortData = component.get("v.selectedValue");
            /*if(sortData){
                sortData = sortData.split('|');
                if(sortData[0]==='relevancyV2'){
                    component.set("v.isChecked", true);
                }
            } */
            
            
            let lst = component.get("v.sortableFields");
            for (let i=0; i<lst.length; i++) {
                lst[i].selected = (lst[i].key === component.get("v.selectedValue"));
                // tracking default sorting order on page init
                if (lst[i].selected && lst[i]!=null ) {
                
                    var trackingEvent = $A.get("e.c:TrackingEvent");
                    trackingEvent.setParam('clickTarget', 'sort_order');
                    trackingEvent.setParam('inputValue', 'automated_' + lst[i]['key']);
                    trackingEvent.setParam('filter', {'sortOrder': lst[i]['key']});
                    trackingEvent.fire();
                }  
                
            }
            
            component.set("v.sortByList", lst);
            //if(component.get("v.enableToggle")){
			  
              
            
            
            //console.debug(sortData+'********************');
            /*if(runningUser && runningUser.companyName === 'AG_EMEA') {
				if (type === 'C' || (component.get("v.reExecuteSearchFlag") && !sortData)) { 
					component.set("v.isChecked", false);
				} else {
					component.set("v.isChecked", true);
				}
			}*/
			
            if (component.get("v.runningUser")) {
                if (component.get('v.selectedValue') == undefined 
                    && runningUser.companyName != 'AG_EMEA') {
                    var trackingEvent = $A.get("e.c:TrackingEvent");
                    trackingEvent.setParam('clickTarget', 'sort_order');
                    trackingEvent.setParam('inputValue', 'automated_relevance');
                    trackingEvent.setParam('filter', {"sortOrder": "relevance"});
                    trackingEvent.fire();            
                }
                
                
            }
        }
    },
    sortChange: function (component, event, helper) {
        var trackingEvent = $A.get("e.c:TrackingEvent");
        component.set('v.filterValue', Math.random().toString().split('.')[1]);
        var sortData = component.get("v.selectedValue");
        trackingEvent.setParam('filter', {"sortOrder": sortData.split("|")[0]});
        trackingEvent.fire();    
        if(sortData){ 
            sortData = sortData.split('|');            
                helper.sortChangeEvt(component, event);
           
        } else {
			let sortEvent = component.getEvent("sortEvent");
			sortEvent.setParams({
				"sortField": "",
				"sortOrder": ""
			});
			sortEvent.fire();
		}
    }
    
 })