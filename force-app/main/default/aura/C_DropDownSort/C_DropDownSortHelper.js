({
	sortChangeEvt : function(component, event) {
	if(component.get("v.selectedValue")!=null){
		let sortData = component.get("v.selectedValue").split('|');
		let sortEvent = component.getEvent("sortEvent");
		sortEvent.setParams({
			"sortField":sortData[0],
			"sortOrder": sortData[1]
		});
		sortEvent.fire();
	}
		

        var trackingEvent = $A.get("e.c:TrackingEvent");
	    trackingEvent.setParam('clickTarget', 'sort_order');
    	trackingEvent.setParam('inputValue', component.get("v.selectedValue") || 'relevance');
        trackingEvent.fire();
	},
})