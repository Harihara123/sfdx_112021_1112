({
    doInit: function(component, event, helper) {
        // Generate random ids for textarea wrapper and input
        let randomId = Math.floor(Math.random() * 100);
        let textAreaId = `textArea${randomId}`;
        let textAreaWrapperId = `textAreaWrapper${randomId}`;

        component.set("v.textAreaId", textAreaId);
        component.set("v.textAreaWrapperId", textAreaWrapperId);


    },

    Xfocus: function (component, event, helper) {
        component.set('v.keywordFocus', true);

        let textAreaWrapper = document.getElementById(component.get("v.textAreaWrapperId"));
        let textarea = document.getElementById(component.get("v.textAreaId"));

        if (textAreaWrapper.classList.contains("text-area-overflow")) {
            textAreaWrapper.classList.remove("text-area-overflow");
        }
        // FOCUS
        if (event.type === 'focus' || event.type === 'paste' || event.type === 'drop') {

            helper.onFocus(component, event);


            // KEYDOWN
        } else if (event.type === 'keydown') {

            // check if Enter
            if (event.keyCode === 13) {

                event.preventDefault();

                let searchEvt = component.getEvent('textareaSearchOnEnter');
                searchEvt.setParam( "criteria" , component.get("v.keyword"));
                searchEvt.fire();
                component.find("inputSearchId").getElement().blur();
                return;

                // if Backspace
            } else if (event.keyCode === 8) {

                helper.removeRows(component, event);

            } else {

                textarea.focus();
            }


        } else if (event.type === 'cut') {

            helper.removeRows(component, event);

        } else if (event.type === 'keyup') {

            if(event.keyCode !== 13 ){
                setTimeout(() => {


                    if (textarea.scrollHeight > textarea.offsetHeight - 2) {


                        while (textarea.scrollHeight > textarea.offsetHeight) {
                            // let rows = textarea.getAttribute('rows');
                            if (!(textarea.scrollHeight > textarea.offsetHeight)) {
                                break
                            }
                            let rows = component.get("v.rows");
                            let maxRows = component.get("v.maxRows");
                            rows = +rows + 1;
                            if (rows > maxRows) {
                                textarea.style.overflowY = 'scroll';
                                rows = maxRows;
                                // textarea.setAttribute('rows', rows);
                                component.set("v.rows", rows);
                                break;
                            }
                            // textarea.setAttribute('rows', rows);
                            component.set("v.rows", rows);

                        }


                    } else if (textarea.scrollHeight < textarea.offsetHeight - 2) {


                        while (textarea.scrollHeight < textarea.offsetHeight) {
                            // let rows = textarea.getAttribute('rows');
                            if (!(textarea.scrollHeight < textarea.offsetHeight)) {
                                break
                            }
                            let rows = component.get("v.rows");
                            rows = +rows - 1;
                            if (rows === 1) {
                                textarea.style.overflowY = 'hidden';
                                rows = 1;
                                // textarea.setAttribute('rows', rows);
                                component.set("v.rows", rows);
                                break;
                            }
                            // textarea.setAttribute('rows', rows);
                            component.set("v.rows", rows);
                        }


                    }

                }, 0);
            }
        } else {
            return;
        }

        if(event.type === "paste" || event.type === "drop"){
            setTimeout(()=>{
                helper.updateKeyword(component);
            },0);
        } else {
            helper.updateKeyword(component);
        }

    },

    Xblur: function (component, event, helper) {
        helper.onBlur(component, event);
    },

    toggleKeywordFieldWidth: function (component, event, helper) {
        let keywordFocus = component.get("v.keywordFocus"),
            textAreaEvt = component.getEvent("dynamicTextareaChange");

        textAreaEvt.setParam("keywordFocus", keywordFocus);
        textAreaEvt.fire();

    }
})