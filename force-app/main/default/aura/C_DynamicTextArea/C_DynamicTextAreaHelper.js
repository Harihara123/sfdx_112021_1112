({
	onFocus: function(component, event) {
        setTimeout(() => {
            let textarea = document.getElementById(component.get("v.textAreaId"));

            while (textarea.scrollHeight > textarea.offsetHeight - 2) {
                if (!(textarea.scrollHeight > textarea.offsetHeight)) {break;}
                // let rows = textarea.getAttribute('rows');
                let rows = component.get("v.rows");
                let maxRows = component.get("v.maxRows");
                rows = +rows + 1;
                // textarea.setAttribute('rows', rows);
                component.set("v.rows", rows);
                if (rows > maxRows) {
                    textarea.style.overflowY = 'scroll';
                    rows = maxRows;
                    // textarea.setAttribute('rows', rows);
                    component.set("v.rows", rows);
                    break;
                }

            }
            while (textarea.scrollHeight < textarea.offsetHeight - 2) {
                // let rows = textarea.getAttribute('rows');
                if (!(textarea.scrollHeight < textarea.offsetHeight)) {break;}
                let rows = component.get("v.rows");
                rows = +rows - 1;
                // textarea.setAttribute('rows', rows);
                component.set("v.rows", rows);
                if (rows === 1) {
                    textarea.style.overflowY = 'hidden';
                    rows = 1;
                    // textarea.setAttribute('rows', rows);
                    component.set("v.rows", rows);
                    break;
                }

            }
            this.updateKeyword(component);
            return;

        }, 250);



    },

    onBlur: function(component, event) {
        event.stopPropagation();
        component.set('v.keywordFocus', false);
        let textarea = document.getElementById(component.get("v.textAreaId"));
        textarea.style.height = 'auto';
        textarea.scrollTop = 0;
        textarea.scrollLeft = 0;
        textarea.style.overflow = 'hidden';
        // textarea.setAttribute('rows', 1);
        component.set("v.rows", 1);
        textarea.style.overflowY = 'hidden';



        let textAreaWrapper = document.getElementById(component.get("v.textAreaWrapperId"));

        const textAreaWidth = textAreaWrapper.clientWidth;
        // Calculate the length of the text based on root font size of the browser
        let rootFontSize = window.getComputedStyle(document.body,null).getPropertyValue("font-size").replace('px','');
        let textWidth = component.find("inputSearchId").getElement().value.length * rootFontSize;

        if(textWidth > textAreaWidth || textarea.scrollHeight < textarea.offsetHeight - 2) {
            textAreaWrapper.classList.add("text-area-overflow");
        }

    },

    removeRows: function (component, event) {
        setTimeout(() => {
            let textarea = document.getElementById(component.get("v.textAreaId"));
            textarea.style.overflow = 'hidden';
            textarea.setAttribute('rows', 1);
            // component.set("v.rows", 1);
            let rows = textarea.getAttribute('rows');
            // let rows = component.get("v.rows");
            let maxRows = component.get("v.maxRows");
            while (textarea.scrollHeight > textarea.offsetHeight - 2) {
                if (!(textarea.scrollHeight > textarea.offsetHeight)) {break;}
                rows = +rows + 1;
                if (rows > maxRows) {
                    textarea.style.overflowY = 'scroll';
                    rows = maxRows;
                    textarea.setAttribute('rows', rows);
                    // component.set("v.rows", rows);
                    break;
                }
                textarea.setAttribute('rows', rows);
                // component.set("v.rows", rows);
            }
            this.updateKeyword(component);
            return;

        }, 0);
        ;
    },

    updateKeyword: function (component) {
	    let textAreaValue = document.getElementById(component.get("v.textAreaId")).value;
	    component.set("v.keyword", textAreaValue);

    }
})