({
    toggleEngine : function(component, event, helper) { 
		component.set('v.toggleTriggered', true);		
        component.set('v.googleSearch', !component.get('v.googleSearch'));
        let gSearch = component.get('v.googleSearch');
        let transactionEvent = $A.get("e.c:TrackingEvent");
        transactionEvent.setParam('clickTarget', 'search-engine');
        transactionEvent.setParam('inputValue', gSearch ? 'Google' : 'ES');
        transactionEvent.fire();
    }
})