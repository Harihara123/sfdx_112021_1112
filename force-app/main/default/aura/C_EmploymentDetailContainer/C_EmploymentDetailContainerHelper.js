({
	createComponent : function(cmp, componentName, targetAttributeName, params) {
         $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    // Show offline error
                }
                else if (status === "ERROR") {
                    // Show error message
                }
            }
        );
    }
})