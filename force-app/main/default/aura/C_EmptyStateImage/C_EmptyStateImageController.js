({
	doInit : function(component, event, helper) {
		var imageName = component.get("v.imageType"),
			img = helper.setImageUrl(component, imageName),
			resourceUrl = $A.get("$Resource.ATS_Empty_State_Images"),
			imgUrl = `${resourceUrl}/slds-empty-state-images${img}`;

		component.set("v.imageUrl", imgUrl);

	}
})