({
	setImageUrl : function(component, imageName) {
		var imageMap = [{
			imgTitle: 'No Content',
			imgUrl: '/noContent.svg'
		},{
			imgTitle: 'No Task',
			imgUrl: '/noTask.svg'
		},{
			imgTitle: 'No Events',
			imgUrl : '/noEvents.svg'
		},{
			imgTitle: 'No Connection',
			imgUrl: '/noConnection.svg'
		},{
			imgTitle: 'No Access',
			imgUrl: '/noAccess.svg'
		},{
			imgTitle: 'No Data',
			imgUrl: '/noData.svg'
		},{
			imgTitle: 'Start',
			imgUrl: '/startImage.svg'
		}];
		
		if(imageName !== undefined){
			// Set Selected Image
			for (var i = 0; imageMap.length > i; i++) {
				if(imageMap[i].imgTitle === imageName){
					return imageMap[i].imgUrl;
				}
			}
		} else {
			// Set Random Image
			var randomImg = Math.floor(Math.random() * imageMap.length);

			return imageMap[randomImg].imgUrl;
		}
	}
})