({
	downloadFile: function (component, event, helper) {
		var action = component.get("c.downloadTheFile");
        action.setParams({
			"fileSrc" : component.get("v.fileSrc"),
			"fileLocator" : component.get("v.fileLocator")
		});

        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {                    
				var resp = JSON.parse(response.getReturnValue());

				if (resp.errorMsg) {
					helper.toastError(resp.errorMsg);
				} else {
					helper.writeToFile(component, resp);
				}
            } else {
				helper.toastError(component, 'Unknown error downloading file!');
			}
        });
        $A.enqueueAction(action);

	}
})