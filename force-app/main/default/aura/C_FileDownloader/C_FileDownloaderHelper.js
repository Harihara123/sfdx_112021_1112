({
	b64toBlob: function (b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, {type: contentType});
		return blob;
	},

	writeToFile: function(component, response) {
		var blob = this.b64toBlob(response.fileContent, "application/octet-stream");

		var link = document.createElement('a');
		link.href = URL.createObjectURL(blob);
		var fnameOVerride = component.get("v.filename");
		link.download = fnameOVerride ? fnameOVerride : response.filename;
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	},

	toastError: function(errorMsg) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error!",
			"message": errorMsg,
			"type": "error",
			"duration": 3000
		});
		toastEvent.fire();
	}

})