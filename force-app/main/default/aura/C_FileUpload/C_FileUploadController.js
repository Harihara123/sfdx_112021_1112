({  
    resetFileUploadComponent : function(component, event, helper) {
        //S-41999 Added by Karthik
        var fileInput = component.find("fileId").getGlobalId(); 
        //fileInput.value = ''; 
        component.set("v.fileDetails", "");
        component.set("v.validationMsg", '');
    },
    
    //S-41999 Added by Karthik
    handleFilesChange: function(component, event, helper) {
        if (event.getSource().get("v.files").length > 0) {
            var fileName = event.getSource().get("v.files")[0]['name'];
            var fileSize = event.getSource().get("v.files")[0]['size'];
        }
        
        var file = event.getSource().get("v.files")[0];
        component.set("v.fileData", file);
        
        var ucEvent = component.getEvent("fileSelected");
        if (file) {
            //console.log("Handle filechange controller: " + fileName);
            component.set("v.fileDetails", fileName + ", " + helper.upsizeUnit(fileSize));
            ucEvent.setParams({ "fileName" : fileName});
        }else{
            component.set("v.fileDetails", "No File Selected");
        }   
        ucEvent.fire();
        
        component.set("v.validationMsg", '');
    },
    //S-41999 Added by Karthik - End

    save : function(component, event, helper) { 
        //console.log('save called ');
		
        var params = event.getParam('arguments');
		//console.log('params ' ,params);
        if (params){
            var Id = params.objectId;
			//console.log('parentId ' ,Id);
            component.set("v.parentId", Id);
            helper.save(component, Id);
        }
        else {
            //throw error
        }            
    },
})