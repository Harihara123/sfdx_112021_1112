({
    MAX_FILE_SIZE: 5242880, /* 6 000 000 * 3/4 to account for base64 */  
    CHUNK_SIZE: 600000, /* Use a multiple of 4 */

    // Save the file: breaks file into manageable chuncks, base64 encodes them 
    // and pass them to Apex controller to attach to parentId.
    save : function(component, Id) {
        //S-41999 Added by Karthik
        //console.log('upload cmp helper save called');
        var file = component.get("v.fileData");
    	//Validate File size and Type
        this.validateAndSaveFile(component, file, Id)
   },
    
    saveFile: function(component, file, Id) {
        var fr = new FileReader();
        var self = this;
        
        fr.onload = $A.getCallback(function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            self.upload(component, file, fileContents, Id);
        });
        fr.readAsDataURL(file);
	},
        
    // Upload initializes the file transfer then control passes to uploadChunk
    upload: function(component, file, fileContents, Id) {
	//console.log('upload called');
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);

       	// start with the initial chunk. Note that we pass an empty fileId (last parameter).
       	// This causes the attachment object to be created on the back end.
        this.uploadChunk(component, file, fileContents, fromPos, toPos, '', Id);   
    },
     
    // Recursive method that continues to pass file chuncks until the entire file is passed.
    uploadChunk : function(component, file, fileContents, fromPos, toPos, attachId, parentId) {
		//console.log('uploadChunk called');
        var action = component.get("c.saveTheChunk"); 
        var chunk = fileContents.substring(fromPos, toPos);
		//console.log('uploadChunk called ',action);
		//console.log('uploadChunk called ',component.get("v.parentId"));
		//console.log('uploadChunk called ',file.name);
	//	console.log('uploadChunk called ',encodeURIComponent(chunk));
	//	console.log('uploadChunk called ', file.type);
	//	console.log('uploadChunk called ', attachId);
        action.setParams({
            parentId: parentId,
            fileName: file.name,
            base64Data: encodeURIComponent(chunk), 
            contentType: file.type,
            fileId: attachId
        });

   //     console.log('uploadChunk params set');
        var self = this;
        action.setCallback(this, function(a) {            
            if(a.getState() === "SUCCESS") {
                attachId = a.getReturnValue();
                fromPos = toPos;
                toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);

                // upload the next chunk. If there are no more chunks, fire the uploadComplete
                // event so that the containing controller can clean up (e.g. hide the spinner).
                if (fromPos < toPos) {
                    self.uploadChunk(component, file, fileContents, fromPos, toPos, attachId);  
                }
                else {
                    //Fire uploadComplete event passing the filename as a parameter
                 //   console.log("firing file upload complete event");
                    var ucEvent = component.getEvent("uploadComplete");
                    ucEvent.setParams({ "fileName" : file.name});
                    ucEvent.fire();

                     


                }
            } else if (a.getState() === "ERROR") {
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        // System Error
                        //cmp.set("v.message", errors[0].message);
                      //  console.log("caught the error" + errors[0].message);
                        return;
                    }
                }
            }
        });
        $A.enqueueAction(action); 
    },

    validateAndSaveFile : function(component, file, Id) {
        if (file.size > this.MAX_FILE_SIZE) {
			component.set("v.validationMsg", 'File size cannot exceed ' + this.upsizeUnit(this.MAX_FILE_SIZE) + ' bytes.\n' +
    		  'Selected file size: ' + this.upsizeUnit(file.size));    		  
        	return;
        }
        
        var fileName = file.name;

        var action = component.get("c.validateFileType"); 
        action.setParams({"fileName": fileName});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var validFileType = response.getReturnValue();
                if (!validFileType) {
                    component.set("v.validationMsg", 'Please select a valid file type');
                } else {
                   // console.log('saveFile called')
                	this.saveFile(component,file, Id);
                }    
            }
        })
        $A.enqueueAction(action);
    },       
        
    upsizeUnit : function(size) {
        var base = size / 1024;
        var unit;

        if (base / 1024 > 1) {
            unit = " MB";
            base = base / 1024;
        } else {
            unit = " KB";
        }

        var floor = Math.floor(base);
        var firstDecimal = Math.floor((base - floor) * 10);
        return floor + "." + firstDecimal + unit;
    }
})