({
	init : function(component, event, helper) {        
        var	errFn = $A.getCallback(function(event){	
            helper.setErrorLog(component,event.error);
 		});        
        window.addEventListener("error",errFn);        
	}
})