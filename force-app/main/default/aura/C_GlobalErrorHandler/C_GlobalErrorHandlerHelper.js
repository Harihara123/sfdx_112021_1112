({
	setErrorLog : function(component,error) {
        var action = component.get('c.saveErrorLog');
        action.setParams({
			"errMsg": error.message,
			"errStackTrace": error.stackTrace,
            "errType" : error.name
        });
		action.setCallback(this,function(response){
			if(response.getState() !=="SUCCESS")	{
                console.log('error during creating log');
                // Report error via email to users.
			}
		});
		$A.enqueueAction(action);	
	}
})