({
	doInit : function(component, event, helper) {
		var loadingSpinner = component.find('loading');
        $A.util.addClass(loadingSpinner, 'slds-hide');
		var noCurrents = component.find('noCurrents');
        $A.util.addClass(noCurrents, 'slds-hide');
        helper.Init(component, event, helper);
	},
	editRecord : function (component, event, helper) {
		var recordId = event.currentTarget.dataset.recordid;
		var editRecordEvent = $A.get("e.force:editRecord");
		editRecordEvent.setParams({
			 "recordId": recordId
		});
		editRecordEvent.fire();
	},
    setSingleSelect : function(component, event, helper) {
        var singleSelect = component.get('v.singleSelect');
        var rowValue = event.getSource().get("v.value");
        if(singleSelect && rowValue === singleSelect) {
            component.set('v.singleSelect', null);
        } else {
            component.set('v.singleSelect', rowValue);
        }
	},
	sortColumn: function (component, event, helper) {
		helper.sortData(component);
    },
	handleFilterChange:function(component, event, helper) {
		helper.getDataforEndDate(component, event, helper);
	},
    matchReq: function (component, event, helper ) {
        //var idx = event.getSource().get("v.name");
        var idx = event.target.parentElement.id;
		var eleId = idx.split("_")[1];
        var record = component.get("v.dataRows");
        var rec = record[eleId];
        var coloumns = rec.dataCells;
        if(coloumns.length>2){
            var contId = coloumns[0].label;
            var accId = coloumns[1].value;
            var srcName = '';
            var familyName = '';
            if(coloumns.length>=7){
              srcName = coloumns[7].label;
            }

            helper.sendToURL(
            $A.get("$Label.c.CONNECTED_Summer18URL")
            + "/n/Search_Reqs?c__matchTalentId=" + accId
            + "&c__contactId=" + contId
            + "&c__srcName=" + srcName + " " + familyName
            + "&c__noPush=true"
            );
        }
    }
})