({
	Init : function(component, event, helper) {
		var factMap = component.get("v.factMap");
		if( factMap ){
			var groupingKey = component.get("v.groupingKey");
			let titles = {
				Green: 'Recent Service Related Activities (Within Last 15 Days)',
				Yellow: 'Service Related Activities Within 15-30 Days Ago',
				Red: 'No Recent Service Related Activities (Within Last 90 Days)'
			}
			factMap[groupingKey+"!T"].rows.forEach((item) => {

				let checklist = ['Red','Yellow','Green'];
				for (let listItem of checklist) {
					if (item.dataCells[4].label && item.dataCells[4].label.includes(listItem)) {
						item.dataCells[4].title = titles[listItem];
					}
				}
				console.log(item.dataCells[4].label)
			});

			component.set("v.dataRows", factMap[groupingKey+"!T"].rows);

		}
        this.alignText(component);
	},
    alignText : function(component) {
        var textAlign = component.get('v.cellTextAlign');
        if( textAlign != null ) {
            var elementToAlign = component.find('elementToAlign');
            if (elementToAlign == null) {
                return;
            }
            switch (textAlign) {
                case 'right':
                    $A.util.addClass(tdToAlign, 'textAlignRight');
                    break;
                case 'left':
                    $A.util.addClass(tdToAlign, 'textAlignLeft');
                    break;
                case 'center':
                    $A.util.addClass(tdToAlign, 'textAlignCenter');
                    break;
            }
        }
    },
	//Start S-124835 for Sorting "My Current Talent" card by Siva 3/28/2019
	sortData: function(component) {
		var loadingSpinner = component.find('loading');
		$A.util.removeClass(loadingSpinner, 'slds-hide');
		var noCurrents = component.find('noCurrents');
		$A.util.addClass(noCurrents, 'slds-hide');
		var col = component.get("v.sortColumn");
		var isAsc = component.get("v.sortDirection");
		var isAscFactor = isAsc ? 1 : -1;
		var list = component.get('v.dataRows');
		var listData = [];
		var filter = component.get("v.filter");
		var minFilterDays = new Date();
		var maxFilterDays = new Date();
		if(filter != 'All') {
			minFilterDays = new Date(minFilterDays.setDate(minFilterDays.getDate() + parseInt(filter-15)));
			maxFilterDays = new Date(maxFilterDays.setDate(maxFilterDays.getDate() + parseInt(filter)));
		}
		for(var i=0 ; i<list.length ; i++) {
			for(var j=0; j<list[i].dataCells.length ; j++) {
				listData[i] = list[i].dataCells;
			}
		}
		listData.sort(function(o1, o2) {
			switch (col) {
				case "Account.Name" : 
					var x = o1[1].label.toLowerCase();
					var y = o2[1].label.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "Account.Talent_Start_Date__c" : 
					var x = o1[2].label=='-'?'':new Date(o1[2].label);
					var y = o2[2].label=='-'?'':new Date(o2[2].label);
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "Account.Talent_End_Date__c" : 
					var x = o1[3].label=='-'?'':new Date(o1[3].label);
					var y = o2[3].label=='-'?'':new Date(o2[3].label);
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "Contact.Service_Status__c" : 
					var x = o1[4].label.toLowerCase();
					var y = o2[4].label.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "Contact.Days_Since_Service__c" :
					var x = o1[5].value;
					var y = o2[5].value;
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "Account.Talent_Current_Employer_Formula__c" :
					var x = o1[6].label.toLowerCase();
					var y = o2[6].label.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;
		
				default :
					var x = o1[3].label=='-'?'':new Date(o1[3].label);
					var y = o2[3].label=='-'?'':new Date(o2[3].label);
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;
			}
		});
		for(var i=0 ; i<listData.length ; i++) {
			list[i].dataCells = listData[i];
		}
		if(filter != 'All') {
			list = list.filter(function (element) {
				switch(filter) {
					case "15" :
						return (new Date(element.dataCells[3].label) <= maxFilterDays) ? true : false;

					case "30" :
					case "45" :
					case "60" :
						return (minFilterDays < new Date(element.dataCells[3].label) && new Date(element.dataCells[3].label) <= maxFilterDays) ? true : false;
						
					case "61" :
						return (new Date(element.dataCells[3].label) >= maxFilterDays) ? true : false;

					default :
						return false;
				}
			});
		}
		component.set('v.dataRows', list);
		$A.util.addClass(loadingSpinner, 'slds-hide');
		if(list.length == 0) {
			$A.util.removeClass(noCurrents, 'slds-hide');
		}
	},
	//End S-124835 for Sorting "My Current Talent" card by Siva 3/28/2019
	getDataforEndDate: function(component, event, helper) {
		var loadingSpinner = component.find('loading');
		$A.util.removeClass(loadingSpinner, 'slds-hide');
		component.set('v.singleSelect', null);
		this.Init(component, event, helper);
		component.set("v.sortColumn", "Account.Talent_End_Date__c");
		component.set("v.sortDirection", "true");
		this.sortData(component);
	},
    sendToURL : function(url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
    }
})