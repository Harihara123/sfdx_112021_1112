// Hotkeys to map:
// G2 - Ctrl+Shift+E
// Log a call - Ctrl+Shift+L
// Save - Ctrl+Shift+Z

({
    init : function(cmp, e, h) {
       if(window.location.pathname.includes('/Contact/')){
            h.initG2Modal(cmp);
       }
        const keyDownFunc = $A.getCallback(function(e){
            if(window.location.pathname.includes('/Contact/')){
                const osName = (navigator.appVersion.indexOf("Win") != -1) ? "Windows OS" : "MacOS";

                if(cmp.isValid()){
                    const modifier = e.ctrlKey || e.metaKey;
                    const shiftkey = e.shiftKey;
                    // Short cut for G2 Modal
                    if(modifier && shiftkey && e.key === 'e' && (osName === 'Windows OS' || osName === 'MacOS')){
                        e.preventDefault();
                        h.handleG2(cmp);
                    }
                    // Other methods below
                }
            }
        });
        cmp.set("v.listener", keyDownFunc);
        window.addEventListener('keydown', keyDownFunc, false);

    },
    locationChangeHandler: function(cmp, e, h) {
        const url = window.location.pathname.split('/');
        if(window.location.pathname.includes('/Contact/') && cmp.get("v.recordId") !== url[4]){
            h.initG2Modal(cmp);
       }
    }
})
