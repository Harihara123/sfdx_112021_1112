({
    // G2 Modal methods
    initG2Modal: function(cmp) {
        const url = window.location.pathname.split('/');

        cmp.set('v.recordId', url[4]);
    },

    handleG2 : function(cmp) {
        const recordId = cmp.get("v.recordId");
		
        const urlEvent = $A.get("e.c:E_TalentSummEditNew");
        urlEvent.setParams({
            "recId": recordId,
            "source": "Profile"
        });
        urlEvent.fire(); 
    },

    // End G2 Modal methods
    handleLogACall: function () {
        
    },

    handleSave: function () {
        
    }

})
