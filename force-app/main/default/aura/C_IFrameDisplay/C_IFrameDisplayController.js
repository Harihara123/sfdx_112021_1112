({
	doInit : function(component, event, helper) {
        var pg = component.get("v.pageURL");
        var recordId = component.get("v.recordId");
        var src = pg.replace('{RecordID}', recordId);
     	component.set("v.src",src);
      
    }
})