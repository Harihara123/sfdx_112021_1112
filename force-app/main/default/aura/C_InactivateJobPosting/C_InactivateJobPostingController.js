({
	doInit : function(component, event, helper) {
		var eventParams = event.getParams();
        let jpId = component.get("v.recordId");
		let action = component.get("c.postingStatus");
		action.setParams({"jpId":jpId});
		action.setCallback(this, function(response){
			if(response.getState() === 'SUCCESS') {
				let sw = response.getReturnValue();
				
						if(sw.owner === 'SAME') {
							if(sw.status === 'ACTIVE') {
								helper.inactivateJobPosting(component, event);
							}
							else {
								helper.showToastMessage('Inactivation Failed: Posting is Inactive.','','WARNING');
							}
						}
						else{
							helper.showToastMessage('Inactivation Failed: Posting not owned by You.','','WARNING');
						}
					
			}
			else if(data.getState() == 'ERROR'){
				helper.showToastMessage('Error occured while inactivating Job Posting','ERROR','ERROR');
			}
		});

		$A.enqueueAction(action);
	},
	onRender : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire(); 

     }
    
    /* handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
			let action = component.get("c.switchSFDC");
			action.setCallback(this, function(response){
				if(response.getState() === 'SUCCESS') {
					let sw = response.getReturnValue();
					if(sw === 'RWS') {
						helper.refreshPosting(component, event, helper);
						console.log("Record is loaded successfully.");
					}
					else {
						helper.inactivateJobPosting(component, event);
					}
				}
				else if(data.getState() == 'ERROR'){
					console.log('$A.log("Errors", a.getError())------>'+data.getError());
					helper.showToastMessage('Error occured while inactivating Job Posting in RWS','ERROR','ERROR');
				}
			});

			$A.enqueueAction(action);
            
           
        } else if(eventParams.changeType === "ERROR") {
            var errorMessage=component.get("v.recordError");
             var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: errorMessage,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
              var dismissActionPanel = $A.get("e.force:closeQuickAction");
             dismissActionPanel.fire(); 
        }
    }*/
})