({
	inactivateJobPosting : function(cmp, event) {
		let jpId = cmp.get("v.recordId");
		let equestId = cmp.get("v.simpleRecord.eQuest_Job_Id__c");
		let action = cmp.get("c.inactivateJobPosting");
		action.setParams({"jpId" : jpId});

		action.setCallback(this, function(response){
			if(response.getState() === 'SUCCESS') {
				this.showToastMessage('Inactivation Successful.','SUCCESS','SUCCESS');
			}
			else if(data.getState() == 'ERROR'){
                this.showToastMessage('Error occured while inactivating Job Posting','ERROR','ERROR');
            }
		});
		$A.enqueueAction(action);
	},
    showToastMessage : function (message,title,type){
	     var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : title,
                    message: message,
                    messageTemplate: '',
                    duration:' 5000',
                    key: 'info_alt',
                    type: type,
                    mode: 'pester'
                });
                toastEvent.fire();
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire(); 
  }

})