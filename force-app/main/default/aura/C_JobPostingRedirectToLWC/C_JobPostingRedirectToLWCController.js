({
	doInit: function (component, event, helper) {		
		const pageRef = component.get("v.pageReference");
		component.set("v.oppId", pageRef.state.c__oppId);
		component.set("v.jpId", pageRef.state.c__jpId);
		component.set("v.isEquestReturnCall",pageRef.state.c__isEquestReturnCall);
		if(pageRef.state.c__isEquestReturnCall){
			helper.emeaCommitCall(component)
		}
		
	},
	redirectToReq: function(cmp, e, h) {
		let redId = e.getParam('recordId');	
		let objName = 'Job_Posting__c';

		if(redId.substring(0, 3) === '006') {
			objName = 'Opportunity';
		}

		const navService = cmp.find("navService");
		const pageRef = {
			"type": "standard__recordPage",
			"attributes": {
				"recordId": redId,
				"objectApiName": objName,
				"actionName": "view"
			}
		};
		e.preventDefault();
		navService.navigate(pageRef);
	},

	setSpellCheck: function(cmp, e, h) {
		const isSpellChecked = e.getParam('spellCheck');
		cmp.set("v.isSpellChecked", isSpellChecked);
	},

	setSoftWarningCheck: function(cmp, e, h) {
		const softWarningCheck = e.getParam('softWarningCheck');
		cmp.set("v.softWarningCheck", softWarningCheck);
	},

	openPreviewPage : function(component, event, helper) {
		let jobPosting = event.getParam('jobPosting');
		let spclBoard = event.getParam('spclBoard');
		let rtLabel = event.getParam('roadtechLbl');
		let emeaBoards = event.getParam('emeaBoards');
		let wrapperResponse = event.getParam('wrapperResponse');
		component.set("v.jobPosting", jobPosting);
		component.set("v.togglePage", 'PRVW_PAGE');
		component.set("v.spclBoard", spclBoard);
		component.set("v.roadtechLbl", rtLabel);
		component.set("v.emeaBoards", emeaBoards);
		component.set("v.wrapperResponse", wrapperResponse);
	},
	openFormPage : function(component, event, helper) {
		const prevData = event.getParam('prevData');
		component.set("v.jobPosting", prevData);		
		component.set("v.togglePage", 'FORM_PAGE');
		component.set("v.previewBack", true); 
	},
	processEquest : function(component, event, helper){
		component.set("v.equestUrl", event.getParam('equestUrl'));
		component.set("v.togglePage", 'EQUEST_PAGE');
	}
})