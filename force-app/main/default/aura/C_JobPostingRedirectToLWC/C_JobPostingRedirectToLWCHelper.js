({
	emeaCommitCall: function (cmp) {
		let jpId = cmp.get("v.jpId");
		let action = cmp.get("c.emeaCommitCall");
		action.setParams({"jpId" : jpId});
		
		action.setCallback(this, function(response){
			let state = response.getState();
			if(state === 'SUCCESS') {				
				let listViewId = response.getReturnValue();
				this.showToast('Success', $A.get("$Label.c.ATS_JOB_POSTING_SUCCESS_TOAST"), 'Success', 'pester');
				this.navigateToMyJobPostingListView(cmp, listViewId);				
			}
			else {
				var errors = action.getError();
				if (errors[0]){
				this.showToast('Error', errors[0].message, 'error', 'sticky');
				}
			}
		});		
		$A.enqueueAction(action);		
	},
	navigateToMyJobPostingListView : function(cmp, listViewId){
		var navService = cmp.find("navService");
		let pageRef = {
			type : 'standard__objectPage'	,
			attributes : {
				objectApiName : 'Job_Posting__c',
				actionName : 'list'
			},
			state : {
				filterName : listViewId
			}			
		}
		navService.navigate(pageRef);
	},
	showToast: function (title, message, type, mode) {
        const toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type,
            mode: mode
        });
        toastEvent.fire();
    }
})