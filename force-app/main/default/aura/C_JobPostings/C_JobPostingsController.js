({
	loadJobPostings : function(component, event, helper) {
        var action;
		//alert(component.get("v.filter"));
        if(component.get("v.filter") == 'my'){
        	action = component.get("c.getMyJobPostings");    
        }else if(component.get("v.filter") == 'recent'){
            action = component.get("c.getRecentJobPostings");
        }
        
		//action.setParams({"filter":  component.get("v.filter")});
        
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
                component.set("v.job_postings", response.getReturnValue());
			} else {
				console.log("ERROR .... ");
			}
		})

		$A.enqueueAction(action);
        
        
	},


	selectJobPosting : function(component, event, helper) {

		var checkboxes = component.find("select-job");
		var clickedElement = event.getSource().get("v.name");
		var jobPostingId = event.getSource().get("v.text");
		var doc = new DOMParser().parseFromString(jobPostingId, "text/xml");
		jobPostingId = doc.firstChild.innerHTML; 



		if( Array.isArray(checkboxes) ){

			for (var i = 0; i < checkboxes.length; i++){

				if( component.find("select-job")[i].get("v.name") == clickedElement){
					if( component.find("select-job")[i].get("v.value") ){
						component.set('v.selectedJobPostingId', clickedElement );
						component.set("v.postingId",jobPostingId);
					}else{
						component.set('v.selectedJobPostingId', null );
					}
	        		continue;
				}
				
	        	component.find("select-job")[i].set("v.value", false);	       
	        }

		}else{

			if(component.find("select-job").get("v.value")){
				component.set('v.selectedJobPostingId', component.find("select-job").get("v.name") );
				component.set("v.postingId",jobPostingId);
					
			}else{
				component.set('v.selectedJobPostingId', null );
			}
			
		}
		
		
	},

	sendInvitation : function(component, event, helper) {

		var params = event.getParam("arguments");
        var email = params.email;
        var talentId = params.talentId;

        var jobPostingId = component.get('v.selectedJobPostingId');
        // var talentId = component.get('v.talentId');

        // alert(jobPostingId);
        if($A.util.isUndefinedOrNull( jobPostingId) ){



        	var toastEvent = $A.get("e.force:showToast");
        	toastEvent.setParams({
                    type: 'error',
                    message: 'Please select a job posting to proceed..'
                });
            toastEvent.fire();
        	return;
        }

        component.set("v.isLoading", true );
        
        helper.callJobPostingService(component, jobPostingId, email, talentId );
		console.log('https:'+'//allegisgroup-stg.phenompro.com/us/en/job/'+ component.get("v.postingId"));
	}


})