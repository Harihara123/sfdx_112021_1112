({
    callJobPostingService : function(component, jobPostingId, email,talentId ) {
        
        var action = component.get("c.callJobPostingService");
        
        action.setParams(
            {	"postingId":  jobPostingId ,
             "toEmail":  email,
             "talentId" : talentId
            }
        );
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
                console.log('Response---> '+ responseValue);
                if(responseValue !="Error"){
                    // var responseBody = JSON.parse(responseValue);
                    var responseBody = responseValue;
                 //   console.log('responseBody.responseMsg--->'+responseBody.responseMsg);
                    if(responseBody =='Success' || JSON.parse(responseBody).responseMsg === 'SUCCESS'){
                     //   console.log('Inside ResponseMessage Success condition');
                        this.showToast(component,'Job Posting Invite process Initiated','success');
                        
                    }else{
                     //   console.log('Inside ResponseMessage Error else condition');
                        this.showToast(component,'Failed to initiate Job Posting Invite process, Please contact helpdesk','error');
                    }
                }else{
                  //  console.log('Inside Class statuscode other than 200 Error else condition');
                    this.showToast(component,'Failed to initiate Job Posting Invite process, Please contact helpdesk','error');
                }

                
            } else {
              //  console.log('Inside Class Exception Error else condition');
                this.showToast(component,'Failed to initiate Job Posting Invite process, Please contact helpdesk','error');
            }
            component.set("v.isLoading", false );
        })
        
        $A.enqueueAction(action);
        
        
    },
    showToast : function(component, message,type ){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            message: message
        });
        toastEvent.fire();
        // Calling Parent Component to Close Modal
        var parentComponent = component.get("v.parent");
                parentComponent.closeModal();
    }
})