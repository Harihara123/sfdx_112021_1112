({
	inputKeyWordsAction : function(component, event, helper) {
        helper.handleInputKeyWordsAction(component);
    },

    addOnEnter : function(component, event, helper) {
        if (event.getParams().keyCode === 13) {
            helper.handleInputKeyWordsAction(component);
        }
    },
    comboValueChangeHandler : function(component, event, helper) {
        component.set("v.currentTextVal", component.get("v.comboValue"));
    },
    newTermSelected : function(component, event, helper) {
        var s = event.getParams();
        helper.addTermsToState(component, [s.value]);
        helper.redrawPills(component);
        helper.fireMatchFacetChangedEvt(component); //To enable multi select gesture on Search/Match
        component.set("v.newMatchTerm",'');
    },
    updatePillsRelatedTerms : function(component, event, helper) {
        helper.updatePillsRelatedTerms(component);
    }
})