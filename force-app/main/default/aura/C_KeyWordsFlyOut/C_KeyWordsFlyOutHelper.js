({
	handleInputKeyWordsAction : function(component){       
        var excludeKeyWords = component.find("exkw");
        var exwkValue = excludeKeyWords.get("v.value");
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'exclude_keyword_facet');
        trackingEvent.setParam('inputValue', exwkValue);
        trackingEvent.fire();
        exwkValue = exwkValue.trim();
        if(exwkValue != "" && exwkValue.length >0){
            var arrPills = [];
            var keyWords = exwkValue.split(",");

            for(var kc = 0; kc<keyWords.length; kc++){
                var pillValue = keyWords[kc];
                pillValue = pillValue.trim();
                if(pillValue.length>0){
                    var pillObj = {};
                    pillObj.label = pillValue;
                    pillObj.id = Math.floor(Math.random() * 10000000000);
                    arrPills.push(pillObj);
                }
            }

            var keyWordsFlyOutEvent = component.getEvent("keyWordsFlyOutEvent");        
            keyWordsFlyOutEvent.setParams({
                handlerId : component.get("v.handlerId"),
                inpputValues : arrPills
            });
            keyWordsFlyOutEvent.fire();
        }
    },
    addTermsToState : function(component, addedTerms) {
        var normalizedData = component.get("v.normalizedData");
        if (normalizedData === undefined || normalizedData === null) {
            normalizedData = [];
        }
        for (var i=0; i<addedTerms.length; i++) {
            normalizedData.push(this.getNewNormalizedTerm(component, addedTerms[i]));
        }

        component.set("v.normalizedData", normalizedData);
    },
    redrawPills : function(component) {
        // New pillsArray - overwrite the entire set every time.
        var pillsArray = [];
        var essPills = [];

        var pillData = component.get("v.normalizedData");
        component.set("v.pillsList", pillsArray);
        component.set("v.essentialPillList", essPills);
        if (pillData.length === 0) {
            return;
        }

        var isPillHidden = false;
        var pillsToShow = 15;

        if(pillData.length > 15 && component.get("v.showMoreLess") === "show more"){
                isPillHidden = true;
                component.set("v.isPillHidden", isPillHidden);

        }else if (pillData.length < 15){
            pillsToShow = pillData.length;
        }
        
        if(component.get("v.showMoreLess") === "show less"){
            pillsToShow = pillData.length;
        }

        var related = component.get("v.relatedTerms");

        for (var j=0; j<pillsToShow; j++) {
            var isRequired = pillData[j].required === "true" || pillData[j].required === true;
            // enableClick = true if this term has related terms.
            var relTerms = related ? related[pillData[j].key] : [];
            var cmpParams = {
                "pillLabel" : pillData[j].key,
                "pillId" : pillData[j].pillId,
                "pillColorClass" : pillData[j].pillColorClass,
                "showFavorite" : true,
                "isFavorite" : isRequired,
                "enableClick" : relTerms !== undefined && relTerms.length > 0
            };

            $A.createComponent("c:C_SearchFacetPill", 
                cmpParams, 
                function(newComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        // SUCCESS  - insert component in markup
                        if (isRequired) {
                            essPills.push(newComponent);
                            component.set("v.essentialPillList", essPills);
                        } else {
                            pillsArray.push(newComponent);
                            component.set("v.pillsList", pillsArray);
                        }
                    } else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.");
                        // Show offline error
                    } else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
            });
        }
    },
    updatePillsRelatedTerms : function(component) {
        var related = component.get("v.relatedTerms");

        var essPills = component.get("v.essentialPillList");
        var othPills = component.get("v.pillsList");

        for (var i=0; i<othPills.length; i++) {
            var relTerms = related ? related[othPills[i].get("v.pillLabel")] : [];
            othPills[i].set("v.enableClick", relTerms.length > 0);
        }
        for (var i=0; i<essPills.length; i++) {
            var relTerms = related ? related[essPills[i].get("v.pillLabel")] : [];
            essPills[i].set("v.enableClick", relTerms.length > 0);
        }
    },
})