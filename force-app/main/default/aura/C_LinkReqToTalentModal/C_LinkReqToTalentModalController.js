({	
    doInit : function (component, event, helper){
        
        
		if(component.get('v.context')!='ReqSearch'){
			return;
		}
		  helper.createTalentSearch(component); //S-84236 Neel - Open talent search in C_LinkReqToTalentModal
		  helper.loadRecentViewdTalent(component);		  
		  //Rajeesh 6/29/2018 S-S-85442
		  var oppFields = component.get('v.oppFields');
		  if(component.get('v.context')=='ReqSearch'){
			component.set("v.modalState", "Open");
		  }
    },
	recInit : function (component, event, helper){
		//invoke from force record data
		var vmsReq = component.get("v.simpleOpp.Req_VMS__c");
        var stage = component.get("v.simpleOpp.StageName");
        //Srini - Set OFCCP param on record data
        var ofccp = component.get("v.simpleOpp.Req_OFCCP_Required__c");
        component.set("v.ofccp",ofccp);
         
        if(stage=='Staging' && vmsReq){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Message:',
                message:'Talent(s) cannot be Linked/Submitted to a staging req',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
        	toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
        }else {
            helper.createTalentSearch(component); //S-84236 Neel - Open talent search in C_LinkReqToTalentModal
            helper.loadRecentViewdTalent(component);
            if(stage =='Closed Wash' || stage =='Closed Lost' || stage =='Closed Won' || stage =='Closed Won (Partial)'){
                //	alert('Cannot Link to Talent if Req in '+stage+' stage');
                var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Message:',
                message:'Cannot Link to Talent if Req is in Closed Stage',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
            toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
            } 
        }
    },
    closeModal : function(component, event, helper) {
		component.set("v.modalState", "Closed"); 
		helper.closeModal(component);
    },
    save : function (component, event, helper){
		
        helper.saveToServer(component, event);
    },
    
    //Added by Krishna
    submit : function (component, event, helper){
		component.set('v.sourceType','Submitted');
        helper.submitOrders(component, event);
    },
    
	saveAsApplicant:function (component, event, helper){
		helper.saveAsApplicant(component, event);
    },
	saveAsApplicantOnEnter : function(component, event, helper) {
		if(event.getParams().keyCode === 13) {
			helper.saveAsApplicant(component, event);
		}
	},
    selectRow : function (component, event, helper){
        
        helper.selectRow(component,event);
    },
	updateSelectedRows: function(component,event,helper){
        var stage = component.get("v.simpleOpp.StageName");
        if(stage !='Closed Wash' && stage !='Closed Lost' && stage !='Closed Won' && stage !='Closed Won (Partial)'){
			helper.updateSelectedRows(component,event);
        }
	},
	handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
           // record is loaded (render other component which needs record data value)
            //console.log("Record is loaded successfully.");
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },
	openSourceModal: function(component,event,helper){
		helper.openSourceModal(component);
	},
	closeSourceModal: function(component,event,helper){
		helper.closeSourceModal(component);
	},
	////S-84237 Neel - 
	talentSearchModalResultsEventHandler : function(component, event, helper) {
        var stage = component.get("v.simpleOpp.StageName");
        var scrollTop = component.find("modalContent").getElement();
        if(stage !='Closed Wash' && stage !='Closed Lost' && stage !='Closed Won' && stage !='Closed Won (Partial)'){
            
                component.find("linkbtn").set("v.disabled",true); 
            if(component.get('v.applicantUser')) {
                component.find("linkapplicantbtn").set("v.disabled",true);
            }
            if(event.getParam("sourcebutton") === "search"){
                component.set("v.showTalentSearchResults", event.getParam("showTalentSearchResults"));

                helper.backToTop(component, scrollTop, 100);
            }else if(event.getParam("sourcebutton") === "refresh"){
                helper.destroyTalentSearch(component);
                helper.createTalentSearch(component);		
                component.set("v.showTalentSearchResults",event.getParam("showTalentSearchResults"));    
            }    
        }
		
	},
    /* Sandeep : Go back to RecentlyViewed button changes*/ 
    backToRecentlyViewed : function(component, event, helper){
	 component.find("linkbtn").set("v.disabled",true); 
	 if(component.get('v.applicantUser')) {
	 	component.find("linkapplicantbtn").set("v.disabled",true);
	 }
     helper.destroyTalentSearch(component);
	 component.set("v.showTalentSearchResults",false);
     helper.createTalentSearch(component);
	}
})