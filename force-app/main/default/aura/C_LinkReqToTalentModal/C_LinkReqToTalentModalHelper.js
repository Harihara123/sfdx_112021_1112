({
    
    closeModal : function(component) {
		this.destroyTalentSearch(component);
		if(component.get('v.context')=='ReqSearch'){
			component.find("overlayLib").notifyClose();
		}
		else{

			$A.get("e.force:closeQuickAction").fire();
		}
		
    },
	//S-84236 Neel - Open talent search in C_LinkReqToTalentModal
	createTalentSearch : function(component) {
		//Rajeesh add opportunity id to make sure that search returns linked value.
		//opportunityId=0061p00000gWdOrAAK&fromsubpage=FALSE
		var optyId = '';
		if(component.get('v.context')=='OptyRecord'){
			optyId = component.get('v.recordId');
		}
		else{
			optyId= component.get('v.oppFields').Id;
		}
		var params = {
			externalsource : 'RQ' ,
		    displayResults : component.get('v.showTalentSearchResults'),
		     "aura:id":"talsearch",
			opportunityId:optyId,
			googleSearch: false
		};
		$A.createComponent(
			"c:C_SearchContainerCandidate",
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					component.set("v.C_SearchContainerCandidate", newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			} 
		); 
		
	},
    loadRecentViewdTalent : function(cmp) {
        //console.log('getting data from server...');
		this.getCurrentUser(cmp);
		//Rajeesh fixing linking issue
		var optyId = '';
		if(cmp.get('v.context')=='OptyRecord'){
			optyId = cmp.get('v.recordId');
		}
		else{
			optyId= cmp.get('v.oppFields').Id;
		}
        let getRecentCons = cmp.get("c.getRecentViewedContacts");
		getRecentCons.setStorable();
		getRecentCons.setParams({optyId:optyId});
        getRecentCons.setCallback(this, function(response){
            let state = response.getState();
			// S-92105: Added by Natalie Lockett 7/19/18
			cmp.set('v.spinner', false);
            if (state === "SUCCESS") {
				var resp = response.getReturnValue();
                //console.log('result ',resp);
				cmp.set('v.contactWrapperList',resp);
				var contactList=[];
				for(var key in resp){
				//console.log('key:'+key+':val'+resp[key]);
					contactList.push(resp[key].con);
				}
                cmp.set("v.contacts", contactList);
            }
        });
        $A.enqueueAction(getRecentCons);
    },
	saveAsApplicant : function(component, event) {
		var appSource = component.find('applicantSource');
		if($A.util.isEmpty(component.get('v.applicantSource')))
		{
			appSource.set("v.errors",[{message:"Source is a required field."}]);
			return;
		}
		else{
			component.set('v.sourceType','Applicant');
			appSource.set("v.errors",null);
			this.saveToServer(component, event);
		}
	},
    saveToServer : function(cmp,evt){
		var spinner = cmp.find('spinner');
		$A.util.removeClass(spinner, 'slds-hide');
        let selectdRowIds = cmp.get("v.selectdRowIds");
        //console.log('selectdRowIds ',selectdRowIds);
		let optyRecord = cmp.get('v.opportunity');
		var optyAccountId='';
		var optyId='';
		if(cmp.get('v.context')=='OptyRecord'){
			optyAccountId = optyRecord.fields.AccountId.value;
			optyId=cmp.get("v.recordId");
		}
		else{
			optyAccountId = cmp.get('v.oppFields').AccountId;
			optyId=cmp.get("v.opportunityId");
			//console.log('account id'+optyAccountId);
		}
		//console.log('optyAccountId--->'+optyAccountId);
		let saveOrders = cmp.get("c.saveOrders");
        //string opportunityId, string optyAccountId, List<Id> conIds
		var successMsg;
		//The Talent was successfully linked to the Req(s) as an Applicant
		if($A.util.isEmpty(cmp.get('v.applicantSource'))){
			successMsg = "The Req was successfully linked to the Talent(s).";
		}
		else{
			successMsg = "The Talent(s) were successfully linked to the Req as Applicant(s).";
			//The talent(s) were successfuly linked to the req as applicant(s)
		}	
		saveOrders.setParams({ 
				opportunityId : optyId,
				optyAccountId : optyAccountId,
				applicantSource: cmp.get("v.applicantSource"),	
				sourceType: cmp.get("v.sourceType"),	
		        conIds : selectdRowIds})
		
		saveOrders.setCallback(this, function(response) {
            var state = response.getState(); 
			var toastEvent = $A.get("e.force:showToast");

			if(state === 'SUCCESS'){
				toastEvent.setParams({
				    "type":"success",
					"message": successMsg
				});
                  //Code to Refresh Submittal Chart
                    var appEvent = $A.get("e.c:RefreshReqChart");
                    appEvent.setParams({
                        "oppId" : optyId });
                    appEvent.fire();
				this.closeModal(cmp);
			}else{
				this.closeModal(cmp);
			 toastEvent.setParams({
				    "type":"error",
					"message": "The Talent(s) were not linked to the Req. Please try again."
				});
			}
			toastEvent.fire();
			   
			});

        $A.enqueueAction(saveOrders);
    },
     //Added by Krishna for Submit
    submitOrders : function(cmp,evt){
		var spinner = cmp.find('spinner');
		$A.util.removeClass(spinner, 'slds-hide');
        let selectdRowIds = cmp.get("v.selectdRowIds");
        //console.log('selectdRowIds ',selectdRowIds);
		let optyRecord = cmp.get('v.opportunity');
		var optyAccountId='';
		var optyId='';
		if(cmp.get('v.context')=='OptyRecord'){
			optyAccountId = optyRecord.fields.AccountId.value;
			optyId=cmp.get("v.recordId");
		}
		else{
			optyAccountId = cmp.get('v.oppFields').AccountId;
			optyId=cmp.get("v.opportunityId");
			//console.log('account id'+optyAccountId);
		}
		//console.log('optyAccountId--->'+optyAccountId);
		let saveOrders = cmp.get("c.saveOrders");
        //string opportunityId, string optyAccountId, List<Id> conIds
		var successMsg = 'The Talent(s) were successfully submitted to the Req.';
		
		saveOrders.setParams({ 
				opportunityId : optyId,
				optyAccountId : optyAccountId,
				applicantSource: cmp.get("v.applicantSource"),	
				sourceType: cmp.get("v.sourceType"),	
		        conIds : selectdRowIds})
		
		saveOrders.setCallback(this, function(response) {
            var state = response.getState(); 
			var toastEvent = $A.get("e.force:showToast");

			if(state === 'SUCCESS'){
				toastEvent.setParams({
				    "type":"success",
					"message": successMsg
				});
                  //Code to Refresh Submittal Chart
                    var appEvent = $A.get("e.c:RefreshReqChart");
                    appEvent.setParams({
                        "oppId" : optyId });
                    appEvent.fire();
				this.closeModal(cmp);
			}else{
				this.closeModal(cmp);
			 toastEvent.setParams({
				    "type":"error",
					"message": "The Talent(s) were not submitted to the Req. Please try again."
				});
			}
			toastEvent.fire();
			   
			});

        $A.enqueueAction(saveOrders);
    },
  /*  selectOrUnselectAll : function(cmp,isAll){
        cmp.set("v.selectdRowIndexes", []);
        let length = cmp.get("v.contacts").length;
        if(isAll && length){
            let selectdRowIndexes=[];
            for(let i=0;i < length ; i++){
                selectdRowIndexes.push(i);
            } 
            cmp.set("v.selectdRowIndexes",selectdRowIndexes);
        }
        this.toggleLinkBtncmp(cmp);
    },*/
    selectRow : function(cmp,evt){
        let rowId = evt.getSource().get("v.text");
        let isChecked= evt.getSource().get("v.value");
        let selectdRowIds = cmp.get("v.selectdRowIds");
        if(isChecked){
            selectdRowIds.push(rowId);
        }else{
            selectdRowIds.splice(rowId,1);
        }
        cmp.set("v.selectdRowIds",selectdRowIds);
        this.toggleLinkBtn(cmp);
    },
	updateSelectedRows: function (cmp,evt){
	//see whether previous method can be reused by defining the same properties?
		let rowId = evt.getParam("recordId");//evt.getSource().get("v.recordId");
        let isChecked= evt.getParam("value");
        let selectdRowIds = cmp.get("v.selectdRowIds");
        if(isChecked){
            selectdRowIds.push(rowId);
        }else{
            selectdRowIds.splice(rowId,1);
        }
        cmp.set("v.selectdRowIds",selectdRowIds);
		this.toggleLinkBtn(cmp);
	},
    toggleLinkBtn : function(cmp){
        if($A.util.isEmpty(cmp.get("v.selectdRowIds") )){
				this.buttonsPlayground(cmp);
				/*cmp.find("linkapplicantbtn").set("v.disabled",true);
				cmp.find("linkbtn").set("v.disabled",true);
				
				var applicantSource = cmp.find('applicantSource');
				$A.util.addClass(applicantSource, 'slds-hide');
		
				var applicantCancel = cmp.find('applicantCancel');
				$A.util.addClass(applicantCancel, 'slds-hide');

				var savebtn = cmp.find('savebtn');
				$A.util.addClass(savebtn, 'slds-hide');
				var modalCancel = cmp.find('modalCancel');
				$A.util.removeClass(modalCancel, 'slds-hide');*/

        }else{
			if(cmp.get('v.applicantUser')){
				cmp.find("linkapplicantbtn").set("v.disabled",false);
				
			}
			//cmp.find("linkbtn").set("v.disabled",false); 
            //var oppofccp = cmp.get("v.simpleOpp.Req_OFCCP_Required__c");
            if(cmp.get('v.ofccp')==false || cmp.get('v.ofccp')=="false"){
             	cmp.find("submitbtn").set("v.disabled",false);   
                cmp.find("linkbtn").set("v.disabled",false); 
            } 
        }
        
    },
	buttonsPlayground : function(cmp) { //S-84236 Enable/Disable and Hide/display button
		if(cmp.get('v.applicantUser')){
			cmp.find("linkapplicantbtn").set("v.disabled",true);
		}
		cmp.find("linkbtn").set("v.disabled",true);
         cmp.find("submitbtn").set("v.disabled",true);
				
		var applicantSource = cmp.find('applicantSource');
		$A.util.addClass(applicantSource, 'slds-hide');
		
		var applicantCancel = cmp.find('applicantCancel');
		$A.util.addClass(applicantCancel, 'slds-hide');

		var savebtn = cmp.find('savebtn');
		$A.util.addClass(savebtn, 'slds-hide');
		var modalCancel = cmp.find('modalCancel');
		$A.util.removeClass(modalCancel, 'slds-hide');
	},
	getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");

        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var r = JSON.parse(response.getReturnValue());
                component.set ("v.runningUser", r);
				if(r.category =='CATEGORY2'){
					component.set('v.applicantUser',true);
				}
			}
        });
        $A.enqueueAction(action);
    },
	openSourceModal: function(component){
		var linkapplicantbtn = component.find('linkapplicantbtn');
		$A.util.addClass(linkapplicantbtn, 'slds-hide');

		var modalCancel = component.find('modalCancel');
		$A.util.addClass(modalCancel, 'slds-hide');
		
		var linkbtn = component.find('linkbtn');
		$A.util.addClass(linkbtn, 'slds-hide');
        
        var submitbtn = component.find('submitbtn');
		$A.util.addClass(submitbtn, 'slds-hide');

		var applicantSource = component.find('applicantSource');
		$A.util.removeClass(applicantSource, 'slds-hide');

		var applicantCancel = component.find('applicantCancel');
		$A.util.removeClass(applicantCancel, 'slds-hide');
		
		var savebtn = component.find('savebtn');
		$A.util.removeClass(savebtn, 'slds-hide');

	},
	closeSourceModal: function(component){
		//this.destroyTalentSearch(component); //Neel - S-84237 
		var modalCancel = component.find('modalCancel');
		$A.util.removeClass(modalCancel, 'slds-hide');
		
		var linkapplicantbtn = component.find('linkapplicantbtn');
		$A.util.removeClass(linkapplicantbtn, 'slds-hide'); 

		
		var linkbtn = component.find('linkbtn');
		$A.util.removeClass(linkbtn, 'slds-hide');
        
         var submitbtn = component.find('submitbtn');
		$A.util.removeClass(submitbtn, 'slds-hide');

		var applicantSource = component.find('applicantSource');
		$A.util.addClass(applicantSource, 'slds-hide');
		
		var applicantCancel = component.find('applicantCancel');
		$A.util.addClass(applicantCancel, 'slds-hide');

		var savebtn = component.find('savebtn');
		$A.util.addClass(savebtn, 'slds-hide');

		var appSource = component.find('applicantSource');
		appSource.set("v.errors",null);
		appSource.set("v.value",'');
		
	},
	destroyTalentSearch : function (component) {
		var tsCmp = component.find("talsearch");
		if (Array.isArray(tsCmp)) {
			tsCmp = tsCmp[tsCmp.length - 1];
		}
		
		if(tsCmp.isValid()){
			tsCmp.destroy();
		}
	},
    backToTop: function(component, element, duration) {

        var target = 0;

        var body = element;

        var scrollTo = function(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop;
                var perTick = difference / duration * 10;
       
                setTimeout(function() {
                    element.scrollTop = element.scrollTop + perTick;
               
                    if (element.scrollTop === to) return;
                    scrollTo(element, to, duration - 10);
                }, 10);
            };
      
        scrollTo(body, target, duration);
    }
       
})