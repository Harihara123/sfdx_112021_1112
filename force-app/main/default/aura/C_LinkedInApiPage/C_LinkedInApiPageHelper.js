({
	hideModals: function (component) {
        if (!component.get('v.shouldShow')) {
            component.set('v.showModalInMailHistory', false);
            component.set('v.showModalProspectNotes', false);
            component.set('v.showModalInMailStub', false);
            component.set('v.currentSeatholder', null);
        }
	}
})