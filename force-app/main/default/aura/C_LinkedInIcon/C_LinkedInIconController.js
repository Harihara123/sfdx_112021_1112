({
	doInit : function(component, event, helper) {

	},

	handleShowPopover : function(component, event, helper) {
      
		$A.createComponent("c:C_LinkedInWidget", { "atsCandidateId" : component.get('v.atsCandidateId')}, 
			function(content, status) {
			if(status === "SUCCESS") {
				component.find('overlayLib').showCustomPopover({
					body: content,
					referenceSelector: ".linkedInIcon"
				}).then(function (overlay) {
						var thisDiv = component.find('reference');
					//console.log(component.getElements());
					//thisDiv.classList.add('noClick');
				});
			}			
		});
	},
	handleShowLinkedInTab : function(component, event, helper) {
		var candLinkedInURL = component.get("v.candidateLinkedInURL");
        if(candLinkedInURL != undefined && candLinkedInURL != null ){
            window.open(candLinkedInURL);
        }
	},
	handleShowLinkedInPopover : function(component, event, helper) {
			component.set("v.showLinkedInPopOver",true);
			helper.addRemoveEventListener(component, event, 'linkdinPopOverId', 'linkdinPopOver', 'linkdinPopOverCls');				
	}
})