({
	promiseWidget : function(component) {
		return component.get('v.widget');
	} ,


	addRemoveEventListener : function(component, event, elementId, headerClass, subClass) {		
			$A.util.addClass(event.target, subClass);
			var clickedFn;
			var myPopover = component.find(elementId);
			clickedFn = $A.getCallback(function(event){
				var popoverEle = myPopover.getElement();			
				$A.util.removeClass(popoverEle, 'slds-hide');				
				if(popoverEle && event.target) {
					if (!event.target.className.includes(headerClass) && !popoverEle.contains(event.target)) {
						//$A.util.addClass(popoverEle, 'slds-hide');
						//$A.util.addClass(event.target, subClass);
						window.removeEventListener('click',clickedFn);
					}
				} 
				else {
					window.removeEventListener('click',clickedFn);
					$A.util.addClass(event.target, subClass);
				} 
			});

			window.addEventListener('click',clickedFn); 
	},
										

	togglePopover: function(component){
        var togglePopover = component.get("v.showPopover");
        component.set("v.showPopover", !togglePopover);
    },

    /* callLinkedinToUpdateStatus : function(component, event, helper) {

      var talentRecordId = component.get('v.atsCandidateId');

       var action = component.get("c.updateTalentLinkedinStatus");
       action.setParams({ contactId : talentRecordId });

       action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // TODO take action
            }
            else if (state === "INCOMPLETE") {
               // TODO take action
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +  errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    } */
})