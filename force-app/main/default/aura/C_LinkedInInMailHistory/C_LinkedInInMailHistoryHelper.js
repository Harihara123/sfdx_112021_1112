({
    getPageData: function (component, event, helper) {
        var url = event.getParam("calloutURL");

        helper.makeInMailCallout(component, helper, url);
    },

    makeInMailCallout: function (component, helper, url) {
        component.set("v.loading", true);

        var action = component.get('c.retrieveWebServiceData');
        action.setParams({ requestUrl : url, seatHolderId : component.get("v.currentSeatholder") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var jsonString = response.getReturnValue();

                jsonString = jsonString.replace(/com\.linkedin\.messaging\.events\.MailContent/g, 'mailcontent');

                var parsed = JSON.parse(jsonString);

                var pagingInfo = parsed['paging'];

                pagingInfo['iterationCount'] = (pagingInfo.start + pagingInfo.count) > pagingInfo.total ? pagingInfo.total : pagingInfo.start + pagingInfo.count;

                component.set('v.inMailData', parsed['elements']);
                component.set('v.pagingInfo', pagingInfo);
                component.set('v.loading', false);
            } else {
                var errors = response.getError();
                var message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                component.set('v.modalError', message);
                component.set('v.loading', false);
            }
        });

        $A.enqueueAction(action);

    },
})