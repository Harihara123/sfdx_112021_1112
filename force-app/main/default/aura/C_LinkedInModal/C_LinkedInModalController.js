({
    doInit : function(component, event, helper) {
        component.set('v.shouldShow', true);
    },
    closeModal : function(component, event, helper) {
        component.set('v.shouldShow', false);
    }
})