({
	onInit: function (component, event, helper) {
        helper.parseURLs(component, event, helper);
	},

    parseURLs: function (component, event, helper) {
        helper.parseURLs(component, event, helper);
    },

    pagePrev: function (component, event, helper) {
        var prevURL = component.get("v.prevURL");
        helper.pageNextPrev(component, prevURL);
    },

    pageNext: function (component, event, helper) {
        var nextURL = component.get("v.nextURL");
        helper.pageNextPrev(component, nextURL);
    }
})