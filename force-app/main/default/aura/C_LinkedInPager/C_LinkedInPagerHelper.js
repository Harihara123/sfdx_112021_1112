({
	parseURLs: function (component, event, helper) {
        var pagingInfo = component.get("v.pagingInfo");
        component.set("v.nextURL", "");
        component.set("v.prevURL", "");

        if(pagingInfo) {
            var links = pagingInfo['links'];

            links.forEach((element) => {
                switch(element['rel']) {
                    case 'next':
                        component.set('v.nextURL', element['href'].slice(element['href'].indexOf('?') + 1));
                        break;
                    case 'prev':
                        component.set('v.prevURL', element['href'].slice(element['href'].indexOf('?') + 1));
                        break;
                    default:
                        break;
                }
            });
        }
	},

    pageNextPrev: function (component, calloutURL) {
        var pageEvent = component.getEvent('pagingEvent');
        pageEvent.setParams({calloutURL : calloutURL});
        pageEvent.fire();
    }
})