({
	closeSelf : function (component, event, helper) {
		var thisDiv = document.getElementsByClassName('linkedInIcon')[0];
		thisDiv.classList.remove('noClick');
	    component.find("overlayLib").notifyClose();
	},

	toggleSpinner : function(component, event) {
		var spinner = component.find("spinner");
		$A.util.toggleClass(spinner, "slds-hide");
	}			
})