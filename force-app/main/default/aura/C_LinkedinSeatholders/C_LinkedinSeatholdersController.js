({
	doInit: function (component, event, helper) {
        helper.getSeatholderData(component, helper);
    },

    getSeatholderData: function (component, event, helper) {
        helper.getPageData(component, event, helper);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'inMailHistory':
                component.set('v.showModalInMailHistory', true);
                component.set('v.currentSeatholder', row.id);
                break;
            case 'prospectNotes':
                component.set('v.showModalProspectNotes', true);
                component.set('v.currentSeatholder', row.id);
                break;
            case 'stubProfiles':
                component.set('v.showModalInMailStub', true);
                component.set('v.currentSeatholder', row.id);
                break; 
        }
    }
})