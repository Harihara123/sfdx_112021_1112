({
	getSeatholderData: function (component, helper) {
        var actions = [
            { label: 'InMail History', name: 'inMailHistory' },
            { label: 'Prospect Notes', name: 'prospectNotes'},
            { label: 'Stub Profiles', name: 'stubProfiles'}
        ];

        component.set('v.columns', [
            {label: 'First Name', fieldName: 'firstName', type: 'text'},
            {label: 'Last Name', fieldName: 'lastName', type: 'text'},
            {label: 'Active', fieldName: 'active', type: 'boolean'},
            {label: 'Email', fieldName: 'email', type: 'email'},
            {label: 'Person', fieldName: 'member', type: 'text'},
            {label: 'Seat Id', fieldName: 'id', type: 'number'},
            {label: 'Contract Id', fieldName: 'contractId', type: 'text'},
            {type: 'action', typeAttributes: { rowActions: actions }}
        ]);

        helper.makeSeatholderCallout(component, helper, '');
    },

    getPageData: function (component, event, helper) {
        var url = event.getParam("calloutURL");
        helper.makeSeatholderCallout(component, helper, url);
    },

    makeSeatholderCallout: function (component, helper, url) {
        component.set("v.loading", true);
        var action = component.get('c.retrieveWebServiceData');
        action.setParams({ requestUrl : url });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var jsonString = response.getReturnValue();
                var parsed = JSON.parse(jsonString);

                var pagingInfo = parsed['paging'];
                pagingInfo['iterationCount'] = (pagingInfo.start + pagingInfo.count) > pagingInfo.total ? pagingInfo.total : pagingInfo.start + pagingInfo.count;

                component.set('v.seatholderData', parsed['elements']);
                component.set('v.pagingInfo', pagingInfo);
                component.set('v.loading', false);
            } else {
                var errors = response.getError();
                var message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                component.set('v.listError', message);
                component.set('v.loading', false);
            }
        });

        $A.enqueueAction(action);
    }

})