({
    
    /*
     * Executes the search server-side action when the c:InputLookupEvt is thrown
     */
    handleInputLookupEvt: function(component, event, helper){
        if (component.get('v.soql') === true) {
            helper.searchSoqlAction(component, event.getParam('searchString'), helper);
        } else {
            helper.searchAction(component, event.getParam('searchString'), helper); 
        }
    },
    
    /*
    	Loads the typeahead component after JS libraries are loaded
    */
    initTypeahead : function(component, event, helper){
        //first load the current value of the lookup field and then
        //creates the typeahead component
        helper.loadFirstValue(component);
    },

    /* Clear the state of the lookup component */
    clearLookup : function(component, event, helper) {
        helper.clearLookup(component, event.getParam("globalId"));
    },

    valueChange : function (component, event, helper) {
        helper.valueChange(component);
    }
})