({
    //typeahead already initialized
    typeaheadInitStatus : {},
    //"old value" to trigger reload on "v.value" change
    typeaheadOldValue : {},
    //suggestione function returned after a successful match
    cb: null,
    searchActionArrays: {},
    /*
        Creates the typeahead component using RequireJS, jQuery, Bootstrap and Bootstrap Typeahead
    */
    createTypeaheadComponent: function(component){
        var self = this;
        var globalId = component.getGlobalId();
        //loading libraries sequentially
        var inputElement = jQuery('[id="'+globalId+'_typeahead"]');
        //init the input element
        inputElement.val(component.get("v.nameValue"));
        
        var timer;

        //handles the change function
        inputElement.keyup(function(){

                if (timer) {
                    clearTimeout(timer);
                }
                timer = setTimeout(function(){
                    if(inputElement.val() !== component.get('v.nameValue')){
                        component.set('v.nameValue',inputElement.val());
                        component.set('v.value', null);
                        //self.typeaheadOldValue[component.getGlobalId()] = null;
                    }
                }
            , 500);
        });

        //inits the typeahead
        inputElement.typeahead({
            hint: false,
            highlight: true,
            minLength: parseInt(component.get("v.minChars")),
        },
        {
            name: 'objects',
            displayKey: 'value',
            source: function(q,cb){

                self.cb = cb;
                q = (q || '').replace(/[\-\[\]\/\{\}\(\)\*\+\?\\\^\$\|]/g, "\\$&");
                var compEvent = component.getEvent("inputLookupEvent");
                compEvent.setParams({"searchString" : q });
                compEvent.fire();
            },
        })//.focus(function(evnt) { self.focusFunction(component);})
        //selects the element
        .bind('typeahead:selected', 
            function(evnt, suggestion){
                component.set('v.value', suggestion.id);
                component.set('v.nameValue', suggestion.value);
                var selectEvent = component.getEvent("valueSelectedEvent");
                selectEvent.setParams({"id" : suggestion.id, 
                                        "value" : suggestion.value,
                                        "useValue" : suggestion.useValue});
                selectEvent.fire();
            });

    },

    /*focusFunction : function(component) {
        // console.log("focused");
        if (component.get('v.soql') === true) {
            var inputElement = jQuery('[id="'+component.getGlobalId()+'_typeahead"]');
            this.searchSoqlAction(component, "");
            var e = jQuery.Event("keyup");

            // e.which is used to set the keycode
            e.which = 40; // it is down
            inputElement.trigger(e);
        }
    },*/
    
    searchSoqlAction : function(component, filterValue) {
        var self = this;
        var params = null;
        var action = '';
        var useValue = '';

        if (component.get("v.recordType") === "CountryList") {
            useValue = "country";
            action = "getCountryList"; 
            params = {"filterValue": filterValue};
        } else if (component.get("v.recordType") === "StateList") {
            useValue = "state";
            var parentKey = component.get("v.parentKey");
            action = "getStateList"; 
            params = {"parentKey":parentKey, "filterValue": filterValue};
        }

        this.callServer(component, 'Lookup', '', action, function(response) {
            var lookup = [];

            var lookUpMap = response;
            for ( var key in lookUpMap ) {
                lookup.push({value:lookUpMap[key], id:key, useValue:useValue});
            }
            self.cb(lookup);
        }, params, false);   

    },
    /*
     * Searches objects (server call)
     */
    searchAction : function(component, q){
        if(!component.isValid()) return;

        // Special check for single character search term to prevent SOSL error for the same.
        if (q.replace(' ','').length < 3) {
            return;
        }
        
        var self = this;
        var params = {
                        'type' : component.get('v.type'),
                        'searchString' : q,
                        'addlReturnFields' : component.get('v.addlReturnFields'),
                        'recTypeName' : component.get('v.recordType'),
                        'nameFieldOverride' : component.get('v.nameFieldOverride')
                    };
        var action = 'searchSObject';

        this.callServer(component,'Lookup','',action,function(response){
            var result = response;
            var matches, substrRegex;
            // an array that will be populated with substring matches
            var matches = [];
            
            // regex used to determine if a string contains the substring `q`
            var substrRegex = new RegExp(q, 'i');
            var strs = JSON.parse(result);
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            jQuery.each(strs, function(i, str) {
                if (substrRegex.test(str.value)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    var addl = (str.addlFields.length > 0) ? ", " + str.addlFields.join(", ") : "";
                    matches.push({ value: str.value + addl, id: str.id, useValue: str.value});
                }
            });
            if(!strs || !strs.length){
                component.set('v.value', null);
            }
            self.cb(matches);
        },params,false);   
    },
    
    
    /*
     * Method used on initialization to get the "name" value of the lookup
     */
    loadFirstValue : function(component) {
        //this is necessary to avoid multiple initializations (same event fired again and again)
        if(this.typeaheadInitStatus[component.getGlobalId()]){ 
            return;
        }
        
        this.typeaheadInitStatus[component.getGlobalId()] = true;
        this.searchActionArrays[component.getGlobalId()] = [];
        this.loadValue(component);
           
    },
    
    /*
     * Method used to load the initial value of the typeahead 
     * (used both on initialization and when the "v.value" is changed)
     */
    loadValue : function(component, skipTypeaheadLoading){
        this.typeaheadOldValue[component.getGlobalId()] = component.get('v.value');

        var self = this;
        var params = {
                        'type' : component.get('v.type'),
                        'value' : component.get('v.value'),
                    };
        var action = 'getCurrentValue';

        this.callServer(component,'Lookup','',action,function(response){
            var result = response;
            var globalId = component.getGlobalId();
            component.set('v.isLoading',false);
            component.set('v.nameValue',result || '');
            if(result)jQuery('[id="'+globalId+'_typeahead"]').val(result || '');
            if(!skipTypeaheadLoading) self.createTypeaheadComponent(component);
        },params,false);          
    },

    clearLookup : function(component, globalId) {
        if (component.getGlobalId() === globalId) {
            component.set("v.value", "");
            component.set("v.nameValue", "");
            component.set("v.isError",false);
            component.set("v.errors", null);
            jQuery('[id="'+globalId+'_typeahead"]').val("");
            $A.util.removeClass(component,"slds-has-error show-error-message");
        }
    },

    valueChange : function (component) {
        var val = component.get("v.nameValue");
        if (typeof val !== "undefined" && val === "") {
            var clearEvent = component.getEvent("valueClearedEvent");
            clearEvent.fire();
        }
    }
})