({
	initHandler : function(component, event, helper) {
		var fieldId= component.get("v.fieldId");	
		if((component.get("v.fromG2Page")) &&(fieldId!="")&& (fieldId == "G2Country"|| fieldId == "G2State" || fieldId == "G2jobTitle")){	
			component.set("v.textboxName",fieldId);	
		}	
		else{	
			component.set("v.textboxName", Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1));			
		}	

        component.set("v.currentTextVal", component.get("v.comboValue"));		

		if (component.get("v.initiateSearchOnRender") === true) {
			helper.search(component);
        }
	},

	focusHandler : function(component, event, helper) {

        if (component.get("v.mouseSelected")) {
            component.set("v.mouseSelected", false);
            return;
        }    

        var input = helper.getInputTextValue(component);
		// If use cache is true, ignore input and make server call. 
		// Otherwise validate input length before call.
        component.set("v.comboBoxContainerStyle", "input-focus");

		if (component.get("v.useCache") === true || (input.length >= component.get("v.minSearchChars"))) {
			helper.search(component);
		}
		if (component.get("v.useCache") === false) {
			// Start request queue processor
			helper.startRequestQProcessor(component);
			if (component.get("v.type") === "apexMethodResponse") {
				helper.search(component);
			}
		}
		
	},

	/**
	 * Handler on keydown only for the TAB key since the other key events don't catch this.
	 */
	tabHandler : function(component, event, helper) {        
		var key = 'which' in event ? event.which : event.keyCode;

		// On TAB, select the item in focus and hide combobox     
		if (key == 9) {            
		// Removing the selection of list items on Tab key, limiting the selection only to "Enter" key
			helper.resetFocusTracker(component);
			if(component.get("v.allowFreeType") && component.get("v.inlinePills")) {
				helper.clearValueAndHideDropdown(component);
				} 
			else {
				helper.detectValidTypedInValue(component);
			}
			helper.hideDropdown(component);
        } 
	},

	keystrokeHandler : function(component, event, helper) {
		var key = 'which' in event ? event.which : event.keyCode;
		
		//console.log("kup handler --- " + key + " - "  + component.get("v.data.length") + " - " + component.get("v.lookupList.length")+ " - "  + component.get("v.focusTracker"));
		var optionPrefix = component.get("v.textboxName") + "-listbox-option-unique-id--";
		//  console.log(key);

		event.preventDefault();

		var focusedEl=document.activeElement.id;

		if (key == 40 || key == 38) {
            // On UP and DOWN keys, defocus old selection, update focusTracker, and focus new selection.
            var trackIndex = component.get("v.focusTracker");
            if (trackIndex === undefined) {
            	trackIndex = 0;
            } 
            helper.defocusItem(optionPrefix + trackIndex);

            if (key == 40) {
            	// focusTracker does not increment more than length of list.
            	var optionsLength = component.get("v.lookupList.length");
            	if (trackIndex < optionsLength - 1) {
            		trackIndex++;
            	}
            } else {
            	// focusTracker does not decrement below zero. -1 (default) implies arrow keys not used yet.
            	if (trackIndex > 0) {
            		trackIndex--;
            	}
            }
            helper.focusItem(optionPrefix + trackIndex,focusedEl);
            helper.scrollToKeyboardFocus(component);
            
            component.set("v.focusTracker", trackIndex);
        } else if (key == 13 || key == 32) {
        	// On ENTER, select item in focus and hide combobox
			// Monika -- If no list item is focused, space bar should have its normal behaviour, else it should select the list item and hide the dropdown
        	if(component.get("v.focusTracker") === -1){
               if(key==13) {
					helper.detectValidTypedInValue(component);
					helper.hideDropdown(component); 
					helper.setFocustoElement(component,component.get("v.textboxName"));
				}
            }
			else{
			if (component.get("v.picklistField")) {
	            // Delegate to pull values from picklist metadata.
	            helper.updatePicklistValues(component);
              }
              helper.setValueAndHideDropdown(component); 
			  helper.resetFocusTracker(component); 
            }
            
        } else if (key == 27) {
        	// On ESC, clear selection and hide combobox.
        	helper.clearValueAndHideDropdown(component);
        }else {
        	// If useCache is true, ignore input and make server call. 
			if (component.get("v.useCache") === true) {
        		helper.filterLookupList(component);
				helper.showDropdown(component);
				helper.resetFocusTracker(component);
        	} else {
				// Otherwise validate input length before server call.
        		var input = helper.getInputTextValue(component);
				if (input.length >= component.get("v.minSearchChars")) {
					helper.search(component);
				} else if (input.length == 0 && component.get("v.type") === "apexMethodResponse") {
					helper.search(component);
				}
                //Adding w.r.t S-173635
                else if (input.length == 0) {
					helper.hideDropdown(component);
				} 
                //Adding w.r.t S-173635
        	}
          
        }
		console.log(`keystrokeHandler typed value ===> ${document.getElementById(component.get("v.textboxName")).value}`);
        //console.log("value ------ " + component.get("v.comboValue"));
        //console.log("after kye handler --- " + component.get("v.focusTracker"));
	},

	blurHandler : function(component, event, helper) {
        // Start - added by akshay for capturing entered text on certification modal on talent landing page.
        
        // Put in a quick timeout to see if the typeahead box was clicked. Bit of a hack. (D-15500)
        setTimeout($A.getCallback(function() {
            var listboxClicked = component.get('v.clickHandled');
            if (!listboxClicked) {
                if (component.get("v.allowFreeType") && component.get("v.executeBlur")) {
                    helper.detectValidTypedInValue(component);
                }
            }
            else {
                component.set('v.clickHandled', false);
            }
        }), 100);
        // End - added by akshay for capturing entered text on certification modal on talent landing page.
        // 
        // HACK alert - delay 250 msec for the option onclick to register and set value.

		setTimeout(function() {
			helper.resetFocusTracker(component);
			helper.hideDropdown(component);
            component.set("v.comboBoxContainerStyle", '');
			// Stop request queue processor
			if (component.get("v.useCache") === false) {
				helper.stopRequestQProcessor(component); 
			}
			
		}, 250); 
	},

	mouseoverHandler : function(component, event, helper) {
		// Reset keyboard option focus when the mouse is moved over the options.
		helper.resetFocusTracker(component);
	},

	/**
	 * Handle change of the parent key. Clear the selection and the lookup list.
	 */
	parentKeyChangeHandler : function(component, event, helper) {
		//console.log(event.getParam("oldValue"));
        //console.log(event.getParam("value"));
		if (component.get("v.parentInitialLoad") === false) {
			helper.clearValueAndHideDropdown(component);
			helper.clearLookupList(component);
		}
		if (component.get("v.parentInitialLoad") === true) {
			component.set("v.parentInitialLoad",false);
		}
	},
    
    changeHandler : function(component, event, helper) {
        // Detect if the input text is a valid option
        helper.checkForCleared(component);
    },

    selectLookupValue : function(component, event, helper) {
        component.set('v.clickHandled', true);
        
    	// the recordid on the option has the index of the selection in the lookup list
		component.set("v.contactId","");
        component.set("v.contactName","");
        var dataVal = event.currentTarget.dataset.recordid;
        var list = component.get("v.lookupList");
        helper.selectLookupValue(component, list[dataVal]);
	},

	setFocusOnRemove : function(component, event, helper) {
		document.getElementById(component.get("v.textboxName")).focus();
    },
    
    selectSuggested : function(component, event, helper) {
        var val = event.getParam('pillLabel');
        helper.forceSetValue(component, {
            "key": val, 
            "value": val
        });

        var suggestedSkills = component.get('v.suggestedPills');
        var suggested = [];
        suggestedSkills.forEach(function(item){
            if (val != item)
                suggested.push(item);
        });
        component.set('v.suggestedPills', suggested);
    }
})