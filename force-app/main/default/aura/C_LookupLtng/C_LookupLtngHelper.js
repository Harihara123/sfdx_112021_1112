({
    // requestQ: [],
    //qProcessorId: -1,

	search : function(component) {
        var filterValue = "";

        // If useCache is false, get data set based on the input text box.
        if (component.get("v.useCache") === false) {
        	filterValue = this.getInputTextValue(component);
        }

        if (component.get("v.picklistField")) {
			// Delegate to pull values from picklist metadata.
			this.updatePicklistValues(component);
		} else if (component.get('v.soql') === true) {
        	// Delegate to soql search method.
        	this.searchSoqlAction(component, filterValue);
        } else {
        	// Delegate to sosl search method.
        	this.searchSoslAction(component, filterValue);
        }

	},

	searchSoqlAction : function(component, filterValue) {

        var action, params;
        if (component.get("v.type") === "Country") {
            action = "getCountryList"; 
            params = {"filterValue": filterValue};

        } else if (component.get("v.type") === "State") {
            var parentKey = component.get("v.parentKey");
           
          
            action = "getStateList"; 
            params = {  "parentKey":parentKey, 
                "filterValue": filterValue
            };
			
        } else if (component.get("v.type") === "apexMethodResponse") {
				action = "customLogicResponse";
				params = {
                'type' : 'myAssignedReqs',
                'searchString' : filterValue,
				'addlReturnFields' : component.get('v.addlReturnFields')
            };
		} else {
			
        	action = "querySObject";
        	params = {
                'type' : component.get('v.type'),
                'searchString' : filterValue,
                'addlReturnFields' : component.get('v.addlReturnFields'),
                'recTypeName' : component.get('v.recordType'),
                'nameFieldOverride' : component.get('v.nameFieldOverride'),
                'idFieldOverride' : component.get('v.idFieldOverride'),
                'useCache' : component.get('v.useCache'),
                'whereCondition' : component.get('v.whereCondition'),
                'sortCondition' : component.get('v.sortCondition')
            };
        }
		
		if (action) {
			this.processRequest(component, action, params);
		}
	},

    /*
     * Searches objects (server call)
     */
    searchSoslAction : function(component, filterValue){
        if(!component.isValid()) return;

        // Special check for single character search term to prevent SOSL error for the same.
        if (filterValue.replace(' ','').length < 3) {
            return;
        }
        
        var params = {
                        'type' : component.get('v.type'),
                        'searchString' : filterValue,
                        'addlReturnFields' : component.get('v.addlReturnFields'),
                        'recTypeName' : component.get('v.recordType'),
                        'nameFieldOverride' : component.get('v.nameFieldOverride'),
                        'idFieldOverride' : component.get('v.idFieldOverride'),
                        'sortCondition' : component.get('v.sortCondition')
                    };
        var action = 'searchSObject';

        this.processRequest(component, action, params);
    },

	updatePicklistValues : function(component) {
		var action = "loadPicklist";

		var params = {
			"picklistField": component.get("v.picklistField")
		};

		this.processRequest(component, action, params);
	},

    /*
     * If useCache is true, immediately call server, else add to request Q
     */
    processRequest : function(component, action, params) {
        var requestQ = component.get("v.requestQ");
        if (component.get("v.useCache") || component.get("v.initiateSearchOnRender")) {
            this.sendRequestToServer(component, action, params);
        } else {
            requestQ.push({
                "action" : action,
                "params" : params
            });
        }
        component.set("v.requestQ", requestQ);
    },

    /*
     * Start the request Q processor.
     */
    startRequestQProcessor : function(component) {
        var self = this;
        // Set interval timer (500 msec) to process server requests.
        var qProcessorId = setInterval($A.getCallback(function() {
            var requestQ = component.get("v.requestQ");
            if (requestQ === undefined) {
                // requestQ undefined implies the component is no longer valid. Clear the interval function.
                clearInterval(qProcessorId);
            } else if (requestQ.length > 0) {
                // If there are queued requests, send the last one to server and clear queue.
                var lastReq = requestQ[requestQ.length - 1];
				if (component.get("v.notToProcess") === false) {
					self.sendRequestToServer(component, lastReq.action, lastReq.params);
				}
				if (component.get("v.notToProcess") === true) {
					component.set("v.notToProcess",false);
				}
                // requestQ = [];
                component.set("v.requestQ", []);
            }

        }), 500);
        component.set("v.qProcessorId", qProcessorId);
    },

    /*
     * Stop the request Q processor.
     */
    stopRequestQProcessor : function(component) {
        // clearInterval(this.qProcessorId);
        clearInterval(component.get("v.qProcessorId"));
    },

    /*
     * Send the request to server and set callback to process response
     */
    sendRequestToServer : function(component, action, params) {
        var self = this;
        var bdata = component.find("bdhelper");
        bdata.callServer(component, 'Lookup', '', action, function(response){
            // Reset focus
            self.resetFocusTracker(component);
            // Process response data
            self.processResponse(component, response);
        }, params, false);
    },

    processResponse : function(component, response) {
        // Update the dropdown list
        var lookup = [];

        if (component.get("v.type") === "Country" || component.get("v.type") === "State") {
            lookup = this.translateCountryOrStateResponse(component, response);
        } else {
        	lookup = this.translateGenericResponse(component, response);
        }

        // If useCache false, ignore data attribute and set directly to lookupList.
        if (component.get("v.useCache") === false) {
        	component.set("v.lookupList", lookup);
        } else {
        	// useCache true -> Set to data attribute and filter down to lookupList
            component.set("v.data", lookup);
            this.filterLookupList(component);
        }
        this.showDropdown(component);
		this.resetFocusTracker(component);
    },

    translateCountryOrStateResponse : function(component, response) {
        var lookup = [];
        var lookUpMap = response;

        for ( var key in lookUpMap ) {
            lookup.push({value:lookUpMap[key], key:key});
        }
        return lookup;
    },

    translateGenericResponse : function(component, response) {
        var lookup = [];
        var responseArray = JSON.parse(response);

        for (var i=0; i<responseArray.length; i++) {
            lookup.push({
            	value: responseArray[i].value, 
            	key: responseArray[i].id, 
            	addlValues: responseArray[i].addlFields.join(", ")
            });
           
        }
        
        return lookup;
    },

	filterLookupList : function(component) {
		// Failsafe - this method does not apply when not using cache.
		if (component.get("v.useCache") === false) {
			return;
		}

		var data = component.get("v.data");
		var input = this.getInputTextValue(component);

		var lookupList = data;
		if (input !== "") {
			lookupList = data.filter(function(item) {
				return item.value.toLowerCase().indexOf(input.toLowerCase()) === 0;
			});
		}
		// debugger;
		component.set("v.lookupList", lookupList);
	},

	setValueAndHideDropdown : function(component) {
        
        var trackIndex = component.get("v.focusTracker");
        var optionPrefix = component.get("v.textboxName") + "-listbox-option-unique-id--";

        var val = this.getItemValue(optionPrefix + trackIndex);
        if (component.get("v.allowFreeType") && (val === "")) {// added this if condition for 
            // If free type allowed, set the value from the text box
            this.detectValidTypedInValue(component);
        } else if (val !== "") {
            var list = component.get("v.lookupList");
            this.forceSetValue(component, list[val]);
        }
		
		this.setFocustoElement(component,component.get("v.textboxName"));
		this.defocusItem(optionPrefix + trackIndex);
    	this.hideDropdown(component);
		if(component.get("v.textboxName") == "G2Country" || component.get("v.textboxName") == "G2State"){	
			this.clearInlineErrorMessage(component);	
		}
	},

	clearValueAndHideDropdown : function(component) {
    	this.clearValue(component);
    	this.hideDropdown(component);
	},

	clearValue : function(component) {
    	component.set("v.comboKey", "");
    	component.set("v.comboValue", "");
        // Explicitly clearing the input value since it it was inconsistent when only clearing the bound attribute.
        var el = document.getElementById(component.get("v.textboxName"));
        if (el !== null) {
            el.value = "";
        }
    	this.fireClearedEvent(component);
	},

	clearLookupList : function(component) {
		component.set("v.data", []);
		component.set("v.lookupList", []);
	},

	hideDropdown : function(component) {
		component.set("v.comboBoxStyle", "slds-is-hide");
	},

	showDropdown : function(component) {
		component.set("v.comboBoxStyle", "slds-is-open");
	},

	resetFocusTracker : function(component) {
		// Remove current selection and update focusTracker to -1 (no selection).
		this.defocusItem(component.get("v.textboxName") + "-listbox-option-unique-id--" + component.get("v.focusTracker"));
		component.set("v.focusTracker", -1);
	},

	focusItem : function(elid,focusedEl) {
		var el = document.getElementById(elid);
		var root=document.getElementById(focusedEl);

		if(root!=null){
			root.setAttribute("aria-activedescendant",elid);
		}

		if (el !== null) {
			el.className = el.className + " has-keyboard-focus";
			if(el.hasAttribute("aria-selected")){
				el.setAttribute("aria-selected","true");
			}
		}
	},

	scrollToKeyboardFocus : function(component) {
		var ft = component.get("v.focusTracker");
		var dropdownEl = document.getElementById(component.get("v.textboxName") + "-ul-id");
		var listLength = component.get("v.lookupList.length");

		// Set the dropdown scroll position. (dropdownEl.scrollHeight / listLength) is the pixel height 
		// of each option. This works assuming the height of the dropdown is set to accommodate 5 rows. 
		// Will require adjustment if that changes.
		dropdownEl.scrollTop = (ft - 3) * (dropdownEl.scrollHeight / listLength);
	},

	defocusItem : function(elid) {
		var el = document.getElementById(elid);
		if (el !== null) {
			el.className = el.className.replace(" has-keyboard-focus", "");
			if(el.hasAttribute("aria-selected")){
				el.setAttribute("aria-selected","false");
			}
		}
	},

	getItemValue : function(elid) {
		var el = document.getElementById(elid);
		return el !== null ? el.getAttribute("data-recordid") : "";
	},

	forceSetValue : function(component, val) {
        //Added by akshay for D-02224  09/14/17 start
        if( typeof val.addlValues !== "undefined" && val.addlValues !== " "){
            var addlval = val.addlValues.split(",");
            var addlnames = component.get("v.addlReturnFields");
            var addlMap = {};
            //console.log(component.get("v.addlReturnFields") + val.addlValues +  addlval.length + addlval);
            for(var i = 0; i<= addlval.length; i++){
                addlMap[addlnames[i]] = addlval[i];
            }
            //console.log(JSON.stringify(addlMap));
            component.set('v.eventaddlValues', addlMap);
        }
        //Added by akshay for D-02224  09/14/17 end
        component.set('v.comboKey', val.key);
		component.set('v.comboValue', val.value);
        
		// Explicitly setting the input value since it it was inconsistent when only setting the bound attribute.
		document.getElementById(component.get("v.textboxName")).value = val.value;
        // console.log(`forceSetValue typed value ===> ${document.getElementById(component.get("v.textboxName")).value}`);
		this.fireSelectedEvent(component);
	},

	/**
	 * getInputTextValue - returns the current value of the input text box
	 * @return value of input text box if available. "" if not.
	 */
	getInputTextValue : function(component) {
      var inputEl = document.getElementById(component.get("v.textboxName"));
      //farid 11/17/2017 handling the trailing space 
      return ((inputEl && inputEl.value && inputEl.value.length > 0) ? inputEl.value.trim() : ""); 
	},

	selectLookupValue : function(component, dataValue) {
        var elementId = document.getElementById(component.get("v.textboxName")).getAttribute("id");
		this.forceSetValue(component, dataValue);
        this.hideDropdown(component);
		if (component.get("v.insideLookupPillBar") === true && dataValue) {
			document.getElementById(component.get("v.textboxName")).focus();
			component.set("v.notToProcess", true);
		}
	},

	/**
	 * Compare input value to the lookup list entries to confirm that the typed in value is legitimate.
	 */
	detectValidTypedInValue : function(component) {
        var val = this.getInputTextValue(component);
    	var list = component.get("v.lookupList");

    	var found = false;
        // If this lookup allow free typed values, don't bother finding
        if (!component.get("v.allowFreeType")) {
            for (var i=0; i<list.length; i++) {
                if (val.toLowerCase() === list[i].value.toLowerCase()) {
                    found = list[i];
                }
            }
        }

    	if (component.get("v.allowFreeType")) {
            // If free type allowed, set the value from the text box
            this.forceSetValue(component, {
                "key": val, 
                "value": val
            });
        } else if (found) {
    		this.forceSetValue(component, found);
    	} else {
    		this.clearValue(component);
    	}

	},

    checkForCleared : function(component) {
        var val = this.getInputTextValue(component);
        if (val === "") {
            this.clearValue(component);
        }
    },

	fireSelectedEvent : function(component) {
        if(component.get("v.comboKey") !== null && component.get("v.comboKey") !== '' &&
           component.get("v.comboValue") !== null && component.get("v.comboValue") !== ''){
            var selectedEvt = component.getEvent("valueSelectedEvent");
            selectedEvt.setParams({
                "key" : component.get("v.comboKey"),
                "value" : component.get("v.comboValue"),
                "displayValue": component.get("v.displayPillValue"),
                "addlValues": component.get("v.eventaddlValues")
            });
            selectedEvt.fire(); 
        }
        
	},

	fireClearedEvent : function(component) {
		var clearedEvt = component.getEvent("valueClearedEvent");
		clearedEvt.fire();
	},

	setFocustoElement:function(component,elementId){
		var inputBox = document.getElementById(component.get("v.textboxName"));	
		if(inputBox !=null)	{	
			if(!document.activeElement.classList.contains("search-req-field")) {	
				window.setTimeout(	
					$A.getCallback(function() {	
				    // wait for element to render then focus	
					inputBox.focus();	
				    //event.preventDefault();	
                    }), 250	
                );	
            }
        }
	},

    clearInlineErrorMessage : function(component,fieldId,errorMessageId){	
        var cmpEventFromClose = $A.get('e.c:E_LocationFieldsErrors');	
        cmpEventFromClose.setParams({"fieldId":component.get("v.textboxName")});	
        cmpEventFromClose.fire();
    }
})