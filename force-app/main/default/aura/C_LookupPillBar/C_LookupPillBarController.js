({  
    doInit: function(component,event,helper){
        helper.updateCS(component);
		helper.redrawPills(component);
		if(!$A.util.isEmpty(component.get('v.talentTitle')) || !$A.util.isUndefined(component.get('v.talentTitle'))) {
			helper.getSuggestedSkills(component,component.get('v.talentTitle'));
		}
    },

	valueSelected : function(component, event, helper) {
        var isValid = true;
        if(helper.dupecheckCS(component,event.getParam("value")) === true ){
                
            if(component.get("v.type") === "Tag_Definition__c"){
                isValid = helper.candidateListValidations(component,event);
            }
            if (isValid === true) {
                    helper.addValueToState(component);
                    helper.clearLookup(component);	
                    helper.updateData(component);
                    helper.redrawPills(component);	   
            }
        }
    },

    titleChanged : function(component, event, helper) {
		console.log('alex-- title Changed');
        if (component.get('v.uniqueId') == 'skillsId') {
			if(!$A.util.isEmpty(component.get('v.talentTitle')) || !$A.util.isUndefined(component.get('v.talentTitle'))) {
				helper.getSuggestedSkills(component,component.get('v.talentTitle'));
			}
        }
    },	

	updateData : function(component, event, helper) {
		helper.updateData(component);
		helper.redrawPills(component);
    },
    
    removefromState : function(component, event, helper) {
        var pillId = event.getParam("pillId");
        
		helper.removeValueFromState(component,pillId);
		helper.updateData(component);
		helper.redrawPills(component);
        helper.refreshSkillsFromInitial(component);
		var nestedcomponent= component.find("nestedCompId");
		nestedcomponent.executeSetFocusOnRemove();
    },
    
    updateCurrentState : function(component, event, helper) {
        //if(component.get("v.runOnce") === true){
            // Set runOnce to false before updating currentState to prevent the recursion.
            // component.set("v.runOnce",false); 
            helper.updateCS(component); 
            helper.redrawPills(component);
        // }
    }
})