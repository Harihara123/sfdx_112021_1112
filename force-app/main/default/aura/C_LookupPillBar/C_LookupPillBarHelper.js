({
    getPillId : function(component, option) {
        var pillIdMap = component.get("v.pillIdMap");
        if (pillIdMap === null) {
            pillIdMap = {};
        }

        var randomId = pillIdMap[option];
        if (randomId === undefined) {
            randomId = Math.floor(Math.random() * 10000000000);
            pillIdMap[option] = randomId;
        }
        
        component.set("v.pillIdMap", pillIdMap);
        return randomId;
    },
    
   clearLookup : function (component) {
        // Fire lookup clear event
        component.set("v.lookupValue", "");
    },

    redrawPills : function(component) {
        var recType = component.get("v.recordType");
        var currentstatearray = component.get("v.currentState");
        var pillsArray = [];
        var pills = [];
        
        if (!component.get('v.inlinePills')) {
        for(var i=0;i< currentstatearray.length;i++){
            if(typeof currentstatearray[i].showPill === "undefined" ){
                // added the showpill parameter for AddCandidatetoList component to draw pills only for select elements from the current state - S-39294 - akshay -9/19/17 
                
                var cmpParams = {
                    "pillLabel" : currentstatearray[i].value,
                    "pillId" : this.getPillId(component, currentstatearray[i].value) ,
                    "showClose": component.get("v.showClose")
                };        
        
        $A.createComponent("c:C_SearchFacetPill", 
            cmpParams, 
            function(newComponent, status, errorMessage) {
                if (status === "SUCCESS") {
                    // SUCCESS  - insert component in markup
                    
                    pillsArray.push(newComponent);
                    component.set("v.selectedPills", pillsArray);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.");
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
        });
            }else{
                //if show pill param is defined, this elese block can used as subsequent actions.
            }
            
            
        } } else {
            let currentPillLength = component.get('v.oldArrLength');
            let css= null;
			var n2p = component.get('v.fromN2p');
			currentstatearray.map((item, index) => {
				css = item.css;
				pills.push({
					pillLabel: item.value,
					pillId: this.getPillId(component, item.value),
					showClose: component.get("v.showClose"),
                    css
				})
			})
		}
		component.set('v.pills', pills);
        if(pillsArray.length === 0){
            component.set("v.selectedPills", pillsArray); 
        }
        
    },

    addValueToState : function(component) {
        var currentState = component.get("v.currentState");
        // Initialize currentState if not already done.        
        if (currentState === undefined || currentState == null) {
            currentState = [];
        }
        if(component.get("v.modifiedPillStructure")){
            currentState.push({
            "key": component.get("v.lookupKey"),    
            "value" : component.get("v.lookupValue"),
            "pillId" : this.getPillId(component, component.get("v.lookupValue"))
        }); 
        }else{
          currentState.push({
            "key": component.get("v.lookupKey"),  
            "value" : component.get("v.lookupValue"),
            "pillId" : this.getPillId(component, component.get("v.lookupValue"))
        });  
        }
        
        component.set("v.currentState", currentState);
        
    },
    
    dupecheckCS : function(component,pillLabel) {
        var isValid = true;
        var cs = component.get("v.data");
        	if(cs) {
	            for(var i=0;i<=cs.length;i++){
	
	                if(typeof cs[i] !== "undefined" ){
	                    var val = component.get("v.dataHasKey") ? cs[i].value : cs[i];//if dataHasKey is true , the structure of v.data will hav key& value attributes, else they wont. 
	                    if(val.toLowerCase() === pillLabel.toLowerCase()){
	                        var toastEvent = $A.get("e.force:showToast");
	                        toastEvent.setParams({
								//S-134551--changed text to labels for french translation
	                            //message: val +' already exists !!',
								message: val+' '+$A.get("$Label.c.ATS_ALREADY_EXISTS"),
	                            type:'error',
	                            //title:'Duplicate Value'
								title:$A.get("$Label.c.ATS_Duplicate_Value")
	                        });
	                        toastEvent.fire(); 
	                        isValid = false;
	                        break;
	                    }
	                  
	                }
	                
	            }
        	}
            
        
       return isValid; 
    },
    
    candidateListValidations : function(component,event) {
        var isValid = true;
        //console.log(JSON.stringify(event.getParam("addlValues")["Candidate_Count__c"]) + ' '+ JSON.stringify(event.getParam("addlValues")["Legacy_List__c"]) + ' '+ component.get("v.type"));
        var cs = component.get("v.selectedPills").length;
		var tagNames = component.get("v.tagNamesCount");
		if(!(tagNames.indexOf(event.getParam("value")) > -1) && (tagNames.length + cs >= 75)) {
           var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            message: 'You have reached your maximum limit for lists.',
                            type:'error',
                            title:'Error' 
                        });
                        toastEvent.fire(); 
                        isValid = false;
		}

        if((event.getParam("addlValues")["Candidate_Count__c"] >= 250) && !(event.getParam("addlValues")["Legacy_List__c"]) ){
           var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            message: event.getParam("value") +' has '+event.getParam("addlValues")["Candidate_Count__c"] +' contacts!!',
                            type:'error',
                            title:'Error'
                        });
                        toastEvent.fire(); 
                        isValid = false;
                       
        }
        
        
       return isValid; 
    },
    
    updateCS : function(component) {
        var data = component.get("v.data");
        var n2p = component.get("v.fromN2p");
        var parsed = component.get("v.parsedSkills");
        var exis = component.get("v.ExistingSkills");
		//console.log("Parsed: " + component.get("v.parsedSkills"));
		//console.log("exis: " + component.get("v.ExistingSkills"));
        if(data === undefined){
           return; 
        }
                
        // Initialize currentState if not already done.
         var currentState = [];
			for(var i=0;i< data.length;i++){
				if(component.get("v.dataHasKey")){
				   currentState.push({
					"key":data[i].key,
					"value" : data[i].value,
					"pillId" : this.getPillId(component, data[i].value),
					   "showPill" : data[i].showPill
				});  
				}else{
					var css = null;
					if(n2p) {
						for(var j=0; j < parsed.length; j++){
							if(data[i] === parsed[j])
								css = 'N2Pcss';
						}
					}
				  currentState.push({
					"key":data[i],
					"value" : data[i],
					"pillId" : this.getPillId(component, data[i]),
					"showPill" : data[i].showPill,
					"css" : css
				});  
				}
             
			}
		       
        component.set("v.currentState", currentState);
    },

	updateData : function(component) {
        var currentState = component.get("v.currentState");
		console.log(currentState)
        var n2p = component.get("v.fromN2p");
        // Initialize currentState if not already done.        
       
        var data = [];

        for(var i=0;i< currentState.length;i++){
            if(component.get("v.dataHasKey")){
                data.push({"key":currentState[i].key,
                           "value":currentState[i].value});
            }else{
              data.push(currentState[i].value);
            } 
        }
        component.set("v.data", data);
    },

    removePill : function(component, pillId) {
        var foundAndRemoved = false;

        var pillsArray = component.get("v.selectedPills");
        var newPills = [];

        if (pillsArray !== undefined && pillsArray !== null) {
            for (var i=0; i<pillsArray.length; i++) {
                if (pillsArray[i].get("v.pillId") !== pillId) {
                    newPills.push(pillsArray[i]);
                } else {
                    foundAndRemoved = true;
                }
            }
        }
        
        component.set("v.selectedPills", newPills);
        return foundAndRemoved;
    },

    removeValueFromState : function(component, pillId) {
        var foundAndRemoved = false;

        var state = component.get("v.currentState");
        var newState = [];

        if (state !== undefined && state !== null) {
            for (var i=0; i<state.length; i++) {
                if (state[i].pillId !== pillId) {
                    newState.push(state[i]);
                } else {
                    foundAndRemoved = true;
                }
            }
        }
        
        if (foundAndRemoved) {
            component.set("v.currentState", newState);
        }
        return foundAndRemoved;
    },

    clearAllSelectedFacets : function(component) {
        // Clear all pills
        component.set("v.selectedPills", []);

        // Clear state of selected values
        component.set("v.currentState", []);
        // Clear state also sets the visibility to false
    },

    getSuggestedSkills : function(component, title) {
        // Clear Suggested if title is blank
        if (title == '') {
            component.set('v.suggestedSkills', []);
            component.set('v.initialSuggestedSkills', []);
        }

        // Leave domain blank to user running user ID
        var params = {title:title, terms:title, max:'20', domain:''};
        var dataHelper = component.find('bdhelper');
        dataHelper.callServer(component, 'ATS', '', 'getSuggestedSkillsByTitleOpco', function(response){

            // Get currently selected pills
            var selectedPills = component.get('v.pills');

            // Put those pills in an array
            var selectedArray = [];
            if (selectedPills != undefined) {
                selectedPills.forEach(function(item){
                    selectedArray.push(item.pillLabel.toLowerCase());
                });
            }

            var skillsList = [];
            var initialSkillsList = [];
            var responseJson = JSON.parse(response);

            if (responseJson[title] != undefined) {
                responseJson[title].forEach(function(item){
                    // Add all skills to list
                    initialSkillsList.push(item.term);

                    // Make sure the suggested skill isn't already selected
                    if (!selectedArray.includes(item.term.toLowerCase()))
                        skillsList.push(item.term);
                });
            }
            component.set('v.suggestedSkills', skillsList);
            component.set('v.initialSuggestedSkills', initialSkillsList);
        }, params, false); 
    },

    refreshSkillsFromInitial : function(component) {
        // Currently selected pills
        var selectedPills = component.get('v.pills');
        
        var skillsList = [];
        var initialSkills = component.get('v.initialSuggestedSkills');
        
        // Put those pills in an array
        var selectedArray = [];
        if (selectedPills != undefined) {
            selectedPills.forEach(function(item){
                selectedArray.push(item.pillLabel.toLowerCase());
            });
        }

        if (initialSkills != undefined) {
            initialSkills.forEach(function(item){
                if (item != undefined) {
                    // Make sure the suggested skill isn't already selected
                    if (!selectedArray.includes(item.toLowerCase()))
                        skillsList.push(item);
                }
            });
        }

        component.set('v.suggestedSkills', skillsList);
    }
})