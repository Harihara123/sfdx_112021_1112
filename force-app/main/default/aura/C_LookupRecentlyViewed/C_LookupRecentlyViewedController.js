({
	initHandler : function(component, event, helper) {
		component.set("v.textboxName", Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1));
        component.set("v.currentTextVal", component.get("v.comboValue"));
	},
    inputHandler: function( component,event,helper){
       
        var input = helper.getInputTextValue(component);
		
       		if(input.length == 0 ){
           		helper.getRecentlyViewed(component,input);
        	}else{
          		if((input.length >= component.get("v.minSearchChars"))){
           			helper.getSearchRecord(component,input);
       			}  
            
        	}
           
    },
	focusHandler : function(component, event, helper) {
         var input = helper.getInputTextValue(component);
               
        if(input.length == 0 ){
           helper.getRecentlyViewed(component,input);
        }else{
          if((input.length >= component.get("v.minSearchChars"))){
           		helper.getSearchRecord(component,input);
       		}  
            
        }
                          
	},

	/**
	 * Handler on keydown only for the TAB key since the other key events don't catch this.
	 */
	tabHandler : function(component, event, helper) {
       
        
		var key = 'which' in event ? event.which : event.keyCode;
		//window.event.preventDefault();
		// console.log("key up handler --- " + key + " - "  + component.get("v.data.length") + " - " + component.get("v.lookupList.length")+ " - "  + component.get("v.focusTracker"));

		// On TAB, select the item in focus and hide combobox
		if (key == 9) {
            helper.setValueAndHideDropdown(component);
        } 
        // console.log("after tab handler --- " + component.get("v.focusTracker"));
	},

	keystrokeHandler : function(component, event, helper) {
        
		var key = 'which' in event ? event.which : event.keyCode;
		//window.event.preventDefault();
       // console.log(window.event);
		//console.log("kup handler --- " + key + " - "  + component.get("v.data.length") + " - " + component.get("v.lookupList.length")+ " - "  + component.get("v.focusTracker"));
		var optionPrefix = component.get("v.textboxName") + "-listbox-option-unique-id--";

		if (key == 40 || key == 38) {
            // On UP and DOWN keys, defocus old selection, update focusTracker, and focus new selection.
            var trackIndex = component.get("v.focusTracker");
            if (trackIndex === undefined) {
            	trackIndex = 0;
            } 
            helper.defocusItem(optionPrefix + trackIndex);

            event.preventDefault();

		var focusedEl=document.activeElement.id;

            if (key == 40) {
            	// focusTracker does not increment more than length of list.
            	var optionsLength = component.get("v.lookupList.length");
            	if (trackIndex < optionsLength - 1) {
            		trackIndex++;
            	}
            } else {
            	// focusTracker does not decrement below zero. -1 (default) implies arrow keys not used yet.
            	if (trackIndex > 0) {
            		trackIndex--;
            	}
            }
            helper.focusItem(optionPrefix + trackIndex,focusedEl);
            helper.scrollToKeyboardFocus(component);
            
            component.set("v.focusTracker", trackIndex);
        } else if (key == 13) {
        	// On ENTER, select item in focus and hide combobox
        	if(component.get("v.focusTracker") === -1){
               //helper.detectValidTypedInValue(component); 
            }else{
              helper.setValueAndHideDropdown(component);  
            }
            
        } else if (key == 27) {
        	// On ESC, clear selection and hide combobox.
        	helper.clearValueAndHideDropdown(component);
        } else {
        	// If useCache is true, ignore input and make server call. 
			if (component.get("v.useCache") === true) {
        		helper.filterLookupList(component);
				helper.showDropdown(component);
				helper.resetFocusTracker(component);
        	} else {
				// Otherwise validate input length before server call.
        		var input = helper.getInputTextValue(component);
				if (input.length >= component.get("v.minSearchChars")) {
					helper.search(component);
				}	
        	}
            component.set("v.currentTextVal", helper.getInputTextValue(component));
        }
        //console.log("value ------ " + component.get("v.comboValue"));
        //console.log("after kye handler --- " + component.get("v.focusTracker"));
	},

	blurHandler : function(component, event, helper) {
        // Start - added by akshay for capturing entered text on certification modal on talent landing page.
        if (component.get("v.allowFreeType") && component.get("v.executeBlur")) {
            //helper.detectValidTypedInValue(component);
        }
        // End - added by akshay for capturing entered text on certification modal on talent landing page.
        // 
        // HACK alert - delay 250 msec for the option onclick to register and set value.
		setTimeout(function() {
			helper.resetFocusTracker(component);
			helper.hideDropdown(component);
			// Stop request queue processor
			if (component.get("v.useCache") === false) {
				helper.stopRequestQProcessor(component);
			}
		}, 250);
	},

	mouseoverHandler : function(component, event, helper) {
		// Reset keyboard option focus when the mouse is moved over the options.
		helper.resetFocusTracker(component);
	},

	/**
	 * Handle change of the parent key. Clear the selection and the lookup list.
	 */
	parentKeyChangeHandler : function(component, event, helper) {
		//console.log(event.getParam("oldValue"));
        //console.log(event.getParam("value"));

		helper.clearValueAndHideDropdown(component);
		helper.clearLookupList(component);
	},
    
    changeHandler : function(component, event, helper) {
        // Detect if the input text is a valid option
    	// helper.detectValidTypedInValue(component);
    	helper.checkForCleared(component);
    	
    },

    selectLookupValue : function(component, event, helper) {
    	// the recordid on the option has the index of the selection in the lookup list
        var dataVal = event.currentTarget.dataset.recordid;
        var list = component.get("v.lookupList");
        helper.selectLookupValue(component, list[dataVal]);
	},

    comboValueChangeHandler : function(component, event, helper) {
        component.set("v.currentTextVal", component.get("v.comboValue"));
	}
})