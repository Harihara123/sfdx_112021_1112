({
	helperFun : function(component,event,secId) {
	  var acc = component.find(secId);
        	for(var cmp in acc) {
        	$A.util.toggleClass(acc[cmp], 'slds-show');  
        	$A.util.toggleClass(acc[cmp], 'slds-hide');  
       }	   
	},
	fireURLEventEmail : function(component, emailValue) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "mailto:" + emailValue
        });
        urlEvent.fire();
    },
	fireURLEventPhone : function(component, phoneValue) {

		if (phoneValue !== null && phoneValue !== undefined){
			phoneValue = phoneValue.replace(/\s/g,'');
        
			var urlEvent = $A.get("e.force:navigateToURL");
			urlEvent.setParams({
				"url": "tel:" + phoneValue
			});
			urlEvent.fire();
		}
    },
})