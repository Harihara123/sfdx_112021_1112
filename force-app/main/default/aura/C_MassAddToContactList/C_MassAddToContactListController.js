({
    doInit : function(cmp,event,helper){
		helper.populateCurrentUser(cmp); 
  		helper.populateRecordTypeName(cmp);               
    },
    saveChanges :function(cmp, event, helper) {        
  		event.getSource().set("v.disabled",true);
  		helper.saveAddtoList(cmp);      
    },
    killComponent :function(cmp, event, helper) {
		var recordType = cmp.get("v.recordType");
		if(recordType === 'No record type name') { 
			helper.backToSource(cmp,recordType); 
		} else {
			helper.backToSource(cmp,'Client');  		
		}    
    },
    cancelChanges :function(cmp, event, helper) {
		var recordType = cmp.get("v.recordType");
		if(recordType === 'No record type name') {
			helper.backToSource(cmp,recordType); 
		} else {
			helper.backToSource(cmp,'Client');  		
		}      
    },
    addContactList: function(component, event, helper) {
       var contactList = component.get("v.cListInit");
       if(contactList.length <= 74) {
           contactList.unshift({
               'key' : '',
               'value':'',
			   'name':'',
               'showPill' : true
           });
           component.set("v.cListInit",contactList);
           var addToListbutton = component.find("addToContactListMass"); 
           if(typeof addToListbutton === 'undefined') {
              addToListbutton = component.find("addToContactList");
           }
           addToListbutton.set("v.disabled", true)   	   
       } else {
          helper.showError('You have reached your maximum limit.','Maximum lists');
       }
    },
	  selectAllCheckboxes: function(component, event, helper) {
        helper.selectAllContactLists(component);
    },

    setName: function(component, event, helper) {
        helper.setName(component,event);
    },
    clearInput: function(component, event, helper){ 
       component.find("contactListInput").set("v.value", "");

    },
	  createContactTagRecords: function(component, event, helper) {
		  helper.createContactTagRecords(component,event);
	  }
})