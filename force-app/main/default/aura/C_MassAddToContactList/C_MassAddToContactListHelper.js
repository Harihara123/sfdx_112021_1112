({ 
    setName:function(component,event){
	    var inputField = component.find('contactListInput'),
            setName = inputField.get("v.value"),
            contactList = component.get("v.cListInit"),
			contactListToServer = component.get("v.contactLists"); 

        if(setName != '') {
    		var isValid = true;
    		for(var i=0; i< contactList.length; i++) {
    			if(contactList[i].name.toLowerCase() != setName.toLowerCase()) {
    				continue;
    			} else {					
    				isValid = false;
    				break;
    			}
    		}
    		if(isValid != true) {
				this.showError('Contact List name already exists!!','Duplicate List');
    		} else {

    			contactList.shift();
				 
    			contactList.unshift({
    				'key' : setName,
    				'value': setName,
    				'name': setName,
    				'showPill' : false,
                    'hasContactTag' : false
    			});

				contactListToServer.push({    				
					'key' : setName,
    				'value': setName,
    				'name': setName,
    				'showPill' : false,
                    'hasContactTag' : false
				});
    			component.set("v.cListInit",contactList); 					
				component.set("v.contactLists",contactListToServer); 
                var addToListbutton = component.find("addToContactListMass"); 
                if(typeof addToListbutton === 'undefined') {
                    addToListbutton = component.find("addToContactList");
                }
                addToListbutton.set("v.disabled", false);
                var checkboxes = component.find("inputCheckboxId");
                //var contactTagsToServer = component.get("v.selectedContactLists");
                for (var i = 0; i < checkboxes.length; i++) {
                    if(checkboxes[i].get("v.name") == setName) {
                        checkboxes[i].set("v.checked",true);                     
                        this.setContactTagRecords(component,setName,true);
                    }
                }					
    		}		 
  		}          	
	},
	saveAddtoList :function(cmp) {
        var self = this;

        this.showSpinner(cmp);        
        var clist =  cmp.get("v.contactLists");
        var cInitMap = cmp.get("v.cMapInit");
        var recordType = cmp.get("v.recordType");
		var contactTagsToServer = cmp.get("v.selectedContactLists");
        var cMapNew = {},cTagMapNew = {};        

        for(var i=0;i<clist.length;i++){
            if(typeof clist[i] !== "undefined"){
                cMapNew[clist[i].key] = clist[i].value;
            }
        }
        for(var i=0;i<contactTagsToServer.length;i++){
            if(typeof contactTagsToServer[i] !== "undefined"){
				if(typeof cInitMap[contactTagsToServer[i]] === "undefined"){
					cTagMapNew[contactTagsToServer[i]] = contactTagsToServer[i];
				} else {
					cTagMapNew[contactTagsToServer[i]] = cInitMap[contactTagsToServer[i]];
				}
            }
        }
        if((Object.keys(cMapNew).length > 0) || (Object.keys(cTagMapNew).length >0) ){
			var selectedContacts = cmp.get("v.selectedContacts");
            var params;
			var contactIds = [];
			for(var i=0;i<selectedContacts.length;i++) {
				var contact = selectedContacts[i];
				contactIds.push(String(contact.key));
			} 
			var contactIdsJSON = JSON.stringify(contactIds);
            params = {
                'contactRecords' : contactIdsJSON,
                'clist' : cMapNew,
				'cTagList' : cTagMapNew,
                'requestId': cmp.get("v.requestId"),
                'transactionId' : cmp.get("v.transactionId")
                
            };
            this.callServer(cmp,'ATS','ContactListFunctions',"saveAllContactLists",function(response){
               
                if(cmp.isValid()){				
                    var toastEvent = $A.get("e.force:showToast");
                    var message = "Your new lists are successfully created";
                    toastEvent.setParams({
                        "type": "success",
                        "title": "Success!",
                        "message": message
                    });
                    toastEvent.fire();
                    this.hideSpinner(cmp);
					self.backToSource(cmp,recordType);
                }
            },params,true); 

        }else{
            if(recordType != 'No record type name') {
				var dismissActionPanel = $A.get("e.force:closeQuickAction");
				dismissActionPanel.fire();
            } 			
			this.hideSpinner(cmp);
            var toastEvent = $A.get("e.force:showToast");
            var message = "No new lists have been added";
            toastEvent.setParams({
                "type": "error",
                "title": "Error",
                "message": message
            });
            toastEvent.fire(); 
            this.hideSpinner(cmp);
			if(recordType != 'Talent') {
	            cmp.find("saveId").set("v.disabled",false);			
			}
			self.backToSource(cmp,recordType);
        }
    },    
    backToSource : function (cmp,recordType) {
		var parentId = cmp.get("v.parentId");
		if(typeof parentId !== 'undefined' && parentId !== '') {
			history.go(-1);
		}
		else {
		if(recordType === 'Talent' ) {     
			//events being fired from a talent record
            var recordId = cmp.get("v.recordId") ;        
			var transactionId = cmp.get("v.transactionId");
			var requestId = cmp.get("v.requestId");
            var refreshAppEvent = $A.get('e.c:E_RefreshChild');
			refreshAppEvent.fire();
            var params = {recordId: recordId};
            /*ak changes var createComponentEvent = cmp.getEvent('createCmpEventModal');
            createComponentEvent.setParams({
                    "componentName" : 'c:C_MassAddToContactListModal'
                    , "params":params
                    ,"targetAttributeName": 'v.C_MassAddToContactListModal' });
            createComponentEvent.fire(); */
		
			//events being fired from a search page 
			var createComponentEvent2 = cmp.getEvent('closeCmpEventContactList');
			var params2 = {recordId: recordId,                 
				  requestId: requestId,
                  transactionId: transactionId};
            createComponentEvent2.setParams({
                    "componentName" : 'c:C_MassAddToContactListModal'
                    , "params":params2
                    ,"targetAttributeName": 'v.C_MassAddToContactListModal' });
            createComponentEvent2.fire();
        } else if(recordType === 'Client'){
			var refreshAppEvent = $A.get('e.c:E_RefreshChild');
			refreshAppEvent.fire();
            $A.get('e.force:refreshView').fire();
			var dismissActionPanel = $A.get("e.force:closeQuickAction");
			dismissActionPanel.fire();
        } else {
			var returnLocation = cmp.get("v.returnVal");
            if(returnLocation == true){
                var urlre='https://'+window.location.hostname+'/lightning/n/Contact_Topics';
            	window.location=urlre;
            }else{
                var navEvent = $A.get("e.force:navigateToList");
                navEvent.setParams({
                    "listViewId": cmp.get("v.listViewId"),
                    "listViewName": "All",
                    "scope": "Contact"
                });
                navEvent.fire();
            }
		}
		}
    },
    populateRecordTypeName:function(cmp) {
        var params;
        var recordId = cmp.get("v.recordId");
        var objName = 'Contact';
        if(typeof recordId !== 'undefined' && recordId.substring(0, 3) === '001'){
            objName = 'Account';
        }
        params = {
                    'recordId' : recordId,
                    'objName' : objName          
                };
        this.callServer(cmp,'','',"getRecordTypeName",function(response){
            if(cmp.isValid()){
                if(response !== 'No record type name') {
                    cmp.set("v.recordType",response);
                } else {
                     cmp.set("v.recordType",'No record type name');
                }           
            }
			
			var recordType = cmp.get("v.recordType");
			this.refreshPopup(cmp,recordType);  
        },params,true);   
       
    }, 
	refreshPopup: function(cmp,recordType) {
		var contactslength = 1;	
		var contactRecords = cmp.get("v.contactRecords");
		var recordID = cmp.get("v.recordId");
		if(typeof contactRecords !== 'undefined' && contactRecords != ''){
			var partsOfStr = contactRecords.split(',');
			if(typeof partsOfStr !== 'undefined') {
			  contactslength = partsOfStr.length;
			}
			this.populateContactRecords(cmp,contactRecords); 
  		} else if(typeof recordID !== 'undefined' && recordID != '') {
  			contactRecords = '['+recordID+']';
			this.populateContactTagLists(cmp, contactslength);
			this.populateContactRecords(cmp,contactRecords);
  		}
		if(recordType == 'No record type name') {
			this.populateContactLists(cmp,contactslength,recordType);		
		}
	},
    populateCurrentUser:function(cmp){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        cmp.set("v.userId",userId);   
    },
    populateContactRecords:function(cmp,contactRecords){
		var params = {
                    'contactIds' : JSON.stringify(contactRecords) 
                };
        this.callServer(cmp,'ATS','ContactListFunctions',"getContactRecords",function(response){
			if(cmp.isValid() && (response.length > 0) ){
				var opt = []; 
					for(var i=0; i<=response.length;i++) {     //this iterates over the returned list
						if(typeof response[i] !== "undefined"){
							opt.push({"key":response[i].Id,
									"value":response[i].Name});							
						}
					}
					cmp.set("v.selectedContacts", opt);//base list used as keys to populate contact names  
                               
				}
			},params,true);   
    },
    populateContactLists:function(cmp,contactslength,recordType){
		var params;
        params = {};
        this.callServer(cmp,'ATS','ContactListFunctions',"getTagDefinitions",function(response){
            if(cmp.isValid() && (response.length > 0) ){
            var opt = []; 
                var initMap = {};
                var key = '';
                var initContactTagMap = cmp.get("v.cMapContactTagInit");
                for(var i=0; i<=response.length;i++) {     //this iterates over the returned map
                    if(typeof response[i] !== "undefined"){
                        var hasContactTag = false;
                        if(initContactTagMap && (typeof initContactTagMap[response[i].Id] !== "undefined")) {
                            hasContactTag = true; 
                        }
                        if(((response[i].Candidate_Count__c + contactslength) > 250) && !response[i].Legacy_List__c){
                            hasContactTag = true; 
                        }
                        opt.push({"key":response[i].Id,
                                "value":response[i].Tag_Name__c + ' (' + response[i].Candidate_Count__c + ')',
								"name": response[i].Tag_Name__c,
                                "showPill":false,
                                "hasContactTag":hasContactTag});
                        initMap[response[i].Id] = response[i].Tag_Name__c; 
                    }
                  
                }
                cmp.set("v.cMapInit", initMap);//to send to backend to create new lists
                cmp.set("v.cListInit", opt);//to populate existing badges, this is not bound with lookup pillbar
				if(recordType == 'Talent') {
					var dismissActionPanel = $A.get("e.force:closeQuickAction");
					dismissActionPanel.fire();
				}               
            }
        },params,false); 
    },    
    populateContactTagLists:function(cmp, contactslength,recordType){
		var params;
        params = {
                'recordId' : cmp.get("v.recordId")          
            };
        this.callServer(cmp,'ATS','ContactListFunctions',"getContactLists",function(response){
		    if(cmp.isValid() && (response.length > 0) ){
                var initContactTagMap = {};
                var key = '';
                for(var i=0; i<=response.length;i++) {     //this iterates over the returned map
                    if(typeof response[i] !== "undefined"){ 
                        initContactTagMap[response[i].Tag_Definition__c] = response[i].Tag_Definition__r.Tag_Name__c;
                    }                 
                }
                cmp.set("v.cMapContactTagInit", initContactTagMap);        
            } 
			this.populateContactLists(cmp,contactslength,recordType);  
        },params,false); 
    },
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    callServer : function(component, className, subClassName, methodName, callback, params, storable, errCallback) {
        var serverMethod = 'c.performServerCall';
        var actionParams = {'className':className,
                            'subClassName':subClassName,
                            'methodName':methodName.replace('c.',''),
                            'parameters': params};


        var action = component.get(serverMethod);
        action.setParams(actionParams);

        action.setBackground();
        
        if(storable){
            action.setStorable();
        }else{
            action.setStorable({"ignoreExisting":"true"});
        }
      
        action.setCallback(this,function(response) {
            var state = response.getState();
                if (state === "SUCCESS") { 
                // pass returned value to callback function
                callback.call(this, response.getReturnValue());   
                
            } else if (state === "ERROR") {
                // Use error callback if available
                if (typeof errCallback !== "undefined") {
                    errCallback.call(this, response.getReturnValue());
                    // return;
                }

                // Fall back to generic error handler
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        this.showError(errors[0].message);
                        //throw new Error("Error" + errors[0].message);
                    }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                        this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                    }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                        this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                    }else{
                        this.showError('An unexpected error has occured. Please try again!');
                    }
                } else {
                    this.showError(errors[0].message);
                }
            }
           
        });
    
        $A.enqueueAction(action);
    },
    // automatically call when the component is done waiting for a response to a server request.  
    hideSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    // automatically call when the component is waiting for a response to a server request.
    showSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
	//select/unselect all contact lists
	selectAllContactLists : function (component) {
		var checkboxes = component.find("inputCheckboxId");
		var selectAllCheckbox = component.find("selectAllInputId").get("v.checked");
		var ln = checkboxes.length;

        var initContactTagMap = component.get("v.cMapContactTagInit");
		if ((typeof ln == "undefined") && !checkboxes.get("v.disabled")) {
			checkboxes.set("v.checked",selectAllCheckbox);
            this.setContactTagRecords(component,checkboxes.get("v.name"),selectAllCheckbox); 
		} else {
			for (var i = 0; i < ln; i++) {
                var checkboxName = checkboxes[i].get("v.name");
				var isDisabled = checkboxes[i].get("v.disabled");
                if(isDisabled || (initContactTagMap && initContactTagMap[checkboxName])) {
                    checkboxes[i].set("v.checked",false);  
                } else {
                    checkboxes[i].set("v.checked",selectAllCheckbox); 
                    this.setContactTagRecords(component,checkboxName,selectAllCheckbox);                   
                }
			}
		}
        var contactTagsToServer = component.get("v.selectedContactLists");
	},
	createContactTagRecords: function(component, event) {
		var selectedKey = event.getSource().get("v.name");
        var isChecked = event.getSource().get("v.checked");
		this.setContactTagRecords(component,selectedKey,isChecked);
	},
    setContactTagRecords: function(component,selectedKey,isChecked) {
        var contactTagsToServer = component.get("v.selectedContactLists");
        if(isChecked) {
            if(!contactTagsToServer.includes(selectedKey)) {
                contactTagsToServer.push(selectedKey);                  
            }          
        } else {
            var index = contactTagsToServer.indexOf(selectedKey);
            if (index > -1) {
              contactTagsToServer.splice(index, 1);
            }
        }
        component.set("v.selectedContactLists",contactTagsToServer);
    }
})