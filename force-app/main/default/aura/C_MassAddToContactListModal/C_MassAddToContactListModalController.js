({
	displayContactListModal : function(component, event, helper) {
		/* ak changesvar recordID = event.getParam("recordId");
		component.set("v.recordId", recordID);*/
		helper.toggleClass(component,'addToContactListModalbackdrop','slds-backdrop--');
		helper.toggleClass(component,'addToContactListModalbox','slds-fade-in-');
	},
    saveChanges :function(component, event, helper) {
		event.getSource().set("v.disabled",true);
		component.set("v.saveList", true);     
    },
    killComponent :function(component, event, helper) {
        helper.backToSource(component);      
    },
    cancelChanges :function(component, event, helper) {      
        helper.backToSource(component);      
    }
 })