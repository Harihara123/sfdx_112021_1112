({
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },      
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    }, 
     
    backToSource : function (cmp) { 
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();      
       /* ak changes var recordId = cmp.get("v.recordId") ;      
        var params = {recordId: recordId};
        var createComponentEvent = cmp.getEvent('createCmpEventModal');
        createComponentEvent.setParams({
                "componentName" : 'c:C_MassAddToContactListModal' 
                , "params":params
                ,"targetAttributeName": 'v.C_MassAddToContactListModal' });
        createComponentEvent.fire();*/

        cmp.destroy();
    }
})