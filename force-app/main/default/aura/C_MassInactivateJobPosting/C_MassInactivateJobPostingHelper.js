({
    inactivatePosting : function(component, event, helper) {
        
        
        var jobPosting=component.get("v.selectedRecords");
        console.log('Selected contacts in Lightning Component'+jobPosting);
        
        var partsOfStr = jobPosting.split(',');
        
        
        var strList=[];
        var x;
        var returnAction=component.get("c.massInactivateJobPostings");
        var successList=[];
        var errorList=[];
        
        for(x in partsOfStr){
            var y=String(partsOfStr[x]);
            strList.push(y);
        }
        
        console.log('---'+strList.length);
        console.log('-->'+strList);
        if(strList[0].trim() == ''){
            alert('Please select atleast one record to proceed.');
             var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'ERROR',
                    message: 'Please select atleast one record to proceed.',
                    type: 'ERROR',
                });
                toastEvent.fire();
        }else{
            returnAction.setParams({"jobPostingIdList":  strList});
            returnAction.setCallback(this, function(response) {
                var data = response;
                var spinner = component.find("rwsSpinner");
                
                
                $A.util.addClass(spinner, "slds-hide");
                // Set the component attributes using values returned by the API call
                if (data.getState() == 'SUCCESS') {
                    console.log('call back success');
                    var postingMap=data.getReturnValue();
                    
                    for ( var key in postingMap ){
                        
                        if(postingMap[key]  === 'SUCCESS'){
                            successList.push(key+'  Inactivation Successful.'); 
                        }
                        else if(postingMap[key] === 'DIFF_OWNER') {
                            errorList.push(key+' Inactivation Failed: Posting not owned by You.'); 
                        }
                            else if(postingMap[key] === 'ALREADY_INACTIVE'){
                                errorList.push(key+' Inactivation Failed: Posting is Inactive.'); 
                            }
                    }
                    
                    
                    
                }else if(data.getState() == 'ERROR'){
                    errorList.push('Error occured while inactivating Job Posting.');
                }           
                component.set("v.successList",successList);
                component.set("v.errorList",errorList);
            });
            $A.enqueueAction(returnAction);  
        }     
        
      
        
        
    },
    showToastMessage : function (component,message,title,type){
        component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Something has gone wrong!",
            "message": "Unfortunately, there was a problem updating the record.",
            closeCallback: function() {
                alert('You closed the alert!');
            }
        });
        
    }
})