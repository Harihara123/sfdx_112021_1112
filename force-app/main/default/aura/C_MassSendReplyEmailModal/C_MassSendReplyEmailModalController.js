({
    initAction: function(component, event, helper) {

        // These lables has to be there to be loaded
        // $Label.c.ATS_IR_IN_OFFICE_REJECT
        // $Label.c.ATS_IR_POSITION_FILLED
        // $Label.c.ATS_IR_REJECT
        // $Label.c.ATS_INTERVIEW_APPLIED_SCHEDULED
        // $Label.c.ATS_GOOD_CANDIDATE_CALL_INTERVIEW
        // $Label.c.ATS_GOOD_CANDIDATE_CANT_REACH_PHONE
        // $Label.c.ATS_GOOD_CANIDATE_RATE_HIGH
        // $Label.c.ATS_GOOD_CANDIDATE_UNDER_QUALIFIED
        // $Label.c.ATS_NO_RELOCATION
        // $Label.c.ATS_REJECTION
        // $Label.c.ATS_OVERWHELMED

        helper.loadAllTemplates(component);

        //var ele = component.get("v.emailTemplate");
        //var htmlContent = $A.get("$Label.c." + ele);
        //component.set("v.htmlData",htmlContent);

        // Load all applicants name and emails
        var records = component.get("v.subIds");
        var emailRecipients = [];
		var emailNotRecipients = [];
        for (var i = 0; i < records.length; i++) {
             if (typeof records[i].jpOrder.ShipToContact !== "undefined" && records[i].jpOrder.ShipToContact !== "") {
                 if((!$A.util.isUndefinedOrNull(records[i].jpOrder.ShipToContact.Email)) && records[i].jpOrder.ShipToContact.Email !== ""){
					//var recipient = {};
					//recipient["email"] = records[i].jpOrder.ShipToContact.Email;
					//recipient["name"] = records[i].jpOrder.ShipToContact.Name;
					//emailRecipients.push(recipient);
					  emailRecipients.push(records[i]);
				 }else{
				     emailNotRecipients.push(records[i].jpOrder.ShipToContact.Name); 
				  }
				} 
            }
        component.set("v.emailToList",emailRecipients);
		component.set("v.emailNotToList",emailNotRecipients);
        //console.log("emailRecipients "+ JSON.stringify(emailRecipients));
    },
	cancelStatus: function(component, event, helper) {
		helper.fireCanceledEvt(component);
	},

	sendEmail: function(component, event, helper) {

        helper.sendEmailHelper(component);
	    
	},

	submitChange: function(component, event, helper) {
		helper.showSpinner(component);
	},
    didSelectTemplate : function(component, event, helper) {
        helper.didSelectTemplateHelper(component, event);
    }
})