({
	fireCanceledEvt : function(component) {
		var evt = component.getEvent("statusCanceledEvt");
		evt.fire();
	},
validateEvt : function(component) {
	var valid = true;
	var list = component.find("InputSelectDynamic")
	if(component.get("v.selectedValue") === ''){
	 
            // Set error
            list.set("v.errors", [{message:"Please select a value."}]);
			valid = false;
        } else {
            // Clear error
            list.set("v.errors", null);
        }

		return valid;
			
	},
	fireSavedEvt : function(component) {
		this.showSpinner(component);
			
		var params = {
				"subIds" : component.get("v.subIds"),
				"newStatus" : component.find("InputSelectDynamic").get("v.value"),
				"npReason" : component.find("NPReason") ? component.find("NPReason").get("v.value") : ''//component.get("v.selectedValue")
			};
		var action = component.get("c.saveSubmittalMassUpdate");
		action.setParams(params);
		action.setCallback(this, function(response) {
			this.hideSpinner(component);
			if (response.getReturnValue() === 'SUCCESS') {
				var reloadEvt = $A.get("e.c:E_TalentActivityReload");
                    reloadEvt.fire();
					var evt = component.getEvent("statusCanceledEvt");
					evt.fire()
			} else {
				console.log("Error saving ");
			}
		});
		$A.enqueueAction(action);
		//To Refresh Activity Timeline
		
	},
    sendEmailHelper: function(component) {        
        this.showSpinner(component);
        var allSubmittals = component.get("v.emailToList");
        var subMittals = [];
        var emailTemplate = component.get("v.emailTemplate");
		/* Start for story S-107087 added by siva*/
		var user = component.get("v.runningUser");
		var userCPCompany = component.get("v.userCPCompany");
		/* End for story S-107087 added by siva*/
        /*if(emailTemplate.length > 10){
            emailTemplate = emailTemplate.substring(0,9);
        }*/
		component.set("v.isDisable", "true");
        for (var i = 0; i < allSubmittals.length; i++) {
            var submittal = {};
            submittal["contactId"] = allSubmittals[i].jpOrder.ShipToContact.Id;
            submittal["salutation"] = allSubmittals[i].jpOrder.ShipToContact.Salutation;
            submittal["firstName"] = allSubmittals[i].jpOrder.ShipToContact.FirstName;
            submittal["lastName"] = allSubmittals[i].jpOrder.ShipToContact.LastName;
           // submittal["jobPostingId"] = allSubmittals[i].event.Job_Posting__r.Source_System_id__c;
            submittal["jobPostingId"] = allSubmittals[i].event.Job_Posting__r.Name;
			submittal["applicationDate"] = allSubmittals[i].jpOrder.EffectiveDate;
            submittal["jobPostingTitle"] = allSubmittals[i].jobTitle;
            submittal["emailTemplateName"]= emailTemplate;
			/* Start for story S-107087 added by siva*/
            submittal["userId"] = user.Id;//allSubmittals[i].event.Job_Posting__r.CreatedBy.Id;
            submittal["userFullName"] = user.Name;//((allSubmittals[i].event.Job_Posting__r.CreatedBy.FirstName != null && allSubmittals[i].event.Job_Posting__r.CreatedBy.FirstName != undefined) ? allSubmittals[i].event.Job_Posting__r.CreatedBy.FirstName :'')+((allSubmittals[i].event.Job_Posting__r.CreatedBy.LastName != null && allSubmittals[i].event.Job_Posting__r.CreatedBy.LastName != undefined) ? allSubmittals[i].event.Job_Posting__r.CreatedBy.LastName : '');
            submittal["userEmail"] = user.Email;//allSubmittals[i].event.Job_Posting__r.CreatedBy.Email;
            submittal["userTitle"] = user.Title;//allSubmittals[i].event.Job_Posting__r.CreatedBy.Title;
            submittal["userPhone"] = user.Phone;//allSubmittals[i].event.Job_Posting__r.CreatedBy.Phone;
            submittal["opco"] = userCPCompany;//allSubmittals[i].jpOrder.Opportunity.OpCo__c;
			/* End for story S-107087 added by siva*/
            submittal["contactEmail"] = allSubmittals[i].jpOrder.ShipToContact.Email;
            submittal["contactMobile"] = allSubmittals[i].jpOrder.ShipToContact.MobilePhone;
            submittal["contactState"] = allSubmittals[i].jpOrder.ShipToContact.MailingState;
            submittal["contactCountry"] = allSubmittals[i].jpOrder.ShipToContact.MailingCountry;
            submittal["language"] = '';
			submittal["submittalUniqueId"] = allSubmittals[i].jpOrder.Unique_Id__c;
            subMittals.push(submittal);
            //delete allSubmittals[i].event.Job_Posting__r;
        }       
        var params = {
                "submittals" : JSON.stringify(subMittals)
            };
        var action = component.get("c.saveEmailRecords");
        action.setParams(params);
        action.setCallback(this, function(response) {

            var toastStatus = 'Success';
            var toastMessage = 'Your email request is being processed.';

            this.hideSpinner(component);
			var rs = response.getReturnValue();
            if (rs === 'SUCCESS') {   
				component.set("v.isDisable", "false");
                  //
            } else {
                toastStatus = 'error';
                toastMessage = rs;
                console.log("Error saving ");
				component.set("v.isDisable", "false");
            }
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : '',
                message: toastMessage,
                messageTemplate: '',
                key: 'info_alt',
                type: toastStatus,
                duration : 10000
            });
            toastEvent.fire();
            var reloadEvt = $A.get("e.c:E_TalentActivityReload");
            reloadEvt.fire();
            var evt = component.getEvent("statusCanceledEvt");
            evt.fire()
        });
        $A.enqueueAction(action);
    },

	showSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},

	hideSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-show");
		$A.util.addClass(spinner, "slds-hide");
	},
    didSelectTemplateHelper: function(component, event){

        var ele = event.getSource().get("v.value");
        var allTemplates = component.get("v.allTemplates");
        var tempDisplayName = '';
        for(var c=0;c<allTemplates.length;c++){
            var template = allTemplates[c];
            if(template["templateId"]==ele){
                tempDisplayName = template["templateName"];
                break;
            }
        }
        component.set("v.tempDisplayName",tempDisplayName);
        component.set("v.emailTemplate",ele);
        var defualtEmailTemplate = component.get("v.defualtEmailTemplate");
        var htmlContent = '';//$A.get("$Label.c." + ele);
        if(tempDisplayName != defualtEmailTemplate){
            htmlContent = $A.get("$Label.c." + ele);
        }
        component.set("v.htmlData",htmlContent);
    },
    loadAllTemplates: function(component){

        var userOpco="";
        var defualtEmailTemplate = component.get("v.defualtEmailTemplate");
        var runningUser = component.get("v.runningUser");
        if(runningUser != undefined && runningUser.OPCO__c!=undefined){
            userOpco = runningUser.OPCO__c;
        }
        var allTemplates = [];
        var template={};

        // Default value
         
        template["templateId"] = 'ATS_ET_DEFAULT_VALUE';
        template["templateName"] = defualtEmailTemplate;
        allTemplates.push(template);
		//S-226643 - added 3 new Opco IND,SJA,EAS by Debasis
        if(userOpco != 'ONS' || userOpco != $A.get("$Label.c.Newco") || userOpco != $A.get("$Label.c.Aston_Carter") || userOpco != $A.get("$Label.c.Aerotek")){

            template={};
            template["templateId"] = 'ATS_IR_IN_OFFICE_REJECT';
            template["templateName"] = 'IR In-Office Reject';
            allTemplates.push(template);

            template={};
            template["templateId"] = 'ATS_IR_REJECT';
            template["templateName"] = 'IR Reject';
            allTemplates.push(template);

            template={};
            template["templateId"] = 'ATS_IR_POSITION_FILLED';
            template["templateName"] = 'IR Position Filled';
            allTemplates.push(template);
        }


        template={};
        template["templateId"] = 'ATS_INTERVIEW_APPLIED_SCHEDULED';
        template["templateName"] = 'Already Applied & Scheduled for an Interview';
        allTemplates.push(template);

        template={};
        template["templateId"] = 'ATS_GOOD_CANDIDATE_CALL_INTERVIEW';
        template["templateName"] = 'Good Candidate/Call for Interview';
        allTemplates.push(template);

        template={};
        template["templateId"] = 'ATS_GOOD_CANDIDATE_CANT_REACH_PHONE';
        template["templateName"] = 'Good Candidate/Can’t Reach by Phone';
        allTemplates.push(template);

        template={};
        template["templateId"] = 'ATS_GOOD_CANIDATE_RATE_HIGH';
        template["templateName"] = 'Good Candidate/Rate Too High for Req';
        allTemplates.push(template);

        template={};
        template["templateId"] = 'ATS_GOOD_CANDIDATE_UNDER_QUALIFIED';
        template["templateName"] = 'Good Candidate/Under Qualified for Req';
        allTemplates.push(template);

        template={};
        template["templateId"] = 'ATS_NO_RELOCATION';
        template["templateName"] = 'No Relocation Offered';
        allTemplates.push(template);

        template={};
        template["templateId"] = 'ATS_REJECTION';
        template["templateName"] = 'Outright Rejection';
        allTemplates.push(template);

        template={};
        template["templateId"] = 'ATS_OVERWHELMED';
        template["templateName"] = 'Overwhelmed/Will Contact if Good Fit';
        allTemplates.push(template);

        component.set("v.allTemplates",allTemplates);
    }
})