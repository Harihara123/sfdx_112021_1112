({
  doInit: function(cmp, event, helper) {
       helper.getPicklistValues(cmp);
    },

	cancelStatus: function(component, event, helper) {
		helper.fireCanceledEvt(component);
	},

	saveStatus: function(component, event, helper) {
	    if(helper.validateEvt(component)){
			helper.fireSavedEvt(component);
		}
	},

	submitChange: function(component, event, helper) {
		//helper.showSpinner(component);
	},
	getDSPReason: function(component, event, helper) {
		helper.getDSPReason(component);
	}
	
})