({
	getPicklistValues : function(component) {
	  var matrix  = component.get("v.dispoMatrix"); 
	  var codeOptionsMap = {};
	  var codeOptions = [];
	  for(var i in matrix){
	      //codeOptions.push({label:matrix[i].Disposition_Code_Label__c,value:matrix[i].Disposition_Code_Value__c }); 
		  codeOptionsMap[matrix[i].Disposition_Code_Label__c] = matrix[i].Disposition_Code_Value__c;
	  }
	  var mapKeys = Object.keys(codeOptionsMap);
	  for(var j in mapKeys ){
	      codeOptions.push({label : mapKeys[j] , value : codeOptionsMap[mapKeys[j]]  });
	  }


	  component.set("v.codeOptions",codeOptions)
	    	
	},
	showSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},

	hideSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-show");
		$A.util.addClass(spinner, "slds-hide");
	},
	fireCanceledEvt : function(component) {
		var evt = component.getEvent("statusCanceledEvt");
		evt.fire();
	},
	getDSPReason : function(component) {
		var dsp = component.find("InputSelectDynamic").get("v.value");
		component.set("v.selectedValue",dsp);
		var reasonOptions = [];
		var matrix  = component.get("v.dispoMatrix"); 
		for(var i in matrix){
	      if( matrix[i].Disposition_Code_Value__c === dsp &&  !$A.util.isUndefinedOrNull(matrix[i].Disposition_Reason_Label__c)){
		     reasonOptions.push({label : matrix[i].Disposition_Reason_Label__c , value : matrix[i].Disposition_Reason_Value__c  });
		  }
		 }
		 component.set("v.reasonOptions",reasonOptions);
	},
	validateEvt : function(component) {
	var valid = true;
	var list = component.find("InputSelectDynamic");
	var dspval = list.get("v.value");
	
	
	if(dspval === '' || $A.util.isUndefinedOrNull(dspval)){
	    // Set error
            list.set("v.errors", [{message:"Please select a value."}]);
			valid = false;
        } else {
            // Clear error
            list.set("v.errors", null);
			var dspreason = component.find("DSPPReason");
			if(!$A.util.isUndefinedOrNull(dspreason)){
				var dsprval = dspreason.get("v.value");
			   if(dsprval === '' || $A.util.isUndefinedOrNull(dsprval)){
					// Set error
					dspreason.set("v.errors", [{message:"Please select a value."}]);
					valid = false;
				} else {
					// Clear error
					dspreason.set("v.errors", null);
				}
			}
			
			}

		return valid;
			
	},
	fireSavedEvt : function(component) {
		this.showSpinner(component);
			
		var params = {
				"submittals" : this.createSubmittalData(component),
				"Code" : component.find("InputSelectDynamic").get("v.value"),
				"Reason" : component.find("DSPPReason") ? component.find("DSPPReason").get("v.value") : ''//component.get("v.selectedValue")
			};
		var action = component.get("c.saveMassDisposition");
		action.setParams(params);
		action.setCallback(this, function(response) {
			this.hideSpinner(component);
			var rs = response.getReturnValue();
			 var toastEvent = $A.get("e.force:showToast");
			if ( rs === 'SUCCESS') {
				var reloadEvt = $A.get("e.c:E_TalentActivityReload");
                    reloadEvt.fire();
					var evt = component.getEvent("statusCanceledEvt");
					evt.fire()
					 toastEvent.setParams({
							"type": "success",
							"message": 'Your changes are successfully saved.'
						});	
			} else {
				
				   toastEvent.setParams({
							"type": "error",
							"message": rs
						});		
			}
			toastEvent.fire();

			/*if (response.getReturnValue() === 'SUCCESS') {
				var reloadEvt = $A.get("e.c:E_TalentActivityReload");
                    reloadEvt.fire();
					var evt = component.getEvent("statusCanceledEvt");
					evt.fire()
			} else {
				console.log("Error saving ");
			}*/
		});
		$A.enqueueAction(action);
		//To Refresh Activity Timeline		
	},
    createSubmittalData:function(component){
        var subMittals = [];
        var allSubmittals = component.get("v.subIds");
        for (var i = 0; i < allSubmittals.length; i++) {
            var submittal = {};
            submittal["contactId"] = allSubmittals[i].jpOrder.ShipToContact.Id;
            submittal["salutation"] = allSubmittals[i].jpOrder.ShipToContact.Salutation;
            submittal["firstName"] = allSubmittals[i].jpOrder.ShipToContact.firstName;
            submittal["lastName"] = allSubmittals[i].jpOrder.ShipToContact.lastName;
            submittal["jobPostingId"] = allSubmittals[i].event.Job_Posting__r.Source_System_id__c;
            submittal["applicationDate"] = allSubmittals[i].jpOrder.EffectiveDate;
            submittal["jobPostingTitle"] = allSubmittals[i].jobTitle;
            submittal["userId"] = allSubmittals[i].event.Job_Posting__r.CreatedBy.Id;
            submittal["userFullName"] = ((allSubmittals[i].event.Job_Posting__r.CreatedBy.FirstName != null && allSubmittals[i].event.Job_Posting__r.CreatedBy.FirstName != undefined) ? allSubmittals[i].event.Job_Posting__r.CreatedBy.FirstName :'')+((allSubmittals[i].event.Job_Posting__r.CreatedBy.LastName != null && allSubmittals[i].event.Job_Posting__r.CreatedBy.LastName != undefined) ? allSubmittals[i].event.Job_Posting__r.CreatedBy.LastName : '');
            submittal["userEmail"] = allSubmittals[i].event.Job_Posting__r.CreatedBy.Email;
            submittal["userTitle"] = allSubmittals[i].event.Job_Posting__r.CreatedBy.Title;
            submittal["userPhone"] = allSubmittals[i].event.Job_Posting__r.CreatedBy.Phone;
            submittal["opco"] = allSubmittals[i].jpOrder.Opportunity.OpCo__c;
            submittal["contactEmail"] = allSubmittals[i].jpOrder.ShipToContact.Email;
            submittal["contactMobile"] = allSubmittals[i].jpOrder.ShipToContact.MobilePhone;
            submittal["contactState"] = allSubmittals[i].jpOrder.ShipToContact.MailingState;
            submittal["contactCountry"] = allSubmittals[i].jpOrder.ShipToContact.MailingCountry;
            submittal["language"] = '';
            submittal["eventId"] = allSubmittals[i].event.Id;
            submittal["vendorId"] = allSubmittals[i].event.Feed_Source_ID__c; 
            submittal["applicationId"] = allSubmittals[i].event.Vendor_Application_ID__c;             
            subMittals.push(submittal);
        }
        return JSON.stringify(subMittals);
    }
})