({
	jobTitleAdded : function(component, event, helper) {
		// Add event.getParam("addedJobTitle") to automatchJobTitles
	},

	jobTitleRemoved : function(component, event, helper) {
		// Remove event.getParam("removedJobTitle") from automatchJobTitles
	},
	
	skillAdded : function(component, event, helper) {
		// Add event.getParam("addedSkill") to automatchSkills
	},
	
	skillRemoved : function(component, event, helper) {
		// Remove event.getParam("addedSkill") from automatchSkills
	},

	handleCriteriaChange : function(component, event, helper) {
		// Fire event E_MatchCriteriaChanged
	},
	
})