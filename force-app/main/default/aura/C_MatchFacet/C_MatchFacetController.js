({ 
	updatePills : function(component, event, helper) {
		helper.normalizeData(component);
		helper.redrawPills(component);
		if (component.get("v.showRelatedPopup")) {
			// Using a timeout so that lightning does not combine this callout with the save search.
			// Combined call causes a "please save work before call out" error.
			setTimeout($A.getCallback(function() {
				helper.fetchRelatedTerms(component);
			}), 1000);
		}
	},

	pillClosed : function(component, event, helper) {
        var isPillExcluded = event.getParam("isExcluded");
		var foundAndUpdated = (isPillExcluded != undefined && isPillExcluded)? helper.removeExcludedTermFromState(component, event.getParams()) : helper.removeTermFromState(component, event.getParams());
		if (foundAndUpdated) {
			helper.redrawPills(component);
			helper.fireMatchFacetChangedEvt(component);   //To enable multi select gesture on Search/Match
		}
	},

	newTermSelected : function(component, event, helper) {
		// console.log("Selected " + JSON.stringify(event.getParams()));
		var s = event.getParams();
		helper.addTermsToState(component, [s.value]);
		helper.redrawPills(component);
		helper.fireMatchFacetChangedEvt(component);	//To enable multi select gesture on Search/Match
        component.set("v.newMatchTerm",'');
        component.set("v.showFlyOut", false);
	},

	
	relatedTermsAdded : function(component, event, helper) {
		// console.log("Added terms --- " + JSON.stringify(event.getParams()));
		helper.addTermsToState(component, event.getParam("addedTerms"));
		helper.redrawPills(component);
		helper.fireMatchFacetChangedEvt(component);
	},

  	toggleFacet : function(component, event, helper) {
		var isCollapsed = component.get("v.isCollapsed");
		component.set("v.isCollapsed", !isCollapsed);
	},
    
	togglePills : function(component, event, helper) {
		helper.togglePills(component);	
	},

	pillFavorited : function(component, event, helper) {
        var isPillExcluded = event.getParam("isExcluded");
		(isPillExcluded != undefined && isPillExcluded)? helper.excludedPillFavorite(component, event.getParams(),true) : helper.favoritePill(component, event.getParams());
		helper.redrawPills(component);
		helper.fireMatchFacetChangedEvt(component);
	},

	pillUnfavorited : function(component, event, helper) {
        var isPillExcluded = event.getParam("isExcluded");
		(isPillExcluded != undefined && isPillExcluded)? helper.excludedPillFavorite(component, event.getParams(),false) :helper.unfavoritePill(component, event.getParams());
		helper.redrawPills(component);
		helper.fireMatchFacetChangedEvt(component);
	},

	updatePillsRelatedTerms : function(component, event, helper) {
		helper.updatePillsRelatedTerms(component);
	},
    showHideExcludeKeyWordsView1 : function(component, event, helper) {
        // How do we have to clear FlyOut in case of hide?
        component.set("v.showFlyOut", !component.get('v.showFlyOut'));
  		// Closing popover on click away
        var windowCloserFn;

        windowCloserFn = (event) => {
            var thisPopover = document.getElementById("keywords-popover");
			if (event.target && thisPopover) {
            console.log(thisPopover.contains(event.target));
            console.log()
				if (component.get('v.showFlyOut') && !thisPopover.contains(event.target) && !event.target.classList.contains('add-button-1')) { 
			        component.set("v.showFlyOut", !component.get('v.showFlyOut'));
					document.removeEventListener("click", windowCloserFn);
				}
			} else {
				document.removeEventListener("click", windowCloserFn);
			}
        }
        document.addEventListener("click", windowCloserFn);

    },
    showHideExcludeKeyWordsView2 : function(component, event, helper) {
        // How do we have to clear FlyOut in case of hide?
        component.set("v.showFlyOut", !component.get('v.showFlyOut'));
  		// Closing popover on click away
        var windowCloserFn;

        windowCloserFn = (event) => {
            var thisPopover = document.getElementById("keywords-popover");
			if (event.target && thisPopover) {
            console.log(thisPopover.contains(event.target));
            console.log()
				if (component.get('v.showFlyOut') && !thisPopover.contains(event.target) && !event.target.classList.contains('add-button-2')) { 
			        component.set("v.showFlyOut", !component.get('v.showFlyOut'));
					document.removeEventListener("click", windowCloserFn);
				}
			} else {
				document.removeEventListener("click", windowCloserFn);
			}
        }
        document.addEventListener("click", windowCloserFn);

    }
    ,
    handleExcludeActionEvent : function(component, event, helper) {
        var handlerId = event.getParam("handlerId");
        if(handlerId==="MatchFacetJobTitles_1" || handlerId==="MatchFacetSkills_1"){
            helper.handlePillExcludeAction(component, event);
            helper.redrawPills(component);
            helper.fireMatchFacetChangedEvt(component);
        }
    },
	handleOpcoDomain:function(component,event,helper) {
		helper.fetchRelatedTerms(component);
	}

})