({
	getPillId : function(component, option) {
		var pillIdMap = component.get("v.pillIdMap");
		if (pillIdMap === null) {
			pillIdMap = {};
		}

		var randomId = pillIdMap[option];
		if (randomId === undefined) {
			randomId = Math.floor(Math.random() * 10000000000);
			pillIdMap[option] = randomId;
		}
		
		component.set("v.pillIdMap", pillIdMap);
		return randomId;
	},


	normalizeData : function(component) {
		var rawData = component.get("v.rawData");
		var normalized;

		switch (component.get("v.type")) {
			case "jobtitles" :

			case "skills" :

			default :
				normalized = this.normalizeDataDefault(component, rawData);
				break;
		}
        // Ignore exluded keywords from the sort Normalize      
		//component.set("v.normalizedData", this.sortNormalized(normalized));
	},

	normalizeDataDefault : function(component, rawData) {
		var normalized = [];
		// var state = this.makeMap(component.get("v.normalizedData"));

        var exldNormalized = [];

		if (rawData !== undefined && rawData !== null) {
			var rss = this.rss(rawData);
			for (var i=0; i<rawData.length; i++) {
				// var oldState = state[this.getPillId(component, rawData[i].term)];
                if(rawData[i].isExcluded){
                    exldNormalized.push({
                    "key" : rawData[i].term,
                    "rawWeight" : rawData[i].weight,
                    // "required" : oldState === undefined ? "false" : oldState.required.toString(),
                    "required" : (rawData[i].required || false).toString(),
                    "pillColorClass" : this.getPillColorClass(component, rawData[i].weight / rss),
                    "pillId" : this.getPillId(component, rawData[i].term),
                    "isExcluded" : rawData[i].isExcluded
                });
                }else{
                    normalized.push({
                    "key" : rawData[i].term,
                    "rawWeight" : rawData[i].weight,
                    // "required" : oldState === undefined ? "false" : oldState.required.toString(),
                    "required" : (rawData[i].required || false).toString(),
                    "pillColorClass" : this.getPillColorClass(component, rawData[i].weight / rss),
                    "pillId" : this.getPillId(component, rawData[i].term),
                    "isExcluded" : rawData[i].isExcluded
                });
                }				
			}
		}

        // Ignore exluded keywords from the sort Normalize
        normalized = this.sortNormalized(normalized);
        if(exldNormalized.length > 0){
            normalized  = exldNormalized.concat(normalized);
        }else{
            component.set("v.excludedData",[]);
        }

        component.set("v.normalizedData", normalized);
		return normalized;
	},

	rss : function(raw) {
		var rss = 0;
		raw.forEach(term => {
			rss += term.weight * term.weight;
		});
		return Math.sqrt(rss);
	},

	/** Function sorts normalized data array in descending order of term weight */
	sortNormalized : function(arr) {
		return arr.sort(function(a, b) {
			return b.rawWeight - a.rawWeight;
		})
	},

	reconstructRawData : function(component) {
		var normalizedData = component.get("v.normalizedData");
		var raw;        
		switch (component.get("v.type")) {
			case "jobtitles" :

			case "skills" :

			default :
				raw = this.reconstructDataDefault(component, normalizedData);
				break;
		}

		// console.log(raw);
		return raw;
		// component.set("v.normalizedData", normalized);
	},

	reconstructDataDefault : function(component, normalizedData) {
		var raw = [];

		for (var i=0; i<normalizedData.length; i++) {            
			raw.push({
				"term" : normalizedData[i].key,
				"weight" : normalizedData[i].rawWeight,
				"required" : normalizedData[i].required
			});
		}

		return raw;
	},

	removeTermFromState : function(component, pillClosedEvtParams) {
		var foundAndUpdated = false;
		var normalizedData = component.get("v.normalizedData");

		var excluded = component.get("v.excludedData");
		var data = (excluded && excluded.length > 0 ? excluded.concat(normalizedData) : normalizedData);

		var newState = [];

		if (data !== undefined && data !== null) {
			for (var i=0; i<data.length; i++) {
				if (data[i].pillId !== pillClosedEvtParams.pillId) {
					newState.push(data[i]);
				} else {
					foundAndUpdated = true;
				}
			}
		}

		component.set("v.normalizedData", newState);
		return foundAndUpdated;
	},
    removeExcludedTermFromState : function(component, pillClosedEvtParams) {
        var foundAndUpdated = false;
        var excludedData = component.get("v.excludedData");
        var newState = [];

        if (excludedData !== undefined && excludedData !== null) {
            for (var i=0; i<excludedData.length; i++) {
                if (excludedData[i].pillId !== pillClosedEvtParams.pillId) {
                    newState.push(excludedData[i]);
                } else {
                    foundAndUpdated = true;
                }
            }
        }
        component.set("v.excludedData", newState);
        // Add rest all excluded data to the normalized data to redraw the pills
        var normalizedData = component.get("v.normalizedData");
        if(normalizedData==undefined){
            normalizedData = [];
        }
        if(foundAndUpdated){
            normalizedData = newState.concat(normalizedData);//normalizedData.concat(newState);
        }
        component.set("v.normalizedData", normalizedData);
        return foundAndUpdated;
    },

	addTermsToState : function(component, addedTerms) {
		var normalizedData = component.get("v.normalizedData");
		if (normalizedData === undefined || normalizedData === null) {
			normalizedData = [];
		}
		for (var i=0; i<addedTerms.length; i++) {
			normalizedData.push(this.getNewNormalizedTerm(component, addedTerms[i]));
		}
        // Redraw will recreate pills, so add excluded list also to normalized data to draw those.
        var excludedData = component.get("v.excludedData");
        var enableExcludeFeature = component.get("v.enableExcludeFeature");
        if(excludedData!=undefined && excludedData.length>0){
            for(var ed=0;ed<excludedData.length;ed++){
                var exlObj = excludedData[ed];
                exlObj.isExcluded = enableExcludeFeature;
                normalizedData.push(exlObj);
            }
            //normalizedData = normalizedData.concat(excludedData);
        }
		component.set("v.normalizedData", normalizedData);
	},

	getNewNormalizedTerm : function(component, addedTerm) {
		var rawWeight = "";
		var normalizedTerm = {
			"key" : addedTerm,
			"rawWeight" : rawWeight,
			"required" : "false",
			"pillId" : this.getPillId(component, addedTerm),
			"pillColorClass" : this.getPillColorClass(component, rawWeight)
		};

		return normalizedTerm;
	},

	fireMatchFacetChangedEvt : function(component) {
		var facetAppliedEvt = component.getEvent("matchFacetAppliedEvt");
        var excludedData = component.get("v.excludedData");
		facetAppliedEvt.setParams({
			"matchFacetType" : component.get("v.type"), 
			"matchFacetData" : this.reconstructRawData(component),
            "excludedData" : this.reconstructDataDefault(component,(excludedData==undefined?[]:excludedData))
		});
		facetAppliedEvt.fire();
	},

	getPillColorClass : function(component, rawWeight) {
        var classList = component.get("v.pillClassList");
		if (rawWeight === "") {
			//return "";

            return classList[1];
		}

		// If no class list provided, return the raw weight as is.
		if (classList.length === 0) {
			return rawWeight;
		}

		// e.g. If list is 2 classes long, scaleSplit = 0.5
		var scaleSplit = 1 / classList.length;

		// For loop will terminate and return the corresponding class when the raw weight matches the criteria.
		for (var i=0; i<classList.length; i++) {
			// 0.01 added to allow for floating point division accuracy to accommodate weights of 1.0
			// e.g. for 3 classes => scaleSplit = 0.3333.. => 0.3333 * 3 = 0.9999...
			if (rawWeight < (scaleSplit * (i + 1) + 0.01) ) {
				return classList[i];
			}
		}
	},

	redrawPills : function(component) {
		// New pillsArray - overwrite the entire set every time.
		var pillsArray = [];
		var essPills = [];
        var excludedObjects = [];
        var nonExcludedObjects = [];
        var enableExcludeFeature = component.get("v.enableExcludeFeature");
		var pillData = component.get("v.normalizedData");
		component.set("v.pillsList", pillsArray);
		component.set("v.essentialPillList", essPills);
		if (pillData.length === 0) {
			return;
		}

		var isSliced = false;
		var pillsToShow = 5;
		component.set("v.isPillHidden", pillData.length > pillsToShow);

		if (pillData.length > pillsToShow && component.get("v.showMoreLess") === $A.get("$Label.c.ATS_SHOW_MORE")){
			isSliced = true;
		} else if (pillData.length < pillsToShow || component.get("v.showMoreLess") === $A.get("$Label.c.ATS_SHOW_LESS")){
			pillsToShow = pillData.length;
		}

		var related = component.get("v.relatedTerms");
        var j = 0;
		for (; j<pillData.length; j++) {
			var isRequired = pillData[j].required === "true" || pillData[j].required === true;
            var isPillExcluded = false;
            if(pillData[j].isExcluded != undefined){
                isPillExcluded = pillData[j].isExcluded;
            }

			// enableClick = true if this term has related terms.
			var relTerms = related ? related[pillData[j].key] : [];
			
			var cmpParams = {
				"pillLabel" : pillData[j].key,
				"pillId" : pillData[j].pillId,
				"pillColorClass" : pillData[j].pillColorClass,
				"showFavorite" : true,
				"isFavorite" : isRequired,
				"enableClick" : (relTerms || []).length > 0,
                "enableExcludeAction" : enableExcludeFeature,
                "isExcluded" : isPillExcluded,
                "eventHandlerId" : component.get('v.type')=="jobtitles"?"MatchFacetJobTitles_1":"MatchFacetSkills_1",
				"relatedTerms" : component.getReference("v.relatedTerms"),
                "facetType": component.getReference("v.type"),
                "scrolledValue": component.getReference("v.scrolledValue")
			};
            pillData[j].isExcluded ? excludedObjects.push(pillData[j]) : nonExcludedObjects.push(pillData[j]);
			$A.createComponent("c:C_SearchFacetPill", 
				cmpParams, 
				function(newComponent, status, errorMessage) {
	                if (status === "SUCCESS") {
	                    // SUCCESS  - insert component in markup
	                    if (isRequired || isPillExcluded) {
		                    essPills.push(newComponent);
		                    component.set("v.essentialPillList", essPills);
	                    } else {
		                    pillsArray.push(newComponent);
		                    component.set("v.pillsList", pillsArray);
	                    }
	                } else if (status === "INCOMPLETE") {
	                    console.log("No response from server or client is offline.");
	                    // Show offline error
	                } else if (status === "ERROR") {
	                    console.log("Error: " + errorMessage);
	                    // Show error message
	                }                    
	        });

			if (isSliced && (j === pillData.length - 1)) {
				component.set("v.pillsList", pillsArray.slice(0, pillsToShow - essPills.length));
			}
		}
        // Post search or pactions needs to draw the pills and seperate excluded pills data.
        var excludedData = component.get("v.excludedData");
        if(j<(pillData.length-1)){
            for(var pc=j;pc<pillData.length;pc++){
                pillData[pc].isExcluded ? excludedObjects.push(pillData[pc]) : nonExcludedObjects.push(pillData[pc]);
            }
        }
        component.set("v.normalizedData",nonExcludedObjects);
        if((excludedData == undefined || excludedData.length==0) && excludedObjects.length>0){
            component.set("v.excludedData",excludedObjects);
        }
	},

	updatePillsRelatedTerms : function(component) {
		var related = component.get("v.relatedTerms");

		var essPills = component.get("v.essentialPillList");
		var othPills = component.get("v.pillsList");

		for (var i=0; i<othPills.length; i++) {
			var relTerms = related ? related[othPills[i].get("v.pillLabel")] : [];
			othPills[i].set("v.enableClick", (relTerms != undefined && relTerms.length) > 0);
		}
		for (var i=0; i<essPills.length; i++) {
			var relTerms = related ? related[essPills[i].get("v.pillLabel")] : [];
			essPills[i].set("v.enableClick", (relTerms != undefined && relTerms.length) > 0);
		}
	},

	togglePills: function(component){
		var showMoreLessLabel = component.get("v.showMoreLess");

		if (showMoreLessLabel === $A.get("$Label.c.ATS_SHOW_MORE")){
         	component.set("v.showMoreLess", $A.get("$Label.c.ATS_SHOW_LESS"));
		}else {
			if (showMoreLessLabel === $A.get("$Label.c.ATS_SHOW_LESS")){
         	component.set("v.showMoreLess", $A.get("$Label.c.ATS_SHOW_MORE"));
		}
		}
        // Add excluded pills to normalized data to redraw
        if(component.get("v.enableExcludeFeature")){
            var normalizedData = component.get("v.normalizedData");
            if(normalizedData==undefined){
                normalizedData = [];
            }
            var excludedData = component.get("v.excludedData");
            if(excludedData != undefined && excludedData.length > 0){
                normalizedData = excludedData.concat(normalizedData);//normalizedData.concat(excludedData);
            }
            component.set("v.normalizedData", normalizedData);
        }
		this.redrawPills(component);
	},

	favoritePill : function(component, evtParams) {
		var normalizedData = component.get("v.normalizedData");
		var newState = [];

		if (normalizedData !== undefined && normalizedData !== null) {
			for (var i=0; i<normalizedData.length; i++) {
				if (normalizedData[i].pillId === evtParams.pillId) {
					normalizedData[i].required = "true";
					break;
				} 
			}
		}

        var excludedData = component.get("v.excludedData");
        if(excludedData != undefined && excludedData.length > 0){
            normalizedData = excludedData.concat(normalizedData);//normalizedData.concat(excludedData);
        }
		component.set("v.normalizedData", normalizedData);
	},

	unfavoritePill : function(component, evtParams) {
		var normalizedData = component.get("v.normalizedData");
		var newState = [];

		if (normalizedData !== undefined && normalizedData !== null) {
			for (var i=0; i<normalizedData.length; i++) {
				if (normalizedData[i].pillId === evtParams.pillId) {
					normalizedData[i].required = "false";
					break;
				} 
			}
		}

        var excludedData = component.get("v.excludedData");
        if(excludedData != undefined && excludedData.length > 0){
            normalizedData = excludedData.concat(normalizedData);//normalizedData.concat(excludedData);
        }

		component.set("v.normalizedData", normalizedData);
	},

	fetchRelatedTerms : function(component) {
		var dt = component.get("v.normalizedData");		
		var fType = component.get("v.type");
		//S-178553
		let opcoDomain = component.get("v.opcoDomain");
		if(opcoDomain != undefined) {
			if(dt){
			var terms = [];
			for (var i=0; i<dt.length; i++) {
				terms.push(dt[i].key);
			}

			// Join all terms on this facet and call search for related terms.
			var action = component.get("c.fetchRelatedTerms");
			action.setParams({
        		"terms" : terms.join(),
				"facetType" : fType,
				"domain" : opcoDomain
			});
			//console.log('terms'+terms.join());
			action.setCallback(this, function(response) {
				if (response.getState() === "SUCCESS") {
            		if (response.getReturnValue() !== "") {
						component.set("v.relatedTerms", JSON.parse(response.getReturnValue()));
						//console.log('fetchRelatedTerms:::Response:::'+JSON.parse(response.getReturnValue()));
            		}
				} 
			});
			$A.enqueueAction(action);
			}
		}
	},
    handlePillExcludeAction : function(component,event) {
        var exPillId = event.getParam("pillId");
        var isExcluded = event.getParam("isExcluded");

        var normalizedData = component.get("v.normalizedData");
        var excludedData = component.get("v.excludedData");

        var tempNormalizedData = [];
        var tempExcludedData = [];

        if(isExcluded){
           // remove from normalize data and add to excludedData
           for(var pc=0;pc<normalizedData.length;pc++){
                var pill = normalizedData[pc];
                if(pill.pillId == exPillId){
                    pill.isExcluded = true;
                    tempExcludedData.push(pill);
                }else{
                    pill.isExcluded = false;
                    tempNormalizedData.push(pill);
                }
           }


           if(excludedData == undefined){
            excludedData = [];
           }
           excludedData = excludedData.concat(tempExcludedData);

           tempNormalizedData = excludedData.concat(tempNormalizedData);//tempNormalizedData.concat(excludedData);
           component.set("v.normalizedData",tempNormalizedData);

           component.set("v.excludedData",excludedData);
        }else{
            // remove from excludedData data and add to normalize
            for(var pc=0;pc<excludedData.length;pc++){
                var pill = excludedData[pc];
                if(pill.pillId == exPillId){
                    pill.isExcluded = false;
                    tempNormalizedData.push(pill);
                }else{
                    pill.isExcluded = true;
                    tempExcludedData.push(pill);
                }
           }
           component.set("v.excludedData",tempExcludedData);
           if(normalizedData == undefined){
            normalizedData = [];
           }
           normalizedData = normalizedData.concat(tempNormalizedData);
           normalizedData = tempExcludedData.concat(normalizedData);//normalizedData.concat(tempExcludedData);

           component.set("v.normalizedData",normalizedData);
        }
    },
    excludedPillFavorite : function(component, evtParams, isFav){

        var normalizedData = component.get("v.normalizedData");
        var excludedData = component.get("v.excludedData");
        var newState = [];

        if (excludedData !== undefined && excludedData !== null) {
            for (var i=0; i<excludedData.length; i++) {
                if (excludedData[i].pillId === evtParams.pillId) {
                    excludedData[i].required = isFav ? "true" : "false";
                    break;
                } 
            }
        }
        //console.log(" Excluded "+JSON.stringify(excludedData));

        component.set("v.excludedData",excludedData);
        if(normalizedData == undefined){
            normalizedData = [];
        }
        normalizedData = excludedData.concat(normalizedData);//normalizedData.concat(excludedData);
        component.set("v.normalizedData", normalizedData);
    }	
})