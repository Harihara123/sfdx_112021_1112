({
	/*renderMatchData : function(component, event, helper) {
		component.set("v.automatchSkills", event.getParam("autoMatchSkills"));
		component.set("v.automatchJobTitles", event.getParam("automatchTitles"));
		// component.set("v.explainHits", event.getParam("explainHits"));
	},*/

	updateMatchFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
            helper.updateExcludes(component,parameters);
            /*
			component.set("v.automatchSkills", parameters.automatchVectors.skills_vector);
			component.set("v.automatchJobTitles", parameters.automatchVectors.title_vector);
            component.set("v.jobTitlesExcludes", parameters.jobTitlesExcludes);
            component.set("v.skillsExcludes", parameters.skillsExcludes);
            //populate excludes in corresponding data tosend to rawData
            helper.updateExcludes(component);
            */
		}
	},

    killComponent : function(component, event, helper) {
        if (component.isValid()) {
            component.destroy();
        }
    },
    
    doInitOwnership : function(component, event, helper) {
        var action = component.get("c.getUserOpco");
       // action.setParams({});

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                if(result !== null){
                   component.set("v.Ownership",result); 
                }
            }
            else if (state === "INCOMPLETE") {
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // optionally set storable, abortable, background flag here

        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    },
	handleAutoMatchSourceUpdate : function(cmp,event,helper) {
		if(cmp.get("v.matchType")) {
			if(cmp.get("v.matchType") === "R2C") {
			
				let action = cmp.get("c.fetchOppOpcoDetails");

				action.setParam({"oppId":cmp.get("v.automatchSourceData.srcLinkId")});

				// Create a callback that is executed after 
				// the server-side action returns
				action.setCallback(this, function(response) {
				
					var state = response.getState();
					var result = response.getReturnValue();
					if (state === "SUCCESS") {
						if(result !== null){
						   //helper.getOpcoDetails(cmp,event, result.OpCo__c);
						   cmp.set("v.opcoDomain",result);
						}
					}
				});
				$A.enqueueAction(action);
			} else {
				console.log('runningUser:'+JSON.stringify(cmp.get("v.runningUser")));
				cmp.set("v.opcoDomain",cmp.get("v.runningUser.opcoDomain"));
			}	
		}
	}
})