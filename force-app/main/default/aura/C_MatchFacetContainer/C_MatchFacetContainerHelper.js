({
    updateExcludes:function(component,parameters){


        var jobTitlesExcludes = parameters.jobTitlesExcludes;//component.get("v.jobTitlesExcludes");
        var skillsExcludes =  parameters.skillsExcludes;//component.get("v.skillsExcludes");

        var automatchJobTitles = parameters.automatchVectors.title_vector;
        var automatchSkills = parameters.automatchVectors.skills_vector;

        if(jobTitlesExcludes != undefined && jobTitlesExcludes.length > 0){

            
            if(automatchJobTitles==undefined){
                automatchJobTitles = [];
            }
            var excludeStateObjects = [];

            for(var jt=0;jt<jobTitlesExcludes.length;jt++){
                excludeStateObjects.push({
                    "required" : "false",
                    "term" : jobTitlesExcludes[jt],
                    "weight" : 0,
                    "isExcluded" : true
                });               
            }
            if(excludeStateObjects.length > 0){
                excludeStateObjects = excludeStateObjects.concat(automatchJobTitles);
                component.set("v.automatchJobTitles",excludeStateObjects);
            }else{
                component.set("v.automatchJobTitles", automatchJobTitles);
            }
        }else{
            component.set("v.automatchJobTitles", automatchJobTitles);
        }


        /* update for skills*/
        if(skillsExcludes != undefined && skillsExcludes.length >0){
            
            if(automatchSkills==undefined){
                automatchSkills = [];
            }
            var excludeStateObjects = [];
            for(var sk=0; sk< skillsExcludes.length; sk++){
                excludeStateObjects.push({
                    "required" : "false",
                    "term" : skillsExcludes[sk],
                    "weight" : 0,
                    "isExcluded" : true
                });
            }
            if(excludeStateObjects.length > 0){
                excludeStateObjects = excludeStateObjects.concat(automatchSkills);
                component.set("v.automatchSkills",excludeStateObjects);
            }else{
                component.set("v.automatchSkills", automatchSkills);
            }
        }else{
            component.set("v.automatchSkills", automatchSkills);
        }
    }
})