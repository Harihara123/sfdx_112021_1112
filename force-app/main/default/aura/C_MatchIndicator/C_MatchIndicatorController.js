({
    doInit: function (cmp, e, h) {
        let record = cmp.get("v.resultsStrength"),
            img = h.setImageUrl(cmp, record),
            resourceUrl = $A.get("$Resource.ATS_MATCH_SCORE_INDICATOR"),
            imageUrl = `${resourceUrl}/Match-Score-Indicators${img.imgUrl}`;

        cmp.set("v.matchGraphUrl", imageUrl);
        cmp.set("v.altText", img.altText);

        const randomId = `match-indicator-${Math.floor((1 + Math.random()) * 0x100000).toString(16).substring(1)}`;
        cmp.set("v.randomId", randomId);

    },

    togglePopover: function (cmp, e, h) {
        let togglePopoverWrapper = document.getElementById(`match-popover-wrapper-${cmp.get("v.randomId")}`);
        let thisPopoverContainer = document.getElementById(cmp.get('v.randomId'));

        setTimeout(()=>{
            if(thisPopoverContainer){
                // if popover visible - align respectively to donut
                h.bottomAlign(cmp, e);

                const closeOnblur = (ev) => {
                    if (!thisPopoverContainer.contains(ev.target)) {

                        togglePopoverWrapper.classList.toggle('slds-hide');
                        document.querySelector("body").removeEventListener("mouseover", closeOnblur);
                    }
                    ev.stopPropagation();
                };
                document.querySelector("body").addEventListener("mouseover", closeOnblur);

                togglePopoverWrapper.classList.toggle("slds-hide");

            }
        }, 500);
        e.stopPropagation();

    }
})