({
    setImageUrl : function(cmp, strength) {
        const imageMap = [{
            matchStr: 0,
            imgUrl: '/0.svg',
            altText: '0%'
        },{
            matchStr: 1,
            imgUrl: '/25.svg',
            altText: '25%'
        },{
            matchStr: 2,
            imgUrl : '/50.svg',
            altText: '50%'
        },{
            matchStr: 3,
            imgUrl: '/75.svg',
            altText: '75%'
        },{
            matchStr: 4,
            imgUrl: '/100.svg',
            altText: '100%'
        }];


        // Set Selected Image
        let imageUrl = {
            imgUrl : imageMap[0].imgUrl,
            altText : imageMap[0].altText
        };

        for (let i = 0; i < imageMap.length; i++) {
            if(imageMap[i].matchStr === strength){
                imageUrl.imgUrl = imageMap[i].imgUrl;
                imageUrl.altText = imageMap[i].altText;
            }
        }


        return imageUrl;
    },
    bottomAlign: function(component) {
        setTimeout(() => {
            let node = document.getElementById(`match-insights-popover-${component.get("v.randomId")}`);
            const cardIndex = component.get("v.cardIndex");

            if (node) {
                let popoverHeight = node.clientHeight + 8;

                let bottom;

                if (cardIndex !== 0) {
                    bottom = 24;
                } else {
                    component.set("v.togglePos", true);

                    bottom = -(popoverHeight);

                }
                component.set("v.bottomAlign", bottom);

            }
        }, 0);
    }
})