({
	formatHeaderText : function(component, event, helper) {
		const headerText = component.get("v.matchType");
		const regex = /\|/g;
		let srcName = component.get("v.automatchSourceData.srcName");

		if(headerText === 'R2C'){
			let delimiter = '|';

			if(!srcName.match(regex)){
				delimiter = '-';
			}

			let newName = helper.formatText(component, srcName, delimiter);
		
			component.set("v.reqId", newName.reqId);
			component.set("v.formattedName", newName.reqName);
			
		} 
	}
})