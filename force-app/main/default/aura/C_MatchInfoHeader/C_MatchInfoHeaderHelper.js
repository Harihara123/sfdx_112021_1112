({
	formatText : function(component, srcName, delimiter) {
		let newNameArr = srcName.split(delimiter),
			formattedName = {},
			newName = '',
			reqId = [];
				
		if(newNameArr != undefined){
			
			for (var i = 0; newNameArr.length > 0; i++) {
				if(i >= 2){
					break;
				} 
				reqId.push(newNameArr[i]);
			}

			reqId = reqId.join(delimiter);
			formattedName.reqId = reqId.trim();
			
			newNameArr.splice(0, 2);
			newName = newNameArr.join(delimiter);
			formattedName.reqName = newName.trim();

			return formattedName;
		}
		
	}
})