({
	updateMatchInsights : function(component, event, helper) {
		helper.updateMatchInsights(component);
		helper.showModal(component);
		/*console.log('Source ID ---> ' + component.get('v.automatchSourceData.Name'));
		console.log('Target ID ---> ' + component.get('v.automatchTargetName'));*/
	},

	closeModal: function(component, event, helper) {
		helper.closeModal(component);
    }
})