({	
	updateMatchInsights : function(component) {
		var rawInsights = component.get("v.matchInsights");
		var pillClassList = component.get("v.pillClassList");
		var intersectArray = [];

		var intersect = rawInsights.intersecting_terms;

		if (intersect !== undefined) {
			// var terms = Object.keys(intersect);
			for (var i=0; i<intersect.length; i++) {
				intersectArray.push({
					"term" : intersect[i].term,
					"matchWeight" : intersect[i].weight,
					"pillColorClass" : this.getPillColorClass(component, intersect[i].weight, intersect[i].required)
				});
			}
		}
		// console.log(intersect);
		// console.log(intersectArray);
		component.set("v.intersectingTerms", this.sortDescendingByWeight(intersectArray));
	},

	sortDescendingByWeight : function(data) {
		return data.sort(function(a, b) {
			return b.matchWeight - a.matchWeight;
		});
	},

	getPillColorClass : function(component, rawWeight, required) {
		if (rawWeight === "") {
			return "";
		}

		if (required) {
			return "essential-pill";
		}

		var classList = component.get("v.pillClassList");
		// If no class list provided, return the raw weight as is.
		if (classList.length === 0) {
			return rawWeight;
		}

		// e.g. If list is 2 classes long, scaleSplit = 0.5
		var scaleSplit = 1 / classList.length;

		// For loop will terminate and return the corresponding class when the raw weight matches the criteria.
		for (var i=0; i<classList.length; i++) {
			// 0.01 added to allow for floating point division accuracy to accommodate weights of 1.0
			// e.g. for 3 classes => scaleSplit = 0.3333.. => 0.3333 * 3 = 0.9999...
			if (rawWeight < (scaleSplit * (i + 1) + 0.01) ) {
				return classList[i];
			}
		}
	},

    showModal : function (component, event, helper) {
        this.toggleClass(component,'backdropMatchInsights','slds-backdrop--');
        this.toggleClass(component,'modaldialogMatchInsights','slds-fade-in-');
    },

	closeModal: function(component, event, helper) {
		this.toggleClassInverse(component,'backdropMatchInsights','slds-backdrop--');
		this.toggleClassInverse(component,'modaldialogMatchInsights','slds-fade-in-');
    },

    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    }
})