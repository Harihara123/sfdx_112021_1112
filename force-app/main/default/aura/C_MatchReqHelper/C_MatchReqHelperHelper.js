({
	getActionWithParameters: function (helperCmp, criteriaCmp, queryStringUrl) {
		var action = criteriaCmp.get(helperCmp.get("v.actionName"));
		var location = criteriaCmp.get("v.usePresetLocation") ? criteriaCmp.get("v.autoMatchLocation") : null;

        if (helperCmp.get("v.httpRequestType") === "GET") {
	        action.setParams({
                "queryString" : queryStringUrl,
                "httpRequestType" : "GET", 
                "requestBody" : null, 
                "location" : null
            });
        } else {
			var autoMatchRequestBody = JSON.stringify(this.getAutoMatchRequestBody(criteriaCmp));
            action.setParams({
                "queryString" : queryStringUrl,
                "httpRequestType" : "POST",
				"requestBody" : autoMatchRequestBody,
                "location" : null
            });
        }

		return action;
	},
	
	getMatchInsightActionWithParameters: function (helperCmp, criteriaCmp, queryStringUrl) {
		var action = criteriaCmp.get(helperCmp.get("v.actionNameMatchInsight"));
		var location = criteriaCmp.get("v.usePresetLocation") ? criteriaCmp.get("v.autoMatchLocation") : null;

        if (helperCmp.get("v.httpRequestType") === "GET") {
	        action.setParams({
                "queryString" : queryStringUrl,
                "httpRequestType" : "GET", 
                "requestBody" : null, 
                "location" : null
            });
        } else {
			var autoMatchRequestBody = JSON.stringify(this.getAutoMatchRequestBody(criteriaCmp));
            action.setParams({
                "queryString" : queryStringUrl,
                "httpRequestType" : "POST",
				"requestBody" : autoMatchRequestBody,
                "location" : null
            });
        }

		return action;
	},

	getBaseParameters: function (helperCmp, criteriaCmp) {
		var	parameters = {
				"size": criteriaCmp.get("v.pageSize"), 
			    "from": criteriaCmp.get("v.offset"), 
			   "type": "talent2job",
			   "docId": criteriaCmp.get("v.automatchSourceId"),
			   "userId": criteriaCmp.get("v.runningUser.id"),
			   "flt.group_filter": helperCmp.get("v.groupFilterOverride") ? 
                                        helperCmp.get("v.groupFilterOverride") : criteriaCmp.get("v.runningUser.opcoSearchFilter")
				
			};

      // if (helperCmp.get("v.httpRequestType") === "GET") {
         //   parameters.docId = criteriaCmp.get("v.automatchSourceId");
      //  }

		return parameters;

	},

	getMatchInsightParameters : function (helperCmp, criteriaCmp) {
		var parameters = {
				//"size": criteriaCmp.get("v.pageSize"), 
			    //"from": criteriaCmp.get("v.offset"), 
				"type": "talent2job",
				"docId": criteriaCmp.get("v.automatchSourceId"),
			    "flt.group_filter": helperCmp.get("v.groupFilterOverride") ? 
                                        helperCmp.get("v.groupFilterOverride") : criteriaCmp.get("v.runningUser.opcoSearchFilter"),
				"insight": "full",
				"flt.job_id":criteriaCmp.get("v.filterCandidateOrJobId")
			 };

		//if (helperCmp.get("v.httpRequestType") === "GET") {
         //   parameters.docId = criteriaCmp.get("v.automatchSourceId");
       // }

		return parameters;

	},

    getAutoMatchRequestBody : function (criteriaCmp) {
        // var autoMatchRequestBody = {};
        var autoMatchRequestBody = { 
            match_info: criteriaCmp.get ("v.matchVectors")
        };
        //Append exlude keywords
        var excludeKeyWords = criteriaCmp.get ("v.excludeKeyWords");
        if(excludeKeyWords != undefined){
            autoMatchRequestBody.match_info.exclude = excludeKeyWords;
        }
        // var automatch = {};
        /*var data = this.appendAutoMatchRequestData (criteriaCmp);
        automatch.data = data;
        autoMatchRequestBody.automatch = automatch*/
        return autoMatchRequestBody;
    },

    /*appendAutoMatchRequestData : function (criteriaCmp) {
        var data = {};
        var autoMatchCriteria = {};

        //autoMatchCriteria.type = "talent2job";
        //autoMatchCriteria.docId = criteriaCmp.get("v.automatchSourceId"); 
        //Append exlude keywords
        var excludeKeyWords = criteriaCmp.get ("v.excludeKeyWords");
        if(excludeKeyWords!=undefined && excludeKeyWords.length > 0){
            autoMatchCriteria.exclude = excludeKeyWords;
        }
        var matchVectors = criteriaCmp.get ("v.matchVectors");
        autoMatchCriteria.jobTitles = matchVectors.title_vector;
        autoMatchCriteria.skills = matchVectors.skills_vector;
        //S-34736 - To include Other_Vector in the request body Added by Karthik
        //autoMatchCriteria.keywords = matchVectors.other_vector;
		autoMatchCriteria.keywords = matchVectors.keyword_vector;
		autoMatchCriteria.acronyms = matchVectors.acronym_vector
		autoMatchCriteria.skillFamily = matchVectors.sf_vector;
		autoMatchCriteria.sf_domain = matchVectors.sf_domain_confidence.skillfamily_domain;
		autoMatchCriteria.sf_domain_confidence = matchVectors.sf_domain_confidence.skillfamily_domain_confidence;

       // data.userId = criteriaCmp.get("v.runningUser.id");
        data.autoMatchCriteria = autoMatchCriteria;
		return data;
    },*/

	createSearchLogEntry: function (helperCmp, criteriaCmp, baseEntry) {
        baseEntry.criteria.multilocation = criteriaCmp.get("v.multilocation");

        baseEntry.criteria.facets = criteriaCmp.get("v.facets");
        baseEntry.criteria.filterCriteriaParams = criteriaCmp.get("v.filterCriteriaParams");
		baseEntry.fdp = criteriaCmp.get("v.facetDisplayProps"); 

        var match = {
            "data" : criteriaCmp.get("v.automatchSourceData"),
            "srcid" : criteriaCmp.get("v.automatchSourceId"),
            // "type" : "talent2job",
            "loc" : criteriaCmp.get("v.autoMatchLocation")
        };
        baseEntry.match = match;

        baseEntry.httpVerb = helperCmp.get("v.httpRequestType");
        baseEntry.matchVectors = criteriaCmp.get("v.matchVectors");

        //Append exlude keywords
        var excludeKeyWords = criteriaCmp.get ("v.excludeKeyWords");
        if(excludeKeyWords!=undefined && excludeKeyWords.length > 0){
            baseEntry.exclude = excludeKeyWords;
        }

        return JSON.stringify(baseEntry);
	},

	restoreCriteriaFromLog: function (helperCmp, criteriaCmp) {
        var logEntry = criteriaCmp.get("v.lastSearchLog");
        if (logEntry !== undefined && logEntry !== null) {
            var searchParams = JSON.parse(logEntry);
            if (searchParams) {
                criteriaCmp.set("v.multilocation", searchParams.criteria.multilocation);

                criteriaCmp.set("v.facets", searchParams.criteria.facets);
                criteriaCmp.set("v.filterCriteriaParams", searchParams.criteria.filterCriteriaParams);
                criteriaCmp.set("v.offset", searchParams.offset);
                criteriaCmp.set("v.pageSize", searchParams.pageSize);
                if (searchParams.sort) {
					criteriaCmp.set("v.sortField", searchParams.sort);
					criteriaCmp.set("v.sortOrder", searchParams.order);
				}
                criteriaCmp.set("v.automatchSourceData", searchParams.match.data);
                criteriaCmp.set("v.automatchSourceId", searchParams.match.srcid);
                // criteriaCmp.set("v.automatchType", searchParams.match.type);
                helperCmp.set("v.httpRequestType", searchParams.httpVerb);
                criteriaCmp.set("v.matchVectors", this.matchVectorsToV2(searchParams.matchVectors));
				criteriaCmp.set("v.facetDisplayProps", searchParams.fdp);

				// type undefined check required for pre-refactoring saved searches. 
		        criteriaCmp.set("v.type", searchParams.type ? searchParams.type : "C2R"); 

                //Append exlude keywords
                var excludeKeyWords = searchParams.exclude;
                if(excludeKeyWords!=undefined && excludeKeyWords.length > 0){
                    criteriaCmp.set("v.excludeKeyWords", this.excludesToV2(excludeKeyWords));
                }
				criteriaCmp.set("v.accountId",searchParams.match.srcid);
            }
        }
	},
    
    /** toV2 functions translate older saved search logs to work with QPL changes made for LTR release */
    matchVectorsToV2: function (mv) {
        // return (mv && mv.skillfamily_domain) ?
        return (mv && mv.sf_domain_confidence) ?
            {
                acronym_vector: mv.acronym_vector,
                keyword_vector: mv.keyword_vector,
                skillfamily_vector: mv.sf_vector,
                skills_vector: mv.skills_vector,
                title_vector: mv.title_vector,
                skillfamily_domain: mv.sf_domain_confidence.skillfamily_domain,
                skillfamily_domain_confidence: mv.sf_domain_confidence.skillfamily_domain_confidence
            } : 
            mv;
    },

    excludesToV2: function(excludes) {
        excludes.forEach(e => {
            if (e.target === "jobTitles") {
                e.target = "title_vector";
            } else if (e.target === "skills") {
                e.target = "skills_vector";
            }
        });
        return excludes;
    }

})