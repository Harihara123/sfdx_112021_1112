({
	reqValueChangeHandler: function (component, event, helper) {
		
		helper.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")
			+ "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + event.getParam("key")
			+ "&c__srcName=" + encodeURIComponent(event.getParam("value"))
			+ "&c__srcLoc=" + helper.translateLocation(component, event.getParam("addlValues"))
            + "&c__noPush=true"
		);
	},

	doInit : function(component,event,helper) {
		helper.addRemoveEventListener(component, event,'popOverId', 'matchReqPopOverCls', 'matchReqPopOver');
	},
})