({
	sendToURL : function(url) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	},
	 
	translateLocation: function (component, item) {
        var company = component.get("v.runningUser.CompanyName");

        if (company && company === 'AG_EMEA') {
			item.location = this.buildCommaSeparatedLocation(item.Account_City__c, item.Account_Country__c);
        } else {
			item.location = this.buildCommaSeparatedLocation(item.Account_City__c, item.Account_State__c);
        }

    },

	buildCommaSeparatedLocation: function(l1, l2) {
		var l = (l1 && l1 !== "") ? l1 : "";
		return (l2 && l2 !== "") ? (l !== "" ? l + ", " + l2 : l2) : l;
	},

	addRemoveEventListener : function(component, event, elementId, headerClass, subClass) {
			$A.util.addClass(event.target, subClass);
			var clickedFn;
			var myPopover = component.find(elementId);

			clickedFn = $A.getCallback(function(event){
				var popoverEle = myPopover.getElement();
				$A.util.removeClass(popoverEle, 'slds-hide');
				
				
				if(popoverEle && event.target) {
					if (!event.target.className.includes(headerClass) && !popoverEle.contains(event.target)) { 
						$A.util.addClass(popoverEle, 'slds-hide');
						//$A.util.addClass(event.target, subClass);	
						var myExternalEvent = window.$A.get("e.c:E_HideMatchReqLookupPopOver");							
						myExternalEvent.fire();
						window.removeEventListener('click',clickedFn);
					}
				} 
				else {	
					window.removeEventListener('click',clickedFn);
					$A.util.addClass(event.target, subClass);
				}
			});

			window.addEventListener('click',clickedFn);
	}

})