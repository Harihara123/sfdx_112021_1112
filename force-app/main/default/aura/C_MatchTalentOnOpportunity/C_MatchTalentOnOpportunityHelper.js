({
	getRecordDetails : function(component){
		var action = component.get("c.getOpptyRecord");
		action.setParams({
				recordId : component.get("v.recordId")				
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                response = response.getReturnValue();
				response.map((oppty) => {
					this.translateLocation(component, oppty);
				});
				component.set("v.record", response);
				const sourceIdTrackingEvt = $A.get("e.c:E_SearchResultsHandler");
				sourceIdTrackingEvt.setParam('sourceId', component.get("v.record")[0].Id);
				sourceIdTrackingEvt.fire();
			}	
			var rec = component.get("v.record");

			this.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")
			+ "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + rec[0].Id 
			+ "&c__srcName=" + encodeURIComponent(rec[0].Name)
			+ "&c__srcLoc=" + rec[0].location
            + "&c__noPush=true&c__srcTitle="+rec[0].Req_Job_Title__c
			+ "&c__srcLat="+rec[0].Req_GeoLocation__Latitude__s+"&c__srcLong="+rec[0].Req_GeoLocation__Longitude__s
			);
		});
        $A.enqueueAction(action);
	},

	getCurrentUser: function (component){
		var action = component.get("c.getRunningUser");
		action.setCallback(this, function(response){
			 var state = response.getState();
             if (state === "SUCCESS") {
				 var r = JSON.parse(response.getReturnValue());
                component.set ("v.runningUser", r);
			 }
			 // Irrespective of success, make the call for card data.
			 this.getRecordDetails(component);
		});
		$A.enqueueAction(action);
	},
	

	translateLocation: function (component, item) {
        item.location = this.normalizedLocation(item.Req_Worksite_City__c, item.Req_Worksite_State__c, item.Req_Worksite_Country__c);
    /**    var company = component.get("v.runningUser.CompanyName");

        if (company && company === 'AG_EMEA') {
			item.location = this.buildCommaSeparatedLocation(item.Req_Worksite_City__c, item.Req_Worksite_Country__c);
        } else {
			item.location = this.buildCommaSeparatedLocation(item.Req_Worksite_City__c, item.Req_Worksite_State__c);
			
        }
	**/
    },

	/** buildCommaSeparatedLocation: function(l1, l2) {
		var l = (l1 && l1 !== "") ? l1 : "";
		return (l2 && l2 !== "") ? (l !== "" ? l + ", " + l2 : l2) : l;
	}, **/
    normalizedLocation: function(city, state, country) {
        if (city && city.length) {
            city.trim();
        }
        if (state && state.length) {
            state.trim();
        }
        if (country && country.length) {
            country.trim();
        }        
      let arr = [city, state, country];
      let validLocations = []
      for (let i = 0; i < arr.length; i++) {
        if (arr[i]) {
          validLocations.push(arr[i])
        }
      }
      let location = validLocations.join(", ");
  
  	return location;
    },                    
	
	sendToURL : function(url) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	}
})