({
	headerLoaded : function(cmp, event, helper){ 
        var params = null; 
        var recordId = cmp.get("v.recordId") ;
        var usrOwnership = event.getParam("usrOwnership");
        params = {recordId: recordId, usrOwnership: usrOwnership};
        var cmps = [//ak'C_TalentSummaryModal',
					//'C_TalentActivityCloseTaskModal',
                    //'C_TalentActivityCloseEventModal',//ak 'C_TalentActivityAddEditModal',
                    //ak'C_TalentEngagementRuleModal', 'C_TalentEmploymentModal',
                    //ak'C_TalentDocumentModal',
					//ak 'C_MassAddToContactListModal', 
					//ak'C_TalentReqSearchModal'
					];

       // if (!cmp.get("v.byPassActivityModalAttr")) {
            for(var i=0;i<cmps.length;i++){
                var cmpEvent = cmp.getEvent("createCmpEventModal");
                cmpEvent.setParams({
                        "componentName" : 'c:' + cmps[i]
                        , "params":params
                        ,"targetAttributeName": 'v.' + cmps[i] });
                   cmpEvent.fire();
            }
       // }  
       // cmp.set("v.byPassActivityModalAttr", false);  
    }, 
	updateDisposition : function (cmp, helper) {
        setTimeout(function() {
			var parsedUrl = new URL(window.location.href);
		    var evtId = parsedUrl.searchParams.get("dispId");
			if(!$A.util.isUndefinedOrNull(evtId)){
				var actionParams = {'evtId':evtId,'Code':'12375','Reason':''};
				var bdata = cmp.find("bdhelper");
                bdata.callServer(cmp,'ATS','TalentOrderFunctions','updateDisposition',function(response){},actionParams,false);
			}
            
		}, 2000);
		
			
    }, 
   /* byPassActivityModal : function (component, helper) {
        component.set("v.byPassActivityModalAttr", true);
    },  */


	/*headerLoaded : function(cmp, event, helper){
        var params = null;
        var recordId = cmp.get("v.recordId") ;
        params = {recordId: recordId };
        var cmps = ['C_TalentSummaryModal','C_TalentActivityCloseTaskModal',
                    'C_TalentActivityCloseEventModal', 'C_TalentActivityAddEditModal',
                    'C_TalentEngagementRuleModal', 'C_TalentEmploymentModal',
                    'C_MassAddToContactListModal',
                    'C_TalentDocumentModal', 'C_TalentReqSearchModal'];
        for(var i=0;i<cmps.length;i++){
            var cmpEvent = cmp.getEvent("createCmpEventModal");
            cmpEvent.setParams({
                    "componentName" : 'c:' + cmps[i]
                    , "params":params
                    ,"targetAttributeName": 'v.' + cmps[i] });
               cmpEvent.fire();
        }
    },*/ 
    handleCreateEvent : function(cmp, event, helper){
        var componentName = event.getParam("componentName");
        var targetAttributeName = event.getParam("targetAttributeName");
        var params = event.getParam("params");
        helper.createComponent(cmp, componentName, targetAttributeName,params);
    },
	showAddEmpModal : function(component, event, helper) {
        component.set('v.modalTitle','Add Employment');
		component.set('v.modalcmp','C_TalentEmploymentModal');
		var params = {"talentId":component.get("v.talentId"),"modalTitle":component.get("v.modalTitle")}; 
        helper.createComponent(component, 'c:C_TalentEmploymentModal', 'v.C_TalentEmploymentModal',params);
    },
	showEditEmpModal : function(component, event, helper) {
       component.set('v.modalTitle','Edit Employment');
	   component.set('v.modalcmp','C_TalentEmploymentModal');
	   var params = {"talentId":component.get("v.talentId"),"modalTitle":component.get("v.modalTitle"),
															  "itemID":event.getParam("itemID")};
       helper.createComponent(component,'c:C_TalentEmploymentModal', 'v.C_TalentEmploymentModal',params);
    },
    showCallSheetModal : function(component, event, helper) { 
        component.set('v.modalTitle', 'Add to Call Sheet');
        component.set('v.modalcmp', 'C_AddToCallSheetModal');
        var contactList = [];
        contactList.push(component.get("v.contactRecord"));

        var params = {"contacts":contactList};

        helper.createComponent(component, 'c:C_AddToCallSheetModal', 'v.C_AddToCallSheetModal', params);
    },
	showTalentEditModal : function(component, event, helper) {
       //S-98221 - added by Natalie Lockett 8/23
	   if (component.get("v.recordId") === event.getParam("recId")) {
		   var params;
		   var contactName = component.get("v.contactRecord");
		   contactName = contactName.Name;
		   component.set('v.modalTitle', contactName);
		   component.set('v.modalcmp','C_TalentSummaryModal');
		   
		   //Rajeesh Notes parser - lab experiment 1/23/2019
		   if(event.getParam("source")=="NotesParser"){
				var parsedInfo = event.getParam("parsedInfo");
				params = {"talentId":component.get("v.talentId"),"modalTitle":component.get("v.modalTitle"),
																	"contactId":component.get("v.recordId"),
																	"source":event.getParam("source"),
																	"accountId":component.get("v.talentId")};
		   }
		   else {
				params = {"talentId":component.get("v.talentId"),"modalTitle":component.get("v.modalTitle"),
																	"contactId":component.get("v.recordId"),
																	"source":event.getParam("source"),
																	"accountId":component.get("v.talentId")};
			}
			helper.createComponent(component,'c:C_TalentSummaryModal', 'v.C_TalentSummaryModal',params);
		}
    },
	displayAddTaskDetail : function(component, event, helper) {
        var params = {"recordId":component.get("v.talentId"),"activityType":event.getParam("activityType")}; 
        helper.createComponent(component, 'c:C_TalentActivityAddEditModal', 'v.C_TalentActivityAddEditModal',params);
    },
	displayEditTaskDetail : function(component, event, helper) {
        //component.set('v.modalTitle','Add Task');
		var params = {"recordId":component.get("v.talentId"),"itemID":event.getParam("itemID"),"activityType":event.getParam("itemType")}; 
        helper.createComponent(component, 'c:C_TalentActivityAddEditModal', 'v.C_TalentActivityAddEditModal',params);
    },
	displayCTDetail: function(component, event, helper) {
		var params = {"recordId":event.getParam("recordId"),"Status":event.getParam("Status"),"Description":event.getParam("Description")};
		helper.createComponent(component, 'c:C_TalentActivityCloseTaskModal', 'v.C_TalentActivityCloseTaskModal',params);
    },
	displayCEDetail: function(component, event, helper) {
		var params = {"recordId":event.getParam("recordId"),"Enddate":event.getParam("Enddate"),"PostNotes":event.getParam("PostNotes")};
		helper.createComponent(component, 'c:C_TalentActivityCloseEventModal', 'v.C_TalentActivityCloseEventModal',params);
    },
	displayContactListModal : function(component, event, helper) {
		var params = {"recordId":event.getParam("recordId")};
		helper.createComponent(component, 'c:C_MassAddToContactListModal', 'v.C_MassAddToContactListModal',params);
	},
	displayERDetail: function(component, event, helper) {
		var params = {"empId":event.getParam("recordId"),"Name":event.getParam("name"),"BillingAddress":event.getParam("street"),"BillingCity":event.getParam("city"),"BillingState":event.getParam("state"),"BillingCountry":event.getParam("country")};
		helper.createComponent(component, 'c:C_TalentEngagementRuleModal', 'v.C_TalentEngagementRuleModal',params);

         
    },
	showReqModal : function(component, event, helper) {
	var params = {"recordId":event.getParam("contactId"),"talentId":event.getParam("talentId"),"record":event.getParam("record"),"runningUserOPCO":event.getParam("category"), "reqOpco":event.getParam("reqOpco")};
		helper.createComponent(component, 'c:C_TalentReqSearchModal', 'v.C_TalentReqSearchModal',params);
		
	},
	showRelateToClientModal : function(component, event, helper) {
        var params = {"recordId" : event.getParam("recordId")};
        helper.createComponent(component, 'c:C_RelateToClientModal', 'v.C_RelateToClientModal', params);
    },
    
	callDocModal : function (component, event, helper) {
	    var isNotDocUpload;
		if($A.util.isEmpty(event.getParam("enableSave"))){
			isNotDocUpload=true;
		}
		else{
			isNotDocUpload = false;  
		}

		var params = {"recordId":event.getParam("talentId"),"isNotDocUpload":isNotDocUpload,"sourceOfEvent":event.getParam("source")};
		helper.createComponent(component, 'c:C_TalentDocumentModal', 'v.C_TalentDocumentModal',params);
    },

    openJobPostingModal : function(component, event, helper) {
        
        var params = {"recordId":event.getParam("contactId"),"talentId":event.getParam("talentId"),"contactRecordFields":event.getParam("record"),"runningUserOPCO":event.getParam("category")};
        helper.createComponent(component, 'c:C_SendJobPostingInviteModal', 'v.C_SendJobPostingInviteModal',params);
        
    },

    showCreateDocumentModal: function(component, event, helper) {
        
        helper.createComponent(component, 'c:C_CreateDocumentModal', 'v.C_CreateDocumentModal',{"contactId" : event.getParam("contactId"),"source":event.getParam("source")});
        
    },

	showTalentEditModalNew : function(component, event, helper) {
       //S-98221 - added by Natalie Lockett 8/23
	   if (component.get("v.recordId") === event.getParam("recId")) {
		   var params;
		   var contactName = component.get("v.contactRecord");
		   contactName = contactName.Name;
		   component.set('v.modalTitle', contactName);
			component.set('v.modalcmp','C_TalentSummaryModalNew');
			//Rajeesh Notes parser - lab experiment 1/23/2019
		   if(event.getParam("source")=="NotesParser"){
				var parsedInfo = event.getParam("parsedInfo");
				var transactionId = event.getParam("transactionId");
				var n2pduration = event.getParam("n2pduration");
				params = {"talentId":event.getParam("recordId"),parsedInfo:parsedInfo,"modalTitle":component.get("v.modalTitle"),
																	transactionId: transactionId,
																	n2pduration: n2pduration,
																	"contactId":component.get("v.recordId"),
																	"source":event.getParam("source"),
																	"accountId":component.get("v.contactRecord.AccountId")};
		   }
		   else {
				params = {"talentId":component.get("v.talentId"),"modalTitle":component.get("v.modalTitle"),
																	"contactId":component.get("v.recordId"),
																	"source":event.getParam("source"),
																	"accountId":component.get("v.talentId"),
																	"consentPrefInfo":event.getParam("consentPrefInfo")};
			}
			/*params = {"talentId":component.get("v.talentId"),"modalTitle":component.get("v.modalTitle"),
																	"contactId":component.get("v.recordId"),
																	"source":event.getParam("source"),
																	"accountId":component.get("v.talentId")};*/
			helper.createComponent(component,'c:C_TalentSummaryModalNew', 'v.C_TalentSummaryModalNew',params);
		}
    },

        

})