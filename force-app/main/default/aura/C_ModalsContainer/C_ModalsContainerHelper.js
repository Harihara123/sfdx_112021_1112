({
	createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
			componentName,
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					cmp.set(targetAttributeName, newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			}
		);
		
    },
	handleShowModal: function(component, evt,params) {
        var modalBody;
        $A.createComponent("c:"+component.get('v.modalcmp'),params ,
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   component.find('overlayLib').showCustomModal({
                       body: modalBody, 
                       showCloseButton: true,
                       cssClass: "mymodal",
                       closeCallback: function() {
					       component.set("v.talentId",null);
						   component.set("v.modalTitle",null)
                           console.log(' modal closed!!!');
                       }
                   })
               }                               
           });
    }
})