({
	doInit : function (component, event, helper) {
		helper.initMyActivities(component);
	},
	handleEdit: function (component, event, helper) {

	},
	handelKeyWordFromLWC: function(component, event, helper) {		
		component.set("v.keyFilterTableData",event.getParam('tableData'))
		helper.keyWordFromLWC(component);
	},
	handleRowAction : function(component, event, helper) {
		let selectedRow = event.getParam('rowDetails');

		helper.handleRowAction(component, selectedRow);
		
	},
	handelFireEvent : function(component, event, helper) {
		console.log('tableData Details Aura controller  filter'+JSON.stringify(event.getParam('tableData')));
	},
	handelValueFromLwc : function(component, event, helper) {
		let rangeFilterValue = event.getParam('rangeFilterValue');
		let stDate = event.getParam('startDate');
		let edDate = event.getParam('endDate');
		let sortableField = event.getParam('sortableField');
		let sortOrder = event.getParam('sortOrder');

		let filtrValues = event.getParam('picklistfilter');
		let selectedUserPref = event.getParam('selectedUserPref');
		let actionType = event.getParam('actionType');

		component.set("v.rangeFilterValue",rangeFilterValue);
		component.set("v.startDate",stDate);
		component.set("v.endDate",edDate);
		component.set("v.sortableField", sortableField);
		component.set("v.sortOrder", sortOrder);
		component.set("v.picklistFilterParams",filtrValues);
		component.set("v.userPreferences", selectedUserPref);

		if(actionType === 'USER_PREF') {
			helper.saveUserPrefFilter(component);
		}
		else {
			if(selectedUserPref === 'NO_UESER_PREF') {
				helper.loadTableUpdated(component, event);
			}
			else {
				helper.initMyActivities(component);
			}
		}
	},
	handleActivityEvents : function(component, event, helper) {
		let actionType = event.getParam("actionType");

		if(actionType === 'SAVE_NEW_TASK' || actionType === 'SAVE_NEW_EVENT') {
			helper.openAddEventOrTaskModal(component, event)
		}
		let picklistFilterParams = component.get('v.picklistFilterParams');
		let startDate = component.get('v.startDate');
		let endDate = component.get('v.endDate');
		if(picklistFilterParams.length > 0) {
			helper.loadFilterData(component,picklistFilterParams, startDate,endDate);
		} else { // when no filter selected
			helper.initMyActivities(component);
		}
	},
	handleMassAction : function(component, event, helper) {		
		let actionType = event.getParam("actionType");		
		if(actionType === 'MassDelete') {
			helper.massDelete(component, event);
		}
		else {
			let taskIdList = event.getParam("taskIdList");
			helper.massUpdate(component, taskIdList);
		}
	},
	handleMassActionEvent  : function(component, event, helper) {  
		component.set("v.resetCheckbox", true);
		let picklistFilterParams = component.get('v.picklistFilterParams');
		let startDate = component.get('v.startDate');
		let endDate = component.get('v.endDate');
		if(picklistFilterParams.length > 0) {
			helper.loadFilterData(component,picklistFilterParams, startDate,endDate);
		} else { // when no filter selected
			helper.initMyActivities(component);
		}
	}
})