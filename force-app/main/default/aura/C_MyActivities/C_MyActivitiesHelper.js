({
	initMyActivities : function(cmp) {
		cmp.set('v.setSpinner',true);
		
		let userPref = cmp.get("v.userPreferences");

		let action = cmp.get("c.getMyActivitiesInit");
		action.setParams({"userPreferences":userPref, "sortableField":cmp.get("v.sortableField"), "sortOrder":cmp.get("v.sortOrder")});
		action.setCallback(this, function(response){
			cmp.set("v.tableData", "");
			let self = this;
			setTimeout( function(){
				let state = response.getState();
				if(state === 'SUCCESS') {
					let results = response.getReturnValue();
					//console.log('results:'+JSON.stringify(results));
					let tableData = results.myAcitvitiesWrapperList;
					let filterParams = results.filterList;
					cmp.set('v.initialFilterParams',filterParams);
					cmp.set('v.picklistFilterParams',filterParams);
					self.buildFormatedTableData(cmp, tableData);

					cmp.set("v.rangeFilterValue",results.dateRangeKey);
					cmp.set("v.startDate",results.startDate);
					cmp.set("v.endDate",results.endDate);
					cmp.set("v.sortableField", results.sortableField);
					cmp.set("v.sortOrder", results.sortOrder);
					cmp.set("v.userPreferences", results.userPrefOption);

					let runningUser = results.currentUserWithOwnership;
					cmp.set('v.runningUser',runningUser.usr);
					cmp.set('v.runningUserCategory', runningUser.userCategory);
					cmp.set('v.usrOwnership', runningUser.userOwnership);	
				}
			},0);
			cmp.set('v.setSpinner',false);
		});

		$A.enqueueAction(action);
	},

	getRelativeDate : function(days) {
		const today = new Date().getTime();
		const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        const ms = today + (days * 24 * 60 * 60 * 1000)
		//const isoString = new Date(ms).toISOString()
		const isoString = new Date(ms - tzoffset).toISOString();
        return isoString.split('T')[0];        
    },
		
	//use try-catch
	/*loadTableData : function (component) { //commented out because this method orphen - 03/23/30
		component.set('v.setSpinner',true);
		let action =component.get("c.getMyActivities");
		let startDate = this.getRelativeDate(-90);
		let endDate = this.getRelativeDate(30);
		action.setParams({ "startDate" :  startDate, "endDate" : endDate, "isFilter" : false});
		let self =this;
		action.setCallback(this, function(response) {
			component.set("v.tableData", "");
			setTimeout( function(){
				let state = response.getState();
				if(state === 'SUCCESS'){
					let dtResult = response.getReturnValue();
					self.buildFormatedTableData(component, dtResult);
				}
			 },0);
			 component.set('v.setSpinner',false);
		 });
		 $A.enqueueAction(action);
	},*/

	buildFormatedTableData : function(component, dtResult) {
		
		let actions = [{action: 'view', icon: 'utility:summarydetail'}];
		component.set("v.actions", actions);

		let activityMap = new Map();
		let tmpTableData = [];
		for(let i=0; i < dtResult.length; i++) {
			activityMap.set(dtResult[i].Id, dtResult[i]);
			let tmpTableRecord;
			 // No relatedTo link required for Submittal type(Inteviewing, Offer Accepted, Linked, Not Proceeding... )
			if(dtResult[i].eventCategory === 'Interviewing' || dtResult[i].eventCategory === 'Submittal') {
				tmpTableRecord = {
									id:dtResult[i].Id, 
									isMassEditable:dtResult[i].isMassEditable,
									isMassDeletable:dtResult[i].isMassDeletable,
									activityType:dtResult[i].activityType,
									cells : [{value:dtResult[i].dueDate, date: true},
											//[{value:$A.localizationService.formatDate(dtResult[i].dueDate), name:'dueDate'},
											{value:dtResult[i].contactName, navItem: {object: 'Contact', id: dtResult[i].contactId, name:'contactName'}, name:'contactName'},
											{value:dtResult[i].type, name:'type'},										
											{value:dtResult[i].subject, name:'subject'},												  
											{value:dtResult[i].relatedTo}, // No relatedTo link required for Submittal type(Inteviewing, Offer Accepted, Linked, Not Proceeding... )
											{value:dtResult[i].status, name:'status'},
											{value:dtResult[i].priority, name:'priority'},												  
											//{value:dtResult[i].assignedTo, navItem: {object: 'User', id: dtResult[i].assignedToId, name:'assignedTo'}},
											{value:dtResult[i].activityType, name:'activityType'},
											{value:dtResult[i].contactType, name:'contactType'}	]
								}
			}
			else {
				tmpTableRecord = {
									id:dtResult[i].Id, 
									isMassEditable:dtResult[i].isMassEditable,
									isMassDeletable:dtResult[i].isMassDeletable,
									activityType:dtResult[i].activityType,
									cells : [{value:dtResult[i].dueDate, date: true},
											//[{value:$A.localizationService.formatDate(dtResult[i].dueDate), name:'dueDate'},
											{value:dtResult[i].contactName, navItem: {object: 'Contact', id: dtResult[i].contactId, name:'contactName'}, name:'contactName'},
											{value:dtResult[i].type, name:'type'},										
											{value:dtResult[i].subject, name:'subject'},												  
											{value:dtResult[i].relatedTo, navItem: {object: 'Account', id: dtResult[i].relatedToId}, name:'relatedTo'},
											{value:dtResult[i].status, name:'status'},
											{value:dtResult[i].priority, name:'priority'},												  
											//{value:dtResult[i].assignedTo, navItem: {object: 'User', id: dtResult[i].assignedToId, name:'assignedTo'}},
											{value:dtResult[i].activityType, name:'activityType'},
											{value:dtResult[i].contactType, name:'contactType'}	]
								}
			}
			tmpTableData.push(tmpTableRecord);
		}
		if(tmpTableData.length > 0) {
			//console.log('tabel date:::::'+JSON.stringify(tmpTableData));
			component.set("v.activityMap", activityMap);
			component.set("v.tableData", tmpTableData);
			component.set("v.tableDataBackup", tmpTableData);
			let noDataDisplay = {"isNoData":false, "msg":''};
			component.set("v.noDataDisplay",noDataDisplay);		
			this.handleRowAction(component, tmpTableData[0]);
		} else {
			let noDataDisplay = {"isNoData":true, "msg":$A.get("$Label.c.ATS_NO_MY_ACTIVITIES")};
			component.set("v.noDataDisplay",noDataDisplay);
			component.set("v.tableData", []);
			component.set("v.tableDataBackup", []);
		}
	},
	//New Method for server call if it works merge it with init metod/loadTable Method
	loadTableUpdated: function (component, evt) {
		let stDate = component.get("v.startDate");
		let edDate = component.get("v.endDate");
		let filtrValues = component.get("v.picklistFilterParams");

		let actionType = evt.getParam('actionType');
		
		this.loadFilterData(component,filtrValues,stDate,edDate);
		
	},

	loadFilterData: function (component,picklistFilterParams, startDate, endDate) {
		component.set('v.setSpinner',true);
		let action = component.get("c.getFilterActivities");
		component.set("v.tableData", "");
		action.setParams({ "filterData" : picklistFilterParams, "startDate" :  startDate, "endDate" : endDate, "sortableField":component.get("v.sortableField"), "sortOrder":component.get("v.sortOrder")});

		 action.setCallback(this, function(response) {
			let state = response.getState();
            if(state === 'SUCCESS'){
				let dtResult = response.getReturnValue();
				this.buildFormatedTableData(component, dtResult);
			}
			component.set('v.setSpinner',false);
		 });
		 $A.enqueueAction(action);
	},

	saveUserPrefFilter :  function(cmp) {
		let filtrValues = cmp.get("v.picklistFilterParams");
		let rangeFilterValue = cmp.get("v.rangeFilterValue");
		let stDate = cmp.get("v.startDate");
		let edDate = cmp.get("v.endDate");

		let action = cmp.get("c.saveUserActivityPreferences"); 
		action.setParams({"filterData":filtrValues, "dateRangeKey":rangeFilterValue, "startDate":stDate, "endDate":edDate, "sortableField": cmp.get("v.sortableField"), "sortOrder":cmp.get("v.sortOrder")}); 

		action.setCallback(this, function(response) {
			let state = response.getState();
			if(state === 'SUCCESS') {
				this.showToast();
			}
		});

		$A.enqueueAction(action);
	},
	
	keyWordFromLWC : function(component) {
		var keyTableData = component.set("v.keyFilterTableData");
		component.set("v.tableData", keyTableData);
		//this.handleRowAction(component, keyTableData);
	},

	//use try-catch
	loadComponentGeneralInfo : function(cmp) { //will recheck again if this method required here or Activity timeline
		let action = cmp.get("c.currentUserWithOwnership");
		
		action.setCallback(this, function(response) {
			let state = response.getState();
			if(state === 'SUCCESS'){
				let results = response.getReturnValue();
				cmp.set('v.runningUser',results.usr);
				cmp.set('v.runningUserCategory', results.userCategory);
				cmp.set('v.usrOwnership', results.userOwnership);	
				this.loadTable(cmp);					
			}
		 });
		  $A.enqueueAction(action);
	},

	handleRowAction : function(component, selectedRow) {
		let cells = selectedRow.cells;
		let activityId = selectedRow.id;

		let activityMap = component.get("v.activityMap");
        let activitySelectedRec = activityMap.get(activityId);

		for(let i=0; i<cells.length; i++) {
			if(cells[i].navItem) {
				/*if(cells[i].navItem.object === 'Account') {
					let accId = cells[i].navItem.id;
					if(accId) {
						if(accId.substring(0, 3) === '001') {
							component.set("v.accountId",cells[i].navItem.id); 					
						}
					}
					
				}*/
				if(cells[i].navItem.object === 'Contact') {
					let accId = cells[i].navItem.id;
					if(accId) {
						if(accId.substring(0, 3) === '003') {
							component.set("v.contactId",cells[i].navItem.id); 		
							component.set("v.isContactAvailable", true);			
						}
					} else {
						component.set("v.isContactAvailable", false);
					}
					
				}
			}
			if(cells[i].name && cells[i].name === 'contactType') {
				component.set("v.contactType", cells[i].value);
				
			}
		}
		if(activitySelectedRec.accountId) {
			let accountId = activitySelectedRec.accountId;
			let contactRec = {"fields":{"AccountId":{"value":accountId}}};
			component.set("v.accountId", accountId);
			component.set("v.contactRecord", contactRec);
			component.set("v.talentId",component.get("v.contactId"));
			component.set("v.enableActivity","true");			
		}
		
		component.set("v.activitySelectedRecord", activitySelectedRec);
		component.set("v.enableTaskEventDiv", true);
		
	},	
	massUpdate : function(cmp, taskIdList) {
		var modalBody;
		let filterList = cmp.get('v.picklistFilterParams');
		let startDate = cmp.get('v.startDate');
		let endDate = cmp.get('v.endDate');
		var mucHeader = $A.get("$Label.c.ATS_Mass_Update_Tasks");

		let limit = $A.get("$Label.c.ATS_Mass_Update_Limit");
		let newTaskIdList = [];
		let setMsg = false;
		if(taskIdList.length <= limit) {
			newTaskIdList = taskIdList;
			setMsg = false;
		} else {
			//component.set("v.setMsg", true);
			setMsg = true;			
			newTaskIdList = taskIdList.slice(0, limit);
		}
		//$A.createComponent("c:C_MyActivitiesMassUpdate", {"taskIdList" : taskIdList},
		$A.createComponent("c:C_MyActivitiesMassUpdate", {"taskIdList" : newTaskIdList,"setMsg":setMsg, "selectedListCount":taskIdList.length},	
			function(content, status) {
				if(status === "SUCCESS") {
					modalBody = content;
					cmp.find('overlayLib').showCustomModal({
						header : mucHeader,   
						body : modalBody,
						closeCallback : function(){
						}
					});
				}
			}
		) ;
	},
	massDelete : function(cmp, event) {
		let numOfRecs = event.getParam("numOfRec");
		let dTaskIds = event.getParam("delTskIds");
		let dEvtIds = event.getParam("delEvtIds");
		let filterList = cmp.get('v.picklistFilterParams');
		let startDate = cmp.get('v.startDate');
		let endDate = cmp.get('v.endDate');
		var modalBody;
		var mdcHeader = $A.get("$Label.c.ATS_Mass_Delete_Confirmation");
		$A.createComponent("c:C_MyActivitiesMassDelete", {"numOfRecords":numOfRecs, "taskIds":dTaskIds, "eventIds":dEvtIds},
			function(content, status) {
				if(status==="SUCCESS") {
					modalBody = content;
					cmp.find('overlayLib').showCustomModal({
						header : mdcHeader,   
						body : modalBody,
						closeCallback : function() {
						}
					});
				}
			}
		);
	},
	openAddEventOrTaskModal : function(component, event) {
		const params = {
			"recordId": event.getParam("recordId"),
			"activityType":event.getParam("activityType"),
			"whoId": event.getParam("whoId"),
			"notMyActivityNewEventOrTask": false
			//"activityType":event.getParam("activityType","")
		};

		$A.createComponent(
			'c:C_TalentActivityAddEditModal',
			params,
			function(newComponent, status, errorMessage){
				if (status === "SUCCESS") {
					component.set("v.C_TalentActivityAddEditModal", newComponent);
				}
				
			}
		);
	},

	showToast : function() {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Success!",
			"type":"success",
			"message": "My default filter has been updated successfully."
		});
		toastEvent.fire();
	}
})