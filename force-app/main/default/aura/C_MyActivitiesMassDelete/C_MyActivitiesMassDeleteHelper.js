({
	doMassDelete : function(cmp, evt) {
		let eventIds = cmp.get("v.eventIds");
		let taskIds = cmp.get("v.taskIds");

		let action = cmp.get("c.massDelete");
		action.setParams({"taskIds":taskIds, "eventIds":eventIds});
		cmp.set("v.spinner", true);
		action.setCallback(this, function(response) {
			cmp.set("v.spinner", false);
			const state = response.getState();
			if(state === 'SUCCESS') {
				this.closeModal(cmp);
				this.showToast('Success','Records have been deleted successfully.','success');
				this.updateMyActivtiiesTableData(cmp, evt);
			}
			else {
				let err = response.getError();
				this.closeModal(cmp);
				this.showToast('Error', err[0].message, 'error');
			}
		});

		$A.enqueueAction(action);

	},
	showToast : function(title, msg, type) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": title,
			"message": msg,
			"type":type
		});
		toastEvent.fire();
	},
	closeModal : function(component) {
		component.find("overlayLib").notifyClose();
    },
	updateMyActivtiiesTableData : function(cmp, event) { 
		var appEvent = $A.get("e.c:E_MyActivitiesMassAction");		
		appEvent.fire();
	}
})