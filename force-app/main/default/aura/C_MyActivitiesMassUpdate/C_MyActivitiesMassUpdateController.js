({
	
	doCancel : function (component, event, helper) {
		helper.closeModal(component);
	},
	
	doMassUpdate : function (component, event, helper) {
		helper.massUpdate(component, event);
		
	}
})