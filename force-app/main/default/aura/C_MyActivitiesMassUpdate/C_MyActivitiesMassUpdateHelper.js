({
	massUpdate: function (cmp, event) {
		let tsk = cmp.get("v.task");
		let status = cmp.find("tstatus");
		let priority = cmp.find("tpriority");

		tsk.Status = status.get("v.value");
		tsk.Priority = priority.get("v.value");

		let taskIdList = cmp.get("v.taskIdList");

		if(Boolean(!this.validForm(cmp, event))) {
			return;
		}
		
		let action = cmp.get("c.massUpdate");
		action.setParams({'task':tsk, 'idList':taskIdList});
		cmp.set("v.spinner", true);
		action.setCallback(this, function(response) {
			 cmp.set("v.spinner", false);
			if(response.getState() === 'SUCCESS') {
				let result = response.getReturnValue();
				this.closeModal(cmp);
				this.updateMyActivtiiesTableData(cmp, event);
			}
			else {
				let err = response.getError();
				this.closeModal(cmp);
				this.showToast('Error', err[0].message, 'error');
			}
		});
		$A.enqueueAction(action);
	},
	
	validForm : function(component, event) {
		let task = component.get("v.task");
		let validActivtiyDate = true;
		let validSubjectLn = true;

		if(!$A.util.isEmpty(task.Subject) && !$A.util.isUndefinedOrNull(task.Subject)) {
			validSubjectLn = this.validateFieldLengths(component, event);
		}

		if(!$A.util.isEmpty(task.ActivityDate) && !$A.util.isUndefinedOrNull(task.ActivityDate)) {
			task.ActivityDate = this.formatDate(component.find('ActivityDate').get('v.value'));
			if (!this.validateDateFormats(component, task)) {
				validActivtiyDate = false;
			}
		}
		

		if(Boolean(!validActivtiyDate) || Boolean(!validSubjectLn))  {
			return false;
		}

		return true;
	},

	validateFieldLengths : function(component, event) {
        let validForm = true;

        var limits = [{"field" : "Subject", "limit" : 255, "errorFieldId" : "subjTooLong"}];
        for (var i=0; i<limits.length; i++) {
            var limitedField = component.find(limits[i].field);

            var fieldValue = limitedField.get("v.value");
            var valueLength = (typeof fieldValue !== "undefined") ? fieldValue.length : 0;

            if (valueLength > limits[i].limit) {
                var warningSpanId = component.find(limits[i].errorFieldId);
                $A.util.removeClass(warningSpanId, "toggle");
                validForm = false;
            } else {
                var warningSpanId = component.find(limits[i].errorFieldId);
                $A.util.addClass(warningSpanId, "toggle");
            }
        }

        return validForm;
    },
	formatDate:function(inputDate){
        var input = inputDate;
        if(input){
             // Typed in dates only work for "/" as delimiter.
            var arr = input.split("/");
            if(arr.length === 3){
                // Pad "0" for single digit date / month
                if(arr[0].length === 1){
                   arr[0] = '0' + arr[0];
                }
                if(arr[1].length === 1){
                   arr[1] = '0' + arr[1];
                }

                // Convert to "yyyy-mm-dd" based on user locale.
                var language = window.navigator.userLanguage || window.navigator.language;
                if(language === 'en-US'){
                    input = arr[2] + '-' + arr[0] + '-' + arr[1];
                }else{
                    input = arr[2] + '-' + arr[1] + '-' + arr[0];
                }
            } 

            // Adjust for timezone since the new Date() constructor sets to UTC midnight.
            var now = this.adjustDateForTimezone(input);
            if (now === false || isNaN(now.getTime())) {
                return false;
            } else {
				let mm = now.getMonth() + 1;
				let dd = now.getDate();
				if( mm < 10) {
					mm = '0' + mm;
				}
				if(dd < 10) {
					dd = '0' + dd;
				}
                return now.getFullYear()+"-"+mm +"-"+dd;
            } 
        }else{
            return '';
        }
       
    },
	 validateDateFormats : function (component, task) {
        var valid = true;
        var errText = "Valid date format is " + this.getAcceptedDateFormat();

        if (task.ActivityDate === false) {
            var warningSpanId = component.find("badActivityDate");
            warningSpanId.set("v.value", errText);
            $A.util.removeClass(warningSpanId, "toggle");
            valid = false;
        } else {
            var warningSpanId = component.find("badActivityDate");
            $A.util.addClass(warningSpanId, "toggle");
        }

        return valid;
    },
	 adjustDateForTimezone : function (inputDate) {
        var theDate = new Date(inputDate);
        if (!isNaN(theDate.getTime())) {
            // Use same date to get the timezone offset so daylight savings is factored in.
            var output = new Date(theDate.getTime() + (theDate.getTimezoneOffset() * 60 * 1000));
            return output;
        } else {
            return false;
        }
    },
	getAcceptedDateFormat : function () {
        var language = window.navigator.userLanguage || window.navigator.language;
        return language === "en-US" ? "mm/dd/yyyy" : "dd/mm/yyyy";
    },

	updateMyActivtiiesTableData : function(cmp, event) { 
		var appEvent = $A.get("e.c:E_MyActivitiesMassAction");
		appEvent.fire();
	},
	showToast : function(title, msg, type) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": title,
			"message": msg,
			"type":type
		});
		toastEvent.fire();
	},
	closeModal : function(component) {
            component.find("overlayLib").notifyClose();
    }
})