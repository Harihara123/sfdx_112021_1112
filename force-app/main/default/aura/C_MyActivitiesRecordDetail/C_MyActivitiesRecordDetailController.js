({
	doInit: function (component, event, helper) {
		component.set("v.isReadOnly", true);
		helper.populateActivityObject(component);
	},
	handleDeleteClick : function(component, event, helper) {
		component.set("v.isReadOnly", true);
		component.set("v.showDialog", true);
	},
	handleEditClick : function(component, event, helper) {
		let activityRec = component.get("v.activityRecord");

		component.set("v.isReadOnly", false);
		//component.set("v.spinner", true); is this required? - Neel
		if(activityRec.activityType === 'Task') {
			helper.populateActivityObject(component);
			helper.createTaskFormComponent(component); 
		}
		else if(activityRec.activityType === 'Event') {
			helper.populateActivityObject(component);
			helper.createEventFormComponent(component); 
		}
		//component.set("v.spinner", false); is this required? - Neel
	},

	handleActivityEvents : function(component, event, helper) {
		let activityType = event.getParam("activityType");
		let actionType = event.getParam("actionType");

		if(activityType === 'Task') {
			if(actionType === 'Save') {
				helper.syncReadOnlyTaskViewAfterSave(component);
			}
			else if(actionType === 'Cancel') {
				helper.populateActivityObject(component);
			}
		}
		else {
			if(actionType === 'Save') {
				helper.syncReadOnlyEventViewAfterSave(component);
			}
			else if(actionType === 'Cancel') {
				helper.populateActivityObject(component);
			}
		}
		
		component.set("v.isReadOnly", true);
	},
	handleFollowUpActions:function(component, event, helper) {
		console.log('activityRecord:'+JSON.stringify(component.get('v.activityRecord.relatedToId')));
		let selectedFollowUp = event.getParam("value");
		let actionType, activityType;		
		let taskEvent = $A.get("e.c:E_MyActivitiesEventTask");
		let whatId = component.get('v.activityRecord.relatedToId');
		let whoId =component.get('v.activityRecord.contactId');
		if(selectedFollowUp === 'task') {
			actionType = 'SAVE_NEW_TASK';
			activityType = 'task';
		} else if(selectedFollowUp === 'event') {
			actionType = 'SAVE_NEW_EVENT';
			activityType = 'event';
		}
		taskEvent.setParams({"actionType":actionType, "activityType":activityType, "recordId":whatId, "whoId":whoId});
		taskEvent.fire();
	},
	handleNo : function(component, event, helper) {
		component.set("v.showDialog", false);
	},
	handleYes : function(component, event, helper) {
		component.set("v.showDialog", false);
		let activityId = event.getParam('recordId');		
		helper.deleteActivity(component, activityId);
	},
	cancelEvent : function(component, event, helper) {
		component.set("v.isReadOnly", true);
		helper.populateActivityObject(component);
	},
	handleSaveNewEvent : function(component, event, helper) {
		let saveNewEvent = $A.get("e.c:E_MyActivitiesSaveNewActivity");
		saveNewEvent.setParam("actionType","SAVE_NEW_EVENT");

		saveNewEvent.fire();
	},
	handleSaveNewTask : function(component, event, helper) {
		let saveNewTask = $A.get("e.c:E_MyActivitiesSaveNewActivity");
		saveNewTask.setParam("actionType","SAVE_NEW_TASK");

		saveNewTask.fire();
	},
	handleSave : function(component, event, helper) {
		let saveEvt = $A.get("e.c:E_MyActivitiesSaveNewActivity");
		saveEvt.setParam("actionType", "SAVE");

		saveEvt.fire();
	}
})