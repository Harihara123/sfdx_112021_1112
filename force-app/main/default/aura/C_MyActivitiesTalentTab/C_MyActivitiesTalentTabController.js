({
	doInit: function (component, event, helper) {
		
		//set Resume for talent		
		helper.createTalentResumePanel(component);   
		helper.getG2Details(component);    
		helper.setupActivities(component);	
		
	},
	refreshG2Tab: function (component, event, helper) {
		component.set("v.enableActivitiesTab",false);
		component.set('v.selectedTab','resumeTab');
		helper.createTalentResumePanel(component); 
		helper.getG2Details(component);
		if(component.get('v.setG2')) {
			component.set('v.setG2', false);
			component.set('v.setG2', true);
		}
		if(component.get('v.setProfile')) {
			component.set('v.setProfile', false);
			component.set('v.setProfile', true);

		}		
	},
	refreshResumeTab: function (component, event, helper) {
		//helper.createTalentResumePanel(component);  
		
	},
	tabSelected : function(component, event, helper) {	
		if(component.get('v.selectedTab') === 'activitiesTab') {
			helper.setupActivities(component);  
			component.set("v.enableActivitiesTab",true);
		}
		else {
			component.set("v.enableActivitiesTab",false);
		}
	}
})