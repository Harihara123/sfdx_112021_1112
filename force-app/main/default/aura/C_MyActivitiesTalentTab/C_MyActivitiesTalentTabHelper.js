({
	createTalentResumePanel: function(component){
        var recordId = component.get("v.contactId");
        //var params = {'recordId' : recordId};

		var serverMethod = 'c.getAttachmentDetails';
        var actionParams = {'contactId': recordId};
		component.set("v.setSpinner", true);
    
        let getResumeDetails = component.get(serverMethod);
        getResumeDetails.setParams(actionParams);
		getResumeDetails.setCallback(this, function(response) {
			let state = response.getState();
			if(state === 'SUCCESS'){
				let result = response.getReturnValue();
				if(result !='' || result != null) {
					if(result.recordAndCount != null ) {
						if(result.recordAndCount.records.length > 0) {
							component.set('v.resumeId',result.recordAndCount.records[0].Id);
							component.set('v.setResume', false);
							component.set('v.setResume', true);
						}
						else {
							component.set('v.resumeId','');
							component.set('v.setResume', false);
						}
							
					}
				}				
			}
			component.set("v.setSpinner", false);
		 });
		  $A.enqueueAction(getResumeDetails);  
    },
	getG2Details: function(component) {
		let actionParams = component.get('v.contactId');    
        let getG2Details = component.get('c.getG2Details');
        getG2Details.setParams({"contactId":actionParams});
		getG2Details.setCallback(this, function(response) {
			let state = response.getState();
			if(state === 'SUCCESS'){
				let result = response.getReturnValue();
				if(result !='' || result != null) {	
					component.set('v.hasG2',result.hasG2);			
					component.set('v.g2LastUpdateDate',result.lastModifiedDate);
					component.set('v.g2LastUpdatedBy', result.lastCreatedBy);
					component.set('v.g2Comments', result.comments);
				}				
			}
		});

		 $A.enqueueAction(getG2Details);  
        
	},
	setupActivities : function(component) {
		
		let accountId = component.get("v.accountId");
		if(accountId) {
			let contactRec = {"fields":{"AccountId":{"value":component.get("v.accountId")}, "id":{"value":component.get("v.contactId")}}};
			component.set("v.contactRecord", contactRec);
		}
	}
})