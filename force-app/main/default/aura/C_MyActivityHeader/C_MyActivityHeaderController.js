({
	doInit: function (component, event, helper) {		
		var rtName = component.get("v.contactRecordFields.Record_Type_Name__c");		
		if(rtName =='Talent'){
			component.set("v.isTalent",true);
		} else {
			component.set("v.isTalent",false);
		}
		//component.find('recordLoader').reloadRecord(true);	
	},	
	refreshData: function (component, event, helper) {		
		var rtName = component.get("v.contactRecordFields.Record_Type_Name__c");
		if(rtName =='Talent'){
			component.set("v.isTalent",true);
		} else {
			component.set("v.isTalent",false);
		}
		component.find('recordLoader').reloadRecord(true);	
	},
	prefPhoneLink : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.Preferred_Phone_Value__c"));   
    },

    callMobilePhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.MobilePhone"));
    }, 
    
    callHomePhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.HomePhone"));
    }, 
    
    callPhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.Phone"));
    },
    
    callOtherPhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.OtherPhone"));
    }, 

    emailMain : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Email"));
    },
    
    emailAlt : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Other_Email__c"));
    },
    
    emailWork : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Work_Email__c"));
    },
	accountRecord : function (cmp, event, helper) {
		var accId = cmp.get("v.contactRecordFields.AccountId");
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": accId,
		  "slideDevName": "detail"
		  //window.open('/' + accId);
		});
		navEvt.fire();
	},
	openTalentRecord : function(cmp,event, helper){
        var recordID= cmp.get("v.contactRecordFields.Account.Current_Employer__c");
        window.open('/' + recordID,'_blank');
    },
	//For Client Record 
	openClientRecord : function(cmp,event, helper){
        var recordID= cmp.get("v.contactRecordFields.AccountId");
        window.open('/' + recordID,'_blank');
    },
	openTalentFromHeader : function(cmp,event, helper){
        var recordID= cmp.get("v.contactRecordFields.Id");
        window.open('/' + recordID,'_blank');
    },
	openCurrentEmployeerRecord : function(cmp,event, helper){
		var id= cmp.get("v.contactRecordFields.Account.Current_Employer__c");
		console.log('---->'+id+'<----');
		if( id != '' && id != ' '   && id != 'null' && id != null){
			var recordID = cmp.get("v.contactRecordFields.Account.Current_Employer__c");
			window.open('/' + recordID,'_blank');
		}
    },
})