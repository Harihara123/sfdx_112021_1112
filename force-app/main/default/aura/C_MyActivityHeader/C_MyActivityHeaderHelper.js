({
	fireURLEventPhone : function(component, phoneValue) {

		if (phoneValue !== null && phoneValue !== undefined){
			phoneValue = phoneValue.replace(/\s/g,'');
        
			var urlEvent = $A.get("e.force:navigateToURL");
			urlEvent.setParams({
				"url": "tel:" + phoneValue
			});
			urlEvent.fire();
		}
    },
    
    fireURLEventEmail : function(component, emailValue) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "mailto:" + emailValue
        });
        urlEvent.fire();
    },
})