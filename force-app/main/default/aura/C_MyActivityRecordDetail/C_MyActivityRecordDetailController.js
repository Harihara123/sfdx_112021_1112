({
	doInit: function (component, event, helper) {
		component.set("v.isReadOnly", true);
		helper.populateActivityObject(component);
	},
	handleDeleteClick : function(component, event, helper) {
		component.set("v.isReadOnly", true);
	},
	handleEditClick : function(component, event, helper) {
		let activityRec = component.get("v.activityRecord");

		component.set("v.isReadOnly", false);
		component.set("v.spinner", true);
		if(activityRec.activityType === 'Task') {
			helper.populateActivityObject(component);
			helper.createTaskFormComponent(component); 
		}
		else if(activityRec.activityType === 'Event') {
			helper.populateActivityObject(component);
			helper.createEventFormComponent(component); 
		}
		component.set("v.spinner", false);
	},

	saveAndNewTask : function(component, event, helper) {
		let activityRecord = component.get("v.activityRecord");
		const params = {
			"recordId": activityRecord.relatedToId, 
			"activityType": "task"
		};

		$A.createComponent(
			'c:C_TalentActivityAddEditModal',
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					component.set("v.C_TalentActivityAddEditModal", newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			}
		);
	},
	saveNewTask : function(component, event, helper) {
	},

	saveNewEvent : function(component, event, helper) {
	},
	handleActivityEvents : function(component, event, helper) {
		let activityType = event.getParam("activityType");
		let actionType = event.getParam("actionType");

		if(activityType === 'Task') {
			if(actionType === 'Save') {
				helper.syncReadOnlyTaskViewAfterSave(component);
			}
			else if(actionType === 'Cancel') {
				helper.populateActivityObject(component);
			}
		}
		else {
			if(actionType === 'Save') {
				helper.syncReadOnlyEventViewAfterSave(component);
			}
			else if(actionType === 'Cancel') {
				helper.populateActivityObject(component);
			}
		}
		
		component.set("v.isReadOnly", true);
	},
	handleFollowUpActions:function(component, event, helper) {
		let selectedFollowUp = event.getParam("value");
		let actionType, activityType;
		console.log('selectedFollowUp:'+selectedFollowUp);
		let taskEvent = $A.get("e.c:E_MyActivitiesEventTask");
		//let whatId = component.get("v.task.WhatId");
		let whatId = component.get('v.activityRecord.relatedToId');
		if(selectedFollowUp === 'task') {
			actionType = 'SAVE_NEW_TASK';
			activityType = 'task';
		} else if(selectedFollowUp === 'event') {
			actionType = 'SAVE_NEW_EVENT';
			activityType = 'event';
		}

		console.log('actionType:'+actionType);
		console.log('activityType:'+activityType);
		console.log('whatId:'+whatId);
		taskEvent.setParams({"actionType":actionType, "activityType":activityType, "recordId":whatId});
		taskEvent.fire();
	}
})