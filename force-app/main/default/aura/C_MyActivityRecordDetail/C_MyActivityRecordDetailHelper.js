({
	populateActivityObject: function (cmp) {
		let activityRec = cmp.get("v.activityRecord");
		console.log('activityType==============='+activityRec.activityType);

		if(activityRec.activityType === 'Task') {
			let tsk = activityRec;
			let task = {};
			task.id = tsk.Id;
			task.WhoId = tsk.contactId;
			task.Type = tsk.type;
			task.ActivityDate = tsk.dueDate;
			task.Subject = tsk.subject;
			task.WhatId = tsk.relatedToId;
			task.Status = tsk.status;
			task.Priority = tsk.priority; 
			task.OwnerId = tsk.assignedToId;
			task.Description = tsk.descripton;
			task.contactName = tsk.contactName;
			task.sobjectType='Task';
			task.attributes=null;
			task.Owner = { 'sobjectType' : 'User','Id' : tsk.assignedToId};
			task.AccountId = tsk.relatedToId; 
			task.RelatedTo = tsk.relatedTo
			
			cmp.set("v.task", task);
			console.log('task--****--'+JSON.stringify(task));
		}
		else if(activityRec.activityType === 'Event') {
			let evt = activityRec;
			let theevent = {};
			theevent.Id = evt.Id;
			theevent.WhoId = evt.contactId;
			theevent.Type = evt.type;
			theevent.StartDateTime = evt.StartDateTime;
			theevent.EndDateTime = evt.EndDateTime;
			theevent.Subject = evt.subject;
			theevent.WhatId = evt.relatedToId;
			//theevent.Status = not on theevent Object
			//theevent.Priority = not on theevent object
			theevent.OwnerId = evt.assignedToId;
			theevent.Description = evt.descripton;
			theevent.contactName = evt.contactName;
			theevent.sobjectType='Event';
			theevent.attributes=null;
			theevent.Owner = {'sobjectType' : 'User','Id' : evt.assignedToId};
			theevent.AccountId = evt.relatedToId; 
			theevent.RelatedTo = evt.relatedTo;
			theevent.Pre_Meeting_Notes__c = evt.preMeetingNote;
			theevent.Submittal_Interviewers__c = evt.submittalInterviewers; 
			theevent.Interview_Status__c = evt.interviewStatus;
			theevent.Not_Proceeding_Reason__c = evt.notProceedReason;
			cmp.set("v.theevent", theevent);
			console.log('theevent--****--'+JSON.stringify(theevent));
			if(evt.type.startsWith('Not')){
				cmp.set("v.notProceedingReason",'True');
				console.log('Inside Not proceeding');
			}else{cmp.set("v.notProceedingReason",'false');}
			if(evt.eventCategory === 'Interviewing' && !$A.util.isUndefinedOrNull(evt.descripton)) {
				cmp.set("v.interviewingNotes", JSON.parse(evt.descripton));
			}
		}

	},

	createTaskFormComponent : function(cmp) {

		let newParams = {};
		let componentName = '';
		let dFormat = $A.get("$Locale.dateFormat");
		let task = cmp.get("v.task");
		newParams = {task: task, runningUser: cmp.get("v.runningUser"), dateFormat: dFormat, isNew: "false", notMyActivity:false, isMyActivityReadonly:false};
		componentName = 'c:C_TalentActivityTaskForm';

		this.createRespectiveComponent(cmp, task, componentName, newParams);
	},

	createEventFormComponent : function(cmp) {

		let newParams = {};
		let componentName = '';
		let dFormat = $A.get("$Locale.dateFormat");
		let theevent1 = cmp.get("v.theevent");

		newParams = {theevent: theevent1, runningUser:cmp.get("v.runningUser"), dateFormat: dFormat , isNew:"false", typeValue : theevent1.Type, notMyActivity:false};
		componentName = 'c:C_TalentActivityEventForm';
		this.createRespectiveComponent(cmp, theevent1, componentName, newParams);
	},
	
	createRespectiveComponent : function(cmp, activityObject, componentName, newParams) {
		$A.createComponent(componentName,newParams,
			function(newComponent, status, errorMessage){
				if(newComponent.isValid() ){
					//Add the new button to the body array
					if (status === "SUCCESS") {
						console.log('Component Created');
						let activityRec = cmp.get("v.activityRecord");
						if(activityRec.activityType === 'Task') {
							newComponent.set("v.task", activityObject);
                			cmp.set("v.C_TalentActivityTaskForm", newComponent); 
						}
						else {
							newComponent.set("v.theevent", activityObject);
                			cmp.set("v.C_TalentActivityEventForm", newComponent); 
						}
						
					}
					else if (status === "INCOMPLETE") {
						console.log("No response from server or client is offline.")
						// Show offline error
					}
					else if (status === "ERROR") {
						console.log("Error: " + errorMessage);
						// Show error message
					}
				}
                        
			}
		);
	},

	syncReadOnlyTaskViewAfterSave : function(cmp) {
		 let tsk = cmp.get("v.activityRecord");
		 let task = cmp.get("v.task");

		 tsk.descripton = task.Description;
		 tsk.status = task.Status;
		 tsk.dueDate = task.ActivityDate;
		 tsk.type = task.Type;
		 tsk.priority = task.Priority ;
		 
		 cmp.set("v.activityRecord", tsk);		
	},
	syncReadOnlyEventViewAfterSave : function(cmp) {
		 let evt = cmp.get("v.activityRecord");
		 let theevent = cmp.get("v.theevent");

		 evt.preMeetingNote = theevent.Pre_Meeting_Notes__c;
		 evt.descripton = theevent.Description;
		 evt.StartDateTime =theevent.StartDateTime;
		 evt.EndDateTime = theevent.EndDateTime;
		 evt.type = theevent.Type;

		 cmp.set("v.activityRecord", evt);		
	}

})