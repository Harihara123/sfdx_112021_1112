({
    doInit: function(cmp, event, helper) {
        var initName = cmp.get("v.list");
        cmp.set("v.initialNameValue", initName.Tag_Name__c);
    },
	bringUpDeleteModal: function(cmp, event, helper) {
        var record = cmp.get("v.list");
        var cmpEvent = cmp.getEvent("deleteListEvent");
        cmpEvent.setParams({"record":record});
        cmpEvent.fire();
    },
    inlineEditName: function(cmp, event, helper) {
        cmp.set("v.isEditMode", true);
        setTimeout(function(){ 
            cmp.find("editBox").focus();
        }, 100);
    },
    onNameChange: function(cmp,event,helper){
        var inputName = event.getSource().get("v.value").trim();

        if(inputName){ 
            cmp.set("v.showSaveCancel", false);	
        }
       
    },  
    cancelChange: function (cmp, event, helper) {
        cmp.set("v.isEditMode", false);
        var originalName = cmp.get("v.initialNameValue");

        cmp.set("v.list.Tag_Name__c", originalName);
        cmp.set("v.showErrorClass", false);
        cmp.set("v.showSaveCancel", false);
       
    },
    saveChange: function (cmp, event, helper) {
        helper.saveChange(cmp);
        helper.closeNameBox(cmp, event);
    },
    closePopover: function(cmp, event, helper) {
        helper.closeNameBox(cmp, event);
    },
    saveOnEnter: function(cmp, event, helper) {
         if(event.keyCode === 13){
            helper.saveChange(cmp);
            helper.closeNameBox(cmp, event);
        }
    }

})