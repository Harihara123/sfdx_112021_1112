({
	saveChange: function (cmp) {
        var action = cmp.get("c.updateList");
        var record = cmp.get("v.list");
        record.LastModifiedDate = new Date(record.LastModifiedDate);
        action.setParams({'changedList':record});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var message = response.getReturnValue();

                record.LastModifiedDate = $A.localizationService.formatDate(record.LastModifiedDate, "D MMM, YYYY h:mm a");


                var toastEvent = $A.get("e.force:showToast");

                if(message === 'SUCCESS') {
                    cmp.set("v.showErrorClass", false);
                    cmp.set("v.showSaveCancel", false);
                        toastEvent.setParams({
                        "type": "success",
                        "title": "Success",
                        "message": "The List's name has been changed"
                    });
                    toastEvent.fire(); 

                    cmp.set("v.initialNameValue", record.Tag_Name__c);
                } else {
                    this.showErrorToast(message);
                    cmp.set("v.showErrorClass", true);
                }
            } else {
            }
        });

        $A.enqueueAction(action);
	},
    showErrorToast: function (cmp, message) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "type": "error",
            "title": "Error",
            "message": message
        });
        toastEvent.fire(); 
    },
    closeNameBox: function (cmp, event) {
        cmp.set("v.isEditMode", false);  

        var originalName = cmp.get("v.initialNameValue"),
            inputName = cmp.find("editBox").get("v.value").trim();

        var duplicateCheck = cmp.get("v.otherLists").filter(function(el) {
            return el.Tag_Name__c === inputName;
        });

        if(inputName == '' || duplicateCheck.length > 1){
            if( !inputName ) {
                var currentList = cmp.get("v.list");
                currentList.Tag_Name__c = originalName;
                cmp.set("v.list", currentList);
            }

            cmp.set("v.showErrorClass",true);
            cmp.set("v.showSaveCancel",true);

            this.showErrorToast(cmp, 'List Names cannot be Empty or Duplicates.');

        } else {
            cmp.set("v.showErrorClass",false);

            if(inputName !== originalName) {
                cmp.set("v.showSaveCancel",true);
            } else {
                cmp.set("v.showSaveCancel",false);
            }
        }
    },
})