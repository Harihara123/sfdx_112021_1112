({
    doInit: function(cmp, event, helper) {
        helper.getResults(cmp);
    },
    sortByName: function(cmp, event, helper) {
        helper.sortRecordList(cmp, helper, 'Tag_Name__c');
    },
    sortByCount: function(cmp, event, helper) {
        helper.sortRecordList(cmp, helper, 'Candidate_Count__c');
    },
    sortByModifiedDate: function(cmp, event, helper) {
        helper.sortRecordList(cmp, helper, 'LastModifiedDate');
    },
    bringUpDeleteModal: function(cmp, event, helper) {
        helper.handleCreateComponent(cmp, event);
    },
    removeRowFromList: function(cmp, event, helper) {
        helper.removeRowFromList(cmp, event, helper);
    },
    createList: function(cmp, event, helper) {
        helper.createNewList(cmp, event, helper);
    },
    toggleCreateMode: function(cmp, event, helper) {
        helper.toggleCreate(cmp, helper);
    },
    onNameChange: function(cmp,event,helper){ 
        helper.handleNameChange(cmp, event, helper);
    },
    updateListOnEnter: function(cmp, event, helper){
        if(event.keyCode === 13){
            //;
            helper.createNewList(cmp, event, helper);
        }
    },
    filterLists: function(cmp, event, helper){
        helper.filterLists(cmp);
    }

})