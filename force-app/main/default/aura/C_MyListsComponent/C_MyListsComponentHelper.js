({
	getResults: function (cmp) {
        var action = cmp.get("c.currentUserLists");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var returnedList = response.getReturnValue();

                returnedList.forEach(function(el) {
                    el.LastModifiedDate = $A.localizationService.formatDate(el.LastModifiedDate, "D MMM, YYYY h:mm a");
                    el.Tag_Name__c = el.Tag_Name__c ? el.Tag_Name__c.replace(/\\/g, '') : el.Tag_Name__c;
                });


                cmp.set("v.lists", returnedList);
                cmp.set("v.currentLists", returnedList);
                cmp.set("v.listsLoaded", true);
            }
        });

        $A.enqueueAction(action);

	},
    sortRecordList: function (cmp, helper, sortField) {
        var recordList = cmp.get("v.lists");
        var sortDirection = cmp.get("v.sortDirection");
        var newSortDirection = sortDirection === 'ASC' ? 'DESC' : 'ASC';

        recordList = helper.sortValues(recordList, sortField, newSortDirection, helper);

        cmp.set("v.currentLists", recordList);
        cmp.set("v.sortDirection", newSortDirection);
        cmp.set("v.sortColumn", sortField);
    },
    sortValues: function (values, sortField, sortDirection, helper) {
        var firstVal = sortDirection === 'ASC' ? 1 : -1;
        var secondVal = sortDirection === 'ASC' ? -1 : 1;

        values.sort(function(a,b) {
            
            var compareFields = helper.setSortFieldValues(a, b, sortField);

            var fieldA = compareFields[0];
            var fieldB = compareFields[1];

            if(fieldA < fieldB) {
                return secondVal;
            }

            if(fieldA > fieldB) {
                return firstVal;
            }

            return 0;
        });

        return values;
    },
    setSortFieldValues: function (a, b, sortField) {
        var fieldA = a[sortField];
        var fieldB = b[sortField];

        if(sortField === 'Tag_Name__c') {
            fieldA = fieldA.toUpperCase();
            fieldB = fieldB.toUpperCase();
        }

        if(sortField === 'LastModifiedDate') {
            fieldA = new Date(fieldA).getTime();
            fieldB = new Date(fieldB).getTime();
        }

        return [fieldA, fieldB];
    },
    handleCreateComponent: function (cmp, event) {
        $A.createComponent(
            "c:C_RemoveContactFromListModal",
            {"record" : event.getParam('record')},
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    cmp.set('v.C_RemoveContactFromListModal', newComponent);
                }
                else if (status === "INCOMPLETE") {
                }
                else if (status === "ERROR") {
                }
            }
        );

    },
    createNewList: function(cmp, event, helper) {
        cmp.set("v.loading", true);
        cmp.set("v.filterText", "");

        var action = cmp.get("c.insertList");
        action.setParams({"tagName":cmp.get("v.newTagName")});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var returnedList = response.getReturnValue();

                returnedList.Candidate_Count__c = 0;

                returnedList.LastModifiedDate = $A.localizationService.formatDate(returnedList.LastModifiedDate, "D MMM, YYYY h:mm a");
                returnedList.Tag_Name__c = returnedList.Tag_Name__c ? returnedList.Tag_Name__c.replace(/\\/g, '') : returnedList.Tag_Name__c;

                var currentLists = cmp.get("v.lists");
                var shownLists = cmp.get("v.currentLists");
                shownLists.push(returnedList);
                currentLists.push(returnedList);

                if(currentLists.length === 75) {
                    cmp.set("v.isCreateMode", false);
                }

                var sortedLists = helper.sortValues(shownLists, cmp.get("v.sortColumn"), cmp.get("v.sortDirection"), helper);

                cmp.set("v.lists", currentLists);
                cmp.set("v.currentLists", sortedLists);

                cmp.set("v.loading", false);

                helper.showToast(cmp, helper, 'SUCCESS', returnedList.Tag_Name__c);
            } else {
                cmp.set("v.loading", false);
                helper.showToast(cmp, helper, 'FAILURE', '');
            }
        });

        $A.enqueueAction(action);
    },
    showToast: function(cmp, helper, status, listName) {
        var toastEvent = $A.get("e.force:showToast");
        var type, title, message;
        if(status == 'SUCCESS') {
            type = "success";
            title = "Success";
            message = "The List '" + listName + "' has been added";
            cmp.set("v.newTagName", "");
            cmp.set("v.showErrorClass", false);
        } else {
            type = "error";
            title = "Error";
            message = "Unable to Create List";
            cmp.set("v.showErrorClass", true);
        }
        toastEvent = helper.setToast(toastEvent, type, title, message);
        toastEvent.fire(); 
    },
    removeRowFromList: function (cmp, event, helper) {
        var lists = cmp.get("v.lists");
        var currentLists = cmp.get("v.currentLists");
        var removedList = event.getParam("removedList");
        var removalSuccess = event.getParam("requestStatus");

        var toastEvent = $A.get("e.force:showToast");
        var type, title, message;

        if(removalSuccess) {
            type = "success";
            title = "Success";
            message = "The list was successfully deleted.";
            var newList = lists.filter(function(el) {
                return el.Id !== removedList.Id;
            });

            var shownLists = currentLists.filter(function(elem) {
                return elem.Id !== removedList.Id;
            });

            cmp.set("v.lists", newList);
            cmp.set("v.currentLists", shownLists);
        }
        else {
            type = "error";
            title = "Error";
            message = "Unable to delete list.";
        }
        toastEvent = helper.setToast(toastEvent, type, title, message);
        toastEvent.fire();
    },
    handleNameChange: function (cmp, event, helper) {
        var lists = cmp.get("v.lists");

        var duplicateCheck = lists.filter((el) => {
            return el.Tag_Name__c === event.getSource().get("v.value").trim();
        });

        if(event.getSource().get("v.value").trim() == '' || duplicateCheck.length > 0){
            cmp.set("v.showErrorClass",true);
            var inputBox = cmp.find("contactListInput");
            var customValidity = event.getSource().get("v.value").trim() == '' ? "Cannot create a List with an empty name." : "Cannot have duplicate list names.";
            inputBox.setCustomValidity(customValidity);
            inputBox.reportValidity();
        } else {
            cmp.set("v.showErrorClass",false);
            var inputBox = cmp.find("contactListInput");
            inputBox.setCustomValidity("");
            inputBox.reportValidity();
        }
    },
    toggleCreate: function (cmp, helper) {
       var showCreate = cmp.get("v.isCreateMode");
        var currentLists = cmp.get("v.lists");
        if(currentLists.length < 75) {
            cmp.set("v.isCreateMode", !showCreate);
            cmp.set("v.showErrorClass", false);
            cmp.set("v.newTagName", "");
        }
        else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent = helper.setToast(toastEvent, 'error', 'Maximum lists', 'You have reached your maximum limit.');
            toastEvent.fire();
        }
    },

    setToast : function(toastEvent, type, title, message) {
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });
        return toastEvent;
    },

    filterLists: function(cmp) {
        var lists = cmp.get('v.lists');
        var filterText = cmp.get('v.filterText');

        if(filterText) {
            filterText = filterText.toUpperCase();
        } else {
            cmp.set('v.currentLists', lists);
            return;
        }

        var filteredList = lists.filter(function(myList) {
            if(myList && myList.Tag_Name__c.toUpperCase().includes(filterText)) {
                return myList;
            }
        });

        cmp.set('v.currentLists', filteredList);

    }
})