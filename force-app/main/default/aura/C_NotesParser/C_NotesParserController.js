({
	/* Rajeesh review comments
	1. The sequence need to be changed. make call out first, and then save.
	2. Checked with Eric- no need of creating activity;
	3. Need to capture the actual note in a visible field like comments on the modal.
	4. Avoid set time out calls, this goes out of typical aura rendering cycles.
	5. Is there ways to remove html elements and use lightning base components?p
	6. Remove all unwanted code, like utility bar...?
	7. test mode is not needed.
	8. No need of server call for user id.
	*/
    doInit: function(component, event, helper) {
		helper.CURRENT_USER_ID = $A.get("$SObjectType.CurrentUser.Id");
    },
	//Rajeesh redo button to prevent any data loss and do all over again.
	handleRedoNoteTask: function(component,event,helper){
		//helper.redoNote(component);


		component.set("v.redo",true);
		//component.destroy();
		

	},
    handleSaveNoteTask: function(component, event, helper) {
        var noteText = component.get("v.noteText");
        var contactId = component.get("v.recId");
		//Rajeesh save a copy of original text for backout/redo.
		helper.originalText = noteText;
        component.find("saveButton1").set('v.disabled',true);
		helper.performExtraction(component, component.get("c.getNotesExtraction"), noteText, contactId);
   
    },    
    handleRejectNoteTask: function(component, event, helper) {
        var rawText = component.get("v.noteText");
        var rejectedFlag = true;
		//Rajeesh to fix remove node error 4/15/2019 for repeated usage.
		helper.selectNodes = [];
        helper.normalizedNamesMap = [];
        helper.logTrainingData(component, {}, rawText, helper,rejectedFlag );
        helper.launchEditTalentSummaryDialogReject(component);
        helper.closeUtilityBar(component);
    },
    handleVerifyNoteTask: function(component, event, helper) {        
        helper.handleVerifyNoteTask(component);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
         component.set("v.Spinner", true); 
    },
     
  // this function automatic call by aura:doneWaiting event 
     hideSpinner : function(component,event,helper){
      // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
     },
     
    textChanged: function(component, event, helper) {
        let txt = component.get("v.noteText");
        let btn = component.find("saveButton1");
        
        if (txt.length < 1) {
            btn.set('v.disabled',true);
        } else {
            btn.set('v.disabled',false);
        };
    }
})