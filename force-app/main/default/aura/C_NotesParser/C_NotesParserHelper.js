({      
    //TODO! Connected developers, turn to false to disable the test mode feature used in scratch-org (e.g. resetting location of contact record)
    TEST_MODE:false,
    // holds the current user id. This is retrieved on initialization of component
	//Rajeesh redo
	originalText:'',
	parsedSkills:[],
	redoNote:function(component){
		console.log('notetext'+component.get("v.noteText"));
		
		//document.getElementById("enrichedNotes").innerHTML = this.originalText;
		/*
		component.set('v.noteText',this.originalText);
		document.getElementById("non_extraction_span").style.display = 'inline';
        document.getElementById("non_extraction_span").style.visibility = 'visible';

		document.getElementById("extraction_span").style.visibility = 'hidden'; 
        document.getElementById("extraction_span").style.display = "none";
		*/
		//"recId" : cmp.get("v.recordId"), "talentId" : cmp.get("v.talentId"),"parentCmp":cmp 
		

	},
	getMyLocation: function(component){
		navigator.geolocation.getCurrentPosition(function(position) {
    		// console.log("User located!!! ");
    		component.set("v.myLocation", position.coords.latitude + "," + position.coords.longitude);
    	});
	},
    CURRENT_USER_ID: null,
    /**
     * For now, the following entity types support "normalization", meaning that a drop-down is presented to 
     * allow the recruiter to change the predicted normalized value of entry, e.g. if we guessed "Columbia, MD"
     * but recruiter really wanted "Columbia, SC". 
     * If an entity type does NOT support normalization, e.g CURRENT_JT, this means the string itself extracted value, e.g.
     * "software developer" is the normalized value as well (e.g. no normalization needs to be performed to store appropriately on the backend)
     */
    //Rajeesh 4/11/2019 Updated for skills.
    supportedNormalizedEntities : ["LIVING_LOCATION", "RELOC_LOCATION", "CURRENT_HOURLY_RATE", "DESIRED_HOURLY_RATE","SKILLS"],
    doesEntityTypeSupportNormalization: function(entType) {
        for (var i=0;i<this.supportedNormalizedEntities.length;i++) {
            if (this.supportedNormalizedEntities[i] == entType) return true;
        }
        return false;        
    },
    // Store the last extraction response here. This JSON will be touched up at the end
    // to account for user modifications/corrections of the "entities".. and then will
    // be sent back down to server as a golden-record to be used to refine/retrain the underlying model.
    LAST_RAW_EXTRACTION_RESPONSE: null,
    /**
     * Call to Apex to execute a REST call to our spacy/prodigy/python trained Entity Extractor
     * model. This model returns a JSON object in the example format at bottom of this file. 
     * @param {*} component 
     * @param {*} actionExtraction 
     * @param {*} noteText 
     * @param {*} contactId 
     */
    performExtraction: function(component, actionExtraction, noteText, contactId) {
        if (noteText.substring(noteText.length-1, noteText.length) != '.') {
          noteText = noteText + '.'        
        }        
        var params = { "noteText" : noteText,  "contactId" : contactId, "geoLoc": component.get("v.myLocation") };
        //if (city) params["city"] = city;
        //if (state) params["state"] = state;
        //if (country) params["country"] = country;
        actionExtraction.setParams(params);        

        var me = this;
		var callbackFn = function(extractionResponse) {

           var extractionState = extractionResponse.getState();
           if (extractionState === "SUCCESS") {                   
            me.LAST_RAW_EXTRACTION_RESPONSE = extractionResponse.getReturnValue();
//console.log('---me.LAST_RAW_EXTRACTION_RESPONSE---'+me.LAST_RAW_EXTRACTION_RESPONSE);            
               var obj = JSON.parse(me.LAST_RAW_EXTRACTION_RESPONSE);               
               // if nothing to show, just reset utility bar and log the call
               //if (!obj.entities || obj.entities.length ==0) {
                //me.closeUtilityBar(component);
               //} else {
                   // render the HTML (pick apart the JSON object and render as SPAN's)
                   
                    var notesRenderedHtml = me.renderNotesResponse(obj);       

					             
                    me.clearMarkNodes();
                    document.getElementById("enrichedNotes").innerHTML =  notesRenderedHtml;                    
                    document.getElementById("enrichedNotes").classList.add("viewport-a");
                    
                    // Make UI appear in the "Extraction verification"-mode, vs the recruiter note input mode
                    me.resetUI(component, true);
                    // place a SELECT dropdown below for supported normalization entities, e.g the LIVING_LOCATION and RELOC extrations so user can verify
                    // the exact address that will be saved (remove ambiguity due to multiple colliding city names, e.g.
                    // Columbia, MD vs. Columbia, SC )
                    me.processNormalizedEntityLabelling(obj);
                    // add listeners so user can remove incorrectly tagged entity extractions (by hovering over the highlight, which shows an X box, then clicking the X)
                    // also allows user to click and drag to highlight newly tagged entities.                    
                    me.addAnnotationListener();
              // }
               
               
           }
        };
        
        actionExtraction.setCallback(this, callbackFn);        
        $A.enqueueAction(actionExtraction);
    },
    /**
     * When user manually annotates  we need to go fetch
     * the normalized geo/rate/etc from python/google to present dropdown to user to select the correct
     * normalized form of the geo/rate, e.g. did "Columbia" mean Columbia, MD or Columbia, SC.
     */
    getNormalizedEntity: function(component, entity, label, contactId) {
        // the entity is a SPAN.. we want to get the innerHTML out of the span, so we need to use technique below
        var div = document.createElement('div');
		//Rajeesh get start idx and end idx
		var startIndex = entity.match(/startidx="(.*?)"/)[1];
		var endIndex = entity.match(/endidx="(.*?)"/)[1];
        div.innerHTML = entity;
		//Rajeesh trying to fix a defect, when selected text is just a blank space
		if($A.util.isEmpty(entity)){
			//console.log('---inside blank entity loop---');
			alert('select some text and annotate');
			return;
		}
        entity = div.firstChild.innerHTML.trim();
        // we send down the concatenated string below to server in place of the noteText.
        // Server knows how to interpet this and get the normalized entity json using this single pipe-delimited
        // field
        var actionNormalizer = component.get("c.getNormalizedEntity");
        var noteText = entity + "|" + label;                
        var params = { "noteText" : noteText,  "contactId" : contactId , "label" : label};        
        actionNormalizer.setParams(params);        

        var me = this;
        actionNormalizer.setCallback(this, function(extractionResponse) {
            var extractionState = extractionResponse.getState();
            if (extractionState === "SUCCESS") {    
               //console.log(extractionResponse.getReturnValue()) ;                               
               var obj = JSON.parse(extractionResponse.getReturnValue());        
			   /*	Rajeesh skills -> because of multi values- it is not possible to attach the drop downs
					after individual manual annotations if we dont know start & end idx of the entity.
					-can't look it up just using id.
					including a dummy entity section in obj so that skill can be looked up correctly after 
					normalization and attach the normalized values next to the correct element.
			   
					entities: [
						{
						  "label": "SKILLS",
						  "resolvedEntity": "javascript",
						  "startIdx": 171,
						  "endIdx": -1,
						  "ruleBased": false
						},

					]
			   */                
			   if(label === "SKILLS")
			   {
					//obj needs entities section for skills.
					var entities = {};
					entities.label = "SKILLS",
					entities.resolvedEntity=entity.toLowerCase();
					entities.startIdx = parseInt(startIndex);
					entities.endIdx=parseInt(endIndex);
					entities.ruleBased="false";

					obj.entities.push(entities);

			   }
               me.processNormalizedEntityLabelling(obj);
               me.repositionAllNormalizedEntities();
            }
        });
        $A.enqueueAction(actionNormalizer);
        
    },
    /**
     * Code below is responsible for picking apart the Entity Extraction API call's JSON response
     * (example at bottom of this file).. Uses CSS, and It shows each token as a span.. this makes is easier
     * to "mark"<mark> elements.
     */
    renderNotesResponse: function(obj) {
        // we want to cache the "startIdx" for each of the extracted entities.
        // this will be used later when we loop through the "tokens" of the notes.
        // we will match up/check if the startIdx of the token is the same as one 
        // of the entities startIdx's. if it is, this is the beginning of the "mark"<mark>
        // html tagging we will need to surround this set of tokens with (until the entities endIdx)
        // this <mark> is then highlighted on the UI, hence making the extracted entity appear highlighted.

        this.cacheEntities(obj.entities);
        var html = ['<div class="c0170">']
        var isMarking = false
        var markingHtml = []
        var ent = null
        // technique below is basically to have "html" array contain the overall html that will
        // eventually be rendered. each word token is added as a <span> and is added to the html
        // array. However, when we find that a token is the beginning of an entity extraction, we , instead
        // start adding this <span> content to a temporary, markingHtml array, until all of the word tokens that make up
        // this entity have been walked over, at which point, we surround this markingHtml content of spans with a <mark> 
        // tag (and some extra styling classes to make the highlighting) and dump this set of spans back ino the html array, and continue.
        // This cumlinates in painting of HTML on the screen as follows, which are picked up by CSS to make look like highlighted text:
        /*
                <span class="c0172" id="8">working </span>
                <span class="c0172" id="9">with </span>
                <mark class="c0193 c0174">
                    <span class="c0172" id="10">AT&amp;T </span>
                    <span class="c0197 c0196">CURRENT_EMPLOYER_ORG
                        <span class="c0175">×</span>
                    </span>
                </mark>
                <span class="c0172" id="11">for </span>

                (note, if we rendered the above with line-breaks [like shown here], the system would not work.. has to be right next to each other, e.g. <span class="c0172" id="8">working </span><span class="c0172" id="9">with </span> )
        */
        for (var i=0;i<obj.tokens.length;i++) {
            var tok = obj.tokens[i]
            //if we find the startIdx of the token IS the same as the startIdx of an entity, start marking this text
            if (this.entities[tok.startIdx]) { // begin start of new mark
                ent = this.entities[tok.startIdx]
                //console.log(ent.label)
                isMarking = true
                //rajeeesh - is this needed?
				//tokens getting deleted for ex. cloud is getting deleted.
				// He is good with cloud foundry and amazon aws. 
				markingHtml = []
            } else if (ent!=null && ent.endIdx <= tok.startIdx) { // we are at the end token, we can now peal off all off the markingHTML                              
                // we can now peal off the contents of our markingHtml array which we have been capturing
                // while running over our extracted entity, and add to the overall full html
                html.push(this.createMarkHtml(markingHtml.join(""), ent, obj))
                isMarking = false
                ent = null          
            }        
            //<span class="c0172" id="11">for </span>
            // very important to set the id='1' property equal to the tok.ordinal (basically just an incremental counter, e..g 1, 2,3, 4, etc)
            // This is necessary later when we ad the mouseup/mousedown events in "addAnnotationListener"
            // this ordinal will serve as the start of the down/up mouse so we know which <span> tag's we are 
            //using (using this id='1' we are encoding here)
            var span = this.createTokenSpan(tok.startIdx, tok.endIdx, tok.ordinal, this.escapeHtml(tok.text));//'<span class="c0172" data-endidx="' +  tok.endIdx + '" data-startidx="' +  tok.startIdx + '" id="' + tok.ordinal + '">' + this.escapeHtml(tok.text) + ' </span>'
            // if we are in "marking mode" (e.g. we are running over an entity, not just a regular word token),
            // add to the markingHtml array, else, just add to the overall html array.
            if (isMarking) {
                markingHtml.push(span)
            } else {
                html.push(span)
            }
        }
        /*if (markingHtml.length > 0 && obj.tokens[obj.tokens.length-1].text!='.') {
            html.push(this.createMarkHtml(markingHtml.join(""), ent, obj))
        }*/
        html.push('</div>')

        return html.join("")
    },
    /**
     * Create a span for one of the word tokens. Embeds start and end index to be used later to record the JSON data for extraction training.
     */
    createTokenSpan: function(startIdx, endIdx, ordinal, text) {
        var span = '<span class="c0172" data-endidx="' +  endIdx + '" data-startidx="' + startIdx + '" id="' + ordinal + '">' + text + ' </span>'
        return span;
    },
    entities : {},
    entityMap : null,
    /**
     * From the REST Extraction API call,   We want to first cache the entity extraction's
     * startIdx's so they can be used to compare against individual word tokens during rendering
     * to decide when to start highlighting the text (to show the "Extraction" on the UI as highlighted)
     */
    cacheEntities: function(entitiesArr) {
        if (!this.entityMap) this.populateEntityMap();
        this.entities = {}
        for (var i=0;i<entitiesArr.length;i++) {
          var entity = entitiesArr[i]
          this.entities[entity.startIdx] = entity
        }
    },
    /**
     * When highlighting the entity on UI, we want to abbreviate the Extraction label's below
     * so they longer names don't take up too much screen real-estate on the UI
     */
	//Rajeesh 4/11/19 Added skills.
    populateEntityMap: function() {
        this.entityMap = {};
        this.entityMap["CURRENT_EMPLOYER_ORG"] = "CUR EMPLYR"    
        this.entityMap["CUR EMPLYR"] = "CURRENT_EMPLOYER_ORG"
        this.entityMap["CURRENT_JT"] = "CUR JT"    
        this.entityMap["CUR JT"] = "CURRENT_JT"    
        this.entityMap["LIVING_LOCATION"] = "LIVING"
        this.entityMap["LIVING"] = "LIVING_LOCATION"
        this.entityMap["RELOC_LOCATION"] = "RELOC"
        this.entityMap["RELOC"] = "RELOC_LOCATION"
        this.entityMap["DESIRED_HOURLY_RATE"] = "DESIR RT"
        this.entityMap["DESIR RT"] = "DESIRED_HOURLY_RATE"
        this.entityMap["CURRENT_HOURLY_RATE"] = "CUR RT"
        this.entityMap["CUR RT"] = "CURRENT_HOURLY_RATE"
        this.entityMap["EXPECTED_END"] = "END DT"
        this.entityMap["END DT"] = "EXPECTED_END"
		this.entityMap["SKILLS"] = "SKILLS"
    },
    escapeHtml: function(unsafe) {
        return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            //.replace(/"/g, "&quot;")
            //.replace(/'/g, "&#039;");
    },
    /**
     * Wrap the entity extraction <span>s  with <mark> html so the set of word tokens/spans appears highlighted on UI
     */
    createMarkHtml: function(spanHtml, ent, obj) {
      var markStyle = ''
      if (this.doesEntityTypeSupportNormalization(ent.label)) {
        // normalized entities dropdowns don't show below the location if we allow pre-wrap stype for white-space
        // so adjusting just for the geo entities.
        markStyle = 'style="white-space:nowrap;"'
        //for (var i=0;i<obj.normalizedEntities.length;i++) {
        //  var normEntity = obj.normalizedEntities[i]          
        //}
        
        
      }
	  //Rajeesh trying to correctly map skill items->multiple values
	  var markHtml='';
	  var skillName ='';
	  if(ent.label === "SKILLS"){
			//console.log("---"+ent.resolvedEntity.split(" ").join(""));
			//markHtml = '<mark  data-entitytype="' + ent.label + '" id="mark_' + ent.label+'_'+ent.resolvedEntity+ '" class="c0193 c0174" ' + markStyle + '>' + spanHtml + '<span class="c0197 c0196">' + this.entityMap[ent.label] + '<span class="c0175">×</span><span class="non-move-x" id="x_' + ent.label+'_'+ent.resolvedEntity + '" >leave same <span style="margin-left:6px;color:red;">x</span> </span></span></mark>'
			markHtml = '<mark  data-entitytype="' + ent.label + '" id="mark_' + ent.label+'_'+ent.resolvedEntity.split(" ").join("")+ '" class="c0193 c0174" ' + markStyle + '>' + spanHtml + '<span class="c0197 c0196">' + this.entityMap[ent.label] + '<span class="c0175">×</span><span class="non-move-x" id="x_' + ent.label+'_'+ent.resolvedEntity.split(" ").join("")+ '" >leave same <span style="margin-left:6px;color:red;">x</span> </span></span></mark>'
			skillName = ent.label+'_'+ent.resolvedEntity.split(" ").join("");
			this.parsedSkills.push(skillName);//Need these values in other places.
	  }
	  else{

		markHtml =  '<mark  data-entitytype="' + ent.label + '" id="mark_' + ent.label + '" class="c0193 c0174" ' + markStyle + '>' + spanHtml + '<span class="c0197 c0196">' + this.entityMap[ent.label] + '<span class="c0175">×</span><span class="non-move-x" id="x_' + ent.label + '" >leave same <span style="margin-left:6px;color:red;">x</span> </span></span></mark>'

	  }
      //console.log('marked')
      //console.log(markHtml)
      return markHtml;
    },
    /**
     * Render a set of dropdowns w/ the normalized entity, e.g. the exact City/State/Country (and approx. distance/mileage from the )
     * current candidate's location underneath geography extractions to allow user to see/change the exact
     * location that this entity is resolved to via google places API. Let's user choose from ambiguous city mentions in 
     * notes, like Columbia could mean Columbia MD, or Columbia SC
     */
    //Rajeesh 4/10/2019 Modified for skills - multi values.
    processNormalizedEntityLabelling: function(obj) {
      var normEntNew = obj.normalizedEntities;
      for (var i=0;i<obj.normalizedEntities.length;i++) {

          var normEntity = obj.normalizedEntities[i];
		  var locEl;
          //de-dupe "otherPredictions"
		  //Rajesh skills related changes
		  if(normEntity.label ==="SKILLS"){
			locEl = document.getElementById("mark_" + normEntity.label+"_"+normEntity.resolvedEntity);
		  }
		  else{

			locEl = document.getElementById("mark_" + normEntity.label);
		  }
          // add the jsonResponse

          if(normEntity.label != "SKILLS") {
              var div = this.createNormalizedEntityDropdown(locEl, normEntity.label, normEntity.jsonResponse.results[0]);
              var selectHtml = ['<select id="select_' + normEntity.label + '" class="geo_class">']
              for (var n = 0; n < normEntity.jsonResponse.results.length; n++) {
                  this.normalizedNamesMap.push(normEntity.jsonResponse.results[n]);
                  this.createNormalizedSelectElement(normEntity.jsonResponse.results[n], selectHtml);
              }
              selectHtml.push('</select>');

              div.innerHTML = selectHtml.join('');
              this.selectNodes.push(div);
              //document.body.appendChild(div);
			  var rootDiv = document.getElementById("enrichedNotes");
			  rootDiv.appendChild(div);
          }

      }
      var skillEntities = obj.entities.filter(function(p){
          return p.label == "SKILLS";
      });
      var skillNormEntities = obj.normalizedEntities.filter(function(q){
          return q.label == "SKILLS";
      });
      /*
      Array.find--->
      */
      var modArray =[];
      var item;
      for(var p=0;p<skillEntities.length;p++){
          for(var q=0;q<skillNormEntities.length;q++){
              if(skillNormEntities[q].resolvedEntity==skillEntities[p].resolvedEntity){
                  item = skillNormEntities[q];
                  item.startIdx = skillEntities[p].startIdx;
                  modArray.push(item);
              }

          }

      }
      //console.log(JSON.stringify(modArray));
      var testElms = document.getElementsByClassName("c0193 c0174");
      var test = Array.from(testElms);//skillNormEntities
      for(var n=0;n<test.length;n++){
      //    if(test[n].id==="mark_SKILLS"){
	      if(test[n].id.includes("mark_SKILLS")){
              for(var m=0;m<skillNormEntities.length;m++){
                  if(skillNormEntities[m].startIdx == test[n].children[0].dataset.startidx){
                      //console.log('inside matching loop');
                      //Rajeesh 
					  //var div = this.createNormalizedEntityDropdown(test[n], skillNormEntities[m].label, skillNormEntities[m].jsonResponse.results[0]);
					  var div = this.createNormalizedEntityDropdown(test[n], skillNormEntities[m].label+"_"+skillNormEntities[m].resolvedEntity, skillNormEntities[m].jsonResponse.results[0]);
                      //var selectHtml = ['<select id="select_' + normEntity.label + '" class="geo_class">']
                      var selectHtml = ['<select id="select_' + skillNormEntities[m].label+"_"+skillNormEntities[m].resolvedEntity.replace(/\s+/g, '') + '" class="geo_class">']
                      for (var q = 0; q < skillNormEntities[m].jsonResponse.results.length; q++) {
                          this.normalizedNamesMap.push(skillNormEntities[m].jsonResponse.results[q]);
                          this.createNormalizedSelectElement(skillNormEntities[m].jsonResponse.results[q], selectHtml);
                      }
                      selectHtml.push('</select>');
                      this.appendHTML(div,selectHtml);
                  }
              }
          }
      }
     
  },
  //Rajeesh 4/10/2019 for skills- refactoring.
  appendHTML: function(div,selectHtml){
      //Rajeesh trying to move a bit of code outside the prev block to call it repeatedly.
         div.innerHTML = selectHtml.join('');
          this.selectNodes.push(div);
		  var rootDiv = document.getElementById("enrichedNotes");
          //document.body.appendChild(div);
		  rootDiv.appendChild(div);
    },
    /**
     * Create the select elements for the normalized entity dropdown
     */
    createNormalizedSelectElement: function(result, selectHtml) {       
        //if (result.miles && !isNaN(result.miles) && result.miles!="10000") {          
        //  selectHtml.push('<option>' + result.normalized_name + ' (' + parseInt(result.miles) + ' mi.)' + '</option>')          
        //} else {
          selectHtml.push('<option>' + result.normalized_name + '</option>')
        //}
    },
    /*
    * When user manually annotates and creates a new highlight span, the UI shifts, therefore
    * we need to reposition all the dropdowns to follow along with the UI shift.
    */
    repositionAllNormalizedEntities: function() {
        //Rajeesh adding the skills array.
		var supportedEntitiesNew = this.supportedNormalizedEntities.concat(this.parsedSkills);
		
		//for (var i=0;i<this.supportedNormalizedEntities.length;i++) {
		for (var i=0;i<supportedEntitiesNew.length;i++) {
            
            //var selectNodeDiv = document.getElementById("select_node_div_"  + this.supportedNormalizedEntities[i]);
            //var highlightSpan = document.getElementById("mark_" + this.supportedNormalizedEntities[i])
			var selectNodeDiv = document.getElementById("select_node_div_"  + supportedEntitiesNew[i]);
            var highlightSpan = document.getElementById("mark_" + supportedEntitiesNew[i]);
			if (selectNodeDiv && highlightSpan) {
                this.repositionNormalizedDropdown(highlightSpan, selectNodeDiv);
            }
			
        }
    
    },
    /**
     * Reposition the dropdown to appear just under the yellow highlghted span
     */
    repositionNormalizedDropdown: function(highlightSpan, div) {
        var bb = highlightSpan.getBoundingClientRect();     
        var enrichedNotesBB = document.getElementById("enrichedNotes").getBoundingClientRect();     
        var scrollTop = document.getElementById("enrichedNotes").scrollTop;   
        //var oneheaderHeight = parseFloat(document.getElementById("oneHeader").getBoundingClientRect().y); 
        //var tlpHeaderHeight = parseFloat(document.getElementById("tlp-header").getBoundingClientRect().y); 
        var headerHeights = 25;
        var x = parseFloat(bb.x);
        var y = parseFloat(bb.y)  + scrollTop + headerHeights - parseFloat(enrichedNotesBB.y);// + window.pageYOffset;// + this.IN_PLACE_EDIT_Y_OFFSET;
        div.style.left = (x-37) + 'px';//(position.x + 28) +  'px'             
        div.style.top = y + 'px';//(position.y + 32 + 534) + 'px'
    },
     repositionNormalizedDropdownOriginal: function(highlightSpan, div) {
        var bb = highlightSpan.getBoundingClientRect();     
        var enrichedNotesBB = document.getElementById("enrichedNotes").getBoundingClientRect();     
        var scrollTop = document.getElementById("enrichedNotes").scrollTop;   
        //var oneheaderHeight = parseFloat(document.getElementById("oneHeader").getBoundingClientRect().y); 
        //var tlpHeaderHeight = parseFloat(document.getElementById("tlp-header").getBoundingClientRect().y); 
        var headerHeights = 25;
        var x = parseFloat(bb.x);
        var y = parseFloat(bb.y)  + scrollTop + headerHeights - parseFloat(enrichedNotesBB.y);// + window.pageYOffset;// + this.IN_PLACE_EDIT_Y_OFFSET;
        div.style.left = (x-47) + 'px';//(position.x + 28) +  'px'             
        div.style.top = y + 'px';//(position.y + 32 + 534) + 'px'
    },
    /**
     * Paint the normalized entities dropdowns using absolute positioning right underneat the extraction <mark> tags
     * TODO: This does not deal w/ browser resize
     */
    createNormalizedEntityDropdown: function(locEl, label, result) {
        
        if (locEl) {
           
            var div = document.createElement("DIV");       
            div.id = "select_node_div_"  + label;
            div.style.position = 'absolute';
            div.style.zIndex = 1000000;
            this.repositionNormalizedDropdown(locEl, div);            
            div.style.border = "1px solid #ffe184"
            div.style.fontSize = ".8em";
            // let's show the "leave same" message only for living location. This indicates that we don't
            // have enough info to move.
            // Below is logic to identify whether we want to consider "leave same", e.g. won't update backend
            if(!$A.util.isEmpty(result)){
				if (label == "LIVING_LOCATION" && ((result.distance && result.distance < 60)|| result.overlapping_geo=='Y')) {              
					document.getElementById("x_" + label).style.opacity = 1;
					document.getElementById("x_" + label).addEventListener('click', function(ev) {
						document.getElementById("x_" + label).style.opacity = 0;
						document.getElementById("x_" + label).dataset.removed_leave_same = true;
						ev.stopPropagation();
					});			  
				  //div.style.textDecoration = '';//'line-through'
				}
			}
            div.style.color = '#583fcf'          
            return div;
            
            //document.getElementById("enrichedNotes").appendChild(div);  
          }
    },
    // an array to hide all of the <select> element nodes
    selectNodes: [],
    // an array of the json representations of the normalized entities mapping to each extraction, e.g. for Geo: JSON address responses from Google. these will be send back when user chooses an item
    // from the select dropdown. The text in the dropdown will either hit an attribute named "normalized_name" that is used in the dropdown, and can be used in 
    // the method findNormalizedNameNodeFromDescription() below to find this corresponding json structure to send to server to use to update SFDC
    normalizedNamesMap : [],
    /**
     * Based on the <select> selection for the normalized entity, we will traverse the json responses, and when we find the corresponding
     * JSON (using "normalized_name" member) that maps to the user selection, we will give this back to the caller. The caller will use this to send to the server
     * to give them a definitive/structured response for normalized entity to update SFDC.
     */
    findNormalizedNameNodeFromDescription: function (desc) {
        
        for (var i=0;i<this.normalizedNamesMap.length;i++) {
            var ent = this.normalizedNamesMap[i];
            if (ent["normalized_name"]==desc) {
                return ent;
            }
        }
            
        
        
        // if not found, now look for 
        return null;
    },
    // when we reset the UI, we want to remove these no-longer-needed <select> normalized entities from UI , e.g. gargbage collection
    // also want to remove the address JSON responses
    clearSelectNodesAndNormalizedNamesMap : function() {
        for (var i=0;i<this.selectNodes.length;i++) {
			try{
				document.getElementById("enrichedNotes").removeChild(this.selectNodes[i]);
			}
			catch(e){
				console.log('exception while removing node'+e.message);
			}
        }
        this.selectNodes = [];
        this.normalizedNamesMap = [];
    },   
    /*
    * Some weird DOM behavior, need to remove the mark nodes before re-adding.. otherwise getting null DOM references
    */
    clearMarkNodes: function() {
        for (var i=0;i<this.supportedNormalizedEntities.length;i++) {
            if (document.getElementById("select_node_div_"+ this.supportedNormalizedEntities[i])) {
                document.getElementById("enrichedNotes").removeChild(document.getElementById("select_node_div_"+ this.supportedNormalizedEntities[i]));
            }
            if (document.getElementById("mark_"+ this.supportedNormalizedEntities[i])) {
                document.getElementById("mark_"+ this.supportedNormalizedEntities[i]).parentNode.removeChild(document.getElementById("mark_"+ this.supportedNormalizedEntities[i]));
            }
        }
    },
    /**
     * When saving the user-verified Extraction data on the screen, we just want the UI (after user
     * has simply accepted the Model's output, or manually corrected the output through deleting entities, or newly
     * annotating entities. We want to find these <mark> tags on th UI, and add then to an extractions object
     * which we will send down to Apex to be saved to the corresponding fields in Salesforce.
     *      
     */
    retrieveVerifiedExtractions: function(includeSameLivingLocation) {
		var skillIndex=0;


        // validate to esure we don't have multiple extractions for same entity type
        var marks = document.getElementsByClassName("c0193");
/*		console.log('marked entities----'+marks);
        if (marks.length == 0) {
            alert("You must select at least one entity to Verify");
            return;
        }
*/        
        var extractions = {};
        //console.log("marks.length:" + marks.length)
        for (var z=0;z<marks.length;z++) {
        //console.log('z:' + z);
            var entType = marks[z].dataset.entitytype;
            var userannotated = marks[z].dataset.userannotated ? "1" : "0";
           // console.log("entType:" + entType + marks[z].parentNode)
            //if (!extractions[entType]) {
                var el = marks[z];            
                var entityStr = [];
                var startIdx = null;
                var endIdx = null;
                for (var i=0;i<el.childNodes.length;i++) {
                    var child = el.childNodes[i];
                    if (child.nodeName.toUpperCase() == "SPAN" && child.id) {
                        entityStr.push(child.innerText.trim());
                        if (startIdx==null) {
                            startIdx = parseInt(child.dataset.startidx);
                        } 
                        if (endIdx == null || parseInt(child.dataset.endidx) > endIdx) {
                            endIdx = parseInt(child.dataset.endidx);
                        }
                    }
                }
                //spaces are needed between spans (with an id attribute) 
				//Rajeesh skills need update
				var removed_leave_same;
				if(entType ==="SKILLS"){
					//Need to implement this.
					removed_leave_same = document.getElementById("x_" + entType+'_'+entityStr.join("").toLowerCase()).dataset && document.getElementById("x_" + entType+'_'+entityStr.join("").toLowerCase()).dataset.removed_leave_same;
				}
				else{
					removed_leave_same = document.getElementById("x_" + entType).dataset && document.getElementById("x_" + entType).dataset.removed_leave_same;
				}
				//Rajeesh change for skill - add multi items in array.
				if(entType !="SKILLS"){
					extractions[entType] = this.createExtractionsNode(entityStr.join(" ").trim(), entType, startIdx, endIdx, userannotated, removed_leave_same);  
				}
				else {
					if(skillIndex==0){
							extractions.SKILLS = [];
					}
					var skillEnt = this.createExtractionsNode(entityStr.join(" ").trim(), entType, startIdx, endIdx, userannotated, removed_leave_same);
					extractions.SKILLS.push(skillEnt); 
				}
				//console.log('extractions--->'+JSON.stringify(extractions));
				if (this.doesEntityTypeSupportNormalization(entType)) {
                  if(entType != "SKILLS"){    
					if (!document.getElementById("select_" + entType)) {                        
						alert (entType + " extraction not  normalized/not working properly. TODO!");                        
						delete  extractions[entType];
					} else if(!includeSameLivingLocation && document.getElementById("x_" + entType).style.opacity > .9) {
						console.log("don't save, since " +  entType + " didn't really change");                    
						delete  extractions[entType];
					} else {
                        
						extractions[entType].unnormalizedResolvedEntity = extractions[entType].resolvedEntity.trim();
						var resolvedEntStr = document.getElementById("select_" + entType).value.trim(); 
						var resolvedEnt = this.findNormalizedNameNodeFromDescription(resolvedEntStr);
						if (!resolvedEnt) resolvedEnt = resolvedEntStr;
						extractions[entType].resolvedEntity = resolvedEnt;        
					}
				  }
				  else{
						var skillItem = {};
						extractions[entType][skillIndex].unnormalizedResolvedEntity = extractions[entType][skillIndex].resolvedEntity.trim();
						//var resolvedEntStr = document.getElementById("select_" + extractions[entType][skillIndex].resolvedEntity.replace(/\s+/g, '').toLowerCase()).value.trim(); 
						var resolvedEntStr = document.getElementById("select_" + entType+"_"+extractions[entType][skillIndex].resolvedEntity.replace(/\s+/g, '').toLowerCase()).value.trim(); 
						var resolvedEnt = this.findNormalizedNameNodeFromDescription(resolvedEntStr);
						if (!resolvedEnt) resolvedEnt = resolvedEntStr;
						
						extractions[entType][skillIndex].resolvedEntity = resolvedEnt; 
						skillIndex++; 
				  }
				}
        }
        return extractions;
    },
    createExtractionsNode: function(text, entType, startIdx, endIdx, userannotated, removed_leave_same) {
        return {"resolvedEntity" : text, "label": entType, "startIdx" : startIdx, "endIdx" : endIdx, "userannotated" : userannotated, "removed_leave_same" : removed_leave_same};
    },
    /**
     * When user clicks verify note, we want to gather up the extractions that are currently on the UI, 
     * including the correct/selected normalized entities from the dropdowns on the screen, as well as the <mark>'d CURRENT_JT
     * (for now, more Entity labels will be added in future) and send them down to Apex along with this contactId
     * so Apex can save them in the corresponding fields for this contact in Salesforce (in this case, Contact.MailingCity, .etc. Contact.Title)
     */
    handleVerifyNoteTask: function(component) {
        var rawText = component.get("v.noteText");
        var action = component.get("c.updateContactWithExtraction");         
        var contactId = component.get("v.recId");
       
        var params = {"contactId" : contactId};
        var extractions = this.retrieveVerifiedExtractions();
        if (!extractions) return;
		//Rajeesh set verified extractions for future use.
		component.set('v.verifiedExtr',extractions);        
        params["extractions"] = JSON.stringify(extractions);
        
//console.log('----Verified Exractions before update------'+JSON.stringify(extractions));
       //Rajesh removing updates, and keeping everything in a single update at the end.
	    //action.setParams(params); 
         // Create a callback that is executed after 
         // the server-side action returns
         //action.setCallback(this, function(response) {             
           //  var state = response.getState();
             //if (state === "SUCCESS") {                 


				this.closeUtilityBar(component);
             //}          
         //});
        //$A.enqueueAction(action);

         // now log the data a Google Pub/Sub (which will push to a file) for further refining/retraining
         // of the underlying model given this new "judgment" being made here as a golden-record
//Rajeesh remove this setTimeout.
/*        var me = this;
         setTimeout(function() {
             try {
                me.logTrainingData(component,me.retrieveVerifiedExtractions(true), rawText, me);
             } catch (e) {}
         }, 500);
 */         
        

    },
    /**
     * Log the training data to Google pub/sub
     */
    logTrainingData: function(component, extractions, text, me, rejectedFlag) {
        var action = component.get("c.logExtractionModelTrainingData");             
        
        
        action.setParams({"trainingJson" : me.createTrainingJsonResponse(component,extractions, text, me, rejectedFlag)}); 
//console.log('training:text'+text);        
//console.log('training:me'+me);
//console.log('training:rejectedFlag'+rejectedFlag);
        action.setCallback(this, function(response) {             
           var state = response.getState();
           if (state === "SUCCESS") {                 
			console.log('success-training data sent');
           }
       });
       $A.enqueueAction(action);
    },
     /**
     * Create a JSON string that is used to retrain the model. Format is in jsonl format specific to the
     * prodigy/spacy python tool for machine learning
     * @param {} me 
     */
    createTrainingJsonResponse: function(component, extractions, rawText, me, rejectedFlag) {
        var objStr =  me.LAST_RAW_EXTRACTION_RESPONSE;
        rejectedFlag = !rejectedFlag ? false : true;
        var obj = JSON.parse(objStr);        
        var trObj = {};
        //if (1==1) trObj = obj;

        
        
        trObj.contactId = component.get("v.recId");
        trObj.currentUserId =me.CURRENT_USER_ID;        
        trObj.currentDate = me.formatDate(new Date());
        //alert(trObj.currentUserId + ":" + trObj.currentDate);
        trObj.meta = {"score": 1, "rejectedFlag:" : rejectedFlag};
        trObj.text = rawText;
        trObj._input_hash = me.hashCode(rawText);
        trObj._task_hash = trObj._input_hash;
        trObj.tokens = [];
        for (var i=0;i<obj.tokens.length;i++) {
            var token = obj.tokens[i];
            trObj.tokens.push({"text" : token.text, "start" : token.startIdx, "end" : token.endIdx, "id" : token.ordinal});
        }
        trObj.spans = [];

        for (var extraction in extractions) {
            var entity = extractions[extraction];
            var token_start = 0;
            for (var z=0;z<obj.tokens.length;z++) {
                var token = obj.tokens[z];
                if (token.startIdx == entity.startIdx) {
                    token_start = token.ordinal;                    
                }
            }

            //console.log("token_start:" + token_start);            
            // for LIVING_LOCATION and RELOC_LOCATION, we want to train on the unnormalized entry
			//Rajeesh Skills is an array.
			if(extraction ==="SKILLS")
			{
				console.log('inside skill extractions.has an array-need diff logic');
				var skillArr = entity;
				var trText;
				var token_end;
				for(var idx=0;idx<skillArr.length;idx++){
					trText= skillArr[idx].unnormalizedResolvedEntity ? skillArr[idx].unnormalizedResolvedEntity : skillArr[idx].resolvedEntity;
					token_end = token_start + (trText.split(' ').length-1);
					trObj.spans.push({"label" : skillArr[idx].label, "text" : trText, "start" : skillArr[idx].startIdx, "end" : skillArr[idx].endIdx, "token_start" : token_start, "token_end" : token_end, "userannotated" : skillArr[idx].userannotated, "removed_leave_same" : skillArr[idx].removed_leave_same});

				}
			}
			else{

				var trText = entity.unnormalizedResolvedEntity ? entity.unnormalizedResolvedEntity : entity.resolvedEntity;
				//for now, this is a little naive (always assumes a space ' ' is the splitting token)
				// to set the end date, but we will do it for now
				var token_end = token_start + (trText.split(' ').length-1);
				trObj.spans.push({"label" : entity.label, "text" : trText, "start" : entity.startIdx, "end" : entity.endIdx, "token_start" : token_start, "token_end" : token_end, "userannotated" : entity.userannotated, "removed_leave_same" : entity.removed_leave_same});
			}
        }
        trObj.answer = "accept";
        var retStr =  JSON.stringify(trObj);        
        //console.log("training obj")
        //console.log(retStr)
        return retStr;
    },
    /**
     * Poor mans hash code. Used for Training data JSON creation (not critical)
     * @param {} str 
     */
    hashCode: function(str) {
        var hash = 0;
        if (str.length == 0) {
            return hash;
        }
        for (var i = 0; i < str.length; i++) {
            var char = str.charCodeAt(i);
            hash = ((hash<<5)-hash)+char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    },    
    /**
     * When user clicks down on the extraction UI, we want to mark a "begin" span that the user 
     * clicked on, and when user mouses up, mark the "end" span (by using the id's , which have been encoded on the server 
     * response, and subsequently placed in the id='' attribute of the spans). These markers will be used on the mouse up event 
     * "annotateText" to repaint the UI (adding the <mark> tag, etc.  to make look highlighted on screen, see renderNotesResponse() method
     * Also, when user clicks the X that is shown when hovering over an extraction, user can click this X firing the
     * deleteAnnotation() method to remove the <mark> tag's, etc. surrounding this element and replacing it with the "naked" <span> content 
     * of these word tokens (unhighlighted)
     */
    addAnnotationListener: function(annotationAction, node) {
        
        var classname = document.getElementsByClassName("c0172");
        var addAnnotationArr = Array.from(classname);
        if (annotationAction && annotationAction == this.ANNOTATION_DELETED_ACTION) {
            // only want to do this one node
            addAnnotationArr = node;
        }
        
        var me = this;        
        
        if (!annotationAction || annotationAction == this.ANNOTATION_DELETED_ACTION) {
            //console.log('adding mousedown listener')
            addAnnotationArr.forEach(function(element) {
                element.addEventListener('mousedown', function() {if(me.findParentByNodeName(this, "mark")) return;me.STARTING_NODE_ID = element.id;});
                element.addEventListener('mouseup', function() {me.ENDING_NODE_ID = element.id;me.annotateTextChooseEntityType(me);});
            });
        }

        var classname = document.getElementsByClassName("c0175");
        var deleteAnnotationArr = Array.from(classname);
        if (annotationAction && annotationAction == this.ANNOTATION_ADDED_ACTION) {
            // only want to do this one node
            deleteAnnotationArr = Array.from(node.getElementsByClassName("c0175"));
        }
        if (!annotationAction || annotationAction == this.ANNOTATION_ADDED_ACTION) {
            deleteAnnotationArr.forEach(function(element) {                
                element.addEventListener('click', function() {me.deleteAnnotation(me, element)});			  
            });
        }
    },    
    STARTING_NODE_ID:  null,
    ENDING_NODE_ID : null,
    ANNOTATION_DELETED_ACTION : 1,
    ANNOTATION_ADDED_ACTION : 2,
    /**
     * Before we actually annotate the text on the screen we need to ask the user what entity
     * type their selection represents.
     */
    annotateTextChooseEntityType : function(me) {
        if (!this.STARTING_NODE_ID) return;
        var annoTypeEl = document.getElementById("chooseAnnotationType");
        annoTypeEl.style.display = "block";
        var bb = document.getElementById("enrichedNotes").getBoundingClientRect();
        annoTypeEl.x = parseFloat(bb.x) + 'px';
        annoTypeEl.y = parseFloat(bb.y) +  window.pageYOffset + 'px'; 
        me.hideShowSelects(true);
        
    },
    /**
     * use the startIdx/endIdx marked by the mousedown/up events on UI to iterate the spans that need highlighting. 
     * first delete these "naked" (unhighlighted) <span>'s from the DOM, then re-add then surrounded by the appropriate
     * <mark> html that is required on the UI to make look highlighted (and also required on the UI to be used later for the
     * handleVerifyNoteTask() method to find the final/verified extractions from the user and save them to SFDC.)
     */
    "annotateText":function(component,me, entityObj) {
		var entityType =entityObj.value;
		document.getElementById("chooseAnnotationType").style.display ="none"; 
		me.hideShowSelects(false);
		var startIdx =parseInt(this.STARTING_NODE_ID);
		var endIdx =parseInt(this.ENDING_NODE_ID);
		var startTokenIdx =null;
		var endTokenIdx =null;
		var removedHTML = [];
		var priorNode =document.getElementById("" + (startIdx-1));
		var nextNode = !priorNode ?document.getElementById("" + (endIdx+1)) :null;
		var resolvedEntitySpans =[];
		for (var i=startIdx;i<=endIdx;i++) {
			removedHTML.push(document.getElementById("" +i).outerHTML);
			var tokenEl =document.getElementById("" +i);
			if (startTokenIdx==null) {
				startTokenIdx = parseInt(tokenEl.dataset.startidx);
			}
			if (endTokenIdx ==null || parseInt(tokenEl.dataset.endidx) >endTokenIdx) {
				endTokenIdx = parseInt(tokenEl.dataset.endidx);
			}
			resolvedEntitySpans.push(tokenEl.outerHTML)
			document.getElementById("" +i).parentNode.removeChild(tokenEl);
		}
		var resolvedEntity =removedHTML.join("");
		//var resolvedEntitySpan = me.createTokenSpan(startTokenIdx, endTokenIdx, -1, resolvedEntity);
		var resolvedEntitySpan =resolvedEntitySpans.join("");
		console.log("resolvedEntitySpan:" +resolvedEntitySpan);
		var ent =me.entityMap[entityType];//"CUR EMPLYR";
//Rajeesh alter code for skills -multi values
/*if(ent.label === "SKILLS"){
	markHtml = '<mark  data-entitytype="' + ent.label + '" id="mark_' + ent.label+'_'+ent.resolvedEntity+ '" class="c0193 c0174" ' + markStyle + '>' + spanHtml + '<span class="c0197 c0196">' + this.entityMap[ent.label] + '<span class="c0175">×</span><span class="non-move-x" id="x_' + ent.label+'_'+ent.resolvedEntity + '" >leave same <span style="margin-left:6px;color:red;">x</span> </span></span></mark>'
}*/
		var markerHtml = '';
		if(ent === "SKILLS"){
			//var test = document.getElementById("" + (startIdx));
			var skillVal = tokenEl.innerText.trim().toLowerCase();
			markerHtml ='<mark data-entitytype="' + entityType + '" data-userannotated="1" id="mark_' +entityType +'_'+skillVal+ '" class="c0193 c0174">' +resolvedEntitySpan + '<span class="c0197 c0196">' + ent + '   <span class="c0175">×</span><span class="non-move-x" id="x_' +entityType +'_'+skillVal+ '" >leave same</span>' +'</span>' + '</mark>';
			//markerHtml ='<mark data-entitytype="' + entityType + '" data-userannotated="1" id="mark_' +entityType + '" class="c0193 c0174">' +resolvedEntitySpan + '<span class="c0197 c0196">' + ent + '   <span class="c0175">×</span><span class="non-move-x" id="x_' +entityType + '" >leave same</span>' +'</span>' + '</mark>';
		}
		else{
			markerHtml ='<mark data-entitytype="' + entityType + '" data-userannotated="1" id="mark_' +entityType + '" class="c0193 c0174">' +resolvedEntitySpan + '<span class="c0197 c0196">' + ent + '   <span class="c0175">×</span><span class="non-move-x" id="x_' +entityType + '" >leave same</span>' +'</span>' + '</mark>';
		}
		if (priorNode) {
			priorNode.insertAdjacentHTML('afterend',markerHtml); 
		} else {
			nextNode.insertAdjacentHTML('beforebegin',markerHtml); 
		} 
		//Rajeesh skill changes
		var markNode;
		if(ent==="SKILLS"){
			markNode =document.getElementById('mark_' +entityType+'_'+skillVal); 
		}
		else{

			markNode =document.getElementById('mark_' +entityType); 
		}

		me.addAnnotationListener(this.ANNOTATION_ADDED_ACTION,markNode);

		if (this.doesEntityTypeSupportNormalization(entityType)) {

		var contactId =component.get("v.recId");

		this.getNormalizedEntity(component,resolvedEntity,entityType, contactId );

		}

		this.STARTING_NODE_ID =null;

		this.ENDING_NODE_ID =null;

	},


    /**
     * Dumb bug w/ MS Windows that allows <select> elements to bleed through Div's (even w/ proper z-index). So need to show.hide
     * while the Select Annotation Type dialog is presented
     */
    hideShowSelects: function(hideMe) {
        var display = hideMe ? 'none' : 'inline';
        for (var i=0;i<this.supportedNormalizedEntities.length;i++) {
            if (document.getElementById('select_' + this.supportedNormalizedEntities[i])) {
                document.getElementById('select_' + this.supportedNormalizedEntities[i]).style.display = display;        
            }
        }        
    },
    /**
     * Delete the <mark> HTML nodes that highlights the annotation, and replace them 
     * with the "naked" <spans> . Just doing this on the UI will also tell handleVerifyNoteTask()
     * to no longer save this extraction as an approved/verified extraction just simply by changing the UI
     * (since handleVerifyNoteTask() is driven by <mark> tags on the UI as the marker for verified extractions)
     */
     "deleteAnnotation":function(me,el) { 
		var markNode =me.findParentByNodeName(el,"mark");
		// get the id's of the child spans
		var childSpanIds = [];
		if(!$A.util.isEmpty(markNode)){
			for (var i=0;i<markNode.childNodes.length;i++)
			{
				//Rajeesh null check.
				if(!$A.util.isEmpty(markNode.childNodes[i])){
					if (markNode.childNodes[i].id) {
						childSpanIds.push(markNode.childNodes[i].id);
					}
				}
			}
			// delete the Location select box as well if it exists.
			var entType = markNode.id.replace("mark_","");

			//Rajeesh skills->
			var skillLabel = entType.split("_");//0 - SKILLS 1-skillname ex. java
			
			var item;
			skillLabel[0]==="SKILLS"?item=skillLabel[0]:item=entType;
			//if (this.doesEntityTypeSupportNormalization(entType)) {
			//if (this.doesEntityTypeSupportNormalization(skillLabel[0])) {
			if (this.doesEntityTypeSupportNormalization(item)) {
				try{
					document.getElementById("enrichedNotes").removeChild(document.getElementById("select_node_div_"+ entType)); 
				}
				catch(e){
					console.log('exception while removing element'+e.message);
				}
				var selects =document.getElementsByClassName("geo_class");
				for (var i=0;i<selects.length;i++) {
					if (selects[i].id.indexOf(entType)!=-1)
					{
					selects[i].style.display = 'none';
					}
				}
			}

			var priorNode = markNode.previousSibling;
			var nextNode = !priorNode ?markNode.nextSibling :null;
			//first delete last node
			var xNode =markNode.lastChild;
			xNode.parentNode.removeChild(xNode);
			var unannotatedNodeHTML =markNode.innerHTML;
			markNode.parentNode.removeChild(markNode);
			if (priorNode) {
				priorNode.insertAdjacentHTML('afterend',
				unannotatedNodeHTML); 
			} else { 
				nextNode.insertAdjacentHTML('beforebegin',
				unannotatedNodeHTML); 
			}
			// get the new nodes added
			var newlyAddedNodes = [];
			for (var i=0;i<childSpanIds.length;i++) {
				newlyAddedNodes.push(document.getElementById(childSpanIds[i]));
			} 
			me.addAnnotationListener(this.ANNOTATION_DELETED_ACTION,newlyAddedNodes);
			// when deleting annotation, sometimes UI shifts (and dropdowns need to follow along)
			me.repositionAllNormalizedEntities();

		}
	},

    /**
     * If user is entering the text, we are in showingExtractionMode = FALSE
     * (show <textarea> and Save button)
     * 
     * If user has clicked save, and an extraction is found (if not found, we simply collapse the Utility Bar)
     * we are in showingExtractionMode = TRUE which hides the <textarea> and shows a <div> that will show
     * the highlighted extractions, as well as showing the Reject/Accept buttons for user to save extraction data
     * and sync the extraction back to corresponding fields in SFDC (e.g. LIVING_LOCATION -> Contact.MailingCity, etc.)
     */
    resetUI: function(component, showingExtractionMode) {        
        component.find("saveButton1").set('v.disabled',!showingExtractionMode);
        this.clearSelectNodesAndNormalizedNamesMap();
        var msgDisplayMode = showingExtractionMode ? "inline" : "none";
        //document.getElementById("verify_msg").style.display = msgDisplayMode;     
        if (!showingExtractionMode) {

            $A.util.removeClass(component.find('noteTextArea'), 'hideMe');
            $A.util.addClass(component.find('noteTextArea'), 'showMe');            
            document.getElementById("enrichedNotes").style.display = "none";    
            
            component.find('noteTextArea').set('v.value','');
            document.getElementById("non_extraction_span").style.display = 'inline';
            document.getElementById("non_extraction_span").style.visibility = 'visible';
            document.getElementById("extraction_span").style.visibility = 'hidden'; 
            document.getElementById("extraction_span").style.display = "none";
            

        } else {

            $A.util.removeClass(component.find('noteTextArea'), 'showMe');
            $A.util.addClass(component.find('noteTextArea'), 'hideMe');            
            document.getElementById("enrichedNotes").style.display = "block";   

            document.getElementById("non_extraction_span").style.visibility = 'hidden';
            document.getElementById("non_extraction_span").style.display = 'none';
            document.getElementById("extraction_span").style.visibility = 'visible';
            document.getElementById("extraction_span").style.display = "inline";
             
        }
    },
    /**
     * Minimizy the utlity bar, and repaint the surroundin contact UI content to show 
     * any newly logged calls on the Activity Timeline
     */
    closeUtilityBar: function(component) {
        $A.util.removeClass(component.find('noteTextArea'), 'hideMe');
        $A.util.addClass(component.find('noteTextArea'), 'showMe');
        document.getElementById("enrichedNotes").style.display = "none";  
		
		                
		//Rajeesh enable N2P button in overview.
		var parentCmp = component.get('v.parentCmp');
		parentCmp.set("v.enableN2PButton",true);

		//also make sure that the reference is de-referenced before modal is open.
		component.set('v.parentCmp',null);
		
		//Rajeesh to fix remove node error 4/15/2019 for repeated usage.
		this.selectNodes = [];
        this.normalizedNamesMap = [];
				             
        //var utilityAPI = component.find("utilitybar");                
        try {
            //utilityAPI.minimizeUtility();                 
            $A.get('e.force:refreshView').fire();
            
            
    
            // now click the edit button on the G2 page:    
			//Rajeesh fire this before resetting UI as the variables are going blank after resetting?       
            this.launchEditTalentSummaryDialog(component);

			this.resetUI(component, false);
            
        } catch (e) {
			console.log('exception in close utility bar'+e.message);
		}
        
        component.destroy();        
    },
    /**
     * Launch the parent summary page that exists on the C_TalentOverview container page that contains this widget
     */
    /* Original- rajeesh altered event to add an optional parameter.*/
	launchEditTalentSummaryDialog: function(component) {
        var recordID = component.get("v.talentId");
        var contID = component.get("v.recId");
		/*Rajeesh Include all parsed information here as an object.
		  Keep the structure easy to consume.
		  Job Title: {!v.contact.Title}
		  Relocation pref: upto 4 places are allowed. But only 1 currently in exp? test this out.
		  v.record.geographicPrefs.desiredLocation[0].country, state and city

		  Plan B- leave the current updates as is. And pass note as g2comments, reloc pref for talent_preference_internal json.
		  and merge these inside new components.
		  currently relocation - is only one place.Ideally upto 4.
		  {
			
		  }
		*/
		var parsedInfo = {}; 
		parsedInfo.G2Comments = component.get('v.noteText');
		var rel = {};
		//rel.City		=	'';
		//rel.State		=	'';
		//rel.Country	=	'';
		//rel.Zip	=	'';
		var verExtr	=	component.get('v.verifiedExtr');
		if(!$A.util.isEmpty(verExtr)){
			if(!$A.util.isEmpty(verExtr.RELOC_LOCATION)){
				if(!$A.util.isEmpty(verExtr.RELOC_LOCATION.resolvedEntity)){
					if(!$A.util.isEmpty(verExtr.RELOC_LOCATION.resolvedEntity.address_components)){
						var addElements = verExtr.RELOC_LOCATION.resolvedEntity.address_components;
	//console.log('addElements json'+JSON.stringify(addElements));
						for(var i=0;i<addElements.length;i++){
							var addElmType = addElements[i]["types"];
							if(addElements[i]["types"][0] =="administrative_area_level_1"){//state
								rel.State = addElements[i]["long_name"];
							}
							if(addElements[i]["types"][0] =="locality"){//city
								rel.City = addElements[i]["long_name"];
							}
							if(addElements[i]["types"][0] =="country"){//country
								rel.Country = addElements[i]["long_name"];
							}
						}
					}
				}
			}	
		}
		var liv = {};
        //liv.City		=	'';
        //liv.State		=	'';
        //liv.Country	=	'';
        //liv.Zip	=	'';
        if(!$A.util.isEmpty(verExtr.LIVING_LOCATION)){
			if(!$A.util.isEmpty(verExtr.LIVING_LOCATION.resolvedEntity)){
				if(!$A.util.isEmpty(verExtr.LIVING_LOCATION.resolvedEntity.address_components)){
					var addAddrElements = verExtr.LIVING_LOCATION.resolvedEntity.address_components;
					for(var i=0;i<addAddrElements.length;i++){
						var addElmType = addAddrElements[i]["types"];
						if(addAddrElements[i]["types"][0] =="administrative_area_level_1"){//state
							liv.State = addAddrElements[i]["long_name"];
						}
						if(addAddrElements[i]["types"][0] =="locality"){//city
							liv.City = addAddrElements[i]["long_name"];
						}
						if(addAddrElements[i]["types"][0] =="country"){//country
							liv.Country = addAddrElements[i]["long_name"];
						}
					}
				}
			}
		}
        var jobTitle='';
        if(!$A.util.isEmpty(verExtr.CURRENT_JT)){
			if(!$A.util.isEmpty(verExtr.CURRENT_JT.resolvedEntity)){
                jobTitle = verExtr.CURRENT_JT.resolvedEntity;
            }
        }
        var desiredRate='';
        if(!$A.util.isEmpty(verExtr.DESIRED_HOURLY_RATE)){
			if(!$A.util.isEmpty(verExtr.DESIRED_HOURLY_RATE.resolvedEntity)){
                desiredRate = verExtr.DESIRED_HOURLY_RATE.resolvedEntity;
            }
        }
		if(!$A.util.isEmpty(rel)){
			parsedInfo.Relocation = rel;
		}
		if(!$A.util.isEmpty(liv)){
			parsedInfo.Living = liv;
		}
		//Rajeesh 4/11/2019 Added for skill
		if(!$A.util.isEmpty(verExtr.SKILLS)){
			var skillArray = [];
			var skillItem;
			for(var k=0;k<verExtr.SKILLS.length;k++){
				if(!$A.util.isEmpty(verExtr.SKILLS[k].resolvedEntity)){
					if(!$A.util.isEmpty(verExtr.SKILLS[k].resolvedEntity.normalized_name)){
						skillItem = verExtr.SKILLS[k].resolvedEntity.normalized_name;
						skillArray.push(skillItem);
					}
				}
			}
			if(!$A.util.isEmpty(skillArray)){
				parsedInfo.Skills = skillArray;
			}
		}
        parsedInfo.JobTitle = jobTitle;
        parsedInfo.DesiredRate = desiredRate;
        var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        urlEvent.setParams({
            "recordId": recordID,
            "recId": contID,
			"parsedInfo": parsedInfo,
            "source": "NotesParser"
        });
        urlEvent.fire(); 
    },
	launchEditTalentSummaryDialogReject: function(component) {
        var recordID = component.get("v.talentId");
        var contID = component.get("v.recId");
		/*Rajeesh Reject route.
		*/
		var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        urlEvent.setParams({
            "recordId": recordID,
            "recId": contID,
			"parsedInfo": {},
            "source": "NotesParser"
        });
        urlEvent.fire(); 
    },
    /*
    * DOM Helper function to find parent by node name
    */
    findParentByNodeName: function(node, nodeName) {
        //console.log("in findParentByNodeName");
        if (!node) return null;
        //console.log("looking for " + nodeName.toLowerCase() + " but is " + node.nodeName.toLowerCase())
        if (node.nodeName.toLowerCase()==nodeName.toLowerCase()) return node;
        return this.findParentByNodeName(node.parentNode, nodeName);
    },
    /**
     * Format a date
     */
    formatDate : function(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        seconds = seconds < 10 ? '0'+seconds : seconds;
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ':' + seconds +  '' + ampm;
        return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
     },
    // TODO: Connected, you can get rid of BELOW is test code.. don't want to use this when going to PROD
    TEST_EXAMPLE_IDX : 0,    
    TEST_RECORDS : [
        {"note":"Alan lives in New York and wants to move to Bromley. He works as a software developer. He wants to make 450-500 a day", "city":"London", "state":"England", "country":"United Kingdom"},            
        {"note":"Looking for a min of £ 20k or £ 120pd. He currently lives in United Kingdom but wants to move to Liverpool", "city":"London", "state":"England", "country":"United Kingdom"},                  
        {"note":"Living in Hayes and very happy working as an automotive mechanic.He is on 450", "city":"London", "state":"England", "country":"United Kingdom"},                  
        {"note":"Chuck lives in Cardiff and very happy working as an automotive mechanic.", "city":"London", "state":"England", "country":"United Kingdom"},                  
        {"note":"No Non Huesten. Let's say 100. He is working as a software developer.", "city":"London", "state":"England", "country":"United Kingdom"},                          
        //{"note":"Alan works at T Rowe Price as a software developer. He loves his job. He currently lives in Hueston but wants to move to SoHo NYC eventually. He currently makes $55/hr. He would like to make 65/hr. He prefers C2H but is open to other options.His contract will be ending in December.", "talentId":"0012400000yFDqxAAG", "city":"Long Reach", "state":"Maryland", "country":"USA","lat":"39.2268013", "lng":"-76.8003"},    
        {"note":"Talked with Steven. He is still working at Queen's as an EPIC analyst. He really enjoys his role there and doesn't see himself move or changing roles. When I asked about referrals he said he doesn't know anyone at the moment.", "talentId":"0012400000yFDqxAAG", "city":"Honolulu", "state":"Hawaii", "country":"USA","lat":"21.33697", "lng":"-157.9229"},
        {"note":"Currently looking for a job as an operator- He changes light bulbs paint, change air filter for HVAC. He has been therefor 4-5 years.  He's targeting Hospitals, because pay. He is located in Pittsburgh. In terms of Pay rate: $30 an hour. BL:$28 an hour. Open to contract and contract to hire. Open to D+B. IOI set", "talentId":"0012400000ra1ZsAAI", "city":"Oakland", "state":"California", "country":"USA","lat":"37.829775", "lng":"-122.260771"},
        {"note":"Designer/Drafter - living in Filipino district - 101 and benotway - main program autocad, sketchup, occasionally revit - open to san fernando & valencia - targeting $35/hr+ for technifex (no engineering, degree, gd&t or fea or solidworks)", "talentId":"0012400001CblTYAAZ", "city":"Los Angeles", "state":"California", "country":"USA","lat":"34.071926", "lng":"-118.275609"},
        {"note":"Currently working for city of Dallas - street divisionWouldn't give details on how much she makes, told her about current positions we have and she said she would pass positions along.", "talentId":"00124000011IuTNAA0", "city":"Cedar Hill", "state":"Texas", "country":"USA","lat":"32.5878", "lng":"-96.9526"},
        {"note":"Nick provided a reference for one of my candidates.He is still the Trinity and enjoying his position. He is now a tier II network analyst. He considers himself an all around IT generalist. Responsible for Windows OS upgrades as well as working heavily with VMmware and Network Administration.", "talentId":"0012400000yodNeAAI", "city":"", "state":"Florida", "country":"USA","lat":"", "lng":""},
        {"note":"Jack is a referral from Joshua Villegas.  He is currently living in Utah, but will be moving to Sacramento when he can find a new role.  He is looking for a PC tech job, migration and upgrades.  Would be a fit for Dignity Health type deployments.", "talentId":"0012400001KQTXqAAP", "city":"SANDY", "state":"Utah", "country":"USA","lat":"40.547785", "lng":"-111.884313"},
        {"note":"currently working at DHL but he is on call currently looking for a warehouse role likes that type of work better. has forklift and cherry picker  experience he is targeting a grave. with a pay 15+ pallet electric ride a long pallet jack. sending over the Wine Warehouse JD Will KIT", "talentId":"0012400000tWbbxAAC", "city":"San Pablo", "state":"California", "country":"USA","lat":"37.990896", "lng":"-122.317759"},
        {"note":"Experience scanning parts, packing parts, working in a warehouse environment.  Looking for day shift.  $16/hour.  Interested in Nordstrom.  Concerned about not knowing much English.  Connect tomorrow to find out about languages at Nordstrom's.", "talentId":"0012400001PFgufAAD", "city":"Hayward", "state":"California", "country":"USA","lat":"37.6348", "lng":"-122.0658"}],
    testRenderExample: function(component) {
        this.clearSelectNodesAndNormalizedNamesMap();
        var rec = this.TEST_RECORDS[this.TEST_EXAMPLE_IDX];
        var contactId = component.get("v.recId");
        component.find('noteTextArea').set('v.value',rec.note);        
        this.performExtraction(component, component.get("c.getNotesExtraction"), rec.note, contactId);//, rec.city, rec.state, rec.country );
        this.TEST_EXAMPLE_IDX++;
    },
/*test
Example JSON format from REST response from Apex/Extraction Model API (with extractions from the notes):
{"entities":[{"label": "CURRENT_JT", "resolvedEntity": "software developer", "startIdx": 66, "endIdx": 84},{"label": "DESIRED_HOURLY_RATE", "resolvedEntity": "450-500 a day", "startIdx": 103, "endIdx": 116},{"label": "LIVING_LOCATION", "resolvedEntity": "New York", "startIdx": 14, "endIdx": 22},{"label": "RELOC_LOCATION", "resolvedEntity": "London", "startIdx": 44, "endIdx": 50}], "tokens":[{"text": "Alan", "startIdx": 0, "endIdx": 4, "ordinal": 0},{"text": "lives", "startIdx": 5, "endIdx": 10, "ordinal": 1},{"text": "in", "startIdx": 11, "endIdx": 13, "ordinal": 2},{"text": "New", "startIdx": 14, "endIdx": 17, "ordinal": 3},{"text": "York", "startIdx": 18, "endIdx": 22, "ordinal": 4},{"text": "and", "startIdx": 23, "endIdx": 26, "ordinal": 5},{"text": "wants", "startIdx": 27, "endIdx": 32, "ordinal": 6},{"text": "to", "startIdx": 33, "endIdx": 35, "ordinal": 7},{"text": "move", "startIdx": 36, "endIdx": 40, "ordinal": 8},{"text": "to", "startIdx": 41, "endIdx": 43, "ordinal": 9},{"text": "London", "startIdx": 44, "endIdx": 50, "ordinal": 10},{"text": ".", "startIdx": 50, "endIdx": 51, "ordinal": 11},{"text": "He", "startIdx": 52, "endIdx": 54, "ordinal": 12},{"text": "works", "startIdx": 55, "endIdx": 60, "ordinal": 13},{"text": "as", "startIdx": 61, "endIdx": 63, "ordinal": 14},{"text": "a", "startIdx": 64, "endIdx": 65, "ordinal": 15},{"text": "software", "startIdx": 66, "endIdx": 74, "ordinal": 16},{"text": "developer", "startIdx": 75, "endIdx":84, "ordinal": 17},{"text": ".", "startIdx": 84, "endIdx": 85, "ordinal": 18},{"text": "He", "startIdx": 86, "endIdx": 88, "ordinal":19},{"text": "wants", "startIdx": 89, "endIdx": 94, "ordinal": 20},{"text": "to", "startIdx": 95, "endIdx": 97, "ordinal": 21},{"text": "make", "startIdx": 98, "endIdx": 102, "ordinal": 22},{"text": "450", "startIdx": 103, "endIdx": 106, "ordinal": 23},{"text": "-", "startIdx": 106, "endIdx": 107, "ordinal": 24},{"text": "500", "startIdx": 107, "endIdx": 110, "ordinal": 25},{"text": "a", "startIdx": 111, "endIdx": 112, "ordinal": 26},{"text": "day", "startIdx": 113, "endIdx": 116, "ordinal": 27},{"text": ".", "startIdx": 116, "endIdx": 117, "ordinal": 28}], "normalizedEntities":[{"label": "DESIRED_HOURLY_RATE", "resolvedEntity": "450-500 a day", "jsonResponse":{"results": [{"amount": 450.0, "frequency": "daily", "normalized_name": "450.0 daily"}, {"amount": 450.0, "frequency": "hourly", "normalized_name": "450.0 hourly"}, {"amount": 450.0, "frequency": "yearly", "normalized_name": "450.0 yearly"}]}},{"label": "LIVING_LOCATION", "resolvedEntity": "New York", "miles": "", "jsonResponse": {"status": "OK", "results": [{"address_components": [{"long_name": "New York", "short_name": "New York", "types": ["locality", "political"]}, {"long_name": "New York", "short_name": "NY", "types": ["administrative_area_level_1", "political"]}, {"long_name": "United States", "short_name": "US", "types": ["country", "political"]}], "formatted_address": "New York, NY, USA", "geometry": {"location": {"lat": 40.7127753, "lng": -74.0059728}, "viewport": {"northeast": {"lat": 40.9175771, "lng": -73.70027209999999}, "southwest": {"lat": 40.4773991, "lng": -74.25908989999999}}}, "id": "7eae6a016a9c6f58e2044573fb8f14227b6e1f96", "name": "New York", "place_id": "ChIJOwg_06VPwokRYv534QaPC8g", "reference": "ChIJOwg_06VPwokRYv534QaPC8g", "scope": "GOOGLE", "types": ["locality", "political"], "utc_offset": -300, "vicinity": "New York", "normalized_name": "New York, NY, USA (2150 km.)", "distance": 2150.67517145181}, {"address_components": [{"long_name": "New York", "short_name": "NY", "types": ["administrative_area_level_1", "political"]}, {"long_name": "United States", "short_name": "US", "types": ["country", "political"]}], "formatted_address": "New York, USA", "geometry": {"location": {"lat": 43.2994285, "lng": -74.21793260000001}, "viewport": {"northeast": {"lat": 45.015865, "lng": -71.777491}, "southwest": {"lat": 40.4773991, "lng": -79.7625901}}}, "id": "349c7fc49816ce54bb586cf8fa2cd79b255746b3", "name": "New York", "place_id": "ChIJqaUj8fBLzEwRZ5UY3sHGz90", "reference": "ChIJqaUj8fBLzEwRZ5UY3sHGz90", "scope": "GOOGLE", "types": ["administrative_area_level_1", "political"], "utc_offset": -300, "normalized_name": "New York, USA (2087 km.)", "distance": 2087.71288162546}]}},{"label": "RELOC_LOCATION", "resolvedEntity": "London", "miles": "", "jsonResponse": {"status": "OK", "results": [{"address_components": [{"long_name": "London", "short_name": "London", "types": ["locality", "political"]}, {"long_name": "London", "short_name": "London", "types": ["postal_town"]}, {"long_name": "Greater London", "short_name": "Greater London", "types": ["administrative_area_level_2", "political"]}, {"long_name": "England", "short_name": "England", "types": ["administrative_area_level_1", "political"]}, {"long_name": "United Kingdom", "short_name": "GB", "types": ["country", "political"]}], "formatted_address": "London, UK", "geometry": {"location": {"lat": 51.5073509, "lng": -0.1277583}, "viewport": {"northeast": {"lat": 51.6723432, "lng": 0.148271}, "southwest":{"lat": 51.38494009999999, "lng": -0.3514683}}}, "id": "b1a8b96daab5065cf4a08f953e577c34cdf769c0", "name": "London", "place_id": "ChIJdd4hrwug2EcRmSrV3Vo6llI", "reference": "ChIJdd4hrwug2EcRmSrV3Vo6llI", "scope": "GOOGLE", "types": ["locality", "political"], "utc_offset": 0, "vicinity": "London", "normalized_name": "London, UK (0 km.)", "distance": 0.621371}, {"address_components": [{"long_name": "City of London", "short_name": "City of London", "types": ["sublocality_level_1", "sublocality", "political"]}, {"long_name": "London", "short_name": "London", "types": ["locality", "political"]}, {"long_name": "Greater London", "short_name": "Greater London", "types": ["administrative_area_level_2", "political"]}, {"long_name": "England", "short_name": "England", "types": ["administrative_area_level_1", "political"]}, {"long_name": "United Kingdom", "short_name": "GB", "types": ["country", "political"]}], "formatted_address": "City of London, London, UK", "geometry": {"location": {"lat": 51.5123443, "lng": -0.09098519999999999}, "viewport": {"northeast": {"lat": 51.5233212, "lng": -0.0727492}, "southwest": {"lat": 51.5067659, "lng": -0.113821}}}, "id": "6097eb566ed8833094306e824a2f3ca658f87da7", "name": "City of London", "place_id": "ChIJX4XfTlUDdkgRwISR0ciFEQo", "reference": "ChIJX4XfTlUDdkgRwISR0ciFEQo", "scope": "GOOGLE", "types": ["sublocality_level_1", "locality", "sublocality", "political"], "utc_offset": 0, "vicinity": "City of London", "normalized_name": "City of London, London, UK (1 km.)", "distance": 1.005705769506472}]}}]}
*/
})