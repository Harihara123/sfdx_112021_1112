({
	// Your renderer method overrides go here
	rerender : function(component,helper){
		this.superRerender();
		
		var listenerAdded = component.get('v.listenerAdded');
		if(!listenerAdded){
			//console.log('---notes parser---inside listenerer loop---'+Date.now());
			var elements = document.getElementsByClassName("annotation_button");
        
			for (var i=0;i<elements.length;i++) {
				var value = elements[i].value;
				elements[i].addEventListener('click', function() {helper.annotateText(component, helper, this)});			  
			}
			document.getElementById("closeAnno").addEventListener('click', function() {
				document.getElementById("chooseAnnotationType").style.display = "none";
			});
			component.set('v.listenerAdded',true);
			helper.getMyLocation(component);
		}
		if(component.get('v.redo')){
			document.getElementById("enrichedNotes").innerHTML = '<span>'+helper.originalText+'</span>';

			$A.util.removeClass(component.find('noteTextArea'), 'hideMe');
            $A.util.addClass(component.find('noteTextArea'), 'showMe');            
            document.getElementById("enrichedNotes").style.display = "none";    
            
            document.getElementById("non_extraction_span").style.display = 'inline';
            document.getElementById("non_extraction_span").style.visibility = 'visible';
            document.getElementById("extraction_span").style.visibility = 'hidden'; 
            document.getElementById("extraction_span").style.display = "none";

			component.set('v.redo',false);
		}

	},
	unrender : function (component, helper) {
        console.log('N2P unrender. send training data now');
		helper.logTrainingData(component,helper.retrieveVerifiedExtractions(true), helper.originalText, helper);
		return this.superUnrender();
    },	
})