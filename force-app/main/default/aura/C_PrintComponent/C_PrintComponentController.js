({
	doInit : function(component, event, helper) {
		var instanceURL = $A.get("$Label.c.Instance_URL");
		var vfPageURL= instanceURL + "/apex/printrecord"+"?oppId="+component.get("v.recordId");
        component.set("v.vfPageURL",vfPageURL);
		console.log('inside DO in it');
		//Event handler for Toast message
		window.addEventListener("message", function(event) {
		   
           if(event.data == 'success'){
                helper.showToast(component, event, helper);
           }
		   if(event.data === 'close'){
                helper.closeQuickAction(component, event, helper);
           }
		   if(event.data === 'textmessage'){
                helper.showTextCopied(component, event, helper);
           }
        }, false);

	}
})