({
	showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
		var title= $A.get("$label.c.ATS_SUCCESS");
		var message = $A.get("$label.c.ATS_Preferences_Saved");
        toastEvent.setParams({
            "title": "Success !",//$A.get("$label.c.ATS_SUCCESS")+"!",
			"type": "success",
            "message": "Preferences are Saved."//$A.get("$label.c.ATS_Preferences_Saved")
        });
        toastEvent.fire();
    },
	closeQuickAction : function(component, event, helper) {
        //component.destroy();
        console.log('InsideCloseMODAL Function');
		var closeEvent = $A.get("e.force:closeQuickAction");
		closeEvent.fire();
		/*var modal = component.find("exampleModal");
        $A.util.removeClass(modal, 'hideDiv');  */
		
		/*setTimeout(function(){
            		$A.get("e.force:closeQuickAction").fire(); 
		}, 1000);*/
    },
	showTextCopied : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
		var title= $A.get("$label.c.ATS_SUCCESS");
		var message = $A.get("$label.c.ATS_Preferences_Saved");
        toastEvent.setParams({
            
			"type": "success",
			"message": "Text Copied"
        });
        toastEvent.fire();
    },
})