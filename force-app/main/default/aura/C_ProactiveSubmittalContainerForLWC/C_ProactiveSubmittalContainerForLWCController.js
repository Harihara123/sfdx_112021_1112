({
    doInit : function(component, event, helper) {
		const pageRef = component.get("v.pageReference");
        var runningUser = pageRef.state.c__runningUser;
		
        component.set("v.runningUser", runningUser);

        //if(pageRef) {
            var recdId = pageRef.state.c__recordId;
            var talentId = pageRef.state.c__talentId;
            var recordType = pageRef.state.c__recordType;
            // Teting for PASF edit mode.
            //recdId = '801P0000000L7rVIAS';
            
            
            component.set("v.recordId", recdId);
			component.set("v.talentId", talentId);
	        component.set("v.recordType", recordType);
            if(runningUser == "" || runningUser == undefined || runningUser == null){
                helper.getCurrentUser(component);
            }
        //}
    }
})