({
    getCurrentUser : function(component) {
        var bdata = component.find("basedatahelper");
        try{
            //bdata.callServer(component,"","","getCurrentUser",function(response){
            bdata.callServer(component,'','',"getCurrentUserWithOwnership",function(response){
                if(response && response.usr){
                    component.set("v.runningUser", response.usr);
                }
            },"",true);
        }catch(err){
            console.log(err);
        }
	}
})