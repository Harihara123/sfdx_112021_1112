({
  dispatchData: function (component) {
    if (component.get("v.dispatch") && localStorage.getItem("sessionObject")) { // added condition by Manish for null check of localStorage before parsing
      let sessionObject = JSON.parse(localStorage.getItem("sessionObject"));
      delete sessionObject.tabsOpen;

      for (let i = 0; i < sessionObject.tabs.length; i++) {
        let download = sessionObject.tabs[i];
        delete download.connectedTabId;
        delete download.sessionStats;
        sessionObject.tabs[i] = download;
      }

      delete sessionObject.sessionStats.sessionClosed;


        let payload = {};
        payload.attributes = {"type": "User_Activity__c"};
        payload.Id = "a411N3wFl0wT3ST1ng";
        payload.OwnerId = sessionObject.sessionStats.userId;
        payload.isDeleted = false;
        payload.Name = "session_name";
        payload.CreatedDate = new Date(sessionObject.sessionStats.lastClickTime).toISOString();
        payload.CreatedById = sessionObject.sessionStats.userId;
        payload.LastModifiedDate = new Date(sessionObject.sessionStats.lastClickTime).toISOString();
        payload.LastModifiedById = sessionObject.sessionStats.userId;
        payload.SystemModstamp = new Date(sessionObject.sessionStats.lastClickTime).toISOString();
        payload.LastActivityDate = null;
        payload.Session_data_JSON__c = JSON.stringify(sessionObject).replace(/\u2028/g, '\\u2028').replace(/\u2029/g, '\\u2029');

        let action = component.get('c.makeCalloutDirectCall');
        action.setParams({"payload": JSON.stringify(payload)});
        action.setCallback(this, () => {
            component.set("v.dispatch", false);
        });
        $A.enqueueAction(action);
      }

    }
})