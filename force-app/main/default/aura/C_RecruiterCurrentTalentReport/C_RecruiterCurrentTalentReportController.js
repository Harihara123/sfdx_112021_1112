({
    determinePermission : function(component, event, helper) {
        var action = component.get("c.checkUserOPCO");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var opco = response.getReturnValue();
                component.set("v.userOPCO", opco);
                var opcoList = component.get('v.allowedOPCOs');
                if(opcoList && component.get('v.allowedOPCOs').split(',').includes(opco)) {
                    component.set("v.allowedOPCO", true);
                } else {
                    component.set("v.allowedOPCO", false);
                }
            } 
        });
        $A.enqueueAction(action);
		// Start STORY S-125537 Added by Siva 4/8/2019
		var caseRecTypeAction = component.get("c.getCaseRecordTypeIds");
		caseRecTypeAction.setCallback(this, function(response) {
			var caseRecTypeActionState = response.getState();
			if(caseRecTypeActionState === 'SUCCESS') {
				var caseRecordTypeIds = response.getReturnValue();
				component.set("v.caseRecordTypeIds", caseRecordTypeIds);
			}
		});
		$A.enqueueAction(caseRecTypeAction);
		// End STORY S-125537 Added by Siva 4/8/2019
    },

    openActivityModal : function(component, event, helper) {
        var singleSelect = component.get('v.singleSelect');
        var activityValue = event.getParam("value");
        if(singleSelect) {
			// Start STORY S-125537 Added by Siva 4/8/2019
			if(activityValue == 'csc') {
                   
				if(component.get('v.modalIsOpen'))
					component.set('v.modalIsOpen', false);
                   
				var childComponent = component.find('child');
				var recordData = childComponent.get('v.factMap');
				//var contactId = recordData["T!T"]["rows"][0]["dataCells"][7]["value"];

                // Get the right talent contact id for the selected talent acc id
                var contactId;
                var allRows = recordData["T!T"]["rows"];
                for(var rec=0;rec<allRows.length;rec++){
                    var record = allRows[rec];
                    if(record["dataCells"][1]["value"]==singleSelect){
                        contactId = record["dataCells"][7]["value"];
                        break;
                    }
                }


				var createRecordEvent = $A.get("e.force:createRecord");
				createRecordEvent.setParams({
					"entityApiName": "Case",
					"recordTypeId": component.get('v.caseRecordTypeIds')[activityValue],
					"defaultFieldValues": {
						"ContactId": contactId,
						"AccountId": component.get("v.singleSelect")
					},
					"navigationLocation":"LOOKUP",
					"panelOnDestroyCallback": function(event) {
                        var isExpanded = component.get("v.isExpanded");
                        if(!isExpanded){
                            component.set('v.modalIsOpen', false);
                        }else{
                            component.set('v.modalIsOpen', true);
                        }						
					}
				});
				createRecordEvent.fire();
			  // End STORY S-125537 Added by Siva 4/8/2019
			} else {
				var params;
				if(component.get('v.modalIsOpen')) {
					params = {"recordId":component.get("v.singleSelect"),"activityType":activityValue,"openBackDrop":false};
				}
				 else {
					params = {"recordId":component.get("v.singleSelect"),"activityType":activityValue};
				}
				helper.createComponent(component, 'c:C_TalentActivityAddEditModal', 'v.C_TalentActivityAddEditModal',params);
			}
        }
        else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: 'Error Message'
                , message: 'Please select a talent!'
                , type: 'error'
            });
            toastEvent.fire();
        }
    },
    refresh : function(component, event, helper) {
        var childComponent = component.find('child');
        component.set('v.singleSelect', null);
        childComponent.refresh();
    },
	openTableModal: function(component, event, helper) {
		helper.createTableModal(component, event, helper);
	}
})