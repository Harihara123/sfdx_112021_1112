({
	createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
			componentName,
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					cmp.set(targetAttributeName, newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			}
		);
    },
	createTableModal : function(component, event, helper) {
		var modalBody;

        $A.createComponent("c:C_RecruiterCurrentTalentReport", 
							{ 
								"reportIdAttribute" : component.get('v.reportIdAttribute'),
								"modalIsOpen" : true,
                                "isExpanded" : true
							},
						   function(content, status) {
							   if (status === "SUCCESS") {
								   component.set("v.modalIsOpen", true);
                                   component.set("v.isExpanded", true);
								   modalBody = content;
								   component.find('overlayLib').showCustomModal({
									   header: $A.get("$Label.c.ATS_MY_CURRENT_TALENT"),
									   body: modalBody,
									   cssClass: 'recCurrentTalentReportModal',
									   showCloseButton: true,
									   closeCallback: () => {
										   component.set("v.modalIsOpen", false);
                                           component.set("v.isExpanded", false);
									   }
								   })
							   }                               
		});
	}
})