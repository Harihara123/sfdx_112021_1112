({
    doInit: function(component, event, helper) {
		helper.getCurrentUser(component);
		// helper.getOpenReqs(component);
    },

    sortColumn: function (component, event, helper) {
		component.set("v.sortColumn", event.getParam("sortField"));
		component.set("v.sortDirection", event.getParam("sortAsc"));
		helper.sortData(component);
    },

    matchReq: function (component, event, helper ) {
		var idx = event.target.parentElement.id;
		var record = component.get("v.openReqs");
		var rec = record[idx];

		helper.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")
			+ "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + rec.Id 
			+ "&c__srcName=" + encodeURIComponent(rec.Name)
			+ "&c__srcLoc=" + rec.location
            + "&c__noPush=true&c__srcTitle="+rec.Req_Job_Title__c
			+ "&c__srcLat="+rec.Req_GeoLocation__Latitude__s+"&c__srcLong="+rec.Req_GeoLocation__Longitude__s
		);

    },
    
    dateFormat: function(component, event, helper) {
        let openReqs = component.get('v.openReqs');
        for (let i = 0; i < openReqs.length; i++) {
            let cDate = openReqs[i].CreatedDate;
            let months = [{name: 'Jan', val: 0},
                         {name: 'Feb', val: 1},
                         {name: 'Mar', val: 2},
                         {name: 'Apr', val: 3},
                         {name: 'May', val: 4},
                         {name: 'Jun', val: 5},
                         {name: 'Jul', val: 6},
                         {name: 'Aug', val: 7},
                         {name: 'Sep', val: 8},
                         {name: 'Oct', val: 9},
                         {name: 'Nov', val: 10},
                         {name: 'Dec', val: 11}];
            let YYYY = new Date(cDate).getYear() + 1900;
            let month = new Date(cDate).getMonth();
            let MMM = months.find(mo => mo.val === month).name;
            let DD = new Date(cDate).getDate();
            let dateString = MMM + ' ' + DD + ', ' + YYYY;
            openReqs[i].CreatedDate = dateString;
        }
        component.set('v.openReqsDuplicate', openReqs);
    }

})