({
    getOpenReqs: function (component) {
		var action = component.get("c.getOpenReqs");
		
        component.set("v.isLoading", true);
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                response = response.getReturnValue();

                response.map((item) => {
                    // Loop through object and format necessary fields
                    this.translateLocation(component, item);
                });

                component.set('v.openReqs', response);
				this.sortData(component);

                component.set("v.isLoading", false);
            }
		});
        $A.enqueueAction(action);

    },

	translateLocation: function (component, item) {
    /**    var company = component.get("v.runningUser.CompanyName");

        if (company && company === 'AG_EMEA') {
			item.location = this.buildCommaSeparatedLocation(item.Req_Worksite_City__c, item.Req_Worksite_Country__c);
        } else {
			item.location = this.buildCommaSeparatedLocation(item.Req_Worksite_City__c, item.Req_Worksite_State__c);
        }
	**/
        item.location = this.normalizedLocation(item.Req_Worksite_City__c, item.Req_Worksite_State__c, item.Req_Worksite_Country__c);
		return item.location;
    },

	/** buildCommaSeparatedLocation: function(l1, l2) {
		var l = (l1 && l1 !== "") ? l1 : "";
		return (l2 && l2 !== "") ? (l !== "" ? l + ", " + l2 : l2) : l;
	}, **/
    normalizedLocation: function(city, state, country) {
        if (city && city.length) {
            city.trim();
        }
        if (state && state.length) {
            state.trim();
        }
        if (country && country.length) {
            country.trim();
        }        
      let arr = [city, state, country];
      let validLocations = []
      for (let i = 0; i < arr.length; i++) {
        if (arr[i]) {
          validLocations.push(arr[i])
        }
      }
      let location = validLocations.join(", ");
  
  	return location;
    },

	getCurrentUser: function (component){
		var action = component.get("c.getCurrentUser");
		action.setCallback(this, function(response){
			 var state = response.getState();
             if (state === "SUCCESS") {
				 var r = JSON.parse(response.getReturnValue());
                component.set ("v.runningUser", r);
			 }
			 // Irrespective of success, make the call for card data.
			 this.getOpenReqs(component);
		});
		$A.enqueueAction(action);
	},

	sendToURL : function(url) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	},

	sortData: function(component) {
		var col = component.get("v.sortColumn");
		var isAsc = component.get("v.sortDirection");
		var isAscFactor = isAsc ? 1 : -1;

		var list = component.get('v.openReqs');
		list.sort(function(o1, o2) {
			switch (col) {
				case "Account_name_Fourmula__c" : 
					var x = o1.Account_name_Fourmula__c.toLowerCase();
					var y = o2.Account_name_Fourmula__c.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "Req_Job_Title__c" : 
					if (o1.Req_Job_Title__c && o2.Req_Job_Title__c) {
						var x = o1.Req_Job_Title__c.toLowerCase();
						var y = o2.Req_Job_Title__c.toLowerCase();
						return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;
					} else {
						return (o1.Req_Job_Title__c ? 1 : -1) * isAscFactor ;
					}

				case "Req_Hiring_Manager__r.Name" : 
				
					if (o1.Req_Hiring_Manager__r && o2.Req_Hiring_Manager__r) {
						if (o1.Req_Hiring_Manager__r.Name && o2.Req_Hiring_Manager__r.Name) {
							var x = o1.Req_Hiring_Manager__r.Name.toLowerCase();
							var y = o2.Req_Hiring_Manager__r.Name.toLowerCase();
							return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;
						}
						else {
							return (o1.Req_Hiring_Manager__r ? 1 : -1) * isAscFactor;
						}
					} else {
						return (o1.Req_Hiring_Manager__r ? 1 : -1) * isAscFactor;
					}

				case "location" :
					if (o1.location && o2.location) {
						var x = o1.location.toLowerCase();
						var y = o2.location.toLowerCase();
						return(x < y ? -1 : (x === y ? 0 : 1) * isAscFactor);
					} else {
						return (o1.location ? 1 : -1) * isAscFactor;
					}

				case "Owner.Name" :
					var x = o1.Owner.Name.toLowerCase();
					var y = o2.Owner.Name.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;


				case "StageName" :
					var x = o1.StageName.toLowerCase();
					var y = o2.StageName.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "CreatedDate" :
					// Substring extracts only the date component of the timestamp
					//var x = o1.CreatedDate.substring(0, 10).toLowerCase();
					//var y = o2.CreatedDate.substring(0, 10).toLowerCase();
					var x = new Date(o1.CreatedDate);
					var y = new Date(o2.CreatedDate);					
					if (x.getTime() === y.getTime()) {
						// CreatedDate is same, sort by redzone indicator trues first						 
						var rz1 = o1.Req_Red_Zone__c;    
                       	return (rz1 ? -1 : 1); 
					} else {
						return (x < y ? -1 : 1) * isAscFactor;
					}
                    
				
				case "Opportunity_Num__c" :
					var x = o1.Opportunity_Num__c.toLowerCase();
					var y = o2.Opportunity_Num__c.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;

				case "Req_Red_Zone__c" :
					
					var x = o1.Req_Red_Zone__c;
					var y = o2.Req_Red_Zone__c;
					return (x ? -1 : (x === y ? 0 : 1)) * isAscFactor;
									
				default :
					var x = o1.Account_Name_Formula__c.toLowerCase();
					var y = o2.Account_Name_Formula__c.toLowerCase();
					return (x < y ? -1 : (x === y ? 0 : 1)) * isAscFactor;
			}
			
		});
		component.set('v.openReqs', list);
	}

})