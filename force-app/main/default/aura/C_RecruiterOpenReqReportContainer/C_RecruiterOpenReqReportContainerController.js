({	
    openTableModal: function (component, event, helper) {

		var modalBody;

        $A.createComponent("c:C_RecruiterOpenReqReport", {},
           function(content, status) {
               if (status === "SUCCESS") {
                   component.set("v.modalIsOpen", true);
                   modalBody = content;
                   component.find('overlayLib').showCustomModal({
                       header: $A.get("$Label.c.ATS_MY_REQ_TEAM_OPEN_OPPORTUNITIES"),
                       body: modalBody,
                       cssClass: 'reqTeamOpenOpptyModal',
                       showCloseButton: true,
                       closeCallback: () => {
                           component.set("v.modalIsOpen", false);
                       }
                       
                   })
               }                               
           });
    }
})