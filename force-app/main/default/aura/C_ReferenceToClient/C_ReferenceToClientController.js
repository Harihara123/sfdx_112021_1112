({
    doInit : function(component, event, helper) {
        component.set('v.styles', true);
        helper.fillReferenceInfo( component );
        helper.fetchCurrentUser( component );
    },
    
    hideModal : function (component, event, helper) {
        component.set('v.styles', false);
        $A.get("e.force:closeQuickAction").fire();
    },
    
     searchOnEnter : function(component, event, helper) {
        var whichOne = event.getSource().getLocalId(); 
        if (event.getParams().keyCode === 13) {
            helper.validateAndConvert(component, event)
        }
    },
    
    confirmConversion : function(component, event, helper) {
        helper.validateAndConvert(component, event);
    }, 
    
    handleCurrentContact: function (component, event , helper) {
        helper.convertToClient(component); 
        helper.closeDedupeOverlay( component );
    },

	handleSelectedContact: function (component, event, helper) {

        if( $A.util.isUndefined( component.get("v.selectedRecord") ) || $A.util.isEmpty(component.get("v.selectedRecord" ) ) ){
            helper.showToast(component, "Please select one record...!", "Error", "error");
            return;
        }else{
            
          helper.use_selected_contact(component);  
        }
        
    },
    
    handleCloseModal: function (component, event, helper) {
        helper.closeDedupeOverlay(component);
        component.set("v.show_popup", true );    
    
    },
    
})