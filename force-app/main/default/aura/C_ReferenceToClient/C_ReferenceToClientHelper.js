({
    fillReferenceInfo : function(component, componentId, className) {
        
        var action = component.get('c.getReferenceData');
        action.setParams({
            "contactId": component.get("v.recordId")
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state 	  		= response.getState();
            var reference_data 	= response.getReturnValue();
            if (state === "SUCCESS") {
                
                if( Object.keys(reference_data).length ){
                    component.set("v.reference_fields", response.getReturnValue() );
                    component.set("v.Spinner", false );
                }else{
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    this.showToast(component, "Successfully Converted To Client", "success", "success", true);
                }
            }
            
        }));
        
        
        $A.enqueueAction(action);
        
    },
    
    fetchCurrentUser : function(component) {
        var action = component.get('c.getCurrentUserWithOwnership');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set ("v.runningUser", response.getReturnValue().usr);
                component.set ("v.userOwnership", response.getReturnValue().userOwnership);                
            }
            
        });
        $A.enqueueAction(action);
    },
    
    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    
    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },
    
    validateAndConvert : function(component, event ) {
        
        var isValid = true;
        isValid = this.validateTitle(component, isValid );
        isValid = this.validateFields(component, isValid, "firstName", "First Name") ;
        isValid = this.validateFields(component, isValid, "lastName", "Last Name") ;
        isValid = this.validateAccountLookup(component, isValid);
        isValid = this.validatePhoneOrEmail(component, isValid) ;
        if(isValid){
            this.checkForDuplicates(component);
           // this.convertToClient(component, event);
        }
        
    },
    
    validateAccountLookup :function(component,isValid){
        
        if(!component.get("v.account.Id")){
            isValid = false;
            component.set("v.accountLookupError", true);
            this.showToast(component, "Please Select valid Client Account.", "error", "error", true);
        } else {
            component.set("v.accountLookupError", false);
        }
      return  isValid;
    },
    
    validateTitle : function(component, isValid) {
        var title = component.get("v.reference_fields.Title");
        if (!title || title === "") {
            isValid = false;
            component.set("v.titleError", true);
            this.showToast(component, "Job title is empty.", "error", "error", true);
        } else {
            component.set("v.titleError", false);
        }
        return isValid;
    },
    
    convertToClient : function(component, event ) {
        this.startSpinner(component);
        var action = component.get('c.convertReferenceToClient');
        
        action.setParams({
            "contact_obj": component.get("v.reference_fields"),
            "account_obj": component.get("v.account"),
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            $A.get("e.force:closeQuickAction").fire();
            
            if (response.getState() === "SUCCESS" && response.getReturnValue() ) {
                $A.get('e.force:refreshView').fire();
                this.showToast(component, "Successfully Converted To Client", "success", "success", true);
            }else{
                this.showToast(component, "Conversion Failed, Please Contact Administrator", "error", "error");
            }
            
            
            this.stopSpinner(component);
            
        }));
        
        $A.enqueueAction(action);
    },
    
    validateFields : function(component, isValid, componentId, fieldName) {
        var currentFieldName = component.find(componentId);
        if(currentFieldName !== undefined){
            var validateField = currentFieldName.get("v.value");
            if (!validateField || validateField === "") {
                isValid = false;
                currentFieldName.set("v.errors", [{message: fieldName + " is Required."}]);
                currentFieldName.set("v.isError",true);
                $A.util.addClass(currentFieldName,"slds-has-error show-error-message");
                this.showToast(component, `${fieldName} is Required.`, "error", "error", true);
            } else {
                currentFieldName.set("v.errors", []);
                currentFieldName.set("v.isError", false);
                $A.util.removeClass(currentFieldName,"slds-has-error show-error-message");
            }
        }    
        return isValid;
    },
    
    
    validatePhoneOrEmail : function(component, isValid) {
        
        var phoneFieldName      = component.find("phone");
        var mobileFieldName     = component.find("mobile");
        var homephoneFieldName  = component.find("homephone");
        var emailFieldName      = component.find("email");
        
        
        var validatePhone = phoneFieldName.get("v.value");
        var validateEmail = emailFieldName.get("v.value");
        var validateHomephone 	= homephoneFieldName.get("v.value");
        var validateMobile 		= mobileFieldName.get("v.value");
        
        if ( ( !validatePhone || validatePhone === "") && ( !validateEmail || validateEmail === "") &&
             ( !validateHomephone || validateHomephone === "") && ( !validateMobile || validateMobile === "" ) ) {
            
            isValid = false;
            phoneFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            phoneFieldName.set("v.isError",true);
            $A.util.addClass(phoneFieldName,"slds-has-error show-error-message");
            
            mobileFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            mobileFieldName.set("v.isError",true);
            $A.util.addClass(mobileFieldName,"slds-has-error show-error-message");
            
            homephoneFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            homephoneFieldName.set("v.isError",true);
            $A.util.addClass(homephoneFieldName,"slds-has-error show-error-message");
            
            emailFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            emailFieldName.set("v.isError",true);
            $A.util.addClass(emailFieldName,"slds-has-error show-error-message");

            this.showToast(component, "Phone Number or Email Address is Required.", "error", "error", true);
        }else {
            phoneFieldName.set("v.errors", []);
            phoneFieldName.set("v.isError", false);
            $A.util.removeClass(phoneFieldName,"slds-has-error show-error-message");
            
            emailFieldName.set("v.errors", []);
            emailFieldName.set("v.isError", false);
            $A.util.removeClass(emailFieldName,"slds-has-error show-error-message");
            
            homephoneFieldName.set("v.errors", []);
            homephoneFieldName.set("v.isError", false);
            $A.util.removeClass(homephoneFieldName,"slds-has-error show-error-message");
            
            mobileFieldName.set("v.errors", []);
            mobileFieldName.set("v.isError", false);
            $A.util.removeClass(mobileFieldName,"slds-has-error show-error-message");
        }
        
        return isValid;
    },
    
    checkForDuplicates : function(component, event ){

        this.startSpinner(component);

        var phoneList = [];

        if( component.get("v.reference_fields.Phone") ){
            phoneList.push( component.get("v.reference_fields.Phone") );
        }

        if( component.get("v.reference_fields.MobilePhone") ){
            phoneList.push( component.get("v.reference_fields.MobilePhone") );
        }

        if( component.get("v.reference_fields.HomePhone") ){
            phoneList.push( component.get("v.reference_fields.HomePhone") );
        }

        var action = component.get('c.callDedupeService');

        action.setParams({
            "fullName"  : component.get("v.reference_fields.FirstName") + ' ' + component.get("v.reference_fields.LastName"),
            "email"     : [component.get("v.reference_fields.Email")] ,
            "phones"    : phoneList,
            "recordType" : 'Client'
        });


        action.setCallback(this, function(response) {

            var state = response.getState();
            
            if (state === "SUCCESS") {
                this.stopSpinner(component);
                var searchResults = JSON.parse( response.getReturnValue() );
        
                if( searchResults != null ){
                    component.set("v.dedupeResults", this.sortReferenceData(searchResults, 'Record_Type_Name__c')); 
        
                    component.set("v.show_popup", false );
                    this.displayResultOverlay(component, event );
                    
                }else{
                    component.set("v.dedupeResults", null );
                    this.convertToClient(component); 
                }
                
                
            }else if (state === "ERROR") {
                this.showToast(component, "There is some issue with Dedupe service, Please contact your Administrator for details.", "error", "error");
                this.stopSpinner(component);
            }
           

        });
        $A.enqueueAction(action);
        
    },
    
    sortReferenceData : function( referenceData , sortByKey) {
        
        var sortedReferences =  referenceData.sort(function(a, b) {
            var x = a[sortByKey]; var y = b[sortByKey];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
        
        /* Return in decending order Talent -> Reference -> Client  */
        return sortedReferences.reverse();
        
    },
    
    displayResultOverlay : function(component, event ){

        $A.createComponents([
                ["c:C_DedupeResultModal",{
                    dedupeResults: component.get("v.dedupeResults"),
                    selectedRecord: component.getReference("v.selectedRecord")}],
                ["c:C_DedupeResultFooter", {
                    	recordType: 'Client',
                        handleSelectedContact: component.getReference("c.handleSelectedContact"),
                        handleCurrentContact: component.getReference("c.handleCurrentContact"),
                        handleCloseModal: component.getReference("c.handleCloseModal"),
                        toast: component.getReference("v.toast"),
                        showToast: component.getReference("v.showToast"),

                }
                ]
            ],
            function(components, status){
                if (status === "SUCCESS") {
                    let modalBody = components[0];
                    let modalFooter = components[1];
                    let overlayPromise = component.find('dedupe_overlay').showCustomModal({
                        header: "Duplicate Client Contacts",
                        body: modalBody,
                        footer: modalFooter,
                        showCloseButton: false,
                        cssClass: "slds-modal_large",
                        closeCallback: function() {
                            
                        }
                    });

                    component.set("v.overlayPromise", overlayPromise );

                }else if (status === "ERROR") {
                    this.showToast(component, 'Some Error Occurred, Please Contact Your Administrator.!', 'error','error');
                }
            }
        );
        
    },

    use_selected_contact : function(component) {   
        
        this.startSpinner( component );

        var action = component.get("c.useSelectedContact");
        action.setParams({
            "selectedContactId"     : component.get("v.selectedRecord"),
            "existingContact_obj"   : component.get("v.reference_fields"),
            "referenceId"           : component.get("v.recordId").Id

        });
        
        action.setCallback(this,function(response) {
            
            var state           = response.getState();
            var reference_data  = response.getReturnValue();

            if (state === "SUCCESS") {
                this.closeDedupeOverlay( component );
                this.showToast(component, "Converted Successfully", "success", "success", true);

                var eUrl= $A.get("e.force:navigateToURL");
                eUrl.setParams({
                  "url": '/lightning/r/Contact/' + component.get("v.selectedRecord") +'/view'
                });
                eUrl.fire();
                

            }else{
                this.showToast(component, 'Something went wrong, Please contact Administrator' , "Error", "error");
            }

            this.stopSpinner( component );

            
        });

        $A.enqueueAction(action);
    },


    closeDedupeOverlay: function (component) {
        component.get("v.overlayPromise").then(
            function (modal) {
                modal.close();
                component.set("v.dedupeResults", null );
                component.set("v.selectedRecord", '' );
                
            }
        );
    },
    
    showToast : function(component, message, title, toastType, place){
        var toastEvent = $A.get("e.force:showToast");
        let toastParams = {
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        };
        if (component.get('v.overlayPromise')) {
            component.set('v.toast', toastParams);
            component.set('v.showToast', true);
        }
        if (place || !component.get('v.overlayPromise')) {
            if (toastParams.title === 'success') {
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            } else {
                component.set('v.toast1', toastParams);
                component.set('v.showToast1', true);
            }
        }
    },
    
    startSpinner: function(component){
        component.set("v.Spinner", true);
        var spinner = component.find('spinnerId');
        $A.util.addClass(spinner, 'slds-show');   
    },

    stopSpinner: function(component){
        var spinner = component.find('spinnerId');
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');
        component.set("v.Spinner", false);   
    },
    
    
})