({
    getContactRecord : function(component, event, helper) {
        var bdata = component.find("bdhelper");
        var accountId = component.get('v.recordId');
        var params = {'soql' : helper.soql(accountId)};

        bdata.callServer(component, '', '', 'getRecord', function(response) {
            component.set('v.contactRecord', response);
        }, params, false);
    },

    close : function(component, event, helper) {
        component.destroy();
    },

    validateContact: function(component, event, helper) {
        var selectedContact = component.get('v.selectedContact');
        if(selectedContact && selectedContact.Id) {
            component.set('v.buttonDisabled', false);
        } else {
            component.set('v.buttonDisabled', true);
        }
    }, 

    associateContact: function(component, event, helper) {
        var selectedContact = component.get('v.selectedContact');
        var currentContact = component.get('v.contactRecord');

        var updateRecord = {
            'Id': currentContact.Id,
            'Related_Contact__c': selectedContact.Id
        };

        var action = component.get('c.updateRecord');
        action.setParams({'record':updateRecord});

        component.set('v.showSpinner', true);

        component.set('v.buttonDisabled', true);

        action.setCallback(this, function(response){
            var state = response.getState();
            component.set('v.showSpinner', false);
            if(state === 'SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"success",
                    "title":"Success",
                    "message":$A.get("$Label.c.ATS_RELATEDCLIENT_ADD_TOAST")
                });

                var searchCompleteEvent = $A.get("e.c:RefreshTalentHeader");
                searchCompleteEvent.fire();

                toastEvent.fire();
                component.destroy();
            } else {
                helper.showToast(component);
            }
        });

        $A.enqueueAction(action);
    }
})