({
    doInit : function(component, event, helper) {
        helper.getRecentlyViewedRecords(component);
    },

    getSuggestedRecords : function(component, event, helper) {
        helper.getSuggestedRecords(component);
    }
})