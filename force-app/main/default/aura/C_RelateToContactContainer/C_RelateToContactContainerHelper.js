({
    getRecentlyViewedRecords : function(component) {
        component.set('v.showSpinner', true);
        var action = component.get('c.queryRecentObject');
        action.setParams({
            'searchText' : ''
            , 'whereClause' : ''
            , 'searchObject' : 'Contact'
            , 'sortField' : ''
            , 'contactRecordType' : component.get('v.contactRecordType')
            , 'sortAsc' : ''
        });
        this.handleActionCallback(component, action, 'v.recentResults');
    },

    getSuggestedRecords : function(component) {
        var contactRecord = component.get('v.contactRecord');
        var action = component.get('c.talentDeDupeSearchService');
        action.setParams({
            'searchText' : contactRecord.Name
            , 'emails' : JSON.stringify(this.parseListForNull([contactRecord.Email, contactRecord.Work_Email__c, contactRecord.Other_Email__c]))
            , 'phones' : JSON.stringify(this.parseListForNull([contactRecord.Phone, contactRecord.MobilePhone, contactRecord.OtherPhone, contactRecord.HomePhone]))
            , 'contactRecordType' : component.get('v.contactRecordType')
        });
        this.handleActionCallback(component, action, 'v.suggestedResults');
    },

    parseListForNull : function(list) {
        var sanitizedList = [];
        for(var item of list) {
            if(item) {
                sanitizedList.push(item);
                }
        }
        return sanitizedList;
    },

    handleActionCallback : function(component, action, setResults) {
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set(setResults, results ? JSON.parse(results) : results);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown error");
                }
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    }
})