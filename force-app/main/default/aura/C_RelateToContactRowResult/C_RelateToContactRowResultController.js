({
	setSelectedContact: function (component, event, helper) {
        var selectedContact = component.get('v.selectedContact');
        var rowContact = component.get('v.row');

        if(selectedContact && rowContact.Id === selectedContact.Id) {
            component.set('v.selectedContact', {});
        } else {
            component.set('v.selectedContact', rowContact);
        }
	}
})