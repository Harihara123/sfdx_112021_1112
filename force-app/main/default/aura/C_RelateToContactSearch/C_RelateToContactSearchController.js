({
    doInit : function(component, event, helper) {
        helper.getCountryMappings(component);
    },

    setDefaults : function(component, event, helper) { 
        var contactRecord = event.getParam('value');
        var inputName = component.find('inputName');
        inputName.set('v.value', contactRecord.Name);
		//S-107675 Neel -> Removing pre populated of State and Countary
        //component.set('v.country', (contactRecord.Talent_Country_Text__c) ? contactRecord.Talent_Country_Text__c : contactRecord.MailingCountry);
        //component.set('v.state', contactRecord.MailingState);
    },

    search : function (component, event, helper) {
        helper.search(component, event, helper);
    },

	searchOnEnter : function(component, event, helper) {
         if (event.which == 13) {
            helper.search(component, event, helper);
    }
	},
})