({
    search : function(component, event, helper) {
        var inputs = this.getInputMap(component);
        var inputElements = this.getInputElements(component);
        component.set('v.selectedContact', {});
        // console.log(component.get('v.selectedContact.Id'));
        if(this.validInput(component, inputs, inputElements)) {
            this.searchContacts(component, inputs);
        }
    },

    getInputMap : function(component) {
        return {
            'Name' : component.find('inputName').get('v.value')
            , 'Job Title' : component.find('inputJobTitle').get('v.value')
            , 'Phone' : component.find('inputPhone').get('v.value')
            , 'Email' : component.find('inputEmail').get('v.value')
            , 'City' : component.find('inputCity').get('v.value')
            , 'Zip' : component.find('inputPostalCode').get('v.value')
            , 'Country' : component.get('v.countryKey')
            , 'State / Province' : component.get('v.state')
            }; 
    },

    getInputElements : function(component) {
        return [
            component.find('inputName')
            , component.find('inputPhone')
            , component.find('inputEmail')
            , component.find('inputCity')
            , component.find('inputPostalCode')
        ];
    },

    validInput : function(component, inputs, inputElements) {
        var errorMessage = this.validateEmptyFields(inputs);
        if(!errorMessage) {
            if(!this.validateInputElements(inputElements)) {
                return false;
            }
            else {
                for(var key in inputs) {
                    var input = inputs[key];
                    errorMessage = this.validateMinimumCharacters(input, key);
                    if(errorMessage) {
                        break;
                    }
                }
            }
        }
        if(errorMessage) {
            this.showToast(component, errorMessage, "Error:", 'error');
            return false;
        }
        return true;
    },

    validateMinimumCharacters : function(input, field) {
        if(input && input.length > 0 && input.length < 2) {
            return 'Minimum character limit on ' + field + ' is 2. Please refine your search criteria.';
        }
    },

    //this is needed because queryObject function doesn't handle well with an empty whereClause
    validateEmptyFields : function(inputs) {
        var atLeastOneField = false;
        for(var key in inputs) {
            if(!$A.util.isEmpty(inputs[key])) {
                atLeastOneField = true;
            }
        }
        if(!atLeastOneField) {
            return $A.get("$Label.c.ATS_at_Least_One_Specified_Field");
        }
    },

    validateInputElements : function(inputElements) {
        for(var input in inputElements) {
            if(!inputElements[input].get('v.validity').valid) {
                return false;
            }
        }
        return true;
    },

    showToast : function(component, message, title, type) {
        var params = {
            "title" : title
            , "message" : message
            , "mode" : 'dismissible'
            , "variant" : type
        };
        component.set('v.toast', params);
        component.set('v.showToast', true);
    },

    //not sure why this is done on client side...
    //following pattern to use currently built classes
    buildWhereClause : function(subSetOfInputs, countryList) {
        var whereClause = '';
        for(var key in subSetOfInputs) {
            var value = subSetOfInputs[key];
            if(value) {
                if(whereClause.length > 0) {
                    whereClause += ' AND ';
                }
                if(key == 'Job Title') {
                    whereClause += "(Title= \'" + value + "\')";
                }
                else if(key == 'City') {
                    whereClause += "(MailingCity= \'" + value + "\' OR OtherCity= \'"+ value + "\')";	
                }
                else if(key == 'Zip') {
                    whereClause += "(MailingPostalCode= \'" + value + "\' OR OtherPostalCode= \'" + value + "\')";
                }
                else if(key == 'State / Province') {
                    //State is dependent on country picklist
                    whereClause += "(MailingState= \'" + value + "\' OR OtherState= \'" + value + "\')";	
                }
                else if(key == 'Country') {
                    //country should be sanitized by now via picklist
                    whereClause += "(MailingCountry= \'" + countryList[value] + "\' OR OtherCountry= \'" + countryList[value] + "\')";
                }
            }
        }
        return whereClause;
    },

    searchContacts : function(component, inputs) {
        component.set('v.showSpinner', true);
        component.set('v.selectedTabId', 'Search');

        var action;
        var searchText = '';
        if(inputs['Phone']) {
            action = component.get('c.searchObjectByPhone');
            searchText = inputs['Phone'].replace(/[\s\-\(\)\+]/g,'');
        }
        else if(inputs['Email']) {
            action = component.get('c.searchObjectByEmail');
            searchText = inputs['Email'].replace(/[\s]/g,'');
        }
        else if(inputs['Name']) {
            action = component.get('c.searchObjectByName');
            searchText = inputs['Name'];
        }
        else {
            action = component.get('c.queryObject');
        }
        searchText = searchText.replace(/^\s+|\s+$/g, "");
        var subSetInputs = inputs;
        delete subSetInputs['Name'];
        delete subSetInputs['Phone'];
        delete subSetInputs['Email'];
        var whereClause = this.buildWhereClause(subSetInputs, component.get('v.countryMap'));
        action.setParams({
            'searchText' : searchText
            , 'whereClause' : whereClause
            , 'searchObject' : 'Contact'
            , 'sortField' : ''
            , 'contactRecordType' : component.get('v.recordTypeName')
            , 'sortAsc' : ''
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set('v.results', results ? JSON.parse(results) : results);
				component.getEvent("searchCalledEvt").fire();
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown error");
                }
            }
                component.set('v.showSpinner', false);
                component.set('v.selectedContact', {});
        });
        $A.enqueueAction(action);
    },

    getCountryMappings : function(component) {   
        var action = component.get("c.getCountryMappings");
        action.setCallback(this,function(response) {
        	var state = response.getState();
			if (state === "SUCCESS" ) {
            	component.set("v.countryMap",response.getReturnValue());
            }else{
            	console.log('ERROR while loading countries');
            }
        });
        $A.enqueueAction(action);
    },
})