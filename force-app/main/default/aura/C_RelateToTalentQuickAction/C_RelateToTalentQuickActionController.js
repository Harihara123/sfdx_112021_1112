({
    getContactRecord : function(component, event, helper) {
        var bdata = component.find("bdhelper");
        var contactId = component.get('v.recordId');
        var params = {'soql' : helper.soql(contactId)};

        bdata.callServer(component, '', '', 'getRecord', function(response) {
            component.set('v.contactRecord', response);
        }, params, false);
    },

    close : function(component, event, helper) {
		component.set('v.qaModal', false);
        $A.get("e.force:closeQuickAction").fire();
		component.find("overlayLib").notifyClose();
    },

    validateContact: function(component, event, helper) {
        var selectedContact = component.get('v.selectedContact');
        if(selectedContact && selectedContact.Id) {
            component.set('v.buttonDisabled', false);
        } else {
            component.set('v.buttonDisabled', true);
        }
    }, 

    associateContact: function(component, event, helper) {
        var selectedContact = component.get('v.selectedContact');
        var currentContact = component.get('v.contactRecord');
        var updateRecord = {
            'Id': currentContact.Id,
            'Related_Contact__c': selectedContact.Id
        };

        currentContact.Related_Contact__c = selectedContact.Id;

        var action = component.get('c.updateRecord');
        action.setParams({'record':updateRecord});

        component.set('v.showSpinner', true);
        component.set('v.buttonDisabled', true);
		component.set('v.createButtonDisabled', true);

        action.setCallback(this, function(response){
            var state = response.getState();
            component.set('v.showSpinner', false);
            if(state === 'SUCCESS') {
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"success",
                    "title":"Success",
                    "message":"The Contact was successfully linked."
				});
                toastEvent.fire();
                component.set('v.qaModal', false);

                $A.get("e.force:closeQuickAction").fire();
				component.find("overlayLib").notifyClose();

            } else {
                helper.showToast(component);
            }
        });
        $A.enqueueAction(action);
    },

	handleSearchCalled: function(component, event, helper) {
		component.set("v.createButtonDisabled", false);
	},

	createAndRelate: function(component, event, helper) {
        var bdata = component.find("bdhelper");

		var currentContact = component.get('v.contactRecord');
        var params = {
			'clientContactId' : currentContact.Id
		};

		component.set('v.showSpinner', true);
		component.set('v.buttonDisabled', true);
		component.set('v.createButtonDisabled', true);

        bdata.callServer(
			component, 
			'ATS', 
			'', 
			'addAndRelateTalent', 
			function(response) {
				component.set('v.showSpinner', false);
				$A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"success",
                    "title":"Success",
                    "message":"The Contact was successfully linked."
				});
                toastEvent.fire();
                component.set('v.qaModal', false);
				component.set('v.buttonDisabled', true);
				component.set('v.createButtonDisabled', false);

                $A.get("e.force:closeQuickAction").fire();
				component.find("overlayLib").notifyClose();
			}, 
			params, 
			false);
	}
})