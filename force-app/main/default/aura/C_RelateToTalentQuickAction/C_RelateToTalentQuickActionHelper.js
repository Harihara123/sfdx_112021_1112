({
    soql : function(contactId) {
        return 'SELECT Id, Name, Preferred_Name__c, Email, Work_Email__c, Other_Email__c, Phone, MobilePhone, OtherPhone, RecordTypeId, MailingState, MailingCountry, OtherState, OtherCountry, Talent_Country_Text__c FROM Contact WHERE Id = \'' + contactId + '\'';  
    },

    showToast: function(component) {
        var params = {
            "title" : 'Error'
            , "message" : 'Unable to link Contacts'
            , "mode" : 'dismissible'
            , "variant" : 'error'
        };
        component.set('v.toast', params);
        component.set('v.showToast', true);
    }
})