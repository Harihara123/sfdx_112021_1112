({
    fetchData: function (cmp, event, helper) {
		cmp.set('v.setSpinner',true);
		cmp.set("v.setRelatedListURL", '/lightning/r/'+cmp.get('v.relatedObjectId')+'/related/'+cmp.get('v.relatedListObject')+'/view');
 		var action = cmp.get("c.initData")
 		var relatedFieldApiName = cmp.get("v.relatedFieldApiName")
        var numberOfRecords = cmp.get("v.numberOfRecords")
        var jsonData = JSON.stringify({fields:cmp.get("v.fields"),
                                       relatedFieldApiName:cmp.get("v.relatedFieldApiName"),
                                       recordId:cmp.get("v.recordId"),
                                       numberOfRecords:numberOfRecords + 1,
                                       sobjectApiName: cmp.get("v.sobjectApiName"),
                                       sortedBy: cmp.get("v.sortedBy"),
                                       sortedDirection: cmp.get("v.sortedDirection")
        });
        action.setParams({jsonData : jsonData});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var jsonData = JSON.parse(response.getReturnValue())
                var records = jsonData.records
                if(records.length > numberOfRecords){
                    records.pop()
                    cmp.set('v.numberOfRecordsForTitle', numberOfRecords + "+")
                }else{
                    cmp.set('v.numberOfRecordsForTitle', Math.min(numberOfRecords,records.length))
                }
				console.log('records:'+JSON.stringify(records));

                records.forEach(record => {
					record.LinkName = '/'+ record[cmp.get('v.linkTo')]
                  //record.LinkName = '/'+record.Id
				  //record.LinkName = '/'+record.Account__c
                  for (const col in record) {
                    const curCol = record[col];
                    if (typeof curCol === 'object') {
                      const newVal = curCol.Id ? ('/' + curCol.Id) : null;
					  //const newVal = curCol.Account__c ? ('/' + curCol.Account__c) : null;
                      helper.flattenStructure(helper,record, col + '_', curCol);
                      if (newVal !== null) {
                        record[col+ '_LinkName'] = newVal;
                      }
                    }
                  }
                });  
				            
                cmp.set('v.records', records)
                cmp.set('v.iconName', jsonData.iconName)
                cmp.set('v.sobjectLabel', jsonData.sobjectLabel)
				cmp.set('v.parentRelationshipApiName', jsonData.parentRelationshipApiName)
				console.log('records2:'+JSON.stringify(cmp.get('v.records')));
				if(cmp.get("v.relatedListName").length > 0) {
					cmp.set('v.sobjectLabelPlural', cmp.get("v.relatedListName"))					
				}
				else {
					cmp.set('v.sobjectLabelPlural', jsonData.sobjectLabelPlural)		
				}
                
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
			cmp.set('v.setSpinner',false);
        });

        $A.enqueueAction(action);        
    },
    flattenStructure : function (helper,topObject, prefix, toBeFlattened) {
      for (const prop in toBeFlattened) {
        const curVal = toBeFlattened[prop];
        if (typeof curVal === 'object') {
          helper.flattenStructure(helper, topObject, prefix + prop + '_', curVal);
        } else {
          topObject[prefix + prop] = curVal;
        }
      }
    },    
    
   initColumnsWithActions: function (cmp, event, helper) {
        var columnsWithActions = []
		if(cmp.get("v.rowAction")) {
			var customActions = cmp.get('v.customActions')
			if( !customActions.length){
				customActions = [
					{ label: 'Edit', name: 'edit' },
					{ label: 'Delete', name: 'delete' }
				]         
			}
			columnsWithActions.push({ type: 'action', typeAttributes: { rowActions: customActions } })
		}

		var columns = cmp.get('v.columns') 
		     
        columnsWithActions.push(...columns)
		console.log('columns:'+JSON.stringify(columnsWithActions));  
        cmp.set('v.columnsWithActions',  columnsWithActions)
    },    
    
    removeRecord: function (cmp, row) {
        var modalBody;
        var modalFooter;
        var sobjectLabel = cmp.get('v.sobjectLabel')
        $A.createComponents([
            ["c:C_DeleteRecordContent",{sobjectLabel:sobjectLabel}],
            ["c:C_DeleteRecordFooter",{record: row, sobjectLabel:sobjectLabel}]
        ],
        function(components, status){
            if (status === "SUCCESS") {
                modalBody = components[0];
                modalFooter = components[1];
                cmp.find('overlayLib').showCustomModal({
                   header: "Delete " + sobjectLabel,
                   body: modalBody, 
                   footer: modalFooter,
                   showCloseButton: true
               })
            }
        }
       );
        
    },
    
	editRecord : function (cmp, row) {
        var createRecordEvent = $A.get("e.force:editRecord");
        createRecordEvent.setParams({
            "recordId": row.Id
        });
        createRecordEvent.fire();
	}
})