({
	closeModal: function (component, event, helper) {
        helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
        helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
	},
    deleteMyList: function (component, event, helper) {
        helper.deleteMyList(component, helper);
    }
})