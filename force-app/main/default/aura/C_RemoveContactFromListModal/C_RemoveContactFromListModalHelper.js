({
	toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    deleteMyList: function(component, helper) {
        var listToDelete = component.get("v.record");
        listToDelete.LastModifiedDate = new Date(listToDelete.LastModifiedDate);
        var action = component.get("c.deleteList");
        action.setParams({"deleteList":listToDelete});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var removeRow = component.getEvent("removeRowEvent");
                var record = component.get("v.record");



                removeRow.setParams({"removedList":record,"requestStatus":true});

                removeRow.fire();

                helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
                helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
            } else {
                var removeRow = component.getEvent("removeRowEvent");
                var record = component.get("v.record");

                removeRow.setParams({"removedList":record,"requestStatus":false});

                removeRow.fire();

                helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
                helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
            }


        });

        $A.enqueueAction(action);
    }
})