({
	closeModal: function (component, event, helper) {
        helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
        helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
    },
    removeContact: function (component, event, helper) {
		if(component.get("v.showListMessage")) {
			helper.removeSelectedContacts(component, helper);
		} else {
			helper.removeContact(component, helper);
		}
    }
})