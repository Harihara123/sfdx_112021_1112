({
	toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    removeContact: function(component, helper) {
        var contactToRemove = component.get("v.record");
        var listRemovedFrom = component.get("v.list");
        var action = component.get("c.removeContactFromList");
        action.setParams({"tagId":listRemovedFrom, "contactId":contactToRemove.Id});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var removeRow = component.getEvent("contactRemovedEvent");

                removeRow.setParams({"requestStatus":response.getReturnValue(), "contact":contactToRemove});

                console.log('success');

                removeRow.fire();

                helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
                helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
            } else {
                var removeRow = component.getEvent("contactRemovedEvent");

                removeRow.setParams({"requestStatus":response.getReturnValue(), "contact":contactToRemove});

                removeRow.fire();

                var errorList = response.getError();

                console.log(errorList[0]);

                helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
                helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
            }


        });

        $A.enqueueAction(action);
    },
	removeSelectedContacts: function(component, helper) {
        var contactsToRemove = component.get("v.contacts");
		var contactIdsToRemove = [];
        var listRemovedFrom = component.get("v.list");
		var numberOfContacts = component.get("v.numberOfRecords");
		for(var i=0; i<contactsToRemove.length; i++) {
			contactIdsToRemove.push(contactsToRemove[i].Id);
		}
        var action = component.get("c.removeSelectedContactsFromList");
        action.setParams({"tagId":listRemovedFrom, "contactIds":contactIdsToRemove});
		
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var removeRow = component.getEvent("selectedContactsRemovedEvent");

                removeRow.setParams({"requestStatus":response.getReturnValue(), "contacts":contactsToRemove, "numberOfContacts" : numberOfContacts});

                removeRow.fire();

                helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
                helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
            } else {
                var removeRow = component.getEvent("selectedContactsRemovedEvent");

                removeRow.setParams({"requestStatus":response.getReturnValue(), "contacts":contactsToRemove, "numberOfContacts" : numberOfContacts});

                removeRow.fire();

                helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop_');
                helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
            }
        });

        $A.enqueueAction(action);
    }
})