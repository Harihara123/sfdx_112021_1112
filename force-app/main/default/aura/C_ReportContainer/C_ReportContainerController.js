({
    doInit : function(component, event, helper) {
        helper.getReport(component, event, helper);
        helper.alignText(component);
    },
    getReport : function(component, event, helper) {
        helper.getReport(component, event, helper);
    },
	sortColumn: function (component, event, helper) {
		component.set("v.sortColumn", event.getParam("sortField"));
		component.set("v.sortDirection", event.getParam("sortAsc"));
    }
})