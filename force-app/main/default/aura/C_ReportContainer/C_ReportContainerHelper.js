({
	getReport : function(component, event, helper) {
        //hide report and show spinner while we process
        var loadingSpinner = component.find('loading');
        $A.util.removeClass(loadingSpinner, 'slds-hide');
        var reportContainer = component.find('report');
        var reportError = component.find('report-error');
        if(!$A.util.hasClass(reportError, 'slds-hide')) {
            $A.util.addClass(reportError, 'slds-hide');
        }

        //get report data from Apex controller using report ID provided
        var serverMethod = component.get('v.serverMethod');
        var action = component.get(serverMethod);
        action.setParams({ 
            reportId: component.get("v.reportIdAttribute")
        });

        //handle response from Apex controller
        action.setCallback(this, function(response){
            // transform into JSON object
            var returnValue = JSON.parse(response.getReturnValue());
            var groupingLabels = {};
            
            if( returnValue && returnValue.reportExtendedMetadata ){
                // categorize groupings into levels so we can access them as we go down grouping level
                var columnInfo = returnValue.reportExtendedMetadata.groupingColumnInfo;
                for( var property in columnInfo ){
                    if( columnInfo.hasOwnProperty(property) ){
                        var level = columnInfo[property].groupingLevel;
                        var label = columnInfo[property].label;
                        groupingLabels[level] = label;
                    }
                }
                
                //set column headers, this assumes that they are returned in order
                var tableHeaders = [];
                var showColumns = [];
                for( var i = 0; i < returnValue.reportMetadata.detailColumns.length; i++ ){
                    var fieldAPIName = returnValue.reportMetadata.detailColumns[i];
                    var fieldLabel = returnValue.reportExtendedMetadata.detailColumnInfo[fieldAPIName].label;
                    tableHeaders.push(fieldLabel);
                    //create map to hide elements by class name
                    //EMEA users should not see service status
                    var opco = component.get("v.userOPCO");
                    if(!((opco === 'TEK' || opco === 'AG_EMEA') && fieldLabel === 'Service Status')) {
                        showColumns.push(i);
                    } 
                }
                component.set("v.columnLabels", tableHeaders);
                component.set('v.showColumns', showColumns);

                // set lightning attributes so we have access to variables in the view
                component.set("v.groupingLevelToLabel", groupingLabels)
                component.set("v.reportData", returnValue);
                component.set("v.factMap", returnValue.factMap);

                var allRows = null;
                Object.keys(returnValue.factMap).forEach(function(key) {
                    if(allRows) {
                        if(returnValue.factMap[key]) {
                            allRows.concat(returnValue.factMap[key].rows);
                        }
                    } else {
                        if(returnValue.factMap[key]) {
                            allRows = returnValue.factMap[key].rows;
                        }
                    }
                });

                component.set("v.factMapRows", allRows);
                //hide spinner, reveal data
                $A.util.addClass(loadingSpinner, 'slds-hide');
            }
            else {
                $A.util.addClass(loadingSpinner, 'slds-hide');
                $A.util.removeClass(reportError, 'slds-hide');
            }
        })
        $A.enqueueAction(action);
    },
    alignText : function(component) {
        var textAlign = component.get('v.cellTextAlign');
        if( textAlign != null ) {
            var elementToAlign = component.find('elementToAlign');
            if (elementToAlign == null) {
                return;
            }
            switch (textAlign) {
                case 'right':
                    $A.util.addClass(tdToAlign, 'textAlignRight');
                    break;
                case 'left':
                    $A.util.addClass(tdToAlign, 'textAlignLeft');
                    break;
                case 'center':
                    $A.util.addClass(tdToAlign, 'textAlignCenter');
                    break;
            }
        }
    }
})