({
	setLabel : function(component, event, helper) {
		var level = component.get("v.level")
		var groupingLevelToLabel = component.get("v.groupingLevelToLabel");
		if( groupingLevelToLabel ){
			component.set("v.fieldLabel", groupingLevelToLabel[level] );	
		}
        helper.alignText(component);
	},
    setSingleSelect : function(component, event, helper) {
        var singleSelect = component.get('v.singleSelect');
        var rowValue = event.getSource().get("v.value");
        if(singleSelect && rowValue === singleSelect) {
            component.set('v.singleSelect', null);
        } else {
            component.set('v.singleSelect', rowValue);
        }
	}
})