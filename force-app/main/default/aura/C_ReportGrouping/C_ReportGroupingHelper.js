({
    alignText : function(component) {
        var textAlign = component.get('v.cellTextAlign');
        if( textAlign != null ) {
            var elementToAlign = component.find('elementToAlign');
            if (elementToAlign == null) {
                return;
            }
            switch (textAlign) {
                case 'right':
                    $A.util.addClass(tdToAlign, 'textAlignRight');
                    break;
                case 'left':
                    $A.util.addClass(tdToAlign, 'textAlignLeft');
                    break;
                case 'center':
                    $A.util.addClass(tdToAlign, 'textAlignCenter');
                    break;
            }
        }
    }
})