({    
   doInit: function(cmp) {
        var action = cmp.get("c.getOpportunity");
        action.setParams({ recordId : cmp.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var opps = response.getReturnValue();
                cmp.set("v.reqOpco", opps.OpCo__c);
                eval("$A.get('e.force:refreshView').fire();");
            }
        });
        $A.enqueueAction(action);
    },
    handleQuickAction : function(cmp, e, h) {
        const details = e.getParams();        
        const actionName = details.action;

        const actionAPI = cmp.find("quickActionAPI"); 
        
        
        // actionAPI.getAvailableActions().then(res => {
        //     //console.log(JSON.stringify(res))
        // }).catch(err => {console.log(err)})


        actionAPI.invokeAction({actionName}).then(function(result){
            //Action selected; show data and set field values
            //console.log('QUICK ACTION', result)
        }).catch(function(rej){
            if(rej.errors){
                console.log(rej.errors)
                //If the specified action isn't found on the page, show an error message in the my component
            }
        });
    },
    handleOppStatusChange: function(cmp, e, h) {
        //console.log('Open position modal..')
        var appEvent = $A.get('e.c:E_PositionModal');
        appEvent.fire();
    },
    removePositionCard: function(cmp, e,h) {
        //console.log('%cDestroying position card', 'background-color: black;color: aqua;')
        const positionCardCmp = cmp.find('positionCard')
        if (positionCardCmp) {
			//Adding w.r.t Defect D-13986
            //positionCardCmp.destroy();
			if (cmp.isRendered()) {
				positionCardCmp.destroy();
			}			
        }
    },
   refreshHeader : function(cmp,event,helper){
       if(cmp.get("v.reqOpco") == 'AG_EMEA' && cmp.find('lwcReqHederEmea')) {			
           cmp.find('lwcReqHederEmea').connectedCallback();    
       }
       if(cmp.get("v.reqOpco") == "TEKsystems, Inc." && cmp.find('lwcReqHederTek')) {           
           cmp.find('lwcReqHederTek').connectedCallback();
       }
       if(cmp.get("v.reqOpco") == "Aerotek, Inc" && cmp.find('lwcReqHederAerotek')) {           
           cmp.find('lwcReqHederAerotek').connectedCallback();
       }
	}
})