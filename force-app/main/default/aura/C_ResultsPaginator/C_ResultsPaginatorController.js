({
    init: function(component, event, helper) {
        var pageSizeEvent = $A.get("e.c:E_PageResize");
        pageSizeEvent.setParam('pageSize', component.get('v.pageSize'));
        pageSizeEvent.fire();   

        let randomId = 'pagination-' + Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
        component.set('v.randomId', randomId);

        const externalSrc = component.get("v.externalsource");
        let paginationPos;
        switch (externalSrc) {
            case 'TD':
            case 'RQ': 
                paginationPos = ''   
                break;
        
            default:
                paginationPos = 'position: fixed; bottom: 4px; z-index: 1001; right: 1rem';
                break;
        }
        component.set("v.pageStyle", paginationPos);
         
    },
    
	handleResultsUpdate: function(component, event, helper) {
		helper.updateState(component);
	},

	handlePageSizeChange : function (component, event, helper) {
        var pageSizeEvent = $A.get("e.c:E_PageResize");
        pageSizeEvent.setParam('pageSize', component.get('v.pageSize'));
        pageSizeEvent.fire();
		helper.firePageSizerEvent(component);
	},

	goToFirst : function (component, event, helper) {
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'pagination--goToFirst');
        trackingEvent.fire();
		helper.firePageEvent(component, 0);
	},

	goToPrevious : function (component, event, helper) {
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'pagination--goToPrevious');
        trackingEvent.fire();
		var offset = component.get("v.currentOffset") - component.get("v.pageSize");
		helper.firePageEvent(component, offset);
	},

	goToNext : function (component, event, helper) {
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'pagination--goToNext');
        trackingEvent.fire();
		var offset = component.get("v.currentOffset") + component.get("v.pageSize");
		helper.firePageEvent(component, offset);
	},

	goToLast : function (component, event, helper) {
		var pagenum = component.get("v.lastPageNum");
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'pagination--goToPage-' + pagenum);
        trackingEvent.fire();
		helper.firePageEvent(component, (pagenum - 1) * component.get("v.pageSize"));
	},

	goToPage : function (component, event, helper) {
        var pagenum = event.currentTarget.name;
        console.log(pagenum);
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'pagination--goToPage-' + pagenum);
        trackingEvent.setParam('clickType', 'button');
        trackingEvent.fire();
		helper.firePageEvent(component, (pagenum - 1) * component.get("v.pageSize"));
	}
})