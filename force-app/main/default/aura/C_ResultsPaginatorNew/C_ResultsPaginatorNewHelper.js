({
    updateState : function(component) {
        var results = component.get("v.results");
        var total = parseInt(results.totalResultCount);
        var offset = parseInt(results.offset);
        var length = results.records ? results.records.length : 0;
        var pgSz = parseInt(results.pageSize);//parseInt(component.get("v.pageSize"));
        var currPage = (offset / pgSz) + 1;

        if(pgSz){
            if(pgSz === 20){
                var lastPgNum = total > 10000 ? 499 : Math.ceil(total / pgSz);
            }
            if(pgSz === 30){
                var lastPgNum = total > 10000 ? 333 : Math.ceil(total / pgSz);
            }
            if(pgSz === 50){
                var lastPgNum = total > 10000 ? 199 : Math.ceil(total / pgSz);
            }
        }


        var pgSeriesStart = currPage - 3 > 0 ? currPage - 3 : 1;
        var pgSeriesEnd = currPage + 3 < lastPgNum ? currPage + 3 : lastPgNum;
        var pgSeries = [];
        for (var i=pgSeriesStart; i<=pgSeriesEnd; i++) {
            pgSeries.push(i);
        }

        component.set("v.totalResults", total);
        component.set("v.currentOffset", offset);
        component.set("v.currentPage", currPage);
        component.set("v.pageSeries", pgSeries);
        component.set("v.displayFrom", offset + 1);
        component.set("v.displayTo", offset + length);
        component.set("v.lastPageNum", lastPgNum);
        component.set("v.pageSize", parseInt(results.pageSize));
    },

    firePageEvent : function(component, offset) {
        var pageEvent = component.getEvent("pageEvent");
        pageEvent.setParams({
            "offset" : offset,
            "pageSize" : component.get("v.pageSize")
        });

        pageEvent.fire();
        if(component.get("v.externalsource") === 'TD'){
            var reqSearchModalEvt = component.getEvent("ReqSearchModalResultsEvent");
            reqSearchModalEvt.setParams({"sourcebutton":"search","showReqSearchResults": true, "disableLinkButton": true});
            reqSearchModalEvt.fire();
        }
        component.set("v.visible", false);
    },

    firePageSizerEvent : function(component) {
        var pageSizerEvt = component.getEvent("pageSizeChangeEvent");
        pageSizerEvt.setParams({
            "pageSize" : component.get("v.pageSize")
        });
        pageSizerEvt.fire();
        component.set("v.visible", false);
    }
})