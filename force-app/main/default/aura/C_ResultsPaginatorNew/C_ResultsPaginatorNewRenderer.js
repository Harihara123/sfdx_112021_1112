({
    afterRender: function(cmp, helper){
        this.superAfterRender();
        let externalSource = cmp.get("v.externalsource");
        if(externalSource !== 'TD' || externalSource !== 'RQ'){
            let lookupPagination = () => {
                setTimeout(() => {
                    let pagination = document.getElementById(cmp.get('v.randomId'));
                    if (pagination) {
                        console.log('pagination');
                        pagination.style.position = 'fixed';
                        pagination.style.zIndex = 99999999;
                        pagination.style.bottom = '4px';
                        pagination.style.maxWidth = '700px';
                        // pagination.style.left = 'calc((100% - 700px)/2)';
                        pagination.style.background = '#fff';
                        pagination.style.left = '50%';
                        pagination.style.transform = 'translateX(-50%)';
                    } else {
                        lookupPagination();
                    }
                }, 1000);
            }
            lookupPagination();
        }

    }
})