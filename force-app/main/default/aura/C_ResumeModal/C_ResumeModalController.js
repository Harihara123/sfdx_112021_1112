({
	displayResumeModal : function(component, event, helper) {
		component.set("v.resumeName", event.getParam("resumeName"));
        const attachmentId = event.getParam("attachmentId");
		const htmlsource = event.getParam("htmlsource");
        const updatedDate = event.getParam("resumeUpdatedDate");
		const incomingId = event.getParam("destinationId");
		const destinationId = component.get("v.destinationId");

		if(incomingId === destinationId){
			if (htmlsource) {
				component.set("v.htmlteaser", htmlsource);
				component.set("v.updatedDate", updatedDate);
				helper.showModal(component);
			} else {
				const componentName = "c:C_TalentResumeHTMLViewer";
				const targetAttributeName = "v.C_TalentResumeHTMLViewer";
				const params = {
					"recordId": attachmentId
				}

				helper.createComponent(component, componentName, targetAttributeName, params);
				helper.showModal(component);
			}
		}
		

	},

	closeModal: function(component, event, helper) {
		helper.closeModal(component);
		component.set("v.attachmentId","");
		component.set("v.htmlteaser","");
	}
})