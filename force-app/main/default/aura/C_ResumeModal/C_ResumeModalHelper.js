({
	showModal : function (component, event, helper) {
        this.toggleClass(component,'backdropResume','slds-backdrop--');
        this.toggleClass(component,'modaldialogResume','slds-fade-in-');
        component.set("v.toggleScroll", true);
    },

	closeModal: function(component, event, helper) {
		this.toggleClassInverse(component,'backdropResume','slds-backdrop--');
		this.toggleClassInverse(component,'modaldialogResume','slds-fade-in-');
        component.set("v.toggleScroll", false);
    },

    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },

    createComponent : function(cmp, componentName, targetAttributeName, params) {
         $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                }
            }
        );
    }
})