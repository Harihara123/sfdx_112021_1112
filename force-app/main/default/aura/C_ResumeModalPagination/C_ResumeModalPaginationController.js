({
    displayResume: function(component, event, helper){
        let resumeArr = component.get("v.resumePairs"),
            resumeCount = component.get("v.resumeCount"),
            currentIndex = component.get("v.currentIndex");

        component.set("v.leftNav", true);
        component.set("v.rightNav", true);

        if(component.get("v.showResumeModal")){
            helper.setResume(component, resumeArr);

            // console.log(`The index on displayResume is ===> ${currentIndex}`);
            if(currentIndex === 0){
                component.set("v.leftNav", false);
            }

            if((currentIndex === resumeCount -1) || currentIndex === resumeCount || currentIndex === 20 || currentIndex === 30 || currentIndex === 50){
                component.set("v.rightNav", false);
            }
            helper.showModal(component);
        }

    },

    closeModal: function(component, event, helper) {
        component.set("v.showResumeModal", false);
        let modalEvt = component.getEvent("closeResumeModal");

        modalEvt.setParam("resumeId", component.get("v.resumePairs").id);
        modalEvt.fire();

        helper.closeModal(component);
    },

    paginateResume: function(component, event, helper) {

        helper.resumeNav(component, event);

    }
})