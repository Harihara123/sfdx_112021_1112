({
    showModal : function (component, event, helper) {
        this.toggleClass(component,'backdropResume','slds-backdrop--');
        this.toggleClass(component,'modaldialogResume','slds-fade-in-');
        component.set("v.toggleScroll", true);
    },

    closeModal: function(component, event, helper) {
        this.toggleClassInverse(component,'backdropResume','slds-backdrop--');
        this.toggleClassInverse(component,'modaldialogResume','slds-fade-in-');
        component.set("v.toggleScroll", false);
        component.set("v.resumeId","");
        component.set("v.htmlResume","");
        component.set("v.resumePairs", {});
    },

    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },

    resumeNav: function(component, event) {
        let tabIndex = component.get("v.currentIndex");
        if (event.target.id === 'prevResume') {
            // tabIndex = +tabIndex;
            // up
            event.preventDefault();
            let newInd = tabIndex - 1;
            if (newInd !== -1) {
                document.querySelectorAll('[tabindex="' + newInd + '"]')[0].focus();
                document.querySelectorAll('[tabindex="' + newInd + '"]')[0].click();
                // console.log(JSON.stringify(document.querySelectorAll('[tabindex="' + newInd + '"]')[0]));
                component.set('v.keyEventFired', true);
                component.set("v.rightNav", true);
            } else {
                component.set("v.leftNav", false);
            }
            component.set("v.loading", true);

            this.fetchResumes(component, newInd);
            this.refreshResume(component, event);

        } else if (event.target.id === 'nextResume') {
            // tabIndex = +tabIndex;
            // down
            event.preventDefault();
            let newInd = tabIndex + 1;
            if (document.querySelectorAll('[tabindex="' + newInd + '"]')[0]) {
                document.querySelectorAll('[tabindex="' + newInd + '"]')[0].focus();
                document.querySelectorAll('[tabindex="' + newInd + '"]')[0].click();
                // console.log(JSON.stringify(document.querySelectorAll('[tabindex="' + newInd + '"]')[0]));
                component.set('v.keyEventFired', true);
                component.set("v.leftNav", true);
            } else {
                component.set("v.rightNav", false);
            }

            component.set("v.loading", true);
            this.fetchResumes(component, newInd);
            this.refreshResume(component, event);
        }
    },

    fetchResumes: function (component, index) {
        let resumeEvt = component.getEvent("fetchResumes");

        resumeEvt.setParams({
            "keyEventFired": component.get("v.keyEventFired"),
            "index": index
        });
        resumeEvt.fire();

    },

    refreshResume: function(component, event) {
        let newIndex = component.get("v.newIndex"),
            resumeArr = component.get("v.resumePairs"),
            resumeCount = component.get("v.resumeCount");

        component.set("v.currentIndex", newIndex);

        // console.log(`The index on refreshResume is ===> ${newIndex}`);
        this.clearResume(component);

        if(newIndex === 0){
            component.set("v.leftNav", false);
        }

        if((newIndex === resumeCount -1) || newIndex === resumeCount){
            component.set("v.rightNav", false);
        }

        this.setResume(component, resumeArr);

        // console.log(`The index is ===> ${newIndex} and the data is ${JSON.stringify(resumeArr, null, ' ')}`);
    },

    setResume: function(component, item){
        component.set("v.loading", false);
        component.set("v.htmlResume", item.resume);
        component.set("v.resumeName", item.resumeName);
        component.set("v.updatedDate", item.resumeUpdatedDate);
        component.set("v.resumeId", item.id);
    },

    clearResume: function (component) {
        component.set("v.htmlResume", '');
        component.set("v.resumeName", '');
        component.set("v.updatedDate", '');
        component.set("v.resumeId", '');
    }
})