({
	checkSMS360Permission:function(component, helper) {
  
      let action = component.get("c.userHasSMS360Access");
      let params = {};

	  action.setCallback(this, function(response) {
			let state = response.getState();
			if(state == 'SUCCESS') {
				component.set('v.userHasSMS360Access',response.getReturnValue());
				if(response.getReturnValue()) {
					this.trackSMS(component, helper);
				}
				else {
					let utilityAPI = component.find("utilitybar");
					utilityAPI.getAllUtilityInfo().then(function (response) {
						response.map((item) => {
								if (item.utilityLabel === 'SMS') {
								utilityAPI.setPanelHeight({heightPX: 0, utilityId: item.id});
								utilityAPI.setPanelWidth({widthPX: 0, utilityId: item.id});
								utilityAPI.setUtilityLabel({label:'', utilityId: item.id});
								utilityAPI.setUtilityIcon({icon:'', utilityId:item.id});
							}
						});
					});
				}
				
			}
			else {
				console.log('Error');
			}
	  });
	  $A.enqueueAction(action);
    },
	trackSMS: function (component, helper) {
		window.setInterval(
            $A.getCallback(function() { 
                helper.getUpdatedSMSCount(component);
            }), 5000
        );
	},
	getUpdatedSMSCount : function(component) {
		var action = component.get("c.getSMSCount");
		action.setCallback(this, function(response){
			let state = response.getState();
			if(state == 'SUCCESS') {
				let smsCount = response.getReturnValue();
				if(smsCount > 0) {
					this.showAlert(component)
				}
				else {
					this.disablePopout(component);
				}
			}
		});
		$A.enqueueAction(action);
	},
	showAlert: function(component) {
		let utilityAPI = component.find("utilitybar");
		utilityAPI.getAllUtilityInfo().then(function (response) {
			response.map((item) => {
				if (item.utilityLabel === 'SMS') {
					utilityAPI.setUtilityHighlighted({highlighted: true, utilityId: item.id});	
					utilityAPI.disableUtilityPopOut({disabled: true});
					utilityAPI.onUtilityClick({
						utilityId: item.id,
						eventHandler: function(response){
							utilityAPI.setUtilityHighlighted({utilityId: item.id,highlighted: false});
						}
					});					
				}
			});
		});		
	}, 
	disablePopout : function(component) { //By default SMS UtilityBar window had icon for popout and thats not required.
		let utilityAPI = component.find("utilitybar");
		utilityAPI.getAllUtilityInfo().then(function (response) {
			response.map((item) => {
				if (item.utilityLabel === 'SMS') {
					utilityAPI.disableUtilityPopOut({disabled: true});
				}
			});
		});
	}

})