({
    openModal : function(component, event, helper) {
        let winloc = decodeURIComponent(window.location.hash);
		component.set('v.disableAlert', (
			helper.getUrlParameter('type') === 'C2R' || winloc.includes('type=C2R')
			// || helper.getUrlParameter('type') === 'R' || winloc.includes('type=R')
		));
        
        component.set('v.alert', false);
        component.set('v.maxAlertWarning', false);

		var parameters = event.getParam("arguments");
		if (parameters) {
			component.set("v.saveSearchTitle",""); 
			helper.parseEntry(component, parameters);
			helper.getSavedSearches(component);
			helper.getUserAlertCount(component);
		}
		helper.openModal(component);
	},
	// handleSelect: function(cmp, e, h) {
	// 	cmp.get('v.searchAlert').Alert_Frequency__c = e.target.value
	// },
	
	changeEmailAlert: function(cmp, e, h) {
		cmp.get('v.searchAlert').Send_Email__c = e.target.checked;
	},
	
	handleCheckChange: function(cmp, e, h) {
		// console.log(e.target.checked, e.type);

		let toggle = {
			true: () => {
				cmp.set('v.alert', true);
			},
			false: () => {
				cmp.set('v.alert', false);
				cmp.set('v.maxAlertWarning', false);
			}
		}
		toggle[e.target.checked]();
	},

	closeModal: function(component, event, helper) {
		helper.closeModal(component);
	},

	saveSearch: function(component, event, helper) {
		helper.saveSearch(component);
	},

    clearName: function(component, event, helper) {
        helper.clearName(component);
	},

	resetSaveSearch: function(component, event, helper){
		helper.resetSearch(component);
	},

	replaceSavedSearch: function(component, event, helper){
		helper.replaceSavedSearch(component,event);
	},

	


})