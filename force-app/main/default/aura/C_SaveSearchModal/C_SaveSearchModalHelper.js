({
    
    getSavedSearches : function (component, event, helper) {
        var action = component.get("c.getUserSearches"); 

        component.set("v.saveSearchTitle", ""); 
        action.setParams({
            'entryType' : ['AllSavedSearches']
        });
        action.setCallback(this, function(response) {
			var srchArray = [];
            if (response.getState() === "SUCCESS") {

                var result = response.getReturnValue();

                if(result !== ""){
                    var parsedSearches = JSON.parse(result);
                    for (var i=0; i<parsedSearches.searches.length; i++) {
                        var srch = JSON.parse(parsedSearches.searches[i]);
                        srch.time = $A.localizationService.formatDate(new Date(srch.execTime));
						if (srch.criteria.multilocation) {
                            srch.loc = this.getMultiLocText(srch.criteria.multilocation);
                        }
						if(srch.type =='C2C' || srch.type =='C2R' ||srch.type =='R2C'){
                            srch.sourceid = srch.match.srcid;
                            if(srch.match.data){
                                srch.sourceName = srch.match.data.srcName;
                           	} else {
                                srch.sourceName = '';
                            }
                                      
                        } else {
                            srch.sourceid = '';
                        }
                        srchArray.push(srch);
                    }
                }
            } 

            component.set('v.savedSearchList', srchArray);
            this.openModal(component);
        });
        $A.enqueueAction(action);
    },
    
    getMultiLocText : function(ml) {
        var txt = "";
        
        for (var i=0; i< ml.length; i++) {
            
            if(i == ml.length - 1){
                txt += ml[i].displayText;
            } else {
                txt += ml[i].displayText + " / ";
            }
            
        }
        return txt;
    },

    validateSaveSearch: function(component) {
        var isValid = true;
        var searchCountLimit = $A.get("$Label.c.SEARCH_HISTORY_COUNT");
        
        var titleField = component.find("saveSearchInput");
        var title = component.get("v.saveSearchTitle");
        if (title === undefined || title === null || title === "") {
            
            titleField.set("v.errors", [{message: "You must enter a title to save a search"}]);
            titleField.set("v.isError",true);
            $A.util.addClass(titleField,"slds-has-error show-error-message");
            isValid =  false;
            
        } else {
            titleField.set("v.errors", []);
            titleField.set("v.isError",false);
            $A.util.removeClass(titleField,"slds-has-error show-error-message");
            
        }
        if(component.get("v.savedSearchList").length >= searchCountLimit ){
            var index = component.get("v.searchReplaceIndex");
            if (index === undefined || index === null || index < 0) {
                isValid = false;
            }
        }

        return isValid;
    },

    replaceSavedSearch : function(component,event){
        var index = event.getSource().get("v.text");
        component.set("v.searchReplaceIndex" , parseInt(index)); 
    },

    saveSearch : function(component){

        if( component.get("v.alertCount") >= $A.get("$Label.c.SEARCH_HISTORY_COUNT")  && component.get("v.alert")  ){
            component.set("v.maxAlertWarning", true);
            this.validateSaveSearch(component);
            return;
        }else{

            if (this.validateSaveSearch(component)) {
                var searchEntry = component.get('v.searchEntry');
                searchEntry.title = component.get("v.saveSearchTitle");
                component.set('v.searchEntry', searchEntry);

                if (component.get('v.alert')) {

                    let searchAlert = component.get('v.searchAlert');
                    searchAlert.Name = searchEntry.title;
                    searchAlert.Alert_Criteria__c = JSON.stringify(searchEntry);
                    searchAlert.Query_String__c = component.get('v.alertQueryString');
                    searchAlert.Request_Body__c = component.get('v.alertRequestBody');    
                    searchAlert.Alert_Type__c = component.get("v.type");
                    if(component.get("v.type") == 'C' || component.get("v.type") == 'C2C' ||component.get("v.type") == 'R2C'){
                        searchAlert.Alert_Trigger__c = $A.get("$Label.c.ATS_TALENT_CREATED");
                    } else {
                        searchAlert.Alert_Trigger__c = $A.get("$Label.c.ATS_REQ_CREATED");
                    }
                 //   let queryStringCmp = component.find("queryStringCmp");
                  // let queryString = queryStringCmp.createQueryString( searchAlert.Alert_Criteria__c, component.get('v.runningUser') );
                    
                    component.set('v.searchAlert', searchAlert );
                }
                
                this.saveSearchLog(component);

            }

        }        

    },
    
    saveSearchLog : function (component) {
        // Call server to save search log entry
        var searchCountLimit = $A.get("$Label.c.SEARCH_HISTORY_COUNT");
        
		if(component.get("v.savedSearchList").length >= searchCountLimit) {
            var list = component.get("v.savedSearchList");
            list[component.get("v.searchReplaceIndex")] = component.get('v.searchEntry');
            component.set("v.savedSearchList",list);
        } 

        if( component.get("v.alert") ){

            var action = component.get("c.saveUserSearchWithAlert"); 
            // component.set("v.searchEntry.alert", true);
            
            action.setParams({
                "searchEntry" : JSON.stringify(component.get('v.searchEntry')),
                "replaceIndex" : component.get("v.searchReplaceIndex"),
                "entryType" : component.get("v.searchType"),
                "searchAlert" : component.get('v.searchAlert'),
            });
        
        }else{

            var action = component.get("c.saveUserSearch"); 
            action.setParams({
                "searchEntry" : JSON.stringify(component.get('v.searchEntry')),
                "replaceIndex" : component.get("v.searchReplaceIndex"),
                "entryType" : component.get("v.searchType")
            });

        }
        

        action.setCallback(this, function(response) {
			// Reset the replace index to -1
			component.set("v.searchReplaceIndex", -1);
            if (response.getState() === "SUCCESS") {
                // Server SUCCESS - save to attribute.
                this.successToast( component.get("v.alert") );
                var utilEvent = $A.get("e.c:E_RefreshUtilityEvent");
                utilEvent.fire();
                this.closeModal(component);
            }else if (response.getState() === "ERROR") {
                this.handleError(response);
                
            }
        });
        $A.enqueueAction(action);
    },

    
    successToast: function( isAlert ){
        var successToast = $A.get("e.force:showToast");
        var successMsg = "Your Search has been saved successfully!"; 

        if(isAlert){
            successMsg = "Your Search and Search Alert have been saved successfully.!"; 
        }
        
        successToast.setParams({
            type: "Success",
            message: successMsg,
        });
        successToast.fire();
    },

    parseEntry: function(component, params){
        component.set('v.searchEntry', JSON.parse(params.searchEntry));
        component.set('v.searchType', params.searchType);
        component.set('v.alertQueryString', params.queryString);
        if(params.matchRequestBody){
        	component.set('v.alertRequestBody', params.matchRequestBody);    
        }
        
    },

    resetSearch: function(component){
        var titleField = component.find("saveSearchInput");
        
        titleField.set('v.value', "");
        titleField.set("v.errors", []);
        titleField.set("v.isError",false);
        component.set('v.alert', false);
        $A.util.removeClass(titleField,"slds-has-error show-error-message");
    },

    closeModal: function(component) {
        this.toggleClassInverse(component,'backdropSaveSearchModal','slds-backdrop--');
        this.toggleClassInverse(component,'saveSearchModal','slds-fade-in-');
		component.set("v.openModalStyles", false);
        component.set("v.searchAlert.Send_Email__c", false );
        component.set("v.searchAlert.Alert_Frequency__c", 'daily');
       
    },

    openModal: function(component){
        this.toggleClass(component,'backdropSaveSearchModal','slds-backdrop--');
        this.toggleClass(component,'saveSearchModal','slds-fade-in-');
		component.set("v.openModalStyles", true);
    },

    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
        //  console.log(modal);
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },



    getUserAlertCount : function(component) {

        var action = component.get("c.getSearchAlertCount"); 
        
        action.setCallback(this, function(response) {

            if (response.getState() === "SUCCESS") {
                component.set('v.alertCount', response.getReturnValue() );
            }else if(response.getState() === "ERROR"){
                this.handleError(response);
            }
        });

        $A.enqueueAction(action);

    },
    
    handleError : function(response){
        
       // Use error callback if available
        if (typeof errCallback !== "undefined") {
            errCallback.call(this, response.getReturnValue());
            // return;
        }
        // Fall back to generic error handler
        var errors = response.getError();
        if (errors) {
            
            if (errors[0] && errors[0].message) {
                this.showError(errors[0].message);
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
            }else{
                this.showError('An unexpected error has occured. Please try again!');
            }
        } else {
            this.showError(errors[0].message);
            //throw new Error("Unknown Error");
        }
    },            

    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

    getUrlParameter : function(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            // if (sParameterName[0] === sParam) {
            //     return sParameterName[1] === undefined ? true : sParameterName[1];
            // }
            let paramName = sParameterName[0] || '';
            if (paramName.includes(sParam)) {
                return (sParameterName[1] === undefined) ? true : sParameterName[1];
            }
        }

        return '';
    }

    
})