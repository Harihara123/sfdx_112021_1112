({
	getActionParams: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.getActionParams(parameters.type, parameters.queryStringUrl, parameters.httpVerb, parameters.autoMatchRequestBody);
		}
	},

	/*getBaseParameters: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.getBaseParameters(component, parameters.component);
		}
	},

	createSearchLogEntry: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.createSearchLogEntry(component, parameters.component, parameters.baseEntry);
		}
	},

	restoreCriteriaFromLog: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.restoreCriteriaFromLog(component, parameters.component);
		}
	},*/

	getMatchInsightActionParams: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.getMatchInsightActionParams(parameters.type, parameters.queryStringUrl, parameters.httpVerb, parameters.autoMatchRequestBody);
		}
	},

	createQueryString: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.createQueryString(parameters.parameters);
		}
	},
    
    createFacetQueryString: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.createFacetQueryString(parameters.parameters);
		}
	},

	createMatchRequestBody: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.createMatchRequestBody(parameters.matchVectors, parameters.excludeKeyWords);
		}
	}
})