({
	isMatch: function(type) {
		return type === "C2R" || type === "R2C" || type === "C2C";
	},

	getActionParams: function (type, queryStringUrl, httpVerb, autoMatchRequestBody) {
		var typeConfig = this.config[type];

		if (this.isMatch(type)) {
			// Auto match request body needs to be passed in.
			// var autoMatchRequestBody = httpVerb === "GET" ? null : JSON.stringify(this.getAutoMatchRequestBody(criteriaCmp));
			return {
				"queryString" : queryStringUrl,
				"httpRequestType" : httpVerb, 
				"requestBody" : autoMatchRequestBody, 
				"location" : null
			};
		} else {
			return {
				"serviceName": typeConfig.serviceName, 
				"queryString": queryStringUrl
			};
		}
	},

	getMatchInsightActionParams: function (type, queryStringUrl, httpVerb, autoMatchRequestBody) {
		return {
			"queryString" : queryStringUrl,
			"httpRequestType" : httpVerb, 
			"requestBody" : autoMatchRequestBody, 
			"location" : null
		};
	},
	
	///////// createQueryString and dependencies ///////////
	createQueryString : function (qParams) {
	/*type, userId, keyword, pageSize, offset, grpFilter, matchSrcId, 
							requestId, transactionId, appId, opco, 
							location, facetProps, filterInitiator, userPsId, filterCriteriaParams, 
							selectedFacets, noSearchYet, isReexecute, singleFacet, noFacets,
							sortField, sortOrder, excludeKeyWords) {*/

		var parameters = this.getBaseParameters(qParams.type, qParams.userId, qParams.keyword, qParams.pageSize, qParams.offset, qParams.grpFilter, qParams.matchSrcId);

        parameters = this.addTrackParameters(parameters, qParams.requestId, qParams.transactionId, qParams.appId, qParams.opco);
        parameters = this.addLocationParameters(parameters, qParams.location);
        if (qParams.noFacets === false) {
			parameters = this.addFilterCriteriaParams(parameters, qParams.facetProps, qParams.filterInitiator, qParams.userPsId, qParams.filterCriteriaParams); //S-32364 Flipped the order with addFacetParameters
			parameters = this.addFacetOptions(parameters, qParams.facetProps, qParams.selectedFacets, qParams.noSearchYet, qParams.userPsId, qParams.isReexecute, qParams.filterInitiator); 
        }
        parameters = this.addFacetParameters(parameters, qParams.selectedFacets);
        parameters = this.addSortParameters(parameters, qParams.sortField, qParams.sortOrder);

        // Adding exclude keywords
        parameters = this.addExcludeKeywords(parameters, qParams.excludeKeyWords);

		return(this.buildUrl(parameters));
	},
    
    /** Method only differs from createQueryString in that ...
     * 1.  sort parameters are not added
     * 2.  noFacets conditional is removed
     * 3.  pageSize and offset are forced to 0
     **/
    createFacetQueryString : function (qParams) {
		var parameters = this.getBaseParameters(qParams.type, qParams.userId, qParams.keyword, 0/*pagesize*/, 0/*offset*/, qParams.grpFilter, qParams.matchSrcId);

        parameters = this.addTrackParameters(parameters, qParams.requestId, qParams.transactionId, qParams.appId, qParams.opco);
        parameters = this.addLocationParameters(parameters, qParams.location);
        parameters = this.addFilterCriteriaParams(parameters, qParams.facetProps, qParams.filterInitiator, qParams.userPsId, qParams.filterCriteriaParams); //S-32364 Flipped the order with addFacetParameters
        parameters = this.addFacetOptions(parameters, qParams.facetProps, qParams.selectedFacets, qParams.noSearchYet, qParams.userPsId, qParams.isReexecute, qParams.filterInitiator); 
        parameters = this.addFacetParameters(parameters, qParams.selectedFacets);

        // Adding exclude keywords
        parameters = this.addExcludeKeywords(parameters, qParams.excludeKeyWords);

		return(this.buildUrl(parameters));
	},

	getBaseParameters: function (type, userId, keyword, pageSize, offset, grpFilter, matchSrcId) {
		var typeConfig = this.config[type];

		if (this.isMatch(type)) {
			return {
				"size": pageSize, 
				"from": offset, 
				"type": typeConfig.matchType,
				"docId": matchSrcId,
				"userId": userId,
				"flt.group_filter": typeConfig.groupFilterOverride ? typeConfig.groupFilterOverride : grpFilter
			};
		} else {
			if (keyword === undefined || keyword === null || keyword === "") {
				// Assumes keywordless search is allowed for the applied facets. Checked during keyword validation.
				keyword = "-randomstring";
			}
			return {
				"id": userId, 
				"q": keyword, 
				"size": pageSize, 
				"from": offset, 
				"flt.group_filter": grpFilter
			};
		}
	},

	addTrackParameters : function (parameters, requestId, transactionId, appId, opco) {
		parameters.requestId = requestId;
		parameters.transactionId = transactionId;
        parameters.appId = appId;
		parameters.opco = opco;
        return parameters;
    },

    addLocationParameters : function (parameters, location) {
        if (location) {
            // Empty array used in location when doing an All location search.
            var loc = this.translateToSearchApiLocation(location);
            // if (loc.length > 0) {
                parameters.location = JSON.stringify(loc);
            // }
        } 

        return parameters;
    },

    translateToSearchApiLocation : function(loc) {
        var val = [];

        for (var i=0; i<loc.length; i++) {
            /*  Output value of "filter"
             *  useRadius   --> |  NONE  |  SELECT  |  AUTO 
             *  alwaysUseRadius |
             *  -------------------------------------------
             *    TRUE          |   N/A  |   FALSE  | FALSE
             *    FALSE         |  TRUE  |   FALSE  | FALSE
             */
            var filter = loc[i].useRadius === "NONE";
            var radius = typeof loc[i].geoRadius === "number" ? loc[i].geoRadius.toFixed() : loc[i].geoRadius;

            if (filter) {
                val.push({
                    "filter" : filter,
                    "city" : loc[i].city,
                    "state" : loc[i].state,
                    "country" : loc[i].country,
                    "postalcode" : loc[i].postalcode
                });
            } else {
                val.push({
                    "latlon" : loc[i].latlon,
                    "geoRadius" : radius,
                    "geoUnitOfMeasure" : loc[i].geoUnitOfMeasure,
                    "filter" : filter
                });
            }
        }

        return val;
    },

	addFilterCriteriaParams : function(parameters, facetProps, filterInitiator, userPsId, filterCriteriaParams) {
        var canListKey = "nested_facet_filter.myCandidateListsFacet";
        // Check for not a facet request OR specifically candidate list facet request.
        if ((filterInitiator === undefined || filterInitiator === canListKey) 
            && facetProps && facetProps[canListKey] && !facetProps[canListKey].isCollapsed) {
            parameters["nested_local_facet_filter.myCandidateListsFacet"] = JSON.stringify([{"User": [userPsId]}]);
        }

        if (filterInitiator) {
            // If filter params available for this specific facet request, add the params.
            if (filterCriteriaParams && filterCriteriaParams[filterInitiator]) {
                var currFacetFilter = filterCriteriaParams[filterInitiator];
                var keys = Object.keys(currFacetFilter);

                for (var j=0; j<keys.length; j++) {
                    parameters[keys[j]] = currFacetFilter[keys[j]];
                }
            } else {
                // If filter params not available and is filterable, add the default filters.
                var fp = facetProps[filterInitiator];
    
                if (fp.isFilterable) {
                    parameters[fp.optionKey + ".contains"] = "";
                    parameters[fp.optionKey + ".size"] = fp.fetchSize;
                }
            }
        } else 
            // Not a specific facet request and prior filter params are available.
            if (filterCriteriaParams) {
                // Include all available filter params for all facets ONLY if facet is open.
                var facets = Object.keys(filterCriteriaParams);

                for (var i=0; i<facets.length; i++) {
                    var facet = filterCriteriaParams[facets[i]];
                    if (!facetProps[facets[i]].isCollapsed) {
                        var facetKeys = Object.keys(facet);
                        for (var j=0; j<facetKeys.length; j++) {
                            parameters[facetKeys[j]] = facet[facetKeys[j]];
                        }
                    }
                }
        }

        return parameters;
    },

	addFacetOptions : function (parameters, facetProps, facets, noSearchYet, userPsId, isReexecute, singleFacet) { 
		if (singleFacet && singleFacet.length > 0) {
			return this.addSingleFacetOptions(parameters, noSearchYet, isReexecute, facetProps[singleFacet], null, userPsId);
		}

        if (facetProps && facetProps !== null) { 
            var keys = Object.keys(facetProps); 
            for (var i=0; i<keys.length; i++) { 
				parameters = this.addSingleFacetOptions(parameters, noSearchYet, isReexecute, facetProps[keys[i]], facets ? facets[keys[i]] : null, userPsId);
            } 
        } 
        return parameters; 
    }, 

	addSingleFacetOptions : function (parameters, noSearchYet, isReexecute, facetProp, facetSelection, userPsId) {
        var oKey = facetProp.optionKey; 
		var iniFilt = facetProp.initialFilter; 

        if (oKey !== undefined) {
            if (iniFilt && noSearchYet && !isReexecute) {
                // If there is an initial filter on this parameter.
                var filtKeys = Object.keys(iniFilt);
                for (var k=0; k<filtKeys.length; k++) {
                    parameters[filtKeys[k]] = iniFilt[filtKeys[k]];
                }
                parameters[oKey] = true; 
            } else if (!facetProp.isCollapsed || (isReexecute && facetSelection !== null && facetSelection !== undefined)) {
                // Turn on the facet option if facet is not collapsed. OR if this is a reexecute and the facet has a selection
				parameters[oKey] = true;
			}

            // If fetching the facets, also add the size parameter if available
            if (parameters[oKey] === true && facetProp.sizeKey !== undefined) {
                parameters[facetProp.sizeKey] = facetProp.fetchSize;
            }

			// If optionFilter exists, setup the include filters
            if (parameters[oKey] === true && facetProp.optionFilter !== undefined) {
                parameters[oKey + ".include"] = facetProp.optionFilter.replace("#psID#", userPsId);
            }
        }

        return parameters; 
    }, 

	addFacetParameters : function (parameters, selectedFacets) {
        if (selectedFacets && selectedFacets !== null) {
            var keys = Object.keys(selectedFacets);
            for (var i=0; i<keys.length; i++) {

                if (!this.isEmptyFacet(selectedFacets[keys[i]])) {
                    if (typeof selectedFacets[keys[i]] === "string") {
                        parameters[keys[i]] = selectedFacets[keys[i]];
                    } else {
                        parameters[keys[i]] = JSON.stringify(selectedFacets[keys[i]]);
                    }
                }
            }
            // console.log(parameters);
        }
        return parameters;
    },

    isEmptyFacet : function (facet) {
        var emptyFacet = false;
        if (facet !== undefined && facet !== null) {
            // Not undefined or null
            if (facet === "") {
                // String or any other basic type empty
                emptyFacet = true;
            } else if (typeof facet === "object" && (facet.length === undefined || facet.length === 0)) {
                // Object / array
                emptyFacet = true;
            }
        } else {
            emptyFacet = true;
        }

        return emptyFacet;
    },

	addSortParameters : function (parameters, sortField, sortOrder) {
		if (sortField) { 
            parameters.sort = sortField + ":" + sortOrder; 
        } 
        return parameters;
    },

    addExcludeKeywords : function (parameters, excludeKeyWords) {
        var allExcludes = [];
        if (excludeKeyWords && excludeKeyWords.length > 0) {
            allExcludes.push(excludeKeyWords);
        }
        if (allExcludes.length > 0) {
            parameters.exclude = allExcludes;
        }
        return parameters;
    },

	buildUrl : function (parameters) {
         var qs = "";
         for(var key in parameters) {
            var value = parameters[key];
            /* S-32457 adding condition to handle multiple facet keys.
               Keys sent as comma separated values for facet parame keys

               NOTE: if comma becomes a valid character this solution will not work. 

               */
            if(key.indexOf(",") > -1){
                var paramValues; 
                var paramKeys = key.split(",");

                if(value !== undefined && value !== null){
                    paramValues = value.split("~");
                    for(var i=0; i<paramValues.length; i++){
                      qs += encodeURIComponent(paramKeys[i]) + "=" + encodeURIComponent(paramValues[i]) + "&"; 
                    }
                }

            }else{
              qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
            }

         }
         if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
         }
         return qs;
    },
	///////// createQueryString and dependencies ///////////

	createMatchRequestBody : function (matchVectors, excludeKeyWords) {
        /*var autoMatchRequestBody = {};
        var automatch = {};
        var data = this.appendAutoMatchRequestData (matchVectors, excludeKeyWords);
        automatch.data = data;
        autoMatchRequestBody.automatch = automatch*/
        let autoMatchRequestBody = { 
            match_info: matchVectors.matchVectors? matchVectors.matchVectors : matchVectors
        };
        if(matchVectors.excludeKeyWords != undefined && matchVectors.excludeKeyWords.length > 0){
            autoMatchRequestBody.match_info.exclude = matchVectors.excludeKeyWords;
        }
        return JSON.stringify(autoMatchRequestBody);
    },

    /*appendAutoMatchRequestData : function (matchVectors, excludeKeyWords) {
       // console.log('yyy-->'+JSON.stringify(matchVectors));
        
        var data = {};
        var autoMatchCriteria = {};
		matchVectors = matchVectors.matchVectors;
        //Append exlude keywords
        if (excludeKeyWords && excludeKeyWords.length > 0) {
            autoMatchCriteria.exclude = excludeKeyWords;
        }
		
        autoMatchCriteria.jobTitles = matchVectors.title_vector;
        autoMatchCriteria.skills = matchVectors.skills_vector;
        autoMatchCriteria.keywords = matchVectors.other_vector;
		autoMatchCriteria.skillFamily = matchVectors.sf_vector;
		autoMatchCriteria.sf_domain = matchVectors.sf_domain_confidence.skillfamily_domain;
		autoMatchCriteria.sf_domain_confidence = matchVectors.sf_domain_confidence.skillfamily_domain_confidence;
        data.autoMatchCriteria = autoMatchCriteria;

		return data;
    },*/

	config: {
		"C": {
			"serviceName": "Search Service"
		},
		"R": {
			"serviceName": "Req Search Service",
			"groupFilterOverride": "AGS,RPO,MLA,AP,AG_EMEA,AEROTEK,TEK"
		},
		"C2C": {
			"matchType": "talent2talent"
		},
		"R2C": {
			"matchType": "job2talent"
		},
		"C2R": {
			"groupFilterOverride": "AGS,RPO,MLA,AP,AG_EMEA,AEROTEK,TEK",
			"matchType": "talent2job"
		}
	}
})