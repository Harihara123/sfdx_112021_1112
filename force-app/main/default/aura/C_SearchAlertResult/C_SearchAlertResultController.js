({
    // Santosh C : 05 FEB 2019 : To open TalentRecord onclick
    doInit: function (cmp, e, h) {
        let data = cmp.get('v.data');
        if (data.resumeLastModifiedDate) {
            let updatedDate = data.resumeLastModifiedDate.split('T')[0];
            data.updatedDate = updatedDate;
            cmp.set('v.data', data);
        }
    },
    openTalent: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": event.currentTarget.id
        });
        navEvt.fire();
        var utilityAPI = component.find("utilitybar");
        utilityAPI.minimizeUtility();
        //window.open('/' + event.currentTarget.id,'_blank');
    }
})