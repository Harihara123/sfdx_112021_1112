({
    doInit : function(component, event, helper) {
        
        //To check user group B permission
        var groupAPermissionsAction = component.get('c.checkGroupPermission');
			groupAPermissionsAction.setCallback(this, function(response) {
				var response = response.getReturnValue();
				//component.set("v.isUserInGroupB", response);
				component.set("v.userInGroups",response);				
				//component.set("v.userGroupInfoLoaded", true);
			});
			$A.enqueueAction(groupAPermissionsAction);
        
        
        component.set("v.showNotification",false);
        helper.getSavedAlerts(component);
        
        // below code is for Utility Bar Notification
        helper.checkUnseenAlerts(component, component.get("v.alerts"));
        
        if(component.get("v.showNotification")){
            let size = {x: window.innerWidth, y: window.innerHeight}
            let utilityAPI = component.find("utilitybar");
            utilityAPI.getAllUtilityInfo().then(function (response) {
                response.map((item) => {
                    if (item.utilityLabel === 'Alerts & Notifications') {
                    
                    utilityAPI.setUtilityHighlighted({
                    utilityId: item.id,
                    highlighted: true
                });
                
                console.log(item);
                utilityAPI.onUtilityClick({
                    utilityId: item.id,
                    eventHandler: function(response){
                        // this will be triggered when utility bar is clicked
                        helper.resetUtilityBarNewResultFlag(component);
                        
                        utilityAPI.setUtilityHighlighted({
                            utilityId: item.id,
                            highlighted: false
                        });
                        utilityAPI.getUtilityInfo().then(function(response) {
                            var myUtilityInfo = response;
                            console.log(myUtilityInfo);
                        }).catch(function(error) {
                            console.log(error);
                        });
                    }
                }).then(function(result){
                    console.log(result);
                }).catch(function(error){
                    console.log(error);
                });
                
               
            }
          })
        })
        .catch(function (error) {
            console.log(error);
        });    
    }
    
    
},
 
 
 toggleSection: function (cmp, e, h) {
    
    const toggleClass = {
        expanded: 'collapsed',
        collapsed: 'expanded'
    }
    
    if ('header-'+e.currentTarget.id === e.target.id) {
        e.currentTarget.className = toggleClass[e.currentTarget.className];
    } else {
        console.log('not the header!')
    }
   
},
    
    handleClick: function (cmp, e, helper) {
        
        e.stopPropagation();
        
        

        console.log(e.target.getAttribute('data-action'), e.currentTarget.id);
        
        const actions = {
                        showResults: () => {
                            cmp.set('v.showSpinner', true);
                            cmp.set('v.result', e.currentTarget.id);
                            helper.resetSeenCount(cmp,e.currentTarget.id);
                        },
                        emailOnOff: () => {
                            
                            helper.setEmailPreference(cmp,e.currentTarget.id);
                            // console.log('turning on/off email notifications for: '+e.currentTarget.id);
                        },
                        delete: () => {
                            helper.deleteAlert(cmp,e.currentTarget.id);
                            // console.log('Deleting alert: '+ e.currentTarget.id)
                        }
                    };
                        actions[e.target.getAttribute('data-action')?e.target.getAttribute('data-action'):'showResults']()
                       // helper.getSavedAlerts(cmp);
         },
                    
  })