({
	// Santosh C : 29 JAN 2019 : 
	// Calls searchController.getUserAlertResultMap() and Sets v.results
    getSavedAlertResults : function(component, alertId) {
        var action = component.get("c.getUserAlertResultMap");  
        action.setParams({
            "alertId" : alertId
        });
        action.setCallback(this, function(response){
            
            var state = response.getState();
            if (state === "SUCCESS"){
				
                if( response.getReturnValue().hasOwnProperty('Error') ){
                     this.showToast("Some error has occured, please contact your System Administrator ","Error","error");
                }else{

                    var result = response.getReturnValue();
                    var arrayMapKeys = [];
                    for(var key in result){
                        var resultString = result[key];
                        var  my_details_in_json = JSON.stringify(resultString);
                        var parsed = JSON.parse(my_details_in_json);
                        var theVal = []; 
                        for(var s in JSON.parse(my_details_in_json)){ 
                            theVal.push(JSON.parse(parsed[s])); 
                        }
                        theVal = theVal.reverse();
                        arrayMapKeys.push({key: key, value: theVal});
                    }
                    component.set("v.results", arrayMapKeys);
                    if(!component.get('v.result') && arrayMapKeys.length > 0 && arrayMapKeys[0].value.length > 0) {
                        component.set("v.result", arrayMapKeys[0].value[0].alertId)
                    }
                    component.set('v.showSpinner', false);

                }
                
            }
            else if(state === "ERROR"){
                this.showToast("Some error has occured, please contact your System Administrator ","Error","error");
            }
        });
        $A.enqueueAction(action);
    },

    getSavedAlerts : function(component) {
       
        var action = component.get("c.getSearchAlertCriteria");  
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                if(response.getReturnValue().length > 0){
                    if(component.get("v.result")){
                        // call getResults when email preference is updated/or alert is deleted and page gets refreshed
                      	this.getSavedAlertResults( component, component.get("v.result") );  
                    } else{
                        // setting the result attribute as after reseting the counter referesh event will fire
                        // this method will be called again then it should go to if condition
                        // purpose of this is by default we show results of recent alert its counter shoul be set to zero
                      component.set("v.result",response.getReturnValue()[0].Id);  
                       this.resetSeenCount(component,response.getReturnValue()[0].Id); 
                      this.getSavedAlertResults( component, response.getReturnValue()[0].Id );  
                    }
                 
                }
                component.set("v.alerts", response.getReturnValue() );
            }else if(state === "ERROR"){
                this.showToast("Some error has occured, please contact your System Administrator ","Error","error");
            }

        });

        $A.enqueueAction(action);
    },
    
   
    
    setEmailPreference :function(component,alertId){
    	var action = component.get("c.setAlertCriteriaEmailPreference");
    	action.setParams({
            "alertId" : alertId
        });

		action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                this.showToast("Email Notification Preference Updated Successfully","Success","success");
            	var utilEvent = $A.get("e.c:E_RefreshUtilityEvent");
                utilEvent.fire();
            }else if( response.getState() === "ERROR"){
                this.showToast("Some error has occured, please contact your System Administrator ","Error","error");
            }
        });
        $A.enqueueAction(action); 
	},
    
    deleteAlert :function(component,alertId){
    	var action = component.get("c.deleteAlertByID");
    	action.setParams({
            "alertId" : alertId
        });

		action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                if(response.getReturnValue()){
                	this.showToast("Alert Deleted Successfully","Success","success"); 
                    var utilEvent = $A.get("e.c:E_RefreshUtilityEvent");
                	utilEvent.fire();
                    
                }else{
                    this.showToast("Failed to delete Alert","Error","error");    
                }
                
            }else if (response.getState() === "ERROR") {
                this.showToast("Some error has occured, please contact your System Administrator ","Error","error");
            }
        });
        $A.enqueueAction(action);
	},
    
    resetSeenCount :function(component,alertId){
    	var action = component.get("c.resetSeenResultCount");
    	action.setParams({
            "alertId" : alertId
        });

		action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                //after reseting counter call refersh event to update the value in front end also
                var utilEvent = $A.get("e.c:E_RefreshUtilityEvent");
                	utilEvent.fire();
                   
            }else if (response.getState() === "ERROR") {
                this.showToast("Some error has occured, please contact your System Administrator ","Error","error");
            }
        });
        $A.enqueueAction(action);
	},
	
    
     checkUnseenAlerts : function(component,alertCriteria){
      
        if(alertCriteria.length>0){
                for( var i =0 ; i< alertCriteria.length ;i++){
                
                if(alertCriteria[i].NewResultFlag__c){
                    component.set("v.showNotification",true);
                    break;
                }
            }    
        }
        
       // console.log('checkUnseenAlerts--showNotification'+component.get("v.showNotification"));
    },
    
    resetUtilityBarNewResultFlag :function(component){
    	component.set("v.showNotification",false);
		var action = component.get("c.resetNewResultFlag");
    	

		action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
               
                this.getSavedAlerts(component);
                   
            }else if (response.getState() === "ERROR") {
                this.showToast("Some error has occured, please contact your System Administrator ","Error","error");
            }
        });
        $A.enqueueAction(action);
	},
    
	showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },    
})