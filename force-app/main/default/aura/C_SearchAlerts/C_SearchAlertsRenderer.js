({
    afterRender: function(cmp, helper){
        this.superAfterRender();
		let utilityAPI = cmp.find("utilitybar");
        utilityAPI.getAllUtilityInfo().then(function (response) {
            response.map(item => {
                if (item.utilityLabel === 'Saved/Recent Search & Match') {
                utilityAPI.onUtilityClick({
                utilityId: item.id, 
                eventHandler: () => {
                	var trackingEvent = $A.get("e.c:TrackingEvent");
		        	trackingEvent.setParam('clickTarget', 'saved_recent_search_match');
        			trackingEvent.fire();
            }
				})
	        } else if (item.utilityLabel === 'Alerts & Notifications') {
                utilityAPI.onUtilityClick({
                utilityId: item.id, 
                eventHandler: () => {
                	var trackingEvent = $A.get("e.c:TrackingEvent");
		        	trackingEvent.setParam('clickTarget', 'search_alerts');
        			trackingEvent.fire();            
                }
                })
    	    }
            })
        })
    }
})