({
    showToast : function(cmp, e, h){
        console.log(e, e.getParams())
        const detail = e.getParams();

        //message, title, toastType
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: detail.title,
            message: detail.message,
            type: detail.toastType
        });
        toastEvent.fire();
    },
    highlightUB: function (cmp, e, h) {


        const detail = e.getParams();
        const searchAlerts = cmp.find('searchAlerts');


        let utilityAPI = cmp.find("alertsUtilityBar");
        utilityAPI.getEnclosingUtilityId()
            .then((utilityId) => {
                utilityAPI.setUtilityHighlighted({highlighted: detail.highlight, utilityId})
                utilityAPI.onUtilityClick({utilityId, eventHandler: () => {
                        utilityAPI.setUtilityHighlighted({highlighted: false, utilityId})
                        searchAlerts.init();
                    }})
            })
            .catch(err => console.log(err))


        // utilityAPI.getAllUtilityInfo()
        //     .then((res) => {
        //         const alerts = res.filter(alert => alert.utilityLabel === 'LWC Search ALerts');
        //         utilityAPI.setUtilityLabel({label: 'LWC Search ALerts (10)', utilityId: alerts.id})
        //         // utilityAPI.setUtilityHighlighted({
        //         //     utilityId: alerts.id,
        //         //      highlighted: true
        //         // });
        //     })
        //     .catch(err => console.log(err))

    },
    updateSearchHistoryTab: function(cmp, e, h) {
        // Update create alert icon on Search History Utility Tab
        const appEvt = $A.get("e.c:E_RefreshUtilityEvent");
        appEvt.fire();
    }
})