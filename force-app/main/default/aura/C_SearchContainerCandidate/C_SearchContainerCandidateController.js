({
    populateReqSearchModal : function(component, event, helper) {
        var obj = event.getParam("reqModalObj");
        obj.externalsource = component.get("v.externalsource");
        component.set("v.ReqSearchModalObj",obj);
    },

    handleCreateEvent : function(cmp, event, helper){
        var componentName = event.getParam("componentName"),
            targetAttributeName = event.getParam("targetAttributeName"),
            params = event.getParam("params"),
            urlEvent = $A.get("e.c:E_MassAddToContactList");

        helper.createComponent(cmp, urlEvent, componentName, targetAttributeName, params);
    },

    openActivityModal: function(component, event, helper) {
        var params = {
            "recordId": event.getParam("recordId"),
            "activityType": event.getParam("activityType"),
            "eventOrigin" : event.getParam("eventOrigin")
        },
        urlEvent;

        helper.createComponent(component, urlEvent, 'c:C_TalentActivityAddEditModal', 'v.C_TalentActivityAddEditModal', params);
    },

	doInit : function(component, event, helper){
			var urlV = new URL(window.location.href);
			component.set("v.urlParams",urlV.searchParams);
			var type = component.get("v.urlParams").get("c__type");	
		
			component.set("v.type",type);	
			var engine = component.get("v.urlParams").get("c__engine"); 
			component.set("v.engine",engine); 

			if (type == 'C2C') {
				var accountId = component.get("v.urlParams").get("c__matchTalentId");
				var contactId =  component.get("v.urlParams").get("c__contactId");
				var name =  component.get("v.urlParams").get("c__srcName");
	
				var automatchSourceData = { 
							"srcLinkId" : contactId, 
							"srcName" : name  
				 } 
	
				component.set("v.automatchSourceId", accountId);
				component.set("v.automatchSourceData", automatchSourceData);		
				component.set("v.opportunityId", null);	
				component.set("v.executeSearch",true); 
				component.set("v.reExecuteSearch", true);
			} 
	
			var groupAPermissionsAction = component.get('c.checkGroupPermission');
				groupAPermissionsAction.setCallback(this, function(response) {
					var response = response.getReturnValue();
					
					component.set("v.userInGroups",response);	
				
					if (component.get("v.userInGroups.isInGroupB") ){
						component.set("v.inGroupB", true);
					}
					component.set("v.defaultSearch", "ES");        
					
					component.set("v.userGroupInfoLoaded", true);
				});
			$A.enqueueAction(groupAPermissionsAction);
	},
	 
	handleMatchRequest: function(component, event, helper) {
		component.set("v.userGroupInfoLoaded", false);     
				var reqType = event.getParam("type");
				component.set("v.type", reqType);
				component.set("v.automatchSourceId", event.getParam("srcId"));
				component.set("v.automatchSourceData", event.getParam("automatchSourceData"));		
				component.set("v.opportunityId", null);	
				component.set("v.executeSearch",true); 
				component.set("v.userGroupInfoLoaded", true);		

	},

	onPrefsLoaded: function(component, event, helper) {
		console.log('Event received ++++++++++++++++++++++++++++');
		//console.log(JSON.parse(event.getParams().userPrefs));
	}

})