({
	createComponent : function(cmp, urlEvent, componentName, targetAttributeName, params) {
		 $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                	if(urlEvent !== undefined){
                		var body = cmp.get(targetAttributeName);
						cmp.set(targetAttributeName, newComponent);
						var recordID = params.recordId;
						var urlEvent;
	                    
						urlEvent.setParams({
							"recordId": recordID
						});
						urlEvent.fire();
                	} else {
                		cmp.set(targetAttributeName, newComponent);
                	}
					
                }
            }
        );
	},

	navigateToTalentSearch : function(component) {	
		var url = $A.get("$Label.c.ConnectedOneSlashURL")+"/n/ATS_CandidateSearchOneColumn?";	
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": url
		});
		urlEvent.fire();
	},
	
	setUserPrefs :  function(component,defaultSearch) {		
		var action = component.get("c.setUserDefaultSearchPreferences");
		action.setParams({'defaultSearch':defaultSearch});
        action.setCallback(this, function(response) {
			component.set("v.serverProcessing",false);
            if (response.getState() === "SUCCESS") {				
				this.navigateToTalentSearch(component);
			}
		});
		$A.enqueueAction(action);		
	},
})