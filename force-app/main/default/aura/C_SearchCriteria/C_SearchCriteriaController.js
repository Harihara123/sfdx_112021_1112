({
	doInit : function(component, event, helper) {
        let engineAndQueryType = $A.get("e.c:TrackingEvent");
        let type = component.get('v.type') === 'C2R' || component.get('v.type') === 'R2C' || component.get('v.type') === 'C2C' ? 'match' : 'search'
        let payload = {"engine": component.get("v.engine"), "type": type};
        if (type == "match") {
            payload.matchType = component.get("v.type");
        }
        engineAndQueryType.setParam('engineType', payload);
        engineAndQueryType.fire();
        
        let locationEvent = $A.get("e.c:E_TrackingLocationInput");
        locationEvent.setParam('location', []);
        locationEvent.fire();
		helper.parseURLParameters(component);
		
		var c__radius = component.get("v.urlParams").get("c__radius");
		var c__radiusUnit =  component.get("v.urlParams").get("c__radiusUnit");
		component.set("v.radius",c__radius)
		component.set("v.radiusUnit",c__radiusUnit)
		component.set("v.sourceType",component.get("v.urlParams").get("c__sourceType"))
	
		// helper.initializeType(component);
		/* For cross tab scenarios (C2R / R2C), lightning runs the init for both tabs with the wrong target.
		 * Checking the target/externalsource against the URL to ensure only the correct components execute.
		 */
		let externalSource = component.get("v.externalsource");

        if ((externalSource === "TS" && window.location.href.indexOf("ATS_CandidateSearchOneColumn") < 0)
			|| (externalSource === "RS" && window.location.href.indexOf("Search_Reqs") < 0)) {
			return;
		} else {
			if ((externalSource === "TD") || (externalSource === "RQ")) {
				component.set("v.searchAppId", "ATS_EntityLink");
			}
			helper.initializeSearch(component);
		}
		/* if (component.get("v.executeSearch")) { // only for C2C case
			component.set("v.multilocation", null);
			component.find("locationCriteriaCmpId").clearLocation();
			// Set HTTP Request to Get
			helper.setHttpRequestType(component, "GET");		
			component.set("v.excludeKeyWords",[])
			helper.clearResults(component);
			helper.clearFacets(component);
			helper.validateAndSearch(component, null, true, false);
			event.stopPropagation();
		}*/

	},

	registerFacetHandler : function(component, event, helper) {
		helper.updateFacetProps(component, event);
        // helper.resetHandler(component);
        if (event.getParam("initialFilter")) {
            helper.updateDefaultFilters(component, event);
        }
		event.stopPropagation();
	},

	validateAndSearchTalents: function (component, event, helper) {
        const lwc = component.find('lwcSearchInput');
        let type = component.get('v.type');
        
        component.set('v.isSmartSearch', false); // Set smart search back to false before checking

        if (lwc && component.get('v.hasSmartSearchPermission') && type == 'C') {
            if (lwc.isSmartSearch()) {
                component.set('v.isSmartSearch', true);

                let queryTerms = lwc.getSelectedTerms();
                helper.smartSearch(component, queryTerms, true);
            }
            else {
                component.set("v.matchVectors", null);
                lwc.search();
            }
        }
		else if(lwc) {
			lwc.search();
		} else {
			helper.validateAndSearch(component, null, false, false); 
		}

        //helper.clearFacets(component);
		//helper.validateAndSearch(component, null, false, false);
	},

    handleSmartSearch: function(component, event, helper) {
        const lwc = component.find('lwcSearchInput');
        let type = component.get('v.type');

        if (lwc && component.get('v.hasSmartSearchPermission') && type == 'C') {
            component.set('v.isSmartSearch', true);

            let queryTerms = lwc.getSelectedTerms();
            helper.smartSearch(component, queryTerms, true);
        }
    },

	resetSearch: function (component, event, helper) {
		helper.fireLinkModalEvent(component,"refresh");
	},

	searchOnEnter: function (component, event, helper) {
		let searchCriteria = event.getParam("criteria");
		component.set("v.keyword", searchCriteria);

		helper.validateAndSearch(component, null, false, false);
	},
	resizeKeywordField: function(component, event, helper) {
		let focusEvt = event.getParam("keywordFocus");
		component.set("v.keywordFocus", focusEvt);
	},

	switchToSearch: function(component, event, helper){
        if (component.get("v.target") === "talent"){
			window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?_3321=" + Math.random();
		}
		else{
			window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Search_Reqs?_3321=" + Math.random();
		}

    },

  	requestFacet: function(component, event, helper) { 
    	helper.requestFacet(component, event); 
		event.stopPropagation();
  	}, 

	filterFacet: function(component, event, helper) {
		helper.saveFilterCriteriaParams(component, event);
		helper.filterFacet(component, event);
		event.stopPropagation();
	},

    applySearchFacets: function(component, event, helper) {
		component.set("v.matchVectors", event.getParam("matchVectors")); //To enable multi select gesture on Search/Match

        component.set("v.facets", event.getParam("facets"));
		component.set("v.facetDisplayProps", event.getParam("displayProps"));

        /* Also adding in the nested_filter parameters */
        helper.updateNestedFacets(component, event);
        // Update the filterParameters for filtered facets.
        helper.updateFacetFilterParams(component, event);
        // Updated for exclude keywords
        var excludeKeyWords = event.getParam("excludeKeyWords");
        component.set("v.excludeKeyWords", event.getParam("excludeKeyWords"));

        let isSmart = component.get('v.isSmartSearch');
        if (!isSmart) {
            helper.validateAndSearch(component, event.getParam("initiatingFacet"), false, false);
        }
        else {
            const lwc = component.find('lwcSearchInput');
            let queryTerms = lwc.getSelectedTerms();
            helper.smartSearch(component, queryTerms, true);
        }

	    //helper.toggleUpdateButton(component, false);

		event.stopPropagation();
	},

	updateFacetState: function(component, event, helper) {
		helper.updateFacetOptions(component, event.getParams());
		event.stopPropagation();
	},

	handleSort: function(component, event, helper) {
		component.set("v.sortField", event.getParam("sortField"));
		component.set("v.sortOrder", event.getParam("sortOrder"));

        let isSmart = component.get('v.isSmartSearch');
        if (!isSmart) {
            helper.validateAndSearch(component, null, true, false);
        }
        else {
            const lwc = component.find('lwcSearchInput');
            let queryTerms = lwc.getSelectedTerms();
            helper.smartSearch(component, queryTerms, true);
        }

		event.stopPropagation();
	},

	scrollSearch: function(component, event, helper) {
		component.set("v.pageSize", event.getParam("pageSize"));
		component.set("v.offset", event.getParam("offset"));
        component.set("v.isPagination", true);
		component.set("v.isMatchTalentInsightCall", false); // to make basic call search
		
        let isSmart = component.get('v.isSmartSearch');
        if (!isSmart) {
            helper.validateAndSearch(component, null, true, false);
        }
        else {
            const lwc = component.find('lwcSearchInput');
            let queryTerms = lwc.getSelectedTerms();
            helper.smartSearch(component, queryTerms, false);
        }
        
		event.stopPropagation();
	},

    facetChanged: function(component, event, helper) {
        //Trigger Update Results button when facets change
        helper.toggleUpdateButton(component, true);
        event.stopPropagation();
    },

	updatePageSize: function(component, event, helper) {
		component.set("v.pageSize", event.getParam("pageSize"));
		component.set("v.offset", 0);

        let isSmart = component.get('v.isSmartSearch');

        if (!isSmart) {
            helper.validateAndSearch(component, null, true, false);
        }
        else {
            const lwc = component.find('lwcSearchInput');
            let queryTerms = lwc.getSelectedTerms();
            helper.smartSearch(component, queryTerms, true);
        }

		event.stopPropagation();
	},

	handlePrefsUpdate: function(component, event, helper) {
		// show on/off update result when prefs add / remove
		var action = component.get("c.saveUserPreferences");
        action.setParams({
            "prefs" : JSON.stringify(component.get("v.userPrefs"))
        });
        action.setCallback(this, function(response) {
            if (response.getState() !== "SUCCESS") {
                console.log("Error saving preferences");
            }
        });

        $A.enqueueAction(action);
		event.stopPropagation();
	},

	handleMatchRequest: function(component, event, helper) {
        var reqType = event.getParam("type");
		component.set("v.type", reqType);
		component.set("v.automatchSourceId", event.getParam("srcId"));
		component.set("v.automatchSourceData", event.getParam("automatchSourceData"));
		component.set("v.multilocation", null);
		component.find("locationCriteriaCmpId").clearLocation();
        // Set HTTP Request to Get
        helper.setHttpRequestType(component, "GET");
        // If the request is CTC - reset exclude keywords to null
        if(reqType==='C2C'){
            component.set("v.excludeKeyWords",[])
        }
        component.set("v.opportunityId", null);
		helper.clearResults(component);
		helper.clearFacets(component);

		// Set to false when new match request happens
		component.set("v.isPagination", false);
		component.set("v.isSmartSearch", false);

		helper.validateAndSearch(component, null, true, false);
		event.stopPropagation();
	},

	saveSearchModal: function(component, event, helper) {
		var saveModalCmp = component.find("saveSearchModal");
		saveModalCmp.openModal(helper.createSearchLogEntry(component), helper.getSavedLogTypeName(component),helper.createQueryStringHelper(component,false,true),helper.createMatchRequestBody(component));
		//saveModalCmp.openModal(helper.createSearchLogEntry(component), helper.getSavedLogTypeName(component),helper.createQueryString(component,false,true),'');

	},

	shareSearchModal: function(component, event, helper) {
		$A.createComponent("c:C_ShareSearchModal", { "searchEntry" : JSON.parse(helper.createSearchLogEntry(component)),
				"runningUser" : component.get("v.runningUser"),
				"shareFrom" : "SearchPage",
				"isMyLists" : component.get("v.isSharable")
			},
			function(content, status) {
				if (status === "SUCCESS") {
					component.find('overlayLib').showCustomModal({
						body: content,
						cssClass: "mymodal"
					})
				}
			});
	},

	kwChangeHandler : function(component, event, helper) {
		let wrapper = document.querySelectorAll('[data-tracker="keywords_search"]')[0];
		if (wrapper) {
			wrapper.querySelector('input').value = component.get('v.keyword');
		}
		//helper.initTrkRequestId(component);
		helper.initTrkTransactionId(component);
	},

	applyMatchFacets : function(component, event, helper) {
		//helper.updateMatchVectors(component, event.getParams());
		helper.toggleUpdateButton(component, true);
		helper.setHttpRequestType(component, "POST");
		//helper.validateAndSearch(component, null, true, false);
		event.stopPropagation();
	},

    updateResults : function(component, event, helper) {
        // GENERATE NEW SubTransactionID
        let subTransactionId = helper.guid();
        component.set('v.subTransactionId', subTransactionId);
        let transactionEvent = $A.get("e.c:ServerCallTracking");
        transactionEvent.setParam('subTransactionId', component.get("v.subTransactionId"));
        transactionEvent.fire();
		component.set("v.isMatchTalentInsightCall", false); // to make a basic search call.
		var facetContainer = component.find("facetContainer");
		facetContainer.applyFacets();
		component.set("v.showUpdateButton", false);
		// if (component.get("v.type") == "R2C") {
		// 	component.set("v.isExpanded", false);
		// }
	},

	matchInsightsRequest : function(component, event, helper) {
		component.set("v.isMatchTalentInsightCall", true);
		component.set("v.type", event.getParam("type"));
		component.set("v.automatchSourceId", event.getParam("srcId"));
		component.set("v.filterCandidateOrJobId",event.getParam("recordId"));
		helper.getMatchInsightData(component, event.getParam("recordName"));
		event.stopPropagation();
    },
    handleKeyWordsFlyOutEvent : function(component, event, helper) {
        // Do we have set Get Or Post based on the inputValues size?
        helper.toggleUpdateButton(component, true);
        helper.setHttpRequestType(component, "POST");
        event.stopPropagation();
    },
    toggleFacetContainer: function(component, event, helper) {
    	let isExpanded = component.get("v.isExpanded");
        var trackingEvent = $A.get("e.c:TrackingEvent");
        if (isExpanded) {
        trackingEvent.setParam('clickTarget', 'hide_facets');   
        } else {
        trackingEvent.setParam('clickTarget', 'show_facets');
        }
        trackingEvent.fire();
        console.log('toggling');
    	component.set("v.isExpanded", !isExpanded);
    },

    updateKeyword : function(component, event, helper) {
    	helper.updateKeyword(component);			
    },

    saveSearchAlert: function (component, event ) {
    	debugger;
        alert('DONE...');
		// var alert = event.getParam("searchAlert");
  //  		var params = event.getParam("params");
  //  		alert('Event Fired');
  //  		console.log('xxxxxxx' + JSON.stringify(alert));
  //  		console.log('xxxxxxx' + JSON.stringify(params));
   },

   openMatchReqPopOver : function (component, event ) {
		if (component.get("v.showMatchReqPopOver") === true) {			
			window.removeEventListener('click');
			component.set("v.showMatchReqPopOver", false);
		} else {
			component.set("v.showMatchReqPopOver", true);
		}
   },

  hideEditPopOverHandler: function (component, event, helper) {
		component.set("v.showMatchReqPopOver", false);
	},

  handleFeedback : function(component, event, helper){
		//var reqId = component.get("v.trkRequestId");
        var trkId = component.get("v.transactionId");
	    //var prevFeedback = component.get("v.prevFeedback");
		/* feedback structure
		{ "resultDocId1": 1, // upvote
		  "resultDocId2": 0, // downvote
		  ...
		  "resultDocIdn": 1 // upvote
		}*/

		var feedback = component.get("v.feedback");
		if (!feedback) {
			feedback = {};
		}
		var evtParams = event.getParams();
		if (evtParams.downvote === false && feedback[evtParams.resultDocId] !== undefined) {
			delete feedback[evtParams.resultDocId];
		}
		// else if (evtParams.upvote === true) {
		// 	feedback[evtParams.resultDocId] = 1;
		// }
		
	    else if(evtParams.downvote === true){
			feedback[evtParams.resultDocId] = 0;
		}  
		
		component.set("v.feedback", feedback);
		
		var action = component.get("c.saveFeedback");

		action.setParams({
			//"requestId": reqId, 
            "transactionId": trkId,
			"feedbackData": JSON.stringify(feedback)
		});
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS") {
				response = response.getReturnValue();
				
			}
		});
		$A.enqueueAction(action);
		
		event.stopPropagation();
      
      
  },
	// Methods for paginated resume modal
	fetchResumes: function (component, event, helper) {
		// component.set("v.currentResumeId", event.getParam("resumeId"));
		component.set("v.newIndex", event.getParam("index"));
		component.set("v.keyEventFired", event.getParam("keyEventFired"));
	},

	setResumeModalData: function (component, event, helper) {
		component.set("v.resumePairs", event.getParam("resumeArr"));
		component.set("v.currentResumeId", event.getParam("resumeId"));
		component.set("v.currentIndex", event.getParam("currentIndex"));
		component.set("v.resumeCount", event.getParam("resumeCount"));
		component.set("v.showResumeModal", true);
	},

	setFocus: function (component, event, helper) {
		component.set("v.selectRow", event.getParam("resumeId"));
		// console.log(`ID after resume close ===> ${event.getParam("resumeId")}`);
	},
    subIdHandler: function(component, event, helper) {
        component.set('v.subTransactionId', component.get('v.subId'));
        let transactionEvent = $A.get("e.c:ServerCallTracking");
        transactionEvent.setParam('subTransactionId', component.get("v.subTransactionId"));
        transactionEvent.fire();
    },
    
    locationChangeLog: function(component, event, helper) {
        let locations = (component.get('v.multilocation') || []).map(location => {
            let formattedLocation = {};
			formattedLocation.name = location.displayText;
            formattedLocation.radius = location.useRadius == 'SELECT' ? location.geoRadius : 'N/A';
			return formattedLocation;
        })
        let locationEvent = $A.get("e.c:E_TrackingLocationInput");
        locationEvent.setParam('location', locations);
        locationEvent.fire();
    },
    
    filterValueChange: function(component, event, helper) {
        helper.subTransactionId(component);
    },
    queryTypeChange: function(component, event, helper) {
        let engineAndQueryType = $A.get("e.c:TrackingEvent");
        let type = component.get('v.type') === 'C2R' || component.get('v.type') === 'R2C' || component.get('v.type') === 'C2C' ? 'match' : 'search'
        let payload = {"engine": component.get("v.engine"), "type": type};
        if (type == "match") {
            payload.matchType = component.get("v.type");
        }
        engineAndQueryType.setParam('engineType', payload);
        engineAndQueryType.fire();        
    },
	onSearch: function (cmp, e, h) {
		const detail = e.getParams();
		console.log(detail.value);
        
        cmp.set("v.isPagination", false); // Reset pagination to go back to page 1 on new search

        // Reset match filters if search isn't smart
        if (!detail.isSmartSearch) {
            cmp.set("v.matchVectors", null);
            cmp.set('v.isSmartSearch', false);
        }
        
		cmp.set("v.keyword", detail.value);
		h.validateAndSearch(cmp, null, false, false);
	},

	displayAddTaskDetail : function(component, event, helper) {
		const params = {
			"recordId": event.getParam("recordId"),
			"activityType":event.getParam("activityType")
		};

		$A.createComponent(
			'c:C_TalentActivityAddEditModal',
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					component.set("v.C_TalentActivityAddEditModal", newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			}
		);
	},

	// Monika -- S-202119 -- Focus on reset button of Req search component if no req is selected in the recently viewed list
	focusonResetButton:function(component, event, helper) {
		const fieldId = event.getParam("fieldIdToGetFocus");
		if(fieldId == null) return;
		helper.setFocusToField(component, event, fieldId);
    }
})