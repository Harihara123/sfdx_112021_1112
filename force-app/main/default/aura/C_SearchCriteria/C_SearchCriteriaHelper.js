({
	
    createQueryStringHelper : function (component, isReexecute, noFacets) {
        var apiHelper = component.find("apiHelper");
        var criteriaHelper = this.findCriteriaHelper(component);

        return apiHelper.createQueryString({
            type: component.get("v.type"), 
            userId: component.get("v.runningUser.id"), 
            keyword: component.get("v.keyword"), 
            pageSize: component.get("v.pageSize"), 
            offset: component.get("v.offset"), 
            grpFilter: criteriaHelper.get("v.groupFilterOverride") ? criteriaHelper.get("v.groupFilterOverride") : component.get("v.runningUser.opcoSearchFilter"),
            matchSrcId: component.get("v.automatchSourceId"), 
            requestId: component.get("v.trkRequestId"), 
            transactionId: component.get("v.transactionId"), 
            appId: component.get("v.searchAppId"), 
            opco: component.get("v.runningUser.opco"), 
            location: component.get("v.multilocation"), 
            facetProps: component.get("v.facetProps"), 
            filterInitiator: null, 
            userPsId: component.get("v.runningUser.psId"), 
            filterCriteriaParams: component.get("v.filterCriteriaParams"), 
            selectedFacets: component.get("v.facets"), 
            noSearchYet: component.get("v.noSearchYet"), 
            isReexecute: isReexecute, 
            noFacets: noFacets,
            sortField: component.get("v.sortField"), 
            sortOrder: component.get("v.sortOrder"), 
            excludeKeyWords: component.get("v.excludeKeyWords"),
			userInGroups: component.get("v.userInGroups")
        });
    },
    
    createFacetQueryString : function (component, initiatingFacet) {
        var apiHelper = component.find("apiHelper");
        var criteriaHelper = this.findCriteriaHelper(component);

        return apiHelper.createQueryString({
            type: component.get("v.type"), 
            userId: component.get("v.runningUser.id"), 
            keyword: component.get("v.keyword"), 
            pageSize: 0, 
            offset: 0, 
            grpFilter: criteriaHelper.get("v.groupFilterOverride") ? criteriaHelper.get("v.groupFilterOverride") : component.get("v.runningUser.opcoSearchFilter"),
            matchSrcId: component.get("v.automatchSourceId"), 
            requestId: component.get("v.trkRequestId"), 
            transactionId: component.get("v.transactionId"), 
            appId: component.get("v.searchAppId"), 
            opco: component.get("v.runningUser.opco"), 
            location: component.get("v.multilocation"), 
            facetProps: component.get("v.facetProps"), 
            filterInitiator: initiatingFacet, 
            userPsId: component.get("v.runningUser.psId"), 
            filterCriteriaParams: component.get("v.filterCriteriaParams"), 
            selectedFacets: component.get("v.facets"), 
            noSearchYet: component.get("v.noSearchYet"), 
            isReexecute: false, 
            noFacets: false,
            sortField: component.get("v.sortField"), 
            sortOrder: component.get("v.sortOrder"), 
            excludeKeyWords: component.get("v.excludeKeyWords")
        });
    },
                
    createMatchRequestBody: function (component) {
        
        //console.log('createMatchRequestBody-->'+JSON.stringify(component.get("v.matchVectors")));
        
        var type = component.get("v.type");
        // Return null if not automatch
        if (type === "R" || type === "C") {
            return null;
        }

        var apiHelper = component.find("apiHelper");
        return apiHelper.createMatchRequestBody({
            matchVectors: component.get("v.matchVectors"), 
            excludeKeyWords: component.get("v.excludeKeyWords")
        });
    },
    
    getReqStage :function(component){
        var sourceID = component.get("v.automatchSourceId");
        var action = component.get("c.getReqStage");
        action.setParams({ SourceReqID : sourceID});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {                    
                     var reqstage = response.getReturnValue();
               
                     component.set("v.reqStage",reqstage);
            }
             
        });
        $A.enqueueAction(action);
    },
    
	parseURLParameters: function(component) {
        if(component.get("v.externalsource") !== 'TD'){
        component.set("v.accountId", undefined);
        }
        var urlV = new URL(window.location.href);
		component.set("v.urlParams", urlV.searchParams);
	},

    initializeSearch : function(component) {
        // Get current user
        this.getCurrentUser(component);
		//this.initTrkRequestId(component);
		this.initTrkTransactionId(component);
	},

    getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var r = JSON.parse(response.getReturnValue());
                component.set ("v.runningUser", r);
				var allowedCompanies = ['TEKsystems, Inc.','Aerotek, Inc','AG_EMEA'],
					externalSource = component.get("v.externalsource");
				if ((r.companyName && allowedCompanies.includes(r.companyName)) && externalSource === 'TS') {
					component.set("v.showMatchButton",true);
				}
                this.getSearchPrefs(component);
				this.postInitializeFn(component);
                
                // IP Story (S-231248)
                component.set('v.hasSmartSearchPermission', (r.userPermissionName.includes('Smart_Searcher')));
			}
        });
        $A.enqueueAction(action);
    },

	/*initTrkRequestId: function(component) {
		component.set("v.trkRequestId", this.guid());
	},*/

	initTrkTransactionId: function(component) {
		component.set("v.transactionId", this.guid());
        component.set('v.subTransactionId', this.guid());
        let transactionEvent = $A.get("e.c:ServerCallTracking");
        transactionEvent.setParam('transaction', component.get("v.transactionId"));
        transactionEvent.setParam('subTransactionId', component.get("v.subTransactionId"));
        transactionEvent.fire();
	},

	postInitializeFn: function(component) {
		if (component.get("v.reExecuteSearch") === true) {

		//Rajeesh S-90490 - refactoring - added the RQ part.
		if (component.get("v.externalsource") !== 'TD' && component.get("v.externalsource") !== 'RQ') {
					
			var cacheKey = component.get("v.urlParams").get("c__cacheKey");
			var execLast = component.get("v.urlParams").get("c__execLast");
			var execIndex = component.get("v.urlParams").get("c__execIndex");
			var matchTalentId = component.get("v.urlParams").get("c__matchTalentId");
			var matchJobId = component.get("v.urlParams").get("c__matchJobId");
			var atsJobId = component.get("v.urlParams").get("c__jobId");
		    var accId =  component.get("v.urlParams").get("c__accountId");
			var oppId =  component.get("v.urlParams").get("c__opportunityId");
			var sharedSearchKey = component.get("v.urlParams").get("c__searchKey");
            var tabType = component.get("v.urlParams").get("c__tabType");
            
            //Added by Srini for OFCCP render
            var ofccp = component.get("v.urlParams").get("c__ofccp");
            if(ofccp == "true" || ofccp == "1"){
                component.set("v.ofccpNotRequired",false);
            }

			if(accId !== null){// this param was added to fulfill C2C linking flow
			   component.set("v.accountId",accId);
			}
			
			if(oppId !== null){//this param was added to fulfill OppSubmittal skuid page flow
			   component.set("v.opportunityId",oppId);
			}

            if (tabType === 'recentlyApplied') {
                component.set("v.fromRecentApplicantsWidget", true);
                component.set("v.facets", {
                    "flt.recent_application_date": "[now-45d/d,now-0d/d]"
                });
            }
		
			if (execLast !== null) {
				component.set("v.type", component.get("v.urlParams").get("c__type"));
				this.executeLastSearchOrMatch(component);
			} else if (cacheKey !== null) {
				component.set("v.type", component.get("v.urlParams").get("c__type"));
				this.executeCachedSearch(component, cacheKey);
			} else if (execIndex !== null) {
				component.set("v.type", component.get("v.urlParams").get("c__type"));
				this.executeSearchAtIndex(component, execIndex, component.get("v.urlParams").get("c__savedType"));
			} else if (sharedSearchKey !== null) {
				component.set("v.type", component.get("v.urlParams").get("c__type"));
				this.executeSearchByKey(component, sharedSearchKey);
			} else if (matchTalentId !== null) {
				component.set("v.automatchSourceId", matchTalentId);
                 var flowType = component.get("v.target") === "talent" ? "C2C" : "C2R";
				component.set("v.type", flowType);
				
				component.set("v.automatchSourceData", {
					"srcLinkId" : component.get("v.urlParams").get("c__contactId"),
					"srcName" : component.get("v.urlParams").get("c__srcName")
				});
				
				// Fetch facets only if C2R (for the default / initial filters)
				var noFacets = component.get("v.type") === "C2C";
				this.validateAndSearch(component, null, noFacets, false);
                // Updated for S-85028
               
                var accountId = component.get("v.accountId");

                if(!($A.util.isUndefinedOrNull(flowType)) && flowType == 'C2R' && !accountId){
                    component.set("v.accountId",matchTalentId);
                }
                
			} else if (matchJobId !== null) {
				component.set("v.automatchSourceId", matchJobId);
				component.set("v.type", "R2C");
				component.set("v.automatchSourceData", {
					"srcLinkId" : matchJobId,
					"srcName" : component.get("v.urlParams").get("c__srcName"),
					"srcLoc" : component.get("v.urlParams").get("c__srcLoc")
				});
                // Updated for S-85722
                var flowType = component.get("v.type");
                var opportunityId = component.get("v.opportunityId");
                if(!opportunityId){
                    component.set("v.opportunityId",matchJobId);
                }
                let lat = component.get("v.urlParams").get("c__srcLat");
                let lng = component.get("v.urlParams").get("c__srcLong");
                if (component.get('v.sourceType') === 'W2C' && lat && lng) {
                    component.set("v.multilocation", [{
                        "displayText": component.get("v.urlParams").get("c__srcLoc"),
                        "geoRadius": component.get('v.radius'),
                        "geoUnitOfMeasure": component.get('v.radiusUnit'),
                        "latlon": [parseFloat(lat), parseFloat(lng)],
                        "filter": false,
                        "useRadius": "SELECT"
                    }]);
                }
                
				this.validateAndSearch(component, null, true, false);
			} else if (atsJobId !== null) {
				component.set("v.type", "C");
                component.set("v.jobId", atsJobId);
			} else {
				component.set("v.type", component.get("v.target") === "talent" ? "C" : "R");
			}
		
		} else {
			component.set("v.type", component.get("v.target") === "talent" ? "C" : "R");
		}
		} else {
			component.set("v.type","C"); // it means redirected from Google Search.
		}

	},

	validateAndSearch : function(component, initiatingFacet, noFacets, isReexecute) {
        let reqEvent = $A.get("e.c:TrackingEvent");
		reqEvent.setParam('requestSent', component.get("v.type"));    
		//S-189728 -- Set engine type attribute to LTR for R2C and user in group B
		let sortField = component.get('v.sortfield');
		let sortOrder = component.get('v.sortOrder');
		if(component.get('v.type') === 'R2C' && component.get('v.userInGroups').isInGroupB && 
			($A.util.isEmpty(sortField) || $A.util.isUndefined(sortField)) && ($A.util.isEmpty(sortOrder) || $A.util.isUndefined(sortOrder)) ) {
			let engineAttr = "LTR";
			
			let payLoadType = 'match';
			let payload = {"engine": engineAttr, "type": payLoadType};
			if (payLoadType == "match") {
				payload.matchType = component.get("v.type");
			}
       		
			reqEvent.setParam('engineType', payload);
			 
		} else { //set default engine type
			let type = component.get('v.type') === 'C2R' || component.get('v.type') === 'R2C' || component.get('v.type') === 'C2C' ? 'match' : 'search'
			let payload = {"engine": component.get("v.engine"), "type": type};
			if (type == "match") {
				payload.matchType = component.get("v.type");
			}
			reqEvent.setParam('engineType', payload);
			
		}
		//Till here
        reqEvent.fire();  
		if (this.validateSearch(component)) { 
		// to handle browser back and toggle so if cache key found then do not execute search for group B.
            var type = component.get("v.type");
        		if(type =='R2C'){
            		this.getReqStage(component);
    			}
			var cacheKey;
			if (!isReexecute && !(component.get("v.externalsource") == 'TD' || component.get("v.externalsource") == 'RQ') ) {
				cacheKey = this.randomStringGen();
				this.updateBrowserHistory(component, cacheKey);
            }
           
             //This reset needs to happen if there is a chnage in the search term, sort order, distance, etc.
                    //Basically, anything that isn't a page change.
            var isPaging = component.get("v.isPagination");
            if(!isPaging && !isReexecute) {
                component.set("v.offset", 0);
            }
           
            var queryStringUrl = this.createQueryString(component, isReexecute, noFacets);
            
            /*if (component.get("v.urlParams").get("c__tabType") == 'recentlyApplied') {
                queryStringUrl += '&flt.recent_application_date=%5Bnow-45d%2Fd%2Cnow-0d%2Fd%5D';
            }*/

            var action = this.getActionWithParametersSmartSearch(component, queryStringUrl);
            action.setCallback(this, function(response) {
                component.set("v.searchInProgress", false);

				// Make sure response parsed only once.
                var searchResponse = JSON.parse(response.getReturnValue());
				component.set("v.smsInfoLoaded", false);
               	this.handleResponse(response.getState(), component, searchResponse, initiatingFacet, isReexecute, component.get('v.isSmartSearch'));

                // Reset offset to 0 so any subsequent calls will load from 0 unless it's a scrolling call.
                component.set("v.showUpdateButton", false);
                this.saveSearchLog(component, this.createSearchLogEntry(component), cacheKey);

                if(isPaging) {
					component.set("v.isPagination", false);
				}
				//Disable Action Buttons
				/*var disableActionButtons = $A.get("e.c:E_DisableSearchActionButtons");
				disableActionButtons.setParams({
					rowsSelected:[],
					rowsSelectedCount:0,
					contacts:[]
				});	
				disableActionButtons.fire();*/

            });
            
            component.set("v.searchInProgress", true);
			component.set("v.isMatchTalentInsightCall", false);  // to initiate base search call.
            this.fireLinkModalEvent(component,"search");
            $A.enqueueAction(action);

        } 
		
        if(type === 'C' || type === 'R') {
        	// Reset/Close Results Sidebar if open
			let searchResults = component.find("searchResults");
            if (searchResults.length && searchResults.length > 0) {
                searchResults = searchResults[0];
            }
			searchResults.set("v.showSidebar", false);
			searchResults.set("v.selectTalentRow", '');
			// Resets search field
        	// setTimeout(()=> {
        	// 	if(component.find("dynamicTextArea") !== undefined){
			// 		component.find("dynamicTextArea").onBlur();
			// 	}
        	// }, 100);

        }
		//fire event to manage Campaign and call sheet button
		/*this.displayCampaignOnTalentSearch(component,type);

		this.disableSearchActionCheckboxes(component);*/
	},

	updateBrowserHistory: function(component, cacheKey) {
		var noPush = component.get("v.urlParams").get("c__noPush");
		var title = component.get("v.target") === "talent" ? "Talent Search" : "Req Search";
		var url = '/lightning'
					+ (component.get("v.target") === "talent" ? "/n/ATS_CandidateSearchOneColumn?c__cacheKey=" : "/n/Search_Reqs?c__cacheKey=")
					+ cacheKey + "&c__type=" + component.get("v.type");
		if (component.get("v.switchFromGoogle") === true) {
			url += 	"&c__gs=true";		
		}

		if (cacheKey !== null && noPush !== "true") {
			window.history.pushState(null, title, url);
		} else if (noPush === "true") {
			window.history.replaceState(null, title, url);
			component.get("v.urlParams").set("c__noPush","false");	
		}
	},

    resetCacheBrowserHistory: function(component) {
        var title = component.get("v.target") === "talent" ? "Talent Search" : "Req Search";
        var url = '/lightning' + (component.get("v.target") === "talent" ? "/n/ATS_CandidateSearchOneColumn" : "/n/Search_Reqs");
        window.history.replaceState(null, title, url);
        component.get("v.urlParams").set("c__noPush","false");
    },

	fireRefreshUtilityEvt: function(component) {
        var utilEvent = $A.get("e.c:E_RefreshUtilityEvent");
        utilEvent.fire();
	},
    
    getSearchPrefs : function(component) {
        var action = component.get("c.getUserPreferences");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
				var target = component.get("v.target");
				var prefModal = response.getReturnValue();
				if (prefModal) {
					// Set Talent / Req / Default Configurations.
					if (target == 'talent' && prefModal.talentConfig) {
						component.set("v.facetsConfig",prefModal.talentConfig);
					} else if (target == 'req' && prefModal.reqConfig) {
						component.set("v.facetsConfig",prefModal.reqConfig);
					} 
					if (prefModal.defaultPrefs) {
						component.set("v.defaultFacets",prefModal.defaultPrefs);
					}
					// Set User Preferences
					if (prefModal.userPrefs) {
						var p = JSON.parse(prefModal.userPrefs);
						if (!p.search) {
							p = this.translateToNewPrefs(p);
						}
						if (!p.facets) {
							p.facets = this.defaultFacetPrefs(component);
						}
						component.set("v.userPrefs", p);
					} else {
						var p = this.defaultNewPrefs();
						p.facets = this.defaultFacetPrefs(component);
						component.set("v.userPrefs", p);
					}
				}
			}

        });
        $A.enqueueAction(action);
    },

	translateToNewPrefs: function(oldPrefs) {
		var newPrefs = this.defaultNewPrefs();
		newPrefs.search.talent.ViewType = oldPrefs.ViewType;
		newPrefs.search.talent.ResumeExpanded = oldPrefs.ResumeExpanded;
		newPrefs.search.req.ResumeExpanded = oldPrefs.ResumeExpanded;
		return newPrefs;
	},

	defaultNewPrefs: function() {
		return {
            "search":{
				"talent": {
            		"ViewType": "Grid",
            		"ResumeExpanded": true
       				},
       			"req": {
           			"ResumeExpanded": true
      			}
            }
		};
	},

	defaultFacetPrefs : function(component) {
		return {
			"talent": "defaults",
			"req": "defaults"
		};
	},

    updateFacetProps : function(component, event) {
        // console.log("Registered --- " + JSON.stringify(event.getParams())); 
        var facetProps = component.get("v.facetProps");
        if (facetProps === undefined || facetProps === null) {
            facetProps = {};
        }
        facetProps[event.getParam("facetParamKey")] = {
            "allowNoKeywordSearch" : event.getParam("allowNoKeywordSearch"), 
            "isVisible" : event.getParam("isVisible"), 
            "isCollapsed" : event.getParam("isCollapsed"), 
            "optionKey" : event.getParam("optionKey"),
            "sizeKey" : event.getParam("sizeKey"),
            "fetchSize" : event.getParam("fetchSize"),
            "initialFilter" : event.getParam("initialFilter"),
            "isFilterable" : event.getParam("isFilterable"),
            "isNested" : event.getParam("isNested"),
            "isUserFiltered" : event.getParam("isUserFiltered"),
			"optionFilter" : event.getParam("optionFilter"),
			"nestedFacetKey" : event.getParam("nestedFacetKey"),
			"nestedKeyinNestedFacet" : event.getParam("nestedKeyinNestedFacet")
        }
        
        component.set("v.facetProps", facetProps);
    },

    updateDefaultFilters : function(component, event) {
        var facets = component.get("v.facets");
        if (facets === undefined || facets === null) {
            facets = {};
        }

        var f = event.getParam("initialFilter");
        var k = Object.keys(f);
        facets[k[0]] = f[k[0]];
        component.set("v.facets", facets);
    },

    fireLinkModalEvent: function(component,src){
	    
        if(component.get("v.externalsource") === 'TD'){
            var reqSearchModalEvt = component.getEvent("ReqSearchModalResultsEvent");
				reqSearchModalEvt.setParams({"sourcebutton":src});
			if(src === "search"){
				reqSearchModalEvt.setParams({"showReqSearchResults": true, "disableLinkButton": true});
            
			}
            reqSearchModalEvt.fire();
        }  //Neel - Can be used only one event
		else if(component.get("v.externalsource") === 'RQ'){
			var talentSearchModalEvt = component.getEvent("DisplaySearchResultInModalEvent");
			if(src === "search"){
				component.set("v.displayResults", true);
				talentSearchModalEvt.setParams({"sourcebutton":"search","showTalentSearchResults": true, "disableLinkButton": true});
			}
			else {
				talentSearchModalEvt.setParams({"sourcebutton":"refresh","showTalentSearchResults": component.get("v.displayResults"), "disableLinkButton": true});
			}
            talentSearchModalEvt.fire();
        }
		else if (src === "refresh"){
			var type = component.get("v.type");

			if (type === "C") {
				if (component.get("v.switchFromGoogle") === true) {
					window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?_3321=" + Math.random()+"&c__gs=true";
				} else {
					window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?_3321=" + Math.random();
				}

			} else if (type === "R") {
				window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Search_Reqs?_3321=" + Math.random();
			} else if (type === "C2C") {
				var automatchSourceData = component.get("v.automatchSourceData");
				if(automatchSourceData){
					var contactId = automatchSourceData.srcLinkId;
					var srcName = automatchSourceData.srcName;
				}
				window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?matchTalentId=" + component.get("v.automatchSourceId") + "&contactId=" + contactId + "&srcName=" + srcName + "&_3321=" + Math.random();
			} else if (type === "R2C") {
				var automatchSourceData = component.get("v.automatchSourceData");
				if(automatchSourceData){
					var srcName = automatchSourceData.srcName;
					var srcLoc = automatchSourceData.srcLoc;
				}
				window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?matchJobId=" + component.get("v.automatchSourceId") + "&srcName=" + srcName + "&srcLoc=" + srcLoc + "&_3321=" + Math.random();
			} else if (type === "C2R") {
				var automatchSourceData = component.get("v.automatchSourceData");
				if(automatchSourceData){
					var contactId = automatchSourceData.srcLinkId;
					var srcName = automatchSourceData.srcName;
				}
				window.location.href = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Search_Reqs?matchTalentId=" + component.get("v.automatchSourceId") + "&contactId=" + contactId + "&srcName=" + srcName + "&_3321=" + Math.random();
			}
		}
    },

    getActionWithParametersSmartSearch: function(component, queryStringUrl) {
        var action;
        if (component.get('v.isSmartSearch')) {
            const lwc = component.find('lwcSearchInput');
            
            if (lwc && component.get('v.hasSmartSearchPermission') && component.get('v.type') == 'C') {
                let smartTerms = lwc.getSelectedTerms();

                var hlp = this.findCriteriaHelper(component);
                action = hlp.getActionWithParameters(component, queryStringUrl, smartTerms);
            }
            else
                action = this.getActionWithParameters(component, queryStringUrl);
        }
        else
            action = this.getActionWithParameters(component, queryStringUrl);

        return action;
    },

	getActionWithParameters: function(component, queryStringUrl) {
		var hlp = this.findCriteriaHelper(component);
		if (component.get("v.isMatchTalentInsightCall") === true) {
			return hlp.getMatchInsightActionWithParameters(component, queryStringUrl);
		}
		else {
			return hlp.getActionWithParameters(component, queryStringUrl);
		}
	},

	findCriteriaHelper: function(component) {
		var hlp = component.find("criteriaHelper");
		if (Array.isArray(hlp)) {
			return hlp[hlp.length - 1];
		} else {
			return hlp;
		}
	},

    randomStringGen: function(component) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },

	validateSearch: function(component) {
		var validSearch = true;

		switch(component.get("v.type")) {
			// Keyword validation for req and candidate search
			case "C" : 
			case "R" :
				this.clearKeywordErrors(component);

				// Validate for keyword if required

				// let keywordField = component.find("dynamicTextArea").find("inputSearchId");

				let keyword = component.get("v.keyword");

				if (!keyword || keyword === "") {

					// No keyword - check if keywordless search allowed for the applied facets.
					validSearch = this.checkAllowNoKeyword(component);
					if (!validSearch) {
						// keywordField.set("v.errors", [{message: "You must enter at least one keyword to perform a search"}]);
						// keywordField.set("v.isError",true);
						// $A.util.addClass(keywordField,"slds-has-error show-error-message");
						component.set("v.hasError", true);
						component.set("v.errors", [{message: $A.get("$Label.c.ATS_SEARCH_VALIDATION")}]);

					}
				}
				break;

			// No validations for any of the matches
			case "C2C" : 
			case "R2C" :
			case "C2R" : 
			default : 
				break;
		}
		return validSearch;
	},

	clearKeywordErrors : function(component) {
		// let inputSearchId = component.get("v.isKeywordTextArea") ? "inputSearchId2" : "inputSearchId",

        // var keywordField = component.find("dynamicTextArea").find("inputSearchId");
		//
		// if (Array.isArray(keywordField) && keywordField.length > 0) {
		// 	keywordField = keywordField[0];
		// }
		//
        // $A.util.removeClass(keywordField,"slds-has-error show-error-message");
        // keywordField.set("v.isError",false);
        // keywordField.set("v.errors", null);
		component.set("v.hasError", false);
		component.set("v.errors", [{message: ''}]);

    },

    checkAllowNoKeyword : function(component) {
        var allow = false;
        var facetProps = component.get("v.facetProps");
        var facets = component.get("v.facets");
        // Keyword is required if no facets selected.
        if (facets !== null) {
            var appliedFacets = Object.keys(facets);
            // Check the facetProps if any of the applied facets have allowNoKeywordSearch set to true.
            for (var i=0; i<appliedFacets.length; i++) {
                var facet = facets[appliedFacets[i]];
                var props = facetProps[appliedFacets[i]];
                if (facet !== "" && props !== undefined && props.allowNoKeywordSearch === true) {
                    allow = true;
                    break;
                }
            }
        }

        return allow;
    },

	getBaseParameters: function(component) {
		var hlp = this.findCriteriaHelper(component);
		if (component.get("v.isMatchTalentInsightCall") === true) {
			return hlp.getMatchInsightParameters(component);
		}
		else {
			return hlp.getBaseParameters(component);
		}
	},

    createQueryString : function (component, isReexecute, noFacets) {
		var parameters = this.getBaseParameters(component);
        
        parameters = this.addTrackParameters(parameters, component);
        parameters = this.addLocationParameters(parameters, component);
        if (noFacets === false) {
			parameters = this.addFilterCriteriaParams(parameters, component); //S-32364 Flipped the order with addFacetParameters
			parameters = this.addFacetOptions(parameters, component, isReexecute, null); 
        }
        parameters = this.addFacetParameters(parameters, component);
        parameters = this.addSortParameters(parameters, component);

        // Adding exclude keywords
        parameters = this.addExcludeKeywords(parameters, component);

        // component.set("v.requestId", requestId);
        component.set("v.trkRequestId", parameters.requestId);
        component.set("v.transactionId", parameters.transactionId);
        component.set("v.searchAppId", parameters.appId);
        if(isReexecute) {
            component.set("v.pageSize", parameters.size);
        }
		var querystr = this.buildUrl(parameters);
        component.set("v.queryString",querystr );
		return(this.buildUrl(parameters));
	},

    s4: function () {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    },

    guid: function() {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
            this.s4() + '-' + this.s4() + this.s4() + this.s4();
    },

    addTrackParameters : function (parameters, component) {
        //parameters.transactionId = this.guid();
		parameters.requestId = this.guid();
		//component.set("v.transactionId", parameters.transactionId);//Added by akshay S - 71890 5/15/18
		component.set("v.trkRequestId", parameters.requestId);
        //parameters.requestId = component.get("v.trkRequestId");
		parameters.transactionId = component.get("v.transactionId");
        parameters.appId = component.get("v.searchAppId");
		parameters.opco = component.get("v.runningUser.opco");
        let transactionEvent = $A.get("e.c:E_SearchRequestTracking");
        transactionEvent.setParam('request', parameters.requestId);
        transactionEvent.fire();
        return parameters;
    },

    addLocationParameters : function (parameters, component) {
        if (component.get("v.multilocation")) {
            // Empty array used in location when doing an All location search.
            var loc = this.translateToSearchApiLocation(component.get("v.multilocation"));
            // if (loc.length > 0) {
                parameters.location = JSON.stringify(loc);
            // }
        } 

        return parameters;
    },

    translateToSearchApiLocation : function(loc) {
        var val = [];

        for (var i=0; i<loc.length; i++) {
            /*  Output value of "filter"
             *  useRadius   --> |  NONE  |  SELECT  |  AUTO 
             *  alwaysUseRadius |
             *  -------------------------------------------
             *    TRUE          |   N/A  |   FALSE  | FALSE
             *    FALSE         |  TRUE  |   FALSE  | FALSE
             */
            var filter = loc[i].useRadius === "NONE";
            var radius = typeof loc[i].geoRadius === "number" ? loc[i].geoRadius.toFixed() : loc[i].geoRadius;

            if (filter) {
                val.push({
                    "filter" : filter,
                    "city" : loc[i].city,
                    "state" : loc[i].state,
                    "country" : loc[i].country,
                    "postalcode" : loc[i].postalcode
                });
            } else {
                val.push({
                    "latlon" : loc[i].latlon,
                    "geoRadius" : radius,
                    "geoUnitOfMeasure" : loc[i].geoUnitOfMeasure,
                    "filter" : filter
                });
            }
        }

        return val;
    },

    addSortParameters : function (parameters, component) {
        /*let runningUserForSort = component.get("v.runningUser"); 
        if(!(component.get("v.sortField")) && runningUserForSort.companyName ==='AG_EMEA'  && component.get('v.type') === 'R2C'){
            component.set("v.sortField","relevancyV2");
            component.set("v.sortOrder","desc");
        }*/
        if (component.get("v.sortField")) { 
            parameters.sort = component.get("v.sortField");
			if (component.get("v.sortOrder")) {
				parameters.sort+= ":" + component.get("v.sortOrder");
			}
        } 
        return parameters;
    },
    addExcludeKeywords : function (parameters, component) {
        var excludeKeyWords = component.get("v.excludeKeyWords");
        var allExcludes = [];
        if(excludeKeyWords!=undefined && excludeKeyWords.length > 0){
            allExcludes.push(excludeKeyWords);
        }
        if(allExcludes.length>0){
            parameters.exclude = allExcludes;
        }
        return parameters;
    },
    addFilterCriteriaParams : function(parameters, component, filterInitiator) {
        var fps = component.get("v.facetProps"); 
        var canListKey = "nested_facet_filter.myCandidateListsFacet,nested_filter.allCandidateListsFacet";

		if(fps[canListKey] !== undefined) {
			// Check for not a facet request OR specifically candidate list facet request.
			if ((filterInitiator === undefined || filterInitiator === canListKey) 
				&& fps && fps[canListKey] && !fps[canListKey].isCollapsed) {
				parameters["nested_local_facet_filter.myCandidateListsFacet"] = JSON.stringify([{"User": [component.get("v.runningUser.psId")]}]);
			}

		}

        var filtParams = component.get("v.filterCriteriaParams");

        if (filterInitiator) {
            // If filter params available for this specific facet request, add the params.
            if (filtParams && filtParams[filterInitiator]) {
                var currFacetFilter = filtParams[filterInitiator];
                var keys = Object.keys(currFacetFilter);

                for (var j=0; j<keys.length; j++) {
                    parameters[keys[j]] = currFacetFilter[keys[j]];
                }
            } else {
                // If filter params not available and is filterable, add the default filters.
                var fp = fps[filterInitiator];
    
                if (fp.isFilterable) {
                    parameters[fp.optionKey + ".contains"] = "";
                    parameters[fp.optionKey + ".size"] = fp.fetchSize;
                }
            }
        } else 
            // Not a specific facet request and prior filter params are available.
            if (filtParams) {
                // Include all available filter params for all facets ONLY if facet is open.
                var facets = Object.keys(filtParams);

                for (var i=0; i<facets.length; i++) {
                    var facet = filtParams[facets[i]];
                    if (fps[facets[i]] && !fps[facets[i]].isCollapsed) {
                        var facetKeys = Object.keys(facet);
                        for (var j=0; j<facetKeys.length; j++) {
                            parameters[facetKeys[j]] = facet[facetKeys[j]];
                        }
                    }
                }
        }

        return parameters;
    },
    
    addFacetOptions : function (parameters, component, isReexecute, singleFacet) { 
        var facetProps = component.get("v.facetProps");
		var facets = component.get("v.facets");
        var noSearchYet = component.get("v.noSearchYet");

		if (singleFacet && singleFacet.length > 0) {
			return this.addSingleFacetOptions(component, parameters, noSearchYet, isReexecute, facetProps[singleFacet], null);
		}

        if (facetProps && facetProps !== null) { 
            var keys = Object.keys(facetProps); 
            for (var i=0; i<keys.length; i++) { 
				parameters = this.addSingleFacetOptions(component, parameters, noSearchYet, isReexecute, facetProps[keys[i]], facets ? facets[keys[i]] : null);
            } 
        } 
        return parameters; 
    }, 

	addSingleFacetOptions : function (component, parameters, noSearchYet, isReexecute, facetProp, facetSelection) {
        var oKey = facetProp.optionKey; 
		var iniFilt = facetProp.initialFilter; 

        if (oKey !== undefined) {
            if (iniFilt && noSearchYet && !isReexecute) {
                // If there is an initial filter on this parameter.
                var filtKeys = Object.keys(iniFilt);
                for (var k=0; k<filtKeys.length; k++) {
                    parameters[filtKeys[k]] = iniFilt[filtKeys[k]];
                }
                parameters[oKey] = true; 
            } else if (!facetProp.isCollapsed || (isReexecute && facetSelection !== null && facetSelection !== undefined)) {
                // Turn on the facet option if facet is not collapsed. OR if this is a reexecute and the facet has a selection
				parameters[oKey] = true;
			}

            // If fetching the facets, also add the size parameter if available
            if (parameters[oKey] === true && facetProp.sizeKey !== undefined) {
                parameters[facetProp.sizeKey] = facetProp.fetchSize;
            }

			// If optionFilter exists, setup the include filters
            if (parameters[oKey] === true && facetProp.optionFilter !== undefined) {
                parameters[oKey + ".include"] = facetProp.optionFilter.replace("#psID#", component.get("v.runningUser.psId"));
            }
        }

        return parameters; 
    }, 

    addFacetParameters : function (parameters, component) {
        var selectedFacets = component.get("v.facets");
        if (selectedFacets && selectedFacets !== null) {
            var keys = Object.keys(selectedFacets);
            for (var i=0; i<keys.length; i++) {

                if (!this.isEmptyFacet(selectedFacets[keys[i]])) {
                    if (typeof selectedFacets[keys[i]] === "string") {
                        parameters[keys[i]] = selectedFacets[keys[i]];
                    } else {
                        parameters[keys[i]] = JSON.stringify(selectedFacets[keys[i]]);
                    }
                }
            }
            // console.log(parameters);
        }
        return parameters;
    },

    isEmptyFacet : function (facet) {
        var emptyFacet = false;
        if (facet !== undefined && facet !== null) {
            // Not undefined or null
            if (facet === "") {
                // String or any other basic type empty
                emptyFacet = true;
            } else if (typeof facet === "object" && (facet.length === undefined || facet.length === 0)) {
                // Object / array
                emptyFacet = true;
            }
        } else {
            emptyFacet = true;
        }

        return emptyFacet;
    },

    buildUrl : function (parameters) {
         var qs = "";
         for(var key in parameters) {
            var value = parameters[key];
            /* S-32457 adding condition to handle multiple facet keys.
               Keys sent as comma separated values for facet parame keys

               NOTE: if comma becomes a valid character this solution will not work. 

               */
            if(key.indexOf(",") > -1){
                var paramValues; 
                var paramKeys = key.split(",");

                if(value !== undefined && value !== null){
                    paramValues = value.split("~");
                    for(var i=0; i<paramValues.length; i++) {
						if (paramValues[i] !== "") {
							qs += encodeURIComponent(paramKeys[i]) + "=" + encodeURIComponent(paramValues[i]) + "&"; 
						}
                    }
                }

            }else{
              qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
            }

         }
         if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
         }
         return qs;
    },

    handleResponse : function (state, component, response, initiatingFacet, isReexecute, isSmartSearch) {
		var errList = [];
		var results = {};

        if (state === "SUCCESS") {
            if (response.fault) {
				errList.push(response.fault.faultstring);
                results.hasError = true;
                results.errors = errList;
                results.initiatingFacet = initiatingFacet;
                component.set("v.badSearchCriteria", true);

            } else if (response.header.errors && response.header.errors.length > 0) {
                var errors = response.header.errors;
                for (var i=0; i<errors.length; i++) {
                    if (errors[i] && errors[i].message) {
                        errList.push(errors[i].message);
                    }
                }
                results.hasError = true;
                results.errors = errList;
                results.initiatingFacet = initiatingFacet;
				component.set("v.badSearchCriteria", true);
                  
            } else {
                var warningArray = this.getWarnings(response.header.request.params);
                this.toastWarning(warningArray, false);

				results.records = this.translateRecords(response,component);
				results.totalResultCount = response.response.hits.total;
				results.offset = this.getResultOffset(response);
                //Jordan - setting pagesize for paginator
                results.pageSize = response.header.request.params.size;
				results.hasError = false;
                results.isReexecute = isReexecute;
				this.updateMatchLocationFromResponse(component, response.response);
				// this.saveMatchVectors(component, response.response.automatch_source_talent_info);
                this.saveMatchVectors(component, response.response.match_info);
				
                // For Defect D-07966 last parameter is boolean flag to determine if it is for facetsSearch
            	// if set to true then set displayFilters to false in facets Container
				console.log(response);
				console.log(response.response);
				console.log(response.response.aggregations);

                // CAll from recent applicants widget - Force using facets instead of null
                let facets = null;
                if (component.get("v.fromRecentApplicantsWidget")) {
                    facets = component.get("v.facets");
                    // Reset flag to false to prevent this from happening again
                    component.set("v.fromRecentApplicantsWidget", false);
                }
                this.updateFacetsContainer(component, response.response.aggregations, isReexecute, facets, false, isSmartSearch);
				
				results.initiatingFacet = initiatingFacet;
				// results.facetDisplayProps = component.get("v.facetDisplayProps");
				results.requestId = component.get("v.trkRequestId");
				results.transactionId = component.get("v.transactionId");
                results.appId = component.get("v.searchAppId");
				if (response.header.request.params.sort) {
					var s = response.header.request.params.sort.split(":");
					results.sortField = s[0];
					results.sortOrder = s[1];
				}

                // this.toggleSaveSearch(component);
                // Setting the flag to false to indicate that a search has happened. 
                // Any default facets / filters will not be explicity applied going forward.
                component.set("v.badSearchCriteria", false);
                component.set("v.noSearchYet", false);
				component.set("v.explainHits", this.translateHitsMap(response.response.hits_explanation));
				
				//S-136887 - To update SMS Indicator in search result
				this.getConsentInfo(component, results);
               
               
				
            }
        } else {
            var errors = response.getError();
            if (errors) {
                for (var i=0; i<errors.length; i++) {
                    if (errors[i] && errors[i].message) {
                        errList.push(errors[i].message);
                    }
                }
            }
            results.hasError = true;
            results.errors = errList;
            results.initiatingFacet = initiatingFacet;
        }
        // searchCompletedEvt.fire();
        component.set("v.errors", errList);
        component.set("v.results", results);
		this.toastWarning(errList, true);

        // Changes icon to allow saving search again
        // component.set("v.searchSaved", false);
    },

    getWarnings: function(params) {
        var warnings = [];
        if (params.performGeoAsFilter === "false" && (params.geo === undefined || params.geo === null || params.geo === "")) {
            warnings.push("The City does not match the State/Province selected, so location was not used in the search. Please update the city information and search again.");
        }
        // For future releases: figure a way to print multiple warnings into a toast

        return warnings;
    },

    toastWarning : function (warningArray, isError){
        for (var i = 0; i < warningArray.length; i++){

          var warningMessage = warningArray[i];
          var toastEvent = $A.get("e.force:showToast");
		  toastEvent.setParams({
			"title": isError ? "Error!" : "Warning!",
			"message": warningMessage,
			"type": isError ? "error" : "warning",
            "duration": 5000
		  });
          
		  toastEvent.fire();
        }
    },

    getResultOffset : function (responseObj) {
        var offset = 0;
        var params = responseObj.header.request.params;
        if (params && params.from) {
            offset = params.from;
        }
        return offset;
    },

	clearCriteria: function(component) {
		component.set("v.keyword", "");
		component.set("v.multilocation", "");
	},

	clearResults: function(component) {
		component.set("v.results",  {
			"hasError": false,
			"records": [],
			"erorrs": [],
			"totalResultCount": 0,
			"offset": 0
		});
	},

	clearFacets: function(component) {
		component.set("v.facets", {});
		component.set("v.facetDisplayProps", {});
		component.set("v.filterCriteriaParams", {});
		component.set("v.matchVectors", null);
		this.clearFacetsContainer(component);
	},
    
    translateRecords : function (searchResponse,component) {
        // var st = new Date().getTime(); 
        var recordset = searchResponse.response.hits.hits;
        let dimensionSet = searchResponse.response.insight || null;
        var translatedData = [];

        // For req search, split query term into regexp tokens.
        var qtokens = [];
        if (searchResponse.header.request.params.index === "job" && searchResponse.header.request.params.type !== "talent2job" ) {
            // replace boolean operators and parentheses before split
            var q = searchResponse.header.request.params.q.replace( new RegExp("(\\sand\\s|\\sor\\s)|[^a-z0-9+#]", "gi") , " ").split(" ");
            var k = 0;
            for (var j=0; j<q.length; j++) {
                var st = q[j].trim();
                if (st !== "") {
                    var strep = st.replace(/(\*|\?)/gi, "\\S$1"),
                        newStrep = strep.replace(/[^\w\s]/gi, "");
                    // Check for ++ due to c++ query issue
                    if(st.includes("++")){
                        
                        // Escapes ++ characters for regex
                        strep = "(" + newStrep + "\\+\\+\\s*|" + newStrep + "\\+\\+\\$)";

                    }else{
                        // strep = strep.replace(/[^\w\s]/gi, "");
                        strep = "(" + strep + "\\s*|" + strep + "$)";
                    }
                   
                    qtokens[k++] = new RegExp(strep, "gi");
                }
            }
        }

        for (var i = 0; i < recordset.length; i++) {
            var tx;
            switch (recordset[i]._type) {
                case "person" : 
                    // tx = this.translatePerson(recordset[i], searchResponse.header.request.params, searchResponse.response.highlights, 
					// 							searchResponse.response.insight && searchResponse.response.insight.length > 0 ? searchResponse.response.insight[i] : null,
                    // 							dimensionSet, component);
                    tx = this.translatePerson(recordset[i], searchResponse.response.insight && searchResponse.response.insight.length > 0 ? searchResponse.response.insight[i] : null,
                        dimensionSet, component);
                    break;

                case "job" :
                    tx = this.translateJob(recordset[i], qtokens);
                    break;

                default :
                    tx = null;
                    break;
            }
            translatedData.push(tx);
        }

        // console.log("---TRANSLATE RESCORDS -- " + (new Date().getTime() - st) + " ms"); 
        return translatedData;
    },

    /**
     * [{"id" : "18-chars", "record" : {}, "score" : 20.221, "type" : "talent" }]
     */
    translatePerson : function (personRecord, insight, dimensionSet, component) {
        var ourPerson = {};
        ourPerson.record = personRecord._source;
        ourPerson.record = this.setPreferredComms(ourPerson.record);
        ourPerson.record = this.setDisplayAddress(ourPerson.record);
        // ourPerson.record = this.setResumeTeaser(searchParams, personRecord, ourPerson.record, automatchHighlights);
        // ourPerson.record = this.setSkillsTeaser(searchParams, personRecord, ourPerson.record);
		ourPerson.record.isPipeline = this.checkPipeline(ourPerson.record, component);
		ourPerson.record = this.translateJobTitle(ourPerson.record);
        ourPerson.id = personRecord._id;
		ourPerson.record = this.translateDimension(ourPerson.record, dimensionSet);
        ourPerson.score = Math.round(personRecord._score * 100);
        ourPerson.type = "talent";
		if(ourPerson.record.candidate_status)
			ourPerson.record.candidate_status = ourPerson.record.candidate_status.toUpperCase();
	  if(ourPerson.record.source_system_id != null && ourPerson.record.source_system_id.includes('R.')){
					ourPerson.record.source_system_id = ourPerson.record.source_system_id.slice(2);
			}
			else{
			ourPerson.record.source_system_id = '';
			}
		if (insight !== null) {
			ourPerson.insight = insight;
		}

        return ourPerson;
    },

    translateJob : function(jobRecord, qtokens) {
        var ourJob = {};
        ourJob.record = jobRecord._source;
        ourJob.record = this.setReqDuration(ourJob.record);
        ourJob.record = this.setReqSkillList(ourJob.record, qtokens);
        ourJob.record = this.setReqBillRate(ourJob.record);
        ourJob.record = this.translateMiscAttr(ourJob.record);
        ourJob.id = jobRecord._id;
        ourJob.score = Math.round(jobRecord._score * 100);
        ourJob.type = "req";
        
        return ourJob;
    },

	translateJobTitle: function(ourPerson) {
		const entities= {
			"&amp;": "&",
			"&lt;": "<",
			"&gt;": ">",
			"&quot;" : "\"",
			"&apos;" : "'"
		};

		let formattedTitle = '',
			jobTitle = ourPerson.employment_position_title,
			positionTitle = ourPerson.current_employer_name;

		for(var prop in entities) {
			if (entities.hasOwnProperty(prop)) {
				if(ourPerson.employment_position_title !== null) {
					jobTitle = jobTitle.replace(new RegExp(prop, "g"), entities[prop]);
				}
				if(ourPerson.current_employer_name !== null) {
					positionTitle = positionTitle.replace(new RegExp(prop, "g"), entities[prop]);
				}
			}
		};

		if (jobTitle !== null && positionTitle !== null){
			formattedTitle = `${jobTitle} @ ${positionTitle}`;
			ourPerson.formattedTitle = formattedTitle;
		} else if (jobTitle !== null){
			ourPerson.formattedTitle = jobTitle;
		} else if (positionTitle !== null){
			ourPerson.formattedTitle = positionTitle;
		}

    	return ourPerson;
	},

	translateDimension: function(ourPerson, dimensionSet) {
    	if(dimensionSet !== null){
    		for (let i =0; i < dimensionSet.length; i++){
    			if(ourPerson.candidate_id === dimensionSet[i].matchId){
    				let dimensionsArr = dimensionSet[i].dimensions;

					for (let j =0; j < dimensionsArr.length; j++){

						if(dimensionsArr[j].dimension.includes("title")){
							ourPerson.titleStrength = dimensionsArr[j].strength;
						} else if (dimensionsArr[j].dimension === undefined){
							ourPerson.titleStrength = null;
						}
						if(dimensionsArr[j].dimension.includes("skills")){
							ourPerson.skillStrength = dimensionsArr[j].strength;
						} else if (dimensionsArr[j].dimension === undefined){
							ourPerson.skillStrength = null;
						}
						if(dimensionsArr[j].dimension.includes("interest")){
							ourPerson.interestStrength = dimensionsArr[j].strength;
						} else if (dimensionsArr[j].dimension === undefined){
							ourPerson.interestStrength = null;
						}
					}

    				break;
				}
			}
		}
    	return ourPerson;
	},

    setReqDuration: function(ourJob){
       
        if(ourJob.duration !== null && ourJob.duration_unit !== null){
            var duration = ourJob.duration,
                durationUnit = ourJob.duration_unit,
                regex = /\(\w+\)|\[\w+\]/,

            durationUnit = durationUnit.split(regex);

            if(duration <= 1){
                durationUnit = durationUnit[0];
            } else {
                durationUnit = durationUnit[0] + 's';          
            }

            ourJob.duration_unit = durationUnit;
            
        }
        return ourJob;

    },

    setReqSkillList: function(ourJob, qtokens) {
        var sk = "";

        if (ourJob.skills && ourJob.skills.length > 0) {
            for(var i = 0; i < ourJob.skills.length; i++){
                var skillEssential = ourJob.skills[i].skill_essential,
                    skillName = ourJob.skills[i].skill_name;

              
                // if(skillEssential !== false){
                    sk += skillName + ", ";                  
                // }
                if(i + 1 == ourJob.skills.length){
                    // remove extra comma on last skill in list
                    sk = sk.slice(0, -2);
                }
            }

            if (sk !== "") {
                // Loop over token regex list and replace with highlight class wrapper.
                for (var i=0; i<qtokens.length; i++) {
                    sk = sk.replace(qtokens[i], "<em class=\"skill-hlt\">$1</em>");
                }
            }
        }

        ourJob.skills = sk;
        return ourJob;
    },

    setReqBillRate: function (ourJob){

        const billRateIteration = {
            'hourly' : '/hr',
            'weekly' : '/wk',
            'daily' : '/d',
            'yearly' : '/yr'
        };

        let billRateInterval = '';
            
        if (ourJob.remuneration_interval_code !== null){
            let intervalCode = ourJob.remuneration_interval_code.toLowerCase();

            for (const key in billRateIteration) {
                if (key.includes(intervalCode)) {
                    billRateInterval = billRateIteration[key];
                }
            }
        }

        ourJob.billRateInterval = billRateInterval;

        const formatRate = (min, max) => {
            const currency = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'USD'
                });
           
            let minRate = currency.format(min);
            let maxRate = currency.format(max);
            
            let reqRate = `${minRate} - ${maxRate}`;

            if (min === 0) {
                reqRate = `${maxRate}`;
            } else if (max === 0) {
                reqRate = `${minRate}`;
            }

            if (min === 0 && max === 0) {
                reqRate = 'N/A';
            }

            return reqRate;
        };

        let reqPayRate = formatRate(ourJob.remuneration_pay_rate_min, ourJob.remuneration_pay_rate_max),
            reqBillRate = formatRate(ourJob.remuneration_bill_rate_min, ourJob.remuneration_bill_rate_max);

        ourJob.reqPayRate = (reqPayRate != "N/A") ? reqPayRate + billRateInterval : reqPayRate;
        ourJob.reqBillRate = (reqBillRate != "N/A") ? reqBillRate + billRateInterval : reqBillRate;
        ourJob.reqSalary = formatRate(ourJob.remuneration_salary_min, ourJob.remuneration_salary_max);
    
        return ourJob;
    },

    translateMiscAttr: function(ourJob) {
        let subVendor = ourJob.can_use_approved_sub_vendor,
			ofccpRequired = ourJob.ofccp_required;
       
        ourJob.subVendor = (subVendor === 'true') ? true : false;
        ourJob.ofccpRequired = (ofccpRequired === 'true') ? true : false;

        return ourJob;
        
    },

    // setSkillsTeaser : function (searchParams, personRecord, ourPerson) {
    //     var skillsTeaser = [];

    //     // Loop over the
    //     if (personRecord.highlight && personRecord.highlight.skills) {
    //         var skills = personRecord.highlight.skills;
    //         for (var i=0; i<skills.length; i++) {
    //             skillsTeaser.push(skills[i]);
    //         }
    //     }
        
    //     ourPerson.skillsTeaser = skillsTeaser.join().replace(/class=\"hlt1\"/g, "class=\"hlt2\"");
    //     return ourPerson;
    // },

    // setResumeTeaser : function (searchParams, personRecord, ourPerson, automatchHighlights) {
    //     var candidateId = "";
    //     var whichResume = "";
    //     // If response has highlight section, find the first resume in that section and break.
    //     if (personRecord.highlight) {
    //         for (var i = 1; i <= 5; i++) {
    //             var resumeHit = personRecord.highlight["resume_" + i + ".body"];
    //             if (resumeHit !== undefined) {
    //                 whichResume = "resume_" + i;
    //                 candidateId = personRecord._id;
    //                 break;
    //             }
    //         }
    //     }
        
    //     // For the resume just found, concat all the available teaser lines.
    //     var resumeText = "";
    //     if (resumeHit !== undefined) {
    //         for (var i=0; i<resumeHit.length; i++) {
    //             resumeText += resumeHit[i];
    //         }
    //     }

    //     // NOTE: TEMPORARY fix for a elastic search bug that generates extra long resume teasers.
    //     // Search will be fixed with ES upgrade. This code should be removed after that.
    //     if (resumeText.length > 400) {
    //         resumeText = resumeText.substring(0, 400);
    //     }

    //     if (personRecord.highlight && resumeText!== "") {
    //         ourPerson.teaser = resumeText.trim();
    //         ourPerson.highlightedResume = whichResume;

    //         // For automatch, searchParams.q is undefined. So get the highlight.doc_highlight_query from response.
    //         if (searchParams.q === undefined) {
    //             ourPerson.searchKeyword = automatchHighlights.doc_highlight_query;
    //         } else {
    //             ourPerson.searchKeyword = searchParams.q;
    //         }
    //     } 
    //     return ourPerson;
    // },

    setDisplayAddress : function (ourPerson) {
        ourPerson.display_address = "";
        if (ourPerson.city_name && ourPerson.city_name !== "") {
            ourPerson.display_address = ourPerson.city_name;
        }
        if (ourPerson.country_sub_division_code && ourPerson.country_sub_division_code !== "") {
            ourPerson.display_address += ourPerson.display_address === "" ? 
                            ourPerson.country_sub_division_code : ", " + ourPerson.country_sub_division_code;
        }
        if (ourPerson.country_code && ourPerson.country_code !== "") {
            ourPerson.display_address += ourPerson.display_address === "" ? 
                            ourPerson.country_code : ", " + ourPerson.country_code;
        }
        return ourPerson;
    },

    setPreferredComms : function (ourPerson) {

        ourPerson.preferred_phone_type = "";
        if (ourPerson.communication_home_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "H";
        } else if (ourPerson.communication_mobile_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "M";
        } else if (ourPerson.communication_work_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "W";
        } else if (ourPerson.communication_other_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "O";
        }

        ourPerson.preferred_email_type = "";
        if (ourPerson.communication_email === ourPerson.communication_preferred_email) {
            ourPerson.preferred_email_type = "E";
        } else if (ourPerson.communication_other_email === ourPerson.communication_preferred_email) {
            ourPerson.preferred_email_type = "O";
        } else if (ourPerson.communication_work_email === ourPerson.communication_preferred_email) {
            ourPerson.preferred_email_type = "W";
        }

        //console.log( 'DATA FROM ELASTIC SEARCH - communication_preferred_email ' + ourPerson.communication_preferred_email );
        //console.log( 'DATA FROM ELASTIC SEARCH - communication_preferred_phone ' + ourPerson.communication_preferred_phone );

        return ourPerson;
    },

	checkPipeline : function(ourPerson,component){
		if(ourPerson.pipelines && ourPerson.pipelines.length > 0){
			var currentUser = component.get("v.runningUser");
			if(ourPerson.pipelines.indexOf(currentUser.psId) != -1){
				return true;
			}
		}
		return false;
	},
    translateHitsMap : function(explainHits) {
        var hitsMap = {};
        if (explainHits !== undefined) {
            for (var i=0; i<explainHits.length; i++) {
                hitsMap[explainHits[i].matchId] = explainHits[i];
            }
        }
        return hitsMap;
    },

	updateMatchLocationFromResponse: function(component, response) {
		var ml = component.get("v.multilocation");
		// Update from response only if location data is not present on the component. 
		// So this block only executes when the location_filter returns as part of automatch where search autmatically applies location.
		if (response.location && response.location.length > 0 && (!(ml && ml.length > 0))) {
            var loctxt = "";
            var l = response.location[0];
            // Location object returned with a latitude => search has geolocation for the source doc and applied a radius.
            if (l && l.lat) {
                loctxt = this.toLocationStr(l.city_name, l.country_sub_division_code, l.country_code);
				component.set("v.multilocation", [{
					"displayText": loctxt,
					"geoRadius": l.geoRadius,
					"geoUnitOfMeasure": l.geoUnitOfMeasure,
					"latlon": [l.lat, l.lon],
					"filter": false,
					"useRadius": "SELECT"
				}]);
            } else {
                component.set("v.multilocation", []);
            }
        //S-96264 - Commenting this piece to prepopulate location text from Search for R2C
			/*if (component.get("v.type") === "R2C") {
				 loctxt = component.get("v.automatchSourceData").srcLoc; 
			} else {*/
				// if (response.automatch_source_talent_info && response.automatch_source_talent_info.talent_location_info) {
			//}
			
			/*var locfilt = response.location_filter[0];
			if (locfilt) {*/
		}
	},

	toLocationStr: function(city, state, country) {
		var loc = city ? city : "";
		loc += state ? (loc !== "" ? ", " : "") + state : "";
		loc += country ? (loc !== "" ? ", " : "") + country : "";

		return loc;
	},

	saveMatchVectors: function(component, matchVectors) {
		if (matchVectors) {
			/*component.set("v.matchVectors", {
				"skills_vector" : matchVectors.skills_vector,
				"title_vector" : matchVectors.title_vector,
				"acronym_vector" : matchVectors.acronym_vector,
				"keyword_vector" : matchVectors.keyword_vector,
				"sf_vector" : matchVectors.skillfamily_vector,
				"sf_domain_confidence" : matchVectors.sf_domain_confidence
            });*/
            component.set("v.matchVectors", matchVectors);
		}
	},
    
    requestFacet : function(component, event) {  
        // Has to happen before the call to addFacetOptions()
        this.updateFacetOptions(component, event.getParams());
        
        // No search call if this is a collapsing event. Update facet registry and return.
        if (event.getParam("isCollapsed")) {
            return;
        }

        this.callForFacet(component, event.getParam("facetParamKey"));
    }, 

    updateFacetOptions : function(component, evtParams) { 
        var facetProps = component.get("v.facetProps"); 
        if (facetProps === null) { 
            // This required? Case should never happen. 
            facetProps = {}; 
        } 
        var prop = facetProps[evtParams.facetParamKey]; 
        if (prop !== undefined && prop !== null) { 
            facetProps[evtParams.facetParamKey].isVisible = true; 
            facetProps[evtParams.facetParamKey].isCollapsed = evtParams.isCollapsed; 
        }  
        component.set("v.facetProps", facetProps); 
    }, 

    //for D-07966 from IsFromFacetSearchRequest boolean variable to determine if from facet search then set it to true 
	updateFacetsContainer: function(component, facets, isReexecute, facetCriteria, IsFromFacetSearchRequest, isSmartSearch) {        
		var facetContainer = component.find("facetContainer");
		facetContainer.updateFacets(
			facets, 
			component.get("v.facetDisplayProps"), 
			component.get("v.matchVectors"), 
			component.get("v.automatchSourceData"), 
			facetCriteria,
			isReexecute,
            //component.get("v.facetDisplayProps"),
            component.get("v.excludeKeyWords"),
            IsFromFacetSearchRequest,
            isSmartSearch
		);
	},

	clearFacetsContainer: function(component) {
		var facetContainer = component.find("facetContainer");
		facetContainer.clearFacets();
	},

	hideFacets: function(component) {
		var facetContainer = component.find("facetContainer");
		facetContainer.hideFacets();
	},

    saveFilterCriteriaParams : function(component, event) {
        var qParams = event.getParam("filterParameters");
        var initiatingFacet = event.getParam("initiatingFacet");

        var filterCriteriaParams = component.get("v.filterCriteriaParams");
        if (filterCriteriaParams === null) {
            filterCriteriaParams = {};
        }
        // var filters = this.cloneParamsObject(qParams);

        var fProps = component.get("v.facetProps"); 
        var isNested = fProps[initiatingFacet].isNested; 
        var isUserFiltered = fProps[initiatingFacet].isUserFiltered; 
 
        var selected = event.getParam("selection"); 
        if (selected !== undefined) { 
            if (isNested || isUserFiltered) { 
                var sKeys = Object.keys(selected); 
                selected = selected[sKeys[0]].join("|"); 
            }  
            //filters[event.getParam("includeKey").replace(".include", ".selected")] = selected;
			qParams[event.getParam("includeKey").replace(".contains", ".selected")] = selected;  
        } 
 
        if (isUserFiltered) { 
            qParams[event.getParam("initiatingFacet").replace("nested_facet_filter", "nested_local_facet_filter")]  
                        = JSON.stringify([{"User" : [component.get("v.runningUser.psId")]}]); 
        } 
 
        filterCriteriaParams[initiatingFacet] = qParams;
        component.set("v.filterCriteriaParams", filterCriteriaParams);
    },

    filterFacet : function(component, event) { 
        this.callForFacet(component, event.getParam("initiatingFacet"));
    },

    callForFacet : function(component, initiatingFacet) {
        // var qParams = {};
		var qParams = this.getBaseParameters(component);
		// Override size. No results fetch on facet option request
        qParams.size = 0;

        qParams = this.addFilterCriteriaParams(qParams, component, initiatingFacet);
        qParams = this.addFacetOptions(qParams, component, false, initiatingFacet); 
        // qParams = this.addBasicFacetParams(qParams, component);
        qParams = this.addTrackParameters(qParams, component);
        // Location and other facets are to be considered when filtering a facet.
        qParams = this.addLocationParameters(qParams, component);
        qParams = this.addFacetParameters(qParams, component);
        
        // Call search API and handle response
        this.callSearchForFacets(component, qParams, initiatingFacet);
    },

    /*addBasicFacetParams : function(qParams, component) {
        qParams["flt.group_filter"] = component.get("v.groupFilterOverride") ? 
                                        component.get("v.groupFilterOverride") : component.get("v.runningUser.opcoSearchFilter");
        qParams["id"] = component.get("v.runningUser.id");
        qParams["size"] = 0;

        var autoID = component.get("v.automatchSourceId");
        if (autoID !== undefined && autoID !== null){
            qParams["type"] = component.get("v.automatchType");
            qParams["docId"] = component.get("v.automatchSourceId");
        }
        else{
            var keyword = component.get("v.keyword");
            qParams["q"] = keyword === undefined || keyword === null || keyword ===""? "-randomstring" : keyword;
        }
        
        return qParams;
    },*/

    callSearchForFacets : function(component, qParams, initiatingFacet) {
        var queryStringUrl = this.buildUrl(qParams);
        // console.log(queryStringUrl);
        /*var autoID = component.get("v.automatchSourceId");
        if (autoID !== undefined && autoID !== null){
            var action = component.get("v.automatchType") === "talent2job" ? component.get("c.matchJobs") : component.get("c.matchTalents");
            var autoMatchRequestBody = JSON.stringify(this.getAutoMatchRequestBody (component));
            action.setParams({"queryString": queryStringUrl,
                              "httpRequestType": "POST",
                          	  "requestBody": autoMatchRequestBody});
        }
        else{*/
            var action = this.getActionWithParametersSmartSearch(component, queryStringUrl);
        //}
        
        action.setCallback(this, function(response) {
            this.handleFacetResponse(component, response, initiatingFacet);
        });
        $A.enqueueAction(action); 
    },

    handleFacetResponse : function(component, response, initiatingFacet) {
        if (response.getState() === "SUCCESS") {
            var value = JSON.parse(response.getReturnValue());
            
			if (value.fault) {
				this.toastWarning(value.fault.faultstring ? [value.fault.faultstring] : ["Error retrieving facets"], true);
			} else {
				//For Defect D-07966 last parameter is boolean flag to determine if it is for facetsSearch
				// if set to true then set displayFilters to false in facets Container 
				this.updateFacetsContainer(component, value.response.aggregations, false, null, true, component.get('v.isSmartSearch'));
			}
        }
    },

    updateNestedFacets : function(component, event) {
        var nf = event.getParam("nestedFilters");
        // console.log(nf);
        if (nf) {
            var fcts = component.get("v.facets");
            var keys = Object.keys(nf);
            for (var i=0; i<keys.length; i++) {
                fcts[keys[i]] = nf[keys[i]];
            }
            component.set("v.facets", fcts);
            // console.log(JSON.stringify(fcts));
        }
    },

    updateFacetFilterParams : function(component, event) {
        var filterCriteriaParams = {};

        var fps = event.getParam("facetFilterParams");
        if (fps) {
            var keys = Object.keys(fps);
            for (var i=0; i<keys.length; i++) {
                filterCriteriaParams[keys[i]] = fps[keys[i]];
            }
        }
        component.set("v.filterCriteriaParams", filterCriteriaParams);
    },

	updateMatchVectors: function(component, matchFacetParams) {
        var matchVectors = component.get("v.matchVectors");

        switch (matchFacetParams.matchFacetType) {
            case "jobtitles" :
                matchVectors.title_vector = matchFacetParams.matchFacetData;
                break;

            case "skills" : 
                matchVectors.skills_vector = matchFacetParams.matchFacetData;
                break;

            default :
                break;
        }

        component.set("v.matchVectors", matchVectors);
	},

	setHttpRequestType: function(component, reqType) {
		var hlp = this.findCriteriaHelper(component);
		hlp.set("v.httpRequestType", reqType);
	},

	createSearchLogEntry: function(component) {
		var baseEntry = {
            "execTime": new Date().getTime(),
            "offset" : component.get("v.offset"),
            "pageSize" : component.get("v.pageSize"),
			"sort" : component.get("v.sortField"),
			"order" : component.get("v.sortOrder"),
			"type": component.get("v.type"),
			"engine" : "ES",
            "criteria": {}
        };

		var hlp = this.findCriteriaHelper(component);
		return hlp.createSearchLogEntry(component, baseEntry);
	},

	saveSearchLog : function (component, logEntry, cacheKey) {
        // Call server to save search log entry
        var action = component.get("c.saveUserSearch"); 
        action.setParams({
            "searchEntry" : logEntry,
            "entryType" : this.findCriteriaHelper(component).get("v.lastLogTypeName"),
			"cacheKey" : cacheKey
        });
        action.setCallback(this, function(response) {
			// Response can be 'SUCCESS' / 'ERROR: <msg>'. Simply refresh utility
            this.fireRefreshUtilityEvt();
        });
        $A.enqueueAction(action);
    },

	restoreCriteriaFromLog: function(component) {
		try {
			var hlp = this.findCriteriaHelper(component);
			return hlp.restoreCriteriaFromLog(component);
		}
		catch (ex) {
                var customMessage = "lastSearchLog = " + component.get("v.lastSearchLog") + " and Type in URL:"+ component.get("v.type");
                var errAction = component.get("c.saveErrorLog");
                errAction.setParams({
                    "errMsg": ex.message,
                    "errStackTrace": ex.stackTrace,
                    "errType" : ex.name,
                    "customException" : customMessage
                });
                errAction.setCallback(this,function(response){
                    if(response.getState() !== "SUCCESS")    {
						console.log(response);   
                    }
                });
				$A.enqueueAction(errAction);
				var toastEvent = $A.get("e.force:showToast");
								toastEvent.setParams({
									"title": "Error!",
									"message": "Error Occurred. Please logout and login back.",
									"type": "error"
								});
			   toastEvent.fire();
		}
	},

	executeCachedSearch: function(component, cacheKey) {
		var action = component.get("c.getCachedUserSearch");
        action.setParams({
            "cacheKey" : cacheKey
		});

		this.reexecuteSearch(component, action, true);
	},

	executeSearchAtIndex: function(component, execIndex, entryType) {
		var action = component.get("c.getUserSearchAtIndex");
        action.setParams({
            "entryIndex" : execIndex,
			"entryType" : entryType
		});

		this.reexecuteSearch(component, action, true);
	},

	executeSearchByKey: function(component, searchKey) {
        var action = component.get("c.getUserSearchByKey");
        action.setParams({
            "searchKey" : searchKey,
            "entryType" : "SharedWithMe"
        });
        this.reexecuteSearch(component, action, true);			
	},

	executeLastSearchOrMatch: function(component) {
		var action = component.get("c.getCachedUserSearch");
        action.setParams({
            "cacheKey" : "nocachekey"
		});

		this.reexecuteSearch(component, action, false);
	},

	/** isNotExecLast used to identify "Last Search or Match" link - required to generate cachekey. */
	reexecuteSearch: function(component, action, isNotExecLast) {
		action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
					if (response.getReturnValue().startsWith('ERROR:')) {
						var errList = [];
						errList.push('Shared Search no longer exists!!');
						this.toastWarning(errList, true);
					} else {
						component.set("v.lastSearchLog", response.getReturnValue());						
						this.restoreCriteriaFromLog(component);
                        this.defaultSortIfRelV2Saved(component);
						component.set("v.reExecuteSearchFlag",true); // to pass in sorting
						// This is a reexecute. Set the facets from the restored data.
					
						// For Defect D-07966 last parameter is boolean flag to determine if it is for facetsSearch
						// if set to true then set displayFilters to false in facets Container
						this.updateFacetsContainer(component, null, true, component.get("v.facets"), false, component.get('v.isSmartSearch'));
						this.validateAndSearch(component, null, false, isNotExecLast);
						// Added below to resolve refresh issue.
						if (isNotExecLast) {
							var res = JSON.parse(response.getReturnValue());							
							if (res.type && res.type === "R2C" && res.match && res.match.srcid) {
								component.set("v.opportunityId",res.match.srcid);
							} 
						}
					}
            } else {
                console.log("Could not get last search log!");
            }
        });
        $A.enqueueAction(action);
	},

    defaultSortIfRelV2Saved: function(component) {
        if (component.get("v.sortField") === "relevancyV2") {
            component.set("v.sortField", "");
            component.set("v.sortOrder", "");
        }
    },

	getSavedLogTypeName: function(component) {
		return this.findCriteriaHelper(component).get("v.savedLogTypeName");
        
	},

    toggleUpdateButton: function(component, toggle) {
        if(toggle === true){
             var facetWidth = component.find("facetsWrapper").getElement().clientWidth;
             component.set("v.buttonWidth", facetWidth);
        }
        component.set("v.showUpdateButton", toggle);
    },

	getMatchInsightData : function(component, recordName) {
		var queryStringUrl = this.createQueryString(component, false, true); 		
            var action = this.getActionWithParameters(component, queryStringUrl);
            action.setCallback(this, function(response) {
				component.set("v.searchInProgress", false);
				var searchResponse = JSON.parse(response.getReturnValue());
				if (response.getState() === "SUCCESS" ) {
					if (searchResponse.header.errors && searchResponse.header.errors.length > 0) {
						this.errorProcessing(component,searchResponse.header.errors);
					}
					else if (searchResponse.response && searchResponse.response.match_insight && searchResponse.response.match_insight[0]) {
							var insightsCmp = component.find("matchInsightsModalCmp");
							insightsCmp.set("v.automatchTargetName", recordName);
							insightsCmp.set("v.matchInsights", searchResponse.response.match_insight[0]); 
					}
				} else {
						this.errorProcessing(component, response.getError());
				}
            });
            component.set("v.searchInProgress", true);
            $A.enqueueAction(action);
			component.set("v.isMatchTalentInsightCall", false);
	},

	errorProcessing : function (component, errors) {
		var errList = [];
		var results = {};
		for (var i=0; i<errors.length; i++) {
			if (errors[i] && errors[i].message) {
				errList.push(errors[i].message);
			}
		}
		results.hasError = true;
		results.errors = errList;
		component.set("v.errors", errList);
		component.set("v.results", results);
		this.toastWarning(errList, true);
	},

	updateKeyword : function(component) {
		let keywordField = component.find("inputSearchId").getElement().value.trim();

			component.set("v.keyword", keywordField);
	},
	
	subTransactionId: function(component) {
		let subTransactionId = this.guid();
		component.set('v.subTransactionId', subTransactionId);
		let transactionEvent = $A.get("e.c:ServerCallTracking");
		transactionEvent.setParam('subTransactionId', component.get("v.subTransactionId"));
		transactionEvent.fire();
	},	
	

	getConsentInfo : function(component, results) {
		 var translatedData = [];
		 var ourPerson = {};
		 var contactIds = [];
		 var sRecords = results.records;
		 for(var i=0; i < sRecords.length; i++) {
			contactIds.push(sRecords[i].record.contact_id);
		 }
		
		var contactConsentMap;
		var action = component.get('c.contactConsent');
		action.setParams({"contactIds" : contactIds});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state == 'SUCCESS') {
				contactConsentMap = response.getReturnValue();
				for (var i = 0; i < sRecords.length; i++) {
					ourPerson = sRecords[i];
					if(sRecords[i].type == 'talent') {
						//var record = ourPerson.record;
						if(ourPerson.record.contact_id in contactConsentMap) {
							console.log(contactConsentMap[ourPerson.record.contact_id]);
							ourPerson.record.consent = contactConsentMap[ourPerson.record.contact_id];
						}
					}
					translatedData.push(ourPerson);
				}
				results.records = translatedData;
				component.set("v.smsInfoLoaded", true);
				component.set("v.results", results);				
				
			}
		 });
		 $A.enqueueAction(action);
	},

	/*disableSearchActionCheckboxes : function(component) {

		var disableButtonEvent = $A.get("e.c:E_DisableSearchActionButtons");
		disableButtonEvent.setParams({ "rowsSelectedCount" : 0 });
		disableButtonEvent.setParams({ "rowsSelected" : [] });
		//for unchecking checkboxes
		disableButtonEvent.setParams({ "contacts" : [] });
				
		disableButtonEvent.fire();   

	},
	displayCampaignOnTalentSearch : function(component, type) {
		var displayCampaign = $A.get("e.c:E_DisplayCampaignEvent");

		displayCampaign.setParams({"searchType":type});
		displayCampaign.fire();

	}*/

	//Monika - Set focus to a particular field
	setFocusToField:function(component,event,fieldId){
		var fieldIdToFocus = component.find(fieldId);
		if(fieldIdToFocus == null) {	
		fieldIdToFocus = document.getElementById(fieldId);	
		if(fieldIdToFocus == null) return;	
		}

		setTimeout(function(){ fieldIdToFocus.focus(); }, 200);
	},


    smartSearch:function(component, smartTerms, resetPage, noFacets) {
        // Reset cache. smart search doesn't have cache yet.
        this.resetCacheBrowserHistory(component);

        if (resetPage) {
            component.set("v.offset", 0);
        }

        component.set("v.searchInProgress", true);
        
        let queryString = this.createQueryString(component, false, false);

        var hlp = this.findCriteriaHelper(component);
        var action = hlp.getActionWithParameters(component, queryString, smartTerms);

        action.setCallback(this, function(response) {
            component.set("v.searchInProgress", false);

            // Make sure response parsed only once.
            var searchResponse = JSON.parse(response.getReturnValue());
            component.set("v.smsInfoLoaded", false);
            this.handleResponse(response.getState(), component, searchResponse, null, false, true);

            // Reset offset to 0 so any subsequent calls will load from 0 unless it's a scrolling call.
            component.set("v.showUpdateButton", false);
            this.saveSearchLog(component, this.createSearchLogEntry(component), 'alex123');
        });

        $A.enqueueAction(action);
    }
})