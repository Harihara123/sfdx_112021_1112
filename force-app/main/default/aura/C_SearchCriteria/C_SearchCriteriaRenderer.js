({
        afterRender: function(cmp, helper){
        this.superAfterRender();

            let container = document.getElementById("searchBodyContainer");
            
            let lookup = () => {
                let facetCol = container.querySelector('.facet-container-scroll');

                if (facetCol) {
                    cmp.set('v.scrolledValue', -10);
                    facetCol.onscroll = (ev) => {
                        let scrollValue = isNaN(parseInt(`-${ev.target.scrollTop + 10}`)) ? 100 : parseInt(`-${ev.target.scrollTop + 10}`);

                        cmp.set('v.scrolledValue', scrollValue);
                       
                    }
                } else {
                    cmp.set('v.scrolledValue', -10);
                    setTimeout(() => {
                        lookup();
                    }, 1000);
                }
	        }
    
		    lookup();
        }
})