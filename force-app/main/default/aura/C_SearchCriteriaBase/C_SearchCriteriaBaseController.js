({
	
    doInit : function(component, event, helper) {
    	// Component reinitialized in the case of match, so defaultFilters need to be set again.
    	if (component.get("v.defaultFilters") && component.get("v.execLast") !== true) {
    		helper.updateFacetsFromDefaultFilters(component);            
    	}
		helper.initializeSearch(component);
	},
	
	//S-80157 Browser back on Talent Search - Karthik
	srcIdChange : function(component, event, helper) {
		console.log("ID change --- " + component.get("v.initSearchLocationTrigger"));
		if (component.get("v.initSearchLocationTrigger")) {
			helper.initSearchLocation(component);
		}
	},
	// S-80157 End

	searchReadyHandler : function(component, event, helper) {
		if (component.get("v.searchReady")) {
       		var hasSrchString = component.get("v.searchString") !== undefined 
								&& component.get("v.searchString") !== null 
								&& component.get("v.searchString") !== "";
			if (component.get("v.execLast") || hasSrchString) {
				helper.validateAndSearch(component, null, false);
			}
			component.set("v.searchReady", false);
		}
	},
    
    matchReadyHandler : function(component, event, helper) {
		if (component.get("v.matchReady")) {
			component.set("v.trkRequestId", helper.guid());
			if (component.get("v.automatchSourceId") && !component.get("v.execLast")) {
				var searchInitializedEvt = $A.get("e.c:E_SearchInitialized");
				searchInitializedEvt.fire();
			}
			component.set("v.matchReady", false);
		}
	},

	
	handleSort : function(component, event, helper) {
		// console.log(event.getParam("sortField"));
		// console.log(event.getParam("sortOrder"));
		component.set("v.sortField", event.getParam("sortField"));
		component.set("v.sortOrder", event.getParam("sortOrder"));
		helper.validateAndSearch(component, null, true);

	},

	searchOnEnter : function(component, event, helper) {
        if (event.getParams().keyCode === 13) {
           	helper.validateAndSearch(component, null, false);
        }
	},

	scrollSearch : function(component, event, helper) {
		component.set("v.pageSize", event.getParam("pageSize"));
		component.set("v.offset", event.getParam("offset"));
		helper.validateAndSearch(component, null, true);
	},

	updatePageSize : function(component, event, helper) {
		component.set("v.pageSize", event.getParam("pageSize"));
		component.set("v.offset", 0);
		helper.validateAndSearch(component, null, true);
	},
    
    getHtmlResume : function(component, event, helper, test) {
		//alert("hello");
    },
    
    applySearchFacets : function(component, event, helper) {
        component.set("v.facets", event.getParam("facets"));
		component.set("v.facetDisplayProps", event.getParam("displayProps"));
        /* Also adding in the nested_filter parameters */
        helper.updateNestedFacets(component, event);
        // Update the filterParameters for filtered facets.
        helper.updateFacetFilterParams(component, event);
		helper.validateAndSearch(component, event.getParam("initiatingFacet"), false);
	},

    performSearch : function(component, event, helper) {
		// Perform search
		// Throw E_SearchCompleted event with response data as params.
		helper.validateAndSearch(component, null, false);
	},

	filterFacet : function(component, event, helper) {
		helper.saveFilterCriteriaParams(component, event);
		// Make the Search API Request to filter down a facet
		helper.filterFacet(component, event);
	},

  	requestFacet : function(component, event, helper) { 
    	// console.log("Facet requested ... sneding to search"); 
    	// Make the Search API Request to retrieve a facet 
    	helper.requestFacet(component, event); 
  	}, 

	killComponent : function(component, event, helper) {
		if (component.isValid()) {
			component.destroy();
		}
	},

	updateFacetState : function(component, event, helper) {
		helper.updateFacetOptions(component, event.getParams());
	},

	defaultFiltersChangeHandler : function(component, event, helper) {
		helper.updateFacetsFromDefaultFilters(component);
	},
    
    kwChangeHandler : function(component, event, helper) {
        // console.log("Generating new TRACK Id for search ... ");
        component.set("v.trkRequestId", helper.guid());
    },

    prefsHandler : function(component, event, helper) {
    	if (!component.get("v.prefUpdate") && component.get("v.userSearchPrefs") && component.get("v.prefsMeta")) {
    		component.set("v.prefUpdate", true);
    		helper.updateSelection(component);
			helper.firePrefsLoaded(component);
	    	component.set("v.prefUpdate", false);
    	}
    }

})