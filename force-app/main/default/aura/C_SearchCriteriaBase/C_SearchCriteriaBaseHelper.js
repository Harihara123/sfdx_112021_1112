({
    initializeSearch : function(component) {
		//S-80157 Browser back on Talent Search/To clear Facets - Karthik
		if (component.get("v.automatchSourceId") && component.get("v.automatchType") !== "talent2job") {
			component.set("v.facets", {});
			component.set("v.filterCriteriaParams", {});
		}
		//S-80157 End

        // Get current user
        this.getCurrentUser(component);
        // Init search location to empty object or automatch source location.
        this.initSearchLocation(component);
	},

    getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var r = JSON.parse(response.getReturnValue());
                component.set ("v.runningUser", r);
				component.set ("v.groupFilter", r.opcoSearchFilter);
                // Get user group filter based on the user Opco
                // this.getUserGroupFilter(component);
                this.getSearchPrefs(component);
			}
			this.updateReadyStatus(component);
        });
        $A.enqueueAction(action);
    },

    /*getUserGroupFilter : function(component) {
        var action1 = component.get("c.getUserGroupFilterByOpco"); 
        action1.setParams({"userOpco": component.get("v.runningUser.OPCO__c")});
        action1.setStorable();
        action1.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                component.set ("v.groupFilter", response);
                // this.initSearchLocation(component);
            }
            this.updateReadyStatus(component);
        });
        $A.enqueueAction(action1);
    },*/

    getSearchPrefs : function(component) {
        var action = component.get("c.getUserPreferences");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var r = response.getReturnValue();
                if (r !== "") {
                    component.set("v.userSearchPrefs", JSON.parse(r));
                } else {
                    component.set("v.userSearchPrefs", {});
                }
            }
        });
        $A.enqueueAction(action);
    },

    initSearchLocation : function(component) {
        if (component.get("v.automatchSourceId") && !component.get("v.execLast")) {
            var action = component.get("c.getAutoMatchData"); 
            action.setParams({ 
                "srcId": component.get("v.automatchSourceId") 
            });
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    var responseValue = response.getReturnValue();
                    /*console.log ("location response >>>>>>>>>>>>");
                    console.log (responseValue);*/
                    var resp = JSON.parse(responseValue); 
 
                    if (resp !== null) { 
                        var location = this.buildLocationString(resp.city, resp.stateValue, resp.countryValue, resp.postalCode);

                        component.set("v.autoMatchData", resp);
                        component.set("v.autoMatchLocation", location);
                        // Set multilocation only if automatch location available.
                        if (location.trim().length > 0) {
							/* Logic here has to match location building logic in C_TalentMatchedREQs */
                            // if latitude (and longitude) present, "SELECT"
                            // for no latitude (and longitude) --- if talent2job "AUTO", else "NONE". Also, radius for talent2job = 50
							// APANACHI 05/24/2018 - AUTO may not be required in this case. Only applies when a location is picked from google?
							var ur = resp.latitude ? "SELECT" : "NONE";
							var r = component.get("v.automatchType") === "talent2job" ? "50" : "25";

							var loc;
							if (ur === "NONE") {
								loc = {
									"filter" : true,
									"useRadius" : ur,
									"displayText" : location
								};
								if (resp.city && resp.city !== "") {
									loc.city = resp.city;
								}
								if (resp.stateValue && resp.stateValue !== "") {
									loc.state = resp.stateValue;
								}
								if (resp.countryValue && resp.countryValue !== "") {
									loc.country = resp.countryValue;
								}
								if (resp.postalCode && resp.postalCode !== "") {
									loc.postalcode = resp.postalCode;
								}
							} else {
								loc = {
									"filter" : false,
									"useRadius" : ur,
									"geoRadius" : r,
									"geoUnitOfMeasure" : "mi",
									"latlon" : [resp.latitude, resp.longitude],
									"displayText" : location
								};
							}

							component.set("v.multilocation", [loc]);
                        } 
                        
                        // Use the automatch location to search only if a non-empty location string available.
                        component.set("v.usePresetLocation", location.trim().length > 0);
                    }

                }
                // Mark match source call has returned.
                component.set("v.matchSrcReturned", true);
                this.updateReadyStatus(component);
            });
            $A.enqueueAction(action); 
        } 
    },

    buildLocationString : function(cty, stt, cntry, postal) {
        var loc = cty !== null ? cty : "";
		loc += postal !== null ? (loc === "" ? postal : " " + postal) : "";
        loc += stt !== null ? (loc === "" ? stt : ", " + stt) : "";
        loc += cntry !== null ? (loc === "" ? cntry : ", " + cntry) : "";
        return loc;
    },
    
    updateReadyStatus : function(component) {
        var isGrpFilterInit = component.get("v.groupFilter") !== undefined 
                            && component.get("v.groupFilter") !== null
                            && component.get("v.groupFilter") !== "";

        // Group filter available
        if (isGrpFilterInit) {
            component.set("v.searchReady", true);
        } 
        // Group filter available and automatch source call has returned / last search (no automatch call required).
        if (isGrpFilterInit && (component.get("v.matchSrcReturned") || component.get("v.execLast"))) {
            component.set("v.matchReady", true);
        }
        
        /*console.log("--- " + isGrpFilterInit + ", " + component.get("v.matchSrcReturned"));
        console.log("Search ready = " + component.get("v.searchReady") + ", Match ready = " + component.get("v.matchReady"));*/
    },

    fireReexecuteLastEvt : function(component) {
        var reexecuteEvt = $A.get("e.c:E_SearchReexecuteLast");
        // var keywordFld = component.find("inputSearchId");
        let keywordFld = component.get("v.keyword");
        reexecuteEvt.setParams({
            "keyword": keywordFld !== undefined ? keywordFld : "",
            "location": component.get("v.locationSelection"),
            "facets": component.get("v.facets")
        });
        reexecuteEvt.fire();
    },

    setKeyword : function (component, selectedKeyword) {
        var concreteComponent = component.getConcreteComponent();
        concreteComponent.getDef().getHelper().setKeyword(concreteComponent, selectedKeyword);
    },
	
    validateAndSearch : function (component, initiatingFacet, noFacets) {
        // Needs check for concrete component not being self? to prevent endless loop!
        var concreteComponent = component.getConcreteComponent();
        concreteComponent.getDef().getHelper().validateAndSearch(
            concreteComponent, 
            initiatingFacet,
            noFacets);
    },

    getResultOffset : function (responseObj) {
        var offset = 0;
        var params = responseObj.header.request.params;
        if (params && params.from) {
            offset = params.from;
        }
        return offset;
    },
    
    translateRecords : function (searchResponse) {
        // var st = new Date().getTime(); 
        var recordset = searchResponse.response.hits.hits;
        var translatedData = [];

        // For req search, split query term into regexp tokens.
        var qtokens = [];
        if (searchResponse.header.request.params.index === "job" && searchResponse.header.request.params.type !== "talent2job" ) {
            // replace boolean operators and parentheses before split
            var q = searchResponse.header.request.params.q.replace( new RegExp("(\\sand\\s|\\sor\\s)|[^a-z0-9+#]", "gi") , " ").split(" ");
            var k = 0;
            for (var j=0; j<q.length; j++) {
                var st = q[j].trim();
                if (st !== "") {
                    var strep = st.replace(/(\*|\?)/gi, "\\S$1"),
                        newStrep = strep.replace(/[^\w\s]/gi, "");
                    // Check for ++ due to c++ query issue
                    if(st.includes("++")){
                        
                        // Escapes ++ characters for regex
                        strep = "(" + newStrep + "\\+\\+\\s*|" + newStrep + "\\+\\+\\$)";

                    }else{
                        // strep = strep.replace(/[^\w\s]/gi, "");
                        strep = "(" + strep + "\\s*|" + strep + "$)";
                    }
                   
                    qtokens[k++] = new RegExp(strep, "gi");
                }
            }
        }

        for (var i = 0; i < recordset.length; i++) {
            var tx;
            switch (recordset[i]._type) {
                case "person" : 
                    tx = this.translatePerson(recordset[i], searchResponse.header.request.params, searchResponse.response.highlights);
                    break;

                case "job" :
                    tx = this.translateJob(recordset[i], qtokens);
                    break;

                default :
                    tx = null;
                    break;
            }
            translatedData.push(tx);
        }

        // console.log("---TRANSLATE RESCORDS -- " + (new Date().getTime() - st) + " ms"); 
        return translatedData;
    },

    /**
     * [{"id" : "18-chars", "record" : {}, "score" : 20.221, "type" : "talent" }]
     */
    translatePerson : function (personRecord, searchParams, automatchHighlights) {
        var ourPerson = {};
        ourPerson.record = personRecord._source;
        ourPerson.record = this.setPreferredComms(ourPerson.record);
        ourPerson.record = this.setDisplayAddress(ourPerson.record);
        ourPerson.record = this.setResumeTeaser(searchParams, personRecord, ourPerson.record, automatchHighlights);
        ourPerson.record = this.setSkillsTeaser(searchParams, personRecord, ourPerson.record);
        ourPerson.id = personRecord._id;
        ourPerson.score = Math.round(personRecord._score * 100);
        ourPerson.type = "talent";

        return ourPerson;
    },

    translateJob : function(jobRecord, qtokens) {
        var ourJob = {};
        ourJob.record = jobRecord._source;
        ourJob.record = this.setReqDuration(ourJob.record);
        ourJob.record = this.setReqSkillList(ourJob.record, qtokens);
        ourJob.id = jobRecord._id;
        ourJob.score = Math.round(jobRecord._score * 100);
        ourJob.type = "req";
        
        return ourJob;
    },

    setReqDuration: function(ourJob){
       
        if(ourJob.duration !== null && ourJob.duration_unit !== null){
            var duration = ourJob.duration,
                durationUnit = ourJob.duration_unit,
                regex = /\(\w+\)|\[\w+\]/,

            durationUnit = durationUnit.split(regex);

            if(duration <= 1){
                durationUnit = durationUnit[0];
            } else {
                durationUnit = durationUnit[0] + 's';          
            }

            ourJob.duration_unit = durationUnit;
            
        }
        return ourJob;

    },

    setReqSkillList: function(ourJob, qtokens) {
        var sk = "";

        if (ourJob.skills && ourJob.skills.length > 0) {
            for(var i = 0; i < ourJob.skills.length; i++){
                var skillEssential = ourJob.skills[i].skill_essential,
                    skillName = ourJob.skills[i].skill_name;

              
                // if(skillEssential !== false){
                    sk += skillName + ", ";                  
                // }
                if(i + 1 == ourJob.skills.length){
                    // remove extra comma on last skill in list
                    sk = sk.slice(0, -2);
                }
            }

            if (sk !== "") {
                // Loop over token regex list and replace with highlight class wrapper.
                for (var i=0; i<qtokens.length; i++) {
                    sk = sk.replace(qtokens[i], "<em class=\"skill-hlt\">$1</em>");
                }
            }
        }

        ourJob.skills = sk;
        return ourJob;
    },

    setSkillsTeaser : function (searchParams, personRecord, ourPerson) {
        var skillsTeaser = [];

        // Loop over the
        if (personRecord.highlight && personRecord.highlight.skills) {
            var skills = personRecord.highlight.skills;
            for (var i=0; i<skills.length; i++) {
                skillsTeaser.push(skills[i]);
            }
        }
        
        ourPerson.skillsTeaser = skillsTeaser.join().replace(/class=\"hlt1\"/g, "class=\"hlt2\"");
        return ourPerson;
    },

    setResumeTeaser : function (searchParams, personRecord, ourPerson, automatchHighlights) {
        var candidateId = "";
        var whichResume = "";
        // If response has highlight section, find the first resume in that section and break.
        if (personRecord.highlight) {
            for (var i = 1; i <= 5; i++) {
                var resumeHit = personRecord.highlight["resume_" + i + ".body"];
                if (resumeHit !== undefined) {
                    whichResume = "resume_" + i;
                    candidateId = personRecord._id;
                    break;
                }
            }
        }
        
        // For the resume just found, concat all the available teaser lines.
        var resumeText = "";
        if (resumeHit !== undefined) {
            for (var i=0; i<resumeHit.length; i++) {
                resumeText += resumeHit[i];
            }
        }

        // NOTE: TEMPORARY fix for a elastic search bug that generates extra long resume teasers.
        // Search will be fixed with ES upgrade. This code should be removed after that.
        if (resumeText.length > 400) {
            resumeText = resumeText.substring(0, 400);
        }

        if (personRecord.highlight && resumeText!== "") {
            ourPerson.teaser = resumeText.trim();
            ourPerson.highlightedResume = whichResume;

            // For automatch, searchParams.q is undefined. So get the highlight.doc_highlight_query from response.
            if (searchParams.q === undefined) {
                ourPerson.searchKeyword = automatchHighlights.doc_highlight_query;
            } else {
                ourPerson.searchKeyword = searchParams.q;
            }
        } 
        return ourPerson;
    },

    setDisplayAddress : function (ourPerson) {
        ourPerson.display_address = "";
        if (ourPerson.city_name && ourPerson.city_name !== "") {
            ourPerson.display_address = ourPerson.city_name;
        }
        if (ourPerson.country_sub_division_code && ourPerson.country_sub_division_code !== "") {
            ourPerson.display_address += ourPerson.display_address === "" ? 
                            ourPerson.country_sub_division_code : ", " + ourPerson.country_sub_division_code;
        }
        if (ourPerson.country_code && ourPerson.country_code !== "") {
            ourPerson.display_address += ourPerson.display_address === "" ? 
                            ourPerson.country_code : ", " + ourPerson.country_code;
        }
        return ourPerson;
    },

    setPreferredComms : function (ourPerson) {

        ourPerson.preferred_phone_type = "";
        if (ourPerson.communication_home_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "H";
        } else if (ourPerson.communication_mobile_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "M";
        } else if (ourPerson.communication_work_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "W";
        } else if (ourPerson.communication_other_phone === ourPerson.communication_preferred_phone) {
            ourPerson.preferred_phone_type = "O";
        }

        ourPerson.preferred_email_type = "";
        if (ourPerson.communication_email === ourPerson.communication_preferred_email) {
            ourPerson.preferred_email_type = "E";
        } else if (ourPerson.communication_other_email === ourPerson.communication_preferred_email) {
            ourPerson.preferred_email_type = "O";
        } else if (ourPerson.communication_work_email === ourPerson.communication_preferred_email) {
            ourPerson.preferred_email_type = "W";
        }

        //console.log( 'DATA FROM ELASTIC SEARCH - communication_preferred_email ' + ourPerson.communication_preferred_email );
        //console.log( 'DATA FROM ELASTIC SEARCH - communication_preferred_phone ' + ourPerson.communication_preferred_phone );

        return ourPerson;
    },
    
    buildUrl : function (parameters) {
         var qs = "";
         for(var key in parameters) {
            var value = parameters[key];
            /* S-32457 adding condition to handle multiple facet keys.
               Keys sent as comma separated values for facet parame keys

               NOTE: if comma becomes a valid character this solution will not work. 

               */
            if(key.indexOf(",") > -1){
                var paramValues; 
                var paramKeys = key.split(",");

                if(value !== undefined && value !== null){
                    paramValues = value.split(",");
                    for(var i=0; i<paramValues.length; i++){
                      qs += encodeURIComponent(paramKeys[i]) + "=" + encodeURIComponent(paramValues[i]) + "&"; 
                    }
                }

            }else{
              qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
            }

         }
         if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
         }
         return qs;
     },

     setSearchInProgress : function (component) {
        component.set("v.searchInProgress", true);
     },

     resetSearchInProgress : function (component) {
        component.set("v.searchInProgress", false);
     },

     addFacetOptions : function (parameters, component, execLast, singleFacet) { 
        var facetProps = component.get("v.facetProps");
        var noSearchYet = component.get("v.noSearchYet");

		if (singleFacet && singleFacet.length > 0) {
			return this.addSingleFacetOptions(parameters, noSearchYet, execLast, facetProps[singleFacet]);
		}

        if (facetProps && facetProps !== null) { 
            var keys = Object.keys(facetProps); 
            for (var i=0; i<keys.length; i++) { 
				parameters = this.addSingleFacetOptions(parameters, noSearchYet, execLast, facetProps[keys[i]]);
            } 
            // console.log(parameters); 
        } 
        return parameters; 
     }, 

	 addSingleFacetOptions : function (parameters, noSearchYet, execLast, facetProp) {
        var oKey = facetProp.optionKey; 
		var iniFilt = facetProp.initialFilter; 

                if (oKey !== undefined) {
                    if (iniFilt && noSearchYet && !execLast) {
                        // If there is an initial filter on this parameter.
                        // console.log(iniFilt);
                        var filtKeys = Object.keys(iniFilt);
                        for (var k=0; k<filtKeys.length; k++) {
                            parameters[filtKeys[k]] = iniFilt[filtKeys[k]];
                        }
                        parameters[oKey] = true; 
                    } else {
                        // Turn on the facet option if facet is not collapsed.
                parameters[oKey] = !facetProp.isCollapsed; 
                    }

                    // If fetching the facets, also add the size parameter if available
            if (parameters[oKey] === true && facetProp.sizeKey !== undefined) {
                parameters[facetProp.sizeKey] = facetProp.fetchSize;
                        }
                    }

        return parameters; 
     }, 

     addFacetParameters : function (parameters, component) {
        var selectedFacets = component.get("v.facets");
        if (selectedFacets && selectedFacets !== null) {
            var keys = Object.keys(selectedFacets);
            for (var i=0; i<keys.length; i++) {

                if (!this.isEmptyFacet(selectedFacets[keys[i]])) {
                    if (typeof selectedFacets[keys[i]] === "string") {
                        parameters[keys[i]] = selectedFacets[keys[i]];
                    } else {
                        parameters[keys[i]] = JSON.stringify(selectedFacets[keys[i]]);
                    }
                }
            }
            // console.log(parameters);
        }
        return parameters;
     },

     isEmptyFacet : function (facet) {
        var emptyFacet = false;
        if (facet !== undefined && facet !== null) {
            // Not undefined or null
            if (facet === "") {
                // String or any other basic type empty
                emptyFacet = true;
            } else if (typeof facet === "object" && (facet.length === undefined || facet.length === 0)) {
                // Object / array
                emptyFacet = true;
            }
        } else {
            emptyFacet = true;
        }

        return emptyFacet;
     },

     addTrackParameters : function (parameters, component) {
        // parameters.userId = component.get("v.runningUser.Id"); - Added in apex code
        // parameters.sessionId = Math.round(Math.random()*1000000000000); - Added in apex code
        parameters.transactionId = this.guid();
		component.set("v.transactionId",parameters.transactionId);//Added by akshay S - 71890 5/15/18
        parameters.requestId = component.get("v.trkRequestId");
        return parameters;
     },

     addLocationParameters : function (parameters, component) {
        if (component.get("v.multilocation")) {
            // Add location parameter if the array is not empty
            var loc = this.translateToSearchApiLocation(component.get("v.multilocation"));
            if (loc.length > 0) {
                parameters.location = JSON.stringify(loc);
            }
        } else if (component.get("v.radius") >= 0 && component.get("v.latlong") !== undefined) {
            parameters.performGeoAsFilter = false;
            parameters.geo = component.get("v.latlong");
            parameters.geoRadius = component.get("v.radius");
            parameters.geoUnitOfMeasure = component.get("v.radiusUnit");
        } else if (component.get("v.radius") < 0 && component.get("v.location") !== undefined) {
            parameters.performGeoAsFilter = true;
            var loc = component.get("v.location");
            var spl = loc.split(",");
            if (spl.length === 2) {
                parameters.geo = spl[0].trim();
                parameters.geoCntry = spl[1].trim();
            } else {
                parameters.geoCntry = loc.trim();
            }
        }

        return parameters;
    },

    translateToSearchApiLocation : function(loc) {
        var val = [];

        for (var i=0; i<loc.length; i++) {
            /*  Output value of "filter"
             *  useRadius   --> |  NONE  |  SELECT  |  AUTO 
             *  alwaysUseRadius |
             *  -------------------------------------------
             *    TRUE          |   N/A  |   FALSE  | FALSE
             *    FALSE         |  TRUE  |   FALSE  | FALSE
             */
            var filter = loc[i].useRadius === "NONE";
            var radius = typeof loc[i].geoRadius === "number" ? loc[i].geoRadius.toFixed() : loc[i].geoRadius;

            if (filter) {
                val.push({
                    "filter" : filter,
                    "city" : loc[i].city,
                    "state" : loc[i].state,
                    "country" : loc[i].country,
                    "postalcode" : loc[i].postalcode
                });
            } else {
                val.push({
                    "latlon" : loc[i].latlon,
                    "geoRadius" : radius,
                    "geoUnitOfMeasure" : loc[i].geoUnitOfMeasure,
                    "filter" : filter
                });
            }
        }

        return val;
    },

    addSortParameters : function (parameters, component) {
        if (component.get("v.sortField")) { 
            parameters.sort = component.get("v.sortField") + ":" + component.get("v.sortOrder"); 
        } 
        return parameters;
    },
        
    validateLocation : function (component) {
        /* Return an error message if city selected without country and state. Boolean true otherwise */
        /*if (this.getCity(component) !== "" && (this.getCountry(component) === "" || this.getState(component) === "")) {
            return "You must include an accurate City, State/Province, and Country for a City location search.";
        } else {
            return true;
        }*/
        return true;
    },

    /**
     * Translate from service returned aggregations to local structure
     * TBD
     * 
     * @param  {Object} aggregations [aggregations part of the response]
     * @return {Object}              [translated to consistent internal format]
     */
    translateFacets : function (aggregations) {
        return aggregations;
    },

    getWarnings: function(params) {
        var warnings = [];
        if (params.performGeoAsFilter === "false" && (params.geo === undefined || params.geo === null || params.geo === "")) {
            warnings.push("The City does not match the State/Province selected, so location was not used in the search. Please update the city information and search again.");
        }
        // For future releases: figure a way to print multiple warnings into a toast

        return warnings;
    },

    toastWarning : function (warningArray, isError){
        for (var i = 0; i < warningArray.length; i++){

          var warningMessage = warningArray[i];
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
                "title": isError ? "Error!" : "Warning!",
                "message": warningMessage,
                "type": isError ? "error" : "warning",
                "duration": 5000
            });
          
          toastEvent.fire();
        }
    },

    handleResponse : function (state, component, response, initiatingFacet) {
        // var st = new Date().getTime(); 
        var searchCompletedEvt = $A.get("e.c:E_SearchCompleted");
        var errList = [];

        if (state === "SUCCESS") {
            // var searchTalentsResponse = response.getReturnValue();
            // var searchResponse = JSON.parse(searchTalentsResponse);
            // console.log(response);
            
            if (response.fault) {
                errList.push(response.fault.faultstring);
                searchCompletedEvt.setParams({
                    "hasError" : true,
                    "errors" : errList,
                    "initiatingFacet" : initiatingFacet
                });
                // console.log("Error!");
            } else if (response.header.errors && response.header.errors.length > 0) {
                var errors = response.header.errors;
                for (var i=0; i<errors.length; i++) {
                    if (errors[i] && errors[i].message) {
                        errList.push(errors[i].message);
                    }
                }
                searchCompletedEvt.setParams({
                    "hasError" : true,
                    "errors" : errList,
                    "initiatingFacet" : initiatingFacet
                });
                // console.log("Error!");
            } else {
                var warningArray = this.getWarnings(response.header.request.params);
                this.toastWarning(warningArray, false);

                searchCompletedEvt.setParams(
                    this.createSearchCompletedEvtParams(component, response, initiatingFacet));

                this.toggleSaveSearch(component);
                // Setting the flag to false to indicate that a search has happened. 
                // Any default facets / filters will not be explicity applied going forward.
                component.set("v.noSearchYet", false);
                /*component.set("v.isCollapsed", true);
                component.set("v.criteriaSummary", this.generateSearchCriteriaSummary(component, response));*/
            }
        } else {
            var errors = response.getError();
            if (errors) {
                for (var i=0; i<errors.length; i++) {
                    if (errors[i] && errors[i].message) {
                        errList.push(errors[i].message);
                    }
                }
            }
            // errList.push("Unexpected error occured. Please try again!");
            searchCompletedEvt.setParams({
                "hasError" : true,
                "errors" : errList,
                "initiatingFacet" : initiatingFacet
            });
        }
        searchCompletedEvt.fire();
        component.set("v.errors", errList);
		this.toastWarning(errList, true);

        // Changes icon to allow saving search again
        component.set("v.searchSaved", false);
    },

    createSearchCompletedEvtParams : function(component, searchResponse, initiatingFacet) {
        // var st = new Date().getTime(); 
        var data = {
          "records" : this.translateRecords(searchResponse),
          "totalResultCount" : searchResponse.response.hits.total,
          "offset" : this.getResultOffset(searchResponse),
          "hasError" : false,
          "facets": this.translateFacets(searchResponse.response.aggregations),
          "initiatingFacet" : initiatingFacet,
		  "facetDisplayProps" : component.get("v.facetDisplayProps"),
		  //Added requestid,transactionid by akshay for S - 71890 5/15/18
		  "requestId" : component.get("v.trkRequestId"),
		  "transactionId" : component.get("v.transactionId")
        };

        if (searchResponse.header.request.params.sort) {
            var s = searchResponse.header.request.params.sort.split(":");
            data.sortField = s[0];
            data.sortOrder = s[1];
        }

        if (component.get("v.automatchSourceId")) {
            var allVectors = component.get("v.matchVectors");
            if (allVectors.title_vector !== undefined) {
                data.automatchTitles = allVectors.title_vector;
            }
            if (allVectors.skills_vector !== undefined) {
                data.autoMatchSkills = allVectors.skills_vector;
            }
            //S-34736 - To include Other_Vector in the request body Added by Karthik  
            if (allVectors.other_vector !== undefined) {
                data.otherVector = allVectors.other_vector;
            }
            data.explainHits = this.translateHitsMap(searchResponse.response.hits_explanation);
            data.autoMatchSourceData = component.get("v.autoMatchData");
        }
        // console.log("Create completed Event params --- " + (new Date().getTime() - st) + " ms"); 
        
        return data;
    },

    translateHitsMap : function(explainHits) {
        var hitsMap = {};
        if (explainHits !== undefined) {
            for (var i=0; i<explainHits.length; i++) {
                hitsMap[explainHits[i].matchId] = explainHits[i];
            }
        }
        return hitsMap;
    },

    checkAllowNoKeyword : function(component) {
        var allow = false;
        var facetProps = component.get("v.facetProps");
        var facets = component.get("v.facets");
        // Keyword is required if no facets selected.
        if (facets !== null) {
            var appliedFacets = Object.keys(facets);
            // Check the facetProps if any of the applied facets have allowNoKeywordSearch set to true.
            for (var i=0; i<appliedFacets.length; i++) {
                var facet = facets[appliedFacets[i]];
                var props = facetProps[appliedFacets[i]];
                if (facet !== "" && props !== undefined && props.allowNoKeywordSearch === true) {
                    allow = true;
                    break;
                }
            }
        }

        return allow;
    },

    /************ Methos to handle last / saved searches *************/
    /**
     * Method saves the search log for the user to the User_Search_Log__c object.
     * This is initially intended for the Last Search only, but will be extended for Last 'N' searches and Saved Searches.
     * The saved structure is a JSON object, which is an array of search criteria. Since we only need the 1 last search 
     * at this point, the array will start with a size of 1. 
     */
    toggleSaveSearch: function(component){
        var validateSearch = this.validateSearch(component),
            checkFacets = component.get("v.facets");
        
        if(validateSearch !== false || checkFacets !== null || checkFacets !== undefined){
            component.set('v.searchSavedToggle', false);
            return true;
        }
        
    },

    //saveSearchLog : function (component, logEntry) {
	saveSearchLog : function (component, logEntry, cacheKey) { //S-80157 Browser back on Talent Search - Karthik
        // Call server to save search log entry
        // var action = component.get("c.saveUserSearchLog"); 
        var action = component.get("c.saveUserSearch"); 
        action.setParams({
            "searchEntry" : logEntry,
            "entryType" : component.get("v.lastLogTypeName"),
			"cacheKey" : cacheKey //S-80157 
          });
        action.setCallback(this, function(response) {
            // console.log(response);
            if (response.getState() === "SUCCESS") {
                // Server SUCCESS - save to attribute.
                component.set("v.lastSearchLog", response.getReturnValue());
            } else {
                // console.log("Log save failed!");
            }
        });
        $A.enqueueAction(action);
    },

	//S-80157 Browser back on Talent Search - Karthik
    randomStringGen: function(component) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
	//S-80157 End

    getSearchLogEntry : function (component) {
        var entry = JSON.stringify({
            "execTime": new Date().getTime(),
            "offset" : component.get("v.offset"),
            "pageSize" : component.get("v.pageSize"),
            "criteria": {}
        });
        
        return entry;
    },

    restoreCriteriaFromLog : function(component) {
        var concreteComponent = component.getConcreteComponent();
        concreteComponent.getDef().getHelper().restoreCriteriaFromLog(concreteComponent);
    },
    /************ Methos to handle last / saved searches *************/

    updateNestedFacets : function(component, event) {
        var nf = event.getParam("nestedFilters");
        // console.log(nf);
        if (nf) {
            var fcts = component.get("v.facets");
            var keys = Object.keys(nf);
            for (var i=0; i<keys.length; i++) {
                fcts[keys[i]] = nf[keys[i]];
            }
            component.set("v.facets", fcts);
            // console.log(JSON.stringify(fcts));
        }
    },

    updateFacetFilterParams : function(component, event) {
        //var filterCriteriaParams = component.get("v.filterCriteriaParams");
        //if (filterCriteriaParams === null) {
            filterCriteriaParams = {};
        //}

        /*var facetKey = event.getParam("initiatingFacet");
        var fprops = component.get("v.facetProps");
        if (facetKey && facetKey !== "" && fprops[facetKey].isFilterable) {
            var fps = event.getParam("facetFilterParams");
            
            if (fps && facetKey !== "") {
                // Update the filter criteria for the facet that initiated this request.
                var filters = this.cloneParamsObject(fps);
                filterCriteriaParams[facetKey] = fps[facetKey];
            }
        }*/
        var fps = event.getParam("facetFilterParams");
        if (fps) {
            var keys = Object.keys(fps);
            for (var i=0; i<keys.length; i++) {
                filterCriteriaParams[keys[i]] = fps[keys[i]];
            }
        }
        component.set("v.filterCriteriaParams", filterCriteriaParams);
    },

    /************ Methods to handle facet filtering. Used from SearchFacetFilteredCheckbox */
    saveFilterCriteriaParams : function(component, event) {
        var qParams = event.getParam("filterParameters");
        var initiatingFacet = event.getParam("initiatingFacet");

        var filterCriteriaParams = component.get("v.filterCriteriaParams");
        if (filterCriteriaParams === null) {
            filterCriteriaParams = {};
        }
        var filters = this.cloneParamsObject(qParams);

        var fProps = component.get("v.facetProps"); 
        var isNested = fProps[initiatingFacet].isNested; 
        var isUserFiltered = fProps[initiatingFacet].isUserFiltered; 
 
        var selected = event.getParam("selection"); 
        if (selected !== undefined) { 
            if (isNested || isUserFiltered) { 
                var sKeys = Object.keys(selected); 
                selected = selected[sKeys[0]].join("|"); 
            }  
            //filters[event.getParam("includeKey").replace(".include", ".selected")] = selected;
			filters[event.getParam("includeKey").replace(".contains", ".selected")] = selected;  
        } 
 
        if (isUserFiltered) { 
            filters[event.getParam("initiatingFacet").replace("nested_facet_filter", "nested_local_facet_filter")]  
                        = JSON.stringify([{"User" : [component.get("v.runningUser.psId")]}]); 
        } 
 
        filterCriteriaParams[initiatingFacet] = filters;
        component.set("v.filterCriteriaParams", filterCriteriaParams);
    },

    cloneParamsObject : function(params) {
        var clone = {};
        var keys = Object.keys(params);
        for (var i=0; i<keys.length; i++) {
            clone[keys[i]] = params[keys[i]];
        }
        return clone;
    },

    addBasicFacetParams : function(qParams, component) {
        qParams["flt.group_filter"] = component.get("v.groupFilterOverride") ? 
                                        component.get("v.groupFilterOverride") : component.get("v.groupFilter");
        qParams["id"] = component.get("v.runningUser.id");
        qParams["size"] = 0;

        var autoID = component.get("v.automatchSourceId");
        if (autoID !== undefined && autoID !== null){
            qParams["type"] = component.get("v.automatchType");
            qParams["docId"] = component.get("v.automatchSourceId");
        }
        else{
            var keyword = component.get("v.keyword");
            qParams["q"] = keyword === undefined || keyword === null || keyword ===""? "-randomstring" : keyword;
        }
        
        return qParams;
    },

    filterFacet : function(component, event) { 
        // var qParams = event.getParam("filterParameters");
        // var initiatingFacet = event.getParam("initiatingFacet");
        // var bucketsPath = event.getParam("bucketsPath");
        this.callForFacet(component, event.getParam("initiatingFacet"));
		
        /*var qParams = {};
        qParams = this.addFilterCriteriaParams(qParams, component, initiatingFacet);
        qParams = this.addBasicFacetParams(qParams, component);
        // Location and other facets are to be considered when filtering a facet.
        qParams = this.addLocationParameters(qParams, component);
        qParams = this.addFacetParameters(qParams, component);
        // Need to remove filter criteria params for any facet other than the the initiating one.
        //qParams = this.removeFilterCriteriaParams(qParams, initiatingFacet, component);
        
		// Call search API and handle response
        this.callSearchForFacets(component, qParams, initiatingFacet);*/
    },

    callForFacet : function(component, initiatingFacet) {
        var qParams = {};

        qParams = this.addFilterCriteriaParams(qParams, component, initiatingFacet);
        qParams = this.addFacetOptions(qParams, component, false, initiatingFacet); 
        qParams = this.addBasicFacetParams(qParams, component);
        qParams = this.addTrackParameters(qParams, component);
        // Location and other facets are to be considered when filtering a facet.
        qParams = this.addLocationParameters(qParams, component);
        qParams = this.addFacetParameters(qParams, component);
        // Need to remove filter criteria params for any facet other than the the initiating one.
        //qParams = this.removeFilterCriteriaParams(qParams, initiatingFacet, component);
        
        // Call search API and handle response
        this.callSearchForFacets(component, qParams, initiatingFacet);
    },

    updateFacetOptions : function(component, evtParams) { 
        var facetProps = component.get("v.facetProps"); 
        if (facetProps === null) { 
            // This required? Case should never happen. 
            facetProps = {}; 
        } 
        var prop = facetProps[evtParams.facetParamKey]; 
        if (prop !== undefined && prop !== null) { 
            facetProps[evtParams.facetParamKey].isVisible = true; 
            facetProps[evtParams.facetParamKey].isCollapsed = evtParams.isCollapsed; 
        }  
        component.set("v.facetProps", facetProps); 
    }, 

    updateFacetsFromDefaultFilters : function(component) {
        var facets = component.get("v.facets");
        if (facets === undefined || facets === null) {
            facets = {};
        }

        var df = component.get("v.defaultFilters");
        var keys = Object.keys(df);
        for (var i=0; i<keys.length; i++) {
            facets[keys[i]] = df[keys[i]];
        }

        component.set("v.facets", facets);
    },
 
    requestFacet : function(component, event) {  
        // var qParams = event.getParam("filterParameters"); 
        // var initiatingFacet = event.getParam("facetParamKey"); 
        // Has to happen before the call to addFacetOptions()
        this.updateFacetOptions(component, event.getParams());
        
        // No search call if this is a collapsing event. Update facet registry and return.
        if (event.getParam("isCollapsed")) {
            return;
        }

        this.callForFacet(component, event.getParam("facetParamKey"));
    }, 

    callSearchForFacets : function(component, qParams, initiatingFacet) {
        var queryStringUrl = this.buildUrl(qParams);
        // console.log(queryStringUrl);
        //S-32364
        var autoID = component.get("v.automatchSourceId");
        if (autoID !== undefined && autoID !== null){
            var action = component.get("v.automatchType") === "talent2job" ? component.get("c.matchJobs") : component.get("c.matchTalents");
            var autoMatchRequestBody = JSON.stringify(this.getAutoMatchRequestBody (component));
            action.setParams({"queryString": queryStringUrl,
                              "httpRequestType": "POST",
                          	  "requestBody": autoMatchRequestBody});
        }
        else{
        	var action = component.get("c.search"); 
        	action.setParams({"serviceName": component.get("v.serviceName"), 
                          "queryString": queryStringUrl});    
        }
        
        action.setCallback(this, function(response) {
            this.handleFacetResponse(component, response, initiatingFacet);
        });
        $A.enqueueAction(action); 
    },

    handleFacetResponse : function(component, response, initiatingFacet) {
        if (response.getState() === "SUCCESS") {
            var value = JSON.parse(response.getReturnValue());
            // console.log(value);
            var filterResponseEvt = $A.get("e.c:E_SearchFacetFilterCompleted");
            filterResponseEvt.setParams({
                // "facets" : this.translateFacetsForFilterResponse(value.response.aggregations, bucketsPath),
                "facets" : this.translateFacets(value.response.aggregations),
                "initiatingFacet" : initiatingFacet
            });
            filterResponseEvt.fire();
        }
    },

    addFilterCriteriaParams : function(parameters, component, filterInitiator) {
        var fps = component.get("v.facetProps"); 
        var canListKey = "nested_facet_filter.myCandidateListsFacet";
        // Check for not a facet request OR specifically candidate list facet request.
        if ((filterInitiator === undefined || filterInitiator === canListKey) 
            && fps && fps[canListKey] && !fps[canListKey].isCollapsed) {
            parameters["nested_local_facet_filter.myCandidateListsFacet"] = JSON.stringify([{"User": [component.get("v.runningUser.psId")]}]);
        }

        var filtParams = component.get("v.filterCriteriaParams");

        if (filterInitiator) {
            // If filter params available for this specific facet request, add the params.
            if (filtParams && filtParams[filterInitiator]) {
                var currFacetFilter = filtParams[filterInitiator];
                var keys = Object.keys(currFacetFilter);

                for (var j=0; j<keys.length; j++) {
                    parameters[keys[j]] = currFacetFilter[keys[j]];
                }
            } else {
                // If filter params not available and is filterable, add the default filters.
                var fp = fps[filterInitiator];
    
                if (fp.isFilterable) {
                    parameters[fp.optionKey + ".contains"] = "";
                    parameters[fp.optionKey + ".size"] = fp.fetchSize;
                }
            }
        } else 
            // Not a specific facet request and prior filter params are available.
            if (filtParams) {
                // Include all available filter params for all facets ONLY if facet is open.
                var facets = Object.keys(filtParams);

                for (var i=0; i<facets.length; i++) {
                    var facet = filtParams[facets[i]];
                    if (!fps[facets[i]].isCollapsed) {
                        var facetKeys = Object.keys(facet);
                        for (var j=0; j<facetKeys.length; j++) {
                            parameters[facetKeys[j]] = facet[facetKeys[j]];
                        }
                    }
                }
        }

        return parameters;
    },
    
    /************ Methods to handle facet filtering. Used from SearchFacetFilteredCheckbox */
    
    //S-32364 Methods to handle POST autoMatchRequestBody Added by Karthik
    s4: function () {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    },

    guid: function() {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
            this.s4() + '-' + this.s4() + this.s4() + this.s4();
    },
    
    getAutoMatchRequestBody : function (component) {
        var autoMatchRequestBody = {};
        var automatch = {};
        var data = this.appendAutoMatchRequestData (component);
        automatch.data = data;
        autoMatchRequestBody.automatch = automatch
        return autoMatchRequestBody;
    },

    
     appendAutoMatchRequestData : function (component) {
        var data = {};
        var autoMatchCriteria = {};

        autoMatchCriteria.type = component.get("v.automatchType");
        autoMatchCriteria.docId = component.get("v.automatchSourceId"); 

        var matchVectors = component.get ("v.matchVectors");
        autoMatchCriteria.jobTitles = matchVectors.title_vector;
        autoMatchCriteria.skills = matchVectors.skills_vector;
        //S-34736 - To include Other_Vector in the request body Added by Karthik
        autoMatchCriteria.keywords = matchVectors.other_vector;
		autoMatchCriteria.skillFamily = matchVectors.sf_vector;
		autoMatchCriteria.sf_domain = matchVectors.sf_domain_confidence.skillfamily_domain;
		autoMatchCriteria.sf_domain_confidence = matchVectors.sf_domain_confidence.skillfamily_domain_confidence;

        data.userId = component.get("v.runningUser.id");
        data.preferredOpco = "";
        data.autoMatchCriteria = autoMatchCriteria;
		return data;
    },

    //S-32364
    
    mergeDefaultPrefs : function(component) {
        var pref = component.get("v.userSearchPrefs");
        var meta = component.get("v.prefsMeta");

        // For each preference option, set user's selection to default if not present.
        for (var i=0; i<meta.length; i++) {
            if (!pref[meta[i].Setting_Name__c]) {
                pref[meta[i].Setting_Name__c] = meta[i].Default__c;
                // console.log("Setting default for : " + meta[i].Setting_Name__c);
            }
        }
        component.set("v.userSearchPrefs", pref);
    },

    updateSelection : function(component) {
        var pref = component.get("v.userSearchPrefs");
        var meta = component.get("v.prefsMeta");

		var pUpdate = false;

        for (var i=0; i<meta.length; i++) {
            // Explicit check for type boolean since value false would fail with the first check
            if (pref[meta[i].Setting_Name__c] || typeof pref[meta[i].Setting_Name__c] === "boolean") {
                meta[i].selection = pref[meta[i].Setting_Name__c];
                // console.log("Setting selection for : " + meta[i].Setting_Name__c + " TO " + meta[i].selection);
            } else {
				// Defaulting since there is no preference for this setting.
				pref[meta[i].Setting_Name__c] = meta[i].Default__c;
				pUpdate = true;
            }
        }
        component.set("v.prefsMeta", meta);
		if (pUpdate) {
			component.set("v.userSearchPrefs", pref);
		}
    },

	firePrefsLoaded : function(component) {
		var evt = component.getEvent("prefsLoadedEvt");
		evt.setParams({
			"preferences" : component.get("v.userSearchPrefs")
		});
		evt.fire();
	}
})