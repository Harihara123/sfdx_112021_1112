({
    afterRender: function (component, helper) {
        this.superAfterRender();
        /* Require to do this here because it fails unless the component is rendered.
         * Should ideally happen only for last search (execLast = true), but that is already reset by this time
         * Not expecting any side effects of doing this always, but look out.*/
        // helper.updateLocationComponent(component, component.get("v.locationSelection"));
    }
})