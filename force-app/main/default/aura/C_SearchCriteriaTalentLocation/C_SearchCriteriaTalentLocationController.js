({
    googlePlaceSelected : function(component, event, helper) {
        // console.log(JSON.stringify(event.getParams()));
        helper.updatePlaceAttributes(component, event.getParams());
        helper.setRadius(component);
            /*helper.enableRadius(component);
        } else {
            helper.disableRadius(component);
        }*/
        // User selected a location. Do not use preset value anymore.
        component.set("v.usePreset", false);
    },

    googlePlaceCleared : function(component, event, helper) {
        // console.log("Google place cleared!");
        helper.clearPlaceAttributes(component);
        helper.disableRadius(component);
        helper.resetRadiusVals(component);
        // User cleared the location. Do not use preset value anymore.
        component.set("v.usePreset", false);
    },

    radiusChangeHandler : function(component, event, helper) {
        helper.copyRadiusVals(component);
    },

    radiusValChangeHandler : function(component, event, helper) {
        helper.updateRadiusDisplay(component);
    }

})