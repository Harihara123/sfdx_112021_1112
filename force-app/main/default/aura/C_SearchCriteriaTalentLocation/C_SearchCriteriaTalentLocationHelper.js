({
    updatePlaceAttributes : function(component, selection) {
        component.set("v.latlong", this.getLatLongString(selection.latlong));
        component.set("v.viewport", selection.viewport);
        component.set("v.placeTypes", selection.placeTypes);
        component.set("v.displayText", selection.displayText);
        component.set("v.addrComponents", selection.addrComponents);
    },

    clearPlaceAttributes : function(component) {
        component.set("v.latlong", "");
        component.set("v.viewport", {});
        component.set("v.placeTypes", []);
        component.set("v.displayText", "");
        component.set("v.addrComponents", []);
    },

    getLatLongString : function(coords) {
        return coords.lat + "," + coords.lng;
    },

    enableRadius : function (component) {
        component.set("v.useRadius", true);
    },

    disableRadius : function (component) {
        component.set("v.useRadius", false);
    },

    resetRadiusVals : function (component) {
        component.set("v.radiusVal", "-1");
        component.set("v.radiusUnitVal", "");
    },

    copyRadiusVals : function (component) {
        component.set("v.radiusVal", component.get("v.radius"));
        component.set("v.radiusUnitVal", component.get("v.radiusUnit"));
    },

    updateRadiusDisplay : function (component) {
        // Location allows radius search, and the radiusVal is a valid number, try to set the select option.
        if (component.get("v.useRadius") && component.get("v.radiusVal") > 0) {
            component.set("v.radius", component.get("v.radiusVal"));
            component.set("v.radiusUnit", component.get("v.radiusUnitVal"));
        }
    },

    updateStateCountryText : function (component) {
        var comps = component.get("v.addrComponents");
        var txt = "";
        for (var i=0; i<comps.length-1; i++) {
            txt += comps[i].long_name + ", ";
        }
        txt += comps[comps.length - 1].long_name;

        component.set("v.displayText", txt);
    },

    /** Haversine function. Code referenced from https://rosettacode.org/wiki/Haversine_formula#JavaScript 
    * Usage:  "viewportDiag" : this.calculateViewportDiagonal(viewport.southwest.lat,
    *                                                  viewport.southwest.lng,
    *                                                  viewport.northeast.lat,
    *                                                  viewport.northeast.lng)
    */
    calculateDiagonal : function(viewport) {
        var radians = Array.prototype.map.call(arguments, function(deg) { return deg/180.0 * Math.PI; });
        var lat1 = radians[0], lon1 = radians[1], lat2 = radians[2], lon2 = radians[3];
        var R = 6372.8; // km
        var dLat = lat2 - lat1;
        var dLon = lon2 - lon1;
        var a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLon / 2) * Math.sin(dLon /2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    },

    setRadius : function(component) {
        var placeTypes = component.get("v.placeTypes"); 
        /* if "alwaysUseRadius" is set to true, use lat long and radius for state / country searches as well.
         * This will work well in the case when most of the data has lat long populated. e.g. Req search.
         */
        if (placeTypes.indexOf("administrative_area_level_1") >= 0 || placeTypes.indexOf("country") >= 0) {
            if (component.get("v.alwaysUseRadius")) {
                this.disableRadius(component);
                component.set("v.radiusVal",  Math.round(this.getViewportDiagonal(component) / 2));
                component.set("v.radiusUnitVal",  "km");
            } else {
                // For state or country selection, update display text using the long_name of the address components.
                // This is done to use the same components as a text filter.
                this.updateStateCountryText(component);
                // Don't use radius for state or country
                this.disableRadius(component);
                this.resetRadiusVals(component);
                // Clear the lat long values.
                component.set("v.latlong", "");
            }
        } else {
            if (placeTypes.indexOf("colloquial_area") >= 0) {
                // Use radius for colloquial area e.g. Bay Area, Southern California, but disable the selection
                this.disableRadius(component);
                component.set("v.radiusVal",  Math.round(this.getViewportDiagonal(component) / 2));
                component.set("v.radiusUnitVal",  "km");
            } else {
                // SMARTER OPTIONS: Maybe check for size of viewport and use radius if viewport less than a threshold?
                this.enableRadius(component);
                this.copyRadiusVals(component);
            }
        }
    },

    getViewportDiagonal : function(component) {
        var viewport = component.get("v.viewport");
        var ne = viewport.northeast;
        var sw = viewport.southwest;
        return this.calculateDiagonal(sw.lat, sw.lng, ne.lat, ne.lng);
    }

})