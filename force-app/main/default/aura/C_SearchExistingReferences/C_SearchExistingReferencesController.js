({
    
    togglePopup: function(component, event, helper) {

        if( component.get("v.showPopup") ){
            helper.toggleClass(component, 'backdropAddDocument', 'slds-backdrop--');
            helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
        }else{

            helper.toggleClassInverse(component, 'backdropAddDocument','slds-backdrop--');
            helper.toggleClassInverse(component, 'modaldialogAddDocument','slds-fade-in-');

            component.set('v.isSearchFlow', false );
            component.set('v.IsSaveAndComplete', false );
            component.set("v.hideTable",true);
            component.set("v.noResults", false );
            component.set("v.searchTableData", null );
            component.set("v.selectedContactId", null );

            /* Resetting all the form fields on closing of modal */
            component.set("v.firstNameFieldValue", null );
            component.set("v.lastNameFieldValue", null );
            component.set("v.emailFieldValue", null );
            component.set("v.phoneFieldValue", null );
            
            
        }
        
    },
    
    
    closeModal: function(component, event, helper) {
        component.set("v.showPopup", false );
        component.togglePopup(component, event, helper);
        
    },
    searchOnEnter : function(component, event, helper) {
        
        if (event.getParams().keyCode === 13) {
            component.set('v.isSearchFlow', true );
            helper.startSpinner(component, event, helper);
            helper.validateSearchForm(component, event, helper);         }
    },
    
    ValidateAndCallDedupeService: function(component, event, helper) {
        component.set('v.isSearchFlow', true );
        helper.startSpinner(component, event, helper);
        helper.validateSearchForm(component, event, helper);
    },
    
    callDedupeServiceDirectly: function(component, event, helper) {
        
        var togglePopup = component.get('c.togglePopup');
        $A.enqueueAction(togglePopup);
        helper.startSpinner(component, event, helper);
        helper.callDedupeService(component);
    },
    
    searchAgain: function(component, event, helper) {
        component.set("v.hideTable",true);
        component.set("v.searchTableData", null );
        component.set("v.noResults", false );
    },
    
    
    createNewReference: function(component, event, helper) {
        component.set("v.noResults", false );
        var closeModal = component.get('c.closeModal');
        $A.enqueueAction(closeModal);
        
        var parentComponent = component.get("v.parent");
        parentComponent.openReferenceForm(
            component.get("v.firstNameFieldValue"),
            component.get("v.lastNameFieldValue"),
            component.get("v.emailFieldValue"),
            component.get("v.phoneFieldValue")
        );
        
        /*
         component.set("v.noResults", false );
        var theReferenceModal = component.find('ReferenceModal');
     	theReferenceModal.addMethod();
        */
    },
    
    selectContactRecord : function(component, event, helper) {
        
        var checkboxes = component.find("select-contact");
        var clickedElement = event.getSource().get("v.name");
        
        
        if( Array.isArray(checkboxes) ){
            
            for (var i = 0; i < checkboxes.length; i++){
                
                if( component.find("select-contact")[i].get("v.name") == clickedElement){
                    if( component.find("select-contact")[i].get("v.value") ){
                        component.set('v.selectedContactId', clickedElement );
                    }else{
                        component.set('v.selectedContactId', null );
                    }
                    continue;
                }
                
                component.find("select-contact")[i].set("v.value", false);         
            }
            
        }else{
            
            if(component.find("select-contact").get("v.value")){
                component.set('v.selectedContactId', component.find("select-contact").get("v.name") );  
            }else{
                component.set('v.selectedContactId', null );
            }
            
        }
        // alert('Selected Contact->'+ component.get("v.selectedContactId"));
        
    },
    
    // This method is used for two functionality
    // 1.When user edit existing "saved" record and click on "Save and complete",Finds dupe and then select one.
    // After user selecting dupe user click on "Use Selected Contact" button
    // 2.
    connectReference: function(component, event, helper) {
        
        helper.startSpinner(component, event, helper);
        if( !component.get("v.selectedContactId") ){
            helper.showToast("Please select one record", "Error", "Error");
            helper.stopSpinner(component);
            return;
        }
        if(component.get("v.isSearchFlow") ){
            component.set("v.shouldValidateSearch", false); 
            var childCmp = component.find("ReferenceModal")
            childCmp.editMethod("Use Selected Contact", component.get( "v.talentrecommendationId"),component.get( "v.talentContactId"),component.get( "v.selectedContactId")); 
            helper.stopSpinner(component);
            var closeModal = component.get('c.closeModal');
            $A.enqueueAction(closeModal);
        }
        else{ // comment #1 
            helper.createOrConnectReference(component, event, helper);
        }
    },
    useCurrentReference : function(component, event, helper) { 
        helper.startSpinner(component, event, helper);
        helper.searchFlowInitWrapper(component, event, helper);
    }, 
    
    /*
	This method is called when user clicks on 'Save & Complete' button. Captures and sets the values 
    passed from TalentReferenceModal.Controller.searchForExisting
    */ 
    setEventParameters : function(component, event, helper) {        
        
        helper.startSpinner(component, event, helper);
        
        if (component.get("v.talentContactId") == event.getParam("talentContactId")) {
			
			//set the values here
			component.set("v.firstNameFieldValue", event.getParam("firstNameFieldValue"));         
			component.set("v.lastNameFieldValue", event.getParam("lastNameFieldValue"));         
			component.set("v.emailFieldValue", event.getParam("emailFieldValue"));         
			component.set("v.phoneFieldValue", event.getParam("phoneFieldValue"));         
			component.set("v.IsSaveAndComplete", true);
			component.set("v.shouldValidateSearch", event.getParam("shouldValidateSearch"));
			component.set("v.mode", event.getParam("mode")); 
			component.set("v.referenceContactRecord", event.getParam("referenceContactRecord"));
			component.set("v.recommendationRecord", event.getParam("recommendationRecord"));
			component.set("v.talentrecommendationId", event.getParam("talentrecommendationId"));
			component.set("v.referenceContactId", event.getParam("referenceContactId"));
			component.set("v.talentContactId", event.getParam("talentContactId"));
			component.set("v.showPopup",event.getParam("showPopup"));
        
			//conditional statement added to avoid recurssion on 'Save & Complete' button and dupesearch
			if(component.get( "v.shouldValidateSearch")) {    
				helper.validateSearchForm(component, event, helper);
			}else{
                helper.stopSpinner(component, event, helper);
            }   
		}        
    }   
})