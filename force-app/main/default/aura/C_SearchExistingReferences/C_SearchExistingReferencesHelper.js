({
    
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    }, 
    
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    
    validateSearchForm : function(component, event, helper) {
        if(component.get( "v.isSearchFlow")){        
            var validTalent = true;
            validTalent = this.validateFields(component, validTalent, "firstName",$A.get("$Label.c.ATS_FIRST_NAME")) ;
            validTalent = this.validateFields(component, validTalent, "lastName", $A.get("$Label.c.ATS_LAST_NAME")) ;
            validTalent = this.validatePhoneOrEmail(component, validTalent) ;
            if(validTalent){
                this.callDedupeService(component, event, helper);
            }
            else{
                this.stopSpinner(component);
            }
        }
        else{
            this.callDedupeService(component, event, helper);
        }
    },
    validateFields : function(component, validTalent, componentId, fieldName) {
        var currentFieldName = component.find(componentId);
        if(currentFieldName !== undefined){
            var validateField = currentFieldName.get("v.value");
            if (!validateField || validateField === "") {
                validTalent = false;
                currentFieldName.set("v.errors", [{message: fieldName +' '+ $A.get("$Label.c.ATS_is_Required")}]);
                currentFieldName.set("v.isError",true);
                $A.util.addClass(currentFieldName,"slds-has-error show-error-message");
            } else {
                currentFieldName.set("v.errors", []);
                currentFieldName.set("v.isError", false);
                $A.util.removeClass(currentFieldName,"slds-has-error show-error-message");
            }
        }    
        return validTalent;
    },
    
    
    validatePhoneOrEmail : function(component, validTalent) {
        
        // component.get( "v.firstNameFieldValue"),
        // component.get( "v.lastNameFieldValue"),
        // component.get( "v.emailFieldValue"),
        // component.get( "v.phoneFieldValue")
        
        
        var phoneFieldName      = component.find("phone");
        var emailFieldName      = component.find("email");
        
        if(phoneFieldName !== undefined){
            if ( ( !component.get( "v.phoneFieldValue") || component.get( "v.phoneFieldValue") === "" ) &&  ( !component.get( "v.emailFieldValue") || component.get( "v.emailFieldValue") === "" )  ) {
                validTalent = false;
                phoneFieldName.set("v.errors", [{message: $A.get("$Label.c.ATS_Phone_Number_or_Email_Address_is_Required")}]);
                phoneFieldName.set("v.isError",true);
                $A.util.addClass(phoneFieldName,"slds-has-error show-error-message");
                
                emailFieldName.set("v.errors", [{message: $A.get("$Label.c.ATS_Phone_Number_or_Email_Address_is_Required")}]);
                emailFieldName.set("v.isError",true);
                $A.util.addClass(emailFieldName,"slds-has-error show-error-message");
                
                
            } else {
                phoneFieldName.set("v.errors", []);
                phoneFieldName.set("v.isError", false);
                $A.util.removeClass(phoneFieldName,"slds-has-error show-error-message");
                
                emailFieldName.set("v.errors", []);
                emailFieldName.set("v.isError", false);
                $A.util.removeClass(emailFieldName,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },
    callDedupeService : function(component, event, helper){
        
        component.set("v.shouldValidateSearch", false); 
        var action = component.get('c.callDedupeService');
        action.setParams({
            "firstName": component.get( "v.firstNameFieldValue"),
            "lastName" : component.get( "v.lastNameFieldValue"),
            "email"    : component.get( "v.emailFieldValue"),
            "phone"    : component.get( "v.phoneFieldValue")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            
            if (state === "SUCCESS" && response.getReturnValue() !== null) {
                
                component.set("v.hideTable",false);
                var responseString = response.getReturnValue();
                var parsedSearches = JSON.parse(responseString);
                component.set("v.referenceRecord",parsedSearches[0].Id);
                if(parsedSearches[0].hasOwnProperty('Message')){
                    delete parsedSearches[0];
                    parsedSearches = parsedSearches;
                }
                else{
                    var referenceContactId = component.get("v.referenceContactId");
                    for (var key in parsedSearches) {
                        if (parsedSearches.hasOwnProperty(key) && parsedSearches[key].Id === referenceContactId) {
                            parsedSearches.splice(key,1);
                            //delete parsedSearches[key];
                            parsedSearches = parsedSearches;  
                        }
                    }
                }
                
                if(parsedSearches.length < 1 || parsedSearches[0] === undefined){
                    if(component.get("v.IsSaveAndComplete")){
                        this.searchFlowInitWrapper(component, event, helper);
                    }else{
                        component.set("v.searchTableData", null );
                        component.set("v.noResults", true );  
                        this.stopSpinner(component);
                    }
                    
                }else{
                    /* TOBE REMOVED 
 					var modal = component.find("modaldialogAddDcoument");
       				//$A.util.removeClass(modal,'slds-modal_small');
        			$A.util.addClass(modal,'show');
                    $A.util.addClass(modal,'slds-modal_large');                    
            		*/
                    
                    component.set("v.searchTableData", this.sortReferenceData(parsedSearches, 'Record_Type_Name__c')); 
                    helper.showDuplicatesRecordModal(component, event, helper);

                }
                
            }else if (state === "ERROR") {
                this.handleError(response);
                this.stopSpinner(component);
            }
            
            // This delay is added only to handle a corner use case
            // when user create talent and immediately add reference & click on "Save And Complete"
            // This issue is inconsistent and delay fix the issue. 
                    
            // window.setTimeout(
            //     $A.getCallback(function() {
            //         helper.relaodDatatableEvent(component, event,helper)
            //     }), 3000
            // ); 

        }));
        $A.enqueueAction(action);
        
    },

    
    createOrConnectReference : function(component, event, helper){
        
        if( component.get( "v.recommendationRecord.Opportunity__c" ) == "" ){
            component.set( "v.recommendationRecord.Opportunity__r", null );
            component.set( "v.recommendationRecord.Opportunity__c", null );
        }
        
        var action = component.get('c.connectTalentReferences');
        action.setParams({
            "recommendationId":         component.get( "v.talentrecommendationId"),
            "talentId" :                component.get( "v.talentContactId"),
            "selectedrecommendation"  : component.get( "v.recommendationRecord"),
            "referenceFrom" : component.get( "v.selectedContactId"),
            "referenceContact"  : null, // we are passing null as we are not updating anything in this perticular use case.
            "buttonClicked" : "SaveAndComplete"
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var res = "Refresh Activity";
                var evt = $A.get("e.c:E_TalentActivityUpdateEvent");
                evt.setParams({ "result": event.getSource().getLocalId()});
                evt.fire(); 
                var evt2 = $A.get("e.c:E_TalentActivityReload");
                evt2.setParams({ "result": event.getSource().getLocalId()});
                evt2.fire(); 
                this.relaodDatatableEvent(component, event,helper); 
                
                component.set("v.searchTableData", null );
                component.set("v.noResults", false );
                component.set("v.hideTable", true );
                
                component.set("v.selectedContactId", null );
                // component.set("v.talentContactId", null );
                component.set("v.talentrecommendationId", null );
                
                component.set("v.lastNameFieldValue", null );
                component.set("v.firstNameFieldValue", null );
                
                component.set("v.emailFieldValue", null );
                component.set("v.phoneFieldValue", null );
                
                
                var closeModal = component.get('c.closeModal');
                $A.enqueueAction(closeModal);
                
                
                // if(buttonClicked == "SaveAndComplete"){
                //     var res = "Refresh Activity";
                //     var evt = $A.get("e.c:E_TalentActivityUpdateEvent");
                //     evt.setParams({ "result": event.getSource().getLocalId()});
                //     evt.fire(); 
                //     var evt2 = $A.get("e.c:E_TalentActivityReload");
                //     evt2.setParams({ "result": event.getSource().getLocalId()});
                //     evt2.fire(); 
                // }
                this.stopSpinner(component);
                this.showToast("Reference has been saved", "success", "Success");
                
            }else if (state === "ERROR") {
                this.handleError(response);
                this.stopSpinner(component);
            }
            
            
            
        }));
        
        
        $A.enqueueAction(action);
    },
    
    startSpinner: function(component, event, helper){
        component.set("v.Spinner", true);
        var spinner = component.find('spinnerId');
        $A.util.addClass(spinner, 'slds-show');   
    },
    
    stopSpinner: function(component ){
        var spinner = component.find('spinnerId');
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');
        component.set("v.Spinner", false);   
    },

    showToast2 : function(component, event, helper, message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
        this.relaodDatatableEvent(component, event,helper);
    },

    showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },

    togglePopup: function(component, event, helper) {
        
        if( component.get("v.showPopup") ){
            helper.toggleClass(component, 'backdropAddDocument', 'slds-backdrop--');
            helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
        }else{
            helper.toggleClassInverse(component, 'backdropAddDocument','slds-backdrop--');
            helper.toggleClassInverse(component, 'modaldialogAddDocument','slds-fade-in-');
            
        }
        
    },
    initWrapper : function(component, event, helper){
        
        var action = component.get('c.initWrapper');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.referenceRecord', response.getReturnValue());
                this.saveMyReference(component, event, helper, "SaveAndComplete");
            }else if (state === "ERROR") {
                this.stopSpinner(component);
                this.handleError(response);
            }
        });
        $A.enqueueAction(action);
    },

    searchFlowInitWrapper : function(component,event, helper){
        console.log( 'talentContactId-->' + component.get("v.talentContactId") );
        
        var action = component.get('c.generateReferenceType');
        action.setParams( {recordId: component.get("v.talentContactId") } );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.referenceRecord', response.getReturnValue());
                this.saveMyReference(component, event, helper, "SaveAndComplete");
                //TOBE REMOVED
                //  helper.editModeAccessSetter(component, event, helper, reference.contact, component.get("v.mode"));
            }else if (state === "ERROR") {
                this.stopSpinner(component);
                this.handleError(response);
            }

        });
        $A.enqueueAction(action);
        
    },
    saveMyReference : function(component, event, helper, buttonClicked){
        
        var referenceWrap = component.get("v.referenceRecord");
        // console.log("dsadasdasd");
        // component.get("v.recommendationRecord").Opportunity__c = null;
        // component.get("v.recommendationRecord").Opportunity__r = null;
        
        
        if( $A.util.isUndefined(component.get( "v.recommendationRecord.Opportunity__c" ) ) || 
           $A.util.isEmpty(component.get( "v.recommendationRecord.Opportunity__c" ) ) ){
            
            var rec = component.get("v.recommendationRecord");
            rec.Opportunity__c = null ;
            rec.Opportunity__r = null ;
            component.set("v.recommendationRecord", rec );
            
        }else{
            var rec = component.get("v.recommendationRecord");
            rec.Opportunity__c = component.get("v.recommendationRecord.Opportunity__c.Id") ;
            component.set("v.recommendationRecord", rec );
        }
        
        // referenceWrap.recommendation.Opportunity__c = component.find("opportunityId").get("v.value");
        
        var recordId = component.get("v.talentContactId");
        
        var shouldInsert = true;
        if(component.get("v.mode") != "Add")
            shouldInsert = false;
        var serverMethod = 'c.saveTalentReference';	
        var params = {
            "contact": component.get("v.referenceContactRecord"),
            "Recommendation" : component.get("v.recommendationRecord"),
            "talentContactId" : recordId,
            "shouldInsert" : shouldInsert,
            "mode" : component.get("v.mode"),
            "action" : buttonClicked
        };
        
        var action = component.get(serverMethod);
        action.setParams(params);
        
        action.setCallback(this,function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") { 
                var reference = response.getReturnValue();
                var parentComponent = component.get("v.parent");
                if(component.get("v.recommendation") === null) // temp logic remove it later	
                    parentComponent.reloadDatatable();
                if(buttonClicked == "SaveAndComplete"){
                    var res = "Refresh Activity";
                    var evt = $A.get("e.c:E_TalentActivityUpdateEvent");
                    evt.setParams({ "result": event.getSource().getLocalId()});
                    evt.fire(); 
                    var evt2 = $A.get("e.c:E_TalentActivityReload");
                    evt2.setParams({ "result": event.getSource().getLocalId()});
                    evt2.fire(); 
                }
                
                this.showToast2(component, event, helper, "Record has been saved", "success", "Success");
                //this.stopSpinner(component);
                //this.relaodDatatableEvent(component, event,helper);
            } else if (state === "ERROR") {
                this.stopSpinner(component);
                this.handleError(response);
            }
        });
        
        $A.enqueueAction(action);  
        //this.hideSearchExistingForms(component, event,helper);
    }, 

    showDuplicatesRecordModal: function(component, event, helper) {
        component.set("v.showPopup", true);
        component.set("v.hideTable",false);
        component.set("v.noResults", false );
        helper.togglePopup(component, event, helper);
        this.stopSpinner(component);
    },
   
    // hideDuplicatesRecordModal: function(component, event, helper) {
    //     component.set("v.showPopup", false);
    //     component.set("v.hideTable",false);
    //     component.set("v.noResults", false );
    //     helper.togglePopup(component, event, helper);
    // },
    
    relaodDatatableEvent : function(component, event,helper) {
        
        // this.hideDuplicatesRecordModal(component, event,helper);

        var closeModal = component.get('c.closeModal');
        $A.enqueueAction(closeModal);

        var cmpEvent = component.getEvent("reloadDataTable");
        cmpEvent.setParams({
            "recordId" : component.get( "v.talentContactId")
        });
        cmpEvent.fire();
        this.stopSpinner(component);
    },
    
    sortReferenceData : function( referenceData , sortByKey) {
        
        var sortedReferences =  referenceData.sort(function(a, b) {
            var x = a[sortByKey]; var y = b[sortByKey];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
        
        /* Return in decending order Talent -> Reference -> Client  */
        return sortedReferences.reverse();
        
    }, 
    
    handleError : function(response){
        
        // Use error callback if available
        if (typeof errCallback !== "undefined") {
            errCallback.call(this, response.getReturnValue());
            // return;
        }
        // Fall back to generic error handler
        var errors = response.getError();
        if (errors) {
            
            if (errors[0] && errors[0].message) {
                this.showError(errors[0].message, "An error occured!");
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
            }else{
                this.showError('An unexpected error has occured. Please try again!');
            }
        } else {
            this.showError(errors[0].message, "An error occured!");
            //throw new Error("Unknown Error");
        }
    },            
    
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    
    
    
})