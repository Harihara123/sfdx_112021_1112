({
	doInit : function(component, event, helper) {
        // console.log("Init facet --- " + component.get("v.facetParamKey"));
        helper.fireRegisterFacetEvt(component);

        if (component.get("v.initialFilter")) {
            var iniFilt = JSON.parse(component.get("v.initialFilter"));
            var filtKeys = Object.keys(iniFilt);
            // No array length check. Assuming that the initialFilter is set correctly and has the facet options.
            component.set("v.iniFiltMarker", true);
            component.set("v.presetState", iniFilt[filtKeys[0]]);
        } 
        if (component.get("v.relatedFacets") !== undefined) {
            helper.initRelatedFacetState(component);
        }
    },

    toggleFacet : function(component, event, helper) {
		var isCollapsed = component.get("v.isCollapsed");
        component.set("v.isCollapsed", !isCollapsed);
        // Request search for facets. "isCollapsed" is part of the request params, so nothing returns if collapsing.
        helper.fireFacetRequestEvt(component);
	},

    presetStateChanged : function(component, event, helper) {
        // Make the facet visible and expanded if not a part of the default filters
        if (!component.get("v.iniFiltMarker")) {
            component.set("v.isVisible", true);
            component.set("v.isCollapsed", false);
        }
        // Fire event for facet state change. 
        helper.fireFacetStateChangedEvt(component);
    },

    pillClosed : function(component, event, helper) {
        var concreteComponent = component.getConcreteComponent();
        concreteComponent.getDef().getHelper().removeFacetOption(concreteComponent, event.getParam("pillId"));
    },

    clearAllSelectedFacets : function(component, event, helper) {
        if (component.get("v.relatedFacets") !== undefined) {
            helper.initRelatedFacetState(component);
        }
        var concreteComponent = component.getConcreteComponent();
        concreteComponent.getDef().getHelper().clearAllSelectedFacets(concreteComponent);
    },

    /*handleReexecuteLast : function(component, event, helper) {
        var facets = event.getParams().facets;
        helper.setPresetState(component, facets);
    },*/

	presetFacet: function(component, event, helper) {
        var parameters = event.getParam("arguments");
		if (parameters) {
			helper.setPresetState(component, parameters.facets);
		}
    },
    
	/*handleSearchCompleted : function(component, event, helper) {
        if (!component.get("v.isVisible")) {
            component.set("v.isVisible", true);
        }
        //var cs = component.get("v.currentState");
        //if (component.get("v.iniFiltMarker") && cs && cs.length > 0) {
        //    component.set("v.isCollapsed", false);
        //}
    },

    
	handleFacetsApplied : function(component, event, helper) {
        if (component.get("v.relatedFacets") !== undefined) {
            helper.updateRelatedFacetState(component, event.getParams());
        }
    }, */

	updateRelatedFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (component.get("v.relatedFacets") !== undefined && parameters) {
            helper.updateRelatedFacetState(component, parameters);
        }
    },

	dpMapChange : function(component, event, helper) {
		var dpMap = component.get("v.dpMap");
		if (dpMap) {
			component.set("v.displayProps", dpMap[component.get("v.facetParamKey")]);
		}
	}

})