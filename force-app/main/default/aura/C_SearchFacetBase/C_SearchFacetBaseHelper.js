({
	fireRegisterFacetEvt : function(component) {
        // var registerFacetEvt = $A.get("e.c:E_SearchFacetRegister");
        var registerFacetEvt = component.getEvent("searchFacetRegisterEvt");
        /*var fSize = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ?
        				component.get("v.fetchSize") * 4 : component.get("v.fetchSize");*/
        var iniFilt;
        if (component.get("v.initialFilter")) {
        	iniFilt = JSON.parse(component.get("v.initialFilter"));
        } 

        registerFacetEvt.setParams({
        	"facetParamKey" : component.get("v.facetParamKey"),
            "allowNoKeywordSearch" : component.get("v.allowNoKeywordSearch"),
            "isVisible" : component.get("v.isVisible"),
            "isCollapsed" : component.get("v.isCollapsed"),
            "optionKey" : component.get("v.optionKey"),
            "sizeKey" : component.get("v.sizeKey"),
			"fetchSize" : component.get("v.fetchSize"),
            // "fetchSize" : fSize,
            "initialFilter" : iniFilt,
            "isFilterable" : component.get("v.isFilterableFacet"),
            "isNested" : component.get("v.relatedFacets") !== undefined ? true : false,
            "isUserFiltered" : component.get("v.isUserFiltered")
        });
        registerFacetEvt.fire();
	},

	fireFacetRequestEvt : function(component) {
		var facetEvt = component.getEvent("searchFacetRequestEvt");
		facetEvt.setParams({
			// "filterParameters" : {},
			"facetParamKey" : component.get("v.facetParamKey"),
            "isCollapsed" : component.get("v.isCollapsed")
		});
        facetEvt.fire();
	},

	fireFacetStateChangedEvt : function(component) {
		var facetEvt = component.getEvent("searchFacetStateChangedEvt");
		facetEvt.setParams({
			"facetParamKey" : component.get("v.facetParamKey"),
            "isCollapsed" : component.get("v.isCollapsed"),
            "isVisible" : component.get("v.isVisible")
		});
        facetEvt.fire();
	},

    fireFacetChangedEvt : function(component) {
        var facetChangedEvent = component.getEvent("searchFacetChanged");

        var selected = this.translateFacetParam(component);
        var applied;
        if (component.get("v.isUserFiltered")) {
        	applied = this.addLocalUserFacetFilter(component, selected);
        } else {
        	applied = this.createNestedFacetFilterParam(component, selected);
        }
        facetChangedEvent.setParams({
            "selectedFacet" : selected, 
            "appliedFacets" : applied,
            "nestedFilters" : component.get("v.hasNestedFilter") ? this.createNestedFilterParam(component, selected) : null,
            "selectedPills" : this.getFacetPillData(component),
            // "hasNestedFilter" : component.get("v.hasNestedFilter"),
            "hasNestedFilter" : component.get("v.relatedFacets") !== undefined ? true : false,
            "facetParamKey" : component.get("v.facetParamKey"),
            // "filterParamKey" : component.get("v.filterParamKey"),
            "overrideKey" : component.get("v.key"),
            "sizeKey" : component.get("v.sizeKey"),
            "size" : component.get("v.fetchSize"),
            "isFilterableFacet" : component.get("v.isFilterableFacet"),
            "isUserFiltered" : component.get("v.isUserFiltered"),
			"displayProps" : component.get("v.displayProps")
        });

        if (component.get("v.isFilterableFacet")) {
        	facetChangedEvent.setParam("includeKey", component.get("v.includeKey"));
        	var ftxt = component.get("v.filterText") ? component.get("v.filterText") : "";
       		//facetChangedEvent.setParam("filterTerm", ".*" + ftxt.toLowerCase() + ".*"); //Commented out for Search Optimization
       		facetChangedEvent.setParam("filterTerm", ftxt.toLowerCase()); //S-78090 - Added by Karthik
        }

        facetChangedEvent.fire();
    },

    fireFacetPresetEvt : function(component) { 
        var facetPresetEvent = component.getEvent("searchFacetPreset"); 
        // var facets = this.translateFacetParam(component); 
        var selected = this.translateFacetParam(component);
        var applied;
        if (component.get("v.isUserFiltered")) {
        	applied = this.addLocalUserFacetFilter(component, selected);
        } else {
        	applied = this.createNestedFacetFilterParam(component, selected);
        }

        facetPresetEvent.setParams({ 
            "selectedFacet" : selected, 
            "appliedFacets" : applied,
            "nestedFilters" : component.get("v.hasNestedFilter") ? this.createNestedFilterParam(component, selected) : null,
            "selectedPills" : this.getFacetPillData(component),
            "hasNestedFilter" : component.get("v.relatedFacets") !== undefined ? true : false,
            "facetParamKey" : component.get("v.facetParamKey"),
            // "filterParamKey" : component.get("v.filterParamKey"),
            "overrideKey" : component.get("v.key"),
            "sizeKey" : component.get("v.sizeKey"),
            "size" : component.get("v.fetchSize"),
            "isFilterableFacet" : component.get("v.isFilterableFacet"),
            "isUserFiltered" : component.get("v.isUserFiltered")
        }); 
        facetPresetEvent.fire(); 
    }, 
 
	getPillId : function(component, option) {
		var pillIdMap = component.get("v.pillIdMap");
		if (pillIdMap === null) {
			pillIdMap = {};
		}

		var randomId = pillIdMap[option];
		if (randomId === undefined) {
			randomId = Math.floor(Math.random() * 10000000000);
			pillIdMap[option] = randomId;
		}
		
		component.set("v.pillIdMap", pillIdMap);
		return randomId;
	},

	setPresetState : function(component, facets) {
		if (facets !== null) {
	        if (component.get("v.key") !== undefined) {
	        	// This is a nested facet.
	        	if (facets[component.get("v.key")] !== undefined && facets[component.get("v.key")] !== "") {
	        		var fullFacet = facets[component.get("v.key")];
	        		for (var i=0; i<fullFacet.length; i++) {
	        			// Match the object key to the nestedKey for the right one.
	        			var key = Object.keys(fullFacet[i])[0];
	        			if (key === component.get("v.nestedKey")) {
	        				component.set("v.presetState", fullFacet[i]);
	        			}
	        		}
	        	}
	        } else {
	        	// Not nested
	        	if (facets[component.get("v.facetParamKey")] !== undefined && facets[component.get("v.facetParamKey")] !== "") {
	        		component.set("v.presetState", facets[component.get("v.facetParamKey")]);
	        	} /*else {
					// Add conditions in this block for facets that have initialFilter applied. 
					// Preset state needs to be overwritten to empty/null to make sure the default filters do not apply.
					if (component.get("v.facetParamKey") === "facetFlt.req_stage") {
						component.set("v.presetState", "");
					}
	        	}*/
	        }
		}
	},

    /**
     * Method required since the search aggregation data is hidden in a sub structure under the main buckets.
     * Required because of the peculiar response structure in the nested degree and org aggregations.
     */
    postFilterFacetData : function(facetData, path, size) {
    	var pArr = path.split(".");
        var cs = [];

        for (var i=0; i<facetData.length; i++) {
            var key = facetData[i].key;
            var cnt = this.resolveCount(facetData[i], pArr);

            if (cnt > 0) {
                cs.push({
                    "key" : key,
                    "doc_count" : cnt
                });
            }
        }
        // Sort facets by count since search returned sorted on the outer bucket count (which is different)
        // Also cut array down to expected size.
        return cs.sort(function(a, b) {
            return b.doc_count - a.doc_count;
        }).slice(0, size);
    },

    resolveCount : function(obj, path) {
    	var count = obj;
    	for (var i=0; i<path.length; i++) {
    		count = count[path[i]];
    	}
    	return count;
    },

	removeFacetOption : function(component, pillId) {
		return;
	},

	clearAllSelectedFacets : function(component) {
		return;
	},

	initRelatedFacetState : function(component) {
		var rf = component.get("v.relatedFacets").split(",");
		var state = {};
		for (var i=0; i<rf.length; i++) {
			state[rf[i].trim()] = {
				"nf" : null,
				"nff" : null 
			};
		}
		component.set("v.relatedFacetState", state);
	},

	updateRelatedFacetState : function(component, evtParams) {
		var state = component.get("v.relatedFacetState");
		// var nfFiltState = component.get("v.nestedFacetFilterState");
		if (state[evtParams.initiatingFacet] !== undefined) {
			state[evtParams.initiatingFacet].nff = evtParams.selectedFacet;
			if (evtParams.hasNestedFilter) {
				state[evtParams.initiatingFacet].nf = evtParams.selectedFacet;
			}
		}

		component.set("v.relatedFacetState", state);
	},

	createNestedFacetFilterParam : function(component, myFacetParams) {
		return this.createNestedParams(component, myFacetParams, "nff");
	},

	createNestedFilterParam : function(component, myFacetParams) {
		return this.createNestedParams(component, myFacetParams, "nf");
	},

	createNestedParams : function(component, myFacetParams, nfOrNff) {
		var prm = myFacetParams;
		if (component.get("v.relatedFacets") !== undefined) {
			var arr = myFacetParams === null ? [] : [myFacetParams];
			var relatedState = component.get("v.relatedFacetState");
			var rs = Object.keys(relatedState);
			for (var i=0; i<rs.length; i++) {
				var rel = relatedState[rs[i]];
				if (rel[nfOrNff] !== null) {
					arr.push(rel[nfOrNff]);
				}
			}
			if (arr.length === 0) {
				prm = null;
			} else if (arr.length === 1) {
				prm = arr;
			} else {
				var ord = component.get("v.nestedKeysOrder");
				var spl = (ord !== undefined && ord !== "") ? ord.split(",") : [component.get("v.nestedKey")];
				prm = this.getOrderedNestedArray(arr, spl);
			}
		}

		return prm;
	},

	getOrderedNestedArray : function(facets, keys) {
		var pMap = {};
		var ordArray = [];
		for (var i=0; i<facets.length; i++) {
			var key = Object.keys(facets[i]);
			pMap[key] = facets[i];
		}
		for (var i=0; i<keys.length; i++) {
			if (pMap[keys[i]]) {
				ordArray.push(pMap[keys[i]]);
			}
		}
		return ordArray;
	},

	addLocalUserFacetFilter : function(component, myFacetParams) {
		if (myFacetParams === null) {
			// Nothing selected. return null
			return null;
		}
		// All other cases, add user filter and selected params as objects to array.
		var ordArray = [];
		ordArray.push({
			"User" : [component.get("v.runningUser.psId").toLowerCase()]
		})

		ordArray.push(myFacetParams);
		return ordArray;
	}

})