({
	setCurrentStateFromOptions : function (component, currentState, options) {
        var checkboxes = JSON.parse(options);
        component.set("v.currentState", 
            this.mergeIncomingToCurrentState(component, checkboxes, currentState));
    },

    updateFacetMetadata : function(component) {
        
        var incomingFacetData = component.get("v.incomingFacetData");
        var currentState = component.get("v.currentState");
        var options = component.get("v.options");
        
        if (options !== undefined && options !== null) {
            // Filters which don't return in the aggregations should have options set.
            this.setCurrentStateFromOptions(component, currentState, options);
            // Set isVisible to true AFTER current state is set and before method returns.
            // component.set("v.isVisible", true);
            return;
        }

        if (incomingFacetData === undefined || incomingFacetData === null) {
            /*// Error condition - when search fails, nothing in the incoming facets.
            // Maybe also check for empty string?
            return;*/
            // Possibly not error condition. If there are no facets, then this could be undefined. KEEP CHECKING!
            incomingFacetData = [];
        }

        if (currentState === undefined || currentState == null) {
            // currentState = incomingFacetData;
            /*currentState = component.get("v.facetParamKey") === "nested_facet_filter.degreeTypeFacet" ? 
                            this.postFilterFacetData(incomingFacetData, "YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingFacetData;*/
			currentState = incomingFacetData;
            for (var i=0; i<currentState.length; i++) {
                currentState[i].checked = false;
                currentState[i].show = true;
                currentState[i].pillId = component.find("facetHelper").getPillId(currentState[i].key);
				if (component.get("v.usePersonCount") === true) {
					currentState[i].doc_count = currentState[i].personCount.doc_count;
				}
            }
            component.set("v.currentState", currentState);
        } else {
            /*var filteredData = component.get("v.facetParamKey") === "nested_facet_filter.degreeTypeFacet" ? 
                                        this.postFilterFacetData(incomingFacetData, "YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingFacetData;*/
            // Merge here
            component.set("v.currentState", 
                this.mergeIncomingToCurrentState(component, incomingFacetData, currentState));
        }

        // Undefined check required in case nothing returns for this facet. 
        // Additionally, this has to happen after the merge process.
        if (incomingFacetData !== undefined) {
            if (incomingFacetData.length > 10 && component.get("v.isExpanded") === "false"){
                component.set("v.showMoreLessLabel", "show more");
            } 
            if (incomingFacetData.length > 10 && component.get("v.isExpanded") === "true"){
                component.set("v.showMoreLessLabel", "show less");
            }
        }

        // Set isVisible to true AFTER current state is set.
        // component.set("v.isVisible", true);
	},

    mergeIncomingToCurrentState : function (component, incomingFacetData, currentState) {
        var newMap = {};

        // Start new state with checked options from the old current state 
        for (var i=0; i<currentState.length; i++) {
            if (currentState[i].checked === true) {
                newMap[currentState[i].key] = currentState[i];
            }
        }

        var incomingMap = {};
        var displayOverrides = JSON.parse(component.get("v.displayOverrides"));

        for (var i=0; i<incomingFacetData.length; i++) {
            incomingMap[incomingFacetData[i].key] = true;
            // Check if the incoming facet was checked in the old current state
            var option = newMap[incomingFacetData[i].key];
			var allSub = displayOverrides["*"];
            var incomingDisplayOverrides = displayOverrides[incomingFacetData[i].key] === undefined ? (allSub ? allSub : incomingFacetData[i].key) : displayOverrides[incomingFacetData[i].key];
            if (option !== undefined) {
               
                // Option was in the old current state, update with count from incoming facets
                option.doc_count = component.get("v.usePersonCount") === true ? incomingFacetData[i].personCount.doc_count : incomingFacetData[i].doc_count;
                // If label overides exist, apply them
                option.label = incomingDisplayOverrides;
            } else {
                // Option was not in the state. Add now as unchecked.
                newMap[incomingFacetData[i].key] = {
                    key : incomingFacetData[i].key,
                    label : incomingDisplayOverrides,
                    show : true,
                    checked : false,
                    pillId : component.find("facetHelper").getPillId(incomingFacetData[i].key),
                    doc_count : component.get("v.usePersonCount") === true ? incomingFacetData[i].personCount.doc_count : incomingFacetData[i].doc_count
                };
            }
        }

        var newCurrentState = [];
        var keys = Object.keys(newMap);
        // Loop over the newly generated state
        for (var i=0; i<keys.length; i++) {
            var option = newMap[keys[i]];
            // If coming from old state, but not in new incoming state, set count to 0.
            if (incomingMap[keys[i]] === undefined) {
                option.doc_count = 0;
            }
            newCurrentState.push(option);
        }

        return newCurrentState;
    },

    groupCurrentState : function(component) {
        var groupDef = JSON.parse(component.get("v.groupDef"));
        var cs = component.get("v.currentState");
        var groups = {};
        var counts = {};

        for (var i=0; i<cs.length; i++) {
            var group = groupDef[cs[i].key];
            if (groups[group] === undefined) {
                groups[group] = [];
                counts[group] = 0;
            }
            groups[group].push(cs[i]);
            counts[group] += cs[i].doc_count;
        }

        var grpArray = [];
        var grps = Object.keys(groups);
        for (var i=0; i<grps.length; i++) {
            grpArray.push({
                "key" : grps[i],
                "list" : groups[grps[i]],
                "count" : counts[grps[i]]
            })
        }
        
        component.set("v.groupedState", grpArray);
    },

    translateFacetParam : function (component) {
        var outgoingFacetData = {};
        var facets = component.get("v.currentState");

        if (facets && facets !== null) {
            var sel = [];
            for (var i=0; i<facets.length; i++) {
                if (facets[i].checked === true) {
                    sel.push(facets[i].key);
                }
            }

            var selObj = {};
            switch(component.get("v.facetParamKey")) {
                case "nested_facet_filter.submittalStatusFacet" :
                    selObj["submittalStatusCounts"] = sel;
                    outgoingFacetData = sel.length > 0 ? [selObj] : null;
                    break;

                case "nested_facet_filter.certFacet" :
                    // Fall through
                case "nested_facet_filter.degreeTypeFacet" :
                    // selObj["DegreeType"] = sel;
                    if (sel.length > 0) {
                        selObj[component.get("v.nestedKey")] = sel;
                        outgoingFacetData = selObj;
                    } else {
                        outgoingFacetData = null;
                    }
                    break;

                case "flt.national_op" :
				//Recruiter Activity Facet 
				case "flt.recruiter_activity" :
                //S-85135 added by Chaithra
            	case "flt.work_remote" :
                case "flt.can_use_approved_sub_vendor" :
                    // Only one option "Yes" - if not selected, return null
                    outgoingFacetData = sel.length > 0 ? sel[0] : null;
                    break;
                    
                case "facetFlt.languages_spoken" :
                case "facetFlt.req_stage" :
                case "facetFlt.candidate_status" :
                case "facetFlt.interaction":
                //Added for S-32461    
                case "facetFlt.security_clearance":

                    
                default :
                    var delim = component.get("v.delimiter");
                    selObj = sel.join(delim);
                    outgoingFacetData = selObj;
                    break;
            }
            
        }

        return outgoingFacetData;
    },

	updateCurrentState: function(component, currentState, key, displayOverrides, allSub) { 
		for (var i=0; i<currentState.length; i++) {
			if (currentState[i].key === key) {
				currentState[i].checked = true;
				return currentState;
			}
		}
        currentState.push(this.newEntry(component, key, displayOverrides, allSub));
		return currentState;
    },

    presetCurrentState : function(component) {
        // var currentState = []; 
		var currentState = component.get("v.currentState"); 
		if (!currentState) {
			currentState = [];
		}
        var presetState = component.get("v.presetState");
        var displayOverrides = JSON.parse(component.get("v.displayOverrides"));
		var allSub = displayOverrides["*"];

        switch(component.get("v.facetParamKey")) {
            case "nested_facet_filter.submittalStatusFacet" :
                if (presetState.length > 0) {
                    var statii = presetState[0].submittalStatusCounts;
                    for (var i=0; i<statii.length; i++) {
                        // currentState.push(this.newEntry(component, statii[i], displayOverrides, allSub));
						currentState = this.updateCurrentState(component, currentState, statii[i], displayOverrides, allSub);
                    }
                }
                break;

            case "nested_facet_filter.degreeTypeFacet" :
            case "nested_facet_filter.certFacet" :
                var selections = presetState[component.get("v.nestedKey")];
                for (var i=0; i<selections.length; i++) {
                    // currentState.push(this.newEntry(component, selections[i], displayOverrides, allSub));
					currentState = this.updateCurrentState(component, currentState, selections[i], displayOverrides, allSub);
                }
                break;

            case "flt.national_op" :
			//Recruiter Activity Facet 
			case "flt.recruiter_activity" :
            case "flt.work_remote" :
            case "flt.can_use_approved_sub_vendor" :
                if (presetState !== null && presetState !== "") {
                    var options = JSON.parse(component.get("v.options"));
                    // currentState.push(this.newEntry(component, options[0].key, displayOverrides, allSub));
					currentState = this.updateCurrentState(component, currentState, options[0].key, displayOverrides, allSub);
                }
                break;

            case "facetFlt.sov_months_experience":
                if (presetState !== "") {
                   
                    var langs = presetState.split(component.get("v.delimiter"));
                   
                    for (var i=0; i<langs.length; i++) {
                        if(langs[i] === "missing"){
                            // currentState.push(this.newEntry(component, "missing|0", displayOverrides, allSub));
							currentState = this.updateCurrentState(component, currentState, "missing|0", displayOverrides, allSub);
                        } else if(langs[i] === "0"){
                            continue;
                        } else {
                            // currentState.push(this.newEntry(component, langs[i], displayOverrides, allSub));
							currentState = this.updateCurrentState(component, currentState, langs[i], displayOverrides, allSub);
                        }
                    }
                }
                break;

            case "facetFlt.languages_spoken" :
            case "facetFlt.req_stage" :
            case "facetFlt.interaction":
            case "facetFlt.sov_months_experience":
            //Added for S-32461    
			case "facetFlt.security_clearance":



            default :
				if (presetState !== "") {
					var langs = presetState.split(component.get("v.delimiter"));
					for (var i=0; i<langs.length; i++) {
						// currentState.push(this.newEntry(component, langs[i], displayOverrides, allSub));
						currentState = this.updateCurrentState(component, currentState, langs[i], displayOverrides, allSub);
					}
				}
                break;
        }

        //console.log("||||||||| " + JSON.stringify(currentState));
        component.set("v.currentState", currentState);
        // this.fireFacetChangedEvt(component);
    },

    newEntry : function(component, key, displayOverrides, allSub) {
        return {
            "key" : key,
            "label" : displayOverrides[key] !== undefined ? displayOverrides[key] : (allSub ? allSub : key),
            "show" : true,
            "checked" : true,
            "pillId" : component.find("facetHelper").getPillId(key),
            "doc_count" : 0
        };
    },

    getFacetPillData : function(component) {
        var outgoingPillData = [];
        var facets = component.get("v.currentState");
        var displayOverrides = JSON.parse(component.get("v.displayOverrides"));
		var allSub = displayOverrides["*"];

        if (facets && facets !== null) {
            for (var i=0; i<facets.length; i++) {
                if (facets[i].checked === true) {
                    var pillData = {};
                    switch(component.get("v.facetParamKey")) {
                        case "flt.work_remote" :
                        case "flt.can_use_approved_sub_vendor" :
                        case "flt.national_op" :
						case "flt.proven_track_record_label" :
						//Recruiter Activity Facet 
						case "flt.recruiter_activity" :
                        case "facetFlt.placement_type" :
                            // Only one option "Yes" - if not selected, return null
                            pillData.id = facets[i].pillId;
                            pillData.label = component.get("v.pillPrefix") + ": " + facets[i].label;
                            break;
                            
                        /*case "flt.remote_work" :
                            // Only one option "Yes" - if not selected, return null
                            pillData.id = facets[i].pillId;
                            pillData.label = component.get("v.pillPrefix") + ": " + facets[i].label;
                            break;*/
                        case "facetFlt.sov_months_experience" :
						case "facetFlt.pipelines" :
                        case "facetFlt.opco_name" :
                            pillData.id = facets[i].pillId;
                            pillData.label = displayOverrides[facets[i].key] !== undefined ? displayOverrides[facets[i].key] : (allSub ? allSub : facets[i].key);
                            break;
                        //Added for S-32461   
                        case "facetFlt.security_clearance": 
                        
                        default :
                            pillData.id = facets[i].pillId;
                            pillData.label = facets[i].label;
                            break;
                    }
                    outgoingPillData.push(pillData);
                }
            }
        }

        return outgoingPillData;
    },

    filterCheckboxes : function(component) {
        var currentState = component.get("v.currentState");
        var filter = component.get("v.filterText");

        for (var i=0; i<currentState.length; i++) {
            currentState[i].show = currentState[i].key.match(/filter/g) === null ? false : true;
        }
        component.set("v.currentState", currentState);
    },
    
    toggleCheckboxShowHide: function(component){
        var toggleText = component.find("hiddenCheckbox");
        if(component.get("v.showMoreLessLabel")=== "show more"){
            component.set("v.showMoreLessLabel", "show less");
            component.set("v.isExpanded", "true");
			 $A.util.removeClass(toggleText, "checkboxHide");            
        	 $A.util.addClass(toggleText, "checkboxShow");
        }else{
			component.set("v.showMoreLessLabel", "show more");
            component.set("v.isExpanded", "false");
             $A.util.removeClass(toggleText, "checkboxShow");            
        	 $A.util.addClass(toggleText, "checkboxHide");
        }
    },

    removeFacetOption : function(component, pillId) {
        var pillMatch = false;

        var state = component.get("v.currentState");
        if (state === undefined) {
            return;
        }

        for (var i=0; i<state.length; i++) {
            if (state[i].pillId === pillId) {
                state[i].checked = false;
                pillMatch = true;
                break;
            }
        }

        if (pillMatch) {
            component.set("v.currentState", state);
            /*if (component.get("v.groupDef") !== undefined) {
                this.groupCurrentState(component);
            }*/
            component.find("facetHelper").fireFacetChangedEvt(component);
        }

    },

    clearAllSelectedFacets : function(component) {
        var state = component.get("v.currentState");
        if (state === undefined) {
            return;
        }

        // Loop through all facet options and set selected check to false.
        for (var i=0; i<state.length; i++) {
            state[i].checked = false;
        }
        component.set("v.currentState", state);
		component.set("v.isCollapsed", true);
    },

	removeFacet: function(component, event){
		var lst=[];
		lst.push({"value": component.get("v.facetParamKey")});
		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": lst,
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId"),
			"isRemove" : true
		});
		prefsChangedEvt.fire();  
	 }
    
})