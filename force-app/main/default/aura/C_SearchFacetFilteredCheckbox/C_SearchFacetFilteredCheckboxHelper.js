({
    /*
     * Add to request Q
     */
    addToFilterRequestQ : function(component) {
        var requestQ = component.get("v.requestQ");
        requestQ.push(this.getFilterEventParams(component));
        component.set("v.requestQ", requestQ);
    },

    /*
     * Start the request Q processor.
     */
    startRequestQProcessor : function(component) {
        var self = this;
        // Set interval timer (1 sec) to process server requests.
        var qProcessorId = setInterval($A.getCallback(function() {
            var requestQ = component.get("v.requestQ");
            // If there are queued requests, send the last one to server and clear queue.
            if (requestQ.length > 0) {
                var lastParams = requestQ[requestQ.length - 1];
                self.fireFilterEvent(component, lastParams);
                component.set("v.requestQ", []);
            }

        }), 1000);
        component.set("v.qProcessorId", qProcessorId);
    },

    /*
     * Stop the request Q processor.
     */
    stopRequestQProcessor : function(component) {
        clearInterval(component.get("v.qProcessorId"));
    },

	setCurrentStateFromOptions : function (component, currentState, options) {
        var checkboxes = JSON.parse(options);
        // console.log("setting current state from options");
        component.set("v.currentState", 
            this.mergeIncomingToCurrentState(checkboxes, currentState, false, component));
    },

    updateFacetMetadata : function(component) {
        component.set("v.waitingForResponse", false);

        var incomingFacetData = component.get("v.incomingFacetData");
        var incomingSelected = component.get("v.incomingSelectedFacets");
        // var currentState = component.get("v.currentState");
        var currentState = component.get("v.currentState") ? component.get("v.currentState") : [];
        // var currentState = [];
        var options = component.get("v.options");

        if (options !== undefined && options !== null) {
            // Filters which don't return in the aggregations should have options set.
            this.setCurrentStateFromOptions(component, currentState, options);
            return;
        }

          if (incomingFacetData === undefined || incomingFacetData === null) {
            // Error condition - when search fails, nothing in the incoming facets.
            // Maybe also check for empty string?
            // Possibly not error condition. If there are no facets, then this could be undefined. STILL UNSURE!
            incomingFacetData = [];
        }

        if (currentState.length === 0) {
            if (incomingSelected) {
                /*currentState = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                            this.postFilterFacetData(incomingSelected, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingSelected;*/
				currentState = incomingSelected;
                for (var i=0; i<currentState.length; i++) {
                    currentState[i].checked = true;
                    currentState[i].show = true;
                    currentState[i].pillId = component.find("facetHelper").getPillId(currentState[i].key);
					if (component.get("v.usePersonCount") === true) {
						currentState[i].doc_count = currentState[i].personCount.doc_count;
					}
                }
            }
            if (incomingFacetData) {
                /*var ifd = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                            this.postFilterFacetData(incomingFacetData, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingFacetData;*/
                for (var i=0; i<incomingFacetData.length; i++) {
                    incomingFacetData[i].checked = false;
                    incomingFacetData[i].show = true;
                    incomingFacetData[i].pillId = component.find("facetHelper").getPillId(incomingFacetData[i].key);
					if (component.get("v.usePersonCount") === true) {
						incomingFacetData[i].doc_count = incomingFacetData[i].personCount.doc_count;
					}
                }
                currentState = currentState.concat(incomingFacetData);
            }
            component.set("v.currentState", currentState);
        } else {
            /*var incFiltered = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                                        this.postFilterFacetData(incomingFacetData, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingFacetData;
            var selFiltered = null;
            if (incomingSelected) {
                selFiltered = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                                        this.postFilterFacetData(incomingSelected, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingSelected;
            }*/
            component.set("v.currentState", 
				this.mergeIncomingToCurrentState(incomingFacetData, incomingSelected, currentState, component));
                // this.mergeIncomingToCurrentState(incFiltered, selFiltered, currentState, component));
        }

	},

    mergeIncomingToCurrentState : function (incomingFacetData, incomingSelected, currentState, component) {
        var incomingMap = {};
        for (var i=0; i<incomingFacetData.length; i++) {
            incomingMap[incomingFacetData[i].key] = incomingFacetData[i];
        }
        if (incomingSelected) {
            for (var i=0; i<incomingSelected.length; i++) {
                incomingMap[incomingSelected[i].key] = incomingSelected[i];
            }
        }

        var newMap = {};
        // Start new state with checked options from the old current state 
        for (var i=0; i<currentState.length; i++) {
            if (currentState[i].checked === true) {
				var opt = incomingMap[currentState[i].key];
                if (opt) {
                    // Update count from incoming
                    // currentState[i].doc_count = incomingMap[currentState[i].key].doc_count;
					currentState[i].doc_count = component.get("v.usePersonCount") === true ? opt.personCount.doc_count : opt.doc_count;
                } else {
                    // Not in incoming, set count to 0
                    currentState[i].doc_count = 0;
                }
                newMap[currentState[i].key] = currentState[i];
            }
        }

        if (incomingSelected) {
            for (var i=0; i<incomingSelected.length; i++) {
                if (!newMap[incomingSelected[i].key]) {
                    newMap[incomingSelected[i].key] = {
                        key : incomingSelected[i].key,
                        show : true,
                        checked : true,
                        pillId : component.find("facetHelper").getPillId(incomingSelected[i].key),
                        doc_count : component.get("v.usePersonCount") === true ? incomingSelected[i].personCount.doc_count : incomingSelected[i].doc_count
                    };
                }
            }
        }
        for (var i=0; i<incomingFacetData.length; i++) {
            // If already on current state and no data on the selected "shadow" facet, retain the state of checked
            // Caters to the user filtered facet (nested_local_facet_filter)
            var checked = (newMap[incomingFacetData[i].key] && !incomingSelected) ? 
                                    newMap[incomingFacetData[i].key].checked : false;
            newMap[incomingFacetData[i].key] = {
                key : incomingFacetData[i].key,
                show : true,
                checked : checked,
                pillId : component.find("facetHelper").getPillId(incomingFacetData[i].key),
                doc_count : component.get("v.usePersonCount") === true ? incomingFacetData[i].personCount.doc_count : incomingFacetData[i].doc_count
            };
        }

        var newCurrentState = [];
        var keys = Object.keys(newMap);
        var sz = component.get("v.fetchSize");
        // Loop over the newly generated state
        for (var i=0; i<(keys.length >= sz ? sz : keys.length); i++) {
            newCurrentState.push(newMap[keys[i]]);
        }

        return newCurrentState;
    },


    filterCheckboxes : function (component) {
        if (component.get("v.filterText") !== undefined) {
            this.fireFilterEvent(component, this.getFilterEventParams(component));
        }
    },

    translateFacetParam : function (component) {
        var outgoingFacetData = {};
        var facets = component.get("v.currentState");

        if (facets && facets !== null) {
            var sel = [];
            for (var i=0; i<facets.length; i++) {
                if (facets[i].checked === true) {
                    sel.push(facets[i].key);
                }
            }

            var selObj = {};
            switch(component.get("v.facetParamKey")) {
                case "nested_facet_filter.job_skills" :
                case "nested_facet_filter.job_functions" :
                    case "nested_facet_filter.industry_code" :
				case "nested_facet_filter.companyName" : 
                case "nested_facet_filter.myCandidateListsFacet" :
                    // Fall through

                case "nested_facet_filter.degreeOrgFacet" :
                case "nested_facet_filter.certFacet" :
                    if (sel.length > 0) {
                        selObj[component.get("v.nestedKey")] = sel;
                        outgoingFacetData = selObj;
                    } else {
                        outgoingFacetData = null;
                    }
                    break;

                case "facetFlt.employment_position_title" :
                    // Fall through to default
                    
                default :
                    var delim = component.get("v.delimiter");
                    selObj = sel.join(delim);
                    outgoingFacetData = selObj;
                    break;
            }
            
        }

        return outgoingFacetData;
    },

	updateCurrentState: function(component, currentState, key) { 
		for (var i=0; i<currentState.length; i++) {
			if (currentState[i].key === key) {
				currentState[i].checked = true;
				return currentState;
			}
		}
        currentState.push(this.newEntry(component, key));
		return currentState;
    },

    presetCurrentState : function(component) { 
        // var currentState = []; 
		var currentState = component.get("v.currentState"); 
		if (!currentState) {
			currentState = [];
		}
        var presetState = component.get("v.presetState"); 
 
        switch(component.get("v.facetParamKey")) { 
            case "nested_facet_filter.job_skills" :
            case "nested_facet_filter.job_functions" :
                case "nested_facet_filter.industry_code" :
			case "nested_facet_filter.companyName" :
            case "nested_facet_filter.degreeOrgFacet" :
			case "nested_facet_filter.certFacet" :

                var selections = presetState[component.get("v.nestedKey")];
                for (var i=0; i<selections.length; i++) {
                    // currentState.push(this.newEntry(component, selections[i]));
					currentState = this.updateCurrentState(component, currentState, selections[i]);
                }
                break;

			case "nested_facet_filter.myCandidateListsFacet" : 
				// Code for different structure
				var lists = [];
				for (var i=0; i<presetState.length; i++) {
					if (presetState[i][component.get("v.nestedKey")]) {
						lists = presetState[i][component.get("v.nestedKey")];
					}
				}
				for (var i=0; i<lists.length; i++) {
                    // currentState.push(this.newEntry(component, lists[i]));
					currentState = this.updateCurrentState(component, currentState, lists[i]);
                } 
				break;

			case "facetFlt.employment_position_title" : 
                // Fall through to default 
 
            default : 
                var filters = presetState.split(component.get("v.delimiter")); 
                for (var i=0; i<filters.length; i++) { 
                    // currentState.push(this.newEntry(component, filters[i])); 
					currentState = this.updateCurrentState(component, currentState, filters[i]);
                } 
                break; 
        } 
 
        component.set("v.currentState", currentState); 
    }, 
 
    newEntry : function(component, key) { 
        return { 
            "key" : key, 
            "label" : key, 
            "show" : true, 
            "checked" : true, 
            "pillId" : component.find("facetHelper").getPillId(key), 
            "doc_count" : 0 
        }; 
    }, 
 
    getFacetPillData : function(component) {
        var outgoingPillData = [];
        var facets = component.get("v.currentState");

        if (facets && facets !== null) {
            for (var i=0; i<facets.length; i++) {
                if (facets[i].checked === true) {
                    var pillData = {};
                    switch(component.get("v.facetParamKey")) {
                        default :
                            pillData.id = facets[i].pillId;
                            pillData.label = facets[i].key;
                            break;
                    }
                    outgoingPillData.push(pillData);
                }
            }
        }

        return outgoingPillData;
    },

    getFilterEventParams : function(component) {
        var filterParams = {};
        filterParams[component.get("v.sizeKey")] = component.get("v.fetchSize");
        filterParams[component.get("v.optionKey")] = true;
        // filterParams[component.get("v.includeKey")] = this.generateFilterRegex(component);
        //filterParams[component.get("v.includeKey")] = ".*" + component.get("v.filterText").toLowerCase() + ".*"; // Commented out for Search Optimization
        filterParams[component.get("v.includeKey")] = component.get("v.filterText").toLowerCase();  //S-78090 - Added by Karthik

        /*if (Object.keys(facets).length > 0) {
            filterParams[component.get("v.includeKey").replace(".include", "_selected.include")] = facets;
            filterParams[component.get("v.includeKey").replace(".include", ".exclude")] = facets;
        }*/

        //S-32364 To ignore 'nested_facet_filter.degreeOrgFacet.School' key in Parameter list
        /*if(component.get("v.parentFacetParamKey") === null || component.get("v.parentFacetParamKey") === undefined){
            var csRegex = this.generateCurrentStateRegex(component);
            if (csRegex !== "") {
                filterParams[component.get("v.facetParamKey")] = csRegex;
            }
        }*/
        
		return filterParams; 
    },

    fireFilterEvent : function(component, filterParams) {
        component.set("v.waitingForResponse", true);
        var filterEvent = component.getEvent("filterFacetEvt");
        filterEvent.setParam("filterParameters", filterParams);
        // filterEvent.setParam("initiatingFacet", component.getGlobalId());
        filterEvent.setParam("initiatingFacet", component.get("v.facetParamKey"));

        var facets = this.translateFacetParam(component);
        if (facets) {
            var t = typeof facets;
            if ( (t === "string" && facets !== "") || (t === "object" && Object.keys(facets).length > 0) ) {
                filterEvent.setParam("selection", facets);
                filterEvent.setParam("includeKey", component.get("v.includeKey"));
            }
        }

        filterEvent.fire();
    },

    /*generateFilterRegex : function(component) {
		var currentState = this.generateCurrentStateRegex(component);
        var filter = ".*" + component.get("v.filterText").toLowerCase() + ".*";

        return filter + (currentState === "" ? "" : "|" + currentState);
    },*/

    generateCurrentStateRegex : function(component) {
        var currentState = component.get("v.currentState");
        
        var selected = [];
        for (var i=0; i<currentState.length; i++) {
            if (currentState[i].checked) {
                selected.push(currentState[i].key);
            }
        }

        return selected.join("|");
    },

    /**
     * Method called to handle individual facet option being unchecked from outside component
     * e.g. Closing pill on facet pill bar.
     */
    removeFacetOption : function(component, pillId) {
        var pillMatch = false;

        var state = component.get("v.currentState");
        if (state === undefined) {
            return;
        }

        for (var i=0; i<state.length; i++) {
            if (state[i].pillId === pillId) {
                state[i].checked = false;
                pillMatch = true;
                break;
            }
        }

        if (pillMatch) {
            component.set("v.currentState", state);
            component.find("facetHelper").fireFacetChangedEvt(component);
        }
		
    },

    clearAllSelectedFacets : function(component) {
        var state = component.get("v.currentState");
        if (state === undefined) {
            return;
        }

        // Loop through all facet options and set selected check to false.
        for (var i=0; i<state.length; i++) {
            state[i].checked = false;
        }
        component.set("v.currentState", state);
		component.set("v.isCollapsed", true);
    },

    clearFilter : function(component) {
        component.set("v.filterText", "");
        this.fireFilterEvent(component, this.getFilterEventParams(component));
    },

	removeFacet: function(component, event){
		var lst=[];
		lst.push({"value": component.get("v.facetParamKey")});
		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": lst,
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId"),
			"isRemove" : true
		});
		prefsChangedEvt.fire();  
	}
})