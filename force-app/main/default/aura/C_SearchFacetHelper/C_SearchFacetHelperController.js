({
	doInit: function (component, event, helper) {
		var facetCmp = component.get("v.facetCmp");
		if (facetCmp.get("v.relatedFacets") !== undefined) {
			helper.initRelatedFacetState(component, facetCmp);
			helper.generateNestedKeysList(component, facetCmp);
		}

		if (facetCmp.get("v.initialFilter")) {
            var iniFilt = JSON.parse(facetCmp.get("v.initialFilter"));
            var filtKeys = Object.keys(iniFilt);
            // No array length check. Assuming that the initialFilter is set correctly and has the facet options.
            // facetCmp.set("v.iniFiltMarker", true);
            facetCmp.set("v.presetState", iniFilt[filtKeys[0]]);
        }
	},

	fireRegisterFacetEvt: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.fireRegisterFacetEvt(component, parameters.component);
		}
	},

	fireFacetRequestEvt: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.fireFacetRequestEvt(component, parameters.component);
		}
	},

	fireFacetStateChangedEvt: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.fireFacetStateChangedEvt(component, parameters.component);
		}
	},

	fireFacetChangedEvt: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.fireFacetChangedEvt(component, parameters.component);
		}
	},

	fireFacetPresetEvt: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.fireFacetPresetEvt(component, parameters.component);
		}
	},

	getPillId: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.getPillId(component, parameters.option);
		}
	},

	updateRelatedFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (parameters) {
            helper.updateRelatedFacetState(component, parameters);
        }
    },

	presetFacet: function(component, event, helper) {
        var parameters = event.getParam("arguments");
		if (parameters) {
			helper.setPresetState(component, component.get("v.facetCmp"), parameters.facets);
		}
    }

})