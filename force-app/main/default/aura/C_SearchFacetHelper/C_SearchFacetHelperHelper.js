({
	fireRegisterFacetEvt : function(helperCmp, facetCmp) {
        var registerFacetEvt = helperCmp.getEvent("searchFacetRegisterEvt");

        var iniFilt;
        if (facetCmp.get("v.initialFilter")) {
        	iniFilt = JSON.parse(facetCmp.get("v.initialFilter"));
        } 

        registerFacetEvt.setParams({
        	"facetParamKey" : facetCmp.get("v.facetParamKey"),
            "allowNoKeywordSearch" : facetCmp.get("v.allowNoKeywordSearch"),
            "isVisible" : facetCmp.get("v.isVisible"),
            "isCollapsed" : facetCmp.get("v.isCollapsed"),
            "optionKey" : facetCmp.get("v.optionKey"),
            "sizeKey" : facetCmp.get("v.sizeKey"),
			"fetchSize" : facetCmp.get("v.fetchSize"),
            "initialFilter" : iniFilt,
            "isFilterable" : facetCmp.get("v.isFilterableFacet"),
            "isNested" : facetCmp.get("v.relatedFacets") !== undefined ? true : false,
            "isUserFiltered" : facetCmp.get("v.isUserFiltered"),
			"optionFilter" : facetCmp.get("v.optionFilter"),
			"nestedFacetKey" : facetCmp.get("v.key"),
			"nestedKeyinNestedFacet" : facetCmp.get("v.nestedKey")
        });
        registerFacetEvt.fire();
	},

	fireFacetRequestEvt : function(helperCmp, facetCmp) {
		var facetEvt = helperCmp.getEvent("searchFacetRequestEvt");

		facetEvt.setParams({
			"facetParamKey" : facetCmp.get("v.facetParamKey"),
            "isCollapsed" : facetCmp.get("v.isCollapsed")
		});
        facetEvt.fire();
	},

	fireFacetStateChangedEvt : function(helperCmp, facetCmp) {
		var facetEvt = helperCmp.getEvent("searchFacetStateChangedEvt");

		facetEvt.setParams({
			"facetParamKey" : facetCmp.get("v.facetParamKey"),
            "isCollapsed" : facetCmp.get("v.isCollapsed"),
            "isVisible" : facetCmp.get("v.isVisible")
		});
        facetEvt.fire();
	},

    fireFacetChangedEvt : function(helperCmp, facetCmp) {
        var facetChangedEvent = helperCmp.getEvent("searchFacetChanged");

        var selected = facetCmp.translateFacetParam();
        var applied;
        if (facetCmp.get("v.isUserFiltered") && facetCmp.get("v.facetParamKey") != "nested_filter.forgottenRelationshipsFacet") {
        	applied = this.addLocalUserFacetFilter(facetCmp, selected);
        } else {
        	applied = this.createNestedFacetFilterParam(helperCmp, facetCmp, selected);
        }
        facetChangedEvent.setParams({
            "selectedFacet" : selected, 
            "appliedFacets" : applied,
            "nestedFilters" : facetCmp.get("v.hasNestedFilter") ? this.createNestedFilterParam(helperCmp, facetCmp, selected) : null,
            "selectedPills" : facetCmp.getFacetPillData(),
            "hasNestedFilter" : facetCmp.get("v.relatedFacets") !== undefined ? true : false,
            "facetParamKey" : facetCmp.get("v.facetParamKey"),
            "overrideKey" : facetCmp.get("v.key"),
            "sizeKey" : facetCmp.get("v.sizeKey"),
            "size" : facetCmp.get("v.fetchSize"),
            "isFilterableFacet" : facetCmp.get("v.isFilterableFacet"),
            "isUserFiltered" : facetCmp.get("v.isUserFiltered"),
			"displayProps" : facetCmp.get("v.displayProps")
        });

        if (facetCmp.get("v.isFilterableFacet")) {
        	facetChangedEvent.setParam("includeKey", facetCmp.get("v.includeKey"));
        	var ftxt = facetCmp.get("v.filterText") ? facetCmp.get("v.filterText") : "";
       		facetChangedEvent.setParam("filterTerm", ftxt.toLowerCase()); //S-78090 - Added by Karthik
        }

        facetChangedEvent.fire();
    },

    fireFacetPresetEvt : function(helperCmp, facetCmp) { 
        var facetPresetEvent = helperCmp.getEvent("searchFacetPreset"); 

        var selected = facetCmp.translateFacetParam();
        var applied; 
        if (facetCmp.get("v.isUserFiltered") && facetCmp.get("v.facetParamKey") != "nested_filter.forgottenRelationshipsFacet") {
        	applied = this.addLocalUserFacetFilter(facetCmp, selected);
        } else {
        	applied = this.createNestedFacetFilterParam(helperCmp, facetCmp, selected);
        }

        facetPresetEvent.setParams({ 
            "selectedFacet" : selected, 
            "appliedFacets" : applied,
            "nestedFilters" : facetCmp.get("v.hasNestedFilter") ? this.createNestedFilterParam(helperCmp, facetCmp, selected) : null,
            "selectedPills" : facetCmp.getFacetPillData(),
            "hasNestedFilter" : facetCmp.get("v.relatedFacets") !== undefined ? true : false,
            "facetParamKey" : facetCmp.get("v.facetParamKey"),
            "overrideKey" : facetCmp.get("v.key"),
            "sizeKey" : facetCmp.get("v.sizeKey"),
            "size" : facetCmp.get("v.fetchSize"),
            "isFilterableFacet" : facetCmp.get("v.isFilterableFacet"),
            "isUserFiltered" : facetCmp.get("v.isUserFiltered")
        }); 

		if (facetCmp.get("v.isFilterableFacet")) {
        	facetPresetEvent.setParam("includeKey", facetCmp.get("v.includeKey"));
			facetPresetEvent.setParam("filterTerm", "");
        }

        facetPresetEvent.fire(); 
    }, 
 
	getPillId : function(helperCmp, option) {
		var pillIdMap = helperCmp.get("v.pillIdMap");
		if (pillIdMap === null) {
			pillIdMap = {};
		}

		var randomId = pillIdMap[option];
		if (randomId === undefined) {
			randomId = Math.floor(Math.random() * 10000000000);
			pillIdMap[option] = randomId;
		}
		
		helperCmp.set("v.pillIdMap", pillIdMap);
		return randomId;
	},

	setPresetState : function(helperCmp, facetCmp, facets) {
		if (facets !== null) {
	        if (facetCmp.get("v.key") !== undefined && facetCmp.get("v.isUserFiltered") !== true) {
	        	// This is a nested facet OR a user filtered facet (e.g. my lists).
	        	if (facets[facetCmp.get("v.key")] && facets[facetCmp.get("v.key")] !== "") {
	        		var fullFacet = facets[facetCmp.get("v.key")];
					var nkFpkMap = helperCmp.get("v.nkFpkMap");
					var ps;
	        		for (var i=0; i<fullFacet.length; i++) {
	        			// Match the object key to the nestedKey for the right one.
	        			var key = Object.keys(fullFacet[i])[0];
						// If a related facet value, update relatedFacetState, else hold to finally update presetState.
	        			if (key === facetCmp.get("v.nestedKey")) {
	        				// facetCmp.set("v.presetState", fullFacet[i]);
							ps = fullFacet[i];
	        			} else {
							this.updateRelatedFacetState(helperCmp, {
								"initiatingFacet" : nkFpkMap[key],
								"selectedFacet" : fullFacet[i]
							});
						}
	        		}
					// presetState should be set last because the value change handler needs the relatedFacetState.
					if (ps) {
						facetCmp.set("v.presetState", ps);
					}
	        	}
	        } else {
	        	// Not nested
	        	if (facets[facetCmp.get("v.facetParamKey")] !== undefined && facets[facetCmp.get("v.facetParamKey")] !== "") {
	        		facetCmp.set("v.presetState", facets[facetCmp.get("v.facetParamKey")]);
	        	} else if (facetCmp.get("v.initialFilter")) {
					// For facets that have initialFilter applied - 
					// Preset state needs to be overwritten to empty/null to make sure the default filters do not apply.
					facetCmp.set("v.presetState", "");
	        	}
	        }
		}
	},

	initRelatedFacetState : function(helperCmp, facetCmp) {
		var rf = facetCmp.get("v.relatedFacets").split(",");
		var state = {};
		for (var i=0; i<rf.length; i++) {
			state[rf[i].trim()] = {
				"nf" : null,
				"nff" : null 
			};
		}
		helperCmp.set("v.relatedFacetState", state);
	},

	generateNestedKeysList : function(helperCmp, facetCmp) {
		if (facetCmp.get("v.nestedKeysOrder")) {
			var keys = facetCmp.get("v.nestedKeysOrder").split(",");
			var nkFpkMap = {};
			var nkList = [];
			for (var i=0; i<keys.length; i++) {
				var spl = keys[i].split("|");
				nkList.push(spl[1]);
				nkFpkMap[spl[1]] = spl[0];
			}

			helperCmp.set("v.nkList", nkList);
			helperCmp.set("v.nkFpkMap", nkFpkMap);
		}
	},

	updateRelatedFacetState : function(helperCmp, evtParams) {
		var state = helperCmp.get("v.relatedFacetState");
		// var nfFiltState = component.get("v.nestedFacetFilterState");
		if (state[evtParams.initiatingFacet] !== undefined) {
			state[evtParams.initiatingFacet].nff = evtParams.selectedFacet;
			if (evtParams.hasNestedFilter) {
				state[evtParams.initiatingFacet].nf = evtParams.selectedFacet;
			}
		}

		helperCmp.set("v.relatedFacetState", state);
	},

	createNestedFacetFilterParam : function(helperCmp, facetCmp, myFacetParams) {
		return this.createNestedParams(helperCmp, facetCmp, myFacetParams, "nff");
	},

	createNestedFilterParam : function(helperCmp, facetCmp, myFacetParams) {
		return this.createNestedParams(helperCmp, facetCmp, myFacetParams, "nf");
	},

	createNestedParams : function(helperCmp, facetCmp, myFacetParams, nfOrNff) {
		var prm = myFacetParams;
		if (facetCmp.get("v.relatedFacets") !== undefined) {
			var arr = myFacetParams === null ? [] : [myFacetParams];
			var relatedState = helperCmp.get("v.relatedFacetState");
			var rs = Object.keys(relatedState);
			for (var i=0; i<rs.length; i++) {
				var rel = relatedState[rs[i]];
				if (rel[nfOrNff] !== null) {
					arr.push(rel[nfOrNff]);
				}
			}
			if (arr.length === 0) {
				prm = null;
			} else if (arr.length === 1) {
				prm = arr;
			} else {
				// var ord = facetCmp.get("v.nestedKeysOrder");
				var ord = helperCmp.get("v.nkList");
				// var spl = (ord !== undefined && ord !== "") ? ord.split(",") : [facetCmp.get("v.nestedKey")];
				prm = this.getOrderedNestedArray(arr, ord);
			}
		}

		return prm;
	},

	getOrderedNestedArray : function(facets, keys) {
		var pMap = {};
		var ordArray = [];
		for (var i=0; i<facets.length; i++) {
			var key = Object.keys(facets[i]);
			pMap[key] = facets[i];
		}
		for (var i=0; i<keys.length; i++) {
			if (pMap[keys[i]]) {
				ordArray.push(pMap[keys[i]]);
			}
		}
		return ordArray;
	},

	addLocalUserFacetFilter: function(facetCmp, myFacetParams) {
		if (myFacetParams === null || myFacetParams === "") {
			// Nothing selected. return null
			return null;
		}
		// All other cases, add user filter and selected params as objects to array.
		var ordArray = '[{"User": ["' + facetCmp.get("v.runningUser.psId").toLowerCase() + '"]}, ' + myFacetParams.replace("~", "") + ']';
		var indx = myFacetParams.indexOf("~");
		if (indx >= 0) {
			ordArray = indx === 0 ? '~' + ordArray : ordArray + '~';
		} 

		return ordArray;
	}

})