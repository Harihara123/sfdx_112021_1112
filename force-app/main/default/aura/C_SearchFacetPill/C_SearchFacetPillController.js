({
	removePill : function(component, event, helper) {
		// var pillClosedEvt = $A.get("e.c:E_SearchFacetPillClosed");
		var pillClosedEvt = component.getEvent("pillClosed");
		pillClosedEvt.setParams({
			"removedFacetValue" : component.get("v.pillLabel"), 
			"pillId" : component.get("v.pillId"),
			"pillGroupKey" : component.get("v.pillGroupKey"),
            "isExcluded" : component.get("v.isExcluded")
		});
		pillClosedEvt.fire();
	},

	// Monika - Remove pill on hitting enter button on the close icon
	removePillOnEnterKey : function(component, event, helper) {
	var key = 'which' in event ? event.which : event.keyCode;

	if (key == 13) {
		//removePill(component, event, helper);
		var pillClosedEvt = component.getEvent("pillClosed");
		pillClosedEvt.setParams({
			"removedFacetValue" : component.get("v.pillLabel"), 
			"pillId" : component.get("v.pillId"),
			"pillGroupKey" : component.get("v.pillGroupKey"),
            "isExcluded" : component.get("v.isExcluded")
		});
		pillClosedEvt.fire();
		}
	},

	toggleFavorite : function(component, event, helper) {
		var isFavorite = !component.get("v.isFavorite");
		component.set("v.isFavorite", isFavorite);

		var evt = isFavorite ? component.getEvent("pillFavorited") : component.getEvent("pillUnfavorited");
		evt.setParams({
			"pillLabel" : component.get("v.pillLabel"), 
			"pillId" : component.get("v.pillId"),
			"pillGroupKey" : component.get("v.pillGroupKey"),
            "isExcluded" : component.get("v.isExcluded")
		});
		evt.fire();
	},
	toggleExclude : function(component, event, helper) {
        var isExcluded = component.get("v.isExcluded");
        isExcluded = !isExcluded;
		component.set("v.isExcluded", isExcluded);
        if (isExcluded) {
            let pill = document.querySelectorAll('[data-tracker="pill_exclude--' + component.get('v.pillLabel') + '"]')[0];
            if (pill) {
                pill.click();
            }
        } else {
            let pill = document.querySelectorAll('[data-tracker="pill_add--' + component.get('v.pillLabel') + '"]')[0];
            if (pill) {
                pill.click();
        
            }
        }
        var eventHandlerId = component.get("v.eventHandlerId");
        if(eventHandlerId!=undefined){
            var excludeActionEvent = component.getEvent("excludeActionEvent");
            excludeActionEvent.setParams({"handlerId":eventHandlerId,"isExcluded":isExcluded,"pillId":component.get("v.pillId")});
            excludeActionEvent.fire();
        }
	},


	pillClickHandler : function(component, event, helper) {
		if (component.get("v.enableClick")) {
        	var trackingEvent = $A.get("e.c:TrackingEvent");
	        trackingEvent.setParam('clickTarget', 'related_terms_pill--' + component.get('v.pillLabel'));
    	    trackingEvent.fire();
			component.set("v.isPopupShowing", true);
			helper.showRelatedTermsPopup(component);
		}
	},

    addPill : function(component, event, helper) {
		var pillAddEvt = component.getEvent("pillClickedEvt");
        pillAddEvt.setParams({
			"pillLabel" : component.get("v.pillLabel")
        });
        pillAddEvt.fire();
    },
    
    addPillOnEnterKey : function(component, event, helper) {
        var key = 'which' in event ? event.which : event.keyCode;
    
        if (key == 13) {
            var pillAddEvt = component.getEvent("pillClickedEvt");
            pillAddEvt.setParams({
                "pillLabel" : component.get("v.pillLabel")
            });
            pillAddEvt.fire();
        }
    }
})