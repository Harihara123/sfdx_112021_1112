({
	showRelatedTermsPopup : function(component) {
		var cmpArray = [];
		var cmpParams = {
			"term" : component.get("v.pillLabel"),
			"relatedTerms" : component.getReference("v.relatedTerms"),
			"isPopupShowing" : component.getReference("v.isPopupShowing"),
            "facetType": component.getReference('v.facetType'),
            "scrolledValue": component.getReference('v.scrolledValue')
		};

		$A.createComponent("c:C_SearchPopupRelatedTerms", 
			cmpParams, 
			function(newComponent, status, errorMessage) {
	            if (status === "SUCCESS") {
	                // SUCCESS  - insert component in markup
	                cmpArray.push(newComponent);
					component.set("v.C_SearchPopupRelatedTerms", cmpArray);
	            } else if (status === "INCOMPLETE") {
	                console.log("No response from server or client is offline.");
	                // Show offline error
	            } else if (status === "ERROR") {
	                console.log("Error: " + errorMessage);
	                // Show error message
	            }   
			}                 
	    );
	}
})