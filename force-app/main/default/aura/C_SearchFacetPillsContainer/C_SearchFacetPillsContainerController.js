({
	updatePills : function(component, event, helper) {
		// component.set("v.pillData", event.getParam("pills"));        
		helper.redrawPills(component);
	},

	updatePillsOnDemand : function (component, event, helper) {
		var parameters = event.getParam("arguments");
        if (parameters) {
			component.set("v.pillData",parameters.pillData);
			helper.redrawPills(component);
		}
	},

	clearAll : function(component, event, helper) {
		var clearAllEvt = component.getEvent("clearAllPillsEvt");
        // Updated to clear exlcude keywords
        component.set("v.excludeKeyWords","");

		clearAllEvt.fire();
	},
	//S-80157 Browser back on Talent Search - Karthik
	clearPills : function(component, event, helper) {
		component.set("v.pillData", []);
		helper.redrawPills(component);
	},

	/*killComponent : function(component, event, helper) {
		if (component.isValid()) {
			component.destroy();
		}
	},*/

	togglePills : function(component, event, helper) {
		helper.togglePills(component);	
	},

	handleSearchComplete : function(component, event, helper) {
		component.set("v.displayCmp", true);
	},
    showHideExcludeKeyWordsView : function(component, event, helper) {
    component.set("v.showExcludeKeyView",!component.get("v.showExcludeKeyView"));

			var windowCloserFn;
        if (component.get("v.showExcludeKeyView")) {
        	windowCloserFn = (event) => {
            
            var thisPopover = document.getElementById("exclude-flyout");
			if (event.target && thisPopover) {
				if (component.get('v.showExcludeKeyView') && !thisPopover.contains(event.target) && !event.target.classList.contains('exclude-keywords')) { 
			        component.set("v.showExcludeKeyView", !component.get('v.showExcludeKeyView'));
					document.removeEventListener("click", windowCloserFn);
				}
			} else {
				document.removeEventListener("click", windowCloserFn);
			}
        }
        document.addEventListener("click", windowCloserFn);
        }
    },
    excludeKeyWordsAction : function(component, event, helper) {
        helper.handleExcludeKeyWordsAction(component);
    },
    pillClosedHandler: function(component, event, helper) {
        var pillItem = event.getParams();
        if(pillItem.pillGroupKey === "flt.excludeKeyWords"){
            helper.removeItemFromFacetSelection(component, pillItem);
        }
    },
    addOnEnter : function(component, event, helper) {
        if (event.getParams().keyCode === 13) {
            helper.handleExcludeKeyWordsAction(component);
        }
    },
    handleKeyWordsFlyOutEvent : function(component, event, helper) {
        var handlerId = event.getParam("handlerId");
        if(handlerId==="SearchFacetPillsContainer_1"){
            helper.updateFlyOutEventData(component, event.getParam("inpputValues"),false);
        }
    },
    getExcludeKeys: function(component, event, helper) {
        //helper.getExcludeKeyWordsForSearchAPI(component, event);
        var arrResults;
        var results;
        var targetWords = [];
        var pillData = component.get("v.pillData");
        if(pillData == undefined){
            return arrResults;
        }
        var keyWordsPills = pillData["flt.excludeKeyWords"];
        if(keyWordsPills!=undefined){
            
            for (var i=0; i<keyWordsPills.length; i++) {
                var pillItem = keyWordsPills[i];
                targetWords.push(pillItem.label);
            }
            if(targetWords.length >0){
                results = {};
                results["term"]=targetWords;
                arrResults = [];
                arrResults.push(results);
            }
        }
        return arrResults;//(arrResults!=undefined)?JSON.stringify(arrResults):arrResults;
    },
    createExcludeKeys :  function(component, event, helper){
        var parameters = event.getParam("arguments");
        if (parameters) {
            var excludeKeyWords = parameters.excludeKeys;
            helper.createExcludeWordsPills(component,excludeKeyWords);
       }
    },

    togglePrefs: function(component, event, helper) {
        let showEditPrefs = component.get("v.showEditPreferences");
        component.set("v.showEditPreferences", !showEditPrefs);
        var trackingEvent = $A.get("e.c:TrackingEvent");
        if (showEditPrefs) {
   		return;
        } else {
        trackingEvent.setParam('clickTarget', 'add_filter');
        }
        trackingEvent.fire();

    },

	hideEditPopOverHandler: function (component, event, helper) {
		component.set("v.showEditPreferences", false);
	},

    collapseFacetSection: function (component, event, helper) {
        let collapseEvt = component.getEvent("toggleFacetContainer");
        collapseEvt.fire();
        event.stopPropagation();
    }
})