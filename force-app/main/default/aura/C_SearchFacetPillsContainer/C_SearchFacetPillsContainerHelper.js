({
	redrawPills : function(component) {
		
		var pillsData = component.get('v.pillData');
        
		// New pillsArray - overwrite the entire set every time.
		var pillsArray = [];

		if (pillsData !== undefined) {
			var pillGroups = Object.keys(pillsData);
			var isPillHidden = component.get('v.isPillHidden');
			var pillCount = 0;
			var pillsToShow = component.get('v.pillsToShow');

			for (var i=0; i < pillGroups.length; i++) {

				var pillGroup = pillsData[pillGroups[i]];

				if (pillGroup !== undefined) {
					
					
					for (var j=0; j<pillGroup.length; j++) {

						pillCount++;

						if(isPillHidden && pillCount > pillsToShow){
							break;
						}
                        
						var cmpParams = {
							"pillLabel" : pillGroup[j].label,
							"pillId" : pillGroup[j].id,
							"pillGroupKey" : pillGroups[i],
							"pillColorClass" : pillGroup[j].applied ? "pill-applied" : "pill-unapplied"
							//"pillColorClass" : pillGroup[j].unAppliedColorClass == 'pill-unapplied' ? "pill-unapplied" : "pill-applied"
						};                        
                        if(pillGroups[i]==="flt.excludeKeyWords"){
                            cmpParams.pillColorClass = "dark_red";
                        }
						$A.createComponent("c:C_SearchFacetPill", 
							cmpParams, 
							function(newComponent, status, errorMessage) {
				                if (status === "SUCCESS") {
				                    // SUCCESS  - insert component in markup
				                    pillsArray.push(newComponent);
				                    component.set("v.selectedPills", pillsArray);
				                } else if (status === "INCOMPLETE") {
				                    console.log("No response from server or client is offline.");
				                    // Show offline error
				                } else if (status === "ER   ROR") {
				                    console.log("Error: " + errorMessage);
				                    // Show error message
				                }
				        });
					}


				}
			}
			if( pillCount > pillsToShow){
				
				component.set("v.showMoreLess", isPillHidden ? $A.get("$Label.c.ATS_SHOW_MORE") : $A.get("$Label.c.ATS_SHOW_LESS"));
				
			}else {
				component.set("v.showMoreLess", "");
			}
		}

		// When the last pill is removed, nothing is set in the for loop
		if (pillsArray.length === 0) {
			component.set("v.selectedPills", pillsArray);
		}
		
	},
	togglePills : function(component){
		var isPillHidden = component.get('v.isPillHidden');

		component.set("v.isPillHidden", !isPillHidden);

		this.redrawPills(component);
	},
    handleExcludeKeyWordsAction : function(component){       
        var excludeKeyWords = component.find("exkw");
        var exwkValue = excludeKeyWords.get("v.value");
        exwkValue = exwkValue.trim();
        if(exwkValue != "" && exwkValue.length >0){
            var arrPills = [];
            var keyWords = exwkValue.split(",");

            for(var kc = 0; kc<keyWords.length; kc++){
                var pillValue = keyWords[kc];
                pillValue = pillValue.trim();
                if(pillValue.length>0){
                    var pillObj = {};
                    pillObj.label = pillValue;
                    pillObj.id = Math.floor(Math.random() * 10000000000);
                    arrPills.push(pillObj);
                }
            }

            var pillData = component.get("v.pillData");
            if(pillData == undefined){
                pillData = {};
            }
            pillData["flt.excludeKeyWords"] = arrPills;
            component.set("v.pillData",pillData);
        }
        component.set("v.showExcludeKeyView",false);
    },
    removeItemFromFacetSelection: function(component, item) {

        var pillData = component.get("v.pillData");
        var keyWordsPills = pillData["flt.excludeKeyWords"];
        var itemIndex = -1;

        var tempArray = [];

        for (var i=0; i<keyWordsPills.length; i++) {
            if (keyWordsPills[i].id === item.pillId) {
                //console.log(keyWordsPills[i].id + " === "+ item.pillId);
                //itemIndex = i;
                //break;
            }else{
                tempArray.push(keyWordsPills[i]);
            }
        }

        var inputValue = "";
        pillData["flt.excludeKeyWords"] = tempArray;
        component.set("v.pillData",pillData);
        for (var i=0; i<tempArray.length; i++) {
            var obj = tempArray[i];
            inputValue = inputValue + obj.label;
            if(i < (tempArray.length-1)){
                inputValue = inputValue + ",";
            }
        }
        component.set("v.excludeKeyWords",inputValue);
        // Need to recheck the impact.
         this.raiseSearchFacetChangedEvent(component);
    },
    updateFlyOutEventData: function(component, arrPills, overridePills) {
            
         //var arrPills = event.getParam("inpputValues");

         var pillData = component.get("v.pillData");
         var currentExlWords = [];
         if(pillData == undefined){
                pillData = {};
         }else{
                currentExlWords = pillData["flt.excludeKeyWords"];
         }
         if(!overridePills && currentExlWords != undefined && currentExlWords.length > 0){
            arrPills = arrPills.concat(currentExlWords);
         }
         pillData["flt.excludeKeyWords"] = arrPills;
         component.set("v.pillData",pillData);        
         component.set("v.showExcludeKeyView",false);
    },
    raiseSearchFacetChangedEvent: function(component){
         var facetChangedEvent = component.getEvent("searchFacetChanged");
         facetChangedEvent.fire();
    },
    getExcludeKeyWordsForSearchAPI:function(component, event){

        var resp;
        var arrResults;
        var results = {};
        var targetWords = [];
        var pillData = component.get("v.pillData");
        var keyWordsPills = pillData["flt.excludeKeyWords"];
        if(keyWordsPills!=undefined){
            for (var i=0; i<keyWordsPills.length; i++) {
                var pillItem = keyWordsPills[i];
                targetWords.push(pillItem.label);
            }
            results["term"]=targetWords;
            arrResults = [];
            arrResults.push(results);
            resp = JSON.stringify(arrResults);
        }
        return resp;
    },
    createExcludeWordsPills : function(component, listExludeWords){
        if(listExludeWords != undefined && listExludeWords.length > 0){
            var arrPills = [];
            for(var kc = 0; kc<listExludeWords.length; kc++){
                var pillValue = listExludeWords[kc];
                pillValue = pillValue.trim();
                if(pillValue.length>0){
                    var pillObj = {};
                    pillObj.label = pillValue;
                    pillObj.id = Math.floor(Math.random() * 10000000000);
                    arrPills.push(pillObj);
                }
            }
            this.updateFlyOutEventData(component,arrPills,true);
        }
    }
})