({
	/*saveChange: function (component, event, helper) {
		var lst=[];
        var options = component.get("v.availableOptions");
        for (var obj in options) {
            if (options[obj].selected === true) {
                lst.push(options[obj]);
            }         
        }
		component.set("v.selectedOptions",lst); 

		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": component.get("v.selectedOptions"),
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId")
		});
		prefsChangedEvt.fire();
	},

	cancelChange: function (component, event, helper) {
		let closeEvt = component.getEvent("facetPrefClosedEvt");
		closeEvt.fire();
	},*/

	reloadCmp : function (component, event, helper) {
		if (component.get("v.reload") === true) {
			helper.addRemoveEventListener(component, event,'facetsPopOverId', 'facetsPrefPopOverCls', 'facetsPrefPopOver');
		}
	},

doInit: function (component, event, helper) {
		/*var spinner = component.find('spinner');
		$A.util.addClass(spinner, 'slds-hide');
		$A.util.removeClass(spinner, 'slds-show'); */
		helper.addRemoveEventListener(component, event,'facetsPopOverId', 'facetsPrefPopOverCls', 'facetsPrefPopOver');
		component.set("v.reload", false); 
	},

/*handleClick: function(component, event, helper) {
    var mainDiv = component.find('main-div');
    $A.util.addClass(mainDiv, 'slds-is-open');
  },*/

 handleSelection: function(component, event, helper) {
	var item = event.currentTarget;
	if (item && item.dataset) {
		/*var value = item.dataset.value;
		var selected = item.dataset.selected;
		 var options = component.get("v.availableOptions");
		for (var obj in options) {
			if (options[obj].value == value) {
				options[obj].selected = selected == "true" ? false : true;
			}         
		}
		component.set("v.availableOptions",options);  */   
		
		var lst=[];
		lst.push({"value": item.dataset.value});
		component.set("v.selectedOptions",lst); 
		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": component.get("v.selectedOptions"),
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId"),
			"isRemove" : false
		});
		prefsChangedEvt.fire();  
	}

	
 }
 /*,

	handleMouseLeave: function(component, event, helper) {
		component.set("v.dropdownOver", false);
		var mainDiv = component.find('main-div');
		$A.util.removeClass(mainDiv, 'slds-is-open');
	},

	handleMouseEnter: function(component, event, helper) {
		component.set("v.dropdownOver", true);
	},

	handleMouseOutButton: function(component, event, helper) {
		window.setTimeout(
			$A.getCallback(function() {
			if (component.isValid()) {
				//if dropdown over, user has hovered over the dropdown, so don't close.
				if (component.get("v.dropdownOver")) {
				return;
				}
				var mainDiv = component.find('main-div');
				$A.util.removeClass(mainDiv, 'slds-is-open');
			}
			}), 200
			);
	}*/
})