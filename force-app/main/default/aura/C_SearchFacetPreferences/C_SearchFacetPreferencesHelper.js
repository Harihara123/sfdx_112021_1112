({
	addRemoveEventListener : function(component, event, elementId, headerClass, subClass) {
			$A.util.addClass(event.target, subClass);
			var clickedFn;
			var init = true;
			var myPopover = component.find(elementId);

			clickedFn = $A.getCallback(function(event){
				var popoverEle = myPopover.getElement();
				$A.util.removeClass(popoverEle, 'slds-hide');
				
				
				if(popoverEle && event.target) {
					if (!init && !event.target.className.includes('popOver')) {					
						$A.util.addClass(popoverEle, 'slds-hide');
						$A.util.addClass(event.target, subClass);	
						var myExternalEvent = window.$A.get("e.c:E_HideEditPopOverCmp");							
						myExternalEvent.fire();					
						window.removeEventListener('click',clickedFn);
					}
					init = false;
				} 
				else {
					$A.util.addClass(popoverEle, 'slds-hide');
					var myExternalEvent = window.$A.get("e.c:E_HideEditPopOverCmp");							
				    myExternalEvent.fire();	
					window.removeEventListener('click',clickedFn);
					$A.util.addClass(event.target, subClass);
				}
			});
			
			window.addEventListener('click',clickedFn);
	}
})