({
    doInit : function(component, event, helper) {
		component.find("facetHelper").fireRegisterFacetEvt(component);
        if (component.get("v.presetState")) {
            // Checks the options on the facet
            helper.presetCurrentState(component);
            // Fire event that causes the facet pills to be updated.
            //helper.fireFacetPresetEvt(component);
			component.find("facetHelper").fireFacetPresetEvt(component);
        }

		if (component.get("v.incomingFacetData")) {
			// Facet redrawn (config change) when facet open and options visible
			helper.updateFacetMetadata(component);
		}
	},

	clearFacets : function(component, event, helper) {
		helper.clearAllSelectedFacets(component);
	},

    updateFacetMetadata : function(component, event, helper) {
        helper.updateFacetMetadata(component);
    },

    onSelect : function(component, event, helper) {
    	helper.updateCurrentState(component, event.getSource().get("v.text"));
        component.find("facetHelper").fireFacetChangedEvt(component);
    },

    handlePresetState : function(component, event, helper){
        helper.presetCurrentState(component);
		component.set("v.isCollapsed", false);
        var hlp = component.find("facetHelper");
        hlp.fireFacetPresetEvt(component);
		hlp.fireFacetStateChangedEvt(component);
    },

	translateFacetParam: function(component, event, helper) {
		return helper.translateFacetParam(component);
	},

	getFacetPillData: function(component, event, helper) {
		return helper.getFacetPillData(component);
	},

	updateRelatedFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (component.get("v.relatedFacets") !== undefined && parameters) {
            component.find("facetHelper").updateRelatedFacets(component, parameters.initiatingFacet, parameters.selectedFacet);
        }
    },

	toggleFacet: function(component, event, helper) {
		var isCollapsed = component.get("v.isCollapsed");
        component.set("v.isCollapsed", !isCollapsed);
        // Request search for facets. "isCollapsed" is part of the request params, so nothing returns if collapsing.
        component.find("facetHelper").fireFacetRequestEvt(component);
	},

	removeFacetOption: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (parameters) {
            helper.removeFacetOption(component, parameters.item.pillId);
		}
	},

	presetFacet: function(component, event, helper) {
        var parameters = event.getParam("arguments");
		if (parameters) {
			component.find("facetHelper").presetFacet(parameters.facets);
		}
    },

    removeFacet: function(component, event, helper){
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'remove_facet--' + component.get('v.displayTitle'));
        trackingEvent.fire();
        helper.removeFacet(component, event);
    }
    
})