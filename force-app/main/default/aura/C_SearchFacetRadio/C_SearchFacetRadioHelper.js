({
	setCurrentStateFromOptions : function (component, currentState, options) {
        var checkboxes = JSON.parse(options);
        component.set("v.currentState", 
            this.mergeIncomingToCurrentState(component, checkboxes, currentState));
    },

    updateFacetMetadata : function(component) {
        
        var incomingFacetData = component.get("v.incomingFacetData");
        var currentState = component.get("v.currentState");
        var options = component.get("v.options");

        if (options !== undefined && options !== null) {
            // Filters which don't return in the aggregations should have options set.
            this.setCurrentStateFromOptions(component, currentState, options);
            // Set isVisible to true AFTER current state is set and before method returns.
            component.set("v.isVisible", true);
            return;
        }

        if (incomingFacetData === undefined || incomingFacetData === null) {
            /*// Error condition - when search fails, nothing in the incoming facets.
            // Maybe also check for empty string?
            return;*/
            // Possibly not error condition. If there are no facets, then this could be undefined. KEEP CHECKING!
            incomingFacetData = [];
        }

        if (currentState === undefined || currentState == null) {
            currentState = incomingFacetData;
            for (var i=0; i<currentState.length; i++) {
            	currentState[i].label = this.generateRadioLabel(component, currentState[i]);
                currentState[i].selected = false;
                currentState[i].show = true;
                currentState[i].pillId = component.find("facetHelper").getPillId(currentState[i].label);
            }
            component.set("v.currentState", currentState);
        } else {
            // Merge here
            component.set("v.currentState", 
                this.mergeIncomingToCurrentState(component, incomingFacetData, currentState));
        }
        //console.log("%%%%%%% " + JSON.stringify(component.get("v.currentState")));

        // Set isVisible to true AFTER current state is set.
        component.set("v.isVisible", true);
	},

    mergeIncomingToCurrentState : function (component, incomingFacetData, currentState) {
        var newMap = {};

        // Start new state with checked options from the old current state 
        // Unlike other facets, using the label as key for the map since the key could variable based on the dates (especially for All)
        for (var i=0; i<currentState.length; i++) {
            if (currentState[i].selected === true) {
                newMap[currentState[i].label] = currentState[i];
            }
        }

        var incomingMap = {};
        var displayOverrides = JSON.parse(component.get("v.displayOverrides"));

        for (var i=0; i<incomingFacetData.length; i++) {
            var label = this.generateRadioLabel(component, incomingFacetData[i]);
            incomingMap[label] = true;
            // Check if the incoming facet was checked in the old current state
            var option = newMap[label];
            if (option !== undefined) {
                // Option was in the old current state, update with count from incoming facets
                option.doc_count = incomingFacetData[i].doc_count;
            } else {
                // Option was not in the state. Add now as unchecked.
                newMap[label] = incomingFacetData[i];
                newMap[label].label = label;
                newMap[label].show = true;
                newMap[label].selected = false;
                newMap[label].pillId = component.find("facetHelper").getPillId(label);
            }
        }

        var newCurrentState = [];
        var keys = Object.keys(newMap);
        // Loop over the newly generated state
        for (var i=0; i<keys.length; i++) {
            var option = newMap[keys[i]];
            // If coming from old state, but not in new incoming state, set count to 0.
            if (incomingMap[keys[i]] === undefined) {
                option.doc_count = 0;
            }
            newCurrentState.push(option);
        }

        return component.get("v.overrideSort") === true ? 
        			this.sortState(component, newCurrentState) : newCurrentState;
    },

    /*
     * Method to order the state array.
     */
    sortState : function(component, state) {
    	switch(component.get("v.facetParamKey")) {
    		default : 
    			// Only implementation for last activity date. "All" at the end and numeric order for the rest. 
    			return state.sort(function(a, b) {
    				if (a.label === "All") {
    					return 1;
    				} else if (b.label === "All"){
    					return -1;
    				} else {
    					return a.label - b.label;
    				}
    			});
    	}
    },

    updateCurrentState : function(component, selectedKey) {
    	var state = component.get("v.currentState");
    	for (var i=0; i<state.length; i++) {
    		if (state[i].key === selectedKey) {
    			state[i].selected = true;
    		} else {
    			state[i].selected = false;
    		}
    	}

    	component.set("v.currentState", state);
    },

    generateRadioLabel : function(component, option) {
    	var label = "";

    	switch(component.get("v.facetParamKey")) {
            case "facetFlt.qualifications_last_activity_date" :
            	var days = Math.floor((option.to - option.from) / (1000 * 60 * 60 * 24));
            	label = days > 90 ? "All" : days;
                break;

            default :
                break;
        }

        return label;
    },

    translateFacetParam : function (component) {
        var outgoingFacetData = "";
        var facets = component.get("v.currentState");

        if (facets && facets !== null) {
            var selected;
            for (var i=0; i<facets.length; i++) {
            	// Process selected facet when found. 
                if (facets[i].selected === true) {
                    selected = facets[i];
		            switch(component.get("v.facetParamKey")) {
		                case "facetFlt.qualifications_last_activity_date" :
		                    outgoingFacetData = selected.key;
		                    break;

		                default :
		                    break;
		            }
		            // Quit loop since only one selection possible on radio.
		            break;
                }
            }
            
        }

        return outgoingFacetData;
    },

	updateFacetCurrentState: function(component, currentState, key) { 
		for (var i=0; i<currentState.length; i++) {
			if (currentState[i].key === key) {
				currentState[i].selected = true;
				return currentState;
			}
		}
        currentState.push(this.newEntry(component, key));
		return currentState;
    },

    presetCurrentState : function(component) { 
        // var currentState = []; 
		var currentState = component.get("v.currentState"); 
		if (!currentState) {
			currentState = [];
		}
        var presetState = component.get("v.presetState");

        switch(component.get("v.facetParamKey")) {
            case "facetFlt.qualifications_last_activity_date" :
                // Check if presetState is not empty to prevent any string split / range errors.
                if (presetState !== "") {
                    // currentState.push(this.newEntry(component, presetState));
					currentState = this.updateFacetCurrentState(component, currentState, presetState);
                }
                break;

            default :
                break;
        }

        component.set("v.currentState", currentState);
    },

    newEntry : function(component, range) {
        var msecs = this.savedRangeToMsecs(range);

        // Current time
        var t = new Date();
        var tMonth = t.getMonth() < 9 ? "0" + (t.getMonth() + 1) : "" + (t.getMonth() + 1);
        var to_as_string = tMonth + "/" + t.getDate() + "/" + (t.getFullYear() - 2000);

        // Current time minus milliseconds in range
        var f = new Date(t.getTime() - msecs);
        var fMonth = f.getMonth() < 9 ? "0" + (f.getMonth() + 1) : "" + (f.getMonth() + 1);
        var from_as_string = fMonth + "/" + f.getDate() + "/" + (f.getFullYear() - 2000);

        var entry = {
            "to" : t.getTime(),
            "to_as_string" : to_as_string,
            "from" : f.getTime(),
            "from_as_string" : from_as_string,
            "key" : from_as_string + "-" + to_as_string,
            "show" : true,
            "selected" : true,
            "doc_count" : 0
        };
        entry.label = this.generateRadioLabel(component, entry);
        entry.pillId = component.find("facetHelper").getPillId(entry.label);

        return entry;
    },

    /* Convert date range to msecs */
    savedRangeToMsecs : function(range) {
        var dates = range.split("-");
        var rFrom = dates[0].split("/").map(function(x) {
            return parseInt(x);
        });
        var rTo = dates[1].split("/").map(function(x) {
            return parseInt(x);
        });

        var tTo = new Date(rTo[2] + 2000, rTo[0] - 1, rTo[1]).getTime();
        var tFrom = new Date(rFrom[2] + 2000, rFrom[0] - 1, rFrom[1]).getTime();

        return tTo - tFrom;
    },

    getFacetPillData : function(component) {
        var outgoingPillData = [];
        var facets = component.get("v.currentState");

        if (facets && facets !== null) {
            for (var i=0; i<facets.length; i++) {
                if (facets[i].selected === true) {
                    var pillData = {};
                    switch(component.get("v.facetParamKey")) {
                        case "facetFlt.qualifications_last_activity_date" :
                            pillData.id = facets[i].pillId;
                            pillData.label = component.get("v.pillPrefix") + ": " + facets[i].label;
                            break;

                        default :
                            break;
                    }
                    outgoingPillData.push(pillData);
                }
            }
        }

        return outgoingPillData;
    },

    removeFacetOption : function(component, pillId) {
        var pillMatch = false;

        var state = component.get("v.currentState");
        if (state === undefined) {
            return;
        }

        for (var i=0; i<state.length; i++) {
            if (state[i].pillId === pillId) {
                state[i].selected = false;
                pillMatch = true;
                break;
            }
        }

        if (pillMatch) {
            component.set("v.currentState", state);
            component.find("facetHelper").fireFacetChangedEvt(component);
        }

    },

    clearAllSelectedFacets : function(component) {
        var state = component.get("v.currentState");
        if (state === undefined) {
            return;
        }

        // Loop through all facet options and set selected check to false.
        for (var i=0; i<state.length; i++) {
            state[i].selected = false;
        }
        component.set("v.currentState", state);
		component.set("v.isCollapsed", true);
    },

	 removeFacet: function(component, event){
		var lst=[];
		lst.push({"value": component.get("v.facetParamKey")});
		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": lst,
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId"),
			"isRemove" : true
		});
		prefsChangedEvt.fire();  
	 }

    
})