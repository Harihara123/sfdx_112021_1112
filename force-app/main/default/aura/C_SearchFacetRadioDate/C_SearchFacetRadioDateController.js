({
    doInit : function(component, event, helper) {
		component.find("facetHelper").fireRegisterFacetEvt(component);
    },

	clearFacets : function(component, event, helper) {
		helper.removeFacetOption(component);
	},
    
    updateFacetMetadata : function(component, event, helper) {
        // Set isVisible to true AFTER current state is set.
        // component.set("v.isVisible", true);
    },

    
	
    applyValues : function (component, event, helper) {	  	
       helper.applyValues(component);
	},

	allSubmRadio : function (component, event, helper) {
		var selected = event.getSource().get("v.name");
		component.set("v.submittalSelected", selected);

		var mys = component.find("mys");
		mys.set("v.value", false);
		helper.applyValues(component);
	},

	mySubmRadio : function (component, event, helper) {
		var selected = event.getSource().get("v.name");
		component.set("v.submittalSelected", selected);

		var alls = component.find("alls");
		alls.set("v.value", false);
		helper.applyValues(component);
	},

	
    handlePresetState : function(component, event, helper){
        helper.presetCurrentState(component);
		component.set("v.isCollapsed", false);
		var hlp = component.find("facetHelper");
        hlp.fireFacetPresetEvt(component);
		hlp.fireFacetStateChangedEvt(component);
    },

    dpChange : function(component, event, helper) {
        // helper.restoreDisplayProps(component);
    },

	translateFacetParam: function(component, event, helper) {
		return helper.translateFacetParam(component);
	},

	getFacetPillData: function(component, event, helper) {
		return helper.getFacetPillData(component);
	},

	updateRelatedFacets: function(component, event, helper) {
    },

	toggleFacet: function(component, event, helper) {
		var isCollapsed = component.get("v.isCollapsed");
        component.set("v.isCollapsed", !isCollapsed);
        // Request search for facets. "isCollapsed" is part of the request params, so nothing returns if collapsing.
        component.find("facetHelper").fireFacetRequestEvt(component);
	},

	removeFacetOption: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (parameters) {
            helper.removeFacetOption(component, parameters.item.pillId);
		}
	},

	presetFacet: function(component, event, helper) {
        var parameters = event.getParam("arguments");
		if (parameters) {
			component.find("facetHelper").presetFacet(parameters.facets);
		}
    },

    removeFacet: function(component, event, helper){
       helper.removeFacet(component, event);
    }
})