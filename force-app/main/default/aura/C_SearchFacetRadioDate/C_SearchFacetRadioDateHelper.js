({
    translateFacetParam : function(component) {
		var user = component.get("v.runningUser");
        var submRadio = component.get("v.submittalSelected") !== null ? component.get("v.submittalSelected") : "";
		
        var lower = ((component.find("alls").get("v.value") === true || component.find("mys").get("v.value") === true) && component.get("v.minDate") === null) ? this.twoYearOldDate() :  component.get("v.minDate");
        var higher = ((component.find("alls").get("v.value") === true || component.find("mys").get("v.value") === true) && component.get("v.maxDate") === null) ? this.todaysDate() : component.get("v.maxDate");			
        
        var outgoingFacetData = {};
		var outData;
        switch(component.get("v.facetParamKey")) {
            case "nested_filter.submitHistory" :
			default :
				outData = '[';
						if(submRadio === 'mySubmittal') {
							if (user.psId) {
								outData += '{"UserId" : ["'+user.psId+'"]},';
							} else {
								outData += '{"UserId" : [""]},';
							}
						}
					if(lower && higher)	{
						outData += '{"Date" : ["'+lower+'","'+higher+'"]}';
					}
					outData += ']';

                    console.log('testttt'+outData);
                outgoingFacetData = outData === "[]" ? "" : outData;
                          
        }
        
        return outgoingFacetData;
    },

	
	presetCurrentState : function(component) {
        var presetState = component.get("v.presetState");			
		var low, high;
		if (presetState) {
			var isPreset = true;

			switch(component.get("v.facetParamKey")) {
				case "nested_filter.submitHistory" :
				default :
					var params = JSON.parse(presetState);
					for(var i=0; i<params.length; i++) {
						if(params.length === 2 && i===0) {
							component.set("v.submittalSelected", "mySubmittal");
							component.find("mys").set("v.value", true);
						}
						else {
							if(params.length === 1 && i===0) {
								component.set("v.submittalSelected", "allSubmittal");
								component.find("alls").set("v.value", true);
							}
							low  = params[i].Date[0];
							high = params[i].Date[1];
						}
					}					
			}

			component.set("v.minDate", low);
			component.set("v.maxDate", high);
			
		}

    },

   getFacetPillData : function(component) {
        var outgoingPillData = [];
        var pillPrefix;
		var selectedSubm = component.get("v.submittalSelected");
        
        if(selectedSubm === 'mySubmittal' || selectedSubm === 'allSubmittal')
        {
            if(component.get("v.submittalSelected") === 'mySubmittal') {
			pillPrefix = $A.get("$Label.c.My_Submittals");
            }
            else {
                pillPrefix = $A.get("$Label.c.All_Submittals");
            }	
            
            if(this.isStateReset(component)){
                pillPrefix = pillPrefix + ": " + $A.localizationService.formatDate(this.twoYearOldDate()) + " - "+ $A.localizationService.formatDate(this.todaysDate());
            }else{
                 pillPrefix = pillPrefix  + ": " + $A.localizationService.formatDate(component.get("v.minDate")) + " - "+ $A.localizationService.formatDate(component.get("v.maxDate"));
            }
            
            var pillData = {
                "id" : component.find("facetHelper").getPillId(pillPrefix),
                "label" : pillPrefix
                };
            
              outgoingPillData.push(pillData);
              
        }
		
        return outgoingPillData;
    },

    isStateReset : function(component) {
        var low;
        var high;

        low = component.get("v.minDate");
        high = component.get("v.maxDate");
        
        return (low === undefined || low === null) && (high === undefined || high === null);
    },

    applyValues : function (component) {
		var isDateValid = true;
        var isDateFilled = true;
		var isRadioChecked = this.submSelect(component);
		var minDate = component.get("v.minDate");
		var maxDate = component.get("v.maxDate");
		isDateFilled = this.checkValidDate(component, minDate, maxDate);
		isDateValid = this.validateDate(component, minDate, maxDate);
              
        if(isRadioChecked && isDateFilled && isDateValid)
        {
               component.find("facetHelper").fireFacetChangedEvt(component);

            /*
            if((minDate == null || minDate==""))
            component.set("v.minDate",this.twoYearOldDate());
            
            if((maxDate == null || maxDate==""))
            component.set("v.maxDate",this.todaysDate());
            
            if(isDateValid)
            {
            	component.find("facetHelper").fireFacetChangedEvt(component);
            }
            else
            return;*/
        }
        else if(isRadioChecked && !isDateValid){
            return;
        }
        else if(!isRadioChecked){
            return;
        }         
        else{
            component.find("facetHelper").fireFacetChangedEvt(component);
        }
	},

	submSelect : function(component) {
		 var errorArr = []; 
		 var allSubm = component.find("alls");
		 var isSelected;
		if(component.get("v.submittalSelected")) {
			isSelected =  true;
		}
		else {
			errorArr.push({message: "Select ALL or My Submittals above."});
		}

		
        if(errorArr.length > 0 ){
            allSubm.set("v.errors", errorArr);
			isSelected = false;
        }else{
            allSubm.set("v.errors", "");
        }
		return isSelected;
	},

    
    removeFacetOption : function(component, pillId) {    
		component.set("v.minDate", null); 
        component.set("v.maxDate", null);  
		component.set("v.submittalSelected", null);
		var mys = component.find("mys");
		mys.set("v.value", false);
		var alls = component.find("alls");
		alls.set("v.value", false);  
        
       component.find("facetHelper").fireFacetChangedEvt(component);
    },

    calculateDate : function(operand, days){
            
       var dayMillisecond = 86400000;
      // var date = new Date(); 
      // var todaysDate = date.getFullYear() + '.' + date.getMonth() + '.' + date.getDate();
       var calculatedDate; 
       
        switch (operand){
           case "add":
             calculatedDate = new Date().getTime() + (dayMillisecond * days); 
           break;
            
            case "minus":
             calculatedDate = new Date().getTime() - (dayMillisecond * days); 
             break; 
            
            default:
                break;
            }
        return calculatedDate;

    }, 
 
    checkValidDate : function(component,minDate,maxDate){
        var isDateValid = true;
         if(maxDate !== null && maxDate !== undefined && (minDate == null || minDate=="")){
               isDateValid = false;           
        }            
         
         if(minDate !== null && minDate !== undefined && (maxDate == null || maxDate=="")){
               isDateValid = false;           
        }  
       return isDateValid;
    },

    validateDate : function(component, minDate, maxDate){

        var maxDateField = component.find("endDate");
        var minDateField = component.find("startDate");
        var isDateValid = true; 
        var errorArr1 = []; 
        var errorArr2 = []; 

        
         if(maxDate !== null && maxDate !== undefined){
               if(minDate == null || minDate==""){
               errorArr2.push({message: "Required field."});
               isDateValid = false;           
        }            
        }  
         if(minDate !== null && minDate !== undefined){
               if(maxDate == null || maxDate==""){
               errorArr1.push({message: "Required field."});
               isDateValid = false;           
        }           
        }  
        /*
        if(maxDate == null || maxDate==""){
             isDateValid = false;             
        }  
         if(minDate == null || minDate==""){
            isDateValid = false;              
        }    */
        

        if(maxDate !== null && maxDate !== undefined){
            
         var maxTimeDiff = Math.ceil((new Date(maxDate).getTime()/ (1000 * 3600 * 24))) - Math.ceil((new Date(this.todaysDate()).getTime()/ (1000 * 3600 * 24)));
            
            if(maxTimeDiff  > 0){
               errorArr1.push({message: "Future date ranges are not allowed.  Please review and select new dates."});
               isDateValid = false;
            }            
        

        }

        if(minDate !== null && minDate !== undefined){
        
           var minTimeDiff =  Math.ceil((new Date(this.todaysDate()).getTime()/ (1000 * 3600 * 24))) - Math.ceil((new Date(minDate).getTime()/ (1000 * 3600 * 24)));

            if (minTimeDiff > 730){
                errorArr2.push({message: "Submittal Date Range cannot be greater than 2 years in the past."});
                isDateValid = false; 
            }
        }


        if((maxDateField.get("v.value") !== undefined && maxDateField.get("v.value") !== null ) && ((minDateField.get("v.value") !== undefined) && minDateField.get("v.value") !== null)){
            if(new Date(maxDateField.get("v.value")).getTime() < new Date(minDateField.get("v.value")).getTime()){
                errorArr1.push({message: "End Date cannot be less than Start Date"});
                isDateValid = false; 
            }

            if(new Date(minDateField.get("v.value")).getTime() > new Date(maxDateField.get("v.value")).getTime()){
                errorArr2.push({message: "Start Date cannot be greater than End Date"});
                isDateValid = false; 
            }
        }
        
   
        if(errorArr1.length > 0 ){
            maxDateField.set("v.errors", errorArr1);
        }else{
            maxDateField.set("v.errors", "");
        }
        if(errorArr2.length > 0 ){
            minDateField.set("v.errors", errorArr2);
        }else{
            minDateField.set("v.errors", "");
        }

        return isDateValid;

    },
    findDiffDays : function(date1, date2){

        var d1 = new Date(date1);
        var d2 = new Date(date2);
        var timeDiff = (d1.getTime() - d2.getTime());
        var diffDays = 0; 

        diffDays = (Math.ceil(timeDiff / (1000 * 3600 * 24))); 
        return diffDays; 
    
    }, 

	todaysDate : function(){
        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth() + 1;
        var year = today.getFullYear();
        var getDate = day.toString();
        if (getDate.length == 1) { 
           getDate = "0" + getDate;
        }
        var getMonth = month.toString();
        if (getMonth.length == 1) {
           getMonth = "0" + getMonth;
        }
		var todaysDate = year + "-" + getMonth + "-" + getDate;
        return todaysDate;
     },
    
    twoYearOldDate : function(){
        var result = new Date();
		result.setDate(result.getDate() -730);
        
        var day = result.getDate();
        var month = result.getMonth() + 1;
        var year = result.getFullYear();
        var getDate = day.toString();
        if (getDate.length == 1) { 
           getDate = "0" + getDate;
        }
        var getMonth = month.toString();
        if (getMonth.length == 1) {
           getMonth = "0" + getMonth;
        }
		var todaysDate = year + "-" + getMonth + "-" + getDate;
        return todaysDate;
     },

	 removeFacet: function(component, event){
		var lst=[];
		lst.push({"value": component.get("v.facetParamKey")});
		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": lst,
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId"),
			"isRemove" : true
		});
		prefsChangedEvt.fire();  
	 }

})