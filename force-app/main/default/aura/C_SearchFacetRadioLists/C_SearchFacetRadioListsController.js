({
	doInit: function (component, event, helper) {
		component.find("facetHelper").fireRegisterFacetEvt(component);
	},

	updateFacetMetadata : function (component, event, helper) {
        helper.updateFacetMetadata(component);
    },

	clearFacets: function (component, event, helper){
		helper.removeFacetOption(component);
	},

	removeFacetOption: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (parameters) {
            helper.removeFacetOption(component, parameters.item.pillId);
		}
	},

	removeFacet: function(component, event, helper){
       helper.removeFacet(component, event);
    },

	updateFacetMetadata : function(component, event, helper) {
        helper.updateFacetMetadata(component);
    },

	filterCheckboxes : function(component, event, helper) {
        helper.addToFilterRequestQ(component);
    },

	translateFacetParam: function(component, event, helper) {
		return helper.translateFacetParam(component);
	},

	handlePresetState : function(component, event, helper){ 
        helper.presetCurrentState(component); 
		component.set("v.isCollapsed", false);
		var hlp = component.find("facetHelper");
		hlp.fireFacetPresetEvt(component);
		hlp.fireFacetStateChangedEvt(component);
    },

	presetFacet: function(component, event, helper) {
        var parameters = event.getParam("arguments");
		if (parameters) {
			component.find("facetHelper").presetFacet(parameters.facets);
		}
    },

	toggleFacet: function(component, event, helper) {
		var isCollapsed = component.get("v.isCollapsed");
        component.set("v.isCollapsed", !isCollapsed);
        // Request search for facets. "isCollapsed" is part of the request params, so nothing returns if collapsing.
		if (!isCollapsed) {
			component.set("v.waitingForResponse", true);
			component.find("facetHelper").fireFacetRequestEvt(component);
		}
	},

    showBeta:function(component, event, helper) {
        component.set('v.showBetaText', true);
    },

    hideBeta:function(component, event, helper) {
        component.set('v.showBetaText', false);
    },

	handleCollapse : function(component, event, helper) {
        if (component.get("v.isCollapsed")) {
            // Clear filter
            component.set("v.filterText", "");
        }         
    },

	startRequestQProcessor : function(component, event, helper) {
        // Clear request Q if there were any pending requests
        component.set("v.requestQ", []);
        helper.startRequestQProcessor(component);
    },

	stopRequestQProcessor : function(component, event, helper) {
        helper.stopRequestQProcessor(component);
    },

    myRelationshipRadio : function(component, event, helper) {
		component.set("v.isMyLists", true);
		component.set("v.isAllLists", false);
        
		var alls = component.find("allRelationships");
		alls.set("v.value", false);
        
        var userID = component.get('v.runningUser').id;
        var scoreThreshold = component.get('v.scoreThreshold');
        var dateThreshold = component.get('v.dateThreshold');

        // Manually set user
        component.set('v.isUserFiltered', false);

        var facetValue = [{"User":[userID]},
                          {"Date":[dateThreshold.my.lower,dateThreshold.my.upper]},
                          {"Score":[scoreThreshold.my.lower,scoreThreshold.my.upper]}];

        component.set('v.filterText', JSON.stringify(facetValue));

		component.find("facetHelper").fireFacetChangedEvt(component);
	},

	allRelationshipRadio : function(component, event, helper) {
		component.set("v.isMyLists", false);
		component.set("v.isAllLists", true);
        
		var mys = component.find("myRelationships");
		mys.set("v.value", false);

        var scoreThreshold = component.get('v.scoreThreshold');
        var dateThreshold = component.get('v.dateThreshold');

        // Manually set user
        component.set('v.isUserFiltered', false);
        var facetValue = [{"User":["-ANY-"]},
                          {"Date":[dateThreshold.all.lower,dateThreshold.all.upper]},
                          {"Score":[scoreThreshold.all.lower,scoreThreshold.all.upper]}];

        component.set('v.filterText', JSON.stringify(facetValue));

		// Also remove all "My" selections
		component.set("v.currentState", []);
		component.find("facetHelper").fireFacetChangedEvt(component);
	},

	myListRadio : function(component, event, helper) {
		component.set("v.isMyLists", true);
		component.set("v.isAllLists", false);

		var alls = component.find("alllist");
		alls.set("v.value", false);

		component.set("v.waitingForResponse", true);
		component.find("facetHelper").fireFacetRequestEvt(component);
	},

	allListRadio : function(component, event, helper) {
		component.set("v.isAllLists", true);
		component.set("v.isMyLists", false);

		var mys = component.find("mylists");
		mys.set("v.value", false);

		// Also remove all "My" selections
		component.set("v.currentState", []);
		component.find("facetHelper").fireFacetChangedEvt(component);
	},

	onCheck : function(component, event, helper) {
        component.find("facetHelper").fireFacetChangedEvt(component);
    },

	getFacetPillData: function(component, event, helper) {
		return helper.getFacetPillData(component);
	},

	updateRelatedFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (component.get("v.relatedFacets") !== undefined && parameters) {
            component.find("facetHelper").updateRelatedFacets(component, parameters.initiatingFacet, parameters.selectedFacet);
        }
    },

	

})