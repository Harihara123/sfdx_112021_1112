({
	updateFacetMetadata : function(component) {
        component.set("v.waitingForResponse", false);

        var incomingFacetData = component.get("v.incomingFacetData");
        var incomingSelected = component.get("v.incomingSelectedFacets");
        // var currentState = component.get("v.currentState");
        var currentState = component.get("v.currentState") ? component.get("v.currentState") : [];
        // var currentState = [];
        var options = component.get("v.options");

        if (options !== undefined && options !== null) {
            // Filters which don't return in the aggregations should have options set.
            this.setCurrentStateFromOptions(component, currentState, options);
            return;
        }

          if (incomingFacetData === undefined || incomingFacetData === null) {
            // Error condition - when search fails, nothing in the incoming facets.
            // Maybe also check for empty string?
            // Possibly not error condition. If there are no facets, then this could be undefined. STILL UNSURE!
            incomingFacetData = [];
        }

        if (currentState.length === 0) {
            if (incomingSelected) {
                /*currentState = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                            this.postFilterFacetData(incomingSelected, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingSelected;*/
				currentState = incomingSelected;
                for (var i=0; i<currentState.length; i++) {
                    currentState[i].checked = true;
                    currentState[i].show = true;
                    currentState[i].pillId = component.find("facetHelper").getPillId(currentState[i].key);
					if (component.get("v.usePersonCount") === true) {
						currentState[i].doc_count = currentState[i].personCount.doc_count;
					}
                }
            }
            if (incomingFacetData) {
                /*var ifd = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                            this.postFilterFacetData(incomingFacetData, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingFacetData;*/
                for (var i=0; i<incomingFacetData.length; i++) {
                    incomingFacetData[i].checked = false;
                    incomingFacetData[i].show = true;
                    incomingFacetData[i].pillId = component.find("facetHelper").getPillId(incomingFacetData[i].key);
					if (component.get("v.usePersonCount") === true) {
						incomingFacetData[i].doc_count = incomingFacetData[i].personCount.doc_count;
					}
                }
                currentState = currentState.concat(incomingFacetData);
            }
            component.set("v.currentState", currentState);
        } else {
            /*var incFiltered = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                                        this.postFilterFacetData(incomingFacetData, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingFacetData;
            var selFiltered = null;
            if (incomingSelected) {
                selFiltered = component.get("v.facetParamKey") === "nested_facet_filter.degreeOrgFacet" ? 
                                        this.postFilterFacetData(incomingSelected, "DegreeTypePath.DegreeType.buckets.0.YearPath.Year.buckets.0.personCount.doc_count", component.get("v.fetchSize")) : incomingSelected;
            }*/
            component.set("v.currentState", 
				this.mergeIncomingToCurrentState(incomingFacetData, incomingSelected, currentState, component));
                // this.mergeIncomingToCurrentState(incFiltered, selFiltered, currentState, component));
        }

	},

	setCurrentStateFromOptions : function (component, currentState, options) {
        var checkboxes = JSON.parse(options);
        // console.log("setting current state from options");
        component.set("v.currentState", 
            this.mergeIncomingToCurrentState(checkboxes, currentState, false, component));
    },

	mergeIncomingToCurrentState : function (incomingFacetData, incomingSelected, currentState, component) {
        var incomingMap = {};
        for (var i=0; i<incomingFacetData.length; i++) {
            incomingMap[incomingFacetData[i].key] = incomingFacetData[i];
        }
        if (incomingSelected) {
            for (var i=0; i<incomingSelected.length; i++) {
                incomingMap[incomingSelected[i].key] = incomingSelected[i];
            }
        }

        var newMap = {};
        // Start new state with checked options from the old current state 
        for (var i=0; i<currentState.length; i++) {
            if (currentState[i].checked === true) {
				var opt = incomingMap[currentState[i].key];
                if (opt) {
                    // Update count from incoming
                    // currentState[i].doc_count = incomingMap[currentState[i].key].doc_count;
					currentState[i].doc_count = component.get("v.usePersonCount") === true ? opt.personCount.doc_count : opt.doc_count;
                } else {
                    // Not in incoming, set count to 0
                    currentState[i].doc_count = 0;
                }
                newMap[currentState[i].key] = currentState[i];
            }
        }

        if (incomingSelected) {
            for (var i=0; i<incomingSelected.length; i++) {
                if (!newMap[incomingSelected[i].key]) {
                    newMap[incomingSelected[i].key] = {
                        key : incomingSelected[i].key,
                        show : true,
                        checked : false,
                        pillId : component.find("facetHelper").getPillId(incomingSelected[i].key),
                        doc_count : component.get("v.usePersonCount") === true ? incomingSelected[i].personCount.doc_count : incomingSelected[i].doc_count
                    };
                }
            }
        }
        for (var i=0; i<incomingFacetData.length; i++) {
            // If already on current state and no data on the selected "shadow" facet, retain the state of checked
            // Caters to the user filtered facet (nested_local_facet_filter)
            var checked = (newMap[incomingFacetData[i].key] && !incomingSelected) ? 
                                    newMap[incomingFacetData[i].key].checked : false;
            newMap[incomingFacetData[i].key] = {
                key : incomingFacetData[i].key,
                show : true,
                checked : checked,
                pillId : component.find("facetHelper").getPillId(incomingFacetData[i].key),
                doc_count : component.get("v.usePersonCount") === true ? incomingFacetData[i].personCount.doc_count : incomingFacetData[i].doc_count
            };
        }

        var newCurrentState = [];
        var keys = Object.keys(newMap);
        var sz = component.get("v.fetchSize");
        // Loop over the newly generated state
        for (var i=0; i<(keys.length >= sz ? sz : keys.length); i++) {
            newCurrentState.push(newMap[keys[i]]);
        }

        return newCurrentState;
    },

	removeFacetOption : function(component, pillId) {
		if (component.get("v.isMyLists") === true) {
			var pillMatch = false;

			var state = component.get("v.currentState");
			if (state === undefined) {
				return;
			}

			for (var i=0; i<state.length; i++) {
				if (state[i].pillId === pillId) {
					state[i].checked = false;
					pillMatch = true;
					break;
				}
			}

            if (component.get("v.facetParamKey") == "nested_filter.forgottenRelationshipsFacet") {
                // "All Relationships" isn't used, so comment this to prevent errors
                //var allRelationships = component.find("allRelationships");
                //allRelationships.set("v.value", false);

                var myRelationships = component.find("myRelationships");
                myRelationships.set("v.value", false);

                component.set("v.isMyLists", false);
                component.set("v.isAllLists", false);

                component.set("v.filterText", "");
				component.find("facetHelper").fireFacetChangedEvt(component);
            }
            else {
                if (pillMatch) {
                    component.set("v.currentState", state);
                    component.find("facetHelper").fireFacetChangedEvt(component);
                }
            }
		} else if (component.get("v.isAllLists") === true) {
            if (component.get("v.facetParamKey") == "nested_filter.forgottenRelationshipsFacet") {
                // "All Relationships" isn't used, so comment this to prevent errors
                //var allRelationships = component.find("allRelationships");
                //allRelationships.set("v.value", false);

                var myRelationships = component.find("allRelationships");
                myRelationships.set("v.value", false);

                component.set("v.isMyLists", false);
                component.set("v.isAllLists", false);

                component.set("v.filterText", "");
				component.find("facetHelper").fireFacetChangedEvt(component);
            }
            else {
                component.set("v.isAllLists", false);
                var alls = component.find("alllist");
                alls.set("v.value", false);
                component.find("facetHelper").fireFacetChangedEvt(component);
            }
		}
		
    },

	removeFacet: function(component, event){
		var lst=[];
		lst.push({"value": component.get("v.facetParamKey")});
		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": lst,
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId"),
			"isRemove" : true
		});
		prefsChangedEvt.fire();  
	 },

	 addToFilterRequestQ : function(component) {
        var requestQ = component.get("v.requestQ");
        requestQ.push(this.getFilterEventParams(component));
        component.set("v.requestQ", requestQ);
     },

	 getFilterEventParams : function(component) {
		var filterParams = {};
        filterParams[component.get("v.sizeKey")] = component.get("v.fetchSize");
        filterParams[component.get("v.optionKey")] = true;
		filterParams[component.get("v.includeKey")] = component.get("v.filterText").toLowerCase();

		return filterParams;
	 },

	 translateFacetParam : function (component) {
        var outgoingFacetData = {};
        var facets = component.get("v.currentState");

        if (facets && facets !== null) {
            var sel = [];
            for (var i=0; i<facets.length; i++) {
                if (facets[i].checked === true) {
                    sel.push(facets[i].key);
                }
            }

            var selObj = {};
            switch(component.get("v.facetParamKey")) {

                case "nested_filter.forgottenRelationshipsFacet" :
                    if (component.get('v.filterText') != '') {
                        outgoingFacetData = component.get('v.filterText');
                    }
                    break;
                case "nested_facet_filter.myCandidateListsFacet,nested_filter.allCandidateListsFacet" :
                default :
					if (component.get("v.isMyLists") === true) {
						// "isUserFiltered" set to true for My Lists so that FacetHelper component can add user ID
						component.set("v.isUserFiltered", true);
						if (sel.length > 0) {
							selObj[component.get("v.nestedKey")] = sel;
							outgoingFacetData = JSON.stringify(selObj) + "~";
						} else {
							outgoingFacetData = "";
						}
					} else if (component.get("v.isAllLists") === true) {
						// "isUserFiltered" set to FALSE for All Lists so that FacetHelper component does NOT add user ID.
						component.set("v.isUserFiltered", false);
						outgoingFacetData = "~ANY";
					}
                    break;
                    
            }
            
        }

		return outgoingFacetData;
     },

	 presetCurrentState : function(component) { 
        // var currentState = []; 
		var currentState = component.get("v.currentState"); 
		if (!currentState) {
			currentState = [];
		}
        var presetState = component.get("v.presetState"); 
 
        switch(component.get("v.facetParamKey")) { 
            case "nested_filter.forgottenRelationshipsFacet" :
                if (presetState && presetState.length > 0) {
                    component.set('v.filterText', presetState);
                    let isAll = presetState.indexOf('-ANY-') > 0;
                    component.set("v.isMyLists", !isAll);
                    component.set("v.isAllLists", isAll);
                    let mys = component.find("myRelationships");
		            mys.set("v.value", !isAll);
                    let alls = component.find("allRelationships");
                    alls.set("v.value", isAll);
                }
                break;

            case "nested_facet_filter.myCandidateListsFacet,nested_filter.allCandidateListsFacet" : 
            default : 
				var indx = presetState.indexOf("~");
				if (indx === 0) {
					component.set("v.isAllLists", true);
					var alls = component.find("alllist");
					alls.set("v.value", true);
				} else if (indx > 0) {
					component.set("v.isMyLists", true);
					var mys = component.find("mylists");
					mys.set("v.value", true);
					presetState = JSON.parse(presetState.substring(0, presetState.indexOf("~")));

					var lists = [];
					for (var i=0; i<presetState.length; i++) {
						if (presetState[i][component.get("v.nestedKey")]) {
							lists = presetState[i][component.get("v.nestedKey")];
						}
					}
					for (var i=0; i<lists.length; i++) {
						// currentState.push(this.newEntry(component, lists[i]));
						currentState = this.updateCurrentState(component, currentState, lists[i]);
					} 
				}
				break;
        }
		
		component.set("v.currentState", currentState); 
     },

	 updateCurrentState: function(component, currentState, key) { 
		for (var i=0; i<currentState.length; i++) {
			if (currentState[i].key === key) {
				currentState[i].checked = true;
				return currentState;
			}
		}
        currentState.push(this.newEntry(component, key));
		return currentState;
     },

	 newEntry : function(component, key) { 
        return { 
            "key" : key, 
            "label" : key, 
            "show" : true, 
            "checked" : true, 
            "pillId" : component.find("facetHelper").getPillId(key), 
            "doc_count" : 0 
        }; 
     },

	 getFacetPillData : function(component) {
        var outgoingPillData = [];

		var pillPrefix =  component.get("v.isMyLists") === true ? $A.get("$Label.c.ATS_MY_LISTS") : (component.get("v.isAllLists") === true ? $A.get("$Label.c.ATS_ALL_LISTS") : null);

        if (component.get("v.facetParamKey") == "nested_filter.forgottenRelationshipsFacet") {
            var isMyRelationships = component.get('v.isMyLists');
            var isAllRelationships = component.get('v.isAllLists');

            if (isMyRelationships || isAllRelationships) {
                var pillData = {};
                pillData.id = component.find("facetHelper").getPillId(pillPrefix);
                pillData.label = component.get("v.pillPrefix") + ' - ' + (isMyRelationships ? 'My' : 'All');
                outgoingPillData.push(pillData);
            }
        }
		else if (component.get("v.isMyLists") === true) {
			var facets = component.get("v.currentState");
			if (facets && facets !== null) {
				for (var i=0; i<facets.length; i++) {
					if (facets[i].checked === true) {
						var pillData = {};
						switch(component.get("v.facetParamKey")) {
                            case "nested_facet_filter.myCandidateListsFacet,nested_filter.allCandidateListsFacet" :
							default :
								pillData.id = facets[i].pillId;
								pillData.label = pillPrefix + " - " + facets[i].key;
								break;
						}
						outgoingPillData.push(pillData);
					}
				}
			}
		} else if (component.get("v.isAllLists") === true) {
			var pillData = {};
			switch(component.get("v.facetParamKey")) {
				case "nested_facet_filter.myCandidateListsFacet,nested_filter.allCandidateListsFacet" :
				default :
					pillData.id = component.find("facetHelper").getPillId(pillPrefix);
					pillData.label = pillPrefix;
					break;
			}
			outgoingPillData.push(pillData);
		}

        return outgoingPillData;
     },	

	 startRequestQProcessor : function(component) {
        var self = this;
        // Set interval timer (1 sec) to process server requests.
        var qProcessorId = setInterval($A.getCallback(function() {
            var requestQ = component.get("v.requestQ");
            // If there are queued requests, send the last one to server and clear queue.
            if (requestQ.length > 0) {
                var lastParams = requestQ[requestQ.length - 1];
                self.fireFilterEvent(component, lastParams);
                component.set("v.requestQ", []);
            }

        }), 1000);
        component.set("v.qProcessorId", qProcessorId);
     },

	 stopRequestQProcessor : function(component) {
        clearInterval(component.get("v.qProcessorId"));
     },

	 fireFilterEvent : function(component, filterParams) {
        component.set("v.waitingForResponse", true);
        var filterEvent = component.getEvent("filterFacetEvt");
        filterEvent.setParam("filterParameters", filterParams);
        // filterEvent.setParam("initiatingFacet", component.getGlobalId());
        filterEvent.setParam("initiatingFacet", component.get("v.facetParamKey"));

        var facets = this.translateFacetParam(component);
        if (facets) {
            var t = typeof facets;
            if ( (t === "string" && facets !== "") || (t === "object" && Object.keys(facets).length > 0) ) {
                filterEvent.setParam("selection", facets);
                filterEvent.setParam("includeKey", component.get("v.includeKey"));
            }
        }

        filterEvent.fire();
    },

	
})