({
    doInit : function(component, event, helper) {
		component.find("facetHelper").fireRegisterFacetEvt(component);
        helper.generateId(component);
        helper.initMultipliers(component);
        helper.initBounds(component);
    },

	clearFacets : function(component, event, helper) {
		helper.clearAllSelectedFacets(component);
	},
    
    updateFacetMetadata : function(component, event, helper) {
        // Set isVisible to true AFTER current state is set.
        // component.set("v.isVisible", true);
    },

    sliderInit : function(component, event, helper) {
        try {
            helper.initSliderControl(component, true);
        } catch (error) {
            helper.showToast(component);
        }
        
        if (component.get("v.presetState")) {
            // Checks the options on the facet
            helper.presetCurrentState(component);
            // Fire event that causes the facet pills to be updated.
			var hlp = component.find("facetHelper");
			hlp.fireFacetPresetEvt(component);
        }
    },

	updateParams : function (component,event,helper) {
		let low = component.get("v.pastdays");
		let high = component.get("v.today");
		low = (isNaN(low) || low == null) ? "" : -low;
        high = (isNaN(high) || high == null) ? "" : -high;
		component.set("v.low",low);
		component.set("v.high",high);
		helper.updateSliderRange(component);
	},
    
	//To enable multi select gesture on Search/Match
    updateSlider : function (component, event, helper) {
		helper.applyValues(component);
        helper.updateSliderRange(component);
	},

    applyValues : function (component, event, helper) {
			helper.applyValues(component);
	},

	factorChange : function(component, event, helper) {
        try {
            helper.initSliderControl(component, true);
        } catch (error) {
            helper.showToast(component);
        }
        
        helper.updateForFactorChange(component, event.getParam("oldValue"), event.getParam("value"));
        helper.scaleSlider(component);
        helper.updateDisplayProps(component);
    },
    
    handlePresetState : function(component, event, helper){
        helper.presetCurrentState(component);
		component.set("v.isCollapsed", false);
		var hlp = component.find("facetHelper");
        hlp.fireFacetPresetEvt(component);
		hlp.fireFacetStateChangedEvt(component);
    },

    dpChange : function(component, event, helper) {
        helper.restoreDisplayProps(component);
    },

	translateFacetParam: function(component, event, helper) {
		return helper.translateFacetParam(component);
	},

	getFacetPillData: function(component, event, helper) {
		return helper.getFacetPillData(component);
	},

	updateRelatedFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (component.get("v.relatedFacets") !== undefined && parameters) {
            component.find("facetHelper").updateRelatedFacets(component, parameters.initiatingFacet, parameters.selectedFacet);
        }
    },

	toggleFacet: function(component, event, helper) {
		var isCollapsed = component.get("v.isCollapsed");
		//Recent Applicant Facet
		if(isCollapsed && component.get("v.facetParamKey") === 'flt.recent_application_date') {
			helper.addDefaultFilterRange(component,event);
		}
        component.set("v.isCollapsed", !isCollapsed);
		
        // Request search for facets. "isCollapsed" is part of the request params, so nothing returns if collapsing.
        component.find("facetHelper").fireFacetRequestEvt(component);
	},

	removeFacetOption: function(component, event, helper) {
		var parameters = event.getParam("arguments");
        if (parameters) {
            helper.removeFacetOption(component, parameters.item.pillId);
		}
	},

	presetFacet: function(component, event, helper) {
        var parameters = event.getParam("arguments");
		if (parameters) {
			component.find("facetHelper").presetFacet(parameters.facets);
		}
    },

    removeFacet: function(component, event, helper){
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'remove_facet--' + component.get('v.displayTitle'));
        trackingEvent.fire();
       helper.removeFacet(component, event);
    }
})