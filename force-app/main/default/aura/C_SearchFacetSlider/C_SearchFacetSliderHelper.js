({
    generateId : function(component) {
        var sid = Math.floor(Math.random() * 1000000000);
        component.set("v.sliderGlobalId", "facetSlider" + sid);
    },

    initMultipliers : function(component) {
        var mStr = component.get("v.multipliers");
        if (mStr !== undefined) {
            component.set("v.multipliersObj", JSON.parse(mStr));
        }
    },
    initSliderDates : function(component) {
        var date = new Date(); 
        var todaysDate = date.getFullYear() + '.' + date.getMonth() + '.' + date.getDate();
        
    },
    initBounds : function(component){
        var bounds = component.get("v.bounds");

        if(bounds){
			if (!Array.isArray(bounds)) {
				bounds = JSON.parse(bounds);
			}
            component.set("v.minBound", bounds[0]);
            component.set("v.maxBound", bounds[1]);
        }

    },
    translateFacetParam : function(component) {
        var factor = component.get("v.factor");
        var lower;
        var higher;

        if(component.get("v.isCalendar") === true){
            lower = component.get("v.minDate") !== null ? component.get("v.minDate") : "";
            higher = component.get("v.maxDate") !== null ? component.get("v.maxDate") : "";
        }else{
            lower = (component.get("v.low") / factor).toFixed(component.get("v.decimalPoints"));
            higher = (component.get("v.high") / factor).toFixed(component.get("v.decimalPoints"));
			if(component.get("v.facetParamKey") !== 'flt.recent_application_date') {
				lower = isNaN(lower) ? "" : lower;
				higher = isNaN(higher)? "" : higher;
			} else {
				lower = (isNaN(lower) || lower == null) ? "" : lower;
				higher = (isNaN(higher) || higher == null) ? "" : higher;
			}
			if(component.get("v.facetParamKey") === 'flt.recent_application_date') {
				if($A.util.isEmpty(component.get("v.pastdays")) && $A.util.isEmpty(component.get("v.today"))) {
					component.set("v.pastdays","");
					component.set("v.today","");
				} else {
					component.set("v.pastdays",-lower);
					component.set("v.today",-higher);
				}
			}
        }


        var outgoingFacetData = {};
        switch(component.get("v.facetParamKey")) {
            case "flt.remuneration.pay_rate" :
            case "flt.remuneration.bill_rate" :
            case "flt.remuneration.salary" :
            case "flt.placement_end_date":
            // Fall through

            case "flt.total_desired_compensation" :
                if (!this.isStateReset(component)) {
                    var delim = component.get("v.delimiter");
                    outgoingFacetData = "[" + lower + delim + higher + "]";
                } else {
					outgoingFacetData = null;
				}
                break;

            /*
                S-32457 Single slider being used to send min and max desired rate. Call to search will be as follows: 
                flt.min_desired_payrate=[;20]
                flt.max_desired_payrate=[60;]
           */
           case "flt.min_desired_payrate,flt.max_desired_payrate" :
                if (!this.isStateReset(component)) {
                    var delim = component.get("v.delimiter");
                    outgoingFacetData = "["+ delim + higher + "]~[" + lower + delim +"]";
                } else {
					outgoingFacetData = null;
				}
                break;

			//Recent applicants
			case "flt.recent_application_date" :
				if (!this.isStateReset(component)) {
					let today = component.get("v.today");
					let pastdays = component.get("v.pastdays");
					outgoingFacetData = "[" + "now-"+pastdays+"d/d,now-"+today+"d/d]";
				} else {
					outgoingFacetData = null;
				}
				break;

            case "flt.job_create_date":
            case "flt.qualifications_last_resume_modified_date" :
                if (!this.isStateReset(component)) {
                    var delim = component.get("v.delimiter");
                    //To adjust for current date
                    if(higher.includes("T23:59:59.999Z")){
                        // Time Zone duplication hack
                        higher = higher.replace("T23:59:59.999Z", "");
                    } 
                    outgoingFacetData = "[" + lower + delim + higher + "T23:59:59.999Z]";
                    //outgoingFacetData = "[" + lower + delim + higher + "]";
				} else {
					outgoingFacetData = null;
				}
                break;
            
            default :
                if (this.isStateReset(component)) {
                    // outgoingFacetData["Year"] = [];
                    outgoingFacetData = null;

                } else {

                    if(lower === ""){
                       lower = component.get("v.min"); 
                    }
                    if(higher ===""){
                        higher = component.get("v.max");
                    }                                        
                    outgoingFacetData[component.get("v.nestedKey")] = ["" + lower, "" + higher]; 
                }
                break; 
        }
        
        return outgoingFacetData;
    },

	/*isEmpty: function (obj) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				return false;
			}
		}
		return true;
	},*/

    presetCurrentState : function(component) {
        var presetState = component.get("v.presetState");
        component.set("v.inPresetProcess", true);

		if (presetState) {
			// Required for nested facets since only the logic in the switch will determine if it is preset.
			var isPreset = true;
			var low, high;

			switch(component.get("v.facetParamKey")) {
				case "flt.remuneration.pay_rate" :
				case "flt.remuneration.bill_rate" :
				case "flt.remuneration.salary" :
				case "flt.placement_end_date":
				case "flt.qualifications_last_resume_modified_date" :
				case "flt.job_create_date":
				// Fall through

				case "flt.total_desired_compensation" :
					var range = presetState.split(component.get("v.delimiter"));
					low = range[0].substring(1, range[0].length);
					high = range[1].substring(0, range[1].length - 1);
					break;

				case "flt.min_desired_payrate,flt.max_desired_payrate" : 
					var r = presetState.split(component.get("v.delimiter"));
					var range = r[1].split("~");
					low = parseInt(range[1].substring(1));
					high = parseInt(range[0]);
					break;

				case "flt.recent_application_date" :
					let rangeSplit = presetState.split(",");
					rangeSplit.forEach(function(range,index) {
						if(index == 0) {
							low = parseInt(-range.match(/\d+/));
							component.set("v.pastdate",low);
						} else if(index == 1) {
							high = parseInt(-range.match(/\d+/));
							component.set("v.today",high);
						}
					});
					break;
                
				default :
					var selection = presetState[component.get("v.nestedKey")];
					low = parseInt(selection[0]);
					high = parseInt(selection[1]);
					break;
			}

			if(component.get("v.isCalendar")){
				component.set("v.minDate", low);
				component.set("v.maxDate", high);
			} else {
				component.set("v.low", low);
				component.set("v.high", high);
				if(component.get("v.facetParamKey") === "flt.recent_application_date") {
					this.updateSliderRange(component);
				}
			}
            component.set("v.inPresetProcess", false);
		}

    },

    getFacetPillData : function(component) {
        var outgoingPillData = [];
        var pillPrefix = component.get("v.pillPrefix");

        if (!this.isStateReset(component)) {
            if(component.get("v.isCalendar") === true){
               var pillData = {
                "id" : component.find("facetHelper").getPillId(pillPrefix),
                "label" : pillPrefix + ": " 
                            + $A.localizationService.formatDate(component.get("v.minDate")) + " - " 
                            + $A.localizationService.formatDate(component.get("v.maxDate"))
                };
            }else{
				//Recent Applicant facet
				let label = '';
				if(component.get("v.facetParamKey") === 'flt.recent_application_date') {
					label = pillPrefix + ": " + component.get("v.pastdays") + " - " + component.get("v.today")+" days";
				} else { // other facets
					label = pillPrefix + ": " + component.get("v.low") + " - " + component.get("v.high");
				}
				var pillData = {
				"id" : component.find("facetHelper").getPillId(pillPrefix),
				"label" : label
				};
				
            }

            outgoingPillData.push(pillData);
        }

        return outgoingPillData;
    },

    isStateReset : function(component) {
        var low;
        var high;

        if(component.get("v.isCalendar") === true){
            low = component.get("v.minDate");
            high = component.get("v.maxDate");
        }else{
            low = component.get("v.low");
            high = component.get("v.high");
        }

        return (low === undefined || low === null) && (high === undefined || high === null);
    },

    initSliderControl : function(component, firstTime) {
        if(component.get("v.isCalendar") === true){
            var sliderMin =  this.calculateDate('minus', component.get("v.minBound")) / 1000; // default date is 365 days prior to today
            var sliderMax = this.calculateDate('add', component.get("v.maxBound")) / 1000; // default date is today; 

            var sid = component.get("v.sliderGlobalId");
            var newMin;
            var newMax;
			var self = this;
            
            $( function() {
                var sliderControl = $( "#" + sid );
                sliderControl.slider({
                    range: true, 
                    min: sliderMin,
                    max: sliderMax, 
                    step: parseInt(component.get("v.dateStep")),
                    values: [sliderMin, sliderMax], 
                    slide: function (event, ui) {

                        var rawDate1 = new Date(ui.values[0] * 1000);
                        var rawDate2 = new Date(ui.values[1] * 1000); 
                        // Stripping out timezones which throw off max date calculations
            
                        var date1 = rawDate1.toISOString().replace(/T.+/, '');
                        var date2 = rawDate2.toISOString().replace(/T.+/, '');

                        component.set("v.minDate", date1);
                        component.set("v.maxDate", date2);
                    }
                });
            });            

        }else{
            var sliderMin = firstTime || (component.get("v.low") === undefined) ? parseInt(component.get("v.min")) : parseInt(component.get("v.low"));
            var sliderMax = firstTime || (component.get("v.high") === undefined) ? parseInt(component.get("v.max")) : parseInt(component.get("v.high"));
            var sid = component.get("v.sliderGlobalId");
            var newMin;
            var newMax;

            $( function() {
                var sliderControl = $( "#" + sid );
                sliderControl.slider({
                    range: true, 
                    min: sliderMin,
                    max: sliderMax, 
                    step: parseInt(component.get("v.step")),
                    values: [sliderMin, sliderMax], 
                    slide: function (event, ui) {
                        component.set("v.low", ui.values[0]);
                        component.set("v.high", ui.values[1]);
                    }
                });
            });

            }
    },

	applyValues : function (component) {
      var isDateValid = true;
      if(component.get("v.isCalendar") === true){
            var minDate = component.get("v.minDate");
            var maxDate = component.get("v.maxDate");
            isDateValid = this.validateDate(component, minDate, maxDate);
        }
        if(component.get("v.isCalendar") && !isDateValid){
            return;
        }else{
			//component.find("facetHelper").fireFacetChangedEvt(component);			 			
			if(component.get("v.facetParamKey") === 'flt.recent_application_date') {
				let isValidData = this.validateFields(component);
				if(!isValidData) {
					return;
				} else if (!component.get("v.inPresetProcess")) {
					component.find("facetHelper").fireFacetChangedEvt(component);	
				}
			} else if (!component.get("v.inPresetProcess")) {
				component.find("facetHelper").fireFacetChangedEvt(component);			 
			}
        }
	},

    updateSliderRange : function(component) {
        var lower;
        var higher;
        var sliderMin; 
        var sliderMax;
        var sliderControl = $( "#" + component.get("v.sliderGlobalId"));

        if(component.get("v.isCalendar") === true){

            var futureDate =  this.calculateDate('add', component.get("v.maxBound"));// add 90 days to current date
            var pastDate = this.calculateDate('minus', component.get("v.minBound"));// subtract 365 days from current date
            lower = component.get("v.minDate");
            higher = component.get("v.maxDate");
            var fd = new Date(futureDate);
            var pd = new Date(pastDate);
          //  sliderMin = fd.getFullYear() + '-' + (parseInt(fd.getMonth() +1)).toString() + '-' + fd.getDate(); 
          //  sliderMax = pd.getFullYear() + '-' + (parseInt(pd.getMonth()+1)).toString() + '-' + pd.getDate();  

          if(lower === undefined || lower === null || lower == ""){
            sliderMin = pastDate/1000;
          }else{
            sliderMin = new Date(lower).getTime()/1000;
          }
          
          if(higher === undefined || higher === null || higher == "" ){
            sliderMax = futureDate/1000;
          }else{
            sliderMax = new Date(higher).getTime()/1000;
          }
           
            sliderControl.slider("option", "values", [sliderMin, sliderMax]);

        }else{
             lower = component.get("v.low");
             higher = component.get("v.high");
             sliderMin = component.get("v.min"); 
             sliderMax = component.get("v.max");            
        }
        
        if (this.isStateReset(component)) {

            sliderControl.slider("option", "min", sliderMin);
            sliderControl.slider("option", "max", sliderMax);
            sliderControl.slider("option", "values", [sliderMin, sliderMax]);
        } else {
        
        if(!component.get("v.isCalendar")){

     
            /* ATS-4470 - Prevent slider from moving out of range on the UI
             when min value is entered and max value is blank */
            if (lower > sliderMax){
                higher = lower + (lower-sliderMax);
            }
            if((higher === undefined || higher === null) && lower !== null){
                higher = sliderMax;
            }
            /* End ATS-4470*/
            /* Visa versa to changes in ATS-4470 */
            if((lower === undefined || lower === null) && higher !== null){
                lower = sliderMin;
            }

            sliderControl.slider("option", "values", [lower, higher]);
			//Recent Applicant
			if(component.get("v.facetParamKey") !== 'flt.recent_application_date') {
				if (lower < sliderMin) {
					sliderControl.slider("option", "min", lower); 
				}
            
				if (higher > sliderMax) {
					sliderControl.slider("option", "max", higher);
				} 
			}           
          }
        }   

    },

    scaleSlider : function(component) {
        var lower = component.get("v.low");
        var higher = component.get("v.high");
        var sliderMin = component.get("v.min"); 
        var sliderMax = component.get("v.max");
        var sliderControl = $( "#" + component.get("v.sliderGlobalId"));
        
        sliderControl.slider("option", "values", [lower, higher]);
        sliderControl.slider("option", "min", sliderMin); 
        sliderControl.slider("option", "max", sliderMax);
    },

    resetSlider : function(component) {
		component.set("v.isCollapsed", true);
        if (component.get("v.isCalendar") === true){
            component.set("v.minDate", null); 
            component.set("v.maxDate", null); 
        }else{
            if(component.get("v.facetParamKey") === 'flt.recent_application_date') {
				component.set("v.pastdays", null);
				component.set("v.today", null);
			}
			component.set("v.low", null);
			component.set("v.high", null);
        }
    },

    removeFacetOption : function(component, pillId) {
        var pillPrefix = component.get("v.pillPrefix");
        var compPillId = component.find("facetHelper").getPillId(pillPrefix);

        if (pillId === compPillId) {
            this.resetSlider(component);
            this.updateSliderRange(component);			
			component.find("facetHelper").fireFacetChangedEvt(component);
        }

    },

    clearAllSelectedFacets : function(component) {
        this.resetSlider(component);
        this.updateSliderRange(component);
    },

    updateForFactorChange : function(component, oldValue, newValue) {
        var multiplier = newValue / oldValue;
        var min = component.get("v.min");
        var max = component.get("v.max");
        var low = component.get("v.low");
        var high = component.get("v.high");
        var step = component.get("v.step");

        if (min !== undefined) {
            component.set("v.min", Math.round(min * multiplier));
        }
        if (max !== undefined) {
            component.set("v.max", Math.round(max * multiplier));
        }
        if (min !== undefined) {
            component.set("v.low", Math.round(low * multiplier));
        }
        if (min !== undefined) {
            component.set("v.high", Math.round(high * multiplier));
        }
        if (min !== undefined) {
            component.set("v.step", Math.round(step * multiplier));
        }

    },

    calculateDate : function(operand, days){
            
       var dayMillisecond = 86400000;
      // var date = new Date(); 
      // var todaysDate = date.getFullYear() + '.' + date.getMonth() + '.' + date.getDate();
       var calculatedDate; 
       
        switch (operand){
           case "add":
             calculatedDate = new Date().getTime() + (dayMillisecond * days); 
           break;
            
            case "minus":
             calculatedDate = new Date().getTime() - (dayMillisecond * days); 
             break; 
            
            default:
                break;
            }
        return calculatedDate;

    }, 

    validateDate : function(component, minDate, maxDate){

        //var today = new Date();
        //var todaysDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var maxDateField = component.find("endDate");
        var minDateField = component.find("startDate");
        var isDateValid = true; 
        var errorArr1 = []; 
        var errorArr2 = []; 

        //Rajeesh 01/08/2018 
         if(maxDate == null || maxDate==""){
               errorArr1.push({message: "Please select a date."});
               isDateValid = false;           
        }  
         if(minDate == null || minDate==""){
               errorArr2.push({message: "Please select a date."});
               isDateValid = false;           
        }  //end Rajeesh      


        /*
         *Check that max date is not greater than 90 days from today 
         *Check that min date is not greater than 365 days prior to today
        */

        if(maxDate !== null && maxDate !== undefined){
            //Rajeesh 01/08/2018 
        /*    var maxDateArr = [];
            maxDateArr = maxDate.split("-");
            if(maxDateArr.length!=3){
                errorArr1.push({message: "Please select a valid date."});
               isDateValid = false;
               return;
            }//end Rajeesh
         */ var maxTimeDiff = Math.ceil((new Date(maxDate).getTime()/ (1000 * 3600 * 24))) - Math.ceil((new Date(this.todaysDate()).getTime()/ (1000 * 3600 * 24)));
            
            if (component.get("v.maxBound") === 0) {
                if (maxTimeDiff  > 0) {
                     errorArr1.push({message: "Future date ranges are not allowed.  Please review and select new dates."});
                    isDateValid = false;
                }
           } else {
            if(maxTimeDiff  > 91){
               errorArr1.push({message: "Please select date less than 90 days in the future"});
               isDateValid = false;
            }            
        }

        }

        if(minDate !== null && minDate !== undefined){
            //Rajeesh 01/08/2018 
        /*    var minDateArr = [];
            minDateArr = minDate.split("-");
            if(minDateArr.length!=3){
                errorArr2.push({message: "Please select a valid date."});
               isDateValid = false;
               return;
            }//end Rajeesh
         */   
           var minTimeDiff =  Math.ceil((new Date(this.todaysDate()).getTime()/ (1000 * 3600 * 24))) - Math.ceil((new Date(minDate).getTime()/ (1000 * 3600 * 24)));

            if (minTimeDiff > 366){
                errorArr2.push({message: "Please select date not greater than 365 days in the past"});
                isDateValid = false; 
            }
        }


        if((maxDateField.get("v.value") !== undefined && maxDateField.get("v.value") !== null ) && ((minDateField.get("v.value") !== undefined) && minDateField.get("v.value") !== null)){
            if(new Date(maxDateField.get("v.value")).getTime() < new Date(minDateField.get("v.value")).getTime()){
                errorArr1.push({message: "End Date cannot be less than Start Date"});
                isDateValid = false; 
            }

            if(new Date(minDateField.get("v.value")).getTime() > new Date(maxDateField.get("v.value")).getTime()){
                errorArr2.push({message: "Start Date cannot be greater than End Date"});
                isDateValid = false; 
            }
        }
        
   
        if(errorArr1.length > 0 ){
            maxDateField.set("v.errors", errorArr1);
        }else{
            maxDateField.set("v.errors", []);
        }
        if(errorArr2.length > 0 ){
            minDateField.set("v.errors", errorArr2);
        }else{
            minDateField.set("v.errors", []);
        }

        return isDateValid;

    },
        findDiffDays : function(date1, date2){

        var d1 = new Date(date1);
        var d2 = new Date(date2);
        var timeDiff = (d1.getTime() - d2.getTime());
        var diffDays = 0; 

        //diffDays = Math.abs(Math.ceil(timeDiff / (1000 * 3600 * 24))); 
        diffDays = (Math.ceil(timeDiff / (1000 * 3600 * 24))); 
        return diffDays; 
    
    }, 

	todaysDate : function(){
        const today = new Date();
        let day = today.getDate();
        let month = today.getMonth() + 1;
        let year = today.getFullYear();
        let getDate = day.toString();
        if (getDate.length == 1) { 
           getDate = "0" + getDate;
        }
        let getMonth = month.toString();
        if (getMonth.length == 1) {
           getMonth = "0" + getMonth;
        }

        return `${year}-${getMonth}-${getDate}`;
     },

     updateDisplayProps : function(component) {
        component.set("v.displayProps", {
            "factor" : component.get("v.factor")
        });
     },

     restoreDisplayProps : function(component) {
        var dp = component.get("v.displayProps");
        if (dp && dp.factor) {
            component.set("v.factor", dp.factor);
        }
     },

	 removeFacet: function(component, event){
		var lst=[];
		lst.push({"value": component.get("v.facetParamKey")});
		var prefsChangedEvt = $A.get("e.c:E_SearchFacetPrefsChanged");
		prefsChangedEvt.setParams({
			"selection": lst,
			"target": component.get("v.target"),
			"handlerGblId": component.get("v.handlerGblId"),
			"isRemove" : true
		});
		prefsChangedEvt.fire();  
     },

    showToast: function (component) {
        const toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            'type' : 'error',
            "title": "Error!",
            "message": "Please contact the help desk with your credientals and what you where doing when the error occured."
        });
        toastEvent.fire();
    },
	addDefaultFilterRange: function(component,event) {
		let pastDays = component.get("v.pastdays");
		let today = component.get("v.today");
		if(($A.util.isUndefined(pastDays) || $A.util.isEmpty(pastDays) )
			&& ($A.util.isUndefined(today) || $A.util.isEmpty(today))) {
			let low = -45;
			let high = 0;
			component.set("v.pastdays",-low);
			component.set("v.today",-high);
			component.set("v.low", low);
			component.set("v.high", high);
			
			this.updateSliderRange(component);
		}

	},
	validateFields: function(component) {
		let max = component.get("v.max");
		let min = component.get("v.min");
		let low = component.get("v.low");
		let high = component.get("v.high");
		let maxField = component.find("max");
        let minField = component.find("min");	
		let isValid = true; 
        let errorArr1 = []; 
        let errorArr2 = []; 

		if($A.util.isUndefined(low) || $A.util.isEmpty(low)){
               errorArr1.push({message: $A.get("$Label.c.ATS_Recent_Applicant_Required")});
               isValid = false;           
        }  
        if($A.util.isUndefined(high) || $A.util.isEmpty(high)){
               errorArr2.push({message: $A.get("$Label.c.ATS_Recent_Applicant_Required")});
               isValid = false;           
        }

		if(high > max || high < min) {
			errorArr2.push({message: $A.get("$Label.c.ATS_Recent_Applicant_Max_Exceed")});
            isValid = false;
		}
		if(low < min || low > max) {
			 errorArr1.push({message: $A.get("$Label.c.ATS_Recent_Applicant_Min_Exceed")});
             isValid = false;
		}
		if(high !== null && high !== undefined && isValid){
			if(high < low || low > high) {
				errorArr1.push({message: $A.get("$Label.c.ATS_Recent_Applicant_Min_Exceed_Max")});
				isValid = false;
			}
		}
		if(low !== null && low !== undefined && isValid){
			if(high < low || low > high) {
				errorArr2.push({message: $A.get("$Label.c.ATS_Recent_Applicant_Max_less_than_Min")});
				isValid = false;
			}
		}

		if(errorArr1.length > 0 ){
            minField.set("v.errors", errorArr1);
        }else{
            minField.set("v.errors", []);
        }
        if(errorArr2.length > 0 ){
            maxField.set("v.errors", errorArr2);
        }else{
            maxField.set("v.errors", []);
        }

        return isValid;

	} 

})