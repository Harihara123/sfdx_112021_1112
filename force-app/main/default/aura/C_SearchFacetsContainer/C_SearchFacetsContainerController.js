({
	updateFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {            
            var excludeKeyWords = parameters.excludeKeys;
            if(excludeKeyWords){
                helper.updateExcludeKeyWords(component,excludeKeyWords);
            }

			if (parameters.facets) {
				helper.updateRefreshedFacets(component, parameters.facets);
				if (parameters.isReexecute) {
					helper.renderFacets(component);
				}
			} else if (parameters.facetCriteria) {
				component.set("v.criteriaFacets", parameters.facetCriteria);
				component.set("v.selectedCountFilter",Object.keys(parameters.facetCriteria).length);
				helper.presetFacetsForReexecute(component, parameters.facetCriteria);
				// rendering facets again - because this method is called after render facets
				if (!parameters.isReexecute) {
					helper.renderFacets(component);
				}
			}

			if (parameters.facetDisplayProps) {
				component.set("v.displayPropsMap", parameters.facetDisplayProps);
			}
            /* for defect D-07966 only display default filter for facet search and show all filters when search using keyword*/
            if(!parameters.IsFromFacetSearchRequest){
            	component.set("v.displayFilters", true);    
            }
			

			if (parameters.automatchVectors) {
				if (!parameters.isSmartSearch) {
					var mfCmp = component.find("matchFacetContainer");
					mfCmp.updateMatchFacets(parameters.automatchVectors,component.get("v.excludedJobTitles"),component.get("v.excludedSkills"));
				}
				component.set("v.matchVectors", parameters.automatchVectors); //To enable multi select gesture on Search/Match
				component.set("v.selectedCountFilter",0);	
                helper.setMatchVectorFilterCount(component);
                helper.setSelectedFilterCount(component);
			}

			if (parameters.automatchSourceData) {
				component.set("v.automatchSourceData", parameters.automatchSourceData);
			}
                        
		}
 	},

	hideFacets: function(component, event, helper) {
		component.set("v.displayFilters", false);
	},

    updateFilteredFacets : function(component, event, helper) {
        // Set initiatingFacet first so that when the facet update handler is fired, this is available
        // component.set("v.searchInitiatingFacet", event.getParam("initiatingFacet"));
        component.set("v.searchInitiatingFacet", event.getParam("facetParamKey"));

        var incomingFacets = event.getParam("facets");
		helper.updateRefreshedFacets(component, incomingFacets);
        /*var currentFacets = component.get("v.facets");

        if (currentFacets === undefined || currentFacets === null) {
            currentFacets = incomingFacets;
        } else if(incomingFacets !== undefined) {
            var facetKeys = Object.keys(incomingFacets);
            for (var i=0; i<facetKeys.length; i++) {
                currentFacets[facetKeys[i]] = incomingFacets[facetKeys[i]];
            }
        } else {
            currentFacets = {}
        }

        component.set("v.facets", currentFacets);*/
    },
    
    facetChanged : function(component, event, helper) {
        helper.updateCriteriaFacets(component, event);
        if (event.getParam("isFilterableFacet")) {
            helper.updateFacetFilterParams(component, event);
        }
        helper.updateCriteriaNestedFilters(component, event);
        helper.updateSelectedPills(component, event);
		helper.updateDisplayProps(component, event);
        //helper.fireFacetApplyEvt(component, event); //Firing the event on click of Update Results

		if (event) {
			// event is available only on E_FacetChangedEvent. "Clear All" case does not require this?
			helper.updateRelatedFacetState(component, event);
		}
    },

	//To enable multi select gesture on Search/Match
	applyFacets : function(component, event, helper) {
		helper.fireFacetApplyEvt(component);
		helper.markAllFacetPillsApplied(component);
		helper.setSelectedFilterCount(component);
    },

	//To enable multi select gesture on Search/Match
	applyMatchFacets : function(component, event, helper) {
        helper.updateMatchVectors(component, event.getParams());
		//event.stopPropagation();
    },

    facetPreset : function(component, event, helper) {
        helper.updateCriteriaFacets(component, event);
		if (event.getParam("isFilterableFacet")) {
            helper.updateFacetFilterParams(component, event);
        }
        helper.updateCriteriaNestedFilters(component, event);
        helper.updateSelectedPills(component, event);

        var isExecuted = false;
		if (component.get("v.prefAddRemove") === true) {
			isExecuted = true;
		}
        //if(!isExecuted && event.getParam("facetParamKey") === "facetFlt.req_stage" ){
		if(!isExecuted){
        	helper.markAllFacetPillsApplied(component);
        	isExecuted = true;
        }
        // helper.fireFacetPresetApplyEvt(component, event);
    },

	clearAllFacets : function(component, event, helper) {
		helper.clearAllFacets(component, true);
    },

	//S-80157 Browser back on Talent Search - Karthik
	clearAllFacetsNoSearch : function(component, event, helper) {
		helper.clearAllFacets(component, false);
    },

	pillClosedHandler: function(component, event, helper) {		
		helper.removeItemFromFacetSelection(component, event.getParams());
	},

	renderFacets: function(component, event, helper) {
		helper.findPermBasedFacets(component);
		helper.renderFacets(component);
	},

	facetPrefsChanged: function(component, event, helper) {
		var showUpdateResultBtn = false;
		showUpdateResultBtn = helper.isUnappliedAppliedPill(component, false);
		if (event.getParam("handlerGblId") === component.getGlobalId()) {
			var userPrefs = component.get("v.userPrefs.facets");
			//userPrefs[event.getParam("target")] = event.getParam("selection");
			var selectedFacets = event.getParam("selection");
			if (event.getParam("isRemove") === false) {
				var arr = userPrefs[event.getParam("target")];
				if (arr === "defaults") {
					arr = [];
					var def = helper.getDefaultConfig(component);
					Object.keys(def).map(function(key) {
						if (def[key].default === true) {
							arr.push(key);
						}
					});
					userPrefs[event.getParam("target")] = arr;
				} 
				for (var obj in selectedFacets) {
					userPrefs[event.getParam("target")].push(selectedFacets[obj].value);     
				}	
			} else if (event.getParam("isRemove") === true) {
				var arr = userPrefs[event.getParam("target")];
				if (arr === "defaults") {
					arr = [];
					var def = helper.getDefaultConfig(component);
					Object.keys(def).map(function(key) {
						if (def[key].default === true) {
							arr.push(key);
						}
					});
				} 
				for (var obj in selectedFacets) {
					for( var i = 0; i < arr.length; i++){ 
						if ( arr[i] === selectedFacets[obj].value) {
							helper.clearCriteriaAndSelectedPills(component,selectedFacets[obj].value);
							showUpdateResultBtn = helper.isUnappliedAppliedPill(component, false);
							arr.splice(i, 1); 
						}
					}
				}

				userPrefs[event.getParam("target")] = arr;
			}
			
			component.set("v.prefAddRemove",true);
			component.set("v.userPrefs.facets", userPrefs);
			helper.fireSearchPrefsUpdateEvt(component,showUpdateResultBtn);
			helper.showToast('Success', 'Filter preferences have been updated successfully.', 'success');
		}
	}

})