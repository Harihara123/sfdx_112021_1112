({
	updateCriteriaFacets : function(component, event) {
        var criteriaFacets = component.get("v.criteriaFacets");
        if (criteriaFacets === undefined || criteriaFacets === null) {
            criteriaFacets = {};
        }

        // criteriaFacets[event.getParam("facetParamKey")] = event.getParam("selectedFacet");
        var key = event.getParam("overrideKey") !== undefined ? 
                    event.getParam("overrideKey") : event.getParam("facetParamKey");
        criteriaFacets[key] = event.getParam("appliedFacets");

        component.set("v.criteriaFacets", criteriaFacets);
	},

	updateRefreshedFacets: function(component, facets) {
		var currentFacets = component.get("v.facets");
		if (!currentFacets) {
			currentFacets = facets;
		} else if(facets) {
			var facetKeys = Object.keys(facets);
			for (var i=0; i<facetKeys.length; i++) {
				currentFacets[facetKeys[i]] = facets[facetKeys[i]];
			}
		} else {
			currentFacets = {}
		}
		component.set("v.facets", currentFacets);
	},

	presetFacetsForReexecute: function(component, facets) {
		var facetsArr = component.find("facetCmpId");
		if (facetsArr) {
			for (var i=0; i<facetsArr.length; i++) {
				facetsArr[i].presetFacet(facets);
			}
		}
	},

    updateFacetFilterParams : function(component, event) {
        var filterParams = component.get("v.facetFilterParams");
        if (filterParams === undefined || filterParams === null) {
            filterParams = {};
        }
      
        var fp = {};
        fp[event.getParam("sizeKey")] = event.getParam("size");
        fp[event.getParam("includeKey")] = event.getParam("filterTerm");

        var selected = event.getParam("selectedFacet");
        // Check for selected required below to prevent failures when all selections removed.
        if (selected) {
            if (event.getParam("hasNestedFilter")|| event.getParam("isUserFiltered")) {
                var sKeys = Object.keys(selected);
                selected = selected[sKeys[0]].join("|");
            }
            // Do not add .selected if no selections on this facet.
            if (selected !== "") {
                //fp[event.getParam("includeKey").replace(".include", ".selected")] = selected; //Commented out for Search Optimization
				fp[event.getParam("includeKey").replace(".contains", ".selected")] = selected;  //S-78090 - Added by Karthik
            }
        }
        // For user filtered facets, add nested_local_facet_filter
        if (event.getParam("isUserFiltered")) {
            fp[event.getParam("facetParamKey").replace("nested_facet_filter", "nested_local_facet_filter")] 
                    = JSON.stringify([{"User" : [component.get("v.runningUser.psId")]}]);
        }

        // One object entry for each facet
        filterParams[event.getParam("facetParamKey")] = fp;

        component.set("v.facetFilterParams", filterParams);
    },

    updateCriteriaNestedFilters : function(component, event) {
        if (event.getParam("hasNestedFilter")) {
            var criteriaFilters = component.get("v.criteriaNestedFilters");
            if (criteriaFilters === undefined || criteriaFilters === null) {
                criteriaFilters = {};
            }

            // criteriaFilters[event.getParam("filterParamKey")] = event.getParam("nestedFilters");
            component.set("v.criteriaNestedFilters", criteriaFilters);
        }
    },

	updateSelectedPills : function(component, event) {
        var selectedPills = component.get("v.selectedPills");
        if (selectedPills === undefined || selectedPills === null) {
            selectedPills = {};
        }
        var facetKey = event.getParam("facetParamKey")
		var current = selectedPills[facetKey];
		var m = {};
		if (current) {
			for (var i=0; i<current.length; i++) {

                // If facet is a slider, do not apply untill user sets slider range
                switch(facetKey){
                    case "flt.remuneration.pay_rate" :
                    case "flt.remuneration.bill_rate" :
                    case "flt.remuneration.salary" :
                    case "flt.placement_end_date":
                    case "flt.total_desired_compensation" :
                    case "flt.min_desired_payrate,flt.max_desired_payrate" :
                    case "flt.job_create_date":
                    case "flt.qualifications_last_resume_modified_date" : 

                        if(current[i].applied === true){
                            current[i].applied = false;
                        }
                        m[current[i].id] = current[i];
                        
                    break;

                    default :
                        m[current[i].id] = current[i];
                    break;
                }
			}
		} 

		
		var newPills = event.getParam("selectedPills");
		if (component.get("v.prefAddRemove") === false ) {
			if(newPills){
				for (var i=0; i<newPills.length; i++) {
					newPills[i].applied = (!m[newPills[i].id] || m[newPills[i].id].applied === false) ? false : true;
				}
			}
		} else {
			
			var currentPills = component.get("v.selectedPills");
			if (currentPills) {
				var pillArray = currentPills[facetKey];
				if(newPills && pillArray && (pillArray.length === newPills.length)){
					for (var i=0; i<newPills.length; i++) {
						if (pillArray[i].label && newPills[i].label && pillArray[i].label == newPills[i].label
							 && pillArray[i].applied === true) {
							newPills[i].applied = true;
						}
					}
				}
			}
		}
	 /*for (all items in event.getParam("selectedPills")) {
			if not present in selectedPills {
				add to selected pills with applied = false.
				{ "label": "Candidate", "id": "1231441231", "applied" : false}
			}
		}*/

        selectedPills[event.getParam("facetParamKey")] = newPills;
        component.set("v.selectedPills", selectedPills);
       
        
	},

	markAllFacetPillsApplied : function(component) {
		var allPills = component.get("v.selectedPills");
		if(allPills){
			var keys = Object.keys(allPills);
			for (var i=0; i<keys.length; i++) {
				var pills = allPills[keys[i]];
				if(pills){
					for (var j=0; j<pills.length; j++) {
						pills[j].applied = true;
					}
				}
			}
		}

		component.set("v.selectedPills", allPills);
		/*for (all items in selectedPills) {
			set applied = true;
		}*/
	},

	setSelectedFilterCount : function(component) {
		var allPills = component.get("v.selectedPills");			
		var counter = 0;
		if (allPills) {
			var keys = Object.keys(allPills);	
			if (keys) {
				for (var i=0; i<keys.length; i++) {
					if (allPills[keys[i]] && allPills[keys[i]].length > 0) {
						counter=counter+1;
					}
				}
			}
			counter+=component.get("v.matchVectoryFilterCount");
			component.set("v.selectedCountFilter",counter);			
		}
		
	},

	setMatchVectorFilterCount : function(component) {
		var allPills = component.get("v.matchVectors");			
		var counter = 0;
		if (allPills) {
			var keys = Object.keys(allPills);
			if (keys) {	
				for (var i=0; i<keys.length; i++) {
					if (( keys[i] == "skills_vector" || keys[i] == "title_vector") && allPills[keys[i]] && allPills[keys[i]].length > 0) {
						counter=counter+1;
					}
				}
			}
			component.set("v.matchVectoryFilterCount",counter);	
			if (!component.get("v.selectedCountFilter")) {
				component.set("v.selectedCountFilter",counter);	
            }		
		}
	},

	updateDisplayProps : function(component, event) {
		var m = component.get("v.displayPropsMap");
		if (m === undefined || m === null) {
			m = {};
		}
		m[event.getParam("facetParamKey")] = event.getParam("displayProps");

		component.set("v.displayPropsMap", m);
	},

    clearSelectedFilterCriteria : function(component) {
        var ffp = component.get("v.facetFilterParams");

        if (ffp) {
			var keys = Object.keys(ffp);
			for (var i=0; i<keys.length; i++) {
				var facet = ffp[keys[i]];
				var newFacet = {};

				var inkeys = Object.keys(facet);
				for (var j=0; j<inkeys.length; j++) {
					var property = facet[inkeys[j]]
					if (!inkeys[j].endsWith(".selected")) {
						newFacet[inkeys[j]] = property;
					}
				}
				ffp[keys[i]] = newFacet;
			}

			component.set("v.facetFilterParams", ffp);
		}
    },

    fireFacetApplyEvt : function(component, event) {
        var facetApplyEvt = component.getEvent("searchFacetsAppliedEvt");
        var excludeKeyWords = this.populateAllExcludeWords(component);

        // event is undefined when the method is not triggered by a facetChangedEvent. e.g. Clear All on pill bar.
        facetApplyEvt.setParams({
            "facets" : component.get("v.criteriaFacets"),
            "facetFilterParams" : component.get("v.facetFilterParams"),
            "nestedFilters" : component.get("v.criteriaNestedFilters"),
            "selectedFacet" : event === undefined ? "" : event.getParam("selectedFacet"),
            "hasNestedFilter" : event === undefined ? false : event.getParam("hasNestedFilter"),
            "pills" : component.get("v.selectedPills"),
            "initiatingFacet" : event === undefined ? "" : event.getParam("facetParamKey"),
			"displayProps" : component.get("v.displayPropsMap"),
			"matchVectors" : component.get("v.matchVectors"),  //To enable multi select Facets on Search/Match
            "excludeKeyWords" : excludeKeyWords, // Updated to handle exclude keywords in search S-95200
            "excludeJobTitles" : component.get("v.excludedJobTitles"),
            "excludeSkills" : component.get("v.excludedSkills")
        });
        facetApplyEvt.fire();

    },
    populateAllExcludeWords: function(component){

            var excludedJobTitles = component.get("v.excludedJobTitles");
            var excludedSkills = component.get("v.excludedSkills");

            var excludeKeyWords;
            var childSFPCmp = component.find("childSearchFacetPIllsCmp");
            excludeKeyWords = childSFPCmp.getExcludeKeys();
            if(excludeKeyWords == undefined){
                excludeKeyWords = [];
            }
            // Add excluded job titles and skill
            if(excludedJobTitles != undefined && excludedJobTitles != null && excludedJobTitles.length > 0){
                var tempTitles = [];
                for(var jt = 0; jt < excludedJobTitles.length; jt++){
                    var jobTitle = excludedJobTitles[jt];
                    tempTitles.push((typeof jobTitle == 'string')?jobTitle:jobTitle.term);
                }
                var results = {};
                // results["target"]="jobTitles";
				results["target"]="title_vector";
                results["term"]=tempTitles;
                excludeKeyWords.push(results);
            }
            // Add excluded skills
            if(excludedSkills != undefined && excludedSkills.length >0){
                var tempSkills = [];
                for(var sk = 0 ; sk < excludedSkills.length; sk++){
                    var exSkill = excludedSkills[sk];
                    tempSkills.push((typeof exSkill == 'string') ? exSkill :exSkill.term);
                }
                var results = {};
                // results["target"]="skills";
				results["target"]="skills_vector";
                results["term"]=tempSkills;
                excludeKeyWords.push(results);
            }
            return excludeKeyWords;
    },
	//To enable multi select gesture on Search/Match
	updateMatchVectors: function(component, matchFacetParams) {
        var matchVectors = component.get("v.matchVectors");
        var excludedData = matchFacetParams.excludedData;
        switch (matchFacetParams.matchFacetType) {
            case "jobtitles" :
                matchVectors.title_vector = matchFacetParams.matchFacetData;
                component.set("v.excludedJobTitles", excludedData);
                break;

            case "skills" : 
                matchVectors.skills_vector = matchFacetParams.matchFacetData;
                component.set("v.excludedSkills", excludedData);
                break;

            default :
                break;
        }

        component.set("v.matchVectors", matchVectors);
	},

	updateRelatedFacetState: function(component, event) {
		var facetsArr = component.find("facetCmpId");
		for (var i=0; i<facetsArr.length; i++) {
			facetsArr[i].updateRelatedFacets(event.getParam("facetParamKey"), event.getParam("selectedFacet"));
		}
	},

    //S-80157 Browser back on Talent Search - Karthik
	clearAllFacets: function(component, fireApplyEvt) {
        this.clearAllFacetsAttributes(component);
        this.setUserPrefrencesOnClearAll(component); // this needed to remove facets not in preferences.
		this.clearAllFacetsAttributes(component); // this need to call again to clear  initial attributes as we are on clear all.
        if (fireApplyEvt) {
			this.fireFacetApplyEvt(component);
		}
		component.set("v.selectedCountFilter",0); // reset the filter counter
	},

	clearAllFacetsAttributes : function (component) {
		this.clearSelectedFilterCriteria(component);
		this.clearFacets(component);
		// clearFacets() needs to be called before container attributes are cleared.
		component.set("v.facets", {});
		component.set("v.criteriaFacets", {});
		component.set("v.facetFilterParams", {});
        component.set("v.criteriaNestedFilters", {});
        component.set("v.selectedPills", {});
        component.set("v.searchInitiatingFacet", "");
	},

	clearFacets: function(component) {
		var facetsArr = component.find("facetCmpId");
		for (var i=0; i<facetsArr.length; i++) {
			facetsArr[i].clearFacets();
		}
	},

	removeItemFromFacetSelection: function(component, item) {
		var facetsArr = component.find("facetCmpId");
		for (var i=0; i<facetsArr.length; i++) {
			if (facetsArr[i].get("v.facetParamKey") === item.pillGroupKey) {
				facetsArr[i].removeFacetOption(item);
				break;
			}
		}

		// Below code to re-render facets if facet of removed pills are not in user preferences.
		this.setUserPrefrencesOnPillRemove(component,item.pillGroupKey);

	},
    updateExcludeKeyWords: function(component, excludeKeyWords) {
        component.set("v.excludeKeyWords", excludeKeyWords);
        // Iterate and seperate free form exclude keywords
        var freeFormKeywords;
        var jobTitlesExcludes;
        var skillsExcludes;

        for(var ffwCounter = 0; ffwCounter < excludeKeyWords.length; ffwCounter++){
            var exldObj = excludeKeyWords[ffwCounter];
            if(exldObj.target == undefined){
                freeFormKeywords = exldObj.term;
            }
            else if(exldObj.target == "title_vector"){
                if(jobTitlesExcludes==undefined){
                    jobTitlesExcludes = [];
                }
                var jtiltes = exldObj.term;
                if(jtiltes!=undefined){
                    for(var jtc=0;jtc<jtiltes.length;jtc++){
                        jobTitlesExcludes.push((jtiltes[jtc] != undefined && typeof jtiltes[jtc] == 'string')?jtiltes[jtc]:jtiltes[jtc].term);               
                    }
                }                            
            }
            else if(exldObj.target == "skills_vector"){
                skillsExcludes = exldObj.term;
            }
        }

        if(freeFormKeywords != undefined){
            component.set("v.freeTextExcludeKeyWords", freeFormKeywords);
            var childSFPCmp = component.find("childSearchFacetPIllsCmp");
            childSFPCmp.setExcludeKeys(freeFormKeywords);
        }
        component.set("v.excludedJobTitles", jobTitlesExcludes);
        component.set("v.excludedSkills", skillsExcludes);    
               
    },

	fireSearchPrefsUpdateEvt: function(component,showUpdateResultBtn) {
        var prefsUpdateEvt = component.getEvent("prefsUpdateEvt");
		prefsUpdateEvt.setParams({
			"prefs" : component.get("v.userPrefs"),
			"showUpdateResultBtn" : showUpdateResultBtn
		});
		prefsUpdateEvt.fire();
    },

	generateComponentParams: function(component, key, cfg, NotInSearchCriteria) {
		var cmpParams = {
			"facetParamKey": key,
			"allowConfigurability": component.get("v.externalsource") !== "TD" && component.get("v.externalsource") !== "RQ",
			"aura:id": "facetCmpId"
		}
		var userLanguage = component.get("v.runningUser.localeLanguage");
		var userLanguage = component.get("v.runningUser.language");
		var k = Object.keys(cfg);
		var facetList = this.getDefaultConfig(component);
		for (var i=0; i<k.length; i++) {
			var v = cfg[k[i]];
			if (typeof v === "string") {
				if (v.startsWith("$Label")) {
					cmpParams[k[i]] = $A.get(v);
				} else if (v.startsWith("v.")) {
					cmpParams[k[i]] = component.getReference(v);
				} else {
					cmpParams[k[i]] = v;
				}
			} else if (k[i] === "displayTitle"){
				cmpParams[k[i]] = v[userLanguage] ? v[userLanguage] : v["en_US"]; 
					
			} else if(k[i] === "helpTextMessage"){
				cmpParams[k[i]] = v[userLanguage] ? v[userLanguage] : v["en_US"];
			}else {
				cmpParams[k[i]] = v;
			}
		}
		// Extra Params for each facets
		cmpParams.removeFacet = !facetList[key].sticky;
		cmpParams.target = component.get("v.target");
		cmpParams.handlerGblId = component.get("v.handlerGblId");
		cmpParams.NotInSearchCriteria = NotInSearchCriteria;

		// neeed to set selected facets filter 
		var fcts = component.get("v.criteriaFacets");
		if (fcts && (fcts[key] || fcts[cfg.key])) {
			/*if (component.get("v.prefAddRemove") === true) {
				cmpParams.presetState = component.get("v.criteriaFacets")[key];
			}*/

			cmpParams.isCollapsed = false;
		} 
		
		
		return cmpParams;
	},

	addFacet: function(component, config, index, facetArray) {
		// pre extending array
		facetArray.push(null);
		var cfg = config.cfg.config;
		$A.createComponent("c:" + config.cfg.component, 
			this.generateComponentParams(component, config.key, config.cfg.config, config.NotInSearchCriteria), 
			function(newComponent, status, errorMessage) {
				if (status === "SUCCESS") {
					//  place component in array at index to ensure correct positioning of facet after creation
					facetArray[index] = newComponent;
					if (component.get("v.criteriaFacets")) {
						newComponent.presetFacet(component.get("v.criteriaFacets"));
					}
					component.set("v.renderedFacets", facetArray);
					component.set("v.facetsCreated", component.get("v.facetsCreated")+1);
					if (component.get("v.facetsCreated") >= component.get("v.configLength")) {
						if (component.get("v.selectedPills")) {
							var childSFPCmp = component.find("childSearchFacetPIllsCmp");
							childSFPCmp.reDrawPillsOnDemand(component.get("v.selectedPills"));
						}
						component.set("v.facetsCreated", 0);
					}
				} else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.");
					// Show offline error
				} else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
					// Show error message
				}
		});

		return facetArray;
	},

	renderFacets: function(component) {
		component.set("v.handlerGblId", component.getGlobalId());
		//component.set("v.selectedPills", component.get("v.selectedPills"));
		if (component.get("v.userPrefs.facets")) {
			var config = this.getPreferredConfig(component);
			var facetArray = [];
			var index = 0;
			for (var i=0; i<config.length; i++) {
				if (config[i].show === true) {
					facetArray = this.addFacet(component, config[i], index++, facetArray);
					component.set("v.configLength",index);
				}
			}
			
		}
	},

	getPreferredConfig: function(component) {
		var prefs = component.get("v.userPrefs.facets")[component.get("v.target")];
		var config = JSON.parse(component.get("v.facetsConfig"));		
		var def = this.getDefaultConfig(component);
		var lang = component.get("v.runningUser.localeLanguage");
		var lang = component.get("v.runningUser.language");
		
		var preferred = [];	
		var availableToAdd = [];

		if (prefs === "defaults") {
			for (var key in def) {
				var criteriaFacets = component.get("v.criteriaFacets");
				var hasFacet = criteriaFacets && (criteriaFacets[key] !== undefined) ;
				var fct = {
					"key": key,
					"show": def[key].default === true || hasFacet,
					"cfg": config[key],
					"NotInSearchCriteria" : def[key].default === false && hasFacet
				};
				preferred.push(fct);
				if (fct.show !== true) {
					var dtitle = config[key].config.displayTitle;
					var l = dtitle[lang] ? dtitle[lang] : dtitle["en_US"];
					availableToAdd.push({
						// "label": $A.get(config[key].config.displayTitle),
						"label": l,
						"value": key
					});
				}
			};
		} else {
			prefs = this.addPermSetBased(component, prefs);
			for (var key in def) {
				var criteriaFacets = component.get("v.criteriaFacets");
				var hasFacet = criteriaFacets && (criteriaFacets[key] !== undefined);
				var nestedFacetKey, nestedKeyInNestedFacet;
				if (!hasFacet) {
					if (component.get("v.facetProps") && component.get("v.facetProps")[key] !== undefined && component.get("v.facetProps")[key].nestedFacetKey) {
						if (component.get("v.facetProps")[key].nestedKeyinNestedFacet) {
							nestedKeyInNestedFacet = component.get("v.facetProps")[key].nestedKeyinNestedFacet;
						} 
						nestedFacetKey = component.get("v.facetProps")[key].nestedFacetKey;
						if (component.get("v.criteriaFacets") && component.get("v.criteriaFacets")[nestedFacetKey]) {
							if (nestedKeyInNestedFacet) { 
								if (JSON.stringify(component.get("v.criteriaFacets")[nestedFacetKey]).includes(nestedKeyInNestedFacet)) {
									hasFacet = true;
								}
							} else {
								hasFacet = true;
							}
							
						}
					}
				}

				var fct = {
					"key": key,
					"show": prefs.indexOf(key) >= 0 || hasFacet || def[key].force,
					"cfg": config[key],
					"NotInSearchCriteria" : prefs.indexOf(key) < 0 && hasFacet
				};
				preferred.push(fct);
				if (fct.show !== true) {
					var dtitle = config[key].config.displayTitle;
					var l = dtitle[lang] ? dtitle[lang] : dtitle["en_US"];
					availableToAdd.push({
						// "label": $A.get(config[key].config.displayTitle),
						"label": l,
						"value": key
					});
				}
			}
		}

		component.set("v.availableFacets", availableToAdd);
	
		return preferred;
	},

	addPermSetBased: function(component, prefs) {
		prefs = [...prefs];
		const psbf = component.get("v.permSetFacets");
		psbf.forEach(f => {
			prefs.push(Object.keys(f)[0]);
		});
		return prefs;
	},

	findPermBasedFacets: function(component) {
		var defaultConfig = JSON.parse(component.get("v.defaultFacets"))[component.get("v.target")];
		var userOpco = component.get("v.runningUser.opco");
		let userPermSets = component.get("v.runningUser.userPermissionName");
		Object.keys(defaultConfig.opcobased).map(function(key) {
			if (key.includes(userOpco)) {
				userOpco = key;
			}
		});

		let permBasedFacets = [];
		userPermSets.forEach(permSet => {
			let fcts = defaultConfig.permsetbased[permSet];
			fcts && Object.keys(fcts).map(function(fctKey){
				let f = {};
				f[fctKey] = fcts[fctKey];
				f[fctKey].force = true;
				permBasedFacets.push(f);
			});
		});

		component.set("v.permSetFacets", permBasedFacets);

	},

	getDefaultConfig: function(component) {
		var defaultConfig = JSON.parse(component.get("v.defaultFacets"))[component.get("v.target")];
		var userOpco = component.get("v.runningUser.opco");

		var prefs = userOpco ? defaultConfig.opcobased[userOpco] : defaultConfig.opcobased["default"];
		if (!prefs) {
			prefs = defaultConfig.opcobased["default"];
		}

		const permBasedFacets = component.get("v.permSetFacets");

		let finalFctList = {};
		let position = 0;
		if (permBasedFacets.length > 0) {
			Object.keys(prefs[0]).map(function(k) {
				finalFctList[k] = prefs[0][k];
				// Handle condition where position never reaches 4 - i.e. opco based facet list has less than 5 entries
				if (position === 4) {
					permBasedFacets.forEach(psFct => {
						let x = Object.keys(psFct);
						finalFctList[x[0]] = psFct[x[0]];
					});
				}
				position++;
			});
		}

		return position === 0 ? prefs[0] : finalFctList;
	},

	showToast: function(title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },

	clearCriteriaAndSelectedPills : function (component, key) {
		var hasFacet = component.get("v.criteriaFacets") && (component.get("v.criteriaFacets")[key]);
		// Check for nested param keys
		var nestedFacetKey, nestedKeyInNestedFacet;
		if (!hasFacet) {
			if (component.get("v.facetProps") && component.get("v.facetProps")[key] !== undefined && component.get("v.facetProps")[key].nestedFacetKey) {
				if (component.get("v.facetProps")[key].nestedKeyinNestedFacet) {
					nestedKeyInNestedFacet = component.get("v.facetProps")[key].nestedKeyinNestedFacet;
				} 
				nestedFacetKey = component.get("v.facetProps")[key].nestedFacetKey;
				if (component.get("v.criteriaFacets") && component.get("v.criteriaFacets")[nestedFacetKey]) {
					hasFacet = true;
				}
			}
		}
		// this code has been added to check if any applied pills. If not let it remove and add to available list.
		if (hasFacet) { 
			var pillsInKey = component.get("v.selectedPills")[key];
			var pillFoundInApplied = false;
			if (pillsInKey && pillsInKey.length > 0) { 
				for (var i=0; i<pillsInKey.length; i++) {
					if (pillsInKey[i].applied == true) {
						pillFoundInApplied = true;
					}
				}
			
				hasFacet = 	pillFoundInApplied;
				// need to remove key from selected pills to remove from top display filters
				if (!pillFoundInApplied) {
					//delete component.get("v.selectedPills")[key];
					var allPills = component.get("v.selectedPills");
					allPills[key] = []; 
					component.set("v.selectedPills", allPills);  // need to set to fire updatePills event.
					//delete component.get("v.criteriaFacets")[key];
					var criteriaFacets = component.get("v.criteriaFacets");
					if (nestedFacetKey || nestedKeyInNestedFacet) {
						key = nestedFacetKey;
					}
					if (nestedFacetKey && criteriaFacets[key].length == 1 ) {
						delete criteriaFacets[key];
					} else if (nestedKeyInNestedFacet) {
						// nestedFacetKey is common so remove only for selected facted
						Object.keys(criteriaFacets[key]).map(function(nestedkeys) {
							if (nestedkeys == nestedKeyInNestedFacet) {
								delete criteriaFacets[key].nestedkeys;
							}
						});
					} else {
						delete criteriaFacets[key];
					}
					component.set("v.criteriaFacets",criteriaFacets);
				} 
			}
		}
	},

	setUserPrefrencesOnPillRemove : function (component, keyOfDeletedPill) {
		// need to handle case of Clear All filter
		//var keyOfDeletedPill = item.pillGroupKey;
		var pillsInKey = component.get("v.selectedPills") && component.get("v.selectedPills")[keyOfDeletedPill] && component.get("v.selectedPills")[keyOfDeletedPill].length > 0 ? true : false;
		// If no more pills for that key in selected filter then remove facets from user preferences only if 
		if (!pillsInKey) {
			var config = this.getPreferredConfig(component);
			var reRenderFacets = false;
			for (var i=0; i<config.length; i++) {
				if (config[i].key === keyOfDeletedPill && config[i].NotInSearchCriteria === true) {
					var availableToAdd = component.get("v.availableFacets");
					availableToAdd.push({
						// "label": $A.get(config[i].cfg.config.displayTitle),
						"label": config[i].cfg.config.displayTitle,
						"value": config[i].key
					});
					component.set("v.availableFacets", availableToAdd);
					reRenderFacets = true;
					config.splice(i, 1); 
					if (component.get("v.criteriaFacets")[keyOfDeletedPill]) {
						var criteriaFacets = component.get("v.criteriaFacets");
						delete criteriaFacets[keyOfDeletedPill];
						component.set("v.criteriaFacets",criteriaFacets);
					}
				}
			}
			if (reRenderFacets) {
				var facetArray = [];
				var index = 0;
				for (var i=0; i<config.length; i++) {
					if (config[i].show === true) {
						facetArray = this.addFacet(component, config[i], index++, facetArray);
					}
				}
			}
		}
	},

	isUnappliedAppliedPill : function (component, checkStatus) {
		// check if any un-applied /applied pills to show update results button.
		var pillUnapplied = false;
		var allPills = component.get("v.selectedPills");
		if(allPills){
			var keys = Object.keys(allPills);
			for (var i=0; i<keys.length; i++) {
				var pills = allPills[keys[i]];
				if(pills){
					for (var j=0; j<pills.length; j++) {
						if (pills[j].applied === checkStatus) {
							pillUnapplied = true;
							break;
						}
					}
				}
				if (pillUnapplied) {
					break;
				}
			}
		}
		return pillUnapplied;
	},

	setUserPrefrencesOnClearAll : function(component) {
		this.renderFacets(component);
	}

})