({
	doInit : function(component, event, helper) {
		helper.getCurrentUser(component);
        //Added w.r.t STORY S-132137
		//To check user group B permission
		//helper.CheckGroupBUser(component);        
		helper.getSavedSearches(component);
		helper.getUserAlertCount(component);
	},

  	createAlert: function(cmp,e,helper) {
  		
  		var alertId = e.getSource().get("v.name");
  		if( !$A.util.isUndefined(alertId) || !$A.util.isEmpty(alertId) ){
  			helper.alertToast('You already have an existing alert for this search.!', 'Error');
  			return;
		}

		if( cmp.get("v.alertCount") >= $A.get("$Label.c.SEARCH_HISTORY_COUNT") ){
  			helper.alertToast('You cannot add more alerts, You have already created the maximum alerts (' + $A.get("$Label.c.SEARCH_HISTORY_COUNT") + ').', 'Error');
  			return;
		}

		cmp.set('v.searchReplaceIndex', e.getSource().get("v.value") );

  		let searchAlert = cmp.get('v.searchAlert');
		let searchEntry = cmp.get("v.savedSearchList")[e.getSource().get("v.value")];

		searchAlert.Alert_Criteria__c = JSON.stringify(searchEntry);
		
 
 		cmp.set('v.searchEntry', searchEntry);

 		cmp.set('v.saveSearchTitle', searchEntry.title);
		cmp.set('v.searchAlert.Name', searchEntry.title);
		cmp.set('v.searchAlert.Send_Email__c', false );
		cmp.set('v.searchAlert.Alert_Criteria__c', JSON.stringify( searchEntry ));
        
        cmp.set('v.searchAlert.Query_String__c', helper.createQueryStringHelper(cmp,false,true));
        if(searchEntry.type !== 'C' && searchEntry.type !== 'R'){
            cmp.set('v.searchAlert.Request_Body__c',helper.createMatchRequestBody(cmp));
        }else{
         	cmp.set('v.searchAlert.Request_Body__c','');   
        }
		cmp.set('v.searchAlert.Alert_Type__c',searchEntry.type); 
        if(searchEntry.type !== 'R'){
            cmp.set('v.searchAlert.Alert_Trigger__c','Talent Created');
        }else{
            cmp.set('v.searchAlert.Alert_Trigger__c','Req Created');
        }
		
		helper.openAlertModal(cmp, e)

	},

    active: function(component, event, helper){
        var tab = event.getSource();
        if (tab.get('v.id') === 'Last Searches') {
        	component.set('v.searchLogType', 'AllLastSearches');
			component.set('v.searchTitle', "Last Searches");
      		component.set('v.searchToggle', "Last Searches");
        } else if (tab.get('v.id') === 'Shared By Me') {
        	component.set('v.searchLogType', 'SharedByMe');
			component.set('v.searchTitle', "Shared By Me");
      		component.set('v.searchToggle', "Shared By Me");
        } else if (tab.get('v.id') === 'Shared With Me') {
        	component.set('v.searchLogType', 'SharedWithMe');
			component.set('v.searchTitle', "Shared With Me");
      		component.set('v.searchToggle', "Shared With Me");
        } else {           
            component.set('v.searchLogType', 'AllSavedSearches'); 
      		component.set('v.searchTitle', "Saved Searches");
      		component.set('v.searchToggle', "Saved Searches"); 
        }
		component.set('v.selectedTabId', tab.get('v.id')); 
        helper.getSavedSearches(component);
    },
    
	toggle: function(component, event, helper){
		if (component.get('v.searchLogType') === 'AllSavedSearches') {
			component.set('v.searchLogType', 'AllLastSearches'); 
			component.set('v.searchTitle', "Last Searches");
			component.set('v.searchToggle', "Saved Searches");
		} else {
			component.set('v.searchLogType', 'AllSavedSearches'); 
			component.set('v.searchTitle', "Saved Searches");
			component.set('v.searchToggle', "Last Searches");
		}
		helper.getSavedSearches(component);      
	},

	deleteSearch: function(component, event, helper) {
		helper.deleteSavedSearch(component,event);
	},

	navigateSearch: function(component, event, helper) {
		helper.toUrl(component, event);
	},

	shareSearchModal : function(component, event, helper) {
		$A.createComponent("c:C_ShareSearchModal", { "searchEntry" : helper.getSelectedSearchEntry(component, event),
													"searchType" : "SharedByMe",
													"shareFrom" : component.get('v.selectedTabId')
													},
				function(content, status) {
					if (status === "SUCCESS") {
						component.find('overlayLib').showCustomModal({
							body: content, 
							cssClass: "mymodal"                                       
						})
					}                               
				});
	},

	closeModal: function(component, event, helper) {
		helper.closePopupModal(component);
	},

	saveAlert: function(component, event, helper) {
		helper.saveAlertAndUpdateSearch(component);
	},

	handleSelect: function(cmp, e, h) {
		cmp.get('v.searchAlert').Alert_Frequency__c = e.target.value
		console.log(e.target.value);
	},
	
	changeEmailAlert: function(cmp, e, h) {
		cmp.get('v.searchAlert').Send_Email__c = e.target.checked;
		console.log(JSON.parse(JSON.stringify(cmp.get('v.searchAlert'))));
		console.log(cmp.get('v.searchAlert').Send_Email__c);
	},

	resetSaveSearch: function(component, event, helper){
		var titleField = component.find("saveSearchInput");
        
        titleField.set('v.value', "");
        titleField.set("v.errors", []);
        titleField.set("v.isError",false);
        $A.util.removeClass(titleField,"slds-has-error show-error-message");
	},
	summonModal: function (cmp, e, h) {
		h.openAlertModal(cmp, e);
	},
	passingMethod: function (cmp, e, h) {
		h.closeAlertModal(cmp, e);
	},

})