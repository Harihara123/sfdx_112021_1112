({

	getSavedSearches : function (component) {
		
		/*var UserGroup = component.get('c.checkGroupPermission');
		UserGroup.setCallback(this, function(response) {
			var response = response.getReturnValue();
			component.set("v.userInGroups",response);
			
            if (component.get("v.userInGroups.isInGroupB") !== true) { */
			// Call server to save search log entry
			var action = component.get("c.getUserSearches"); 
			action.setParams({
				'entryType' : [component.get('v.searchLogType')]
			});
			//background
			action.setBackground();
			action.setCallback(this, function(response) {
				var srchArray = [];
				if (response.getState() === "SUCCESS") {
					var result = response.getReturnValue();
					if (result !== "" && result !== null) {
						var parsedSearches = JSON.parse(result);
								
						for (var i=0; i<parsedSearches.searches.length; i++) {
							var srch = JSON.parse(parsedSearches.searches[i]);
							// Adding display only elements to the saved search object. 
							srch.time = $A.localizationService.formatDate(new Date(srch.execTime));
							if (srch.criteria.multilocation) {
								srch.loc = this.getMultiLocText(srch.criteria.multilocation);
							}
							if (!srch.engine) {
								srch.engine = "ES";
							}
							/* Start For story S-78203 & S-82094 */
						
								if(srch.type =='C2C' || srch.type =='C2R' ||srch.type =='R2C'){
									srch.sourceid = srch.match.srcid;
									if(srch.match.data){
										srch.sourceName = srch.match.data.srcName;
									} else {
										srch.sourceName = '';
									}
										  
								} else {
									srch.sourceid = '';
								}
						
							/*End for story S-78203 & S-82094 */

							srchArray.push(srch); 
						}

					}

				} else {
					console.log("Could not get saved searches!");
				}
				
				component.set('v.savedSearchList', srchArray);
			});
			$A.enqueueAction(action);
            /*}
				
		});
		$A.enqueueAction(UserGroup);	*/	
       
    },

    getMultiLocText : function(ml) {
        var txt = "";
            
        for (var i=0; i< ml.length; i++) {

             if(i == ml.length - 1){
                txt += ml[i].displayText;
            } else {
                txt += ml[i].displayText + " / ";
            }
            
        }
        return txt;
    },
   
    deleteSavedSearch : function(component,event) {
        var list = component.get("v.savedSearchList");
        var index = event.getSource().get("v.value");
        list.splice(index, 1);
        component.set("v.savedSearchList", list);

		var action = component.get("c.deleteSavedSearch"); 

        action.setParams({
            "entryType" : component.get('v.searchLogType'), //"AllSavedSearches",
            "index": index
        });

		action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
               	this.deleteToast();
            } else {
                console.log("Log delete failed!");
            }
        });
        $A.enqueueAction(action);
    },

    deleteToast: function() {
        var deleteToast = $A.get("e.force:showToast");

        deleteToast.setParams({
            type: "Success",
            message: "Your Saved Search has been successfully deleted!",
        });
        deleteToast.fire();
    },

    toUrl : function(component,event){
		var list = component.get("v.savedSearchList");
        var index = event.getSource().get("v.value");
        var searchtype = list[index].type;
		var engine = "";
        if (list[index].engine) {
            engine = list[index].engine;
        }

		let reqId = ((list[index]|| {})['match'] || {})['srcid'];
        let kword = ((list[index]|| {})['criteria'] || {})['keyword'];
        var trackingEvent = $A.get("e.c:TrackingEvent");
        if (searchtype === 'R2C') {
        trackingEvent.setParam('clickTarget', 'req-talent_match_button--' + reqId);            
        } else if (searchtype === 'C') {
        trackingEvent.setParam('clickTarget', 'talent_search_keyword--' + kword);                        
        } else if (searchtype === 'R') {
        trackingEvent.setParam('clickTarget', 'req_search_keyword--' + kword);                        
        }
        trackingEvent.fire();
        
       	var url; 
        if (searchtype === 'C' || searchtype === 'C2C' || searchtype === 'R2C') {
			url = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?c__execIndex=" +  index + "&c__savedType=" + component.get("v.searchLogType") + "&c__type=" +searchtype+"&c__engine=" + engine;		   
        } else if (searchtype === 'C2R' || searchtype === 'R' ) {
			url = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Search_Reqs?c__execIndex=" +  index + "&c__savedType="+ component.get("v.searchLogType") + "&c__type="+searchtype;
        }
    
        var utilityAPI = component.find("utilitybar");
        utilityAPI.minimizeUtility();
    	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },

	getSelectedSearchEntry : function (component, event) {
		var list = component.get("v.savedSearchList");
        var index = event.getSource().get("v.value");
		return list[index];
	},

    alertToast: function( message , type) {
        var toast = $A.get("e.force:showToast");

        toast.setParams({
            type: type,
            message: message,
        });
        toast.fire();
    },

    closePopupModal: function(component) {
        this.toggleClassInverse(component,'backdropSaveSearchModal','slds-backdrop--');
        this.toggleClassInverse(component,'saveSearchModal','slds-fade-in-');
        
    },

    openModal: function(component){
        this.toggleClass(component,'backdropSaveSearchModal','slds-backdrop--');
        this.toggleClass(component,'saveSearchModal','slds-fade-in-');
        
    },

    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
        //  console.log(modal);
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },

    saveAlertAndUpdateSearch : function(component, event) {

        var list = component.get("v.savedSearchList");
        list[component.get("v.searchReplaceIndex")] = component.get('v.searchEntry');
        component.set("v.savedSearchList",list);

        //let queryStringCmp = component.find("queryStringCmp");
        //let queryString = queryStringCmp.createQueryString( component.get('v.searchAlert.Alert_Criteria__c'), component.get('v.runningUser') );
        //component.set('v.searchAlert.Query_String__c', queryString );
        
        var action = component.get("c.saveUserSearchWithAlert"); 
        
        action.setParams({
            "searchEntry" : JSON.stringify( component.get('v.searchEntry') ),
            "replaceIndex" : component.get("v.searchReplaceIndex"),
            "entryType" : component.get("v.searchLogType"),
            "searchAlert" : component.get('v.searchAlert'),
        });

        console.log('^^^^^^^^^^^^^--> ' + JSON.stringify( component.get('v.searchAlert') ));

        action.setCallback(this, function(response) {
            // Reset the replace index to -1
            if (response.getState() === "SUCCESS") {
                // Server SUCCESS - save to attribute.
                this.alertToast('Successfully Created Alert..!', 'Success');
                var utilEvent = $A.get("e.c:E_RefreshUtilityEvent");
                utilEvent.fire();
                this.closeAlertModal(component, event);
                //this.closePopupModal(component);
            }else if(response.getState() === "ERROR"){
                this.handleError(response);
            }
        });

        $A.enqueueAction(action);

    },

    getUserAlertCount : function(component) {

        var action = component.get("c.getSearchAlertCount"); 
        
        action.setCallback(this, function(response) {

            if (response.getState() === "SUCCESS") {
                component.set('v.alertCount', response.getReturnValue() );
            }else if(response.getState() === "ERROR"){
                this.handleError(response);
            }
        });

        $A.enqueueAction(action);

    },

    openAlertModal: function (cmp, e) {
        const self = this;
        $A.createComponents([
                ["c:C_CreateAlertBody",{saveSearchTitle: cmp.get('v.saveSearchTitle')}],
                ["c:C_CreateAlertFooter",{
                    saveAlert: cmp.getReference("c.saveAlert"),
                    handleSelect: cmp.getReference("c.handleSelect"),
                    changeEmailAlert: cmp.getReference("c.changeEmailAlert"),
                    disabledCreateAlert: true,
                    closeModal: cmp.getReference("v.closeAlertModal")
                }
                ]
            ],
            function(components, status){
                if (status === "SUCCESS") {
                    let modalBody = components[0];
                    let modalFooter = components[1];
                    cmp.find('overlayLib').showCustomModal({
                        header: "Create an Alert",
                        body: modalBody,
                        footer: modalFooter,
                        showCloseButton: true,
                        cssClass: "my-modal,my-custom-class,my-other-class",
                        closeCallback: function() {

                        }
                    });

                }else if (status === "ERROR") {
                    self.alertToast('Some Error Occurred, Please Contact Your Administrator.!', 'Error');
                }
            }
        );
    },
    closeAlertModal: function (cmp, e) {
        cmp.set('v.closeAlertModal', !cmp.get('v.closeAlertModal'));
    },

     handleError : function(response){
        
       // Use error callback if available
        if (typeof errCallback !== "undefined") {
            errCallback.call(this, response.getReturnValue());
            // return;
        }
        // Fall back to generic error handler
        var errors = response.getError();
        if (errors) {
            
            if (errors[0] && errors[0].message) {
                this.showError(errors[0].message);
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
            }else{
                this.showError('An unexpected error has occured. Please try again!');
            }
        } else {
            this.showError(errors[0].message);
            //throw new Error("Unknown Error");
        }
    },            

    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

    getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var r = JSON.parse(response.getReturnValue());
				component.set ("v.runningUser", r);
            }
        });
        $A.enqueueAction(action);
    },

	//Added w.r.t STORY S-132137
	//To check user group B permission
	CheckGroupBUser : function(component) {
        var UserGroup = component.get('c.checkGroupPermission');
		UserGroup.setCallback(this, function(response) {
			var response = response.getReturnValue();
			component.set("v.userInGroups",response);
		});
		$A.enqueueAction(UserGroup);
			
    },

	createQueryStringHelper : function (component, isReexecute, noFacets) {
        var searchEntry = component.get("v.searchEntry");
        var sortField ='';
        var sortOrder ='';
        var excludeKeyWords ='';
        var automatchSourceId ='';
        if(searchEntry.exclude){
            excludeKeyWords = searchEntry.exclude;
        }
        
        if (searchEntry.sort) {
            sortField = searchEntry.sort;
            sortOrder = searchEntry.order;
        }
        if(searchEntry.match){
			automatchSourceId = searchEntry.match.srcid;
        }
        
        var apiHelper = component.find("apiHelper");
        return apiHelper.createQueryString({
            type: searchEntry.type, 
            userId: component.get("v.runningUser.id"), 
            keyword: searchEntry.criteria.keyword, 
            pageSize: searchEntry.pageSize, 
            offset: searchEntry.offset, 
            grpFilter: component.get("v.runningUser.opcoSearchFilter"), 
            matchSrcId: automatchSourceId, 
            requestId: '', // have doubt about this 
            transactionId: '', // doubt about this 
            appId: component.get("v.searchAppId"), 
            opco: component.get("v.runningUser.opco"), 
            location: searchEntry.criteria.multilocation, 
            facetProps: searchEntry.fdp, 
            filterInitiator: null, 
            userPsId: component.get("v.runningUser.psId"), 
            filterCriteriaParams: searchEntry.criteria.filterCriteriaParams, 
            selectedFacets: searchEntry.criteria.facets, 
            noSearchYet: false, // have doubt about this
            isReexecute: isReexecute, 
            noFacets: noFacets,
            sortField: sortField, 
            sortOrder: sortOrder, 
            excludeKeyWords: excludeKeyWords
        });
    },
    
    
                
    createMatchRequestBody: function (component) {
        var searchParams = component.get("v.searchEntry");

        var apiHelper = component.find("apiHelper");
        return apiHelper.createMatchRequestBody({
            matchVectors: searchParams.matchVectors, 
            excludeKeyWords: searchParams.exclude
        });
    },
     

})