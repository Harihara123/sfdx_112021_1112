({
	doInit:	function(component, event, helper) {
		var uid = component.get("v.uid");
		// If nested in a C_SearchLocationInputPopover, uid will be passed in and same needs to be used.
		if (!uid || uid === "") {
			component.set("v.uid", "gglLkp" + Math.floor(Math.random() * 100000000) + "_");
        }
        // component.find("radiusElementId").set("v.value", 30);
       
	},

    googlePlaceSelected : function(component, event, helper) {
        // console.log(JSON.stringify(event.getParams()));
        helper.updatePlaceAttributes(component, event.getParams());
        helper.setUseRadius(component);
        helper.generateValueObject(component);
        event.stopPropagation();
        helper.fireLocationUpdateEvent(component);

    },

    googlePlaceCleared : function(component, event, helper) {
        // console.log("Google place cleared!");
        helper.clearPlaceAttributes(component);
        // helper.disableRadius(component);
        component.set("v.useRadius", "NONE");
        helper.clearValueObject(component);

    },

    multiValueUpdateHandler : function(component, event, helper) {
        let subId = helper.guid();
        component.set('v.subId', subId);
        if (!component.get("v.valueMap")) {
            // No valueMap => reexecuted search / fresh automatch. Create map from valuesMulti
            helper.multiValuesToMap(component);
            helper.valueMapToMain(component);
        }
        if (component.get("v.valuesMulti.length") > 1) {
            // Update pills if length > 1
            helper.redrawPills(component, component.get("v.valuesMulti"));
            // Disable the location inputs
            component.set("v.isDisabled", true);
        } else {
            // Enable the location inputs
            component.set("v.isDisabled", false);
        }
    },

    valueUpdateHandler : function(component, event, helper) {
        // Update useRadius on the component.
        if (component.get("v.value.useRadius")) {
            component.set("v.useRadius", component.get("v.value.useRadius"));
        }

        // If the value for radius / unit has changed AND useRadius is SELECT, update the drop downs.
        if (component.get("v.value")) {
            if (component.get("v.useRadius") === 'SELECT' &&
                ((component.get("v.value.geoRadius") != component.get("v.radius"))
                    || (component.get("v.value.geoUnitOfMeasure") != component.get("v.radiusUnit")))) {
                component.set("v.radius", component.get("v.value.geoRadius").toString());
                component.set("v.radiusUnit", component.get("v.value.geoUnitOfMeasure"));
            }
        }

        // If this is a multi value container, update the valuesMulti attribute
        if (component.get("v.multiInput") > 1) {
            component.set("v.vu", true);
            if (!component.get("v.mvu")) {
                var vm = component.get("v.valueMap");
                if (component.get("v.value")) {
                    if (!vm || Object.keys(vm).length === 0) {
                        // Value available and (valueMap empty / radius update )
						var x = {};
						x[component.get("v.uid") + "0"] = component.get("v.value");
                        component.set("v.valueMap", x);
                    } else {
                        // value updated in top level google box, but was originally moved up 
                        // from one of the lookups in the popover. Replace same in valueMap.
                        var k = Object.keys(vm);
                        if (k.length === 1) {
                            vm[k[0]] = component.get("v.value");
                            component.set("v.valueMap", vm);
                        }
                    }
                } else if (component.get("v.value") === null) {
                    // value === null => google place was cleared. clear valueMap
                    component.set("v.valueMap", null);
                }

                var mv = helper.generateMultiValuesObject(component);
                component.set("v.valuesMulti", mv);
            }
            component.set("v.vu", false);
        }
    },

    radiusUpdateHandler : function(component, event, helper) {
        component.set("v.ru", true);
        component.set("v.value.geoRadius", component.get("v.radius"));
        component.set("v.ru", false);
        helper.fireLocationUpdateEvent(component);
    },

    unitUpdateHandler : function(component, event, helper) {
        component.set("v.ru", true);
        component.set("v.value.geoUnitOfMeasure", component.get("v.radiusUnit"));
        component.set("v.ru", false);
        helper.fireLocationUpdateEvent(component);
    },

    popoverEventHandler: function(component, event, helper) {
        component.set("v.isDisabled", !event.getParam("popoverClosed"));

        if (event.getParam("popoverClosed")) {
            component.set("v.valuesMulti", helper.generateMultiValuesObject(component));
            helper.valueMapToMain(component);
        } 
    },

    lookupSigHandler : function(component, event, helper) {
        // if lookupSig === 1, clear value.
        var c = component.get("v.lookupSig");
        if (c[component.get("v.multiIndex")] === 1) {
            component.set("v.value", {});
        }
    },

    handlePillClosed : function(component, event, helper) {
        helper.removeLocation(component, event.getParam("location"));
        component.set("v.valuesMulti", helper.generateMultiValuesObject(component));
        helper.valueMapToMain(component);
        event.stopPropagation();
    },

	clearLocation: function(component, event, helper) {
		component.set("v.valuesMulti", null);
		component.set("v.valueMap", null);
	},

    togglePills: function(component, event, helper) {
       helper.togglePills(component);
       helper.redrawPills(component, component.get("v.valuesMulti"));
    },

    togglePillContainer: function(component, event, helper) {
        let limitPills = component.get("v.keywordFocus"),
            pillsToShow = limitPills ? 1 : 3,
            locations = component.get("v.valuesMulti");
        
        if(locations !== undefined){
             component.set("v.pillsToShow", pillsToShow);

            if(component.get("v.isPillHidden") === false){
                helper.togglePills(component);
            } 
            helper.redrawPills(component, locations);
        }
       
       
    }

})