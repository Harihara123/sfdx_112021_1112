({
    updatePlaceAttributes : function(component, selection) {
        component.set("v.latlong", this.translateLatLong(selection.latlong));
        component.set("v.viewport", selection.viewport);
        component.set("v.placeTypes", selection.placeTypes);
        component.set("v.displayText", selection.displayText);
        component.set("v.addrComponents", selection.addrComponents);
        component.set("v.autoRadius", selection.autoRadiusKm);
    },

    generateValueObject : function(component) {
        var city, state, country, postcode;

        var addr = component.get("v.addrComponents");
        for (var i=0; i<addr.length; i++) {
            if (addr[i].types.indexOf("postal_code") >= 0) {
                postcode = addr[i].long_name;
            }
            if (addr[i].types.indexOf("locality") >= 0) {
                city = addr[i].long_name;
            }
            if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
                state = addr[i].long_name;
            }
            if (addr[i].types.indexOf("country") >= 0) {
                country = addr[i].long_name;
            }
        }

        var v = {
            "displayText" : component.get("v.displayText"),
            "latlon" : component.get("v.latlong"),
            // "geoRadius" : component.get("v.useRadius") ? component.get("v.radius") : component.get("v.autoRadius"),
            "geoRadius" : component.get("v.useRadius") === "AUTO" ? component.get("v.autoRadius") : component.get("v.radius"),
            // "geoUnitOfMeasure" : component.get("v.useRadius") ? component.get("v.radiusUnit") : component.get("v.autoRadiusUnit"),
            "geoUnitOfMeasure" : component.get("v.useRadius") === "AUTO" ? component.get("v.autoRadiusUnit") : component.get("v.radiusUnit"),
            // "filter" : !component.get("v.alwaysUseRadius") || (component.get("v.useRadius") === "NONE"),
            "useRadius" : component.get("v.useRadius"),
            "alwaysUseRadius" : component.get("v.alwaysUseRadius"),
            "city" : city,
            "state" : state,
            "country" : country,
            "postalcode" : postcode
        };
        component.set("v.value", v);
    },

    clearValueObject : function(component) {
        component.set("v.value", null);
    },

    generateMultiValuesObject : function(component) {
        var multiVal = [];
        var valueMap = component.get("v.valueMap");
        if (valueMap) {
            var k = Object.keys(valueMap);
            for (var j=0; j<k.length; j++) {
                // Only push into the location array if not null.
                if (valueMap[k[j]] !== null) {
                    multiVal.push(valueMap[k[j]]);
                }
            }
        }

        return multiVal;
    },

    multiValuesToMap : function(component) {
        var mv = component.get("v.valuesMulti");
        if (mv) {
            var vmap = {};
            for (var i=0; i<mv.length; i++) {
                vmap[component.get("v.uid") + i] = mv[i];
            }
            component.set("v.valueMap", vmap);
        }
    },

    valueMapToMain : function(component) {
        var vmap = component.get("v.valueMap");
        if (vmap) {
            var k = Object.keys(vmap);
            if (k.length === 0) {
                component.set("v.value", null);
            } else {
                for (var i=0; i<3; i++) {
                    if (vmap[component.get("v.uid") + i]) {
                        component.set("v.value", vmap[component.get("v.uid") + i]);
                        break;
                    }
                }
            }
        }
    },

    removeLocation : function(component, lText) {
        // Remove the location from map if location text matches value.displayText
        var vmap = component.get("v.valueMap");
        if (vmap) {
            var k = Object.keys(vmap);
            for (var i=0; i<k.length; i++) {
                if (vmap[k[i]].displayText === lText) {
                    delete vmap[k[i]];
                    break;
                }
            }
            component.set("v.valueMap", vmap);
        }
    },

    clearPlaceAttributes : function(component) {
        component.set("v.latlong", []);
        component.set("v.viewport", {});
        component.set("v.placeTypes", []);
        component.set("v.displayText", "");
        component.set("v.addrComponents", []);
        component.set("v.autoRadius", -1);
    },

    translateLatLong : function(coords) {
        return [coords.lat,coords.lng];
    },

    /*enableRadius : function (component) {
        component.set("v.useRadius", true);
    },

    disableRadius : function (component) {
        component.set("v.useRadius", false);
    },*/

    disableGoogleInput :function(component, event){
        component.set("v.isDisabled", true);
    },

    resetRadiusVals : function (component) {
        component.set("v.autoRadius", "-1");
    },

    updateStateCountryText : function (component) {
        var comps = component.get("v.addrComponents");
        var txt = "";
        for (var i=0; i<comps.length-1; i++) {
            txt += comps[i].long_name + ", ";
        }
        txt += comps[comps.length - 1].long_name;

        component.set("v.displayText", txt);
    },

    /** Haversine function. Code referenced from https://rosettacode.org/wiki/Haversine_formula#JavaScript 
    * Usage:  "viewportDiag" : this.calculateViewportDiagonal(viewport.southwest.lat,
    *                                                  viewport.southwest.lng,
    *                                                  viewport.northeast.lat,
    *                                                  viewport.northeast.lng)
    */
    calculateDiagonal : function(viewport) {
        var radians = Array.prototype.map.call(arguments, function(deg) { return deg/180.0 * Math.PI; });
        var lat1 = radians[0], lon1 = radians[1], lat2 = radians[2], lon2 = radians[3];
        var R = 6372.8; // km
        var dLat = lat2 - lat1;
        var dLon = lon2 - lon1;
        var a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLon / 2) * Math.sin(dLon /2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    },

    setAutoRadius : function(component) {
        component.set("v.autoRadius",  Math.round(this.getViewportDiagonal(component) / 2));
    },

    setUseRadius : function(component) {
        var placeTypes = component.get("v.placeTypes"); 
        /* if "alwaysUseRadius" is set to true, use lat long and radius for state / country searches as well.
         * This will work well in the case when most of the data has lat long populated. e.g. Req search.
         */
        if (placeTypes.indexOf("administrative_area_level_1") >= 0 || placeTypes.indexOf("country") >= 0) {
            if (component.get("v.alwaysUseRadius")) {
                // USe auto radius for state and country values if so configured via "alwaysUseRadius".
                component.set("v.useRadius", "AUTO");
            } else {
                // Use a text filter, not radius for state or country
                component.set("v.useRadius", "NONE");
            }
        } else {
            if (placeTypes.indexOf("colloquial_area") >= 0) {
                // Use radius for colloquial area e.g. Bay Area, Southern California, but disable the selection
                component.set("v.useRadius", "AUTO");
            } else {
                // SMARTER OPTIONS: Maybe check for size of viewport and use radius if viewport less than a threshold?
                // this.enableRadius(component);
                component.set("v.useRadius", "SELECT");
            }
        }
    },

    getViewportDiagonal : function(component) {
        var viewport = component.get("v.viewport");
        var ne = viewport.northeast;
        var sw = viewport.southwest;
        return this.calculateDiagonal(sw.lat, sw.lng, ne.lat, ne.lng);
    },

    getPillParams : function(location) {
        return {
            "location" : location.displayText,
            "radius" : location.geoRadius,
            "unit" : location.geoUnitOfMeasure,
            // "showDistance" : !location.filter
            "showDistance" : location.useRadius === "SELECT"
        };
    },

    redrawPills : function(component, locations) {
        var pillsArray = [];
        
        var pillCount = 0;
        var pillsToShow = component.get('v.pillsToShow');
        var isPillHidden = component.get('v.isPillHidden');
        
        for (var i=0; i<locations.length; i++) {
            var cmpParams = this.getPillParams(locations[i]);

            pillCount++;

            if(isPillHidden && pillCount > pillsToShow){
                break;
            }

            $A.createComponent("c:C_SearchLocationPill", 
                cmpParams, 
                function(newComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        // SUCCESS  - insert component in markup
                        pillsArray.push(newComponent);
                        component.set("v.locationPills", pillsArray);
                    } else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.");
                        // Show offline error
                    } else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
                }
            );
        }

        if( pillCount > pillsToShow){
            component.set("v.showMoreLess", isPillHidden ? $A.get("$Label.c.ATS_SHOW_MORE") : $A.get("$Label.c.ATS_SHOW_LESS"));       
        }else {
            component.set("v.showMoreLess", "");
        }

        // When the last pill is removed, nothing is set in the for loop
        if (pillsArray.length === 0) {
            component.set("v.locationPills", pillsArray);
        }
        
    },

    fireLocationUpdateEvent : function(component) {
        var updateEvt = component.getEvent("locationUpdateEvt");
        updateEvt.setParams({
            "lookupId" : component.get("v.uid") + component.get("v.multiIndex"),
            "location" : component.get("v.value")
        });
        updateEvt.fire();
    },

    fireGoogleClearEvent : function(component) {
        var clearEvt = component.getEvent("placeCleared");
        clearEvt.setParams({
            "lookupId" : component.get("v.uid") + component.get("v.multiIndex")
        });
        clearEvt.fire();
    },

    togglePills: function(component) {
        var isPillHidden = component.get('v.isPillHidden');

        component.set("v.isPillHidden", !isPillHidden);
    },

    guid: function() {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
            this.s4() + '-' + this.s4() + this.s4() + this.s4();
    },
    
    s4: function () {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
})