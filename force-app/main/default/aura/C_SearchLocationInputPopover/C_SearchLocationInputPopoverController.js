({
	doInit : function(component, event, handler) {
		var c = parseInt(component.get("v.count"));
		var clearers = [];
		var countList = [];
		for (var i=0; i<c; i++) {
			countList.push(i);
			clearers.push(0);
		}
		component.set("v.countList", countList);
		component.set("v.clearers", clearers);
	},

	valueMapUpdateHandler : function(component, event, helper) {
		var vm = component.get("v.valueMap");
		
		if (vm) {
			component.set("v.value0", vm[component.get("v.uid") + "0"] ? vm[component.get("v.uid") + "0"] : null);
			component.set("v.value1", vm[component.get("v.uid") + "1"] ? vm[component.get("v.uid") + "1"] : null);
			component.set("v.value2", vm[component.get("v.uid") + "2"] ? vm[component.get("v.uid") + "2"] : null);
			component.set("v.value3", vm[component.get("v.uid") + "3"] ? vm[component.get("v.uid") + "3"] : null);
			component.set("v.value4", vm[component.get("v.uid") + "4"] ? vm[component.get("v.uid") + "4"] : null);
		} else {
			component.set("v.value0", null);
			component.set("v.value1", null);
			component.set("v.value2", null);
			component.set("v.value3", null);
			component.set("v.value4", null);
		}
	},

	togglePopover : function(component, event, helper){
		var popoverToggle = !component.get("v.showPopover");
		component.set("v.showPopover", popoverToggle);
		
		helper.firePopoverClicked(component);
	},

	hidePopover: function(component, event, helper){
		component.set("v.showPopover", false);
		
		helper.firePopoverClicked(component);
	},

	addLocations: function(component, event, helper){
        //console.log("Adding locations to search ... ");
        // helper.fireLocationsAddedEvent(component);
        component.set("v.showPopover", false);
        helper.firePopoverClicked(component);
	},

	removeLocation: function(component, event, helper){
		component.set("v.showPopover", false);
	},

	clearAllLocations : function(component, event, helper){
		helper.clearAllLocations(component);
		component.set("v.showPopover", false);
		// helper.fireLocationsAddedEvent(component);
        helper.firePopoverClicked(component);
	},

	googlePlaceSelected : function(component, event, helper) {
        //console.log("Google place selected on # " + event.getParam("lookupId"));
        helper.updateValueMap(component, event.getParams());
        //console.log("po set " + JSON.stringify(component.get("v.valueMap")));
        event.stopPropagation();
    },

    locationUpdateHandler : function(component, event, helper) {
        //console.log("Location updated on # " + event.getParam("lookupId"));
        helper.updateValueMap(component, event.getParams());
        //console.log("po set " + JSON.stringify(component.get("v.valueMap")));
    },

	googlePlaceCleared : function(component, event, helper) {
        //console.log("Google place CLEARED on # " + event.getParam("lookupId"));
        helper.cleanValueMap(component, event.getParam("lookupId"));
        //console.log("po clear " + JSON.stringify(component.get("v.valueMap")));
        event.stopPropagation();
    }

})