({	
    updateValueMap : function(component, selection) {
        // console.log("Update value map for = " + selection.lookupId);
        var v = component.get("v.valueMap");
        if (v === undefined || v === null) {
            v = {};
        }

        v[selection.lookupId] = selection.location;
        component.set("v.valueMap", v);
        // console.log(JSON.stringify(v));
    },

    cleanValueMap : function(component, key) {
        // console.log("Clean value map for = " + key);
        var v = component.get("v.valueMap");
		if (v && v[key]) {
			delete v[key];
			component.set("v.valueMap", v);
		}
        // console.log(JSON.stringify(v));
    },

    clearAllLocations : function(component){
        component.set("v.valueMap", {});
        // Also clear all the individual lookups
        var clearers = component.get("v.clearers");
        for (var i=0; i<clearers.length; i++) {
            clearers[i] = 1;
        }
        component.set("v.clearers", clearers);
        for (var i=0; i<clearers.length; i++) {
            clearers[i] = 0;
        }
        component.set("v.clearers", clearers);
    },

    restoreAllLocations : function(component){
        // component.set("v.valueMap", {});
        // Set restore signal for all the lookups.
        var clearers = component.get("v.clearers");
        for (var i=0; i<clearers.length; i++) {
            clearers[i] = 2;
        }
        component.set("v.clearers", clearers);
        for (var i=0; i<clearers.length; i++) {
            clearers[i] = 0;
        }
        component.set("v.clearers", clearers);
    },

    firePopoverClicked : function(component) {
        var clickEvt = component.getEvent("popoverClickedEvt");
        
        clickEvt.setParams({
            "popoverClosed" : !component.get("v.showPopover")
        });
        clickEvt.fire();
    }

})