({
	removePill : function(component, event, helper) {
		var pillClosedEvt = component.getEvent("pillClosed");
		pillClosedEvt.setParams({
			"location" : component.get("v.location") 
		});
		pillClosedEvt.fire();
	}

})