({
    init: function(component, event, helper) {
        console.log(component.get('v.switchTracking'));
        console.log('pew')
        let switchTracking = localStorage.getItem('switchTracking');
        // if component attr is true and localStorage attr is true
        if ((component.get('v.switchTracking') && switchTracking)
            // or if component attr is true and localStorage attr is not present
            || (component.get('v.switchTracking') && switchTracking === undefined)) {
            helper.init(component);
        } else {
            // otherwise either component attr or localSotrage attr is false
            component.set('v.switchTracking', false);
            localStorage.setItem('switchTracking', false);
            let body = document.querySelectorAll('body')[0];
            body.onkeydown = (e) => {
                if (e.shiftKey && e.altKey && e.keyCode === 83) {
                    // Shift + Alt + S - toggle switch
                    localStorage.setItem('switchTracking', !component.get('v.switchTracking'));
                    component.set('v.switchTracking', !component.get('v.switchTracking'));
                }
            };
        }
    },

    toggleView: function(component, e, helper) {
        if (component.get('v.switchTracking')) {
            helper.toggleView(component);
        }
    },

    captureClick: function(component, e, helper) {
        if (component.get('v.switchTracking')) {
            helper.captureClick(component, e);
        }
    },

    captureChange: function(component, e, helper) {
        if (component.get('v.switchTracking')) {
            helper.captureChange(component, e);
        }
    },

    downloadFile: function(component, e, helper) {
        if (component.get('v.switchTracking')) {
            helper.downloadFile(component, e);
        }
    },

    uploadFile: function(component, e, helper) {
        if (component.get('v.switchTracking')) {
            helper.uploadFile(component, e);
        }
    },

    analyzeClicks: function(component, e, helper) {
        if (component.get('v.switchTracking')) {
            helper.analyzeClicks(component, e);
        }
    },

    addFunnel: function(component, e, helper) {
        if (component.get('v.switchTracking')) {
            // TODO funnel should allow setting an action that determines session success
            component.set('v.state.funnelEdit', !component.get('v.state.funnelEdit'));
        }
    },

    uploadClick: function(component, e, helper) {
        if (component.get('v.switch')) {
            document.getElementById('real-input').click();
        }
    },

    analyzeQuery: function(component, e, helper) {
        if (component.get('v.switch')) {
            helper.analyzeQuery(component, e);
        }
    },

    setSuccess: function(component, e, helper) {
        if (component.get('v.switch')) {
            helper.setSuccess(component, e)
        }
    },

    updateResults: function(component, e, helper) {
        if (component.get('v.switch')) {
            let newState = component.get('v.state');
            let searchTriggerIndex = newState['session']['interactions'].findIndex(el => el.dataTracker === 'search_button');
            if (searchTriggerIndex !== -1) {
                let filteredResults = component.get('v.results.records').map(el => {
                    return {'id': el.id, 'score': el.score}
                });
                newState['session']['interactions'][searchTriggerIndex].results = filteredResults;
                newState['session']['interactions'][searchTriggerIndex].reqTime = new Date().getTime() - component.get('v.state.session.lastClickTime');
                component.set('v.state', newState);
            }
        }
    }
})