({
    init: function(component, event) {
        // save session params
        let sessionTime = new Date().getTime();
        component.set('v.state.session.sessionTime', sessionTime);
        let windowWidth = window.innerWidth;
        let windowHeight = window.innerHeight;
        component.set('v.state.session.screenSize.width', windowWidth);
        component.set('v.state.session.screenSize.height', windowHeight);


        if (component.get('v.tracking') === 'plugin') {
            // TODO plugin logic
            console.log('tracking plugin initialized');

            let body = document.querySelectorAll('body')[0];
            body.onkeydown = (e) => {
                if (e.shiftKey && e.altKey && e.keyCode == 68) {
                    // Shift + Alt + D - download file
                    var element = document.createElement('a');
                    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(
                        JSON.stringify(component.get('{!v.state.session}'))
                    ));
                    element.setAttribute('download', 'userMetrics');
                    element.style.display = 'none';
                    document.body.appendChild(element);
                    element.click();
                    document.body.removeChild(element);
                } else if (e.shiftKey && e.altKey && e.keyCode === 86) {
                    // Shift + Alt + V - toggle view
                    let container = document.getElementById('tracking-container');
                    if (+container.style.opacity) {
                        container.style.opacity = '0';
                        container.style.zIndex = -99999;
                    } else {
                        container.style.opacity = '1';
                        container.style.zIndex = 99999;
                    }
                }
            };


            let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            svg.setAttribute('id', 'tracking-canvas');
            svg.style.background = 'rgba(0,0,0,.2)';
            svg.style.position = 'fixed';
            svg.style.top = '0';
            svg.style.right = '0';
            svg.style.bottom = '100px';
            svg.style.left = '0';
            svg.style.width = '100%';
            svg.style.height = '100%';

            // cannot use a class here to toggle tracking canvas as it belongs to body object
            // hence setting a class in the tracking component will not help
            // toggle opacity + z-index instead

            let container = document.createElement('div');
            container.setAttribute('id', 'tracking-container');
            container.style.position = 'fixed';
            container.style.top = '0';
            container.style.right = '0';
            container.style.bottom = '100px';
            container.style.left = '0';
            container.style.width = '100%';
            container.style.height = '100%';

            container.appendChild(svg);
            body.appendChild(container);

            // capture click
            body.onclick = (e) => {
                e.stopPropagation();
                if (e.target) {

                    let limit = 4; // bubble up to 5 levels up
                    let target = e.target;
                    if (target.nodeName === 'path' || target.nodeName === 'svg') {
                        limit = 6; // for buttons with nested icons bubble up to 7 levels up
                    }
                    let i = 0;

                    if (
                        e.target.nodeName === 'BODY'
                        || (e.target.nodeName === 'A' && !e.target.innerHTML)
                        || e.target.nodeName === 'tracking-canvas'
                        || e.target.nodeName === 'tracking-container'
                    ) { return }
                    else if ( e.target.nodeName !== 'INPUT'
                              && e.target.nodeName !== 'TEXTAREA'
                              // data-type should be input-text or input-check
                              && e.target.getAttribute('data-type').split('-')[0] !== 'input'
                            ) {

                        console.log('onclick');

                        if (!target.getAttribute('data-tracker')) {
                            while (i < limit && !target.getAttribute('data-tracker')) {
                                target = target.parentElement;
                                i++;
                                if (target.getAttribute('data-tracker')) {
                                    break
                                }
                            }
                        }
                            if (!target) {
                                return
                            }

                        let targetDiv = target.getAttribute('data-tracker');
                        if (!targetDiv) {
                            return
                        }

                        let currentInteractions = [...component.get('v.state.session.interactions')];
                        let lastDataType = currentInteractions[currentInteractions.length - 1]['dataType'];

                        if (lastDataType.split('-')[0] === 'input') {
                            let lastDataTracker = currentInteractions[currentInteractions.length - 1]['dataTracker'];
                            let lastWrapper = document.querySelectorAll(`[data-tracker="${lastDataTracker}"]`)[0];
                            console.log(lastWrapper.querySelector('input').value)
                        }

                        // // HACK: Location search picklist updates input value upon clicking on the ul below
                        // // value is updated after a short delay
                        // // code below fetches it and updating tracker's state
                        // if (target.getAttribute('data-type') === 'picklist') {
                        //     setTimeout(() => {
                        //         let currentInteractions = [...component.get('v.state.session.interactions')];
                        //         let lastDataTracker = currentInteractions[currentInteractions.length - 1]['dataTracker'];
                        //
                        //         currentInteractions[currentInteractions.length - 1].input = lastWrapper.value;
                        //         component.set('v.state.session.interactions', currentInteractions);
                        //     }, 300);
                        //         return;
                        // }


                        let divClick = {
                            dataTracker: targetDiv,
                            dataType: target.getAttribute('data-type'),
                            delayTime: new Date().getTime() - component.get('v.state.session.lastClickTime'),
                            sequence: component.get('v.state.session.interactions').length + 1,
                            input: null
                        };

                        let newDivsClicked = component.get('v.state.session.divsClicked');
                        newDivsClicked.push(targetDiv);

                        component.set('v.state.session.interactions', [...component.get('v.state.session.interactions'), divClick]);
                        component.set('v.state.session.lastClickTime', new Date().getTime());
                        component.set('v.state.session.divsClicked', newDivsClicked);
                        this.drawVectors(targetDiv, component);

                    } else if (

                        e.target.nodeName === 'INPUT'
                        || e.target.nodeName === 'TEXTAREA'
                        || e.target.getAttribute('data-type').split('-')[0] === 'input') {

                        console.log('onchange');

                        let delayTime = new Date().getTime() - component.get('v.state.session.lastClickTime');

                        if (!target.getAttribute('data-tracker')) {
                            while (i < limit && !target.getAttribute('data-tracker')) {
                                target = target.parentElement;
                                i++;
                                if (target.getAttribute('data-tracker')) {
                                    break
                                }
                            }
                        }


                        let targetDiv = target.getAttribute('data-tracker');
                        if (!targetDiv) {
                            return
                        }


                            let divClick = {
                                dataTracker: targetDiv,
                                dataType: target.getAttribute('data-type'),
                                delayTime: delayTime,
                                sequence: component.get('v.state.session.interactions').length + 1,
                                input: null
                            };

                            let newDivsClicked = component.get('v.state.session.divsClicked');
                            newDivsClicked.push(targetDiv);

                            component.set('v.state.session.interactions', [...component.get('v.state.session.interactions'), divClick]);
                            component.set('v.state.session.lastClickTime', new Date().getTime());
                            component.set('v.state.session.divsClicked', newDivsClicked);
                            this.drawVectors(targetDiv, component);
                        };
                    }


            };

            }

         else if (component.get('v.tracking') === 'wrapper') {
            // TODO wrapper logic
            console.log('tracking wrapper initialized');

            let body = document.querySelectorAll('body')[0];
            body.onkeydown = (e) => {
                if (e.shiftKey && e.altKey && e.keyCode == 77) {
                    var element = document.createElement('a');
                    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(
                        JSON.stringify(component.get('{!v.state.session}'))
                    ));
                    element.setAttribute('download', 'userMetrics');
                    element.style.display = 'none';
                    document.body.appendChild(element);
                    element.click();
                    document.body.removeChild(element);
                }
            };

            let canvas = document.getElementById('canvas');
            canvas.style.position = "absolute";
            canvas.style.opacity = '0';
            let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            svg.setAttribute('id', 'svg');
            svg.setAttribute('position', 'fixed');
            svg.setAttribute('width', '100vw');
            svg.setAttribute('height', '100vh');
            svg.setAttribute('z-index', '-99');
            svg.style.top = '0';
            svg.style.left = '0';

            canvas.appendChild(svg);

            component.set('v.state.lastClickTime', new Date().getTime());
        } else {
            console.log('tracking is disabled');
        }
    },

    toggleView: function(component) {
        if (component.get('v.tracking') === 'wrapper') {
            let canvas = document.getElementById('canvas');
            if (canvas.classList.value.split(' ').find((el) => el === 'top')) {
                canvas.style.opacity = '0';
                canvas.classList.remove('top');
                canvas.style.background = 'none';
                let backdrop = document.getElementById('queryBackdrop');
                if (backdrop) {
                    canvas.removeChild(backdrop);
                }
            } else {
                canvas.style.opacity = '1';
                canvas.classList.add('top');
            }
        } else if (component.get('v.tracking') === 'plugin') {
            let body = document.querySelectorAll('body')[0];
            let canvas = document.getElementById('tracking-canvas');
            // if canvas is visible -> remove & return
            if (document.getElementById('tracking-canvas')) {
                body.removeChild(canvas);
            } else {
                let canvas = document.createElement('div');
                canvas.setAttribute('id', 'tracking-canvas');
                canvas.style.position = 'absolute';
                canvas.style.zIndex = '9998'; //analytics block on top
                canvas.style.top = '0';
                canvas.style.right = '0';
                canvas.style.bottom = '80px';
                canvas.style.left = '0';
                canvas.style.background = 'rgba(0,0,0,.5)';
                body.appendChild(canvas);
            }
        }
    },


    // ****************************************************************
    // ****************************************************************
    // ****************************************************************
    // ONCLICK

    captureClick: function(component, e) {
        if (!component.get('v.state.session.editMode')) {

            // if the following condition is met, clicked el should be handled with onchange method
            if (e.target.nodeName === 'INPUT' || e.target.nodeName === 'TEXTAREA') {
                return;
            }

            e.stopPropagation();

            if (!component.get('v.state.funnelEdit')) {

                let targetDiv;
                let divClick;

                if (e.target.getAttribute('data-tracker')) {
                    // if clicked element has data-tracker on it, create object based on the clicked element metadata
                    targetDiv = e.target.getAttribute('data-tracker');
                    if (targetDiv) {
                        divClick = {
                        dataTracker: targetDiv,
                        divType: e.target.nodeName.toLowerCase(),
                        delayTime: new Date().getTime() - component.get('v.state.session.lastClickTime'),
                        sequence: component.get('v.state.session.interactions').length + 1,
                        input: (e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea') ? e.target.value : null
                    }   
                    }
                    // path is nested in svg, which is then nested in a div. If node clicked is path - go 2 levels up
                } else if (e.target.nodeName === 'PATH') {
                    targetDiv = e.target.parentElement.parentElement.getAttribute('data-tracker');
                    if (targetDiv) {
                    divClick = {
                        dataTracker: targetDiv,
                        divType: e.target.parentElement.parentElement.nodeName.toLowerCase(),
                        delayTime: new Date().getTime() - component.get('v.state.session.lastClickTime'),
                        sequence: component.get('v.state.session.interactions').length + 1,
                        input: (e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea') ? e.target.value : null
                    }                        
                    }
                } else {
                    // if clicked element does not have data-tracker -> bubble one level up to check parent
                    targetDiv = e.target.parentElement.getAttribute('data-tracker');
                    if (targetDiv) {
                     divClick = {
                        dataTracker: targetDiv,
                        divType: e.target.parentElement.nodeName.toLowerCase(),
                        delayTime: new Date().getTime() - component.get('v.state.session.lastClickTime'),
                        sequence: component.get('v.state.session.interactions').length + 1,
                        input: (e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea') ? e.target.value : null
                    }   
                    }
                }
                if (
                    // check if data-tracker of a clicked el belongs to service container
                    targetDiv === 'structure' ||
                    targetDiv === 'btn' ||
                    targetDiv === 'canvas' ||
                    targetDiv === 'svg' ||
                    targetDiv === 'queryBackdrop' ||
                    // check if data-tracker does not exist on the clicked el and parent el
                    !targetDiv || !divClick
                ) {
                    return;
                } else {
                    let newDivsClicked = component.get('v.state.session.divsClicked');
                    newDivsClicked.push(targetDiv);

                    component.set('v.state.session.interactions', [...component.get('v.state.session.interactions'), divClick]);
                    component.set('v.state.session.lastClickTime', new Date().getTime());
                    component.set('v.state.session.divsClicked', newDivsClicked);
                    this.drawVectors(targetDiv, component);
                }

                // FIXME set actions funnel mode is ON - setting success trigger, SHOULD NOT be set from the frontend in PROD
            } else {
                let targetDiv = null;
                if (e.target.getAttribute('data-tracker')) {
                    targetDiv = e.target.getAttribute('data-tracker');
                } else {
                    targetDiv = e.target.parentElement.getAttribute('data-tracker');
                }
                if (targetDiv === 'structure' ||
                    targetDiv === 'btn' ||
                    targetDiv === 'canvas' ||
                    targetDiv === 'svg' ||
                    targetDiv === 'queryBackdrop' ||
                    // check if data-tracker does not exist on the clicked el and parent el
                    !targetDiv || !divClick
                ) {
                    return;
                }
                this.drawFunnel(targetDiv, component);
            }

            // FIXME edit mode is on - setting success trigger, SHOULD NOT be set from the frontend in PROD
        } else {
            e.stopPropagation();
            let targetDiv = null;
            if (e.target.getAttribute('data-tracker')) {
                targetDiv = e.target.getAttribute('data-tracker');
            } else {
                targetDiv = e.target.parentElement.id;
            }
            if (targetDiv === 'structure' ||
                targetDiv === 'btn' ||
                targetDiv === 'canvas' ||
                targetDiv === 'svg' ||
                targetDiv === 'queryBackdrop' ||
                // check if data-tracker does not exist on the clicked el and parent el
                !targetDiv || !divClick
            ) {
                return;
            } else {
                let targetDivValue = targetDiv.split('-');
                targetDivValue.shift();

                if(confirm(`Does ${targetDiv.split('-')[0]} item need to be set as success trigger?`)) {
                    if (confirm(`Success trigger is set to ${targetDiv.split('-')[0]} item. Do you want value ${targetDivValue.join(' ')} to be added to success trigger as well?`)) {
                        component.set('v.state.session.successTrigger', targetDiv);
                        component.set('v.state.session.editMode', false);
                    } else {
                        component.set('v.state.session.successTrigger', targetDiv.split('-')[0]);
                        component.set('v.state.session.editMode', false);
                    }
                }
            }
        }
    },

    // ****************************************************************
    // ****************************************************************
    // ****************************************************************
    // ONCHANGE

    captureChange: function(component, e) {
        if (!component.get('v.state.session.editMode')) {
            e.stopPropagation();

            if (!component.get('v.state.funnelEdit')) {
                let targetDiv;
                let divClick;
                if (e.target.getAttribute('data-tracker')) {
                    targetDiv = e.target.getAttribute('data-tracker');
                    divClick = {
                        dataTracker: targetDiv,
                        // example: input-text, input-checkbox
                        divType: e.target.nodeName.toLowerCase() + '-' + e.target.type,
                        delayTime: new Date().getTime() - component.get('v.state.session.lastClickTime'),
                        sequence: component.get('v.state.session.interactions').length + 1,
                        input: e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea' ? e.target.value : null
                    }
                    // path is nested in svg, which is then nested in a div. If node changed is path - go 2 levels up
                } else if (e.target.nodeName === 'PATH') {
                    targetDiv = e.target.parentElement.parentElement.getAttribute('data-tracker');
                    divClick = {
                        dataTracker: targetDiv,
                        divType: e.target.parentElement.parentElement.nodeName.toLowerCase(),
                        delayTime: new Date().getTime() - component.get('v.state.session.lastClickTime'),
                        sequence: component.get('v.state.session.interactions').length + 1,
                        input: (e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea') ? e.target.value : null
                    }
                } else {
                    targetDiv = e.target.parentElement.getAttribute('data-tracker');
                    divClick = {
                        dataTracker: targetDiv,
                        // example: input-text, input-checkbox
                        divType: e.target.nodeName.toLowerCase() + '-' + e.target.type,
                        delayTime: new Date().getTime() - component.get('v.state.session.lastClickTime'),
                        sequence: component.get('v.state.session.interactions').length + 1,
                        input: e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea' ? e.target.value : null
                    }
                }
                if (targetDiv === 'structure' ||
                    targetDiv === 'btn' ||
                    targetDiv === 'canvas' ||
                    targetDiv === 'svg' ||
                    targetDiv === 'queryBackdrop' ||
                    // check if data-tracker does not exist on the clicked el and parent el
                    !targetDiv || !divClick

                ) {
                    return;
                } else {
                    let newDivsClicked = component.get('v.state.session.divsClicked')
                    newDivsClicked.push(targetDiv);

                    component.set('v.state.session.interactions', [...component.get('v.state.session.interactions'), divClick]);
                    component.set('v.state.session.lastClickTime', new Date().getTime());
                    component.set('v.state.session.divsClicked', newDivsClicked);
                    this.drawVectors(targetDiv, component);
                }
            } else {
                let targetDiv = null;
                let divClick;
                if (e.target.getAttribute('data-tracker')) {
                    targetDiv = e.target.getAttribute('data-tracker');
                    divClick = {
                        divID: targetDiv,
                        divType: e.target.nodeName.toLowerCase() + '-' + e.target.type,
                        sequence: component.get('v.state.funnel.interactions').length + 1,
                        input: e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea' ? e.target.value : null
                    }
                } else {
                    targetDiv = e.target.parentElement.getAttribute('data-tracker');
                    divClick = {
                        divID: targetDiv,
                        divType: e.target.nodeName.toLowerCase() + '-' + e.target.type,
                        sequence: component.get('v.state.funnel.interactions').length + 1,
                        input: e.target.nodeName.toLowerCase() === 'input' || e.target.nodeName.toLowerCase() === 'textarea' ? e.target.value : null
                    }
                }
                if (targetDiv === 'structure' ||
                    targetDiv === 'btn' ||
                    targetDiv === 'canvas' ||
                    targetDiv === 'svg' ||
                    targetDiv === 'queryBackdrop' ||
                    // check if data-tracker does not exist on the clicked el and parent el
                    !targetDiv || !divClick
                ) {
                    return;
                } else {
                    let newDivsClicked = component.get('v.state.funnel.divsClicked')
                    newDivsClicked.push(targetDiv);

                    component.set('v.state.funnel.interactions', [...component.get('v.state.funnel.interactions'), divClick]);
                    component.set('v.state.funnel.divsClicked', newDivsClicked);
                    this.drawFunnel(targetDiv, component);
                }
            }}
    },

    downloadFile: function(component, e, helper) {
		e.stopPropagation();
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(
            JSON.stringify(component.get('{!v.state.session}'))
        ));
        element.setAttribute('data-tracker', 'ignore');
		element.innerHTML = 'IGNORE';
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();

       document.body.removeChild(element);
    },

    uploadFile: function (component, e) {
        const input = e.target;
        if ('files' in input && input.files.length > 0) {
            const reader = new FileReader();
            return new Promise((resolve, reject) => {
                reader.onload = e => resolve(e.target.result);
                reader.onerror = error => reject(error);
                reader.readAsText(input.files[0])
            }).then(res => {
                let newState = JSON.parse(res);
                component.set('v.state.session', newState);
                component.set('v.state.fileUploaded', true);
            })
        }
    },

    analyzeClicks: function(component, e) {
        let canvas = document.getElementById('canvas');
        while (canvas.firstChild) {
            canvas.removeChild(canvas.firstChild);
        }
        let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        svg.setAttribute('id', 'svg');
        svg.setAttribute('position', 'fixed');
        svg.setAttribute('width', '100vw');
        svg.setAttribute('height', '100vh');
        svg.style.top = '0';
        svg.style.left = '0';
        canvas.appendChild(svg);
        component.get('v.state.session.linesCoordinates').map(coordSet => {
            let line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            line.setAttribute('x1', coordSet.x1);
            line.setAttribute('y1', coordSet.y1);
            line.setAttribute('x2', coordSet.x2);
            line.setAttribute('y2', coordSet.y2);
            line.setAttribute('stroke', 'rgba(255,0,0,.1)');
            line.setAttribute('stroke-width', '10px');
            document.getElementById('svg').appendChild(line);
        })
    },

    analyzeQuery: function (component, e) {
        let canvas = document.getElementById('canvas');
        if (canvas.classList.value.split(' ').find((el) => el === 'top')) {
            canvas.style.opacity = '0';
            canvas.classList.remove('top');
            canvas.style.background = 'none';
            let backdrop = document.getElementById('queryBackdrop');
            if (backdrop) {
                canvas.removeChild(backdrop);
            }
        } else {
            canvas.style.opacity = '1';
            canvas.classList.add('top');
            canvas.style.background = 'rgba(0,0,0,.1)';
            let backdrop = document.createElement('div');
            backdrop.setAttribute('id', 'queryBackdrop');
            backdrop.style.position = 'fixed';
            backdrop.style.top = '1.25rem';
            backdrop.style.right = '1.25rem';
            backdrop.style.bottom = '1.25rem';
            backdrop.style.left = '1.25rem';
            backdrop.style.borderRadius = '0.3rem';
            backdrop.style.background = 'white';
            backdrop.style.padding = '1.25rem';
            backdrop.style.boxShadow = '0 0 5px rgba(0,0,0,.5)';

            let task = document.createElement('div');
            let taskTitle = document.createElement('h2');
            taskTitle.innerHTML = 'Talent search';
            taskTitle.style.fontWeight = '600';
            taskTitle.style.fontSize = '1rem';
            let timeSpent = 0;
            component.get('v.state.session.interactions').map(div => {
                if (!div.delayTime) {
                    return;
                } else {
                    timeSpent += div.delayTime;
                }
            });

            let taskSuccess = false;

            component.get('v.state.session.interactions').map((div, index) => {
                let str = document.createElement('div');
                str.style.border = '.2rem solid gray';
                str.style.borderRadius = '.3rem';
                str.style.padding = ".5rem";
                str.style.marginBottom = '1rem';
                str.borderRadius = '.3rem';
                str.style.height = 'auto';
                str.style.display = 'inline-block';
                // example: selectRow || previewResume
                let navItem = div.dataTracker.split('--')[0];
                if (navItem) {

                    // check if taken action means session was successful
                    // in this case action descriptor and action value are checked
                    // example: selectRow--MahmoudHamid
                    if (div.dataTracker === component.get('v.state.session.successTrigger')) {
                        taskSuccess = true;
                        str.style.border = '.2rem solid green';
                        str.style.color = 'green';

                        // in this case ONLY action descriptor is checked
                        // example: selectRow
                    } else if (div.dataTracker.split('--')[0] === component.get('v.state.session.successTrigger')) {
                        taskSuccess = true;
                        str.style.border = '.2rem solid green';
                        str.style.color = 'green';
                    } else if (!taskSuccess) {
                        taskSuccess = false;
                    }
                    if (div.divType.split('-')[0] === 'input' || div.divType.split('-')[0] === 'textarea') {
                        if (div.divType.split('-')[1] === 'text' || div.divType.split('-')[1] === 'textarea') {
                            // TODO logic to transform action descriptor in the following fashion
                            // if (navItem === 'reqSearchLocation') {
                            //     navItem = 'Search by location';
                            // } else if (navItem === 'reqSearchKeywords') {
                            //     navItem = 'Search by keywords';
                            // }

                            navItem += ' query: ' + div.input;
                            str.innerHTML = div.sequence + ". " + navItem;
                        } else if (div.divType.split('-')[1] === 'checkbox') {
                            let filterValue = div.dataTracker.split('--').map(arrStr => arrStr.charAt(0).toUpperCase() + arrStr.slice(1));
                            filterValue.shift();
                            // check if checkbox in question is a search filter
                            if (div.dataTracker.split('--')[0] === 'filter') {
                                str.innerHTML = div.sequence + '. filter: ' + filterValue.join(' ');
                            } else if (div.dataTracker.split('--')[0] === 'selectRow') {
                                str.innerHTML = div.sequence + '. talent selected: ' + filterValue.join(' ');
                            }
                        }

                    } else {
                        let nav = div.dataTracker.split('--')[0];
                        // TODO logic to parse descriptor
                        // if (nav === 'talentAction') {
                        //     nav = 'Contacted talent';
                        // }
                        let navValue = div.dataTracker.split('--').map(arrStr => arrStr.charAt(0).toUpperCase() + arrStr.slice(1));
                        navValue.shift();
                        str.innerHTML = div.sequence + '. ' + nav + ': ' + navValue.join(' ');
                    }
                    if (index !== component.get('v.state.session.interactions').length - 1) {
                        let outer = document.createElement('div');
                        outer.classList.add('session-step');
                        let nextDivDelay = component.get('v.state.session.interactions')[index + 1].delayTime;
                        outer.setAttribute('title', (nextDivDelay/1000).toFixed(2) + `s →`);
                        outer.appendChild(str);
                        backdrop.appendChild(outer);
                    } else {
                        str.style.color = 'gray';
                        str.style.fontSize = '1rem';
                        str.style.fontWeight = '600';
                        backdrop.appendChild(str);
                    }

                }

            });
            let taskInfo = document.createElement('div');
            taskInfo.innerHTML = `Time spent: 
            ${timeSpent < 60000
                ? (timeSpent/1000).toFixed(2)
                : (timeSpent/60000).toFixed(2)
                } 
            ${timeSpent < 60000
                ? 'seconds'
                : 'minutes'}
            <!--<br/> Task status: ${taskSuccess ? 'Success' : 'Failure'}-->
            <hr/>
            `
            task.appendChild(taskTitle);
            task.appendChild(taskInfo);
            backdrop.insertBefore(task, backdrop.firstChild);
            canvas.appendChild(backdrop);
        }
    },

    drawVectors: function(div, component) {

        if (component.get('v.tracking') === 'wrapper') {
            let coordinates = component.get('v.state.session.coordinates');
        let linesCoordinates = component.get('v.state.session.linesCoordinates');
        const createCenters = (el) => {
            let elem = document.querySelectorAll(`[data-tracker="${div}"]`)[0];
            if (elem) {

                if (elem.style.display === 'none') {
                    elem.style.display = 'block';
                }

                let elWidth = elem.offsetWidth;
                let elHeight = elem.offsetHeight;
                let elX = elem.offsetLeft;
                let elY = elem.offsetTop;
                let elCenter = document.createElement('div');
                elCenter.style.position = 'absolute';
                elCenter.style.width = '20px';
                elCenter.style.height = '20px';
                elCenter.style.borderRadius = '10px';
                elCenter.style.background = 'white';
                elCenter.style.border = '1px solid rgba(255,0,0,.3)';
                elCenter.style.color = 'black';
                elCenter.style.textAlign = 'center';
                elCenter.style.fontSize = '.8rem';


                let seq = component.get('v.state.session.interactions').length;
                elCenter.innerHTML = seq;
                elCenter.classList.add('p-indicator');
                elCenter.onclick = () => {
                    if (!elCenter.classList.value.split(' ').find(el => el === 'sequence-indicator')) {
                        elCenter.classList.add('sequence-indicator');
                    } else {
                        elCenter.classList.remove('sequence-indicator');
                    }
                };

                elCenter.style.left = (elX + elWidth / 2 - 10) + 'px';
                elCenter.style.top = (elY + elHeight / 2 - 10) + 'px';

                document.getElementById('canvas').appendChild(elCenter);

                let pair = {x: elX + elWidth / 2, y: elY + elHeight / 2};
                let newCoordinates = [...coordinates, pair];

                component.set('v.state.session.coordinates', newCoordinates);
                coordinates = component.get('v.state.session.coordinates');
                if (coordinates.length > 1) {
                    let lastCoordinate = coordinates[coordinates.length - 1];
                    let line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
                    if (coordinates[coordinates.length - 2].x < lastCoordinate.x) {
                        line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                        line.setAttribute('y1', coordinates[coordinates.length - 2].y - 10);
                        line.setAttribute('x2', lastCoordinate.x);
                        line.setAttribute('y2', lastCoordinate.y - 10);
                    } else if (coordinates[coordinates.length - 2].x > lastCoordinate.x) {
                        line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                        line.setAttribute('y1', coordinates[coordinates.length - 2].y + 10);
                        line.setAttribute('x2', lastCoordinate.x);
                        line.setAttribute('y2', lastCoordinate.y + 10);
                    } else if (coordinates[coordinates.length - 2].x === lastCoordinate.x) {
                        if (coordinates[coordinates.length - 2].y > lastCoordinate.y) {
                            line.setAttribute('x1', coordinates[coordinates.length - 2].x + 10);
                            line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                            line.setAttribute('x2', lastCoordinate.x + 10);
                            line.setAttribute('y2', lastCoordinate.y);
                        } else if (coordinates[coordinates.length - 2].y < lastCoordinate.y) {
                            line.setAttribute('x1', coordinates[coordinates.length - 2].x - 10);
                            line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                            line.setAttribute('x2', lastCoordinate.x - 10);
                            line.setAttribute('y2', lastCoordinate.y);
                        }
                    }


                    line.setAttribute('id', `line-${linesCoordinates.length}`);
                    line.setAttribute('stroke', 'rgba(255,0,0,.3)');
                    line.setAttribute('stroke-width', '1px');
                    line.setAttribute('marker-end', "url(#arrow)");
                    line.classList.add('liner');

                    setTimeout(() => {
                        document.getElementById(`line-${linesCoordinates.length}`).classList.remove('liner')
                    }, 500)

                    let newLinesCoordinates = [...linesCoordinates,
                        {
                            x1: coordinates[coordinates.length - 2].x - 2,
                            y1: coordinates[coordinates.length - 2].y - 2,
                            x2: lastCoordinate.x - 2,
                            y2: lastCoordinate.y - 2
                        }];

                    component.set('v.state.session.linesCoordinates', newLinesCoordinates);

                    document.getElementById('svg').appendChild(line);
                    let canvas = document.getElementById('canvas');
                    canvas.appendChild(document.getElementById('svg'));

                    // FIXME add mousetravel to each object
                    let index = component.get('v.state.session.interactions').length - 1;
                    let mouseTravelString = `v.state.session.interactions[${index}].mouseTravel`;

                    component.set(mouseTravelString, +(document.getElementById(`line-${linesCoordinates.length}`).getTotalLength().toFixed(0)));


                    let p1 = line.getPointAtLength(0);
                    let p2 = line.getPointAtLength(3);
                    let deg = Math.atan2(p1.y - p2.y, p1.x - p2.x) * (180 / Math.PI);
                    let textNode = document.createElement('p');
                    textNode.classList.add('delay-indicator');
                    if (lastCoordinate.x >= coordinates[coordinates.length - 2].x) {
                        textNode.innerHTML = (component.get('v.state.session.interactions')[component.get('v.state.session.interactions').length - 1].delayTime / 1000).toFixed(2) + ' s >>';
                    } else {
                        textNode.innerHTML = "<< " + (component.get('v.state.session.interactions')[component.get('v.state.session.interactions').length - 1].delayTime / 1000).toFixed(2) + ' s';
                    }

                    textNode.style.transform = `rotate(${180 + deg}deg)`;
                    textNode.style.position = 'absolute';
                    textNode.style.margin = '0 !important';
                    textNode.style.background = 'white';
                    textNode.style.padding = '2px 4px';
                    textNode.style.borderRadius = '5px';
                    textNode.style.fontSize = '0.7rem';
                    textNode.style.border = '1px solid rgba(255,0,0,.3)';

                    // x2 > x1
                    if (lastCoordinate.x >= coordinates[coordinates.length - 2].x) {
                        textNode.style.transform = `rotate(${180 + deg}deg)`;

                        // y2 > y1
                        if (lastCoordinate.y >= coordinates[coordinates.length - 2].y) {
                            textNode.style.left = (lastCoordinate.x + coordinates[coordinates.length - 2].x) / 2 - 20 + 'px';
                            textNode.style.top = ((lastCoordinate.y + coordinates[coordinates.length - 2].y) / 2 - 20) + 'px';
                            // y1 > y2
                        } else {
                            textNode.style.left = (lastCoordinate.x + coordinates[coordinates.length - 2].x) / 2 - 20 + 'px';
                            textNode.style.top = ((coordinates[coordinates.length - 2].y + lastCoordinate.y) / 2 - 20) + 'px';
                        }
                        // x1 > x2
                    } else if (lastCoordinate.x < coordinates[coordinates.length - 2].x) {
                        textNode.style.transform = `rotate(${360 + deg}deg)`;
                        // y2 > y1
                        if (lastCoordinate.y >= coordinates[coordinates.length - 2].y) {
                            textNode.style.left = (coordinates[coordinates.length - 2].x + lastCoordinate.x) / 2 - 20 + 'px';
                            textNode.style.top = ((lastCoordinate.y + coordinates[coordinates.length - 2].y) / 2) + 'px';
                            // y1 > y2
                        } else {
                            textNode.style.left = (coordinates[coordinates.length - 2].x + lastCoordinate.x) / 2 - 30 + 'px';
                            textNode.style.top = (coordinates[coordinates.length - 2].y + lastCoordinate.y) / 2 - 10 + 'px';
                        }
                    }
                    canvas.appendChild(textNode);
                }
            }
        };
        createCenters(div);

    }
        else if (component.get('v.tracking') === 'plugin') {
            console.log('drawing vectors for plugin')
            let coordinates = component.get('v.state.session.coordinates');
            let linesCoordinates = component.get('v.state.session.linesCoordinates');

            const createCenters = (el) => {
                let elem = document.querySelectorAll(`[data-tracker="${el}"]`)[0];
                if (elem) {
                    if (elem.style.display === 'none') {
                        elem.style.display = 'block';
                    }

                    function offset(el) {
                        let rect = el.getBoundingClientRect(),
                            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
                            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
                        return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
                    }

                    let elWidth = elem.offsetWidth;
                    let elHeight = elem.offsetHeight;
                    let elX = offset(elem).left;
                    let elY = offset(elem).top;
                    let elCenter = document.createElement('div');
                    elCenter.style.position = 'absolute';
                    elCenter.style.width = '20px';
                    elCenter.style.height = '20px';
                    elCenter.style.borderRadius = '10px';
                    elCenter.style.background = 'white';
                    elCenter.style.border = '2px solid rgba(255,0,0,.7)';
                    elCenter.style.color = 'black';
                    elCenter.style.textAlign = 'center';
                    elCenter.style.fontSize = '.8rem';
                    elCenter.style.cursor = 'pointer';
                    elCenter.style.transition = 'all .3s ease-out';


                    let seq = component.get('v.state.session.interactions').length;
                    elCenter.innerHTML = seq;
                    elCenter.classList.add('p-indicator');
                    elCenter.onclick = () => {
                        if (!elCenter.classList.value.split(' ').find(el => el === 'sequence-indicator')) {
                            elCenter.classList.add('sequence-indicator');
                            elCenter.style.transform = 'translate(-20px, -20px)';
                        } else {
                            elCenter.classList.remove('sequence-indicator');
                            elCenter.style.transform = 'translate(0px, 0px)';
                        }
                    };

                    elCenter.style.left = (elX + elWidth / 2 - 10) + 'px';
                    elCenter.style.top = (elY + elHeight / 2 - 10) + 'px';
                    let container = document.getElementById('tracking-container');

                    container.appendChild(elCenter);

                    let pair = {x: elX + elWidth / 2, y: elY + elHeight / 2};
                    let newCoordinates = [...coordinates, pair];

                    component.set('v.state.session.coordinates', newCoordinates);
                    coordinates = component.get('v.state.session.coordinates');
                    if (coordinates.length > 1) {
                        let lastCoordinate = coordinates[coordinates.length - 1];
                        let line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
                        if (coordinates[coordinates.length - 2].x < lastCoordinate.x) {
                            line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                            line.setAttribute('y1', coordinates[coordinates.length - 2].y - 10);
                            line.setAttribute('x2', lastCoordinate.x);
                            line.setAttribute('y2', lastCoordinate.y - 10);
                        } else if (coordinates[coordinates.length - 2].x > lastCoordinate.x) {
                            line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                            line.setAttribute('y1', coordinates[coordinates.length - 2].y + 10);
                            line.setAttribute('x2', lastCoordinate.x);
                            line.setAttribute('y2', lastCoordinate.y + 10);
                        } else if (coordinates[coordinates.length - 2].x === lastCoordinate.x) {
                            if (coordinates[coordinates.length - 2].y > lastCoordinate.y) {
                                line.setAttribute('x1', coordinates[coordinates.length - 2].x + 10);
                                line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                                line.setAttribute('x2', lastCoordinate.x + 10);
                                line.setAttribute('y2', lastCoordinate.y);
                            } else if (coordinates[coordinates.length - 2].y < lastCoordinate.y) {
                                line.setAttribute('x1', coordinates[coordinates.length - 2].x - 10);
                                line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                                line.setAttribute('x2', lastCoordinate.x - 10);
                                line.setAttribute('y2', lastCoordinate.y);
                            }
                        }


                        line.setAttribute('id', `line-${linesCoordinates.length}`);
                        line.setAttribute('stroke', 'rgba(255,0,0,.7)');
                        line.setAttribute('stroke-width', '2px');
                        // line.setAttribute('marker-end', "url(#arrow)");
                        // line.classList.add('liner');
                        //
                        // setTimeout(() => {
                        //     document.getElementById(`line-${linesCoordinates.length}`).classList.remove('liner')
                        // }, 500)

                        let newLinesCoordinates = [...linesCoordinates,
                            {
                                x1: coordinates[coordinates.length - 2].x - 2,
                                y1: coordinates[coordinates.length - 2].y - 2,
                                x2: lastCoordinate.x - 2,
                                y2: lastCoordinate.y - 2
                            }];

                        component.set('v.state.session.linesCoordinates', newLinesCoordinates);
                        let svg = document.getElementById('tracking-canvas');

                        svg.appendChild(line);

                        // FIXME add mousetravel to each object
                        let index = component.get('v.state.session.interactions').length - 1;
                        // let mouseTravelString = `v.state.session.interactions[${index}].mouseTravel`;

                        // component.set(mouseTravelString, +(document.getElementById(`line-${linesCoordinates.length}`).getTotalLength().toFixed(0)));


                        let p1 = line.getPointAtLength(0);
                        let p2 = line.getPointAtLength(3);
                        let deg = Math.atan2(p1.y - p2.y, p1.x - p2.x) * (180 / Math.PI);
                        let textNode = document.createElement('p');
                        textNode.style.zIndex = '99999999';

                        textNode.addEventListener('mouseover', () => {
                            textNode.style.zIndex = '100000000';
                        });
                        textNode.addEventListener('mouseleave', () => {
                            textNode.style.zIndex = '99999999';
                        });
                        if (lastCoordinate.x >= coordinates[coordinates.length - 2].x) {
                            textNode.innerHTML = (component.get('v.state.session.interactions')[component.get('v.state.session.interactions').length - 1].delayTime / 1000).toFixed(2) + ' s >>';
                        } else {
                            textNode.innerHTML = "<< " + (component.get('v.state.session.interactions')[component.get('v.state.session.interactions').length - 1].delayTime / 1000).toFixed(2) + ' s';
                        }

                        textNode.style.transform = `rotate(${180 + deg}deg)`;
                        textNode.style.position = 'absolute';
                        textNode.style.margin = '0 !important';
                        textNode.style.background = 'white';
                        textNode.style.padding = '2px 4px';
                        textNode.style.borderRadius = '5px';
                        textNode.style.fontSize = '0.7rem';
                        textNode.style.cursor = 'pointer';
                        textNode.style.border = '2px solid rgba(255,0,0,.7)';


                        // x2 > x1
                        if (lastCoordinate.x >= coordinates[coordinates.length - 2].x) {
                            // textNode.style.transform = `rotate(${180 + deg}deg)`;

                            // y2 > y1
                            // ↘
                            if (lastCoordinate.y >= coordinates[coordinates.length - 2].y) {
                                container.appendChild(textNode);
                                let textWidth = textNode.offsetWidth;
                                let textHeight = textNode.offsetHeight;
                                textNode.style.left = (lastCoordinate.x + coordinates[coordinates.length - 2].x) / 2 - (textWidth/2) - 5 + 'px';
                                textNode.style.top = ((lastCoordinate.y + coordinates[coordinates.length - 2].y) / 2 - (textHeight/2)) - 5 + 'px';
                                textNode.style.transform = `rotate(${180 + deg}deg)`;
                                // y1 > y2
                                // ↗
                            } else {
                                container.appendChild(textNode);
                                let textWidth = textNode.offsetWidth;
                                let textHeight = textNode.offsetHeight;
                                textNode.style.left = ((lastCoordinate.x + coordinates[coordinates.length - 2].x) / 2 - (textWidth/2) + 5) + 'px';
                                textNode.style.top = ((coordinates[coordinates.length - 2].y + lastCoordinate.y) / 2 - (textHeight/2)) + 5 + 'px';
                                textNode.style.transform = `rotate(${180 + deg}deg)`;
                            }
                            // x1 > x2
                        } else if (lastCoordinate.x < coordinates[coordinates.length - 2].x) {
                            // textNode.style.transform = `rotate(${360 + deg}deg)`;
                            // y2 > y1
                            // ↙
                            if (lastCoordinate.y >= coordinates[coordinates.length - 2].y) {
                                container.appendChild(textNode);
                                let textWidth = textNode.offsetWidth;
                                let textHeight = textNode.offsetHeight;
                                textNode.style.left = ((coordinates[coordinates.length - 2].x + lastCoordinate.x) / 2 - (textWidth/2) + 5) + 'px';
                                textNode.style.top = ((lastCoordinate.y + coordinates[coordinates.length - 2].y) / 2 - (textHeight/2)) - 5 + 'px';
                                textNode.style.transform = `rotate(${360 + deg}deg)`;
                                // y1 > y2
                                // ↖
                            } else {
                                container.appendChild(textNode);
                                let textWidth = textNode.offsetWidth;
                                let textHeight = textNode.offsetHeight;
                                textNode.style.left = (coordinates[coordinates.length - 2].x + lastCoordinate.x) / 2 - (textWidth/2) - 5 + 'px';
                                textNode.style.top = (coordinates[coordinates.length - 2].y + lastCoordinate.y) / 2 - (textHeight/2) + 5 + 'px';
                                textNode.style.transform = `rotate(${360 + deg}deg)`;
                            }
                        }

                    }
                }
            };
            createCenters(div);

        }
        }
    ,

    // funnel
    drawFunnel: function(div, component) {
//		return;
        let coordinates = component.get('v.state.funnel.coordinates');
        let linesCoordinates = component.get('v.state.funnel.linesCoordinates');

        let elem = document.getElementById(div);

        const createCenters = (el) => {
            let elem = document.getElementById(el);
            if (elem) {

                if (elem.style.display === 'none') {
                    elem.style.display = 'block';
                }

                let elWidth = elem.offsetWidth;
                let elHeight = elem.offsetHeight;
                let elX = elem.offsetLeft;
                let elY = elem.offsetTop;

                let pair = {x: elX + elWidth / 2, y: elY + elHeight / 2};
                let newCoordinates = [...coordinates, pair];

                component.set('v.state.funnel.coordinates', newCoordinates);
                coordinates = component.get('v.state.funnel.coordinates');
                if (coordinates.length > 1) {
                    let lastCoordinate = coordinates[coordinates.length - 1];
                    let line = document.createElementNS('http://www.w3.org/2000/svg', 'line');

                    if (coordinates[coordinates.length - 2].x < lastCoordinate.x) {
                        line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                        line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                        line.setAttribute('x2', lastCoordinate.x);
                        line.setAttribute('y2', lastCoordinate.y);
                    } else if (coordinates[coordinates.length - 2].x > lastCoordinate.x) {
                        line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                        line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                        line.setAttribute('x2', lastCoordinate.x);
                        line.setAttribute('y2', lastCoordinate.y);
                    } else if (coordinates[coordinates.length - 2].x === lastCoordinate.x) {
                        if (coordinates[coordinates.length - 2].y > lastCoordinate.y) {
                            line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                            line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                            line.setAttribute('x2', lastCoordinate.x);
                            line.setAttribute('y2', lastCoordinate.y);
                        } else if (coordinates[coordinates.length - 2].y < lastCoordinate.y) {
                            line.setAttribute('x1', coordinates[coordinates.length - 2].x);
                            line.setAttribute('y1', coordinates[coordinates.length - 2].y);
                            line.setAttribute('x2', lastCoordinate.x);
                            line.setAttribute('y2', lastCoordinate.y);
                        }
                    }


                    line.setAttribute('id', `line-${linesCoordinates.length}`);
                    line.setAttribute('stroke', 'rgba(0,0,255,.4)');
                    line.setAttribute('stroke-width', '5px');
                    line.setAttribute('marker-end', "url(#arrow)");
                    line.classList.add('liner');

                    setTimeout(() => {document.getElementById(`line-${linesCoordinates.length}`).classList.remove('liner')}, 500)

                    let newLinesCoordinates = [...linesCoordinates,
                        {   x1: coordinates[coordinates.length - 2].x -2,
                            y1: coordinates[coordinates.length - 2].y -2,
                            x2: lastCoordinate.x -2,
                            y2: lastCoordinate.y -2
                        }];

                    component.set('v.state.funnel.linesCoordinates', newLinesCoordinates);
                    document.getElementById('svg').appendChild(line);
                    let canvas = document.getElementById('canvas');
                    canvas.appendChild(document.getElementById('svg'));





                    let p1 = line.getPointAtLength(0);
                    let p2 = line.getPointAtLength(3);
                    let deg = Math.atan2(p1.y - p2.y, p1.x - p2.x) * (180 / Math.PI);
                    let textNode = document.createElement('p');
                    textNode.classList.add('delay-indicator');
                    if (lastCoordinate.x >= coordinates[coordinates.length - 2].x) {
                        textNode.innerHTML = '>>';
                    } else {
                        textNode.innerHTML = "<<";
                    }

                    textNode.style.transform = `rotate(${180 + deg}deg)`;
                    textNode.style.position = 'absolute';
                    textNode.style.margin = '0 !important';
                    textNode.style.background = 'white';
                    textNode.style.padding = '2px 4px';
                    textNode.style.borderRadius = '5px';
                    textNode.style.fontSize = '0.7rem';
                    textNode.style.border = '3px solid rgba(0,0,255,.4)';

                    // x2 > x1
                    if (lastCoordinate.x >= coordinates[coordinates.length - 2].x) {
                        textNode.style.transform = `rotate(${180 + deg}deg)`;

                        // y2 > y1
                        if (lastCoordinate.y >= coordinates[coordinates.length - 2].y) {
                            textNode.style.left = (lastCoordinate.x + coordinates[coordinates.length - 2].x)/2 - 20 + 'px';
                            textNode.style.top = ((lastCoordinate.y + coordinates[coordinates.length - 2].y)/2 - 13) + 'px';
                            // y1 > y2
                        } else {
                            textNode.style.left = (lastCoordinate.x + coordinates[coordinates.length - 2].x)/2 - 20 + 'px';
                            textNode.style.top = ((coordinates[coordinates.length - 2].y + lastCoordinate.y)/2 - 13) + 'px';
                        }
                        // x1 > x2
                    } else if (lastCoordinate.x < coordinates[coordinates.length - 2].x) {
                        textNode.style.transform = `rotate(${360 + deg}deg)`;
                        // y2 > y1
                        if (lastCoordinate.y >= coordinates[coordinates.length - 2].y) {
                            textNode.style.left = (coordinates[coordinates.length - 2].x + lastCoordinate.x)/2 - 20 + 'px';
                            textNode.style.top = ((lastCoordinate.y + coordinates[coordinates.length - 2].y)/2 - 13) + 'px';
                            // y1 > y2
                        } else {
                            textNode.style.left = (coordinates[coordinates.length - 2].x + lastCoordinate.x)/2 - 20 + 'px';
                            textNode.style.top = (coordinates[coordinates.length - 2].y + lastCoordinate.y)/2 - 13 + 'px';
                        }
                    }
                    canvas.appendChild(textNode);
                }
            }}
        createCenters(div);
    },

    showVectors: function(component) {
        component.get('v.state.session.linesCoordinates').map(coordinateSet => {
            let line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            if (coordinateSet.x1 < coordinateSet.x2) {
                line.setAttribute('x1', coordinateSet.x1);
                line.setAttribute('y1', coordinateSet.y1 - 10);
                line.setAttribute('x2', coordinateSet.x2);
                line.setAttribute('y2', coordinateSet.y2 - 10);
            } else if (coordinateSet.x1 > coordinateSet.x2) {
                line.setAttribute('x1', coordinateSet.x1);
                line.setAttribute('y1', coordinateSet.y1 + 10);
                line.setAttribute('x2', coordinateSet.x2);
                line.setAttribute('y2', coordinateSet.y2 + 10);
            } else if (coordinateSet.x1 === coordinateSet.x2) {
                if (coordinateSet.y1 > coordinateSet.y2) {
                    line.setAttribute('x1', coordinateSet.x1 + 10);
                    line.setAttribute('y1', coordinateSet.y1);
                    line.setAttribute('x2', coordinateSet.x2 + 10);
                    line.setAttribute('y2', coordinateSet.y2);
                } else if (coordinateSet.y1 < coordinateSet.y2) {
                    line.setAttribute('x1', coordinateSet.x1 - 10);
                    line.setAttribute('y1', coordinateSet.y1);
                    line.setAttribute('x2', coordinateSet.x2 - 10);
                    line.setAttribute('y2', coordinateSet.y2);
                }
            }
            line.setAttribute('id', `line-${component.get('v.state.session.linesCoordinates').length}`);
            line.setAttribute('stroke', 'rgba(255,0,0,.3)');
            line.setAttribute('stroke-width', '1px');
            line.setAttribute('marker-end', "url(#arrow)");
            document.getElementById('svg').appendChild(line);
            document.getElementById('canvas').appendChild(document.getElementById('svg'));
        })
    },

    setSuccess: function(component, e) {
        component.set('v.state.session.editMode', !component.get('v.state.session.editMode'));
        alert(`Please select UI element that determines successful completion of a user session`);
    },
    log: function (message) {
        console.log(message);
    }
})