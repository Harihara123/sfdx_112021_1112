({
	handleTermInput : function(component, event, helper) {
		let adjustedScrolledValue = +component.get('v.scrolledValue');
        if (adjustedScrolledValue) {
            adjustedScrolledValue = adjustedScrolledValue - 8;
        } else {
            adjustedScrolledValue = 0;
        }
        component.set('v.adjustedScrolledValue', adjustedScrolledValue); 
        // console.log(component.get('v.scrolledValue'));
		// If there is a selected term, Call the related terms API, set attribute, and show modal.
		if (component.get("v.term") && component.get("v.term") !== "") {
			helper.updateSelected(component);
			if (component.get("v.selected").length > 0) {
				helper.showModal(component);
				helper.addRemoveEventListener(component, event);
			}
		}
	},

    hideModal : function(component, event, helper) {
    	helper.clearSelection(component);
		helper.hideModal(component);
    },

    applyTerms : function(component, event, helper) {
    	helper.hideModal(component);
    	helper.applyTerms(component);
    	helper.clearSelection(component);
    },
    
    adjustScrolledValue: function(component, event, helper) {
        let adjustedScrolledValue = +component.get('v.scrolledValue');
        if (adjustedScrolledValue) {
            adjustedScrolledValue = adjustedScrolledValue - 8;
        } else {
            adjustedScrolledValue = 0;
        }
        component.set('v.adjustedScrolledValue', adjustedScrolledValue); 
    }

})