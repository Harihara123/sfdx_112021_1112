({
   	showModal : function(component) {
        // this.toggleClass(component,'backdropRelatedTerms','slds-backdrop--');
		    this.toggleClass(component,'modalRelatedTerms','slds-');
    },

	addRemoveEventListener: function(component, event) {
		var windowCloserFn;
		var self = this;

		windowCloserFn = $A.getCallback(function(event) {
			var thisPopover = document.getElementById("modalRelatedTerms");			
			if (event.target && thisPopover) {				 
				if (!event.target.className.includes("has-popover") && !thisPopover.contains(event.target)) {
					self.hideModal(component);
					window.removeEventListener("click", windowCloserFn);
				}
			} else {
				window.removeEventListener("click", windowCloserFn);
			}
		});

		window.addEventListener("click", windowCloserFn);
	},

    hideModal : function(component) {
    	// this.toggleClassInverse(component,'backdropRelatedTerms','slds-backdrop--');
    	this.toggleClassInverse(component,'modalRelatedTerms','slds-');
		component.set("v.isPopupShowing", false);
    },

    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
  	    $A.util.removeClass(modal,className+'hide');
  	    $A.util.addClass(modal,className+'show');
  	},
 
    toggleClassInverse: function(component,componentId,className) {
  	    var modal = component.find(componentId);
  	    $A.util.addClass(modal,className+'hide');
  	    $A.util.removeClass(modal,className+'show');
  	},

  	updateSelected : function (component) {
  		var allTerms = component.get("v.relatedTerms");
  		var selected = allTerms[component.get("v.term")];
  		if (selected && selected.length > 0) {
  			component.set("v.selected", selected);
  		} else {
  			this.clearSelection(component);
  		}
  	},

    clearSelection : function (component) {
        component.set("v.selected", []);
        component.set("v.term", "");
    },

  	applyTerms : function (component) {
    	var terms = component.get("v.selected");
    	// Add the checked terms to a new list.
    	var newTerms = [];
    	for (var i=0; i<terms.length; i++) {
    		if (terms[i].checked) {
    			newTerms.push(terms[i].term);
    		}
    	}

    	if (newTerms.length === 0) {
    		// Nothing selected. no action
    		return;
    	}

    	// Fire the event to apply the terms.
    	var evt = component.getEvent("termsAppliedEvt");
    	evt.setParams({
    		"addedTerms" : newTerms
    	})
    	evt.fire();
  	}

})