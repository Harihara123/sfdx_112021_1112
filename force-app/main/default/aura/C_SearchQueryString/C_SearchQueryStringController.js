({
	createQueryString : function(component, event, helper) {
        
                component.set('v.runningUser', event.getParam('arguments').currentUser );
                
                let criteria 	= event.getParam('arguments').alertCriteria;
                let objCriteria = JSON.parse( criteria )
                
                if( objCriteria.hasOwnProperty('criteria')){  
                	component.set('v.criteria', objCriteria.criteria );
                }
                
                if( component.get("v.criteria").hasOwnProperty('facets')){ 
                	component.set('v.facets', component.get("v.criteria.facets") );
                }
                
                var parameters = helper.getBaseParameters(component);
                parameters = helper.addLocationParameters(parameters, component);
                parameters = helper.addFacetParameters(parameters, component);
                return( helper.buildUrl(parameters) );
                
        },
    
    
    
    
})