({
	getBaseParameters: function(component) {
		let baseParams = {
			"id": component.get("v.runningUser.id"), 
			"q": component.get("v.criteria.keyword"), 
			"size": 30, /* page size */
			"from": 0, 	/* page offset */ 
			"flt.group_filter": component.get("v.runningUser.opcoSearchFilter")
		};
        
        return baseParams
	},
    
    addLocationParameters : function (parameters, component) {
        let objCriteria = component.get("v.criteria");
        if( objCriteria.hasOwnProperty('multilocation')){        
            var loc = this.translateToSearchApiLocation( objCriteria.multilocation );
            parameters.location = JSON.stringify(loc);
        } 

        return parameters;
    },
    
    
    translateToSearchApiLocation : function(loc) {
        var val = [];

        for (var i=0; i<loc.length; i++) {
            /*  Output value of "filter"
             *  useRadius   --> |  NONE  |  SELECT  |  AUTO 
             *  alwaysUseRadius |
             *  -------------------------------------------
             *    TRUE          |   N/A  |   FALSE  | FALSE
             *    FALSE         |  TRUE  |   FALSE  | FALSE
             */
            var filter = loc[i].useRadius === "NONE";
            var radius = typeof loc[i].geoRadius === "number" ? loc[i].geoRadius.toFixed() : loc[i].geoRadius;

            if (filter) {
                val.push({
                    "filter" : filter,
                    "city" : loc[i].city,
                    "state" : loc[i].state,
                    "country" : loc[i].country,
                    "postalcode" : loc[i].postalcode
                });
            } else {
                val.push({
                    "latlon" : loc[i].latlon,
                    "geoRadius" : radius,
                    "geoUnitOfMeasure" : loc[i].geoUnitOfMeasure,
                    "filter" : filter
                });
            }
        }

        return val;
    },
    
    addFacetParameters : function (parameters, component) {
        console.log('DDD--->' + component.get("v.facets") );
        var selectedFacets = component.get("v.facets");
        if (selectedFacets && selectedFacets !== null) {
            var keys = Object.keys(selectedFacets);
           
            for (var i=0; i<keys.length; i++) {

                if (!this.isEmptyFacet(selectedFacets[keys[i]])) {
                    if (typeof selectedFacets[keys[i]] === "string") {
                        parameters[keys[i]] = selectedFacets[keys[i]];
                    } else {
                        parameters[keys[i]] = JSON.stringify(selectedFacets[keys[i]]);
                    }
                }
            }
            // console.log(parameters);
        }
        return parameters;
    },
    
    isEmptyFacet : function (facet) {
        var emptyFacet = false;
        if (facet !== undefined && facet !== null) {
            // Not undefined or null
            if (facet === "") {
                // String or any other basic type empty
                emptyFacet = true;
            } else if (typeof facet === "object" && (facet.length === undefined || facet.length === 0)) {
                // Object / array
                emptyFacet = true;
            }
        } else {
            emptyFacet = true;
        }

        return emptyFacet;
    },
    
    buildUrl : function (parameters) {
         var qs = "";
         for(var key in parameters) {
            var value = parameters[key];
            /* S-32457 adding condition to handle multiple facet keys.
               Keys sent as comma separated values for facet parame keys

               NOTE: if comma becomes a valid character this solution will not work. 

               */
            if(key.indexOf(",") > -1){
                var paramValues; 
                var paramKeys = key.split(",");

                if(value !== undefined && value !== null){
                    paramValues = value.split(",");
                    for(var i=0; i<paramValues.length; i++){
                      qs += encodeURIComponent(paramKeys[i]) + "=" + encodeURIComponent(paramValues[i]) + "&"; 
                    }
                }

            }else{
              qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
            }

         }
         if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
         }
         return qs;
    },
    
    
    
})