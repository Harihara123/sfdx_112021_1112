({
	/*renderFacets : function(component, event, helper) {
        var results = component.get("v.results");

		component.set("v.automatchSourceData", results.autoMatchSourceData);

        var facetsObj = results.facets;
        if(facetsObj !== undefined){
            component.set("v.facets", facetsObj);
 		}

		var fdp = results.facetDisplayProps;
		if (fdp !== undefined) {
			component.set("v.displayPropsMap", fdp);
		}

        component.set("v.displayFilters", true);
 	},*/
	updateFacets: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			if (parameters.facets) {
				helper.updateRefreshedFacets(component, parameters.facets);
			}

			if (parameters.facetDisplayProps) {
				component.set("v.displayPropsMap", parameters.facetDisplayProps);
			}
			component.set("v.displayFilters", true);

			if (parameters.isReexecute) {
				helper.presetFacetsForReexecute(component, parameters.facets);
			}

			if (parameters.automatchVectors) {
				var mfCmp = component.find("matchFacetContainer");
				mfCmp.updateMatchFacets(parameters.automatchVectors);
				component.set("v.matchVectors", parameters.automatchVectors); //To enable multi select gesture on Search/Match
			}

			if (parameters.automatchSourceData) {
				component.set("v.automatchSourceData", parameters.automatchSourceData);
			}
		}
 	},

	hideFacets: function(component, event, helper) {
		component.set("v.displayFilters", false);
	},

    updateFilteredFacets : function(component, event, helper) {
        // Set initiatingFacet first so that when the facet update handler is fired, this is available
        component.set("v.searchInitiatingFacet", event.getParam("initiatingFacet"));

        var incomingFacets = event.getParam("facets");
        var currentFacets = component.get("v.facets");

        if (currentFacets === undefined || currentFacets === null) {
            currentFacets = incomingFacets;
        } else if(incomingFacets !== undefined) {
            var facetKeys = Object.keys(incomingFacets);
            for (var i=0; i<facetKeys.length; i++) {
                currentFacets[facetKeys[i]] = incomingFacets[facetKeys[i]];
            }
        } else {
            currentFacets = {}
        }
        component.set("v.facets", currentFacets);
    },
    
    facetChanged : function(component, event, helper) {
        helper.updateCriteriaFacets(component, event);
        if (event.getParam("isFilterableFacet")) {
            helper.updateFacetFilterParams(component, event);
        }
        helper.updateCriteriaNestedFilters(component, event);
        helper.updateSelectedPills(component, event);
		helper.updateDisplayProps(component, event);
        //helper.fireFacetApplyEvt(component, event);	//Firing the event on click of Update Results
		if (event) {
			// event is available only on E_FacetChangedEvent. "Clear All" case does not require this?
			helper.updateRelatedFacetState(component, event);
		}
    },

	//To enable multi select Facets on Search/Match
	applyFacets : function(component, event, helper) {
		helper.fireFacetApplyEvt(component);
    },

	//To enable multi select gesture on Search/Match
	applyMatchFacets : function(component, event, helper) {
        helper.updateMatchVectors(component, event.getParams());
		//event.stopPropagation();
    },

    facetPreset : function(component, event, helper) {
        helper.updateCriteriaFacets(component, event);
        helper.updateCriteriaNestedFilters(component, event);
        helper.updateSelectedPills(component, event);
        // helper.fireFacetPresetApplyEvt(component, event);
    },

    clearAllFacets : function(component, event, helper) {
		helper.clearAllFacets(component, true);
    },
	
	clearAllFacetsNoSearch : function(component, event, helper) {
		helper.clearAllFacets(component, false);
    }

})