({
	updateCriteriaFacets : function(component, event) {
        var criteriaFacets = component.get("v.criteriaFacets");
        if (criteriaFacets === undefined || criteriaFacets === null) {
            criteriaFacets = {};
        }
      
        // criteriaFacets[event.getParam("facetParamKey")] = event.getParam("selectedFacet");
        var key = event.getParam("overrideKey") !== undefined ? 
                    event.getParam("overrideKey") : event.getParam("facetParamKey");
        criteriaFacets[key] = event.getParam("appliedFacets");

        component.set("v.criteriaFacets", criteriaFacets);
	},

	updateRefreshedFacets: function(component, facets) {
		var currentFacets = component.get("v.facets");
		if (!currentFacets) {
			currentFacets = facets;
		} else if(facets) {
			var facetKeys = Object.keys(facets);
			for (var i=0; i<facetKeys.length; i++) {
				currentFacets[facetKeys[i]] = facets[facetKeys[i]];
			}
		} else {
			currentFacets = {}
		}
		component.set("v.facets", currentFacets);
	},

	presetFacetsForReexecute: function(component, facets) {
		var facetsArr = component.find("rsFacet");
		for (var i=0; i<facetsArr.length; i++) {
			facetsArr[i].presetFacet(facets);
		}
	},

    updateFacetFilterParams : function(component, event) {
        var filterParams = component.get("v.facetFilterParams");
        if (filterParams === undefined || filterParams === null) {
            filterParams = {};
        }
      
        var fp = {};
        fp[event.getParam("sizeKey")] = event.getParam("size");
        fp[event.getParam("includeKey")] = event.getParam("filterTerm");

        var selected = event.getParam("selectedFacet");
        // Check for selected required below to prevent failures when all selections removed.
        if (selected) {
            if (event.getParam("hasNestedFilter")) {
                var sKeys = Object.keys(selected);
                selected = selected[sKeys[0]].join("|");
            }
            // Do not add .selected if no selections on this facet.
            if (selected !== "") {
                //fp[event.getParam("includeKey").replace(".include", ".selected")] = selected; //Commented out for Search Optimization
				fp[event.getParam("includeKey").replace(".contains", ".selected")] = selected; //S-78090 - Added by Karthik
            }
        }
        // For user filtered facets, add nested_local_facet_filter
        if (event.getParam("isUserFiltered")) {
            fp[event.getParam("facetParamKey").replace("nested_facet_filter", "nested_local_facet_filter")] 
                    = JSON.stringify([{"User" : [component.get("v.runningUser.psId")]}]); 
        }

        // One object entry for each facet
        filterParams[event.getParam("facetParamKey")] = fp;

        component.set("v.facetFilterParams", filterParams);
    },

    updateCriteriaNestedFilters : function(component, event) {
        if (event.getParam("hasNestedFilter")) {
            var criteriaFilters = component.get("v.criteriaNestedFilters");
            if (criteriaFilters === undefined || criteriaFilters === null) {
                criteriaFilters = {};
            }

            // criteriaFilters[event.getParam("filterParamKey")] = event.getParam("nestedFilters");
            component.set("v.criteriaNestedFilters", criteriaFilters);
        }
    },

	updateSelectedPills : function(component, event) {
        var selectedPills = component.get("v.selectedPills");
        if (selectedPills === undefined || selectedPills === null) {
            selectedPills = {};
        }

        selectedPills[event.getParam("facetParamKey")] = event.getParam("selectedPills");

        component.set("v.selectedPills", selectedPills);
	},

	updateDisplayProps : function(component, event) {
		var m = component.get("v.displayPropsMap");
		if (m === undefined || m === null) {
			m = {};
		}
		m[event.getParam("facetParamKey")] = event.getParam("displayProps");

		component.set("v.displayPropsMap", m);
	},

    clearSelectedFilterCriteria : function(component) {
        var ffp = component.get("v.facetFilterParams");

        if (ffp) {
			var keys = Object.keys(ffp);
			for (var i=0; i<keys.length; i++) {
				var facet = ffp[keys[i]];
				var newFacet = {};

				var inkeys = Object.keys(facet);
				for (var j=0; j<inkeys.length; j++) {
					var property = facet[inkeys[j]]
					if (!inkeys[j].endsWith(".selected")) {
						newFacet[inkeys[j]] = property;
					}
				}
				ffp[keys[i]] = newFacet;
			}

			component.set("v.facetFilterParams", ffp);
		}
    },

    fireFacetApplyEvt : function(component, event) {
        var facetApplyEvt = component.getEvent("searchFacetsAppliedEvt");
        var excludeKeyWords;
        var childSFPCmp = component.find("childSearchFacetPIllsCmp");
        if(childSFPCmp!=undefined){
            excludeKeyWords = childSFPCmp.getExcludeKeys();
        }

        facetApplyEvt.setParams({
            "facets" : component.get("v.criteriaFacets"),
            "facetFilterParams" : component.get("v.facetFilterParams"),
            "nestedFilters" : component.get("v.criteriaNestedFilters"),
            "selectedFacet" : event === undefined ? "" : event.getParam("selectedFacet"),
            "hasNestedFilter" : event === undefined ? false : event.getParam("hasNestedFilter"),
            "pills" : component.get("v.selectedPills"),
            "initiatingFacet" : event === undefined ? "" : event.getParam("facetParamKey"),
			"displayProps" : component.get("v.displayPropsMap"),
			"matchVectors" : component.get("v.matchVectors"),  //To enable multi select Facets on Search/Match
             "excludeKeyWords" : excludeKeyWords // Updated to handle exclude keywords in search S-95200
        });
        facetApplyEvt.fire();
	},

	//To enable multi select gesture on Search/Match
	updateMatchVectors: function(component, matchFacetParams) {
        var matchVectors = component.get("v.matchVectors");

        switch (matchFacetParams.matchFacetType) {
            case "jobtitles" :
                matchVectors.title_vector = matchFacetParams.matchFacetData;
                break;

            case "skills" : 
                matchVectors.skills_vector = matchFacetParams.matchFacetData;
                break;

            default :
                break;
        }

        component.set("v.matchVectors", matchVectors);
	},
	
	updateRelatedFacetState: function(component, event) {
		var facetsArr = component.find("rsFacet");
		for (var i=0; i<facetsArr.length; i++) {
			facetsArr[i].updateRelatedFacets(event.getParam("facetParamKey"), event.getParam("selectedFacet"));
		}
	},

	clearAllFacets: function(component, fireApplyEvt) {
        this.clearSelectedFilterCriteria(component);
		this.clearFacets(component);
		// clearFacets() needs to be called before container attributes are cleared.
        component.set("v.facets", {});
		component.set("v.criteriaFacets", {});
        component.set("v.criteriaNestedFilters", {});
        component.set("v.selectedPills", {});
        component.set("v.searchInitiatingFacet", "");

        if (fireApplyEvt) {
			this.fireFacetApplyEvt(component);
		}

	},

	clearFacets: function(component) {
		var facetsArr = component.find("rsFacet");
		for (var i=0; i<facetsArr.length; i++) {
			facetsArr[i].clearFacets();
		}
	}
    
})