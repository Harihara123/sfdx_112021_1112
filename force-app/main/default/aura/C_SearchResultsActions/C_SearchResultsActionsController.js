({
	doInit:function(component,event,helper){
		//get User details
		 var userDetails = component.get('c.getUserDetails');
		 userDetails.setCallback(this, function(a) {
	        var state = a.getState();
             
	        if (state === "SUCCESS") {
			   component.set('v.profileName',a.getReturnValue().Profile.Name);
			   component.set('v.isMarketingUser',a.getReturnValue().UserPermissionsMarketingUser);
	        } 
	        else if (state === "ERROR") {
                
	           // helper.doErrorCallSheetActions(a);
	        }
	    });
		$A.enqueueAction(userDetails);

		if(component.get("v.type") != 'C') {
			component.set("v.rowsSelected", []);
			component.set("v.rowsSelectedCount", 0);
			component.set("v.rowsSelectedContacts",[]);

		}

	},
	handleSearchComplete : function(component, event, helper) {
        // Clear all selections after a search executes.
        component.set("v.rowsSelected", []);
        component.set("v.rowsSelectedCount", 0);

		component.set("v.rowsSelectedContacts",[]);
	},
	handleSearchAction : function(component, event) {
		
    	var idAdd = event.getParam("idToAdd");
		var idRemove = event.getParam("idToRemove");
		var contactIdAdd = event.getParam("contactIdToAdd");
		var contactIdRemove = event.getParam("contactIdToRemove");
		var rows = component.get("v.rowsSelected");
		var contactRows = component.get("v.rowsSelectedContacts");

        if (rows === undefined || rows === null) {
            rows = [];			
        }  
        if (contactRows === undefined || contactRows === null) {
			contactRows = [];			
        } 
		
		if(contactIdAdd != "" && contactIdRemove === "") {
			var foundContactId = false;
			for(var i=0; i<contactRows.length; i++) {
				if(contactRows[i].Id == contactIdAdd){//already exisitng
					foundContactId = true;
				}
			}
			if(!foundContactId) {
				var con = new Object();
				con.Id = contactIdAdd;
				contactRows.push(con);
			}
		} else if (contactIdAdd === "" && contactIdRemove !== "") {
			for(var i=0; i<contactRows.length; i++) {
				if(contactRows[i].Id === contactIdRemove){//already exisitng
					
					contactRows.splice(i,1);
				}
			}
		} else {

		}

        //Add Id to list, unless it's already there
        if (idAdd !== "" && idRemove === "") {
            if (rows.indexOf(idAdd) < 0) {
            	rows.push(idAdd);
        	}
        }
        //Remove Id from list.
        else if (idAdd === "" && idRemove !== "") {
            var i = rows.indexOf(idRemove);
            if (i >= 0) {
                rows.splice(i, 1);
            }
        } else {
            
        }		
        component.set("v.rowsSelected", rows);
        component.set("v.rowsSelectedCount", rows.length);
		component.set("v.rowsSelectedContacts",contactRows);
		
		
	},
    
    submitSearchAction : function (component, event, helper) {
        var rows = component.get("v.rowsSelected");
        //var action = component.get("c.saveCandidateToCallSheet");
        helper.addToCallSheet(component, rows);
    },
    
    showModalBox : function (component, event, helper) {
        //component.set("v.subjectValMessage",""); 
        //component.set("v.dueDateValMessage","");
        helper.showModalBox(component);
       // helper.addToCallSheet(component, rows, action);
    },
    closeModalBox : function (component, event, helper) {
        component.set("v.subjectValMessage",""); 
        component.set("v.dueDateValMessage","");
        helper.closeModalBox(component);    
    },
     onRenders: function(component, event, helper) {
    		// helper.onRender(component);
    	      
    },
	bringUpCampaignModal: function(component, event, helper) {
		var cType = "C"; 
		$A.createComponent(
            "c:C_AddToCampaignModal",
            {
                "contacts" : component.get("v.rowsSelectedContacts"),
				"cType" : cType
            },
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    component.set('v.C_AddToCampaignModal', newComponent);
                }
                else if (status === "INCOMPLETE") {
                }
                else if (status === "ERROR") {
                    console.log('error');
                }
            }
        );
	},
	bringUpAllCampaignModal: function(component, event, helper) {
		var cType = "C"; 
		$A.createComponent(
            "c:C_AddToCampaignModal",
            {
				"contacts" : component.get("v.rowsSelectedContacts"),
				"allAllIndicator" :"true",
				"queryString" :component.get("v.queryString"),
				"cType" : cType
            },
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    component.set('v.C_AddToCampaignModal', newComponent);
                }
                else if (status === "INCOMPLETE") {
                }
                else if (status === "ERROR") {
                    console.log('error');
                }
            }
        );
	},
	stickyDropdown : function(component, event){
	 var stayDrop = component.find('stayDropCampaign');     
	 $A.util.addClass(stayDrop,'fade');	
	},
	stickyDropdown1 : function(component, event){
	 var stayDrop1 = component.find('stayDropCampaign');     
	 $A.util.addClass(stayDrop1,'fade2');	
	},
	handleDisableCampaignButton:function(component, event) {		
		component.set("v.rowsSelected", event.getParam("rowsSelected"));
        component.set("v.rowsSelectedCount", event.getParam("rowsSelectedCount"));
		component.set("v.rowsSelectedContacts",[]);
	},
	/*handleCheckboxTracking:function(component, event) {
		var sourceString = event.getParam("clickTarget");
		var contactList = component.get("v.rowsSelectedContacts");
		if(component.get("v.rowsSelectedCount") > 0) {
			if(sourceString != null && sourceString != '' && typeof sourceString != undefined) {
				if(sourceString.includes("pagination")) {
					if(component.get("v.firestatusEvent"))
						component.set("v.firestatusEvent", false);
					else	
						component.set("v.firestatusEvent", true);
				}
			}
		} else {
			component.set("v.rowsSelectedContacts",[]);
		}
	},
	updateSearchCheckboxes: function(component, event) {
		
		var contactList = component.get("v.rowsSelectedContacts");
		var setCheckboxStatusEvent = $A.get("e.c:E_SetSearchCheckboxStatus");
		
		setCheckboxStatusEvent.setParams({
            contacts:contactList,
            setStatus:true
        });	
		setCheckboxStatusEvent.fire();

	}*/
})