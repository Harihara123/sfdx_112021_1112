({
     initialize: function(component) {
        try {
            var action=component.get("c.initialize")
            action.setCallback(this, function(a) {
                var state = a.getState();
                if (state === "SUCCESS") {
                var model = a.getReturnValue();
                	
                	
                var typeArray = [];
                var typeMap = model.typeMapping;
                for ( var key in typeMap) {
                    typeArray.push({value:typeMap[key], key:key});
                }
                component.set("v.typeList", typeArray);
                
                
                var statusArray = [];
                var statusMap = model.statusMapping;
                for ( var key in statusMap) {
                    statusArray.push({value:statusMap[key], key:key});
                }
                component.set("v.statusList",statusArray);
                
                
                
                var priorityArray = [];
                var priorityMap = model.priorityMapping;
                for ( var key in priorityMap) {
                    priorityArray.push({value:priorityMap[key], key:key});
                }
                component.set("v.priorityList",priorityArray);
                
                component.set("v.tsk",model.taskObj);
                if(component.get("v.searchKeyWord")!=null)
                component.set("v.tsk.subject",component.get("v.searchKeyWord"));
                
                if(component.get("v.oppname")!=null)
                component.set("v.tsk.subject",component.get("v.oppname"));
                    
                    
                component.set("v.tsk.ActivityDate",model.taskObj.ActivityDate);
                    
                component.set("v.tsk.Type",model.taskObj.Type);
                component.set("v.tsk.Status",model.taskObj.Status);
                component.set("v.tsk.Priority",model.taskObj.Priority);

                component.set("v.tsk.TransactionID__c",component.get("v.transactionId"));
                component.set("v.tsk.RequestID__c",component.get("v.requestId"));
                    
                                	
               }
                else if (state === "ERROR") {
                  
                }
            });
            $A.enqueueAction(action);
        }
        catch(e) {
            
        }
    },
    
    addToCallSheet : function(component, rows) {
        try {
            var contactList = component.get("v.rowsSelectedContacts");
            var tsk=component.get("v.tsk");
            var flag=this.validate(component,tsk);
            if(flag){
            	this.doInfoCallSheetActions();
	            var action = component.get("c.saveCandidateToCallSheet");
	            var tsky = JSON.stringify(tsk);
	            action.setParams({ 
	                recordIdCSV : rows.join(),
	                tasks :tsky
	            });
	            component.set("v.showModal",!component.get("v.showModal")); 
				
	            action.setCallback(this, function(a) {
	                var state = a.getState();
	                if (state === "SUCCESS") {
	                    this.doSuccessCallSheetActions(component);
						//send event to uncheck checkboxes in results
						/*var disableButtonEvent = $A.get("e.c:E_DisableSearchActionButtons");
						disableButtonEvent.setParams({ "rowsSelectedCount" : 0 });
						disableButtonEvent.setParams({ "rowsSelected" : [] });
						//for unchecking checkboxes
						disableButtonEvent.setParams({ "contacts" : contactList });
				
						disableButtonEvent.fire();	*/					
	                }
	                else if (state === "ERROR") {
	                    this.doErrorCallSheetActions(a);
	                }
	            });
	            
	            $A.enqueueAction(action);
            }
        }
        catch(e) {
            
        }
    },
    validate : function(component,tsk) {
     var sflag=true;
     var oflag=true;
       try {
    	   	var searchWord=component.get("v.tsk.subject");
    	   	if(searchWord==null || searchWord=='' ||typeof searchWord==undefined){
    	   		sflag=false;
    	   		component.set("v.subjectValMessage","Subject is required.");
    	   	}else{
    	   		component.set("v.subjectValMessage","");
    	   	}
           
            var dueDate=component.get("v.tsk.ActivityDate");
           	if(dueDate==null || dueDate=='' ||typeof dueDate==undefined){
    	   		sflag=false;
    	   		component.set("v.dueDateValMessage","Due Date is required.");
    	   	}else{
    	   		component.set("v.dueDateValMessage","");
    	   	}
           
           
           var oppnamevar=component.get("v.oppname");
    	   	if((searchWord==null || searchWord=='' ||typeof searchWord==undefined) && (oppnamevar==null || oppnamevar=='' ||typeof oppnamevar==undefined)){
    	   		oflag=false;
    	   		component.set("v.subjectValMessage","Subject is required.");
    	   	}else{
               	if(sflag)
    	   		component.set("v.subjectValMessage","");
    	   	}
           
          return (sflag && oflag);
        }catch(e) {
            
        }
    },
    showModalBox : function(component) {
       try {
    	   	var searchWord=component.get("v.searchKeyWord");
    	   	var flag=component.get("v.showModal");
            component.set("v.showModal",true);
            var tdate = new Date().toJSON().slice(0,10).replace(/-/g,'-'); 
            component.set("v.tsk.ActivityDate",tdate);
            
            component.set("v.tsk.Type","Call");
            component.set("v.tsk.Status","Not Started");
            component.set("v.tsk.Priority","Normal");
            // STORY S-71888

           component.set("v.tsk.TransactionID__c",component.get("v.transactionId"));
            component.set("v.tsk.RequestID__c",component.get("v.requestId"));
           
           if(!component.get("v.oneflag")){
            	this.initialize(component);
                component.set("v.oneflag",true);
           }else{
                if(component.get("v.searchKeyWord")!=null)
                component.set("v.tsk.subject",component.get("v.searchKeyWord"));
                
                if(component.get("v.oppname")!=null)
                component.set("v.tsk.subject",component.get("v.oppname"));
                    
           }
           
           
          //  $A.enqueueAction(action);
        }catch(e) {
            
        }
    },
    
     closeModalBox : function(component) {
       try {
    	   	
            component.set("v.showModal",false);
        }catch(e) {
            
        }
    },
    
	doSuccessCallSheetActions : function(component) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": "success",
            "title": "Success!",
            "message": "The record(s) have been added to your call sheet."
        });
        resultsToast.fire();
        
        component.set("v.rowsSelectedCount", 0);
        component.set("v.rowsSelected", []);
        
        //Send event to search results to uncheck checkboxes
        var appEvent = $A.get("e.c:E_MassRowUpdateCompleted");
        appEvent.setParams({ "recordType" : "Talent" });
        appEvent.setParams({ "recordIdsCSV" : "" });
        appEvent.setParams({ "resetUpdate" : true });
        appEvent.fire();
	},
    
    doErrorCallSheetActions : function(a) {
        var errors = a.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                
            }
        } 
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": "error",
            "title": "Error",
            "message": "Something has gone wrong. Please try again."
        });
        resultsToast.fire();
    },
    
    doInfoCallSheetActions : function() {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": "info",
            "title": "Processing...",
            "message": "Your records are processing. Please wait."
        });
        resultsToast.fire();
       
    },
    onRender : function(component) {
        // console.debug('Inside the onRender');
        var searchWord=component.get("v.searchKeyWord");
        var tskObj=component.get("v.tsk");
        //component.set("v.tsk.subject",component.get("v.searchKeyWord"));
        var tdate = new Date().toJSON().slice(0,10).replace(/-/g,'-'); // console.log(utc);
        component.set("v.tsk.ActivityDate",tdate);
        component.set("v.tsk.Type","Call");
        component.set("v.tsk.Status","Not Started");
        component.set("v.tsk.Priority","Normal");
    }
})