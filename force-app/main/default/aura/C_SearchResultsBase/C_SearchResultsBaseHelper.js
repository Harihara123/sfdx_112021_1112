({
    fireMatchInsightsRequestEvt : function(component, recordName) {
        var matchInsightsReqEvt = component.getEvent("matchInsightsRequestEvt");
        matchInsightsReqEvt.setParams({
            "recordId" : component.get("v.recordId"),
            "recordName" : recordName
        });
        matchInsightsReqEvt.fire();
    },
    
})