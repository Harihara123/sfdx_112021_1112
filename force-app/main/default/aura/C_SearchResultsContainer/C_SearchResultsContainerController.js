({

	doInit: function (component, event, helper) {
		var reqDropDown = '[{"key":"","value":"' + $A.get("$Label.c.ATS_RELEVANCE") + '"},{"key":"job_create_date|asc","value":"' + $A.get("$Label.c.ATS_OPPORTUNITY_CREATED_DATE_ASC") + '"},{"key":"job_create_date|desc","value":"' + $A.get("$Label.c.ATS_OPPORTUNITY_CREATED_DATE_DESC") + '"}]';
		component.set("v.reqDropDown", reqDropDown);

		var talentDropDown = '[{"key":"","value":"' + $A.get("$Label.c.ATS_RELEVANCE") + '"},{"key":"placement_end_date|asc","value":"' + $A.get("$Label.c.ATS_AG_END_DATE_ASC") + '"},{"key":"placement_end_date|desc","value":"' + $A.get("$Label.c.ATS_AG_END_DATE_DSC") + '"},{"key":"qualifications_last_activity_date|desc","value":"' + $A.get("$Label.c.ATS_LAST_ACTIVITY_DATE") + '"},{"key":"qualifications_last_resume_modified_date|desc","value":"' + $A.get("$Label.c.ATS_FACET_LAST_RESUME_UPDATED") + '"}]';
		component.set("v.talentDropDown", talentDropDown);
	},

	handleResultsUpdate: function (component, event, helper) {
		if (!component.get("v.smsInfoLoaded")) {
			helper.updateSelectedSort(component);
			helper.updateResults(component);
			helper.setScrollPosition(component);
			helper.setSearchTrackingIds(component);
			component.set("v.selectAllCheck", false);
		} else {
			var results = component.get("v.results");
			component.set("v.records", results.records);
			//component.set("v.smsInfoLoaded", true);
		}
	},

	facetsApplied: function (component, event, helper) {
		// Clear out the results every time facets are applied.
		helper.resetResults(component);
	},

	setScrollPosition: function (component, event, helper) {
		helper.setScrollPosition(component);
	},

	massUpdateCompleted: function (component, event, helper) {
		component.set("v.selectAllCheck", false);
	},

	openJobPostingTab: function (component, event, helper) {
		var context = event.getParam("contextFlag");
		component.set("v.displayJobPostingModal", true);
		console.log('Inside openJobPostingTab--');

	},
	hideJobPostingTab: function (component, event, helper) {
		var context = event.getParam("disableFlag");
		component.set("v.displayJobPostingModal", false);
		console.log('Inside openJobPostingTab--');

	},
	recordsChanged: function (component, event, helper) {
		let records = component.get('v.records');
		if (!records.length) {
			var trackingEvent = $A.get("e.c:E_SearchResultsHandler");
			trackingEvent.setParam('resultsAlert', true);
			trackingEvent.fire();
		}
	}

})