({

    updateResults: function (component, event) {
        var results = component.get("v.results");

        if (results.hasError === false) {
            this.setResultValues(component);

        }
        // implement else block to show the errors

    },

    updateSelectedSort: function (component, event) {
        var results = component.get("v.results");
        if (results.sortField) {
            component.set("v.selectedSort", results.sortField + "|" + results.sortOrder);
        }
    },

    setScrollPosition: function (component) {
        // HACK ALERT - Using a timeout makes the results paint and the scrollTop set correctly.
        setTimeout(function () {
            var posn = component.get("v.position");
            if ((posn !== undefined && posn !== null && posn !== "")
                && document.getElementById("r-m-" + posn) !== null) {
                // Specific record position to set the scroll is available. And it is present on the page.
                var y = document.getElementById("r-m-" + posn).offsetTop;
                document.body.scrollTop = y;
                // Cross browser backup. If value was not set, try the alternative.
                if (document.body.scrollTop === 0) {
                    document.documentElement.scrollTop = y;
                }
                // Unset the position so it skips this condition after any page scrolls.
                component.set("v.position", "");
            } else {
                document.body.scrollTop = 0;
                // Cross browser backup.
                document.documentElement.scrollTop = 0;
            }
        }, 100);
        component.set("v.scrollPositionSet", true);
    },

    setResultValues: function (component) {
        // Get the records attribute.
        var results = component.get("v.results");

        //var currentOffset = parseInt(component.get("v.offset"));

        component.set("v.records", results.records);
        component.set("v.offset", parseInt(results.offset));
        component.set("v.totalResultCount", results.totalResultCount);

        var recs = component.get("v.records");//event.getParam("records");
        var oppId = component.get("v.opportunityId");
        var accId = component.get("v.accountId");
        var jobId = component.get("v.jobId");
        var recIds = [];

        for (var i = 0; i < recs.length; i++) {
            recIds.push(recs[i].id);
        }

        if (recIds.length > 0) {
            this.getContactListBadges(component, event, recIds);
        }

        if ((oppId !== null && oppId !== undefined) || (jobId !== null && jobId !== undefined) || (accId !== null && accId !== undefined)) {

            if (recIds.length > 0) {
                //console.log(recIds);debugger;
                var action = component.get("c.getOrder");
                action.setParams({
                    "recordIds": recIds,
                    "opportunityId": oppId,
                    "jobId": jobId,
                    "accountId": accId
                });
                action.setCallback(this, function (response) {
                    if (response.getState() === "SUCCESS") {
                        var responseOrder = response.getReturnValue();
                        //console.log(responseOrder);debugger;
                        if (responseOrder !== null) {
                            if (responseOrder.length > 0) {
                                for (var i = 0; i <= responseOrder.length; i++) {
                                    if (typeof responseOrder[i] !== "undefined") {

                                        for (var j = 0; j <= recs.length; j++) {
                                            if (typeof recs[j] !== "undefined") {
                                                if ((oppId !== null && oppId !== undefined) || (jobId !== null && jobId !== undefined)) {
                                                    if (responseOrder[i].ShipToContact.AccountId === recs[j].id) {
                                                        recs[j].linked = true;
                                                    }
                                                } else if ((accId !== null && accId !== undefined)) {
                                                    if (responseOrder[i].OpportunityId === recs[j].id) {
                                                        recs[j].linked = true;
                                                    }
                                                }

                                            }

                                        }
                                    }

                                }
                                component.set("v.records", recs);
                            }
                        }

                    } else if (response.getState() === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                /* console.log("Error message: " + 
                                             errors[0].message);*/
                            }
                        } else {
                            //console.log("Unknown error");
                        }
                    }

                });
                $A.enqueueAction(action);
            }

        }
    },
    getContactListBadges: function (component, event, recordIds) {
        var action = component.get('c.getContactListBadges');
        var recs = component.get("v.records");
        action.setParams({ "accountIds": recordIds });
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var responseContactListBadges = response.getReturnValue();
                if (responseContactListBadges) {
                    for (var i = 0; i <= recs.length; i++) {
                        if (typeof recs[i] !== "undefined") {
                            if (recs[i].id in responseContactListBadges) {
                                recs[i].contactList = responseContactListBadges[recs[i].id];
                            }
                        }
                    }
                    component.set("v.records", recs);
                }
            }
        });
        $A.enqueueAction(action);
    },

    resetResults: function (component) {
        component.set("v.records", []);
        component.set("v.totalResultCount", 0);
        component.set("v.offset", 0);
    },

    fireSearchPrefsUpdateEvt: function (component) {
        var prefsUpdateEvt = component.getEvent("prefsUpdateEvt");
        prefsUpdateEvt.setParams({
            "prefs": component.get("v.userPrefs")
        });
        prefsUpdateEvt.fire();
    },
    setSearchTrackingIds: function (component, event) {
        var results = component.get("v.results");
        component.set("v.transactionId", results.transactionId);
        component.set("v.requestId", results.requestId);
    }
})