({	
    doInit: function(component,event,helper){
		var reqDropDown = '[{"key":"","value":"'+$A.get("$Label.c.ATS_RELEVANCE") +'"},{"key":"job_create_date|asc","value":"'+ $A.get("$Label.c.ATS_OPPORTUNITY_CREATED_DATE_ASC") +'"},{"key":"job_create_date|desc","value":"'+ $A.get("$Label.c.ATS_OPPORTUNITY_CREATED_DATE_DESC") +'"}]';
		component.set("v.reqDropDown",reqDropDown);
		var talentDropDown = '[{"key":"","value":"'+$A.get("$Label.c.ATS_RELEVANCE") +'"},{"key":"placement_end_date|asc","value":"'+ $A.get("$Label.c.ATS_AG_END_DATE_ASC") +'"},{"key":"placement_end_date|desc","value":"'+ $A.get("$Label.c.ATS_AG_END_DATE_DSC") +'"},{"key":"qualifications_last_activity_date|desc","value":"'+ $A.get("$Label.c.ATS_LAST_ACTIVITY_DATE") +'"},{"key":"qualifications_last_resume_modified_date|desc","value":"'+ $A.get("$Label.c.ATS_FACET_LAST_RESUME_UPDATED") +'"}]';
        component.set("v.talentDropDown",talentDropDown);
        
        /*To check user group A permission
        var groupAPermissionsAction = component.get('c.checkGroupPermission');
			groupAPermissionsAction.setCallback(this, function(response) {
				var response = response.getReturnValue();
				
				component.set("v.userInGroups",response);				
				
			});
			$A.enqueueAction(groupAPermissionsAction);*/
        if (component.get("v.type") === 'R2C'){
            component.set("v.enableToggle", true);
        }
	},
    
    typeChangeHandler: function(component, event, helper) {
        if (component.get("v.type") === 'R2C'){
            component.set("v.enableToggle", true);
        }
    },
    
    handleResultsUpdate : function(component, event, helper) {
		if (!component.get("v.smsInfoLoaded")) {
			helper.buildResumeQuery(component);
            helper.getResumes(component, event);
			component.set("v.loadActionMenu", false);
			helper.updateSelectedSort(component);
			helper.updateResults(component);
			helper.setScrollPosition(component);
			helper.setSearchTrackingIds(component);
			component.set("v.selectAllCheck", false);
			let results = component.get('v.results.records');
			if(results && results.length > 0) {
				let recordId = results[0].id;
				let recordIdArr = [];
				results.map((items)=>{
					recordIdArr.push(items.id);
				});
				component.set("v.resumeIdArr", recordIdArr);
				component.set("v.selectTalentRow", recordId);
			}
		}
		else {
			 var results = component.get("v.results");       
			 if (results.hasError === false) {
				component.set("v.records", results.records);
			 }
		}

    },

    facetsApplied : function(component, event, helper) {
        // Clear out the results every time facets are applied.
        helper.resetResults(component);
    },

    setTalentCardFocus: function(component, event, helper){
        component.set("v.selectTalentRow", event.getParam("resumeId"));
    },

    setScrollPosition : function(component, event, helper) {
        helper.setScrollPosition(component);
    },

    massUpdateCompleted : function(component, event, helper) {
        component.set("v.selectAllCheck", false);
    },

    viewTalentSidePanel : function(component, event, helper) {

        // Order is important. set showSidebar to true before setting contactId
        component.set("v.showSidebar", true);
        component.set("v.selectTalentRow", event.getParam("recordId"));
        component.set("v.sidebarRecords", event.getParam("record"));
        component.set("v.contactId", event.getParam("contactId"));

        helper.setSidebarPos(component);

        // console.log(component.get("v.sidebarRecords"));
    },

    openJobPostingTab: function(component, event, helper) {
        var context = event.getParam("contextFlag");
        component.set("v.displayJobPostingModal",true);
        console.log('Inside openJobPostingTab--');

    },
    hideJobPostingTab: function(component, event, helper) {
        var context = event.getParam("disableFlag");
        component.set("v.displayJobPostingModal",false);
        console.log('Inside openJobPostingTab--');

    },

    processResumeRequest: function(component, event, helper) {
    // console.log('[RES] got request for the next batch');
    let x = component.get('v.resumeFetchCap');    
    let id = event.getParam("talentID");
	let remainderIDs = component.get('v.remainderIDs');
    let index = remainderIDs.findIndex(rec => rec === id );
        let ids;
        if (index !== -1) {
        component.set('v.rowClickCounter', 0);
            if (index >=0 && index <=9) {
                ids = remainderIDs.slice(0, 10);
            } else if (index >=10 && index <=19) {
                ids = remainderIDs.slice(10, 20);
            } else if (index >=20 && index <=29) {
                ids = remainderIDs.slice(20, 30);
            } else if (index >=30 && index <=39) {
                ids = remainderIDs.slice(30, 40);
            }
            if (ids.length) {
                helper.resumeRequest(component, event, ids, x);   
            }
        }
    },
	rowClickCounter: function(component, event, helper) {
    let counter = component.get('v.rowClickCounter');
        if (counter == 3) {
            component.set('v.rowClickCounter', 0);
                let x = component.get('v.resumeFetchCap');    
            	let remainderIDs = component.get('v.remainderIDs');
		        let ids = remainderIDs.slice(0, 10)
            	if (ids.length) {
	                helper.resumeRequest(component, event, ids, x);   
    	        }
        }
	},
	passSelectedRecordToSideBar : function(component, event, helper) {
		var accountId = component.get("v.selectTalentRow");
		component.set("v.readyToLoadCandidateDetails",false);
		var recs = component.get("v.records");
		for (var i=0; i<recs.length; i++) {
            if (recs[i].id == accountId) {
				component.set("v.selectedRecord",recs[i]);							
				break;
			}
        }
		component.set("v.readyToLoadCandidateDetails",true);
		
	},

    setFocus:function(cmp, e, h) {
        let selectRow = cmp.get("v.selectRow");
        cmp.set("v.selectTalentRow", selectRow);
	},
	recordsChanged: function(component, event, helper) {
    	let records = component.get('v.records');
        if (!records.length) {
            var trackingEvent = $A.get("e.c:E_SearchResultsHandler");
            trackingEvent.setParam('resultsAlert', Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1));
            trackingEvent.fire();
        }
	},

})