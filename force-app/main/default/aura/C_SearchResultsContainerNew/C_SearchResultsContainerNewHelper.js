({
    resumeRequest: function(component, event, ids, x) {
        // console.log('fetching resumes for ' + ids.slice(0, x).join())
       
        const records = component.get('v.results.records');
       
        let filteredUserData = records.map(record => {return {id: record.id, resumeUpdatedDate: (record.record.resume_1 || {}).modified, resumeName: `${record.record.family_name}, ${record.record.given_name}`}});
        let qry = encodeURIComponent(component.get("v.resumeQuery"));
        
        let queryString = `id=${ids.slice(0, x).join()}&q=${qry}&highlight.fields=resume_html&flt.group_filter=all&size=10&_source_includes=resume_html`;
      //console.log('queryString'+queryString);
		const action = component.get("c.search");
		action.setParams({"serviceName": "Resume Highlight",
                           "queryString": queryString});
        let resumePairs = [];
		action.setCallback(this, function(response) {
            let res = JSON.parse(response.getReturnValue());
            if (typeof res === 'object' 
                && res['response']
                && res['response']['hits']
                && res['response']['hits']['hits']) {
                let hits = res['response']['hits']['hits'];
                ids.slice(0, x).map(id => {
                    let resumePair = { id };
                    let idHit = hits.find(hit => hit._id === id);
                    if (idHit) {
                        // console.log('Found resume hit for - ' + id);
                        const highlightedResume = (((idHit || {}).highlight || {})["resume_html"] || [])[0],
                            nonHighlightedResume = ((idHit || {})._source || {}).resume_html;
                        resumePair.resume = (highlightedResume || nonHighlightedResume) ? ((highlightedResume) ? highlightedResume : `<span class="htl2">${nonHighlightedResume}</span>` ) : null;
                        resumePair.resumeUpdatedDate = (filteredUserData.find(user => user.id === idHit._id) || {}).resumeUpdatedDate;
                        resumePair.resumeName = (filteredUserData.find(user => user.id === idHit._id) || {}).resumeName;
                    } else {
                        // console.log('No resume hit for - ' + id);
                        resumePair.resume = null;
                    }
                    resumePairs.push(resumePair);
                });
                let currentPairs = component.get('v.resumePairs');
                if (currentPairs !== '[]') {

                    resumePairs = [...currentPairs, ...resumePairs];
                }
                component.set('v.resumePairs', resumePairs);
                // console.log(JSON.parse(JSON.stringify(component.get('v.resumePairs'))));
                // 3. Rest of the results ids (20 - 40) assign to a variable remainderIDs
                let remainderIDs = [];
                if (!(component.get('v.remainderIDs') || []).length) {
                    // page loaded for the first time, no remainder ids
                    remainderIDs = ids;
                    remainderIDs.splice(0, x);
                } else {
                    // subsequent click, remainder ids are present
                    remainderIDs = component.get('v.remainderIDs');
                    for (let i = 0; i < ids.length; i++) {
                        remainderIDs = remainderIDs.filter(id => id !== ids[i]);
                        // remainderIDs.splice(remainderIDs.findIndex(id => id === ids[i]),1);
                    }
                }
                component.set('v.remainderIDs', remainderIDs);
            } else {
                let trackingEvent = $A.get("e.c:ServerError");
                trackingEvent.setParam('requestFailed', Math.random());
                trackingEvent.fire();
                // show server error toast
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Server Error",
                    "message": "Please try refreshing page."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },

    getResumes: function(component, event) {

        /**
        Page is loaded for the first time - no results
        1. New results come in (30 - 50)
        2. Fetch first 10 resumes
        3. Rest of the results ids (20 - 40) assign to a variable remainderIDs
        4. LISTEN TO USER ACTIVITY
        5. User clicked on xyz number of talent cards
        6. Get first 10 ids from remainderIDs and fetch their resumes

        User clicks record that does not have fetched resume
        2.1. show spinner
        2.2. look up record’s ID in remainderIDs
        2.3. determine if it is within [0]-[9], or [10]-[19], or [20]-[29], or [30]-[39]
        2.4. Fetch respective batch of 10 resumes
        2.5. update remainderIDs
        2.6. update resumePairs
        2.7. User clicked on xyz number of talent cards
        2.8. Get first 10 ids from remainderIDs and fetch their resumes
        **/
        component.set("v.resumePairs", []);
        let x = component.get('v.resumeFetchCap'); // set value to fetch different number of resumes
        
        // 1. New results come in (30 - 50)
        component.set('v.remainderIDs', []); // reset remainder each time new results come in
        
        if ((component.get('v.results.records') || []).length) {
            // let filteredUserData = component.get('v.results.records').map(record => {return {id: record.id, resumeUpdatedDate: (record.record.resume_1 || {}).modified}});
            let ids = component.get('v.results.records').map(el => el.id);
            // console.log('[RES] number of talents fetched: ' + ids.length);
            // 2. Fetch first 10 resumes
            this.resumeRequest(component, event, ids, x);
    
	        return;
        }
    },
    buildResumeQuery: function(component){
        const searchType = component.get("v.type");
        const resumeQuery = component.get("v.resumeQuery");
        let queryString = '';

        if(searchType === 'C'){
           
            queryString = component.get("v.keywordString");

        } else {
            let matchVectors = component.get("v.matchVectors");

            const regex = /\s/g;

            let queryStringArr = [];

            if (matchVectors !== null) {
                ['title_vector', 'skills_vector'].forEach((key) => {
                    matchVectors[key].forEach((vector) => {
                        // check for whitespace and add double quotes
                        let matchTerm = vector.term;

                        if (matchTerm.match(regex) !== null) {
                            matchTerm = `"${matchTerm.replace(regex, '+')}"`;
                        }

                        if (vector.required){
                            queryStringArr.unshift(matchTerm);
                        } else {
                            queryStringArr.push(matchTerm);
                        }
                    });
                });
            }

            const stringCheck = (string) => {
                if (string.length > 2500) {
                    let splitString = string.split(',');
                    splitString.splice(-1);
                    string = splitString.join(',');
                    stringCheck(string);
                }
                return string;
            };

            if (queryStringArr.length > 0) {
                if (searchType !== 'C') {
                    queryString = stringCheck(queryStringArr.join(','));
                }
            }
        }
        
        if (queryString !== resumeQuery) {
            // Clear cached resumes when user updates queryString
            component.set("v.resumePairs", []);
            component.set("v.resumeQuery", queryString);
        } else {
            component.set("v.resumeQuery", queryString);
        }
        
    },

    updateResults : function(component, event) {
		component.set("v.readyToLoadCandidateDetails",false);
        var results = component.get("v.results");

        if (results.hasError === false) {
            this.setResultValues(component);

        }
        // implement else block to show the errors

    },

    updateSelectedSort : function(component, event) {
        var results = component.get("v.results");
        if (results.sortField) {
            component.set("v.selectedSort", results.sortField + "|" + results.sortOrder);
        }
    },

    setScrollPosition : function(component) {
        // HACK ALERT - Using a timeout makes the results paint and the scrollTop set correctly.
        setTimeout(function() {
            var posn = component.get("v.position");
            if ((posn !== undefined && posn !== null && posn !== "")
                && document.getElementById("r-m-" + posn) !== null) {
                // Specific record position to set the scroll is available. And it is present on the page.
                var y = document.getElementById("r-m-" + posn).offsetTop;
                document.body.scrollTop = y;
                // Cross browser backup. If value was not set, try the alternative.
                if (document.body.scrollTop === 0) {
                    document.documentElement.scrollTop = y;
                }
                // Unset the position so it skips this condition after any page scrolls.
                component.set("v.position", "");
            } else {
                document.body.scrollTop = 0;
                // Cross browser backup.
                document.documentElement.scrollTop = 0;
            }
        }, 100);
        component.set("v.scrollPositionSet", true);
    },

    setResultValues : function(component) {
        // Get the records attribute.
        var results = component.get("v.results");

        //var currentOffset = parseInt(component.get("v.offset"));

        component.set("v.records", results.records);
        component.set("v.offset", parseInt(results.offset));
        component.set("v.totalResultCount", results.totalResultCount);

        var recs = component.get("v.records");//event.getParam("records");
        var oppId = component.get("v.opportunityId");
        var accId = component.get("v.accountId");
        var jobId = component.get("v.jobId");
        var recIds = [];

        for (var i=0; i<recs.length; i++) {
            recIds.push(recs[i].id);
        }		
        if(recIds.length > 0) {
            this.getContactListBadges(component, event, recIds);
        }

        if((oppId !== null && oppId !== undefined)||(jobId !== null && jobId !== undefined) || (accId !== null && accId !== undefined)){

            if(recIds.length > 0){
                //console.log(recIds);debugger;
                var action = component.get("c.getOrder");
                action.setParams({
                    "recordIds": recIds,
                    "opportunityId": oppId,
                    "jobId": jobId,
                    "accountId": accId
                });
                action.setCallback(this, function(response) {
                    if (response.getState() === "SUCCESS") {
                        var responseOrder = response.getReturnValue();
                        //console.log(responseOrder);debugger;
                        if(responseOrder !== null){
                            if(responseOrder.length > 0){
                                for(var i=0;i<=responseOrder.length;i++){
                                    // add dummy data here...
                                    if(typeof responseOrder[i] !== "undefined"){

                                        for(var j=0;j<=recs.length;j++){
                                            if(typeof recs[j] !== "undefined"){
                                                if((oppId !== null && oppId !== undefined)||(jobId !== null && jobId !== undefined)){
                                                    if(responseOrder[i].ShipToContact.AccountId === recs[j].id){
                                                        recs[j].linked = true;
                                                    }
                                                }else if((accId !== null && accId !== undefined)){
                                                    if(responseOrder[i].OpportunityId === recs[j].id){
                                                        recs[j].linked = true;
                                                    }
                                                }

                                            }

                                        }
                                    }

                                }
                                component.set("v.records", recs);
								component.set("v.selectedRecord",recs[0]);
                            }
                        }

                    } else if (response.getState() === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                /* console.log("Error message: " +
                                             errors[0].message);*/
                            }
                        } else {
                            //console.log("Unknown error");
                        }
                    }
					component.set("v.loadActionMenu", true);
                });
                $A.enqueueAction(action);
            }			
        }
		var recsUpdated = component.get("v.records");
		if (recsUpdated && recsUpdated.length > 0 ) {
			component.set("v.selectedRecord",recsUpdated[0]);			
			component.set("v.readyToLoadCandidateDetails",true);
		}

    },
    getContactListBadges : function(component, event, recordIds) {
        var action = component.get('c.getContactListBadges');
        var recs = component.get("v.records");
        action.setParams({"accountIds" : recordIds});
        action.setCallback(this, function(response) {
            if(response.getState() === "SUCCESS") {
                var responseContactListBadges = response.getReturnValue();
                if(responseContactListBadges) {
                    for(var i = 0; i <= recs.length; i++) {
                        if(typeof recs[i] !== "undefined") {
                            if(recs[i].id in responseContactListBadges) {
                                recs[i].contactList = responseContactListBadges[recs[i].id];
                            }
                        }
                    }
                    component.set("v.records", recs);
					component.set("v.selectedRecord",recs[0]);
                }
            }
        });
        $A.enqueueAction(action);
    },

    resetResults : function(component) {
        component.set("v.records", []);
        component.set("v.totalResultCount", 0);
        component.set("v.offset", 0);
    },

    saveUserPreferences : function(component) {
        component.set("v.userPrefs", {
            "ViewType" : component.get("v.toggleSearchView"),
            "ResumeExpanded" : component.get("v.expandToggle")
        });

        var action = component.get("c.saveUserPreferences");
        action.setParams({
            "prefs" : JSON.stringify(component.get("v.userPrefs"))
        });
        action.setCallback(this, function(response) {
            if (response.getState() !== "SUCCESS") {
                //console.log("Error saving preferences");
            }
        });

        $A.enqueueAction(action);
    },
    fireSearchPrefsUpdateEvt: function(component) {
        var prefsUpdateEvt = component.getEvent("prefsUpdateEvt");
        prefsUpdateEvt.setParams({
            "prefs" : component.get("v.userPrefs")
        });
        prefsUpdateEvt.fire();
    },
    setSearchTrackingIds: function(component, event){
        var results = component.get("v.results");
        component.set("v.transactionId", results.transactionId);
        component.set("v.requestId", results.requestId);
    }
})