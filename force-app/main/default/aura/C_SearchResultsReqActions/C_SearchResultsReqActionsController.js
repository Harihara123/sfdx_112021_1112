({
	
	findSimilar : function(component, event, helper) {
		var record = component.get("v.record");
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'req-talent_match_button--' + component.get('v.contextObjId'));
        trackingEvent.fire();
		if (record.geolocation && record.geolocation.lon && record.geolocation.lat) {
			helper.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")
				+ "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + component.get("v.contextObjId") 
				+ "&c__srcName=" + encodeURIComponent(record.opportunity_name)
				+ "&c__srcLoc=" + helper.buildLocationString(record)
				+ "&c__noPush=true&c__srcTitle="+record.position_title
				+ "&c__srcLat="+record.geolocation.lat+"&c__srcLong="+record.geolocation.lon
			);
		} else {
			helper.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")
				+ "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + component.get("v.contextObjId") 
				+ "&c__srcName=" + encodeURIComponent(record.opportunity_name)
				+ "&c__srcLoc=" + helper.buildLocationString(record)
				+ "&c__noPush=true&c__srcTitle="+record.position_title
			);
		}
	
	},


	linkSpecificCandidate:function(component, event, helper) {
		//S-85442 New modal for recently viewed candidate Rajeesh
		helper.linkSpecificCandidate(component,event);
	},
	addReqToList : function(component, event, helper) {
		
    },

	addSelfToOppTeam : function(component,event,helper){
	  helper.AddSelfToReqTeam(component);
	},
	showHideLinkMenuItem : function(component, event, helper) {
		helper.showHideLinkMenuItem(component);
	},
    
    showLinkToJobModal : function(component, event, helper) {
        //S-85028 - Ignore modal for Tek and AeroTek        
        var record = component.get("v.record");
        //console.log('showLinkToJobModal record = ' + record.opco_name);
        var opcoNamesToIgnoreModal = ["TEKsystems, Inc.", "Aerotek, Inc","AG_EMEA"];
        var currentOpcoName = record.opco_name;
        if(record != undefined && currentOpcoName != undefined && opcoNamesToIgnoreModal.includes(currentOpcoName)){
            // Save record and Toast
            helper.saveOrder(component);            
        }else{
			helper.fireLinkToJobEvent(component);            
        }				
	},

	redirectToCandSearch : function(component, event, helper) {
		//Neel summer 18 URL change->
		helper.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?opportunityId=" + component.get("v.contextObjId") + "&fromsubpage=FALSE");
	},
     openTabForCreateJobPosting : function(cmp, event, helper) {
        
       var compEvents = cmp.getEvent("jobPostingEvent");
       compEvents.setParams({ "contextFlag" : true });
       compEvents.fire();
       /*
        *  $A.createComponent(
            "c:CRM_RWSNavigation",{},
            function(newComp, status, errorMessage){
            if (status === "SUCCESS") {
                var body = cmp.get("v.body");
                body.push(newComp);
                cmp.set("v.body", body);
            }
            else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.")
            }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
            }
        }            
        );
        */
        
        //window.open("http://rws.allegisgroup.com/RWS/posting/displayDefaultSearchCriteria.action?screenName=MANAGEMY JOB POSTINGS","_blank");
        
    }



})