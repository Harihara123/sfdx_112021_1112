({
	sendToURL : function(url) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	},
    linkSpecificCandidate:function(component,event){

		//Rajeesh S-85442 - Link Req to Talent modal from req search results
            var modalBody;
            var modalHeader;
            var modalFooter;
            //var conrecord = component.get("v.contactrecord");
			var optyId = component.get("v.contextObjId");
            var talentId=component.get("v.accountId");
        
        	var record = component.get("v.record");
        	var ofccp = record.ofccp_required;
        
			//console.log('optyId:'+optyId+':accntId:'+talentId);//{"opportunityId":optyId}
			//console.log('oppfields'+component.get('v.oppFields'));
			/*
				"opportunity_name":
				"organization_id":
				"job_id": "0062400000ZuIEoAAN"
			*/
            $A.createComponents([
                ["c:C_LinkReqToTalentModal",{"opportunityId":optyId,"externalsource" : 'RQ',"aura:id":"reqsearchlinkmodal","oppFields":component.get('v.oppFields'),"ofccp":ofccp,"context":"ReqSearch"}]
            ],
                function(components, status){
                    if (status === "SUCCESS") {
                        modalBody = components[0];
                        modalHeader = "";
                        modalFooter = ""; 
                        component.find('overlayLib').showCustomModal({
                            header: modalHeader,
                            body: modalBody,
                            footer:modalFooter,
							//cssClass: "ResumeParseModal",
                            showCloseButton: true,
                            closeCallback: function() {
                                //console.log('closed the modal');
                            }
                        });
                    }
                }
               );
		
	},
     fireLinkToJobEvent : function(component) {
       var linkToJobEvent = $A.get("e.c:E_TalentLinkToJob");
        linkToJobEvent.setParams({
            "opportunityId": component.get("v.contextObjId"),
            "talentId": component.get("v.accountId"),
            "fromsubpage": "TALENT"
           // "opportunityId": component.get("v.opportunityId"), 
           // "jobId": component.get("v.jobId"), 
           // "talentId": component.get("v.contextObjId"),
           // "fromsubpage": component.get("v.fromsubpage")
        });
        linkToJobEvent.fire();
    },

	AddSelfToReqTeam : function(component){
	  var optyId = component.get("v.contextObjId");	  
	   var action = component.get("c.addSelfToOpportunityTeam"); 
        action.setParams({
            "optyId": optyId
        });

		action.setCallback(this, function(response) {
            
            var message ='' ;
            var toastStatus = "success";
            
            var toastEvent = $A.get("e.force:showToast");
            
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
				console.log('response'+response);
				if(response.isMemberAlreadyAdded){
				    message = "You are already added to the Opportunity team";
                    toastStatus = "error";
                }else{
                    toastStatus = "success";
                    message = 'You have been successfully added to the Opportunity team';
                }  
				                                              
                toastEvent.setParams({
				    "type":toastStatus,
					"message": message
				});
                toastEvent.fire();
                
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) || 
                        (errors[0] && errors[0].pageErrors[0] 
                            && errors[0].pageErrors[0].message)) {
                        /*console.log("Error message: " + 
                                 errors[0].message + " " + errors[0].pageErrors[0].message);*/
                    }
                } else {
                    console.log("Unknown error");
                }               
            }                                               
           });
         $A.enqueueAction(action); 
	},
    
	showHideLinkMenuItem : function(component) {
       /* var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');

		var action = component.get("c.getOrder"); 
        action.setParams({
        	"talentId": component.get("v.contextObjId"),
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId")
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                component.set ("v.getOrder", response);
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');

        });
        $A.enqueueAction(action); */
	},
    saveOrder: function(component) {
        var orderType = component.get ("v.jobId") == null ? "opportunity" : "job";
        var sourceField = component.find("sourceId");
        var source = null;
        if (sourceField != undefined) {
            source = sourceField.get("v.value");
        } 
        var action = component.get("c.saveOrder"); 
        action.setParams({
            "opportunityId": component.get("v.contextObjId"),
            "jobId": component.get("v.jobId"), 
            "accountId": component.get("v.accountId"),//component.get("v.talentId"),
            "status": "Linked", 
            "applicantSource": source,
            "type": orderType,
			"requestId": component.get("v.requestId"),
			"transactionId": component.get("v.transactionId"),
        });
        action.setCallback(this, function(response) {
            
            var message = "The Talent was successfully linked to the Req(s).";
            var toastStatus = "success";
            
            var toastEvent = $A.get("e.force:showToast");
            
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                if(response){
                    toastStatus = "success";
                    var appEvent = $A.get("e.c:E_RefreshSearchResultsLinked");
	                appEvent.setParams({
                    "opportunityId": component.get("v.contextObjId"),
                    "talentId": component.get("v.accountId") });
               		 appEvent.fire();
                }else{
                    message = "The Talent was not linked to the Req(s). Please try again.";
                    toastStatus = "error";
                }                                                
                toastEvent.setParams({
				    "type":toastStatus,
					"message": message
				});
                toastEvent.fire();
                
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) || 
                        (errors[0] && errors[0].pageErrors[0] 
                            && errors[0].pageErrors[0].message)) {
                        console.log("Error message: " + 
                                 errors[0].message + " " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }               
            }                                               
           });
        $A.enqueueAction(action); 
    },

	buildLocationString: function(record) {
		var loc = record.city_name ? record.city_name : "";
		loc += record.country_sub_division_code ? (loc !== "" ? ", " : "") + record.country_sub_division_code : "";
		loc += record.country_code ? (loc !== "" ? ", " : "") + record.country_code : "";

		return loc;
	}
})