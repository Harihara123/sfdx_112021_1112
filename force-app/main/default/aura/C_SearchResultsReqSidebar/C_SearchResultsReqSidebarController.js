({
  doInit: function(component, event, helper) {
    helper.doInit(component, event) ;
  },
    closeSidebar : function(component, event, helper) {
       helper.closeSidebar(component, event, helper)
    },
  showMoreToggle: function(cmp, e, h){
    const textToggle = cmp.get("v.textToggle");
    cmp.set("v.textToggle", !textToggle);
  }
});