({
  doInit: function(component) {
    const action = component.get("c.getOppSearchRecord");
    let recId = component.get("v.recordId");
    const incomingData = component.get("v.incomingData");
    //recId = "0065500000CcUj6";
    action.setParams({
      recordId: recId
    });
    component.set("v.loading", true);

    action.setCallback(this, function(response) {
      // console.log("return value" + JSON.stringify(response.getReturnValue()));
      let data = response.getReturnValue();
      let sidebarData = {};

      data.forEach(item => {
        if (item.Req_Hiring_Manager__r) {
          sidebarData.contactLink = "/" + item.Req_Hiring_Manager__r.Id;
          sidebarData.Name = item.Req_Hiring_Manager__r.Name; 
          const navService = component.find("navService");
          // Sets the route to /lightning/o/Account/home
          let pageReference = {
            type: "standard__recordPage",
            attributes: {
              objectApiName: "Contact",
              actionName: "view",
              recordId: item.Req_Hiring_Manager__r.Id
            }
          };
          navService.generateUrl(pageReference).then(
            $A.getCallback(function(url) {
              sidebarData.contactLink = url;
            }),
            $A.getCallback(function(error) {
              sidebarData.contactLink = '#';
            })
          );
        }
        
        if (item.Req_Skill_Details__c) {
          sidebarData.Req_Skill_Details__c = item.Req_Skill_Details__c;
          sidebarData.showMoreSkills = false;
          if(sidebarData.Req_Skill_Details__c.length > 500){
            sidebarData.trimmedReqSkillDetails = this.trimCommentText(sidebarData.Req_Skill_Details__c, 500);
            sidebarData.showMoreSkills = true;
          }
        } else {
          sidebarData.Req_Skill_Details__c = "N/A";
        }

        if (item.Req_Open_Positions__c) {
          sidebarData.Req_Open_Positions__c = item.Req_Open_Positions__c;
        } else {
          sidebarData.Req_Open_Positions__c = "N/A";
        }

        if (item.Req_EVP__c) {
            sidebarData.Req_EVP__c = item.Req_EVP__c;
            sidebarData.showMoreEVP = false;
            if(sidebarData.Req_EVP__c.length > 500){
              sidebarData.trimmedReqEvp = this.trimCommentText(sidebarData.Req_EVP__c, 255);
              sidebarData.showMoreEVP = true;
            }
        } else {
          sidebarData.Req_EVP__c = "N/A";
        }

        sidebarData.Req_Red_Zone__c = (item.Req_Red_Zone__c) ? "yes" : "no";

      });

      sidebarData.reqDuration = (incomingData.duration) ? `${incomingData.duration} ${(incomingData.duration_unit) ? incomingData.duration_unit : ''}` : 'N/A';
      sidebarData.Req_OFCCP_Required__c = (incomingData.ofccpRequired) ? "yes" : "no";
      sidebarData.Type = (incomingData.position_offering_type_code) ? incomingData.position_offering_type_code : "N/A";
      sidebarData.Req_Job_Description__c = (incomingData.position_description) ? incomingData.position_description : 'N/A';
      sidebarData.reqPayRate = incomingData.reqPayRate;
      sidebarData.reqSalary = incomingData.reqSalary;

      component.set("v.record", sidebarData);

      component.set("v.loading", false);

    });
    $A.enqueueAction(action);
  },
    
  closeSidebar : function(component, event, helper) {   

    component.set("v.showSidebar", 'hide-sidebar');
    component.set("v.selectedTab", "detailsTab");
       
    },

  trimCommentText: function(txt, length) {
    if(parseInt(txt.length) > length) {
      return txt.substring(0, length) + '...';
    } else {
      return '';
    }
  },
});