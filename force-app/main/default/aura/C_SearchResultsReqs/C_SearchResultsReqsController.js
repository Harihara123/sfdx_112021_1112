({
    doInit:function(component, event, helper){
		                
		//Rajeesh link req to talent modal- send all the data needed downstream as an object.
		var rcd = component.get('v.record');
		var oppFields = {Name:rcd.opportunity_name, AccountId:rcd.organization_id, Id: rcd.job_id}; 
		
		var recordId = component.get("v.recordId");

		component.set('v.oppFields',oppFields);
		
		 component.set("v.rowId", recordId);
		
    },

    setLinked : function(component, event, helper) {
        if((event.getParam("talentId") == component.get("v.accountId")) && (event.getParam("opportunityId") == component.get("v.recordId"))){
            component.set("v.linked",true);  
        } 
    },

    // handleMatchInsights : function(component, event, helper) {
    //     helper.fireMatchInsightsRequestEvt(component, component.get("v.record.opportunity_name"));
    // },

	// updateSelectRows : function(component, event, helper) {
	   
	// 	helper.sendIdsToMassActionCmp(component, event);
    // },
	//   expandCollapse : function(component, event, helper){
    //     var expanded = component.get("v.expanded"),
    //         rowId = component.get("v.rowId"),
    //         recordId = component.get("v.recordId");

    //     if(expanded && rowId == recordId){

    //         component.set("v.expanded", false);
            
    //     }else{
    //         component.set("v.expanded", true);
            
    //     }
    // },
	//  handleExpandAll : function(component, event, helper) {
    //     component.set("v.expanded", component.get("v.expandAllState"));
    // }
   
})