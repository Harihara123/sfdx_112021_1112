({
	sendIdsToMassActionCmp : function(component, event) {
        var reqID = event.getSource().get("v.text");	
		var orgID = event.getSource().get("v.name");
		var val = event.getSource().get("v.value");
        var appEvent = component.getEvent("massCandidateSelectTD");
		appEvent.setParams({ "val" : val ,
         "reqID" : reqID ,
         "orgID" : orgID });
        appEvent.fire();
    },
	//S-90955 - Match Insights
	fireMatchInsightsRequestEvt : function(component, recordName) {
        var matchInsightsReqEvt = component.getEvent("matchInsightsRequestEvt");
		matchInsightsReqEvt.setParams({
            "recordId" : component.get("v.recordId"),
			"type" : "C2R",
            "srcId" : component.get("v.automatchSourceId"),
			"recordName" : recordName
        });
        matchInsightsReqEvt.fire();
    }
})