({
 
    doInit: function (component, event, helper) {

        helper.buildListItems(component);

        const randomId = btoa(Math.random()).substring(0,12).toLowerCase();

        component.set("v.id", randomId);
    },

    handleActions: function (component, event, helper) {
        let action = event.currentTarget.getAttribute('data-action');

        const actions = {
            matchToTalent: () =>{
                helper.matchReqToTalent(component);
            },
            addSelfToOpptyTeam: () =>{
                helper.addSelfToOpptyTeam(component);
            }
        };

        actions[action]();
    }

})