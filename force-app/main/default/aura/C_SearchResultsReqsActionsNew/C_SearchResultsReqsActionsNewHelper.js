({
    sendToURL: function (url) {
        const urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },

    buildLocationString: function (record) {
        let loc = record.city_name ? record.city_name : "";
        loc += record.country_sub_division_code ? (loc !== "" ? ", " : "") + record.country_sub_division_code : "";
        loc += record.country_code ? (loc !== "" ? ", " : "") + record.country_code : "";

        return loc;
    },

    buildListItems: function (component) {

        const menuItems = [
            {
                label: $A.get('$Label.c.ATS_MATCH_REQ_TO_TALENT'),
                disabled: false,
                customIcon: $A.get('$Resource.ATS_MATCH_LOGO'),
                action: 'matchToTalent'
            },
            {
                label: $A.get('$Label.c.ATS_ADD_SELF_TO_OPPORTUNITY_TEAM'),
                disabled: false,
                action: 'addSelfToOpptyTeam'
            }

        ];

        component.set("v.menuItems", menuItems);
    },

    addSelfToOpptyTeam: function (component) {
        let optyId = component.get("v.contextObjId");
        let action = component.get("c.addSelfToOpportunityTeam");

        action.setParams({
            "optyId": optyId
        });

        let self = this.addSelfToOpptyTeam;
        action.setCallback(self, function (response) {

            let message = '';
            let toastStatus = "success";

            const toastEvent = $A.get("e.force:showToast");
            if(response){
                if (response.getState() === "SUCCESS") {
                    let responseVal = response.getReturnValue();

                    if (responseVal.isMemberAlreadyAdded) {
                        message = "You are already added to the Opportunity team";
                        toastStatus = "error";
                    } else {
                        toastStatus = "success";
                        message = 'You have been successfully added to the Opportunity team';
                    }

                } else if (response.getState() === "ERROR") {
                    let errors = response.getError();
                    if (errors) {
                        if ((errors[0] && errors[0].message) ||
                            (errors[0] && errors[0].pageErrors[0]
                                && errors[0].pageErrors[0].message)) {

                            toastStatus = "error";
                            message = "Error, unable to add to opportunity team , please contact admin.";
                            console.log(errors[0].message + " " + errors[0].pageErrors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }

                }
            } else {
                toastStatus = "error";
                message = "Unknown error , please contact admin.";
            }

            toastEvent.setParams({
                "type": toastStatus,
                "message": message
            });
            toastEvent.fire();
        });
        $A.enqueueAction(action); 
    },

    matchReqToTalent: function (component) {
        const record = component.get("v.record");
        const opptyId = component.get("v.contextObjId");
        
        const sourceIdTrackingEvt = $A.get("e.c:E_SearchResultsHandler");
        sourceIdTrackingEvt.setParam('sourceId', opptyId);
        sourceIdTrackingEvt.fire();

        const trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'req-talent_match_button--' + opptyId);
        trackingEvent.fire();

        if (record.geolocation && record.geolocation.lon && record.geolocation.lat) {
            this.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")
                + "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + opptyId
                + "&c__srcName=" + encodeURIComponent(record.opportunity_name)
                + "&c__srcLoc=" + this.buildLocationString(record)
                + "&c__noPush=true&c__srcTitle=" + record.position_title
                + "&c__srcLat=" + record.geolocation.lat + "&c__srcLong=" + record.geolocation.lon
            );
        } else {
            this.sendToURL($A.get("$Label.c.CONNECTED_Summer18URL")
                + "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + opptyId
                + "&c__srcName=" + encodeURIComponent(record.opportunity_name)
                + "&c__srcLoc=" + this.buildLocationString(record)
                + "&c__noPush=true&c__srcTitle=" + record.position_title
            );
        }

    }

})