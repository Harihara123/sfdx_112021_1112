({
    doInit: function (component, event, helper) {
        if (component.get('v.itemIndex') === 0) {
            let trackingEvent = $A.get("e.c:E_SearchResultsHandler");
            trackingEvent.setParam('clickTarget', Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1));
            trackingEvent.fire();
        }
        //Rajeesh link req to talent modal- send all the data needed downstream as an object.
        const rcd = component.get('v.record');
        let oppFields = { Name: rcd.opportunity_name, AccountId: rcd.organization_id, Id: rcd.job_id };

        const recordId = component.get("v.recordId");

        component.set('v.oppFields', oppFields);
        component.set("v.contextObjId", oppFields.Id);
        component.set("v.opportunityId", rcd.job_id );

        component.set("v.rowId", recordId);

        helper.setupSubmitLinkAction(component);
        
    },
    
    setLinked: function (component, event, helper) {
        if ((event.getParam("talentId") == component.get("v.accountId")) && (event.getParam("opportunityId") == component.get("v.recordId"))) {
            component.set("v.linked", true);
        }
        let trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'req_linked--' + component.get('v.recordId'));
        trackingEvent.fire();
    },

    togglePopover: function(cmp, e, h) {
        const showPopover = cmp.get("v.showPopover");
        cmp.set("v.showPopover", !showPopover);
    },

    toggleSkillList: function(cmp, e, h) {
        const toggleSkillState = cmp.get("v.skillToggle");
        const recordSkills = cmp.get("v.record").skills;

        cmp.set("v.skillToggle", !toggleSkillState);

        if (!toggleSkillState){
            cmp.set("v.trimmedSkills", recordSkills);
        } else {
            h.trimSkillsList(cmp, recordSkills);
        }
    },

    handleSubmitLinkAction: function(cmp, e, h){
        let action = e.currentTarget.getAttribute('data-action');

        const actions = {
            isDisabled : ()=>{
                return null;
            },
            
            linkSpecificCandidate: ()=> {
                h.linkSpecificCandidate(cmp);
            },

            showLinkToJobModal: ()=> {
                h.showLinkToJobModal(cmp);
            }

        }

        actions[action]();
        
    },
                
    toggleSidebar: function(component, event, helper){
        const sidebarState = component.get("v.showReqSidebar");
        const reqSidebar = component.find("reqSidebar");
        const recordId = component.get("v.recordId");

        switch (sidebarState) {
            case 'show-sidebar':
                component.set("v.showReqSidebar", 'hide-sidebar');
                break;
            case 'hide-sidebar':
                component.set("v.showReqSidebar", 'show-sidebar');
                reqSidebar.set("v.recordId", recordId);
                break;
            default:
                component.set("v.showReqSidebar", 'show-sidebar');
                reqSidebar.set("v.recordId", recordId);
                break;
        }
        let trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'req_preview--' + component.get('v.recordId'));
        trackingEvent.fire();
    },

    updateSelectRows: function (component, event, helper) {

        helper.sendIdsToMassActionCmp(component, event);
    }
})