({
    trimSkillsList : function(cmp, skills) {
       
        const skillDiv = document.getElementById(`skill-list-${cmp.get("v.rowId")}`);

        const windowStyle = window.getComputedStyle(skillDiv, null);

        let lineHeight = parseInt(windowStyle.getPropertyValue("line-height")),
            fontSize = parseInt(windowStyle.getPropertyValue("font-size").substring(0,2));

        // Check to see if line height is normal
        lineHeight = (isNaN(lineHeight)) ? (fontSize * 1.5) : lineHeight;
    
        let skillArea = Math.floor(((lineHeight * 3) * skillDiv.clientWidth) / 10);

        if (skills.length * fontSize > skillArea) {
            cmp.set("v.showMoreLess", true);
        } else {
            cmp.set("v.showMoreLess", false);
            cmp.set("v.skillToggle", true);
        }

        while (skills.length * fontSize > skillArea) {
            let skillsList = skills.split(',');
            skillsList.pop();
            skills = skillsList.join();
        }

        let trimmedSkills = skills;

        cmp.set("v.trimmedSkills", trimmedSkills);
       
    },

    setupSubmitLinkAction: function (cmp) {
        const reqStage = cmp.get("v.record.req_stage"),
              accountId = cmp.get("v.accountId");

        let reqMenuAction = {};

        if (accountId === null || accountId === undefined) {
            if (reqStage === 'Closed Wash' || reqStage === 'Closed Lost' || reqStage === 'Closed Won' || reqStage === 'Closed Won (Partial)') {

                reqMenuAction.label = $A.get('$Label.c.ATS_REQ_IS_ALREADY_CLOSED');
                reqMenuAction.action = 'disabled';
                reqMenuAction.disabled = true;

            } else {

                reqMenuAction.label = $A.get('$Label.c.ATS_LINK_TO_SPECIFIC_TALENT');
                reqMenuAction.action = 'linkSpecificCandidate';
                reqMenuAction.disabled = false;
            }
        } else {
            if (cmp.get("v.getOrder")) {
                reqMenuAction.label = $A.get('$Label.c.ATS_REQ_ALREADY_LINKED');
                reqMenuAction.action = 'disabled';
                reqMenuAction.disabled = true;

            } else {
                if (reqStage === 'Closed Wash' || reqStage === 'Closed Lost' || reqStage === 'Closed Won' || reqStage === 'Closed Won (Partial)') {
                    reqMenuAction.label = $A.get('$Label.c.ATS_REQ_IS_ALREADY_CLOSED');
                    reqMenuAction.action = 'disabled';
                    reqMenuAction.disabled = true;
                } else {
                    reqMenuAction.label = $A.get('$Label.c.ATS_LINK_TALENT_TO_REQ');
                    reqMenuAction.action = 'showLinkToJobModal';
                    reqMenuAction.disabled = false;

                }
            }
        }

        cmp.set("v.submitLinkAction", reqMenuAction);

    },

    linkSpecificCandidate: function (component) {

        //Rajeesh S-85442 - Link Req to Talent modal from req search results
        let modalBody;
        let modalHeader;
        let modalFooter;
        //var conrecord = component.get("v.contactrecord");
        const optyId = component.get("v.contextObjId");
        const talentId = component.get("v.accountId");

        const record = component.get("v.record");
        const ofccp = record.ofccp_required;

        $A.createComponents([
            ["c:C_LinkReqToTalentModal", { "opportunityId": optyId, "externalsource": 'RQ', "aura:id": "reqsearchlinkmodal", "oppFields": component.get('v.oppFields'), "ofccp": ofccp, "context": "ReqSearch" }]
        ],
            function (components, status) {
                if (status === "SUCCESS") {
                    modalBody = components[0];
                    modalHeader = "";
                    modalFooter = "";
                    component.find('overlayLib').showCustomModal({
                        header: modalHeader,
                        body: modalBody,
                        footer: modalFooter,
                        //cssClass: "ResumeParseModal",
                        showCloseButton: true,
                        closeCallback: function () {
                            //console.log('closed the modal');
                        }
                    });
                }
            }
        );

    },

    showLinkToJobModal: function (component) {
        //S-85028 - Ignore modal for Tek and AeroTek        
        const record = component.get("v.record");

        const opcoNamesToIgnoreModal = ["TEKsystems, Inc.", "Aerotek, Inc", "AG_EMEA"];
        let currentOpcoName = record.opco_name;
        if (record != undefined && currentOpcoName != undefined && opcoNamesToIgnoreModal.includes(currentOpcoName)) {
            // Save record and Toast
            this.saveOrder(component);
        } else {
            this.fireLinkToJobEvent(component);
        }
    },

    saveOrder: function (component) {
        let orderType = component.get("v.jobId") == null ? "opportunity" : "job";
        let sourceField = component.find("sourceId");
        let source = null;
        if (sourceField != undefined) {
            source = sourceField.get("v.value");
        }
        const action = component.get("c.saveOrder");
        let self = this.saveOrder;
        action.setParams({
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId"),
            "accountId": component.get("v.accountId"),//component.get("v.talentId"),
            "status": "Linked",
            "applicantSource": source,
            "type": orderType,
            "requestId": '',
            "transactionId": ''
        });
        action.setCallback(self, function (response) {

            let message = "The Talent was successfully linked to the Req(s).";
            let toastStatus = "success";

            const toastEvent = $A.get("e.force:showToast");

            if (response.getState() === "SUCCESS") {
                const responseVal = response.getReturnValue();
                if (responseVal) {
                    toastStatus = "success";
                    const appEvent = $A.get("e.c:E_RefreshSearchResultsLinked");
                    appEvent.setParams({
                        "opportunityId": component.get("v.contextObjId"),
                        "talentId": component.get("v.accountId")
                    });
                    appEvent.fire();
                } else {
                    message = "The Talent was not linked to the Req(s). Please try again.";
                    toastStatus = "error";
                }


            } else if (response.getState() === "ERROR") {
                const errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) ||
                        (errors[0] && errors[0].pageErrors[0]
                            && errors[0].pageErrors[0].message)) {
                        toastStatus = "error";
                        message = "The Talent was not linked to the Req(s). Please contact admin.";
                        console.log("Error message: " +
                            errors[0].message + " " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            toastEvent.setParams({
                "type": toastStatus,
                "message": message
            });
            toastEvent.fire();
        });
        $A.enqueueAction(action);
    },

    fireLinkToJobEvent: function (component) {
        const linkToJobEvent = $A.get("e.c:E_TalentLinkToJob");
        linkToJobEvent.setParams({
            "opportunityId": component.get("v.contextObjId"),
            "talentId": component.get("v.accountId"),
            "fromsubpage": "TALENT"
        });
        linkToJobEvent.fire();
    },

    sendIdsToMassActionCmp: function (component, event) {
        var reqID = event.getSource().get("v.text");
        var orgID = event.getSource().get("v.name");
        var val = event.getSource().get("v.value");
        var appEvent = component.getEvent("massCandidateSelectTD");
        appEvent.setParams({
            "val": val,
            "reqID": reqID,
            "orgID": orgID
        });
        appEvent.fire();
    }
})