({

    afterRender: function(cmp, h){
        this.superAfterRender();

        setTimeout(()=> {
            if (cmp.get("v.externalsource") !== 'TD'){
                h.trimSkillsList(cmp, cmp.get("v.record.skills"));
            }
        }, 100);
    }


})