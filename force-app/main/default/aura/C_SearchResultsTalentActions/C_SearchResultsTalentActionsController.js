({
	findSimilar : function(component, event, helper) {
		
		 var matchRequestEvt = component.getEvent("matchRequestEvt");
		matchRequestEvt.setParams({
			"type" : "C2C",
			"srcId" : component.get("v.contextObjId"),
			"automatchSourceData" : {
				"srcLinkId" : component.get("v.record.contact_id"),
				"srcName" : component.get("v.record.given_name") + " " + component.get("v.record.family_name")
			}
		});
		matchRequestEvt.fire();
		/*var record = component.get("v.record");
		helper.sendToURL(
                    $A.get("$Label.c.CONNECTED_Summer18URL")
                    + "/n/ATS_CandidateSearchOneColumn?c__matchTalentId=" + component.get("v.contextObjId")
                    + "&c__contactId=" + component.get("v.record.contact_id")
                    + "&c__srcName=" + record.given_name + " " + record.family_name
                    + "&c__noPush=true&c__type=C2C"
                );*/
    },

    automatchToReq : function(component, event, helper) {
		var record = component.get("v.record");

		 helper.sendToURL(
			$A.get("$Label.c.CONNECTED_Summer18URL")
			+ "/n/Search_Reqs?c__matchTalentId=" + component.get("v.contextObjId")
			+ "&c__contactId=" + component.get("v.record.contact_id")
			+ "&c__srcName=" + record.given_name + " " + record.family_name
			+ "&c__noPush=true"
		);

		/*var pageReference = {
            "type": "standard__navItemPage",
            "attributes": {
               "apiName": "Search_Reqs
            }
        };        
		component.set("v.pageReference", pageReference);
		var pr = component.get("v.pageReference");
        var navCmp = component.find("navCmp");
		event.preventDefault();
		navCmp.navigate(pageReference);*/
	},

    linkCandidateToJob : function(component, event, helper) {
        var profile = component.get("v.runningUser.profileName");
        
        if((profile == 'ATS_Recruiter')){
			helper.sendToURL("/apex/c__Search_Jobs?accountId=" + component.get("v.contextObjId"));
        }
		else{
		    var reqModalOpen = component.getEvent("OpenReqModal");
			// S-84229 open req search modal
			var reqModalObj = {"record":component.get("v.record"), "runningUser": component.get("v.runningUser")};
          reqModalOpen.setParams({"reqModalObj":reqModalObj});   
          reqModalOpen.fire();


		}
    },

    editCandidateContact : function(component, event, helper) {
        helper.sendToURL("/apex/c__Add_Edit_CandidateContact?id=" + component.get("v.contextObjId"));
    },

    showHideLinkMenuItem : function(component, event, helper) {
        //helper.showHideLinkMenuItem(component);
    },

    showLinkToJobModal : function(component, event, helper) {
                        
         //S-85722 - Ignore modal for Tek, AeroTek and EMEA
        var runningUser = component.get("v.runningUser");
        //S-228927 - adding new opcos SJA,IND,EAS 
        var newAerotek = $A.get("$Label.c.Aerotek");
        var newAstonCarter = $A.get("$Label.c.Aston_Carter");
        var newNewco = $A.get("$Label.c.Newco");
        var opcoNamesToIgnoreModal = ["TEK", "ONS","AG_EMEA", newAerotek, newAstonCarter, newNewco];
        var currentOpcoName = runningUser.opco;
        if(runningUser != undefined && currentOpcoName != undefined && opcoNamesToIgnoreModal.includes(currentOpcoName)){
            // Save record and Toast
            helper.saveOrder(component);
        }else{
			helper.fireLinkToJobEvent(component);
        }        
    },
     
    //Added by Krishna for Submit Talent to Req
    submitTalentToReq : function(component, event, helper) {
        helper.submitTalentToReq(component);    
    },

    openContactToListModal : function(cmp, event, helper) {
        // Tracking of the dynamically generated element - Add To List
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'add_to_list--'  + cmp.get('v.contextObjId'));
        trackingEvent.fire();
        
        var params = null;
        var recordId = cmp.get("v.contextObjId"); 
        var transactionId = cmp.get("v.transactionId");
        var requestId = cmp.get("v.requestId");
        params = {recordId: recordId,
                  requestId: requestId,  //S-71888 attributes for GUID added params
                  transactionId: transactionId
                 };
        var cmps = 'C_MassAddToContactListModal';
        var cmpEvent = cmp.getEvent("createCmpEventContactList");
        cmpEvent.setParams({
                "componentName" : 'c:' + cmps
                , "params":params
                ,"targetAttributeName": 'v.' + cmps });
        cmpEvent.fire();					 
    },

    openActivityModal: function(cmp, event, helper) {
        var modalEvt = cmp.getEvent("openActivityModal");
        var recordID = cmp.get("v.record.candidate_id");

        modalEvt.setParams({
            "recordId" : recordID, 
            "activityType" : "call",
            "eventOrigin" : "search" 
        });

        modalEvt.fire();

    },
    /*sendSMS :function(cmp,event,helper){
        var returnURL="/lightning/r/Contact/"+cmp.get("v.recordId");
        var pageName="tdc_tsw__SendSMS_SLDS?id";
        var modalBody;
        $A.createComponent("c:SendSMSContactComp", {
            "recordIdd": cmp.get("v.recordId"),
            "returnURL": returnURL,
            "pageName": pageName
         },
         function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                cmp.find('SMSoverlayLib').showCustomModal({
                       body: modalBody, 
                       showCloseButton: true,
                       cssClass: "mymodal",
                   })
               }                               
           });
    },*/
})