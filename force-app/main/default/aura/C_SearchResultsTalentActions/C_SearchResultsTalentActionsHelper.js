({
	sendToURL : function(url) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	},

	fireLinkToJobEvent : function(component) {
        var subPage = component.get("v.fromsubpage");
		var jobId = component.get ("v.jobId");
        if(subPage == undefined || subPage == null){
			if(jobId !== undefined && jobId !== null){
				subPage = "JOB";  // This is for Job Search Flow
			}
			else{
				subPage = "REQ"; // Indicates - Redirect to Req details page.
			}
        }else{
            subPage = "SUB"; // Indicates - Redirect to Req Submittal Page.
        }
        var linkToJobEvent = $A.get("e.c:E_TalentLinkToJob");
        linkToJobEvent.setParams({
            "opportunityId": component.get("v.opportunityId"), 
            "jobId": component.get("v.jobId"), 
            "talentId": component.get("v.contextObjId"),
            "fromsubpage": subPage
        });
        linkToJobEvent.fire();
    },

	showHideLinkMenuItem : function(component) {
       /* var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');

		var action = component.get("c.getOrder"); 
        action.setParams({
        	"talentId": component.get("v.contextObjId"),
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId")
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                component.set ("v.getOrder", response);
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');

        });
        $A.enqueueAction(action);*/
	},

	getParameterByName : function(cmp, name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    saveOrder: function(component)
    {
        var record = component.get ("v.record");
        var accountId = '';
        if(record.candidate_id != undefined){
            accountId = record.candidate_id;
        }
        var orderType = component.get ("v.jobId") == null ? "opportunity" : "job";
        var sourceField = component.find("sourceId");
        var source = null;
        if (sourceField != undefined) {
            source = sourceField.get("v.value");
        } 
        var action = component.get("c.saveOrder"); 
        action.setParams({
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId"), 
            "accountId": accountId,//component.get("v.talentId"),
            "status": "Linked", 
            "applicantSource": source,
            "type": orderType,
			"requestId": component.get("v.requestId"),
			"transactionId": component.get("v.transactionId"),
        });
        
        action.setCallback(this, function(response) {
            
            var message = "The Talent was successfully linked to the Req(s).";
            var toastStatus = "success";            
            var toastEvent = $A.get("e.force:showToast");            
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                if(response){
                    toastStatus = "success";
                    var appEvent = $A.get("e.c:E_RefreshSearchResultsLinked");
	                appEvent.setParams({
                    "opportunityId": component.get("v.opportunityId"),
                    "talentId": accountId });
               		 appEvent.fire();
                }else{
                    message = "The Talent was not linked to the Req(s). Please try again.";
                    toastStatus = "error";
                }                                                
                toastEvent.setParams({
				    "type":toastStatus,
					"message": message
				});
                toastEvent.fire();
                
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) || 
                        (errors[0] && errors[0].pageErrors[0] 
                            && errors[0].pageErrors[0].message)) {
                        console.log("Error message: " + 
                                 errors[0].message + " " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }               
            }                                               
           });
        $A.enqueueAction(action); 
     },
    
    //Added by Krishna for Submit Talent to Req
    submitTalentToReq: function(component)
    {
        var record = component.get ("v.record");
        var accountId = '';
        if(record.candidate_id != undefined){
            accountId = record.candidate_id;
        }
        
        var orderType = component.get ("v.jobId") == null ? "opportunity" : "job";
        var sourceField = component.find("sourceId");
        var source = null;
        if (sourceField != undefined) {
            source = sourceField.get("v.value");
        } 
        var action = component.get("c.saveOrder"); 
        action.setParams({
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId"), 
            "accountId": accountId,//component.get("v.talentId"),
            "status": "Submitted", 
            "applicantSource": source,
            "type": orderType,
			"requestId": component.get("v.requestId"),
			"transactionId": component.get("v.transactionId"),
        });
        
        action.setCallback(this, function(response) {
            
            var message = "The Talent was successfully submitted to the Req(s).";
            var toastStatus = "success";            
            var toastEvent = $A.get("e.force:showToast");            
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                if(response){
                    toastStatus = "success";
                    var appEvent = $A.get("e.c:E_RefreshSearchResultsLinked");
	                appEvent.setParams({
                    "opportunityId": component.get("v.opportunityId"),
                    "talentId": accountId });
               		 appEvent.fire();
                }else{
                    message = "The Talent was not submitted to the Req(s). Please try again.";
                    toastStatus = "error";
                }                                                
                toastEvent.setParams({
				    "type":toastStatus,
					"message": message
				});
                toastEvent.fire();
                
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) || 
                        (errors[0] && errors[0].pageErrors[0] 
                            && errors[0].pageErrors[0].message)) {
                        console.log("Error message: " + 
                                 errors[0].message + " " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }               
            }                                               
           });
        $A.enqueueAction(action); 
    }
})