({
    doInit:function(component,event,helper){
        if (component.get('v.itemIndex') === 0) {
            var trackingEvent = $A.get("e.c:E_SearchResultsHandler");
            trackingEvent.setParam('clickTarget', Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1));
            trackingEvent.fire();
        }

		helper.showFlameFlag(component, event);
        helper.populatePrevFeedback(component,event,helper);
        helper.buildMenuItems(component);

    },
	selectRecord : function(component, event, helper) {
        let counter = component.get('v.rowClickCounter');
        counter += 1;
        component.set('v.rowClickCounter', counter);
		const recordId = event.currentTarget.id;  
		// var prevRecordId = component.get("v.prevSelectedRecordId");

        //component.set("v.selectRow", '');
		// if (!prevRecordId || prevRecordId != recordId) {

        //     // THIS APPEARS TO BE REDUNDANT. ROW SELECTION IS PASSED TO SIDEBAR VIA ATTRIBUTE | Alex 4/26
		// 	// var talentSelectionEvt = $A.get("e.c:E_OpenTalentSideBarEvent");
		// 	// talentSelectionEvt.setParams({ "recordId":  recordId});
		// 	// talentSelectionEvt.fire();

        //     component.set("v.selectRow", recordId);
           
		// } else {
        //     component.set("v.selectRow", recordId );
           
        // }

        component.set("v.selectRow", recordId);

		component.set("v.prevSelectedRecordId",recordId);

    },

    deSelectRecord: function(component, event, helper) {
        component.set("v.visited", true);
		component.set("v.prevSelectedRecordId","");
        component.set("v.hidePopover", false);
    },

    handleAction: function(component, event, helper) {
        let targetId = event.currentTarget.id,
            action = event.currentTarget.getAttribute('data-action'),
            value = event.currentTarget.getAttribute('data-value');

        const actions = {
            phoneLink: () => {
                value = value.replace(/\s/g,'');
                helper.sendToURL("tel:" + value);
            },
            emailLink: () =>{
                helper.sendToURL("mailto:" + value);
            },
            fireToast: () =>{
                let toastMessage,
                    phoneId = `phone-${component.get("v.recordId")}`,
                    emailId = `email-${component.get("v.recordId")}`;

                if(component.get("v.showPhoneMenu") &&  targetId === phoneId){
                    toastMessage = 'No Phone Number Available';
                }
                if (component.get("v.showEmailMenu") && targetId === emailId) {
                    toastMessage = 'No Email Address Available';
                }

                helper.toastMessage(component, event, '', toastMessage, 'Error');
                event.stopPropagation();
            }

        };

        actions[action]();
        event.stopPropagation();

    },

    emailLink : function(component, event, helper) {
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'email_talent--' + component.get('v.recordId'));
        trackingEvent.fire();
        helper.sendToURL("mailto:" + component.get('v.emailAddr'));
        event.stopPropagation();
    },

    defPhoneLink : function(component, event, helper) {
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', 'call_talent--' + component.get('v.recordId'));
        trackingEvent.fire();
        let defPhone = component.get("v.phoneNumber");
        defPhone = defPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + defPhone);
        event.stopPropagation();
    },

    fireToast: function(component, event, helper) {
        let toastMessage,
            phoneId = `phone-${component.get("v.recordId")}`,
            emailId = `email-${component.get("v.recordId")}`;

        if(component.get("v.noPhone") && event.target.id === phoneId){
            var trackingEvent = $A.get("e.c:TrackingEvent");
            trackingEvent.setParam('clickTarget', 'call_talent--' + component.get('v.recordId'));
            trackingEvent.fire();
            toastMessage = 'No Phone Number Available';

        } else if (component.get("v.noEmail") && event.target.id === emailId) {

            var trackingEvent = $A.get("e.c:TrackingEvent");
            trackingEvent.setParam('clickTarget', 'email_talent--' + component.get('v.recordId'));
            trackingEvent.fire();
            toastMessage = 'No Email Address Available';

        }
        helper.toastMessage(component, event, '', toastMessage, 'Error');
        event.stopPropagation();
    },

    setLinked : function(component, event, helper) {
        if((event.getParam("talentId") == component.get("v.recordId")) && (event.getParam("opportunityId") == component.get("v.opportunityId"))){
            component.set("v.loadActionMenu",false);
            component.set("v.linked",true);
			component.set("v.loadActionMenu",true);
        }

    },

    highlightRow : function(component, event, helper) {
            var trackingEvent = $A.get("e.c:TrackingEvent");
            trackingEvent.setParam('clickTarget', 'select_row--' + component.get('v.recordId'));
            trackingEvent.fire();
        var toggleSearchView = component.get("v.toggleSearchView"),
            chkCmp;

        chkCmp = component.find("chkAddToCallSheetGrid");
        helper.sendIdsToMassActionCmp(component, chkCmp);

    },
    selectRow: function(component,event,helper){
        //Rajeesh S-84244 Ability to link from talent search: reusing the checkbox for linking as call list is not a functionality available from RQ search modal
        helper.selectRow(component,event);
    },
    handleSelectAll : function(component, event, helper) {
        var toggleSearchView = component.get("v.toggleSearchView"),
            chkCmp;

		component.find("chkAddToCallSheetGrid").set("v.value", component.get("v.selectAll"));
        chkCmp = component.find("chkAddToCallSheetGrid");        

        helper.sendIdsToMassActionCmp(component, chkCmp);
		helper.showFlameFlag(component, event, helper);
    },

    toggleSidebar : function(component, event, helper) {
        var recordId = component.get("v.recordId"),
            rowId = component.get("v.rowId");

        component.set("v.rowId", recordId);
        // component.set("v.showSidebar", true);
        helper.sendDataToSidebar(component);
    },

    updateFeedback: function (component, event, helper) {
        // let likedState = component.get("v.liked"),
        let disLikedState = component.get("v.disLiked"),
            buttonId = event.getSource().get("v.id");
         	var trackingEvent = $A.get("e.c:TrackingEvent");
        if (!disLikedState) {
            trackingEvent.setParam('clickTarget', 'set_thumbs_down--' + component.get('v.recordId')); 
        } else {
            trackingEvent.setParam('clickTarget', 'revert_thumbs_down--' + component.get('v.recordId'));             
        }
            trackingEvent.fire();  
        // if (buttonId.includes('liked')) {
        //     component.set("v.liked", !likedState);
        //     component.set("v.disLiked", false);
        // } else if (buttonId.includes('disLiked')) {
            component.set("v.disLiked", !disLikedState);
            component.set("v.liked", false);
        // }


		//var resultid = event.target.id;
		var feedbackEvt = component.getEvent("feedbackEvt");

		feedbackEvt.setParams({
			"resultIndex": component.get("v.itemIndex"),
			"resultDocId": component.get("v.recordId"),
			//"feedback": buttonId.includes('liked') ? component.get("v.liked") : false
            // "upvote": buttonId.includes('liked') ? component.get("v.liked"): false,
            "downvote": buttonId.includes('disLiked') ? component.get("v.disLiked"): false
		});

		feedbackEvt.fire();
        event.stopPropagation();

    },
    onClickTalentName: function(component, event, helper) {
            var trackingEvent = $A.get("e.c:TrackingEvent");
            trackingEvent.setParam('clickTarget', 'candidate_name--' + component.get('v.recordId'));
            trackingEvent.fire();
    }
})