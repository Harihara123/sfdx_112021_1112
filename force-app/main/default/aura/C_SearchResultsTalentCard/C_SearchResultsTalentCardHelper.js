({
            //var checkUndfn = queryStringUrl.slice(2, 11); //To check if teaser has search string

                    //console.log(JSON.stringify(component.get("v.record")));
                    //console.log(searchResponse);



                        /*var x = window.open('', '_blank');

                        if (x != null){
                            x.document.body.innerHTML = htmlResume;
                            x.document.title = fullName;
                        }
                        else {
                            var toastEvt = $A.get("e.force:showToast");

                            toastEvt.setParams({
                                title: "Opps, something went wrong!",
                                message: "In order to view resume, please allow pop-ups from this page.",
                                type: "error"
                            });

                            toastEvt.fire();
                        }*/




    buildUrl : function (parameters) {
        var qs = "";
        for(var key in parameters) {
            var value = parameters[key];
            qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
        }
        if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
        }
        return qs;





                    //window.open("/servlet/servlet.FileDownload?file=" + attachmentId, '_blank');

    },


    sendToURL : function(url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },

    buildMenuItems: function(component) {

        let record = component.get("v.record");
        // For Phone Numbers

        let phoneItems = [{
                label: 'Work: ',
                value: record.communication_work_phone
            },{
                label: 'Mobile: ',
                value: record.communication_mobile_phone
            },{
                label: 'Home: ',
                value: record.communication_home_phone
            },{
                label: 'Other: ',
                value: record.communication_other_phone
        }];

        this.processActionItems(component, phoneItems, 'phoneLink');


        // For Email addresses
        let emailItems = [{
            label: 'Email: ',
            value: record.communication_email
        },{
            label: 'Work: ',
            value: record.communication_work_email
        },{
            label: 'Other: ',
            value: record.communication_other_email
        }];

        this.processActionItems(component, emailItems, 'emailLink');

    },

    processActionItems: function(component, record, actionType) {
        let menuItems = [];
        let isNull = 0;
        let itemValue;

        record.map((items) => {
            let actionItem = {};
            if(items.value !== null) {
                let itemLabel = items.label + items.value;
                itemValue = items.value;
                actionItem.value = itemValue;
                actionItem.action = actionType;
                actionItem.label = itemLabel;
                menuItems.push(actionItem);
            } else {
                isNull++;
                actionItem.value = '';
                actionItem.action = 'fireToast';
                actionItem.label = items.label;
                menuItems.push(actionItem);
            }
        });

        if(menuItems.length === isNull){

            if(actionType === 'phoneLink') {
                component.set("v.noPhone", true);
            } else if(actionType === 'emailLink'){
                component.set("v.noEmail", true);
            }

        } else {
            if((menuItems.length - isNull) > 1) {

                if(actionType === 'phoneLink'){
                    component.set("v.menuItemsPhone", menuItems);
                    component.set("v.showPhoneMenu", true);
                } else if(actionType === 'emailLink'){
                    component.set("v.menuItemsEmail", menuItems);
                    component.set("v.showEmailMenu", true);
                }

            } else {
                // If menuItems only has 1 phone/email, return value
                if(actionType === 'phoneLink'){
                    component.set("v.showPhoneMenu", false);
                    component.set("v.phoneNumber", itemValue);
                } else if(actionType === 'emailLink') {
                    component.set("v.showEmailMenu", false);
                    component.set("v.emailAddr", itemValue);
                }

            }
        }

    },
    
    showFlameFlag : function(component, event,helper) {
  		var formattedEndDate = new Date(component.get("v.record.placement_end_date"));
		var formattedAvailableDate = new Date(component.get("v.record.person_availability"));
		var differEndDate = this.calculateDateDifference(new Date(),formattedEndDate);
		if(-46 < differEndDate && differEndDate < 31){
            component.set("v.fireIconEndDate",true);
        }
		var differAvailableDate = this.calculateDateDifference(new Date(),formattedAvailableDate);
		if(-46 < differAvailableDate && differAvailableDate < 31){
            component.set("v.fireIconAvailableDate",true);
        }
		if(component.get("v.fireIconAvailableDate") && component.get("v.fireIconEndDate")) {
            component.set("v.fireIconAvailableDate",false);
        }

    },

	calculateDateDifference : function (date1,date2){
		var timeDiffrence = (date2.getTime() - date1.getTime());
		var differDays = Math.ceil(timeDiffrence / (1000 * 3600 * 24)); 
        return differDays;
	},

    toastMessage: function(component, event, title, message, type ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title" : title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },


	populatePrevFeedback : function(component, event,helper){
        var feedback = component.get("v.feedback");
        var record = component.get("v.record");
        if(feedback){                    
            if(feedback[record.candidate_id]===1){
            	component.set("v.liked",true)
            }else if(feedback[record.candidate_id]===0){
                component.set("v.disLiked",true)
            }
            
        }
                
    },

    sendIdsToMassActionCmp : function(component, chkCmp) {
        var candidateId = component.get("v.recordId");
		var contactId = component.get("v.record.contact_id");
        //Get Event
        var appEvent = $A.get("e.c:E_SearchResultsAction");

        var chkCmp;
        if (chkCmp !== null && chkCmp !== undefined) {
           if (chkCmp.get("v.value")) {
                appEvent.setParams({ "idToAdd" : candidateId });
                appEvent.setParams({ "idToRemove" : "" });
				appEvent.setParams({ "contactIdToAdd" : contactId });
                appEvent.setParams({ "contactIdToRemove" : "" });
            } else {
                appEvent.setParams({ "idToAdd" : "" });
                appEvent.setParams({ "idToRemove" : candidateId });
				appEvent.setParams({ "contactIdToAdd" : "" });
                appEvent.setParams({ "contactIdToRemove" : contactId });
            } 

            //Fire Event
            appEvent.fire();
        }
    },

    sendDataToSidebar : function( component) {
        var record = component.get("v.record"),
            sidebarId = component.get("v.recordId"),
            //showSidebar = component.get("v.showSidebar"),
            contactId = record.contact_id,
            evt = component.getEvent("displayResultsSidebar");

        evt.setParams({
            "record" : record,
            "recordId" : sidebarId,
            // "toggleSidebar" : showSidebar,
            "contactId" : contactId
        });

        evt.fire();
    },

    selectRow: function(component,event){
        //Rajeesh selecting rows for linking.
        //send the row that is selected to linkreqtotalentmodal....

        var rowId = event.getSource().get("v.text");
        var isChecked= event.getSource().get("v.value");
        var talentSelectionEvt = $A.get("e.c:E_LinkReqToTalentSelectionUpdated");
        talentSelectionEvt.setParams({ "recordId":  rowId,"value":isChecked});
        talentSelectionEvt.fire();
    //S-90955 - Match Insights
    }

})