({
    afterRender: function(cmp, helper){
        this.superAfterRender();
		let card = document.getElementById(cmp.get('v.recordId'));
        if (card && card.getAttribute('tabindex') === '1') {
            card.focus();
            card.click();
        }

            card.addEventListener('keydown', (ev) => {

                if (ev.keyCode === 38) {
                    cmp.set("v.hidePopover", false);

                    let tabIndex;

                    try {
                        tabIndex = +document.activeElement.getAttribute('tabindex')
                    } catch (error) {
                        return false;
                    }

                    // up
                    ev.preventDefault();
                    let newInd = tabIndex - 1;
                if (newInd) {
                document.activeElement.classList.remove('selected');
				document.querySelectorAll('[tabindex="' + newInd + '"]')[0].focus();
                
                let cardInFocus = document.activeElement;
                let arrowTimer;
                let interval = 300;

                let keyUpEvt = (ev) => {

                if (ev.keyCode === 38 || ev.keyCode === 40) {

				clearTimeout(arrowTimer);
                arrowTimer = setTimeout(() => {

                cardInFocus.click();
				cardInFocus.removeEventListener('keyup', keyDownEvt);
				cardInFocus.removeEventListener('keyup', keyUpEvt);
            	}, interval);
	            }
	            }
    
			    let keyDownEvt = (ev) => {

			    if (ev.keyCode === 38 || ev.keyCode === 40) {
				clearTimeout(arrowTimer);				
				}
				}			  
                cardInFocus.addEventListener('keyup', keyUpEvt);
                cardInFocus.addEventListener('keydown', keyDownEvt);

	            }

            } else if (ev.keyCode === 40) {
                cmp.set("v.hidePopover", false);

                let tabIndex;

                try {
                    tabIndex = +document.activeElement.getAttribute('tabindex')
                } catch (error) {
                    return false;
                }

               
                // down
				ev.preventDefault();
                    document.activeElement.classList.remove('selected');
                let newInd = tabIndex + 1;
                if (document.querySelectorAll('[tabindex="' + newInd + '"]')[0]) {
                document.querySelectorAll('[tabindex="' + newInd + '"]')[0].focus();

                let cardInFocus = document.activeElement;
                let arrowTimer;
                let interval = 300;

                let keyUpEvt = (ev) => {

                if (ev.keyCode === 38 || ev.keyCode === 40) {

				clearTimeout(arrowTimer);
                arrowTimer = setTimeout(() => {

                cardInFocus.click();
				cardInFocus.removeEventListener('keyup', keyDownEvt);
				cardInFocus.removeEventListener('keyup', keyUpEvt);
            	}, interval);
	            }
	            }
    
			    let keyDownEvt = (ev) => {

			    if (ev.keyCode === 38 || ev.keyCode === 40) {
				clearTimeout(arrowTimer);				
				}
				}			  
                cardInFocus.addEventListener('keyup', keyUpEvt);
                cardInFocus.addEventListener('keydown', keyDownEvt);
                }

            }
            });
    
    }
})