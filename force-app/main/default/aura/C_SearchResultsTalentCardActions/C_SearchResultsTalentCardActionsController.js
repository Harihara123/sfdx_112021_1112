({
    doInit: function(component, event, helper) {

        helper.buildListItems(component);

    },

    handleActions : function (component, event, helper) {
        let Id = event.currentTarget.id,
            action = event.currentTarget.getAttribute('data-action');

        component.set("v.id", Id);
		
		helper.fireTrackingEvent(component, action);

        const actions = {
            matchToReq: () => {
                let record = component.get("v.record");
                let trackingEvent = $A.get("e.c:E_SearchResultsHandler");

                trackingEvent.setParam('sourceId', component.get("v.record.candidate_id"));
                trackingEvent.fire();

                helper.sendToURL(
                    $A.get("$Label.c.CONNECTED_Summer18URL")
                    + "/n/Search_Reqs?c__matchTalentId=" + component.get("v.id")
                    + "&c__contactId=" + component.get("v.record.contact_id")
                    + "&c__srcName=" + record.given_name + " " + record.family_name
                    + "&c__noPush=true"
                );
            },

            matchToSimilarTalent: () => {
                let trackingEvent = $A.get("e.c:E_SearchResultsHandler");
                let talentId = component.get("v.id");

                trackingEvent.setParam('sourceId', talentId);
                trackingEvent.fire();

                let matchRequestEvt = component.getEvent("matchRequestEvt");
                matchRequestEvt.setParams({
                    "type" : "C2C",
                    "srcId": talentId,
                    "automatchSourceData" : {
                        "srcLinkId" : component.get("v.record.contact_id"),
                        "srcName" : component.get("v.record.given_name") + " " + component.get("v.record.family_name")
                    }
                });
                matchRequestEvt.fire();

            },

            linkToReq: () => {

                let reqModalOpen = component.getEvent("OpenReqModal");
                // S-84229 open req search modal
                let reqModalObj = {"record":component.get("v.record"), "runningUser": component.get("v.runningUser")};
                reqModalOpen.setParams({"reqModalObj":reqModalObj});
                reqModalOpen.fire();

            },

            showLinkToJobModal: () => {
                //S-85722 - Ignore modal for Tek, AeroTek and EMEA
                var runningUser = component.get("v.runningUser");
                //S-228927 - adding new opcos SJA,IND,EAS 
        		var newAerotek = $A.get("$Label.c.Aerotek");
        		var newAstonCarter = $A.get("$Label.c.Aston_Carter");
        		var newNewco = $A.get("$Label.c.Newco");
                var opcoNamesToIgnoreModal = ["TEK", "ONS","AG_EMEA", newAerotek, newAstonCarter, newNewco];
                var currentOpcoName = runningUser.opco;
                if(runningUser != undefined && currentOpcoName != undefined && opcoNamesToIgnoreModal.includes(currentOpcoName)){
                    // Save record and Toast
                    helper.saveOrder(component);
                }else{
                    helper.fireLinkToJobEvent(component);
                }
            },

            submitTalentToReq: () => {
                helper.submitTalentToReq(component);
            },

            addToLists: () => {
                // Tracking of the dynamically generated element - Add To List
                /*let trackingEvent = $A.get("e.c:TrackingEvent");
                trackingEvent.setParam('clickTarget', 'add_to_list--'  + component.get('v.id'));
                trackingEvent.fire();*/

                let params = null;
                let recordId = component.get("v.id");
                let transactionId = component.get("v.transactionId");
                let requestId = component.get("v.requestId");
                params = {recordId: recordId,
                    requestId: requestId,  //S-71888 attributes for GUID added params
                    transactionId: transactionId
                };
                let cmps = 'C_MassAddToContactListModal';
                let cmpEvent = component.getEvent("createCmpEventContactList");
                cmpEvent.setParams({
                    "componentName" : 'c:' + cmps
                    , "params":params
                    ,"targetAttributeName": 'v.' + cmps });
                cmpEvent.fire();
            },

            logCall: () => {
                let modalEvt = component.getEvent("openActivityModal");
                let recordID = component.get("v.record.candidate_id");

                modalEvt.setParams({
                    "recordId" : recordID,
                    "activityType" : "call",
                    "eventOrigin" : "search"
                });

                modalEvt.fire();
            }
        };

        actions[action]();
    }

})