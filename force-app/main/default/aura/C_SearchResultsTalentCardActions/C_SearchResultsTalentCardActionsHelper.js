({
    sendToURL : function(url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },

    fireLinkToJobEvent : function(component) {
        var subPage = component.get("v.fromsubpage");
        var jobId = component.get ("v.jobId");
        if(subPage == undefined || subPage == null){
            if(jobId !== undefined && jobId !== null){
                subPage = "JOB";  // This is for Job Search Flow
            }
            else{
                subPage = "REQ"; // Indicates - Redirect to Req details page.
            }
        }else{
            subPage = "SUB"; // Indicates - Redirect to Req Submittal Page.
        }
        var linkToJobEvent = $A.get("e.c:E_TalentLinkToJob");
        linkToJobEvent.setParams({
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId"),
            "talentId": component.get("v.id"),
            "fromsubpage": subPage
        });
        linkToJobEvent.fire();
    },

    getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    //Added by Krishna for Submit Talent to Req
    submitTalentToReq: function(component) {

        var record = component.get ("v.record");
        var accountId = '';
        if(record.candidate_id != undefined){
            accountId = record.candidate_id;
        }

        var orderType = component.get ("v.jobId") == null ? "opportunity" : "job";
        var sourceField = component.find("sourceId");
        var source = null;
        if (sourceField != undefined) {
            source = sourceField.get("v.value");
        }
        var action = component.get("c.saveOrder");
        action.setParams({
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId"),
            "accountId": accountId,//component.get("v.talentId"),
            "status": "Submitted",
            "applicantSource": source,
            "type": orderType,
            "requestId": component.get("v.requestId"),
            "transactionId": component.get("v.transactionId"),
        });

        action.setCallback(this, function(response) {

            var message = "The Talent was successfully submitted to the Req(s).";
            var toastStatus = "success";
            var toastEvent = $A.get("e.force:showToast");
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                if(response){
                    toastStatus = "success";
                    var appEvent = $A.get("e.c:E_RefreshSearchResultsLinked");
                    appEvent.setParams({
                        "opportunityId": component.get("v.opportunityId"),
                        "talentId": accountId });
                    appEvent.fire();
                }else{
                    message = "The Talent was not submitted to the Req(s). Please try again.";
                    toastStatus = "error";
                }
                toastEvent.setParams({
                    "type":toastStatus,
                    "message": message
                });
                toastEvent.fire();

            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) ||
                        (errors[0] && errors[0].pageErrors[0]
                            && errors[0].pageErrors[0].message)) {
                        console.log("Error message: " +
                            errors[0].message + " " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    saveOrder: function(component){

        var record = component.get ("v.record");
        var accountId = '';
        if(record.candidate_id != undefined){
            accountId = record.candidate_id;
        }
        var orderType = component.get ("v.jobId") == null ? "opportunity" : "job";
        var sourceField = component.find("sourceId");
        var source = null;
        if (sourceField != undefined) {
            source = sourceField.get("v.value");
        }
        var action = component.get("c.saveOrder");
        action.setParams({
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId"),
            "accountId": accountId,//component.get("v.talentId"),
            "status": "Linked",
            "applicantSource": source,
            "type": orderType,
            "requestId": component.get("v.requestId"),
            "transactionId": component.get("v.transactionId"),
        });

        action.setCallback(this, function(response) {

            var message = "The Talent was successfully linked to the Req(s).";
            var toastStatus = "success";
            var toastEvent = $A.get("e.force:showToast");
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                if(response){
                    toastStatus = "success";
                    var appEvent = $A.get("e.c:E_RefreshSearchResultsLinked");
                    appEvent.setParams({
                        "opportunityId": component.get("v.opportunityId"),
                        "talentId": accountId });
                    appEvent.fire();
                }else{
                    message = "The Talent was not linked to the Req(s). Please try again.";
                    toastStatus = "error";
                }
                toastEvent.setParams({
                    "type":toastStatus,
                    "message": message
                });
                toastEvent.fire();

            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) ||
                        (errors[0] && errors[0].pageErrors[0]
                            && errors[0].pageErrors[0].message)) {
                        console.log("Error message: " +
                            errors[0].message + " " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    buildListItems: function(component){
        
        let opptyId = component.get("v.opportunityId") || null,
            jobId = component.get("v.jobId") || null,
            reqStage = component.get("v.reqStage");

        const type = component.get("v.type");

        let menuItems = [
            {
                label: (type === 'R2C') ? $A.get('$Label.c.ATS_MATCH_TO_REQ') : $A.get('$Label.c.ATS_FIND_SIMILAR_CANDIDATE'),
                disabled: false,
                customIcon: $A.get('$Resource.ATS_MATCH_LOGO'),
                action: (type === 'R2C') ? 'matchToReq' : 'matchToSimilarTalent'
            },
            {
                label: (type === 'R2C') ? $A.get('$Label.c.ATS_FIND_SIMILAR_CANDIDATE') : $A.get('$Label.c.ATS_MATCH_TO_REQ'),
                disabled: false,
                customIcon: $A.get('$Resource.ATS_MATCH_LOGO'),
                action: (type === 'R2C') ? 'matchToSimilarTalent' : 'matchToReq' 
            },
            {
                label: $A.get('$Label.c.ATS_ADD_TO_LIST'),
                disabled: false,
                action: 'addToLists'
            },
            {
                label: $A.get('$Label.c.ATS_LOG_A_CALL'),
                disabled: false,
                action: 'logCall'
            },

        ];

        let reqMenuAction = {};
		let reqMenuActionSubmit = {};

        if(opptyId === null && jobId === null) {
            if(reqStage === 'Closed Wash' || reqStage === 'Closed Lost' || reqStage === 'Closed Won' || reqStage === 'Closed Won (Partial)'){

                reqMenuAction.label = $A.get('$Label.c.ATS_REQ_IS_ALREADY_CLOSED');
                reqMenuAction.disabled = true;

            } else {
                reqMenuAction.label = $A.get('$Label.c.ATS_LINK_JOB');
                reqMenuAction.action = 'linkToReq';
                reqMenuAction.disabled = false;
            }
        } else {
            if(component.get("v.getOrder")) {
                reqMenuAction.label = $A.get('$Label.c.ATS_CANDIDATE_ALREADY_LINKED');
                reqMenuAction.disabled = true;

            } else {
                if(reqStage === 'Closed Wash' || reqStage === 'Closed Lost' || reqStage === 'Closed Won' || reqStage === 'Closed Won (Partial)') {
                    reqMenuAction.label = $A.get('$Label.c.ATS_REQ_IS_ALREADY_CLOSED');
                    reqMenuAction.disabled = true;
                } else {
                    reqMenuAction.label = $A.get('$Label.c.ATS_LINK_TALENT_TO_REQ');
                    reqMenuAction.action = 'showLinkToJobModal';
                    reqMenuAction.disabled = false;

					if(component.get("v.ofccpNotRequired")) {						
						reqMenuActionSubmit.label = $A.get('$Label.c.ATS_SUBMIT_TALENT_TO_REQ');
						reqMenuActionSubmit.action = 'submitTalentToReq';
						reqMenuActionSubmit.disabled = false;					
					}
                }				
            }
        }
        menuItems.splice(2, 0, reqMenuAction);
		if (reqMenuActionSubmit.label) {
			menuItems.splice(3, 0, reqMenuActionSubmit);
		}

       /* if(component.get("v.ofccpNotRequired")) {
            reqMenuAction = {};
            reqMenuAction.label = $A.get('$Label.c.ATS_SUBMIT_TALENT_TO_REQ');
            reqMenuAction.action = 'submitTalentToReq';
            reqMenuAction.disabled = false;
            menuItems.splice(3, 0, reqMenuAction);
        } */

        component.set("v.menuItems", menuItems);
    },

    fireTrackingEvent: function(component, action) {
        let trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('clickTarget', action + '--'  + component.get('v.id'));
        trackingEvent.fire();
    }
})