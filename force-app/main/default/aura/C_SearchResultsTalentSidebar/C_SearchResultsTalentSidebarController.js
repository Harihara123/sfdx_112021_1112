({
    closeSidebar : function(component, event, helper) {   
        helper.toggleSidebar(component);
        component.set("v.showSidebar", false);
		component.set("v.Comment", '');
        component.set("v.OrderLastModifiedDate", '');
        component.set("v.selectedTab", "profileTab");
    },

    emailLink : function(component, event, helper) {
    	helper.sendToURL("mailto:" + component.get('v.records.communication_email'));
    },
	 
    otherEmailLink : function(component, event, helper) {
    	helper.sendToURL("mailto:" + component.get('v.records.communication_other_email'));
    },

    prefEmailLink : function(component, event, helper) {
        helper.sendToURL("mailto:" + component.get('v.records.communication_preferred_email'));
    },

    //D-05950 - Added by Karthik
    prefPhoneLink : function(component, event, helper) {
        var prefPhone = component.get('v.records.communication_preferred_phone');
        prefPhone = prefPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + prefPhone);
       
    },

    homePhoneLink : function(component, event, helper) {
        var homePhone = component.get('v.records.communication_home_phone');
        homePhone = homePhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + homePhone);
       
    },

    workPhoneLink : function(component, event, helper){
        var workPhone = component.get("v.records.communication_work_phone");
        workPhone = workPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + workPhone);
    },

    mobilePhoneLink : function(component, event, helper) {
        var mobilePhone = component.get('v.records.communication_mobile_phone');
        mobilePhone = mobilePhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + mobilePhone);
        
    },

    otherPhoneLink : function(component, event, helper) {
        var otherPhone = component.get('v.records.communication_other_phone');
        otherPhone = otherPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + otherPhone);
        
    },

    loadTabData : function(component, event, helper){
        // Clear previous tab data
        helper.clearTabData(component);
       
        component.set("v.loading",true);

        //helper.checkSMS360Permission(component); 

        if(component.get("v.contactId").length > 0){
			//SMS tab refreshing
			/*let smsCmp = component.find("smsContainer");
			if(smsCmp) {
				smsCmp.refreshSMS();
			}*/
            // Get Resume Last Updated Date

            helper.getLastResumeDate(component);
            // Get Activities tab data
            helper.getResults(component,helper);
            helper.getResultsG2(component,helper);
            // Get G2 tab data
            helper.getAccountG2Data(component, helper);
            helper.getOrderRecords(component, event);
        }


    },

    setScrollHeight : function(component, event, helper){
        var resultsHeight = component.get("v.sidebarHeight");
   
        resultsHeight = resultsHeight - 64;
        component.set("v.scrollHeight", resultsHeight);
    },

    displayMoreComments: function(component, event, helper) {
		var i = event.getSource().get("v.title"),
            filterTitle = i + '',
            regex = /[a-z]/g,
            acts = component.get("v.activityRecords"),
            g2 = component.get("v.g2Activities");

            filterTitle = regex.test(filterTitle);

        if(filterTitle){
            i = i.replace(regex , '');
            acts[i].less = !acts[i].less;
            component.set("v.activityRecords", acts);
        } else {
            g2[i].less = !g2[i].less;
            component.set("v.g2Activities", g2);
        }

    },

	displayMoreDetails: function(component, event, helper) {
		var i = event.getSource().get("v.title"),
            filterTitle = i + '',
            acts = component.get("v.activityRecords");

        acts[i].less = !acts[i].less;
        component.set("v.activityRecords", acts);
    },

    displayMoreBadges: function(component, event, helper) {

        var initArray = component.get("v.currentSkills"),
            slicedArray = initArray.slice(0,10),
            badgeList = component.get("v.badgeList"),
            showMoreBadges = component.get("v.showMoreBadges");

        if(badgeList.length === 10) {
            component.set("v.badgeList", initArray);
        } else {
            component.set("v.badgeList", slicedArray);
        }

        component.set("v.showMoreBadges", !showMoreBadges);
        
    },

    displayMoreGoals: function(component, event, helper){

        var moreG2GoalText = component.get("v.moreG2Goals"),
            trimmedG2Goals = moreG2GoalText.substring(0, 255) + '&hellip;',
            g2Goals = component.get("v.g2Goals"),
            showMoreGoals = component.get("v.showMoreGoals"),
            goalTextLength = parseInt(g2Goals.length);

            if(moreG2GoalText){
                if(goalTextLength <= 263){
                    component.set("v.g2Goals", moreG2GoalText);
                } else {
                    component.set("v.g2Goals", trimmedG2Goals);
                }

                component.set("v.showMoreGoals", !showMoreGoals);
            }

    },

	displayMoreSkComments: function(component, event, helper){

        var moreSkComments = component.get("v.moreSkComments"),
            trimmedSkComments = moreSkComments.substring(0, 255) + '&hellip;',
            skComments = component.get("v.skComments"),
            showMoreSkComments = component.get("v.showMoreSkComments"),
            skCommentsLength = parseInt(skComments.length);

            if(moreSkComments){
                if(skCommentsLength <= 263){
                    component.set("v.skComments", moreSkComments);
                } else {
                    component.set("v.skComments", trimmedSkComments);
                }

                component.set("v.showMoreSkComments", !showMoreSkComments);
            }

    }
})