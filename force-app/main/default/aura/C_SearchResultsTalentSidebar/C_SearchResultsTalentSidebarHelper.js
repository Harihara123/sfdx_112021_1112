({
	toggleSidebar : function( component) {
        var evt = component.getEvent("displayResultsSidebar");
		var Validate = false;
        component.set("v.OrderRecVal" , Validate);
            evt.setParams({
                "recordId" : '',
                'contactId' : ''
            });

            evt.fire();
    },
	
	getLastResumeDate: function(component) {
	  var recId = component.get("v.records.candidate_id");
	  var bdata = component.find("basedatahelper");
	  var soqlString = `SELECT CreatedDate FROM Talent_Document__c WHERE mark_for_deletion__c = false and (Document_Type__c like 'Resume%' OR Document_Type__c like 'communities%') and committed_document__c = true and Talent__c = '${recId}' ORDER BY CreatedDate DESC`;

	  var params = {
		soql: soqlString,
		maxRows: '1'
	  };

	  bdata.callServer(component, '', '','c.getRows', function(response) {
		if(response.length > 0) {		  
		  component.set('v.resumeLastUpdated', response[0].CreatedDate);

		} else {
		  component.set('v.resumeLastUpdated', '');
		}
	  }, params, false );

	  },
	sendToURL : function(url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
    },
	
    getResults : function(component,helper){
        var listView = component.get("v.viewList");
        var recordID = component.get("v.contactId");

        if(listView && recordID !== ''){
            var apexFunc = listView[0].apexFunction;

            if(apexFunc){
                var params = listView[0].params;
                this.getViewFromApexFunction(component, apexFunc, recordID, params, helper, 'All');
            }

        }

    },
	
	getOrderRecords : function(component){
        var recordID = component.get("v.contactId"); 
        //alert(recordID);
        var Validate = false;
        component.set("v.OrderRecVal" , Validate);
        var params = {
            OrderContactRecordID: recordID,
            obj:''
	  	};
		var bdata = component.find("basedatahelper");        
        if(recordID !== ''){
            bdata.callServer(component, '', '','c.getOrderRecords', function(response) {
                //alert(response);
                if(response !=null){
                    var orderStatus = response.Status;
                    if(orderStatus.substr(0,14) == 'Not Proceeding') {	
                        Validate = true;
                        component.set("v.NotProceeding",true);
                        component.set("v.OrderRecVal" , Validate);
                        component.set("v.NorProceedinReason", response.Submittal_Not_Proceeding_Reason__c);	
						component.set("v.Comment", response.Comments__c);
                        component.set("v.OrderLastModifiedDate", response.Display_LastModifiedDate__c);						
                    } else {
                        component.set("v.OrderRecVal" , Validate);
                        component.set("v.NorProceedinReason", '');
                        component.set("v.Comment", response.Comments__c);
                        component.set("v.OrderLastModifiedDate", response.Display_LastModifiedDate__c);
                    }
                }else {
                    component.set("v.OrderRecVal" , Validate);
                    component.set("v.Comment", '');
                    component.set("v.OrderLastModifiedDate", '');
                }
                
			}, params, false );
        }

    },

    getResultsG2 : function(component,helper){
        var listView = component.get("v.viewListG2");
        var recordID = component.get("v.contactId");

        if(listView && recordID !== ''){
            var apexFunc = listView[0].apexFunction;

            if(apexFunc){
                var params = listView[0].params;
                this.getViewFromApexFunction(component, apexFunc, recordID, params, helper, 'G2');

            }

        }
    },

    getViewFromApexFunction : function(component, apexFunction, recordID, params, helper, whichList){
        var className = '';
        var subClassName = '';
        var methodName = '';
        var self = this;

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        if (methodName !== '' && recordID !== '') {
            component.set("v.loadingData",true);
            var actionParams = {};
            if(params && params.length != 0) {

                // if(whichList === 'All'){
                //     actionParams = {
                //         "recordId": recordID,   // <===== RECORD ID IS CONTACT ID!!!
                //         "maxItems" : params.maxItems
                //     };
                // } else if(whichList === 'G2'){
                //     actionParams = {
                //         "recordId": recordID,   // <===== RECORD ID IS CONTACT ID!!!
                //         "maxItems" : params.maxItems,
                //         "filterCriteria" : params.filterCriteria[0]
                //     };
                // }

                if(whichList === 'All'){
                    actionParams = {
                        "recordId": recordID,
                        "maxItems" : '5',
                        "filterCriteria" : '',
                        "getActivityIcons" : false
                    };
                } else if (whichList === 'G2'){
                    actionParams = {
                        "recordId": recordID,
                        "maxItems" : '2',
                        "filterCriteria" : 'G2',
                        "getActivityIcons" : false
                    };
                }
            }
         
            var bdata = component.find("basedatahelper");

			var self = this;
            var records = [];
            var hasG2ActivityComments = false;

            bdata.callServer(component, className, subClassName, methodName,function(response) {
               
                if (response && response.length > 0) {

                   
                    for (var i = 0; i < response.length; i++) {
                        response[i].less = true;
                        response[i].sidebarList = true;
						response[i].detailsTxt = self.getDetailsText(response[i]);
                        if(response[i].Detail !== undefined){
                            hasG2ActivityComments = true;
                        }
                        records.push(response[i]);
                    }

                    if(records.length > 0) {
                       if (whichList === 'All') {
                            component.set ("v.hasPastActivity", true);
                            component.set("v.activityRecords", records);
                       } else if (whichList === 'G2') {
                            component.set("v.hasG2Activities", hasG2ActivityComments);
                            component.set("v.hasG2Info", hasG2ActivityComments);
                            component.set("v.g2Activities", records);
                        }
                    }
                }
                  
                    component.set("v.loadingCount",false);
                    component.set("v.loading", false);
            }, actionParams, false );
        }
    },

	getDetailsText : function(activity) {
		var dTxt = {};

		if (activity.ActivityType ==='Interviewing') {
			dTxt = this.parseInterviewComments(dTxt, activity);
		} else {
			// Text formatted for unescapedHtml component
			if(activity.PreMeetingNotes !== undefined) {
				dTxt["preNotes"] = activity.PreMeetingNotes;
			}

			if(activity.PostMeetingNotes !== undefined) {
				// Events will have PostMeetingNotes
				dTxt["postNotes"] =  activity.PostMeetingNotes;
				dTxt["trimmed"] = this.trimCommentText(dTxt.postNotes);
			} else if (activity.Detail !== undefined) {
				// Tasks will have Detail
				dTxt["details"] = activity.Detail;
				dTxt["trimmed"] = this.trimCommentText(dTxt.details);
			}
		}

		return dTxt;
	},

	parseInterviewComments : function(dTxt, activity) {
        var description = activity.PostMeetingNotes;
        if (description) {
            description = description.replace(/(?:\r\n)/g, '<br/>');
                                                   
            if (description !='not defined') {
                try {
                    var obj = JSON.parse(description);
                    var candidateCommentText =  obj.CandidateComments;
                    var clientCommentText  =    obj.ClientComments;
                                               
                    if(candidateCommentText){
                        dTxt["candComments"] =  candidateCommentText;
                    }
                    if(clientCommentText){
                        dTxt["clientComments"] =  clientCommentText;
                    }
					dTxt["trimmed"] = description.length > 350 ? candidateCommentText.substring(0, 255) + ' ...' : '';
                                                           
                } catch(error) {
                    // Do nothing. Interview comments JSON not well formed.
                }
            }
        } 

		return dTxt;
	},

    trimCommentText: function(txt) {
        if(parseInt(txt.length) > 255) {
            return txt.substring(0, 255) + '...';
        } else {
            return '';
        }
    },

    getAccountG2Data: function(component, helper){
        var recordId = component.get("v.sidebarId"),
            soql = `SELECT Id, (Select AccountId, Website_URL__c, WorkExtension__c, RWS_DNC__c, Do_Not_Use__c from Contacts where AccountId = '${recordId}' LIMIT 1), Skills__c, Skill_Comments__c, Goals_and_Interests__c, G2_Completed_By__c, G2_Completed_Date__c, G2_Completed__c, Do_Not_Contact__c FROM Account WHERE Id = '${recordId}'`,
            params = {"soql": soql},
            bdata = component.find("basedatahelper");

            if(recordId !== undefined){
                bdata.callServer(component, '', '', "getRecord", function(response){
              
                    if(response !== null && response !== undefined){
							//Added by Kaavya K: S-85063 To get contact info for Profile panel based on one-to-one contact for the Account AKA Website URL
							for (var i = 0; i < response.Contacts.length; i++) { 
								component.set("v.contactRecord", response.Contacts[i]); 
								
							}
							component.set("v.DNC", response.Do_Not_Contact__c);
                            component.set("v.DNU", response.Contacts[0].Do_Not_Use__c);

                        //
                            if (response.Skills__c && response.Skills__c !== '') {
								var skillList = JSON.parse(response.Skills__c);
								var badgeList = skillList.skills;
								var badgeCount = badgeList.length;
							}

                            if(badgeCount !== undefined && badgeCount > 0){

                                // Skills formatting
                    
                                if(badgeCount > 10){
                                        var trimmedSkillList = badgeList.slice(0,10);

                                        component.set("v.badgeList", trimmedSkillList);
                                        component.set("v.currentSkills", badgeList);
                                        component.set("v.showMoreBadges", true);
                                } else {
                                         component.set("v.currentSkills", badgeList);
                                         component.set("v.badgeList", badgeList);
                                }

                                component.set("v.hasG2Info", true);
                            }

                            if (!$A.util.isEmpty(response.Goals_and_Interests__c)) {
                                // Goals Formatting
                                var goalText = response.Goals_and_Interests__c;

                                    if(parseInt(goalText.length) > 255){
                                        var trimmedGoalText = goalText.substring(0, 255) + '&hellip;';
                                       component.set("v.g2Goals", trimmedGoalText);
                                       component.set("v.moreG2Goals", goalText);
                                    } else {
                                        component.set("v.g2Goals", goalText);
                                    }
                                
                                component.set("v.hasG2Info", true);
                            }

							if (!$A.util.isEmpty(response.Skill_Comments__c)) {
                                // Skill comments Formatting
                                var skCommText = response.Skill_Comments__c;

                                    if(parseInt(skCommText.length) > 255){
                                        var trimmedSkCommText = skCommText.substring(0, 255) + '&hellip;';
                                       component.set("v.skComments", trimmedSkCommText);
                                       component.set("v.moreSkComments", skCommText);
                                    } else {
                                        component.set("v.skComments", skCommText);
                                    }
                                
                                component.set("v.hasG2Info", true);
                            }

                    }
                    
              
            }, params, false);

        }
    },

    clearTabData: function(component){

			//Added by Kaavya K: S-85063 Clear contact record 
				component.set("v.contactRecord",null);
            
            //Clear Activities
                component.set("v.activityRecords",[]);
                component.set("v.hasPastActivity", false);

            // Clear G2 info
                component.set("v.showMoreGoals", false);
                component.set("v.g2Activities", []);
                component.set("v.hasG2Activities", false);
                component.set("v.g2Goals","");
                component.set("v.moreG2Goals", "");
                component.set("v.skComments","");
                component.set("v.moreSkComments", "");
				component.set("v.currentSkills", []);
                component.set("v.badgeList", []);
                component.set("v.hasG2Info", false);

    },

	/*checkSMS360Permission:function(component) { 

  
      var bdata = component.find("basedatahelper");

      var params = { permissionName: 'SMS' };

      bdata.callServer(component, '', '','c.userHasAccessPermission', function(response) {

        if(response) 
            {
                console.log('user has access SMS 360 App' + response);
                component.set('v.userHasSMS360Access',true);
                
            }
            else{
                console.log('something unexpected happened---');
            }

      }, params, false );
 
    }*/
})