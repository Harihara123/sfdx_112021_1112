({

    emailLink : function(component, event, helper) {
    	helper.sendToURL("mailto:" + component.get('v.records.communication_email'));
    },
	 
    otherEmailLink : function(component, event, helper) {
    	helper.sendToURL("mailto:" + component.get('v.records.communication_other_email'));
    },

    prefEmailLink : function(component, event, helper) {
        helper.sendToURL("mailto:" + component.get('v.records.communication_preferred_email'));
    },

    //D-05950 - Added by Karthik
    prefPhoneLink : function(component, event, helper) {
        var prefPhone = component.get('v.records.communication_preferred_phone');
        prefPhone = prefPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + prefPhone);
       
    },

    homePhoneLink : function(component, event, helper) {
        var homePhone = component.get('v.records.communication_home_phone');
        homePhone = homePhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + homePhone);
       
    },

    workPhoneLink : function(component, event, helper){
        var workPhone = component.get("v.records.communication_work_phone");
        workPhone = workPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + workPhone);
    },

    mobilePhoneLink : function(component, event, helper) {
        var mobilePhone = component.get('v.records.communication_mobile_phone');
        mobilePhone = mobilePhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + mobilePhone);
        
    },

    otherPhoneLink : function(component, event, helper) {
        var otherPhone = component.get('v.records.communication_other_phone');
        otherPhone = otherPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + otherPhone);
        
    },

    loadTabData : function(component, event, helper){
        let s = component.get('v.sidebarId');

        let pairsReceived;
        if (s) {
            helper.getResults(component,helper);
            component.set('v.resumeLoaded', false);
            component.set('v.htmlResume', '');
            pairsReceived = component.get('v.resumePairs') || [];



            if (pairsReceived.length && pairsReceived[0].id && component.get('v.sidebarId')) {
                     component.set('v.resumeLoaded', true);
                     let resumeToDisplay = pairsReceived.find(resume => resume.id === component.get('v.sidebarId'));
                if (resumeToDisplay) {
                if ((resumeToDisplay|| {}).resume) {
                    // const regex = /<em(.*?)\/em>/g;
                    //
                    // resumeToDisplay.resume = resumeToDisplay.resume.replace(regex, `<a class="resume-link">$&</a>`);

                    component.set('v.htmlResume', resumeToDisplay);
                } else {
                    // a resume for selected talent ID was requested, but it was missing in the response
            }
            } else {
                component.set('v.rowClickCounter', 0);
                component.set('v.resumeLoaded', false);
                var resumeEvt = component.getEvent("resumeRequest");
				resumeEvt.setParams({ "talentID" : component.get('v.sidebarId') });
				resumeEvt.fire()
            }   
        }
        }
		if (component.get("v.selectedRecord.record")) {
			var record = component.get("v.selectedRecord.record");
			if (record && record.contact_id) {
				component.set("v.records",record);
				component.set("v.contactId",record.contact_id);
			}
		}
        // Clear previous tab data
        helper.clearTabData(component);
       
        component.set("v.loading",true);

        const currentUser = component.get("v.runningUser");

        if (currentUser.hasSMSAccess) {
            helper.checkSMS360Permission(component);
        }


        if(component.get("v.contactId")) {

            //SMS tab refreshing
            let smsCmp = component.find("smsContainer");
            if(smsCmp) {
                smsCmp.refreshSMS();
            }

            // Get Resume Last Updated Date
            helper.getLastResumeDate(component);
            // Get Activities tab data
            helper.getResults(component);

            // Get G2 tab data
            helper.getOrderRecords(component, event);
            // Get AG Work History
            helper.getResultsAGHistory(component);

        }


    },

    toggleG2: function(cmp, e, h) {
        const g2Loaded = e.getParam("g2Loaded");
        cmp.set("v.hasG2", g2Loaded);

    },
    
    resumeChanged: function(component, event, helper) {
        component.set('v.resumeLoaded', false);
		component.set('v.htmlResume', '');
        let pairsReceived = component.get('v.resumePairs') || [];

        if (pairsReceived.length && pairsReceived[0].id && component.get('v.sidebarId')) {
                 component.set('v.resumeLoaded', true);
                 let resumeToDisplay = pairsReceived.find(resume => resume.id === component.get('v.sidebarId'));
            if (resumeToDisplay) {
            if ((resumeToDisplay|| {}).resume) {
			component.set('v.htmlResume', resumeToDisplay);
            } 
            }   
        }
    },


    activateResumeTab: function(component, event, helper) {
        let idEls = document.querySelectorAll('[id]');
        let resumeTab;
        for (let i = 0; i < idEls.length; i++) {
            if (idEls[i].getAttribute('id').includes('resumeTab')) {
               resumeTab = idEls[i]; 
            }
        }
        if (resumeTab) {
            resumeTab.click();
        }
    },

    fetchResumes: function(component, event, helper){
        let modalEvt = component.getEvent("openResumeModal"),
            pairsReceived = component.get('v.resumePairs') || [],
            resumeModalNav = component.get("v.resumeIdArr") || [],
            index = component.get("v.newIndex"),
            resumeId = resumeModalNav[index],
            resumeIndex = pairsReceived.findIndex(item => item.id === resumeId);

        if(resumeIndex !== -1){
            component.set("v.sidebarId", resumeId);
            modalEvt.setParams({
                "resumeID" : resumeId,
                "resumeArr" : pairsReceived[resumeIndex],
                "currentIndex" : index,
                "resumeCount" : resumeModalNav.length
            });
            modalEvt.fire();
        }

    },

    openResumeModal: function (component, event, helper) {
        helper.openResumeModal(component);
    },
    
    handleServerError: function(component, event, helper) {
    component.set('v.serverError', true);
        setTimeout(() => {
            let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            svg.setAttribute('height', '300px');
            svg.setAttribute('width', '300px');
            svg.style.display = 'block';
            svg.style.margin = '100px auto 0 auto';
            svg.setAttribute('viewBox', '-21 -21 682 682.66669');

            
        let g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        
        g.setAttribute('fill', '#428dff');
        
        let path1 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path1.setAttribute('d', 'm256.1875 640c-17.613281-.042969-31.957031-14.390625-32-31.96875v-10.804688c-.019531-7.753906 1.203125-15.433593 3.625-22.847656.671875-2.238281 1.550781-4.671875 2.558594-7.039062 11.960937-27.277344 38.820312-44.855469 68.472656-44.886719h10.675781c32.433594.140625 61.394531 19.542969 73.835938 49.453125 2.269531 5.4375-.3125 11.679688-5.75 13.9375-5.421875 2.285156-11.679688-.296875-13.941407-5.746094-9.132812-21.964844-30.410156-36.214844-54.199218-36.300781h-10.621094c-21.1875.023437-40.386719 12.601563-48.894531 32.007813-.664063 1.558593-1.28125 3.265624-1.792969 4.996093-1.792969 5.472657-2.65625 10.914063-2.648438 16.417969v10.78125c.011719 5.859375 4.8125 10.65625 10.691407 10.667969h181.296875c5.859375-.011719 10.660156-4.808594 10.667968-10.699219v-63.007812c0-5.898438 4.765626-10.667969 10.664063-10.667969s10.671875 4.769531 10.671875 10.667969v63.039062c-.046875 17.609375-14.390625 31.957031-31.96875 32zm0 0');

        let path2 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path2.setAttribute('d', 'm309.519531 640h-181.335937c-17.628906-.0625-31.964844-14.433594-31.996094-32.023438v-10.707031c-.015625-41.160156 33.472656-74.722656 74.644531-74.792969h10.6875c29.683594 0 56.542969 17.566407 68.4375 44.765626 2.367188 5.398437-.097656 11.695312-5.503906 14.039062-5.390625 2.375-11.679687-.101562-14.035156-5.503906-8.492188-19.425782-27.691407-31.980469-48.898438-31.980469h-10.667969c-29.40625.054687-53.339843 24.03125-53.332031 53.464844v10.683593c.011719 5.878907 4.8125 10.6875 10.699219 10.710938h181.300781c5.886719 0 10.667969 4.765625 10.667969 10.664062 0 5.898438-4.78125 10.679688-10.667969 10.679688zm0 0');  

        let path3 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path3.setAttribute('d', 'm53.585938 533.15625c-12.6875 0-25.402344-4.484375-35.550782-13.5625-21.808594-19.539062-23.738281-53.246094-4.277344-75.15625 9.03125-10.109375 17.246094-21.042969 24.421876-32.503906 3.136718-4.988282 9.71875-6.484375 14.699218-3.371094 4.992188 3.136719 6.496094 9.71875 3.371094 14.714844-7.816406 12.464844-16.757812 24.359375-26.570312 35.347656-11.644532 13.117188-10.492188 33.351562 2.59375 45.078125 13.085937 11.730469 33.332031 10.667969 45.105468-2.371094 4.066406-4.582031 8.097656-9.320312 11.914063-14.175781 3.652343-4.617188 10.363281-5.417969 14.992187-1.769531 4.628906 3.644531 5.414063 10.355469 1.78125 14.972656-4.078125 5.167969-8.375 10.242187-12.785156 15.203125-10.535156 11.667969-25.097656 17.59375-39.695312 17.59375zm0 0');

        let path4 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path4.setAttribute('d', 'm26.609375 245.644531c-2.820313-.492187-5.710937.191407-8 1.886719-2.292969 1.695312-3.796875 4.265625-4.160156 7.09375-2.15625 14.589844-3.363281 29.300781-3.597657 44.042969h21.332032c.238281-13.699219 1.367187-27.359375 3.363281-40.917969.679687-5.761719-3.230469-11.0625-8.9375-12.105469zm0 0');

        let path5 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path5.setAttribute('d', 'm309.519531 21.332031v-21.332031c-54.519531.179688-107.957031 15.199219-154.582031 43.46875-11.617188 6.984375-22.753906 14.730469-33.335938 23.1875-5.320312 4.179688-10.121093 8.992188-15.199218 13.46875l-7.503906 6.851562c-2.382813 2.398438-4.589844 4.972657-6.894532 7.457032-18.625 19.722656-34.464844 41.910156-47.0625 65.9375l-4.808594 9.964844c-1.3125 2.53125-1.511718 5.492187-.566406 8.179687.953125 2.691406 2.964844 4.863281 5.566406 6.03125 5.335938 2.3125 11.554688.125 14.261719-5.015625l4.449219-9.257812c11.703125-22.3125 26.410156-42.914063 43.710938-61.230469 2.132812-2.300781 4.183593-4.710938 6.390624-6.941407l7-6.335937c4.726563-4.148437 9.160157-8.652344 14.109376-12.523437 9.835937-7.851563 20.179687-15.039063 30.96875-21.523438 43.269531-26.25 92.882812-40.203125 143.496093-40.386719zm0 0');

        let path6 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path6.setAttribute('d', 'm23.878906 211.4375c-.695312 2.835938-.34375 5.832031 1.011719 8.425781l1.214844 1.695313c.46875.554687 1.015625 1.042968 1.609375 1.449218.503906.460938 1.082031.828126 1.695312 1.117188.65625.304688 1.324219.546875 2.007813.761719 2.71875.667969 5.589843.328125 8.074219-.980469 1.164062-.695312 2.1875-1.609375 3.007812-2.691406 2.398438-3.316406 2.859375-7.65625 1.21875-11.410156-.652344-1.214844-1.523438-2.296876-2.574219-3.195313-1.03125-.925781-2.238281-1.640625-3.539062-2.117187-2.710938-.898438-5.664063-.71875-8.242188.5-2.636719 1.320312-4.609375 3.644531-5.484375 6.445312zm0 0');

        let path7 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path7.setAttribute('d', 'm512.261719 565.160156c-12.695313 0-25.417969-4.476562-35.5625-13.578125-21.804688-19.527343-23.734375-53.238281-4.277344-75.144531 35.839844-39.988281 56.96875-91.511719 59.507813-145.109375.277343-5.878906 5.324218-10.210937 11.167968-10.148437 5.878906.28125 10.433594 5.285156 10.144532 11.171874-2.773438 58.472657-25.824219 114.6875-64.894532 158.28125-11.648437 13.113282-10.484375 33.347657 2.589844 45.070313 13.097656 11.734375 33.320312 10.671875 45.109375-2.382813 52.515625-58.757812 81.933594-134.578124 82.796875-213.4375.617188-46.21875-17.097656-143.402343-87.402344-214.667968-54.925781-55.660156-129.589844-83.882813-221.921875-83.882813-5.890625 0-10.664062-4.777343-10.664062-10.664062s4.773437-10.667969 10.664062-10.667969c98.25 0 178.027344 30.355469 237.109375 90.230469 70.371094 71.316406 94.378906 168.417969 93.558594 229.90625-.929688 84.003906-32.265625 164.789062-88.246094 227.445312-10.539062 11.660157-25.089844 17.570313-39.679687 17.578125zm0 0');

        let path8 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path8.setAttribute('d', 'm148.453125 547.945312h-.105469c-2.125 0-4.207031-.640624-5.964844-1.820312-82.453124-55.535156-131.625-148.042969-131.542968-247.46875 0-5.886719 4.78125-10.65625 10.671875-10.65625h.007812c5.890625 0 10.664063 4.789062 10.65625 10.675781-.085937 92.160157 45.429688 177.929688 121.761719 229.519531 3.0625 1.882813 5.117188 5.246094 5.117188 9.09375.007812 5.890626-4.710938 10.65625-10.601563 10.65625zm0 0');
     
		let path9 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path9.setAttribute('d', 'm374.253906 589.976562c-4.863281 0-9.257812-3.339843-10.386718-8.292968-1.3125-5.738282 2.273437-11.453125 8.019531-12.769532 28.789062-6.589843 56.191406-17.710937 81.421875-33.0625 5.382812-3.351562 10.867187-6.964843 16.136718-10.691406 4.789063-3.402344 11.464844-2.261718 14.867188 2.550782s2.261719 11.46875-2.546875 14.871093c-5.601563 3.964844-11.4375 7.808594-17.273437 11.433594-27.285157 16.601563-56.796876 28.585937-87.835938 35.691406-.808594.179688-1.609375.269531-2.402344.269531zm0 0');

        let path10 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path10.setAttribute('d', 'm115.449219 147.679688c5.28125-10.570313 13.546875-19.339844 23.785156-25.226563 10.308594-6.199219 21.964844-9.773437 33.976563-10.421875 11.828124-.578125 23.636718 1.3125 34.695312 5.527344 5.347656 2.070312 10.507812 4.566406 15.449219 7.464844 4.703125 3.117187 9.195312 6.53125 13.449219 10.21875 1.375 1.195312 1.78125 3.179687.96875 4.8125-.808594 1.640624-2.621094 2.515624-4.414063 2.152343l-.480469-.105469c-5.332031-1.109374-10.25-2.730468-15.167968-4.128906-5.035157-.945312-9.832032-2.503906-14.695313-3.25-9.421875-1.773437-19-2.488281-28.582031-2.125-9.402344.691406-18.683594 2.59375-27.609375 5.644532-9.5 3.316406-18.628907 7.582031-27.246094 12.742187l-.328125.191406c-1.027344.601563-2.316406.460938-3.195312-.339843-.871094-.796876-1.105469-2.089844-.605469-3.15625zm0 0');      

        let path11 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path11.setAttribute('d', 'm414.117188 150.984375c-8.628907-5.160156-17.757813-9.425781-27.253907-12.742187-8.925781-3.050782-18.210937-4.953126-27.613281-5.644532-9.582031-.363281-19.160156.339844-28.578125 2.125-4.863281.730469-9.664063 2.289063-14.699219 3.25-4.917968 1.40625-9.832031 3.019532-15.164062 4.128906l-.484375.09375c-1.792969.375-3.605469-.511718-4.414063-2.140624-.8125-1.640626-.414062-3.617188.972656-4.8125 4.261719-3.6875 8.753907-7.101563 13.445313-10.21875 4.941406-2.898438 10.101563-5.394532 15.449219-7.464844 11.058594-4.214844 22.875-6.105469 34.699218-5.527344 11.996094.648438 23.664063 4.222656 33.972657 10.421875 10.238281 5.886719 18.507812 14.664063 23.785156 25.226563.511719 1.066406.269531 2.34375-.605469 3.144531-.863281.800781-2.160156.953125-3.179687.367187zm0 0');

        let path12 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path12.setAttribute('d', 'm128.183594 320c-2.726563 0-5.457032-1.046875-7.539063-3.125-4.167969-4.171875-4.167969-10.914062 0-15.082031l106.667969-106.667969c4.167969-4.167969 10.910156-4.167969 15.082031 0 4.167969 4.171875 4.167969 10.910156 0 15.085938l-106.664062 106.664062c-2.082031 2.078125-4.816407 3.125-7.546875 3.125zm0 0');

        let path13 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path13.setAttribute('d', 'm234.855469 320c-2.734375 0-5.464844-1.046875-7.542969-3.125l-106.667969-106.664062c-4.167969-4.175782-4.167969-10.914063 0-15.085938 4.171875-4.167969 10.914063-4.167969 15.085938 0l106.664062 106.667969c4.167969 4.167969 4.167969 10.910156 0 15.082031-2.082031 2.078125-4.808593 3.125-7.539062 3.125zm0 0');

        let path14 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path14.setAttribute('d', 'm298.855469 320c-2.730469 0-5.464844-1.046875-7.546875-3.125-4.167969-4.171875-4.167969-10.914062 0-15.082031l106.671875-106.667969c4.167969-4.167969 10.90625-4.167969 15.082031 0 4.171875 4.171875 4.171875 10.910156 0 15.085938l-106.667969 106.664062c-2.082031 2.078125-4.8125 3.125-7.539062 3.125zm0 0');

        let path15 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path15.setAttribute('d', 'm405.519531 320c-2.730469 0-5.460937-1.046875-7.539062-3.125l-106.671875-106.664062c-4.167969-4.175782-4.167969-10.914063 0-15.085938 4.171875-4.167969 10.914062-4.167969 15.085937 0l106.667969 106.667969c4.171875 4.167969 4.171875 10.910156 0 15.082031-2.09375 2.078125-4.824219 3.125-7.542969 3.125zm0 0');

        let path16 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path16.setAttribute('d', 'm373.523438 426.664062c-5.90625 0-10.671876-4.765624-10.671876-10.664062v-10.667969c0-5.886719-4.777343-10.664062-10.667968-10.664062h-149.332032c-5.886718 0-10.667968-4.769531-10.667968-10.667969s4.78125-10.664062 10.667968-10.664062h149.332032c17.648437 0 32.003906 14.355468 32.003906 31.996093v10.667969c0 5.898438-4.78125 10.664062-10.664062 10.664062zm0 0');

        let path17 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path17.setAttribute('d', 'm256.1875 437.335938h-10.667969c-5.890625 0-10.664062-4.773438-10.664062-10.671876 0-5.898437 4.773437-10.664062 10.664062-10.664062h10.667969c5.890625 0 10.664062 4.765625 10.664062 10.664062 0 5.898438-4.773437 10.671876-10.664062 10.671876zm0 0');        
        
        	g.appendChild(path1);
        	g.appendChild(path2);
            g.appendChild(path3);
            g.appendChild(path4);
            g.appendChild(path5);
            g.appendChild(path6);
            g.appendChild(path7);
            g.appendChild(path8);
            g.appendChild(path9);
            g.appendChild(path10);
            g.appendChild(path11);
            g.appendChild(path12);
            g.appendChild(path13);
            g.appendChild(path14);
            g.appendChild(path15);
            g.appendChild(path16);
            g.appendChild(path17);
            
        	svg.appendChild(g);
            
            let errorMessage = document.createElement('p');
            errorMessage.innerHTML = 'Server Error. Please try refreshing page.'
            errorMessage.style.color = "#428dff";
            errorMessage.style.textAlign = "center";
            errorMessage.style.fontSize = "1.5rem";
        
        let errorOutlet = document.getElementById('error-outlet');
        
        if (errorOutlet) {
            errorOutlet.appendChild(svg);
            errorOutlet.appendChild(errorMessage);
        }
		}, 50);
    },

    addCandidateCall:function(cmp,event,helper){
        const userevent = $A.get("e.c:E_TalentActivityAddTask");
        const recordID = cmp.get("v.sidebarId");
        userevent.setParams({"recordId":recordID, activityType:"call" })
        userevent.fire();
    },

    reloadActivities: function(component, event, helper){
        component.set("v.contactId", component.get("v.selectedRecord.record.contact_id"));
        component.set("v.loading",true);

        if(component.get("v.contactId")) {
            helper.getResults(component, helper);
        }
    }

})