({

	getLastResumeDate: function(component) {
	  var recId =  component.get('v.sidebarId');// component.get("v.initialRecord.record.candidate_id"); //component.get("v.records.candidate_id");
	  var bdata = component.find("basedatahelper");
	  var soqlString = `SELECT CreatedDate FROM Talent_Document__c WHERE mark_for_deletion__c = false and (Document_Type__c like 'Resume%' OR Document_Type__c like 'communities%') and committed_document__c = true and Talent__c = '${recId}' ORDER BY CreatedDate DESC`;

	  var params = {
		soql: soqlString,
		maxRows: '1'
	  };

	  bdata.callServer(component, '', '','c.getRows', function(response) {
		if(response.length > 0) {		  
		  component.set('v.resumeLastUpdated', response[0].CreatedDate);

		} else {
		  component.set('v.resumeLastUpdated', '');
		}
	  }, params, false );

	  },

	sendToURL : function(url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
    },
	
    getResults : function(component){
        let recordID = component.get("v.contactId");

        if(recordID !== '' && recordID){
            const viewListFunction = 'ATS.TalentActivityFunctions.getTalentPastActivities';

            this.getViewFromApexFunction(component, recordID, 'All', viewListFunction);

        }

    },
	
	getOrderRecords : function(component){
        var recordID = component.get("v.contactId"); 
        //alert(recordID);
        var Validate = false;
        component.set("v.OrderRecVal" , Validate);
        var params = {
            OrderContactRecordID: recordID,
            obj:''
	  	};
		var bdata = component.find("basedatahelper");        
        if(recordID !== ''){
            bdata.callServer(component, '', '','c.getOrderRecords', function(response) {
                //alert(response);
                if(response !=null){
                    var orderStatus = response.Status;
                    if(orderStatus.substr(0,14) == 'Not Proceeding') {	
                        Validate = true;
                        component.set("v.NotProceeding",true);
                        component.set("v.OrderRecVal" , Validate);
                        component.set("v.NorProceedinReason", response.Submittal_Not_Proceeding_Reason__c);	
						component.set("v.Comment", response.Comments__c);
                        component.set("v.OrderLastModifiedDate", response.Display_LastModifiedDate__c);						
                    } else {
                        component.set("v.OrderRecVal" , Validate);
                        component.set("v.NorProceedinReason", '');
                        component.set("v.Comment", response.Comments__c);
                        component.set("v.OrderLastModifiedDate", response.Display_LastModifiedDate__c);
                    }
                }else {
                    component.set("v.OrderRecVal" , Validate);
                    component.set("v.Comment", '');
                    component.set("v.OrderLastModifiedDate", '');
                }
                
			}, params, false );
        }

    },

    getResultsAGHistory: function(component) {
        // const recordId =  component.get("v.contactId");
        const recordId = component.get('v.sidebarId');

        if(recordId !== '' && recordId){
            const viewListFunction = 'ATS.TalentEmploymentFunctions.getWorkHistory';

            this.getViewFromApexFunction(component, recordId, 'AGHistory', viewListFunction);
        }


    },

    getViewFromApexFunction : function(component, recordID, whichList, viewListFunction){

        let arr = viewListFunction.split('.');
        let className = arr[0];
        let subClassName = arr[1];
        let methodName = arr[2];

            if(methodName !== '' && recordID !== '') {

                const type = component.get("v.type");
                let actionParams = {};

                switch (whichList) {

                    case 'AGHistory':
                        actionParams = {
                            "recordId": recordID,
                            "maxRows" : '20',
                            "personaIndicator" : "Recruiter"
                        };
                    break;


                    default:
                        actionParams = {
                            "recordId": recordID,
                            "maxItems" : '25',
                            "filterCriteria" : '',
                            "getActivityIcons": (type.includes('R2C') || type.includes('C2C') || type.includes('C'))
                        };
                    break;

                }

            const bdata = component.find("basedatahelper");


            let records = [];
            component.set("v.loading", true);
            bdata.callServer(component, className, subClassName, methodName,function(response) {

                if (response) {

                    if(whichList === 'AGHistory'){

                        let historyArr = response.AGWHList;

                        if(historyArr.length > 0 ){
                            historyArr.map(items => records.push(items));
                        }

                    } else {

                        if(response.length > 0){
                            response.map(items => records.push(items));
                        }

                    }

                    if(records.length > 0) {
                        switch (whichList) {

                            case 'AGHistory':
                                component.set("v.hasHistory", true);
                                component.set("v.historyRecords", records);

                                break;

                            default:
                                component.set ("v.hasPastActivity", true);
                                component.set("v.activityRecords", records);
                                let lastG2Comments = [];
                                records.map(record => {
                                    if (record.ActivityType == "G2" && record.Complete) {
                                        lastG2Comments.push(record);
                                    }
                                });
                                lastG2Comments.sort((a, b) => (new Date(a.LastModifiedDate).getTime() < new Date(b.LastModifiedDate).getTime()) ? 1 : -1);
                                component.set("v.lastG2Comment", lastG2Comments[0]);
                            break;

                        }

                    }
                }

                component.set("v.loading", false);
            }, actionParams, false );
        }
    },

    trimCommentText: function(txt) {
        if(parseInt(txt.length) > 255) {
            return txt.substring(0, 255) + '...';
        } else {
            return '';
        }
    },


    clearTabData: function(component){

        //Added by Kaavya K: S-85063 Clear contact record
            component.set("v.contactRecord",null);

        //Clear Activities
            component.set("v.activityRecords",[]);
            component.set("v.hasPastActivity", false);


    }, 

    /*checkSMS360Permission:function(component) { 

  
      var bdata = component.find("basedatahelper");

      var params = { permissionName: 'SMS' };

      bdata.callServer(component, '', '','c.userHasAccessPermission', function(response) {

        if(response) 
            {
                // console.log('user has access SMS 360 App' + response);
                component.set('v.userHasSMS360Access',true);
                
            }
            else{
                console.log('something unexpected happened---');
            }

      }, params, false );
 
    },*/
    openResumeModal: function(component, event) {
        // let modalEvt = $A.get("e.c:E_SearchResumeModalPagination"),
        // console.log("Modal Fired!");
        let modalEvt = component.getEvent("openResumeModal"),
            record = component.get("v.records") || {},
            resumeId = record.candidate_id,
            resumePairs = component.get("v.resumePairs") || [],
            resumeModalNav = component.get("v.resumeIdArr") || [];

        let resumeIndex = resumePairs.findIndex(item => item.id === resumeId),
            currentIndex = resumeModalNav.findIndex(item => item === resumeId);


        if(currentIndex !== -1 && resumeIndex !== -1){
            modalEvt.setParams({
                "resumeId" : resumeId,
                "resumeArr" : resumePairs[resumeIndex],
                "currentIndex" : currentIndex,
                "resumeCount" : resumeModalNav.length
            });
            modalEvt.fire();
        }
    }
})