({
    getHighlightedResume : function(component, event, helper) {
        helper.getHighlightedResume(component);
    },

    emailLink : function(component, event, helper) {
    	helper.sendToURL("mailto:" + component.get('v.record.communication_email'));
    },

    otherEmailLink : function(component, event, helper) {
    	helper.sendToURL("mailto:" + component.get('v.record.communication_other_email'));
    },

    //D-05950 - Added by Karthik
    defPhoneLink : function(component, event, helper) {
        var defPhone = component.get('v.record.communication_preferred_phone');
        defPhone = defPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + defPhone);
    },

    homePhoneLink : function(component, event, helper) {
        var homePhone = component.get('v.record.communication_home_phone');
        homePhone = homePhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + homePhone);
        //helper.sendToURL("tel:" + component.get('v.record.communication_home_phone'));
    },

    mobilePhoneLink : function(component, event, helper) {
        var mobilePhone = component.get('v.record.communication_mobile_phone');
        mobilePhone = mobilePhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + mobilePhone);
        //helper.sendToURL("tel:" + component.get('v.record.communication_mobile_phone'));
    },

    otherPhoneLink : function(component, event, helper) {
        var otherPhone = component.get('v.record.communication_other_phone');
        otherPhone = otherPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + otherPhone);
        //helper.sendToURL("tel:" + component.get('v.record.communication_other_phone'));
    },
    //D-05950 -End

    otherPhoneLink : function(component, event, helper) {
        var workPhone = component.get('v.record.communication_work_phone');
        workPhone = workPhone.replace(/\s/g,'');
        helper.sendToURL("tel:" + workPhone);
        //helper.sendToURL("tel:" + component.get('v.record.communication_other_phone'));
    },

    doInit:function(component,event,helper){
        if (component.get('v.itemIndex') === 0) {
        var trackingEvent = $A.get("e.c:E_SearchResultsHandler");
		trackingEvent.setParam('clickTarget', Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1));
		trackingEvent.fire();
        }
        //console.log("----record---- " + JSON.stringify( component.get("v.record" ) ) );
        var teaser = component.get("v.resumeTeaser");
        var resumeId = component.get("v.record.candidate_id") + "-resumelink";
        var recordId = component.get("v.recordId");
        //console.log(component.get("v.record"));
        if(teaser){
            var btn = component.find("viewResume");
            var regex = /<em(.*?)\/em>/g;

            teaser = teaser.replace(regex, "<a class='"+ resumeId + "' target='_blank'>$&</a>");
            component.set("v.resumeTeaser",teaser);
        }
        component.set("v.rowId", recordId);
		component.set("v.expanded", component.get("v.expandAllState"));

        if( ( component.get("v.record.communication_mobile_phone")  != null && component.get("v.record.preferred_phone_type") != 'M' ) ||
            ( component.get("v.record.communication_home_phone")    != null && component.get("v.record.preferred_phone_type") != 'H' ) ||
            ( component.get("v.record.communication_work_phone")    != null && component.get("v.record.preferred_phone_type") != 'W' ) ||
            ( component.get("v.record.communication_other_phone")   != null && component.get("v.record.preferred_phone_type") != 'O' ) ){

            component.set("v.showDropdownMenu", true);

        }
		/*console.log('Search Type:'+component.get("v.type"));
		helper.resetCheckboxes(component, event);*/
    },  
    setLinked : function(component, event, helper) {
        if((event.getParam("talentId") == component.get("v.recordId")) && (event.getParam("opportunityId") == component.get("v.opportunityId"))){
          component.set("v.linked",true);  
        }
    },
    viewResume : function(component, event, helper) {
        helper.viewResume(component, event);
    },

    handleMatchInsights : function(component, event, helper) {
        helper.fireMatchInsightsRequestEvt(component, component.get("v.record.given_name") + " " + component.get("v.record.family_name"));
    },
    
    highlightRow : function(component, event, helper) {
        var toggleSearchView = component.get("v.toggleSearchView"),
            chkCmp;
        
        document.querySelectorAll('[data-tracker="select_row--' + component.get('v.recordId') + '"]')[0].querySelector('input').click();
            
        if(toggleSearchView === 'Grid'){
            chkCmp = component.find("chkAddToCallSheetGrid");
        } else {
            chkCmp = component.find("chkAddToCallSheetCard");
        }

        helper.sendIdsToMassActionCmp(component, chkCmp);

    },
    selectRow: function(component,event,helper){
		//Rajeesh S-84244 Ability to link from talent search: reusing the checkbox for linking as call list is not a functionality available from RQ search modal
		helper.selectRow(component,event);
	},
    handleSelectAll : function(component, event, helper) {
        var toggleSearchView = component.get("v.toggleSearchView"),
            chkCmp;

        if(toggleSearchView === 'Grid'){
            component.find("chkAddToCallSheetGrid").set("v.value", component.get("v.selectAll"));
            chkCmp = component.find("chkAddToCallSheetGrid");
        } else {
            component.find("chkAddToCallSheetCard").set("v.value", component.get("v.selectAll"));
            chkCmp = component.find("chkAddToCallSheetCard");
        }
        
        helper.sendIdsToMassActionCmp(component, chkCmp);
    },

    toggleSidebar : function(component, event, helper) {
        var recordId = component.get("v.recordId"),
            rowId = component.get("v.rowId");
            
            component.set("v.rowId", recordId);
        // component.set("v.showSidebar", true);
            helper.sendDataToSidebar(component);
    },

    expandCollapse : function(component, event, helper){
        var expanded = component.get("v.expanded"),
            rowId = component.get("v.rowId"),
            recordId = component.get("v.recordId");

        if(expanded && rowId == recordId){

            component.set("v.expanded", false);
            
        }else{
            component.set("v.expanded", true);
            
        }
    },
	
    handleExpandAll : function(component, event, helper) {
        component.set("v.expanded", component.get("v.expandAllState"));
    }/*,
	handleDisableCheckboxes: function(component, event, helper) {
		var contacts = event.getParam("contacts");
		if(contacts.length >0) {
			component.set("v.checkBoxStatus", false);
			component.set("v.selectedActionContacts",contacts);
			helper.resetCheckboxes(component, event);
		}

	}*/
})