({
    getHighlightedResume : function(component) {
		var q = component.get("v.record.searchKeyword");
		if(q && q !== "undefined"){
			var queryStringUrl = this.createResumeHighlightQueryString(component); 
            console.log(queryStringUrl);
			//var checkUndfn = queryStringUrl.slice(2, 11); //To check if teaser has search string
			//if(checkUndfn && checkUndfn !== "undefined"){
				var action = component.get("c.search"); 
				action.setParams({"serviceName": "Resume Highlight", 
                              "queryString": queryStringUrl});
				action.setCallback(this, function(response) {
                var errList = [];
                if (response.getState() === "SUCCESS") {
                    var searchTalentsResponse = response.getReturnValue();
                    var searchResponse = JSON.parse(searchTalentsResponse);
                    
                    //console.log(JSON.stringify(component.get("v.record")));
                    //console.log(searchResponse);
                    if (searchResponse.response.hits.hits["0"].highlight) {
                        var htmlResume = searchResponse.response.hits.hits["0"].highlight[component.get("v.record.highlightedResume") + ".html"]["0"];

                        var familyName = component.get("v.record.family_name") !== null ? component.get("v.record.family_name") : "";
                        var givenName = component.get("v.record.given_name") !== null ? component.get("v.record.given_name") : ""; 
                        var fullName = (familyName  + ', ' + givenName).replace(/['"]+/g, '\\\'');
                        let updatedDate = component.get("v.record.resume_1.modified");

                        var modalEvt= $A.get("e.c:E_SearchResumeModal");
                        modalEvt.setParams({
                            "htmlsource" : htmlResume,
                            "resumeName" : fullName,
                            "resumeUpdatedDate": updatedDate,
                            "destinationId": "SearchContainerCandidate"
                        });
                        modalEvt.fire(); 
    
                        /*var x = window.open('', '_blank');
						
                        if (x != null){
                            x.document.body.innerHTML = htmlResume;
                            x.document.title = fullName;
                        } 
                        else {
                            var toastEvt = $A.get("e.force:showToast");

                            toastEvt.setParams({
                                title: "Opps, something went wrong!",
                                message: "In order to view resume, please allow pop-ups from this page.",
                                type: "error"
                            });

                            toastEvt.fire();
                        }*/
                       
                    } else {
                        this.viewResume(component);
                    }
			
					if (searchResponse.fault) {
                        errList.push(searchResponse.fault.faultstring);
                        searchCompletedEvt.setParams({"hasError" : true,
                                                  "errors" : errList});
                        console.log("Error!");
                    } else if (searchResponse.header.errors && searchResponse.header.errors.length > 0) {
                        var errors = searchResponse.header.errors;
                        for (var i=0; i<errors.length; i++) {
                            if (errors[i] && errors[i].message) {
                                errList.push(errors[i].message);
                            }
                        }
                        searchCompletedEvt.setParams({"hasError" : true,
                                                  "errors" : errList});
                        console.log("Error!");
                    } 
                } else {
                    var errors = response.getError();
                    if (errors) {
                        for (var i=0; i<errors.length; i++) {
                            if (errors[i] && errors[i].message) {
                                errList.push(errors[i].message);
                            }
                        }
                    }
                }
                component.set("v.errors", errList);
				});
				$A.enqueueAction(action); 
			}
			else{
				this.viewResume(component);
			}
		},

    createResumeHighlightQueryString : function (component) {
        var parameters = {
                          q: component.get("v.record.searchKeyword"), 
                          id: component.get("v.record.candidate_id"), 
                          "highlight.fields": component.get("v.record.highlightedResume") + ".html",
						  "app_id": "ATS_Informational"
                         };
        return(this.buildUrl(parameters));
    },

    buildUrl : function (parameters) {
         var qs = "";
         for(var key in parameters) {
            var value = parameters[key];
            qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
         }
         if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
         }
         return qs;
    },
    
    viewResume : function(component, event) {
        var action = component.get("c.getAttachmentId");
       

        action.setParams({"candidateId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                
                var attachmentId = response.getReturnValue();
                var familyName = component.get("v.record.family_name") !== null ? component.get("v.record.family_name") : "";
                var givenName = component.get("v.record.given_name") !== null ? component.get("v.record.given_name") : ""; 
                var fullName = (familyName  + ', ' + givenName).replace(/['"]+/g, '\\\'');

                if (attachmentId != null && attachmentId != undefined) {
                    //window.open("/servlet/servlet.FileDownload?file=" + attachmentId, '_blank');
                    var modalEvt= $A.get("e.c:E_SearchResumeModal");
                    modalEvt.setParams({
                        "attachmentId" : attachmentId,
                        "resumeName" : fullName,
                        "destinationId": "SearchContainerCandidate"
                    });
                    modalEvt.fire();
                } 
            } 

        });
        $A.enqueueAction(action);
    },

    sendToURL : function(url) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
	},

    sendIdsToMassActionCmp : function(component, chkCmp) {
        var candidateId = component.get("v.recordId");
		var contactId = component.get("v.record.contact_id");		
        //Get Event
        var appEvent = $A.get("e.c:E_SearchResultsAction");

        var chkCmp;
        if (chkCmp !== null && chkCmp !== undefined) {
            if (chkCmp.get("v.value")) {
                appEvent.setParams({ "idToAdd" : candidateId });
                appEvent.setParams({ "idToRemove" : "" });
				appEvent.setParams({ "contactIdToAdd" : contactId });
                appEvent.setParams({ "contactIdToRemove" : "" });
            } else {
                appEvent.setParams({ "idToAdd" : "" });
                appEvent.setParams({ "idToRemove" : candidateId });
				appEvent.setParams({ "contactIdToAdd" : "" });
                appEvent.setParams({ "contactIdToRemove" : contactId });
            }  
            //Fire Event
            appEvent.fire();
        }
    },

    sendDataToSidebar : function( component) {
        var record = component.get("v.record"),
            sidebarId = component.get("v.recordId"),
            //showSidebar = component.get("v.showSidebar"),
            contactId = record.contact_id,
            evt = component.getEvent("displayResultsSidebar");

        evt.setParams({
            "record" : record,
            "recordId" : sidebarId,
            // "toggleSidebar" : showSidebar,
            "contactId" : contactId
        });

        evt.fire();
    },

	selectRow: function(component,event){
		//Rajeesh selecting rows for linking.
		//send the row that is selected to linkreqtotalentmodal....

		var rowId = event.getSource().get("v.text");
        var isChecked= event.getSource().get("v.value");
		var talentSelectionEvt = $A.get("e.c:E_LinkReqToTalentSelectionUpdated");
        talentSelectionEvt.setParams({ "recordId":  rowId,"value":isChecked});
        talentSelectionEvt.fire();
	},
	//S-90955 - Match Insights
	fireMatchInsightsRequestEvt : function(component, recordName) {
        var matchInsightsReqEvt = component.getEvent("matchInsightsRequestEvt");
		matchInsightsReqEvt.setParams({
            "recordId" : component.get("v.recordId"),
			"type" : component.get("v.type"),
            "srcId" : component.get("v.automatchSourceId"),
			"recordName" : recordName
        });
        matchInsightsReqEvt.fire();
	}/*,
	resetCheckboxes : function(component, event) {
		var selectedContacts = component.get("v.selectedActionContacts");
		var checkboxStatus = component.get("v.checkBoxStatus");
		var record = component.get('v.record');
		if(selectedContacts.length > 0) {
			for(var i=0;i<selectedContacts.length;i++){
				if(record.contact_id  === selectedContacts[i].Id) {
					component.set("v.checkedContact",checkboxStatus);
				}
			}
		}
	}*/
    
})