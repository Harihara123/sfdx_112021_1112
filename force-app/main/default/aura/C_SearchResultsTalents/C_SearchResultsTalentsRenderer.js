({
    afterRender : function(component, helper) {
        this.superAfterRender();
        
        var lnkId = component.get("v.record.candidate_id") + "-teaserlink";
        var resumeCls = component.get("v.record.candidate_id") + "-resumelink";

        var elems = document.getElementsByClassName(resumeCls);
		for (var i=0; i<elems.length; i++) {
            elems[i].addEventListener("click", function() {
                document.getElementById(lnkId).click();
                console.log("Clicked!");
            });
        }

    }
})