({
	doInit : function(component, event, helper) {
		helper.getPreferenceMetadata(component);
	},

	toggleSettings : function(component, event, helper) {
		component.set("v.isOpen", !component.get("v.isOpen"));
	},

	saveAndApplyPrefs : function(component, event, helper) {
		helper.updateUserPreferences(component);
		helper.savePreferences(component);
		helper.applyPreferences(component);
		component.set("v.isOpen", false);
	},

	handlePrefsUpdate : function(component, event, helper) {
		if (!component.get("v.updateFlg")) {
			component.set("v.updateFlg", true);
			helper.evalEnables(component);
			component.set("v.updateFlg", false);
		}
	},

	handleChange : function(component, event, helper) {
		helper.updateSwitchSelections(component, event);
		helper.updateUserPreferences(component);
		helper.evalEnables(component);
	},

	hidePopover : function(component, event, helper){
		var isOpen = component.get("v.isOpen");
		component.set("v.isOpen", !isOpen);
	}
})