({
	getPreferenceMetadata : function(component) {
		var action = component.get("c.getPreferenceOptions");
		action.setParams({
			"targets" : component.get("v.targets").split(",")
		});
		action.setStorable();
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
                var meta = response.getReturnValue();
				this.handleMetadata(component, meta);
            }
		});

		$A.enqueueAction(action);
	},

	handleMetadata : function(component, meta) {
		var prefs = component.get("v.prefs");
		for (var i=0; i<meta.length; i++) {
			meta[i].options = JSON.parse(meta[i].Options__c);
			// For "Switch" type, typecast Default__c the value to boolean.
			if (meta[i].Type__c === "Switch") {
				meta[i].Default__c = (meta[i].Default__c === "true");
			}
			meta[i].selection = meta[i].Default__c;
			meta[i].enabled = this.checkEnableCondition(meta[i].Enable_Condition__c, prefs);
		}

		component.set("v.prefsMeta", meta);
	},

	evalEnables : function(component) {
		var meta = component.get("v.prefsMeta");
		var p = component.get("v.prefs");

		for (var i=0; i<meta.length; i++) {
			meta[i].enabled = this.checkEnableCondition(meta[i].Enable_Condition__c, p);
		}

		component.set("v.prefsMeta", meta);
	},

	checkEnableCondition : function(ec, prefs) {
		if (!prefs) {
			return false;
		}
		if (ec && ec !== "") {
			var e = JSON.parse(ec);
			return (prefs[e.setting] === e.value);
		} else {
			return true;
		}
	},

	savePreferences : function(component) {
		var action = component.get("c.saveUserPreferences");
		action.setParams({
			"prefs" : JSON.stringify(component.get("v.prefs"))
		});
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
                this.showToast("success", "Saved", "Preferences successfully set.");
            } else {
				console.log("Error saving preferences");
				this.showToast("error", "Error", "Preferences could not be saved!");
			}
		});

		$A.enqueueAction(action);
	},

	updateSwitchSelections : function(component, event) {
		if (event.srcElement && event.srcElement.name === "pref-switch") {
			// For "switch types, set the value from the checkbox "checked" setting.
			var p = component.get("v.prefsMeta");
			for (var i=0; i<p.length; i++) {
				if (event.srcElement.id.indexOf(p[i].Setting_Name__c) >= 0) {
					p[i].selection = event.srcElement.checked;
				}
			}
		}
	},

	updateUserPreferences : function(component) {
		var meta = component.get("v.prefsMeta");
		var prefs = component.get("v.prefs");

		for (var i=0; i<meta.length; i++) {
			prefs[meta[i].Setting_Name__c] = meta[i].selection;
		}

		component.set("v.prefs", prefs);
	},

	applyPreferences : function(component) {
		var applyEvt = component.getEvent("prefsApplyEvt");
		applyEvt.setParams({
			"preferences" : component.get("v.prefs")
		});
		applyEvt.fire();
	},

	showToast : function(type, title, message) {
		var evt = $A.get("e.force:showToast");
		evt.setParams({
			"type" : type,
			"title" : title,
			"message" : message
		});
		evt.fire();
	}
})