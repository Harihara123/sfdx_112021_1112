({
	getActionWithParameters: function (helperCmp, criteriaCmp, queryStringUrl) {
		var action = criteriaCmp.get(helperCmp.get("v.actionName"));

		action.setParams({
			"serviceName": helperCmp.get("v.serviceName"), 
			"queryString": queryStringUrl
		});

		return action;
	},
	
	getBaseParameters: function (criteriaCmp) {
		// var keyword = criteriaCmp.find("inputSearchId").get("v.value");
		let keyword = criteriaCmp.get("v.keyword");
		if (keyword === undefined || keyword === null || keyword === "") {
			// Assumes keywordless search is allowed for the applied facets. Checked during keyword validation.
			keyword = "-randomstring";
		}
		return {
			"id": criteriaCmp.get("v.runningUser.id"), 
			"q": keyword, 
			"size": criteriaCmp.get("v.pageSize"), 
			"from": criteriaCmp.get("v.offset"), 
			"flt.group_filter": criteriaCmp.get("v.runningUser.opcoSearchFilter")
		};
	},

	createSearchLogEntry: function (helperCmp, criteriaCmp, baseEntry) {
        // baseEntry.criteria.keyword = criteriaCmp.find("inputSearchId").get("v.value");
        baseEntry.criteria.keyword = criteriaCmp.get("v.keyword");
        baseEntry.criteria.multilocation = criteriaCmp.get("v.multilocation");
        baseEntry.criteria.facets = criteriaCmp.get("v.facets");
        baseEntry.criteria.filterCriteriaParams = criteriaCmp.get("v.filterCriteriaParams");
		baseEntry.fdp = criteriaCmp.get("v.facetDisplayProps"); 
        
        return JSON.stringify(baseEntry);
	},

	restoreCriteriaFromLog: function (helperCmp, criteriaCmp) {
		var logEntry = criteriaCmp.get("v.lastSearchLog");
		if (logEntry !== undefined && logEntry !== null) {
            var searchParams = JSON.parse(logEntry);

            if (searchParams) {
            	
				// criteriaCmp.find(inputSearchId).set("v.value", searchParams.criteria.keyword);
				criteriaCmp.set("v.keyword", searchParams.criteria.keyword);
                criteriaCmp.set("v.multilocation", searchParams.criteria.multilocation);
                /*component.set("v.location", searchParams.criteria.location.text);
                component.set("v.latlong", searchParams.criteria.location.geo);
                if (searchParams.criteria.location.geo) {
                    // If geo coordinates were on this restored search, then enable radius.
                    component.set("v.useRadius", true);
                }
                component.set("v.radius", searchParams.criteria.location.rad);
                component.set("v.radiusUnit", searchParams.criteria.location.unit);*/
				criteriaCmp.set("v.facets", searchParams.criteria.facets);
				criteriaCmp.set("v.filterCriteriaParams", searchParams.criteria.filterCriteriaParams);
                criteriaCmp.set("v.offset", searchParams.offset);
                criteriaCmp.set("v.pageSize", searchParams.pageSize);
                if (searchParams.sort) {
					criteriaCmp.set("v.sortField", searchParams.sort);
					criteriaCmp.set("v.sortOrder", searchParams.order);
				}
				criteriaCmp.set("v.facetDisplayProps", searchParams.fdp);

				// type undefined check required for pre-refactoring saved searches.
				criteriaCmp.set("v.type", searchParams.type ? searchParams.type : "C");
			}
		}
	}
})