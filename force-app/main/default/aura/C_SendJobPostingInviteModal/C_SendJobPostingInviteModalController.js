({
    showModal : function(component, event, helper) {
        
        helper.showModal(component, event);
        helper.emailsInit(component, event);
        component.set('v.selectedEmail', component.get('v.contactRecordFields.Preferred_Email_Value__c') );

   //      var jobPostingCmp = component.find('my_posting');
   //  	component.set('v.selectedTab', 'my_posting' );
 		// jobPostingCmp.refreshJobPostings();   
    },
    
    hideModal : function(component, event, helper) {
        helper.hideModal(component);
    },

    refreshTab : function (component, event) {

    	var loadTab = event.getSource().get("v.id");
    	var jobPostingCmp = component.find(loadTab);
    	component.set('v.selectedTab', loadTab );
 		jobPostingCmp.refreshJobPostings();   
    },

    emailChanged : function (component, event) {
    	component.set('v.selectedEmail', event.getSource().get("v.value"));
    },

    sendInvitation : function (component, event) {

    	var jobPostingCmp = component.find( component.get('v.selectedTab') ) ;
        jobPostingCmp.sendInvitation(  component.get('v.selectedEmail'), component.get('v.talentId')  );
    },
    addEmail: function(component, event) {
		let overlayState = component.get('v.overlay');
		if (overlayState) {
			component.set('v.overlay', false);
		} else {
			component.set('v.overlay', true);
			setTimeout(() => component.find('newEmail').focus(), 100)
		  }
    },
    handleChange: function(component, event) {
        component.set('v.selectedEmail', component.find('select').get('v.value'));
    },
	handleEnter: function(component, event, helper) {
		if (event.keyCode == 13) {
			helper.saveNewEmail(component, event);
		}
	},

    handleChangeAddEmail: function(component, event) {
        component.set('v.newEmail', component.find('newEmail').get('v.value'));
    },

    clickedRadio: function(component, event) {
        component.set('v.selectedEmailType', event.currentTarget.value);
		component.set('v.showTypeError', false);

        
    },

    saveNewEmail: function(component, event, helper) {       
		helper.saveNewEmail(component, event);
    }
    
})