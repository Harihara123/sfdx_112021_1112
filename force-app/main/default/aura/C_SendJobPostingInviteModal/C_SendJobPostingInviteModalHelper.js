({
	showModal : function(component, event, helper) {
		
				component.set("v.contactName", component.get("v.record.Name"));
				
				this.initModal(component, event);  
		
	},
    initModal : function (component, event) {
	                this.toggleClass(component,'backdropSendPostingInviteDetail','slds-backdrop--');
            this.toggleClass(component,'modaldialogSendPostingInvite','slds-fade-in-');
        
    },
    hideModal : function (component) {
		
			
			this.toggleClassInverse(component,'backdropSendPostingInviteDetail','slds-backdrop--');
			this.toggleClassInverse(component,'modaldialogSendPostingInvite','slds-fade-in-');

			component.set("v.record",null);
			component.set("v.contactName",null);
			component.set("v.runningUserOPCO",null);
		
    },
    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },
    emailsInit: function(component, event) {
        let fields = component.get('v.contactRecordFields');
        let emailsList = [];
        const emails = [
            ['Email', 'Email'],
            ['Work','Work_Email__c'],
            ['Other', 'Other_Email__c']
        ];
        for (let email of emails) {
            let obj = {};
            obj['value'] = fields[email[1]] ? fields[email[1]] : '';
            obj['type'] = email[1];
            obj['label'] = email[0];
            emailsList.push(obj);
        }
		component.set('v.emailsList', emailsList);
    },
	 saveNewEmail: function(component, event) {
	    let newEmail = component.get('v.newEmail');
        let emailType = component.get('v.selectedEmailType')
        
        if (this.validateEmail(newEmail) && emailType) {
			let input = component.find('newEmail');
			input.setCustomValidity('');
			input.reportValidity();
			component.set('v.showTypeError', false)
            component.set('v.showSpinner', true);
			this.updateEmail(component, event);
         } else {
            if(!this.validateEmail(newEmail)) {
				let input = component.find('newEmail');
				input.setCustomValidity('Please enter a correct email address');
				input.reportValidity();

            } else {
                if(!emailType) {
					let input = component.find('newEmail');
					component.set('v.showTypeError', true)
					input.setCustomValidity('');
					input.reportValidity();
                }
            }
        }
	 },
     updateEmail : function(component, componentId, className) {
        
        component.set('v.contactRecordFields.'+component.get('v.selectedEmailType') , component.get('v.newEmail') )
        
        var action = component.get("c.updateContact");
        action.setParams({"contact" : component.get('v.contactRecordFields') });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var label = '';
                component.set('v.showSpinner', false);
                component.set('v.overlay', false );
                var emailsList = component.get('v.emailsList');

                for( var i = 0; i < emailsList.length; i++){ 
                    if(emailsList[i]['type'] == component.get('v.selectedEmailType') ){
                        label = emailsList[i]['label'];
                        emailsList.splice(i, 1); 
                        break;
                    }
                }				
                emailsList.push({"value":component.get('v.newEmail').trim(),
                                 "type":component.get('v.selectedEmailType'),
                                 "label":label }
                                );                

                component.set('v.emailsList', emailsList );
                component.set('v.selectedEmail', component.get('v.newEmail') );
                
                component.set('v.selectedEmailType', '');
                component.set('v.newEmail', '');

                var appEvent = $A.get("e.c:RefreshTalentHeader");
                appEvent.fire();
            
            } else {                
                console.log("ERROR.... ");
                component.set('v.showSpinner', false);
            }
        })

        $A.enqueueAction(action);

    },
	   validateEmail : function(emailID){
        if(emailID && emailID.trim().length > 0) {
            
            var isValidEmail = emailID.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

            if(isValidEmail != null) {
                return true;
            }
            else {
                return false;
            } 

        }
		
        return false;
    }

   
})