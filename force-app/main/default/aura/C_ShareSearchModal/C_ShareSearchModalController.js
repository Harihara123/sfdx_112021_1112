({
	doInit: function (component, event, helper) {
		component.set("v.isShareable", helper.isShareableSearchEntry(component));
		if(component.get("v.isShareable") === true){
			helper.initModalData(component);
		}
		component.set("v.openModalStyles", true); 
	 },

	checkUserSelection : function(component, event, helper) {
		helper.checkUserSelection(component);
	},

	closeModal: function(component, event, helper) {
		helper.closeModal(component);
	}, 

	shareSearch: function(component, event, helper) {	
		helper.shareSearch(component, event);
	},

	openReplaceSearchModal : function(component, event, helper) {	
		helper.populateSelectedUsers(component);
        if (helper.validateShareSearch(component)) {
			helper.getSavedSearches(component);	
		}
	},

	saveShareReplace : function(component, event, helper) {	
		helper.shareSearch(component, event);
	},

    clearName: function(component, event, helper) {
        helper.clearName(component);
	},

	resetSharedSearch: function(component, event, helper){
		helper.resetSearch(component);
	},

	replaceSavedSearch: function(component, event, helper){
		helper.replaceSavedSearch(component,event);
	},

	cancelReplaceSearch : function(component, event, helper){
		component.set("v.savedSearchList","[]"); 
	},

	validateTitle: function(component, event, helper){        
        
        var titleField = component.find("shareSearchInput");
        var titleText = titleField.get("v.value");
       
      
		 if(titleText != undefined && titleText.length > 0){
	    	titleField.set("v.errors", []);
	        titleField.set("v.isError",false);
	        $A.util.removeClass(titleField,"slds-has-error show-error-message");
	    }
        
 
	}

})