({
    
    getSavedSearches : function (component, event, helper) {
        var action = component.get("c.getUserSearches"); 

        component.set("v.saveSearchTitle", ""); 
        action.setParams({
            'entryType' : ['AllSavedSearches']
        });
        action.setCallback(this, function(response) {
			var srchArray = [];
            if (response.getState() === "SUCCESS") {

                var result = response.getReturnValue();

                if(result !== "" && result !== null){
                    var parsedSearches = JSON.parse(result);
                    for (var i=0; i<parsedSearches.searches.length; i++) {
                        var srch = JSON.parse(parsedSearches.searches[i]);
                        srch.time = $A.localizationService.formatDate(new Date(srch.execTime));
						if (srch.criteria.multilocation) {
                            srch.loc = this.getMultiLocText(srch.criteria.multilocation);
                        }
						if(srch.type =='C2C' || srch.type =='C2R' ||srch.type =='R2C'){
                            srch.sourceid = srch.match.srcid;
                            if(srch.match.data){
                                srch.sourceName = srch.match.data.srcName;
                           	} else {
                                srch.sourceName = '';
                            }
                                      
                        } else {
                            srch.sourceid = '';
                        }
                        srchArray.push(srch);
                    }
                }
            } 

            component.set('v.savedSearchList', srchArray);
			//this.openModal(component);
			if (srchArray.length < $A.get("$Label.c.SEARCH_HISTORY_COUNT")) {
				helper.shareSearch(component, event);
			}
        });
        $A.enqueueAction(action);
    },
    
    getMultiLocText : function(ml) {
        var txt = "";
        
        for (var i=0; i< ml.length; i++) {
            
            if(i == ml.length - 1){
                txt += ml[i].displayText;
            } else {
                txt += ml[i].displayText + " / ";
            }
            
        }
        return txt;
    },

    validateShareSearch: function(component) {
        var isValid = true;
        var searchCountLimit = $A.get("$Label.c.SEARCH_HISTORY_COUNT");
        
        var titleField = component.find("shareSearchInput");
        var title = component.get("v.shareSearchTitle");
        
        var minUsers = component.get("v.selectedUsers").length === 0;
        var maxUsers = component.get("v.selectedUsers").length > 10;
        var errorText = (minUsers || maxUsers) ? "show-error" : "show-message";

		if (minUsers) {
			component.find("userError").set("v.value",$A.get("$Label.c.ATS_SELECT_ONE_USER")); 
			$A.util.addClass(component.find("userError"), errorText); 
            component.set("v.lookupError", true);
			isValid =  false; 
		}  else if(maxUsers){
			component.find("userError").set("v.value",$A.get("$Label.c.ATS_SHARE_MAX_LIMIT_MESSAGE"));
			$A.util.addClass(component.find("userError"), errorText);
			isValid =  false; 
		} else {
			
			$A.util.removeClass(component.find("userError"), errorText);
             component.set("v.lookupError", false);
		}


		// if (!minUsers && maxUsers) {
			
		// } else {
  //           $A.util.removeClass(component.find("userError"),);
  //            component.set("v.lookupError", false);
  //       }

        if (title === undefined || title === null || title === "") {            
            titleField.set("v.errors", [{message: $A.get("$Label.c.ATS_SAVE_TITLE_REQUIRED")}]); 
            titleField.set("v.isError",true);
            $A.util.addClass(titleField,"slds-has-error show-error-message");
            isValid =  false;            
        } else {
            titleField.set("v.errors", []);
            titleField.set("v.isError",false);
            $A.util.removeClass(titleField,"slds-has-error show-error-message");
		} 
        /*if(component.get("v.savedSearchList").length >= searchCountLimit ){
            var index = component.get("v.searchReplaceIndex");
            if (index === undefined || index === null || index < 0) {
                isValid = false;
            }
        }*/

        return isValid;
    },

    replaceSavedSearch : function(component,event){
        var index = event.getSource().get("v.text");
        component.set("v.searchReplaceIndex" , parseInt(index)); 
    },

    shareSearch : function(component, event){
		this.populateSelectedUsers(component);
        if (this.validateShareSearch(component)) {
			let button = event.getSource();
			button.set('v.disabled',true);
			var searchEntry = component.get('v.searchEntry');
            searchEntry.title = component.get("v.shareSearchTitle");
			searchEntry.userName = component.get("v.runningUser.name");
			searchEntry.message = component.get("v.shareMessage");
			searchEntry.key = Math.floor(Math.random() * 1000000000).toString();
			searchEntry.execTime = new Date().getTime();
            component.set('v.searchEntry', searchEntry);
            this.saveSearchLog(component);
        }
    },
    
    saveSearchLog : function (component) {
        // Call server to save search log entry
        var searchCountLimit = $A.get("$Label.c.SEARCH_HISTORY_COUNT");
        
		/* if(component.get("v.savedSearchList").length >= searchCountLimit) {
            var list = component.get("v.savedSearchList");
            list[component.get("v.searchReplaceIndex")] = component.get('v.searchEntry');
            component.set("v.savedSearchList",list);
        } */
		component.set("v.savedSearchList","[]");  // clear the list to close replace search modal
        var action = component.get("c.saveSharedSearches"); 
        action.setParams({
            "searchEntry" : JSON.stringify(component.get('v.searchEntry')),
			"replaceIndex" : component.get("v.searchReplaceIndex"),
            "entryType" : component.get("v.searchType"),
			"userIds" : component.get("v.selectedUserIds"),
			"isReshare" : component.get("v.isReshare")
        });

        action.setCallback(this, function(response) {
			// Reset the replace index to -1
			component.set("v.searchReplaceIndex", -1);
            if (response.getState() === "SUCCESS") {
                // Server SUCCESS - save to attribute.                
				if (response.getReturnValue() === 'SUCCESS') {
					this.successToast();
					var utilEvent = $A.get("e.c:E_RefreshUtilityEvent");
					utilEvent.fire();
					//this.closeModal(component);
					component.find("overlayLib").notifyClose();
				} else {
					var users = component.find("onShareError");
					var dispMessage = this.buildDisplayMessage(component,response.getReturnValue());
					users.set("v.value",dispMessage);					
					$A.util.addClass(users,"show-error");
					component.set("v.selectedUsers",[]);
					users = component.find("userError");
					users.set("v.value","");
					$A.util.removeClass(users,"show");
					component.find('shareBtn').set('v.disabled',false);
				}
            }
        });
        $A.enqueueAction(action);
		component.set("v.isReshare", false); // set it back to false after every share.
    },

    successToast: function(){
        var successToast = $A.get("e.force:showToast");
        
        successToast.setParams({
            type: "Success",
            message: "Your Search has been shared!",
        });
        successToast.fire();
    },

    parseEntry: function(component, params){
        component.set('v.searchEntry', JSON.parse(params.searchEntry));
        //component.set('v.searchType', params.searchType);
    },

    resetSearch: function(component){
        var titleField = component.find("shareSearchInput");
        
        titleField.set('v.value', "");
        titleField.set("v.errors", []);
        titleField.set("v.isError",false);
        $A.util.removeClass(titleField,"slds-has-error show-error-message");
		component.set("v.shareSearchTitle",""); 
		component.set("v.selectedUsers",[]);
		component.set("v.shareMessage","");
		component.find('shareBtn').set('v.disabled',false);	
		var userError = component.find("userError");
		userError.set("v.value","");
		$A.util.removeClass(userError,"show-error");
		$A.util.removeClass(userError,"show-message");
		var shareError = component.find("onShareError");
		shareError.set("v.value","");
		$A.util.removeClass(shareError,"show-message");
		$A.util.removeClass(shareError,"show-error");
		titleField.focus();	
    },

    closeModal: function(component) {
        //this.toggleClassInverse(component,'backdropSaveSearchModal','slds-backdrop--');
        //this.toggleClassInverse(component,'shareSearchModal','slds-fade-in-');
		component.set("v.openModalStyles", false);
		component.find("overlayLib").notifyClose();
    },

    openModal: function(component){
        this.toggleClass(component,'backdropSaveSearchModal','slds-backdrop--');
        this.toggleClass(component,'shareSearchModal','slds-fade-in-');
		
    },

    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
        //  console.log(modal);
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },

	populateSelectedUsers : function (component) {
		var currentUsers =  [];
		var codes = []; 
		if(component.get("v.selectedUsers").length > 0){
			currentUsers = component.get("v.selectedUsers"); 
			for(var i = 0;i<currentUsers.length;i++){
				codes.push(currentUsers[i].key);
			}         
		}
		console.log(codes);
		component.set("v.selectedUserIds",codes);
	},

	setUserSelectionCriteria : function (component) {
		var user = component.get("v.runningUser");
		var searchEntry = component.get('v.searchEntry');
		if (user) {
			var conditon = "Id !='"+user.id+"'";  // UNCOMMENT THISLINE ON COMMIT
			var companyName = user.companyName;
			if ($A.get("$Label.c.ATS_TEK_AEROTEK_EMEA_AGS").includes(companyName)) { 
				conditon = conditon+ " AND ( CompanyName IN ("+$A.get("$Label.c.ATS_TEK_AERO_EMEA_AGS_QUOTE")+"))";
			} else if ($A.get("$Label.c.ATS_MLA_AP").includes(companyName)) { 
				conditon = conditon+ " AND ( CompanyName IN ("+$A.get("$Label.c.ATS_MLA_AP_QUOTE")+"))";
			}
			/*if (companyName === 'TEKsystems, Inc.' || companyName === 'Allegis Global Solutions, Inc.' || companyName === 'Aerotek, Inc' || companyName === 'AG_EMEA') { 
				conditon = conditon+ "( CompanyName IN ('TEKsystems, Inc.','Allegis Global Solutions, Inc.','Aerotek, Inc','AG_EMEA'))";
			} else if (companyName === 'Allegis Partners, LLC' || companyName === 'Major, Lindsey & Africa, LLC') {
				conditon = conditon+"( CompanyName IN ('Major, Lindsey & Africa, LLC','Allegis Partners, LLC'))";
			} */			
			component.set("v.whereCondition", conditon);
		}	
	},

	setAttributes : function (component) {
		this.setPermissionSetId(component);
		if (!component.get("v.runningUser")) {
			 this.getCurrentUser(component);

		} else {
			this.setUserSelectionCriteria(component);
		}
		this.resetSearch(component);
	},

	checkUserSelection : function (component) {
		var users = component.find("userError");
		var len = component.get("v.selectedUsers").length;
		if (len <= 0) {
				users.set("v.value",$A.get("$Label.c.ATS_SELECT_ONE_USER")); 
				$A.util.addClass(users,"show-error");
				$A.util.removeClass(users,"show-message");
		}  else if (len > 10) {
				users.set("v.value",$A.get("$Label.c.ATS_MAX_10_SHARE"));
				$A.util.addClass(users,"show-error");
				$A.util.removeClass(users,"show-message");
		} else {
			var remaining = Number(10-len);
			if (remaining != 0) {
				users.set("v.value","You may add " + remaining +" more user(s).");
				$A.util.addClass(users,"show-message");
				$A.util.removeClass(users,"show-error");
			} else {
				users.set("v.value","");
				$A.util.removeClass(users,"show-error");
				$A.util.removeClass(users,"show-message");
			}
		}
		var ids ='';
		var users = component.get("v.selectedUsers");
		for (var i=0; i<len; i++) {
			if (ids.length == 0) {
				ids = "'"+users[i].key+"'";
			} else {
				ids+=",'"+users[i].key+"'";
			}
		}
		this.setUserSelectionCriteria(component);
		var condition = component.get("v.whereCondition");
		if (len > 0 && len <=10) {
			component.set("v.whereCondition", condition+" AND ID NOT IN ("+ids+")");
		}
	},

	isShareableSearchEntry : function(component) {
		var facetList = $A.get("$Label.c.ATS_NOT_SHAREABLE_FACET_LIST").split("|");
		var searchEntry = component.get('v.searchEntry');
		var selectedFacetList = searchEntry.criteria.facets;
		var isShareable = true;
        
		if (selectedFacetList) {
			facetList.forEach(fct => {
				var facetVal = selectedFacetList[fct];
				if (facetVal && (facetVal.includes('"UserId"') || facetVal.includes('"User"'))) {
					isShareable = false;
				}
			});
		}
                
		return isShareable;
	},

	initModalData : function (component) {
		this.setAttributes(component);
		var searchEntry = component.get('v.searchEntry');
		component.set("v.shareSearchTitle",searchEntry.title); 
		if (searchEntry.message) {
			component.set("v.shareMessage",searchEntry.message); 
		}
		if (component.get('v.shareFrom') === 'Shared By Me') {
			component.set("v.isReshare", true);
		}				
	},

	setPermissionSetId : function(component) {
		component.set("v.permissionSetId","0PS1p000000zWunGAE");
        var action = component.get("c.getPermissionSetIdByName");
		action.setParams({ "permissionSetName" : "User_Group_B"});
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.permissionSetId", response.getReturnValue());
				this.setUserSelectionCriteria(component);              	                
			}
        });
        $A.enqueueAction(action);
    },

	getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var r = JSON.parse(response.getReturnValue());
                component.set ("v.runningUser", r); 
				this.setUserSelectionCriteria(component);               	                
			}
        });
        $A.enqueueAction(action);
    },

	buildDisplayMessage : function (component, response) {	
		var users = component.get("v.selectedUsers");
		var sentList = '';
		for (var i=0; i<users.length; i++) {
			if (!response.includes(users[i].value)) {
				if (sentList.length == 0) {
					sentList = users[i].value;
				} else {
					sentList+= ', '+users[i].value;
				}
			}
		}
		// A notification has been sent out to them to clean up their list to get new shares.
		var returnMessage ='<br/><div class="show-error">'+$A.get("$Label.c.ATS_SHARE_ATTEMPET_UNSUCCESSFUL")+'\r\n '+response+'.</div>';
		if (sentList.length > 0) {
			returnMessage='<div class="slds-text-color_success">'+$A.get("$Label.c.ATS_SHARE_SUCCESSFUL_MESSAGE")+': '+ sentList+ '.</div> \r\n\r\n'+returnMessage;
		}
		return returnMessage;
	}
})