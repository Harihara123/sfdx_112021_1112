({
	getActionWithParameters: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.getActionWithParameters(component, 
                parameters.component, 
                parameters.queryStringUrl, 
                parameters.smartTerms);
		}
	},

	getBaseParameters: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.getBaseParameters(parameters.component);
		}
	},

	createSearchLogEntry: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.createSearchLogEntry(component, parameters.component, parameters.baseEntry);
		}
	},

	restoreCriteriaFromLog: function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			return helper.restoreCriteriaFromLog(component, parameters.component);
		}
	},

	getMatchInsightParameters : function(component, event, helper) {

	},

	getMatchInsightActionWithParameters : function(component, event, helper) {

	}
})