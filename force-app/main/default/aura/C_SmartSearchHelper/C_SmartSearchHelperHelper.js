({
	getActionWithParameters: function (helperCmp, criteriaCmp, queryStringUrl, smartTerms) {
		var action = criteriaCmp.get(helperCmp.get("v.actionName")); 
		// var location = criteriaCmp.get("v.usePresetLocation") ? criteriaCmp.get("v.autoMatchLocation") : null;

        // var smartSearchPostBody = JSON.stringify(this.getSmartSearchPostBody(criteriaCmp));
        action.setParams({
            "queryString" : queryStringUrl,
            "postBody" : JSON.stringify(this.getSmartSearchPostBody(smartTerms))
        });

		return action;
	},

    getSmartSearchPostBody : function (smartTerms) {
        var smartSearchPostBody = { 
            match_info: {
                title_vector: [],
                jobcode_vector: [],
                skills_vector: [],
                skillset_vector: [],
                industry_vector: [],
                company_names: []
            }
        };

        let mapper = {
            title: "title_vector",
            job_function: "jobcode_vector",
            industry: "industry_vector",
            company: "company_names",
            skill: "skills_vector",
            skillset: "skillset_vector"
        }
        
        smartTerms.forEach(term => {
            smartSearchPostBody.match_info[mapper[term.type]].push({
                term: term.name
            });
        });
        
        return smartSearchPostBody;
    },
	
	getBaseParameters: function (criteriaCmp) {
		// var keyword = criteriaCmp.find("inputSearchId").get("v.value");
		/*let keyword = criteriaCmp.get("v.keyword");
		if (keyword === undefined || keyword === null || keyword === "") {
			// Assumes keywordless search is allowed for the applied facets. Checked during keyword validation.
			keyword = "-randomstring";
		}*/
		return {
			"id": criteriaCmp.get("v.runningUser.id"), 
			//"q": keyword, 
            "type": "smart",
            "dimensions_definition": "titlevector:VECTOR:1.0:NONE,skillsvector:VECTOR:1:NONE,jobcodevector:VECTOR:1.0:NONE,skillsetvector:VECTOR:1.0:NONE,industryvector:VECTOR:1.0:NONE,company_names:ARRAY:1.0:NONE",
            "probability": false,
			"size": criteriaCmp.get("v.pageSize"), 
			"from": criteriaCmp.get("v.offset"), 
			"flt.group_filter": criteriaCmp.get("v.runningUser.opcoSearchFilter"),
            "appId": "ATS_SmartSearch",
            "rescore": "true",
            "insight": "full"
		};
	},

	createSearchLogEntry: function (helperCmp, criteriaCmp, baseEntry) {
        // baseEntry.criteria.keyword = criteriaCmp.find("inputSearchId").get("v.value");
        baseEntry.criteria.keyword = criteriaCmp.get("v.keyword");
        baseEntry.criteria.multilocation = criteriaCmp.get("v.multilocation");
        baseEntry.criteria.facets = criteriaCmp.get("v.facets");
        baseEntry.criteria.filterCriteriaParams = criteriaCmp.get("v.filterCriteriaParams");
		baseEntry.fdp = criteriaCmp.get("v.facetDisplayProps"); 
        
        return JSON.stringify(baseEntry);
	},

	restoreCriteriaFromLog: function (helperCmp, criteriaCmp) {
		var logEntry = criteriaCmp.get("v.lastSearchLog");
		if (logEntry !== undefined && logEntry !== null) {
            var searchParams = JSON.parse(logEntry);

            if (searchParams) {
            	
				// criteriaCmp.find(inputSearchId).set("v.value", searchParams.criteria.keyword);
				criteriaCmp.set("v.keyword", searchParams.criteria.keyword);
                criteriaCmp.set("v.multilocation", searchParams.criteria.multilocation);
                /*component.set("v.location", searchParams.criteria.location.text);
                component.set("v.latlong", searchParams.criteria.location.geo);
                if (searchParams.criteria.location.geo) {
                    // If geo coordinates were on this restored search, then enable radius.
                    component.set("v.useRadius", true);
                }
                component.set("v.radius", searchParams.criteria.location.rad);
                component.set("v.radiusUnit", searchParams.criteria.location.unit);*/
				criteriaCmp.set("v.facets", searchParams.criteria.facets);
				criteriaCmp.set("v.filterCriteriaParams", searchParams.criteria.filterCriteriaParams);
                criteriaCmp.set("v.offset", searchParams.offset);
                criteriaCmp.set("v.pageSize", searchParams.pageSize);
                if (searchParams.sort) {
					criteriaCmp.set("v.sortField", searchParams.sort);
					criteriaCmp.set("v.sortOrder", searchParams.order);
				}
				criteriaCmp.set("v.facetDisplayProps", searchParams.fdp);

				// type undefined check required for pre-refactoring saved searches.
				criteriaCmp.set("v.type", searchParams.type ? searchParams.type : "C");
			}
		}
	}
})