({
	getWorkflowStatuses : function (component, event, helper) {
		var parameters = event.getParam("arguments");
		console.log('parameters:'+JSON.stringify(parameters));
		if (parameters) {
			// console.log("Get available statuses - " + JSON.stringify(parameters));
			helper.getWorkflowStatuses(component, parameters.params, parameters.callback);
		}
	},
    getAllDispositionStatuses: function (component, event, helper) {
        var parameters = event.getParam("arguments");
        if (parameters) {
            helper.getAllDispositionStatuses(component,parameters.callback);
            }
    },

	updateStatus : function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			//alert(JSON.stringify(parameters));
			console.log("Update status - " + JSON.stringify(parameters));
			var params = {
				"submittalId" : parameters.submittalId,
				"status" : parameters.status,
                "submittalEventId" : parameters.submittalEventId,
                "dispCode" : parameters.dispcode,
                "dispReason" : parameters.dispReason
			};
			helper.updateStatus(component, params, parameters.callback);
		}
		  
	}, 
	massUpdateStatus : function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			var params = {
				"subIds" : parameters.subIds
			};
			component.set("v.subMassIds", parameters.subIds);
			helper.massUpdateStatus(component, params, parameters.callback);
		}
		   
	},
	massUpdateDisposition : function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			component.set("v.subMassIds", parameters.subIds);
			helper.massUpdateDisposition(component, parameters.subIds, parameters.callback);
		}
		   
	},

	massSendEmail : function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			var params = {
				"subIds" : parameters.subIds
			};
			component.set("v.subMassIds", parameters.subIds);
			helper.massSendEmail(component, params, parameters.callback);
		}
		  
	},
	//added by akshay for S-106132 11/9/2018
	massAddToCallSheet : function(component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			component.set("v.massContacts", parameters.contacts);
			component.set("v.subject", parameters.subject);
			helper.massAddToCallSheet(component, parameters);
		}
		  
	},

	selectStatus : function(component, event, helper) {
		console.log('Inside SH Controller');
		
		var parameters = event.getParam("arguments");
		if (parameters){
			component.set("v.submittalId", parameters.submittalId);
			component.set("v.submittal", parameters.submittal);
			component.set("v.targetStatus", parameters.status);
			component.set("v.isApplicationCard",parameters.isApplicationCard);
			console.log('Parameters:::'+JSON.stringify(parameters));
			//console.log('parameters.status:'+parameters.status);
			if(!parameters.isApplicationCard) {
				if(parameters.submittal.RecordType.DeveloperName == 'Proactive' ) {
					if(parameters.status == 'Interviewing' || parameters.status == 'Add New Interview' || parameters.status == 'Edit Interview') {
						component.set("v.reqOpco", "Proactive+"+parameters.status);
						console.log('v.reqOpco:'+component.get("v.reqOpco"));
					}
					else
						component.set("v.reqOpco", "Proactive");

				} 
				else {
					component.set("v.opptyReqProduct", parameters.submittal.Opportunity.Req_Product__c);
					component.set("v.reqOpco", parameters.submittal.Opportunity.Req_OpCo_Code__c);
				}
			}
			else {
				component.set("v.opptyReqProduct", parameters.submittal.Opportunity.Req_Product__c);
				component.set("v.reqOpco", parameters.submittal.Opportunity.Req_OpCo_Code__c);
			}
			var totPositions,filledPositions, openPositions;
			if(!parameters.isApplicationCard) {
				if(parameters.submittal.RecordType.DeveloperName != 'Proactive') {
					totPositions = parameters.submittal.Opportunity.Req_Total_Positions__c;
					filledPositions = parameters.submittal.Opportunity.Req_Total_Filled__c;
					openPositions =  parameters.submittal.Opportunity.Req_Open_Positions__c;
				} 
			} else {
				totPositions = parameters.submittal.Opportunity.Req_Total_Positions__c;
				filledPositions = parameters.submittal.Opportunity.Req_Total_Filled__c;
				openPositions =  parameters.submittal.Opportunity.Req_Open_Positions__c;
			}
			component.set("v.maxPositionsFilled", openPositions === 1);
			// helper.selectStatus(component, params, parameters.callback); //not commented by me
			var rTotFill,rTotLost,rTotLost,rTotWash,totalCount=0;
			if(!parameters.isApplicationCard) {
				if(parameters.submittal.RecordType.DeveloperName != 'Proactive') {
					rTotFill= parameters.submittal.Opportunity.Req_Total_Filled__c || 0;
      				rTotLost= parameters.submittal.Opportunity.Req_Total_Lost__c || 0;
      				rTotWash= parameters.submittal.Opportunity.Req_Total_Washed__c || 0;
      				totalCount=parameters.submittal.Opportunity.Req_Total_Positions__c	-(rTotFill+rTotLost+rTotWash) ;
				}
			} else {
				rTotFill= parameters.submittal.Opportunity.Req_Total_Filled__c || 0;
      			rTotLost= parameters.submittal.Opportunity.Req_Total_Lost__c || 0;
      			rTotWash= parameters.submittal.Opportunity.Req_Total_Washed__c || 0;
      			totalCount=parameters.submittal.Opportunity.Req_Total_Positions__c	-(rTotFill+rTotLost+rTotWash) ;
			}
			if(parameters.status === "Offer Accepted" && parameters.submittal.RecordType.DeveloperName == 'Proactive' && parameters.isApplicationCard == false) {
				helper.openModal(component);
			}else{
			//if(parameters.status != "Offer Accepted"){
					if(!parameters.isApplicationCard) {
						if (parameters.status === "Not Proceeding" || parameters.status === "Edit Not Proceeding"
								|| parameters.status === "Interviewing" || parameters.status === "Add New Interview" || parameters.status === "Edit Interview"
								|| (parameters.status === "Offer Accepted"  && parameters.submittal.Opportunity.Req_OpCo_Code__c !== "AEROTEK" && parameters.submittal.Opportunity.Req_OpCo_Code__c !== "TEK")
								|| (parameters.status === "Offer Accepted"  && component.get("v.maxPositionsFilled") && (parameters.submittal.Opportunity.Req_OpCo_Code__c === "AEROTEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c === "TEK" || parameters.submittal.RecordType.DeveloperName === 'Proactive'))
								|| parameters.status === "Remove Applicant" || parameters.status === "Remove Link") {
								helper.openModal(component);
						}
						else if(parameters.status === "Offer Accepted" && totalCount==0  && (parameters.submittal.Opportunity.Req_OpCo_Code__c=="AEROTEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="TEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="AG_EMEA" || parameters.submittal.RecordType.DeveloperName === 'Proactive')){
							helper.openModal(component);
						}
                        else if(parameters.status === "Started" && (parameters.submittal.Opportunity.Req_OpCo_Code__c=="AEROTEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="TEK")){
                            helper.openModal(component);
                        } 
                        else {
							console.log('Modal is not openning, should try to save it using other methods.')
							//JSON.parse(JSON.stringify(component.get('v.lwcSubmittals')))


							if(component.get('v.lwcSubmittals') && parameters.status === 'Offer Accepted') {
								// helper.updateStatus(function(response) {
								// 	component.get('lwcSubmittals').toggleSpinner(false);
								// 	component.get('lwcSubmittals').showToast('success', 'Success', 'Submittal Updated Successfully.')
								// 	}, parameters.submittalId, parameters.status, "", "",""
								// );
								let params = {
									"submittalId" : parameters.submittalId,
									"status" : parameters.status,
									"submittalEventId" : parameters.submittalEventId,
									"dispCode" : parameters.dispcode,
									"dispReason" : parameters.dispReason
								};
								helper.updateStatus(component, params, ()=>{
									component.get('v.lwcSubmittals').toggleSpinner(false);
									component.get('v.lwcSubmittals').showToast('success', 'Success', 'Submittal Updated Successfully.')
									component.get('v.lwcSubmittals').refreshSubmittals()
								});
							}
						}
					} else {
						if (parameters.status === "Not Proceeding" || parameters.status === "Edit Not Proceeding"
							|| parameters.status === "Interviewing" || parameters.status === "Add New Interview" || parameters.status === "Edit Interview" 
							|| (parameters.status === "Offer Accepted"  && parameters.submittal.Opportunity.Req_OpCo_Code__c !== "AEROTEK" && parameters.submittal.Opportunity.Req_OpCo_Code__c !== "TEK")
							|| (parameters.status === "Offer Accepted"  && component.get("v.maxPositionsFilled") && (parameters.submittal.Opportunity.Req_OpCo_Code__c === "AEROTEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c === "TEK" ))
							|| parameters.status === "Remove Applicant" || parameters.status === "Remove Link") {
							helper.openModal(component);
						}
						else if(parameters.status === "Offer Accepted" && totalCount==0  && (parameters.submittal.Opportunity.Req_OpCo_Code__c=="AEROTEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="TEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="AG_EMEA" || parameters.submittal.RecordType.DeveloperName === 'Proactive')){
							helper.openModal(component);
						}
                        else {
							console.log('Modal is not openning, should try to save it using other methods.')
							//JSON.parse(JSON.stringify(component.get('v.lwcSubmittals')))


							if(component.get('v.lwcSubmittals') && parameters.status === 'Offer Accepted') {
								// helper.updateStatus(function(response) {
								// 	component.get('lwcSubmittals').toggleSpinner(false);
								// 	component.get('lwcSubmittals').showToast('success', 'Success', 'Submittal Updated Successfully.')
								// 	}, parameters.submittalId, parameters.status, "", "",""
								// );
								let params = {
									"submittalId" : parameters.submittalId,
									"status" : parameters.status,
									"submittalEventId" : parameters.submittalEventId,
									"dispCode" : parameters.dispcode,
									"dispReason" : parameters.dispReason
								};
								helper.updateStatus(component, params, ()=>{
									component.get('v.lwcSubmittals').toggleSpinner(false);
									component.get('v.lwcSubmittals').showToast('success', 'Success', 'Submittal Updated Successfully.')
									component.get('v.lwcSubmittals').refreshSubmittals()
								});
							}
						}       

					}
			}
			//}
			//else if(parameters.status === "Offer Accepted"){	
			// Added conditions to not show popover for all conditions as per the PROD
			/*else if(parameters.status === "Offer Accepted" && totalCount==0) {
				if(parameters.submittal.RecordType.DeveloperName != 'Proactive') {
					if(parameters.submittal.Opportunity.Req_OpCo_Code__c=="AEROTEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="TEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="AG_EMEA") {
						helper.openModal(component);
					}

				} else {
						 
					helper.openModal(component);
				}
			}*/
            /*else if(parameters.status === "Offer Accepted" && totalCount==0  && (parameters.submittal.Opportunity.Req_OpCo_Code__c=="AEROTEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="TEK" || parameters.submittal.Opportunity.Req_OpCo_Code__c=="AG_EMEA"  )){
				
				helper.openModal(component);
			}*/
		}
		  
	},

	openModal : function(component, event, helper) {
        helper.openModal(component);
	},

	closeModal: function(component, event, helper) {
		helper.closeModal(component);
	},

	handleChangeCancel : function(component, event, helper) {
		helper.clearAttributes(component);
		helper.closeModal(component);
	},

	handleChangeSave : function(component, event, helper) {
        if (component.get("v.submittal.ShipToContactId") === event.getParam("contactId")) {
			helper.clearAttributes(component);
			helper.closeModal(component);
		}
	}/*,
	OpenConvertToOppModal : function(cmp, event, helper) {
		console.log('Open Conver to Opp mOdal');
		cmp.set('v.submittalId',event.getParam("submittal.Id"));
		cmp.set('v.submittal', event.getParam("submittal"));
		cmp.set('v.targetStatus', event.getParam("targetStatus"));
		cmp.set('v.reqOpco', event.getParam("reqOpco"));
		cmp.set('v.runningUser', event.getParam("runningUser"));
		
	}*/
})