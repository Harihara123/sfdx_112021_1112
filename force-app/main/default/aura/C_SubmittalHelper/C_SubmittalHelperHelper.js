({
	getWorkflowStatuses : function (component, params, callback) {
		var action = component.get("c.getAvailableStatuses");
		action.setParams(params);
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				var stList = response.getReturnValue();
				// If returned option list does not have the current status, add the same to the top of the list.
				if (!stList.includes(params.status)) {
					stList.unshift(params.status);
				}
				callback.call(this, stList);
			} else {
				console.log("Error retrieving status workflow options");
			}
		}); 
		$A.enqueueAction(action);
	},

    
    getAllDispositionStatuses : function (component, callback) {
        var action = component.get("c.getDispositionMatrix");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var stList = response.getReturnValue();

                callback.call(this, stList);
            } else {
                console.log("Error retrieving Disposition options");
            }
        }); 
        $A.enqueueAction(action);
    },

	updateStatus : function(component, params, callback) {
		// console.log("Helper save submittal status");
		var action = component.get("c.saveSubmittalStatusUpdate");
		action.setParams(params);
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				callback.call(this, response.getReturnValue());
                this.fireSavedEvt(component);
			} else {
				console.log("Error updating submittal status");
			}
		});
		$A.enqueueAction(action);
	},

	massUpdateStatus : function(component, params, callback) {
		// console.log("Helper save submittal status");
		
		
		var action = component.get("c.getSubmittalMassUpdate");
		action.setParams(params);
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				if(response.getReturnValue().length > 0){
					component.set("v.massOptions",response.getReturnValue());
					component.set("v.targetStatus", "mass");
					this.openModal(component);
				}else{
						var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								mode: 'dismissible',
								title: $A.get("$Label.c.ATS_WARNING"),
								message: $A.get("$Label.c.ATS_MASS_UPDATE_NO_STATUS_AVAILABLE"),
								type: 'error'
							});
						toastEvent.fire();
				}
			    
				//callback.call(this, response.getReturnValue());
			} else {
				console.log("Error updating submittal status");
			}
		});
		$A.enqueueAction(action);
	},

	massUpdateDisposition : function(component, params, callback) {
		var action = component.get("c.getDispositionMatrix");
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				if(response.getReturnValue().length > 0){
					component.set("v.targetStatus", "massDisposition");
					component.set("v.dispoMatrix",response.getReturnValue());
					this.openModal(component);
			    }else{
						var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								mode: 'dismissible',
								title: $A.get("$Label.c.ATS_WARNING"),
								message: $A.get("$Label.c.ATS_MASS_UPDATE_NO_STATUS_AVAILABLE"),
								type: 'error'
							});
						toastEvent.fire();
				}
			    
			} else {
				console.log("Error updating disposition status");
			}
		});
		$A.enqueueAction(action);
		
		},
	

	massSendEmail : function(component, params, callback) {
		component.set("v.targetStatus", "massSendEmail"); 
		this.openModal(component);
	},

	//added by akshay for S-106132 11/9/2018
	massAddToCallSheet : function(component, params, callback) {
		component.set("v.targetStatus", "massCallSheet"); 
		//this.openModal(component);
	},

	selectStatus : function(component, params, callback) {
		// console.log("Helper submittal status change");
		callback.call(this, "Response select status!");
	},

	closeModal: function(component) {
		this.toggleClassInverse(component,'backdropModal','slds-backdrop_');
		this.toggleClassInverse(component,'mainModal','slds-fade-in-');
    },

    openModal: function(component){
        this.toggleClass(component,'backdropModal','slds-backdrop_');
        this.toggleClass(component,'mainModal','slds-fade-in-');
    },

    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },

	clearAttributes : function(component) {
		component.set("v.targetStatus", "");
		// Clearing the submittal ID causes an error on the lightning:recordEditForm component as it tries to reload the form data.
		//component.set("v.submittalId", "");
		component.set("v.submittal", null);
	},	
    fireSavedEvt : function(component) {
		var evt = $A.get("e.c:E_SubmittalStatusChangeSaved");
        evt.setParam("contactId", component.get("v.submittal.ShipToContactId"));
		evt.fire();
	},
})