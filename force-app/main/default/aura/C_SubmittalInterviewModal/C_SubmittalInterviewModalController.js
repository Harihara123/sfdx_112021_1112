({
    doInit : function(component, event, helper){
		console.log('~~~~~~~~~~~~~~~~~'+component.get("v.modalPromise"));
        helper.queryEvents (component, event, helper);
    },
    closeModal: function (cmp, e, h) {
        let promise = cmp.get('v.modalPromise')
        promise.then(
            function (modal) {
                modal.close();
            }
        );
    },
    validateDate : function(component, event, helper){
        helper.validateDate(component,event,helper);
    },

    createNewEvent : function(component, event, helper){
        helper.createNewEvent(component,event,helper);
    },
    editEvent : function(component, event, helper){
        //var arr = event.getSource().get("v.name").split("~~");
        var indvar = event.getSource().get("v.name");
        helper.editEvent(component, indvar);
    },
    saveSubmittal : function (component, event, helper) {
		//Blocked  1st if else to redirect it to Convert to Opp if check box is checked.
        if(component.get("v.submittal.Opportunity.Req_OFCCP_Required__c") && !component.get("v.submittal.Has_Application__c"))
            helper.showError(component, "Req has OFCCP flag and requires an Application. No Application is found.  Talent cannot be submitted.", "Error", "Error");
        else
            helper.validateAndSaveSubmittal(component);

		/*else if(component.get("v.isConvert")) {
			helper.openConvertToOpportunityModel(component,event);
			
		}
		else
			helper.validateAndSaveSubmittal(component);*/
		/*console.log('isConvert----------------------------SIM'+component.get("v.isConvert"));
		if(component.get("v.isConvert")) {
			helper.openConvertToOpportunityModel(component,event);			
		}else if(component.get("v.submittal.Opportunity.Req_OFCCP_Required__c") && !component.get("v.submittal.Has_Application__c")){
            helper.showError(component, "Req has OFCCP flag and requires an Application. No Application is found.  Talent cannot be submitted.", "Error", "Error");
		}else{
            helper.validateAndSaveSubmittal(component);
		}*/
    },

    cancelChange: function(component, event, helper) {
        helper.fireCanceledEvt(component);
    },
    //Sandeep: When start date time is changed, set end date time to next hour
    onStartDateChange : function(component, event, helper) {
        console.log('on StartDate Change ');
        var whichfield = event.getSource();
        if(whichfield.getLocalId() == 'startdatetime'){
            helper.setEndDate(component,true);
        }
        //console.log('whichfield ',whichfield.getGlobalId());
        helper.validateDate(component,event,helper);
    },

})