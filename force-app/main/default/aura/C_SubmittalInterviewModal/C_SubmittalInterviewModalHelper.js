({
	queryEvents : function(component, event, helper) {
		var isEditInt = component.get("v.editMode");
		console.log("From editMode: " +isEditInt);
		if(!isEditInt){
			var millisecondsPerHour  = 3600000;
			var currentHour = new Date().getTime() / millisecondsPerHour;
			var sDate =  new Date(Math.ceil(currentHour) * millisecondsPerHour);
			var formatSDate = sDate.toISOString();
			component.set("v.event.StartDateTime", formatSDate);

			var newEDate = sDate.setMinutes(sDate.getMinutes() + 60);
			var eDate = new Date(newEDate);
			var formatEDate = eDate.toISOString();
			component.set("v.event.EndDateTime", formatEDate);
        } 

		var action = component.get("c.queryInterviewEvents");
		action.setParams({ "contactId" : component.get("v.submittal.ShipToContactId"), "submittalId" : component.get("v.submittal.Id")});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseEvts = response.getReturnValue();
				//console.log("From server: " + JSON.stringify(responseEvts));
				component.set("v.events", response.getReturnValue());
					
				if(isEditInt){
					let editEvt = Object.assign({}, responseEvts[0])
					//console.log("editEvt: " + JSON.stringify(editEvt));
					component.set("v.event", editEvt);
					component.set("v.eventId", editEvt.Id);
					component.find("subInterviewer").set("v.value", editEvt.Submittal_Interviewers__c);
					component.find("intType").set("v.value", editEvt.Interview_Type__c);
                    component.find("intStatus").set("v.value", editEvt.Interview_Status__c);
					if(!$A.util.isUndefinedOrNull(editEvt.Description)){
						var editComments = JSON.parse(editEvt.Description);
						component.set("v.candComments", editComments.CandidateComments);
						component.set("v.clientComments", editComments.ClientComments);
					}
					if (component.get('v.preselectedEvent')) {
						this.preselectEvent(component)
					}
				}
			}
			else if (state === "INCOMPLETE") {
				// do something
			}
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + 
									errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},

	createNewEvent : function(component, event, helper) {
		component.set("v.editMode", false);
		
		var millisecondsPerHour  = 3600000;
		var currentHour = new Date().getTime() / millisecondsPerHour;
		var sDate =  new Date(Math.ceil(currentHour) * millisecondsPerHour);
		var formatSDate = sDate.toISOString();
        console.log('StartDateTime ',formatSDate);
		component.set("v.event.StartDateTime", formatSDate);

		var newEDate = sDate.setMinutes(sDate.getMinutes() + 60);
		var eDate = new Date(newEDate);
		var formatEDate = eDate.toISOString();
        console.log('EndDateTime ',formatSDate);
		component.set("v.event.EndDateTime", formatEDate);

		component.find("subInterviewer").set("v.value", "");
		component.find("intType").set("v.value", "In-Person");
		component.find("intStatus").set("v.value", "Open");
		component.set("v.candComments", "");
		component.set("v.clientComments", "");
	},

	//Edit Event
	editEvent : function(component, indvar) {
		
		component.set("v.editMode", true);
		
		component.set("v.eventId", component.get("v.events")[indvar].Id);
		component.set("v.event.StartDateTime", component.get("v.events")[indvar].StartDateTime);
		component.set("v.event.EndDateTime", component.get("v.events")[indvar].EndDateTime);
		component.find("subInterviewer").set("v.value", component.get("v.events")[indvar].Submittal_Interviewers__c);
		component.find("intType").set("v.value", component.get("v.events")[indvar].Interview_Type__c);
		component.find("intStatus").set("v.value", component.get("v.events")[indvar].Interview_Status__c);
		
		if(!$A.util.isUndefinedOrNull(component.get("v.events")[indvar].Description)){
			var comments = JSON.parse(component.get("v.events")[indvar].Description);
			component.set("v.candComments", comments.CandidateComments);
			component.set("v.clientComments", comments.ClientComments);
		}
		
	},
	
	validateDate : function(component, event, helper) {
		var validForm = true;

		var startDateTimeCmp = component.find("startdatetime");
		if(startDateTimeCmp){
			var startDateTime = startDateTimeCmp.get("v.value");
		}

		var endDateTimeCmp = component.find("enddatetime");
		if(endDateTimeCmp){
			var endDateTime = endDateTimeCmp.get("v.value");
		}

		if(startDateTime !== undefined && endDateTime !== undefined && startDateTime !== null && endDateTime !== null){
			var sDate = Date.parse(startDateTime);
			var eDate = Date.parse(endDateTime);

			if (sDate > eDate) {
                component.set("v.dispWarning",true);
				var endDateWarning = component.find("endDateWarning");
				$A.util.removeClass(endDateWarning, "toggle");
				validForm = false;
			} else {
				var endDateWarning = component.find("endDateWarning");
				$A.util.addClass(endDateWarning, "toggle");
			}

			// Check if event is longer than 14 days - 14 (days) * 24 (hrs) * 60 (mins) * 60 (secs) * 1000 (msecs)
			if ((eDate - sDate) > 1209600000) {
                component.set("v.dispWarning",true);
				var longEventWarning = component.find("longEventWarning");
				$A.util.removeClass(longEventWarning, "toggle");
				validForm = false;
			} else {
				var longEventWarning = component.find("longEventWarning");
				$A.util.addClass(longEventWarning, "toggle");
			}
		}
				
		var saveBtnCmp = component.find("btnSaveSubmittal");
		if(saveBtnCmp){
			if(validForm){
				saveBtnCmp.set("v.disabled",false);
			}else{
				saveBtnCmp.set("v.disabled",true);
			}	
		}
	},

	validateAndSaveSubmittal : function (component) {
        if (this.validateEvent(component)) {
            this.saveEventAndSubmittal(component);
			if(component.get("v.isConvert")) {
				let reloadEvt = $A.get("e.c:E_TalentActivityReload");
				reloadEvt.fire();
				this.openConvertToOpportunityModel(component);
			}
        }
		
    },

	validateEvent : function(component){
		var validForm = true;

		var subIntCmp = component.find("subInterviewer");
		var subIntValue = subIntCmp.get("v.value");
		if(subIntValue === undefined || subIntValue === ""){
	  	//subIntCmp.set("v.errors", [{message:"Interviewer(s) is a required field"}]);
		subIntCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_Interviewer_s_is_a_required_field")}]);
			validForm = false;
		}
		else {
			subIntCmp.set("v.errors", null);
		}

		return validForm;

    },

	saveEventAndSubmittal : function(component){
		var spinner = component.find('spinner');
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, 'slds-show');
		
		var candComments = component.find("candComments").get("v.value");
		var clientComments = component.find("clientComments").get("v.value");
		var descJSON = {CandidateComments : candComments, ClientComments: clientComments};
		var descJSONStr = JSON.stringify(descJSON);

		var isEditInt = component.get("v.editMode");
		if(!isEditInt){
			var newEvent = {"sobjectType" : "Event",
							"StartDateTime" : component.find("startdatetime").get("v.value"),
							"EndDateTime" : component.find("enddatetime").get("v.value"),
							"Submittal_Interviewers__c" : component.find("subInterviewer").get("v.value"),
							"Interview_Type__c" : component.find("intType").get("v.value"),
							"Interview_Status__c" : component.find("intStatus").get("v.value"),
							"Description" : descJSONStr,
							"WhoId" : component.get("v.submittal.ShipToContactId"),
							"WhatId" : component.get("v.submittal.Id"),
							"OwnerId" : $A.get("$SObjectType.CurrentUser.Id"),
							"Type" : 'Interviewing',
							"Activity_Type__c" : 'Interviewing',
							"Subject" : 'Interviewing',
							"Order_Id__c": component.get("v.submittal.Id"),
							};
		}
		else{
			var newEvent = {"sobjectType" : "Event",
							"Id" : component.get("v.eventId"),
							"StartDateTime" : component.find("startdatetime").get("v.value"),
							"EndDateTime" : component.find("enddatetime").get("v.value"),
							"Submittal_Interviewers__c" : component.find("subInterviewer").get("v.value"),
							"Interview_Type__c" : component.find("intType").get("v.value"),
							"Interview_Status__c" : component.find("intStatus").get("v.value"),
							"Description" : descJSONStr,
							"Subject" : 'Interviewing',
							/*"WhoId" : component.get("v.submittal.ShipToContactId"),
							"WhatId" : component.get("v.submittal.Id"),
							"OwnerId" : '005240000038GjfAAE',
							"Type" : 'Interviewing',
							"Activity_Type__c" : 'Interviewing',*/
							};
		}

		
		//console.log('AKR@@'+ JSON.stringify(newEvent));

		var action = component.get("c.saveSubmittalInterview");
        action.setParams({ 
            "intEvent" : JSON.stringify(newEvent), 
            "submittalId" : component.get("v.submittal.Id"), 
            "isEditInt" : component.get("v.editMode")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log("From server: " + response.getReturnValue());
				const lwc = component.get('v.updateLwcStatus');
				// if (lwc) {
				// 	lwc.lwcStatusUpdate('Interviewing', lwc.submittalId);
				// 	component.set('v.updateLwcStatus', null);
				// }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
				if (lwc) {
					lwc.lwcStatusUpdate(lwc.prevStatus, lwc.submittalId);
					component.set('v.updateLwcStatus', null);
				}
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
			$A.util.removeClass(spinner, 'slds-show');
			$A.util.addClass(spinner, 'slds-hide');
			//this.closeModal(component);
			/*var evt = component.getEvent("statusSavedEvt");
			evt.fire();*/
            this.fireSavedEvt(component);
            //S-78898 -  To Refresh Activity Timeline
			var reloadEvt = $A.get("e.c:E_TalentActivityReload");
            reloadEvt.fire();
        });

        $A.enqueueAction(action);
	},

	fireCanceledEvt : function(component) {
		let promise = component.get('v.modalPromise')
		promise.then(
			function (modal) {
				// const lwc = component.get('v.updateLwcStatus');
				// if (lwc) {
				// 	lwc.lwcStatusUpdate(lwc.prevStatus, lwc.submittalId);
				// 	component.set('v.updateLwcStatus', null);
				// }
				var evt = component.getEvent("statusCanceledEvt");
				evt.fire();
				modal.close();
			}
		);
	},

	fireSavedEvt : function(component) {
		var evt = $A.get("e.c:E_SubmittalStatusChangeSaved");
        evt.setParam("contactId", component.get("v.submittal.ShipToContactId"));
		evt.fire();

		let promise = component.get('v.modalPromise')
		promise.then(
			function (modal) {
				modal.close();
			}
		);
	},
	 showError : function(component, message, messageType, theTitle){
        var title = theTitle;
        var errorMessage = message;
        var theType = messageType;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: theType
        });
        toastEvent.fire();
    },
    /*Sandeep : Set end date to be one hour after start date*/
    setEndDate : function(component,enforce){
        //console.log('afterRender called');
        let startdate=component.find("startdatetime").get("v.value");
        if (startdate) {
			var dateNow= new Date();
			dateNow.setTime(Date.parse(startdate));
			dateNow.setHours(dateNow.getHours() +1);
			component.find("enddatetime").set("v.value",dateNow.toISOString());
		}
    },

	openConvertToOpportunityModel : function(cmp) {
		
		var params = {  "submittalId":cmp.get("v.submittal.Id")
                        ,"submittal" :cmp.get("v.submittal")
                        ,"reqOpco":"Proactive"
						,"targetStatus":cmp.get("v.targetStatus")
                        ,"runningUser":cmp.get("v.runningUser")};
		console.log('submittal--- from SubInterviewModal--'+JSON.stringify(params));
		cmp.find("interviewModal").notifyClose();
		var handleNewModal = $A.get("e.c:E_ConverToOppModalOpen");
		handleNewModal.setParams(params);
		handleNewModal.fire();

		
	},
	 toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    preselectEvent: function(cmp) {
    	const eventId = cmp.get('v.preselectedEvent');
    	const events = cmp.get("v.events");

    	const preselectedIndex = events.findIndex(event => event.Id === eventId);

    	if (preselectedIndex !== -1) {
    		this.editEvent(cmp, preselectedIndex);
		}

    	console.log('preselecting a record')
	}
})