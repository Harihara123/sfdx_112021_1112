({
	cancelStatus: function(component, event, helper) {
		helper.fireCanceledEvt(component);
	},

	saveStatus: function(component, event, helper) {
	    if(helper.validateEvt(component)){
			helper.fireSavedEvt(component);
		}
	},

	submitChange: function(component, event, helper) {
		helper.showSpinner(component);
	}
})