({
	fireCanceledEvt : function(component) {
		var evt = component.getEvent("statusCanceledEvt");
		evt.fire();
	},
validateEvt : function(component) {
	var valid = true;
	var list = component.find("InputSelectDynamic")
	if(component.get("v.selectedValue") === ''){
	 
            // Set error
            list.set("v.errors", [{message:"Please select a value."}]);
			valid = false;
        } else {
            // Clear error
            list.set("v.errors", null);
        }

		return valid;
			
	},
	fireSavedEvt : function(component) {
		this.showSpinner(component);
			
		var params = {
				"subIds" : component.get("v.subIds"),
				"newStatus" : component.find("InputSelectDynamic").get("v.value"),
				"npReason" : component.find("NPReason") ? component.find("NPReason").get("v.value") : ''//component.get("v.selectedValue")
			};
		var action = component.get("c.saveSubmittalMassUpdate");
		action.setParams(params);
		action.setCallback(this, function(response) {
			this.hideSpinner(component);
			if (response.getReturnValue() === 'SUCCESS') {
				var reloadEvt = $A.get("e.c:E_TalentActivityReload");
                	/*Below params used in the C_TalentSubmittalTab for the LWC Submittals component*/
                	reloadEvt.setParams({
                    "subIds" : component.get("v.subIds"),
                    "newStatus" : component.find("InputSelectDynamic").get("v.value")
                    });
                    reloadEvt.fire();
					var evt = component.getEvent("statusCanceledEvt");
					evt.fire()
			} else {
				console.log("Error saving ");
			}
		});
		$A.enqueueAction(action);
		//To Refresh Activity Timeline
		
	},

	showSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},

	hideSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-show");
		$A.util.addClass(spinner, "slds-hide");
	}
})