({
	cancelChange: function(component, event, helper) {
		helper.fireCanceledEvt(component);
	},

	saveChange: function(component, event, helper) {
        helper.hideSpinner(component);
        var npReason = component.find("reason").get("v.value");
        var errorField = component.find("errorCondition");
        if(npReason===''){
            $A.util.removeClass(errorField, 'slds-hide');
        }
        else{          
			helper.fireSavedEvt(component);
        }
	},

	submitChange: function(component, event, helper) {
		helper.showSpinner(component);
	}
})