({
	fireCanceledEvt : function(component) {
		var evt = component.getEvent("statusCanceledEvt");
		evt.fire();
	},

	fireSavedEvt : function(component) {
		var evt = $A.get("e.c:E_SubmittalStatusChangeSaved");
        evt.setParam("contactId", component.get("v.submittal.ShipToContactId"));
		evt.fire();
		//To Refresh Activity Timeline
		var reloadEvt = $A.get("e.c:E_TalentActivityReload");
        reloadEvt.fire();
	},

	showSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},

	hideSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-show");
		$A.util.addClass(spinner, "slds-hide");
	}
})