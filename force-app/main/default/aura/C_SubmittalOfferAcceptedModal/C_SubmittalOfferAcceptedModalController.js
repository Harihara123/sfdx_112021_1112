({
	doInit : function(component, event, helper){
		helper.querySubmittal(component);
   }, 

	onRadio1Change : function(component, event, helper) {
		//var selected = event.getSource().get("v.label");
		var selected = event.getSource().get("v.name");
		component.set("v.radioLabel", selected);
		var checkCmp = component.find("radio2");
		checkCmp.set("v.value", false);
	},
	
	onRadio2Change : function(component, event, helper) {
		var selected = event.getSource().get("v.name");
		component.set("v.radioLabel", selected);
		var checkCmp = component.find("radio1");
		checkCmp.set("v.value", false);
	},
    
    checkClientCont : function(component, event, helper) {
		component.set("v.createClientContact", true);
	},

	cancelChange: function(component, event, helper) {
		helper.fireCanceledEvt(component);
	},

	saveChange: function(component, event, helper) {
		if(component.get("v.submittal.Opportunity.Req_OFCCP_Required__c") && !component.get("v.submittal.Has_Application__c"))
        	helper.showError(component, "Req has OFCCP flag and requires an Application. No Application is found.  Talent cannot be submitted.", "Error", "Error");
        else
        	helper.validateAndSaveSubmittal(component);
	},
    increaseTotalPositions: function(component, event, helper) {
        helper.validateAndSaveSubmittal(component);
        helper.increaseTotalPositions(component);		
	}
})