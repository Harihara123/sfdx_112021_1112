({
	fireCanceledEvt : function(component) {
		var evt = component.getEvent("statusCanceledEvt");
		evt.fire();
	},

	fireSavedEvt : function(component) {
		var evt = $A.get("e.c:E_SubmittalStatusChangeSaved");
        evt.setParam("contactId", component.get("v.submittal.ShipToContactId"));
		evt.fire();
	},

	querySubmittal : function(component){
		var action = component.get("c.querySubmittal");
		action.setParams({"submittalId" : component.get("v.submittal.Id")});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseEvt = response.getReturnValue();
				//console.log("From server: " + JSON.stringify(responseEvt));
				component.set("v.submittal", response.getReturnValue());
                component.set("v.OFCCP", responseEvt.Opportunity.Req_OFCCP_Required__c);
                component.set("v.has_Application", responseEvt.Has_Application__c);
                if(responseEvt.ShipToContact.Related_Contact__c !== null && responseEvt.ShipToContact.Related_Contact__c !== undefined){
                    component.set("v.hasRelatedContact", true);
                    console.log(component.get("v.hasRelatedContact"));
                } 
                this.validateAdditionalSubmittal(component,responseEvt);
			}
			else if (state === "INCOMPLETE") {
				// do something
			}
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + 
									errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
    
    validateAdditionalSubmittal : function (component,responseEvt){
      console.log('Opporunity in Req_Total_Positions__c---->'+responseEvt.Opportunity.Req_Total_Positions__c);
      console.log('Opporunity in Req_Sub_Started__c---->'+responseEvt.Opportunity.Req_Sub_Started__c);
      console.log('Opporunity in Req_Sub_Offer_Accepted__c---->'+responseEvt.Opportunity.Req_Sub_Offer_Accepted__c);
	  console.log('Opporunity in Total_Positions---->'+responseEvt.Opportunity.Req_Sub_Offer_Accepted__c);
      console.log('Opporunity in Total Filled---->'+responseEvt.Opportunity.Req_Total_Filled__c);
      console.log('Opporunity in Total Lost---->'+responseEvt.Opportunity.Req_Total_Lost__c);
       console.log('Opporunity in Total Washed---->'+responseEvt.Opportunity.Req_Total_Washed__c);
      var rTotFill= responseEvt.Opportunity.Req_Total_Filled__c || 0;
      var rTotLost= responseEvt.Opportunity.Req_Total_Lost__c || 0;
      var rTotWash= responseEvt.Opportunity.Req_Total_Washed__c || 0;
      var totalCount=responseEvt.Opportunity.Req_Total_Positions__c	-(rTotFill+rTotLost+rTotWash) ;
      
      console.log('Verify totalCount---->'+totalCount);  
        
      console.log('Opporunity in OpCo__c------>'+responseEvt.Opportunity.OpCo__c);
        
     
      var ismlaApAGS=false; 
      var isaerotekTekEMEA=false;
      var isTotalEq=false;
      if(totalCount==0){
          isTotalEq=true;
          component.set("v.isTotalEq",true);
          if(responseEvt.Opportunity.OpCo__c=='Major, Lindsey & Africa, LLC' || responseEvt.Opportunity.OpCo__c=='Allegis Partners, LLC' || 
             responseEvt.Opportunity.OpCo__c=='Allegis Global Solutions, Inc.' || 
                   responseEvt.Opportunity.OpCo__c=='AG_EMEA'){
              component.set("v.ismlaApAGS",true);
          }else if(responseEvt.Opportunity.OpCo__c=='Aerotek, Inc' || responseEvt.Opportunity.OpCo__c=='TEKsystems, Inc.' ){
              component.set("v.isaerotekTekEMEA",true);
          }      
      }else{
          component.set("v.isTotalEqElse",true);
      }
        
        
    },

	validateAndSaveSubmittal : function (component) {
        if (this.validateSubmittal(component)) {
            this.saveSubmittal(component);
        }
    },

	validateSubmittal : function(component){
		var validForm = true;

		var subSourceCmp = component.find("source");
		if(subSourceCmp){
			var subSourceValue = subSourceCmp.get("v.value");
			if(subSourceValue === undefined || subSourceValue === ""){
				//subSourceCmp.set("v.errors", [{message:"Submittal Source is a required field"}]);
				subSourceCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_Submittal_Source_is_a_required_field")}]);
				validForm = false;
			}
			else {
				subSourceCmp.set("v.errors", null);
			}
		}
		
		var payRateCmp = component.find("payRate");
		if(payRateCmp){
			var payRateValue = payRateCmp.get("v.value");
			if(payRateValue === undefined || payRateValue === ""){
				payRateCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_PAYRATE_IS_REQURIED")}]);
				validForm = false;
			}
			else {
				payRateCmp.set("v.errors", null);
			}
		}
		
		var billRateCmp = component.find("billRate");
		if(billRateCmp){
			var billRateValue = billRateCmp.get("v.value");
			if(billRateValue === undefined || billRateValue === ""){
				billRateCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_BILL_RATE_IS_A_REQUIRED")}]);
				validForm = false;
			}
			else {
				billRateCmp.set("v.errors", null);
			}
		}
		
		var burdenCmp = component.find("burden");
		if(burdenCmp){
			var burdenValue = burdenCmp.get("v.value");
			if(burdenValue === undefined || burdenValue === ""){
				burdenCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_BURDEN_IS_A_REQUIRED_FIELD")}]);
				validForm = false;
			}
			else {
				burdenCmp.set("v.errors", null);
			}
		}
		
		var rateFreqCmp = component.find("rateFreq");
		if(rateFreqCmp){
			var rateFreqValue = rateFreqCmp.get("v.value");
			if(rateFreqValue === undefined || rateFreqValue === ""){
				rateFreqCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_RATE_FREQUENCY_IS_A_REQUIRED_FIELD")}]);
				validForm = false;
			}
			else {
				rateFreqCmp.set("v.errors", null);
			}
		}

		var yearlySalaryCmp = component.find("yearlySalary");
		if(yearlySalaryCmp){
			var yearlySalaryValue = yearlySalaryCmp.get("v.value");
			if(yearlySalaryValue === undefined || yearlySalaryValue === ""){
				yearlySalaryCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_YEARLY_IS_A_REQUIRED_FIELD")}]);
				validForm = false;
			}
			else {
				yearlySalaryCmp.set("v.errors", null);
			}
		}

		var bonusPercentCmp = component.find("bonusPercent");
		if(bonusPercentCmp){
			var bonusPercentValue = bonusPercentCmp.get("v.value");
			if(bonusPercentValue === undefined || bonusPercentValue === ""){
				bonusPercentCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_BONUS_PERCENT_IS_A_REQURIED")}]);
				validForm = false;
			}
			else {
				bonusPercentCmp.set("v.errors", null);
			}
		}

		var addCompCmp = component.find("addComp");
		if(addCompCmp){
			var addCompValue = addCompCmp.get("v.value");
			if(addCompValue === undefined || addCompValue === ""){
				//addCompCmp.set("v.errors", [{message:"Additional Compensation is a required field"}]);
				addCompCmp.set("v.errors", [{message:$A.get("$Label.c.ATS_Additional_Compensation_is_a_required_field")}]);
				validForm = false;
			}
			else {
				addCompCmp.set("v.errors", null);
			}
		}

		var radioLabel = component.get("v.radioLabel");
		var maxPosFill = component.get("v.maxPositionsFilled");
		if(maxPosFill){
			if(radioLabel === undefined || radioLabel === ""){
				var radioWarning = component.find("checkRadioWarning");
				$A.util.removeClass(radioWarning, "toggle");
				validForm = false;
			}
			else {
				var radioWarning = component.find("checkRadioWarning");
				$A.util.addClass(radioWarning, "toggle");
			}
		}
		
		return validForm;

    },

	saveSubmittal : function(component) {
		var spinner = component.find('spinner');
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, 'slds-show');
		console.log("$$$$$");
        console.log(component.get("v.reqOpco"));
        console.log(component.get("v.opptyReqProduct"));
		var opptyReqProduct = component.get("v.opptyReqProduct");
		var opptyReqOpco = component.get("v.reqOpco");
        var updateOtherOrdersStatusToNotProceeding = component.get("v.radioLabel") === "updateSubStages";


		if(opptyReqProduct === 'Contract' && opptyReqOpco !== 'AEROTEK' && opptyReqOpco !== 'TEK'){
			var sub = {"sobjectType" : "Order",
					   "Id" : component.get("v.submittal.Id"),
					   "Status" : 'Offer Accepted',
					   "Submittal_Placement_Source__c" : component.find("source").get("v.value"),
					   "Bill_Rate__c" : component.find("billRate").get("v.value"),
					   "Pay_Rate__c" : component.find("payRate").get("v.value"),
					   "Burden__c" : component.find("burden").get("v.value"),
					   "Rate_Frequency__c" : component.find("rateFreq").get("v.value"),
                       "MovetoNP_Flag__c" : updateOtherOrdersStatusToNotProceeding
					};
		}

		else if(opptyReqProduct === 'Permanent' && opptyReqOpco !== 'AEROTEK' && opptyReqOpco !== 'TEK'){
			var sub = {"sobjectType" : "Order",
					   "Id" : component.get("v.submittal.Id"),
					   "Status" : 'Offer Accepted',
					   "Submittal_Placement_Source__c" : component.find("source").get("v.value"),
					   "Salary__c" : component.find("yearlySalary").get("v.value"),
					   "Bonus__c" : component.find("bonusPercent").get("v.value"),
					   "Additional_Compensation__c" : component.find("addComp").get("v.value"),
                       "MovetoNP_Flag__c" : updateOtherOrdersStatusToNotProceeding
					};
		}
		
		else if(opptyReqProduct === 'Contract to Hire' && opptyReqOpco !== 'AEROTEK' && opptyReqOpco !== 'TEK'){
			var sub = {"sobjectType" : "Order",
					   "Id" : component.get("v.submittal.Id"),
					   "Status" : 'Offer Accepted',
					   "Submittal_Placement_Source__c" : component.find("source").get("v.value"),
					   "Salary__c" : component.find("yearlySalary").get("v.value"),
					   "Bonus__c" : component.find("bonusPercent").get("v.value"),
					   "Additional_Compensation__c" : component.find("addComp").get("v.value"),
					   "Bill_Rate__c" : component.find("billRate").get("v.value"),
					   "Pay_Rate__c" : component.find("payRate").get("v.value"),
					   "Burden__c" : component.find("burden").get("v.value"),
					   "Rate_Frequency__c" : component.find("rateFreq").get("v.value"),
                       "MovetoNP_Flag__c" : updateOtherOrdersStatusToNotProceeding
					};
		}

		else if(opptyReqOpco === 'AEROTEK' || opptyReqOpco === 'TEK'){
			var sub = {"sobjectType" : "Order",
					   "Id" : component.get("v.submittal.Id"),
					   "Status" : 'Offer Accepted',
                       "MovetoNP_Flag__c" : updateOtherOrdersStatusToNotProceeding
					  };
		}
				

		var action = component.get("c.saveSubmittalOfferAccepted");
        action.setParams({ "subOffAcctd" : JSON.stringify(sub), "radioLabel" : component.get("v.radioLabel") === "updateSubStages",
						   "submittalId" : component.get("v.submittal.Id"), "opptyId" : component.get("v.submittal.Opportunity.Id"),
                          "clientCont" : component.get("v.createClientContact") , "contId" : component.get("v.submittal.ShipToContactId")});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log("From server: " + response.getReturnValue());
				var agPortalURL = $A.get("$Label.c.AG_Portal_URL");
				if(component.get("v.submittal.Opportunity.Req_OpCo_Code__c") === "AG_EMEA"){
					var urlEvent = $A.get("e.force:navigateToURL");
					urlEvent.setParams({
					"url": agPortalURL + "?SubmittalId="+ response.getReturnValue()
					});
					urlEvent.fire();
				}
			}
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
			$A.util.removeClass(spinner, 'slds-show');
			$A.util.addClass(spinner, 'slds-hide');
			this.fireSavedEvt(component);
           
            //S-78898 -  To Refresh Activity Timeline
			var reloadEvt = $A.get("e.c:E_TalentActivityReload");
            reloadEvt.fire();
        });

        $A.enqueueAction(action);
	},
    increaseTotalPositions : function(component){
        console.log('Inside increase TOT POS');
        var action= component.get("c.updateTotalPositions");
        action.setParams({"opId" :component.get("v.submittal.Opportunity.Id")});
    	action.setCallback(this, function(response) {
            var state = response.getState();
             if (state === "SUCCESS") {
                // this.showSuccess('You\'ve successfully added the submittal at Offer Accepted status, and an email updating the number of positions has been sent to the Req Opportunity Owner.');
				 this.showSuccess($A.get("$Label.c.ATS_Submittal_Offer_Accepted_Successful_Msg"));
             	 component.set("v.isaerotekTekEMEA","false");
                 component.set("v.isTotalEqElse","false");
                 component.set("v.ismlaApAGS","false");
                this.fireSavedEvt(component);
             }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });    
         $A.enqueueAction(action);
    },
    showSuccess : function(messageToDisplay){
       
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'sticky',
            title: 'Confirmation',
            message: messageToDisplay,
            duration:' 5000',
            type: 'success'
        });
        toastEvent.fire();
    },
     showError : function(component, message, messageType, theTitle){
        var title = theTitle;
        var errorMessage = message;
        var theType = messageType;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: theType
        });
        toastEvent.fire();
    }
})