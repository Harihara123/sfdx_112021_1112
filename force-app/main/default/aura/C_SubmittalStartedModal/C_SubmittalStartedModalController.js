({
	cancelChange: function(component, event, helper) {
		helper.fireCanceledEvt(component);
	},

	saveChange: function(component, event, helper) {
        helper.hideSpinner(component);
        helper.fireSavedEvt(component); 
	},

	submitChange: function(component, event, helper) {
		helper.showSpinner(component);
        var fieldStartDate = component.find("FieldStartDate").get("v.value");
        var errorField = component.find("errorCondition");
        if(fieldStartDate === '' || fieldStartDate === null){
            component.set("v.dateErrorMessage",$A.get("$Label.c.CRM_Date_Required"));
            $A.util.removeClass(errorField, 'slds-hide');
            helper.hideSpinner(component);
         }else{
            var today = new Date();
            var startDate = new Date(fieldStartDate);
            if(startDate <= today){
               component.find("startDateEditForm").submit(); 
              // helper.fireSavedEvt(component); 
            }else{
               component.set("v.dateErrorMessage",$A.get("$Label.c.CRM_Select_Valid_Date"));
               $A.util.removeClass(errorField, 'slds-hide'); 
               helper.hideSpinner(component);
            }
        }
	}
})