({
	doInit: function(component, event, helper) {
		helper.setModalLabels(component)
	},

	cancelChange: function(component, event, helper) {
		helper.fireCanceledEvt(component);
	},

	saveChange: function(component, event, helper) {
		helper.hideSpinner(component);
		helper.fireSavedEvt(component);
	},

	submitChange: function(component, event, helper) {
		helper.showSpinner(component);
		var s = component.get("v.status");
		if (s === "Remove Link") {
			helper.removeLink(component);
		} else if (s === "Remove Applicant") {
			helper.removeApplicant(component);
		}
	}
})