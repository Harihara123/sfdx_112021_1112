({
	fireCanceledEvt : function(component) {
		var evt = component.getEvent("statusCanceledEvt");
		evt.fire();
	},

	fireSavedEvt : function(component) {
		var evt = $A.get("e.c:E_SubmittalStatusChangeSaved");
        evt.setParam("contactId", component.get("v.submittal.ShipToContactId"));
		evt.fire();
	},

	showSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},

	hideSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "slds-show");
		$A.util.addClass(spinner, "slds-hide");
	},

	removeLink : function(component) {
		var action = component.get("c.removeLink");
		action.setParam("submittalId", component.get("v.submittalId"));
		action.setCallback(this, function(response) {
			this.hideSpinner(component);
			if (response.getState() === "SUCCESS") {
				this.fireSavedEvt(component);
			} else {
				console.log("ERROR removing Link");
			}
		});
		$A.enqueueAction(action);
	},

	removeApplicant : function(component) {
		var action = component.get("c.removeApplicant");
		action.setParam("submittalId", component.get("v.submittalId"));
		action.setCallback(this, function(response) {
			this.hideSpinner(component);
			if (response.getState() === "SUCCESS") {
				this.fireSavedEvt(component);
			} else {
				console.log("ERROR removing Applicant");
			}
		});
		$A.enqueueAction(action);
	},

	setModalLabels : function(component) {
		var status = component.get("v.status");
		var h, m, c, s;

		var validateMsg = this.validate(component, status);

		if (validateMsg === true) {
			switch (status) {
				case "Remove Link" : 
					h = $A.get("$Label.c.ATS_CONFIRM_REMOVE_LINK_HEADER");
					m = $A.get("$Label.c.ATS_CONFIRM_REMOVE_LINK_MESSAGE");
					c = $A.get("$Label.c.ATS_BUTTON_CANCEL");
					s = $A.get("$Label.c.ATS_REMOVE_LINK");
					break;

				case "Remove Applicant" : 
					h = $A.get("$Label.c.ATS_CONFIRM_REMOVE_APPLICANT_HEADER");
					m = $A.get("$Label.c.ATS_CONFIRM_REMOVE_APPLICANT_MESSAGE");
					c = $A.get("$Label.c.ATS_BUTTON_CANCEL");
					s = $A.get("$Label.c.ATS_REMOVE_APPLICANT");
					break;

				default : 
					h = "Confirm Action";
					m = "Please confirm you want to continue";
					c = $A.get("$Label.c.ATS_BUTTON_CANCEL");
					s = $A.get("$Label.c.ATS_SAVE");
			}
			component.set("v.isValid", true);
		} else {
			h = $A.get("$Label.c.ATS_WARNING");
			m = validateMsg;
			c = $A.get("$Label.c.ATS_OK");
			component.set("v.isValid", false);
		}

		component.set("v.header", h);
		component.set("v.message", m);
		component.set("v.cancelLabel", c);
		component.set("v.saveLabel", s);
	},

	validate : function(component, status) {
		var usr = component.get("v.runningUser");
		var subm = component.get("v.submittal");
		if (usr.Profile.Name !== "System Administrator" && usr.Id !== subm.CreatedById) {
			return $A.get("$Label.c.ATS_RemoveError");
		} else {
			return true;
		}
	}
})