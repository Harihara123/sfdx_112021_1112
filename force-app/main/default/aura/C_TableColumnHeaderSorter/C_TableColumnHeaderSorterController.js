({
	applySort : function(component, event, helper) {
		// console.log("apply the sort ... ");
		var sortAsc = component.get("v.sortAsc");
		var sortEvt = component.getEvent("sortEvt");
		sortEvt.setParams({
			"sortField" : component.get("v.field"),
			"sortAsc" : sortAsc
		});

		sortEvt.fire();
		component.set("v.sortAsc", !sortAsc);
		// component.set("v.isActive", true);
	}
})