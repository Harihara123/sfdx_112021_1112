({
    toggleModalHeight: function(component, event, helper) {
        var expand = event.getParam('expand');
        var modalClass = component.get('v.containerClass');

        if(expand) {
            modalClass = 'slds-modal__container expandModal';
        } else {
            modalClass = 'slds-modal__container';
        }

        component.set('v.containerClass', modalClass);

    },
    
	displayAddTaskDetail : function(component, event, helper) {
        var activityType = component.get("v.activityType");//ak changes = event.getParam("activityType");
		var itemID = component.get("v.itemID");
        //ak changes component.set("v.recordId", event.getParam("recordId"));
		var flag = true;
        
        /* Updated By Rohit Maharashi for User Story S-181487
         * - Make "Log a Call" custom component JAWS compatible.
         * - Code to set tab focus to close button of Log a Call popup. 
       */
        const modalCloseButton = component.find("modalCloseButton");
        window.setTimeout(
            $A.getCallback(function() {
               // wait for element to render then focus
               modalCloseButton.focus();
            }), 100
        );                
        
        helper.getItemToAddEdit(component,event, itemID, activityType,helper, flag);

		if(itemID !== "0"){
			if(activityType === "event"){
				component.set("v.modalTitle", $A.get("$Label.c.ATS_EDIT_EVENT"));//"Edit Event");
			}else if(activityType==="task"){
	    		component.set("v.modalTitle", $A.get("$Label.c.ATS_EDIT_TASK"));//"Edit Task");
			}
		}else{
			if(activityType === "event"){
			component.set("v.modalTitle", $A.get("$Label.c.ATS_ADD_EVENT"));//"Add Event");
			}else if(activityType==="task"){
	    		component.set("v.modalTitle", $A.get("$Label.c.ATS_ADD_TASK"));//"Add Task");
			}else if(activityType==="call"){
	    		component.set("v.modalTitle", $A.get("$Label.c.ATS_LOG_A_CALL"));//"Log a Call");
			}
		}
      
	},
    reloadAddTaskDetail : function(component, event, helper) {// S - 32431 - Added by akshay on 10/10/17 
        //S-32433 - Added by Karthik
        var activityType = event.getParam("activity").sobjectType;
        var flag= true;        
        // S-100053 - Added by Joe // S-232220 - Added 'allocatedwidget' by Alex
        if(component.get("v.eventOrigin") === "search" || component.get("v.eventOrigin") === "allocatedwidget"){
            activityType = "Call";
        }       
        if(activityType === "Task"){

            component.set("v.modalTitle", "Add Task");
        	helper.getItemToAddEdit(component,event, "0", "task", helper, flag);

        } else if (activityType === "Event") {

            component.set("v.modalTitle", "Add Event");
        	helper.getItemToAddEdit(component,event,"0","event", helper, flag);

        } else if (activityType === "Call") {

            component.set("v.modalTitle", "Add Call");
            helper.getItemToAddEdit(component,event, "0", "call", helper, flag);

        }
    },
    displayEditTaskDetail : function(component,event,helper){
        
		var activityType = event.getParam("itemType");
		var itemID = event.getParam("itemID");
		helper.getItemToAddEdit(component,event, itemID, activityType,helper);

		if(activityType === "event"){
			component.set("v.modalTitle", "Edit Event");
		}else if(activityType==="task"){
	    	component.set("v.modalTitle", "Edit Task");
		}

	},
	hideModal : function(component, event, helper){
	    component.destroy();
        //Toggle CSS styles for hiding Modal
		 /* ak changes helper.toggleClassInverse(component,'backdropAddTaskDetail','slds-backdrop--');
		 helper.toggleClassInverse(component,'modaldialogAddTaskDetail','slds-fade-in-');*/

    },
    saved : function(component,event,helper){
        var type = event.getParam("buttonClick");// S - 32431 - Added by akshay on 10/10/17
        helper.toggleClassInverse(component,'backdropAddTaskDetail','slds-backdrop--');
        helper.toggleClassInverse(component,'modaldialogAddTaskDetail','slds-fade-in-');
         
        if(type !== null && type !== 'undefined' && type == 'btnSubmitNew'){//added if block for  S - 32431 - Added by akshay on 10/10/17 - start
            //show modal again            
            var saveAndNewEvt = component.getEvent("reloadEvt");
            saveAndNewEvt.setParams({activity : component.get("v.body")[0].get("v.task"),
                                     recordId : component.get("v.body")[0].get("v.task").AccountId});
            saveAndNewEvt.fire();
        }
        // S - 32431 - Added by akshay on 10/10/17 - end

        //S-32433 - Added by Karthik
        if(type !== null && type !== 'undefined' && type == 'btnSubmitSaveNew'){
        	//console.log('eventbody'+JSON.stringify(component.get("v.body")[0].get("v.theevent")));
            var saveAndNewEvt = component.getEvent("reloadEvt");
            saveAndNewEvt.setParams({activity : component.get("v.body")[0].get("v.theevent"),
                                     recordId : component.get("v.body")[0].get("v.theevent").AccountId});
            saveAndNewEvt.fire();
        } 
        //S-32433 - Added by Karthik

        // S-100051 - Added by Joe - checks to see if the event is fired from search or talent landing page
        if(component.get("v.eventOrigin") !== "search"){
            try {
           var reloadEvt = $A.get("e.c:E_TalentActivityReload");
           reloadEvt.fire();
        } 
            catch(exception) {

            }
        } 
        
    }, 
    canceled: function(component, event, helper){
        var userevent = $A.get("e.c:E_TalentActivityCancel");

        if(userevent){
            userevent.fire();
        }

		 helper.focusSource(component);
    },
    
    /* 
     * Updated By Rohit Maharashi for User Story #S-181487 
     * 	- Make "Log A Call" component JAWS complaiant 
     * 	- Function to let tab focus cycle through the popup
    */
    focusOnClose: function(component, event, helper) {
        const modalCloseButton = component.find("modalCloseButton");
        window.setTimeout(
            $A.getCallback(function() {
               // wait for element to render then focus
               modalCloseButton.focus();
            }), 100
        );
    },

	// Monika -- S-182912 -- Reverse tabbing from Close to Save button for "Log a call" modal
	focusSaveBtnFromClose:function(component, event, helper) {
        if (event.shiftKey && event.keyCode == 9) {
			event.preventDefault();
			var cmpEventFromClose = $A.get('e.c:E_NavigateBackFromCloseToSave');
			cmpEventFromClose.setParams({"fieldIdToGetFocus":"btnSubmit"});
			cmpEventFromClose.fire();
        }
		
    }
})