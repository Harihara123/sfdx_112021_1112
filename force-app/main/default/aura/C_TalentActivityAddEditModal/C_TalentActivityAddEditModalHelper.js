({
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
		$A.util.removeClass(modal,className+'hide');
		$A.util.addClass(modal,className+'open');
	}
    ,toggleClassInverse: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.addClass(modal,className+'hide');
		$A.util.removeClass(modal,className+'open');
	}
    ,getItemToAddEdit:function(component,event, itemID, activityType,helper,flag){
        this.toggleClass(component,'backdropAddTaskDetail','slds-backdrop--');
        var actionName = "";
        var params = '';
        //console.log(component.get("v.recordId"));//+' '+event.getParam("recordId")
        // var recordId = event.getParam("recordId");//component.get("v.recordId");

        //Changed how recordId retried under for S-57312
        var recordId = component.get("v.recordId");
        let notMyActNewEventOrTask = component.get("v.notMyActivityNewEventOrTask");
		let whoId = component.get("v.whoId");
        if(activityType === "task"){
            if(itemID === "0"){
				if(notMyActNewEventOrTask) {
					actionName = "getNewAccountTask";                
					params = {"recordId" : recordId};
				}
				else {
					actionName = "getNewContactTask";
					params = {"recordId" : recordId, "contactId": whoId};  
				}
				
            }else{
                actionName = "getAccountTask";   
                params = {"itemID" : itemID};                
            }
        }else if(activityType === "event"){
            if(itemID === "0"){
				if(notMyActNewEventOrTask) {
					actionName = "getNewAccountEvent"; 
					params = {"recordId" : recordId};                 
				}
				else {
					actionName = "getNewContactEvent";
					params = {"recordId" : recordId, "contactId": whoId};  
				}
                
            }else{
                actionName = "getAccountEvent";
                params = {"itemID" : itemID};  
            }
        }else if(activityType === "call"){
            if(itemID === "0"){
                actionName = "getNewAccountTask";
                params = {"recordId" : recordId};  
            }else{
                actionName = "getNewAccountTask";
                params = {"itemID" : itemID};  
            }
        }
        var self = this;
        var bdata = component.find("bdhelper");
            bdata.callServer(component,'ATS','TalentActivityFunctions',actionName,function(response){
            if(component.isValid()){
             var me = self;

                var newParams = {};
                var componentName = '';
                var task = {};
                var theevent1 = {};
                var dFormat = $A.get("$Locale.dateFormat");

                if(activityType === "task"){
                    task = response[0];
                    task.sobjectType='Task';
                    task.attributes=null;
                    task.Owner = { 'sobjectType' : 'User','Id' : task.OwnerId };
                    //added by akshay for S-48141 10/23/17 start
                    task.AccountId = recordId;
                    if(typeof task.WhatId !== "undefined" && task.WhatId.startsWith("001") ){
                       delete task.What.Name; 
                    }//added by akshay for S-48141 10/23/17 end
                     // S - 32431 - Added by akshay on 10/10/17 - start
                    if( typeof event.getParam("activity") !== "undefined"){
                        var reload = event.getParam("activity");
                       task.Type = reload.Type;
                       task.Subject = reload.Subject;
                       task.Status = reload.Status;
                       task.Priority = reload.Priority;
                       
                    }
					
					/*if(flag==true){
                        task.Priority = 'Normal';
                        task.Status = 'Not Started';
                    }*/
                     // S - 32431 - Added by akshay on 10/10/17 - end
					// S-91925 07/19/18 - Neel- commented out 'if' block code.
                    /*if(itemID === 0){
                        delete task.ActivityDate;
                    }*/

                    //var newTask = {'sobjectType':'Task','OwnerId':task.Owner.Id}; //Sandeep:  this line
                    //overrides above object instantiation.
                    newParams = {task: task, runningUser: response[1], dateFormat: dFormat, isNew: flag, notMyActivityNewTask:component.get("v.notMyActivityNewEventOrTask")};
                    componentName = 'c:C_TalentActivityTaskForm';  
                    
                }
                else if(activityType === "event"){
                    theevent1 = response[0];
                    theevent1.sobjectType='Event';
                    theevent1.attributes=null;
                    if(itemID === "0"){
                    	theevent1.Owner = { 'sobjectType' : 'User','Id' : theevent1.OwnerId };
                    }else{
                        theevent1.Owner.sobjectType='User'; 
                    }
                    //added by akshay for S-48141 10/23/17 start 
                    theevent1.AccountId = recordId;
                        if(typeof theevent1.WhatId !== "undefined" && theevent1.WhatId.startsWith("001") ){
                       delete theevent1.What.Name; 
                    }//added by akshay for S-48141 10/23/17 end
                    //S-32433 - Added by Karthik
                    if(typeof event.getParam("activity") !== "undefined"){

                        var reload = event.getParam("activity");
                       
                        theevent1.Subject = reload.Subject;
                        theevent1.Type = reload.Type;
                    }
                    //S-32433 - Added by Karthik
                    var newEvent1 = {'sobjectType':'Event','OwnerId':theevent1.Owner.Id,'Owner':theevent1.Owner,'StartDateTime':theevent1.StartDateTime, 'Type': theevent1.Type, 'AccountId': theevent1.AccountId};
                    // S-81179 - Added by Pranav
					//S-91932 - Added by Rajeesh - added isNew flag to default type with subject
                    //newParams = {theevent: newEvent, runningUser: response[1], dateFormat: dFormat , OpportunityName:response[2]};
					newParams = {theevent: newEvent1, runningUser: response[1], dateFormat: dFormat , OpportunityName:response[2],isNew:flag,typeValue : theevent1.Type, notMyActivityNewEvent:component.get('v.notMyActivityNewEventOrTask')};
                   // newParams = {theevent: newEvent, runningUser: response[1], dateFormat: dFormat};
                    componentName = 'c:C_TalentActivityEventForm';
                    
                }
                else if(activityType === "call"){
                    task = response[0];
                    task.sobjectType='Task';
                    task.attributes=null;
                    task.Owner = { 'sobjectType' : 'User','Id' : task.OwnerId };
                    //added by akshay for S-48141 10/23/17 start
                     task.AccountId = recordId;
                    if(typeof task.WhatId !== "undefined" && task.WhatId.startsWith("001") ){
                       delete task.What.Name; 
                    }//added by akshay for S-48141 10/23/17 end
                    task.Type = 'Attempted Contact';
                    task.Priority = 'Normal';
                    task.Subject = 'Call';
                    task.Status = 'Completed';
                    var newTask = {'sobjectType':'Task','OwnerId':task.Owner.Id};
                    //sandeep: Adding task type param.
                    newParams = {task: newTask, runningUser: response[1], dateFormat: dFormat, OpportunityName: response[2],taskType : 'call', notMyActivityNewTask:component.get("v.notMyActivityNewEventOrTask")};
                    componentName = 'c:C_TalentActivityTaskForm';
                }
                
                $A.createComponent(componentName,newParams,
                    function(newComponent, status, errorMessage){
                        if(newComponent.isValid() ){
                            //Add the new button to the body array
                            if (status === "SUCCESS") {
                                console.log('Component Created');
                                if(activityType === "task"){
                                    newComponent.set("v.task", task);
                                }
                                else if(activityType === "event"){
                                    newComponent.set("v.theevent", theevent1);
                                }else if(activityType === "call"){
                                    newComponent.set("v.task", task);
                                }
                                
                				component.set("v.body", newComponent); 

                              
                                me.toggleClass(component,'modaldialogAddTaskDetail','slds-fade-in-');  
                                me.toggleClass(component,'backdropAddTaskDetail','slds-backdrop--');             
                            }
                            else if (status === "INCOMPLETE") {
                                console.log("No response from server or client is offline.")
                                // Show offline error
                            }
                            else if (status === "ERROR") {
                                console.log("Error: " + errorMessage);
                                // Show error message
                            }
                        }
                        
                    }
                );
            }
         },params,false);   
    },

	focusSource : function(component){
		var fieldToGetFocus = "logACallBtn";
		if(fieldToGetFocus=="" || fieldToGetFocus == null) return;
		var cmpEventFromClose = $A.get('e.c:E_FocusField');
		cmpEventFromClose.setParams({"fieldIdToGetFocus":fieldToGetFocus});
		cmpEventFromClose.fire();
  }
})