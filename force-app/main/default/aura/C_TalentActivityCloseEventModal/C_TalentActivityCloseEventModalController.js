({
    displayCTDetail: function(component, event, helper) {
       /*ak changes component.set("v.recordId", event.getParam("recordId"));
        component.set("v.Enddate",event.getParam("Enddate") );
        component.set("v.PostNotes", event.getParam("PostNotes"));
        document.getElementById("errorDivId").style.display = "none";*/
        helper.toggleClass(component,'backGroundSectionId','slds-backdrop--');
		helper.toggleClass(component,'newERSectionId','slds-fade-in-');
                
    },
        
     closeModal: function(component, event, helper) {
		component.destroy();
       /* helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop--');
		helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-'); */
    },
    validateData: function(component, event, helper) {
        var validForm = true;
        var fields=["PostNotesInput"];
        var field = "";
        // console.log(fields);
        for (var i=0; i<fields.length; i++) {
            field=component.find(fields[i]);

            var value = field.get("v.value");
            var required = field.get("v.required");

            /*console.log(value);
            console.log(required);*/
            if(value == undefined){
                value="";
            }
        
            var validity=field.get('v.validity');
            if(validity){
                if (typeof validity == 'object') {
                                if (validity.badInput || 
                                    validity.patternMismatch || 
                                    validity.rangeOverflow || 
                                    validity.rangeUnderflow ||
                                    validity.stepMismatch || 
                                    validity.tooLong || 
                                    validity.typeMismatch || 
                                    validity.valueMissing) {
                                    
                                    validForm = false;
                                }
                            }
            }
            
            if(required && value === ""){
                validForm = false;
            }
            
        }
        // console.log(validForm);
        if(validForm){
            component.find("btnSubmit").set("v.disabled",false);
        }else{
            component.find("btnSubmit").set("v.disabled",true);
        }
    },
    saveCE: function(component, event, helper) {
        document.getElementById("errorDivId").style.display = "none";
        var params = {post: component.get("v.PostNotes"),
                      recId: component.get("v.recordId")};

        var bdata = component.find("bdhelper");
            bdata.callServer(component,'ATS','TalentActivityFunctions','saveEvent',function(response){
            var userevent = component.getEvent('activitySavedEvent');
            userevent.fire();
            var reloadEvt = $A.get("e.c:E_TalentActivityReload");
            reloadEvt.fire();
            var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
            updateLMD.fire();
            helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop--');
            helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
        },params,false);   
    }
})