({
    displayCTDetail: function(component, event, helper) {
        /*ak changes component.set("v.recordId", event.getParam("recordId"));
        component.set("v.Status",event.getParam("Status") );
        component.set("v.Description", event.getParam("Description"));
        document.getElementById("errorDivId").style.display = "none";*/
        helper.toggleClass(component,'backGroundSectionId','slds-backdrop--');
		helper.toggleClass(component,'newERSectionId','slds-fade-in-');
        helper.popStatus(component, component.get("v.recordId")); 
    },
        
     closeModal: function(component, event, helper) {
	    component.destroy();/*ak changes
        helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop--');
		helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');*/
    },
    validateData: function(component, event, helper) {
        var validForm = true;
        var fields=["Description"];
        var field = "";

        for (var i=0; i<fields.length; i++) {
            field=component.find(fields[i]);


            var value = field.get("v.value");
            var required = field.get("v.required");

            if(value == undefined){
                value="";
            }
        
            var validity=field.get('v.validity');
            if(validity){
                if (typeof validity == 'object') {
                                if (validity.badInput || 
                                    validity.patternMismatch || 
                                    validity.rangeOverflow || 
                                    validity.rangeUnderflow ||
                                    validity.stepMismatch || 
                                    validity.tooLong || 
                                    validity.typeMismatch || 
                                    validity.valueMissing) {
                                    
                                    validForm = false;
                                }
                            }
            }
            // console.log(value);
            if(required && value === ""){
                validForm = false;
            }
            
        }

        if(validForm){
            component.find("btnSubmit").set("v.disabled",false);
        }else{
            component.find("btnSubmit").set("v.disabled",true);
        }
    },      
    
     saveER: function(component, event, helper) {
        document.getElementById("errorDivId").style.display = "none";

        var bdata = component.find("basedatahelper")
        
        var params = {sc: component.get("v.Status"),
                      des: component.get("v.Description"),
                      recId: component.get("v.recordId")};
        // helper.callServer(component,'ATS','TalentActivityFunctions','saveTask',function(response){
        bdata.callServer(component,'ATS','TalentActivityFunctions','saveTask',function(response){
            var userevent = component.getEvent('activitySavedEvent');
            userevent.fire();
            var reloadEvt = $A.get("e.c:E_TalentActivityReload");
            reloadEvt.fire();
            var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
            updateLMD.fire();
            helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop--');
            helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
        },params,false);   
    }
    
})