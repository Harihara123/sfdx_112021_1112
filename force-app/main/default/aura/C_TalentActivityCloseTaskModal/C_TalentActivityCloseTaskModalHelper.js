({
	toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
		$A.util.removeClass(modal,className+'hide');
		$A.util.addClass(modal,className+'open');
	},
    toggleClassInverse: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.addClass(modal,className+'hide');
		$A.util.removeClass(modal,className+'open');
	},
    popStatus: function(component){
        component.set("v.statusList", null);

        var params = null;
        var bdata = component.find("bdhelper");
            bdata.callServer(component,'ATS','TalentActivityFunctions','getTaskStatusList',function(response){
            var status = component.get("v.Status");
            var scopeType = [];
            var scopeTypeMap = response;
            for (var key in scopeTypeMap ) {
                scopeType.push({value:scopeTypeMap[key], key:key});
            }
            component.set("v.statusList", scopeType);
        },params,false);   
    }
})