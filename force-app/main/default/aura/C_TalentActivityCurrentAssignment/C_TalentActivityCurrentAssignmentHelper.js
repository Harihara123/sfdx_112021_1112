({
	getCurrentAssignment: function (component) {
        var talentId = component.get("v.talentId");

        var action = component.get("c.getCurrentTalentAssignment");
        action.setParam("talentId", talentId);

        action.setCallback(this, function(response) {
            var state = response.getState();

            if(state === "SUCCESS") {
                component.set("v.assignment", response.getReturnValue());
                component.set("v.loading", false);
            } else {
                component.set("v.loading", false);
            }
        });

        $A.enqueueAction(action);
	}
})