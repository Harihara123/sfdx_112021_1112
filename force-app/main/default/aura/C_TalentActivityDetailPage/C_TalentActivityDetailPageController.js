({
   /* FROM BaseCardRelatedList for Change S-57312 */
     loadData: function(cmp, event, helper) {
        cmp.set("v.loading",true);
        cmp.set("v.records",[]);
        cmp.set("v.recordCount",0);
        helper.getResults(cmp,helper);
    }
    ,doInit: function(cmp, event, helper) {
        //Added following for Change S-57312
        var contact = cmp.get("v.contactRecord");
        cmp.set("v.talentId", contact.fields.AccountId.value);
        helper.getResults(cmp,helper);  
    }
    ,updateLoading : function(cmp,event,helper){
        helper.updateLoading(cmp);
    }
    ,reloadData : function(cmp,event,helper) {
        var rd = cmp.get("v.reloadData");
        // if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);

            helper.getResults(cmp,helper);
            cmp.set("v.reloadData", false);
        // }

    },
    
    shouldLoadMore : function(component, event, helper) {
        
        var childParamValue= event.getParam("shouldLoadMore");
        component.set ("v.displayLoadMore", childParamValue);
        
    },

    onSelectPresentChange : function(component, event, helper) {
        var spinner = component.find('spinnerLoadMore');
        $A.util.addClass(spinner, 'slds-show'); 

        var selected = parseInt(component.find("loadMorePresentDropdown").get("v.value")) + parseInt(component.get ("v.presentActivityCount"));   
        component.set ("v.presentActivityCount", selected);
        component.set("v.loadMorePresent",[0,5,10,25,50]);
        component.set("v.loadMorePresent",["Select","5","10","25","50"]);

        setTimeout(function() {
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
        }, 1500);

    },
    showMoreNextActivities: function(component, event, helper) {
	   helper.showSpinner(component,"spinnerPastLoadMore");
       //var action = component.get();
	   // action.setCallback(this,function(response){
        var selected = parseInt(component.get ("v.moreRecordsSize")) + parseInt(component.get ("v.presentActivityCount"));   
        component.set ("v.presentActivityCount", selected);
        component.set("v.loadMorePresent",[0,5,10,25,50]);
        component.set("v.loadMorePresent",["Select","5","10","25","50"]);
		setTimeout(function() {
           helper.hideSpinner(component,"spinnerPastLoadMore");
        }, 1500);
    },

   onSelectPastChange : function(component, event, helper) {

        var spinner = component.find('spinnerLoadMore');
        $A.util.addClass(spinner, 'slds-show'); 

        var selected = parseInt(component.find("loadMorePastDropdown").get("v.value")) + parseInt(component.get ("v.pastActivityCount"));   
        component.set ("v.pastActivityCount", selected);
        component.set("v.loadMorePast",[0,5,10,25,50]);
        component.set("v.loadMorePast",["Select","5","10","25","50"]);

        setTimeout(function() {
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
        }, 1500);
    },
    showMorePastctivities : function(component, event, helper) {

        helper.showSpinner(component,"spinnerPastLoadMore");

        var selected =  parseInt(component.get ("v.moreRecordsSize")) + parseInt(component.get ("v.pastActivityCount"));   
        component.set ("v.pastActivityCount", selected);
        component.set("v.loadMorePast",[0,5,10,25,50]);
        component.set("v.loadMorePast",["Select","5","10","25","50"]);
        
        setTimeout(function() {
           helper.hideSpinner(component,"spinnerPastLoadMore");
        }, 1500);
    },
    /* END FROM BaseCardRelatedList S-57312*/

    sendParametersToParent : function(component, event, helper) {
        component.set("v.filterCriteria", "");
        var parametersEvent = component.getEvent("talentActivityLoadMore");
        parametersEvent.setParams({
            "shouldLoadMore" : true
        });
        // Fire the event
        parametersEvent.fire();
	},
    setViewList : function(component, event, helper){
       // console.log('Load method 2');
        var viewList =  component.get("v.viewList");
        if (viewList && viewList["0"] && viewList["0"].params && viewList["0"].params.maxItems) {
            viewList["0"].params.maxItems = component.get("v.recordCounter");
            component.set ("v.viewList", viewList);
        }
        
        //component.set("v.reloadData",true);
    },
    
	setViewListFilter : function(component, event, helper){
        var viewList =  component.get("v.viewList");
        viewList["0"].params.filterCriteria = component.get("v.filterCriteria");
        component.set ("v.viewList", viewList);

        helper.getResults(component, helper); 
       
        var parametersEvent = component.getEvent("talentActivitySpinner");
        parametersEvent.fire();
        
    },
    showMoreActivities : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//Neel summer 18 URL change-> "url": "/one/one.app#/n/Activity_Details?recordId=" + component.get('v.recordId'),
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Activity_Details?recordId=" + component.get('v.recordId'),
            "isredirect":true
        });
        urlEvent.fire(); 
    },
    showActivities : function(cmp, event, helper){
        // console.log('Load method 4');
        var recordID = cmp.get("v.recordId");
        var recordCounter = cmp.get("v.recordCounter");
        var params = {"recordId": recordID, "recordCounter": recordCounter};
		
        helper.callServer(cmp,'ATS','','getTalentContactId',function(response){
            var contactId  = response;
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/apex/c__CandidateActivities?accountId=" + recordID + "&contactid=" + contactId
            });
            urlEvent.fire(); 
        },params,false);
        
    },
     
    showMoreDocuments : function(component, event, helper) {
		// console.log('Load method 5');
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//Neel summer 18 URL change->"url": "/one/one.app#/n/Activity_Details?recordId=" + component.get('v.recordId'),
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Activity_Details?recordId=" + component.get('v.recordId'),
            "isredirect":true
        });
        urlEvent.fire(); 
    },    
    updateLoading: function(cmp, event, helper) {
            if(!cmp.get("v.loadingData")) {
                cmp.set("v.loading",false);
            }
    },
    addCandidateTask:function(cmp,event,helper){
        
        var certEvent = $A.get("e.c:E_TalentActivityAddTask");
        // var recordID = cmp.get("v.recordId");
        var recordID = cmp.get("v.talentId");
        certEvent.setParams({"recordId":recordID, activityType:"task"});
        certEvent.fire();
    }, 
    addCandidateCall:function(cmp,event,helper){
        var userevent = $A.get("e.c:E_TalentActivityAddTask");
        // var recordID = cmp.get("v.recordId");
        var recordID = cmp.get("v.talentId");
        userevent.setParams({"recordId":recordID, activityType:"call" })
        userevent.fire();
    },
    addCandidateEvent:function(cmp,event,helper){
        // console.log('In Event');
        var userevent = $A.get("e.c:E_TalentActivityAddTask");
        // var recordID = cmp.get("v.recordId");
        var recordID = cmp.get("v.talentId");
        userevent.setParams({activityType:"event","recordId":recordID }) 
        userevent.fire();
    }

    ,deleteItem : function(cmp,event,helper){
        var recordId = event.getParam("recordId");
        var params = {"recordId": recordId};

        helper.callServer(cmp,'','','deleteRecord',function(response){
            cmp.set("v.reloadData",true);

            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "message": "Activity deleted!",
                "type": "success"
            });
            toastEvent.fire();
        },params,false);   
    },
    
    deleteActivity : function(component, event, helper) {
        if (event.getParam('confirmationVal') === 'Y') {
            var recordID = component.get("v.recordId");
            var talentDocumentId = component.get("v.talentDocumentId");
            // console.log(talentDocumentId);
            helper.deleteTheActivity(component, recordID,talentDocumentId,helper);
        }
    },
    
     loadMoreData : function(component, event, helper) {        
        // console.log('Load method 190');
         var recordCounter = component.get("v.recordCounter");
        recordCounter = recordCounter + 1;
        component.set("v.recordCounter", recordCounter);
        //component.set("v.reloadData",true);
    },
        expandPastActivity : function(component, event, helper) {
        helper.expandCollapseHelper(component, "expandPastActivity", true, "v.pastActivityExpanded");
    },
    collapsePastActivity: function(component, event, helper) {
        helper.expandCollapseHelper(component, "expandPastActivity", false, "v.pastActivityExpanded");
    },
    expandNextSteps : function(component, event, helper) {
        helper.expandCollapseHelper(component, "expandNextSteps", true, "v.nextStepsExpanded");
    },
    collapseNextSteps: function(component, event, helper) {
        helper.expandCollapseHelper(component, "expandNextSteps", false, "v.nextStepsExpanded");
    },
    
})