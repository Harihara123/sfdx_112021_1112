({
	
    loadTalent : function(component, recordId, helper) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c.getRecords");
        action.setParams({ recordId : component.get("v.recordId")});

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.selectedTalent",response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
       
    },
    
    loadTalentDocuments : function(component, recordId, helper) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c.talentDocuments");
        action.setParams({ recordId : component.get("v.talentId")});

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //refresh component
                // console.log('@@ Get Talent');
                // console.log(response.getReturnValue());
                 component.set("v.lstTalentDocuments",response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
//        component.set("v.reloadData", true);
    },
    deleteTheActivity : function(component, recordId,talentDocumentId, helper) {
         var params = {"talentDocumentId": talentDocumentId, "recordId":recordId};
       	 
         this.callServer(component,'ATS','TalentDocumentFunctions',"deleteDocumentRecord",function(response){
//         	 $A.get('e.force:refreshView').fire();
             component.set("v.reloadData", true);
         },params,false);
    },
   setAsDefaultDoc : function(component, recordId,talentDocumentId, helper) {
        var params = {"talentDocumentId": talentDocumentId, "recordId":recordId};
       
        this.callServer(component,'ATS','TalentDocumentFunctions',"updateDefaultResumeFlag",function(response){
//            $A.get('e.force:refreshView').fire();
            component.set("v.reloadData", true);
        },params,false);
       
	},

    callServer : function(component, className, subClassName, methodName, callback, params, storable, errCallback) {
        /*console.log('callServer - method - ' + methodName);
        console.log('className - ' + className);
        console.log('subClassName - ' + subClassName);
        console.log(params);*/
        var serverMethod = 'c.performServerCall';
        var actionParams = {'className':className,
                            'subClassName':subClassName,
                            'methodName':methodName.replace('c.',''),
                            'parameters': params};

 
        var action = component.get(serverMethod);
        action.setParams(actionParams);

        action.setBackground();
        
        if(storable){
            action.setStorable();
        }else{
            action.setStorable({"ignoreExisting":"true"});
        }
      
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                // pass returned value to callback function
                callback.call(this, response.getReturnValue());
            } else if (state === "ERROR") {
                // Use error callback if available
                if (typeof errCallback !== "undefined") {
                    errCallback.call(this, response.getReturnValue());
                    // return;
                }

                // Fall back to generic error handler
                var errors = response.getError();
                if (errors) {
                    console.log("Errors", errors);
                    if (errors[0] && errors[0].message) {
                        this.showError(errors[0].message);
                        //throw new Error("Error" + errors[0].message);
                    }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                        this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                    }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                        this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                    }else{
                        this.showError('An unexpected error has occured. Please try again!');
                    }
                } else {
                    this.showError(errors[0].message);
                    //throw new Error("Unknown Error");
                }
            }
           
        });
    
        $A.enqueueAction(action);
    }
    ,showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    }, 

    /* Start changes for S-57312 */
       // Start -  functions from BaseCardRelatedList
   getViewFromApexFunction : function(cmp, apexFunction, recordID, params,helper){
        var className = '';
        var subClassName = '';
        var methodName = '';
        var self = this;

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordID, "maxRows": cmp.get("v.maxRows")};
            // Using new activity time line view - LWC
            // We may not need to set new record count, as the existing max record count does the same job.
            /*var newTimeLine = cmp.get('v.newTimeLine');
            if(newTimeLine != undefined && newTimeLine==true){
                actionParams.maxNextItems = cmp.get('v.maxNextItems');
                actionParams.maxPastItems = cmp.get('v.maxPastItems');
                actionParams.isNewTimeLineView = "Yes";
            }*/
            // User records
            var userRecordsChecked = cmp.get("v.userRecordsChecked");
            actionParams.isUserRecords = userRecordsChecked?"Yes":"No";

            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }
            // this.callServer(cmp,className,subClassName,methodName,function(response){
            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
               
                cmp.set("v.records", response);
                cmp.set("v.loadingData",false);
                var recordCount = 0;
                if(response){
                    if(response.length > 0){
                        if(response[0].totalItems){
                            recordCount = response[0].totalItems;
                        }

                        if (response[0].presentRecordCount !== "undefined") {
                            cmp.set ("v.presentRecordCount", response[0].presentRecordCount);
                            cmp.set ("v.pastRecordCount", response[0].pastRecordCount);
                            cmp.set ("v.hasNextSteps", response[0].hasNextSteps);
                            cmp.set ("v.hasPastActivity", response[0].hasPastActivity);
                        }
                    }
                }
                cmp.set("v.recordCount", recordCount); // returnVal.length);
                cmp.set("v.loadingCount",false);
            },actionParams,false);
        }

    }
    ,getViewListItems : function(cmp, viewListFunction, recordID,helper) {

        cmp.set("v.loadingData",true);
        var actionParams = {'recordId':recordID};
        var className = '';
        var subClassName = '';
        var methodName = '';

        var arr = viewListFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
            cmp.set("v.loadingData",false);
            var returnVal = response;

            /*var newArr = (new Function("return [" + returnVal + "];")());
            cmp.set("v.viewList", newArr);*/
            cmp.set("v.loadingData",false);
            this.getResults(cmp, this);

        },actionParams,false);
    }
    ,getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");

        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var apexFunc = listView[indexNo].apexFunction;
                    if(apexFunc){
                        var params = listView[indexNo].params;
                        this.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                    }
                }
            }
    ,updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },

    refreshList : function(cmp) {
        cmp.set("v.reloadData", true);
    },
    
    getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    expandCollapseHelper : function(component, className, expand, attributeToSet) {
        try
        {
            component.set(attributeToSet, expand);
        }catch(e){

        }
        
    },
	
	showSpinner: function(component, spinnerid) {
		var spinnerMain =  component.find(spinnerid);
		$A.util.removeClass(spinnerMain, "slds-hide");
		$A.util.addClass(spinnerMain, "slds-show");

	},
 
	hideSpinner: function(component,spinnerid) {
		var spinnerMain =  component.find(spinnerid);
		
		$A.util.addClass(spinnerMain, "slds-hide");
	},

   // END -  functions from BaseCardRelatedList
    /*END - changes for S-57312 */
})