({
    
    doInit  : function(component, event, helper) {
        var contact = component.get("v.contactRecord");
        component.set("v.talentId", contact.fields.AccountId.value);
        component.set("v.Spinner", true);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show'); 
        var talentId = component.get("v.talentId");
        helper.loadTalent(component, helper);
		helper.fetchCurrentUser(component,helper);   
    },
    
    onSelectChange : function(component, event, helper) {
        var selected = component.find("options").get("v.value");
        component.set ("v.recordCounter", selected);
    },
    
    //Neel summer 18 URL change-> This method not in use. Method calling code commented in cmp.
    showMoreDocuments : function(component, event, helper) {
		
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//Neel summer 18 URL change->"url": "/one/one.app#/n/Talent_Document_Details?recordId=" + component.get('v.recordId'),
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Talent_Document_Details?recordId=" + component.get('v.recordId'),
            "isredirect":true
        });
        urlEvent.fire();  
    },    
    actvitySaved:function(cmp,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": $A.get("$Label.c.ATS_Activity_Saved"),
            "type": "success"
        });
        toastEvent.fire();
    
        var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
        updateLMD.fire();
    },
    showActivities : function(cmp, event, helper){
        var recordID = cmp.get("v.recordId");
        var params = {"recordId": recordID};
        var bdata = cmp.find("bdhelper");  
        bdata.callServer(cmp,'ATS','','getTalentContactId',function(response){
            var contactId  = response;
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/apex/c__CandidateActivities?accountId=" + recordID + "&contactid=" + contactId
            });
            urlEvent.fire(); 
        },params,false);
        
    },
    //Neel summer 18 URL change-> This method not in use. Method calling code commented in cmp. 
    showMoreActivities : function(component, event, helper) {
		
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//Neel summer 18 URL change-> "url": "/one/one.app#/n/Activity_Details?recordId=" + component.get('v.recordId'),
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Activity_Details?recordId=" + component.get('v.recordId'),
            "isredirect":true
        });
        urlEvent.fire(); 
	},
    updateLoading: function(cmp, event, helper) {
            if(!cmp.get("v.loadingData")) {
                cmp.set("v.loading",false);
            }
    },
    addCandidateTask:function(cmp,event,helper){
        var userevent = $A.get("e.c:E_TalentActivityDetailAddTask");
        var recordID = cmp.get("v.recordId");
        userevent.setParams({"recordId":recordID, activityType:"task" })
        userevent.fire();
    }, 
    
    addCandidateEvent:function(cmp,event,helper){
        var userevent = $A.get("e.c:E_TalentActivityAddTask");
        var recordID = cmp.get("v.recordId");
        userevent.setParams({activityType:"event","recordId":recordID }) 
        userevent.fire();
    },
    deleteItem : function(cmp,event,helper){
        var recordId = event.getParam("recordId");
        var params = {"recordId": recordId};
        var bdata = cmp.find("bdhelper");
        bdata.callServer(cmp,'','','deleteRecord',function(response){
            cmp.set("v.reloadData",true);

            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "message": "Activity deleted!",
                "type": "success"
            });
            toastEvent.fire();
        },params,false);   
    },reloadData:function(component,event,helper){
         component.set("v.reloadData",true);
    },
    
     loadMoreData : function(component, event, helper) {        
        var recordCounter = component.get("v.recordCounter");
        recordCounter = recordCounter + 1;
        component.set("v.recordCounter", recordCounter);
        component.set("v.reloadData",true);
    },
    filterTalentActivity : function(component, event, helper) {
        component.set("v.reloadData", false);
        var childParamValue= event.getParam("filterCriteria");
        var setParentval=component.find("childval");
        setParentval.set("v.value", childParamValue);
        component.set("v.filterCriteria", childParamValue);
        
	},
    
    
})