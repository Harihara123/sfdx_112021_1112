({
	
    loadTalent : function(component, helper) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller        
        var action = component.get("c.getRecords");
        action.setParams({ recordId : component.get("v.talentId")});

        var self = this;
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //refresh component
                component.set("v.selectedTalent",response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.Spinner", false);
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-show'); 
        });
        $A.enqueueAction(action);
    } ,
	//Added to Fetch user Details By Debasis. To be approved after Phani's approval.
	/*fetchCurrentUser : function(component) {
		var params = null;
        this.callServer(component,'','',"getCurrentUser",function(response){
            if(component.isValid()){
            	component.set ("v.currentUser", response); 
            }
        }, params,false);
    },*/
	fetchCurrentUser : function(component,helper) {
        
        var action = component.get("c.getCurrentUser");
        var self = this;
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //refresh component
				var resp = response.getReturnValue();
				component.set("v.currentUser",resp);
            }			
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
			
            component.set("v.Spinner", false);
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-show'); 
        });
        $A.enqueueAction(action);
        //component.set("v.reloadData", true);
    } ,
	//Function End 
    getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
})