({  
    doInit:function(component, event, helper){  		
        component.set("v.typeValue","typeValue1");
         component.set("v.Owner",component.get("v.theevent.Owner.Name") ? component.get("v.theevent.Owner.Name") : component.get("v.runningUser.Name") );
        
        //S-114526 - Set Not Proceeding
		var ActivityType = component.get("v.theevent.Type");        
        if(ActivityType.substr(0,14)=='Not Proceeding'){
            component.set("v.NotProceeding", true);
        }

    },
    saveEvent:function(component, event, helper){  
		let whichbutton=event.getSource().getLocalId();
        helper.saveEvent(component,event, whichbutton); 
    },
    cancelEvent:function(component, event, helper){  
		const notMyActivity = component.get("v.notMyActivity");
		if(Boolean(notMyActivity)) {
			var userevent = $A.get("e.c:E_TalentActivityCancel");

			if(userevent){
				userevent.fire();
			}
		}
		else {
			let taskEvent = $A.get("e.c:E_MyActivitiesEventTask");
			taskEvent.setParams({"activityType":"Task", "actionType":"Cancel"});

			taskEvent.fire();
		}
    },
	handleMyActivitiesSaveNew : function(component, event, helper) {
		let actionType = event.getParam("actionType");
		if(actionType === 'SAVE_NEW_EVENT') {
			helper.saveEvent(component,event, 'btnSubmitSaveNew'); 
		}
		else if(actionType === 'SAVE_NEW_TASK') {
			helper.saveEvent(component,event, 'btnSubmitSaveNewTask'); 
		}
		else if(actionType === 'SAVE') {
			helper.saveEvent(component,event, 'btnSubmit'); 
		}
	},
    validateData : function(component, event, helper) {
        helper.validateData(component,event,helper);    
    },
    //Sandeep: When start date time is changed, set end date time to next hour
    onStartDateChange : function(component, event, helper) {
        var whichfield = event.getSource();
        if(whichfield.getLocalId() == 'StartDateTime'){
            helper.setEndDate(component,true);
        }
        helper.validateData(component,event,helper);    
    },   
	//S-91932 Rajeesh-added to default type in subject field.
	updateSubject : function(component, event, helper){
		const notMyActivity = component.get("v.notMyActivity");
		if(Boolean(notMyActivity)){
			var isNew = component.get("v.isNew");
			var priorSubjectValue = component.get("v.priorSubjectValue");
			var type = component.find("Type");
			var subject = component.find("Subject");
			//Sandeep: subject override fixes start -->
			if(!$A.util.isUndefinedOrNull(type) && isNew){
				if($A.util.isUndefinedOrNull(priorSubjectValue)){
					component.set("v.priorSubjectValue", type.get("v.value"));
				}
				//before updaating the subject check the previous value of the subject
				//if currents subject value is not equal to prior value , then don't update the subject
				if(priorSubjectValue == subject.get("v.value") || 
				   $A.util.isEmpty(subject.get("v.value"))){
					subject.set("v.value", type.get("v.value"));
					component.set("v.priorSubjectValue", type.get("v.value"));
				}   
			}

			var evt = $A.get('e.c:E_ExpandModal');
			if(type.get("v.value") === 'Service Touchpoint' || type.get("v.value") === 'Performance Feedback') {
				evt.setParam('expand', true);
			} else {
				evt.setParam('expand', false);
			}

			evt.fire();
		
			var evt = $A.get('e.c:E_ExpandModal');
			if(type.get("v.value") === 'Service Touchpoint' || type.get("v.value") === 'Performance Feedback') {
				evt.setParam('expand', true);
			} else {
				evt.setParam('expand', false);
			}

			evt.fire();
		}
	}
})