({ 
    validateFieldLengths : function(component, event) {
        
        var validForm = true;
        var limits;
        
        if( component.get("v.isEditEvent") ){
            limits =[];            
            if( component.get("v.theevent.Type") == 'Submitted' || 
               component.get("v.theevent.Type") == 'Offer Accepted' || 
               component.get("v.theevent.Type") == 'Started' ||
               component.get("v.NotProceeding")){
                limits =[ {"field" : "Comments", "limit" : 32000, "errorFieldId": "CommentsTooLong"} ];
            }
            
        }else{
			const notMyActivity = component.get("v.notMyActivity");
			if(Boolean(notMyActivity)) {
				limits =[  
					{"field" : "Subject", "limit" : 255, "errorFieldId" : "subjTooLong"}, 
					{"field" : "PreMeetingNotes", "limit" : 255, "errorFieldId": "preNotesTooLong"},
					{"field" : "PostMeetingNotes", "limit" : 32000, "errorFieldId": "postNotesTooLong"}
				];
			}
			else {
				limits =[  
					{"field" : "PreMeetingNotes", "limit" : 255, "errorFieldId": "preNotesTooLong"},
					{"field" : "PostMeetingNotes", "limit" : 32000, "errorFieldId": "postNotesTooLong"}
				];
			}
            
            
        }
        
        for (var i=0; i<limits.length; i++) {
            var limitedField = component.find(limits[i].field);
            
            var fieldValue = limitedField.get("v.value");
            
            var valueLength = (typeof fieldValue !== "undefined") ? fieldValue.length : 0;
            
            if (valueLength > limits[i].limit) {
                var warningSpanId = component.find(limits[i].errorFieldId);
                $A.util.removeClass(warningSpanId, "toggle");
                validForm = false;
            } else {
                var warningSpanId = component.find(limits[i].errorFieldId);
                $A.util.addClass(warningSpanId, "toggle");
            }
        }
        
        return validForm;
    },
    
    validateDateFormats : function (component, evt) {
        var valid = true;
        var errText = "Valid date/time format is " + this.getAcceptedDateFormat() + " " + this.getAcceptedTimeFormat();
        if (evt.StartDateTime === false) {
            var warningSpanId = component.find("badStartDate");
            $A.util.removeClass(warningSpanId, "toggle");
            warningSpanId.set("v.value", errText);
            valid = false;
        } else {
            var warningSpanId = component.find("badStartDate");
            $A.util.addClass(warningSpanId, "toggle");
        }
        if (evt.EndDateTime === false) {
            var warningSpanId = component.find("badEndDate");
            $A.util.removeClass(warningSpanId, "toggle");
            warningSpanId.set("v.value", errText);
            valid = false;
        } else {
            var warningSpanId = component.find("badEndDate");
            $A.util.addClass(warningSpanId, "toggle");
        }
        
        return valid;
    },
    
    validateRequiredFields : function (component, event) {
		const notMyActivity = component.get("v.notMyActivity");
        var valid = true;
        var requiredFields = [component.find('Subject'), component.find('StartDateTime'), component.find('EndDateTime')];
        
        if(requiredFields !== undefined){
            for(var i=0; i<requiredFields.length; i++){
                if( component.get("v.theevent.Type") != 'Interviewing'){
                    if(Boolean(notMyActivity) && requiredFields[i] === component.find('Subject') && $A.util.isEmpty(requiredFields[i].get("v.value"))){
                        requiredFields[i].set("v.errors", [{message: $A.get("$Label.c.ATS_Subject_is_required")}]);
                        valid = false;
                        
                    } else if(Boolean(notMyActivity) && requiredFields[i] === component.find('Subject') && !($A.util.isEmpty(requiredFields[i].get("v.value")))){
                        requiredFields[i].set("v.errors", []);
                    }
                }
                if(requiredFields[i] === component.find('StartDateTime') && $A.util.isEmpty(requiredFields[i].get("v.value"))){
                    //requiredFields[i].set("v.errors", [{message: "Start Date is required!"}]);
                    requiredFields[i].showHelpMessageIfInvalid();
                    valid = false;
                } else if(requiredFields[i] === component.find('StartDateTime') && !($A.util.isEmpty(requiredFields[i].get("v.value")))){
                    //requiredFields[i].set("v.errors", []);
                }
                if(requiredFields[i] === component.find('EndDateTime') && $A.util.isEmpty(requiredFields[i].get("v.value"))){
                    //requiredFields[i].set("v.errors", [{message: "End Date is required!"}]);
                    requiredFields[i].showHelpMessageIfInvalid();
                    valid = false;
                } else if(requiredFields[i] === component.find('EndDateTime') && !($A.util.isEmpty(requiredFields[i].get("v.value")))){
                    //requiredFields[i].set("v.errors", []);
                }
                
            }  
        }
        return valid;
        
    },
    validateTheDate : function(component, event) {
		const notMyActivity = component.get("v.notMyActivity");
		
        var validForm = true;
        
        var theevent = component.get("v.theevent");
        var today = new Date();
        var fields;
		if(Boolean(notMyActivity)) {
			fields=["StartDateTime","Type", "Subject","EndDateTime","OwnerId"];
		}
		else {
			fields=["StartDateTime","Type", "EndDateTime"];
		}
        var field = "";
        
        for (var i=0; i<fields.length; i++) {
            field=component.find(fields[i]);
            
            
            var value = field.get("v.value");
            var required = field.get("v.required");
            
            if(value == undefined){
                value="";
            }
            
            var validity=field.get('v.validity');
            if(validity){
                if (typeof validity == 'object') {
                    if (validity.badInput || 
                        validity.patternMismatch || 
                        validity.rangeOverflow || 
                        validity.rangeUnderflow ||
                        validity.stepMismatch || 
                        validity.tooLong || 
                        validity.typeMismatch || 
                        validity.valueMissing) {                        
                        validForm = false;
                    }
                }
            }
            
            if(required && value === ""){
                validForm = false;
            }
            
        }
        
        theevent.StartDateTime = this.formatDateTime(theevent.StartDateTime);
        theevent.EndDateTime = this.formatDateTime(theevent.EndDateTime);
        if (!this.validateDateFormats(component, theevent)) {
            return;
        }
        
        var sDate = Date.parse(theevent.StartDateTime);
        var eDate = Date.parse(theevent.EndDateTime);
        
        if (sDate > eDate) {
            var endDateWarning = component.find("endDateWarning");
            $A.util.removeClass(endDateWarning, "toggle");
            validForm = false;
        } else {
            var endDateWarning = component.find("endDateWarning");
            $A.util.addClass(endDateWarning, "toggle");
        }
        
        // Check longer than 14 days - 14 (days) * 24 (hrs) * 60 (mins) * 60 (secs) * 1000 (msecs)
        if ((eDate - sDate) > 1209600000) {
            var longEventWarning = component.find("longEventWarning");
            $A.util.removeClass(longEventWarning, "toggle");
            validForm = false;
        } else {
            var longEventWarning = component.find("longEventWarning");
            $A.util.addClass(longEventWarning, "toggle");
        }
        
        var showPostNotesBeforeStartError = false;
        
        //Check the start date on post meeting notes entry. Must be in the past.
        if(sDate > Date.now()){
            var pmNotes = theevent.Description;
            if(pmNotes){
                if(pmNotes.length > 0){
                    validForm = false;
                    showPostNotesBeforeStartError = true;
                }
            }
        }
        
        var postNotesBeforeStart = component.find("postNotesBeforeStart");
        if(showPostNotesBeforeStartError){
            $A.util.removeClass(postNotesBeforeStart, "toggle");
        }else{
            $A.util.addClass(postNotesBeforeStart, "toggle");
        }
        
        
        
        return validForm;
    },
    //This method validates the Related To Field making sure the enter value is valid
    validateRelatedToField : function(component, event){
        /* commented by akshay for codemerge start
        var valid = true;
        // var relatedToFieldText = document.getElementById("WhatId").value;
        var relatedToFieldText, 
            oppLkpCmp = component.find("evtOppLookup");
        if (oppLkpCmp) {
            relatedToFieldText = oppLkpCmp.get("v.currentTextVal"); 
        }
        var warningSpanId = component.find("relatedFieldNotValid");
        if (relatedToFieldText != null && relatedToFieldText != undefined && relatedToFieldText.length > 0) {
            
            var theevent = component.get("v.theevent");
             relatedToFieldText = relatedToFieldText.trim();
            if (relatedToFieldText === "") {
                  // document.getElementById("WhatId").value = "";
                  $A.util.addClass(warningSpanId, "toggle");
            }  else if (theevent !== null && typeof theevent.What !== 'undefined' && typeof theevent.What.Name !== 'undefined') {
                if (theevent.What.Name === relatedToFieldText) {
                        $A.util.addClass(warningSpanId, "toggle");
                } else {
                        valid = false;
                        $A.util.removeClass(warningSpanId, "toggle");  
                }
            } else {
                valid = false;
                $A.util.removeClass(warningSpanId, "toggle");
            } 
        } else {
            $A.util.addClass(warningSpanId, "toggle");
        }
        return valid; 
        commented by akshay for codemerge end
        */
        
        var valid = true;
        // var relatedToFieldText = document.getElementById("WhatId").value;
        var relatedToFieldText = component.find("evtOppLookup").get("v.currentTextVal"); 
        var warningSpanId = component.find("relatedFieldNotValid");
        if (relatedToFieldText != null && relatedToFieldText != undefined && relatedToFieldText.length > 0) {
            
            var theevent = component.get("v.theevent");
            relatedToFieldText = relatedToFieldText.trim();
            if (relatedToFieldText === "") {
                // document.getElementById("WhatId").value = "";
                $A.util.addClass(warningSpanId, "toggle");
            }  else if (theevent !== null && typeof theevent.What !== 'undefined' && typeof theevent.What.Name !== 'undefined') {
                if (theevent.What.Name === relatedToFieldText) {
                    $A.util.addClass(warningSpanId, "toggle");
                } else {
                    valid = false;
                    $A.util.removeClass(warningSpanId, "toggle");  
                }
            } else {
                valid = false;
                $A.util.removeClass(warningSpanId, "toggle");
            } 
        } else {
            $A.util.addClass(warningSpanId, "toggle");
        }
        return valid; 
    },
    
    saveEvent:function(component, event, whichbutton){ 
        
        
        if (!this.validateFieldLengths(component, event)) {
            return;
        }
        
        var theevent = component.get("v.theevent");
        
        //S-114526 - Update to include all Not Proceeding variations
        var NotProceeding = component.get("v.NotProceeding");
        
        if (!this.validateRequiredFields(component, event)) {
            return;
        }
        
        if (component.find("evtOppLookup") && !this.validateRelatedToField(component, event)) {
            return;
        }   
        
        if(!component.get("v.isEditEvent")){
            if (!this.validateTheDate(component, event)) {
                return;
            }
        }
        
        //added by akshay for S-48141 10/23/17 start
        if(typeof theevent.What !=='undefined'){
            delete theevent.What ;
        }
        if((typeof theevent.WhatId ==="undefined") || (theevent.WhatId === "") || (theevent.WhatId === null) ||(theevent.WhatId ==="undefined") ){
            theevent.WhatId = theevent.AccountId; 
        }
        
        //added by akshay for S-48141 10/23/17 end
        theevent.StartDateTime = this.formatDateTime(theevent.StartDateTime);
        theevent.EndDateTime = this.formatDateTime(theevent.EndDateTime);
        if (!this.validateDateFormats(component, theevent)) {
            return;
        }
        
        // set the sobjectType!
        theevent.sobjectType='Event';
        delete theevent.attributes;
        delete theevent.Owner;
       
        if(!component.get("v.isEditEvent")){
            var Post_Meeting_Notes__c = component.find("PostMeetingNotes").get("v.value");
            /*if(typeof Post_Meeting_Notes__c !== "undefined" && Post_Meeting_Notes__c.length > 32000){
                Post_Meeting_Notes__c = Post_Meeting_Notes__c.substring(0, 32000);
            }*/
            //theevent.Description = Post_Meeting_Notes__c;
            
            var Pre_Meeting_Notes__c = component.find("PreMeetingNotes").get("v.value");
            theevent.Pre_Meeting_Notes__c = Pre_Meeting_Notes__c;
            
        }
        
        if(component.get("v.isEditEvent")){
            
            if(theevent.Type == 'Interviewing' ){
                
                var clientComments = "";
                var candidateComments = "";
                
                if (component.find("ClientComments") && component.find("ClientComments").get("v.value")) {
                    clientComments = component.find("ClientComments").get("v.value") 
                }
                
                if (component.find("CandidateComments") && component.find("CandidateComments").get("v.value")) {
                    candidateComments = component.find("CandidateComments").get("v.value") 
                }
                
                var commentStr = '{"ClientComments":"' + clientComments + '", "CandidateComments":"' + candidateComments + '"}';
                theevent.Description = commentStr;
                
            }else if(   theevent.Type == 'Submitted' || 
                     theevent.Type == 'Offer Accepted' ||
                     theevent.Type == 'Started' || NotProceeding ){
                
                theevent.Description = component.find("Comments").get("v.value");
            }
        }
        
        var params = {"newEvent" : JSON.stringify(theevent)};
        component.set("v.shouldDisabled", true);
		component.set("v.setSpinner",true);
        var bdata = component.find("bdhelper");
        var self = this;
        bdata.callServer(component,'ATS','TalentActivityFunctions','putAccountEvent',function(response){
			component.set("v.setSpinner",false);
            if(response === "Inserted"){
                var userevent = component.getEvent('activitySavedEvent');
                userevent.setParam("buttonClick",event.getSource().getLocalId()); //S-32433 - Added by Karthik
                userevent.fire();
                self.actvitySaved(component, event);
                //Sandeep: Based which button lciked , we need to open new  Task or Event form
               
                let openActvity = $A.get("e.c:E_TalentActivityAddTask");
                let recordID = component.get("v.theevent.WhatId");
				
				const notMyActivity = component.get("v.notMyActivity");
				const notMyActivityNewEvent = component.get("v.notMyActivityNewEvent");
				if(Boolean(notMyActivity) && Boolean(notMyActivityNewEvent)) { //Neel : Why these two variable(attributes) used? check description of attribute in cmp file.
					if( whichbutton == 'btnSubmit'){
						var hideEvent = $A.get("e.c:E_TalentActivitySaveAndHide");
						hideEvent.fire();
					}
					else if(whichbutton == 'btnSubmitSaveNew'){
						//Sandeep : open New Event modal after event is saved.This app eevnt handled by C_ModalContainer.cmp .
						openActvity.setParams({activityType:"event","recordId":recordID }) 
						openActvity.fire();
                    
					}
					else if(whichbutton == 'btnSubmitSaveNewTask'){
						//Sandeep : open New Task modal after event is saved.This app eevnt handled by C_ModalContainer.cmp .
						openActvity.setParams({activityType:"task","recordId":recordID }) 
						openActvity.fire();
					}
				}
				else {
					let whoId =  component.get("v.theevent.WhoId");
					let taskEvent = $A.get("e.c:E_MyActivitiesEventTask");
					let actionType = "SAVE";
					if(whichbutton == 'btnSubmit'){
						taskEvent.setParams({"actionType":actionType});
					}
					else if(whichbutton == 'btnSubmitSaveNewTask'){
						actionType = "SAVE_NEW_TASK";
						//taskEvent.setParams({"actionType":actionType, "activityType":tasktype, "recordId":recordID});
						taskEvent.setParams({"actionType":actionType, "activityType":'task', "recordId":recordID, "whoId": whoId});
					}
					else if(whichbutton == 'btnSubmitSaveNew'){
						actionType = "SAVE_NEW_EVENT"
						taskEvent.setParams({"actionType":actionType, "activityType":"event", "recordId":recordID, "whoId": whoId});
					}
					
					taskEvent.fire();
				}
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "ERROR",
                    "message": "This activity has not been saved.  Please contact Connected Support for assistance.",
                    "type": "error"
                });
                toastEvent.fire();
            }
            component.destroy();
        },params,false);  		
    },
    
    validateData : function(component, event, helper) {
        const notMyActivity = component.get("v.notMyActivity");
        
        var validForm = true;
        
        var theevent = component.get("v.theevent");
        var NotProceeding = component.get("v.NotProceeding");
        var today = new Date();
		var fields;
		if(Boolean(notMyActivity)) {
			fields=["StartDateTime","Type", "Subject","EndDateTime","OwnerId"];
		}
		else {
			fields=["StartDateTime","Type","EndDateTime"];
		} 
        
        var field = "";        
        if( component.get("v.theevent.Id") ){            
            if( theevent.Type == 'Interviewing' || theevent.Type == 'Submitted' || theevent.Type == 'Offer Accepted'|| theevent.Type == 'Started' || NotProceeding ){
                
                component.set("v.isEditEvent", true );
                component.set("v.showPrePostNotes" , false );
            }else{
                component.set("v.showPrePostNotes" , true );
            }
            
            if(theevent.Type == 'Interviewing' ){
                try{
                    var description;
                    
                    if ($A.util.isUndefinedOrNull( theevent.Description )) {
                        theevent.Description = '{"ClientComments":" ", "CandidateComments":" "}';
                    }else{
                        
                        var strDescriptionObject = theevent.Description;
                        
                        strDescriptionObject = strDescriptionObject.replace(/(?:\r\n)/g, ' ');
                        description = JSON.parse(strDescriptionObject);
                        
                        
                        if(!$A.util.isUndefinedOrNull(theevent.ClientComments)) {
                            description.ClientComments      =  theevent.ClientComments ;
                        }
                        
                        if(!$A.util.isUndefinedOrNull(theevent.CandidateComments)) {
                            description.CandidateComments   =  theevent.CandidateComments ; 
                        }
                        
                        component.set("v.ClientComments",  description.ClientComments );
                        component.set("v.CandidateComments",  description.CandidateComments );                        
                    }
                }
                catch(exe){
                    console.log('Error while Parsing Json - Edit');
                }
            }else if(   theevent.Type == 'Submitted' || 
                     theevent.Type == 'Offer Accepted' ||
                     theevent.Type == 'Started' ||
                     NotProceeding){
                component.set("v.Comments",  theevent.Description );
            }
        }
        
        for (var i=0; i<fields.length; i++) {
            field=component.find(fields[i]);
            
            
            var value = field.get("v.value");
            var required = field.get("v.required");
            
            if(value == undefined){
                value="";
            }
            
            var validity=field.get('v.validity');
            if(validity){
                if (typeof validity == 'object') {
                    if (validity.badInput || 
                        validity.patternMismatch || 
                        validity.rangeOverflow || 
                        validity.rangeUnderflow ||
                        validity.stepMismatch || 
                        validity.tooLong || 
                        validity.typeMismatch || 
                        validity.valueMissing) {
                        
                        validForm = false;
                    }
                }
            }
            
            if(required && value === ""){
                validForm = false;
            }
            
        }
        
        theevent.StartDateTime = this.formatDateTime(theevent.StartDateTime);
        theevent.EndDateTime = this.formatDateTime(theevent.EndDateTime);
        if (!this.validateDateFormats(component, theevent)) {
            return;
        }
        
        var sDate = Date.parse(theevent.StartDateTime);
        var eDate = Date.parse(theevent.EndDateTime);
        
        if (sDate > eDate) {
            var endDateWarning = component.find("endDateWarning");
            $A.util.removeClass(endDateWarning, "toggle");
            validForm = false;
        } else {
            var endDateWarning = component.find("endDateWarning");
            $A.util.addClass(endDateWarning, "toggle");
        }
        
        // Check longer than 14 days - 14 (days) * 24 (hrs) * 60 (mins) * 60 (secs) * 1000 (msecs)
        if ((eDate - sDate) > 1209600000) {
            var longEventWarning = component.find("longEventWarning");
            $A.util.removeClass(longEventWarning, "toggle");
            validForm = false;
        } else {
            var longEventWarning = component.find("longEventWarning");
            $A.util.addClass(longEventWarning, "toggle");
        }
        
        var showPostNotesBeforeStartError = false;
        
        //Check the start date on post meeting notes entry. Must be in the past.
        if(sDate > Date.now()){
            var pmNotes = theevent.Description;
            if(pmNotes){
                if(pmNotes.length > 0){
                    validForm = false;
                    showPostNotesBeforeStartError = true;
                }
            }
        }
        
        var postNotesBeforeStart = component.find("postNotesBeforeStart");
        if(showPostNotesBeforeStartError){
            $A.util.removeClass(postNotesBeforeStart, "toggle");
        }else{
            $A.util.addClass(postNotesBeforeStart, "toggle");
        }        
    },
    formatDate:function(inputDate){
        var input = inputDate;
        if(input){
            // Typed in dates only work for "/" as delimiter.
            var arr = input.split("/");
            if(arr.length === 3){
                // Pad "0" for single digit date / month
                if(arr[0].length === 1){
                    arr[0] = '0' + arr[0];
                }
                if(arr[1].length === 1){
                    arr[1] = '0' + arr[1];
                }                
                // Convert to "yyyy-mm-dd" based on user locale.
                var language = window.navigator.userLanguage || window.navigator.language;
                if(language === 'en-US'){
                    input = arr[2] + '-' + arr[0] + '-' + arr[1];
                }else{
                    input = arr[2] + '-' + arr[1] + '-' + arr[0];
                }
            } 
            
            // Adjust for timezone since the new Date() constructor sets to UTC midnight.
            var now = this.adjustDateForTimezone(input);
            if (now === false || isNaN(now.getTime())) {
                return false;
            } else {
                return now.getFullYear()+"-"+(now.getMonth() + 1) +"-"+now.getDate();
            } 
        }else{
            return '';
        }
        
    }
    ,formatDateTime:function(inputDateTime){
        var output = inputDateTime;
        
        if(inputDateTime){
            
            // Split the input string on space
            var arr = inputDateTime.split(' ');
            
            
            // Works only with the standard date format. Spaces in date will break this.
            if(arr.length >= 2){
                // Call function to format date part of the string.
                var sDate = this.formatDate(arr[0]);
                
                // Split time part of the string by colon. 
                var hhmm = arr[1].split(":");
                var hrs, mins, ampm;
                ampm = '';
                if (hhmm.length >= 2) {
                    // Pad single digit hours with "0".
                    hrs = hhmm[0].length === 1 ? "0" + hhmm[0] : hhmm[0];
                    // If am/pm is not separated from time by space, use substring.
                    mins = hhmm[1].substring(0, 2);
                    if (hhmm[1].length > 2) {
                        ampm = hhmm[1].substring(2, hhmm[1].length);
                    }
                }
                // If am/pm separated from time by space
                if (arr.length === 3) {
                    ampm = arr[2];
                }
                // Adjust to 24 hour clock
                if (ampm.toLowerCase() === "pm" && hrs !== "12") {
                    hrs = parseInt(hrs) + 12;
                } else if (ampm.toLowerCase() === "am" && hrs === "12") {
                    hrs = "00";
                }
                
                // Initialize Date object from generated date and time strings.
                var sTime = " " + hrs + ":" + mins + ":00.000Z";
                
                // Initialize date and readjust for timezone because previous parsing steps set it to UTC
                var tDate = new Date(sDate + sTime);
                var theDate = new Date(tDate.getTime() + (tDate.getTimezoneOffset() * 60 * 1000));
                
                // Check for invalid date format. Return boolean false if invalid.
                if (!isNaN(theDate.getTime())) {
                    output = theDate.toISOString();
                } else {
                    output = false;
                }
            } else {
                // Datepicker was used, so input is ISO string and passed right back as output.
                output = inputDateTime;
            }
        }
        return output;
    }
    ,adjustDateForTimezone : function (inputDate) {
        var theDate = new Date(inputDate);
        if (!isNaN(theDate.getTime())) {
            // Use same date to get the timezone offset so daylight savings is factored in.
            var output = new Date(theDate.getTime() + (theDate.getTimezoneOffset() * 60 * 1000));
            return output;
        } else {
            return false;
        }
    },
    getAcceptedDateFormat : function () {
        var language = window.navigator.userLanguage || window.navigator.language;
        return language === "en-US" ? "mm/dd/yyyy" : "dd/mm/yyyy";
    },
    
    getAcceptedTimeFormat : function () {
        return "hh:mm AM/PM";
    },
    actvitySaved:function(cmp,event){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": $A.get("$Label.c.ATS_Activity_Saved"),
            "type": "success"
        });
        toastEvent.fire();
        
        var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
        updateLMD.fire();
    },
    /*Sandeep : Set end date to be one hour after start date*/
    setEndDate : function(component,enforce){
        let startdate=component.find("StartDateTime").get("v.value");
        let endDate=component.find("EndDateTime").get("v.value");
        var theevent = component.get("v.theevent");
        var objectId;
        if(theevent != undefined){
            objectId = theevent.Id;
        }
        var dateNow= new Date();
        dateNow.setTime(Date.parse(startdate));
        (objectId != undefined && objectId != null) ? dateNow.setTime(Date.parse(endDate)):dateNow.setHours(dateNow.getHours() +1);
        component.find("EndDateTime").set("v.value",dateNow.toISOString());
    }
})