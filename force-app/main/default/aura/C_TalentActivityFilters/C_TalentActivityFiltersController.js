({
	loadFilterList: function(component, event, helper) {
  	// call the helper function for fetch contact from apex class 
  		helper.onLoad(component, event);
        
 	},
 
 	// For count the selected checkboxes. 
 	checkboxSelect: function(component, event, helper) {
  		var selectedRec = event.getSource().get("v.value");
  		var getSelectedNumber = component.get("v.selectedCount");
  		if(selectedRec == true) {
   			getSelectedNumber++;
  		}else {
   			getSelectedNumber--;
  		}
        component.set("v.selectedCount", getSelectedNumber);
 	},
 
 
	selectAll: function(component, event, helper) {
    	//get the header checkbox value  
  		var selectedHeaderCheck = event.getSource().get("v.value");
  		var getAllId = component.find("boxPack");
  		if (selectedHeaderCheck == true) {
           for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", true);
            component.set("v.selectedCount", getAllId.length);
           }
  		} else {
           for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", false);
            component.set("v.selectedCount", 0);
           }
  		}
 	},

	selectAll2: function(component, event, helper) {
    	//get the header checkbox value  
  		var selectedHeaderCheck = event.getSource().get("v.value");
  		var getAllId = component.find("boxPack1");
  		if (selectedHeaderCheck == true) {
           for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack1")[i].set("v.value", true);
            component.set("v.selectedCount", getAllId.length);
           }
  		} else {
           for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack1")[i].set("v.value", false);
            component.set("v.selectedCount", 0);
           }
  		}
 	},    
    
	resetAll: function(component, event, helper) {
    	
        //get the header checkbox value  
  		var selectedHeaderCheck = false;
  		var getAllId = component.find("boxPack");
  		component.find("box3").set("v.value", false);
        for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", false);
            component.set("v.selectedCount", 0);
        }
        var getAllId2 = component.find("boxPack1");
  		component.find("box33").set("v.value", false);
        for (var i = 0; i < getAllId2.length; i++) {
            component.find("boxPack1")[i].set("v.value", false);
            component.set("v.selectedCount", 0);
        }
  		helper.selectedFilterHelper(component, event, '');
       
 	},    
    
	//For Getting selected filters 
 	selectedFilters: function(component, event, helper) {
        // create var for store record id's for selected checkboxes  
        component.set("v.Spinner", true);
        var spinner = component.find('spinnerFilter');
        $A.util.addClass(spinner, 'slds-show'); 

        var filterId = [];
        var filterIdString = "NA,";
        // get all checkboxes 
        var getAllId = component.find("boxPack");
        var getAllId2 = component.find("boxPack1");
        // if value is checked(true) then add those Id (store in Text attribute on checkbox) in delId var.
  		for (var i = 0; i < getAllId.length; i++) {
   			if (getAllId[i].get("v.value") == true) {
    			filterId.push(getAllId[i].get("v.text"));
                filterIdString = filterIdString + getAllId[i].get("v.text") + ',';
   			}
  		}
        filterIdString = filterIdString + ",IsTaskOrEvent:";
        for (var i = 0; i < getAllId2.length; i++) {
   			if (getAllId2[i].get("v.value") == true) {
    			filterId.push(getAllId2[i].get("v.text"));
                filterIdString = filterIdString + getAllId2[i].get("v.text") + ',';
            }
  		}
        
        filterIdString = filterIdString.substring(0, filterIdString.length-1);
        
        //S-114526 - Replace Not Proceeding with all Not Proceeding variations
        var NPLabels = $A.get("$Label.c.CRM_All_NotProceedingValues");
        filterIdString = filterIdString.replace("Not Proceeding",NPLabels);

  		// call the helper function and pass all selected Filter id's.    
  		helper.selectedFilterHelper(component, event, filterIdString);
        
      setTimeout(function() {
          $A.util.removeClass(spinner, 'slds-show');
          $A.util.addClass(spinner, 'slds-hide');
      }, 1500);

 	},
 
})