({
 	onLoad: function(component, event) {
  		
    	// set deafult count and select all checkbox value to false on load 
    	component.set("v.selectedCount", 0);
    	component.find("box3").set("v.value", false);
   
 	},
 
	selectedFilterHelper: function(component, event, filterIds) {
  		
        if(filterIds == "NA,,IsTaskOrEvent"){
        	filterIds = "";    
        }
        var viewList =  component.get("v.viewList");
        if (viewList && viewList["0"] && viewList["0"].params && viewList["0"].params.filterCriteria) {
            viewList["0"].params.filterCriteria = filterIds;
        }
        component.set ("v.viewList", viewList);
        
        var parametersEvent = component.getEvent("talentActivityFilter");
        parametersEvent.setParams({
            "filterCriteria" : filterIds,
            "Spinner" : true
        });
        
        // Fire the event
        parametersEvent.fire();
        
 	}
})