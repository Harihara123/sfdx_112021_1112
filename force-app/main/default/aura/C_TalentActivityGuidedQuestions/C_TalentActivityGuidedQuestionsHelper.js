({
	getGuidedQuestions : function (component, helper) {
        var action = component.get('c.getGuidedQuestionsForUserOpco');

        action.setCallback(this, function(response) {
            var state = response.getState();

            if(state === 'SUCCESS') {
                var results = response.getReturnValue();
                var guidedQuestionsMap = [];
                for(var key in results) {
                    guidedQuestionsMap.push({key : key, value : results[key]});             
                }
                component.set('v.guidedQuestions', guidedQuestionsMap);
            }
            else {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                    else {
                        console.log("Unknown error");
                    }
                }
            }
        });
        $A.enqueueAction(action);
	}
})