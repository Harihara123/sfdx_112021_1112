({
    doinit:function(component, event, helper){
	    /* Dasaradh - #user story #S-181984 Keyboard shortcut for log a call modal save & close that is compatible with Windows and Mac os
                     ctrl+A should work for Windows
                     cmd+A should work for Mac.
                     Note: Could use custom labels for keycodes if necessary.*/
       var Name = "Not known"; 
        if (navigator.appVersion.indexOf("Win") != -1) Name =  
          "Windows OS";
        if (navigator.appVersion.indexOf("Mac") != -1) Name =  
          "MacOS"; 
		  
       window.addEventListener('keydown', $A.getCallback(function(e) {
	   if(component.isValid()){
            var modifier = e.ctrlKey || e.metaKey;
			var shiftkey = e.shiftKey;
           if((modifier && shiftkey && e.which == 90) && (Name == 'Windows OS' || Name == 'MacOS')){
              e.preventDefault();
			  helper.saveTaskKeyboradShortcut(component, event, helper);
           }
		   }
       }));//End Dasaradh
	}, 
	//Dasaradh - S-189680 to destroy the component
	destoryCmp:function(component, event, helper){ 
	  component.destroy();
	},

    saveTask:function(component, event, helper){  
		let whichbutton = event.getSource().getLocalId();
        helper.saveTask(component,event, whichbutton); 
	},
    cancelTask:function(component, event, helper){  
		 const notMyActivity = component.get("v.notMyActivity");
		if(Boolean(notMyActivity)) {
			var userevent = $A.get("e.c:E_TalentActivityCancel");

			if(userevent){
				userevent.fire();
			}
		}
		else {
			let taskEvent = $A.get("e.c:E_MyActivitiesEventTask");
			taskEvent.setParams({"activityType":"Task", "actionType":"Cancel"});

			taskEvent.fire();
		}
       helper.focusSource(component,event);
	},
	handleMyActivitiesSaveNew : function(component, event, helper) {
		let actionType = event.getParam("actionType");
		if(actionType === 'SAVE_NEW_EVENT') {
			helper.saveTask(component,event, 'btnSubmitNewEvent'); 
		}
		else if(actionType === 'SAVE_NEW_TASK') {
			helper.saveTask(component,event, 'btnSubmitNew'); 
		}
		else if(actionType === 'SAVE') {
			helper.saveTask(component,event, 'btnSubmit'); 
		}
	},
    validateData : function(component, event, helper) {
		console.log('validateData:'+JSON.stringify(component.get('v.task')));
	  const notMyActivity = component.get("v.notMyActivity");
      if(component.isValid() && Boolean(notMyActivity)){
            var disableButton = false;
            var fields=["Status","ActivityDate","Type", "Subject","RecurrenceInterval","OwnerId", "Priority"];  
            for (var i=0; i<fields.length; i++) {
                var field = component.find(fields[i]);
                var value = field.get("v.value");
                var required = field.get("v.required");

                if(value == undefined){
                    value="";
                }
                
                var validity=field.get('v.validity');
                if(validity){
                    if (typeof validity == 'object') {
                                    if (validity.badInput || 
                                        validity.patternMismatch || 
                                        validity.rangeOverflow || 
                                        validity.rangeUnderflow ||
                                        validity.stepMismatch || 
                                        validity.tooLong || 
                                        validity.typeMismatch || 
                                        validity.valueMissing) {
                                        
                                        disableButton = true;
                                    }
                                }
                }
                
                if(required && value === ""){
                    disableButton = true;
                }                
            }          
        }

    },

    enableDisableRecurrenceInterval : function (component, event, handler) {
	   const notMyActivity = component.get("v.notMyActivity");
	   if(Boolean(notMyActivity)) {
		   var type = component.get("v.task.RecurrenceRegeneratedType");
			var intervalField = component.find("RecurrenceInterval");
			if (typeof type === "undefined" || type === "") {
				intervalField.set("v.disabled", true);
				intervalField.set("v.required", false);
				//intervalField.set("v.value", 0);
				intervalField.set("v.errors", null); 
				//intervalField.showHelpMessageIfInvalid();
				if (typeof component.get("v.task.RecurrenceInterval") !== "undefined") {
					component.set("v.task.RecurrenceInterval", "");
				}
			} else {
				intervalField.set("v.disabled", false);
				intervalField.set("v.required", true);				
			}
		}
    },
    
	updateSubject : function(component, event, helper){
	  const notMyActivity = component.get("v.notMyActivity");
	  if(Boolean(notMyActivity)) {
			var isNew = component.get("v.isNew");
			var type = component.find("Type");
			var subject = component.find("Subject");
			var priorSubjectValue = component.get("v.priorSubjectValue");
			/*if(type && isNew) {				
				var subject = component.find("Subject");
				subject.set("v.value", type.get("v.value"));
			}*/
			 //Sandeep: subject override fixes start -->
			if(!$A.util.isUndefinedOrNull(type) && isNew){
				if($A.util.isUndefinedOrNull(priorSubjectValue)){
					component.set("v.priorSubjectValue", type.get("v.value"));
				}
				//before updaating the subject check the previous value of the subject
				//if currents subject value is not equal to prior value , then don't update the subject
				if(priorSubjectValue == subject.get("v.value") || 
				   $A.util.isEmpty(subject.get("v.value"))){
					subject.set("v.value", type.get("v.value"));
					component.set("v.priorSubjectValue", type.get("v.value"));
				}   
			}

			var evt = $A.get('e.c:E_ExpandModal');
			if(type.get("v.value") === 'Service Touchpoint' || type.get("v.value") === 'Performance Feedback') {
				evt.setParam('expand', true);
			} else {
				evt.setParam('expand', false);
			}

			evt.fire();

			var evt = $A.get('e.c:E_ExpandModal');
			if(type.get("v.value") === 'Service Touchpoint' || type.get("v.value") === 'Performance Feedback') {
				evt.setParam('expand', true);
			} else {
				evt.setParam('expand', false);
			}			
			evt.fire();
		}
	},
    
    /*
     * Updated By Rohit Maharashi for User Story S-181487
     * 	- Make "Log a call" component JAWS complaint
     * 	- Function to confirm focus on Save button on popup and fire event
    */ 
    hasFocusOnSave:function(component, event, helper) {
         event.preventDefault();
        if ( event.keyCode == 9 ) {
            var cmpEvent = component.getEvent("cmpEvent");
            cmpEvent.fire();
        }

		/* Monika -- S-182912 -- Reverse tabbing from Close to Save button for "Log a call" modal
		   After getting focus back to Save button, Shift+Tab is setting back to close button, focus is trap between those two buttons
		   So setting back the focus to "Save & New" button on Shift+Tab of Save button */
		if (event.shiftKey && event.keyCode == 9) {
			helper.setFocusToField(component, event, "btnSubmitNew");
			}
		},

	// Monika -- S-182912 -- Reverse tabbing from Close to Save button for "Log a call" modal 
	focusOnSaveBtnFromClose:function(component, event, helper) {
		const fieldId = event.getParam("fieldIdToGetFocus");
		if(fieldId == null) return;
		helper.setFocusToField(component, event, fieldId);
    }
})