({   selectViewMenuItem : function(component, event, helper) {
        var triggerCmp = component.find("trigger");

        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            
            //annoying - you can only access the label right now.., 
            //will have to parse array until iD works
            
            var views = component.get("v.viewList");
			var arrayLength = views.length;
            
			for (var i = 0; i < arrayLength; i++) {
    			
                if (views[i].label === label) {
                    component.set("v.viewListIndex",i);
                }
			}
          
        }
	},
    showActivities : function(cmp, event, helper){
        var recordID = cmp.get("v.talentId");
        var params = {"recordId": recordID};
        var contactId = cmp.get("v.recordId");
         var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/apex/c__CandidateActivities?accountId=" + recordID + "&contactid=" + contactId
            });
            urlEvent.fire(); 
        
        
    },
     
    showMoreActivities : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/one/one.app#/n/Activity_Details?recordId=" + component.get('v.talentId'),
            "isredirect":true
        });
        urlEvent.fire(); 
    },    
    updateLoading: function(cmp, event, helper) {
            if(!cmp.get("v.loadingData")) {
                cmp.set("v.loading",false);
            }
    },
    addCandidateTask:function(cmp,event,helper){
        var userevent = $A.get("e.c:E_TalentActivityAddTask");
        var recordID = cmp.get("v.talentId");
        userevent.setParams({"recordId":recordID, activityType:"task" })
        userevent.fire();
    }, 
    addCandidateCall:function(cmp,event,helper){
        var userevent = $A.get("e.c:E_TalentActivityAddTask");
        var recordID = cmp.get("v.talentId");
        userevent.setParams({"recordId":recordID, activityType:"call" })
        userevent.fire();
    },
    addCandidateEvent:function(cmp,event,helper){
        var userevent = $A.get("e.c:E_TalentActivityAddTask");
        var recordID = cmp.get("v.talentId");
        userevent.setParams({activityType:"event","recordId":recordID }) 
        userevent.fire();
    }

    ,deleteItem : function(cmp,event,helper){
        var recordId = event.getParam("recordId");
        var params = {"recordId": recordId};

            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,'','','deleteRecord',function(response){
            cmp.set("v.reloadData",true);

            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "message": "Activity deleted!",
                "type": "success"
            });
            toastEvent.fire();
        },params,false);   
    },reloadData:function(component,event,helper){
         component.set("v.reloadData",true);
    },
    /* base card timeline start */
    
    shouldLoadMore : function(component, event, helper) {
        
        var childParamValue= event.getParam("shouldLoadMore");
        component.set ("v.displayLoadMore", childParamValue);
        
	}, 
    
    onSelectPresentChange : function(component, event, helper) {
        var spinner = component.find('spinnerLoadMore');
        $A.util.addClass(spinner, 'slds-show'); 

        var selected = parseInt(component.find("loadMorePresentDropdown").get("v.value")) + parseInt(component.get ("v.presentActivityCount"));   
        component.set ("v.presentActivityCount", selected);
        component.set("v.loadMorePresent",[0,5,10,25,50]);
        component.set("v.loadMorePresent",["Select","5","10","25","50"]);

        setTimeout(function() {
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
        }, 1500);

    },

    onSelectPastChange : function(component, event, helper) {

        var spinner = component.find('spinnerLoadMore');
        $A.util.addClass(spinner, 'slds-show'); 

        var selected = parseInt(component.find("loadMorePastDropdown").get("v.value")) + parseInt(component.get ("v.pastActivityCount"));   
        component.set ("v.pastActivityCount", selected);
        component.set("v.loadMorePast",[0,5,10,25,50]);
        component.set("v.loadMorePast",["Select","5","10","25","50"]);

        setTimeout(function() {
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
        }, 1500);
    },
    
    expandPastActivity : function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandPastActivity", true, "v.pastActivityExpanded");
	},
	collapsePastActivity: function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandPastActivity", false, "v.pastActivityExpanded");
	},
	expandNextSteps : function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandNextSteps", true, "v.nextStepsExpanded");
	},
	collapseNextSteps: function(component, event, helper) {
		helper.expandCollapseHelper(component, "expandNextSteps", false, "v.nextStepsExpanded");
	},
    
    
    //base card timelien end
    //base card rr start
loadData: function(cmp, event, helper) {
        cmp.set("v.loading",true);
        cmp.set("v.records",[]);
        cmp.set("v.recordCount",0);


        helper.getResults(cmp,helper);
    }
    ,doInit: function(cmp, event, helper) {
        helper.getCurrentUser(cmp);
        var conrecord = cmp.get("v.contactRecord");
		console.log('conrecord.fields.AccountId.value---------------------------------'+conrecord.fields.AccountId.value);
        cmp.set("v.talentId",conrecord.fields.AccountId.value);
        var recordID = cmp.get("v.talentId");
        helper.getResults(cmp,helper);  
      /* Dasaradh - #user story #S-172419 Keyboard shortcut for log a call on page loading that is compatible with Windows and Mac
                     ctrl+L should work for Windows
                     cmd+L should work for Mac.
                     Note: Could use custom labels for keycodes if necessary.*/
       
       var Name = "Not known"; 
        if (navigator.appVersion.indexOf("Win") != -1) Name =  
          "Windows OS";
        if (navigator.appVersion.indexOf("Mac") != -1) Name =  
          "MacOS"; 
		  
		  var a = function(e){
		  if(cmp.isValid()){
            var modifier = e.ctrlKey || e.metaKey;
			var shiftkey = e.shiftKey;
           if(modifier && shiftkey && e.which == 76 && (Name == 'Windows OS' || Name == 'MacOS')){
               e.preventDefault();
			   helper.addCandidateCall(cmp, event, helper);
           }
		   }
		   }
		    window.addEventListener('keydown', $A.getCallback(a));
       //End Dasaradh
	  
  },
  //Dasaradh - S-189680 to destroy the component
  destoryCmp: function(cmp, event, helper){
		cmp.destroy();
	}
    ,reloadData : function(cmp,event,helper) {
        var rd = cmp.get("v.reloadData");
        //if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);

            helper.getResults(cmp,helper);

            cmp.set("v.reloadData", false);
      //  }
    },

	// Monika -- S-189738 -- Focus back on source after Log a Call modal is closed/cancel button is hit 
	focusonSource:function(component, event, helper) {
		const fieldId = event.getParam("fieldIdToGetFocus");
		if(fieldId == null) return;
		helper.setFocusToField(component, event, fieldId);
    }
    //base card rr end
    
})