({getAddressString : function(fieldList) {
        var address = '';
        if (fieldList && fieldList.length > 0) {
            address = fieldList[0];
            for (var i = 1; i < fieldList.length; i++) {
                address = (fieldList[i] && fieldList[i].length > 0 ? (address && address.length > 0 ? (address + ", " + fieldList[i]) : fieldList[i]) : address); 
            }
        }
        return address;
	},
     /* base card timeline start. */
	expandCollapseHelper : function(component, className, expand, attributeToSet) {
		try
		{
			component.set(attributeToSet, expand);
		}catch(e){

		}
		
	}
     /* base card timeline end. */
    /*bcrr start*/
  
    ,getViewFromApexFunction : function(cmp, apexFunction, recordID, params,helper){
        var className = '';
        var subClassName = '';
        var methodName = '';
        var self = this;

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordID, "maxRows": cmp.get("v.maxRows")};
            var newTimeLine = cmp.get('v.newTimeLine');
            if(newTimeLine != undefined && newTimeLine==true){
                actionParams.maxNextItems = cmp.get('v.maxNextItems');
                actionParams.maxPastItems = cmp.get('v.maxPastItems');
                actionParams.isNewTimeLineView = "Yes";
            }
            // User records
            var userRecordsChecked = cmp.get("v.userRecordsChecked");
            actionParams.isUserRecords = userRecordsChecked?"Yes":"No";

            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }
            // this.callServer(cmp,className,subClassName,methodName,function(response){
            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
                cmp.set("v.records", response);
                cmp.set("v.loadingData",false);
                var recordCount = 0;
                var totalPast30Days = 0;
                cmp.set("v.totalActivitiesLast30Days", totalPast30Days);
                if(response){
                    if(response.length > 0){
                        if(response[0].totalItems){
                            recordCount = response[0].totalItems;
                        }
                        if(response[0].totalCountNDays){
                            totalPast30Days = response[0].totalCountNDays;
                            cmp.set("v.totalActivitiesLast30Days", totalPast30Days); 
                        }

                        if (response[0].presentRecordCount !== "undefined") {
                            cmp.set ("v.presentRecordCount", response[0].presentRecordCount);
                            cmp.set ("v.pastRecordCount", response[0].pastRecordCount);
                            cmp.set ("v.hasNextSteps", response[0].hasNextSteps);
                            cmp.set ("v.hasPastActivity", response[0].hasPastActivity);
						}

						if(response[0].lastmeetingdate !== "undefined"){//added by akshay for S - 82631 5/21/18 start
							  cmp.set("v.lastMeetingDate", response[0].lastmeetingdate);
							  cmp.set("v.lastMeetingWith", response[0].lastmeetingwith);
						  }//added by akshay for S - 82631 5/21/18 end
                          
                        // Added for STORY S-133689
                        if (response[0].totalOpenRecords != undefined) {
                            cmp.set ("v.totalOpenRecords", response[0].totalOpenRecords);
                            //console.log(response[0].taskStartDateTime);
                        }else{
                            cmp.set ("v.totalOpenRecords", 0);
                        }
                        if (response[0].totalPastRecords != undefined) {
                            cmp.set ("v.totalPastRecords", response[0].totalPastRecords);
                        }
                    }
                }

                self.updateInsights(cmp, event); 
                cmp.set("v.recordCount", recordCount); // returnVal.length);
                cmp.set("v.loadingCount",false);
            },actionParams,false);
        }

    }
    ,getViewListItems : function(cmp, viewListFunction, recordID,helper) {

        cmp.set("v.loadingData",true);
        var actionParams = {'recordId':recordID};
        var className = '';
        var subClassName = '';
        var methodName = '';

        var arr = viewListFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
            cmp.set("v.loadingData",false);
            var returnVal = response;
            //Rajeesh S-59499 -- removing new Function statement.
            var newArr = JSON.parse(returnVal);
           // var newArr = (new Function("return [" + returnVal + "];")());
            cmp.set("v.viewList", newArr);
            cmp.set("v.loadingData",false);
            this.getResults(cmp, this);

        },actionParams,false);
    }
    ,getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");

        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var apexFunc = listView[indexNo].apexFunction;
                    if(apexFunc){
                        var params = listView[indexNo].params;
                        this.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                    }
                }
            }
    ,updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },

    refreshList : function(cmp) {
        cmp.set("v.reloadData", true);
    },
        getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }, 
    /* bcrr end */
    updateInsights: function(cmp, event){
        var actvityEvent = $A.get("e.c:E_UpdateInsightTalentActivity"); 
        actvityEvent.setParams({
            "recordId": cmp.get("v.recordId"), 
            "talentActivityRecords": cmp.get("v.records"), 
            "totalActivitiesLast30Days": cmp.get("v.totalActivitiesLast30Days"),
			"lastMeetingDate":cmp.get("v.lastMeetingDate"),//added by akshay for S - 82631 5/21/18 start
			"lastMeetingWith":cmp.get("v.lastMeetingWith")//added by akshay for S - 82631 5/21/18 start
        });
        actvityEvent.fire();
    },
	getCurrentUser : function(component) {
		var bdata = component.find("basedatahelper");
        bdata.callServer(component,"","","getCurrentUser",function(response){
        component.set("v.currentUser", response);
		},"",true);
	},

	//Dasaradh - call this method in controller init for keyboard shortcut
	addCandidateCall:function(cmp,event,helper){
        var userevent = $A.get("e.c:E_TalentActivityAddTask");
        var recordID = cmp.get("v.talentId");
        userevent.setParams({"recordId":recordID, activityType:"call" })
        userevent.fire();
    },

	//Monika - Set focus to a particular field
	setFocusToField:function(component,event,cmpId){
		const fieldIdToGetFocus = component.find(cmpId);
		if(fieldIdToGetFocus == null) return;
		window.setTimeout(
			$A.getCallback(function() {
				// wait for element to render then focus
				fieldIdToGetFocus.focus();
			}), 100
		);
	},
})