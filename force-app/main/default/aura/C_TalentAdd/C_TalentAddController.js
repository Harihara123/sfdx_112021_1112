({
	doInit : function(component, event, helper) {
        var p = helper.getParameterByName("clientcontactid");
        component.set("v.clientContactId", p);

        // clientcontactid parameter valid. Related contact flow.
        if (p !== null && p !== "") {
        	component.set("v.mode", "search");
        	helper.fireInitEvent(component);
        } 
	},

	chooseSearch : function(component, event, helper) {
		component.set("v.mode", "search");
		helper.fireInitEvent(component);
	},

	setModeForClose : function(component, event, helper) {
		component.set("v.mode", "");
	},

	chooseUpload : function(component, event, helper) {
		component.set("v.mode", "upload");
		helper.createUncommittedTalent(component);
	}

})