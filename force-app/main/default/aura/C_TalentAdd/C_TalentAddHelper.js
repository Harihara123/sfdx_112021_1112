({
	getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        	results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

	fireInitEvent : function(component) {
		var evt;
		if (component.get("v.mode") === "search") {
			evt = $A.get("e.c:E_TalentSearchCreateInit");
			if (component.get("v.clientContactId") !== "") {
				evt.setParams({
					"clientContactId" : component.get("v.clientContactId")
				});
			}
		} else if (component.get("v.mode") === "upload") {
			evt = $A.get("e.c:E_TalentUploadCreateInit");
		} else {
			// Failover. Do nothing if mode is not in the expected values.
			return;
		}

		evt.fire();
	},

	createUncommittedTalent : function(component, postCreateAction) {
		var action = component.get("c.createUncommittedTalent");

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.talentId", response.getReturnValue());
				this.postTalentCreateAction(component);
			} else {
				console.log("ERROR .... ");
			}
		})

		$A.enqueueAction(action);
	},

	postTalentCreateAction : function(component) {
		var theVal = component.get("v.addTrigger")
        component.set("v.addTrigger", !theVal);
		this.fireInitEvent(component);
	}
})