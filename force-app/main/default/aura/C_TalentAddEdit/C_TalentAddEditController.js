({
    validateAndSaveTalent : function(component, event, helper) {
        //component.set("v.g2Comments", event.getParam("g2Comments"));
		var params = event.getParam('arguments')
        if(!$A.util.isUndefinedOrNull(params)){
			component.set("v.isG2Checked",params.g2checked)
		    component.set("v.g2Comments", params.g2comments);
		}
		
        
        helper.validateAndSaveTalent(component);
    },

    sectionAllProfile: function(component, event, helper) {
        var acc = component.get('v.acc');

        acc[1].icon = 'utility:down';
        acc[2].icon = 'utility:down';

        acc[1].className = 'show';
        acc[2].className = 'show';

        component.set('v.acc', acc);

        component.set('v.expanded', true);
        component.set('v.expandAll', false);

    },

    sectionCloseAllProfile: function(component, event, helper) {
        var acc = component.get('v.acc');

        acc[1].icon = 'utility:right';
        acc[2].icon = 'utility:right';

        acc[1].className = 'hide';
        acc[2].className = 'hide';

        component.set('v.acc', acc);

        component.set('v.expanded', false);
        component.set('v.expandAll', true);
    },

    toggleSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronright': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronright',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

    toggleSubSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronup': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronup',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

    toggleLayout: function(component, event, helper) {
        let newLayout = component.get("v.toggleLayout");
        component.set("v.toggleLayout", newLayout);
    },

    updateJobTitleOnSummaryTab: function(component, event, helper) {
        //console.log('handler called');

        var contactRecord = component.get("v.context");

        var oldTitle = event.getParam("oldValue");
        var currentTitle = event.getParam("value");

        //console.log(oldTitle);
        //console.log(currentTitle);

        //console.log('in event firing');
        //console.log(contactRecord);
        //console.log('after log');

        if(oldTitle !== currentTitle && component.get("v.context") === "modal") {
            var updateTitleEvent = $A.get("e.c:E_UpdateJobTitleOnSummaryTab");
            //console.log('I am here');
            if(updateTitleEvent) {
                updateTitleEvent.setParam("newTitle", currentTitle);
                updateTitleEvent.fire();
                //console.log('event fired');
            }
        }
    },
    
    // pass the component, Picklist field Name and Picklist_Tag_Id(Aura:Id)
   doInit: function(component, event, helper) {
        if(component.get("v.context") !== "modal") {
            helper.initializeAddEditTalent(component);// for defect D-07495 
        } else {
            if(component.get("v.account.Do_Not_Contact__c")) {
                helper.onDNCCheck(component);
            }

        }
        helper.getCurrentUser(component);
        helper.getPicklist(component, 'Salutation', 'salutation');
        helper.getCountryMappings(component); //S-65378 - Added by KK
		
		//S-85004 - URL isAddressable - Karthik
        var myPageRef = component.get("v.pageReference");
        if(myPageRef){
            var mode = myPageRef.state.c__mode;
            if (mode !== null && mode !== "") {
                component.set("v.mode", mode);
                if (mode === "upload") {
                    var isResumeParsed = myPageRef.state.c__isResumeParsed;
                    component.set ("v.isResumeParsed", isResumeParsed);
                }
            }

            var accountId = myPageRef.state.c__accountId;
            //console.log('accountId '+accountId);
            if (accountId !== null && accountId !== undefined) {
                component.set("v.accountId", accountId);

                helper.getTalentData(component, accountId);
                

            }

            var filterFields = myPageRef.state.c__filteringFields;
            //console.log('filterFields '+filterFields);
            if (filterFields !== null){
                component.set("v.filteringFields", filterFields);
                helper.copyFilterValues(component);
            }
        }else{
            let accid=component.get("v.accountId");
            if(accid != null){
                var context = component.get("v.context");
               if(context !== "modal") {
                   //console.log('not caught...');
                   helper.getTalentData(component, accid); 
               }
            }
            
        }
        //sandeep: critical update changes End....
        var accountId = helper.getParameterByName(component, event, "id");
        if (accountId !== null) {
            component.set("v.accountId", accountId);
            var context = component.get("v.context");
            if(context !== "modal") {
                //console.log('not caught...');
                helper.getTalentData(component, accountId);
            }
        }

        var relatedContactElement = component.find("relatedClient");
        $A.util.toggleClass(relatedContactElement, "slds-hide");
        
        // Set the attribute to enable error messages in Google Autocomplete
        component.set("v.inTalentLocation", 'true');
    },

	//S-85004 - URL isAddressable - Karthik
	onPageReferenceChanged: function(component, event, helper) {
		var myPageRef = component.get("v.pageReference");
        if(myPageRef){
            var mode = myPageRef.state.c__mode;
            if (mode !== null && mode !== "") {
                component.set("v.mode", mode);
                if (mode === "upload") {
                    var isResumeParsed = myPageRef.state.c__isResumeParsed;
                    component.set ("v.isResumeParsed", isResumeParsed);
                }
            }

            var accountId = myPageRef.state.c__accountId;
            if (accountId !== null && accountId !== undefined) {
                component.set("v.accountId", accountId);
                helper.getTalentData(component, accountId);
            }

            var filterFields = myPageRef.state.c__filteringFields;
            if (filterFields !== null){
                component.set("v.filteringFields", filterFields);
                helper.copyFilterValues(component);
            }
        }
	},

    saveModalClose : function(component, event, helper) {
        helper.saveTalent (component);
    },


    onDNCCheck : function(component, event, helper) {
        helper.onDNCCheck(component)
    },    

    onMobilePhoneCheck : function(component, event, helper) {
        var checkCmp = component.find("homePhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("workPhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("otherPhonepreferred");
        checkCmp.set("v.value", false);
    },

    onHomePhoneCheck : function(component, event, helper) {
        var checkCmp = component.find("mobilePhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("workPhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("otherPhonepreferred");
        checkCmp.set("v.value", false);
    },

    onWorkPhoneCheck : function(component, event, helper) {
        var checkCmp = component.find("mobilePhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("homePhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("otherPhonepreferred");
        checkCmp.set("v.value", false);
    },
    
    onOtherPhoneCheck : function(component, event, helper) {
        var checkCmp = component.find("mobilePhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("homePhonepreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("workPhonepreferred");
        checkCmp.set("v.value", false);
    },

    onEmailCheck : function(component, event, helper) {
        var checkCmp = component.find("otherEmailPreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("workEmailPreferred");
        checkCmp.set("v.value", false);
    },

    onOtherEmailCheck : function(component, event, helper) {
        var checkCmp = component.find("emailPreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("workEmailPreferred");
        checkCmp.set("v.value", false);
    },
	
    onWorkEmailCheck : function(component, event, helper) {
        var checkCmp = component.find("emailPreferred");
        checkCmp.set("v.value", false);
        checkCmp = component.find("otherEmailPreferred");
        checkCmp.set("v.value", false);
    },
    
    deleteRelatedContact: function(component, event, helper) {
       	//This method in the helper hides the div and make the related Client field empty
        helper.deleteRelatedContact(component);
        // Make the div visible again so that newly selected related client can be displayed
        var relatedContactElement = component.find("relatedClient");
        $A.util.toggleClass(relatedContactElement, "slds-hide");
    },

	updateMailingCountryCode: function(cmp, event, helper) {
	    helper.updateMailingCountryCode(cmp,event);
    },
    
    redirectToPreviousPage : function(component, event, helper) {
        //Neel summer 18 URL change->
    	/* var urlEvent = $A.get("e.force:navigateToURL");
        if (component.get("v.accountId") && component.get("v.mode") !== "upload") {
            urlEvent.setParams({
              "url": "/one/one.app#/sObject/" + component.get("v.accountId") +"/view",
               "isredirect":'true'
            });
        } else {
            urlEvent.setParams({
              "url": "/one/one.app#/n/Find_or_Add_Candidate",
               "isredirect":'true'
            });
        }    
        urlEvent.fire(); */
        var urlEvent = $A.get("e.force:navigateToURL");
        var accountId = component.get("v.accountId");
        var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
        var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
        var dURL;
        
        if(isFlagOn === "true") {
			  dURL = ligtningNewURL+"/r/Account/" + accountId +"/view";
		}
		  else {
			  dURL = ligtningNewURL+"/sObject/" + accountId +"/view"
		}
		
        if (component.get("v.accountId") && component.get("v.mode") !== "upload") {
            urlEvent.setParams({
                "url": dURL,
                "isredirect":'true'
            });
        } else {
            urlEvent.setParams({
            	
                //"url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Find_or_Add_Candidate",
                "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Find_or_Add_Candidate",
                "isredirect":'true'
            });
        }    
        urlEvent.fire();  
        //Neel summer 18 URL change end->
    },

    killComponent : function(component, event, helper) {
        if (component.isValid()) {
            //component.destroy();
        }
    },
	//S-65378 - Added by KK - Update Mailing fields on location selection
    handleLocationSelection : function(component,event, helper) {
		component.set("v.isGoogleLocation", true);
        var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode,countrymap,displayText;
		mailingstreet = '';
		streetnum = '';
		streetname = '';
		city = '';
		state = '';
		country = '';
		countrycode = '';
		postcode = '';
		displayText = '';
		var addr = event.getParam("addrComponents");
		var latlong = event.getParam("latlong");    //S-96255 - Update MailLat, MailLong fields on Contact - Karthik    
		countrymap = component.get("v.countriesMap"); 
		displayText = event.getParam("displayText");
        //console.log('address component'+JSON.stringify(addr));
        for (var i=0; i<addr.length; i++) {
            if (addr[i].types.indexOf("street_number") >= 0) {
                streetnum = addr[i].long_name;
            }
            if (addr[i].types.indexOf("route") >= 0) {
                streetname = addr[i].long_name;
            }        
            if (addr[i].types.indexOf("postal_code") >= 0) {
                postcode = addr[i].long_name;
            }
            if (addr[i].types.indexOf("locality") >= 0) {
                city = addr[i].long_name;
            }
            if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
                state = addr[i].long_name;
            }
            if (addr[i].types.indexOf("country") >= 0) {
                country = addr[i].long_name;
                countrycode = countrymap[addr[i].short_name];
            }        
        }
        //console.log('teststreet',streetnum,streetname);
        if(streetnum) {
            mailingstreet = streetnum;
        } 
        if(streetnum && streetname) {
            mailingstreet = mailingstreet + ' ';
        }
        if(streetname) {
            if(mailingstreet != ''){
                mailingstreet = mailingstreet + streetname;        
            } else {
                mailingstreet = streetname;
            }
        }
        //console.log(mailingstreet,city,state,countrycode,country,postcode,displaytext);
        component.set("v.contact.MailingStreet",mailingstreet);
        component.set("v.contact.MailingCity",city);
        component.set("v.contact.MailingState",state);
        component.set("v.contact.MailingCountry",countrycode);
        component.set("v.contact.Talent_Country_Text__c",country);
        component.set("v.contact.MailingPostalCode",postcode);
		//var str = displayText.substr(0,displayText.indexOf(',')); // added to display only street address - Manish
        component.set("v.presetLocation",mailingstreet);
		component.set("v.streetAddress2","");
		//S-96255 - Update MailLat, MailLong fields on Contact
		if(latlong && (mailingstreet || city || postcode)){
			//component.set("v.contact.GoogleMailingLatLong__Latitude__s",latlong.lat);
			//component.set("v.contact.GoogleMailingLatLong__Longitude__s",latlong.lng);
			component.set("v.contact.MailingLatitude",latlong.lat);
			component.set("v.contact.MailingLongitude",latlong.lng);				
		} else {
			component.set("v.contact.MailingLatitude",null);
			component.set("v.contact.MailingLongitude",null);				
		}
		component.set("v.contact.LatLongSource__c","Google");
		component.set("v.viewState","EDIT");
		component.set("v.parentInitialLoad",true);
		var countryCode = helper.getKeyByValue(component.get("v.countriesMap"),countrycode);
		component.set("v.MailingCountryKey",countryCode);
    }

	

})