({
        
    initializeAddEditTalent : function(component) {
        component.set("v.contact", {'sobjectType':'Contact', 'FirstName':'', 'MiddleName': '', 'LastName':'','Other_Email__c':'', 'Email':'','Salutation':'', 'LinkedIn_URL__c':'','Website_URL__c':'', 'Title':'','MiddleName':'','HomePhone' : '', 'Phone': '', 'MobilePhone':'','MailingCountry':'', 'MailingStreet':'', 'MailingCity':'','MailingState':'','MailingPostalCode':'',
                                    'Talent_State_Text__c': '', 'Talent_Country_Text__c': '','Work_Email__c' :'','OtherPhone' :'', 'Related_Contact__c' :  null,
      'Preferred_Email__c': '', 'Preferred_Phone__c': '', 'Preferred_Name__c' : '' ,'Id': null});

        if(component.get("v.context") !== 'modal') {
            component.set("v.account", {'sobjectType':'Account', 'Name':'', 'Talent_Gender__c':'', 'Talent_Race__c':'', 'Talent_Gender_Information__c':'', 'Talent_Race_Information__c':'', 'Do_Not_Contact__c':'', 'Do_Not_Contact_Reason__c': '', 'Do_Not_Contact_Date__c': '', 'Talent_Source_Other__c': '', 'Id': null});
        } else {
            

        }
	
        component.set("v.homePhonePreferred", false);
        component.set("v.mobilePhonePreferred", false);
        component.set("v.workPhonePreferred", false);
        component.set("v.otherPhonePreferred", false);
        component.set("v.emailPreferred", false);
        component.set("v.otherEmailPreferred", false);
        component.set("v.workEmailPreferred", false);
    }, 
    
    //S-65378 - Added by KK - Get country map data on component init
    getCountryMappings : function(component) {   
        var action = component.get("c.getCountryMappings");
        
        action.setCallback(this,function(response) {
            component.set("v.countriesMap",response.getReturnValue());
			if(component.get("v.contact.Talent_Country_Text__c").length == 0 && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
			component.set("v.contact.Talent_Country_Text__c",'United States');
			component.set("v.MailingCountryKey",'US');	
		}   
        });
        $A.enqueueAction(action);
    },

	updateMailingCountryCode: function(cmp, event) {
		var conKey = cmp.get('v.MailingCountryKey');
		var countrymap = cmp.get("v.countriesMap");
		var countrycode ;
		if (conKey !== null && typeof conKey !== 'undefined') {
				countrycode = countrymap[conKey];
				if(countrycode !== null && typeof countrycode !== 'undefined'){
					cmp.set("v.contact.MailingCountry",countrycode);
				}
			}
	},

	getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
				var user = response.usr;
                component.set ("v.runningUser", user);

               
				component.set ("v.usrOwnership", response.userOwnership);


                /* Start for story S-79062 */
                component.set("v.usrCategory",response.userCategory);
                /* End for story S-79062 */
                if (user.Profile.Name === 'Single Desk 2' ||
                    user.Profile.Name === 'System Administrator') {
                    this.getAccountPicklist(component, 'Talent_Gender__c', 'Gender'); 
                    this.getAccountPicklist(component, 'Talent_Race__c', 'Race'); 
                    this.getAccountPicklist(component, 'Talent_Race_Information__c', 'RaceInfo'); 
                    this.getAccountPicklist(component, 'Talent_Gender_Information__c', 'GenderInfo'); 
                } 
                if (user.Profile.Name !== 'ATS_Recruiter') {
                    this.getAccountPicklist(component, 'Do_Not_Contact_Reason__c', 'Reason');
                }        
            }
        });
        $A.enqueueAction(action);
    },

    /*getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                component.set ("v.runningUser", response);
                if (response.Profile.Name === 'Single Desk 2' ||
                    response.Profile.Name === 'System Administrator') {
                    this.getAccountPicklist(component, 'Talent_Gender__c', 'Gender'); 
                    this.getAccountPicklist(component, 'Talent_Race__c', 'Race'); 
                    this.getAccountPicklist(component, 'Talent_Race_Information__c', 'RaceInfo'); 
                    this.getAccountPicklist(component, 'Talent_Gender_Information__c', 'GenderInfo'); 
                } 
                if (response.Profile.Name !== 'ATS_Recruiter') {
                    this.getAccountPicklist(component, 'Do_Not_Contact_Reason__c', 'Reason');
                }        
            }
        });
        $A.enqueueAction(action);
    },*/

    getParameterByName : function(cmp,event,name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    getTalentData : function(component, accountId) {
       var account = component.get("v.account");
       account.Id = accountId;
       component.set("v.account", account); 
       var serverMethod = 'c.getTalent';
        var params = {
            "accountId": component.get("v.accountId")
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.isShow",false);
                var talent = response.getReturnValue();
                if(talent != null){
                if(component.get("v.mode") ==="upload" && talent.MailingState){
                    var mstate = talent.MailingState;
                    if(mstate.indexOf(".") > -1){
                    	talent.MailingState= talent.MailingState.slice(3);// For Defect D-07493 removing Country Code from State if from Resume Upload.    
                    }                    
                }

				if(component.get("v.mode") ==="upload") {
					component.set("v.viewState","EDIT");
				}
				
                this.translateTalent(component, talent, account);
                }else{
                    component.set("v.isResumeParsed","N")
                }
            } 
            else if(state === "ERROR"){
                if ((component.get("v.mode") ==="upload" 
                    && component.get("v.isResumeParsed") === "Y") 
                    || (component.get("v.mode") !=="upload")){
                    this.checkError(response, component);
                }
            }
        });
        $A.enqueueAction(action);
    },

    translateTalent : function(component, talent, account) {
        this.translateContact(component, talent);
        this.translateAccount(component, talent, account);
    },

    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

    saveTalent : function(component) {
        
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
		$A.util.removeClass(spinner, 'slds-hide');

        this.clearServerValidationError(component);    
		var contact = component.get("v.contact");
		this.setLatLongAddress(component);	
		
		var mode = component.get("v.mode");
        var isResumeParsed = component.get("v.isResumeParsed");

        if (component.get("v.accountId") && !contact.Id && 
            mode==="upload" && isResumeParsed==="Y") {
            this.showError ("An unexpected error has occured, please contact helpdesk for support");
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide'); 
            return;
        } else if ((component.get("v.accountId") && !contact.Id && mode!=="upload") 
            || (!component.get("v.accountId") && contact.Id)) {
            this.showError ("An unexpected error has occured, please contact helpdesk for support");
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide'); 
            return;
        } 

        contact = this.updatePreferredValues(contact, component);
        //contact = this.updateState(contact, component);
        var serverMethod = 'c.saveTalentRecord';
        var params = {
            "contact": contact, 
            "account": component.get("v.account")
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        
       action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.isShow",false);
                var contactId = response.getReturnValue();
                
                /*Start For defect D-07495 On successful save Reset the Attribute variable */
                this.initializeAddEditTalent(component);
                
                /*End for defect D-07495 */
                
                
                // console.log('Saved Account ID ' + accountId);
                //Neel summer 18 URL change->  
                /*var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/one/one.app#/sObject/" + accountId +"/view"
                });
                */
                var urlEvent = $A.get("e.force:navigateToURL");
                var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
                var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
                var dURL;

                
                if(isFlagOn === "true") {
	    			  dURL = ligtningNewURL+"/r/Contact/" + contactId +"/view";
	    		  }
	    		  else {
	    			  dURL = ligtningNewURL+"/sObject/" + contactId +"/view"
	    		  }
                urlEvent.setParams({                	
                	"url": dURL                  
                });
                //Neel change end 
                urlEvent.fire();
                component.destroy(); 
            } else if (state === "ERROR") {
                this.checkError(response, component);
            } 
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide'); 
        });
        $A.enqueueAction(action);
    },   

    saveTalentModal : function(component) {
        var ev = $A.get("e.c:E_ShowSpinnerOnModal");
        ev.fire();
        component.set('v.showSpinner', true);
        var contactId = component.get("v.contactId");
		this.setLatLongAddress(component);		
        var contact = component.get("v.contact");
        var account = component.get("v.account");
        var g2Comments;        
        if(component.isValid()){
            this.savePickList(component);
            this.populateLanguageCode(component);
            
            component.set("v.account.Talent_Preference_Internal__c",JSON.stringify(component.get("v.record")));
            component.set("v.account.Skills__c",JSON.stringify(component.get("v.currentSkills")));
            
            if (component.get("v.isG2Checked")) {
                if (component.get("v.isG2DateToday")) {
                    component.set("v.account.G2_Completed__c", false);
                } else {
                    component.set("v.account.G2_Completed__c", true);
                }
            } else {
                component.set("v.account.G2_Completed__c", false);
            }
            
            var dnc = component.get("v.account.G2_Completed__c");
            //console.log('final bool: ', dnc);
            
            //account.Do_Not_Contact__c = component.get("v.isChecked");
            //component.set("v.account", account);
            
            
            
            var g2Comments = component.get("v.g2Comments");
            
            this.clearServerValidationError(component);    
            
            contact = this.updatePreferredValues(contact, component);
            //contact = this.updateState(contact, component);
            
            var serverMethod = 'c.saveTalentRecordModal';
            var params = {
                "contact": contact, 
                "account": component.get("v.account"),
                "g2Comments" : g2Comments
            };
            
            var acc = component.get("v.account");
            var action = component.get(serverMethod);
            action.setParams(params);
            
            action.setCallback(this,function(response) {
                //console.log('response from apex'+response);
                
                var state = response.getState();
                if (state === "SUCCESS") { 
                    component.set("v.cssStyle", "");
                    component.set("v.callServer",true);

                    var appEvent = $A.get("e.c:RefreshTalentHeader");
                    appEvent.fire();

                    var closeEvent = $A.get("e.c:E_CloseTalentSummaryModal");
                    closeEvent.fire();
                    
                    $A.get('e.force:refreshView').fire();
                    component.destroy();
                    
                } 
                else if (state === "ERROR") {
                    this.checkError(response, component);
                } 
                //$A.util.removeClass(spinner, 'slds-show');
                //$A.util.addClass(spinner, 'slds-hide'); 

                var toggle = $A.get("e.c:E_ShowSpinnerOnModal");
                toggle.fire();
            });
            $A.enqueueAction(action);
        }
    },

    validateAndSaveTalent : function(component) {
        var mode = component.get("v.mode"); 
        var context = component.get("v.context");
        if (this.validTalent(component)) {
            //Commenting as popup modal is not required
            /*if (mode !==null && mode === "upload" 
                && (component.get("v.runningUser.Profile.Name") === 'Single Desk 1'  
                || component.get("v.runningUser.Profile.Name") === 'Single Desk 2' 
                || component.get("v.runningUser.Profile.Name") === 'ATS_Recruiter')) {
                var theVal = component.get("v.saveTrigger")
                component.set("v.saveTrigger", !theVal);
            } else {
                this.saveTalent (component);
            }*/ 
            if(context === 'page') {
                this.saveTalent (component);   
            } else {
                this.saveTalentModal (component);
            }
        }    
    },
    
    
    checkError : function(response, component){    
        //console.log('checkError called ...'+JSON.stringify(response.getError()));
        var errors = response.getError();
        if (errors) {
            if (errors[0].fieldErrors) {
                this.showServerSideValidation(errors, component);   
            } else if (errors[0] && errors[0].message) {
                this.showError(errors[0].message, 'An error occured!');
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,'An error occured!');
            }else{
                this.showError('An unexpected error has occured. Please try again!');
            }
        } else {
            this.showError(errors[0].message);
            //throw new Error("Unknown Error");
        }
    },    

    showServerSideValidation : function(errors, component) {
        for (var fieldName in errors[0].fieldErrors) {
            if (fieldName === "LinkedIn_URL__c") {

                var linkedinUrlField = component.find("linkedinUrl");

                linkedinUrlField.set("v.errors", [{message: "Please enter the valid LinkedIn url."}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");

            } else if (fieldName === "Email") {

                var emailField = component.find("email");

                emailField.set("v.errors", [{message: "Please enter a valid email address."}]);
                emailField.set("v.isError",true);
                $A.util.addClass(emailField,"slds-has-error show-error-message");

            } else if (fieldName === "Other_Email__c") {

                var otherEmailField = component.find("otherEmail");

                otherEmailField.set("v.errors", [{message: "Please enter a valid email address."}]);
                otherEmailField.set("v.isError",true);
                $A.util.addClass(otherEmailField,"slds-has-error show-error-message");

            }  else if (fieldName === "Work_Email__c") {

                var workEmailField = component.find("workEmail");

                workEmailField.set("v.errors", [{message: "Please enter a valid email address."}]);
                workEmailField.set("v.isError",true);
                $A.util.addClass(workEmailField,"slds-has-error show-error-message");

            } else {
                this.showError (errors[0].fieldErrors[fieldName][0].message, errors[0].fieldErrors[fieldName][0].statusCode);
            }
        }  //end of field errors forLoop                   

     },   

    clearServerValidationError : function(component) {
        var linkedinUrlField = component.find("linkedinUrl");
        linkedinUrlField.set("v.errors", []);
        linkedinUrlField.set("v.isError", false);
        $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");

        var emailField = component.find("email");
        emailField.set("v.errors", []);
        emailField.set("v.isError", false);
        $A.util.addClass(emailField,"slds-has-error show-error-message");

        var otherEmailField = component.find("otherEmail");
        otherEmailField.set("v.errors", []);
        otherEmailField.set("v.isError", false);
        $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
        
        var workEmailField = component.find("workEmail");
        workEmailField.set("v.errors", []);
        workEmailField.set("v.isError", false);
        $A.util.addClass(workEmailField,"slds-has-error show-error-message");

    },    

    updatePreferredValues : function(contact, component) {
        
        if (component.get("v.homePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Home';
        } else if (component.get("v.mobilePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Mobile';
        } else if (component.get("v.workPhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Work';
        } else if (component.get("v.otherPhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Other';
        }
        

        if (component.get("v.emailPreferred") === true) {
            contact.Preferred_Email__c = 'Email';
        } else if (component.get("v.otherEmailPreferred") === true) {
            contact.Preferred_Email__c = 'Alternate';
        }
        else if (component.get("v.workEmailPreferred") === true) {
            contact.Preferred_Email__c = 'Work';
        }

        if(component.get("v.pageReference") && component.get("v.pageReference").state.c__isResumeParsed === 'Y'){
            contact.Source_Type__c = 'Uploaded';
        }else{
            contact.Source_Type__c = 'User Entered';
        }

        return contact;
    },

    validTalent: function(component) {
        var validTalent = true;
        validTalent = this.validateFirstName (component, validTalent);
        validTalent = this.validateLastName (component, validTalent);
        // validTalent = this.validateTitleError (component, validTalent);
        var expirationDateField = component.find("expirationDate");
        if (expirationDateField) {
            validTalent = this.validateExpirationDate (component, validTalent);
        }
        validTalent = this.validateLinkedInUrl (component, validTalent);
        validTalent = this.validateWebsiteUrl (component, validTalent);
        validTalent = this.validateStreetAddress(component, validTalent); // Added by Manish
        //S-59229 Validate Preferred Options - Added by Karthik
        validTalent = this.validatePrefOptions (component, validTalent);
        
        //S-226585 - added 3 new Opco IND,SJA,EAS
       // if (component.get("v.runningUser.OPCO__c") === 'ONS'  
          //  || component.get("v.runningUser.OPCO__c") === 'TEK') {
		   if (component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aerotek") 
		    || component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aston_Carter") 
			|| component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Newco")  
		    || component.get("v.runningUser.OPCO__c") === 'ONS' 
            || component.get("v.runningUser.OPCO__c") === 'TEK') {
              // var validTalent = this.validateZipAndState (component, validTalent);
            validTalent = this.validateStateCountry(component, validTalent);
                // for story S-96223 TEK or ONS user phone or email mandatory.
            validTalent = this.validatePhoneEmailLinkedInForTEKONS (component, validTalent); 
        }
        else{            
            // for Users other than TEK or ONS phone or email or LinkedIN url is mandatory
           validTalent = this.validatePhoneEmailLinkedIn (component, validTalent); 
        }
        /* Start for Strory S-79062 */
   //     validTalent = this.validateEMEAUser(component, validTalent);// for story S-83514
        /* End for Strory S-79062 */
        return validTalent;
    }, 

	validateStreetAddress : function(component, validTalent) {
		component.set("v.invalidLocation", false);
		if (component.get("v.streetAddress2") && !component.get("v.presetLocation")) {
			validTalent = false;
            // If location is not present or invalid, trigger error message / styles
            component.set("v.invalidLocation", true);
		}
		return validTalent;
	},
    // for story S-96223 TEK or ONS user phone or email mandatory.
    validatePhoneEmailLinkedInForTEKONS: function (component , validTalent){
       var mobilePhoneField = component.find("mobilePhone");
        var mobilePhone = mobilePhoneField.get("v.value");
        if(!this.validateMobilePhone(mobilePhone)) {
           mobilePhoneField.set("v.errors", [{message: "Please enter valid Mobile Phone."}]);
           mobilePhoneField.set("v.isError",true);
           validTalent = false;
        }

        var homePhoneField = component.find("homePhone");
        var homePhone = homePhoneField.get("v.value");
        if(!this.validateMobilePhone(homePhone)) {
           homePhoneField.set("v.errors", [{message: "Please enter valid Home Phone."}]);
           homePhoneField.set("v.isError",true);
           validTalent = false;
        }

        var workPhoneField = component.find("workPhone");
			if(workPhoneField){
				var workPhone = workPhoneField.get("v.value");
				var workPhoneValMessage = "Please enter a valid Work Phone.";
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if((workExtension !== '' && typeof workExtension !== 'undefined') && (workPhone === '' || typeof workPhone == 'undefined')  ){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						}
					
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			 if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			}		
		}

		var otherPhoneField = component.find("otherPhone");
        var otherPhone = otherPhoneField.get("v.value");
        if(!this.validateMobilePhone(otherPhone)) {
           otherPhoneField.set("v.errors", [{message: "Please enter valid Other Phone."}]);
           otherPhoneField.set("v.isError",true);
           validTalent = false;
        }
       
        var emailField = component.find("email");
        var email = emailField.get("v.value");
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: "Please enter valid Email."}]);
            emailField.set("v.isError",true);
            validTalent = false;
        }

        var otherEmailField = component.find("otherEmail");
        var otherEmail = otherEmailField.get("v.value");
        if(!this.validateEmail(otherEmail)) {
            otherEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
            otherEmailField.set("v.isError",true);
            validTalent = false;
        }
        var workEmailField = component.find("workEmail");
        var workEmail = workEmailField.get("v.value");
        if(!this.validateEmail(workEmail)) {
            workEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
            workEmailField.set("v.isError",true);
            validTalent = false;
        } 
        
        
        //var linkedinUrlField = component.find("linkedinUrl");
        //var linkedinUrl = linkedinUrlField.get("v.value");
        
        if (!mobilePhone && !homePhone && !workPhone && !otherPhone
            && !email && !otherEmail && !workEmail) {
            validTalent = false;
            this.showError("One of the contact method either Phone or Email is required.");
        } 
        return validTalent; 
    },
    /* Start for Strory S-79062 */
    validateEMEAUser : function(component,flag) {
        
        var UserCategory = component.get("v.usrCategory");
        
        //console.log('UserCategory::'+UserCategory);
        if(UserCategory === 'CATEGORY3'){
            //console.log('Inside CATEGORY3 ');
            var emailField = component.find("email");
            var email = emailField.get("v.value");
            //console.log('EMail value::'+email);
            
            var otherEmailField = component.find("otherEmail");
            var otherEmail = otherEmailField.get("v.value");
            //console.log('otherEmail value::'+otherEmail);
            
            var workEmailField = component.find("workEmail");
            var workEmail = workEmailField.get("v.value");
            //console.log('workEmail value::'+workEmail);
            
            if((!email || email == " ")&&(!otherEmail || otherEmail == " ")&&(!workEmail || workEmail == " ") ){                  
                //console.log('Inside if condition where all email blank ');
                
                flag = false;
                emailField.set("v.errors", [{message: "Email mandatory for EMEA User."}]);
                emailField.set("v.isError",true);
                $A.util.addClass(emailField,"slds-has-error show-error-message");
                this.showError("Email mandatory for EMEA User");
                
            }
            else{
                emailField.set("v.errors", []);
                emailField.set("v.isError", false);
                $A.util.removeClass(emailField,"slds-has-error show-error-message");
            }
        }
        
        //console.log('Before Returning the value::'+ flag);
        
        return flag;
    },
    /* End for Strory S-79062 */
    
    validateZipAndState : function(component, validTalent) {
        var validZip = true;
        var validState = true;

        var zipField = component.find("MailingPostalCode");
        if(zipField){
            var zip = zipField.get("v.value");
            if (!zip || zip === "") {
                validZip = false;
            }     
        }
        //S-65378 - Added by KK: Updated to use Mailing State instead of Talent_State_Text__c
        var state =  component.get("v.contact.MailingState");
     //   if(state){
            if (!state || state === "") {
                validState = false;
            }
       // }
        
        if (!validZip && !validState) {
            validTalent = false;
            this.showError("Please enter a valid Postal Code or a State.");
        }
        return validTalent;
    },    


    validateStateCountry : function(component, validTalent) {

     var state    =      component.get("v.contact.MailingState");
     var country  =      component.get("v.contact.Talent_Country_Text__c");

     var mailingCountry = component.get("v.contact.MailingCountry");

     if ( $A.util.isEmpty(state) || $A.util.isEmpty(country) ) {
            validTalent = false; 
            this.showError("Please enter a valid Country and State.");
      } else if (mailingCountry === '' ) {
            var countryMap = component.get("v.countriesMap");
            var countryCode = component.get("v.MailingCountryKey");
            if (countryMap && countryCode && countryCode.length > 0) {
               try {
                   var countryValue = countryMap[countryCode];
                   if (countryValue && countryValue.length > 0 ) {
                       component.set("v.contact.MailingCountry", countryValue);
                   } else {
                     validTalent = false; 
                     this.showError("Please enter a valid Country and State.");
                   }
               } catch(error) {
                   console.error(error);
                   validTalent = false;
                   this.showError("Please enter a valid Country and State."); 
               }
            } else {
                     validTalent = false;
                      this.showError("Please enter a valid Country and State."); 
                   }
      } 
      return validTalent;        
    },


	// validateTitleError : function(component, validTalent) {
 //        var hiddenField = component.find("hiddenTitle");
 //        var hidden = hiddenField.get("v.value");
 //        if (!hidden || hidden === "") {
 //            validTalent = false;
	// 		console.log('setting v.titleError to TRUE')
 //            component.set("v.titleError", true);
	// 		this.showError("Please enter a valid job title.")
 //        } else {
 //            component.set("v.titleError", false);
 //        }
 //        return validTalent;
 //    },



    validateFirstName : function(component, validTalent) {
        var firstNameField = component.find("firstName");
        var firstName = firstNameField.get("v.value");
        if (!firstName || firstName === "") {
            validTalent = false;
            firstNameField.set("v.errors", [{message: "Please enter a valid First Name"}]);
            firstNameField.set("v.isError",true);
            $A.util.addClass(firstNameField,"slds-has-error show-error-message");
        } else {
            firstNameField.set("v.errors", []);
            firstNameField.set("v.isError", false);
            $A.util.removeClass(firstNameField,"slds-has-error show-error-message");
        }
        return validTalent;
    },    

    validateLastName : function(component, validTalent) {
        var lastNameField = component.find("lastName");
        var lastName = lastNameField.get("v.value");
        if (!lastName || lastName === "") {
            validTalent = false;
            lastNameField.set("v.errors", [{message: "Please enter a valid Last Name"}]);
            lastNameField.set("v.isError",true);
            $A.util.addClass(lastNameField,"slds-has-error show-error-message");
        } else {
            lastNameField.set("v.errors", []);
            lastNameField.set("v.isError", false);
            $A.util.removeClass(lastNameField,"slds-has-error show-error-message");
        }
        return validTalent;
    },    
    
    validateExpirationDate : function(component, validTalent) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        
        if(dd<10) {
            dd = '0'+dd
        } 
        
        if(mm<10) {
            mm = '0'+mm
        } 
        
        today = yyyy + '-' + mm + '-' + dd;
        var expirationDateField = component.find("expirationDate");
        var expirationDate = expirationDateField.get("v.value");
        
       if (expirationDate < today && expirationDate != "") {
           validTalent = false;
           expirationDateField.set("v.errors", [{message: "Expiration Date can not be a Past Date."}]);
           expirationDateField.set("v.isError",true);
           $A.util.addClass(expirationDateField,"slds-has-error show-error-message");
       } else {
           expirationDateField.set("v.errors", []);
           expirationDateField.set("v.isError", false);
           $A.util.removeClass(expirationDateField,"slds-has-error show-error-message");
       }
       return validTalent;
    },    
    
    validateLinkedInUrl : function(component, validTalent){

        var linkedinUrlField = component.find("linkedinUrl");
        if(linkedinUrlField){
            var linkedinUrl = linkedinUrlField.get("v.value");    
        }

        linkedinUrlField.set("v.errors", []);
        linkedinUrlField.set("v.isError", false);
        $A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
        
        if(linkedinUrl && linkedinUrl != ""){
            // /(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
            // var linkedInRegex = new RegExp(".*(linkedin.com/).*", "i");
            // var linkedInRegex = new RegExp(/(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/);

            var linkedInRegex = new RegExp(/(http|https):\/\/\/?\S+$/);

            if (!linkedInRegex.test(linkedinUrl)) {
                validTalent = false;
                linkedinUrlField.set("v.errors", [{message: "Please enter valid LinkedIn url."}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");

                if(!linkedinUrl.includes('http')){
                    this.showError("Update URL link to include https:// or http:// for LinkedIn.", "Error");
                }else{
                    this.showError("Please Enter Valid URL format for LinkedIn Url.", "Error");
                }
                
                
            } else {
                linkedinUrlField.set("v.errors", []);
                linkedinUrlField.set("v.isError", false);
                $A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },

    
    validateWebsiteUrl : function(component, validTalent){

        var websiteUrlField = component.find("Website_URL__c");
        // alert(websiteUrlField);
        if(websiteUrlField){
            var websiteUrl = websiteUrlField.get("v.value");    
        }

        websiteUrlField.set("v.errors", []);
        websiteUrlField.set("v.isError", false);
        $A.util.removeClass(websiteUrlField,"slds-has-error show-error-message");
        
        if(websiteUrl && websiteUrl != ""){
            
            var websiteUrlRegex = new RegExp(/(http|https):\/\/\/?\S+$/);

            if (!websiteUrlRegex.test(websiteUrl)) {
                
                validTalent = false;
                websiteUrlField.set("v.errors", [{message: "Please enter valid Website url."}]);
                websiteUrlField.set("v.isError",true);
                $A.util.addClass(websiteUrlField,"slds-has-error show-error-message");

                if(!websiteUrl.includes('http')){
                    this.showError("Update URL link to include https:// or http:// for Website.", "Error");
                }else{
                    this.showError("Please Enter Valid URL format for website url.", "Error");
                }


            } else {
                websiteUrlField.set("v.errors", []);
                websiteUrlField.set("v.isError", false);
                $A.util.removeClass(websiteUrlField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },
    
    
    //S-59229 Validate Preferred Options - Added by Karthik
    validatePrefOptions : function(component, validTalent) {
        if (component.get("v.homePhonePreferred") === true) {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                var homePhone = homePhoneField.get("v.value");
                if(homePhone === "" || !homePhone){
                    validTalent = false;
                    homePhoneField.set("v.errors", [{message: "Please enter Home Phone."}]);
                    homePhoneField.set("v.isError",true);
                    $A.util.addClass(homePhoneField,"slds-has-error show-error-message");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    homePhoneField.set("v.errors", []);
                    homePhoneField.set("v.isError", false);
                    $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                homePhoneField.set("v.errors", []);
                homePhoneField.set("v.isError", false);
                $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.mobilePhonePreferred") === true) {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                var mobilePhone = mobilePhoneField.get("v.value");
                if(mobilePhone === "" || !mobilePhone){
                    validTalent = false;
                    mobilePhoneField.set("v.errors", [{message: "Please enter Mobile Phone."}]);
                    mobilePhoneField.set("v.isError",true);
                    $A.util.addClass(mobilePhoneField,"slds-has-error show-error-message");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    mobilePhoneField.set("v.errors", []);
                    mobilePhoneField.set("v.isError", false);
                    $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                mobilePhoneField.set("v.errors", []);
                mobilePhoneField.set("v.isError", false);
                $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.workPhonePreferred") === true) {
            var workPhoneField = component.find("workPhone");
            if(workPhoneField){
                var workPhone = workPhoneField.get("v.value");
                if(workPhone === "" || !workPhone){
                    validTalent = false;
                    workPhoneField.set("v.errors", [{message: "Please enter Work Phone."}]);
                    workPhoneField.set("v.isError",true);
                    $A.util.addClass(workPhoneField,"slds-has-error show-error-message");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    workPhoneField.set("v.errors", []);
                    workPhoneField.set("v.isError", false);
                    $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var workPhoneField = component.find("workPhone");
            if(workPhoneField){
                workPhoneField.set("v.errors", []);
                workPhoneField.set("v.isError", false);
                $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.otherPhonePreferred") === true) {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                var otherPhone = otherPhoneField.get("v.value");
                if(otherPhone === "" || !otherPhone){
                    validTalent = false;
                    otherPhoneField.set("v.errors", [{message: "Please enter Work Phone."}]);
                    otherPhoneField.set("v.isError",true);
                    $A.util.addClass(otherPhoneField,"slds-has-error show-error-message");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    otherPhoneField.set("v.errors", []);
                    otherPhoneField.set("v.isError", false);
                    $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                otherPhoneField.set("v.errors", []);
                otherPhoneField.set("v.isError", false);
                $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.emailPreferred") === true) {
            var emailField = component.find("email");
            if(emailField){
                var email = emailField.get("v.value");
                if(email === "" || !email){
                    validTalent = false;
                    emailField.set("v.errors", [{message: "Please enter Email."}]);
                    emailField.set("v.isError",true);
                    $A.util.addClass(emailField,"slds-has-error show-error-message");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    emailField.set("v.errors", []);
                    emailField.set("v.isError", false);
                    $A.util.removeClass(emailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var emailField = component.find("email");
            if(emailField){
                emailField.set("v.errors", []);
                emailField.set("v.isError", false);
                $A.util.removeClass(emailField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.otherEmailPreferred") === true) {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                var otherEmail = otherEmailField.get("v.value");
                if(otherEmail === "" || !otherEmail){
                    validTalent = false;
                    otherEmailField.set("v.errors", [{message: "Please enter Other Email."}]);
                    otherEmailField.set("v.isError",true);
                    $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    otherEmailField.set("v.errors", []);
                    otherEmailField.set("v.isError", false);
                    $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                otherEmailField.set("v.errors", []);
                otherEmailField.set("v.isError", false);
                $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.workEmailPreferred") === true) {
            var workEmailField = component.find("workEmail");
            if(workEmailField){
                var workEmail = workEmailField.get("v.value");
                if(workEmail === "" || !workEmail){
                    validTalent = false;
                    workEmailField.set("v.errors", [{message: "Please enter Other Email."}]);
                    workEmailField.set("v.isError",true);
                    $A.util.addClass(workEmailField,"slds-has-error show-error-message");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    workEmailField.set("v.errors", []);
                    workEmailField.set("v.isError", false);
                    $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var workEmailField = component.find("workPhone");
            if(workEmailField){
                workEmailField.set("v.errors", []);
                workEmailField.set("v.isError", false);
                $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
            }
        }
        
        return validTalent;
    },
    //S-59229 Validate Preferred Options - End

   validatePhoneEmailLinkedIn : function(component, validTalent) {
        var mobilePhoneField = component.find("mobilePhone");
        var mobilePhone = mobilePhoneField.get("v.value");
        if(!this.validateMobilePhone(mobilePhone)) {
           mobilePhoneField.set("v.errors", [{message: "Please enter valid Mobile Phone."}]);
           mobilePhoneField.set("v.isError",true);
           validTalent = false;
        }

        var homePhoneField = component.find("homePhone");
        var homePhone = homePhoneField.get("v.value");
        if(!this.validateMobilePhone(homePhone)) {
           homePhoneField.set("v.errors", [{message: "Please enter valid Home Phone."}]);
           homePhoneField.set("v.isError",true);
           validTalent = false;
        }

       var workPhoneField = component.find("workPhone");
			if(workPhoneField){
				var workPhone = workPhoneField.get("v.value");
				var workPhoneValMessage = "Please enter a valid Work Phone.";
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if((workExtension !== '' && typeof workExtension !== 'undefined') && (workPhone === '' || typeof workPhone == 'undefined')){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						}
					
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			 if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			}		
		}

		var otherPhoneField = component.find("otherPhone");
        var otherPhone = otherPhoneField.get("v.value");
        if(!this.validateMobilePhone(otherPhone)) {
           otherPhoneField.set("v.errors", [{message: "Please enter valid Other Phone."}]);
           otherPhoneField.set("v.isError",true);
           validTalent = false;
        }
       
        var emailField = component.find("email");
        var email = emailField.get("v.value");
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: "Please enter valid Email."}]);
            emailField.set("v.isError",true);
            validTalent = false;
        }

        var otherEmailField = component.find("otherEmail");
        var otherEmail = otherEmailField.get("v.value");
        if(!this.validateEmail(otherEmail)) {
            otherEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
            otherEmailField.set("v.isError",true);
            validTalent = false;
        }
        var workEmailField = component.find("workEmail");
        var workEmail = workEmailField.get("v.value");
        if(!this.validateEmail(workEmail)) {
            workEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
            workEmailField.set("v.isError",true);
            validTalent = false;
        } 
       

        var linkedinUrlField = component.find("linkedinUrl");
        var linkedinUrl = linkedinUrlField.get("v.value");

        if (!mobilePhone && !homePhone && !workPhone && !otherPhone
            && !email && !otherEmail && !workEmail && !linkedinUrl) {
                validTalent = false;
                this.showError("One of the contact method either Phone or Email or LinkedIn URL is required.");
        } 
        return validTalent;
    },       

     validateMobilePhone : function(mobilePhone) {
        if(mobilePhone && mobilePhone.length > 0) {
            var mp = mobilePhone.replace(/\s/g,'');
            if(mp.length >= 10 && mp.length <= 17) {
                
                var regex = new RegExp("^[0-9-.+()/]+$");
                var isValid = mp.match(regex);
                
                if(isValid != null) {
                    return true; 
                }
                else{
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true; 
    },   
	
   validateEmail : function(emailID){
        if(emailID && emailID.trim().length > 0) {
            
            var isValidEmail = emailID.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

            if(isValidEmail != null) {
                return true;
            }
            else {
                return false;
            } 

        }

        return true;
    },

   translateContact : function(component, talent) {
        try {
            var contact = component.get("v.contact");
            contact.Id = talent.Id;

            contact.FirstName = talent.FirstName ? talent.FirstName : '';
            contact.MiddleName = talent.MiddleName ? talent.MiddleName : '';
            contact.LastName = talent.LastName ? talent.LastName : '';
            contact.Preferred_Name__c = talent.Preferred_Name__c ? talent.Preferred_Name__c : '';
            contact.Other_Email__c = talent.Other_Email__c ? talent.Other_Email__c : '';
            contact.Work_Email__c = talent.Work_Email__c ? talent.Work_Email__c : '';
            contact.Email = talent.Email ? talent.Email : '';
            contact.Salutation = talent.Salutation ? talent.Salutation : '';
            contact.Suffix = talent.Suffix ? talent.Suffix : '';
            contact.LinkedIn_URL__c = talent.LinkedIn_URL__c ? talent.LinkedIn_URL__c : '';
            contact.Website_URL__c = talent.Website_URL__c ? talent.Website_URL__c : '';
            contact.Title = talent.Title ? talent.Title : '';
            contact.HomePhone = talent.HomePhone ? talent.HomePhone : '';
            contact.MobilePhone = talent.MobilePhone ? talent.MobilePhone : '';
            contact.Phone = talent.Phone ? talent.Phone : '';
			contact.WorkExtension__c =  talent.WorkExtension__c ? talent.WorkExtension__c : '';
            contact.OtherPhone = talent.OtherPhone ? talent.OtherPhone : '';
            contact.MailingCountry = talent.MailingCountry ? talent.MailingCountry : '';
            contact.Talent_Country_Text__c = talent.Talent_Country_Text__c ? talent.Talent_Country_Text__c : '';
            contact.MailingState = talent.MailingState ? talent.MailingState : '';
            contact.Talent_State_Text__c = talent.Talent_State_Text__c ? talent.Talent_State_Text__c : '';
            contact.MailingStreet = talent.MailingStreet ? talent.MailingStreet : '';
            contact.MailingCity = talent.MailingCity ? talent.MailingCity : '';
            contact.MailingPostalCode = talent.MailingPostalCode ? talent.MailingPostalCode : '';
            contact.Related_Contact__c = talent.Related_Contact__c ? talent.Related_Contact__c : '';
            if (talent.Related_Contact__r) {
                contact.Related_Contact__r = talent.Related_Contact__r.Name;
            }
            component.set("v.contact", contact);
            if (talent.Preferred_Phone__c) {
                if (talent.Preferred_Phone__c === 'Home') {
                    component.set("v.homePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Mobile') {
                    component.set("v.mobilePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Work') {
                    component.set("v.workPhonePreferred", true);
                }  else if (talent.Preferred_Phone__c === 'Other') {
                    component.set("v.otherPhonePreferred", true);
                }
            }    
            if (talent.Preferred_Email__c) {            
                if (talent.Preferred_Email__c === 'Email') {
                    component.set("v.emailPreferred", true);
                } else if (talent.Preferred_Email__c === 'Alternate') {
                    component.set("v.otherEmailPreferred", true);
                }  else if (talent.Preferred_Email__c === 'Work') {
                    component.set("v.workEmailPreferred", true);
                }
            } 
            // Added to split in Address 1 and 2
            var strs;
            if (contact.MailingStreet) {
                if( contact.MailingStreet.indexOf(',') != -1 ){
                    /*strs = contact.MailingStreet.split(',');
				}
				if (strs) { */
                    //console.log('reading address');
                    component.set("v.presetLocation",contact.MailingStreet.substr(0,contact.MailingStreet.indexOf(',')));
                    component.set("v.streetAddress2",contact.MailingStreet.indexOf(',')+1 != undefined ? contact.MailingStreet.substr(contact.MailingStreet.indexOf(',')+1).trim() : "");
                } else {
                    component.set("v.presetLocation",contact.MailingStreet);
                }
            }   else {
                component.set("v.presetLocation","");
            }           
            var countryCode = this.getKeyByValue(component.get("v.countriesMap"),contact.MailingCountry);
            component.set("v.MailingCountryKey",countryCode);
            //console.log('translateContact done');
        } catch (error) {
            contact.Id = null;
            component.set("v.contact", contact);
            this.showError ("An unexpected error has occured, please contact helpdesk for support - " + error.message);
            //console.log ("Error occured = " + error.message);
        }     
   },    
    
    translateAccount : function(component, talent, account) {
        try {
            //        var account = component.get("v.account");
            account.Name = talent.Account.Name ? talent.Account.Name : '';
            account.Talent_Gender__c = talent.Account.Talent_Gender__c;
            account.Talent_Race__c = talent.Account.Talent_Race__c;
            account.Talent_Gender_Information__c = talent.Account.Talent_Gender_Information__c;
            account.Talent_Race_Information__c = talent.Account.Talent_Race_Information__c;
            account.Do_Not_Contact__c = talent.Account.Do_Not_Contact__c;
            if (talent.Account.Do_Not_Contact__c) {
                this.onDNCCheck (component);
            } 
            account.Do_Not_Contact_Reason__c = talent.Account.Do_Not_Contact_Reason__c;
            account.Do_Not_Contact_Date__c = talent.Account.Do_Not_Contact_Date__c;
            account.Talent_Source_Other__c = talent.Account.Talent_Source_Other__c ? talent.Account.Talent_Source_Other__c : '';
            account.G2_Completed_By__c = talent.Account.G2_Completed_By__c ? talent.Account.G2_Completed_By__c : '';
            account.G2_Completed_Date__c = talent.G2_Completed_Date__c ? talent.G2_Completed_Date__c : '';
            account.G2_Completed__c = talent.Account.G2_Completed__c;
            
            
            // G2 
            var g2Date = "";
            var day = new Date().getDate();
            var month = new Date().getMonth() + 1;
            var year = new Date().getFullYear();
            
            var getDate = day.toString();
            if (getDate.length == 1){ 
                getDate = "0"+getDate;
            }
            var getMonth = month.toString();
            if (getMonth.length == 1){
                getMonth = "0"+getMonth;
            }
            
            var todaysDate = year + "-" + getMonth + "-" + getDate;
            
            if(typeof talent.Account.G2_Completed_Date__c !== "undefined"){
                var tempDate = talent.Account.G2_Completed_Date__c.split("T");
                if(tempDate.length > 0){
                    g2Date = tempDate[0];
                    var todayInMilliSeconds = new Date(todaysDate).getTime(); 
                    var g2DateInMilliSeconds =  new Date(talent.Account.G2_Completed_Date__c).getTime();
                    component.set("v.isG2DateToday", (todayInMilliSeconds === g2DateInMilliSeconds));
                    component.set("v.isG2Checked", (todayInMilliSeconds === g2DateInMilliSeconds));
                    
                }
            }
            //        account.Id = talent.AccountId;
            
            component.set("v.account", account);
            //console.log('translateAccount done');
        } catch (error) {
            component.set("v.account", account);
            this.showError ("An unexpected error has occured, please contact helpdesk for support - " + error.message);
            //console.log ("Error occured = " + error.message);
        }     
    },    
    
    onDNCCheck : function(component) {
        var dncFlagField = component.find("dncFlag");
        var dncFlag = dncFlagField.get("v.value");
        if (!dncFlag) {
            var account = component.get("v.account");
            account.Do_Not_Contact_Reason__c = "";
            account.Do_Not_Contact_Date__c = "";
            component.set("v.account", account);
        }

        var dncElement = component.find("dncDivIdReason");
        $A.util.toggleClass(dncElement, "slds-hide");
        var dncElement = component.find("dncDivIdExpDate");
        $A.util.toggleClass(dncElement, "slds-hide");
    },    

    getPicklist: function(component, fieldName, elementId) {
        var serverMethod = 'c.getPicklistValues';
        var params = {
            "objectStr": "Contact",
            "fld": fieldName
        };
        var action = component.get(serverMethod);
        action.setParams(params);

        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }

                component.find("salutation").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    getAccountPicklist: function(component, fieldName, elementId) {
        var serverMethod = 'c.getPicklistValues';
        var params = {
            "objectStr": "Account",
            "fld": fieldName
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                if(elementId == "Gender")
                    component.find("Gender").set("v.options", opts);
                if(elementId == "GenderInfo")
                    component.find("GenderInfo").set("v.options", opts);
                
                if(elementId == "Race")
                    component.find("Race").set("v.options", opts);
                if(elementId == "RaceInfo")
                    component.find("RaceInfo").set("v.options", opts);
                // if(elementId == "Reason")
                //     component.find("Reason").set("v.options", opts);
                
            }
        });
        $A.enqueueAction(action);
    },
    
    deleteRelatedContact : function(component) {
        var contact = component.get("v.contact");
        contact.Related_Contact__c = "";
        contact.Related_Contact__r = "";
        component.set("v.contact", contact);
        var relatedContactElement = component.find("relatedClient");
        $A.util.toggleClass(relatedContactElement, "slds-hide");

    },
    
    copyFilterValues : function(component) {
        //console.log("------CONTACT-----" + JSON.stringify( component.get('v.contact' ) ) );
		component.set('v.contact.FirstName', 			component.get("v.filteringFields.FirstName"));
        component.set('v.contact.LastName', 			component.get("v.filteringFields.LastName"));
        component.set('v.contact.Preferred_Name__c',	component.get("v.filteringFields.Preferred_Name__c"));
        component.set('v.contact.MobilePhone', 			component.get("v.filteringFields.Phone"));
        component.set('v.contact.Email',				component.get("v.filteringFields.Preferred_Email__c"));
        component.set('v.contact.Title',				'');
        
   },

   getKeyByValue : function(map, searchValue) {
		for(var key in map) {
			if (map[key] === searchValue) {
				return key;
			}
		}
	},

	setLatLongAddress : function (component) {
		var contact = component.get("v.contact");
		if(!component.get("v.isGoogleLocation")){
			component.set("v.contact.MailingLatitude",null);
			component.set("v.contact.MailingLongitude",null);
			if (contact.LatLongSource__c != null && contact.MailingCity == null && contact.MailingPostalCode == null) {
				component.set("v.contact.LatLongSource__c","");
			}	
		}
		var address = component.get("v.presetLocation");
		if (component.get("v.streetAddress2")) {
			address = address  +', '+component.get("v.streetAddress2");			
		}		
		component.set("v.contact.MailingStreet",(address != undefined ? address : ""));
	},

    toggleAccordion: function(component, event, toggleState) {
        var toggleState;

        var acc = component.get("v.acc");    
        var accordionId = event.currentTarget.id;
        var icon = acc[accordionId].icon;
        var className = acc[accordionId].className;     
                
        component.set(`v.acc[${accordionId}].icon`, toggleState[icon]);
        component.set(`v.acc[${accordionId}].className`, toggleState[className]);    
    }

})