({
	callServer: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			helper.callServer(component, parameters.callingCmp, parameters.className, parameters.subClassName, parameters.methodName, parameters.callback, parameters.params, parameters.storable, parameters.errCallback);
		}
	}
})