({
	callServer : function(component, callingCmp, className, subClassName, methodName, callback, params, storable, errCallback) {
        var serverMethod = 'c.performServerCall';
        var actionParams = {'className':className,
                            'subClassName':subClassName,
                            'methodName':methodName.replace('c.',''),
                            'parameters': params};


        var action = component.get(serverMethod);
        action.setParams(actionParams);

		if(methodName === 'getRecord'
            || methodName === 'getTalentOverviewModel'
			|| methodName === 'getCurrentUserWithOwnership'
			|| methodName === 'getTalentBaseFields'
			|| methodName === 'getLanguages'
			|| methodName === 'getCurrencyList'
			|| methodName === 'getDocumentListModal') {
        action.setBackground();
		}
        
        if(storable){
            action.setStorable();
        }else{
            action.setStorable({"ignoreExisting":"true"});
        }
      
        action.setCallback(this,function(response) {
            var state = response.getState();
             if (state === "SUCCESS") { 
                // pass returned value to callback function
                callback.call(this, response.getReturnValue());   
                
            } else if (state === "ERROR") {
                // Use error callback if available
                // perform null check before passing value
                if (typeof errCallback !== "undefined" && errCallback !== null) {
                    errCallback.call(this, response.getReturnValue());
                  // return;
                }

                // Fall back to generic error handler
                var errors = response.getError();
                if (errors) {
                    console.log("Errors", errors);
                    if (errors[0] && errors[0].message) {
                        this.showError(errors[0].message);
                        //throw new Error("Error" + errors[0].message);
                    }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                        this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                    }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                        this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                    }else{
                        this.showError('An unexpected error has occured. Please try again!');
                    }
                } else {
                    this.showError(errors[0].message);
                    //throw new Error("Unknown Error");
                }
            }
           
        });
    
        $A.enqueueAction(action);
    }, 

	showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
})