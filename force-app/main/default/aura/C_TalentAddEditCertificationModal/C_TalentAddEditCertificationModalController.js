({
	displayAddCertDetail : function(component, event, helper) {
		//Toggle CSS styles for opening Modal
		component.set("v.modalTitle", "Add Certification");
        helper.toggleClass(component,'backdropAddCertDetail','slds-backdrop--');
		
		helper.initializeCertModal(component);
		helper.toggleClass(component,'modaldialogAddCertDetail','slds-fade-in-');
        var recordId = event.getParam("recordId");
        component.set("v.recordId", recordId);
	},

    displayEditCertDetail : function(component,event,helper){
        var itemId = event.getParam("itemID");
        component.set("v.modalTitle", "Edit Certification");
        helper.initializeCertModal(component);
        component.set("v.id", itemId);
        helper.getCertificationToEdit(component);
        helper.toggleClass(component,'backdropAddCertDetail','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddCertDetail','slds-fade-in-');
    },

    valueSelected : function(component, event, helper) {
       component.set("v.Certification", event.getParam("value"));
       
    },
    hideModal : function(component, event, helper){
        //Toggle CSS styles for hiding Modal
		helper.toggleClassInverse(component,'backdropAddCertDetail','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialogAddCertDetail','slds-fade-in-');
		var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
        helper.resetCertLookUp(component);
    },
    
    saveCert : function(component, event, helper){
		helper.validateAndSaveCert(component);
        
    }

    ,validateCert:function(component,event,helper){
    	helper.validateCert(component);
    }
})