({

    initializeCertModal: function(component,componentId,className) {
		component.find("inputYearId").set("v.value", '');
        component.find("inputYearId").set("v.errors", null);
        component.find("notesId").set("v.value", '');
        component.find("notesId").set("v.errors", null);
        component.set("v.certerrors",null);
        component.set("v.id", null);

        this.createLookupComponent(component);
	},

    createLookupComponent : function(component) {
        //<c:C_LookupLtng type="Global_LOV__c" recordType="CertificationList"  comboKey="{!v.Certification}" comboValue="{!v.Certification}" aura:id="certId" lookupId="certId" allowFreeType="true" />
                              
        var componentName = "c:C_LookupLtng";
        var componentParams = {"type" : "Global_LOV__c", 
                                "recordType" : "CertificationList",
                                "aura:id" : "certId",
                                "lookupId" : "certId",
                                "allowFreeType" : true,
                                "executeBlur" : true,
                                "comboKey" : component.getReference("v.Certification"),
                                "comboValue" : component.getReference("v.Certification"),
                               "class" : "slds-input"
                              };
        $A.createComponent(componentName, componentParams, 
            function(newComponent, status, errorMessage) {
                if (status === "SUCCESS") {
                    // SUCCESS  - insert component in the body
                    var body = component.get("v.body");
                    body.push(newComponent);
                    component.set("v.body", newComponent);

                    var dynamicComponentsByAuraId = {};
                    dynamicComponentsByAuraId[newComponent.getLocalId()] = newComponent;
                    component.set("v.dynamicComponentsByAuraId", dynamicComponentsByAuraId);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
        });
    },

    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
		$A.util.removeClass(modal,className+'hide');
		$A.util.addClass(modal,className+'open');
	},
 
    toggleClassInverse: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.addClass(modal,className+'hide');
		$A.util.removeClass(modal,className+'open');
	},
    
   validateAndSaveCert: function(component) {
       if (this.validateCert(component)) {
           this.saveCert(component);
            this.toggleClassInverse(component,'backdropAddCertDetail','slds-backdrop--');
            this.toggleClassInverse(component,'modaldialogAddCertDetail','slds-fade-in-');
                       
       }
	},
    
    resetCertLookUp: function(component){
        var certField = component.get("v.body");//component.find("certId");
        $A.util.removeClass(certField,"slds-has-error show-error-message");
        //console.log(certField[0].find("certId").isValid());
        //if(certField[0].find("certId").isValid()){
          if(certField[0] && certField[0].find("certId") != null && certField[0].find("certId") != undefined 
           && certField[0].find("certId").isValid()){//added new condition and commented old by akshay for code merge
          certField[0].find("certId").destroy();  
         }
        
        component.set("v.certerrors", null);
        component.set("v.Certification",''); 
        //this.createLookupComponent(component);
    },
    
    validateCert: function(component) {
        var validCert = true;
        // Validate Year - The graduation year must be between 1940 and 2050
        var yearField = component.find("inputYearId");
        var year = yearField.get("v.value");
        if ($A.util.isEmpty(year) === false && (isNaN(year) || year < 1940 || year > 2050)) {
            validCert = false;
            yearField.set("v.errors", [{message:"{!$Label.c.ATS_GRAD_YEAR_ERROR}"}]); 
        } else {
    		yearField.set("v.errors", null);
        }

        var certField = component.find("certId");//get("v.dynamicComponentsByAuraId").certId;
        var certName = component.get("v.Certification");
        if (certName == ''){
            validCert = false;
            component.set("v.certerrors", $A.get("$Label.c.TC_Certification_Error"));
            //certField.set("v.isError",true);
             $A.util.addClass(certField,"slds-has-error show-error-message");
        } else {
            $A.util.removeClass(certField,"slds-has-error show-error-message");
            //certField.set("v.isError",false);
    		component.set("v.certerrors", null);
        }
        return(validCert);
    },  
    
    saveCert: function(component, event) {
        var gardYear = (component.get("v.Graduation_Year") ? component.get("v.Graduation_Year") : null);
        var Notes__c = (component.get("v.Notes") ? component.get("v.Notes") : null);
        var Certification__c = (component.get("v.Certification") ? component.get("v.Certification") : null);
        //D-09760 : santosh C
        var talentID;
        if(component.get("v.recordId") === null)
           talentID = component.get("v.Certification.Talent__c");
	    else         
           talentID = component.get("v.recordId");
        
 	    var newCertification = {'sobjectType': 'Talent_Experience__c', 
                            'Talent__c': talentID,
                            'Type__c': 'Training',
                        	'Graduation_Year__c' : gardYear,
                        	'Notes__c' : Notes__c,
                       		'Certification__c' : Certification__c,
                            'Id' : component.get("v.id")
                           };
   
        var params = {"talentExperience": JSON.stringify(newCertification)};
        var bdata = component.find("basedatahelper");
        var self = this;
        bdata.callServer(component,'ATS','',"saveTalentExperience",function(response){
            var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
            updateLMD.fire();
            var savedEvent = component.getEvent("certificationSavedEvent");
            savedEvent.fire();
            self.resetCertLookUp(component);
            
        },params,false);

	},

      getCertificationToEdit : function(component) {
         
         var params = {"soql": "select Talent__c, Type__c, Graduation_Year__c, Notes__c, Certification__c from Talent_Experience__c where Id = '" + component.get("v.id") + "'"};
         var bdata = component.find("basedatahelper");
         bdata.callServer(component,'','',"getRecord",function(response){
             component.set("v.Graduation_Year", response.Graduation_Year__c);
             component.set("v.Certification", response.Certification__c);
             component.set("v.Notes", response.Notes__c);

         },params,false);
    },

})