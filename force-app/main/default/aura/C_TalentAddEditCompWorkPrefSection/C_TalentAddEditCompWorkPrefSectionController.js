({
	doInit: function (component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",true);

		helper.populateCurrencyType(component);
		if (component.get("v.context") === 'page') {
			component.set("v.talentLoaded", true);
		} else if (component.get("v.context") === 'modal') {
			let pageAcc = component.get('v.acc');

			pageAcc[4].className = 'show';
			pageAcc[4].icon = 'utility:chevrondown';

			component.set("v.acc", pageAcc);
		} else if (component.get("v.context") === 'inline') {
			helper.calculateTotalComp(component);
			helper.desiredPlacementToArray(component);
			helper.shiftToArray(component);
		}
	},

	talentLoaded : function (component, event, helper) {
		if (component.get("v.talentLoaded") === true) {
			helper.calculateTotalComp(component);
			helper.desiredPlacementToArray(component);
			helper.shiftToArray(component);
		}
	},

	toggleSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronright': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronright',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

    toggleSubSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronup': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronup',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

	updateMaxSalary: function (component, event, helper){
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
       helper.updateMaxSalary(component, event);
      }, 

    updateMax: function (component, event, helper){
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
       helper.updateMax(component, event);
      }, 

    calculateTotalComp: function(component, event, helper){
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
        helper.calculateBonusAmountOnSalary(component);
        helper.calculateBonusPctOnSalary(component);
        helper.calculateTotalComp(component, event);
     },

	  rateTypeChange : function (cmp, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
		helper.calculateBonusAmount(cmp);
		helper.calculateTotal(cmp);
	},
   
	rateChange : function (cmp, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
		helper.calculateBonusAmount(cmp);
		helper.calculateTotal(cmp);
	},
  
	bonusPctChange : function (cmp, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
		helper.calculateBonusAmountOnSalary(cmp);
		helper.calculateTotalComp(cmp);
	},
  
	bonusChange : function (cmp, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
		// helper.calculateBonusPct(cmp);
		helper.calculateBonusPctOnSalary(cmp);
		helper.calculateTotalComp(cmp);
	},
  
	AddlCompChange : function (cmp, event, helper) {
			//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
		helper.calculateTotalComp(cmp);
	},

	validTalent : function(component, event, helper){
		return helper.validTalent(component);
	},

	translateDesPlace: function(component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
		if (component.get("v.desiredPlacement")) {
			component.set("v.account.Desired_Placement_type__c", component.get("v.desiredPlacement").join(';'));
		}
	},

	translateShift: function(component, event, helper) {
		component.set("v.initOnly",false);
		if (component.get("v.shift")) {
			component.set("v.account.Shift__c", component.get("v.shift").join(';'));
		}
	},

	enableInlineEdit: function (component, event, helper) {
		let evt = component.getEvent("enableInlineEdit"),
			editMode = component.get("v.edit");

		component.set("v.edit", !editMode);
		evt.fire();

	},

	invokeDoInit : function (component, event, helper) { 
		if (component.get("v.context") === 'inline') {
			helper.calculateTotalComp(component);
			helper.desiredPlacementToArray(component);
			helper.shiftToArray(component);
		}
	}

})