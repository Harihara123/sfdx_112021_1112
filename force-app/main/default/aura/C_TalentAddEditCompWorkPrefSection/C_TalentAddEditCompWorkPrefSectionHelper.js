({
	toggleAccordion: function(component, event, toggleState) {
        var toggleState;

        var acc = component.get("v.acc");    
        var accordionId = event.currentTarget.id;
        var icon = acc[accordionId].icon;
        var className = acc[accordionId].className;     
                
        component.set(`v.acc[${accordionId}].icon`, toggleState[icon]);
        component.set(`v.acc[${accordionId}].className`, toggleState[className]);    
    },

	populateCurrencyType : function(component) {
        var params = null;
		var hlp = component.find("addEditBaseHelper");
        hlp.callServer(component,'ATS','TalentEmploymentFunctions','getCurrencyList',function(response){
            if(component.isValid()){
                var desiredCurrency = component.get("v.account.Desired_Currency__c");
                component.set("v.desiredCurrency",desiredCurrency); 
                var CurrLevel = [];
                var CurrLevelMap = response;
                for ( var key in CurrLevelMap ) {
                    CurrLevel.push({value:key, key:key,selected: key === desiredCurrency});
                }
                component.set("v.CurrencyList", CurrLevel); 
            }
        },params,false);
    },

	calculateTotalComp:function(cmp) {
		if(cmp.get("v.edit")){
            var pay = (((cmp.find("desiredsalary") !== null) && (cmp.find("desiredsalary") !== undefined)) ? cmp.find("desiredsalary").get("v.value") : 0);
            var bonus = (((cmp.find("desiredBonusId") !== null) && (cmp.find("desiredBonusId") !== undefined))  ? cmp.find("desiredBonusId").get("v.value") : 0);
            var addl = (((cmp.find("desiredAdditionalCompensationId") !== null) && (cmp.find("desiredAdditionalCompensationId") !== undefined)) ? cmp.find("desiredAdditionalCompensationId").get("v.value") : 0);  // adding this step to check for blank the addl bonus to prevent NaN issue. 
            
        }else{
            var pay = (((cmp.find("readdesiredrsalary") !== null) && (cmp.find("readdesiredrsalary") !== undefined)) ? cmp.find("readdesiredrsalary").get("v.value") : 0);
            var bonus = (((cmp.find("readdesiredBonusId") !== null) && (cmp.find("readdesiredBonusId") !== undefined))  ? cmp.find("readdesiredBonusId").get("v.value") : 0);
            var addl = (((cmp.find("readdesiredAdditionalCompensationId") !== null) && (cmp.find("readdesiredAdditionalCompensationId") !== undefined)) ? cmp.find("readdesiredAdditionalCompensationId").get("v.value") : 0);  // adding this step to check for blank the addl bonus to prevent NaN issue. 
        }  
            bonus = (isNaN(bonus) || bonus === null) ? 0 : bonus;
            addl = (isNaN(addl) || addl === null) ? 0 : addl;
            pay = (isNaN(pay) || pay === null) ? 0 : pay; 
            pay = parseFloat(pay) + parseFloat(bonus) + parseFloat(addl);
            pay = parseFloat(pay).toFixed(2);
			if (pay === 'NaN') {
				pay = 0;
			}
            cmp.set("v.totalComp",pay);  

    },

    calculateBonusAmountOnSalary:function(cmp) {
        var bonus = (cmp.find("desiredBonusPercentId").get("v.value") != null ? cmp.find("desiredBonusPercentId").get("v.value") : 0);
              bonus = parseFloat(bonus).toFixed(2);
        var pay  = (cmp.find("desiredsalary").get("v.value") != null ? cmp.find("desiredsalary").get("v.value") : 0);
            pay = (pay * bonus)/100 ;    
            cmp.find("desiredBonusPercentId").set("v.value",bonus);
            cmp.find("desiredBonusId").set("v.value",pay);
        },
    
    calculateBonusPctOnSalary:function(cmp) {
        var bonus = (cmp.find("desiredBonusId").get("v.value") != null ? cmp.find("desiredBonusId").get("v.value") : 0);
            bonus = parseFloat(bonus).toFixed(2);
        var paytype = cmp.find("ratetypeId").get("v.value"); 
        var pay = (cmp.find("desiredsalary").get("v.value") != null ? cmp.find("desiredsalary").get("v.value") : 0);
        
            pay = (pay != 0 ? (parseInt(bonus)/parseInt(pay))*100 : 0);
            pay = parseFloat(pay).toFixed(2);
            
            cmp.find("desiredBonusPercentId").set("v.value",pay) ;
        
    }, 

    updateMaxSalary: function(component, event){

       var minSalaryField = component.find("desiredsalary"); 
       var maxSalaryField = component.find("desiredsalarymax");
       var minSalary = minSalaryField.get("v.value");
       var maxSalary = maxSalaryField.get("v.value");

       if($A.util.isEmpty(maxSalary)){
        maxSalaryField.set("v.value", minSalary);
       }
       if($A.util.isEmpty(minSalary) && !$A.util.isEmpty(maxSalary)){
        minSalaryField.set("v.errors", [{message:$A.get("$Label.c.ADD_NEW_TALENT_ENTER_MIN_SALARY")}]);
       }else{
        minSalaryField.set("v.errors", null);
       }


       if(minSalary > maxSalary && !$A.util.isEmpty(maxSalary)){
        maxSalaryField.set("v.errors", [{message:$A.get("$Label.c.ADD_NEW_TALENT_MAX_SALARY")}]);
       }else{
        maxSalaryField.set("v.errors", null);
       }
 

    },

    updateMax: function(component, event){

       var minField = component.find("desiredrateId"); 
       var maxField = component.find("desiredratemaxId");
       var minValue = minField.get("v.value");
       var maxValue = maxField.get("v.value");

       if($A.util.isEmpty(maxValue)){
        maxField.set("v.value", minValue);
       }
       if($A.util.isEmpty(minValue) && !$A.util.isEmpty(maxValue)){
        minField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_MINIMUM_RATE")}]);
       }else{
        minField.set("v.errors", null);
       }

       if(minValue > maxValue && !$A.util.isEmpty(maxValue)){
        maxField.set("v.errors", [{message:$A.get("$Label.c.ADD_NEW_TALENT_MAXIMUM_RATE")}]);
       }else{
        maxField.set("v.errors", null);
       }       

    },
	
	validTalent :  function(component) {
		var minField = component.find("desiredrateId"); 
        var maxField = component.find("desiredratemaxId");
	    var minSalaryField = component.find("desiredsalary"); 
        var maxSalaryField = component.find("desiredsalarymax");

		if ( (maxField.get("v.errors") && maxField.get("v.errors")[0] && maxField.get("v.errors")[0].message) ||
			(minField.get("v.errors") && minField.get("v.errors")[0] && minField.get("v.errors")[0].message)|| 
			(minSalaryField.get("v.errors") && minSalaryField.get("v.errors")[0] && minSalaryField.get("v.errors")[0].message) || 
			(maxSalaryField.get("v.errors") && maxSalaryField.get("v.errors")[0] && maxSalaryField.get("v.errors")[0].message)) {
			return false;
	   }
	   return true;
	},

	desiredPlacementToArray: function(component) {
		if (component.get("v.account.Desired_Placement_type__c")) {
			component.set("v.desiredPlacement", component.get("v.account.Desired_Placement_type__c").split(";"));
		}
	},

	shiftToArray: function(component) {
		if (component.get("v.account.Shift__c")) {
			component.set("v.shift", component.get("v.account.Shift__c").split(";"));
		}
	}

})