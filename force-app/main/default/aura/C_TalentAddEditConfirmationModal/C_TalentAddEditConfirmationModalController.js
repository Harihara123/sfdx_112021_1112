({
	showModal : function (component, event, helper) {
        helper.toggleClass(component,'backdrop','slds-backdrop--');
        helper.toggleClass(component,'modaldialog','slds-fade-in-');
    },

	acknowledgeModal : function (component, event, helper) {
		helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');
        var modalCloseEvt = component.getEvent("addEditCloseModalEvent");
        modalCloseEvt.fire();
    },

    closeModal : function (component, event, helper) {
        helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
        helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');
    },

})