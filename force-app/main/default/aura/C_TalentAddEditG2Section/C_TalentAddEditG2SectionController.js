({
	doInit: function(component,event,helper){
		//console.log('g2commentsid---in init addeditg2section---'+component.get('v.g2CommentsId'));..
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",true);
        if(component.get("v.context") === 'modal') {
            let pageAcc = component.get('v.acc');

            pageAcc[6].className = 'show';
            pageAcc[6].icon = 'utility:chevrondown';

            component.set("v.acc", pageAcc);
        }
	},
	toggleSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronright': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronright',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },
	updateG2Comments: function(component,event,helper){
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
	//Rajeesh Notes Parser lab experiment 1/24/2019
		component.set('v.g2CommentsId',component.get('v.parsedG2Comments'));
	},
    toggleSubSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronup': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronup',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

	getG2Comments : function(component, event, helper) {
		 var g2Comm =  component.get("v.g2CommentsId") ? component.get("v.g2CommentsId") : '' ;
		 //Rajeesh 4/1/2019 Notes parser g2 comments not creating task - bug fix
		 if(!$A.util.isEmpty(component.get("v.parsedg2CommentsId"))){
			//at a time either notes parser g2 comments or regular comments will be existing. not both
			g2Comm = component.get("v.parsedg2CommentsId");
		 }
		 return g2Comm;
	},

    enableInlineEdit: function (component, event, helper) {
        let evt = component.getEvent("enableInlineEdit"),
            editMode = component.get("v.edit");

        component.set("v.edit", !editMode);
        evt.fire();

    }
})