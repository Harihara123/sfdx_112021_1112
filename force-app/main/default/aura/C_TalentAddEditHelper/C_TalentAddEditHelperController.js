({
	loadPicklistValues: function (component, event, helper) {
		var parameters = event.getParam("arguments");
		if (parameters) {
			helper.loadPicklistValues(component, parameters.object, parameters.field, parameters.picklistCmp);
		}
	}
})