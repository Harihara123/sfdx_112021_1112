({
	loadPicklistValues: function(component, objName, fieldName, picklistCmp) {
        var action = component.get("c.getPicklistValues");
        component.set('v.vals', picklistCmp);
        action.setParams({
            "objectStr": objName,
            "fld": fieldName
        });

        var opts = [];
        let newOpts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues) {
                    allValues.forEach(item => newOpts.push({label: item, value: item}));                    
					if (allValues.length > 0) {
						opts.push({
							class: "optionClass",
							label: "--- None ---",
							value: ""
						});
					}

					for (var i = 0; i < allValues.length; i++) {
						opts.push({
							class: "optionClass",
							label: allValues[i],
							value: allValues[i]
						});
					}
                }
                if (fieldName == 'Do_Not_Contact_Reason__c') {
					component.set('v.vals', newOpts);
				} else {
					picklistCmp.set("v.options", opts);
				}
            }
        });
        $A.enqueueAction(action);
    }
})