({
	doInit: function (component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",true);
		if (component.get("v.context") === 'page') {
			var ipjsonstr = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
            var ipjson = JSON.parse(ipjsonstr);
            component.set("v.record",ipjson);

			if (component.get("v.talentLoaded") === false) {
				component.set("v.talentLoaded", true);
			} else {
				// Run the data translations that are required when talent is loaded since change handler will not fire
				helper.runTalentLoadActions(component);
			}
		} else if (component.get("v.context") === 'modal') {
			let pageAcc = component.get('v.acc');

			pageAcc[5].className = 'show';
			pageAcc[5].icon = 'utility:chevrondown';

			component.set("v.acc", pageAcc);
		} else if (component.get("v.context") === 'inline') {
			if(component.get("v.talentLoaded")) {
				helper.runTalentLoadActions(component);
			}
		}
        var radioList = component.get("v.radioOptions");
        radioList[0].label=$A.get("$Label.c.FIND_ADD_TALENT_Unknown");
        radioList[1].label=$A.get("$Label.c.ATS_YES");
        radioList[2].label=$A.get("$Label.c.ATS_NO");
	}, 

	talentLoaded : function (component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
		if (component.get("v.talentLoaded") === true) {
			helper.runTalentLoadActions(component);
		}
	},

	toggleSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronright': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronright',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

    toggleSubSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronup': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronup',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

    toggleLayout: function(component, event, helper) {
        let newLayout = component.get("v.toggleLayout");
        component.set("v.toggleLayout", newLayout);
    },

	addNewLocation :function(cmp, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
		var locs = [];
		if (cmp.get("v.desiredLocationArray")) {
			locs = cmp.get("v.desiredLocationArray");  
		}
		locs.push(helper.getEmptyPreferredLocation());
		cmp.set("v.desiredLocationArray", locs);
		cmp.set("v.showLocTable", true);
	},

    removeLocation :function(cmp, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
		var index = event.getSource().get('v.value');
		var locs = cmp.get("v.desiredLocationArray");
		locs.splice(index, 1);//deleting the element in the array
		cmp.set("v.desiredLocationArray", locs);
   },

   /*getLocationUnMappedData : function(cmp, event, helper) {
		return cmp.get("v.commuteType");
   },*/

		//Rajeesh client side data backup fix after G2 redesign.
	translateNationalOpp: function(component, event, helper) {
		component.set("v.record.geographicPrefs.nationalOpportunities", helper.translateRadio(component.get("v.nationalOpp")));
		component.set("v.initOnly",false);
	},
	translateWillingToRelocate: function(component, event, helper) {
		component.set("v.account.Willing_to_Relocate__c", helper.translatePicklist(component.get("v.willingToRelocate")));
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
	},
	translateReliableTransport: function(component, event, helper) {
		component.set("v.record.employabilityInformation.reliableTransportation", helper.translateRadio(component.get("v.reliableTransport")));
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
	},
	translateLocationArray: function(component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
		component.set("v.record.geographicPrefs.desiredLocation", helper.translateLocationArray(component.get("v.desiredLocationArray")));
	},
	translateCommuteUnit: function(component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
		component.set("v.record.geographicPrefs.commuteLength.unit", component.get("v.commuteUnit"));
	},

	enableInlineEdit: function (component, event, helper) {
		let evt = component.getEvent("enableInlineEdit"),
			editMode = component.get("v.edit");

		component.set("v.edit", !editMode);
		evt.fire();

	},

	invokeDoInit : function (component, event, helper) {
		if (component.get("v.context") === 'inline') {
			helper.runTalentLoadActions(component);
		}
	}
})