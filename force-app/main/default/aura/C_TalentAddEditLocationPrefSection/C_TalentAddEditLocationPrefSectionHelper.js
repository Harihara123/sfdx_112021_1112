({
	runTalentLoadActions: function(component) {
		component.set("v.reliableTransport", this.codeRadio(component.get("v.record.employabilityInformation.reliableTransportation")));
		component.set("v.nationalOpp", this.codeRadio(component.get("v.record.geographicPrefs.nationalOpportunities")));
		component.set("v.willingToRelocate", this.codePicklist(component.get("v.account.Willing_to_Relocate__c")));
		component.set("v.desiredLocationArray", this.codeLocationArray(component));
		component.set("v.commuteUnit", this.codeCommuteUnit(component.get("v.record.geographicPrefs.commuteLength.unit")));

		// Rajeesh Notes Parser Lab experiment updating relocation preferences from free text.
		this.updateParsedLocation(component);
	},

	toggleAccordion: function(component, event, toggleState) {
        var toggleState;

        var acc = component.get("v.acc");    
        var accordionId = event.currentTarget.id;
        var icon = acc[accordionId].icon;
        var className = acc[accordionId].className;     
                
        component.set(`v.acc[${accordionId}].icon`, toggleState[icon]);
        component.set(`v.acc[${accordionId}].className`, toggleState[className]);    
    },

	updateParsedLocation: function(component) {
		var parsedReloc = component.get("v.parsedRelocation");
		// If parsed location available
		if (!$A.util.isEmpty(parsedReloc)) {
			var desLoc = component.get("v.desiredLocationArray");
			var idx = desLoc.length - 1;
			var last = desLoc[idx];
			// If last row of preferred locations is all empty, replace the values with parsed data.
			if (last.country === "" && last.state === "" && last.city === "") {
				var updated = false;
				if (!$A.util.isEmpty(parsedReloc.Country)) {
					last.country = parsedReloc.Country;
					component.set("v.countryKey" + idx, parsedReloc.State);
					updated = true;
				}
				if (!$A.util.isEmpty(parsedReloc.State)) {
					last.state = parsedReloc.State;
					component.set('v.stateKey', parsedReloc.State);
					updated = true;
				}
				if (!$A.util.isEmpty(parsedReloc.City)) {
					last.city = parsedReloc.City;
					updated = true;
				}
				if (updated) {
					component.set("v.willingToRelocate", "Yes");
				}
				component.set("v.desiredLocationArray", desLoc);
			}
		}
	},

	codeCommuteUnit : function(unit) {
		return unit != null ? unit : "";
	},

	codeRadio: function(value) {
		return (value === undefined || value === null || value === "") ? "Unknown" : (value == true ? "Yes" : "No");
	},

	codePicklist: function(value) {
		return (value === undefined || value === null || value === "") ? "Unknown" : value;
	},

	translateRadio: function(value) {
		return value === "Unknown" ? null : (value === "Yes" ? true : false);
	},

	translatePicklist: function(value) {
		return value === "Unknown" ? null : value;
	},

	codeLocationArray: function(component) {
		var arr = component.get("v.record.geographicPrefs.desiredLocation");
		var coded = [];
		if ($A.util.isEmpty(arr)) {
			coded.push(this.getEmptyPreferredLocation());
			component.set("v.countryKey0", "");
			component.set('v.stateKey', "");
		} else {
			coded = arr.slice();
			if (arr.length <= 4) {
				// Slots available. Add an empty row
				coded.push(this.getEmptyPreferredLocation());

				var idx = parseInt(arr.length);
				component.set("v.countryKey" + idx, "");
				component.set('v.stateKey', "");
			}
		}

		return coded;
	},

	translateLocationArray: function(coded) {
		var translated = [];
		// Add only non-empty elements to the translated array.
		coded.forEach(item => {
			if (item.country !== "" || item.state !== "" || item.city !== "") {
				translated.push(item);
			}
		});
		return translated;
	},

	getEmptyPreferredLocation: function() {
		return {
			"country": "",
			"state": "",
			"city": ""
		};
	}
})