({
	rerender : function(component, helper){
		
    		/* Rajeesh S-108773 - Ability to Restore Backup from 'refreshed' G2 on Inline Edit Card
			 * create a track fields object with all the variables that need to be backed up 
    		 * and take a backup using the backup component.
			 * backup component creates a key for the card (using card number and contact id), and create a json and base 64 encode
			 * the string before storing in session storage.
			 * 1/29/2019 - G2 card redesign- modal is comprised of many smaller child components. changes in child components doesnt force
			 * rerender of parent component. so setting a dummy variable from parent to force it.
			 */
			this.superRerender();
			var initOnly = component.get("v.initOnly");
			var eraseBackup = component.get('v.eraseBackup');
			//var usrHasAccess = component.get('v.usrHasAccess');
			//console.log('renderer userhasaccess'+usrHasAccess);
			if(!initOnly && !eraseBackup){
				var addEditComp = component.get("v.immedParent");
				var dummy = {};
				dummy.updated=true;
				//addEditComp.set('v.dummy',dummy);//trigger parent's re-render, // commented by manish
				addEditComp.set('v.initOnly',false);//make sure that backup is written
				//addEditComp.set('v.eraseBackup',false);//make sure that backup is written
			}
	}
})