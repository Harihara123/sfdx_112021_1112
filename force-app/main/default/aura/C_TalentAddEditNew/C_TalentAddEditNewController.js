({
	doInit: function(component, event, helper) {
		
		/*
			Rajeesh S-108774 - Ability to Restore Backup from 'refreshed' G2 on summary modal
			Feature enabled based on alias included in label. If label is set to GA, rolled out to everyone.
			Label contains comma seperated userids
		*/
// console.log('---name:'+component.getName()+'--:init timestamp:'+Date.now());
		component.set("v.initOnly",true);
		//End Rajeesh
        if(component.get("v.context") !== "modal") {
            helper.initializeAddEditTalent(component);// for defect D-07495 
        } 

        helper.getCurrentUser(component);
		var startTime = Date.now();
		component.set("v.g2start", startTime);
		
		//S-85004 - URL isAddressable - Karthik
        var myPageRef = component.get("v.pageReference");
        if(myPageRef){
            var mode = myPageRef.state.c__mode;
            if (mode !== null && mode !== "") {
                component.set("v.mode", mode);
                if (mode === "upload") {
                    var isResumeParsed = myPageRef.state.c__isResumeParsed;
                    component.set ("v.isResumeParsed", isResumeParsed);
                }
            }

            var accountId = myPageRef.state.c__accountId;
            // console.log('accountId '+accountId);
            if (accountId !== null && accountId !== undefined) {
                component.set("v.accountId", accountId);
                helper.getTalentData(component, accountId);
            } else {
				// In the add flow, dncFields is null and needs to be initialized.
				helper.initializeNewDNC(component);
			}

            var filterFields = myPageRef.state.c__filteringFields;
            //console.log('filterFields '+filterFields);
            if (filterFields !== null){
                component.set("v.filteringFields", filterFields);
                helper.copyFilterValues(component);
            }
			
        }else{
			// This is Edit Call.
			var spinner = component.find('spinner');
			$A.util.addClass(spinner, 'slds-show');
			$A.util.removeClass(spinner, 'slds-hide'); 

			var recordID = component.get("v.accountId");
			helper.getTalentData(component, recordID);
			if(!component.get("v.resumeLoaded")){
				helper.createResumePreviewCmp(component, event);
			}
        }
    },

	redirectToPreviousPage : function(component, event, helper) {
		//Rajeesh clear backup on cancel/redirect?
		//Rajeesh stop writing client side backup
		component.set("v.initOnly",false);
		component.set('v.eraseBackup',true);
		var dataBackup = component.find("clientSideBackup"); 
		dataBackup.clearDataBackup();

        var urlEvent = $A.get("e.force:navigateToURL");
        var accountId = component.get("v.accountId");
        var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
        var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
        var dURL;
        
        if(isFlagOn === "true") {
			  dURL = ligtningNewURL+"/r/Account/" + accountId +"/view";
		}
		  else {
			  dURL = ligtningNewURL+"/sObject/" + accountId +"/view"
		}
		
        if (component.get("v.accountId") && component.get("v.mode") !== "upload") {
            urlEvent.setParams({
                "url": dURL,
                "isredirect":'true'
            });
        } else { 
            urlEvent.setParams({
            	
                //"url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Find_or_Add_Candidate",
                "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Find_or_Add_Candidate",
                "isredirect":'true'
            });
        }    
        urlEvent.fire();  
        //Neel summer 18 URL change end->

		
    },

	validateAndSaveTalent : function(component, event, helper) {
		let profileSection = component.find('profileSection');
        profileSection.toggleSection(true);
        helper.validateAndSaveTalent(component);

    },

	hideModal :  function(component, event, helper) {
		  var closeEvent = $A.get("e.c:E_CloseTalentSummaryModal");
          closeEvent.fire();
		  component.destroy();
	},

	validateAndSaveTalentOnEdit :function(component, event, helper) {
        let profileSection = component.find('profileSection');
        profileSection.toggleSection(true);
		var params = event.getParam('arguments')
        if(!$A.util.isUndefinedOrNull(params)){
			component.set("v.isG2Checked",params.g2checked);
		}
		helper.validateAndSaveTalent(component);

		//Rajeesh stop writing client side backup
		/*component.set('v.eraseBackup',true);
		var dataBackup = component.find("clientSideBackup"); 
		dataBackup.clearDataBackup();*/
	},

	handleDNCUpdates: function(component, event, helper) {
		var account = component.get("v.account");
		var dncf = component.get("v.dncFields");

		account.Do_Not_Contact__c = dncf.Do_Not_Contact__c;
		account.Do_Not_Contact_Reason__c = dncf.Do_Not_Contact_Reason__c;
		account.Do_Not_Contact_Date__c = dncf.Do_Not_Contact_Date__c;

		component.set("v.account", account);
	}

})