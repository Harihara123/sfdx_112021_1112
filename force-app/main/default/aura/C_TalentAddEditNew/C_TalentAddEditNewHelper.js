({
    initializeAddEditTalent : function(component) {
        component.set("v.contact", {'sobjectType':'Contact', 'FirstName':'', 'MiddleName': '', 'LastName':'','Other_Email__c':'', 'Email':'','Salutation':'', 'LinkedIn_URL__c':'','Website_URL__c':'', 'Title':'','MiddleName':'','HomePhone' : '', 'Phone': '', 'MobilePhone':'','MailingCountry':'', 'MailingStreet':'', 'MailingCity':'','MailingState':'','MailingPostalCode':'',
                                    'Talent_State_Text__c': '', 'Talent_Country_Text__c': '','Work_Email__c' :'','OtherPhone' :'', 'Related_Contact__c' :  null,
      'Preferred_Email__c': '', 'Preferred_Phone__c': '', 'Preferred_Name__c' : '' ,'Id': null});

        if(component.get("v.context") !== 'modal') {
            component.set("v.account", { 'sobjectType': 'Account', 'Name': '', 'Talent_Gender__c': '', 'Talent_Race__c': '', 'Talent_Gender_Information__c': '', 'Talent_Race_Information__c': '', 'Do_Not_Contact__c': '', 'Do_Not_Contact_Reason__c': '', 'Do_Not_Contact_Date__c': '', 'Talent_Source_Other__c': '', 'G2_Completed__c': false, 'Id': null, 'Talent_Current_Employer__c' : ''});
        }
        this.initializeNewDNC(component);
    }, 

    getCurrentUser : function(component) {
        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                var user = response.usr;
                user.userOwnership = response.userOwnership;
                user.userCategory = response.userCategory;
                component.set ("v.runningUser", user);
            }
        });
        $A.enqueueAction(action);
    },

	copyFilterValues : function(component) {
		component.set('v.contact.FirstName', 			component.get("v.filteringFields.FirstName"));
        component.set('v.contact.LastName', 			component.get("v.filteringFields.LastName"));
        component.set('v.contact.Preferred_Name__c',	component.get("v.filteringFields.Preferred_Name__c"));
        component.set('v.contact.MobilePhone', 			component.get("v.filteringFields.Phone"));
        component.set('v.contact.Email',				component.get("v.filteringFields.Preferred_Email__c"));
        component.set('v.contact.Title',				'');

   },

   getParameterByName : function(cmp,event,name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    getTalentData : function(component, accountId) {
       var account = component.get("v.account");
       account.Id = accountId;
       component.set("v.account", account); 
       var serverMethod = 'c.getTalentForModal';
        var params = {
            "accountId": component.get("v.accountId")
        };
        
        var action = component.get(serverMethod);
        action.setParams(params);
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.isShow",false);
                var talent = response.getReturnValue();
                var talentBackup = response.getReturnValue();
                component.set("v.preSaveContact", talentBackup);
                const acc = JSON.parse(JSON.stringify(talentBackup.Account));
                component.set("v.preSaveAccount", acc);

                if(talent != null){
                    if(component.get("v.mode") ==="upload" && talent.MailingState){
                        var mstate = talent.MailingState;
                        if(mstate.indexOf(".") > -1){
                            talent.MailingState= talent.MailingState.slice(3);// For Defect D-07493 removing Country Code from State if from Resume Upload.    
                        }                    
                    }

                    if(component.get("v.mode") ==="upload") {
                        component.set("v.viewState","EDIT");
                    }
                    component.set("v.account", talent.Account); 
                    this.translateTalent(component, talent, talent.Account);
                }else{
                    component.set("v.isResumeParsed","N")
                }
                    var spinner = component.find('spinner');
                    $A.util.removeClass(spinner, 'slds-show');
                    $A.util.addClass(spinner, 'slds-hide'); 

            } 
            else if(state === "ERROR"){
                if ((component.get("v.mode") ==="upload" 
                    && component.get("v.isResumeParsed") === "Y") 
                    || (component.get("v.mode") !=="upload")){
                    this.checkError(response, component);
                }
            }
        });
        $A.enqueueAction(action);
    },

    translateTalent : function(component, talent, account) {
        var g2Comm = component.find("g2CommentsId");
            if(g2Comm){
                g2Comm.set("v.value", null);
        }
        this.translateContact(component, talent);
        this.translateAccount(component, talent, account);
        if (talent.Tasks && talent.Tasks[0]) {
            
            this.translateActivity(component, talent.Tasks[0]);
        }
        /*Rajeesh 3/21/2019 Notes Parser flow - talent fields are getting updated from notes parser.
          Needs to override contact fields fetched from salesforce with the ones that are parsed from N2P.
        
        */ 
        this.overWriteN2PFields(component);
    },
    overWriteN2PFields: function(component){
        //use this section to set N2P parsed fields on contact.
        //but leave the N2P parsed object in all other child components as it is used for highlightning which fields got updated from N2P flow.
        var parsedInfo = component.get('v.parsedInfo');
        if(!$A.util.isEmpty(parsedInfo)){
            var contact = component.get('v.contact');
            var account = component.get('v.account');
            if(!$A.util.isEmpty(parsedInfo.JobTitle)){
                contact.Title = parsedInfo.JobTitle; 
            }
            /*if(!$A.util.isEmpty(parsedInfo.Living)){
                contact.MailingCountry = parsedInfo.Living.Country;
                contact.MailingState = parsedInfo.Living.State;
                contact.MailingCity =parsedInfo.Living.City;
                contact.MailingStreet = ''; 
                contact.MailingPostalCode=null; 
            }*/
            if(!$A.util.isEmpty(parsedInfo.DesiredRate)){
                account.Desired_Rate__c = parsedInfo.DesiredRate.amount;
                account.Desired_Rate_Max__c = parsedInfo.DesiredRate.amount;
                account.Desired_Rate_Frequency__c =parsedInfo.DesiredRate.frequency;
            }
            if(!$A.util.isEmpty(parsedInfo.Skills)){
                //Rajeesh 4/11/2019 Notes parser skills.
                var parsedSkillsArrayExisting = account.Skills__c_parsed.skills;

                if(!$A.util.isEmpty(parsedSkillsArrayExisting))
                    parsedSkillsArrayExisting = parsedSkillsArrayExisting.toLocaleString().toLowerCase().split(',');
                var parsedSkillsArray = parsedInfo.Skills;
                var bacupParsedSkills = JSON.stringify(parsedInfo.Skills);
                //make sure that these are only new ones.
                for(var m=0;m<parsedSkillsArray.length;m++){
                    for(var n=0;n<parsedSkillsArrayExisting.length;n++){
                        if(parsedSkillsArray[m] == parsedSkillsArrayExisting[n]){
                            parsedSkillsArray.splice(m,1);
                            m--;
                        }
                    }

                }
                parsedInfo.Skills = JSON.parse(bacupParsedSkills);
                component.set("v.ParsedSkills", parsedSkillsArray);
                component.set("v.ExistingSkills", parsedSkillsArrayExisting);
                account.Skills__c_parsed.skills = parsedSkillsArray.concat(parsedSkillsArrayExisting)
                component.set("v.parsedExistSkillArrLength",parsedSkillsArrayExisting.length);
                component.set('v.account.Skills__c',JSON.stringify(account.Skills__c_parsed));
            }

        }
        else{
            console.log('There is nothing to update from N2P');
        }


    },
    showToast : function(toastMessage, title, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: toastMessage,
            type: type
        });
        toastEvent.fire();
    },

saveTalent : function(component) {
    component.set('v.disableSave', true);
    var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
        $A.util.removeClass(spinner, 'slds-hide');
   
        var contact = component.get("v.contact");
        var mode = component.get("v.mode");
        var isResumeParsed = component.get("v.isResumeParsed");

        if (component.get("v.accountId") && !contact.Id && 
            mode==="upload" && isResumeParsed==="Y") {
            this.showToast ($A.get("$Label.c.FIND_ADD_TALENT_Helpdesk"),$A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"), "error");
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide'); 
            return;
        } else if ((component.get("v.accountId") && !contact.Id && mode!=="upload") 
            || (!component.get("v.accountId") && contact.Id)) {
            this.showToast ($A.get("$Label.c.FIND_ADD_TALENT_Helpdesk"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"), "error");
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide'); 
            return;
        } 
        var profileSectionComp = component.find("profileSection");
        contact = profileSectionComp.setDataBeforeSaveTalentCall(true,contact);

        component.set("v.account.Talent_Preference_Internal__c", JSON.stringify(component.get("v.record")));
        component.set("v.account.Skills__c", JSON.stringify(component.get("v.account.Skills__c_parsed")));
        // Remove the Skills__c_parsed key from this object before saving to server since this is not a valid filed on Account.
        delete component.get("v.account").Skills__c_parsed;

        var G2Comp = component.find("G2Section");
        var g2Comments = G2Comp.getG2Comments();
        if (component.get("v.isG2Checked") === true) {
              component.set("v.account.G2_Completed__c", true);
        }

        var serverMethod = 'c.saveTalentRecord';
        //Rajeesh S-115724 - ability to report usage inline vs modal. Adding g2source-> stored in task callresult/disposition.
        var params = {
            "contact": contact, 
            "account": component.get("v.account"),
             "g2Comments" : g2Comments,
             "g2Source": "inline-edit"
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        
       action.setCallback(this,function(response) {
            component.set('v.disableSave', false);
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.isShow",false);
                var contactId = response.getReturnValue();
                
                /*Start For defect D-07495 On successful save Reset the Attribute variable */
                this.initializeAddEditTalent(component);
                //this.showToast("Talent has been successfuly saved!", "Success!", "success");
                //S-134551 -- Added label instead of text
                this.showToast($A.get("$Label.c.ATS_SUCCESS_MSG"),$A.get("$Label.c.ATS_SUCCESS")+"!","success");
                /*End for defect D-07495 */
                
                
                // console.log('Saved Account ID ' + accountId);
                //Neel summer 18 URL change->  
                /*var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/one/one.app#/sObject/" + accountId +"/view"
                });
                */
                var urlEvent = $A.get("e.force:navigateToURL");
                var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
                var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
                var dURL;

                
                if(isFlagOn === "true") {
                      dURL = ligtningNewURL+"/r/Contact/" + contactId +"/view";
                  }
                  else {
                      dURL = ligtningNewURL+"/sObject/" + contactId +"/view"
                  }
                urlEvent.setParams({                    
                    "url": dURL                  
                });
                //Neel change end 
                urlEvent.fire();
                component.destroy(); 
            } else if (state === "ERROR") {
                this.checkError(response, component);
            } 
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide'); 
        });
        $A.enqueueAction(action);
    },   

    saveTalentModal : function(component) { 
        var ev = $A.get("e.c:E_ShowSpinnerOnModal"); 
        var dis = component.get("v.currentEmpDisabled");
        ev.fire();
        component.set('v.showSpinner', true);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
        $A.util.removeClass(spinner, 'slds-hide');
        var contactId = component.get("v.contactId");       
        var contact = component.get("v.contact");
        var account = component.get("v.account");
        var UnProxy = JSON.parse(JSON.stringify(account));
        var endTime = Date.now();
        var startTime = component.get("v.g2start");
        var duration = Math.round((endTime - startTime) / 1000);  

        var con = component.get("v.preSaveContact");
        var proxAcc = component.get("v.preSaveAccount");
        var acc = JSON.parse(JSON.stringify(proxAcc));

        var parsedInfo = component.get('v.parsedInfo');

        var saving = component.get('v.saveFlag');


        if(component.isValid() && saving === false){

            component.set('v.saveFlag', true);
            
            // this.savePickList(component);
            // this.populateLanguageCode(component);
            
            component.set("v.account.Talent_Preference_Internal__c", JSON.stringify(component.get("v.record")));
            component.set("v.account.Skills__c", JSON.stringify(component.get("v.account.Skills__c_parsed")));
            // Remove the Skills__c_parsed key from this object before saving to server since this is not a valid filed on Account.
            delete component.get("v.account").Skills__c_parsed;
            
            var G2Comp = component.find("G2Section");
            var g2Comments = G2Comp.getG2Comments();

            var g2comptoday = component.get("v.isG2DateToday");
            var g2checked = component.get("v.isG2Checked");
            var g2completed = false;

            if(g2comptoday || g2checked)
                g2completed = true;

            if (component.get("v.isG2Checked")) {
                if (component.get("v.isG2DateToday")) {
                    component.set("v.account.G2_Completed__c", false);
                } else {
                    component.set("v.account.G2_Completed__c", true);
                }
            } else {
                component.set("v.account.G2_Completed__c", false);
            }
            
            var dnc = component.get("v.account.G2_Completed__c");
            
            var profileSectionComp = component.find("profileSection");
            contact = profileSectionComp.setDataBeforeSaveTalentCall(true,contact);
            //Rajeesh 4/25/2019 N2P reporting    
            var g2Source = '';
            if($A.util.isEmpty(parsedInfo)){
                g2Source = 'modal';
            }
            else{
                g2Source = 'n2p';
                //Adding n2p Transaction Id to contact record
                contact.N2pTransactionId__c = component.get("v.transactionId");
                console.log(contact.N2pTransactionId__c);

                //console.log(duration);
                
            } 
            //If Current Employer has been edited - Removed all Current Assignment flags from
            // Talent_Work_History and Talent_Experience - S-195499
            console.log(UnProxy.Id);
            if( UnProxy.Talent_Current_Employer_Text__c != acc.Talent_Current_Employer_Text__c){
                var servMethod = 'c.removeCurrentAssignmentAfterEdit';
                var TWHparams = {"AccountId":UnProxy.Id}
                var action = component.get(servMethod);
                action.setParams(TWHparams);
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS"){
                        console.log('removing current assignments...');
                    }
                    else if(state === "ERROR"){
                         this.checkError(response, component);
                    }
                });
                $A.enqueueAction(action);
            }
            
            var serverMethod = 'c.saveTalentRecordModal';
            var params = {
                "contact": contact, 
                "account": component.get("v.account"),
                "g2Comments" : g2Comments,
                "g2Source": g2Source,
                "duration" : duration,
                "acutalContact" : con,
                "acutalAccount" : acc,
                "parsedInfo" : JSON.stringify(parsedInfo),
                "g2checked" : g2completed,
                "disable" : dis
            };
            
            var acc = component.get("v.account");
            var action = component.get(serverMethod);
            action.setParams(params);
            
            action.setCallback(this,function(response) {
                //console.log('response from apex'+response);
                
                var state = response.getState();
                if (state === "SUCCESS") { 
                    component.set("v.cssStyle", "");
                    component.set("v.callServer",true);

                    //this.showToast("Talent has been successfuly saved!", "Success!", "success");
                    //S-134551 -- Added label instead of text
                    this.showToast($A.get("$Label.c.ATS_SUCCESS_MSG"),$A.get("$Label.c.ATS_SUCCESS")+"!","success");

                    var appEvent = $A.get("e.c:RefreshTalentHeader");
                    appEvent.fire();

                    var closeEvent = $A.get("e.c:E_CloseTalentSummaryModal");
                    closeEvent.fire();
                    
                    $A.get('e.force:refreshView').fire();
                    appEvent = $A.get("e.c:E_RefreshTalentSummaryCard"); // only handled in TalentOverviewNew
                    appEvent.fire();
                    component.destroy();
                    
                } 
                else if (state === "ERROR") {
                    component.set('v.saveFlag', false);
                    this.checkError(response, component);
                } 
                //$A.util.removeClass(spinner, 'slds-show');
                //$A.util.addClass(spinner, 'slds-hide'); 

                var toggle = $A.get("e.c:E_ShowSpinnerOnModal");
                toggle.fire();
                $A.util.removeClass(spinner, 'slds-show');
                $A.util.addClass(spinner, 'slds-hide'); 
            });
            $A.enqueueAction(action);
        }
    },

    validateAndSaveTalent : function(component) {   
        //Andy toggling v.setValues gets most current values from c:CustomTextArea components
        component.set("v.setValues", !component.get("v.setValues"));
        //Rajeesh stop writing client side backup
        component.set("v.initOnly",false);
        component.set('v.eraseBackup',true);
        var dataBackup = component.find("clientSideBackup"); 
        dataBackup.clearDataBackup();

        var mode = component.get("v.mode"); 
        var context = component.get("v.context");
        var profileSectionValid = component.find("profileSection").validTalent();
        var overViewSectionValid = component.find("overViewSection").validTalent();
        var compWorkSectionValid = component.find("compWorkSection").validTalent();

        var dncFormValid = true;
        dncFormValid = component.find("profileSection").dncFormValid();

		// if (component.get("v.runningUser.Profile.Name") !== 'ATS_Recruiter') {
		// 	if(context === 'page') {
		// 		dncFormValid = component.find("dncEditForm").validTalent();
		// 	} else  {
		// 		dncFormValid = component.get("v.parentCmp").find("dncEditForm").validTalent();
		// 	}
		// }

        if (profileSectionValid && overViewSectionValid && compWorkSectionValid && dncFormValid) {
            if(context === 'page') {
                this.saveTalent (component);   
            } else {
                this.saveTalentModal (component);
            }
        }    
    },
    

      translateContact : function(component, talent) {
        try {
            var contact = component.get("v.contact");
            contact.Id = talent.Id;
            contact.FirstName = talent.FirstName ? talent.FirstName : '';
            contact.MiddleName = talent.MiddleName ? talent.MiddleName : '';
            contact.LastName = talent.LastName ? talent.LastName : '';
            contact.Preferred_Name__c = talent.Preferred_Name__c ? talent.Preferred_Name__c : '';
            contact.Other_Email__c = talent.Other_Email__c ? talent.Other_Email__c : '';
            contact.Work_Email__c = talent.Work_Email__c ? talent.Work_Email__c : '';
            contact.Email = talent.Email ? talent.Email : '';
            contact.Salutation = talent.Salutation ? talent.Salutation : '';
            contact.Suffix = talent.Suffix ? talent.Suffix : '';
            contact.LinkedIn_URL__c = talent.LinkedIn_URL__c ? talent.LinkedIn_URL__c : '';
            contact.Website_URL__c = talent.Website_URL__c ? talent.Website_URL__c : '';
            contact.Title = talent.Title ? talent.Title : '';
            contact.HomePhone = talent.HomePhone ? talent.HomePhone : '';
            contact.MobilePhone = talent.MobilePhone ? talent.MobilePhone : '';
            contact.Phone = talent.Phone ? talent.Phone : '';
            contact.WorkExtension__c = talent.WorkExtension__c ? talent.WorkExtension__c : '';
            contact.OtherPhone = talent.OtherPhone ? talent.OtherPhone : '';
            contact.MailingCountry = talent.MailingCountry ? talent.MailingCountry : '';
            contact.Talent_Country_Text__c = talent.Talent_Country_Text__c ? talent.Talent_Country_Text__c : '';
            contact.MailingState = talent.MailingState ? talent.MailingState : '';
            contact.Talent_State_Text__c = talent.Talent_State_Text__c ? talent.Talent_State_Text__c : '';
            contact.MailingStreet = talent.MailingStreet ? talent.MailingStreet : '';
            contact.MailingCity = talent.MailingCity ? talent.MailingCity : '';
            contact.MailingPostalCode = talent.MailingPostalCode ? talent.MailingPostalCode : '';
            contact.Related_Contact__c = talent.Related_Contact__c ? talent.Related_Contact__c : '';
            contact.Preferred_Email__c = talent.Preferred_Email__c;
            contact.Preferred_Phone__c = talent.Preferred_Phone__c;
            contact.RWS_DNC__c = talent.RWS_DNC__c;
            contact.Candidate_Status__c = talent.Candidate_Status__c;
            //Start STORY S-109328 Added Communities Fields by Siva
            contact.TC_FirstName__c = talent.TC_FirstName__c ? talent.TC_FirstName__c : '';
            contact.TC_LastName__c = talent.TC_LastName__c ? talent.TC_LastName__c : '';
            contact.TC_Full_Name__c = talent.TC_Full_Name__c ? talent.TC_Full_Name__c : '';
            contact.TC_Mailing_Address__c = talent.TC_Mailing_Address__c ? talent.TC_Mailing_Address__c : '';
            contact.TC_Phone__c = talent.TC_Phone__c ? talent.TC_Phone__c : '';
            contact.TC_Email__c = talent.TC_Email__c ? talent.TC_Email__c : '';
            contact.TC_Title__c = talent.TC_Title__c ? talent.TC_Title__c : '';
            //End STORY S-109328 Added Communities Fields by Siva
            if (talent.Related_Contact__r) {
                contact.Related_Contact__r = talent.Related_Contact__r.Name;
            }

            //console.log('preferred values:');
            //console.log(talent.Preferred_Phone__c);
            //console.log(talent.Preferred_Email__c);
            
           /* if (talent.Preferred_Phone__c) {
                if (talent.Preferred_Phone__c === 'Home') {
                    component.set("v.homePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Mobile') {
                    component.set("v.mobilePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Work') {
                    component.set("v.workPhonePreferred", true);
                }
                    else if (talent.Preferred_Phone__c === 'Other') {
                        component.set("v.otherPhonePreferred", true);
                    }
            }
            
            if (talent.Preferred_Email__c) {            
                if (talent.Preferred_Email__c === 'Email') {
                    component.set("v.emailPreferred", true);
                } else if (talent.Preferred_Email__c === 'Alternate') {
                    component.set("v.otherEmailPreferred", true);
                } else if (talent.Preferred_Email__c === 'Work') {
                    component.set("v.workEmailPreferred", true);
                }
            }  */
            
            if (talent.MailingState) {
                component.set("v.stateKey", talent.MailingState);
            } else {
                component.set("v.stateKey", '');
            }

            if(talent.Candidate_Status__c ){
                component.set("v.candidateStatus", talent.Candidate_Status__c);
            }
            /*if (talent.Talent_State_Text__c) {
                component.set("v.stateValue", talent.MailingState);
            } else {
                component.set("v.stateValue", '');
            }*/
            if (component.get("v.countriesMap") !== null && contact.Talent_Country_Text__c) {
                var countryCode = this.getKeyByValue(component.get("v.countriesMap"),contact.MailingCountry);
                component.set("v.parentInitialLoad",true);
                component.set("v.MailingCountryKey",countryCode);
            } else if(component.get("v.countriesMap") !== null && contact.Talent_Country_Text__c.length == 0 && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
                    component.set("v.contact.Talent_Country_Text__c",'United States');
                    component.set("v.MailingCountryKey",'US');
                    component.set("v.parentInitialLoad",true);
            }  
            
            // Added to split in Address 1 and 2
            var strs;
            if (contact.MailingStreet) {
                if( contact.MailingStreet.indexOf(',') != -1 ){
                    /*strs = contact.MailingStreet.split(',');
                }
                if (strs) { */
                    console.log('reading address');
                    component.set("v.presetLocation",contact.MailingStreet.substr(0,contact.MailingStreet.indexOf(',')));
                    component.set("v.streetAddress2",contact.MailingStreet.indexOf(',')+1 != undefined ? contact.MailingStreet.substr(contact.MailingStreet.indexOf(',')+1).trim() : "");
                } else {
                    component.set("v.presetLocation",contact.MailingStreet);
                }
            }   else {
                component.set("v.presetLocation","");
            }           
            component.set("v.contact", contact);
            
        } catch (error) {
            contact.Id = null;
            component.set("v.contact", contact);
            this.showToast ($A.get("$Label.c.FIND_ADD_TALENT_Helpdesk") + error.message, $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"), 'error');
            //console.log ("Error occurred = " + error.message);
        }     
    },    
    
    translateAccount : function(component, talent, account) {
        try {
            account.Name = talent.Account.Name ? talent.Account.Name : '';
            account.Talent_Gender__c = talent.Account.Talent_Gender__c;
            account.Talent_Race__c = talent.Account.Talent_Race__c;
            account.Talent_Gender_Information__c = talent.Account.Talent_Gender_Information__c;
            account.Talent_Race_Information__c = talent.Account.Talent_Race_Information__c;
            account.Do_Not_Contact__c = talent.Account.Do_Not_Contact__c;
            component.set("v.isChecked",talent.Account.Do_Not_Contact__c);
            
            account.Do_Not_Contact_Reason__c = talent.Account.Do_Not_Contact_Reason__c;
            account.Do_Not_Contact_Date__c = talent.Account.Do_Not_Contact_Date__c;

            component.set("v.dncFields", {
                "Do_Not_Contact__c": talent.Account.Do_Not_Contact__c,
                "Do_Not_Contact_Reason__c": talent.Account.Do_Not_Contact_Reason__c,
                "Do_Not_Contact_Date__c": talent.Account.Do_Not_Contact_Date__c
            });

            account.Talent_Source_Other__c = talent.Account.Talent_Source_Other__c ? talent.Account.Talent_Source_Other__c : '';
            
            account.Talent_Overview__c = talent.Account.Talent_Overview__c;
            account.Skill_Comments__c = talent.Account.Skill_Comments__c;
            account.Goals_and_Interests__c = talent.Account.Goals_and_Interests__c;
            account.Desired_Currency__c = talent.Account.Desired_Currency__c;
            account.Desired_Rate__c = talent.Account.Desired_Rate__c;
            // Start - Fields added for S-58916 and S-76380 
            account.Desired_Rate_Max__c = talent.Account.Desired_Rate_Max__c;
            account.Desired_Salary__c = talent.Account.Desired_Salary__c;
            account.Desired_Salary_Max__c = talent.Account.Desired_Salary_Max__c;
            // End - Fields added for S-58916 and S-76380 
            account.Desired_Rate_Frequency__c = talent.Account.Desired_Rate_Frequency__c;
            account.Desired_Bonus__c = talent.Account.Desired_Bonus__c;
            account.Desired_Bonus_Percent__c = talent.Account.Desired_Bonus_Percent__c;
            account.Desired_Additional_Compensation__c = talent.Account.Desired_Additional_Compensation__c;
            account.Desired_Placement_type__c = talent.Account.Desired_Placement_type__c;
            account.Desired_Schedule__c = talent.Account.Desired_Schedule__c;
            account.Willing_to_Relocate__c = talent.Account.Willing_to_Relocate__c;
            account.G2_Completed_Date__c = talent.Account.G2_Completed_Date__c ? talent.Account.G2_Completed_Date__c : '';
            account.G2_Completed_By__c = talent.Account.G2_Completed_By__c ? talent.Account.G2_Completed_By__c : '';
            account.G2_Completed__c = talent.Account.G2_Completed__c;
            account.Talent_Security_Clearance_Type__c = talent.Account.Talent_Security_Clearance_Type__c;
            account.Talent_FED_Agency_CLR_Type__c = talent.Account.Talent_FED_Agency_CLR_Type__c;

            account.Skills__c = typeof talent.Account.Skills__c !== 'undefined' ? talent.Account.Skills__c : '{"skills":[]}';
            account.Talent_Current_Employer__c = talent.Account.Talent_Current_Employer__c;
            account.Talent_Current_Employer_Text__c = talent.Account.Talent_Current_Employer_Text__c;
            try {
                account.Skills__c_parsed  =  JSON.parse(account.Skills__c);
            } catch (parseExcpetion) {
                this.showToast($A.get("$Label.c.FIND_ADD_TALENT_Helpdesk_Before_Update"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"), 'error');
            }
            //console.log('checking bools...');
            //console.log(account.G2_Completed__c, account.G2_Completed_Date__c, account.Do_Not_Contact__c);
            
            //Summary Tab Fields
            if (talent) {
                if(talent.Account.Talent_Preference_Internal__c && talent.Account.Talent_Preference_Internal__c.length > 2){
                    var ipjson = JSON.parse(talent.Account.Talent_Preference_Internal__c);
					let index = -1;
                   
					if (ipjson.geographicPrefs.desiredLocation) {
                        for(let j=0 ;j < ipjson.geographicPrefs.desiredLocation.length;j++){
						    if(ipjson.geographicPrefs.desiredLocation[j].country =='' && ipjson.geographicPrefs.desiredLocation[j].state ==''  && ipjson.geographicPrefs.desiredLocation[j].city ==''){
							    index= j;
						    }
					    }
					    if(index != -1){
						    ipjson.geographicPrefs.desiredLocation.splice(index,1);
					    }
                    }
					component.set("v.record", ipjson);
					//console.log('ipjson------------------'+JSON.stringify(ipjson));
                } else{
                    var ipjsonstr = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
                    var ipjson = JSON.parse(ipjsonstr);
						
                    component.set("v.record",ipjson );
					//console.log('ipjson--else----------------'+ipjson);
                } 
                
                
                // G2 
                var g2Date = "";
                var day = new Date().getDate();
                var month = new Date().getMonth() + 1;
                var year = new Date().getFullYear();
                
                var getDate = day.toString();
                if (getDate.length == 1){ 
                    getDate = "0"+getDate;
                }
                var getMonth = month.toString();
                if (getMonth.length == 1){
                    getMonth = "0"+getMonth;
                }
                
                var todaysDate = year + "-" + getMonth + "-" + getDate;
                
                if(typeof talent.Account.G2_Completed_Date__c !== "undefined"){
                    var tempDate = talent.Account.G2_Completed_Date__c.split("T");
                    if(tempDate.length > 0){
                        g2Date = tempDate[0];
                        var todayInMilliSeconds = new Date(todaysDate).getTime(); 
                        var g2DateInMilliSeconds =  new Date(talent.Account.G2_Completed_Date__c).getTime();
                        component.set("v.isG2DateToday", (todayInMilliSeconds === g2DateInMilliSeconds));
                        component.set("v.isG2Checked", (todayInMilliSeconds === g2DateInMilliSeconds));

                    }
                }
            }
            component.set("v.account", account);
            component.set("v.talentLoaded", true);
            var loadEvent = component.getEvent("talentAddEditLoaded");
            loadEvent.setParams({ 
                "isG2DateToday" :  component.get("v.isG2DateToday"), 
                "RWSDoNotCallFlag" : component.get("v.contact.RWS_DNC__c"),
                "candidateStatus" : talent.Candidate_Status__c
            });
            loadEvent.fire();
        } 
        catch (error) {
            component.set("v.account", account);
            this.showToast ($A.get("$Label.c.FIND_ADD_TALENT_Helpdesk") + error.message, $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"), 'error');
            //console.log ("Error occurred = " + error.message);
        }     
    },  

    translateActivity : function(component, lastActivity) {
            component.set("v.lastActivityDetails",lastActivity);
    },    

    onDNCCheck : function(component) {
        var dncFields = component.get("v.dncFields");
        if (!dncFields.Do_Not_Contact__c || dncFields.Do_Not_Contact__c == false) {
            dncFields.Do_Not_Contact_Reason__c = "";
            dncFields.Do_Not_Contact_Date__c = "";
            component.set("v.dncFields", dncFields);
        }
    }, 
   
    
    deleteRelatedContact : function(component) {
        var contact = component.get("v.contact");
        contact.Related_Contact__c = "";
        contact.Related_Contact__r = "";
        component.set("v.contact", contact);
        var relatedContactElement = component.find("relatedClient");
        $A.util.toggleClass(relatedContactElement, "slds-hide");

    },
    
    copyFilterValues : function(component) {
        //console.log("------CONTACT-----" + JSON.stringify( component.get('v.contact' ) ) );
		component.set('v.contact.FirstName', 			component.get("v.filteringFields.FirstName"));
        component.set('v.contact.LastName', 			component.get("v.filteringFields.LastName"));
        component.set('v.contact.Preferred_Name__c',	component.get("v.filteringFields.Preferred_Name__c"));
        component.set('v.contact.MobilePhone', 			component.get("v.filteringFields.Phone"));
        component.set('v.contact.Email',				component.get("v.filteringFields.Preferred_Email__c"));
        component.set('v.contact.Title',				'');
        
   },

   getKeyByValue : function(map, searchValue) {
        for(var key in map) {
            if (map[key] === searchValue) {
                return key;
            }
        }
    },

    createResumePreviewCmp : function(cmp,evt){
        var params = {recordId: cmp.get("v.recordId"),
                    showUploadButton:false,
                    contactRecord :cmp.get("v.contactRecord"),
                    talentId : cmp.get("v.accountId")};
    $A.createComponent(
            "c:C_TalentResumePreview",
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    /* if(componentName == 'C_TalentResumePreview'){ // Sandeep :for C_TalentResumePreview cmp assign instance to extra C_TalentResumePreview_leftpanel 
                        cmp.set("v.C_TalentResumePreview_leftpanel",newComponent);
                    }*/
                    cmp.set("v.C_TalentResumePreview", newComponent);
                    cmp.set("v.resumeLoaded",true)
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
        
    
    },

    createCandidateStatusBadge : function(cmp,evt){
        
        var params = {
                    candidateStatus: cmp.get("v.candidateStatus"),
                    class : "slds-m-right_x-small"
                };
        $A.createComponent(
                "c:C_TalentStatusBadge",
                params,
                function(newComponent, status, errorMessage){
                    
                    if (status === "SUCCESS") {
                        
                        cmp.set("v.C_TalentStatusBadge", newComponent);
                        
                    }
                    else if (status === "INCOMPLETE") {
                       // console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                    else if (status === "ERROR") {
                       // console.log("Error: " + errorMessage);
                        // Show error message
                    }
                }
            );
        
    
    },

    getandpoplanguages:function(component){
        this.callServer(component,'','',"getLanguages",function(response){
                    if(component.isValid()){
                        var opt = {};  
                        var key = '';
                        for(key in response) {     
                            opt[key] = response[key];
                        }
                        component.set("v.languageList", opt);
                        this.populateLanguage(component);
                    }
                },null,true);
    },

    checkError : function(response, component){
        var errors = response.getError();
        if (errors) {
            //console.log("Errors", errors);
            if (errors[0].fieldErrors) {
                //this.showServerSideValidation(errors, component);   
                var profileSectionComp = component.find("profileSection");
                profileSectionComp.showServerSideValidation(errors);
            } else if (errors[0] && errors[0].message) {
                this.showToast($A.get("$Label.c.ATS_UNEXPECTED_ERROR")+ `${errors[0].message}`, $A.get("$Label.c.FIND_ADD_TALENT_An_error_occurred"), 'error');
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showToast($A.get("$Label.c.ATS_UNEXPECTED_ERROR")+ `${errors[0].fieldErrors[0].statusCode}`,$A.get("$Label.c.FIND_ADD_TALENT_An_error_occurred"), 'error');
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showToast($A.get("$Label.c.ATS_UNEXPECTED_ERROR")+ `${errors[0].pageErrors[0].statusCode}`,$A.get("$Label.c.FIND_ADD_TALENT_An_error_occurred"), 'error');
            }else{
                this.showToast($A.get("$Label.c.ATS_UNEXPECTED_ERROR"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"), 'error');
            }
        } else {
            this.showToast($A.get("$Label.c.ATS_UNEXPECTED_ERROR")+ `${errors[0].message}`, $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"), 'error');
        }
    },   

    showServerSideValidation : function(errors, component) {
        for (var fieldName in errors[0].fieldErrors) {
            if (fieldName === "LinkedIn_URL__c") {
                
                var linkedinUrlField = component.find("linkedinUrl");
                if(linkedinUrlField){
                    linkedinUrlField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_LINKEDIN_URL")}]);
                    linkedinUrlField.set("v.isError",true);
                    $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");
                }
                
            } else if (fieldName === "Email") {
                
                
                var emailField = component.find("email");
                if(emailField){
                   /* Start for story S-79062 */
                var message = ''; 
                var thisFieldError = errors[0].fieldErrors[fieldName];
                for(var j=0; j < thisFieldError.length; j++) {
                                    message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
                                }
                   //console.log('Message::'+ message);
                      emailField.set("v.errors", [{message: message}]);
                     // before it was emailField.set("v.errors", [{message: "Please enter valid email address"}]);
                    /* End for story S-79062 */
                    emailField.set("v.isError",true);
                    $A.util.addClass(emailField,"slds-has-error show-error-message");
                }
                
            } else if (fieldName === "Other_Email__c") {
                
                var otherEmailField = component.find("otherEmail");
                if(otherEmailField){
                    otherEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL")}]);
                    otherEmailField.set("v.isError",true);
                    $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
                }
                
            }  else if (fieldName === "Work_Email__c") {
                
                var workEmailField = component.find("workEmail");
                if(workEmailField){
                    workEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL")}]);
                    workEmailField.set("v.isError",true);
                    $A.util.addClass(workEmailField,"slds-has-error show-error-message");
                }
                
            } else {
                this.showToast (`An unexpected error has occurred. Please try again! ${errors[0].fieldErrors[fieldName][0].message + errors[0].fieldErrors[fieldName][0].statusCode}`, 'Error!', 'error');
            }
        }  
        
    },   

    closeModal : function(component) {
        component.set("v.cssStyle", "");
        this.toggleClassInverse(component,'backdropAddEditSumm','slds-backdrop--');
        this.toggleClassInverse(component,'modaldialogAddEditSumm','slds-fade-in-');
        component.set("v.callServer",true);
        component.set("v.ProfileTabCreated", false);
        // var e = component.getEvent("refreshTalentHeaderComponent");
        var appEvent = $A.get("e.c:RefreshTalentHeader");
        appEvent.fire();
        
        $A.get('e.force:refreshView').fire();
        component.destroy();
    },

    /*savePickList:function(component) {
        var locationSectionComp = component.find("locationSection");
        var commuteType = locationSectionComp.getLocationUnMappedData();
        if(commuteType != "" && commuteType != "Select"){
           component.set("v.record.geographicPrefs.commuteLength.unit",commuteType); 
        }

    },*/

    initializeNewDNC: function(component) {
        component.set("v.dncFields", {
            "Do_Not_Contact__c":  null,
            "Do_Not_Contact_Reason__c": "",
            "Do_Not_Contact_Date__c": null
        }); 
    }
})