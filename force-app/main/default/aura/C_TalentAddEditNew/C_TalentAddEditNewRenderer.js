({
		rerender: function(component, helper) {
		/* Rajeesh S-108774 - Ability to Restore Backup from 'refreshed' G2 on Talent Summary Modal
			 * create a track fields object with all the variables that need to be backed up 
    		 * and take a backup using the backup component.
			 * backup component creates a key for the card (using card number and contact id), and create a json and base 64 encode
			 * the string before storing in session storage.

			 Rajeesh 1/29/2019: Refactoring After G2 changes.
			 The component is constructed using multiple child components, and data elements get modified in child components which will not trigger
			 parents rerender. Forcing it by passing a reference to parent's instance and updating a dummy variable in parent for every change that happens in any of the children,
		*/
		this.superRerender();
		var trackObj={};
		var initOnly = component.get("v.initOnly");
		var eraseBackup = component.get('v.eraseBackup');
		//var usrHasAccess = component.get('v.usrHasAccess');
		//console.log('renderer userhasaccess'+usrHasAccess);
		if(!initOnly && !eraseBackup){
//console.log('---name:'+component.getName()+'--:rerender timestamp:'+Date.now());
/* Original
			trackObj.context			=	component.get('v.context');
			trackObj.contact			=	component.get('v.contact');
			trackObj.account			=	component.get('v.account');
			trackObj.dncFields			=	component.get('v.dncFields');
			trackObj.filteringFields	=	component.get('v.filteringFields');
			trackObj.mode				=	component.get('v.mode');
			trackObj.runningUser		=	component.get('v.runningUser');
			//trackObj.initOnly			=	component.get('v.initOnly');
			//trackObj.actualObj			=	component.get('v.actualObj');
			//trackObj.eraseBackup=component.get('v.eraseBackup');
			trackObj.g2CommentsId		=	component.get('v.g2CommentsId');
			trackObj.parsedInfo			=	component.get('v.parsedInfo');
			trackObj.lastActivityDetails=	component.get('v.lastActivityDetails');
			trackObj.isResumeParsed		=	component.get('v.isResumeParsed');
			trackObj.isG2Checked		=	component.get('v.isG2Checked');
			trackObj.isG2DateToday		=	component.get('v.isG2DateToday');
			trackObj.presetLocation		=	component.get('v.presetLocation');
			trackObj.toggleLayout		=	component.get('v.toggleLayout');
			trackObj.talentLoaded		=	component.get('v.talentLoaded');
			//trackObj.C_TalentResumePreview=	component.get('v.C_TalentResumePreview');
			//trackObj.C_TalentStatusBadge=	component.get('v.C_TalentStatusBadge');
			//trackObj.parentCmp			=	component.get('v.parentCmp');
			trackObj.accountId			=	component.get('v.accountId');
			trackObj.contactId			=	component.get('v.contactId');
*/
			trackObj.g2CommentsId		=	component.get('v.g2CommentsId');
			trackObj.talentId			=	component.get('v.talentId');
			trackObj.talent				=	component.get('v.talent');
			//trackObj.recordId			=	component.get('v.recordId');
			//trackObj.edit				=	component.get('v.edit');
			trackObj.record				=	component.get('v.record');
			trackObj.currentSkills		=	component.get('v.currentSkills');
			trackObj.languageList		=	component.get('v.languageList');
			trackObj.currentLanguages	=	component.get('v.currentLanguages');
			trackObj.countries			=	component.get('v.countries');
			trackObj.CurrencyList		=	component.get('v.CurrencyList');
			trackObj.reliable			=	component.get('v.reliable');
			trackObj.nationalOpp		=	component.get('v.nationalOpp');
			trackObj.commuteType		=	component.get('v.commuteType');
			trackObj.desiredCurrency	=	component.get('v.desiredCurrency');
			trackObj.totalComp			=	component.get('v.totalComp');
			trackObj.totalCompMax		=	component.get('v.totalCompMax');
			trackObj.countryKey0		=	component.get('v.countryKey0');
			trackObj.countryKey1		=	component.get('v.countryKey1');
			trackObj.countryKey2		=	component.get('v.countryKey2');
			trackObj.countryKey3		=	component.get('v.countryKey3');
			trackObj.countryKey4		=	component.get('v.countryKey4');
			trackObj.countryValue		=	component.get('v.countryValue');
			trackObj.stateKey			=	component.get('v.stateKey');
			trackObj.stateValue			=	component.get('v.stateValue');
			trackObj.geoComments		=	component.get('v.geoComments');
			trackObj.isG2Checked		=	component.get('v.isG2Checked');
			trackObj.todayDate			=	component.get('v.todayDate');
			trackObj.g2Date				=	component.get('v.g2Date');
			trackObj.isG2DateToday		=	component.get('v.isG2DateToday');
			trackObj.runningUser		=	component.get('v.runningUser');
			trackObj.usrOwnership		=	component.get('v.usrOwnership');
			trackObj.isOPCOInDictionary	=	component.get('v.isOPCOInDictionary');
			trackObj.modalTitle			=	component.get('v.modalTitle');
			//trackObj.C_TalentAddEdit	=	component.get('v.C_TalentAddEdit');
			//trackObj.C_TalentResumePreview=	component.get('v.C_TalentResumePreview');
			//trackObj.C_TalentStatusBadge=	component.get('v.C_TalentStatusBadge');
			trackObj.contactRecord		=	component.get('v.contactRecord');
			trackObj.countriesMap		=	component.get('v.countriesMap');
			trackObj.presetLocation		=	component.get('v.presetLocation');
			trackObj.resumeLoaded		=	component.get('v.resumeLoaded');
			trackObj.jobTitle			=	component.get('v.jobTitle');
			trackObj.contact			=	component.get('v.contact');
			trackObj.account			=	component.get('v.account');
			//this variable is needed to make certain server calls- dont back it up 
			trackObj.callServer			=	component.get('v.callServer');
			trackObj.source				=	component.get('v.source');
			trackObj.usrCategory		=	component.get('v.usrCategory');
			trackObj.accountId			=	component.get('v.accountId');
			trackObj.contactId			=	component.get('v.contactId');
			trackObj.attachmentId		=	component.get('v.attachmentId');
			trackObj.homePhonePreferred	=	component.get('v.homePhonePreferred');
			trackObj.mobilePhonePreferred=component.get('v.mobilePhonePreferred');
			trackObj.workPhonePreferred	=	component.get('v.workPhonePreferred');
			trackObj.otherPhonePreferred=	component.get('v.otherPhonePreferred');
			trackObj.emailPreferred		=	component.get('v.emailPreferred');
			trackObj.otherEmailPreferred=	component.get('v.otherEmailPreferred');
			trackObj.workEmailPreferred	=	component.get('v.workEmailPreferred');
			trackObj.candidateStatus	=	component.get('v.candidateStatus');
			trackObj.MailingStateKey	=	component.get('v.MailingStateKey');
			trackObj.MailingCountryKey	=	component.get('v.MailingCountryKey');
			trackObj.stateError			=	component.get('v.stateError');
			trackObj.isChecked			=	component.get('v.isChecked');
			trackObj.expandAll			=	component.get('v.expandAll');
			trackObj.expanded			=	component.get('v.expanded');
			trackObj.viewListFunction	=	component.get('v.viewListFunction');
			trackObj.viewListIndex		=	component.get('v.viewListIndex');
			trackObj.viewList			=	component.get('v.viewList');
			trackObj.loadingData		=	component.get('v.loadingData');
			trackObj.loadingCount		=	component.get('v.loadingCount');
			trackObj.recordCount		=	component.get('v.recordCount');
			trackObj.maxRows			=	component.get('v.maxRows');
			trackObj.additional			=	component.get('v.additional');
			//trackObj.reloadData			=	component.get('v.reloadData');
			trackObj.records			=	component.get('v.records');
			trackObj.isGoogleLocation	=	component.get('v.isGoogleLocation');
			trackObj.showSpinner		=	component.get('v.showSpinner');
			trackObj.iconName			=	component.get('v.iconName');
			trackObj.parentInitialLoad	=	component.get('v.parentInitialLoad');
			trackObj.titleError			=	component.get('v.titleError');

			var dataBackup = component.find("clientSideBackup"); 
			dataBackup.setDataBackup(trackObj); 
		}
		//component.set("v.initOnly",false);
		//helper.setReRenderAttributes(component);
		//return this.superRerender();
	} 
})