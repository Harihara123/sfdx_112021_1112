({
    doInit: function(component,event,helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",true);
		var parsedJobTitle = component.get('v.parsedJobTitle');
        if(!$A.util.isEmpty(parsedJobTitle)){
			component.set('v.contact.Title',parsedJobTitle);
		}
        helper.populateLanguage(component);
		helper.showMoreForOverview(component);
    },

	talentLoaded: function(component,event,helper) {
        helper.showMoreForOverview(component);
        if(component.get("v.edit")) {
            helper.initCurrentCompany(component);
        }
       
    },

	recordChangeHandler: function(component,event,helper) {
		// Load language list if not already done.
		if (!component.get("v.languagesLoaded")) {
			helper.loadLanguageList(component);
		}

		component.set("v.initOnly",false);
		
	},

	toggleSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronright': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronright',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

    toggleSubSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronup': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronup',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
		var acc = component.get("v.acc");
        if(acc[3].className == 'show'){
            helper.setFocusToComponent(component,event,'clearanceTypeId');
        }
       
        if(acc[3].className == 'hide'){
            helper.setFocusToField(component,"G2jobTitle");
        }
        event.stopPropagation();
    },

	validTalent : function(component, event, helper) {
		return helper.validateTitleError(component,event);
	},

	languagesUpdateHandler: function(component, event, helper) {
		// Check boolean indicator to ignore the initial load.
		if (component.get("v.languagesLoaded") || component.get("v.context") === 'inline') {
			helper.populateLanguageCode(component);
		}
	},

    enableInlineEdit: function (component, event, helper) {
        let evt = component.getEvent("enableInlineEdit"),
            editMode = component.get("v.edit");

        component.set("v.edit", !editMode);
        evt.fire();
        if(!editMode) {
            helper.initCurrentCompany(component);
        }
       
    },

	invokeDoInit : function (component, event, helper) {
		if (component.get("v.context") === 'inline') {
			helper.loadLanguageList(component);
		}
    },

    lwcJobTitleChange : function(cmp, e, helper) {
        var jobTitle = e.getParam('jobtitle');
        cmp.set('v.contact.Title', jobTitle)
    }
})