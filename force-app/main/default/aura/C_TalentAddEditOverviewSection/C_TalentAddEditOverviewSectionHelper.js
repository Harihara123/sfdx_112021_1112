({
	toggleAccordion: function(component, event, toggleState) {
        var toggleState;

        var acc = component.get("v.acc");    
        var accordionId = event.currentTarget.id;
        var icon = acc[accordionId].icon;
        var className = acc[accordionId].className;

        component.set(`v.acc[${accordionId}].icon`, toggleState[icon]);
        component.set(`v.acc[${accordionId}].className`, toggleState[className]);

    },

	validateTitleError : function(component,event) {
        var validTalent = true;
		var hiddenField = component.find("hiddenTitle");
        var hidden = hiddenField.get("v.value");
        if (!hidden || hidden === "") {
            validTalent = false;
            component.set("v.titleError", true);
			
			event.preventDefault();
            this.showError(component.get("v.errorMessageJobTitle"));
			this.setFocusToField(component,"G2jobTitle");
			
	}
         else {
            component.set("v.titleError", false);
        }
        return validTalent;
    },

	showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

	loadLanguageList: function(component) {
		var action = component.get("c.getLanguages");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
				var opt = {};  
                var key = '';
                for(key in response) {     
                    opt[key] = response[key];
                }
                component.set("v.languageList", opt);
                this.populateLanguage(component);
				// After languages are updated, set boolean indicator
				component.set("v.languagesLoaded", true);
            }
        });
        $A.enqueueAction(action);
	},

	populateLanguage: function(component){
		var currentLang =  [];
		var cLangObj = [];
		var langMap = {};
		if (component.get("v.record") !== '') {
			if(component.get("v.record.languages").length > 0) {
				currentLang = component.get("v.record.languages"); 
				langMap = component.get("v.languageList"); 
				for(var i = 0;i<currentLang.length;i++) {
					cLangObj.push({"key" : currentLang[i],"value" : langMap[currentLang[i]]});
				}
			}
			component.set("v.currentLanguages", cLangObj);      
		}
	},

	populateLanguageCode:function(component) {
		var currentLang =  [];
		var codes = []; 
		if (component.get("v.currentLanguages").length > 0) {
			currentLang = component.get("v.currentLanguages"); 
			for (var i=0; i<currentLang.length; i++) {
				codes.push(currentLang[i].key);
			}
		}
		component.set("v.record.languages", codes);
	},

	showMoreForOverview: function(component) {
		var oview = component.get("v.account.Talent_Overview__c");
		if (oview && oview.length > 0) {
            this.switchToShowMore(component);
        }
	},

	switchToShowMore: function(component) {
        let pageAcc = component.get('v.acc');

        pageAcc[3].className = 'show';
        pageAcc[3].icon = 'utility:chevronup';

        component.set("v.acc", pageAcc);
	},

	setFocusToComponent: function(component, event, elementAuraId) {
		event.preventDefault();
		if(elementAuraId==null || (component.get("v.context")=="inline")) return;
		const elementId = component.find(elementAuraId);
			window.setTimeout(
            $A.getCallback(function() {
               // wait for element to render then focus
			   
               elementId.focus();
            }), 500
        );
	},

	setFocusToField : function(component,fieldId){
		if(fieldId==null) return;
		window.setTimeout(	
				$A.getCallback(function() {		
					document.getElementById(fieldId).focus();	
			}), 250);
	},

	initCurrentCompany: function (cmp) {
		const account = cmp.get("v.account");
		const currEmployer = (account.Talent_Current_Employer_Text__c || '');
		
		account.Talent_Current_Employer_Text__c = currEmployer;

		cmp.set("v.account", account);
		
		//Used to Disable/Enable currentCompany field
	
		const params = { "AccountId": account.Id };
		const serverMethod = 'c.enableCurrentCompany';
		const action = cmp.get(serverMethod);
		action.setParams(params);
		action.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
				const res = response.getReturnValue();
				
				cmp.set("v.curEmpDisabled", res);
				// If there is new data from N2P and current company field isnt disabled
				if (cmp.get('v.fromN2p') && !res) {

					const parsedEmp = cmp.get('v.parsedEmployer');

					if (parsedEmp != undefined && parsedEmp != "") {
						account.Talent_Current_Employer_Text__c = parsedEmp;
						cmp.set('v.account', account);
						//cmp.find("currentCompany").set("v.value", parsedEmp);
					}

				}
			}
			else {
				console.log("FAILED INITILIZING CURRENT COMPANY FIELD");
			}
		});
		$A.enqueueAction(action);

		

	}
})