({
	doInit: function (component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",true);
		if(component.get("v.context") !== "modal") {
			component.set("v.talentLoaded", true);
            helper.initializeAddEditTalent(component);// for defect D-07495
            let pageAcc = component.get('v.acc');

            pageAcc[2].className = 'show';
            pageAcc[2].icon = 'utility:chevronup';

            component.set("v.acc", pageAcc);
			//component.set("v.talentLoaded", true);
			
        }
		//Rajeesh Notes Parser refactoring 3/12/2019
		var parsedLivingAddress = component.get('v.parsedLivingAddress');
        if(!$A.util.isEmpty(parsedLivingAddress)){
            /*
            var addressSearchText = parsedLivingAddress.City+', '+parsedLivingAddress.State+', '+parsedLivingAddress.Country;
            //console.log('--addressSearchText--'+addressSearchText);
			if(addressSearchText !=', , '){
				component.set('v.addressSearchText',addressSearchText);
			}*/
            // Fixing issue D-12854
            var addressSearchText = ($A.util.isEmpty(parsedLivingAddress.City)?'':(parsedLivingAddress.City));
            if(!$A.util.isEmpty(parsedLivingAddress.State)){
                addressSearchText = (addressSearchText.length>0?(addressSearchText+','):'')+parsedLivingAddress.State;
            }
            if(!$A.util.isEmpty(parsedLivingAddress.Country)){
                addressSearchText = (addressSearchText.length>0?(addressSearchText+','):'')+parsedLivingAddress.Country;
            }
            component.set('v.addressSearchText',addressSearchText);
        }
		helper.getCountryMappings(component,helper);

	},

	talentLoaded : function (component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
		if (component.get("v.talentLoaded") === true) {
		var talent = component.get("v.contact");
			if (talent.Preferred_Phone__c) {
                if (talent.Preferred_Phone__c === 'Home') {
                    component.set("v.homePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Mobile') {
                    component.set("v.mobilePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Work') {
                    component.set("v.workPhonePreferred", true);
                }
                    else if (talent.Preferred_Phone__c === 'Other') {
                        component.set("v.otherPhonePreferred", true);
                    }
            }
            
            if (talent.Preferred_Email__c) {            
                if (talent.Preferred_Email__c === 'Email') {
                    component.set("v.emailPreferred", true);
                } else if (talent.Preferred_Email__c === 'Alternate') {
                    component.set("v.otherEmailPreferred", true);
                } else if (talent.Preferred_Email__c === 'Work') {
                    component.set("v.workEmailPreferred", true);
                }
            }  

			var parsedLivingAddress = component.get('v.parsedLivingAddress');
			var talentLocation;

			if ($A.util.isEmpty(parsedLivingAddress)) {
				talentLocation = helper.generateAddress(talent.MailingStreet,talent.MailingCity,talent.MailingState,talent.Talent_Country_Text__c,talent.MailingPostalCode);
			} else {
				talentLocation = component.get('v.addressSearchText');
				component.set("v.contact.MailingCity", parsedLivingAddress.City);
				component.set("v.contact.MailingState", parsedLivingAddress.State);
				component.set("v.contact.MailingCountry", parsedLivingAddress.Country);
				component.set("v.contact.MailingPostalCode", null);
			}

			if (talentLocation) {
				component.set("v.presetLocation", talentLocation);
			}
            /*if (talent.MailingStreet) {
				component.set("v.presetLocation", talent.MailingStreet);
			}*/

			helper.getPicklist(component, 'Salutation', 'salutation');
			if(component.get("v.context") !== "page") {
				helper.loadSD2Picklists(component);
			}
			
		}
	},


	updateMailingCountryCode: function(cmp, event, helper) {
	//Rajeesh client side data backup fix after G2 redesign.
		cmp.set("v.initOnly",false);
	    helper.updateMailingCountryCode(cmp,event);
    },

	handleLocationSelection : function(component,event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
		component.set("v.isGoogleLocation", true);
        var talent = component.get("v.contact");			
        var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode,countrymap,displayText;
		mailingstreet = '';
		streetnum = '';
		streetname = '';
		city = '';
		state = '';
		country = '';
		countrycode = '';
		postcode = '';
		displayText = '';
		var addr = event.getParam("addrComponents");
		var latlong = event.getParam("latlong");    //S-96255 - Update MailLat, MailLong fields on Contact - Karthik    
		countrymap = component.get("v.countriesMap"); 
		displayText = event.getParam("displayText");
        //console.log('address component'+JSON.stringify(addr));
        for (var i=0; i<addr.length; i++) {
            if (addr[i].types.indexOf("street_number") >= 0) {
                streetnum = addr[i].long_name;
            }
            if (addr[i].types.indexOf("route") >= 0) {
                streetname = addr[i].long_name;
            }        
            if (addr[i].types.indexOf("postal_code") >= 0) {
                postcode = addr[i].long_name;
            }
            if (addr[i].types.indexOf("locality") >= 0) {
                city = addr[i].long_name;
            }
            if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
                state = addr[i].long_name;
            }
            if (addr[i].types.indexOf("country") >= 0) {
                country = addr[i].long_name;
                countrycode = countrymap[addr[i].short_name];
            }        
        }
        //console.log('teststreet',streetnum,streetname);
        if(streetnum) {
            mailingstreet = streetnum;
        } 
        if(streetnum && streetname) {
            mailingstreet = mailingstreet + ' ';
        }
        if(streetname) {
            if(mailingstreet != ''){
                mailingstreet = mailingstreet + streetname;        
            } else {
                mailingstreet = streetname;
            }
        }
        //console.log(mailingstreet,city,state,countrycode,country,postcode,displaytext);
        component.set("v.contact.MailingStreet",mailingstreet);
        component.set("v.contact.MailingCity",city);
        component.set("v.contact.MailingState",state);
        component.set("v.contact.MailingCountry",countrycode);
        component.set("v.contact.Talent_Country_Text__c",country);
        component.set("v.contact.MailingPostalCode",postcode);
       
		//var str = displayText.substr(0,displayText.indexOf(',')); // added to display only street address - Manish
		                          
        component.set("v.presetLocation",helper.generateAddress(mailingstreet,city,state,country,postcode));
		component.set("v.streetAddress2","");
        
		//S-96255 - Update MailLat, MailLong fields on Contact
		if(latlong && (mailingstreet || city || postcode)){
			//component.set("v.contact.GoogleMailingLatLong__Latitude__s",latlong.lat);
			//component.set("v.contact.GoogleMailingLatLong__Longitude__s",latlong.lng);
			component.set("v.contact.MailingLatitude",latlong.lat);
			component.set("v.contact.MailingLongitude",latlong.lng);				
		} else {
			component.set("v.contact.MailingLatitude",null);
			component.set("v.contact.MailingLongitude",null);				
		}
		component.set("v.contact.LatLongSource__c","Google");
		component.set("v.viewState","EDIT");
		component.set("v.parentInitialLoad",true);
		var countryCode = helper.getKeyByValue(component.get("v.countriesMap"),countrycode);
		component.set("v.MailingCountryKey",countryCode);
    },

	toggleSection: function(component, event, helper) {
        component.set('v.sectionCollapsed', !component.get('v.sectionCollapsed'));
        var toggle = {
            'utility:chevronright': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronright',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        event.stopPropagation();
    },

    
    toggleSubSection: function(component, event, helper) {
        component.set('v.expanded', !component.get('v.expanded'));
        var toggle = {
            'utility:chevronup': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronup',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
        var acc = component.get("v.acc");
        if(acc[2].className == 'show'){
            helper.setFocusToComponent(component,'MiddleName');
        }
       
        if(acc[2].className == 'hide'){
            helper.setFocusToComponent(component,'firstName');
        }
        event.preventDefault();
    },
	//Start STORY S-114510 Added Communities Data UI update by Siva
	toggleAllSections: function(component, event, helper) {
		helper.setFocusToComponent(component,"firstName");
		var acc = component.get("v.acc");   
		if(component.get('v.acc[1].className') === 'hide' && component.get('v.acc[2].className') === 'show') {
			component.set('v.acc[2].icon', 'utility:chevrondown');
			component.set('v.acc[2].className', 'show');
		} else {
			var toggle = {
				'utility:chevronup': 'utility:chevrondown',
				'utility:chevrondown': 'utility:chevronup',
				show: 'hide',
				hide: 'show'
			}
			component.set('v.acc[2].icon', toggle[acc[2].icon]);
			component.set('v.acc[2].className', toggle[acc[2].className]);
		}	
		component.set('v.acc[1].icon', 'utility:chevrondown');
        component.set('v.acc[1].className', 'show');
	},
	//End STORY S-114510 Added Communities Data UI update by Siva
    toggleLayout: function(component, event, helper) {
        let newLayout = component.get("v.toggleLayout");
        component.set("v.toggleLayout", newLayout);
    },

	// Below methods called from TalentAddEditNew (Parent Component)
	validTalent : function(component, event, helper) {
		return helper.validTalent(component, event);
	},
    dncFormValid: function(cmp, e, h) {
        console.log('dnc in profSection controller');
        return h.dncFormValid(cmp, e);
    },

	setDataBeforeSaveTalentCall : function(component, event, helper) {
		//Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
		var contact = helper.setDataBeforeSaveTalentCall(component, event);
		return contact;
	},

	  onMobilePhoneCheck : function(component, event, helper) {
	  //Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
    var checkCmp = component.find("homePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workPhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherPhonepreferred");
    checkCmp.set("v.value", false);  
  },

  onHomePhoneCheck : function(component, event, helper) {
  //Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
    var checkCmp = component.find("mobilePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workPhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherPhonepreferred");
    checkCmp.set("v.value", false);    
  },

  onWorkPhoneCheck : function(component, event, helper) {
  //Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
    var checkCmp = component.find("mobilePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("homePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherPhonepreferred");
    checkCmp.set("v.value", false);    
  },
    
  onOtherPhoneCheck : function(component, event, helper) {
  //Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
    var checkCmp = component.find("mobilePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("homePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workPhonepreferred");
    checkCmp.set("v.value", false);    
  },  

  onEmailCheck : function(component, event, helper) {
  //Rajeesh client side data backup fix after G2 redesign.
		component.set("v.initOnly",false);
    var checkCmp = component.find("otherEmailPreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workEmailPreferred");
    checkCmp.set("v.value", false);  
  },

  onOtherEmailCheck : function(component, event, helper) {
   //Rajeesh client side data backup fix after G2 redesign.
	component.set("v.initOnly",false);
    var checkCmp = component.find("emailPreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workEmailPreferred");
    checkCmp.set("v.value", false);  
  },

   onWorkEmailCheck : function(component, event, helper) {
    //Rajeesh client side data backup fix after G2 redesign.
	component.set("v.initOnly",false);
    var checkCmp = component.find("emailPreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherEmailPreferred");
    checkCmp.set("v.value", false);  
  },  

  userDataReceived1: function(component, event, helper) {
   //Rajeesh client side data backup fix after G2 redesign.
	component.set("v.initOnly",false);
	if(component.get("v.context") === "page") {
		helper.loadSD2Picklists(component);
	}
	helper.getCountryMappings(component);
  },

  showServerSideValidation : function(component, event, helper) {
		var parameters = event.getParam("arguments");
		helper.showServerSideValidation(parameters.errors, component);
  },
  toggleProfSection: function(component, event, helper) {
        var params = event.getParam('arguments');
      if (params.toggle && component.get('v.sectionCollapsed')) {
          if (document.getElementById('1')) { 
              document.getElementById('1').click();
          }
      }
      if (params.toggle && !component.get('v.expanded')) {
          if (document.getElementById('2')) { 
              document.getElementById('2').click();
          }
      }
  }
})