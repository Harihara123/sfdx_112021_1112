({
	initializeAddEditTalent : function(component) {
		component.set("v.homePhonePreferred", false);
        component.set("v.mobilePhonePreferred", false);
        component.set("v.workPhonePreferred", false);
        component.set("v.otherPhonePreferred", false);
        component.set("v.emailPreferred", false);
        component.set("v.otherEmailPreferred", false);
        component.set("v.workEmailPreferred", false);
	},

	getCountryMappings : function(component,helper) {   
        var action = component.get("c.getCountryMappings");
		var conrecord = component.get("v.contact");
        var countryCode;
        action.setCallback(this,function(response) {
            component.set("v.countriesMap",response.getReturnValue());
			/*if (component.get("v.contact.Talent_Country_Text__c") !== 'undefined' && component.get("v.contact.Talent_Country_Text__c") !== null
				 && component.get("v.contact.Talent_Country_Text__c").length === 0 
					&& component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States') {
				component.set("v.contact.Talent_Country_Text__c",'United States');
				component.set("v.MailingCountryKey",'US');	
			} */
			
			if (conrecord.Talent_Country_Text__c) {
				countryCode = this.getKeyByValue(component.get("v.countriesMap"),conrecord.MailingCountry);
				component.set("v.MailingCountryKey",countryCode);
			}
			if((conrecord.Talent_Country_Text__c === undefined || conrecord.Talent_Country_Text__c === null || conrecord.Talent_Country_Text__c.length === 0) && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
				component.set("v.contact.Talent_Country_Text__c",'United States');
				component.set("v.MailingCountryKey",'US');
			}  
        });
        $A.enqueueAction(action);
    },

	updateMailingCountryCode: function(cmp, event) {
		var conKey = cmp.get('v.MailingCountryKey');
		var countrymap = cmp.get("v.countriesMap");
		var countrycode ;
		if (conKey !== null && typeof conKey !== 'undefined' && countrymap !== null) {
				countrycode = countrymap[conKey];
				if(countrycode !== null && typeof countrycode !== 'undefined'){
					cmp.set("v.contact.MailingCountry",countrycode);
				}
			}
	},

	toggleAccordion: function(component, event, toggleState) {
        var toggleState;

        var acc = component.get("v.acc");    
        var accordionId = event.currentTarget.id;
        var icon = acc[accordionId].icon;
        var className = acc[accordionId].className;     
                
        component.set(`v.acc[${accordionId}].icon`, toggleState[icon]);
        component.set(`v.acc[${accordionId}].className`, toggleState[className]);    
    },
/*Ashwini-- Make "G2 component" component JAWS complaint
             Inorder to get focus on middleName after clicking on Showmore link*/
	setFocusToComponent: function(component,elementAuraId) {
	if(elementAuraId==null) return;
	const elementId = component.find(elementAuraId);              
	setTimeout(function(){ elementId.focus(); }, 200);
   
	},
    
    generateAddress : function(street,city,state,country,postcode){
        
        var talentLocation = this.appendAddress(talentLocation, street);
        talentLocation = this.appendAddress(talentLocation, city);
        talentLocation = this.appendAddress(talentLocation, state);
        talentLocation = this.appendAddress(talentLocation, country);
        talentLocation = this.appendAddress(talentLocation, postcode);
        return talentLocation;
    },

	appendAddress : function (address, appendAddress) {     
        if (appendAddress && appendAddress.length > 0 ) {
            address = ((address && address.length > 0 ) ? ( address + ', ' + appendAddress) : appendAddress);
        }  
        return address;
    },

	 getPicklist: function(component, fieldName, elementId) {
        var serverMethod = 'c.getPicklistValues';
        var params = {
            "objectStr": "Contact",
            "fld": fieldName
        };
        var action = component.get(serverMethod);
        action.setParams(params);

        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }

                component.find("salutation").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    getAccountPicklist: function(component, fieldName, elementId) {
        var serverMethod = 'c.getPicklistValues';
        var params = {
            "objectStr": "Account",
            "fld": fieldName
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                if(elementId == "Gender")
                    component.find("Gender").set("v.options", opts);
                if(elementId == "GenderInfo")
                    component.find("GenderInfo").set("v.options", opts);
                
                if(elementId == "Race")
                    component.find("Race").set("v.options", opts);
                if(elementId == "RaceInfo")
                    component.find("RaceInfo").set("v.options", opts);
                // if(elementId == "Reason")
                //     component.find("Reason").set("v.options", opts);
                
            }
        });
        $A.enqueueAction(action);
    },


	updatePreferredValues : function(contact, component) {
        
        if (component.get("v.homePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Home';
        } else if (component.get("v.mobilePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Mobile';
        } else if (component.get("v.workPhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Work';
        } else if (component.get("v.otherPhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Other';
        } else {
			contact.Preferred_Phone__c = null;
		}
        

        if (component.get("v.emailPreferred") === true) {
            contact.Preferred_Email__c = 'Email';
        } else if (component.get("v.otherEmailPreferred") === true) {
            contact.Preferred_Email__c = 'Alternate';
        }
        else if (component.get("v.workEmailPreferred") === true) {
            contact.Preferred_Email__c = 'Work';
        } else {
			contact.Preferred_Email__c = null;
		}

        if(component.get("v.pageReference") && component.get("v.pageReference").state.c__isResumeParsed === 'Y'){
            contact.Source_Type__c = 'Uploaded';
        }else{
            contact.Source_Type__c = 'User Entered';
        }

        return contact;
    },

    validTalent: function(component,event) {
        var validTalent = true;
        validTalent = this.validateFirstName (component, validTalent);
        validTalent = this.validateLastName (component, validTalent);
        // validTalent = this.validateTitleError (component, validTalent);
        var expirationDateField = component.find("expirationDate");
        if (expirationDateField) {
            validTalent = this.validateExpirationDate (component, validTalent);
        }
        validTalent = this.validateLinkedInUrl (component, validTalent);
        validTalent = this.validateWebsiteUrl (component, validTalent);
        validTalent = this.validateStreetAddress(component, validTalent); // Added by Manish
        //S-59229 Validate Preferred Options - Added by Karthik
        validTalent = this.validatePrefOptions (component, validTalent);
        
          //S-226585 - added 3 new Opco IND,SJA,EAS
        if (component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aerotek")
		    || component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aston_Carter")  
			|| component.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Newco")  
		    || component.get("v.runningUser.OPCO__c") === 'ONS'  
            || component.get("v.runningUser.OPCO__c") === 'TEK' 
			|| component.get("v.runningUser.OPCO__c") === 'ASI' ) {
              // var validTalent = this.validateZipAndState (component, validTalent);
            validTalent = this.validateStateCountry(component, validTalent,event);
                // for story S-96223 TEK or ONS user phone or email mandatory.
            validTalent = this.validatePhoneEmailLinkedInForTEKONS (component, validTalent); 
        }
        else{            
            // for Users other than TEK or ONS phone or email or LinkedIN url is mandatory
           validTalent = this.validatePhoneEmailLinkedIn (component, validTalent); 
        }
        /* Start for Strory S-79062 */
   //     validTalent = this.validateEMEAUser(component, validTalent);// for story S-83514
        /* End for Strory S-79062 */
        return validTalent;
    },
    dncFormValid: function(cmp, e) {
        //placeholder for dnc validation
        console.log('dnc in profSection helper');
        var dncFormValid = true;
        if (cmp.get("v.runningUser.Profile.Name") !== 'ATS_Recruiter') {
                dncFormValid = cmp.find("dncEditForm").validTalent();
        }
        return dncFormValid;
    },
	validateStreetAddress : function(component, validTalent) {
		component.set("v.invalidLocation", false);
		if (component.get("v.streetAddress2") && !component.get("v.presetLocation")) {
			validTalent = false;
            // If location is not present or invalid, trigger error message / styles
            component.set("v.invalidLocation", true);
		}
		return validTalent;
	},
    // for story S-96223 TEK or ONS user phone or email mandatory.
    validatePhoneEmailLinkedInForTEKONS: function (component , validTalent){
       var mobilePhoneField = component.find("mobilePhone");
        var mobilePhone = mobilePhoneField.get("v.value");
        if(!this.validateMobilePhone(mobilePhone)) {
           mobilePhoneField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_MOBILE")}]);
           mobilePhoneField.set("v.isError",true);
           validTalent = false;
		   this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_MOBILE"));
		   this.setFocusToComponent(component,"mobilePhone");
        }

        var homePhoneField = component.find("homePhone");
        var homePhone = homePhoneField.get("v.value");
        if(!this.validateMobilePhone(homePhone)) {
           homePhoneField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_HOMEPHONE")}]);
           homePhoneField.set("v.isError",true);
           validTalent = false;
		   this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_HOMEPHONE"));
		   this.setFocusToComponent(component,"homePhone");
        }

        var workPhoneField = component.find("workPhone");
			if(workPhoneField){
				var workPhone = workPhoneField.get("v.value");
				var workPhoneValMessage = $A.get("$Label.c.ADD_NEW_TALENT_VALID_WORKPHONE");
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if((workExtension !== '' && typeof workExtension !== 'undefined') && (workPhone === '' || typeof workPhone == 'undefined')  ){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						this.showError(workPhoneValMessage);
						this.setFocusToComponent(component,"workPhone");
						}
					
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			 if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			   this.showError(workPhoneValMessage);
			   this.setFocusToComponent(component,"workPhone");
			}		
		}

		var otherPhoneField = component.find("otherPhone");
        var otherPhone = otherPhoneField.get("v.value");
        if(!this.validateMobilePhone(otherPhone)) {
           otherPhoneField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_OTHERPHONE")}]);
           otherPhoneField.set("v.isError",true);
           validTalent = false;
		   this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_OTHERPHONE"));
		   this.setFocusToComponent(component,"otherPhone");
        }
       
        var emailField = component.find("email");
        var email = emailField.get("v.value");
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL")}]);
            emailField.set("v.isError",true);
            validTalent = false;
			this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL"));
			this.setFocusToComponent(component,"email");
        }

        var otherEmailField = component.find("otherEmail");
        var otherEmail = otherEmailField.get("v.value");
        if(!this.validateEmail(otherEmail)) {
            otherEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_OTHEREMAIL")}]);
            otherEmailField.set("v.isError",true);
            validTalent = false;
			//this.showError("Please enter valid Email.");
            this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_OTHEREMAIL"));
			this.setFocusToComponent(component,"otherEmail");
        }
        var workEmailField = component.find("workEmail");
        var workEmail = workEmailField.get("v.value");
        if(!this.validateEmail(workEmail)) {
            workEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_WORKEMAIL")}]);
            workEmailField.set("v.isError",true);
            validTalent = false;
			//this.showError("Please enter valid Email.");
            this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_WORKEMAIL"));
			this.setFocusToComponent(component,"workEmail");
        } 
        
        
        //var linkedinUrlField = component.find("linkedinUrl");
        //var linkedinUrl = linkedinUrlField.get("v.value");
        
        if (!mobilePhone && !homePhone && !workPhone && !otherPhone
            && !email && !otherEmail && !workEmail) {
            validTalent = false;
            this.showError($A.get("$Label.c.ADD_NEW_TALENT_CONTACTMETHOD"));
			this.setFocusToComponent(component,"email");
        } 
        return validTalent; 
    },
    /* Start for Strory S-79062 */
    validateEMEAUser : function(component,flag) {
        
        var UserCategory = component.get("v.usrCategory");
        
        //console.log('UserCategory::'+UserCategory);
        if(UserCategory === 'CATEGORY3'){
            //console.log('Inside CATEGORY3 ');
            var emailField = component.find("email");
            var email = emailField.get("v.value");
            //console.log('EMail value::'+email);
            
            var otherEmailField = component.find("otherEmail");
            var otherEmail = otherEmailField.get("v.value");
            //console.log('otherEmail value::'+otherEmail);
            
            var workEmailField = component.find("workEmail");
            var workEmail = workEmailField.get("v.value");
            //console.log('workEmail value::'+workEmail);
            
            if((!email || email == " ")&&(!otherEmail || otherEmail == " ")&&(!workEmail || workEmail == " ") ){                  
                //console.log('Inside if condition where all email blank ');
                
                flag = false;
                emailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_EMEA_USER")}]);
                emailField.set("v.isError",true);
                $A.util.addClass(emailField,"slds-has-error show-error-message");
                this.showError($A.get("$Label.c.ADD_NEW_TALENT_EMEA_USER"));
				this.setFocusToComponent(component,"email");
                
            }
            else{
                emailField.set("v.errors", []);
                emailField.set("v.isError", false);
                $A.util.removeClass(emailField,"slds-has-error show-error-message");
            }
        }
        
        //console.log('Before Returning the value::'+ flag);
        
        return flag;
    },
    /* End for Strory S-79062 */
    
    validateZipAndState : function(component, validTalent) {
        var validZip = true;
        var validState = true;

        var zipField = component.find("MailingPostalCode");
        if(zipField){
            var zip = zipField.get("v.value");
            if (!zip || zip === "") {
                validZip = false;
            }     
        }
        //S-65378 - Added by KK: Updated to use Mailing State instead of Talent_State_Text__c
        var state =  component.get("v.contact.MailingState");
     //   if(state){
            if (!state || state === "") {
                validState = false;
            }
       // }
        
        if (!validZip && !validState) {
            validTalent = false;
            this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_POSTAL_STATE"));
			this.setFocusToComponent(component,"MailingPostalCode");
        }
        return validTalent;
    },    


    validateStateCountry : function(component, validTalent,event) {

     var state    =      component.get("v.contact.MailingState");
     var country  =      component.get("v.contact.Talent_Country_Text__c");

     var mailingCountry = component.get("v.contact.MailingCountry");

	 var errorMsg = $A.get("$Label.c.ADD_NEW_TALENT_VALID_COUNTRY_STATE");	

     if ( $A.util.isEmpty(state) || $A.util.isEmpty(country) ) {
            validTalent = false; 

			if($A.util.isEmpty(country))	
				this.setInlineErrorMessage(component,event,"G2Country");	
			else if($A.util.isEmpty(state)){				
				this.setInlineErrorMessage(component,event,"G2State");	
			}
			event.preventDefault();
			this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_COUNTRY_STATE"));
            
      } else if (mailingCountry === '' ) {
            var countryMap = component.get("v.countriesMap");
            var countryCode = component.get("v.MailingCountryKey");
            if (countryMap && countryCode && countryCode.length > 0) {
               try {
                   var countryValue = countryMap[countryCode];
                   if (countryValue && countryValue.length > 0 ) {
                       component.set("v.contact.MailingCountry", countryValue);
                   } else {
                     validTalent = false; 

					 this.setInlineErrorMessage(component,event,"G2Country");	
					 event.preventDefault();
                     this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_COUNTRY_STATE"));
                   }
               } catch(error) {
                   console.error(error);
                   validTalent = false;

				   this.setInlineErrorMessage(component,event,"G2Country");	
				   event.preventDefault();
                   this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_COUNTRY_STATE")); 
               }
            } else {
                     validTalent = false;

					  this.setInlineErrorMessage(component,event,"G2Country");	
					  event.preventDefault();
                      this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_COUNTRY_STATE")); 
                   }
      } 
      return validTalent;        
    },

    validateFirstName : function(component, validTalent) {
        var firstNameField = component.find("firstName");
        var firstName = firstNameField.get("v.value");
        if (!firstName || firstName === "") {
            validTalent = false;
            firstNameField.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_errorFirstName")}]);
            firstNameField.set("v.isError",true);
            $A.util.addClass(firstNameField,"slds-has-error show-error-message");
			this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_errorFirstName"));
			this.setFocusToComponent(component,"firstName");
        } else {
            firstNameField.set("v.errors", []);
            firstNameField.set("v.isError", false);
            $A.util.removeClass(firstNameField,"slds-has-error show-error-message");
        }
        return validTalent;
    },    

    validateLastName : function(component, validTalent) {
        var lastNameField = component.find("lastName");
        var lastName = lastNameField.get("v.value");
        if (!lastName || lastName === "") {
            validTalent = false;
            lastNameField.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_errorLastName")}]);
            lastNameField.set("v.isError",true);
            $A.util.addClass(lastNameField,"slds-has-error show-error-message");
			this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_errorLastName"));
			this.setFocusToComponent(component,"lastName");
        } else {
            lastNameField.set("v.errors", []);
            lastNameField.set("v.isError", false);
            $A.util.removeClass(lastNameField,"slds-has-error show-error-message");
        }
        return validTalent;
    },    
    
    validateExpirationDate : function(component, validTalent) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        
        if(dd<10) {
            dd = '0'+dd
        } 
        
        if(mm<10) {
            mm = '0'+mm
        } 
        
        today = yyyy + '-' + mm + '-' + dd;
        var expirationDateField = component.find("expirationDate");
        var expirationDate = expirationDateField.get("v.value");
        
       if (expirationDate < today && expirationDate != "") {
           validTalent = false;
           expirationDateField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_EXPIRATION_DATE")}]);
           expirationDateField.set("v.isError",true);
           $A.util.addClass(expirationDateField,"slds-has-error show-error-message");
		   this.showError($A.get("$Label.c.ADD_NEW_TALENT_EXPIRATION_DATE"));
       } else {
           expirationDateField.set("v.errors", []);
           expirationDateField.set("v.isError", false);
           $A.util.removeClass(expirationDateField,"slds-has-error show-error-message");
       }
       return validTalent;
    },    
    
    validateLinkedInUrl : function(component, validTalent){

        var linkedinUrlField = component.find("linkedinUrl");
        if(linkedinUrlField){
            var linkedinUrl = linkedinUrlField.get("v.value");    
        }

        linkedinUrlField.set("v.errors", []);
        linkedinUrlField.set("v.isError", false);
        $A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
        
        if(linkedinUrl && linkedinUrl != ""){
            // /(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
            // var linkedInRegex = new RegExp(".*(linkedin.com/).*", "i");
            // var linkedInRegex = new RegExp(/(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/);

            var linkedInRegex = new RegExp(/(http|https):\/\/\/?\S+$/);


            if (!linkedInRegex.test(linkedinUrl)) {
                validTalent = false;
                linkedinUrlField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_LINKEDIN_URL")}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");

                if(!linkedinUrl.includes('http')){
                    this.showError($A.get("$Label.c.ADD_NEW_TALENT_UDATE_LINKEDIN_URL"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
                }else{
                    this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_LINKEDIN_URL_FORMAT"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
                }
				this.setFocusToComponent(component,"linkedinUrl");
				 
            } else if (linkedinUrl.search("recruiter/profile")>0) {
				validTalent = false;
                linkedinUrlField.set("v.errors", [{message: $A.get("$Label.c.ATS_PublicProfile")}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");
				this.showError($A.get("$Label.c.ATS_PublicProfile"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
				this.setFocusToComponent(component,"linkedinUrl");
			
			} else {
                linkedinUrlField.set("v.errors", []);
                linkedinUrlField.set("v.isError", false);
                $A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },

    
    validateWebsiteUrl : function(component, validTalent){

        var websiteUrlField = component.find("Website_URL__c");
        // alert(websiteUrlField);
        if(websiteUrlField){
            var websiteUrl = websiteUrlField.get("v.value");    
        }

        websiteUrlField.set("v.errors", []);
        websiteUrlField.set("v.isError", false);
        $A.util.removeClass(websiteUrlField,"slds-has-error show-error-message");
        
        if(websiteUrl && websiteUrl != ""){
            
            var websiteUrlRegex = new RegExp(/(http|https):\/\/\/?\S+$/);

            if (!websiteUrlRegex.test(websiteUrl)) {
                
                validTalent = false;
                websiteUrlField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_WEBSITE_URL")}]);
                websiteUrlField.set("v.isError",true);
                $A.util.addClass(websiteUrlField,"slds-has-error show-error-message");

                if(!websiteUrl.includes('http')){
                    this.showError($A.get("$Label.c.ADD_NEW_TALENT_UPDATE_URL"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
                }else{
                    this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_WEBSITE_URL_FORMAT"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
                }
				this.setFocusToComponent(component,"Website_URL__c");

            } else {
                websiteUrlField.set("v.errors", []);
                websiteUrlField.set("v.isError", false);
                $A.util.removeClass(websiteUrlField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },
    
    
    //S-59229 Validate Preferred Options - Added by Karthik
    validatePrefOptions : function(component, validTalent) {
        if (component.get("v.homePhonePreferred") === true) {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                var homePhone = homePhoneField.get("v.value");
                if(homePhone === "" || !homePhone){
                    validTalent = false;
                    homePhoneField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_HOMEPHONE")}]);
                    homePhoneField.set("v.isError",true);
                    $A.util.addClass(homePhoneField,"slds-has-error show-error-message");
					this.showError($A.get("$Label.c.ADD_NEW_TALENT_HOMEPHONE"));
					this.setFocusToComponent(component,"homePhone");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    homePhoneField.set("v.errors", []);
                    homePhoneField.set("v.isError", false);
                    $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                homePhoneField.set("v.errors", []);
                homePhoneField.set("v.isError", false);
                $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.mobilePhonePreferred") === true) {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                var mobilePhone = mobilePhoneField.get("v.value");
                if(mobilePhone === "" || !mobilePhone){
                    validTalent = false;
                    mobilePhoneField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_MOBILEPHONE")}]);
                    mobilePhoneField.set("v.isError",true);
                    $A.util.addClass(mobilePhoneField,"slds-has-error show-error-message");
					this.showError( $A.get("$Label.c.ADD_NEW_TALENT_MOBILEPHONE"));
					this.setFocusToComponent(component,"mobilePhone");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    mobilePhoneField.set("v.errors", []);
                    mobilePhoneField.set("v.isError", false);
                    $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                mobilePhoneField.set("v.errors", []);
                mobilePhoneField.set("v.isError", false);
                $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.workPhonePreferred") === true) {
            var workPhoneField = component.find("workPhone");
            if(workPhoneField){
                var workPhone = workPhoneField.get("v.value");
                if(workPhone === "" || !workPhone){
                    validTalent = false;
                    workPhoneField.set("v.errors", [{message:  $A.get("$Label.c.ADD_NEW_TALENT_WORKPHONE")}]);
                    workPhoneField.set("v.isError",true);
                    $A.util.addClass(workPhoneField,"slds-has-error show-error-message");
					this.showError($A.get("$Label.c.ADD_NEW_TALENT_WORKPHONE"));
					this.setFocusToComponent(component,"workPhone");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    workPhoneField.set("v.errors", []);
                    workPhoneField.set("v.isError", false);
                    $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var workPhoneField = component.find("workPhone");
            if(workPhoneField){
                workPhoneField.set("v.errors", []);
                workPhoneField.set("v.isError", false);
                $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.otherPhonePreferred") === true) {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                var otherPhone = otherPhoneField.get("v.value");
                if(otherPhone === "" || !otherPhone){
                    validTalent = false;
                    otherPhoneField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_OTHERPHONE")}]);
                    otherPhoneField.set("v.isError",true);
                    $A.util.addClass(otherPhoneField,"slds-has-error show-error-message");
					this.showError($A.get("$Label.c.ADD_NEW_TALENT_OTHERPHONE"));
					this.setFocusToComponent(component,"otherPhone");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    otherPhoneField.set("v.errors", []);
                    otherPhoneField.set("v.isError", false);
                    $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                otherPhoneField.set("v.errors", []);
                otherPhoneField.set("v.isError", false);
                $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.emailPreferred") === true) {
            var emailField = component.find("email");
            if(emailField){
                var email = emailField.get("v.value");
                if(email === "" || !email){
                    validTalent = false;
                    emailField.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_errorEmail")}]);
                    emailField.set("v.isError",true);
                    $A.util.addClass(emailField,"slds-has-error show-error-message");
					this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_errorEmail"));
					this.setFocusToComponent(component,"email");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    emailField.set("v.errors", []);
                    emailField.set("v.isError", false);
                    $A.util.removeClass(emailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var emailField = component.find("email");
            if(emailField){
                emailField.set("v.errors", []);
                emailField.set("v.isError", false);
                $A.util.removeClass(emailField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.otherEmailPreferred") === true) {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                var otherEmail = otherEmailField.get("v.value");
                if(otherEmail === "" || !otherEmail){
                    validTalent = false;
                    otherEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_OTHER_EMAIL")}]);
                    otherEmailField.set("v.isError",true);
                    $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
					this.showError($A.get("$Label.c.ADD_NEW_TALENT_OTHER_EMAIL"));
					this.setFocusToComponent(component,"otherEmail");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    otherEmailField.set("v.errors", []);
                    otherEmailField.set("v.isError", false);
                    $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                otherEmailField.set("v.errors", []);
                otherEmailField.set("v.isError", false);
                $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
            }
        }

        if (component.get("v.workEmailPreferred") === true) {
            var workEmailField = component.find("workEmail");
            if(workEmailField){
                var workEmail = workEmailField.get("v.value");
                if(workEmail === "" || !workEmail){
                    validTalent = false;
                    workEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_WORK_EMAIL")}]);
                    workEmailField.set("v.isError",true);
                    $A.util.addClass(workEmailField,"slds-has-error show-error-message");
					this.showError($A.get("$Label.c.ADD_NEW_TALENT_WORK_EMAIL"));
					this.setFocusToComponent(component,"workEmail");
                    //this.showError("Not all fields on the Profile Tab passed validation. Please return to the Profile Tab and review.");
                }
                else {
                    workEmailField.set("v.errors", []);
                    workEmailField.set("v.isError", false);
                    $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var workEmailField = component.find("workPhone");
            if(workEmailField){
                workEmailField.set("v.errors", []);
                workEmailField.set("v.isError", false);
                $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
            }
        }
        
        return validTalent;
    },
    //S-59229 Validate Preferred Options - End

   validatePhoneEmailLinkedIn : function(component, validTalent) {
        var mobilePhoneField = component.find("mobilePhone");
        var mobilePhone = mobilePhoneField.get("v.value");
        if(!this.validateMobilePhone(mobilePhone)) {
           mobilePhoneField.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_errorMobilePhone")}]);
           mobilePhoneField.set("v.isError",true);
           validTalent = false;
		   this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_errorMobilePhone"));
		   this.setFocusToComponent(component,"mobilePhone");
        }

        var homePhoneField = component.find("homePhone");
        var homePhone = homePhoneField.get("v.value");
        if(!this.validateMobilePhone(homePhone)) {
           homePhoneField.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_errorHomePhone")}]);
           homePhoneField.set("v.isError",true);
           validTalent = false;
		   this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_errorHomePhone"));
		   this.setFocusToComponent(component,"homePhone");
        }

       var workPhoneField = component.find("workPhone");
			if(workPhoneField){
				var workPhone = workPhoneField.get("v.value");
				var workPhoneValMessage = $A.get("$Label.c.ADD_NEW_TALENT_errorWorkPhone");
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if((workExtension !== '' && typeof workExtension !== 'undefined') && (workPhone === '' || typeof workPhone == 'undefined')){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						this.showError(workPhoneValMessage);
						this.setFocusToComponent(component,"workPhone");
						}
					
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			 if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			   this.showError(workPhoneValMessage);
			   this.setFocusToComponent(component,"workPhone");
			}		
		}

		var otherPhoneField = component.find("otherPhone");
        var otherPhone = otherPhoneField.get("v.value");
        if(!this.validateMobilePhone(otherPhone)) {
           otherPhoneField.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_errorOtherPhone")}]);
           otherPhoneField.set("v.isError",true);
           validTalent = false;
		   this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_errorOtherPhone"));
		   this.setFocusToComponent(component,"otherPhone");
        }
       
        var emailField = component.find("email");
        var email = emailField.get("v.value");
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL")}]);
            emailField.set("v.isError",true);
            validTalent = false;
			this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL"));
			this.setFocusToComponent(component,"email");
        }

        var otherEmailField = component.find("otherEmail");
        var otherEmail = otherEmailField.get("v.value");
        if(!this.validateEmail(otherEmail)) {
            otherEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_OTHEREMAIL")}]);
            otherEmailField.set("v.isError",true);
            validTalent = false;
			this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_OTHEREMAIL"));
			this.setFocusToComponent(component,"otherEmail");
        }
        var workEmailField = component.find("workEmail");
        var workEmail = workEmailField.get("v.value");
        if(!this.validateEmail(workEmail)) {
            workEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_WORKEMAIL")}]);
            workEmailField.set("v.isError",true);
            validTalent = false;
			this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_WORKEMAIL"));
			this.setFocusToComponent(component,"workEmail");
        } 
       

        var linkedinUrlField = component.find("linkedinUrl");
        var linkedinUrl = linkedinUrlField.get("v.value");

        if (!mobilePhone && !homePhone && !workPhone && !otherPhone
            && !email && !otherEmail && !workEmail && !linkedinUrl) {
                validTalent = false;
                this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_LINKEDIN"));
				this.setFocusToComponent(component,"linkedinUrl");
        } 
        return validTalent;
    },       

     validateMobilePhone : function(mobilePhone) {
        if(mobilePhone && mobilePhone.length > 0) {
            var mp = mobilePhone.replace(/\s/g,'');
            if(mp.length >= 10 && mp.length <= 17) {
                
                var regex = new RegExp("^[0-9-.+()/]+$");
                var isValid = mp.match(regex);
                
                if(isValid != null) {
                    return true; 
                }
                else{
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true; 
    },   
	
   validateEmail : function(emailID){
        if(emailID && emailID.trim().length > 0) {
            
            var isValidEmail = emailID.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

            if(isValidEmail != null) {
                return true;
            }
            else {
                return false;
            } 

        }

        return true;
    },

	checkError : function(response, component){    
        //console.log('checkError called ...'+JSON.stringify(response.getError()));
        var errors = response.getError();
        if (errors) {
            if (errors[0].fieldErrors) {
                this.showServerSideValidation(errors, component);   
            } else if (errors[0] && errors[0].message) {
                this.showError(errors[0].message, 'An error occured!');
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,'An error occured!');
            }else{
                this.showError($A.get("$Label.c.ADD_NEW_TALENT_unexpected_error"));
            }
        } else {
            this.showError(errors[0].message);
            //throw new Error("Unknown Error");
        }
    },    

    showServerSideValidation : function(errors, component) {
        for (var fieldName in errors[0].fieldErrors) {
            if (fieldName === "LinkedIn_URL__c") {

                var linkedinUrlField = component.find("linkedinUrl");

                linkedinUrlField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_LINKEDIN_URL")}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");

            } else if (fieldName === "Email") {

                var emailField = component.find("email");

                emailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL")}]);
                emailField.set("v.isError",true);
                $A.util.addClass(emailField,"slds-has-error show-error-message");

            } else if (fieldName === "Other_Email__c") {

                var otherEmailField = component.find("otherEmail");

                otherEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL")}]);
                otherEmailField.set("v.isError",true);
                $A.util.addClass(otherEmailField,"slds-has-error show-error-message");

            }  else if (fieldName === "Work_Email__c") {

                var workEmailField = component.find("workEmail");

                workEmailField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_EMAIL")}]);
                workEmailField.set("v.isError",true);
                $A.util.addClass(workEmailField,"slds-has-error show-error-message");

            } else {
                this.showError (errors[0].fieldErrors[fieldName][0].message, errors[0].fieldErrors[fieldName][0].statusCode);
            }
        }  //end of field errors forLoop                   

     },      

	 setLatLongAddress : function (component) {
		var contact = component.get("v.contact");
		if(!component.get("v.isGoogleLocation")){
			component.set("v.contact.MailingLatitude",null);
			component.set("v.contact.MailingLongitude",null);
			if (contact.LatLongSource__c != null && contact.MailingCity == null && contact.MailingPostalCode == null) {
				component.set("v.contact.LatLongSource__c","");
			}	
		}
         
         var address = contact.MailingStreet;
  		//var address = component.get("v.presetLocation")
		if (component.get("v.streetAddress2")) {
            if(address) {
               address = address  +', '+component.get("v.streetAddress2"); 
            }else {
                address = component.get("v.streetAddress2");
            }
						
		}		
		component.set("v.contact.MailingStreet",(address != undefined ? address : ""));
	},

	clearServerValidationError : function(component) {
        var linkedinUrlField = component.find("linkedinUrl");
        linkedinUrlField.set("v.errors", []);
        linkedinUrlField.set("v.isError", false);
        $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");

        var emailField = component.find("email");
        emailField.set("v.errors", []);
        emailField.set("v.isError", false);
        $A.util.addClass(emailField,"slds-has-error show-error-message");

        var otherEmailField = component.find("otherEmail");
        otherEmailField.set("v.errors", []);
        otherEmailField.set("v.isError", false);
        $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
        
        var workEmailField = component.find("workEmail");
        workEmailField.set("v.errors", []);
        workEmailField.set("v.isError", false);
        $A.util.addClass(workEmailField,"slds-has-error show-error-message");

    }, 

	setDataBeforeSaveTalentCall : function(component, event) {
		this.clearServerValidationError(component);  
		this.setLatLongAddress(component);
		var contact = component.get("v.contact");
		contact = this.updatePreferredValues(contact, component);
		return contact;
	},

	getKeyByValue : function(map, searchValue) {
		for(var key in map) {
			if (map[key] === searchValue) {
				return key;
			}
		}
	},

	showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

	loadSD2Picklists: function(component) {
		var usr = component.get("v.runningUser");

		if (usr.Profile.Name === 'Single Desk 2' || usr.Profile.Name === 'System Administrator') {
			var hlp = component.find("addEditHelper");

            hlp.loadPicklistValues('Account', 'Talent_Gender__c', component.find('Gender')); 
            hlp.loadPicklistValues('Account', 'Talent_Race__c', component.find('Race')); 
            hlp.loadPicklistValues('Account', 'Talent_Race_Information__c', component.find('RaceInfo')); 
            hlp.loadPicklistValues('Account', 'Talent_Gender_Information__c', component.find('GenderInfo')); 
        } 
	},

	setInlineErrorMessage : function(component,event,fldId){
		if(fldId==null)  return;
		var fieldId = document.getElementById(fldId);
		fieldId.setAttribute("aria-invalid", false);

		var appLocationInlineEditing = $A.get('e.c:E_LocationFieldsInlineEditingErrors');	
		appLocationInlineEditing.setParams({"fieldId":fldId});	
		appLocationInlineEditing.fire();

		window.setTimeout(	
			$A.getCallback(function() {		
			document.getElementById(fldId).focus();	
		}), 250);

        event.preventDefault();
	}
})