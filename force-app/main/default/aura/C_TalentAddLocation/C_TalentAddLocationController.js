({
	doInit : function(component, event, helper){
		if (component.get("v.context") === "page") {
			component.set("v.viewState", "DEFAULT");
		} else {
			component.set("v.viewState", "EDIT");
		}
		// component.set("v.inTalentLocation", 'true');
		// if(component.get("v.inTalentLocation") === 'true'){
            component.set("v.placeholder", $A.get("$Label.c.FIND_ADD_TALENT_Location_Placeholder"));
            component.set("v.helpText", `Utilizing the address lookup (in Street Address) and putting in complete information will give the most accurate results for Search/Match results.`);
        // }	
	},

	// toggleEdit : function(component, event, helper) {
	// 	component.set("v.viewState", "EDIT");
	// }

	//validate lookup for empty values -- for G2 state and country
	validateG2StateCountryError : function(component,event,helper) {
		const fieldId = event.getParam("fieldId");	
		var hiddenTitle="";
		var errorField="";

		if(fieldId == "G2Country")
			hiddenTitle="hiddenCountry";
		else
			hiddenTitle="hiddenState";

		var hiddenField = component.find(hiddenTitle);
        var hidden = hiddenField.get("v.value");
        if (!hidden || hidden === "") {
            if(fieldId == "G2Country")
				component.set("v.countryError",true);
			else if(fieldId == "G2State")
				component.set("v.stateError",true);
		}
		
		//helper.showErrorAndFocusField(component,fieldId);
    },

})