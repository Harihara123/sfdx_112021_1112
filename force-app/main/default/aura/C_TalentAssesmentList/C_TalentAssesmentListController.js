({
    
    doInitialization : function(component, event, helper) {
        helper.loadSortedData(component);
    },
    
    confirmDeletion : function(component, event, helper) {
        component.set("v.assessmentId", event.getSource().get("v.name") );
        var theVal = component.get("v.showConfirmationModal")
        component.set("v.showConfirmationModal", !theVal);    
    }, 
    
    deleteAssessment : function(component, event, helper) {
        
        if (event.getParam('confirmationVal') === 'Y') {
            helper.deleteAssessment(component);
        }    
    }, 
    
    editAssessment : function(component, event, helper) {
        // clearing previously set values in attributes
        component.set("v.rowRecordID","");
        component.set("v.mode","");
        var AssessmentID = event.getSource().get("v.name");
        component.set("v.rowRecordID",AssessmentID);
        component.set("v.mode","Edit");
        component.set("v.showAssesmentModal", "true");
        var childCmp = component.find("assesmentModal")
        childCmp.EditMethod();
    }, 
    
    addNewAssesment : function(component, event, helper) {
        component.set("v.showAssesmentModal", "true");
        var childCmp = component.find("assesmentModal")
        childCmp.addMethod();
    },

    applySort : function(component, event, helper) {
    	component.set("v.sortField", event.getParam("sortField"));
		component.set("v.sortAsc", event.getParam("sortAsc"));
		
		helper.loadSortedData(component);
	},
    
    
})