({

	loadSortedData : function(component) {

		var action = component.get('c.getSortedTalentAssessments');

		action.setParams({
            "contactId"	: component.get('v.recordId'),
            "sortField"	: component.get("v.sortField"),
			"sortAsc"	: component.get("v.sortAsc")
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

            	var assesments = response.getReturnValue();
            	//console.log('xxxxxxxxxx' + JSON.stringify(assesments) );
            	
                component.set('v.assesmentData', assesments );
                var loadingText = component.find('loading');
                $A.util.addClass( loadingText, 'show-loading');
                $A.util.removeClass(loadingText, 'hide-loading');
                // this.getReferenceData(component);
                component.set('v.totalAssesments', assesments.length );
		        
		
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }

        }));
        $A.enqueueAction(action);	
    },

	deleteAssessment : function(component ) {

		var action = component.get('c.deleteTalentAssessment');

		action.setParams({
            "assessmentId": component.get("v.assessmentId")
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	
                component.set('v.assesmentData', null );
                var loadingText = component.find('loading');
                $A.util.addClass( loadingText, 'show-loading');
                $A.util.removeClass(loadingText, 'hide-loading');
                this.loadSortedData(component);
		        this.showToast("Record has been Deleted", "Success", "Success");

		
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
                this.showToast("Something went wrong, please try again", "Error", "Error");
            }

        }));


        $A.enqueueAction(action);
	},

    showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },
})