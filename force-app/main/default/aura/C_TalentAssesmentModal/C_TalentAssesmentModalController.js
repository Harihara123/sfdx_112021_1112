({
    doInit : function(component,event,helper){
        
    },
    addNewAssesment : function(component, event, helper) {
        component.set("v.isDisabled","false");
        helper.clearAssesmentForm(component,event,helper);
        helper.startSpinner(component, event, helper);
        helper.addEditFormModal(component, event, helper, "Add");
    },
    
    callAddModal : function(component,event,helper){
        helper.toggleClass(component,'backdropAddDocument','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-'); 
    },
    saveAssesment : function(component, event, helper) {
        component.set("v.isDisabled","true");
        helper.startSpinner(component, event, helper);
        helper.validateAndSaveAssesment(component, event,helper, "Save");
    },
    saveNewAssesment : function(component, event, helper) {
        component.set("v.isDisabled","true");
        helper.startSpinner(component, event, helper);
        helper.validateAndSaveAssesment(component, event,helper, "SaveNew");
    },
    closeModal1: function(component, event, helper) {
        helper.closeModal1(component, event, helper);  
    },
    
    editAssesment : function(component, event, helper) {
        component.set("v.isDisabled","false");
        component.set("v.disableLink", false);
        helper.clearAssesmentForm(component,event,helper);
        helper.startSpinner(component, event, helper);
        helper.addEditFormModal(component, event, helper, "Edit");
    },
    
    toggleAssessmentLink : function(component, event, helper) {
        component.set("v.assesmentwrap.Quiz_Assessment__c",""); // to reset dependent lookup value
        var value = event.getSource().get("v.value");
    },

    dateKeypressValidation: function(component, event, helper){
        helper.preventNonNumericKeys(component, event);
    },

    
    
    
})