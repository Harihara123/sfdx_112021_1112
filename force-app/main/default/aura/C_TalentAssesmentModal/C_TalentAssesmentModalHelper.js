({
	startSpinner: function(component, event, helper){
        component.set("v.Spinner", true);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');   
    },
    stopSpinner: function(component, event, helper){
        var spinner = component.find('spinner');
    	$A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');
        component.set("v.Spinner", false);   
    },
    addEditFormModal: function(component, event, helper, mode){
        var theVal = component.get("v.addModalTrigger")
        component.set("v.addModalTrigger", !theVal);
        component.set("v.mode", mode);
      	var assessmentPicklist = "Vendor__c";
      	
        //  For Add
        if(mode == 'Add'){
        	this.initWrapper(component,event,helper);    
        }
        
        //for getting picklist value
        this.getAllPicklist(component, event, helper, assessmentPicklist, mode);
        
        // For Edit
        if(mode =='Edit'){
     		this.getTalentAssessment(component,event,helper);       
        }
      },
    
    
    initWrapper : function(component, event,helper){
    	var action = component.get('c.initWrapper');
 		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.assesmentwrap', response.getReturnValue());
            }
		});
        $A.enqueueAction(action);
        
        this.stopSpinner(component,event,helper);
    },
    
    getAllPicklist: function(component, event, helper, fieldName,  mode) {
        var serverMethod = 'c.getAllPicklistValues';
        var params = {
            "objectStr": "Assessment__c",
            "fld": fieldName
        };
        var action = component.get(serverMethod);
        action.setParams(params);

        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                // Just declaration
                var picklist = allValues["Contact.Solution"];
                var idName = "salutation";
                
                //this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Assessment.Vendor__c"];
                idName = "vendor";
                this.setPicklistValues(component, picklist, idName);
            }
        });
        $A.enqueueAction(action);
    },
    
    setPicklistValues : function(component, picklist, fildName){
		var opts = [];
        var shouldDefault = false;
        if (shouldDefault == false && picklist != undefined && picklist.length > 0) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
        }
        
        for (var i = 0; i < picklist.length; i++) {
            
                opts.push({
                    class: "optionClass",
                    label: picklist[i],
                    value: picklist[i]
                });
            }
        
        component.find(fildName).set("v.options", opts);        
    },

    getTalentAssessment :function(component, event, helper){
      var objID = component.get("v.AssessmentRecordID");
               
        var serverMethod = 'c.getTalentAssessment';	
        var params = {
            "assessmentId": objID
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        
       action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var ResponseObj = response.getReturnValue();
               component.set('v.assesmentwrap', ResponseObj);
/*               if( component.get('v.assesmentwrap.Id').includes(component.get('v.assesmentwrap.Name') ) ){
                    component.set('v.assesmentwrap.Name', '' );
                }
*/            } else if (state === "ERROR") {
                
            } 
            this.stopSpinner(component, event, helper);
        
        });
        $A.enqueueAction(action);  
    },
    
    validateAndSaveAssesment : function(component, event, helper, action) {

        var mode = component.get("v.mode"); 
        if (this.validRecord(component)){
            
            var QuizField = component.find("quizAssessment");
        	var QuizFieldName = component.get( "v.assesmentwrap.Quiz_Assessment__c");
            
          	var serverMethod = 'c.validateQuizAssessmentValue';	
            var params = {
                "quiz" : component.get( "v.assesmentwrap.Quiz_Assessment__c"),
                "vendor" : component.get( "v.assesmentwrap.Vendor__c"),
            };
    
            var action1 = component.get(serverMethod);
            action1.setParams(params);
            
            action1.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") { 
                    if(!response.getReturnValue()){
                        component.set("v.isDisabled","false");
                        component.set("v.quizError", "Please enter a valid Quiz Assessment");
                        QuizField.set("v.isError",true);
                        $A.util.addClass(QuizField,"slds-has-error show-error-message");
                        return false;
                    }else{
                        component.set("v.quizError", "");
                        // QuizField.set("v.errors", []);
                        QuizField.set("v.isError", false);
                        $A.util.removeClass(QuizField,"slds-has-error show-error-message");
                        this.saveAssesment(component, event, helper, action);
                    }
                } 
            });
            $A.enqueueAction(action1);  
        } else {
            this.stopSpinner(component, event, helper);  
        }
       
    },
    
    saveAssesment : function(component, event, helper, buttonClicked){
		
    	var assesmentwrap = component.get("v.assesmentwrap");
        var recordId = component.get("v.recordId");
        var shouldInsert = true;
		if(component.get("v.mode") != "Add")
            shouldInsert = false;
        var serverMethod = 'c.saveTalentAssesment';	
        var params = {
            
            "assessmentObj" : assesmentwrap,
            "talentContactId" : recordId,
            "shouldInsert" : shouldInsert,
            "mode" : component.get("v.mode")
            
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        
       action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var parentComponent = component.get("v.parent");
                parentComponent.doInitialization();
                
               	this.stopSpinner(component, event, helper);
                this.showToast("Record has been saved", "Success", "Success");
                this.closeModal1(component, event, helper);
                if(buttonClicked =='SaveNew'){
                    parentComponent.addNewAssesment();
                }
                
            } else if (state === "ERROR") {
                this.stopSpinner(component, event, helper);
                this.showToast(response.getReturnValue(), "Error", "Error");
            } 
            this.stopSpinner(component, event, helper);
        });
       
        $A.enqueueAction(action);  
    },
    
    validRecord: function(component) {
        var validRecord = true;
        validRecord = this.validateVendor(component,validRecord);
        validRecord = this.validateQuizAssessment(component,validRecord);
        // validRecord = this.validateName(component,validRecord);
        validRecord = this.validateDateFields (component,validRecord);
        validRecord = this.validateLink(component,validRecord);
        return validRecord;
    },
    
    validateVendor :function(component,validRecord){
        var vendorField = component.find("vendor");
        var vendorName = vendorField.get("v.value");

        if (!vendorName || vendorName === "") {
            component.set("v.isDisabled","false");
            validRecord = false;
            vendorField.set("v.errors", [{message: "Please enter a valid vendor"}]);
            vendorField.set("v.isError",true);
            $A.util.addClass(vendorField,"slds-has-error show-error-message");
        } else {
            vendorField.set("v.errors", []);
            vendorField.set("v.isError", false);
            $A.util.removeClass(vendorField,"slds-has-error show-error-message");
        }
        return validRecord;
    },

    validateQuizAssessment :function(component,validRecord){
        var QuizField = component.find("quizAssessment");
        var QuizFieldName = component.get( "v.assesmentwrap.Quiz_Assessment__c");

        if (!QuizFieldName || QuizFieldName === "") {
            component.set("v.isDisabled","false");
            validRecord = false;
            component.set("v.quizError", "Please enter a valid Quiz Assessment");
            // QuizField.set("v.errors", [{message: "Please enter a valid Quiz Assessment"}]);
            QuizField.set("v.isError",true);
            $A.util.addClass(QuizField,"slds-has-error show-error-message");
        } else {
            component.set("v.quizError", "");
            // QuizField.set("v.errors", []);
            QuizField.set("v.isError", false);
            $A.util.removeClass(QuizField,"slds-has-error show-error-message");
        }
        return validRecord;
    },


    validateDateFields : function(component, validRecord) {
        var assessmentDateField = component.find("Assessment_Date");
        var assessmentDateExists = typeof assessmentDateField.get('v.value') !== "undefined";
        var assessmentDate;
        
        if(assessmentDateExists){
            assessmentDate = this.formatDate(assessmentDateField.get("v.value"));
            if (assessmentDate === false) {
                assessmentDateField.set("v.errors", [{message : "Valid date format is " + this.getAcceptedDateFormat()}]);
                component.set("v.isDisabled","false");
                validRecord = false;
                return validRecord;
            } else {
                assessmentDateField.set("v.errors", null);
            }
        }

        if (!assessmentDate || assessmentDate === "" || assessmentDate == 'NULL') {
        	component.set("v.isDisabled","false");
            validRecord = false;
            assessmentDateField.set("v.errors", [{message: "Please enter a valid Assessment Date."}]);
            assessmentDateField.set("v.isError",true);
            $A.util.addClass(assessmentDateField,"slds-has-error show-error-message");
            return validRecord;
        } 
        
        
        if(new Date(assessmentDate) > new Date() && assessmentDate != "") {
            component.set("v.isDisabled","false");
            validRecord = false;
            assessmentDateField.set("v.errors", [{message: "Assessment Date can not be set to Future Date."}]);
            assessmentDateField.set("v.isError",true);
            $A.util.addClass(assessmentDateField,"slds-has-error show-error-message");
        } else {
            assessmentDateField.set("v.errors", []);
            assessmentDateField.set("v.isError", false);
            $A.util.removeClass(assessmentDateField,"slds-has-error show-error-message");
        }
        return validRecord;
    },

    getAcceptedDateFormat : function () {
        var language = window.navigator.userLanguage || window.navigator.language;
        return language === "en-US" ? "mm/dd/yyyy" : "dd/mm/yyyy";
    },
    
    formatDate:function(inputDate){
        var input = inputDate;
        if(input){
             // Typed in dates only work for "/" as delimiter.
            var arr = input.split("/");
            if(arr.length === 3){
                // Pad "0" for single digit date / month
                if(arr[0].length === 1){
                   arr[0] = '0' + arr[0];
                }
                if(arr[1].length === 1){
                   arr[1] = '0' + arr[1];
                }

                // Convert to "yyyy-mm-dd" based on user locale.
                var language = window.navigator.userLanguage || window.navigator.language;
                if(language === 'en-US'){
                    input = arr[2] + '-' + arr[0] + '-' + arr[1];
                }else{
                    input = arr[2] + '-' + arr[1] + '-' + arr[0];
                }
            } 

            // Adjust for timezone since the new Date() constructor sets to UTC midnight.
            var now = this.adjustDateForTimezone(input);
            if (now === false || isNaN(now.getTime())) {
                return false;
            } else {
                return now.getFullYear()+"-"+(now.getMonth() + 1) +"-"+now.getDate();
            } 
        }else{
            return '';
        }
       
    },

    adjustDateForTimezone : function (inputDate) {
        var theDate = new Date(inputDate);
        if (!isNaN(theDate.getTime())) {
            // Use same date to get the timezone offset so daylight savings is factored in.
            var output = new Date(theDate.getTime() + (theDate.getTimezoneOffset() * 60 * 1000));
            return output;
        } else {
            return false;
       }
    },

    validateLink :function(component,validRecord){
        var AssmtLinkField = component.find("AssesmentLink");
        var AssmtLink = AssmtLinkField.get("v.value");
        if (AssmtLink) {
            if(!AssmtLink.startsWith('https://') && !AssmtLink.startsWith('http://')){
            	component.set("v.isDisabled","false");
            	validRecord = false;
            	AssmtLinkField.set("v.errors", [{message: "Please Update Assessment Link to include https:// or http://"}]);
            	AssmtLinkField.set("v.isError",true);
            	$A.util.addClass(AssmtLinkField,"slds-has-error show-error-message");    
            }else{
                AssmtLinkField.set("v.errors", []);
            	AssmtLinkField.set("v.isError", false);
            	$A.util.removeClass(AssmtLinkField,"slds-has-error show-error-message");
            }
            
        } else {
            AssmtLinkField.set("v.errors", []);
            AssmtLinkField.set("v.isError", false);
            $A.util.removeClass(AssmtLinkField,"slds-has-error show-error-message");
        }
        return validRecord;
    },

    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },

    closeModal1: function(component, event, helper) {
        this.clearAssesmentForm(component, event, helper);
        this.toggleClassInverse(component,'backdropAddDocument','slds-backdrop--');
		this.toggleClassInverse(component,'modaldialogAddDocument','slds-fade-in-');
        
    },

    clearAssesmentForm :function(component,event,helper){
        component.set('v.assesmentwrap',{'sobjectType':'Assessment__c','Assessment_Name__c': '','Assessment_Link__c':'','Quiz_Assessment__c':'','Achieved_Score__c':''});
        
        //Assessment Name Field
        var AssmtNameField = component.find("AssesmentName");
        AssmtNameField.set("v.errors", []);
        AssmtNameField.set("v.isError", false);
        $A.util.removeClass(AssmtNameField,"slds-has-error show-error-message");
        
        //QuizAssessment Field
        var QuizField = component.find("quizAssessment");
        component.set("v.quizError", "");
        QuizField.set("v.isError", false);
        $A.util.removeClass(QuizField,"slds-has-error show-error-message");
        
        //Vendor field
        var vendorField = component.find("vendor");
        vendorField.set("v.errors", []);
        vendorField.set("v.isError", false);
        $A.util.removeClass(vendorField,"slds-has-error show-error-message");
        
        //Date Field
        var assessmentDateField = component.find("Assessment_Date");
        assessmentDateField.set("v.errors", []);
        assessmentDateField.set("v.isError", false);
        $A.util.removeClass(assessmentDateField,"slds-has-error show-error-message");
        
        //Link Field
        var AssmtLinkField = component.find("AssesmentLink");
        AssmtLinkField.set("v.errors", []);
        AssmtLinkField.set("v.isError", false);
        $A.util.removeClass(AssmtLinkField,"slds-has-error show-error-message");
     	
    },
    showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },

    preventNonNumericKeys: function(component, event){
        // var charCode = (window.event.which) ? window.event.which : window.event.keyCode;
        var charCode = event.getParams().keyCode;
        if (charCode < 47 || charCode > 57){
            event.preventDefault();
        }
    },
})