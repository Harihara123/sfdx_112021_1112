({
    refreshCertificationListAppEventHandler : function(component, event, helper) {
        helper.refreshList(component);

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Certification record saved successfully.",
            "type": "success"
        });
        toastEvent.fire();
    },

    deleteRefresh : function(component, event, helper) {
        helper.refreshList(component);
    },
     addCertification:function(cmp,event,helper){
      
        var certEvent = $A.get("e.c:E_TalentCertificationAdd");
        var recordId = cmp.get("v.recordId");
        certEvent.setParams({"recordId":recordId});
        certEvent.fire();
    },
    
})