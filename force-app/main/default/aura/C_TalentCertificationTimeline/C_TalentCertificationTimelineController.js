({
   
    doInit: function(cmp, event, helper){
        /*
        var conrecord = cmp.get("v.contactRecord");
        cmp.set("v.talentId",conrecord.fields.AccountId.value);
        var recordID = cmp.get("v.talentId");
        console.log('init:record id:accountid:'+recordID);
        */
        var contact = cmp.get("v.contactRecord");
        var talentId = contact.AccountId;
        cmp.set("v.talentId", talentId);
        // helper.getResults(cmp);
        // helper.getResultCount(cmp);
        helper.getInitialViewListLoad(cmp, helper);
    },


   viewEducationAndCerts :function(cmp, event, helper) {
        var recordID = cmp.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__CandidateEducationCertification?id=" + recordID
        });
        urlEvent.fire();
	},

    showEducationAndCerts : function(component, event, helper) {
        //var contact = component.get("v.contactRecord");
        var talentId = component.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//"url": "/one/one.app#/n/Talent_Education_And_Cert_Details?recordId=" + component.get('v.recordId'),
        	//Neel summer 18 URL change-> "url": "/one/one.app#/n/Talent_Education_And_Cert_Details?recordId=" + talentId,
        	"url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Talent_Education_And_Cert_Details?recordId=" + talentId,            
            "isredirect":true
        });
        urlEvent.fire(); 
	}, 

    addCertification:function(cmp,event,helper){
        var certEvent = $A.get("e.c:E_TalentCertificationAdd");
        // var recordId = cmp.get("v.recordId");
       //var contact = component.get("v.contactRecord");
        var talentId = cmp.get("v.talentId");
        certEvent.setParams({"recordId":talentId});
        certEvent.fire();
    },
    
    refreshCertificationListAppEventHandler : function(component, event, helper) {

        //helper.refreshList(component);
        helper.getInitialViewListLoad(component, helper);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Certification record saved successfully.",
            "type": "success"
        });
        toastEvent.fire();
    },

    deleteRefresh : function(component, event, helper) {
        // helper.refreshList(component);
        helper.getInitialViewListLoad(component, helper);
    },
    
    displayCertDetails : function(cmp, event, helper) {
       // var recordID = cmp.get("v.itemDetail.ParentRecordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        //var contact = component.get("v.contactRecord");
        var talentId = cmp.get("v.talentId");
        urlEvent.setParams({
        	//Neel summer 18 URL change-> "url": "/one/one.app#/n/Talent_Education_And_Cert_Details?recordId=" + talentId,
             "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Talent_Education_And_Cert_Details?recordId=" + talentId,
             "isredirect":true        
        });
        urlEvent.fire();
    },
        updateLoading: function(cmp, event, helper) {
        if(!cmp.get("v.loadingData")) {
            cmp.set("v.loading",false);
        }
    },
    reloadData : function(cmp,event,helper) {
           var rd = cmp.get("v.reloadData");
           if (cmp.get("v.reloadData") === true){
               cmp.set("v.loading",true);
                cmp.set("v.records",[]);
                
                helper.getInitialViewListLoad(cmp, helper);
                // helper.getResultCount(cmp);
                // helper.getResults(cmp);
    
                cmp.set("v.reloadData", false);
             }
         }
})