({
	refreshCertificationList : function(component) {
        this.getResults(component);
	},
	// refactored from C_BaseCardRelatedListHelper
     getInitialViewListLoad : function(cmp,helper){

        var recordID = cmp.get("v.recordId");
        var talentID;
        var viewListFunction = cmp.get("v.viewListFunction"); 
        
        cmp.set("v.loadingData",false);
        talentID = cmp.get("v.talentId");
        this.getViewListItems(cmp, viewListFunction, talentID, this);

	},

    getViewListItems : function(cmp, viewListFunction, recordID,helper) {

        cmp.set("v.loadingData",true);
        // var actionParams = {'recordId':recordID};
           var actionParams = {
                            'recordId':recordID,
                            'maxRows': cmp.get("v.maxRows"),
							//S-82159:Added personaIndicator parameter to params by Siva 12/6/2018
							"personaIndicator":"Recruiter"
                        };
        var className = '';
        var subClassName = '';
        var methodName = '';
        var bdata = cmp.find("basedatahelper");
        var self = this;

        var arr = viewListFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        bdata.callServer(cmp,className,subClassName,methodName,function(response){
            cmp.set("v.loadingData",false);
            var returnVal = response;

          //  var newArr = (new Function("return [" + returnVal + "];")());
          //  cmp.set("v.viewList", newArr);
            cmp.set("v.itemDetail", returnVal);
            cmp.set("v.loadingData",false);
            self.getResults(cmp, self);
            // this.getResultCount(cmp, this);


        },actionParams,false);
    }
    ,getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");
        var self = this;

        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.talentId");
            var apexFunc = listView[indexNo].apexFunction;
               if(apexFunc){
                 var params = listView[indexNo].params;
                 self.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
               }
            }
        }
    ,
	 refreshList : function(component) {
        component.set("v.reloadData", true);
    },
  
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    getViewFromApexFunction : function(cmp, apexFunction, recordID, params,helper){
        var className = '';
        var subClassName = '';
        var methodName = '';
        var self = this;

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordID, "maxRows": cmp.get("v.maxRows"),
							//S-82159:Added personaIndicator parameter to params by Siva 12/6/2018
							"personaIndicator":"Recruiter"};

            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }
            // this.callServer(cmp,className,subClassName,methodName,function(response){
            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
               
                cmp.set("v.records", response);
                cmp.set("v.loadingData",false);
                var recordCount = 0;
                if(response){
                    if(response.length > 0){
                        if(response[0].totalItems){
                            recordCount = response[0].totalItems;
                        }

                        if (response[0].presentRecordCount !== "undefined") {
                            cmp.set ("v.presentRecordCount", response[0].presentRecordCount);
                            cmp.set ("v.pastRecordCount", response[0].pastRecordCount);
                            cmp.set ("v.hasNextSteps", response[0].hasNextSteps);
                            cmp.set ("v.hasPastActivity", response[0].hasPastActivity);
                        }
                    }
                }

                cmp.set("v.recordCount", recordCount); // returnVal.length);
                cmp.set("v.loadingCount",false);
            },actionParams,false);
        }

    }
})