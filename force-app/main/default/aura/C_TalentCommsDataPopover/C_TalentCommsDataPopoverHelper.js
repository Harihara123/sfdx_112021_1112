({
	toggleCorePopover: function(component, event) {
		if (event.type === 'mouseenter' && component.get('v.corePopoverState') === 'slds-hide') {
			component.set('v.corePopoverState', 'slds-show');
		} else {
			setTimeout(()=>component.set('v.corePopoverState', 'slds-hide'), 200);	
		}
	},
	/*Start STORY S-109326 Siva - Display Comms data in F/A Talent on a pop up
	addCommsData: function (component, event) {
		var rowContact = component.get("v.contact");
		if (rowContact !== undefined && rowContact !== null && rowContact !== "") {
			rowContact.displayCommPhone = this.getDisplayCommPhone(rowContact);
			rowContact.displayCommEmail = this.getDisplayCommEmail(rowContact);
			component.set("v.dataRowPhone", rowContact.displayCommPhone);
			component.set("v.dataRowEmail", rowContact.displayCommEmail);
		}
	},
	getDisplayCommPhone : function(contact) {
		var commPhone = (contact.TC_Phone__c !== null && contact.TC_Phone__c !== "" && contact.TC_Phone__c !== undefined) ? contact.TC_Phone__c.replace(/[^0-9]/g, '') : "";
		if(commPhone !== null && commPhone !== "" && commPhone !== undefined) {
			var mobilePhone = (contact.MobilePhone !== null && contact.MobilePhone !== "" && contact.MobilePhone !== undefined)? contact.MobilePhone.replace(/[^0-9]/g, '') : "";
			var homePhone = (contact.HomePhone !== null && contact.HomePhone !== "" && contact.HomePhone !== undefined) ? contact.HomePhone.replace(/[^0-9]/g, '') : "";
			var phone = (contact.Phone !== null && contact.Phone !== "" && contact.Phone !== undefined) ? contact.Phone.replace(/[^0-9]/g, '') : "";
			var otherPhone = (contact.OtherPhone !== null && contact.OtherPhone !== "" && contact.OtherPhone !== undefined) ? contact.OtherPhone.replace(/[^0-9]/g, '') : "";
			return commPhone==mobilePhone?"":(commPhone==homePhone?"":(commPhone==phone?"":(commPhone==otherPhone?"":commPhone)));
		}
		return null;
	},
	getDisplayCommEmail : function(contact) {
		if(contact.TC_Email__c !== null && contact.TC_Email__c !== "") {
			return contact.TC_Email__c==contact.Email?"":(contact.TC_Email__c==contact.Other_Email__c?"":(contact.TC_Email__c==contact.Work_Email__c?"":contact.TC_Email__c));
		}
		return null;
	}
	//End STORY S-109326 Siva - Display Comms data in F/A Talent on a pop up*/
})