({
    doInit : function(component, event, helper) {
        helper.fetchCurrentUser(component);
        helper.fillReferenceInfo( component, event, helper);
        helper.getCountryMappings(component);
        component.set('v.styles', true);
    },
    toggleLayout: function(component, event, helper) {
        let newLayout = component.get("v.toggleLayout");
        component.set("v.toggleLayout", newLayout);
    },
     killComponent : function(component, event, helper) {
        if (component.isValid()) {
            //component.destroy();
        }
    },
	//S-65378 - Added by KK - Update Mailing fields on location selection
    handleLocationSelection : function(component,event, helper) {
		component.set("v.isGoogleLocation", true);
        var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode,countrymap,displayText;
		mailingstreet = '';
		streetnum = '';
		streetname = '';
		city = '';
		state = '';
		country = '';
		countrycode = '';
		postcode = '';
		displayText = '';
		var addr = event.getParam("addrComponents");
		var latlong = event.getParam("latlong");    //S-96255 - Update MailLat, MailLong fields on Contact - Karthik    
		countrymap = component.get("v.countriesMap"); 
		displayText = event.getParam("displayText");
        
        for (var i=0; i<addr.length; i++) {
            if (addr[i].types.indexOf("street_number") >= 0) {
                streetnum = addr[i].long_name;
            }
            if (addr[i].types.indexOf("route") >= 0) {
                streetname = addr[i].long_name;
            }        
            if (addr[i].types.indexOf("postal_code") >= 0) {
                postcode = addr[i].long_name;
            }
            if (addr[i].types.indexOf("locality") >= 0) {
                city = addr[i].long_name;
            }
            if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
                state = addr[i].long_name;
            }
            if (addr[i].types.indexOf("country") >= 0) {
                country = addr[i].long_name;
                countrycode = countrymap[addr[i].short_name];
            }        
        }
        
        if(streetnum) {
            mailingstreet = streetnum;
        } 
        if(streetnum && streetname) {
            mailingstreet = mailingstreet + ' ';
        }
        if(streetname) {
            if(mailingstreet != ''){
                mailingstreet = mailingstreet + streetname;        
            } else {
                mailingstreet = streetname;
            }
        }
        
        component.set("v.contact.MailingStreet",mailingstreet);
        component.set("v.contact.MailingCity",city);
        component.set("v.contact.MailingState",state);
        component.set("v.contact.MailingCountry",countrycode);
        component.set("v.contact.Talent_Country_Text__c",country);
        component.set("v.contact.MailingPostalCode",postcode);
		//var str = displayText.substr(0,displayText.indexOf(',')); // added to display only street address - Manish
        component.set("v.presetLocation",mailingstreet);
		component.set("v.streetAddress2","");
		//S-96255 - Update MailLat, MailLong fields on Contact
		if(latlong && (mailingstreet || city || postcode)){
			//component.set("v.contact.GoogleMailingLatLong__Latitude__s",latlong.lat);
			//component.set("v.contact.GoogleMailingLatLong__Longitude__s",latlong.lng);
			component.set("v.contact.MailingLatitude",latlong.lat);
			component.set("v.contact.MailingLongitude",latlong.lng);				
		} else {
			component.set("v.contact.MailingLatitude",null);
			component.set("v.contact.MailingLongitude",null);				
		}
		component.set("v.contact.LatLongSource__c","Google");
		component.set("v.viewState","EDIT");
		component.set("v.parentInitialLoad",true);
		var countryCode = helper.getKeyByValue(component.get("v.countriesMap"),countrycode);
		component.set("v.MailingCountryKey",countryCode);
    },

    hideModal : function (component, event, helper) {
        component.set('v.styles', false);
        $A.get("e.force:closeQuickAction").fire();
        helper.toggleClassInverse(component,'backdropSendPostingInviteDetail','slds-backdrop--');
        helper.toggleClassInverse(component,'convert_to_client','slds-fade-in-');
    },
    
    //Redudent : Remove
     searchOnEnter : function(component, event, helper) {
        var whichOne = event.getSource().getLocalId(); 
        
        if (event.getParams().keyCode === 13) {
            //helper.startSpinner(component, event, helper);
            //helper.validateSearchForm(component, event, helper);         
            
        }
    },
    
    confirmConversion : function(component, event, helper) {
        helper.startSpinner(component, event, helper);
        helper.validateAndConvert(component, event, helper);
        
    },
    
    updateMailingCountryCode: function(cmp, event, helper) {
	    helper.updateMailingCountryCode(cmp,event);
    },
    
    handleSelectedContact: function (component, event, helper) {

        if( $A.util.isUndefined( component.get("v.selectedRecord") ) || $A.util.isEmpty(component.get("v.selectedRecord" ) ) ){
            helper.showToast(component, "Please select one record...!", "Error", "error");
            return;
        }
        helper.use_selected_contact(component);
        
    },

    handleCurrentContact: function (component, event , helper) {
        helper.convertToTalent(component); 
        helper.closeDedupeOverlay( component );
        
    },


    handleCloseModal: function (component, event, helper) {
        component.set("v.show_modal", true );
        helper.closeDedupeOverlay(component);
    },
    
})