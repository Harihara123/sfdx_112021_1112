({
    fillReferenceInfo : function(component, event, helper, componentId, className) {
        this.startSpinner(component, event, helper);
        var action = component.get('c.getReferenceData');
        action.setParams({
            "contactId": component.get("v.recordId")
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state 	  		= response.getState();
            var reference_data 	= response.getReturnValue();
            
            if (state === "SUCCESS") {
                
                if( Object.keys(reference_data).length ){
                    component.set("v.reference_fields", response.getReturnValue() );
                    component.set("v.show_modal", true );
                    this.stopSpinner(component, event, helper);
                }else{
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    this.showMeToast(component, event, helper, "Successfully Converted To Client", "success", "Success");
                    this.stopSpinner(component, event, helper);
                }
            }
            
        }));
        
        
        $A.enqueueAction(action);
        
    },
     fetchCurrentUser : function(component) {
        var action = component.get('c.getCurrentUserWithOwnership');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set ("v.runningUser", response.getReturnValue().usr);
                component.set ("v.userOwnership", response.getReturnValue().userOwnership);  
                if(component.get("v.userOwnership") == "ONS" 
                    || component.get("v.userOwnership") == "TEK"
                    || component.get("v.userOwnership") == $A.get("$Label.c.Aerotek")
                    || component.get("v.userOwnership") == $A.get("$Label.c.Aston_Carter")
                    || component.get("v.userOwnership") == $A.get("$Label.c.Newco"))
                	component.set("v.showLocation",  true);
            }
            
        });
        $A.enqueueAction(action);
    },
    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    
    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },
    
    showMeToast : function(component, event, helper, message, title, toastType){
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
        
    },
    validateAndConvert : function(component, event, helper) {

        helper.startSpinner(component, event, helper);
        var isValid = true;
        isValid = this.validateTitle(component, isValid);
        isValid = this.validateFields(component, isValid, "firstName", "First Name") ;
        isValid = this.validateFields(component, isValid, "lastName", "Last Name") ;
        if(component.get("v.showLocation")){
        	isValid = this.validateStateCountry(component, isValid);
        }    
        isValid = this.validatePhoneOrEmail(component, isValid) ;
       
        if(isValid){
            this.checkForDuplicates(component);
            // this.convertToTalent(component, event);
        }
        else{
            this.stopSpinner(component, event, helper);
        }
    },
    
    
    
    getCountryMappings : function(component) {   
        var action = component.get("c.getCountryMappings");
        
        action.setCallback(this,function(response) {
            component.set("v.countriesMap",response.getReturnValue());
			if(component.get("v.contact.Talent_Country_Text__c").length == 0 && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
			component.set("v.contact.Talent_Country_Text__c",'United States');
			component.set("v.MailingCountryKey",'US');	
		}   
        });
        $A.enqueueAction(action);
    },

    updateMailingCountryCode: function(cmp, event) {
		var conKey = cmp.get('v.MailingCountryKey');
		var countrymap = cmp.get("v.countriesMap");
		var countrycode ;
		if (conKey !== null && typeof conKey !== 'undefined') {
				countrycode = countrymap[conKey];
				if(countrycode !== null && typeof countrycode !== 'undefined'){
					cmp.set("v.contact.MailingCountry",countrycode);
				}
			}
	},
    convertToTalent : function(component, event, helper) {
        
        var action = component.get('c.convertReferenceToTalent');
        component.get("v.reference_fields").MailingCountry = component.get("v.contact").MailingCountry;
        component.get("v.reference_fields").MailingStreet = component.get("v.contact").MailingStreet;
        component.get("v.reference_fields").MailingCity = component.get("v.contact").MailingCity;
        component.get("v.reference_fields").MailingState = component.get("v.contact").MailingState;
        component.get("v.reference_fields").MailingPostalCode = component.get("v.contact").MailingPostalCode;
        component.get("v.reference_fields").Talent_Country_Text__c = component.get("v.contact").Talent_Country_Text__c;
        component.get("v.reference_fields").talent_Ownership__c = component.get("v.userOwnership");
        action.setParams({
            "contact_obj": component.get("v.reference_fields")
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state 	  		= response.getState();
            var reference_data 	= response.getReturnValue();
           
            if (state === "SUCCESS") {
                
                if( Object.keys(reference_data).length ){
                    component.set("v.reference_fields", response.getReturnValue() );
                    component.set("v.show_modal", true );
                }else{
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    this.showMeToast(component, event, helper, "Successfully Converted To Talent", "success", "Success", true);
                }
                this.stopSpinner(component, event, helper);
            }
            
        }));
        
        $A.enqueueAction(action);
    },
    validateTitle : function(component, isValid) {
        var title = component.get("v.reference_fields.Title");
        if (!title || title === "") {
            isValid = false;
            component.set("v.titleError", true);
            this.showToast(component, "Title is Required.", "error", "error", true);
        } else {
            component.set("v.titleError", false);
        }
        return isValid;
    },
    
    validateStateCountry : function(component, validTalent) {
        
        var state    =      component.get("v.contact.MailingState");
        var country  =      component.get("v.contact.Talent_Country_Text__c");
        
        var mailingCountry = component.get("v.contact.MailingCountry");
        
        if ( $A.util.isEmpty(state) || $A.util.isEmpty(country) ) {
            validTalent = false; 
            component.set("v.invalid_address", true );
            this.showToast(component, "Please enter a valid Country and State.", "error", "error", true);
        } else if (mailingCountry === '' ) {
            var countryMap = component.get("v.countriesMap");
            var countryCode = component.get("v.MailingCountryKey");
            
            if (countryMap && countryCode && countryCode.length > 0) {
                try {
                    var countryValue = countryMap[countryCode];
                    if (countryValue && countryValue.length > 0 ) {
                        component.set("v.contact.MailingCountry", countryValue);
                        component.set("v.invalid_address", false );
                    } else {
                        validTalent = false; 
                        component.set("v.invalid_address", true );
                        this.showToast(component, "Please enter a valid Country and State.", "error", "error", true);
                    }
                } catch(error) {
                   
                    validTalent = false;
                    component.set("v.invalid_address", true );
                    this.showToast(component, "Please enter a valid Country and State.", "error", "error", true);
                }
            } else {
                validTalent = false;
                component.set("v.invalid_address", true );
                this.showToast(component, "Please enter a valid Country and State.", "error", "error", true);
            }
        } 
        return validTalent;        
    },
    
    validateStreetAddress : function(component, validTalent) {
		component.set("v.invalidLocation", false);
		if (component.get("v.streetAddress2") && !component.get("v.presetLocation")) {
			validTalent = false;
            // If location is not present or invalid, trigger error message / styles
            component.set("v.invalidLocation", true);
            this.showToast(component, "Please enter a valid Street Address.", "error", "error", true);
		}
		return validTalent;
	},
    
    validateFields : function(component, isValid, componentId, fieldName) {
        var currentFieldName = component.find(componentId);
        if(currentFieldName !== undefined){
            var validateField = currentFieldName.get("v.value");
            if (!validateField || validateField === "") {
                isValid = false;
                currentFieldName.set("v.errors", [{message: fieldName + " is Required."}]);
                currentFieldName.set("v.isError",true);
                this.showToast(component, `${fieldName} is Required`, "error", "error", true);
                $A.util.addClass(currentFieldName,"slds-has-error show-error-message");
            } else {
                currentFieldName.set("v.errors", []);
                currentFieldName.set("v.isError", false);
                $A.util.removeClass(currentFieldName,"slds-has-error show-error-message");
            }
        }    
        return isValid;
    },
    
    
    validatePhoneOrEmail : function(component, isValid) {
        
        var phoneFieldName      = component.find("phone");
        var mobileFieldName     = component.find("mobile");
        var homephoneFieldName  = component.find("homephone");
        var emailFieldName      = component.find("email");
        
        
        var validatePhone = phoneFieldName.get("v.value");
        var validateEmail = emailFieldName.get("v.value");
        var validateHomephone 	= homephoneFieldName.get("v.value");
        var validateMobile 		= mobileFieldName.get("v.value");
        
        if ( ( !validatePhone || validatePhone === "") && ( !validateEmail || validateEmail === "") &&
             ( !validateHomephone || validateHomephone === "") && ( !validateMobile || validateMobile === "" ) ) {
            
            isValid = false;
            phoneFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            phoneFieldName.set("v.isError",true);
            $A.util.addClass(phoneFieldName,"slds-has-error show-error-message");
            
            mobileFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            mobileFieldName.set("v.isError",true);
            $A.util.addClass(mobileFieldName,"slds-has-error show-error-message");
            
            homephoneFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            homephoneFieldName.set("v.isError",true);
            $A.util.addClass(homephoneFieldName,"slds-has-error show-error-message");
            
            emailFieldName.set("v.errors", [{message: "Phone Number or Email Address is Required."}]);
            emailFieldName.set("v.isError",true);
            $A.util.addClass(emailFieldName,"slds-has-error show-error-message");

            this.showToast(component, "Phone Number or Email Address is Required.", "error", "error", true);
        }else {
            phoneFieldName.set("v.errors", []);
            phoneFieldName.set("v.isError", false);
            $A.util.removeClass(phoneFieldName,"slds-has-error show-error-message");
            
            emailFieldName.set("v.errors", []);
            emailFieldName.set("v.isError", false);
            $A.util.removeClass(emailFieldName,"slds-has-error show-error-message");
            
            homephoneFieldName.set("v.errors", []);
            homephoneFieldName.set("v.isError", false);
            $A.util.removeClass(homephoneFieldName,"slds-has-error show-error-message");
            
            mobileFieldName.set("v.errors", []);
            mobileFieldName.set("v.isError", false);
            $A.util.removeClass(mobileFieldName,"slds-has-error show-error-message");
        }
        
        return isValid;
    },
    
    getKeyByValue : function(map, searchValue) {
		for(var key in map) {
			if (map[key] === searchValue) {
				return key;
			}
		}
	},

	setLatLongAddress : function (component) {
		var contact = component.get("v.contact");
		if(!component.get("v.isGoogleLocation")){
			component.set("v.contact.MailingLatitude",null);
			component.set("v.contact.MailingLongitude",null);
			if (contact.LatLongSource__c != null && contact.MailingCity == null && contact.MailingPostalCode == null) {
				component.set("v.contact.LatLongSource__c","");
			}	
		}
		var address = component.get("v.presetLocation");
		if (component.get("v.streetAddress2")) {
			address = address  +', '+component.get("v.streetAddress2");			
		}		
		component.set("v.contact.MailingStreet",(address != undefined ? address : ""));
	},
    startSpinner: function(component, event, helper){
        component.set("v.Spinner", true);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');   
    },
    stopSpinner: function(component, event, helper){
        var spinner = component.find('spinner');
    	$A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');
        component.set("v.Spinner", false);   
    },

    checkForDuplicates : function(component, event ){

        this.startSpinner(component);

        var phoneList = [];

        if( component.get("v.reference_fields.Phone") ){
            phoneList.push( component.get("v.reference_fields.Phone") );
        }

        if( component.get("v.reference_fields.MobilePhone") ){
            phoneList.push( component.get("v.reference_fields.MobilePhone") );
        }

        if( component.get("v.reference_fields.HomePhone") ){
            phoneList.push( component.get("v.reference_fields.HomePhone") );
        }

        var action = component.get('c.callDedupeService');

        action.setParams({
            "fullName"  : component.get("v.reference_fields.FirstName") + ' ' + component.get("v.reference_fields.LastName"),
            "email"     : [component.get("v.reference_fields.Email")] ,
            "phones"    : phoneList,
            "recordType" : 'Talent'
        });


        action.setCallback(this, function(response) {

            var state = response.getState();
            
            if (state === "SUCCESS") {
                this.stopSpinner(component);
                var searchResults = JSON.parse( response.getReturnValue() );
        
                if( searchResults != null ){
                    component.set("v.dedupeResults", this.sortReferenceData(searchResults, 'Record_Type_Name__c')); 
        
                    component.set("v.show_modal", false);
                    this.displayResultOverlay(component, event );
                    
                }else{
                    component.set("v.dedupeResults", null );
                    this.convertToTalent(component); 
                }
                
                
            }else if (state === "ERROR") {
                this.showToast(component, "There is some issue with Dedupe service, Please contact your Administrator for details.", "error", "error");
                this.stopSpinner(component);
            }
           

        });
        $A.enqueueAction(action);
        
    },

    sortReferenceData : function( referenceData , sortByKey) {
        
        var sortedReferences =  referenceData.sort(function(a, b) {
            var x = a[sortByKey]; var y = b[sortByKey];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
        
        /* Return in decending order Talent -> Reference -> Client  */
        return sortedReferences.reverse();
        
    }, 

                
    
    displayResultOverlay : function(component, event ){

        $A.createComponents([
                ["c:C_DedupeResultModal",{
                    dedupeResults: component.get("v.dedupeResults"),
                    selectedRecord: component.getReference("v.selectedRecord")}],
                ["c:C_DedupeResultFooter", {
                    	recordType: 'Talent',
                        handleSelectedContact: component.getReference("c.handleSelectedContact"),
                        handleCurrentContact: component.getReference("c.handleCurrentContact"),
                        handleCloseModal: component.getReference("c.handleCloseModal"),
                        toast: component.getReference("v.toast"),
                        showToast: component.getReference("v.showToast"),
                    } 
                ]
            ],
            function(components, status){
                if (status === "SUCCESS") {
                    let modalBody = components[0];
                    let modalFooter = components[1];
                    let overlayPromise = component.find('dedupe_overlay').showCustomModal({
                        header: "Duplicate Talent Contacts",
                        body: modalBody,
                        footer: modalFooter,
                        showCloseButton: false,
                        cssClass: "slds-modal_large",
                        closeCallback: function() {
                            
                        }
                    });

                    component.set("v.overlayPromise", overlayPromise );

                }else if (status === "ERROR") {
                    this.showToast(component, 'Some Error Occurred, Please Contact Your Administrator.!', 'error','error');
                }
            }
        );
        
    },

    use_selected_contact : function(component) {   
        
        this.startSpinner( component );

        var action = component.get("c.useSelectedContact");
        action.setParams({
            "selectedContactId"     : component.get("v.selectedRecord"),
            "existingContact_obj"   : component.get("v.reference_fields"),
            "referenceId"           : component.get("v.recordId").Id

        });
        
        action.setCallback(this,function(response) {
            
            var state           = response.getState();
            var reference_data  = response.getReturnValue();

            if (state === "SUCCESS") {
                this.closeDedupeOverlay( component );
                this.showToast(component, "Converted Successfully", "success", "success", true);
                $A.get("e.force:closeQuickAction").fire();

                
                // window.open('/lightning/r/Contact/' + component.get("v.selectedRecord") +'/view' ); 
                
                var eUrl= $A.get("e.force:navigateToURL");
                eUrl.setParams({
                  "url": '/lightning/r/Contact/' + component.get("v.selectedRecord") +'/view'
                });
                eUrl.fire();
                

            }else{
                this.showToast(component, 'Something went wrong, Please contact Administrator' , "Error", "error");
            }

            this.stopSpinner( component );

            
        });

        $A.enqueueAction(action);
    },


    closeDedupeOverlay: function (component) {
        component.get("v.overlayPromise").then(
            function (modal) {
                modal.close();
                component.set("v.dedupeResults", null );
                component.set("v.selectedRecord", '' );

                // $A.get("e.force:closeQuickAction").fire();
                // $A.get('e.force:refreshView').fire();
            }
        );
        console.log(component.get("v.overlayPromise"));
    },

    showToast : function(component, message, title, toastType, place){
        
        var toastEvent = $A.get("e.force:showToast");
        let toastParams = {
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        };
        if (component.get('v.overlayPromise')) {
            component.set('v.toast', toastParams);
            component.set('v.showToast', true);
        }
        if (place || !component.get('v.overlayPromise')) {
            if (toastParams.title === 'success') {
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            } else {
                component.set('v.toast1', toastParams);
                component.set('v.showToast1', true);
            }
        }
    },

})