({
	doInit: function(component, event, helper) {
		helper.loadDncReasons(component);
		helper.getMinDate(component, event);
	},

	onDNCCheck: function(component, event, helper) {
		helper.onDNCCheck(component);
	},

	/* Method forcing object update to get over spring 19 known issue with object binding */
	dncReasonChange: function(component, event, helper) {
		component.set("v.dncFields", component.get("v.dncFields"));
	},
	reasonChange: function (cmp, e, h) {
		cmp.set("v.dncFields", cmp.get("v.dncFields"));
		//console.log(JSON.parse(JSON.stringify(cmp.get('v.dncFields'))));
	},

	/* Method forcing object update to get over spring 19 known issue with object binding */
	dncDateChange: function(component, event, helper) {
		/*component.set("v.errors", []);
		if (helper.isValidExpirationDate(component)) {*/
		component.set("v.dncFields", component.get("v.dncFields"));
		//console.log(JSON.parse(JSON.stringify(component.get('v.dncFields'))));
		// } 
	},

	validTalent : function(component, event, helper) {
		return helper.isValidExpirationDate(component);
	},
	loadedOptions: function (cmp, e, h) {
		cmp.set('v.optionsSpinner', false);
	}
})