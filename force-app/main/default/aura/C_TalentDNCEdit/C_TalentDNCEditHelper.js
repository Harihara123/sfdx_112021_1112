({
    onDNCCheck : function(component) {
		var dncFields = component.get("v.dncFields");
		//console.log('dnc clicked', JSON.parse(JSON.stringify(dncFields)));
        if (!dncFields.Do_Not_Contact__c || dncFields.Do_Not_Contact__c == false) {
            dncFields.Do_Not_Contact_Reason__c = "";
            dncFields.Do_Not_Contact_Date__c = "";
            component.set("v.dncFields", dncFields);
        }
		component.set("v.dncFields", component.get("v.dncFields"));
    }, 
   
	loadDncReasons: function(component) {
		var hlp = component.find("addEditHelper");
		//component.find('Reason')


		hlp.loadPicklistValues('Account', 'Do_Not_Contact_Reason__c', component.getReference('v.options'));
	},
	setPicklistValues: function(cmp, e, values) {
		//console.log(values);
	},

	isValidExpirationDate: function(component) {
		// let data = component.get('v.dncFields');
		// var dateCmp = component.find("expirationDate");
		// dateCmp.set("v.errors", []);

		// if (data.Do_Not_Contact__c && !data.Do_Not_Contact_Reason__c) {
		// 	this.showError($A.get("$Label.c.FIND_ADD_NEW_TALENT_DNC_REQUIRED"));
		// 	return false;
		// }

		// No expiration date selected
		// var expDate = component.get("v.dncFields.Do_Not_Contact_Date__c");
		// if (!expDate || expDate === "") {
		// 	return true;
		// }

		// var today = new Date();
		// var m = "" + (today.getMonth() + 1);
		// m = m.length === 1 ? "0" + m : m;
		// var d = today.getDate();
		// d = d < 10 ? "0" + d : d;
		// var todayStr = today.getFullYear() + "-" + m + "-" + d;

		const dateField = component.find("expDateInput");
		if(dateField) {
			const validity = dateField.reportValidity();
			if (!validity) {
				this.showError($A.get("$Label.c.ADD_NEW_TALENT_EXPIRATION_DATE"));
				return validity;
			}
			return true
		} else {return true}
		//
		//
		// if (component.get("v.dncFields.Do_Not_Contact_Date__c") < todayStr) {
		// 	dateCmp.set("v.errors", [{message:$A.get("$Label.c.ADD_NEW_TALENT_EXPIRATION_DATE")}]);
		// 	this.showError($A.get("$Label.c.ADD_NEW_TALENT_EXPIRATION_DATE"));
		// 	return false;
		// } else {
		// 	return true;
		// }
	},

	showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
	getMinDate: function (cmp, e) {
		let now = new Date();
		let minDate = now.toISOString().split('T')[0];

		cmp.set('v.minDate', minDate);
	},
})