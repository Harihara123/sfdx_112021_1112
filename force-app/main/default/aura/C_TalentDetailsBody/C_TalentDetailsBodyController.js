({
    handleRecordUpdated : function(cmp, event, helper) {
        //console.log('talent Details body handleRecordUpdated called ');
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {

            var conrecord = cmp.get("v.contactRecord");
            //console.log('conrecord ',conrecord.fields.AccountId.value);
           	cmp.set("v.talentId",conrecord.fields.AccountId.value);
            if(cmp.get("v.runningUser")) {
                helper.loadCmps(cmp);
            }
            else {
                
				var bdata = cmp.find("bdhelper");
				try {
					bdata.callServer(cmp,'','',"getCurrentUserWithOwnership",function(response){
						if(response && response.usr){
							cmp.set('v.runningUser',response.usr);
							cmp.set('v.runningUserCategory', response.userCategory);
							cmp.set('v.usrOwnership', response.userOwnership);
							helper.loadCmps(cmp);
						}
					},null,true);
				}
				catch (err) {
					var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
							"title": "Error!",
							"message": "Error during talent load. Please refresh the page.",
							"type": "error"
						});
					toastEvent.fire();
				}
            }
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }   
        
    },    
    activityReference : function(component, event, helper) {
        //console.log("#### Updating Activity ####");
        helper.loadCmps(component);
    },
    actvitySaved:function(cmp,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": $A.get("$Label.c.ATS_Activity_Saved"),
            "type": "success"
        });
        toastEvent.fire();
        
        var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
        updateLMD.fire();
    },
    
    
    refreshDocumentListAppEventHandler : function(cmp, event, helper) {
        //console.log('refreshDocumentListAppEventHandler  handled in body');
        //Sandeep: Fix for preview refresh.
        //Sandeep: Increased wait time from 3 seconds to 10 seconds.
        //Sandeep: Spinner  will displayed on UI during wait.
        //Sandeep: Leftpanel resume preview and right panel resume preview will be destroyed and created after the wait time.
        var params = null;
        var recordId = cmp.get("v.recordId") ;
        var contactRecord = cmp.get("v.contactRecord");
        //console.log('refreshDocumentListAppEventHandler contactRecord ',contactRecord);
        var timeToWait = helper.getTimeToWaitBeforeCallingResumingPreview(); 
        var runningUserCategory = cmp.get("v.runningUserCategory");
        var usrOwnership = cmp.get("v.usrOwnership");        
        params = {	recordId: recordId,
                  contactRecord : contactRecord,
                  runningUserCategory : runningUserCategory,
                  usrOwnership : usrOwnership,
                  refreshTimeOut :timeToWait }; 
        
        
       
		//Sandeep: Performance fixes//destroy existingpreview  compoennts before refresh/recreate
		let previewLeft = cmp.find("previewleft");
		if(!$A.util.isUndefinedOrNull(previewLeft)){
			previewLeft.destroy();
			//console.log('previewLeft component destroyed');
		}
		let previewright = cmp.find("previewright");
		if(!$A.util.isUndefinedOrNull(previewright)){
			previewright.destroy();
			//console.log('previewright component destroyed');
		}
        
        params["aura:id"]="previewleft";
        helper.createComponent(cmp, 'c:C_TalentResumePreview', 'v.C_TalentResumePreview_leftpanel',params);
        params["aura:id"]="previewright";
        helper.createComponent(cmp, 'c:C_TalentResumePreview', 'v.C_TalentResumePreview',params);
        helper.openResumeCompareModal(cmp, event);
        
        //Sandeep:fire refresh event after comps are recreated
        //E_TalentResumePreviewRefresh is same as TalentDocumentrefreshEvent .
       
                                
    },
    //Sandeep : Split screen change 
    split_toggle : function(component, event, helper) {
        helper.getTalentResumeLeft(component);
        let maindiv=component.find("maincontent");
        $A.util.toggleClass(maindiv, "fullRight");
        $A.util.toggleClass(maindiv, "split");
        let iconName=component.get("v.iconName");
        if(iconName == "right"){
            component.set("v.iconName","left");
            component.set("v.deviceSize","12");            
        }else{
            component.set("v.iconName","right");
            component.set("v.deviceSize","6");
        }        
    }
    
})