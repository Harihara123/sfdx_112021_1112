({
    createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );

    },

    loadCmps : function(cmp) {
        var recordId = cmp.get("v.recordId") ;
        var contactRecord = cmp.get("v.contactRecord");
        var runningUserCategory = cmp.get("v.runningUserCategory");
        var usrOwnership = cmp.get("v.usrOwnership");
        var params = {recordId: recordId
                      , contactRecord :contactRecord
                      , runningUserCategory : runningUserCategory
                      , usrOwnership : usrOwnership};        
        //Sandeep : needed two instances of C_TalentResumePreview component.
        var cmps = [
            'C_TalentInsights',
            'C_TalentEmploymentTimeline',
            'C_TalentActivityTimeline',
            'C_TalentSubmittals',
            'C_TalentApplications',
            'C_TalentResumePreview'
        ];

        if(contactRecord.fields.Lead_Talent__c.value){
            cmps.push('C_PhenomActivityLeadWrapper');
        }

        var usr = cmp.get("v.runningUser");
        if( usr.Profile.Name != 'ATS_Recruiter') {
            // For SD1, SD2, sys admin, and IS admin profiles, load matched reqs component.
            cmps.unshift('C_TalentMatchedREQs');
        }

        //Sandeep: performance fix . removed event to create components.Instaed creating the compoennts directly.
        for (var i=0;i<cmps.length;i++) {
        var componentName;
        var targetAttributeName;
            var params = {recordId: recordId
                          , contactRecord :contactRecord
                          , runningUserCategory : runningUserCategory
                          , usrOwnership : usrOwnership,
                          newTimeLine : true}; 

            if(cmps[i] === "C_TalentResumePreview"){
                this.getTalentResumes(cmp);
            } else {
               
                switch (cmps[i]) {

                    case "C_TalentApplications":
                        params = {
                            "recordId": recordId, 
                            "isApplicationCard": true, 
                            "runningUser": usr, 
                            "reloadSubmittals": cmp.getReference("v.reloadSubmittals"),
                              "talentData": cmp.getReference("v.contactRecord")
                             };

                        componentName = "c:C_TalentSubmittals";
                        targetAttributeName ="v.C_TalentApplications"; 
                    break;

                    case "C_TalentSubmittals": 
                        params = {
                            "recordId": recordId, 
                            "runningUser": usr, 
                            "reloadSubmittals": cmp.getReference("v.reloadSubmittals"),
                            "talentData": cmp.getReference("v.contactRecord")
                        };

                        componentName = "c:C_TalentSubmittals";
                        targetAttributeName ='v.C_TalentSubmittals';
                    break;

                    case "C_TalentMatchedREQs": 
                         params['usrOpco'] = cmp.getReference('v.runningUser.OPCO__c');
                        componentName =   'c:C_TalentMatchedREQs';
                        targetAttributeName = 'v.C_TalentMatchedREQs';
                    break;

                    case "C_PhenomActivityLeadWrapper":
                        params = {
                            "talentId": recordId
                        };

                        componentName = "c:C_PhenomActivityLeadWrapper";
                        targetAttributeName ='v.C_PhenomActivityLeadWrapper';
                    break;

                    default:
                        componentName =   'c:' + cmps[i] ;
                        targetAttributeName = 'v.' + cmps[i];
                    break;
                }

               
                this.createComponent(cmp, componentName, targetAttributeName,params);
            } 
        } 
    } , 

    getTimeToWaitBeforeCallingResumingPreview : function() {
        var defaultInt = 9000; 
        var timeToWaitNum; 
        try {
            timeToWaitNum =  parseInt($A.get("$Label.c.ATS_TIME_TO_WAIT_RESUME_REFRESH"));
        } catch (error) {
            //console.log('failed to parse label value to Int !! '  + error.message);
            timeToWaitNum  = defaultInt; 
        }
        
        if (isNaN(timeToWaitNum)) {
            timeToWaitNum = defaultInt; 
        }
        
        return timeToWaitNum; 
    },
    openResumeCompareModal : function(component,event) {
        //console.log('openResumeCompareModal called',event.getParam('isUploadCompare'));
        //console.log('event recordId ',event.getParam("recordId"));
        //console.log('cmp talentId ',component.get("v.talentId"));
		//Rajeesh
		//Rajeesh stopping multiple modals to open.
		if (event.getParam("recordId") != component.get("v.talentId")) {//v.recordId
			// Do nothing if this is not the same contact
			return;
		}
        if(event.getParam('isUploadCompare') == true) {
            var uploadedDocumentID = event.getParam('uploadedDocID');
            var compareRecord = event.getParam('compareRecord');
            var talentId = component.get('v.talentId');
            var modalBody;
            var modalHeader;
            var modalFooter;
            var conrecord = component.get("v.contactRecord");
            
            //console.log('conrecord ',conrecord.Name);
            //console.log('compareRecord ',compareRecord);
            $A.createComponents([
                ["c:C_TalentResumeCompareAndUpdateModal_Body_Contact",{"items":JSON.parse(compareRecord)}],
                ["c:C_TalentResumeCompareAndUpdateModal_Header",{"fName":component.get("v.contactRecord.fields.Name.value"),"lName":conrecord.LastName}],
                ["c:C_TalentResumeCompareAndUpdateModal_Footer",{"items":JSON.parse(compareRecord)}]
            ],
                                function(components, status){
                                    if (status === "SUCCESS") {
                                        modalBody = components[0];
                                        modalHeader = components[1];
                                        modalFooter = components[2]; 
                                        component.find('overlayLib').showCustomModal({
                                            header: modalHeader,
                                            body: modalBody,
                                            footer:modalFooter,
                                            cssClass: "ResumeParseModal",
                                            showCloseButton: true,
                                            closeCallback: function() {
                                                //console.log('Resume compare modal closed!!!');
                                                
                                            }
                                        });
                                    }
                                }
                               );//Create component end
        }
        
    },
    
    getTalentResumes : function(component) {
        this.createTalentResumePanel(component,'previewright','v.C_TalentResumePreview');        
    },
    getTalentResumeLeft : function(component) {
        this.createTalentResumePanel(component,'previewleft','v.C_TalentResumePreview_leftpanel');    
    },
    createTalentResumePanel: function(component,comp_auraid,targetAttribute){
        var recordId = component.get("v.contactRecord.fields.AccountId.value");
        var actionParams = {'recordId' : recordId};
        var bdhelper = component.find('bdhelper');
        bdhelper.callServer(component, 'ATS', 'TalentDocumentFunctions', 'getTalentResumeModel', function(response){
            var componentRecordId = component.get("v.recordId");
            var parseViewList = JSON.parse(response.viewList);
            var cmpEvent = component.getEvent("createCmpEvent");
            var params = {
                "recordId" : componentRecordId, 
                "talentId" : recordId,
                "contactRecord" : component.get("v.contactRecord"),
                "aura:id" : comp_auraid,
                "viewList" : parseViewList,
                "recordCount" : response.recordAndCount.count,
                "records" : response.recordAndCount.records,
                "bInitialize" : false};
            
            if(parseViewList[0].additional) {
                params["additional"] = parseViewList[0].additional;
            }
            if(response.recordAndCount.count === '0') {
                params["additional"] = '';
            }
            
            var componentName = 'c:C_TalentResumePreview';
            
            $A.createComponent(
                componentName,
                params,
                function(newComponent, status, errorMessage){
                    if (status === "SUCCESS") {
                        component.set(targetAttribute, newComponent);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                            // Show error message
                        }
                }
            );
        }, actionParams, true);
    }
})