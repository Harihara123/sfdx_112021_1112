({
	
    doInit : function(component, event, helper) {
        var recordId = helper.getParameterByName("recordId");
        component.set ("v.recordId", recordId);
        helper.loadTalent(component, helper);
        helper.getResults(component,helper);
        helper.getResultCount(component);
//        helper.loadTalentDocuments(component, helper);
    },
    
    showModalUploadResumeAppEvent : function(component, event, helper) {
        var theVal = component.get("v.addTrigger")
        component.set("v.addTrigger", !theVal);
 	},
     
    refreshDocumentListAppEventHandler : function(component, event, helper) {
        var refreshType = event.getParam("refreshType");
        if(refreshType != "setDefault"){
            $A.get('e.force:refreshView').fire();
            //component.set("v.reloadData", true);
        }
    },
    
   viewCandidateResumes :function(cmp, event, helper) {
        var recordID = cmp.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__CandidateDocuments?id=" + recordID
        });
        urlEvent.fire();
	},

    setAsDefaultDoc : function(cmp, event, helper) {
        var recordID = cmp.get("v.recordId");
        var talentDocumentId = event.currentTarget.dataset.recordid;
        helper.setAsDefaultDoc(cmp, recordID,talentDocumentId,helper);
		var appEvent = $A.get("e.c:E_TalentDocumentsRefresh");
        appEvent.setParams({"refreshType":"setDefault"});
        appEvent.fire();
    },
    
    deleteDocument : function(component, event, helper) {
        if (event.getParam('confirmationVal') === 'Y') {
            var recordID = component.get("v.recordId");
            var talentDocumentId = component.get("v.talentDocumentId");
            // console.log(talentDocumentId);
            helper.deleteTheDoc(component, recordID,talentDocumentId,helper);
        }
    },

    confirmDelete : function(component, event, helper) {
        var theVal = component.get("v.showConfirmationModal")
        component.set("v.showConfirmationModal", !theVal);
        component.set ("v.talentDocumentId", event.currentTarget.dataset.recordid)
    }
    ,reloadData : function(cmp,event,helper) {
        var rd = cmp.get("v.reloadData");
        if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);

            helper.getResults(cmp,helper);
            helper.getResultCount(cmp);

            cmp.set("v.reloadData", false);
        }
    }
})