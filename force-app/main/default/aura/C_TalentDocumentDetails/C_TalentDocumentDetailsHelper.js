({
	 getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");
        var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var soql = listView[indexNo].soql;
			
            soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
  
                        var apexFunction = "c.getRows";

                        var params = {
                            "soql": soql,
                            "maxRows": cmp.get("v.maxRows")
                        };

                        cmp.set("v.loadingData",true);
                        var actionParams = {'recordId':recordID};
                        if(params){
                            if(params.length != 0){
                             Object.assign(actionParams,params);
                            }
                        }

                        var bdata = cmp.find("bdhelper");
                            bdata.callServer(cmp,'','',apexFunction,function(response){
                                // console.log(response);
                            cmp.set("v.records", response);
                            cmp.set("v.loadingData",false);
                        
                            if(listView[indexNo].additional){
                                cmp.set("v.additional",listView[indexNo].additional);
                            }
                            
                        },actionParams,false);
        }
    ,getResultCount : function(cmp){
        var listView = cmp.get("v.viewList");

            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var countSoql = listView[indexNo].countSoql;    
            
                    countSoql = countSoql.replace("%%%parentRecordId%%%","'"+recordID+"'")
                    var params = {"soql": countSoql};
                    cmp.set("v.loadingCount",true);
                    var bdata = cmp.find("bdhelper");
                        bdata.callServer(cmp,'','','getRowCount',function(response){
                            
                            cmp.set("v.recordCount", response);
                            cmp.set("v.loadingCount",false);
                        
                    },params,false);

        
    },
    loadTalent : function(component, recordId, helper) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c.getRecords");
        action.setParams({ recordId : component.get("v.recordId")});

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //refresh component
                //console.log('@@ Get Talent');
                //console.log(response.getReturnValue());
                 component.set("v.selectedTalent",response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    deleteTheDoc : function(component, recordId,talentDocumentId, helper) {
         var params = {"talentDocumentId": talentDocumentId, "recordId":recordId};
       	 
         var bdata = component.find("bdhelper");
          bdata.callServer(component,'ATS','TalentDocumentFunctions',"deleteDocumentRecord",function(response){
             component.set("v.reloadData", true);
         },params,false);
    },
    
   setAsDefaultDoc : function(component, recordId,talentDocumentId, helper) {
        var params = {"talentDocumentId": talentDocumentId, "recordId":recordId};
       
        var bdata = component.find("bdhelper");
        bdata.callServer(component,'ATS','TalentDocumentFunctions',"updateDefaultResumeFlag",function(response){
            component.set("v.reloadData", true);
        },params,false);
       
	},
    getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
 
})