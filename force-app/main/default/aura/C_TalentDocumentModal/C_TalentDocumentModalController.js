({	
    fileSelectedCompleteEventHandler: function(component, event, helper) {
        // console.log('fileselected event captured');
        var fileName = event.getParam("fileName");
        component.set("v.fileName", fileName);
        helper.enableSaveBtn(component, component.get("v.replaceResumeId"));
    },

    fileUploadCompleteEventHandler: function(component, event, helper) {
       // console.log('fileupload complete event captured');
        helper.performPostUploadAction(component);
    },

    documentTypeChange: function(component, event, helper) {
        helper.documentTypeChange(component, event);
    //     var recordId = component.get ("v.recordId");
    //     var documentType = component.find("documentTypeSelectId").get("v.value");
    //     var defaultResumeDiv = component.find("defaultResumeDiv");
    //     var resumeCount = component.get ("v.resumeCount");
    //     var resumeDubbedCount = component.get("v.resumeDubbedCount"); //S-40390 Added by Karthik
    //     component.set("v.replaceResumeSelected" , null);
    //     if (documentType === "Resume")
    //     {
    //         $A.util.addClass(defaultResumeDiv, "show");
    //         component.set('v.defaultFlag', false);
    //         component.set("v.resumeDubbedReplaceList", null);
    //         //S-40390
    //         var replaceResumeDubbedListDiv = component.find("replaceResumeDubbedListDiv");
    //         $A.util.removeClass(replaceResumeDubbedListDiv, "show");
    //         $A.util.addClass(replaceResumeDubbedListDiv, "hide");
    //         document.getElementById("replaceResumeDubbedValidationDivId").style.display = "none";
    //         if (resumeCount > 4) {
    //             helper.getResumeListToReplace (component, recordId);
    //             var replaceResumeListDiv = component.find("replaceResumeListDiv");
    //             $A.util.addClass(replaceResumeListDiv, "show");
    //             document.getElementById("replaceResumeValidationDivId").style.display = "block";
    //         }
    //     }
    //     //S-40390 - Dubbed Resume Added by Karthik
    //     else if (documentType === "Resume - Dubbed")
    //     {
    //         component.set('v.defaultFlag', false);
    //         $A.util.removeClass(defaultResumeDiv, "show");
    //         $A.util.addClass(defaultResumeDiv, "hide");
    //         var replaceResumeListDiv = component.find("replaceResumeListDiv");
    //         $A.util.removeClass(replaceResumeListDiv, "show");
    //         $A.util.addClass(replaceResumeListDiv, "hide");
    //         component.set("v.resumeReplaceList", null);
    //         document.getElementById("replaceResumeValidationDivId").style.display = "none";
    //         if (resumeDubbedCount > 2){
    //             helper.getResumeDubbedListToReplace (component, recordId);
    //             var replaceResumeDubbedListDiv = component.find("replaceResumeDubbedListDiv");
    //             $A.util.addClass(replaceResumeDubbedListDiv, "show");
    //             document.getElementById("replaceResumeDubbedValidationDivId").style.display = "block";
    //         }
    //     }
    //     else
    //     {
    //         component.set('v.defaultFlag', false);
    //         component.set("v.resumeReplaceList", null);
    //         component.set("v.resumeDubbedReplaceList", null);
    //         $A.util.removeClass(defaultResumeDiv, "show");
    //         $A.util.addClass(defaultResumeDiv, "hide");
    //         document.getElementById("replaceResumeValidationDivId").style.display = "none";
    //         document.getElementById("replaceResumeDubbedValidationDivId").style.display = "none";
    //         var replaceResumeListDiv = component.find("replaceResumeListDiv");
    //         $A.util.removeClass(replaceResumeListDiv, "show");
    //         $A.util.addClass(replaceResumeListDiv, "hide");
    //         //S-40390
    //         var replaceResumeDubbedListDiv = component.find("replaceResumeDubbedListDiv");
    //         $A.util.removeClass(replaceResumeDubbedListDiv, "show");
    //         $A.util.addClass(replaceResumeDubbedListDiv, "hide");
    //      }
    //     helper.enableSaveBtn(component);
    },
    
    onReplaceResumeChange: function(component, event, helper) {
        var selectedResume = event.getSource().get("v.text");
        component.set("v.replaceResumeId" , selectedResume);
        if (component.get('v.sourceOfEvent') !== 'FAT_Upload') {

            //console.log(selectedResume);
            helper.enableSaveBtn(component, selectedResume);
        }
    },

    onReplaceResumeDubbedChange: function(component, event, helper) {
        var selectedResume = event.getSource().get("v.text");
        //console.log(selectedResume);
        component.set("v.replaceResumeId" , selectedResume); 
        helper.enableSaveBtn(component, selectedResume);
    },

    saveTalentDocument : function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.isDisabled', true);
		component.set('v.isUpdateCompareDisabled', true);
		component.set("v.isUploadCompare", false);
        helper.saveTalentDocument(component);
    },

    closeModal: function(component, event, helper) {
        // console.log('closing the model');
        helper.closeModal(component, event);
		// var docModalCloseEvt = component.getEvent("documentModalCloseEvent");
        // docModalCloseEvt.fire();
        //
		// if(!component.get("v.isUploadCreate")){
		//     component.destroy();
		// }else{
		//     helper.toggleClassInverse(component,'backdropAddDocument','slds-backdrop--');
		// 	helper.toggleClassInverse(component,'modaldialogAddDocument','slds-fade-in-');
        // }

    },
	callDocModal  : function (component, event, helper) {
	if(!component.get("v.isUploadCreate")){
	    helper.initializeDocumentModal(component);
        
        helper.toggleClass(component,'backdropAddDocument','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
        
        var recordId = component.get("v.recordId");
        helper.getResumeCount(component, recordId);
        helper.getResumeDubbedCount(component, recordId); //S-40390
        
        var replaceResumeListDiv = component.find("replaceResumeListDiv");
            if (component.get('v.sourceOfEvent') === "FAT_Upload") {
                $A.util.addClass(replaceResumeListDiv, "show");
                helper.getResumeListToReplace(component, recordId);
            } else {
                $A.util.removeClass(replaceResumeListDiv, "show");
                $A.util.addClass(replaceResumeListDiv, "hide");
            }


        //S-40390
        var replaceResumeDubbedListDiv = component.find("replaceResumeDubbedListDiv");
        $A.util.removeClass(replaceResumeDubbedListDiv, "show");
        $A.util.addClass(replaceResumeDubbedListDiv, "hide");
	   
	  }
	
	},
    callAddModal : function (component, event, helper) {
	   
        //Sandeep: getting talent Id from event
        //Rajeesh disabling the save & update button for places other than documents tab.
		var isNotDocUpload;
		if($A.util.isEmpty(event.getParam("enableSave"))){
			isNotDocUpload=true;
		}
		else{
			isNotDocUpload = false;  
		}
		//Rajeesh for add candidate recordId is already set.
		
		if(!component.get('v.isUploadCreate'))
		{
			component.set("v.recordId",event.getParam("talentId"));
			//console.log('event.getParam("talentId")'+event.getParam("talentId"));
		}
        
		//component.set("v.sourceOfEvent",event.getParam("source"));
		//console.log('event.getParams'+event.getParams());
		//for(var key in event.getParams()){console.log('key'+key+':val'+event.getParams()[key]);}
		component.set("v.isNotDocUpload",isNotDocUpload);
		//console.log('v.isNotDocUpload ',isNotDocUpload);
		//console.log('v.isUploadCreate ',component.get("v.isUploadCreate"));
        //console.log('add model called ',component.get("v.recordId"));
		
        helper.initializeDocumentModal(component);
        
        helper.toggleClass(component,'backdropAddDocument','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
        
        var recordId = component.get("v.recordId");
        helper.getResumeCount(component, recordId);
        helper.getResumeDubbedCount(component, recordId); //S-40390
        
        document.getElementById("replaceResumeValidationDivId").style.display = "none";
        document.getElementById("replaceResumeDubbedValidationDivId").style.display = "none"; //S-40390
        var replaceResumeListDiv = component.find("replaceResumeListDiv");
        $A.util.removeClass(replaceResumeListDiv, "show");
        $A.util.addClass(replaceResumeListDiv, "hide");
        //S-40390
        var replaceResumeDubbedListDiv = component.find("replaceResumeDubbedListDiv");
        $A.util.removeClass(replaceResumeDubbedListDiv, "show");
        $A.util.addClass(replaceResumeDubbedListDiv, "hide");
    },
	saveResumeUpdateTalent : function(component, event, helper) {
		component.set("v.isUploadCompare", true);
		component.set('v.showSpinner', true);
        component.set('v.isDisabled', true);
		component.set('v.isUpdateCompareDisabled', true);
        helper.saveTalentDocument(component);
	},
    toggleResults: function (cmp, e, h) {
        h.showDupeResults(cmp, e)
    },
    invokeMerge: function (cmp, e, h) {
        h.invokeMerge(cmp,e)
    },
    closeResults: function (cmp, e, h) {
        h.closeDupeResults(cmp, e)
    },
    replaceDoc: function (cmp, e, h) {
        h.replaceDoc(cmp, e)
    },
})