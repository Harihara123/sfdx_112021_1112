({
    // Added by Santosh - 5th Oct 2017
    // Call TalentDocumentViewer.cmp
   doInit: function(cmp, event, helper) {
        var conrecord = cmp.get("v.contactrecord");
        cmp.set("v.talentId",conrecord.fields.AccountId.value);
       
        helper.getResults(cmp,helper);
        //helper.getResultCount(cmp);  
        
    },
    // STORY S-110973: Creating Document modal on the Documents tab 
    showCreateDocumentCreateModal : function(component, event, helper) { 
        var modalEvt = $A.get("e.c:E_CreateDocumentModal");
        modalEvt.setParams({'contactId' : component.get('v.talentId'),'source':'TalentDocument'});
        modalEvt.fire(); 

    },
    deleteDocument : function(component, event, helper) {
        if (event.getParam('confirmationVal') === 'Y') {
            var recordID = component.get("v.recordId");
            var talentDocumentId = component.get("v.talentDocumentId");
            // console.log(talentDocumentId);
            helper.deleteTheDoc(component, recordID,talentDocumentId,helper);
        }
    },
    showMoreDocuments : function(component, event, helper) { 
		
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	//Neel summer 18 URL change->"url": "/one/one.app#/n/Talent_Document_Details?recordId=" + component.get('v.talentId'),
             "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Talent_Document_Details?recordId=" + component.get('v.talentId'),            
            "isredirect":true
        });
        urlEvent.fire(); 

	},
    
    showModalUploadResumeAppEvent : function(component, event, helper) {
         /* Sandeep : C_TalentDocumentModal moved to Modalscontainer comp.Attribute switch will not work.
             * Firing documentModalOpenEvent */
      
        let docModelevt= $A.get("e.c:E_DocumentModalOpen");
            docModelevt.setParam("talentId",component.get('v.talentId'));
			//Neel setting source component name which firing event
			docModelevt.setParam("source","TalentDocument");
			
			//Rajeesh setting the flag to enable save and upload button
			docModelevt.setParam("enableSave",true);
            docModelevt.fire();
            //console.log('docModelevt fired:enable save');
 	},
    
    refreshDocumentListAppEventHandler : function(component, event, helper) {
		var refreshType = event.getParam("refreshType");
        if(refreshType != "setDefault"){
            component.set("v.reloadData", true);
        }		
    },
    
   viewCandidateResumes :function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__CandidateDocuments?id=" + recordID
        });
        urlEvent.fire();        
	},

    setAsDefaultDoc : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");
        var talentDocumentId = event.currentTarget.dataset.recordid;
        helper.setAsDefaultDoc(cmp, recordID,talentDocumentId,helper);

        var appEvent = $A.get("e.c:E_TalentDocumentsRefresh");
        appEvent.setParams({"refreshType":"setDefault"});
        appEvent.fire();
    }
    ,updateLoading : function(cmp,event,helper){
        helper.updateLoading(cmp);
    },
    confirmDelete : function(component, event, helper) {
        var theVal = component.get("v.showConfirmationModal")
        component.set("v.showConfirmationModal", !theVal);
        component.set ("v.talentDocumentId", event.currentTarget.dataset.recordid)
    }
    ,reloadData : function(cmp,event,helper) {
        // console.log('reloadData called--- resetting the comp');
        var rd = cmp.get("v.reloadData");
        if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);

            helper.getResults(cmp,helper);
            //helper.getResultCount(cmp);
			//closing the model
			//cmp.find("docModal").closeModal(); //Sandeep : docmodel is moved to modals container..
            cmp.set("v.reloadData", false);
        }
    }, 
	openResumeCompareModal : function(component, event, helper) {
		helper.openResumeCompare(component, event);
	},
    openResumeModal: function(cmp, e, h){

        const attachmentId = e.currentTarget.dataset.id;
        const resumeName = cmp.get("v.contactrecord").fields.Name.value;


        if (attachmentId && resumeName) {
            const appEvent = $A.get("e.c:E_SearchResumeModal");
            const params = {
                "attachmentId": attachmentId,
                "resumeName": resumeName,
                "destinationId": "TalentDocuments"
            };
            
            appEvent.setParams(params);
            appEvent.fire();
        }
    },
    downloadFile: function(cmp, e, h) {
        e.preventDefault();
        const url = window.location.origin.split('.')[0];
        const attachmentId = e.currentTarget.dataset.id;
        const windowUrl = `${url}--c.documentforce.com/servlet/servlet.FileDownload?file=${attachmentId}`;

        window.open(windowUrl, '_blank');
    }
})