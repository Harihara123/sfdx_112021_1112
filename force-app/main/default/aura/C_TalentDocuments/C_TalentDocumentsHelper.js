({
	
    setAsDefaultDoc : function(component, recordId,talentDocumentId, helper) {
        var params = {"talentDocumentId": talentDocumentId, "recordId":recordId};
        var bdata = component.find("bdhelper");
        bdata.callServer(component,'ATS','TalentDocumentFunctions',"updateDefaultResumeFlag",function(response){
            component.set("v.reloadData", true);
        },params,false);
	},
    deleteTheDoc : function(component, recordId,talentDocumentId, helper) {
        var params = {"talentDocumentId": talentDocumentId, "recordId":recordId};

        
        var bdata = component.find("bdhelper");
        bdata.callServer(component,'ATS','TalentDocumentFunctions',"deleteDocumentRecord",function(response){
            component.set("v.reloadData", true);
            var docDeteledEvt = $A.get("e.c:E_DocumentDeleted");
            docDeteledEvt.fire(); 
        },params,false);

    },
    updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },
     refreshList : function(cmp) {
        cmp.set("v.reloadData", true);
    },
    getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");
        var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.talentId");
            var soql = listView[indexNo].soql;
			
            soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
  
                        var apexFunction = "c.getRows";

                        var params = {
                            "soql": soql,
                            "maxRows": cmp.get("v.maxRows")
                        };

                        cmp.set("v.loadingData",true);
                        var actionParams = {'recordId':recordID};
                        if(params){
                            if(params.length != 0){
                             Object.assign(actionParams,params);
                            }
                        }
						var temprecords = [];
						var finalRecords = [];
                        var bdata = cmp.find("bdhelper");
                            bdata.callServer(cmp,'','',apexFunction,function(response){
							for( var i= 0 ; i< response.length ; i++){

							//S-149256 -- To check most recent document from OFCCP and Job board.
								if (response[i].Document_Type__c == 'Resume - Job Board' || response[i].Document_Type__c == 'Resume - OFCCP'){
									
									temprecords.push(response[i]);
								}else {
									finalRecords.push(response[i]);
								}
							}
							if(temprecords.length > 0){
								temprecords.sort(function(a,b) {
									var x = new Date(a.CreatedDate);
									var y = new Date(b.CreatedDate);

									return (x < y ? -1 : 1) * -1;
								});
								finalRecords.push(temprecords[0]);
							} 
							finalRecords.sort(function(a,b) {
									var x = new Date(a.LastModifiedDate );
									var y = new Date(b.LastModifiedDate );

									return (x < y ? -1 : 1) * -1;
							});
							

                            cmp.set("v.records", finalRecords);
                            cmp.set("v.loadingData",false);
                        
                            if(listView[indexNo].additional){
                                cmp.set("v.additional",listView[indexNo].additional);
                            }
                            
                        },actionParams,false);
        },
	 
	 openResumeCompare : function(component, event) {
		if(event.getParam('isUploadCompare') === true) {
			//Rajeesh stopping multiple modals to open.
			 if (event.getParam("recordId") != component.get("v.talentId")) {//v.recordId
				// Do nothing if this is not the same contact
				return;
			}
			var uploadedDocumentID = event.getParam('uploadedDocID');
			var compareRecord = event.getParam('compareRecord');
			//Rajeesh S-76509 - Parse resume and update talent record
            var modalBody;
            var modalHeader;
            var modalFooter;
            var conrecord = component.get("v.contactrecord");
            $A.createComponents([
                ["c:C_TalentResumeCompareAndUpdateModal_Body_Contact",{"items":JSON.parse(compareRecord)}],
                ["c:C_TalentResumeCompareAndUpdateModal_Header",{"fName":conrecord.fields.FirstName.value,"lName":conrecord.fields.LastName.value}],
                ["c:C_TalentResumeCompareAndUpdateModal_Footer",{"items":JSON.parse(compareRecord)}]
            ],
                function(components, status){
                    if (status === "SUCCESS") {
                        modalBody = components[0];
                        modalHeader = components[1];
                        modalFooter = components[2]; 
                        component.find('overlayLib').showCustomModal({
                            header: modalHeader,
                            body: modalBody,
                            footer:modalFooter,
							cssClass: "ResumeParseModal",
                            showCloseButton: true,
                            closeCallback: function() {
                                //console.log('closed the modal');
                            }
                        });
                    }
                }
               );
		}
		
		component.set("v.isUploadCompare", false);

        var refreshType = event.getParam("refreshType");
        if(refreshType != "setDefault"){
            // console.log('refreshing the view----');
            component.set("v.reloadData", true);
            //$A.get('e.force:refreshView').fire();
            
        }
	 },
    createComponent: function (cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
            componentName,
            params,
            function (newComponent, status, errorMessage) {
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                }
            }
        );
    }
    
    
})