({
    init: function (cmp, e, h) {
       // cmp.set('v.selectedResults',null);
        h.randomId(cmp, e);
        console.log(JSON.stringify(cmp.get('v.results')));
    },
    onChange: function (cmp, e, h) {
        h.onChange(cmp,e)
    },
    merge: function (cmp, e, h) {
        h.merge(cmp, e)
    },
    closeResults: function (cmp, e, h) {
        cmp.set('v.showDupeResults', false);
    },
    sort: function (cmp, e, h) {
        //console.log('sorting', e.target.getAttribute('data-type'));
        
        h.sort(cmp, e);
    },
    getCID: function(cmp, e, h) {
        return h.getCID(cmp, e);
    },
    itemsChange:function(cmp, e, h){
    	
        console.log('AccountID'+cmp.get('v.accountRecordId'));
        var results = cmp.get('v.results');
        if(cmp.get('v.accountRecordId')){
            //console.log('first record ID' +results[0].candidateId);
            results.forEach(talent => {
                if (talent.candidateId === cmp.get('v.accountRecordId')) {
                    cmp.set('v.master', talent.talentId)
                }
            })
            // if(results[0].candidateId == cmp.get('v.accountRecordId')){
            //    // select first record
            //     cmp.set('v.master', results[0].talentId)
            // }
        }
        
	},
    getTalentName: function(cmp, e, h) {
        return h.getTalentName(cmp);
    }
    
})