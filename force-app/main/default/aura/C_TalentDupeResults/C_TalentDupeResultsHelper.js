({
    randomId: function (cmp, e) {
        let randomID = Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
        cmp.set('v.randomID', randomID);
        
        //console.log(randomID)
    },
    onChange: function (cmp, e) {
        let selected = cmp.get('v.selectedResults');
        const master = cmp.get('v.master');
        //console.log(e.target.value, e.target.checked);
        if(selected){
            if (e.target.checked) {            //add to list
                // if (selected.length === 2) {
                //     e.target.checked = false;
                //     return e.preventDefault();
                // }

                selected.push(e.target.value)
                if (master) {
                    if (selected.length > 0) {
                        cmp.set('v.enabledMergeButton', true);
                    } else {
                        cmp.set('v.enabledMergeButton', false);
                    }
                } else {
                    if (selected.length > 1) {
                        cmp.set('v.enabledMergeButton', true);
                    } else {
                        cmp.set('v.enabledMergeButton', false);
                    }
                }
            } else {
                //remove from list
                const index = selected.indexOf(e.target.value);
                if (index > -1) {
                    selected.splice(index, 1);
                }
                if (master) {
                    if (selected.length > 0) {
                        cmp.set('v.enabledMergeButton', true);
                    } else {
                        cmp.set('v.enabledMergeButton', false);
                    }
                } else {
                    if (selected.length > 1) {
                        cmp.set('v.enabledMergeButton', true);
                    } else {
                        cmp.set('v.enabledMergeButton', false);
                    }
                }
            }
    }
        //console.log(selected)
    },
    merge: function (cmp, e) {
        //console.log('merge method!');
        const selected = cmp.get('v.selectedResults');
        const master = cmp.get('v.master');

        if (master) {
            if (selected.length !== 1) {
                //alert('select 2 contacts to merge');
                this.showError('You must select 2 record for merge.','Error');
                return
            } else {
                this.navigateToMerge(cmp, e)
                return
            }
        }

        if (selected.length !== 2) {
            //alert('select 2 contacts to merge');
            this.showError('You must select 2 records for merge.','Error');
            return
        }
        this.navigateToMerge(cmp, e)
        // const pageReference = {
        //     type: 'standard__component',
        //     attributes: {
        //         "componentName" : "c__C_TalentMergeComparison"
        //     },
        //     state: {
        //         "c__masterID": master?master:selected[0],
        //         "c__duplicateID": selected[1],
        //         "c__FromDuplicateComponent": "yes"
        //     }
        // };
        // const navService = cmp.find("dupeNavService");
        // e.preventDefault();
        // navService.navigate(pageReference);
    },
    sort: function (cmp, e) {
        const type = e.target.getAttribute('data-type');
        let sorting = cmp.get('v.sorting');

        if (type === sorting.current) {
            cmp.set('v.sorting.'+type, !sorting[type]);
            console.log(cmp.get('v.sorting')[type]);
            this.sortString(cmp, e, cmp.get('v.sorting')[type]);

        } else {
            sorting = {
                current: type,
                name:  false,
                employer: false,
                title: false,
                location: false,
                lastmod: false,
                lastact: false
            }
            //sorting[type] = true;
            cmp.set('v.sorting', sorting);
            this.sortString(cmp, e, false);
            console.log(cmp.get('v.sorting').current);
            //
        }
    },
    sortString: function (cmp, e, sort) {
        const type = e.target.getAttribute('data-type');
        let sortedResults = cmp.get('v.results');

        sortedResults = sortedResults.sort((a, b) => {
            if (!a[type]) {
                return (sort?1:-1);
            }
            if (!b[type]) {
                return (sort?-1:1);
            }

            let nameA = a[type].toUpperCase();
            let nameB = b[type].toUpperCase();

            if (nameB > nameA) {

                return (sort?-1:1)

            }
            if (nameB < nameA) {
                return (sort?1:-1)
            }
            return 0
        })
        cmp.set('v.results', sortedResults);
    },
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    getCID: function(cmp, e) {
        const selected = cmp.get('v.selectedResults');
        if (selected.length === 1) {
            //return CID
            //console.log('selected[0]-->'+selected[0]);
            return selected[0];
        } else {
            this.showError('You must select single record to proceed.','Error');
        }
    },
    getTalentName: function(cmp) {
        const randomId = cmp.get('v.randomID');
        const selected = cmp.get('v.selectedResults');

        if (selected.length === 1) {
            const record = document.getElementById(randomId+'-'+selected[0]);
            const fname = record.getAttribute('data-fname');
            const lname = record.getAttribute('data-lname');

            return {lname, fname}
        } else {
            this.showError('You must select single record to proceed.','Error');
        }
    },
    navigateToMerge: function (cmp, e) {
        const selected = cmp.get('v.selectedResults');
        const master = cmp.get('v.master');

        const pageReference = {
            type: 'standard__component',
            attributes: {
                "componentName" : "c__C_TalentMergeComparison"
            },
            state: {
                "c__masterID": master?master:selected[0],
                "c__duplicateID": master?selected[0]:selected[1],
                "c__FromDuplicateComponent": "yes"
            }
        };
        const navService = cmp.find("dupeNavService");
        e.preventDefault();
        navService.navigate(pageReference);
    },
})