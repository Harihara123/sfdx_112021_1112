({
	
	doInit : function(component, event, helper) {
		helper.getCountryMappings(component);
		helper.generateTransactionID(component); 
        
        
		var p = helper.getParameterByName("clientcontactid");
        component.set("v.clientContactId", p);

        // clientcontactid parameter valid. Related contact flow.
        if (p !== null && p !== "") {
        	component.set("v.mode", "search");
        	helper.getClientContactData(component,p);
        } 
        
        //S-228927 - adding new opcos SJA,IND,EAS  
        var newAerotek = $A.get("$Label.c.Aerotek"); 
        var newAstonCarter = $A.get("$Label.c.Aston_Carter");
        var newNewco = $A.get("$Label.c.Newco");
        
        var action = component.get("c.getCurrentUserWithOwnership");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var results = response.getReturnValue();
			if (component.isValid() && state === "SUCCESS") {
                component.set("v.runningUser", results.usr.Profile.Name);
                var traineeUserTitle	= 'recruiter trainee';
	            var giveAccess = false; 
                if(component.get("v.runningUser") == 'System Administrator' || component.get("v.runningUser") == 'IS Admin')
                    giveAccess = true;
                if(giveAccess == false && (results.userOwnership == 'AP' || results.userOwnership == 'MLA' || (results.userOwnership == 'RPO' && results.usr.Title != 'Recruitment Sourcing Manager' && (component.get("v.runningUser") == 'Single Desk 1' || component.get("v.runningUser") == 'ATS_Recruiter')) || ((results.userOwnership == 'ONS' || results.userOwnership == newAerotek || results.userOwnership == newAstonCarter || results.userOwnership == newNewco) && results.usr && results.usr.Title && results.usr.Title.toLowerCase() == traineeUserTitle) )){  // || results.userOwnership == newAerotek || results.userOwnership == newAstonCarter || results.userOwnership == newNewco 
                    component.set("v.disableMerge", true);
                }else{
                    component.set("v.disableMerge", false);
                } 
 
				// Set United States by default based on user's country
				/*if (results.usr && results.usr.Country && results.usr.Country === 'United States') {
					component.set("v.filteringFields.Country",'United States');
					component.set("v.filteringFields.CountryKey",'US');
				} */
            } else {
                console.log('There was an error');
            }
        });
        $A.enqueueAction(action);

        
  		
	},
	
     // For count the selected checkboxes. 
     checkboxSelect: function(component, event, helper) {
     	// get the selected checkbox value  
      	var selectedRec = event.getSource().get("v.value");
      	// get the selectedCount attrbute value(default is 0) for add/less numbers. 
      	var getSelectedNumber = component.get("v.selectedCount");
      	// check, if selected checkbox value is true then increment getSelectedNumber with 1 
      	// else Decrement the getSelectedNumber with 1     
      	if (selectedRec == true) {
       		getSelectedNumber++;
      	} else {
       		getSelectedNumber--;
      	}
      	// set the actual value on selectedCount attribute to show on header part. 
  	  	component.set("v.selectedCount", getSelectedNumber);
 	},
    //For getting selected Talent Id and call Merge
    mergeTalent: function(component, event, helper) {
		helper.logAction(component,'MergeTalent');
    	var getSelectedNumber = component.get("v.selectedCount");
        if(getSelectedNumber == 2){        
        	// create var for store record id's for selected checkboxes  
            var selectedTalentId = [];
            var firstTalentId = '';
            var secondTalentId = '';
            // get all checkboxes 
            var getAllId = component.find("boxPack");
            // If the local ID is unique[in single record case], find() returns the component. not array
            var recordCount = 1;
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    if(recordCount == 1){
                        firstTalentId = getAllId[i].get("v.text");
                        recordCount++;
                    }    
                    else{
                        secondTalentId = getAllId[i].get("v.text");
                    }
                }
            }
            // call the helper function and pass all selected Talent id's.    
            helper.mergeTalentRecord(component, event, firstTalentId, secondTalentId);
        }
        else{
            if(getSelectedNumber < 2)
            	helper.showToast($A.get("$Label.c.FIND_ADD_Merge_Error"));
            else
                helper.showToast($A.get("$Label.c.FIND_ADD_SelectMerge_Error"));
        }    
     },
	/*setFieldValue: function(component,event,helper){
        var data = component.get("v.filteringFields");
        for(var p in data){
            //console.log('property:'+p+': val'+data[p]);
            if(p==0){
                for(var k in data[p]){
                    //console.log('inner property k:'+k+' val:'+data[p][k]);
                }
            }
        }
	},*/

	toggleSearch : function(component, event, helper){
		var isCollapsed = component.get("v.searchToggle");
		component.set("v.searchToggle", !isCollapsed);
	}, 

	searchOnEnter : function(component, event, helper) {
	 	//console.log(event.getParams().keyCode);
         if (event.getParams().keyCode === 13) {
            	component.searchHandler();
         }
	},

	chooseUpload : function(component, event, helper) {
        component.set("v.isCreateDocument", false);//sandeep
		component.set("v.mode", "upload");
        component.set("v.noResults", false);
		helper.createUncommittedTalent(component);
	},

 	openModalDragDrop : function(component) {
         var urlEvent = $A.get("e.c:E_DragDropModal_POC");
      	 urlEvent.fire();
    },
    
    setModeForClose : function(component, event, helper) {
		component.set("v.mode", "");
		component.set("v.talentId",undefined);
	},

	handleUploadParseComplete : function(component, event, helper) {
        var contact = event.getParam("contact");
		var accountId = event.getParam("accountId");
        var duplicateResults = event.getParam("dupeResults");
        var talentDocId = event.getParam("talentDocId");
		component.set("v.talentDocumentId",talentDocId);
		component.set("v.tempAccountId",accountId);
		component.set("v.tempContactId",JSON.stringify(contact.contactid));
        helper.updateTalentId(component, accountId);
        component.set("v.mode", "upload");
        if(duplicateResults){
        	// duplicate result from Talent document modal
        	helper.initSearchStringUpload(component, contact);
            component.set("v.isResumeParsed", "Y");
			component.set("v.isVisible", true);
            if(duplicateResults !=='No_Result_Found'){
            	component.set('v.dupeResults', duplicateResults);    
            }else{
                
                helper.updateDataTable(component,'[]');
            }
			
			
        }	
        else if (contact) {
			
            helper.initSearchStringUpload(component, contact);
			component.set("v.isResumeParsed", "Y");
            //component.searchHandler();			
            helper.updateDataTable(component,'[]');// it will redirect to talent record
			
		} else {
			helper.showError ($A.get("$Label.c.FIND_ADD_TALENT_ParseDocument"));
			component.set("v.isResumeParsed", "N");
		}	
		
		//helper.showComponent(component);
		helper.enableAddButton(component);
		
	},
	

	searchHandler : function(component, event, helper) {
		var valid = helper.validateInput(component,event);
		component.set('v.dupeResults', null)
		var searchText = "";
		var phone = component.get("v.filteringFields").Phone;
		var email = component.get("v.filteringFields").Preferred_Email__c;
		var fname = component.get("v.filteringFields").FirstName;
		var lname = component.get("v.filteringFields").LastName;
		var talentId = component.get("v.filteringFields").Talent_Id__c;
		
		
		if(valid){
			if(talentId === null || talentId === ""){
				if((phone !="" && phone!=null)||(email!="" && email!=null)||(fname!="" && fname!=null)||(lname!="" && lname!=null)){
					/*if(phone !="" && phone!=null){
						phone = phone.replace(/[\s\-\(\)\+]/g,'');
						searchText = phone;	
					}
					else if(email!="" && email!=null){
						email = email.replace(/[\s]/g,'');
						searchText = email;
					}
					else {
						searchText = fname+" "+lname;
					}*/

					if(phone !="" && phone!=null){
						phone = phone.replace(/[\s\-\(\)\+]/g,'');
						searchText = '(' + phone + ')';
					}
					if(email!="" && email!=null){
						email = email.replace(/[\s]/g,'');
						if(searchText.length > 0){
							searchText += ' OR ';
						}
						searchText += '(' + email + ')';
					}
					if (searchText=="" ||  searchText==null) {
						searchText = fname+" "+lname;
					}
					
					searchText = searchText.replace(/^\s+|\s+$/g, ""); //Remove leading and trailing spaces.
					
					component.set("v.searchText",searchText);
					//SOSL search - Phone,Email and Names
					helper.queryTalent(component,searchText, true);
				}
				else{
					//direct soql based on other fields.
					helper.queryTalentWithSOQL(component,true);
				}
			}
			else{
				//direct soql based on other fields.
				helper.queryTalentWithSOQL(component,true);
			}
			

			helper.enableAddButton(component);
			helper.showComponent(component);
			
		}
		else{
			//user needs to fix validation errors and proceed.
		}

	},

	addTalent : function(component, event, helper) {
        console.log('addTalent Method');
        helper.logAction(component,'addNewTalent');
		var conId = component.get("v.clientContactId");
		if (conId!== "" && conId!=null) {
			helper.addAndRelateTalent(component);
		} else {
			helper.navigateToAddEditCandidate(component, event);
		}
	},

	linkRelatedContact : function(component, event, helper) {
		helper.linkRelatedContact(component, event.getSource().get("v.value"));
	},

	applySort : function(component, event, helper) {
		component.set("v.sortField", event.getParam("sortField"));
		component.set("v.sortAsc", event.getParam("sortAsc"));
		component.searchHandler();
	},

	/*scrollSearch : function(component, event, helper) {
		component.set("v.pageSize", event.getParam("pageSize"));
		component.set("v.offset", event.getParam("offset"));
		component.searchHandler();
	},

	updatePageSize : function(component, event, helper) {
		component.set("v.pageSize", event.getParam("pageSize"));
		component.set("v.offset", 0);
		component.searchHandler();
	},*/

	refreshSearch: function(component, event, helper) {
		helper.clearSearchFields(component, event);
		component.set("v.isVisible", false);
        
	},

	showCreateDocumentModal : function(component, event, helper) { 
		component.set("v.isCreateDocument", true);
        helper.createUncommittedTalent(component);
    },
	uploadResume: function (cmp, e, h) {
		//Call Helper
		h.uploadResume(cmp);
	},
	proceedUpload: function (cmp, e, h) {
		h.proceedUpload(cmp, e);
	},
	invokeDupeMerge: function (cmp, e, h) {
		h.logAction(cmp,'MergeTalent');
		h.invokeDupeMerge(cmp, e);
	},
	saveAndUpdate: function(cmp, e, h) {
		h.saveAndUpdate(cmp);
		//h.showCompareModal(cmp, null, "Test", "Test"); 
		
	}
})