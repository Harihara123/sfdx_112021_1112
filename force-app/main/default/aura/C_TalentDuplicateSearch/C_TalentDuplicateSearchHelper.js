({
// new method for generating transaction id
	generateTransactionID : function(component) {
		var action = component.get("c.generateTransactionID");
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				var response = response.getReturnValue();
				component.set("v.transactionId",response);
                
			} else if (response.getState() === "ERROR") {
            	this.checkError(response);
        	}
		});
		$A.enqueueAction(action);
	},
    
    logAction : function(component,nxtAction){
	
		var action = component.get("c.logInformation");
		action.setParams({
            "methodName":"logInformation",
            "request": "",
            "result" : "",
			"nxtAction": nxtAction,
			"transactionId" : component.get("v.transactionId")
		});
		action.setCallback(this, function(response) {
			if (response.getState() === "ERROR") {
            	this.checkError(response);
				console.log('error while logging action');
        	}
		});
		$A.enqueueAction(action);
	
	
	},

	getParameterByName : function(name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        	results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
	addNewTalent : function(component) {
        
		var conId = component.get("v.clientContactId");
		if (conId!== "" && conId!=null) {
			this.addAndRelateTalent(component);
		} else {
			this.navigateToAddEditCandidate(component);
		}
	},
    mergeTalentRecord : function(component, event, firstTalentId, secondTalentId) {
       
	   var pageReference = {
			type: 'standard__component',
            attributes: {
                "componentName" : "c__C_TalentMergeComparison"
            },
            state: {
                "c__masterID": firstTalentId,
                "c__duplicateID": secondTalentId,
                "c__FromDuplicateComponent": "yes" //for story S-82099
            }
        };
		component.set("v.pageReference", pageReference);
		
        var navService = component.find("navService");
        var pageReference = component.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);

/*        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            //"url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Talent_Merge?masterID=" + firstTalentId + "&duplicateID=" + secondTalentId,
            "url": "/lightning/n/Talent_Merge?masterID=" + firstTalentId + "&duplicateID=" + secondTalentId,
            "isredirect":'true'
        });
        urlEvent.fire();
*/    },
    showToast : function(message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"),
            "message": message,
            type: "Error"
        });
        toastEvent.fire();
    },
    /*sendRequestToServer : function(component, action) {
    	component.set("v.showSpinner", true);
    	$A.enqueueAction(action);
    	component.set ("v.currentRequestId" , action.getId());
    },
    
	searchTalent : function(component, immediate) {
		var searchText = component.get("v.searchText");
		var tokenizedInput = this.tokenize(searchText, component.get("v.booleanOp"));

		if (tokenizedInput === "") {
			return;
		}
		var action = component.get("c.searchObject");
		action.setParams({
			"searchText": tokenizedInput,
			"searchObject": component.get("v.searchObject"),
			"sortField": component.get("v.sortField"),
			"sortAsc": component.get("v.sortAsc")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS" ) {
				if ((component.get("v.currentRequestId") === response.getId()) || immediate) {
					this.updateDataTable(component, response.getReturnValue());
				}
			} else {
				console.log("ERROR .... ");
			}
			component.set("v.showSpinner", false);
		});

		if (immediate) {
			component.set("v.showSpinner", true);
			$A.enqueueAction(action);
		} else {
			this.queueRequest(component, action);
		}
	},*/
	
	validateInput : function(component,event){
		var validated = true;
		
		var fname = component.find("FirstName");
		var lname = component.find("LastName");
		var pname = component.find("Preferred_Name__c");
		var phone = component.find("Phone");
		var email = component.find("Preferred_Email__c");
		var city  = component.find("MailingCity");
		var zip   = component.find("MailingPostalCode");
		var stateVal = component.get("v.filteringFields").MailingState;
		var talentId = component.find("Talent_Id__c");
		
		var fnameEmpty = $A.util.isEmpty(fname.get("v.value"));
		var lnameEmpty = $A.util.isEmpty(lname.get("v.value"));
		var pnameEmpty = $A.util.isEmpty(pname.get("v.value"));
		var emailEmpty = $A.util.isEmpty(email.get("v.value"));
		var phoneEmpty = $A.util.isEmpty(phone.get("v.value"));
		var cityEmpty  = $A.util.isEmpty(city.get("v.value"));
		var zipEmpty   = $A.util.isEmpty(zip.get("v.value"));		
		var stateEmpty = $A.util.isEmpty(stateVal);
		var talentIdEmpty = $A.util.isEmpty(talentId.get("v.value"));
		
		var fnameVal = fname.get("v.value");
		var lnameVal = lname.get("v.value");
		var pnameVal = pname.get("v.value");
		var cityVal  = city.get("v.value");
		var zipVal   = zip.get("v.value");
		var talentIdVal = talentId.get("v.value");

		if(talentIdEmpty){

			if (fnameEmpty && lnameEmpty && pnameEmpty && emailEmpty && phoneEmpty && talentIdEmpty && cityEmpty && stateEmpty && zipEmpty){
				validated = false;
	        	var errorMessage = $A.get("$Label.c.ADD_NEW_TALENT_ERROR_SEARCH_MESSAGE");
	            this.showError(errorMessage,$A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"));
			}
		
			else if(!fnameEmpty && fnameVal.length <2){
				validated = false;
				var errorMessage = $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_FIRSTNAME");
	    		this.showError(errorMessage,$A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"));
	    	}

	    	else if(!lnameEmpty && lnameVal.length <2){
				validated = false;
				var errorMessage = $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_LASTNAME");
	    		this.showError(errorMessage,$A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"));
	    	}

	    	else if(!pnameEmpty && pnameVal.length <2){
	    		validated = false;
	        	var errorMessage = $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_PREFERREDNAME");
	            this.showError(errorMessage,$A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"));
	    	}

	    	else if(!zipEmpty && zipVal.length <2){
	    		validated = false;
	        	var errorMessage = $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_ZIPCODE");
	            this.showError(errorMessage,$A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"));
	    	}

			else if(!cityEmpty && cityVal.length <2){
				validated = false;
	        	var errorMessage = $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_CITY");
	            this.showError(errorMessage,$A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"));
			}

			else if((!cityEmpty || !stateEmpty) && (fnameEmpty && lnameEmpty && phoneEmpty && emailEmpty)){
				validated = false;
	        	var errorMessage = $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_ALL");
	            this.showError(errorMessage,$A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MSG"));
			}

			else {
				//Phone Validation
				if(!phoneEmpty){ 
					var phoneVal = phone.get("v.value");
					//var phoneRegEx = new RegExp('(^([0-9]){10}$)|(^\\(([0-9]){3}\\)( ){0,1}([0-9]){3}\\-([0-9]){4}$)|(^([0-9]){3}([\\ .-])([0-9]){3}([\\ .-])([0-9]){4}$)');
					phoneVal = phoneVal.replace(/[\s\-\(\)\+]/g,'');
					var phoneRegEx = new RegExp("^[0-9-.+()/]+$");
					var valPh = phoneRegEx.test(phoneVal);
					if(valPh){
						phone.set("v.errors",null);
						phone.set("v.isError",false);
	                	$A.util.removeClass(phone,"slds-has-error show-error-message");					
					}
					else{
						validated=false;
						phone.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_PHONE")}]);
						phone.set("v.isError",true);
		                //$A.util.addClass(phone,"slds-has-error show-error-message");
		                //this.showError("Please enter a valid phone number.","Invalid Phone Number.");
		            }
				}
				else{
					phone.set("v.errors",null);
				}
				
				//Zip Validation
				var zipcode = component.find('MailingPostalCode');
				var zip 	= zipcode.get("v.value");
				if(!$A.util.isEmpty(zip)){
				//	var regex = /^[a-zA-Z0-9\s]+$/;//^\d{5}$/;
					var regex = /^[a-zA-Z0-9\s\-]+$/; // for story S-S-82222
					zip = zip.replace(/^\s+|\s+$/g, "");
					if(regex.test(zip)){
						zipcode.set("v.errors",null);
						zipcode.set("v.isError",false);
						$A.util.removeClass(zipcode,"slds-has-error show-error-message");	
					}	
					else{
						zipcode.set("v.errors",[{message : $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_POSTALCODE")}]);
						zipcode.set("v.isError",true);
						validated = false;
		                $A.util.addClass(zipcode,"slds-has-error show-error-message");
		                this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_POSTAL_CODE"),"Invalid Postal Code.");
		            }
				}
				else{
					zipcode.set("v.errors",null);
				}
	        	
	        	//Email Validation
				if(!emailEmpty){
					var emailVal = email.get("v.value");
					emailVal = emailVal.replace(/[\s]/g,'');
					var emailRegEx = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
					var valEmail = emailRegEx.test(emailVal);
					if(valEmail){
						email.set("v.errors",null);
						email.set("v.isError",false);
	                	$A.util.removeClass(email,"slds-has-error show-error-message");					
					}
					else{
						validated=false;
						email.set("v.errors", [{message: $A.get("$Label.c.FIND_ADD_NEW_TALENT_ERROR_MESSAGE_EMAIL")}]);
						email.set("v.isError",true);
		                //$A.util.addClass(email,"slds-has-error show-error-message");
		                //this.showError("Please enter a valid Email.","Invalid Email.");
		            }
				}

	        	/*fname.set("v.errors", null);
	        	lname.set("v.errors", null);
	        	talentId.set("v.errors",null);*/
	        }
		}

		else {
			validated=true;
		}

		
		return validated;
	},
	
	clearSearchFields: function(component, event){
		var fname = component.find("FirstName");
		var lname = component.find("LastName");
		var pname = component.find("Preferred_Name__c");
		var phone = component.find("Phone");
		var email = component.find("Preferred_Email__c");
		var city  = component.find("MailingCity");
		var zip   = component.find("MailingPostalCode");
		var talentId = component.find("Talent_Id__c");
		var stateVal = component.get("v.filteringFields").MailingState;
		var countryVal = component.get("v.filteringFields").Country;
		
		fname.set("v.value", '');
		lname.set("v.value", '');
		pname.set("v.value", '');
		phone.set("v.value", '');
		email.set("v.value", '');
		city.set("v.value", '');
		zip.set("v.value", '');
		talentId.set("v.value", '');
		component.set("v.filteringFields.MailingState",'');
		component.set("v.filteringFields.Country",'');
        component.set("v.mode", "");
		component.set("v.talentId",undefined);
	},
	
	queryTalent : function(component,searchText,immediate) {
		var filteringFields = component.get("v.filteringFields");
		var fname = filteringFields.FirstName;
		var lname = filteringFields.LastName;
		var pname = filteringFields.Preferred_Name__c;
		var phone = filteringFields.Phone;
		var email = filteringFields.Preferred_Email__c;
		var talentId = filteringFields.Talent_Id__c;
		
		if ((phone !='' && phone!=null) || (email!='' && email != null )) {
			var action = component.get("c.searchObjectByPhoneEmailName"); 
		} else if((fname!=''&&fname!=null)||(lname!=''&&lname!=null)){
			var action = component.get("c.searchObjectByName"); 
		} else{
			var action = component.get("c.queryObject"); 
		}
		/*if(phone !=''&& phone!=null){
			var action = component.get("c.searchObjectByPhone"); 
		}
		else if(email!='' && email != null){
			var action = component.get("c.searchObjectByEmail"); 
		}
		else if((fname!=''&&fname!=null)||(lname!=''&&lname!=null)){
			var action = component.get("c.searchObjectByName"); 
		}
		else{
			var action = component.get("c.queryObject"); 
		}*/
		//var booleanOp = component.get("v.booleanOp");
		//console.log('searchText::'+searchText);
		//Removing tokenization
		//var tokenizedInput = this.tokenize(searchText, booleanOp);
		
		var whereClause = this.buildWhereClause(component);

		action.setParams({
			//"searchText": tokenizedInput,
			"searchText": searchText,
			"whereClause": whereClause,
			"searchObject": component.get("v.searchObject"),
			"sortField": component.get("v.sortField"),
            "contactRecordType" : 'Talent',
			"sortAsc": component.get("v.sortAsc"),
			"transactionId" : component.get("v.transactionId")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS" ) {
				this.updateDataTable(component, response.getReturnValue());
			} else {
				// console.log("ERROR .... ");
			}
			component.set("v.showSpinner", false);
		});

		if (immediate) {
			component.set("v.showSpinner", true);
			$A.enqueueAction(action);
		} else {
			//this.queueRequest(component, action);
		}
	},
	
	buildWhereClause: function(component){
		var data = component.get("v.filteringFields");
		var whereClause = '';
		for(var key in data){
			if(key=="Talent_Id__c"){
				if(data[key] && data[key] !== "") {
					if(data[key].substring(0,1) === "C"){  //S-90416 Search with People Soft Id - Karthik
						whereClause = 'Talent_Id__c='+'\''+data.Talent_Id__c+'\'';							
					} else {
						whereClause = '(Peoplesoft_ID__c='+'\'R.'+data.Talent_Id__c+'\''+' OR Peoplesoft_ID__c='+'\''+data.Talent_Id__c+'\')';
					}
				}
			}
			else if(key=="MailingCity"){
				if(whereClause.length<1){
					if(data[key]!='' && data[key]!=null){//This is needed when user inputs some value and removes it later.
						whereClause = '(MailingCity='+'\''+data[key]+'\' OR OtherCity='+'\''+data[key]+'\')';							
					}
				}
				else{
					if(data[key]!='' && data[key]!=null){
						whereClause = whereClause+' AND (MailingCity='+'\''+data[key]+'\' OR OtherCity='+'\''+data[key]+'\')';	
					}
				}
			}
			else if(key=="MailingStateKey"){
				var stateName = data.MailingState;
				if(whereClause.length<1){
					if(stateName!='' && stateName!=null){//This is needed when user inputs some value and removes it later.
						whereClause = '(MailingState='+'\''+stateName+'\' OR OtherState='+'\''+stateName+'\')';							
					}
				}
				else{
					if(stateName!='' && stateName!=null){
						whereClause = whereClause+' AND (MailingState='+'\''+stateName+'\' OR OtherState='+'\''+stateName+'\')';	
					}
				}
				// alert(whereClause);

			}
			else if(key=="CountryKey"){
				var countryName = data.CountryKey;
				//var country = data.Country;
				var countryList =  component.get("v.countriesMap");

				if(whereClause.length<1){
					if(countryName!='' && countryName!=null){
						whereClause = '(MailingCountry ='+'\''+countryList[countryName]+'\' OR MailingCountry='+'\''+data.Country+'\')';							
					}
				}
				else{
					if(countryName!='' && countryName!=null){
						whereClause = whereClause+' AND (MailingCountry='+'\''+countryList[countryName]+'\' OR MailingCountry='+'\''+data.Country+'\')';	
					}
				}
				
			}

			else if(key=="MailingPostalCode"){
				if(whereClause.length<1){
					if(data[key]!='' && data[key]!=null){//This is needed when user inputs some value and removes it later.
						whereClause = '(MailingPostalCode='+'\''+data[key]+'\' OR OtherPostalCode='+'\''+data[key]+'\')';							
					}
				}
				else{
					if(data[key]!='' && data[key]!=null){
						whereClause = whereClause+' AND (MailingPostalCode='+'\''+data[key]+'\' OR OtherPostalCode='+'\''+data[key]+'\')';	
					}
				}	
			}
			else if(key=="Preferred_Name__c"){
				if(whereClause.length<1){
					if(data[key]!='' && data[key]!=null){//This is needed when user inputs some value and removes it later.
						whereClause = '(Preferred_Name__c='+'\''+data[key]+'\')';							
					}
				}
				else{
					if(data[key]!='' && data[key]!=null){
						whereClause = whereClause+' AND (Preferred_Name__c='+'\''+data[key]+'\')';	
					}
				}	
			}
			else{
			}
		
		}
		//console.log('whereClause::'+whereClause);	
		return whereClause;
	},
	
	queryTalentWithSOQL : function(component, immediate) {
		var data = component.get("v.filteringFields");
		var whereClause = this.buildWhereClause(component);
	
		var action = component.get("c.queryObject");
		action.setParams({
			"searchText": "",
			"whereClause": whereClause,
			"searchObject": component.get("v.searchObject"),
			"sortField": component.get("v.sortField"),
            "contactRecordType" : "Talent",
			"sortAsc": component.get("v.sortAsc"),
			"transactionId" : component.get("v.transactionId")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			var result = response.getReturnValue();
			//console.log('AKR::'+JSON.stringify(result));
			if (result =="" || result == null){
				result = "[]";
			}
			if (state === "SUCCESS" ) {
				this.updateDataTable(component, result);
			} else {
				// console.log("ERROR .... ");
			}
			component.set("v.showSpinner", false);
		});
		
		if (immediate) {
			component.set("v.showSpinner", true);
			$A.enqueueAction(action);
		} 
		else {
			this.queueRequest(component, action);
		}	
	},
		
	tokenizeSearchText : function(component) {
		var st = component.get("v.searchText");
		var o = [];

		// Split at whitespaces 
		var spl = st.split(/\s+/);
		for (var i=0; i<spl.length; i++) {
			// trim, uppercase, and and asterisk if not a boolean operator
			var a = spl[i].trim().toUpperCase();
		  	if (!(a === "AND" || a === "OR" || a === "NOT")) {
		  		a = "\"" + a + "*\"";
		  	}
		  	o.push(a);
		}

		return o.join(" ");
	},

	updateDataTable : function(component, contactList) {
        var tabledata = this.addDisplayFields(JSON.parse(contactList));
        
		component.set("v.dataTable", tabledata);
		//console.log(component.get("v.dataTable.length"));
		if (component.get("v.dataTable.length") === undefined || component.get("v.dataTable.length") === 0) {
			component.set("v.noResults", true);
            if(component.get("v.mode") === "upload"){
            	this.addNewTalent(component);
            }    
		} else {
			component.set("v.noResults", false);
			this.enableAddButton(component);
		}
	},

	addDisplayFields : function(contactList) {		
		if (contactList !== undefined && contactList !== null) {
			for (var i=0; i<contactList.length; i++) {
				contactList[i].displayPhone = this.getDisplayPhone(contactList[i]);
				contactList[i].displayEmail = this.getDisplayEmail(contactList[i]);
				contactList[i].displayLocation 
					= this.getDisplayLocation({
						MailingCity: contactList[i].MailingCity,
						MailingState: contactList[i].MailingState,
						MailingCountry: contactList[i].MailingCountry,
						MailingPostalCode: contactList[i].MailingPostalCode,
						FormattedAddress: contactList[i].Mailing_Address_Formatted__c
					});
				contactList[i].lastModifiedDate = contactList[i].Account.Talent_Profile_Last_Modified_Date__c;
			}
		}
		return contactList;
	},

	getDisplayPhone : function(contact) {			
		var ph = "";
		var mobilePhone = this.getTelLinkHtml(contact.MobilePhone, contact.MobilePhone, "(M)");
		var homePhone = this.getTelLinkHtml(contact.HomePhone, contact.HomePhone, "(H)");
		var phoneExtension = contact.WorkExtension__c ? ` (${contact.WorkExtension__c})` : ``;
		var phone = this.getTelLinkHtml(contact.Phone, contact.Phone + phoneExtension, "(W)") + `<span class="text-color_light-grey">${phoneExtension}</span>`;
        var otherPhone = this.getTelLinkHtml(contact.OtherPhone, contact.OtherPhone, "(O)");
		ph = mobilePhone;
		ph += mobilePhone.length > 0 ? "<br/>" : "";
		ph += homePhone;
		ph += homePhone.length > 0 ? "<br/>" : "";
        ph += phone;
		ph += phone.length > 0 ? "<br/>" : "";
		ph += otherPhone;
		return ph;
	},
	
	//D-05950 - Added by Karthik
	getTelLinkHtml : function(phone, title, prefix) {	
		if(phone !== null && phone !== undefined){
			var phone = phone.replace(/\s/g,'');
			
			return `${prefix} <a title="${title}" href="tel://${phone}">${phone}</a>`;
			
			
			
		}
		//return phone !== undefined && phone !== "" ? prefix + " <a href='tel://" + phone + "'>" + phone + "</a>" : "";
		else{
			return "";
		}
	},
	//D-05950 - End

	getDisplayEmail : function(contact) {	
		var eml = this.getEmailLinkHtml(contact.Email);
		eml += eml.length > 0 ? "<br/>" : "";
		eml += this.getEmailLinkHtml(contact.Other_Email__c);
		eml += eml.length > 0 ? "<br/>" : "";
		eml += this.getEmailLinkHtml(contact.Work_Email__c);
		return eml;
	},
	
	getEmailLinkHtml : function(email) {	
		return email !== undefined && email !== "" ? "<a href='mailto://" + email + "'>" + email + "</a>" : "";
	},

	getDisplayLocation : function(loc) {
		let USschema = ['MailingCity', 'MailingState', 'MailingPostalCode'];
		let nonUSSchema = ['MailingCity', 'MailingCountry', 'MailingPostalCode'];
		let locArray = [];
		let addr;
		if (loc.MailingCountry === 'USA') {
			for(let item of USschema) {
					if (loc[item]){
					locArray.push({
						value: loc[item],
						id: item				
					})
				}
			}			
		} else {
			for(let item of nonUSSchema) {
				if(loc[item]) {
				locArray.push({
					value: loc[item],
					id: item				
				})}
			}			
		}
		if (locArray.length === 0 && loc.FormattedAddress) {
			locArray[0] = loc.FormattedAddress;
		} 
		
		//var hasCity = city !== undefined && city !== "";
		//var hasState = state !== undefined && state !== "";
		//var addr = "";
		//if (hasCity && hasState) {
		//	addr = city + ", " + state;
		//} else if (hasCity) {
		//	addr = city;
		//} else if (hasState) {
		//	addr = state;
		//} 

		return locArray;
	},

	updateTalentId : function(component, accountId) {		
		component.set("v.talentId", accountId);
	},

	initSearchStringUpload : function(component, contact) {	
		if (contact !== undefined && contact !== null) {
			this.addParsedFields(component,contact);
		}
	},
	
	addParsedFields: function(component, contact){	
		//console.log('contact::'+JSON.stringify(contact));
		var filteringFields = component.get("v.filteringFields");
		
		filteringFields.FirstName 			= contact.firstname;
		filteringFields.LastName 			= contact.lastname;
		//filteringFields.MailingCity  		= contact.city;
		//filteringFields.Country             = contact.country;
		//filteringFields.MailingState 		= contact.state;
		//filteringFields.MailingPostalCode 	= contact.postalcode;
		filteringFields.Phone 				= (contact.mobilephone ==""||contact.mobilephone==null)?((contact.homephone==""||contact.homephone==null)?contact.workphone:contact.homephone):contact.mobilephone;
		filteringFields.Preferred_Email__c 	= (contact.email ==""||contact.email==null)?contact.bestemail:contact.email;
		
		component.set("v.filteringFields",filteringFields);
	},
	
	addParsedFieldsFromClientCon: function(component, contact){	
		//console.log('clientContact::'+JSON.stringify(contact));
		var filteringFields = component.get("v.filteringFields");
		
		filteringFields.FirstName 			= contact.FirstName;
		filteringFields.LastName 			= contact.LastName;
		filteringFields.Phone 				= (contact.MobilePhone ==""||contact.MobilePhone==null)?((contact.HomePhone==""||contact.HomePhone==null)?contact.OtherPhone:contact.HomePhone):contact.MobilePhone;
		filteringFields.Preferred_Email__c 	= (contact.Email ==""||contact.Email==null)?contact.Other_Email__c:contact.Email;
		filteringFields.MailingCity  		= contact.MailingCity;
		filteringFields.Country             = contact.MailingCountry;
		filteringFields.MailingState 		= contact.MailingState;
		filteringFields.MailingPostalCode 	= contact.MailingPostalCode;
		
		component.set("v.filteringFields",filteringFields);
	},
	
	createSearchString : function(contact, flow) {
		var searchStr = "";
		if (flow === "upload") {
			searchStr += contact.firstname;
			if (contact.lastname !== null && contact.lastname !== "") {
				searchStr += " AND " + contact.lastname;
			}
			if (contact.bestemail || contact.bestphone) {
				searchStr += " AND (";
			}
			if (contact.bestemail && contact.bestphone) {
				searchStr += contact.bestemail + " OR " + contact.bestphone;
			} else if (contact.bestemail) {
				searchStr += contact.bestemail;
			} else if (contact.bestphone) {
				searchStr += contact.bestphone;
			}
			if (contact.bestemail || contact.bestphone) {
				searchStr += ")";
			}
		}else if (flow === "client") {
			searchStr += contact.FirstName;
			if (contact.LastName !== null && contact.LastName !== "") {
				searchStr += " AND " + contact.LastName;
			}
			if (contact.MailingCity || contact.MailingState) {
				searchStr += " AND (";
			}
			if (contact.MailingCity && contact.MailingState) {
				searchStr += contact.MailingCity + " OR " + contact.MailingState;
			} else if (contact.MailingCity) {
				searchStr += contact.MailingCity;
			} else if (contact.MailingState) {
				searchStr += contact.MailingState;
			}
			if (contact.MailingCity || contact.MailingState) {
				searchStr += ")";
			}
		}
		return searchStr;
	},

	showComponent : function(component) {	
		component.set("v.isVisible", true);
	},
	postTalentCreateAction : function(component) {		
		var theVal = component.get("v.addTrigger");
        // console.log('talent id---',component.get("v.talentId"));
        component.set("v.addTrigger", !theVal);
		this.fireInitEvent(component);
	},
	
	fireInitEvent : function(component) {	
		var evt;
		if (component.get("v.mode") === "search") {
			evt = $A.get("e.c:E_TalentSearchCreateInit");
			if (component.get("v.clientContactId") !== "") {
				evt.setParams({
					"clientContactId" : component.get("v.clientContactId")
				});
			}
		} else if (component.get("v.mode") === "upload") {
			evt = $A.get("e.c:E_TalentUploadCreateInit");
		} else {
			// Failover. Do nothing if mode is not in the expected values.
			return;
		}

		evt.fire();
	},
	
	createUncommittedTalent : function(component, postCreateAction) {		
		var action = component.get("c.createUncommittedTalent");

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.talentId", response.getReturnValue());
				if(component.get("v.isCreateDocument") === true) { //S-115302 - Neel
					this.showCreateDocumentModal(component);
				}
				else {
					this.postTalentCreateAction(component);
				}
			} else {
				// console.log("ERROR .... ");
			}
		})

		$A.enqueueAction(action);
	},
	enableAddButton : function(component) {					
		component.set("v.addDisabled", false);
	},

	navigateToAddEditCandidate : function(component, event) {							
        //Sandeep: Navigate to component
        /*var evt = $A.get("e.force:navigateToComponent");
            
        evt.setParams({
            componentDef : "c:C_TalentAddEdit",
            componentAttributes: {
                mode  : component.get("v.mode"),
                accountId :component.get("v.talentId"),
                isResumeParsed: component.get("v.isResumeParsed"),
                getParamsFromURL : false, 
                filteringFields : component.get("v.filteringFields")
            }
        });
        evt.fire();*/

		//S-85004 - URL isAddressable - Karthik
        var pageReference = {
			type: 'standard__component',
            attributes: {
                "componentName" : "c__C_TalentAddEditNew"
				//"componentName" : "c__C_TalentAddEdit"
            },
            state: {
                "c__mode": component.get("v.mode"),
                "c__accountId": component.get("v.talentId"),
                "c__isResumeParsed": component.get("v.isResumeParsed"),
                //"c__getParamsFromURL": false,
                "c__filteringFields": component.get("v.filteringFields")  
            }
        };
		component.set("v.pageReference", pageReference);
		
        var navService = component.find("navService");
        var pageReference = component.get("v.pageReference");
        //event.preventDefault();
        navService.navigate(pageReference);		
		this.delayedRefresh();
		//S-85004

	},
	
	getClientContactData : function(component,clientContactId) {									
		var action = component.get("c.getClientContactData");
		action.setParams({
			"clientContactId": clientContactId
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var contactVal = JSON.parse(response.getReturnValue());
				this.initSearchStringClient(component, JSON.parse(response.getReturnValue()));
				
				component.searchHandler();
				
				this.enableAddButton(component);
			} else {
				// console.log("ERROR .... ");
			}
		});

		$A.enqueueAction(action);
	},

	initSearchStringClient: function(component, contact) {
		//console.log('AKR::'+JSON.stringify(contact));
		if (contact !== undefined && contact !== null) {
			this.addParsedFieldsFromClientCon(component,contact);
		}
	},

	addAndRelateTalent : function(component) {													
		var action = component.get("c.addAndRelateTalent");
		action.setParams({
			"clientContactId": component.get("v.clientContactId")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				this.createTalentExperience(component, response);
				this.navigateToClientContact(component);
			} else if (state === "ERROR") {
            	// console.log ("Error....Failed to save Account and Contact");
				this.checkError(response);
        	}
			component.set("v.showSpinner", false);
		});

		$A.enqueueAction(action);
		component.set("v.showSpinner", true);
	},

    createTalentExperience : function(component, response) {													    	
        var actionTE = component.get("c.createTalentExperience"); 
        actionTE.setParams({
			"clientContact": response.getReturnValue()[0],
			"talentContact": response.getReturnValue()[1]
		});
        actionTE.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
            } else if (response.getState() === "ERROR") {
            	// console.log ("Error....Failed to save Talent Experience");
				this.checkError(response);
        	}
        });
        $A.enqueueAction(actionTE);
    },

	linkRelatedContact : function(component, talentContactId) {													    			
		// console.log("Linking ... helper");
		var action = component.get("c.relateTalent");
		action.setParams({
			"clientContactId": component.get("v.clientContactId"),
			"talentContactId": talentContactId
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				this.navigateToClientContact(component);
			} else {
				// console.log("ERROR .... ");
			}
			component.set("v.showSpinner", false);
		});

		$A.enqueueAction(action);
		component.set("v.showSpinner", true);
	},

	navigateToClientContact : function(component) {		
        var sObjEvent = $A.get("e.force:navigateToSObject");
        var talentId = component.get("v.clientContactId");
        sObjEvent.setParams({
          	"recordId": component.get("v.clientContactId")
        });
        sObjEvent.fire();
	},

	/************ Boolean validation and tokenization code *********/
	REGEX_MULTI_SPACE : "\\s+",
	REGEX_EMPTY_QUOTES : "\"\"+",
	REGEX_LEADING_OP : "^(and|or|and not)+\\s+",
	REGEX_TRAILING_OP : "\\s+(and|or|and not)+$",
	REGEX_SOSL_SP_CHARS : "\\?|&|!|{|}|[|]|\\^|~|:|\\\\|'|\\+|-",
	REGEX_ASTERISK_END : ".+\\*$",

	/**
	 * Function checks for special characters in the token and escapes with \. Following checks are done.
	 * 1) The token is a "TERM" type
	 * 2) The token contains matches the regex defined by SP_CHARS_REGEX.
	 * 
	 * @param token
	 * @param type
	 * @return
	 */
	escapeSpecialChar : function(ch) {			
		// Identifies special characters mentioned in the SOSL spec.
		SoslSpecialChars = new RegExp(this.REGEX_SOSL_SP_CHARS);
		return SoslSpecialChars.test(ch) ? "\\" + ch : ch;
	},

	/**
	 * Apply * at the end of a TERM token if not already so. Also enclose in quotes if not already.
	 * @param  {string} token - the token 
	 * @param  {string} type  - the type.
	 * @return {string}
	 */
	starizeToken : function(token, type) {						
		// Identifies terms with asterisk at the end
		var StarAtEnd = new RegExp(this.REGEX_ASTERISK_END);
		var val = token;
		if (type === "TERM") {
		  	if (StarAtEnd.test(token)) {
		    	val = "\"" + token + "\"";
		    } else if (token.charAt(0) !== "\"") {
		    	val = "\"" + token + "*\"";
		    }
		}
	  	return val;
	},

	/**
	 * Utility that checks whether a given input is a search operator,
	 * left parenthesis, right parenthesis, or a search term.
	 * Used for validation of boolean query syntax. 
	 * @param t - Input string (token) to check
	 * @return
	 */
	getType : function(t) {			
		// Determine token type
		t = t.toLowerCase();	
		if(this.isOperator(t)) return("OP")
		else if(t == "(") return("LP");
		else if(t == ")") return("RP");
		else return("TERM");
	},

	/**
	 * Verifies whether a given input is a boolean operator (and / or / and not).
	 * @param t - Input string (token) to check
	 * @return - True if input is a valid operator, else false
	 */
	isOperator : function(t) {			
		if (t != null || t != "") {
			t = this.trim(t.toLowerCase());	
			if (t === "and" || t === "or" || t === "and not") {
				return true;
			}
		}
		return false;
	}, 
		
	/**
	 * Strips leading and lagging whitespaces from a given
	 * input string.
	 * @param s - Input string to clean
	 * @return
	 */
	trim : function(s) {
	//RAJ adding var						
		var StripLeadingWhitespace = new RegExp("^[\\n\\t\\r ]+");
		var StripTrailingWhitespace = new RegExp("[\\n\\t\\r ]+$");
		// Strip leading and trailing whitespace characters
		return ( s ) ? s.replace(StripLeadingWhitespace, "").replace(StripTrailingWhitespace, "") : "";
	},

	/**
	 * Validate user-entered search text for common mistakes and convert it to a valid query.
	 * @param value -  The user's query terms.
	 * @param operator - The default operator to insert between adjacent search terms. Typically set to 'AND' or 'OR'.
	 * @return
	 */
	tokenize : function(value, operator) {									
		var valueOut = "";      // the "fixed" query
		var tokensIn = [];   	// a list of tokens parsed out of the query string
		var types = [];      	// type of each token: left parenthesis (LP), right parenthesis (RP), operator (OP), query term (TERM)
		var ti = 0;             // token index
		var parens = 0;         // parentheses nesting level
		var mismatch = false;   // mis-matched parentheses flag
		var quotes = 0;         // double quote nesting level
		var token = "";         // an extracted token
		var inphrase = false;   // a flag indicating that the tokenizer is parsing between quotation marks (a phrase)
		var ch = "";            // character from the query string
		var oldch = " ";        // previous character from the query string
		var errMsg = [];		// Stores the text for all error messages
		var badParens = 0;		// all below are error counts to prevent the same error from appearing multiple times
		var badQuotes = 0;
		var badEmptyParens = 0;
		var badLeftParen = 0;
		var badRightParen = 0;
		var badOps = 0;
		var termHasError = false; 	// Set to true if not valid.
		
		// strip multiple spaces from the query
		var CompactMultipleSpaces = new RegExp(this.REGEX_MULTI_SPACE, "g");
		// strip empty quotes
		var StripEmptyQuotes = new RegExp(this.REGEX_EMPTY_QUOTES, "g"); 
		//does query start with operator
		var LeadingOperator = new RegExp(this.REGEX_LEADING_OP, "i");
		//does query end with operator
		var TrailingOperator = new RegExp(this.REGEX_TRAILING_OP, "i");
		//var StarInMiddle = new RegExp(".+\\*.+");

		// Clean up query string
		var valueIn = this.trim(value.replace(CompactMultipleSpaces, " "));
		valueIn = valueIn.replace(StripEmptyQuotes, "");
		
		// Break query string into tokens and tag as one of 4 types: OP, TERM, LP or RP
		var len = valueIn.length;
		for(var c = 0; c < len; c++) {
			// ch = this.escapeSpecialChar(valueIn.charAt(c));
	    	ch = valueIn.charAt(c);

			// Perform some rudimentary checks for malformed parentheses and quotes, and inappropriate scope references
			if(ch == '(') parens++;
			else if(ch == ')') parens--;
			else if(ch == '"') quotes++;
			if(parens < 0) mismatch = true;

			// Delimit each token
			if(ch == "(" || ch == ")") {
				if(token != "") {
					types[ti] = this.getType(token);
	        		tokensIn[ti] = token;
					ti++;
					token = "";
				}
				tokensIn[ti] = ch;
				types[ti++] = this.getType(ch);
			} else if ((oldch == " " || oldch == "(" || oldch == ")" || oldch == ":") && !(ch == " ") && !inphrase) {	// leading edge of new token
				// Check for compound operator and not.
	    		if ((token+valueIn.substring(c, c+3)).toLowerCase() === "andnot") {
	      			token += " ";
	      		} else if(token != ""){
					types[ti] = this.getType(token);
	        		tokensIn[ti] = token;
					ti++;
					token = "";
				}
			}
			if(ch == '"'){	// detect when in a phrase
				if(inphrase) inphrase = false;
				else inphrase = true;
			}
			if(inphrase || !(ch == " " || ch == "(" || ch == ")")) token += ch;	// add non whitespace character to token

			oldch = ch;
		}
		// Finish final token
		//var finalToken = (token != null) ? trim(token.substring(token.indexOf(" "), token.length)) : "";
		if (token != "") {
			types[ti] = this.getType(token);
	    	tokensIn[ti] = token;
			ti++;
		}

		// Report format errors
		if (mismatch || parens != 0) {
			if(badParens < 1) 
			badParens++;
			termHasError = true;
		}
		if (quotes % 2 != 0) {
			if(badQuotes < 1) 
			badQuotes++;
			termHasError = true;
		}

		// Combine tokens by comparing adjacent tokens and applying rules
		if(tokensIn.length > 0) {
			// Output first token
			valueOut += this.starizeToken(tokensIn[0], types[0]);
		}

	  	var tokenLen = tokensIn.length;
		for(var t = 1; t < tokenLen; t++) {

			switch(types[t-1]) {   // First token of a pair
				case "LP":   // Left parenthesis
					switch(types[t]) {  // Second token of a pair
						case "LP":		valueOut += tokensIn[t]; break;
						case "RP":		
							if(badEmptyParens < 1) {
								// console.log("a.keyword.is.required.if.using.parentheses");
								termHasError = true;
							}
							badEmptyParens++;
							break;
						case "OP":
							if (badLeftParen < 1) {
								// console.log("boolean.operators.cannot.be.preceded.by.parentheses"); 
								termHasError = true;
							}
							badLeftParen++;
							break;
						case "TERM":
							valueOut += this.starizeToken(tokensIn[t], types[t]); 
							break;
					}
					break;
				case "RP":   // Right parenthesis
					switch(types[t]) {  // Second token of a pair
						case "LP":		valueOut += " " + operator + " " + tokensIn[t]; break;
						case "RP":		valueOut += tokensIn[t]; break;
						case "OP":		valueOut += " " + tokensIn[t]; break;
						case "TERM":	valueOut += " " + operator + " " + this.starizeToken(tokensIn[t], types[t]); break;
					}
					break;
				case "OP":   // Operator
					switch(types[t]) {  // Second token of a pair
						case "LP":		valueOut += " " + tokensIn[t]; break;
						case "RP":		
							if(badRightParen < 1) {
								// console.log("boolean.operators.cannot.be.followed.by.parentheses"); 
								termHasError = true;
							}
							badRightParen++;
							break;
						case "OP":		
							if(badOps < 1) {
								// console.log("boolean.operators.must.be.separated.by.terms"); 
								termHasError = true;
							}
							badOps++;
							break; 
						case "TERM":	
							valueOut += " " + this.starizeToken(tokensIn[t], types[t]); 
							break;
					}
					break;
				case "TERM":   // Query term
					switch(types[t]) {  // Second token of a pair
						case "LP":		valueOut += " " + operator + " " + tokensIn[t]; break;
						case "RP":		valueOut += tokensIn[t]; break;
						case "OP":		valueOut += " " + tokensIn[t]; break;
						case "TERM":	valueOut += " " + operator + " " + this.starizeToken(tokensIn[t], types[t]); break;
					}
					break;
			}
		}
		
		if(LeadingOperator.test(valueOut)) {
			// console.log("cannot.start.with.an.operator");
			termHasError = true;
		}
		if(TrailingOperator.test(valueOut)) {
			// console.log("cannot.end.with.an.operator");
			termHasError = true;
		}
		if(termHasError) {
			// console.log("Search term is invalid!");
			// this.fireErrorToast("Invalid search terms. Please check and correct.");
			valueOut = "";
		}
		//console.log(valueOut);

		return(valueOut);
	},


    checkError : function(response){								    	
       var errors = response.getError();
        if (errors) {
            // console.log("Errors", errors);
			if (errors[0] && errors[0].message) {
                this.showError(errors[0].message, 'An error occured!');
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,'An error occured!');
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,'An error occured!');
            }else{
                this.showError($A.get("$Label.c.FIND_ADD_TALENT_Unexpectederror"));
            }
        } else {
            this.showError(errors[0].message);
            //throw new Error("Unknown Error");
        }
    },


    showError : function(errorMessage, title){								    	    	
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
	/************ Boolean validation and tokenization code *********/


	 getCountryMappings : function(component) {   
        var action = component.get("c.getCountryMappings");
        
        action.setCallback(this,function(response) {
        	var state = response.getState();
			if (state === "SUCCESS" ) {
            	component.set("v.countriesMap",response.getReturnValue());
            	// console.log('xxxxxxxxxxxxxxxxxxx' + JSON.stringify( component.get("v.countriesMap") ) );
            }else{
            	console.log('ERROR while loading countries');
            }
        });

        $A.enqueueAction(action);
    },

	delayedRefresh : function(milliseconds){
		let ms = milliseconds || 100;
		window.setTimeout($A.getCallback(function(){
			$A.get('e.force:refreshView').fire();
		}),ms);
	},
	showCreateDocumentModal : function (component) {  //S-115302 - Neel
		 $A.createComponent(
			"c:C_CreateDocumentModal",
			{"accountId" : component.get("v.talentId"), "source" : "Find_Add_Talent","transactionId" : component.get("v.transactionId")},
			function(newComponent, status, errorMessage){
				if (status === "SUCCESS") {
					component.set('v.C_CreateDocumentModal', newComponent);
				}
				else if (status === "INCOMPLETE") {
				}
				else if (status === "ERROR") {
				}
			}
        );
	},
	uploadResume: function (cmp) {
		let getSelectedNumber = cmp.get("v.selectedCount");
		let selectedFromDupe;

		if (cmp.get('v.dupeResults')) {
			const dupeResults = cmp.find('tduperesults');
			selectedFromDupe = dupeResults.cID();
			if (selectedFromDupe) {
				getSelectedNumber = 1;
			}
		}

		if (getSelectedNumber === 1) {
			//INIT THE RESUME UPLOAD PROCESS
			let selectedRecord;
			const records = cmp.find("boxPack");

			if (cmp.get('v.dupeResults')) {
				cmp.set("v.tempSelectedRecord", selectedFromDupe);
			} else {
				if (Array.isArray(records)) {
					selectedRecord = records.find(record => record.get('v.value'));
					cmp.set("v.tempSelectedRecord",selectedRecord.get("v.text"));
				} else {
					selectedRecord = records;
					cmp.set("v.tempSelectedRecord",selectedRecord.get("v.text"));
				}
			}


			//DocCount for Selected Record


			this.getTalentResumeCount(cmp, selectedFromDupe? selectedFromDupe:selectedRecord.get("v.text"), (count) => {

				if (!count) {
					//SHOW TOAST HERE THAT ERROR HAPPENED
					this.showError('Selected talent does not exist. Please try again later.', 'Error')
					cmp.set("v.showSpinner",false);
				} else {
					if (count[0] < 5) {
						this.uploadResumeToTalent(cmp, selectedFromDupe?selectedFromDupe:selectedRecord.get("v.text"));
					} else {
						//..need to show document replace ID.
						this.handleShowModal(cmp, count[1], 'TalentDocument', true);
					}
				}
			})


			// var action = cmp.get("c.talentResumeCount");
			//
			// action.setParams({
			// 	"conId": selectedFromDupe? selectedFromDupe:selectedRecord.get("v.text"),
			// });
			// action.setCallback(this,function(response) {
			// 	var state = response.getState();
			// 	if (state === 'SUCCESS') {
			// 		const owner = response.getReturnValue();
			// 		if(owner){
            //             console.log('Doc owner: ', owner[0]);
			// 			if (owner[0] > 4) {
			// 				//let docModelevt= $A.get("e.c:E_DocumentModalOpen");
			// 				this.handleShowModal(cmp, owner[1], 'TalentDocument', true);
			// 				//console.log('docModelevt fired:enable save');
			// 			} else {
			// 				this.uploadResumeToTalent(cmp, selectedFromDupe?selectedFromDupe:selectedRecord.get("v.text"));
			// 			}
			// 			console.log('Doc owner: ', owner);
			//
			// 		}else{
			// 			this.showError('Please try again..!!','An error occured!');
			// 		}
			// 	}else{
			// 		this.showError('Please try again..!!','An error occured!');
			// 	}
			// });
			// $A.enqueueAction(action);

            //this.uploadResumeToTalent(selectedRecord.get("v.text"), cmp);
			//console.log('RESUME UPLOAD INIT FOR:', selectedRecord.get("v.text"));
		} else {
			//this.showError('Please select One talent for resume upload','Error');
		}
	},
    uploadResumeToTalent: function (cmp, selectedRecord,source) {
        console.log("uploadResumeToTalent : " );
        var tempaccountId = cmp.get("v.tempAccountId");
        var action = cmp.get("c.attachResumeToTalent");
        
        action.setParams({
            "accId": cmp.get("v.tempAccountId"),
            "conId": selectedRecord,
        });
        action.setCallback(this,function(response) {
            var state = response.getState();     
            if (state === 'SUCCESS') {
                var conId = response.getReturnValue();      
                if(conId){
                    if(!source && !cmp.get('v.uploadType')){
                        var sObjEvent = $A.get("e.force:navigateToSObject");
                        sObjEvent.setParams({
                            "recordId": conId
                        });
                            sObjEvent.fire();
                    } else {
                    	if (cmp.get('v.uploadType')) {
							cmp.set('v.uploadType', null)
							const talentDocId = cmp.get('v.talentDocumentId');
							this.getCompareData(cmp, talentDocId, null, (data) => {

								let names = cmp.get('v.names');
								this.showCompareModal(cmp, data, names.fname, names.lname, conId);
							});
						}
					}
                 }else{
                    this.showError('Unable to upload Please try again..!!','An error occured!');
                }
            }else{
                console.log('Error');
                this.showError('Unable to upload Please try again..!!','An error occured!');
            }
        });
        $A.enqueueAction(action);
    },
	handleShowModal: function(cmp, recordId, source, enableSave) {

		$A.createComponent(
			"c:C_TalentDocumentModal",
			{
				docData: cmp.getReference("v.docData"),
				recordId,
				sourceOfEvent: 'FAT_Upload',
				enableSave,
				defaultDocumentTypeSelectedValue: true
			},
			function(newComp, status, errorMessage){ 
				//Add the new button to the body array
				if (status === "SUCCESS") {
                    cmp.set("v.showSpinner",false);
					var modal = cmp.get("v.C_CreateDocumentModal");
					modal.push(newComp);
					cmp.set("v.C_CreateDocumentModal", modal);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
					// Show offline error
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
					// Show error message
				}
			}
		);

	},
	proceedUpload: function (cmp, e, h) {
        var docDeleteId = cmp.get('v.docData');
        var action = cmp.get("c.deleteDocument");
        cmp.set("v.showSpinner",true);
        action.setParams({
            "replaceResumeId": docDeleteId,
        });
        action.setCallback(this,function(response) {
            var state = response.getState();     
            if (state === 'SUCCESS') {
                this.uploadResumeToTalent(cmp, cmp.get("v.tempSelectedRecord"));
            }
            else{
                this.showError('Please try again..!!','An error occured!');
            }
        });
        $A.enqueueAction(action);
	},
    invokeDupeMerge: function (cmp, e) {
    	const dupeResults = cmp.find('tduperesults');
        if (dupeResults) {
            dupeResults.merge();
        }
        
	},

	saveAndUpdate: function(cmp) {
		if (cmp.get('v.dupeResults')) {
			const dupeResults = cmp.find('tduperesults');
			var selectedFromDupe = dupeResults.cID();
			const names = dupeResults.talentName();
            var talentDocumentId = cmp.get('v.talentDocumentId');
			if (names) {
				cmp.set('v.names', names);
			}

			if (selectedFromDupe) {
                cmp.set("v.showSpinner",true);
				cmp.set("v.tempSelectedRecord", selectedFromDupe);
				this.getTalentResumeCount(cmp, selectedFromDupe, (count) => {

					if (!count) {
						//SHOW TOAST HERE THAT ERROR HAPPENED
						this.showError('Selected talent does not exist. Please try again later.', 'Error')
						cmp.set("v.showSpinner",false);
					} else {
						if (count[0] < 5) {
                    		this.uploadResumeToTalent(cmp,selectedFromDupe,"SaveAndUpdate");
							this.getCompareData(cmp, talentDocumentId, null, (data) => {
								console.log(data);
								this.showCompareModal(cmp, data, names.fname, names.lname, count[1]);
							});
						} else {
							//..need to show document replace ID.
							//handleShowModal: function(cmp, recordId, source, enableSave)
							cmp.set('v.uploadType', 'saveAndUpdate');
							this.handleShowModal(cmp, count[1], 'TalentDocument', true);
						}
					}
				})
         }
		}
		
	},
	getTalentResumeCount: function (cmp,conId, callback) {

			const action = cmp.get("c.talentResumeCount");
			action.setParams({conId});

			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === 'SUCCESS') {
					callback(response.getReturnValue());
				}
				else{
                    cmp.set("v.showSpinner",false);          
					this.showError('Please try again..!!','An error occured!');
				}
			});
			$A.enqueueAction(action);
	},
	getCompareData: function (cmp, talentDocumentId, replaceResumeId, callback) {

			const params = {talentDocumentId,	replaceResumeId};

			let bdhelper = cmp.find('bdhelper')
			bdhelper.callServer(bdhelper,'ATS','TalentDocumentFunctions','getCoreContactCompareRecord', function(response) {
				callback(JSON.parse(response));
			},params,false)
	 },
	showCompareModal: function (cmp, items, fName, lName, goTo) {
		//...placeholder for comparison modal

		let modalBody;
		let modalHeader;
		let modalFooter;

        
		$A.createComponents([
            	["c:C_TalentResumeCompareAndUpdateModal_Body_Contact",{"items":items}],
            	["c:C_TalentResumeCompareAndUpdateModal_Header",{"fName":fName,"lName":lName}],
            	["c:C_TalentResumeCompareAndUpdateModal_Footer",{"items":items, "goTo": goTo}]
			],
			function(components, status,errorMessage){
				if (status === "SUCCESS") {
					modalBody = components[0];
					modalHeader = components[1];
					modalFooter = components[2];
					cmp.find('overlayLib').showCustomModal({
						header: modalHeader,
						body: modalBody,
						footer:modalFooter,
						cssClass: "ResumeParseModal",
						showCloseButton: true,
						closeCallback: function() {
							//console.log('Resume compare modal closed!!!');

						}
					});
                    cmp.set("v.showSpinner",false);
                }else{
                  cmp.set("v.showSpinner",false);  
                }
			}
		);//Create component end
       
  	
	},
        

})