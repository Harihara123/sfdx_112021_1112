({	
	doInit : function(component,event,helper) {
		var conrecord = component.get("v.contactRecordFields");
		component.set("v.parentInitialLoad",true);
		var countryCode;
		if(component.get("v.countriesMap") === null){
			var action = component.get("c.getCountryMappings");
			action.setStorable();
			action.setCallback(this,function(response) {
				component.set("v.countriesMap",response.getReturnValue());
				if (conrecord.Talent_Country_Text__c) {
					countryCode = helper.getKeyByValue(component.get("v.countriesMap"),conrecord.MailingCountry);
					component.set("v.MailingCountryKey",countryCode);
				}
				if(conrecord.Talent_Country_Text__c === null && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
					component.set("v.contactRecordFields.Talent_Country_Text__c",'United States');
					component.set("v.MailingCountryKey",'US');
				 }
			});
			$A.enqueueAction(action);
		}
		else if (conrecord.Talent_Country_Text__c) {
			countryCode = helper.getKeyByValue(component.get("v.countriesMap"),conrecord.MailingCountry);
			component.set("v.MailingCountryKey",countryCode);
		} else if(conrecord.Talent_Country_Text__c === null && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
					component.set("v.contactRecordFields.Talent_Country_Text__c",'United States');
					component.set("v.MailingCountryKey",'US');
		}
		helper.reInitLoad(component);
		helper.addRemoveEventListenerNew(component, event,'popOverId', 'locationPopOverCls', 'locationPopOver');
		var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-hide');
        $A.util.removeClass(spinner, 'slds-show'); 
	},

	toggleEditPopover: function (component, event, helper) {
		helper.addRemoveEventListener(component, event,'popOverId', 'locationPopOverCls', 'locationPopOver');
		component.set("v.inTalentLocation", "true");
		
	},

	checkforHide : function (component, event, helper) {
		var myPopover = component.find('popOverId');
		var popoverEle = myPopover.getElement();
		console.log('target:' + event.target.value);
		if(popoverEle && !popoverEle.contains(event.target)){
			helper.togglePopover(component);

		} 
	},

	saveTalentLocation: function (component, event, helper) {
		helper.validateAndSave(component);
		
    },

	updateMailingCountryCode: function(component, event, helper) {
		helper.updateMailingCountryCode(component, event);
	},

	handleLocationSelection : function(component,event, helper) {
		component.set("v.isGoogleLocation", true);
		var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode, countrymap, displayText;

	  	mailingstreet = streetnum = streetname = city = state = country = countrycode = postcode = displayText = '';
	    var addr = event.getParam("addrComponents");
		var latlong = event.getParam("latlong");    //S-96255 - Update MailLat, MailLong fields on Contact - Karthik
		countrymap = component.get("v.countriesMap"); 
	    displayText = event.getParam("displayText");
	    for (var i=0; i<addr.length; i++) {
	        if (addr[i].types.indexOf("street_number") >= 0) {
	            streetnum = addr[i].long_name;
	        }
	        if (addr[i].types.indexOf("route") >= 0) {
	            streetname = addr[i].long_name;
	        }        
	        if (addr[i].types.indexOf("postal_code") >= 0) {
	            postcode = addr[i].long_name;
	        }
	        if (addr[i].types.indexOf("locality") >= 0 || addr[i].types.indexOf("postal_town") >= 0 ) {
	            city = addr[i].long_name;
	        }
	        if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
	            state = addr[i].long_name;
	        }
	        if (addr[i].types.indexOf("country") >= 0) {
	            country = addr[i].long_name;
	            countrycode = countrymap[addr[i].short_name];
	        }        
	    }

	    if(streetnum) {
	        mailingstreet = streetnum;
	    } 
	    if(streetnum && streetname) {
	        mailingstreet = mailingstreet + ' ';
	    }
	    if(streetname) {
	        if(mailingstreet != ''){
	            mailingstreet = mailingstreet + streetname;        
	        } else {
	            mailingstreet = streetname;
	        }
	    }

	    component.set("v.contactRecordFields.MailingStreet",mailingstreet);
	    component.set("v.contactRecordFields.MailingCity",city);
	    component.set("v.contactRecordFields.MailingState",state);
	    component.set("v.contactRecordFields.MailingCountry",countrycode);
	    component.set("v.contactRecordFields.Talent_Country_Text__c",country);
	    component.set("v.contactRecordFields.MailingPostalCode",postcode);
		component.set("v.presetLocation",mailingstreet); //displayText
		component.set("v.streetAddress2","");
		//S-96255 - Update MailLat, MailLong fields on Contact
		if(latlong){
			//component.set("v.contactRecordFields.GoogleMailingLatLong__Latitude__s",latlong.lat);
			//component.set("v.contactRecordFields.GoogleMailingLatLong__Longitude__s",latlong.lng);
			component.set("v.contactRecordFields.MailingLatitude",latlong.lat);
			component.set("v.contactRecordFields.MailingLongitude",latlong.lng);			
		} else {
			component.set("v.contactRecordFields.MailingLatitude",null);
			component.set("v.contactRecordFields.MailingLongitude",null);	
		}
		component.set("v.contactRecordFields.LatLongSource__c","Google");
		component.set("v.parentInitialLoad",true);	
		var countryCode = helper.getKeyByValue(component.get("v.countriesMap"),countrycode);
		component.set("v.MailingCountryKey",countryCode);		
	},

	closeEditPopover : function (component, event, helper) {
		component.set("v.togglePopover", false);
		component.set("v.parentInitialLoad",true);	
		var appEvent = $A.get("e.c:E_HideEditPopOverCmp");
		appEvent.fire();			
	},

	onRender : function (component, event, helper) {
		component.set("v.parentInitialLoad",true);
	}
})