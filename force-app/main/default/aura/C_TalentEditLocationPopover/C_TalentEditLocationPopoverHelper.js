({
	togglePopover: function(component){
        var togglePopover = component.get("v.togglePopover");
        component.set("v.togglePopover", !togglePopover);
    },

	validateAndSave : function(component) {
		if(this.validTalent(component)){
			this.saveTalent(component);    
		}
		component.set("v.parentInitialLoad",true);
	},

	validTalent: function(component) {
        var validTalent = true;
        
		//S-228927 - adding new opcos SJA,IND,EAS 
        var newAerotek = $A.get("$Label.c.Aerotek");
        var newAstonCarter = $A.get("$Label.c.Aston_Carter");
        var newNewco = $A.get("$Label.c.Newco");       
		if (component.get("v.runningUser.OPCO__c") === 'ONS' || component.get("v.runningUser.OPCO__c") === 'TEK' || component.get("v.runningUser.OPCO__c") === newAerotek || component.get("v.runningUser.OPCO__c") === newAstonCarter || component.get("v.runningUser.OPCO__c") === newNewco) { 
			validTalent = this.validateStateCountry(component, validTalent);
        }
		validTalent = this.validateStreetAddress(component, validTalent);
        return validTalent;
    },

	saveTalent : function(component) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
        $A.util.removeClass(spinner, 'slds-hide'); 
		var contact = component.get("v.contactRecordFields");         
        if(!component.get("v.isGoogleLocation")){
			component.set("v.contactRecordFields.MailingLatitude",null);
			component.set("v.contactRecordFields.MailingLongitude",null);
			if (contact.LatLongSource__c != null && contact.MailingCity == null && contact.MailingPostalCode == null) {
				component.set("v.contactRecordFields.LatLongSource__c","");
			}
		}
		var address = component.get("v.presetLocation");
		if (component.get("v.streetAddress2") ) {
			address = address +', '+component.get("v.streetAddress2");			
		}		
		component.set("v.contactRecordFields.MailingStreet",(address != undefined ? address : ""));
		
		var params = {"contact": contact};
        var action = component.get("c.saveTalentInlineEdit");
        action.setParams(params);
            
        action.setCallback(this,function(response) {
            if (response.getState() === "SUCCESS") { 
				var compEvent = component.getEvent("refreshTalentHeaderComponent");
				compEvent.fire(); 
                this.togglePopover(component);   
                this.showToast('Success', 'Record has been saved.', 'success');
				var appEvent = $A.get("e.c:E_HideEditPopOverCmp");
				appEvent.fire(); 
            } 
            else if (response.getState() === "ERROR") {
                this.showToast('Error', 'Something went wrong, please try again.', 'error');    
            }
			$A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');			 			
        });
        $A.enqueueAction(action);		
    },

	validateStateCountry : function(component, validTalent) {
		var state   =  component.get("v.contactRecordFields.MailingState");
		var country =  component.get("v.contactRecordFields.Talent_Country_Text__c");
		var mailingCountry = component.get("v.contactRecordFields.MailingCountry");

		if ( $A.util.isEmpty(state) || $A.util.isEmpty(country) ) {
			validTalent = false; 
            this.showToast('Error', 'Please enter a valid Country and State.', 'error');
		} else if (mailingCountry === '') {
            var countryMap = component.get("v.countriesMap");
            var countryCode = component.get("v.MailingCountryKey");
            if (countryMap && countryCode && countryCode.length > 0) {
               try {
                   var countryValue = countryMap[countryCode];
                   if (countryValue && countryValue.length > 0 ) {
                       component.set("v.contactRecordFields.MailingCountry", countryValue);
                   } else {
                       validTalent = false; 
                       this.showToast('Error', 'Please enter a valid Country and State.', 'error');
                   }
               } catch(error) {
                   console.error(error);
                   validTalent = false;
                   this.showToast('Error', 'Please enter a valid Country and State.', 'error'); 
               }
            } else {
                validTalent = false;
                this.showToast('Error', 'Please enter a valid Country and State.', 'error'); 
            }
		} 
		return validTalent;        
    },

	validateStreetAddress : function(component, validTalent) {
		if (component.get("v.streetAddress2") && !component.get("v.presetLocation")) {
			validTalent = false;
			this.showToast('Error', 'Please enter a Street Address along with Street Address 2.', 'error');
		}
		return validTalent;
	},

    updateMailingCountryCode: function(component, event) {
        var conKey = component.get('v.MailingCountryKey');
        var countrymap = component.get("v.countriesMap");
        var countrycode ;
        if (conKey !== null && typeof conKey !== 'undefined') {
            countrycode = countrymap[conKey];
            if(countrycode !== null && typeof countrycode !== 'undefined'){
                component.set("v.contactRecordFields.MailingCountry",countrycode);
            } else {
				component.set("v.contactRecordFields.MailingCountry","");
			}
        }
		/*if (component.get('v.initialLoad')) {			
			component.set("v.initialLoad",false);
		} else {
			component.set("v.parentInitialLoad",false);
		}*/
    },
    
    showToast: function(title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },

	getKeyByValue : function(map, searchValue) {
		for(var key in map) {
			if (map[key] === searchValue) {
				return key;
			}
		}
	},

	setReRenderAttributes : function (component) {
		/*if (component.get("v.parentInitialLoad") !== true) {
			component.set("v.parentInitialLoad",false);
		}*/
	},
	setRenderAttributes : function (component) {
		component.set("v.parentInitialLoad",true);
	},

	addRemoveEventListener : function(component, event, elementId, headerClass, subClass) {
		if (event.target.className.includes(subClass)) {
				component.set("v.togglePopover", true);
				this.reInitLoad(component);
				//var a = component.get('c.doInit');
				//$A.enqueueAction(a);

			} else {
				this.togglePopover(component);				
			}

			$A.util.addClass(event.target, subClass);
			var clickedFn;
			var myPopover = component.find(elementId);

			clickedFn = $A.getCallback(function(event){
				var popoverEle = myPopover.getElement();
				$A.util.removeClass(popoverEle, 'slds-hide');

				var myExternalEvent;
				if(window.$A && (myExternalEvent = window.$A.get("e:E_HideEditPopOverCmp"))) {				
						myExternalEvent.fire();
				}
				
				if(popoverEle && event.target) {
					if (!event.target.className.includes(headerClass) && !popoverEle.contains(event.target)) {
						$A.util.addClass(popoverEle, 'slds-hide');
						$A.util.addClass(event.target, subClass);						
						window.removeEventListener('click',clickedFn);
					}
				} 
				else {
					window.removeEventListener('click',clickedFn);
					$A.util.addClass(event.target, subClass);
				}
			});

			window.addEventListener('click',clickedFn);
	},

	reInitLoad : function(component) {
		component.set("v.viewState","EDIT");
		component.set("v.parentInitialLoad",true);
		var mailingStreet = component.get("v.contactRecordFields.MailingStreet");
		var strs;
		if (mailingStreet) {
			if( mailingStreet.indexOf(',') != -1 ){				
				component.set("v.presetLocation",mailingStreet.substr(0,mailingStreet.indexOf(',')));
				component.set("v.streetAddress2",mailingStreet.indexOf(',')+1 != undefined ? mailingStreet.substr(mailingStreet.indexOf(',')+1).trim() : "");
			} else {
				component.set("v.presetLocation",mailingStreet);
			}
		} else {
			component.set("v.presetLocation","");
		}		
	},

	addRemoveEventListenerNew : function(component, event, elementId, headerClass, subClass) {
			$A.util.addClass(event.target, subClass);
			var clickedFn;
			var myPopover = component.find(elementId);

			clickedFn = $A.getCallback(function(event){
				var popoverEle = myPopover.getElement();
				$A.util.removeClass(popoverEle, 'slds-hide');
				
				
				if(popoverEle && event.path[0]) {
					if (!event.path[0].className.includes(headerClass) && !popoverEle.contains(event.path[0])) {
						$A.util.addClass(popoverEle, 'slds-hide');
						$A.util.addClass(event.target, subClass);	
						var myExternalEvent = window.$A.get("e.c:E_HideEditPopOverCmp");							
						myExternalEvent.fire();					
						document.removeEventListener('click',clickedFn);
					}
				} 
				else {
					//var myExternalEvent = window.$A.get("e.c:E_HideEditPopOverCmp");							
					//myExternalEvent.fire();
					document.removeEventListener('click',clickedFn);
					$A.util.addClass(event.path[0], subClass);
				}
			});

			document.addEventListener('click',clickedFn);
	}
})