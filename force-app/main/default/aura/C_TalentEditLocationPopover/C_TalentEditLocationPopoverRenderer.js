({
	rerender: function(component, helper) {
		helper.setReRenderAttributes(component);
		return this.superRerender();
	},

	render: function(component, helper) {
		helper.setRenderAttributes(component);
		return this.superRender();
	} 
})