({
	doInit : function(cmp, event, helper) {
        //helper.loadTalent(component);
	},
    
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");
        
        if(expanded){
            cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
        }
    },
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
        cmp.set("v.expanded", expanded);
    },
})