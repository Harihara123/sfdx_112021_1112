({
	doInit : function(component, event, helper) {
	   // helper.getEducationDetails(component);
	    component.set("v.expanded", true);
        component.set("v.expandAll", true);
    },


	displayEducationDetails : function(cmp, event, helper) {
        var recordID = cmp.get("v.itemDetail.ParentRecordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__CandidateEducationCertification?id=" + recordID
        });
        urlEvent.fire();
	},
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");

        if(expanded){
            cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
        }
    },
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
        cmp.set("v.expanded", expanded);
    },
    
    addEducation:function(cmp,event,helper){
        var certEvent = $A.get("e.c:E_TalentEducationAdd");
        var recordId = cmp.get("v.recordId");        
        certEvent.setParams({"recordId":recordId});
        certEvent.fire();
    },

    deleteRefresh : function(component, event, helper) {
        helper.refreshList(component);
    },

    addEditRefresh : function(component, event, helper) {
        helper.refreshList(component);

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Education record saved successfully.",
            "type": "success"
        });
        toastEvent.fire();
    }
   
})