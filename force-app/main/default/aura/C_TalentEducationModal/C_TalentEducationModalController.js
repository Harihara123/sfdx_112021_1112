({
	displayAddEducationDetail : function(component, event, helper) {
		//Toggle CSS styles for opening Modal
		helper.initializeEducationModal(component, false);
		component.find("degreeId").set("v.value", '--None--');
		component.set("v.modalTitle", "Add Education");
        component.set("v.edit",false);
        var recordId = event.getParam("recordId");
        component.set("v.recordId", recordId);
	},

    displayEditEducationDetail : function(component,event,helper){
		var itemId = event.getParam("itemID");
		// console.log("Edit recordId" + itemId);
		component.set("v.modalTitle", "Edit Education");
        component.set("v.edit",true);
		helper.initializeEducationModal(component, true);
		component.set("v.education.Id", itemId);
	},

    
    hideModal : function(component, event, helper){
        //Toggle CSS styles for hiding Modal
		helper.toggleClassInverse(component,'backdropAddEducationDetail','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialogAddEducationDetail','slds-fade-in-');
		var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
    },
    
    saveEducation : function(component, event, helper){
		helper.validateAndSaveEducation(component);
    }
})