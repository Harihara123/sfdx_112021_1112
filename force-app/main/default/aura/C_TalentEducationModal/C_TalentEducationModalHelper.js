({
    initializeEducationModal : function(component, editMode) {
        this.resetAddForm (component);

//        if (!component.get("v.degreeLevelList").length>0) {
        var params = null;
//changes for S-5526
            var self = this;
            var bdata = component.find("basedatahelper");
            // this.callServer(component,'ATS','TalentEducationFunctions','getDegreeLevelList',function(response){
            bdata.callServer(component,'ATS','TalentEducationFunctions','getDegreeLevelList',function(response){
            if(component.isValid()){
                var degreeLevel = [];
                degreeLevel.push({value:'--None--', key:'--None--'}); 
                var degreeLevelMap = response;
                for ( var key in degreeLevelMap ) {
                    degreeLevel.push({value:degreeLevelMap[key], key:key});
                }
                component.set("v.degreeLevelList", degreeLevel);
                component.find("degreeId").set("v.value", '--None--'); 

                if(editMode === true){
                    self.getEducationToEdit(component)
                }
                self.toggleClass(component,'backdropAddEducationDetail','slds-backdrop--');
                self.toggleClass(component,'modaldialogAddEducationDetail','slds-fade-in-');
          }
        },params,false);
  //     } 
    },

    resetAddForm: function(component) {
        // Fire event to clear lookup field first 

        component.find("inputYearId").set("v.value", '');
        component.find("inputYearId").set("v.errors", null);
//        component.find("degreeId").set("v.value", '');
        component.find("degreeId").set("v.errors", null);
        component.find("majorId").set("v.value", '');
        component.find("majorId").set("v.errors", null);
        component.find("honorsId").set("v.value", '');
        component.find("honorsId").set("v.errors", null);
        component.find("notesId").set("v.value", '');
        component.find("notesId").set("v.errors", null);
        component.set("v.education.Education_Organization__c",'');
        component.set("v.education.Maxhire_Education_Value__c",'');
        component.set("v.education.Organization_Name__c",'');
        component.set("v.education.Id", null);
        
    },


    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
    $A.util.removeClass(modal,className+'hide');
    $A.util.addClass(modal,className+'open');
  },
 
    toggleClassInverse: function(component,componentId,className) {
    var modal = component.find(componentId);
    $A.util.addClass(modal,className+'hide');
    $A.util.removeClass(modal,className+'open');
  },
    
   validateAndSaveEducation: function(component) {
       if (this.validateEducation(component)) {
          this.saveEducation(component);
          this.toggleClassInverse(component,'backdropAddEducationDetail','slds-backdrop--');
          this.toggleClassInverse(component,'modaldialogAddEducationDetail','slds-fade-in-');
       }
  },
    
    validateEducation: function(component) {
        var validEducation = true;
        // Validate Year - The graduation year must be between 1940 and 2050
        var yearField = component.find("inputYearId");
        var year = yearField.get("v.value");
        if ($A.util.isEmpty(year) === false && (isNaN(year) || year < 1940 || year > 2050)) {
            validEducation = false;
            //yearField.set("v.errors", [{message:"The graduation year must be between 1940 and 2050"}]);
            yearField.set("v.errors", [{message:$A.get("$Label.c.ATS_GRAD_YEAR_ERROR")}]);
        yearField.set("v.errors", null);
        }
        
        var degreeField = component.find("degreeId");
        var degreeName = degreeField.get("v.value");
         if (degreeName === '--None--'){
            validEducation = false;
            //degreeField.set("v.errors", [{message:"Please select a valid Degree."}]);
             degreeField.set("v.errors", [{message:$A.get("$Label.c.ATS_Please_select_a_valid_Degree")}]);
        } else {
        degreeField.set("v.errors", null);
        }
      return(validEducation);
    },  
    
    saveEducation: function(component, event) {

       var Graduation_Year = (component.find("inputYearId").get("v.value") ? component.find("inputYearId").get("v.value") : null);

       // var schoolField = component.get("v.dynamicComponentsByAuraId").schoolNameId;
       /*var schoolNamevalue  = (schoolField.get("v.nameValue") ? schoolField.get("v.nameValue") : null);
       var schoolNameId = (schoolField.get("v.value") ? schoolField.get("v.value") : null);*/
       var schoolNamevalue = (component.get("v.education.Organization_Name__c") ? component.get("v.education.Organization_Name__c") : null);
       var schoolNameId  = (component.get("v.education.Education_Organization__c") ? component.get("v.education.Education_Organization__c") : null);
       var Major__c = (component.find("majorId").get("v.value") ? component.find("majorId").get("v.value"): null);
       var Honor__c = (component.find("honorsId").get("v.value") ? component.find("honorsId").get("v.value") : null);
       var Notes__c = (component.find("notesId").get("v.value") ? component.find("notesId").get("v.value") : null);
	   
       //D-09760 : santosh C
       var talentID;
       if(component.get("v.recordId") === null)
           talentID = component.get("v.education.Talent__c");
	   else         
           talentID = component.get("v.recordId");
        
       var newEducation = {'sobjectType': 'Talent_Experience__c', 
                            'Talent__c': talentID,
                            'Type__c': 'Education',
                          'Degree_Level__c' : component.find("degreeId").get("v.value"),
                            'Graduation_Year__c' : Graduation_Year,
                          'Organization_Name__c' : schoolNamevalue,
                            'Education_Organization__c' : schoolNameId, 
                          'Major__c' : Major__c,
                            'Honor__c' : Honor__c,
                            'Notes__c' : Notes__c,                          
                            'Id' : component.get("v.education.Id")
                       }; 
     

        var params = {"talentExperience": JSON.stringify(newEducation)};
        var bdata = component.find("basedatahelper");
        bdata.callServer(component,'ATS','','saveTalentExperience',function(response){
            if(component.isValid()){
                var newEducationId = response;
                var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
                updateLMD.fire();

                var savedEvent = component.getEvent("educationSavedEvent");
                savedEvent.fire();
            }
        },params,false);
  },


      getEducationToEdit : function(component) {

         var params = {"soql": "select Talent__c, Degree_Level__c, Graduation_Year__c,Maxhire_Education_Value__c, Organization_Name__c, Education_Organization__c, Major__c,Honor__c, Notes__c from Talent_Experience__c where Id = '" + component.get("v.education.Id") + "'"};
         var bdata = component.find("basedatahelper");         
         bdata.callServer(component,'','',"getRecord",function(response){
             component.set("v.education", response);
             /*console.log (">>>>>>>>>>>>>>>>>>>>");
             console.log (response);*/
         },params,false);
    },
})