({
    doInit: function(cmp, event, helper){
        var contact = cmp.get("v.contactRecord");
        var talentId = contact.AccountId;
        cmp.set("v.talentId", talentId);
        helper.refreshList(cmp);
        helper.getInitialViewListLoad(cmp, helper);

    },
    viewEducationAndCerts :function(cmp, event, helper) {
        // var recordID = cmp.get("v.recordId");
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__CandidateEducationCertification?id=" + recordID
        });
        urlEvent.fire();
	},
        
    showEducationAndCerts : function(component, event, helper) {

        var contact = component.get("v.contactRecord");
        var talentId = contact.AccountId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
         //   "url": "/one/one.app#/n/Talent_Education_And_Cert_Details?recordId=" + component.get('v.recordId'),
              "url": "/one/one.app#/n/Talent_Education_And_Cert_Details?recordId=" + talentId,
            "isredirect":true
        });
        urlEvent.fire(); 
	}, 
	    
     addEducation:function(cmp,event,helper){
        if(cmp.get("v.contactRecord")) {
			var certEvent = $A.get("e.c:E_TalentEducationAdd");
			// var recordId = cmp.get("v.recordId");
			var contact = cmp.get("v.contactRecord");
			var recordId = contact.AccountId;
			certEvent.setParams({"recordId":recordId});
			certEvent.fire();
		}
    },

    deleteRefresh : function(component, event, helper) {
        // helper.refreshList(component);
        helper.getInitialViewListLoad(component, helper);
    },

    refreshEducationListAppEventHandler : function(component, event, helper) {
       // helper.refreshList(component);
       helper.getInitialViewListLoad(component, helper);

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Education record added successfully.",
            "type": "success"
        });
        toastEvent.fire();
    },
    updateLoading: function(cmp, event, helper) {
        if(!cmp.get("v.loadingData")) {
            cmp.set("v.loading",false);
        }
    },
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");

        if(expanded){
             cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
          }
      },
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
          cmp.set("v.expanded", expanded);
        }

})