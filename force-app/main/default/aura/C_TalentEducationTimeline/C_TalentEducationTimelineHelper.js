({
	refreshEducationList : function(component) {
        this.getResults(component);
	},
//From C_BaseCardRelatedListHelper
     getInitialViewListLoad : function(cmp,helper){
        var recordID = cmp.get("v.recordId");
        var viewListFunction = cmp.get("v.viewListFunction"); 
        var talentID = cmp.get("v.talentId");
        
        this.getViewListItems(cmp, viewListFunction, talentID, this);
        
	},

getViewListItems : function(cmp, viewListFunction, recordID,helper) {

        var bdata = cmp.find("basedatahelper");
        var self = this;
        var comp = cmp;

        cmp.set("v.loadingData",true);
        // var actionParams = {'recordId':recordID};
           var actionParams = {
                            'recordId':recordID,
                            'maxRows': cmp.get("v.maxRows"),
							//S-82159:Added personaIndicator parameter to params by Siva 12/6/2018
							"personaIndicator":"Recruiter"
                        };
        var className = '';
        var subClassName = '';
        var methodName = '';

        var arr = viewListFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        bdata.callServer(comp,className,subClassName,methodName,function(response){
            cmp.set("v.loadingData",false);
            var returnVal = response;

          //  var newArr = (new Function("return [" + returnVal + "];")());
          //  cmp.set("v.viewList", newArr);
            comp.set("v.itemDetail", returnVal);
            comp.set("v.loadingData",false);
            self.getResults(comp, self);
            self.getResultCount(comp, self);

        },actionParams,false);
    }
    ,getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");
        var self = this;
        var comp = cmp;

        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            //var recordID = cmp.get("v.recordId");
            var recordID = cmp.get("v.talentId");
            var soql = listView[indexNo].soql;
			
            if(soql){
                if(soql != ''){
                    soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
  
                        var apexFunction = "c.getRows";

                        var params = {
                            "soql": soql,
                            "maxRows": cmp.get("v.maxRows"),
							//S-82159:Added personaIndicator parameter to params by Siva 12/6/2018
							"personaIndicator":"Recruiter"
                        };

                        cmp.set("v.loadingData",true);
                        var actionParams = {'recordId':recordID};
                        if(params){
                            if(params.length != 0){
                             Object.assign(actionParams,params);
                            }
                        }

                        var bdata = comp.find("basedatahelper");
                        bdata.callServer(cmp,'','',apexFunction,function(response){
                            cmp.set("v.records", response);
                            cmp.set("v.loadingData",false);
                        
                            if(listView[indexNo].additional){
                                cmp.set("v.additional",listView[indexNo].additional);
                            }
                            
                        },actionParams,false);
                    }
                }else{
                    var apexFunc = listView[indexNo].apexFunction;
                    if(apexFunc){
                        var params = listView[indexNo].params;
                        self.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                    }
                }
            }
        }
    ,getResultCount : function(cmp){
        var listView = cmp.get("v.viewList");
        var bdata = cmp.find("basedatahelper");
		//var isPersona = cmp.get("v.isPersonaIndicator");//S-111810:Removed TalentMergeIndicator usage by Siva 1/10/2019
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            // var recordID = cmp.get("v.recordId");
            var recordID = cmp.get("v.talentId");
            var countSoql = listView[indexNo].countSoql;    
            if(countSoql){
                 if(countSoql != ''){
                    countSoql = countSoql.replace("%%%parentRecordId%%%","'"+recordID+"'");
					//S-111810:Removed TalentMergeIndicator usage by Siva 1/10/2019
					//if(isPersona.toLowerCase() === "true") {
						//countSoql = countSoql.concat(countSoql, " and PersonaIndicator__c = \'Recruiter\'");
                        countSoql = countSoql.concat(" and PersonaIndicator__c = \'Recruiter\'");
					//}
                    var params = {"soql": countSoql};
                    cmp.set("v.loadingCount",true);
                    
                    bdata.callServer(cmp,'','','getRowCount',function(response){
                            cmp.set("v.recordCount", response);
                            cmp.set("v.loadingCount",false);
                        
                    },params,false);

                 }else{
                    cmp.set("v.loadingCount",false);
                 }
            }
        }
        
    },

	 refreshList : function(component) {
        component.set("v.reloadData", true);
    },
 
        showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    getViewFromApexFunction : function(cmp, apexFunction, recordID, params,helper){
        var className = '';
        var subClassName = '';
        var methodName = '';
        var self = this;

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordID, "maxRows": cmp.get("v.maxRows")};

            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }
            // this.callServer(cmp,className,subClassName,methodName,function(response){
            var bdata = cmp.find("basedatahelper");
            bdata.callServer(cmp,className,subClassName,methodName,function(response){
               
                cmp.set("v.records", response);
                cmp.set("v.loadingData",false);
                var recordCount = 0;
                if(response){
                    if(response.length > 0){
                        if(response[0].totalItems){
                            recordCount = response[0].totalItems;
                        }
//syntax check - condition will always be true using String "undefined"
                        if (response[0].presentRecordCount !== "undefined") {
                            cmp.set ("v.presentRecordCount", response[0].presentRecordCount);
                            cmp.set ("v.pastRecordCount", response[0].pastRecordCount);
                            cmp.set ("v.hasNextSteps", response[0].hasNextSteps);
                            cmp.set ("v.hasPastActivity", response[0].hasPastActivity);
                        }
                    }
                }

                if(recordCount == 0){
                    self.getResultCount(cmp);
                }

                cmp.set("v.recordCount", recordCount); // returnVal.length);
                cmp.set("v.loadingCount",false);
            },actionParams,false);
        }

    }
})