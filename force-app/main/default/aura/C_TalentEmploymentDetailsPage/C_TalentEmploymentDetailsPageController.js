({
    doInit: function(cmp,event,helper){
        // changes for S-55255 
        var bdata = cmp.find("basedatahelper");
        bdata.callServer(cmp,'','',"getCurrentUser",function(response){
            if(response){
                if(response.Profile){
                    cmp.set('v.runningUser',response);
                }
            }
            
        },null,true);
    },
    showEmploymentModal : function(cmp, event, helper) {
        var trg = cmp.get("v.modalTrigger");
        cmp.set("v.modalTrigger", !trg);
    },

	deleteRefresh : function(component, event, helper) {
        helper.refreshList(component);
    },

    refreshEmploymentTimeline : function(cmp, event, helper) {
       if (cmp.get("v.recordId") === event.getParam("talentId")) {
            helper.refreshList(cmp);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "message": "Employment record saved successfully.",
                "type": "success"
            });
            toastEvent.fire();
        }
    }
})