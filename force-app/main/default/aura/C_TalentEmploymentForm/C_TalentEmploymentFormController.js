({
    doInit:function(component, event, helper){
         var bdata = component.find("basedatahelper");
        bdata.callServer(component,'','',"getCurrentUser",function(response){
            if(response){
                if(response.Profile){
                    component.set('v.runningUser',response);
                }
            }
        },null,true);
        //Rajeesh job title ownership
        bdata.callServer(component,'','',"getCurrentUserWithOwnership",function(response){
            if(response){
                    component.set('v.usrOwnership',response.userOwnership);
            }
        },null,true);//End Rajeesh
		bdata.callServer(component,'','',"getUserCategory",function(response){
            if(response){
                if(response){
                    component.set('v.runningUserCategory',response);
                }
            }
        },null,true);
/*      var contact = component.get("v.contactRecord");
      component.set("v.talentId", contact.fields.AccountId.value);*/
      //helper.getAccountId(component);
      helper.populateCurrencyType(component);  
      helper.populateRateFrequency(component);
    },
    searchChange: function(component, event, helper) {
		component.set("v.isClientAccountBlank", true);  
    },
    updateJobTitle: function(component,event,helper){
        //Rajeesh S-65417 adding look up to job title
        //The second instance of lookup component messes up with this one.
        //seperating the variables used.
        var jobTitle = component.get('v.jobTitle');
        component.set('v.employment.Title__c',jobTitle);
    },
	validateAndSaveEmployment : function(component, event, helper){  
        component.set('v.disabled', true);
        helper.validateAndSaveEmployment(component); 
    },

    cancelEmployment : function(component, event, helper){  
        var cancelEvent = component.getEvent("formCanceledEvent");

        if(cancelEvent){
            cancelEvent.fire();
        }
    },
    updateOrgName : function(component, event, helper) {
        helper.updateOrgName(component, event);
    },

    clearAndEnableOrgName : function(component, event, helper) {
        helper.clearAndEnableOrgName(component, event);
    },

    dateKeypressValidation: function(component, event, helper){
		helper.preventNonNumericKeys(component, event);
    },

    rateTypeChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
   
    rateChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
    
    bonusPctChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
    
    bonusChange : function (cmp, event, helper) {
        helper.calculateBonusPct(cmp);
        helper.calculateTotal(cmp);
    },
    
    addlCompChange : function (cmp, event, helper) {
        helper.calculateTotal(cmp);
    },
    //Call by aura:waiting event  
   
    //Call by aura:doneWaiting event 

})