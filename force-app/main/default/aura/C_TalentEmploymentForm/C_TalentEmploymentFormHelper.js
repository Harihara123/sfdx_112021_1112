({
    /** 52 * 40 assuming a 40 hour work week */
    HOURLY_TO_ANNUAL_FACTOR : 2080,
    DAILY_TO_ANNUAL_FACTOR : 260,
    WEEKLY_TO_ANNUAL_FACTOR : 52,
    MONTHLY_TO_ANNUAL_FACTOR : 12,
    populateCurrencyType : function(component) {
        var params = null;
        var bdata = component.find("basedatahelper");
        bdata.callServer(component,'ATS','TalentEmploymentFunctions','getCurrencyList',function(response){
            if(component.isValid()){
                var CurrLevel = [];
                var CurrLevelMap = response;
                for ( var key in CurrLevelMap ) {
                    CurrLevel.push({value:CurrLevelMap[key], key:key});
                }
                component.set("v.CurList", CurrLevel);
            }
        },params,false);
    },
    populateRateFrequency : function(component) {
        var params = null;
        var self = this;
        var bdata = component.find("basedatahelper");
        bdata.callServer(component,'ATS','TalentEmploymentFunctions','getRateFrequencyList',function(response){
            if(component.isValid()){
                var RateFrequency = [];
                var RateFrequencyMap = response;
                for ( var key in RateFrequencyMap ) {
                    RateFrequency.push({value:RateFrequencyMap[key], key:key});
                }
                component.set("v.rateFrequency", RateFrequency);
                if(component.get('v.edit') == true){
                    var employmentId = component.get('v.employmentId');
                    component.set('v.employment.Id',employmentId);
                    self.getEmploymentToEdit(component);
                }
            }
        },params,false);
    },
    clearErrors : function (component) {
        component.find("inputOrgName").set("v.errors", null);
        component.find("inputTitle").set("v.errors", null);
        component.find("inputDept").set("v.errors", null);
        component.find("inputStartDate").set("v.errors", null);
        component.find("inputEndDate").set("v.errors", null);
        component.find("inputReasonForLeaving").set("v.errors", null);
        component.find("inputAGPlacement").set("v.errors", null);
        component.find("inputCurrEmployer").set("v.errors", null);
        component.find("inputSalary").set("v.errors", null);
        component.find("inputPayrate").set("v.errors", null);
        component.find("inputBonus").set("v.errors", null);
        component.find("inputBonusPctId").set("v.errors", null);
        component.find("inputAddlCompId").set("v.errors", null);
        component.find("inputNotes").set("v.errors", null);
        component.find("CtypeId").set("v.errors", null);
    },

    validateAndSaveEmployment : function (component) {
        if (this.validateEmployment(component)) {
            this.saveEmployment(component);
        }else {
            component.set('v.disabled', false);
        }
    },

    validateEmployment : function (component) {
        var validated = true;
        var bdata = component.find("basedatahelper");
        // Organization name is required
        var orgNameField = component.find("inputOrgName");
        if ($A.util.isEmpty(orgNameField.get("v.value"))) {
            validated = false;
            orgNameField.set("v.errors", [{message : $A.get("$Label.c.ATS_Organization_Name_is_required")}]);
        } else {
            orgNameField.set("v.errors", null);
        }

        var titleField = component.find("inputTitle");
        if ($A.util.isEmpty(titleField.get("v.value"))) {
            validated = false;
            //Rajeesh job title lookup story S-59000
            var titleFieldLookup = component.find("titleLookup");
            $A.util.addClass(titleFieldLookup,'slds-has-error');
            
            var titleErrSpan = component.find("inputTitleErrSpan");
            $A.util.removeClass(titleErrSpan,'slds-hide');

        } else {
            var titleFieldLookup = component.find("titleLookup");
            $A.util.removeClass(titleFieldLookup,'slds-has-error');
            var titleErrSpan = component.find("inputTitleErrSpan");          
            $A.util.addClass(titleErrSpan,'slds-hide');
            
        }

        var sDateField = component.find("inputStartDate");
        var eDateField = component.find("inputEndDate");
        //Rajeesh allowing users to empty an already filled in date. defect D-04884
        //var sDateExists = typeof sDateField.get('v.value') !== "undefined" && sDateField.get('v.value') !== "";
        //var eDateExists = typeof eDateField.get('v.value') !== "undefined" && eDateField.get('v.value') !== "";
        var sDateExists = typeof sDateField.get('v.value') !== "undefined";
        var eDateExists = typeof eDateField.get('v.value') !== "undefined";

        var sDateStr, eDateStr;

        if (sDateExists) {
            sDateStr = this.formatDate(sDateField.get('v.value'));
            if (sDateStr === false) {
                sDateField.set("v.errors", [{message : "Valid date format is " + this.getAcceptedDateFormat()}]);
                validated = false;
            } else {
                sDateField.set("v.errors", null);
            }
        }

        if (eDateExists) {
            eDateStr = this.formatDate(eDateField.get('v.value'));
            if (eDateStr === false) {
                eDateField.set("v.errors", [{message : "Valid date format is " + this.getAcceptedDateFormat()}]);
                validated = false;
            } else {
                eDateField.set("v.errors", null);
            }
        }

        if (sDateExists && eDateExists && sDateStr !== false && eDateStr !== false) {
            //Rajeesh allowing users to empty an already filled in date. defect D-04884
            //exclude blank string scenario.
            var sDate,eDate;
            if(sDateStr!=""){
                sDate = new Date(sDateStr);
            }
            else {
                sDate="";    
            }
            if(eDateStr!=""){
                eDate = new Date(eDateStr);
            }
            else{
                eDate="";
            }
            if ((eDate!=""&&sDate!="") && (eDate < sDate)) {//End Rajeesh
                validated = false;
                eDateField.set("v.errors", [{message : "End date should be after start date!"}]);
            } else {
                eDateField.set("v.errors", null);
            }
        }  
 
        var payrateField = component.find("inputRate");
        var payrate = payrateField.get("v.value");
        if (!$A.util.isEmpty(payrate) && payrate < 0) {
            validated = false;
            payrateField.set("v.errors", [{message : "Rate cannot be less than 0!"}]);
        } else {
            payrateField.set("v.errors", null);
        }

        var bonusField = component.find("inputBonus");
        var bonus = bonusField.get("v.value");
        if (!$A.util.isEmpty(bonus) && bonus < 0) {
            validated = false;
            bonusField.set("v.errors", [{message : "Bonus cannot be less than 0!"}]);
        } else {
            bonusField.set("v.errors", null);
        }

        var bonusPctField = component.find("inputBonusPctId");
        var bonusPct = bonusPctField.get("v.value");
        if (!$A.util.isEmpty(bonusPct) && bonusPct < 0) {
            validated = false;
            bonusPctField.set("v.errors", [{message : "Bonus percentage cannot be less than 0!"}]);
        } else {
            bonusPctField.set("v.errors", null);
        }

        var addlCompField = component.find("inputAddlCompId");
        var addlComp = addlCompField.get("v.value");
        if (!$A.util.isEmpty(addlComp) && addlComp < 0) {
            validated = false;
            addlCompField.set("v.errors", [{message : "Additional Compensation cannot be less than 0!"}]);
        } else {
            addlCompField.set("v.errors", null);
        }
        
        var CtypeField = component.find("CtypeId");
        var cType = CtypeField.get("v.value");
        if (((!$A.util.isEmpty(payrate)) && payrate > 0) && (cType == '' ||cType == undefined)) { 
            validated = false;
            CtypeField.set("v.errors", [{message : "Please select Currency."}]);
        } else {
            CtypeField.set("v.errors", null);
        }

        var rateFreqField = component.find("inputRateFrequency");
        var rateFreqValue = rateFreqField.get("v.value");
        if (((!$A.util.isEmpty(payrate)) && payrate > 0) && (rateFreqValue == '' || rateFreqValue == undefined)) { 
            validated = false;
            rateFreqField.set("v.errors", [{message : "Please select Rate Frequency."}]);
        } else {
            rateFreqField.set("v.errors", null);
        }

        return validated;
    },

    saveEmployment : function(component) {  
        var saveButton = component.find("saveButton");
        // saveButton.set("v.disabled", true);

        var sDateField = component.find("inputStartDate");
        var eDateField = component.find("inputEndDate");
        var sDateStr, eDateStr;
        //Rajeesh allowing users to empty an already filled in date. defect D-04884
        //if (typeof sDateField.get('v.value') !== "undefined" && sDateField.get('v.value') !== "") {
        if (typeof sDateField.get('v.value') !== "undefined"){
            if(sDateField.get('v.value') !== "") {
                sDateStr = this.formatDate(sDateField.get('v.value'));
            }
            else{
               sDateStr=null; 
            }
        }
        if (typeof eDateField.get('v.value') !== "undefined") {
            if(eDateField.get('v.value') !== ""){
            eDateStr = this.formatDate(eDateField.get('v.value'));
            }
            else{
                eDateStr=null;
            }
        }
        if(component.get("v.isClientAccountBlank"))
           component.set("v.employment.Client__c", ""); 
        var newEmpl = {"sobjectType" : "Talent_Experience__c",
                        "Type__c" : "Work",
                        "Talent__c" : component.get("v.talentId"),
                        "Client__c" : component.get("v.employment.Client__c"),
                        "Organization_Name__c" : component.get("v.employment.Organization_Name__c"),
                        "Title__c" : component.get("v.employment.Title__c"),
                        "Department__c" : component.get("v.employment.Department__c"),
                        // "Start_Date__c" : (typeof sDateStr === "undefined" || sDateStr === false ? "" : sDateStr),
                       //"End_Date__c" : (typeof eDateStr === "undefined" || eDateStr === false ? "" : eDateStr),
                       "Start_Date__c" : (typeof sDateStr === "undefined" || sDateStr === false ? null : sDateStr),
                       "End_Date__c" : (typeof eDateStr === "undefined" || eDateStr === false ? null : eDateStr),
                        "Reason_For_Leaving__c" : component.get("v.employment.Reason_For_Leaving__c"),
                        "Allegis_Placement__c" : component.get("v.employment.Allegis_Placement__c"),
                        "Current_Assignment__c" : component.get("v.employment.Current_Assignment__c"),
                        "Salary__c" : component.get("v.employment.Salary__c"),
                        "Payrate__c" : component.get("v.employment.Payrate__c"),
                       	"Daily_Rate__c" : component.get("v.employment.Daily_Rate__c"),
                        "Bonus__c" : component.get("v.employment.Bonus__c"),
                        "Bonus_Percentage__c" : component.get("v.employment.Bonus_Percentage__c"),
                        "Other_Compensation__c" : component.get("v.employment.Other_Compensation__c"),
                        "Notes__c" : component.get("v.employment.Notes__c"),
                        "Currency__c": component.find("CtypeId").get("v.value"),
                        "Rate_Frequency__c": component.get("v.employment.Rate_Frequency__c"),
                        "Compensation_Total__c": component.get("v.employment.Compensation_Total__c"),
                        "Id" : component.get("v.employment.Id")};
		
        component.set("v.isClientAccountBlank", false); // Reset to false
        //Rajeesh allowing users to empty an already filled in date. defect D-04884                    
        /*if(newEmpl.Start_Date__c == ''){
            delete newEmpl.Start_Date__c;
        }

        if(newEmpl.End_Date__c == ''){
            delete newEmpl.End_Date__c;
        }*/
        var params = {"talentExperience" : JSON.stringify(newEmpl)};
        var bdata = component.find("basedatahelper");
        bdata.callServer(component,'ATS','','saveTalentExperience',function(response){
            if(component.isValid()){
                var userevent = $A.get("e.c:E_TalentEmploymentSaved");
                userevent.setParams({"recordId" : component.get("v.recordId"), 
                					"talentId" : component.get("v.talentId")});
                userevent.fire();
                var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
                updateLMD.fire();

                saveButton.set("v.disabled", false);
            }
        },params,false, function(response) {
        	saveButton.set("v.disabled", false);
        });
    },
	getEmploymentToEdit : function(component) {
         
         var params = {"soql": "select Talent__c, Client__c, Organization_Name__c, Title__c, Department__c, Start_Date__c,End_Date__c, Reason_For_Leaving__c, Allegis_Placement__c, Current_Assignment__c, Salary__c, Payrate__c, Daily_Rate__c, Bonus__c, Bonus_Percentage__c, Other_Compensation__c, Notes__c,Currency__c, Rate_Frequency__c,Compensation_Total__c  from Talent_Experience__c where Id = '" + component.get("v.employment.Id") + "'"};
         var bdata = component.find("basedatahelper");
         bdata.callServer(component,'','',"getRecord",function(response){
             component.set("v.employment", response);
             // console.log ("*****Employement Edit function******");
             // console.log (response);
             component.set("v.employment.Currency__c", response.Currency__c);
             component.set("v.employment.Rate_Frequency__c", response.Rate_Frequency__c);
            
         },params,false);
    },
    
    updateOrgName : function(component, event) {
        var orgNameField = component.find("inputOrgName");
        orgNameField.set("v.value", event.getParam("value"));
        orgNameField.set("v.disabled", true);
    },

    clearAndEnableOrgName : function(component, event) {
        var orgNameField = component.find("inputOrgName");
        orgNameField.set("v.value", "");
        orgNameField.set("v.disabled", false);
    },

    updatePayrate : function (component) {
        var payrate =  component.get("v.employment.Salary__c") / this.HOURLY_TO_ANNUAL_FACTOR;
        component.set("v.employment.Payrate__c", payrate);
        component.set("v.payRateOldV", payrate);

    },

    updateHourlyRate: function (component){
      var dailyRate = component.find("dailyRateInput").get("v.value");
      var newHourlyRate = dailyRate / 8; 
      component.find("inputPayrate").set("v.value", newHourlyRate);
      
    },
    
    preventNonNumericKeys: function(component, event){
        // var charCode = (window.event.which) ? window.event.which : window.event.keyCode;
        var charCode = event.getParams().keyCode;
        if (charCode < 47 || charCode > 57){
            event.preventDefault();
        }
    },

    calculateBonusPct:function(cmp) {
        var bonus = (cmp.find("inputBonus").get("v.value") != null ? cmp.find("inputBonus").get("v.value") : 0);
            bonus = parseFloat(bonus).toFixed(2);
        var paytype = cmp.find("inputRateFrequency").get("v.value"); 
        var pay = (cmp.find("inputRate").get("v.value") != null ? cmp.find("inputRate").get("v.value") : 0);
        
        
        if(paytype != null && paytype != ""){
            if(paytype == 'Hourly'){
                pay = (pay * this.HOURLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'Daily'){
                pay = (pay * this.DAILY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'Weekly'){
                pay = (pay * this.WEEKLY_TO_ANNUAL_FACTOR); 
            }
            else if(paytype == 'Monthly'){
                pay = (pay * this.MONTHLY_TO_ANNUAL_FACTOR); 
            }
            pay = (pay != 0 ? (parseInt(bonus)/parseInt(pay))*100 : 0);
            pay = parseFloat(pay).toFixed(2);
            
            cmp.find("inputBonusPctId").set("v.value",pay) ;
        }
        
    },
    calculateBonusAmount:function(cmp) {
        var bonus = (cmp.find("inputBonusPctId").get("v.value") != null ? cmp.find("inputBonusPctId").get("v.value") : 0);
            bonus = parseFloat(bonus).toFixed(2);
        var paytype = cmp.find("inputRateFrequency").get("v.value");
        var pay  = (cmp.find("inputRate").get("v.value") != null ? cmp.find("inputRate").get("v.value") : 0);
              
        if(paytype != null && paytype != ""){
            if(paytype == 'Hourly'){
                pay = (pay * this.HOURLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'Daily'){
                pay = (pay * this.DAILY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'Weekly'){
                pay = (pay * this.WEEKLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'Monthly'){
                pay = (pay * this.MONTHLY_TO_ANNUAL_FACTOR); 
            }
            pay = (pay * bonus)/100 ;
            
            cmp.find("inputBonusPctId").set("v.value",bonus);
            cmp.find("inputBonus").set("v.value",pay);
        }
        
    },
    calculateTotal:function(cmp) {
        var paytype = cmp.find("inputRateFrequency").get("v.value");
        var pay = (((cmp.find("inputRate").get("v.value") != null) && (cmp.find("inputRate").get("v.value") != "")) ? cmp.find("inputRate").get("v.value") : 0);
        var bonus = (((cmp.find("inputBonus").get("v.value") != null) && (cmp.find("inputBonus").get("v.value") != ""))  ? cmp.find("inputBonus").get("v.value") : 0);
        var addl = (((cmp.find("inputAddlCompId").get("v.value") != null) && (cmp.find("inputAddlCompId").get("v.value") != "")) ? cmp.find("inputAddlCompId").get("v.value") : 0);  // adding this step to check for blank the addl bonus to prevent NaN issue. 
            
        if(paytype != null && paytype != "Select"){
            if(paytype == 'Hourly'){
                pay = pay * this.HOURLY_TO_ANNUAL_FACTOR; 
            }else if(paytype == 'Daily'){
                pay = pay * this.DAILY_TO_ANNUAL_FACTOR; 
            }else if(paytype == 'Weekly'){
                pay = (pay * this.WEEKLY_TO_ANNUAL_FACTOR); 
            }else if(paytype == 'Monthly'){
                pay = (pay * this.MONTHLY_TO_ANNUAL_FACTOR); 
            }
            bonus = (isNaN(bonus)) ? 0 : bonus;
            addl = (isNaN(addl)) ? 0 : addl;
            pay = parseFloat(pay) + parseFloat(bonus) + parseFloat(addl);
            pay = parseFloat(pay).toFixed(2)
            cmp.set("v.employment.Compensation_Total__c",pay);  
        }
    },
    formatDate:function(inputDate){
        var input = inputDate;
        if(input){
             // Typed in dates only work for "/" as delimiter.
            var arr = input.split("/");
            if(arr.length === 3){
                // Pad "0" for single digit date / month
                if(arr[0].length === 1){
                   arr[0] = '0' + arr[0];
                }
                if(arr[1].length === 1){
                   arr[1] = '0' + arr[1];
                }

                // Convert to "yyyy-mm-dd" based on user locale.
                var language = window.navigator.userLanguage || window.navigator.language;
                if(language === 'en-US'){
                    input = arr[2] + '-' + arr[0] + '-' + arr[1];
                }else{
                    input = arr[2] + '-' + arr[1] + '-' + arr[0];
                }
            } 

            // Adjust for timezone since the new Date() constructor sets to UTC midnight.
            var now = this.adjustDateForTimezone(input);
            if (now === false || isNaN(now.getTime())) {
                return false;
            } else {
                return now.getFullYear()+"-"+(now.getMonth() + 1) +"-"+now.getDate();
            } 
        }else{
            return '';
        }
       
    }
    ,formatDateTime:function(inputDateTime){
        var output = inputDateTime;

        if(inputDateTime){

            // Split the input string on space
            var arr = inputDateTime.split(' ');
           

            // Works only with the standard date format. Spaces in date will break this.
            if(arr.length >= 2){
                // Call function to format date part of the string.
                var sDate = this.formatDate(arr[0]);

                // Split time part of the string by colon. 
                var hhmm = arr[1].split(":");
                var hrs, mins, ampm;
                ampm = '';
                if (hhmm.length >= 2) {
                    // Pad single digit hours with "0".
                    hrs = hhmm[0].length === 1 ? "0" + hhmm[0] : hhmm[0];
                    // If am/pm is not separated from time by space, use substring.
                    mins = hhmm[1].substring(0, 2);
                    if (hhmm[1].length > 2) {
                        ampm = hhmm[1].substring(2, hhmm[1].length);
                    }
                }
                // If am/pm separated from time by space
                if (arr.length === 3) {
                    ampm = arr[2];
                }
                // Adjust to 24 hour clock
                if (ampm.toLowerCase() === "pm" && hrs !== "12") {
                    hrs = parseInt(hrs) + 12;
                } else if (ampm.toLowerCase() === "am" && hrs === "12") {
                    hrs = "00";
                }

                // Initialize Date object from generated date and time strings.
                var sTime = " " + hrs + ":" + mins + ":00.000Z";

                // Initialize date and readjust for timezone because previous parsing steps set it to UTC
                var tDate = new Date(sDate + sTime);
                var theDate = new Date(tDate.getTime() + (tDate.getTimezoneOffset() * 60 * 1000));

                // Check for invalid date format. Return boolean false if invalid.
                if (!isNaN(theDate.getTime())) {
                    output = theDate.toISOString();
                } else {
                    output = false;
                }
            } else {
                // Datepicker was used, so input is ISO string and passed right back as output.
                output = inputDateTime;
            }
        }
        return output;
    },

    getAcceptedDateFormat : function () {
        var language = window.navigator.userLanguage || window.navigator.language;
        return language === "en-US" ? "mm/dd/yyyy" : "dd/mm/yyyy";
    },

    getAcceptedTimeFormat : function () {
        return "hh:mm AM/PM";
    },

    adjustDateForTimezone : function (inputDate) {
        var theDate = new Date(inputDate);
        if (!isNaN(theDate.getTime())) {
            // Use same date to get the timezone offset so daylight savings is factored in.
            var output = new Date(theDate.getTime() + (theDate.getTimezoneOffset() * 60 * 1000));
            return output;
        } else {
            return false;
       }
    },
      getAccountId: function(cmp){
        var recordID = cmp.get("v.recordId");
        var bdata = cmp.find("basedatahelper");
        var actionParams = {'recordId':recordID};
        bdata.callServer(cmp,'ATS','TalentHeaderFunctions','getTalentHeaderModel',function(response){
            cmp.set("v.record", response);
            if(typeof response.contact.AccountId !== 'undefined'){
                    cmp.set("v.talentId",response.contact.AccountId);
                   // console.log(cmp.get("v.talentId")); 
                }
        },actionParams,false);

     }

})