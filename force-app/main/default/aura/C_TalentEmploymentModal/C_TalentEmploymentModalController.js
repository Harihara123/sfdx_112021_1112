({
    /*onInit: function(component, event, helper){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        component.set("v.runningUser", userId); 
        var contact = cmp.get("v.contactRecord");
        component.set("v.talentId", contact.fields.AccountId.value);
        console.log("TALENT ID EMP MODAL ", component.get("v.talentId"));
    },*/

    initAddEmploymentForm : function(component, event, helper) {
        //ak changes component.set('v.modalTitle','Add Employment');
		if(component.get("v.modalTitle") === "Edit Employment" ){
            component.set("v.modalTitle",$A.get("$Label.c.ATS_EDIT_EMPLOYMENT"));
			helper.initEditEmploymentForm(component,event);
		}else{
           component.set("v.modalTitle",$A.get("$Label.c.ATS_ADD_EMPLOYMENT"));
		   helper.initAddEmploymentForm(component, event);
		}
        
    },
	initEditEmploymentForm : function(component, event, helper) {
       component.set('v.modalTitle','Edit Employment');
       
    },

	hideModal : function(component, event, helper) {
	   component.destroy();
		/*ak changes if (typeof event.getParam("talentId") === "undefined" 
			|| (component.get("v.talentId") === event.getParam("talentId"))) {
	        helper.destroyForm(component);
	        //Toggle CSS styles for hiding Modal
			helper.toggleClassInverse(component,'backdropAddEmploymentDetail','slds-backdrop--');
			helper.toggleClassInverse(component,'modaldialogAddEmploymentDetail','slds-fade-in-');
		}*/
    }

})