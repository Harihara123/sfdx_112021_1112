({
    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },

    showModal : function (component) {
        this.toggleClass(component,'backdropAddEmploymentDetail','slds-backdrop--');
        this.toggleClass(component,'modaldialogAddEmploymentDetail','slds-fade-in-');
    },

    initAddEmploymentForm : function (component, event) {

        var componentName = "c:C_TalentEmploymentForm";
        var componentParams = {//"runningUser" : event.getParam("runningUser"),
                                "aura:id" : "addEmploymentForm",
                                "formCanceledEvent" : component.getReference("c.hideModal"),
                                "talentId" : component.get("v.talentId")};
       // component.set("v.talentId",event.getParam("talentId"));
        var self = this;
        var cmp = component;
        $A.createComponent(componentName, componentParams, 
            function(newComponent, status, errorMessage) {
                if (status === "SUCCESS") {
                    // SUCCESS  - insert component in the body
                    var body = cmp.get("v.body");
                    body.push(newComponent);
                    cmp.set("v.body", body);

                    self.showModal(cmp);
                    cmp.set("v.formComponentRef", newComponent);

                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
        });
    },
    
    initEditEmploymentForm : function (component,event) {
        //ak changes var itemId = event.getParam("itemID");
        var componentName = "c:C_TalentEmploymentForm";
        var componentParams = {//"runningUser" : component.get("v.runningUser"),
                                "aura:id" : "editEmploymentForm",
                                "formCanceledEvent" : component.getReference("c.hideModal"),
                               	"employmentId" : component.get("v.itemID"),
                               	"edit" : true,
                                "talentId" : component.get("v.talentId")};
        var self = this;
        $A.createComponent(componentName, componentParams, 
            function(newComponent, status, errorMessage) {
                if (status === "SUCCESS") {
                    // SUCCESS  - insert component in the body
                    var body = component.get("v.body");
                    body.push(newComponent);
                    component.set("v.body", body);

                    self.showModal(component);
                    component.set("v.formComponentRef", newComponent);

                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
        });
    },

    destroyForm : function (component) {
        var formComponent = component.get("v.formComponentRef");
        if (typeof formComponent !== "undefined" && formComponent !==null) {
            formComponent.destroy();
        }
    }

})