({
    
    onInit : function(cmp, event, helper) {
        const contact = cmp.get("v.contactRecord");

        if(cmp.get("v.readOnly")){
            cmp.set("v.expandAll", true);
            cmp.set("v.enableEdit", false);
            cmp.set("v.enableExpand", false);
        }

        if(!cmp.get("v.talentId")){
            cmp.set("v.talentId", contact.fields.AccountId.value);
        }

        helper.getResults(cmp,'init');

    },
    
    showCandidateEmployment : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var recordID = component.get("v.talentId");
        urlEvent.setParams({
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Employment_Details?recordId=" + recordID,
            "isredirect":true
        });
        urlEvent.fire(); 
    },
    
    showEmploymentModal : function(cmp, event, helper) {
        var trg = cmp.get("v.modalTrigger");
        cmp.set("v.modalTrigger", !trg);
        
        
    },
    
    addEmploymentEvent : function(cmp, event, helper) {
        var userevent = $A.get("e.c:E_TalentEmploymentAdd");
        var recordID = cmp.get("v.talentId");
        userevent.setParams({"recordId" : recordID});
        userevent.fire();
        
    },
    refreshEmploymentTimeline : function(cmp, event, helper) {
        helper.getResults(cmp,'refresh');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Employment record added successfully.",
            "type": "success"
        });
        toastEvent.fire();
    },
    
    reloadData : function(cmp, event, helper) {
        helper.getResults(cmp,'refresh');
    },
    expandCollapse : function(cmp,event,helper){
        var expanded = cmp.get("v.expanded");
        if(expanded){
            cmp.set("v.expanded", false);
        }else{
            cmp.set("v.expanded", true);
        }
    },
    
    expandCollapseAll : function(cmp,event,helper){
        var expanded = cmp.get("v.expandAll");
        cmp.set("v.expandAll", !expanded);
    },
    
    selectViewMenuItem : function(component, event, helper) {
        helper.selectViewMenuItem(component);
        
    }, 
    handleClick : function (cmp, event, helper) {
        var buttonstate = cmp.get('v.expandAll');
        cmp.set('v.expandAll', !buttonstate);
    }
})