({
    getResults : function(cmp,mode){
        // var listView = cmp.get("v.viewList");
        var self = this;
        
        var bdata = cmp.find("basedatahelper");
        var recordID = cmp.get("v.talentId");
        var arr = cmp.get("v.viewListFunction").split('.');
        var className = arr[0];
        var subClassName = arr[1];
        var methodName = arr[2];
		// S-82159:Added personaIndicator parameter to params by Siva 12/4/2018
        var params  = {'recordId':recordID, "maxRows": cmp.get("v.maxRows"), "personaIndicator":"Recruiter"};
		// var self = this;
        
        bdata.callServer(cmp,className,subClassName,methodName,function(response){
            // var result = response.AWHList;
            var viewList = [];

            viewList.push({label:'All Work History', value: 'AllWorkHistory', recordcount:response.AWHCount,records:response.AWHList },
                          {label:'Allegis Work History', value: 'AllegisWorkHistory', recordcount:response.AGWHCount,records:response.AGWHList });
            cmp.set("v.viewList",viewList);
            if(mode === 'init'){
                var Ucat = response.Ucategory;

                cmp.set("v.runningUserCategory",response.Ucategory);
                if((Ucat === 'CATEGORY1' || Ucat == 'CATEGORY3')&&(response.AGWHCount !== 0)){
                    cmp.set("v.viewListIndex",1);
                }
            }
            var index = cmp.get("v.viewListIndex");

            cmp.set("v.records", viewList[index].records);
            cmp.set("v.recordCount",viewList[index].recordcount);
			self.updateInsights(cmp);
            
        },params,false);
        
    },
    selectViewMenuItem : function(cmp) {
        var index = cmp.get("v.viewListIndex"); 
        if(index === 0){
            index = 1; 
        }else{
            index = 0; 
        }
        cmp.set("v.viewListIndex",index);
        cmp.set("v.records",cmp.get("v.viewList")[index].records);
    }
    ,showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    }, 
    
    buildEmploymentDateRange : function(workExItem) {
        var output = "";
        var sDate = workExItem.EmploymentStartDate;
        var eDate = workExItem.EmploymentEndDate;
        
        if ((typeof sDate === "undefined" || sDate == null) 
            && (typeof eDate === "undefined" || eDate == null)) {
            output = "No Date Information";
        } else if (typeof sDate === "undefined" || sDate == null) {
            output = "No Start Date - " + $A.localizationService.formatDate(eDate, "MMM yyyy");
        } else if (typeof eDate === "undefined" || eDate == null) {
            output = $A.localizationService.formatDate(sDate, "MMM yyyy") + " - No End Date";
        } else {
            var sdt = new Date(sDate);
            var edt = new Date(eDate);
            output = $A.localizationService.formatDate(sDate, "MMM yyyy") + " - " + $A.localizationService.formatDate(eDate, "MMM yyyy");
            
            var dur = $A.localizationService.duration(edt.getTime() - sdt.getTime());
            var yrs = $A.localizationService.getYearsInDuration(dur);
            var months = $A.localizationService.getMonthsInDuration(dur);
            
            var duration = ' (' 
            + (yrs > 0 ? yrs + (yrs == 1 ? ' Year ' : ' Years ') : '') 
            + (months > 0 ? months + (months == 1 ? ' Month' : ' Months') : '') 
            + ')';
            if (duration != ' ()') {
                output += duration;
            }
            // console.log(sDate + " - " + eDate + " / " + output);
        }
        return output;
    },
    updateInsights: function(component){
        var empEvent = $A.get("e.c:E_UpdateTalentInsightsEmployment");
        empEvent.setParams({
            "recordId": component.get("v.recordId"),
            "talentEmployment": component.get("v.records")
        }); 
        empEvent.fire();
    }, 
    
})