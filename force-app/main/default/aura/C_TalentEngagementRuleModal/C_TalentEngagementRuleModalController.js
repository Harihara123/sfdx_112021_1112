({
    displayERDetail: function(component, event, helper) {
        /* ak changes 
		component.set("v.empId", event.getParam("recordId"));
        component.set("v.Name", event.getParam("name"));
        component.set("v.BillingAddress", event.getParam("street"));
        component.set("v.BillingCity", event.getParam("city"));
        component.set("v.BillingState", event.getParam("state"));
        component.set("v.BillingCountry", event.getParam("country"));*/
        component.find("scopeTypeId").set("v.value", "--None--");
        component.find("scopeTypeId").set("v.errors", null);
        component.find("startdateId").set("v.value", null);
        component.find("startdateId").set("v.errors", null);
        component.find("expdateId").set("v.value", null);
        component.find("expdateId").set("v.errors", null);
        component.find("descId").set("v.value", null);
        component.find("descId").set("v.errors", null);
        //document.getElementById("errorDivId").style.display = "none";
        
        helper.toggleClass(component,'backGroundSectionId','slds-backdrop--');
		helper.toggleClass(component,'newERSectionId','slds-fade-in-');
        helper.popScope(component, component.get("v.empId"));
        
        
    },
        
    closeModal: function(component, event, helper) {
	   component.destroy();
       /* ak changes 
	    helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop--');
		  helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-'); */
    },
    
     saveER: function(component, event, helper) {
        document.getElementById("errorDivId").style.display = "none";
        var bdata = component.find("basedatahelper");
         //$A.localizationService.formatDate(activityList[i].LastModifiedDate, 'MM-DD-YYYY')
         /*if(helper.validateForm(component)){
            var params = {sd: helper.formatDate(component.get("v.StartDate")),
                          ed: helper.formatDate(component.get("v.ExpDate")),
                          sc: component.get("v.Scope"),
                          des: component.get("v.Description"),
                          recId: component.get("v.empId")};*/
if(helper.validateForm(component)){
            var params = {sd: $A.localizationService.formatDate(component.get("v.StartDate"), 'YYYY-MM-DD'),
                          ed: $A.localizationService.formatDate(component.get("v.ExpDate"), 'YYYY-MM-DD'),
                          sc: component.get("v.Scope"),
                          des: component.get("v.Description"),
                          recId: component.get("v.empId")};                          


            bdata.callServer(component,'ATS','TalentEngagementFunctions','createER',function(response){
                if(component.isValid()){
                    
                    helper.toggleClassInverse(component,'backGroundSectionId','slds-backdrop--');
                    helper.toggleClassInverse(component,'newERSectionId','slds-fade-in-');
                    var appEvent = $A.get("e.c:E_TalentSummaryUpdateDNR");
                    appEvent.fire();
                }
             },params,false);   

         }    
    }
    
})