({
	toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
		$A.util.removeClass(modal,className+'hide');
		$A.util.addClass(modal,className+'open');
	},
    toggleClassInverse: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.addClass(modal,className+'hide');
		$A.util.removeClass(modal,className+'open');
	},

    
    validateForm: function(component) {
        
        // Simplistic error checking
        var validER = true;
        var bdata = component.find("basedatahelper");
        
        var scopeField = component.find("scopeTypeId");
        var sc = scopeField.get("v.value");
        if (sc == "--None--"){
            validER = false;
            scopeField.set("v.errors", [{message:"{!$Label.c.ATS_Scope_Error}"}]);
        }else{
            scopeField.set("v.errors", null);
        }
        
        
        var sdField = component.find("startdateId");
        // var sd = this.formatDate(sdField.get("v.value"));
        var sd = $A.localizationService.formatDate(sdField.get("v.value"), 'YYYY-MM-DD');
        if ($A.util.isEmpty(sd)){
            validER = false;
            sdField.set("v.errors", [{message:"{!$Label.c.ATS_StartDate_Error}"}]);
        }else{
            sdField.set("v.errors", null);
        }
       
        
        var edField = component.find("expdateId");
        // var ed = this.formatDate(edField.get("v.value"));
        var ed = $A.localizationService.formatDate(edField.get("v.value"), 'YYYY-MM-DD');
        if ($A.util.isEmpty(ed)){
            validER = false;
            edField.set("v.errors", [{message:"{!$Label.c.ATS_EndDate_Error}"}]);
        }else{
            edField.set("v.errors", null);
        }
        return(validER);
    },
    
    popScope: function(component){
        var params = null;
        var bdata = component.find("basedatahelper");
        bdata.callServer(component,'ATS','TalentEngagementFunctions','getScopeTypeList',function(response){
            if(component.isValid()){
                var scopeType = [];
                var scopeTypeMap = response;
                for (var key in scopeTypeMap ) {
                    scopeType.push({value:scopeTypeMap[key], key:key});
                }
                component.set("v.scopeTypeList", scopeType);
            }
         },params,false);   
    }
})