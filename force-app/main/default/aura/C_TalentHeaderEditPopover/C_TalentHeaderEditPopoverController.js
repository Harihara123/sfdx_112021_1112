({
	doInit: function (component, event, helper) {
		helper.init(component);
	},

	saveTalentInfo: function (component, event, helper) {
		helper.validateAndSave(component);
	},
	saveButtonDisble: function (component, event, helper) {
	var linkedinUrlField = component.find("LinkedInURL");
        if(linkedinUrlField && linkedinUrlField.get("v.value")!=null && linkedinUrlField.get("v.value").length>0){
		  component.set("v.disabled", false);
		  }
		  else
		  {
		   component.set("v.disabled", true);
		  }
	}
	,

	toggleEditPopover: function(component, event, helper){

		if(component.get("v.recordType") === 'Phone'){
			helper.addRemoveEventListener(component, event, 'phonePopOverId', 'PhonePopOverCls', 'phonePopOver');		
		}
		if(component.get("v.recordType") === 'Email'){
			helper.addRemoveEventListener(component, event, 'emailPopOverId', 'EmailPopOverCls', 'emailPopOver');		
		}

	if(component.get("v.recordType") === 'LinkedInURL'){
			helper.addRemoveEventListener(component, event, 'linkdinPopOverId', 'LinkdinPopOverCls', 'linkdinPopOver');		
		}
	},

	closePopover: function(component, event, helper){
        helper.clearPrefOptions(component);
		helper.togglePopover(component);
		//component.set("v.togglePopover", false);
    },

	onMobilePhoneCheck : function(component, event, helper) {
		var checkCmp = component.find("homePhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("workPhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("otherPhonepreferred");
		checkCmp.set("v.value", false);  
	},

	onHomePhoneCheck : function(component, event, helper) {
		var checkCmp = component.find("mobilePhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("workPhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("otherPhonepreferred");
		checkCmp.set("v.value", false);    
	},

	onWorkPhoneCheck : function(component, event, helper) {
		var checkCmp = component.find("mobilePhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("homePhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("otherPhonepreferred");
		checkCmp.set("v.value", false);    
	},
    
	onOtherPhoneCheck : function(component, event, helper) {
		var checkCmp = component.find("mobilePhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("homePhonepreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("workPhonepreferred");
		checkCmp.set("v.value", false);    
	},  

	onEmailCheck : function(component, event, helper) {
		var checkCmp = component.find("otherEmailPreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("workEmailPreferred");
		checkCmp.set("v.value", false);  
	},

	onOtherEmailCheck : function(component, event, helper) {
		var checkCmp = component.find("emailPreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("workEmailPreferred");
		checkCmp.set("v.value", false);  
	},

  
	onWorkEmailCheck : function(component, event, helper) {
		var checkCmp = component.find("emailPreferred");
		checkCmp.set("v.value", false);
		checkCmp = component.find("otherEmailPreferred");
		checkCmp.set("v.value", false);  
	}  
})