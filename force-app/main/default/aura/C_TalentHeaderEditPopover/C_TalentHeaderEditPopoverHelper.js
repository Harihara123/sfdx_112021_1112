({
	init : function(component) {

        if (component.get('v.useController') && component.get('v.showPopover')) {

            var serverMethod = 'c.getTalentById';
            var params = {
                "recordId": component.get("v.recordId")
            };

            var action = component.get(serverMethod);
            action.setParams(params);
            action.setCallback(this,function(response){
                var state = response.getState();
                if (state === "SUCCESS") {

                    let talentData = response.getReturnValue();
                    component.set('v.contactRecordFields', talentData)

                    if (talentData) {
                        this.setDNCFields(component, talentData);
                        if (talentData.Account) {
                            component.set('v.accountRecordFields', talentData.Account);
                        }
                        if (talentData.Preferred_Phone__c) {
                            if (talentData.Preferred_Phone__c === 'Home') {
                                component.set("v.homePhonePreferred", true);
                            } else if (talentData.Preferred_Phone__c === 'Mobile') {
                                component.set("v.mobilePhonePreferred", true);
                            } else if (talentData.Preferred_Phone__c === 'Work') {
                                component.set("v.workPhonePreferred", true);
                            } else if (talentData.Preferred_Phone__c === 'Other') {
                                component.set("v.otherPhonePreferred", true);
                            }
                        }
                        if (talentData.Preferred_Email__c) {
                            if (talentData.Preferred_Email__c === 'Email') {
                                component.set("v.emailPreferred", true);
                            } else if (talentData.Preferred_Email__c === 'Alternate') {
                                component.set("v.otherEmailPreferred", true);
                            } else if (talentData.Preferred_Email__c === 'Work') {
                                component.set("v.workEmailPreferred", true);
                            }
                        }

                    }
                }
            })
            $A.enqueueAction(action);
        }
        else {
            var conrecord = component.get("v.contactRecordFields");
            console.log('conrecord-->'+JSON.stringify(conrecord));

            if (conrecord) {
                this.setDNCFields(component, conrecord);
                if (conrecord.Account) {
                    component.set('v.accountRecordFields', conrecord.Account);
                }

                if (conrecord.Preferred_Phone__c) {
                    if (conrecord.Preferred_Phone__c === 'Home') {
                        component.set("v.homePhonePreferred", true);
                    } else if (conrecord.Preferred_Phone__c === 'Mobile') {
                        component.set("v.mobilePhonePreferred", true);
                    } else if (conrecord.Preferred_Phone__c === 'Work') {
                        component.set("v.workPhonePreferred", true);
                    } else if (conrecord.Preferred_Phone__c === 'Other') {
                        component.set("v.otherPhonePreferred", true);
                    }
                }

                if (conrecord.Preferred_Email__c) {
                    if (conrecord.Preferred_Email__c === 'Email') {
                        component.set("v.emailPreferred", true);
                    } else if (conrecord.Preferred_Email__c === 'Alternate') {
                        component.set("v.otherEmailPreferred", true);
                    } else if (conrecord.Preferred_Email__c === 'Work') {
                        component.set("v.workEmailPreferred", true);
                    }
                }
            } 
        }
		// var conrecord = component.get("v.contactRecordFields");
        // console.log('conrecord-->'+JSON.stringify(conrecord));
        //
		// if (conrecord) {
        //     this.setDNCFields(component, conrecord);
        //     if (conrecord.Account) {
        //         component.set('v.accountRecordFields', conrecord.Account);
        //     }
        //
		// 	if (conrecord.Preferred_Phone__c) {
		// 		if (conrecord.Preferred_Phone__c === 'Home') {
		// 			component.set("v.homePhonePreferred", true);
		// 		} else if (conrecord.Preferred_Phone__c === 'Mobile') {
		// 			component.set("v.mobilePhonePreferred", true);
		// 		} else if (conrecord.Preferred_Phone__c === 'Work') {
		// 			component.set("v.workPhonePreferred", true);
		// 		} else if (conrecord.Preferred_Phone__c === 'Other') {
		// 			component.set("v.otherPhonePreferred", true);
		// 		}
		// 	}
        //
		// 	if (conrecord.Preferred_Email__c) {
		// 		if (conrecord.Preferred_Email__c === 'Email') {
		// 			component.set("v.emailPreferred", true);
		// 		} else if (conrecord.Preferred_Email__c === 'Alternate') {
		// 			component.set("v.otherEmailPreferred", true);
		// 		} else if (conrecord.Preferred_Email__c === 'Work') {
		// 			component.set("v.workEmailPreferred", true);
		// 		}
		// 	}
		// }
	},

	validateAndSave : function(component) {
		if(this.validTalent(component)){
			this.saveTalent(component);    
		}
	},

	validTalent: function(component) {
	    let type = component.get('v.recordType');
        var validTalent = true;
		validTalent = this.validatePrefOptions (component, validTalent);
		validTalent = this.validatePhoneEmailLinkedIn(component, validTalent);
        if (['Phone', 'Email'].includes(type)) {
		    validTalent = component.find('talentDNC').validTalent();
        }
		validTalent = this.saveLinkedIn(component, validTalent);
        return validTalent;
    },

	saveTalent : function(component) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
        $A.util.removeClass(spinner, 'slds-hide'); 
        
        var contact = component.get("v.contactRecordFields");
        let account = component.get("v.accountRecordFields");
        if (['Phone', 'Email'].includes(component.get('v.recordType'))) {
        account = this.saveDNCFields(component, account);
        }
		contact = this.updatePreferredValues(component, contact);
		var params = {contact, account};
        var action = component.get("c.saveTalentInlineEdit");
        action.setParams(params);
            
        action.setCallback(this,function(response) {
            if (response.getState() === "SUCCESS") {
				var compEvent = component.getEvent("refreshTalentHeaderComponent");
				compEvent.fire();
				
                this.showToast('Success','Record has been saved.', 'success');
                this.togglePopover(component);
				this.clearPrefOptions(component);       
            } 
            else if (response.getState() === "ERROR") {
			var errors = response.getError();
			if (errors[0].message.includes("LinkedIn_URL__c")) {
                var linkedinUrlField = component.find("LinkedInURL");
                linkedinUrlField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_LINKEDIN_URL")}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");
			}
			else
			{
			this.showToast('Error','Something went wrong, please try again.', 'error');
			}
            }
			$A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');  
        });
        $A.enqueueAction(action);
    },

    showToast: function(title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },

	validatePrefOptions : function(component, validTalent) {
        if (component.get("v.homePhonePreferred") === true) {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                var homePhone = homePhoneField.get("v.value");
                if(homePhone === "" || !homePhone){
                    validTalent = false;
                    homePhoneField.set("v.errors", [{message: "Please enter Home Phone."}]);
                    homePhoneField.set("v.isError",true);
                    $A.util.addClass(homePhoneField,"slds-has-error show-error-message");
                }
                else {
                    homePhoneField.set("v.errors", []);
                    homePhoneField.set("v.isError", false);
                    $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                homePhoneField.set("v.errors", []);
                homePhoneField.set("v.isError", false);
                $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.mobilePhonePreferred") === true) {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                var mobilePhone = mobilePhoneField.get("v.value");
                if(mobilePhone === "" || !mobilePhone){
                    validTalent = false;
                    mobilePhoneField.set("v.errors", [{message: "Please enter a Mobile Phone."}]);
                    mobilePhoneField.set("v.isError",true);
                    $A.util.addClass(mobilePhoneField,"slds-has-error show-error-message");
                }
                else {
                    mobilePhoneField.set("v.errors", []);
                    mobilePhoneField.set("v.isError", false);
                    $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                mobilePhoneField.set("v.errors", []);
                mobilePhoneField.set("v.isError", false);
                $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.workPhonePreferred") === true) {
            var workPhoneField = component.find("workPhone");
			var workPhoneValMessage = "Please enter a valid Work Phone.";
            if(workPhoneField){
                var workPhone = workPhoneField.get("v.value");
                if(workPhone === "" || !workPhone){
                    validTalent = false;
                    workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
                    workPhoneField.set("v.isError",true);
                    $A.util.addClass(workPhoneField,"slds-has-error show-error-message");	
			    }
				var workExtensionField = component.find("workExtension");
				    if (workExtensionField){
						var workExtension = workExtensionField.get("v.value");
						if((workExtension != null && workPhone === null) || (workExtension != null && workPhone === "")){
							workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
							workPhoneField.set("v.isError",true);
							validTalent = false;
							}
						
						else {
							workExtensionField.set("v.errors", null);
							workExtensionField.set("v.isError",false);
						}
					}
                else {
                    workPhoneField.set("v.errors", []);
                    workPhoneField.set("v.isError", false);
                    $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
                }    
            }
        }
		else if(component.get("v.workPhonePreferred") === false) {
            var workPhoneField = component.find("workPhone");
			var workPhoneValMessage = "Please enter a valid Work Phone.";
            if(workPhoneField){
                var workPhone = workPhoneField.get("v.value");
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if((workExtension != null && workPhone === null) || (workExtension != null && workPhone === "")){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						}
					
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			}
		}
        else {
            var workPhoneField = component.find("workPhone");
            if(workPhoneField){
                workPhoneField.set("v.errors", []);
                workPhoneField.set("v.isError", false);
                $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
            
            }
        }
        


		/*
		
			workPhone : function (component, workPhone){
	 var workPhoneField = component.find("workPhone");
			if(workPhoneField){
				var workPhone = workPhoneField.get("v.value");
				var workPhoneValMessage = "Please enter a valid Work Phone.";
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if(workExtension != '' && workPhone == '' ){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						}
					else if(!this.validateWorkExtension(workExtension)) {
						workExtensionField.set("v.errors", [{message: "Please enter a valid work extension."}]);
						workExtensionField.set("v.isError",true);
						validTalent = false;
					}
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			else if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			}		
			}
		},

		
		
		
		if(workPhoneField){
				var workPhone = workPhoneField.get("v.value");
				var workPhoneValMessage = "Please enter a valid Work Phone.";
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if(workExtension != '' && workPhone == '' ){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						}
					else if(!this.validateWorkExtension(workExtension)) {
						workExtensionField.set("v.errors", [{message: "Please enter a valid work extension."}]);
						workExtensionField.set("v.isError",true);
						validTalent = false;
					}
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			else if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			}*/




        if (component.get("v.otherPhonePreferred") === true) {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                var otherPhone = otherPhoneField.get("v.value");
                if(otherPhone === "" || !otherPhone){
                    validTalent = false;
                    otherPhoneField.set("v.errors", [{message: "Please enter a Work Phone."}]);
                    otherPhoneField.set("v.isError",true);
                    $A.util.addClass(otherPhoneField,"slds-has-error show-error-message");
                }
                else {
                    otherPhoneField.set("v.errors", []);
                    otherPhoneField.set("v.isError", false);
                    $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                otherPhoneField.set("v.errors", []);
                otherPhoneField.set("v.isError", false);
                $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.emailPreferred") === true) {
            var emailField = component.find("email");
            if(emailField){
                var email = emailField.get("v.value");
                if(email === "" || !email){
                    validTalent = false;
                    emailField.set("v.errors", [{message: "Please enter a Email."}]);
                    emailField.set("v.isError",true);
                    $A.util.addClass(emailField,"slds-has-error show-error-message");
                }
                else {
                    emailField.set("v.errors", []);
                    emailField.set("v.isError", false);
                    $A.util.removeClass(emailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var emailField = component.find("email");
            if(emailField){
                emailField.set("v.errors", []);
                emailField.set("v.isError", false);
                $A.util.removeClass(emailField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.otherEmailPreferred") === true) {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                var otherEmail = otherEmailField.get("v.value");
                if(otherEmail === "" || !otherEmail){
                    validTalent = false;
                    otherEmailField.set("v.errors", [{message: "Please enter a Other Email."}]);
                    otherEmailField.set("v.isError",true);
                    $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
                }
                else {
                    otherEmailField.set("v.errors", []);
                    otherEmailField.set("v.isError", false);
                    $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                otherEmailField.set("v.errors", []);
                otherEmailField.set("v.isError", false);
                $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.workEmailPreferred") === true) {
            var workEmailField = component.find("workEmail");
            if(workEmailField){
                var workEmail = workEmailField.get("v.value");
                if(workEmail === "" || !workEmail){
                    validTalent = false;
                    workEmailField.set("v.errors", [{message: "Please enter a Other Email."}]);
                    workEmailField.set("v.isError",true);
                    $A.util.addClass(workEmailField,"slds-has-error show-error-message");
                }
                else {
                    workEmailField.set("v.errors", []);
                    workEmailField.set("v.isError", false);
                    $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var workEmailField = component.find("workEmail");
            if(workEmailField){
                workEmailField.set("v.errors", []);
                workEmailField.set("v.isError", false);
                $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
            }
        }
        
        return validTalent;
    },
       
    validatePhoneEmailLinkedIn : function(component, validTalent) {
        //S-228927 - adding new opcos SJA,IND,EAS
        var newAerotek = $A.get("$Label.c.Aerotek");
        var newAstonCarter = $A.get("$Label.c.Aston_Carter");
        var newNewco = $A.get("$Label.c.Newco");  
		if(component.get("v.recordType") === 'Phone'){
			var mobilePhoneField = component.find("mobilePhone");
			if(mobilePhoneField){
				var mobilePhone = mobilePhoneField.get("v.value");    
        }
        
        if(!this.validateMobilePhone(mobilePhone)) {
            mobilePhoneField.set("v.errors", [{message: "Please enter a valid Mobile Phone."}]);
            mobilePhoneField.set("v.isError",true);
            validTalent = false;
        }
        
        var homePhoneField = component.find("homePhone");
        if(homePhoneField){
            var homePhone = homePhoneField.get("v.value");    
        }
        if(!this.validateMobilePhone(homePhone)) {
            homePhoneField.set("v.errors", [{message: "Please enter a valid Home Phone."}]);
            homePhoneField.set("v.isError",true);
            validTalent = false;
        }
        
        var workPhoneField = component.find("workPhone");
        if(workPhoneField){
            var workPhone = workPhoneField.get("v.value");    
        }
        if(!this.validateMobilePhone(workPhone)) {
            workPhoneField.set("v.errors", [{message: "Please enter a valid Work Phone."}]);
            workPhoneField.set("v.isError",true);
            validTalent = false;
        }
        
        var otherPhoneField = component.find("otherPhone");
        if(otherPhoneField){
            var otherPhone = otherPhoneField.get("v.value");    
        }
        if(!this.validateMobilePhone(otherPhone)) {
            otherPhoneField.set("v.errors", [{message: "Please enter a valid Work Phone."}]);
            otherPhoneField.set("v.isError",true);
				validTalent = false;
			}

			var linkedinURL = component.get("v.contactRecordFields.LinkedIn_URL__c");
			var email = component.get("v.contactRecordFields.Email");
			var workEmail = component.get("v.contactRecordFields.Work_Email__c");
			var otherEmail = component.get("v.contactRecordFields.Other_Email__c");

            //S-228927 new opcos added IND,SJA,EAS
			// if (component.get("v.runningUser.OPCO__c") === 'ONS' || component.get("v.runningUser.OPCO__c") === 'TEK') {
            if (component.get("v.runningUser.OPCO__c") === 'ONS' || component.get("v.runningUser.OPCO__c") === 'TEK' || component.get("v.runningUser.OPCO__c") === newAerotek || component.get("v.runningUser.OPCO__c") === newAstonCarter || component.get("v.runningUser.OPCO__c") === newNewco) {
				if (!mobilePhone && !homePhone && !workPhone && !otherPhone && !email && !otherEmail && !workEmail) {
					validTalent = false;
					this.showToast(`Error`,`One of the contact methods (Phone or Email) are required to proceed.`, `error`);
				}
			} else {
				if (!mobilePhone && !homePhone && !workPhone && !otherPhone && !email && !otherEmail && !workEmail && !linkedinURL) {
					validTalent = false;
					this.showToast(`Error`, `One of the contact methods (Phone, Email or LinkedIn) is required to proceed.`, `error`);
				} 
			}
		}
        
		if(component.get("v.recordType") === 'Email'){
			var emailField = component.find("email");
			if(emailField){
				var email = emailField.get("v.value");
        }
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: "Please enter a valid Email."}]);
            emailField.set("v.isError",true);
            
            validTalent = false;
        }
        
        var otherEmailField = component.find("otherEmail");
        if(otherEmailField){
            var otherEmail = otherEmailField.get("v.value");    
        }
        if(!this.validateEmail(otherEmail)) {
            otherEmailField.set("v.errors", [{message: "Please enter a valid Email."}]);
            otherEmailField.set("v.isError",true);
            
            validTalent = false;
        }
        
        var workEmailField = component.find("workEmail");
        if(workEmailField){
            var workEmail = workEmailField.get("v.value");
        }
        if(!this.validateEmail(workEmail)) {
            workEmailField.set("v.errors", [{message: "Please enter a valid Email."}]);
            workEmailField.set("v.isError",true);
            
				validTalent = false;
			}

			var linkedinURL = component.get("v.contactRecordFields.LinkedIn_URL__c");
			var workPhone = component.get("v.contactRecordFields.Phone");
			var homePhone = component.get("v.contactRecordFields.HomePhone");
			var mobilePhone = component.get("v.contactRecordFields.MobilePhone");
			var otherPhone = component.get("v.contactRecordFields.OtherPhone");

            //S-228927 new opcos added IND,SJA,EAS
			//if (component.get("v.runningUser.OPCO__c") === 'ONS' || component.get("v.runningUser.OPCO__c") === 'TEK') {
            if (component.get("v.runningUser.OPCO__c") === 'ONS' || component.get("v.runningUser.OPCO__c") === 'TEK' || component.get("v.runningUser.OPCO__c") === newAerotek || component.get("v.runningUser.OPCO__c") === newAstonCarter || component.get("v.runningUser.OPCO__c") === newNewco) {
				if (!mobilePhone && !homePhone && !workPhone && !otherPhone && !email && !otherEmail && !workEmail) {
					validTalent = false;
					this.showToast(`Error`,`One of the contact methods (Phone or Email) are required to proceed.`, `error`);
				}
			} else {
				if (!mobilePhone && !homePhone && !workPhone && !otherPhone && !email && !otherEmail && !workEmail && !linkedinURL) {
					validTalent = false;
					this.showToast(`Error`, `One of the contact methods (Phone, Email or LinkedIn) is required to proceed.`, `error`);
				} 
			}
        }

        return validTalent;    
    },
    
    validateMobilePhone : function(mobilePhone) {
        if(mobilePhone && mobilePhone.length > 0) {
            var mp = mobilePhone.replace(/\s/g,'');
            if(mobilePhone && mp.length >= 10 && mp.length <= 17) {
                
                var regex = new RegExp("^[0-9-.+()/]+$");
                var isValid = mp.match(regex);
                
                if(isValid != null) {
                    return true; 
                }
                else{
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true; 
    },   
    
    validateEmail : function(emailID){
        if(emailID && emailID.trim().length > 0) {
            var isValidEmail = emailID.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            if(isValidEmail != null) {
                return true;
            }
            else {
                return false;
            } 
        }
        return true;
    },

	updatePreferredValues : function(component, contact) {
        if (component.get("v.homePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Home';
        } 
        else if (component.get("v.mobilePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Mobile';
        } 
            else if (component.get("v.workPhonePreferred") === true) {
                contact.Preferred_Phone__c = 'Work';
            }
                else if (component.get("v.otherPhonePreferred") === true) {
                    contact.Preferred_Phone__c = 'Other';
                }
                    else {
                        contact.Preferred_Phone__c = '';
                    }
        //Email
        if (component.get("v.emailPreferred") === true) {
            contact.Preferred_Email__c = 'Email';
        } 
        else if (component.get("v.otherEmailPreferred") === true) {
            contact.Preferred_Email__c = 'Alternate';
        }
            else if (component.get("v.workEmailPreferred") === true) {
                contact.Preferred_Email__c = 'Work';
            }
                else {
                    contact.Preferred_Email__c = '';
                }
        
        return contact;
    },

	togglePopover: function(component){
        var togglePopover = component.get("v.showPopover");
        component.set("v.showPopover", !togglePopover);
		component.set("v.showLinkedInPopOver",false);
    },

	clearPrefOptions: function(component){
		if(component.get("v.recordType") === 'Phone'){
			component.set("v.homePhonePreferred", false);
			component.set("v.mobilePhonePreferred", false);
			component.set("v.workPhonePreferred", false);
			component.set("v.otherPhonePreferred", false);
		}
        if(component.get("v.recordType") === 'Email'){
			component.set("v.emailPreferred", false);
			component.set("v.otherEmailPreferred", false);
			component.set("v.workEmailPreferred", false);
		}
    },

	addRemoveEventListener : function(component, event, elementId, headerClass, subClass) {
		if (event.target.className.includes(subClass)) {
				component.set("v.showPopover", true);
				this.init(component);
			} else {
				this.togglePopover(component);
			}

			$A.util.addClass(event.target, subClass);
			var clickedFn;
			var myPopover = component.find(elementId);

			clickedFn = $A.getCallback(function(event){
				var popoverEle = myPopover.getElement();
				$A.util.removeClass(popoverEle, 'slds-hide');
				if(popoverEle && event.path[0]) {
					if (!event.path[0].className.includes(headerClass) && !popoverEle.contains(event.path[0])) {
						$A.util.addClass(popoverEle, 'slds-hide');
						//$A.util.addClass(event.target, subClass);
						document.removeEventListener('click',clickedFn);
					}
				}
				else {
					document.removeEventListener('click',clickedFn);
					//$A.util.addClass(event.target, subClass);
				}
			});

			document.addEventListener('click',clickedFn);
	},
    setDNCFields: function (cmp, contact) {
	    let dncFields = {};
	    if (contact.Account) {
            ['Do_Not_Contact_Date__c', 'Do_Not_Contact_Reason__c', 'Do_Not_Contact__c'].forEach(field => {dncFields[field] = contact.Account[field]})
        }

	    cmp.set('v.dncFields', dncFields);
    },
    saveDNCFields: function (cmp, account) {
        const dncFields = cmp.get('v.dncFields');
        if (account) {
            Object.keys(dncFields).forEach(key => {
                account[key] = dncFields[key];
            })
        }
        return account;
    },
	saveLinkedIn: function (component, validTalent){
	if(component.get("v.recordType") === 'LinkedInURL')
	{

	var linkedinUrlField = component.find("LinkedInURL");
        if(linkedinUrlField){
            var linkedinUrl = linkedinUrlField.get("v.value");   
       }

        linkedinUrlField.set("v.errors", []);
        linkedinUrlField.set("v.isError", false);
        $A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
        
        if(linkedinUrl && linkedinUrl != ""){
            // /(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
            // var linkedInRegex = new RegExp(".*(linkedin.com/).*", "i");
            // var linkedInRegex = new RegExp(/(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/);

            var linkedInRegex = new RegExp(/(http|https):\/\/\/?\S+$/);


            if (!linkedInRegex.test(linkedinUrl)) {
                validTalent = false;
                linkedinUrlField.set("v.errors", [{message: $A.get("$Label.c.ADD_NEW_TALENT_VALID_LINKEDIN_URL")}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");
				validTalent=false;
                if(!linkedinUrl.includes('http')){
                    this.showError($A.get("$Label.c.ADD_NEW_TALENT_UDATE_LINKEDIN_URL"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
					validTalent=false;
                }else{
                    this.showError($A.get("$Label.c.ADD_NEW_TALENT_VALID_LINKEDIN_URL_FORMAT"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
					validTalent=false;
                }                
            } else if (linkedinUrl.search("recruiter/profile")>0) {
				validTalent = false;
                linkedinUrlField.set("v.errors", [{message: $A.get("$Label.c.ATS_PublicProfile")}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");
				this.showError($A.get("$Label.c.ATS_PublicProfile"), $A.get("$Label.c.ADD_NEW_TALENT_ERROR_MSG"));
				validTalent=false;
			
			} else {
					linkedinUrlField.set("v.errors", []);
					linkedinUrlField.set("v.isError", false);
					$A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
            }
        }
		}
		  return validTalent;
		}
		,
		showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    }
      
})