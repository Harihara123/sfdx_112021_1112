({
	init : function(component, event, helper) {
	}, 

	dataFromHeader: function(component, event, helper){
		var talentRecord = event.getParam("talentRecord");
        component.set("v.talentRecord", talentRecord);
	}, 

	dataFromEmployment: function(component, event, helper){
		helper.dataFromEmployment(component, event); 
	}, 

	dataFromActivities: function(component, event, helper){
		helper.dataFromActivities(component, event); 

	}, 

     dataFromResumePreview: function(component, event, helper){
        helper.dataFromResumePreview(component, event);
    },

	killComponent : function(component, event, helper) {
        if (component.isValid()) {
            component.destroy();
        }
    },

	onInsightsRender : function(component, event, helper) {
        helper.getActivitiesDetailsForInsights(component, event, helper);
        var params = null;
        var recordId = component.get("v.recordId") ;
        var params = {recordId: recordId,isTalent: true };
        var cmps = [
            'C_ContactListBadges'
        ];
        for (var i=0;i<cmps.length;i++) {
			var componentName = 'c:' + cmps[i];
			var params = params;
			var targetAttributeName = 'v.' + cmps[i];
			helper.createComponent(component, componentName, targetAttributeName,params);
		}
	}
})