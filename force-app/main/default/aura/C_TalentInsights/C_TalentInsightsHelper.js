({
 dataFromEmployment: function(component, event){
	var talentEmployment = event.getParam("talentEmployment");
	var recordId = component.get("v.recordId");
    var currentEmpCount = 0; 
    var currentAssignments = {"currentEmployment": []};
    //var currentAssignmentObj = JSON.parse(currentAssignments);

    if(recordId === event.getParam("recordId")){
		component.set("v.talentEmployment", talentEmployment);
		component.set("v.currentEmployer", ""); 
		component.set("v.talentJobTitle", ""); 
		component.set("v.hasCurrentEmployment", false); 
        component.set("v.isCandidateFormer", false);
		var employment = component.get("v.talentEmployment"); 

        // console.log('employment = ' + JSON.stringify(employment));

		if(employment !== null && employment !== undefined){       
		 for(var obj in employment){

            var candiateStatus = employment[obj].candidateStatus;
            if(candiateStatus!=null && candiateStatus == "Former"){
                    component.set("v.currentEmployer", employment[obj].OrgName); 
                    component.set("v.talentJobTitle", employment[obj].JobTitle); 
                    component.set("v.employmentEndDate", employment[obj].EmploymentEndDate);
                    component.set("v.isCandidateFormer", true);
                    currentAssignments.currentEmployment.push({"employer": employment[obj].OrgName,
                                                                    "jobTitle": employment[obj].JobTitle,
                                                                    "endDate": employment[obj].EmploymentEndDate
                                                                    });
                   break;
            }
		 	else if(employment[obj].CurrentAssignment === true){
		 		component.set("v.currentEmployer", employment[obj].OrgName); 
		 		component.set("v.talentJobTitle", employment[obj].JobTitle); 
                component.set("v.employmentEndDate", employment[obj].EmploymentEndDate);
		 		component.set("v.hasCurrentEmployment", true);
                currentAssignments.currentEmployment.push({"employer": employment[obj].OrgName,
                                                                "jobTitle": employment[obj].JobTitle,
                                                                "endDate": employment[obj].EmploymentEndDate
                                                                });
                currentEmpCount ++;
                }
		 	}

             /*
             var hasCurrentEmployment = component.get("v.hasCurrentEmployment");
             // Handling Insight message for Formers  S-91079
             if(employment.length >0 && !hasCurrentEmployment){
                for(var obj in employment){
                var candiateStatus = employment[obj].candidateStatus;
                if(candiateStatus == "Former"){
                    component.set("v.currentEmployer", employment[obj].OrgName); 
                    component.set("v.talentJobTitle", employment[obj].JobTitle); 
                    component.set("v.employmentEndDate", employment[obj].EmploymentEndDate);
                    component.set("v.isCandidateFormer", true);
                    currentAssignments.currentEmployment.push({"employer": employment[obj].OrgName,
                                                                    "jobTitle": employment[obj].JobTitle,
                                                                    "endDate": employment[obj].EmploymentEndDate
                                                                    });                    
                }
                break;
                }
             }
             */

    	  }	
        if(currentEmpCount > 1){
            var employmentJSON = JSON.stringify(currentAssignments); 
            var employmentObj = JSON.parse(employmentJSON);
            var today = new Date();
            var endDate; 
            var earliestEndDate; 
            for(var i = 0; i < employmentObj.currentEmployment.length; i++) {
                var obj = employmentObj.currentEmployment[i];
                if(earliestEndDate === undefined){
                    earliestEndDate = new Date(obj.endDate);
                }
                endDate = new Date(obj.endDate); 
                if(earliestEndDate > today && earliestEndDate >= endDate){
                    component.set("v.currentEmployer", obj.employer); 
                    component.set("v.talentJobTitle", obj.jobTitle); 
                    component.set("v.employmentEndDate", obj.endDate);
                    earliestEndDate = endDate; 
                }
                console.log(obj.endDate);
                }
            }		
	    }
     
 }, 

  dataFromActivities: function(component, event){
  //this function has been changed by akshay for S - 82631 5/21/18 
	var talentActivities = event.getParam("talentActivityRecords"); 
	var recordId = component.get("v.recordId"); 
	var meetingDate; 
	var meetingWith;
	var lastMeetingDate = new Date("1970-01-01T04:00:00.000Z").getTime(); 
	var today = new Date().getTime();
	component.set("v.hasActivities", false); 
	if(recordId === event.getParam("recordId")){
		if(talentActivities !== null && talentActivities !== undefined){
	      if( typeof event.getParam("lastMeetingDate") !== "undefined" && event.getParam("lastMeetingDate") !== ""){
								component.set("v.hasActivities", true); 
			   lastMeetingDate = new Date(event.getParam("lastMeetingDate").replace(/-/g, '\/').replace(/T.+/, '')).getTime();
			   component.set("v.lastMeetingWith",event.getParam("lastMeetingWith"));

			  if(lastMeetingDate >= today && lastMeetingDate !== ""){
				component.set("v.lastMeetingText", $A.get("$Label.c.ATS_Next_inperson_meeting"));
			}else{
				component.set("v.lastMeetingText", $A.get("$Label.c.ATS_Last_meeting_inperson"));
			}
		component.set("v.lastMeetingDate", lastMeetingDate);
			 }
          
		component.set("v.totalActvities30Days", event.getParam("totalActivitiesLast30Days"));
	}
	  }
}, 

  dataFromResumePreview: function(component, event){
        var resumeDate = event.getParam("lastResumeUploadDate");
        var recordId = component.get("v.recordId");
        if(recordId === event.getParam("recordId")){
	        if(resumeDate !== null && resumeDate != undefined){
	           component.set("v.lastResumeUploadDate",  new Date(resumeDate).getTime());
	           }
	         component.set("v.hasResume", event.getParam("hasResume"));
	        }
 }, 

  getLongDate: function(inputDate){
     var language = window.navigator.userLanguage || window.navigator.language;
     var dateArray; 
     if(inputDate !== null && inputDate !== null){
     	dateArray = inputDate.toDateString().split(" "); 
     }     
     if(language === "en-US"){
     		return dateArray[1]  + " " + dateArray[2] + ", " + dateArray[3];
     	}else{
     		return dateArray[2]  + " " + dateArray[1] + " " + dateArray[3];
     	}
     },

 getActivitiesDetailsForInsights : function(component, event, helper){
        var className = 'ATS';
        var subClassName = 'TalentActivityFunctions';
        var methodName = 'getActivitiesForInsights';
        var actionParams = {'recordId': component.get("v.recordId")}
        var bdata = component.find("basedatahelper");
        bdata.callServer(component,className,subClassName,methodName,function(response){
            if(response){
                if(response.length > 0) {
                    component.set("v.g2CompletedDate", response[0].CreatedDate);
                    if (response[0].CreatedDate) {
                      component.set("v.hasG2Completed", true);
                    }
                }
            }
        },actionParams,false);
    },     	
	 
	createComponent : function(cmp, componentName, targetAttributeName, params) {
         $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
					newComponent.addEventHandler("c:E_RefreshChild", newComponent.getReference("c.updateBadges"));
                	var enableCmp = cmp.get("v.enableCmp");
                    cmp.set("v.enableCmp", !enableCmp);
                    cmp.set(targetAttributeName, newComponent);

                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
  
})