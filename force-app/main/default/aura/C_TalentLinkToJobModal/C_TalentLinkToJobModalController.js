({
	displayLinkToJob : function(component, event, helper) {
        var talentId = event.getParam("talentId");
        component.set("v.talentId", talentId);
        
        var opportunityId = event.getParam("opportunityId");
        component.set("v.opportunityId", opportunityId);

        var jobId = event.getParam("jobId");
        component.set("v.jobId", jobId);
        
        var fromsubpage  = event.getParam("fromsubpage");
        component.set("v.fromsubpage", fromsubpage);

        helper.showModal(component);

        component.set("v.applicantRadioValue", false);
        component.set("v.linkRadioValue", false);
        component.set("v.sourceType", "");
        component.set("v.source", "");
        component.set('v.isDisabled', true);
	},
    
    hideModal : function(component, event, helper){
        helper.hideModal(component);
    },
    
    saveAndContinue : function(component, event, helper){
		helper.saveOrder(component);
        helper.hideModal(component);
        
    },
    
    saveAndComplete : function(component, event, helper){
        component.set("v.isComplete", true); 
        helper.saveOrder(component);
    },

    enableSave : function(component, event, helper) {
        if (component.get("v.source").length > 2){
             component.set('v.isDisabled', false);
       } else {
            component.set('v.isDisabled', true);
       }
    },
    refresh : function(component, event, helper) {
       $A.get('e.force:refreshView').fire();
        if(component.isValid()){
           component.destroy(); 
        }
       
    },
	//Added by akshay for S - 71890 5/15/18
	SearchCompleteHandler : function (cmp,evt,helper){
	    cmp.set("v.requestId",evt.getParam("requestId"));
		cmp.set("v.transactionId",evt.getParam("transactionId"));
	},
    
    onSelect: function(component, event) {
         var sourceType = event.getSource().get("v.text");
         component.set("v.sourceType", sourceType);
         if (sourceType == "Linked") {
            component.set('v.isDisabled', false);
         }  else {
            component.set('v.isDisabled', true);
         } 
     }
})