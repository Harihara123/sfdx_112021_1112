({
    
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    
    hideModal: function(component) {
        //Toggle CSS styles for hiding Modal
        this.toggleClassInverse(component,'backdropLinkToJob','slds-backdrop--');
        this.toggleClassInverse(component,'modaldialogLinkToJob','slds-fade-in-');
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    }, 
    
    showModal: function(component) {
        var action = component.get("c.isLinkedStatus");
        action.setParams({
            "opportunityId": component.get("v.opportunityId"),
            "jobId": component.get("v.jobId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var retVal = data.getReturnValue();
                if(retVal == true){
                     component.set("v.isLinked", true);
                } else{
                    component.set("v.isLinked", false);
                } 
                //Toggle CSS styles for hiding Modal
                this.toggleClass(component,'backdropLinkToJob','slds-backdrop--');
                this.toggleClass(component,'modaldialogLinkToJob','slds-fade-in-');
            }  
        });
        // Invoke the service
           $A.enqueueAction(action);
    },
    

    saveOrder: function(component) {
        var orderType = component.get ("v.jobId") == null ? "opportunity" : "job";
        var sourceField = component.find("sourceId");
        var source = null;
        if (sourceField != undefined) {
            source = sourceField.get("v.value");
        } 
        var action = component.get("c.saveOrder"); 
        action.setParams({
            "opportunityId": component.get("v.opportunityId"), 
            "jobId": component.get("v.jobId"), 
            "accountId": component.get("v.talentId"),
            "status": component.get("v.sourceType"), 
            "applicantSource": source,
            "type": orderType,
			"requestId": component.get("v.requestId"),
			"transactionId": component.get("v.transactionId"),
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                var appEvent = $A.get("e.c:E_RefreshSearchResultsLinked");
                appEvent.setParams({
                    "opportunityId": component.get("v.opportunityId"),
                    "talentId": component.get("v.talentId") });
                appEvent.fire();
                
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if ((errors[0] && errors[0].message) || 
                        (errors[0] && errors[0].pageErrors[0] 
                            && errors[0].pageErrors[0].message)) {
                        console.log("Error message: " + 
                                 errors[0].message + " " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
               
            }if (component.get("v.isComplete")){this.redirect(component);}
                                               
           });
        $A.enqueueAction(action); 
    },

    redirect: function(component) {
        var subPage = component.get ("v.fromsubpage");

        var urlEvent = $A.get("e.force:navigateToURL");
        /*var orderType = component.get ("v.jobId") == null ? "opportunity" : "job";
        if (orderType == "job" || component.get ("v.fromsubpage") == null) { 
            urlEvent.setParams({
              "url": "/" + component.get("v.talentId")
            });
         } else if ( == "FALSE") {
            urlEvent.setParams({
              "url": "/" + component.get("v.opportunityId")
            });
         } else if (component.get ("v.fromsubpage") == "TRUE"){
            urlEvent.setParams({
            	//"url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/alohaRedirect/apex/Req_Submittal_Detail?id=" + component.get("v.opportunityId")
				"url": "/one/one.app#/alohaRedirect/apex/Req_Submittal_Detail?id=" + component.get("v.opportunityId")
            });
         }  
         */
         // updated for S-85722
         if(subPage != undefined && subPage != null){
            if(subPage == "SUB"){
                urlEvent.setParams({
					"url": "/one/one.app#/alohaRedirect/apex/Req_Submittal_Detail?id=" + component.get("v.opportunityId")
				});
            }
            else if ((subPage == "REQ")){
                urlEvent.setParams({
                    "url": "/" + ( (subPage == "REQ") ? component.get("v.opportunityId") : component.get("v.talentId"))
                });
            }
			else if ((subPage == "JOB") || (subPage == "TALENT")){
                urlEvent.setParams({
                    "url": "/" + component.get("v.talentId")
                });
            }
         }
        urlEvent.fire();
    }
})