({
    doInit: function (component, event, helper) {
        helper.checkAllSelected(component);
    },
    sortByName: function (component, event, helper) {
         helper.sortRecordList(component, helper, 'Name');    
    },
    sortByTitle: function (component, event, helper) {
         helper.sortRecordList(component, helper, 'Title');
    },
    sortByCompanyName: function (component, event, helper) {
         helper.sortRecordList(component, helper, 'Company Name');
    },
    sortBySubmittalStatus: function (component, event, helper) {
        helper.sortRecordList(component, helper, 'Account.Talent_Latest_Submittal_Status__c');
    },
    sortBySubmittalDate: function (component, event, helper) {
        helper.sortRecordList(component, helper, 'Submittal Date');
    },
	sortByLastActivityDate: function (component, event, helper) {
		helper.sortRecordList(component, helper, 'Last Activity Date');
	},
    bringUpDeleteModal: function(cmp, event, helper) {
        helper.handleCreateComponent(cmp, event);
    },
    bringUpCallSheetModal: function(cmp, event, helper) {
        helper.handleCreateComponent(cmp, event);
    },
    checkAll: function(cmp, event, helper) {
        helper.checkAll(cmp);
    },
    checkAllSelected: function (component, event, helper) {
        helper.checkAllSelected(component);
    },
    removeSelectedContact: function(component, event, helper) {
        helper.unselectRemovedContact(component, event);
    },
    viewTalentSidePanel : function(component, event, helper) {
		// Order is important. set showSidebar to true before setting contactId
		component.set("v.showSidebar", true);
        component.set("v.accountId", event.getParam("recordId"));
        var talent = event.getParam("record");
        var talentId = event.getParam("contactId");
        var talentSubmittals = component.get("v.talentSubmittals");
        component.set("v.talent", helper.tryAppendingSubmittalsToTalent(talentSubmittals, talentId, talent));
        component.set("v.talentId", talentId);
       	helper.setSidebarPos(component);
    },
    noteEntered: function(component, event, helper) {
        component.set("v.noteText", event.target.value);
    },
    saveAndClose: function(component, event, helper) {
        component.set("v.showNotesModal", false);
        // v.talentInModal is the record to be updated
        // v.noteText is the note entered by recruiter
        let tableData = component.get("v.tableData");
        let talentInModal = component.get("v.talentInModal");
        let talentIndex = tableData.findIndex(talent => talent.Id === talentInModal.Id);
        if (talentIndex != -1) {
            tableData[talentIndex].Notes = component.get("v.noteText");
            component.set("v.tableData", tableData);
			helper.setNotes(component,"update",talentInModal.Id);
        }        
    },
    cancelAndClose: function(component, event, helper) {
        component.set("v.showNotesModal", false);        
    },
    deleteNote: function(component, event, helper) {
        let c = confirm(`Deleting the note for ${component.get("v.talentInModal").Name} will delete all content and the note.  Are you sure?`);
	    if (c) {
    		component.set("v.showNotesModal", false);
	        // v.talentInModal is the record to be updated
    	    // v.noteText is the note entered by recruiter
        	let tableData = component.get("v.tableData");
	        let talentInModal = component.get("v.talentInModal");
    	    let talentIndex = tableData.findIndex(talent => talent.Id === talentInModal.Id);
       		if (talentIndex != -1) {
            	tableData[talentIndex].Notes = "";
            	component.set("v.tableData", tableData);
				helper.setNotes(component,"delete",talentInModal.Id);
        	}
		}                
    },
    previewFile: function(cmp, e, h) {
        const navService = cmp.find('navService');
        const doc = e.target.getAttribute('data-id')

        const docMap = {
            terms: $A.get("$Label.c.ATS_RESTRICTED_TERMS_DOC_ID"),
            eeo: $A.get("$Label.c.ATS_EEO_POLICY_DOC_ID")
        }

        e.preventDefault();
        if (doc && docMap[doc]) {
            navService.navigate({
                type: 'standard__namedPage',
                attributes: {
                    pageName:'filePreview'
                },
                state: {
                    recordIds: docMap[doc],
                    selectedRecordId: docMap[doc]
                }
            });   
        }        
    }
})