({
    sortRecordList: function (cmp, helper, sortField) {
        var recordList = cmp.get("v.tableData");
        var sortDirection = cmp.get("v.sortDirection");
        var newSortDirection = sortDirection === 'ASC' ? 'DESC' : 'ASC';

        var firstVal = newSortDirection === 'ASC' ? 1 : -1;
        var secondVal = newSortDirection === 'ASC' ? -1 : 1;

        recordList.sort(function(a,b) {
            
            var compareFields = helper.setSortFieldValues(a, b, sortField);

            var fieldA = compareFields[0];
            var fieldB = compareFields[1];

            if(fieldA < fieldB) {
                return secondVal;
            }

            if(fieldA > fieldB) {
                return firstVal;
            }

            return 0;
        });

        cmp.set("v.tableData", recordList);
        cmp.set("v.sortDirection", newSortDirection);
        cmp.set("v.sortColumn", sortField);
    },
    setSortFieldValues: function (a, b, sortField) {
        var fieldA, fieldB;
        var valA = a; 
        var valB = b;

        switch(sortField) {
            case 'Account.Talent_Latest_Submittal_Status__c':
                console.log('a ', a);
                console.log('b ', b);
                fieldA = a['Account']['Talent_Latest_Submittal_Status__c'] ? a['Account']['Talent_Latest_Submittal_Status__c'] : '';
                fieldB = b['Account']['Talent_Latest_Submittal_Status__c'] ? b['Account']['Talent_Latest_Submittal_Status__c'] : '';
                break;
            case 'Title':
                fieldA = valA['Title'] ? valA['Title'].toUpperCase() : '';
                fieldB = valB['Title'] ? valB['Title'].toUpperCase() : '';
                break;
            case 'Company Name':
                fieldA = valA['Account']['Talent_Current_Employer_Formula__c'] ? valA['Account']['Talent_Current_Employer_Formula__c'].toUpperCase() : '';
                fieldB = valB['Account']['Talent_Current_Employer_Formula__c'] ? valB['Account']['Talent_Current_Employer_Formula__c'].toUpperCase() : '';
                break;
            case 'Submittal Date':
                fieldA = a['Account']['Talent_Latest_Submittal_Timestamp__c'] ? a['Account']['Talent_Latest_Submittal_Timestamp__c'] : '';
                fieldB = b['Account']['Talent_Latest_Submittal_Timestamp__c'] ? b['Account']['Talent_Latest_Submittal_Timestamp__c'] : '';
                break;
			case 'Last Activity Date':
				 fieldA = valA['Last_Activity_Date__c'] ? valA['Last_Activity_Date__c'].toUpperCase() : '';
                 fieldB = valB['Last_Activity_Date__c'] ? valB['Last_Activity_Date__c'].toUpperCase() : '';
				 break;
            case 'Name':
            default:
                fieldA = valA['Name'] ? valA['Name'].toUpperCase() : '';
                fieldB = valB['Name'] ? valB['Name'].toUpperCase() : '';
                break;
        }

        return [fieldA, fieldB];
    },
    handleCreateComponent: function (cmp, event) {
        var logger = event.getParam('contact');
        console.log(logger.Id);
        console.log(cmp.get("v.list"));
        $A.createComponent(
            "c:C_RemoveContactModal",
            {"record" : event.getParam('contact'), "list" : cmp.get("v.list"), "selectedList" : cmp.get("v.selectedList")},
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    cmp.set('v.C_RemoveContactModal', newComponent);
                }
                else if (status === "INCOMPLETE") {
                }
                else if (status === "ERROR") {
                }
            }
        );
    },
    checkAll: function (cmp) {
        var rowItems = cmp.find('rowItem');
        var selectedList = cmp.get('v.selectedList');

        selectedList = selectedList.filter(function(el) {
            return el.RecordType.Name !== 'Talent';
        });

        console.log(selectedList);

        if(rowItems.length > 1) {
            for(var i = 0; i < rowItems.length; i++) {
                rowItems[i].set('v.isSelected', cmp.get('v.allSelected'));
                if(cmp.get('v.allSelected')) {
                    selectedList.push(rowItems[i].get('v.talent'));
                }
            }
        } else {
            if(Array.isArray(rowItems)) {
                rowItems[0].set('v.isSelected', cmp.get('v.allSelected'));
                if(cmp.get('v.allSelected')) {
                    selectedList.push(rowItems[0].get('v.talent'));
                }
            } else {
                rowItems.set('v.isSelected', cmp.get('v.allSelected'));
                if(cmp.get('v.allSelected')) {
                    selectedList.push(rowItems.get('v.talent'));
                }
            }
        }

        cmp.set('v.selectedList', selectedList);
    },
    checkAllSelected: function (cmp) {
        var rowItems = cmp.find('rowItem');
        var selectedList = cmp.get('v.selectedList');
        var allSelected = false;

        if(Array.isArray(rowItems)) {
            allSelected = rowItems.length === selectedList.length;
        } else {
            allSelected = selectedList.length === 1;
        }

        cmp.set('v.allSelected', allSelected);
    },
    unselectRemovedContact: function (component, event) {
        var removedContact = event.getParam('contactId');
        var rowItems = component.find('rowItem');
        var selectedContacts = component.get('v.selectedList');

        if(rowItems.length > 1) {
            for(var i = 0; i < rowItems.length; i++) {
                if(rowItems[i].get('v.talent').Id === removedContact) {
                    rowItems[i].set('v.isSelected', false);
                    break;
                }
            }
        } else {
            if(Array.isArray(rowItems)) {
                if(rowItems[0].get('v.talent').Id === removedContact) {
                    rowItems[0].set('v.isSelected', false);
                }
            } else {
                if(rowItems.get('v.talent').Id === removedContact) {
                    rowItems.set('v.isSelected', false);
                }
            }
        }

        var tempList = selectedContacts.splice(selectedContacts.map(function(x) {return x.Id}).indexOf(removedContact), 1);

        if(selectedContacts.length < rowItems.length || !Array.isArray(rowItems)) {
            component.set('v.allSelected', false);
        }

        component.set('v.selectedList', selectedContacts);
    }, 

    setSidebarPos: function(component) {
        var resultsHeight = document.getElementById("resultsContainer").clientHeight + 200 + 165,
            showSidebar = component.get("v.showSidebar");
        if(resultsHeight >= 727) {
            component.set("v.sidebarHeight", resultsHeight);
        }
        else {
            component.set("v.sidebarHeight", 727);
        }
        if(showSidebar === true){
            resultsHeight = resultsHeight - 64;
            component.set("v.scrollHeight", resultsHeight);
        }
    },

    tryAppendingSubmittalsToTalent : function(talentSubmittals, talentId, talent) {
        if(talentSubmittals && talentSubmittals[talentId]) {
            var AssociatedPositionOpening = {
                "UserArea" : {"Status" : talentSubmittals[talentId].Status}
            }
            talent.associated_position_openings = {"AssociatedPositionOpening" : [AssociatedPositionOpening]};
        }
        return talent;
    },

	setNotes : function(component, operation, contactId) {
		 if (operation === "delete") {
			component.set("v.noteText","");
		 }
		var action = component.get("c.setNotesInContactTag");
		action.setParams({
			notes : component.get("v.noteText"),
			tagId: component.get("v.list"),
			contactId : contactId
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state !== 'SUCCESS') {
				// handle error 
			}
		});
		$A.enqueueAction(action);
	}
})