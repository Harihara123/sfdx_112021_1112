({	
    /* Sandeep: add to queue changes */
    doInit : function(component, event, helper){
        let plUsers= helper.getPLString(component,event);
        //console.log('in talent list item plUsers',plUsers);		
        let usrAlias= component.get("v.userAlias");
		let userPplSoftID= component.get("v.userPplSoftID");
        //console.log('in talent list item usrAlias',usrAlias);
        if($A.util.isEmpty(plUsers)){
            component.set("v.inPipeline",false);            
        }else{
             component.set("v.inPipeline",plUsers.includes(userPplSoftID));
            
        }
        helper.setSelected(component, event, helper);
        //console.log('row item inits');
		helper.setActionMenuItems(component, event); 
        let mCity = component.get("v.talent.MailingCity");
        let mState = component.get("v.talent.MailingState");
        let mZip = component.get("v.talent.MailingPostalCode");        
        let fullAddress = mCity ? mCity : "";
        fullAddress += (mCity && (mState || mZip)) ? ", " : "";
        fullAddress += mState ? mState : "";
        fullAddress += (mState && mZip) ? ", " : "";
        fullAddress += mZip ? mZip : "";
		component.set("v.fullAddress", fullAddress);
    },
    setSelected : function(component, event, helper) {
        helper.setSelected(component, event, helper);
    },
     /* Sandeep:End */
    selectTalent: function (component, event, helper) {
        var isSelected = component.get('v.isSelected');
        var selectedList = component.get('v.selectedList');
        
        if(isSelected) {
            selectedList.push(component.get('v.talent'));
            component.set('v.selectedList', selectedList);    
        } else {
            var rowTalent = component.get('v.talent');
            selectedList = selectedList.filter(talent => talent.Id !== rowTalent.Id);
            component.set('v.selectedList', selectedList);
        }
        
    },
    
    bringUpDeleteModal : function(cmp, event, helper) {
		helper.bringUpDeleteModal(cmp, event);
    },
    
    bringUpResumeModal: function(component, evt, helper) {
        helper.getTalentResume(component, helper);
        
    },
    openReqSearchModal : function(component, event, helper) {
		helper.openReqSearchModal(component, event);
    },
     /* Sandeep: add to queue changes */
    removeFromPipeline : function(component, evt, helper) {
       helper.removeFromPipeline(component, evt);
    },
     
    addToPipeline : function(component, evt, helper) {
          helper.appendToPipeline(component, evt);
    },
    /* Sandeep:End */
    refreshListView : function(component, evt, helper) {
        
    },
	handleMenuSelect: function (cmp, e, h) {
		h[e.getParam("value")](cmp,e);
	}, 

    toggleSidebar : function(component, event, helper) {
		helper.toggleSidebar(component, event);
    },
    openNotesModal: function(component, event, helper) {
        helper.openNotesModal(component);
    }
})