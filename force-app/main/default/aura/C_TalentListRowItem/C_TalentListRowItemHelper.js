({
    //S-96322 Sandeep added for pipeline functionality.
    appendToPipeline: function(component,event){
        var contact=component.get("v.talent");
        var plUsers = this.getPLString(component);
        console.log('plUsers ',plUsers);
        var usrPplSoftId= component.get("v.userPplSoftID");
        if($A.util.isEmpty(plUsers)){
            plUsers = ':'+usrPplSoftId+':';
        }
        else if(!plUsers.includes(usrPplSoftId)){
            plUsers = plUsers+usrPplSoftId+':';
        }
        console.log('plUsersNew ',plUsers);
        if(plUsers.length>1530){
            //take last 1530 right after a : 
            while(plUsers.length>1530){
                plUsers = plUsers.substr(plUsers.indexOf(':')+1,plUsers.length);
            }
            
        }
        console.log('plUsersNew ',plUsers);
        this.updatePipeline(component,plUsers,'Create');
    },
    updatePipeline: function(component,plUsersNew,oper){
        //Update Account using LDS and then call apex action to create/delete record.
        var fldName = "v.accountRecordFields.Target_Account_Stamp_";
        component.set("v.talentId",component.get("v.talent.AccountId"));
        var usrId= component.get("v.userId");
        var conIds = [];
        conIds.push(component.get('v.talent.Id'));
        for(var i=0;i<=5;i++){
            component.set(fldName+i+'__c',plUsersNew.substr(i*254,254));
        }
        component.find("accountTalentData").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                console.log("saved to account using LDS. now call server side controller to create other records.");
            }
        }));
        this.updPipelineRcd(component,conIds,usrId,oper);
    },
    updPipelineRcd: function(component,conIds,usrId,oper){
        var toastEvent = $A.get("e.force:showToast");
        var actionParams ='';
        if(oper=='Create'){
            actionParams = {'conIds': JSON.stringify(conIds),'userId': usrId};
            var action = component.get("c.addToPipeLine");
			this.setActionMenuItems(component);
            action.setParams({
                "methodName" : "addToPipeline",
                "parameters" : actionParams
            });
            
            action.setCallback(this, function(response) {
                var statusMsg = response.getReturnValue();
                if(!statusMsg.indexOf('Success')) {
                     // create task
            		//this.createTask(component);
                    component.set("v.inPipeline",true);
					this.setActionMenuItems(component);
                    toastEvent.setParams({
                        type: 'success',
                        message: 'This Talent was successfully added to your Pipeline and will remain there for 90 days, or until you Remove from Pipeline'
                    });
                    toastEvent.fire();  
                } else {
                   toastEvent.setParams({
                        type: 'error',
                        message: 'The Talent was not added to the Pipeline, please contact the support desk'
                    });
                    toastEvent.fire();
                }
            });
            
            $A.enqueueAction(action);
        }
        else{
            actionParams = {'conId': conIds[0],'userId': usrId};
            var action = component.get("c.removeFromPipeLine");
            action.setParams({
                "methodName" : "removeFromPipeline",
                "parameters" : actionParams
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == 'SUCCESS') {
                    component.set("v.inPipeline",false);
					this.setActionMenuItems(component);
                    toastEvent.setParams({
                        type: 'success',
                        message: 'The Talent was successfully removed from the Pipeline'
                    });
                    toastEvent.fire();  
                } else {
                    toastEvent.setParams({
                        type: 'error',
                        message: 'The Talent was not removed from the Pipeline, please contact the support desk'
                    });
                    toastEvent.fire();
                }
            });
            
            $A.enqueueAction(action);
        }
        
    },
    removeFromPipeline: function(component,event){
        var plUsers = this.getPLString(component);
        var usrPplSoftId = component.get("v.userPplSoftID");
        var idx,stFirstStr,endFirstStr,stSecStr,endSecStr;
        var len = Number(usrPplSoftId.length)+1;
        if(!$A.util.isEmpty(plUsers)){
            idx = plUsers.indexOf(usrPplSoftId);
            stFirstStr = 0;
            endFirstStr= Number(idx)-1;
            stSecStr = Number(idx)+Number(usrPplSoftId.length);
            endSecStr = plUsers.length;
            if(idx>0)
            {
                //ex. :rothe:nkama: --
                plUsers = plUsers.substring(stFirstStr,endFirstStr)+plUsers.substring(stSecStr,endSecStr);
                if(plUsers==':') {
                    plUsers='';
                }
                this.updatePipeline(component,plUsers,'Delete');
            }
        }
    },
    getPLString: function(component){
        //get the pipeline string.
        //concatinate  Target_Account_Stamp_0__c, Target_Account_Stamp_1__c, Target_Account_Stamp_2__c, Target_Account_Stamp_3__c, 
        //Target_Account_Stamp_4__c, Target_Account_Stamp_5__c fields
        var plUsers = '';
        var fldName = "v.talent.Account.Target_Account_Stamp_";
        for(var i=0;i<=5;i++){
            if(!$A.util.isEmpty(component.get(fldName+i+'__c'))){
                plUsers = plUsers+component.get(fldName+i+'__c');
            }
            
        }
        return plUsers;
    },
    /*S-96322 Sandeep : end */
    getTalentResume: function(component, helper) {
        var record = component.get('v.talent');
        var action = component.get("c.getTalentDefaultResume");
        action.setParams({
            record : record
        });

        action.setCallback(this, function(response) {
            var state = response.getState();


            if(state === 'SUCCESS') {
                var resumeId = response.getReturnValue();
                component.set('v.documentId', resumeId);
                if(resumeId) {
                     helper.showTalentResume(component, helper);
                } else {
                    helper.showToast('error',  'No Available Resume', 'Unable to retrieve a resume for the specified Talent.');
                }

            } else {
                var errors = response.getError();

                if(errors && errors.length) {
                    helper.showToast('error',  'No Available Resume', 'Unable to retrieve a resume for the specified Talent.');
                }
            }
        });

        $A.enqueueAction(action);
    },
    showTalentResume: function(component, helper) {
        var recordID = component.get('v.documentId');
        var modalEvt = $A.get("e.c:E_SearchResumeModal");
        var record = component.get('v.talent');
        var fullName = record.Name;

        modalEvt.setParams({
            "attachmentId" : recordID,
            "resumeName" : fullName,
            "destinationId": "ContactListContainer"
        });

        modalEvt.fire();

    },

    showToast: function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type, 
            title: title,
            message: message
        });
        toastEvent.fire();
    },
    setSelected : function(component, event, helper) {
        var rowTalent = component.get('v.talent');
        var talentList = component.get('v.selectedList');

        var isSelected = false;
        if(rowTalent !== undefined && rowTalent !== null) {
            isSelected = talentList.map(function(x) {return x.Id}).includes(rowTalent.Id);
        }

        component.set('v.isSelected', isSelected);
    }, 

    formatContactRecord : function(talent) {
        var formattedContactRecord = {
            "current_employer_name" : talent.Account.Talent_Current_Employer_Formula__c
            , "dnc": talent.Account.Do_Not_Contact__c || talent.RWS_DNC__c? true : false
            , "employment_position_title" : talent.Title
            , "communication_preferred_email" : talent.Preferred_Email_Value__c
            , "communication_email" : talent.Email
            , "communication_other_email" : talent.Other_Email__c
            , "communication_work_email" : talent.Work_Email__c
            , "preferred_phone_type" : this.abstractPreferredPhone(talent)
            , "communication_preferred_phone" : talent.Preferred_Phone_Value__c
            , "communication_mobile_phone" : talent.MobilePhone
            , "communication_other_phone" : talent.OtherPhone
            , "communication_home_phone" : talent.HomePhone
            , "communication_work_phone" : talent.Phone
            , "display_address" : talent.Mailing_Address_Formatted__c
            , "talent_id" : talent.Talent_Id__c
            , "associated_position_openings" : null
            , "candidate_id" : talent.AccountId
            //, "associated_position_openings.AssociatedPositionOpening[0].UserArea.Status" :
        };
        return formattedContactRecord;
    },

	toggleSidebar: function(component, event){
        var talent = component.get("v.talent"),
            event = component.getEvent("displayResultsSidebar");

        event.setParams({
            "record" : this.formatContactRecord(talent),
            "recordId" : talent.AccountId,
            "contactId" : talent.Id
        });
        event.fire();	
	},
	openReqSearchModal: function(component, event) {
        var cmpEvent = component.getEvent("EReqSearchModalOpen");
        cmpEvent.setParams({"contactId":component.get("v.talent.Id")
                            ,"talentId":component.get("v.talent.AccountId")
                            ,"record":component.get("v.talent")
                            ,"category":component.get("v.userCategory")});
        cmpEvent.fire();	
	},
	bringUpDeleteModal : function(cmp, event) {
        console.log('showRemoveContactModal');
        var record = cmp.get("v.talent");
        var cmpEvent = cmp.getEvent("removeContactEvent");
        cmpEvent.setParams({"contact":record});
        cmpEvent.fire();
    },
	setActionMenuItems: function(cmp, e) {
		let menuItems = [
			{label: 'Link or Submit to Specific Req', value: 'openReqSearchModal', title: 'Link or Submit to Specific Req'},
			{label: 'Delete from List', value: 'bringUpDeleteModal', title: 'Delete from List'}
		]
		if (cmp.get('v.inPipeline')) {
			menuItems.unshift({label: 'Remove from Pipeline', value: 'removeFromPipeline', title: 'Remove from Pipeline'})
		} else {
			menuItems.unshift({label: 'Add to Pipeline', value: 'appendToPipeline', title: 'Add to Pipeline'})
		}
		cmp.set('v.actionItems', menuItems);
	},

    abstractPreferredPhone : function(talent) {
        var preferredPhoneText = talent.Preferred_Phone__c;
        if(preferredPhoneText == 'Mobile') return 'M';
        if(preferredPhoneText == 'Home') return 'H';
        if(preferredPhoneText == 'Work') return 'W';
        if(preferredPhoneText == 'Other') return 'O';
        if(!preferredPhoneText) {
            if(talent.MobilePhone) return 'M';
            if(talent.HomePhone) return 'H';
            if(talent.Phone) return 'W';
            if(talent.OtherPhone) return 'O';
        }
    },
    openNotesModal: function(component) {
        component.set("v.showNotesModal", !component.get("v.showNotesModal"));
        component.set("v.talentInModal", component.get("v.talent"));
    },
})