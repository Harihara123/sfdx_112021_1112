({
	viewMore : function(component, event, helper) {
		var records = component.get("v.records");
		if(records){
			if(records.length > 0){
				var contactID = records[0].contactId;
				if(contactID){
					var relatedListEvent = $A.get("e.force:navigateToRelatedList");
				    relatedListEvent.setParams({
				        "relatedListId": "et4ae5__SendDefinitions__r",
				        "parentRecordId": contactID
				    });
				    relatedListEvent.fire();
				}
			}
		}
	}
})