({  
    doInit: function(cmp, event, helper) {
        var contactRecord = cmp.get("v.contactRecord");
        cmp.set("v.AccountrecordId", contactRecord.fields.AccountId.value);
		// cmp.set("v.autoMatchLocation", helper.buildAutomatchLocation(contactRecord));
        helper.validateAndSearch(cmp);
    },
    
    linkToViewMore : function(cmp, event, helper) {
        var recordID = cmp.get("v.AccountrecordId");
		var contactRecord = cmp.get("v.contactRecord");
        
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")
				+ "/n/Search_Reqs?c__matchTalentId=" + recordID
				+ "&c__contactId=" + contactRecord.fields.Id.value
				+ "&c__srcName=" + contactRecord.fields.Name.value
				+"&c__noPush=true"
        });
        urlEvent.fire();
	},
	
	//Neel summer 18 URL change-> This method not in use. Method calling code commented in cmp.
    linkToOppty: function(cmp, event, helper) {
        var recordID = event.currentTarget.dataset.recordid;
        //window.open('/one/one.app#/sObject/' + recordID, '_blank');
        var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
        var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
        var dURL;
        
        if(isFlagOn === "true") {
			  dURL = ligtningNewURL+"/r/Opportunity/" + recordID +"/view";
		}
		  else {
			  dURL = ligtningNewURL+"/sObject/" + recordID +"/view";
		}
        
        window.open(dURL, '_blank');
    }
})