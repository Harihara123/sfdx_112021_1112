({
	/* Logic here has to match location building logic in C_SearchCriteriaBase */
	/*buildAutomatchLocation : function(contact) {
		var loc = [];
		if (contact.fields.MailingLatitude.value) {
			loc.push({
				filter : false,
				latlon : [parseFloat(contact.fields.MailingLatitude.value), parseFloat(contact.fields.MailingLongitude.value)],
				geoRadius : "50",
				geoUnitOfMeasure : "mi"
			});
		} else if (contact.fields.MailingCity.value || contact.fields.MailingState.value || contact.fields.Talent_Country_Text__c.value || contact.fields.MailingPostalCode.value) {
			var l = {
				filter : true
			};
			if (contact.fields.MailingCity.value) {
				l.city = contact.fields.MailingCity.value;
			}
			if (contact.fields.MailingState.value) {
				l.state = contact.fields.MailingState.value;
			}
			if (contact.fields.Talent_Country_Text__c.value) {
				l.country = contact.fields.Talent_Country_Text__c.value;
			}
			if (contact.fields.MailingPostalCode.value) {
				l.postalcode = contact.fields.MailingPostalCode.value;
			}
			loc.push(l);
		} 

		return loc;
    },*/
    
    validateAndSearch : function(component) {
        var queryStringUrl = this.createQueryString(component);         
        var action = component.get("c.matchJobs"); 
		        
        action.setParams({
            "queryString" : queryStringUrl,
            "httpRequestType" : "GET", 
            "requestBody" : null, 
            "location" : null
        });

		//set to background
		action.setBackground();

        action.setCallback(this, function(response) {
            var errorMsg = "";
            if (response.getState() === "SUCCESS") {
                var searchResponse = JSON.parse(response.getReturnValue());
                //console.log('searchResponse', searchResponse);

                if (searchResponse.fault) {
                    errorMsg = searchResponse.fault.faultstring;
					component.set("v.errorMessage", errorMsg);
                } else if (searchResponse.header.errors && searchResponse.header.errors.length > 0) {
                    var errors = searchResponse.header.errors;
                    for (var i=0; i<errors.length; i++) {
                        if (errors[i] && errors[i].message) {
                            errorMsg += errors[i].message + " ";
                        }
                    }
                } else if (searchResponse && searchResponse.response && searchResponse.response.hits && searchResponse.response.hits.hits) {
                    if (searchResponse.response.hits.hits.length > 0) {
                        component.set("v.records", searchResponse.response.hits.hits);
                        component.set("v.recordCount", searchResponse.response.hits.total);
                    } else {
                        errorMsg = "No matched REQs found";
                        component.set("v.recordCount", 0);
                    }

					if (!(searchResponse.response.location
						&& searchResponse.response.location.length > 0
                        && searchResponse.response.location[0].geoRadius )) {
						component.set("v.errMessage", $A.get("$Label.c.ATS_MATCHED_REQS_INACCURATE_LOCATION"));
					}
                } else {
                    errorMsg = $A.get("$Label.c.UNKNOWN_ERROR_FETCHING_RESULTS");
                }

				component.set("v.loading", false);
            } else {
                var errors = response.getError();
                if (errors) {
                    for (var i=0; i<errors.length; i++) {
                        if (errors[i] && errors[i].message) {
                            errorMsg += errors[i].message + " ";
                        }
                    }
                }
            }

			/*var loc = component.get("v.autoMatchLocation");
            if (loc.length === 0 || loc[0].filter) {
				// Warning message if results were retrieved using no location / location "filter" instead of geo radius.
                component.set("v.errMessage", $A.get("$Label.c.ATS_MATCHED_REQS_INACCURATE_LOCATION"));
            }*/

            // console.log(errorMsg);
        });
        $A.enqueueAction(action);

	},
    
    createQueryString : function (component) {

        var parameters = {
            "docId" : component.get("v.AccountrecordId"),
            "size" : "5", 
            "from" : "0",
            "type": "talent2job",
            "flt.group_filter" : "AGS,AG_EMEA,RPO,MLA,AP,AEROTEK,TEK",
            "facetFlt.req_stage" : "draft|qualified|presenting|interviewing",
            "appId" : component.get("v.searchAppId"),
            "opco": component.get("v.usrOpco")
        };

		/*if (component.get("v.autoMatchLocation")) {
			parameters.location = JSON.stringify(component.get("v.autoMatchLocation"));
		}*/

		return (this.buildUrl(parameters));
	},
    
    buildUrl : function (parameters) {
         var qs = "";
         for(var key in parameters) {
            var value = parameters[key];
            qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
         }
         if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
         }
         return qs;
     }
})