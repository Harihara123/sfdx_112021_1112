({
    doInit : function(component, event, helper) {
        component.set("v.schema", [		
        [$A.get('$Label.c.ATS_CANDIDATE_STATUS'), "Customer_Status__c"],		
		[$A.get('$Label.c.ATS_PEOPLESOFT_ID'), "Peoplesoft_ID__c"],		
        [$A.get('$Label.c.ATS_PREFIX'), "Salutation"],		
        [$A.get('$Label.c.ATS_FIRST_NAME'), "FirstName"],		
        [$A.get('$Label.c.ATS_PREFERRED_NAME'), "Preferred_Name__c"],		
        [$A.get('$Label.c.ATS_MIDDLE_NAME'), "MiddleName"],		
        [$A.get('$Label.c.ATS_LAST_NAME'), "LastName"],		
        [$A.get('$Label.c.ATS_SUFFIX'), "Suffix"],		
        [$A.get('$Label.c.ATS_JOB_TITLE'), "Title"],		
        [$A.get('$Label.c.ATS_CURRENT_LOCATION')],		
        [$A.get('$Label.c.ATS_MOBILE_PHONE'), "MobilePhone"],		
        [$A.get('$Label.c.ATS_HOME_PHONE'), "HomePhone"],		
        [$A.get('$Label.c.ATS_WORK_PHONE'), "Phone"],		
        [$A.get('$Label.c.ATS_OTHER_PHONE'), "OtherPhone"],		
        [$A.get('$Label.c.ATS_EMAIL'), "Email"],		
        [$A.get('$Label.c.ATS_WORK_EMAIL'), "Work_Email__c"],		
        [$A.get('$Label.c.ATS_OTHER_EMAIL'), "Other_Email__c"],			
        [$A.get('$Label.c.ATS_SKILLS'), "Skills__c"],		
        [$A.get('$Label.c.ATS_CANDIDATE_STATUS'), "candidate_Status__c"]		
        //["WorkExtension", "WorkExtension__c"]		
    ]);
		//
        helper.reset (component, event, helper);
		//component.set("v.recordIndicator",'master');
        var myPageRef = component.get("v.pageReference");
        if(myPageRef){
            var masterID = myPageRef.state.c__masterID;
            component.set("v.masterID", masterID);
            var duplicateID = myPageRef.state.c__duplicateID;
            if (duplicateID) {
                component.set("v.duplicateID", duplicateID);
            } else {
                component.set("v.duplicateID", null);
            }
            component.set("v.masterPlaceHolder",$A.get("$Label.c.ATS_MASTER"));
			component.set("v.duplicatePlaceHolder",$A.get("$Label.c.ATS_DUPLICATE"));
            component.set("v.mergeTalent.masterId", component.get("v.masterID") );
            component.set("v.mergeTalent.duplicateId", component.get("v.duplicateID") );
            
            component.set("v.shouldSetMasterDefault",true);
          
            /*    Start for story S-82099 */
           var FromDuplicateComponent = myPageRef.state.c__FromDuplicateComponent;
		   console.log("FromDuplicateComponent-"+FromDuplicateComponent);
             if(FromDuplicateComponent ==='yes'){
                helper.showCompareBlock(component, event, helper);
                component.set('v.compareTable',true);
				
            }
          /* End for story S-82099 */
            
      }
	
    },

    
    onPageReferenceChanged: function(component, event, helper) {
        
        helper.reset (component, event, helper);
        var myPageRef = component.get("v.pageReference");
        if(myPageRef){
            var masterID = myPageRef.state.c__masterID;
            component.set("v.masterID", masterID);
            var duplicateID = myPageRef.state.c__duplicateID;
            if (duplicateID) {
                component.set("v.duplicateID", duplicateID);
            } else {
                component.set("v.duplicateID", null);
            }
            
            component.set("v.mergeTalent.masterId", component.get("v.masterID") );
            component.set("v.mergeTalent.duplicateId", component.get("v.duplicateID") );
            
            component.set("v.shouldSetMasterDefault",true);
          /*    Start for story S-82099 */
           var FromDuplicateComponent = myPageRef.state.c__FromDuplicateComponent;
             if(FromDuplicateComponent ==='yes'){
                helper.showCompareBlock(component, event, helper);
                component.set('v.compareTable',true);
            }
          /* End for story S-82099 */
            
        }
      },
    
    /* Start For Story STORY S-80480 */
    
    NavigatetoMasterRecord : function(component , event, helper){
        var RecordID = component.get("v.masterRecord.Id");
        window.open('/lightning/r/Contact/' + RecordID+'/view','_Blank');  
    },
    
    NavigatetoDuplicateRecord : function (component, event, helper) {
        var RecordID = component.get("v.duplicateRecord.Id");
        window.open('/lightning/r/Contact/' + RecordID+'/view','_Blank');  
    },
    
    /* End For Story STORY S-80480 */
    
    apply: function(component,event,helper){
		
		component.set("v.schema", [		
        [$A.get('$Label.c.ATS_CANDIDATE_STATUS'), "Customer_Status__c"],		
		[$A.get('$Label.c.ATS_PEOPLESOFT_ID'), "Peoplesoft_ID__c"],		
        [$A.get('$Label.c.ATS_PREFIX'), "Salutation"],		
        [$A.get('$Label.c.ATS_FIRST_NAME'), "FirstName"],		
        [$A.get('$Label.c.ATS_PREFERRED_NAME'), "Preferred_Name__c"],		
        [$A.get('$Label.c.ATS_MIDDLE_NAME'), "MiddleName"],		
        [$A.get('$Label.c.ATS_LAST_NAME'), "LastName"],		
        [$A.get('$Label.c.ATS_SUFFIX'), "Suffix"],		
        [$A.get('$Label.c.ATS_JOB_TITLE'), "Title"],		
        [$A.get('$Label.c.ATS_CURRENT_LOCATION')],		
        [$A.get('$Label.c.ATS_MOBILE_PHONE'), "MobilePhone"],		
        [$A.get('$Label.c.ATS_HOME_PHONE'), "HomePhone"],		
        [$A.get('$Label.c.ATS_WORK_PHONE'), "Phone"],		
        [$A.get('$Label.c.ATS_OTHER_PHONE'), "OtherPhone"],		
        [$A.get('$Label.c.ATS_EMAIL'), "Email"],		
        [$A.get('$Label.c.ATS_WORK_EMAIL'), "Work_Email__c"],		
        [$A.get('$Label.c.ATS_OTHER_EMAIL'), "Other_Email__c"],			
        [$A.get('$Label.c.ATS_SKILLS'), "Skills__c"],		
        [$A.get('$Label.c.ATS_CANDIDATE_STATUS'), "candidate_Status__c"]		
        //["WorkExtension", "WorkExtension__c"]		
    ]);

        component.set("v.Spinner", true);
        //var spinner = component.find('spinner');
        //$A.util.addClass(spinner, 'slds-show'); 
        var selectedValue;
        var duplicateValue;
        
        if (document.getElementById('radio-1').checked) {
            selectedValue  = document.getElementById('radio-1').value;
            duplicateValue = document.getElementById('radio-2').value;
            component.set("v.mergeTalent.masterId", selectedValue);
        	component.set("v.mergeTalent.duplicateId", duplicateValue );   
        }
        else if(document.getElementById('radio-2').checked){
            selectedValue = document.getElementById('radio-2').value;
            duplicateValue = document.getElementById('radio-1').value;
            component.set("v.mergeTalent.masterId", selectedValue );
        	component.set("v.mergeTalent.duplicateId", duplicateValue );
        }
        
        var masterID = component.get("v.masterID");
        var duplicateID = component.get("v.duplicateID");
        var Duplicate = component.find("Duplicate");
        var message = "";
        var messageType = "Error";
        var title = "Error";
        
        if(masterID == '' || masterID == undefined){
            //message = "Master Candidate field can not be blank."  
            message = $A.get("$Label.c.ATS_MAST_CND_BLANK");
        }else{
            masterID = masterID.trim();
            if(!masterID.startsWith('C-')){
                if(!masterID.startsWith('c-')){
                	//message = "Please enter valid Talent Id for Master."
                	message = $A.get("$Label.c.ATS_VLD_MSTRID");
                }
            }
            
        }
        
        if(duplicateID == '' || duplicateID == undefined){
            //message = "Duplicate Candidate field can not be blank."
            message = $A.get("$Label.c.ATS_DUPT_CND_BLANK");
        }else{
            duplicateID = duplicateID.trim();            
            if(!duplicateID.startsWith('C-')){
                if(!duplicateID.startsWith('c-')){
                	//message = "Please enter valid Talent Id for Duplicate."
                    message = $A.get("$Label.c.ATS_VLD_DUPTID");
                }
            }
            
        }
        
        
        
        if((masterID == '' || masterID == undefined) && (duplicateID == '' || duplicateID == undefined)){
            //message = "Master Candidate and Duplicate Candidate fields can not be Blank."
            message = $A.get("$Label.c.ATS_MAST_DUPT_BLANKID");
        }else if((masterID != '' || masterID != undefined) && (duplicateID == '' || duplicateID == undefined)){
            if(!masterID.startsWith('C-')){
                if(!masterID.startsWith('c-')){
                	//message = "Please enter valid Talent Id for Master and Duplicate Candidate field can not be Blank."
                    message = $A.get("$Label.c.ATS_MAST_VLD_CND_ID");
                }
            }
        }else if((masterID == '' || masterID == undefined) && (duplicateID != '' || duplicateID != undefined)){
            if(!duplicateID.startsWith('C-')){
                if(!duplicateID.startsWith('c-')){
                	//message = "Please enter valid Talent Id for Duplicate and Master Candidate field can not be Blank."
                    message = $A.get("$Label.c.ATS_CND_VLD_MAST_ID");
                }
            }
        }else{
            if(!masterID.startsWith('C-') && !duplicateID.startsWith('C-')){
                if(!masterID.startsWith('c-') && !duplicateID.startsWith('c-')){
                	//message = "Please enter valid Talent Id for Master and Duplicate."
                    message = $A.get("$Label.c.ATS_MAST_CND_VLD");
                }
            }else if(masterID === duplicateID){
                //message = "Master Candidate and Duplicate Candidate can not be Same."  
                message = $A.get("$Label.c.ATS_MAST_CND_ID_SAME");
            }
        }
        
        
        if(message !== ""){
            helper.showError(component, message, messageType, title);
            helper.validate(component, event, helper, masterID, duplicateID);
            
            //$A.util.removeClass(spinner, 'slds-show');
            //$A.util.addClass(spinner, 'slds-hide');
            component.set("v.Spinner", false);
        }else{
            if(!(document.getElementById('radio-1').checked || document.getElementById('radio-2').checked)){
                //message = "Please select the Master Candidate."
                message = $A.get("$Label.c.ATS_VLD_MSTR");
                var masterField = component.find("Master");
                var duplicateField = component.find("Duplicate");
                masterField.set("v.errors", null);
                duplicateField.set("v.errors", null);
                helper.showError(component, message, messageType, title);
                //$A.util.removeClass(spinner, 'slds-show');
                //$A.util.addClass(spinner, 'slds-hide');
                component.set("v.Spinner", false);
            }
            else{
                var masterField = component.find("Master");
                var duplicateField = component.find("Duplicate");
                masterField.set("v.errors", null);
                duplicateField.set("v.errors", null);
                //$A.util.removeClass(spinner, 'slds-show');
                //$A.util.addClass(spinner, 'slds-hide');
                //component.set("v.Spinner", false);
                helper.showCompareBlock(component, event, helper);
                component.set('v.compareTable',true);
            }
        }    
    },
    
    selectedRecord : function(cmp,event,helper){
        var recordIndicator = cmp.get('v.recordIndicator');
        var selectedValue;
        var duplicateValue;
        if (document.getElementById('radio-1').checked) {
            selectedValue  = document.getElementById('radio-1').value;
            duplicateValue = document.getElementById('radio-2').value;
            recordIndicator = 'master';
			//recordIndicator=$A.get("$Label.c.ATS_MASTER");
			cmp.set("v.masterPlaceHolder",$A.get("$Label.c.ATS_MASTER"));
			cmp.set("v.duplicatePlaceHolder",$A.get("$Label.c.ATS_DUPLICATE"));

            cmp.set("v.mergeTalent.masterId", selectedValue );
            cmp.set("v.mergeTalent.duplicateId", duplicateValue );

			cmp.set("v.masterID", selectedValue );
            cmp.set("v.duplicateID", duplicateValue );
			cmp.set("v.shouldSetMasterDefault",true);
			
        }
        else if(document.getElementById('radio-2').checked){
            selectedValue = document.getElementById('radio-2').value;
            duplicateValue = document.getElementById('radio-1').value;
            recordIndicator = 'duplicate';
			cmp.set("v.duplicatePlaceHolder",$A.get("$Label.c.ATS_MASTER"));
			cmp.set("v.masterPlaceHolder",$A.get("$Label.c.ATS_DUPLICATE"));
			//recordIndicator = $A.get("$Label.c.ATS_DUPLICATE").toLowerCase();
            cmp.set("v.mergeTalent.masterId", selectedValue );
            cmp.set("v.mergeTalent.duplicateId", duplicateValue );
			
			cmp.set("v.masterID", duplicateValue );
            cmp.set("v.duplicateID", selectedValue );
			cmp.set("v.shouldSetMasterDefault",false);
			/*if(cmp.get("v.masterRecordID") != '' && cmp.get("v.duplicateRecId") !=''){
				component.set("v.mergeTalent.masterRecId", cmp.get("v.duplicateRecordID"));
				component.set("v.mergeTalent.duplicateRecId", cmp.get("v.masterRecordID"));
            }*/
        }
        
        //helper.showCompareBlock(cmp, event, helper, true);
        cmp.set('v.recordIndicator',recordIndicator);
        //console.log(recordIndicator);
		//console.log('MasterRecID----'+v.mergeTalent.masterRecId);
		//console.log('DuplicateRecID----'+v.mergeTalent.duplicateRecordID);
    },
    reset : function(component, event, helper){
        helper.reset(component, event, helper);
    },
    
    confirmMerge : function(component, event, helper){

    	console.log('==================' + JSON.stringify( component.get('v.mergeTalent') ) );
        component.set('v.showConfirmationModal',true);
        component.set('v.showConfirmationModal',false); 
        
    },
    talentMergeCall : function(component, event, helper){
        
        if (event.getParam('confirmationVal') === 'Y') {
            //change the method name           
            helper.talentMerge (component, event, helper);
        }
        else{
            //$A.get('e.force:refreshView').fire();
            //console.log('In the Talent Merge call else part')
            // helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
            // helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');
            component.set('v.showConfirmationModal',false);
        }
    },
    
    handleError:function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.addClass(comp, "error");   
    },
    logData: function(cmp, event) {
        var mRecord = cmp.get("masterRecord");
        var dRecord = cmp.get("duplicateRecord");
        console.log("master:", mRecord);
        console.log("duplicate:", dRecord);
    },
    handleClearError:function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.removeClass(comp, "error");   
    },
    goToAccount : function(component,event,helper){
        
        var urlEvent = $A.get("e.force:navigateToURL");
        var recordId = component.get("v.recordId");
        urlEvent.setParams({
            "url": "/one/one.app#/sObject/" + recordId +"/view",
            "isredirect":true
        });
        urlEvent.fire(); 
    },
    NavigateMaster : function(component,event,helper){
        
        // component.set('v.spinner', true);
        //$A.util.addClass(spinner, 'slds-show');
        var masterID = component.get("v.masterID");
        var Duplicate = component.find("Duplicate");
        var message = "";
        var messageType = "Error";
        var title = "Error";
        if(masterID == '' || masterID == undefined){
            //message = "Master Candidate field can not be blank."
            message = $A.get("$Label.c.ATS_MAST_CND_BLANK");
        }else{
            masterID = masterID.trim();
            if(!masterID.startsWith('C-')){
                if(!masterID.startsWith('c-')){
                	//message = "Please enter valid Talent Id for Master.";
                    message = $A.get("$Label.c.ATS_VLD_MSTRID");
                }
                else{
        			helper.NavigatetoRecord(component,masterID);            
                }
            }
            else{
                helper.NavigatetoRecord(component,masterID);
            }
            
        }
        
        if(message !== ""){
            helper.showError(component, message, messageType, title);
            //$A.util.removeClass(spinner, 'slds-show');
            //$A.util.addClass(spinner, 'slds-hide');
           // component.set("v.Spinner", false);
        }
        
    },
    NavigateDuplicate : function(component,event,helper){
        
      //  component.set('v.Spinner', true);
        //$A.util.addClass(spinner, 'slds-show');
      var duplicateID = component.get("v.duplicateID");
        var Duplicate = component.find("Duplicate");
        var message = "";
        var messageType = "Error";
        var title = "Error";
        if(duplicateID == '' || duplicateID == undefined){
            //message = "Duplicate Candidate field can not be blank."   
            message = $A.get("$Label.c.ATS_DUPT_CND_BLANK");
        }else{
            duplicateID = duplicateID.trim();            
            if(!duplicateID.startsWith('C-')){
                if(!duplicateID.startsWith('c-')){
                	//message = "Please enter valid Talent Id for Duplicate."
                    message = $A.get("$Label.c.ATS_VLD_DUPTID");
                }
                else{
                  helper.NavigatetoRecord(component,duplicateID);   
                }
            }
            else{
                helper.NavigatetoRecord(component,duplicateID);   
            }
            
        }
        if(message !== ""){
            helper.showError(component, message, messageType, title);
            //$A.util.removeClass(spinner, 'slds-show');
            //$A.util.addClass(spinner, 'slds-hide');
           // component.set("v.Spinner", false);
        }
        
    }
})