({
/*	doInit: function(component, event, helper) {
    component.set('v.schema', [
        ["Candidate Status", "Customer_Status__c"],
		["Peoplesoft ID", "Peoplesoft_ID__c"],
        ["Prefix", "Salutation"],
        [$A.get('$Label.c.ATS_FIRST_NAME'), "FirstName"],
        ["Preferred Name", "Preferred_Name__c"],
        ["Middle Name", "MiddleName"],
        ["Last Name", "LastName"],
        ["Suffix", "Suffix"],
        ["Job Title", "Title"],
        ["Current Location"],
        ["Mobile Phone", "MobilePhone"],
        ["Home Phone", "HomePhone"],
        ["Work Phone", "Phone"],
        ["Other Phone", "OtherPhone"],
        ["Email", "Email"],
        ["Work Email", "Work_Email__c"],
        ["Other Email", "Other_Email__c"],
        ["Skills", "Skills__c"],
        ["candidate Status", "candidate_Status__c"]
        //["WorkExtension", "WorkExtension__c"]
    ])},*/
    isInAccount: {
        "Skills": true,
        "Last Modified Date": true,
		"Peoplesoft ID": true
    },
	getPresentableData: function( component, schema, mergeData) {
	console.log(component.get('v.schema'));
        
        let newArray = [];
        let values = {
            differentValues: 0,
            lostValues: 0,
            masterId: component.get("v.masterID"),
            duplicateId: component.get("v.duplicateID")
        }
        for (let item of schema) {
            let data = {
                label: item[0],
                key: item[1],
                masterValue: this.isInAccount[item[0]] ? mergeData[0].Account[item[1]] : mergeData[0][item[1]],		//Check if skill is nested inside Account object.
                duplicateValue: this.isInAccount[item[0]] ? mergeData[1].Account[item[1]] : mergeData[1][item[1]],	//^^
            };	
				
            data.validation = this.validatedPair(data.masterValue, data.duplicateValue);
            if (data.label === 'Skills') {
				if(data.masterValue && data.duplicateValue) { //Checking if this value exists.
					let parsed = {
						master: JSON.parse(data.masterValue).skills,
						duplicate: JSON.parse(data.duplicateValue).skills
					}
					if(parsed.master.length === 0 && parsed.duplicate.length === 0) {
						data.masterValue = null;
						data.duplicateValue = null;
					} else {
						let comparedData = this.compareArrays(parsed.master, parsed.duplicate); // compareArrays method checks for same elements between arrays.
						data.masterValue = comparedData.validatedMasterSkills;
						data.duplicateValue = comparedData.validatedDuplicateSkills;
					}

				} else {
					if(!data.masterValue && !data.duplicateValue) {
						data.validation.exists = false;
					} else {
						if(data.masterValue) {
							let parsedMaster = JSON.parse(data.masterValue).skills;
							console.log(parsedMaster)
							if(parsedMaster.length > 0) {
								data.masterValue = this.arrayToObjArray(parsedMaster);
								//data.masterSliced = data.masterValue.slice(0,5);
								//data.masterFull = data.masterValue;
								//data.masterValue = data.masterSliced;
							} else {data.masterValue = null;}							
						}
						if(data.duplicateValue) {
							let parsedDuplicate = JSON.parse(data.duplicateValue).skills;
							console.log(parsedDuplicate)
							if(parsedDuplicate.length > 0) {
								data.duplicateValue = this.arrayToObjArray(parsedDuplicate);
								//data.duplicateSliced = data.duplicateValue.slice(0,5);
								//data.duplicateFull = data.duplicateValue;
								//data.duplicateValue = data.duplicateSliced;
							} else {data.duplicateValue = null;}							
						}						
						//data.masterValue = data.masterValue ? this.arrayToObjArray(JSON.parse(data.masterValue).skills, false) : null;
						//data.duplicateValue = data.duplicateValue ? this.arrayToObjArray(JSON.parse(data.duplicateValue).skills, false) : null;
						data.validation = this.validatedPair(data.masterValue, data.duplicateValue);
					}				
				}
				data.validation = this.validatedPair(data.masterValue, data.duplicateValue);
            }	
            if (data.label === 'Current Location') {
                data.masterValue = this.appendAddress('', mergeData[0]);
                data.duplicateValue = this.appendAddress('', mergeData[1]);
                
                data.validation = this.validatedPair(data.masterValue, data.duplicateValue);
            }
            // Check work extention S-101373
            if(data.label === 'Work Phone'){
                // Assuming that the work phone has already been validated
                var primExtn = mergeData[0]['WorkExtension__c'];
                var dupExtn = mergeData[1]['WorkExtension__c'];
                if(primExtn != undefined){
                    data.masterValue = data.masterValue + ' (' + primExtn +')';
                }
                if(dupExtn != undefined){
                    data.duplicateValue = data.duplicateValue + ' (' + dupExtn +')';
                }
                if(data.validation.equals){
                    data.validation.equals = (primExtn == dupExtn);
                }
            }
			
            if(!data.validation.equals && data.validation.exists) {values.differentValues++};
            if(!data.validation.equals) {
                if(data.duplicateValue && data.masterValue) {values.lostValues++};			
            }
            
            newArray.push(data)
        }
        newArray.unshift(values);
        return newArray;
    },
	compareArrays: function(arr1, arr2) {
		let normalizedArray1 = arr1.join('|').toLowerCase().split('|'); //converting an array into a string and lower casing.
		let normalizedArray2 = arr2.join('|').toLowerCase().split('|'); //^^
		let matches = normalizedArray2.filter(function(skill) { //array.filter() returns an array of all items that are true
		  return normalizedArray1.includes(skill); //array.includes(skill) is true if skill element is found in array.
		})

		let array1Diff = this.arrayToObjArray(normalizedArray1.filter(function(skill){return !matches.includes(skill)}), false); //returns elements in array that are NOT the same
		let array2Diff = this.arrayToObjArray(normalizedArray2.filter(function(skill){return !matches.includes(skill)}), false); //^^
		matches = this.arrayToObjArray(matches, true); // arrayToObjArray converts a simple array into object array with label and class key value pairs

		return {
		  validatedMasterSkills: matches.concat(array1Diff), //add matched skills array with differences skills array
		  validatedDuplicateSkills: matches.concat(array2Diff) //^^
		}		
	},
	  arrayToObjArray: function(array, matches) {
    let returnArray = [];

    for (let skill of array) {
      returnArray.push({label: skill, class: matches ? 'same-skill' : 'diff-skill'})
    }
    return returnArray;  
  },
    showCompareBlock : function(component, event, helper ) {
		      
        var action = component.get('c.getTalentRecords');
        action.setParams({masterTalentID : component.get('v.masterID').trim(),duplicateTalentID: component.get('v.duplicateID').trim()});
      
        action.setCallback(this,function(response){
	
			component.set('v.Spinner', false);			
            var state = response.getState();
	
            if(state == 'SUCCESS'){
				console.log('Return Value:'+response.getReturnValue());
				var responseRecord = response.getReturnValue();
				if(responseRecord[0].Record_Type_Name__c != 'Talent') {
					var message = 'Talent Merge is only allowed between two talents. Talent ID ' + responseRecord[0].Talent_Id__c +' is a client Account.';
					this.showError(component, message, 'Error','Error');
					component.set("v.isTalent", false);
				} else if(responseRecord[1].Record_Type_Name__c != 'Talent') {
					var message = 'Talent Merge is only allowed between two talents. Talent ID ' + responseRecord[1].Talent_Id__c +' is a client Account.';
					this.showError(component, message, 'Error','Error');
					component.set("v.isTalent", false);					
				}
				else {
					component.set("v.isTalent", true);
					component.set('v.mergeArray', this.getPresentableData(component, component.get('v.schema'), response.getReturnValue()));				
                
					//var responseRecord = response.getReturnValue();
					var masterCurrentLocation = component.get('v.masterCurrentLocation');
					var duplicateCurrentLocation = component.get('v.duplicateCurrentLocation');
					var masterSkills = [];
					var duplicateSkills = [];
					var masterTalentPrefernces ;
					var duplicateTalentPrefernces;
					var mastercountriesAuthWork = [];
					var duplicatecountriesAuthWork = [];
					//console.log("+++++"+ JSON.stringify( responseRecord));
                
				   /* if(responseRecord[0].Record_Type_Name__c != 'Talent') {
						var message = 'Talent Merge is only allowed between two talents. Talent ID ' + responseRecord[0].Talent_Id__c +' is a client Account.';
						this.showError(component, message, 'Error','Error');
						component.set("v.isTalent", false);
					} else if(responseRecord[1].Record_Type_Name__c != 'Talent') {
						var message = 'Talent Merge is only allowed between two talents. Talent ID ' + responseRecord[1].Talent_Id__c +' is a client Account.';
						this.showError(component, message, 'Error','Error');
						component.set("v.isTalent", false);					
					}*/
				
					if(responseRecord[0].Account.Skills__c == undefined || responseRecord[0].Account.Skills__c == null){
						masterSkills = [];
					}else{
						masterSkills = JSON.parse(responseRecord[0].Account.Skills__c);
					}
                
					if(responseRecord[1].Account.Skills__c == undefined || responseRecord[1].Account.Skills__c == null){
						duplicateSkills = [];
					}else{
						duplicateSkills = JSON.parse(responseRecord[1].Account.Skills__c);
					}
                
                
					if(responseRecord[0].Account.Talent_Preference_Internal__c == undefined || responseRecord[0].Account.Talent_Preference_Internal__c == "\"\""){
						mastercountriesAuthWork = [];
					}else{
						masterTalentPrefernces = JSON.parse(responseRecord[0].Account.Talent_Preference_Internal__c);
						mastercountriesAuthWork = masterTalentPrefernces.employabilityInformation.eligibleIn;
					}
                
					if(responseRecord[1].Account.Talent_Preference_Internal__c == undefined || responseRecord[1].Account.Talent_Preference_Internal__c == "\"\""){
						duplicatecountriesAuthWork = [];
                    
					}else{
						duplicateTalentPrefernces = JSON.parse(responseRecord[1].Account.Talent_Preference_Internal__c);
						duplicatecountriesAuthWork = duplicateTalentPrefernces.employabilityInformation.eligibleIn;
					}
                
					//console.log(mastercountriesAuthWork);
                
					masterCurrentLocation = this.appendAddress(masterCurrentLocation,responseRecord[0] );
					duplicateCurrentLocation = this.appendAddress(masterCurrentLocation,responseRecord[1] );
                
                
                
					component.set('v.mastercountriesAuthWork',mastercountriesAuthWork);
					component.set('v.duplicatecountriesAuthWork',duplicatecountriesAuthWork);
					component.set('v.masterTalentPrefernces',masterTalentPrefernces);
					component.set('v.duplicateTalentPrefernces',duplicateTalentPrefernces);
					component.set('v.masterSkills',masterSkills.skills);
					component.set('v.duplicateSkills',duplicateSkills.skills);
					component.set('v.masterCurrentLocation',masterCurrentLocation);
					component.set('v.duplicateCurrentLocation',duplicateCurrentLocation);
					component.set('v.masterRecord',responseRecord[0]);
					component.set('v.duplicateRecord',responseRecord[1]);
					component.set('v.showMerge',true);
					
					component.set('v.mergeTalent.masterRecId', responseRecord[0].Id );
					component.set('v.mergeTalent.duplicateRecId', responseRecord[1].Id );
					component.set('v.masterRecordID',responseRecord[0].Id );
					component.set('v.duplicateRecordID',responseRecord[1].Id );
               
                               
					helper.callServer(component,'','',"getLanguages",function(response){
						if(component.isValid()){
							var opt = {};  
							var key = '';
							for(key in response) {     //this iterates over the returned map
								opt[key] = response[key];
							}
							component.set("v.languageList", opt);
							this.populateLanguage(component);
                        
						}
					},null,true);
             
				}   
                
            }
            else if(state == 'INCOMPLETE'){
                console.log('State is Incomplete')
            }
                else if(state== 'ERROR'){
                    var errors = response.getError();
                    if(errors){
                        if(errors[0] && errors[0].message){
                            console.log('Error Message '+ errors[0].message);
							
							this.showError(component, 'Unable to Compare, Please contact System Administrator' ,'Error','Error');
                        }
                    }
                    else{
                        console.log('Unkonw Errors');
                    }
                    
                }
				
            
        });
        
        $A.enqueueAction(action);
    },
    validatedPair: function(master, duplicate) {
        if(master && duplicate) {
            if(master === duplicate) {
                return {equals: true, exists: true};
            } else {
                return {equals: false, exists: true};
            }
        } else if (master || duplicate) {
            return {equals: false, exists: true};
        } else {return {equals: false, exists: false}}
    },
    appendAddress : function(currentLocation, record){  
        currentLocation = '';
        if(record.MailingStreet == undefined){
            currentLocation =currentLocation;
        } else {
            currentLocation =currentLocation+ record.MailingStreet +' ';
        }
        if(record.MailingCity == undefined){
            currentLocation =currentLocation ;
        } else {
            currentLocation =currentLocation+ record.MailingCity +' ';
        }
        if(record.MailingState == undefined){
            currentLocation =currentLocation ;
        } else {
            currentLocation =currentLocation+ record.MailingState +' ';
        }
        if(record.MailingPostalCode == undefined){
            currentLocation =currentLocation ;
        } else {
            currentLocation =currentLocation+ record.MailingPostalCode +' ';
        }
        if(record.Talent_Country_Text__c == undefined){
            currentLocation =currentLocation ;
        } else {
            currentLocation =currentLocation+ record.Talent_Country_Text__c +' ';
        }
        return currentLocation;
    },
    populateLanguage:function(cmp){
        // console.log("Language List : "+cmp.get("v.languageList"));
        var currentLangMaster =  [];
        var currentLangDuplicate =  [];
        var cLangObjMaster = [];
        var cLangObjDuplicate = [];
        var langMap = {};
        
        
        if(cmp.get("v.masterTalentPrefernces") != '\"\"' && cmp.get("v.masterTalentPrefernces") != undefined) {
            if(cmp.get("v.masterTalentPrefernces.languages").length > 0){
                currentLangMaster = cmp.get("v.masterTalentPrefernces.languages"); 
                langMap = cmp.get("v.languageList"); 
                for(var i = 0;i<currentLangMaster.length;i++){
                    cLangObjMaster.push({"key" : currentLangMaster[i],"value" : langMap[currentLangMaster[i]]});
                }
                
                cmp.set("v.masterLanguages",cLangObjMaster);
            }
            
        }
        
        if(cmp.get("v.duplicateTalentPrefernces") != '\"\"' && cmp.get("v.duplicateTalentPrefernces") != undefined) {
            if(cmp.get("v.duplicateTalentPrefernces.languages").length > 0){
                currentLangDuplicate = cmp.get("v.duplicateTalentPrefernces.languages"); 
                langMap = cmp.get("v.languageList");                
                for(var i = 0;i<currentLangDuplicate.length;i++){
                    cLangObjDuplicate.push({"key" : currentLangDuplicate[i],"value" : langMap[currentLangDuplicate[i]]});
                }
                
                cmp.set("v.duplicateLanguages",cLangObjDuplicate);
            }
            
        }
        
    },
    
    talentMerge : function(component, event, helper) {
        
        component.set("v.Spinner", true);
        //var spinner = component.find('spinner');
        //$A.util.addClass(spinner, 'slds-show'); 
        var masterID = component.get('v.masterID').trim();
        var duplicateID = component.get('v.duplicateID').trim();
        var message = "";
        var messageType = "Error";
        var title = "Error"; 
        var recordIndicator = component.get('v.recordIndicator');
		//recordIndicator = recordIndicator.toLowerCase();
		//var duplicate = 'duplicate';
		if(recordIndicator == 'duplicate'){
            masterID = component.get('v.duplicateID');
            duplicateID = component.get('v.masterID');
        }
        console.log('master ID------------------- '+masterID);
        console.log('duplicate ID---------------- '+duplicateID);
		console.log('recordIndicator---------------- '+recordIndicator);
        var action = component.get("c.talentMerge");
        action.setParams({ masterRecordId :masterID , duplicateRecordId :duplicateID });
        action.setCallback(this, function(response) {
			component.set("v.Spinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                const parsedResponse = JSON.parse(response.getReturnValue())
                if(parsedResponse.Message != 'Merge is Successful.'){
                    
                    // validation or precheck rule failure
                    message = parsedResponse.Message;
                    messageType = "Error";
                    title = "Error";
                    this.showError(component, message, messageType, title);
                }
                else{
                    
                    //Successful merge with sucessfull precheck rule
                    message = parsedResponse.Message;
                    messageType = "success";
                    title = "Success";
                    // this.gotoNextScreen(component, message, messageType, masterID, duplicateID);
                  //  this.showError(component, message, messageType, title);
				  component.set("v.mergeMessage", 'Merge is Successful.');
                    this.showMergeMasterLink(component, message, messageType, masterID, duplicateID);
                }
                //$A.util.removeClass(spinner, 'slds-show');
                //$A.util.addClass(spinner, 'slds-hide');
                component.set("v.Spinner", false);
                var comp = event.getSource();
                $A.util.removeClass(comp, "error");
                
            }
            else if (state === "INCOMPLETE") {
                
                message = "Something went wrong. Merge is INCOMPLETE";
                messageType = "Error";
                title = "Error";
                this.showError(component, message, messageType, title);
                //$A.util.removeClass(spinner, 'slds-show');
                //$A.util.addClass(spinner, 'slds-hide');
                component.set("v.Spinner", false);
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            message = response.getReturnValue();
                            messageType = "Error";
                            title = "Error";
                            this.showError(component, message, messageType, title);
                            //$A.util.removeClass(spinner, 'slds-show');
                           //$A.util.addClass(spinner, 'slds-hide');
						   component.set("v.Spinner", false);
                        }
                    }else{
                        
                        message = "Unknown Error.";
                        messageType = "Error";
                        title = "Error";
                        this.showError(component, message, messageType, title);
                        //$A.util.removeClass(spinner, 'slds-show');
                        //$A.util.addClass(spinner, 'slds-hide');
						component.set("v.Spinner", false);
                    }
                } });
        $A.enqueueAction(action);
        
    },
    
    showError : function(component, message, messageType, theTitle){
        var title = theTitle;
        var errorMessage = message;
        var theType = messageType;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: theType
        });
        toastEvent.fire();
    },
    showToast : function(component, message, messageType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": message,
            type: messageType
        });
        toastEvent.fire();
    },
    validate : function(component, event, helper, masterID, duplicateID) {
        
        var masterField = component.find("Master");
        var duplicateField = component.find("Duplicate");
        masterField.set("v.errors", null);
        duplicateField.set("v.errors", null);
        
        
        if(masterID == '' || masterID == undefined){
            //masterField.set("v.errors", [{message: "Master Candidate field can not be blank."}]);  
            masterField.set("v.errors", [{message: $A.get("$Label.c.ATS_MAST_CND_BLANK")}]);   
        }else{
            masterID = masterID.trim();
            if(!masterID.startsWith('C-')){
                if(!masterID.startsWith('c-')){
                    //masterField.set("v.errors", [{message: "Please enter valid Talent Id for Master."}]);
					masterField.set("v.errors", [{message:$A.get("$Label.c.ATS_VLD_MSTRID")}]);
                }
            }
            
        }
        
        if(duplicateID == '' || duplicateID == undefined){
            //duplicateField.set("v.errors", [{message: "Duplicate Candidate field can not be blank."}]);
			duplicateField.set("v.errors", [{message: $A.get("$Label.c.ATS_DUPT_CND_BLANK")}]);    
        }else{
            duplicateID = duplicateID.trim();
            if(!duplicateID.startsWith('C-')){
                if(!duplicateID.startsWith('c-')){
                    //duplicateField.set("v.errors", [{message: "Please enter valid Talent Id for Duplicate."}]);
					duplicateField.set("v.errors", [{message: $A.get("$Label.c.ATS_VLD_DUPTID")}]);
                }
            }
            
        }
        
        
        
        if((masterID == '' || masterID == undefined) && (duplicateID == '' || duplicateID == undefined)){
            //masterField.set("v.errors", [{message: "Master Candidate field can not be blank."}]);
            //duplicateField.set("v.errors", [{message: "Duplicate Candidate field can not be blank."}]);
			masterField.set("v.errors", [{message: $A.get("$Label.c.ATS_MAST_CND_BLANK")}]);
            duplicateField.set("v.errors", [{message: $A.get("$Label.c.ATS_DUPT_CND_BLANK")}]);
        }else if((masterID != '' || masterID != undefined) && (duplicateID == '' || duplicateID == undefined)){
            if(!masterID.startsWith('C-')){
                if(!masterID.startsWith('c-')){
                    //masterField.set("v.errors", [{message: "Please enter valid Talent Id for Master."}]);
					masterField.set("v.errors", [{message: $A.get("$Label.c.ATS_VLD_MSTRID")}]);
                }
                //duplicateField.set("v.errors", [{message: "Duplicate Candidate field can not be blank."}]);
				duplicateField.set("v.errors", [{message: $A.get("$Label.c.ATS_DUPT_CND_BLANK")}]);
            }
        }else if((masterID == '' || masterID == undefined) && (duplicateID != '' || duplicateID != undefined)){
            if(!duplicateID.startsWith('C-')){
                
                //masterField.set("v.errors", [{message: "Master Candidate field can not be blank."}]);
				masterField.set("v.errors", [{message: $A.get("$Label.c.ATS_MAST_CND_BLANK")}]);
                if(!duplicateID.startsWith('c-')){
                    //duplicateField.set("v.errors", [{message: "Please enter valid Talent Id for Duplicate."}]);
					duplicateField.set("v.errors", [{message: $A.get("$Label.c.ATS_VLD_DUPTID")}]);
                }
            }
        }else{
            if(!masterID.startsWith('C-') && !duplicateID.startsWith('C-')){
                if(!masterID.startsWith('c-') && !duplicateID.startsWith('c-')){
                    //masterField.set("v.errors", [{message: "Please enter valid Talent Id for Master."}]);
                    //duplicateField.set("v.errors", [{message: "Please enter valid Talent Id for Duplicate."}]);
					masterField.set("v.errors", [{message: $A.get("$Label.c.ATS_VLD_MSTRID")}]);
                    duplicateField.set("v.errors", [{message: $A.get("$Label.c.ATS_VLD_DUPTID")}]);
                }
            }else if(masterID === duplicateID){
                //masterField.set("v.errors", [{message: "Master Candidate and Duplicate Candidate can not be Same."}]);
                //duplicateField.set("v.errors", [{message: "Master Candidate and Duplicate Candidate can not be Same."}]);
				masterField.set("v.errors", [{message: $A.get("$Label.c.ATS_MAST_CND_ID_SAME")}]);
                duplicateField.set("v.errors", [{message: $A.get("$Label.c.ATS_MAST_CND_ID_SAME")}]); 
            }
        }
        
        
    },
    gotoNextScreen : function(component, message, type, masterID, duplicateID){
        var evt = $A.get("e.force:navigateToComponent");
        //console.log('evt'+evt);
        evt.setParams({
            componentDef: "markup://c:C_TalentMergeForm",
            componentAttributes: {
                mergeMessage: message,
                messageType : type,
                masterID : masterID,
                duplicateID : duplicateID
            }
        });
        evt.fire();    
    },
    
    showMergeMasterLink : function(component, message, messageType, masterID, duplicateID){
        
        
        if(message == 'Merge is Successful.'){
            var action = component.get("c.getMasterRecordId");
            action.setParams({ masterTalentID : masterID});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.recordId",response.getReturnValue());
                    console.log("Record ID : "+response.getReturnValue());
                    var recordID = response.getReturnValue();
                    this.RedirecttoTalentRecord(component,recordID);
                }
                else if (state === "INCOMPLETE") {
                    console.log("INCOMPLETE");
                    
                }
                    else if (state === "ERROR") {
                        
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log(errors[0].message);
                            }
                        }else{
                            console.log("Unknown Error");
                            
                        }
                    } 
            });
            $A.enqueueAction(action);   
        } 
        
        component.set('v.mergeMasterID',masterID);
        component.set("v.mergeMessage",message);
        component.set('v.compareTable',false);
        component.set('v.showMerge',false);
        document.getElementById('radio-1').checked = false;
        document.getElementById('radio-2').checked = false;
        component.set('v.masterID','');
        component.set('v.duplicateID','');
        component.set('v.shouldSetMasterDefault',false);
        var masterField = component.find("Master");
        var duplicateField = component.find("Duplicate");
        masterField.set("v.errors", null);
        duplicateField.set("v.errors", null);
        
        
    },
    
    
    reset : function(component, event, helper){
        
        //$A.get('e.force:refreshView').fire();
		let baseUrl = window.location.href.split("?")[0];
		window.history.pushState('name', '', baseUrl);
		
        component.set('v.masterID','');
        component.set('v.duplicateID','');
        console.log("**** "+component.get('v.masterID'));
        component.set('v.compareTable',false);


        //component.set('v.recordIndicator', 'Master');
		component.set('v.recordIndicator','master');
        if (document.getElementById('radio-1')) {
            document.getElementById('radio-1').checked = false;
        }
        if (document.getElementById('radio-2')){
            document.getElementById('radio-2').checked = false;
        }
        var masterField = component.find("Master");
		//var masterField = component.find($A.get("$Label.c.ATS_MASTER"));
        var duplicateField = component.find("Duplicate");
		//var duplicateField = component.find($A.get("$Label.c.ATS_DUPLICATE"));
		component.set("v.masterPlaceHolder",$A.get("$Label.c.ATS_MASTER"));
		component.set("v.duplicatePlaceHolder",$A.get("$Label.c.ATS_DUPLICATE"));
        
        masterField.set("v.errors", null);
        duplicateField.set("v.errors", null);
        
        component.set('v.mastercountriesAuthWork',[]);
        component.set('v.duplicatecountriesAuthWork',[]);
        component.set('v.masterTalentPrefernces','');
        component.set('v.duplicateTalentPrefernces','');
        component.set('v.masterSkills','');
        component.set('v.duplicateSkills','');
        component.set('v.masterCurrentLocation','');
        component.set('v.duplicateCurrentLocation','');
        component.set('v.masterRecord','');
        component.set('v.duplicateRecord','');
        component.set("v.masterLanguages",[]);
        component.set("v.duplicateLanguages",[]);
        component.set('v.showMerge',false);
		component.set("v.mergeArray", null);
        component.set("v.mergeTalent.masterId", null);
        component.set("v.mergeTalent.duplicateId", null );
            
    },
    
    
    toggleClassInverse: function(component,componentId,className) {
        
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    
    NavigatetoRecord : function (component, recordCID){
        
        
        var action = component.get("c.getMasterRecordId");
        action.setParams({ masterTalentID : recordCID});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {                    
                //    console.log("Record ID : "+response.getReturnValue());
                if(response.getReturnValue() !== null){
                    var recordID = response.getReturnValue();   
                    window.open('/lightning/r/Contact/' + recordID +'/view','_Blank');
                }
                
            }
            else if (state === "INCOMPLETE") {
                console.log("INCOMPLETE");
                
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(errors[0].message);
                        }
                    }else{
                        console.log("Unknown Error");
                        
                    }
                } 
        });
        $A.enqueueAction(action);

    },
    RedirecttoTalentRecord :function(component,recordID){
         // To redirect to record
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
            "url": "/one/one.app#/sObject/" + recordID +	"/view?showToast=true",
            
            "isredirect":true
        });
        urlEvent.fire(); 
        
       
    }
})