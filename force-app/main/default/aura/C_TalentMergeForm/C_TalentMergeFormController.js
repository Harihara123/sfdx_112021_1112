({
	doInit : function(component, event, helper) {
        if(component.get("v.mergeMessage") != ""){
            console.log("Merge Message "+component.get("v.mergeMessage"));
            helper.showToast(component, component.get("v.mergeMessage"), component.get("v.messageType"));
        }
        
        if(component.get("v.mergeMessage") == 'Merge is Successful.'){
           var action = component.get("c.getMasterRecordId");
            action.setParams({ masterTalentID :component.get("v.masterID").trim()});
            action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
				component.set("v.recordId",response.getReturnValue());
                console.log("Record ID : "+response.getReturnValue()) ;   
            }
            else if (state === "INCOMPLETE") {
                console.log("INCOMPLETE");
                
             }
             else if (state === "ERROR") {
                 
                 var errors = response.getError();
                 if (errors) {
                     if (errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                     }
                 }else{
                     console.log("Unknown Error");
                    
                 }
             } 
           });
         $A.enqueueAction(action);   
        }
          
    },    
    gotoURL:function(component,event,helper){
        component.set("v.Spinner", true);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show'); 
        
        var masterID = component.get("v.masterID").trim();
        var duplicateID = component.get("v.duplicateID").trim();
        var Duplicate = component.find("Duplicate");
        var message = "";
        var messageType = "Error";
        var title = "Error";
        if(!masterID.startsWith('C-'))
            message = "Please enter valid Talent Id for Master."
        if(!duplicateID.startsWith('C-'))
            message = "Please enter valid Talent Id for Duplicate."
        if(!masterID.startsWith('C-') && !duplicateID.startsWith('C-'))
            message = "Please enter valid Talent Id for Master and Duplicate."
            
        if(masterID === "")
            message = "Master Candidate field can not be blank."
        if(duplicateID === "")
            message = "Duplicate Candidate field can not be blank."
        if(masterID === "" && duplicateID === "")
            message = "Master Candidate and Duplicate Candidate fields can not be blank."    
        if(masterID === duplicateID && duplicateID !== "")
            message = "Master Candidate and Duplicate Candidate can not be the same."
        
            
            
        if(message !== ""){
            helper.showError(component, message, messageType, title);
            helper.validate(component, event, helper, masterID, duplicateID);
            //alert(message); 
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
            component.set("v.Spinner", false);
        }else{
            var evt = $A.get("e.force:navigateToComponent");
            console.log('evt'+evt);
            evt.setParams({
                componentDef: "markup://c:C_TalentMergeComparison",
                componentAttributes: {
                    masterID: component.get("v.masterID"),
            		duplicateID: component.get("v.duplicateID")
                }
            });
            evt.fire();
        }    
    },
    "talentMerger" : function(component, event, helper) {
        
        component.set("v.Spinner", true);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show'); 
        
        var masterID = component.get("v.masterID");
        var duplicateID = component.get("v.duplicateID");
        var Duplicate = component.find("Duplicate");
        var message = "";
        var messageType = "Error";
        var title = "Error";
        if(!masterID.startsWith('C-'))
            message = "Please enter valid Talent Id for Master."
        if(!duplicateID.startsWith('C-'))
            message = "Please enter valid Talent Id for Duplicate."
        if(!masterID.startsWith('C-') && !duplicateID.startsWith('C-'))
            message = "Please enter valid Talent Id for Master and Duplicate."
            
        if(masterID === "")
            message = "Master Candidate field can not be blank."
        if(duplicateID === "")
            message = "Duplicate Candidate field can not be blank."
        if(masterID === "" && duplicateID === "")
            message = "Master Candidate and Duplicate Candidate fields can not be blank."    
        if(masterID === duplicateID && duplicateID !== "")
            message = "Master Candidate and Duplicate Candidate can not be the same."
        
            
            
        if(message !== ""){
            helper.showError(component, message, messageType, title);
            
            //alert(message); 
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');
            component.set("v.Spinner", false);
        }
        else{    
        	var action = component.get("c.talentMerge");
            action.setParams({ masterRecordId : masterID, duplicateRecordId : duplicateID});
			action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
            	if(response.getReturnValue() != 'Merge is Successful.'){
                        // validation or precheck rule failure
                	message = response.getReturnValue();
                    messageType = "Error";
                    title = "Error";
                    helper.showError(component, message, messageType, title);
                }
                else{
                    //Successful merge with sucessfull precheck rule
                    message = response.getReturnValue();
                    messageType = "success";
                    title = "Success";
                    helper.showError(component, message, messageType, title);
                }
                $A.util.removeClass(spinner, 'slds-show');
                $A.util.addClass(spinner, 'slds-hide');
                component.set("v.Spinner", false);
                var comp = event.getSource();
        		$A.util.removeClass(comp, "error");
                   
			}
            else if (state === "INCOMPLETE") {
                message = "Something went wrong. Merge is incomplete!";
                messageType = "Error";
                title = "Error";
                helper.showError(component, message, messageType, title);
            	$A.util.removeClass(spinner, 'slds-show');
                $A.util.addClass(spinner, 'slds-hide');
                component.set("v.Spinner", false);
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                        message = response.getReturnValue();
                        messageType = "Error";
                        title = "Error";
                        helper.showError(component, message, messageType, title);
                    	$A.util.removeClass(spinner, 'slds-show');
                        $A.util.addClass(spinner, 'slds-hide');
                    }
                }else{
                	message = "Unknown Error.";
                    messageType = "Error";
                    title = "Error";
                    helper.showError(component, message, messageType, title);
                    $A.util.removeClass(spinner, 'slds-show');
                    $A.util.addClass(spinner, 'slds-hide');
                }
            } });
        
        $A.enqueueAction(action);
        }    
        helper.validate(component, event, helper, masterID, duplicateID);
    },
    
    handleError:function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.addClass(comp, "error");   
    },

    handleClearError:function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.removeClass(comp, "error");   
    }
})