({	
    showError : function(component, message, messageType, theTitle){
        var title = theTitle;
        var errorMessage = message;
        var theType = messageType;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: theType
        });
        toastEvent.fire();
    },
    showToast : function(component, message, messageType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": message,
            type: messageType
        });
        toastEvent.fire();
    },
    validate : function(component, event, helper, masterID, duplicateID) {

        var masterField = component.find("Master");
        var duplicateField = component.find("Duplicate");
        masterField.set("v.errors", null);
        duplicateField.set("v.errors", null);
        if(!masterID.startsWith('C-'))
            masterField.set("v.errors", [{message: "Please enter valid Talent Id for Master."}]);
        if(!duplicateID.startsWith('C-'))
            duplicateField.set("v.errors", [{message: "Please enter valid Talent Id for Duplicate."}]);
        if(!masterID.startsWith('C-') && !duplicateID.startsWith('C-')){
            masterField.set("v.errors", [{message: "Please enter valid Talent Id for Master."}]);
            duplicateField.set("v.errors", [{message: "Please enter valid Talent Id for Duplicate."}]);
        }    
            
        if(masterID === "")
            masterField.set("v.errors", [{message: "Master Candidate field can not be blank."}]); 
        if(duplicateID === "")
            duplicateField.set("v.errors", [{message: "Duplicate Candidate field can not be blank."}]);
        if(masterID === "" && duplicateID === ""){
            masterField.set("v.errors", [{message: "Master Candidate field can not be blank."}]);
            duplicateField.set("v.errors", [{message: "Duplicate Candidate field can not be blank."}]);
        }    
        if(masterID === duplicateID && duplicateID !== "")
            duplicateField.set("v.errors", [{message: "Master Candidate and Duplicate Candidate can not be Same."}]);
            
        
    }
})