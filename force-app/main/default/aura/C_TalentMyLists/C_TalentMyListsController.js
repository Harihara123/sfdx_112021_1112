({
    doInitialization: function(component, event, helper) {
        helper.loadMyListsData(component);
    },

    filterContacts: function(cmp, event, helper) {
        helper.filterContactLists(cmp);
    },
    selectAll: function(component, event, helper) {
        //get the header checkbox value  
        var selectedHeaderCheck = event.getSource().get("v.value");
        // get all checkbox on table with "boxPack" aura id (all iterate value have same Id)
        // return the List of all checkboxs element 
        var getAllId = component.find("boxPack");
        // If the local ID is unique[in single record case], find() returns the component. not array   
        if (!Array.isArray(getAllId)) {
            if (selectedHeaderCheck == true) {
                component.find("boxPack").set("v.value", true);
                component.set("v.selectedCount", 1);
            } else {
                component.find("boxPack").set("v.value", false);
                component.set("v.selectedCount", 0);
            }
        } else {
            // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
            // and set the all selected checkbox length in selectedCount attribute.
            // if value is false then make all checkboxes false in else part with play for loop 
            // and select count as 0 
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", true);
                    component.set("v.selectedCount", getAllId.length);
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", false);
                    component.set("v.selectedCount", 0);
                }
            }
        }

    },
    // For count the selected checkboxes. 
    checkboxSelect: function(component, event, helper) {
        // get the selected checkbox value  
        var selectedRec = event.getSource().get("v.value");
        var selectedId = event.getSource().get("v.text");
        //console.log('selectedNoteRec TalentMyListController----------------' + selectedId);
        // get the selectedCount attrbute value(default is 0) for add/less numbers. 
        var getSelectedNumber = component.get("v.selectedCount");
        // check, if selected checkbox value is true then increment getSelectedNumber with 1 
        // else Decrement the getSelectedNumber with 1     
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
        // set the actual value on selectedCount attribute to show on header part. 
        component.set("v.selectedCount", getSelectedNumber);
    },

    //This Click on Delete Button will set Confirmation Modal to True so that it will pop-up .
    confirmationModalsetTrue: function(component, event, helper) {
        component.set("v.isOpen", true);
    },

    //For Delete selected records 
    deleteSelected: function(component, event, helper) {
        // create var for store record id's for selected checkboxes  
        var delId = [];
        // get all checkboxes 
        var getAllId = component.find("boxPack");
        // If the local ID is unique[in single record case], find() returns the component. not array
        if (!Array.isArray(getAllId)) {
            if (getAllId.get("v.value") == true) {
                delId.push(getAllId.get("v.text"));
                //console.log('getAllId.get("v.text")----------------' + getAllId.get("v.text"));
            }
        } else {
            // play a for loop and check every checkbox values 
            // if value is checked(true) then add those Id (store in Text attribute on checkbox) in delId var.
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    delId.push(getAllId[i].get("v.text"));
                }
            }
        }

        // call the helper function and pass all selected record id's.    
        helper.deleteSelectedHelper(component, event, delId);
        component.set("v.isOpen", false);
    },

    closeModal: function(component, event, helper) {
        component.set("v.isOpen", false);
        component.set("v.isModalOpen", false);
        component.set("v.showNotesModal", false);
    },

    //test
    openNotesModal: function(component) {
        component.set("v.showNotesModal", !component.get("v.showNotesModal"));
        //component.set("v.talentInModal", component.get("v.talent"));
    },

    cancelAndClose: function(component, event, helper) {
        component.set("v.showNotesModal", false);
        component.set("v.isModalOpen", false);
    },

    noteEntered: function(component, event, helper) {
        component.set("v.noteText", event.target.value);
    },

    deleteNote: function(component, event, helper) {
        let c = confirm(`Deleting the note ${component.get("v.selectedListName")} will delete all content and the note. Are you sure?`);
        if(c) {
            component.set("v.isOpen", false);
            component.set("v.isModalOpen", false);
            component.set("v.showNotesModal", false);
            var deleteId = component.get("v.tagId");
            component.set("v.deleteRecordsIds", deleteId);
            helper.deleteNote(component, event, helper);
        }
    },

    openModal: function(component, event, helper) {

        var selectedNotes = event.currentTarget.dataset.value;
		var selectedlistname = event.currentTarget.dataset.listname;
        //console.log('selectedlistname---on open modal --------' + selectedlistname);
		component.set("v.selectedListName",selectedlistname);
        component.set("v.noteText", selectedNotes);
        component.set("v.isModalOpen", true);
        // component.set("v.showNotesModal", !component.get("v.showNotesModal"));
        // console.log('targetId --------' + targetId);
        let targetId = event.currentTarget.dataset.targetId;
        component.set("v.tagId", targetId);
        if (selectedNotes === undefined) {
            component.set("v.hideDelete", false);
        }
        if (selectedNotes != undefined && selectedNotes != '') {
            component.set("v.hideDelete", true);
        }

        component.set("v.showNotesModal", !component.get("v.showNotesModal"));
        component.set("v.talentInModal", component.get("v.talent"));
    },

    SaveNClose: function(component, event, helper) {
        //console.log('noteTextupdated---on open modal --------' + component.get("v.noteText"));
        component.set("v.isOpen", false);
        component.set("v.isModalOpen", false);
        component.set("v.showNotesModal", false);
        helper.saveUpdatedNotes(component, event, helper);
    },
    previewFile: function(component, event, helper) {
        //console.log('previewFile');		
		const doc =event.getSource().get("v.value");
		//console.log('doc---'+doc);
        const navService = component.find('navService');        
        const docMap = {
            terms: $A.get("$Label.c.ATS_RESTRICTED_TERMS_DOC_ID"),
            eeo: $A.get("$Label.c.ATS_EEO_POLICY_DOC_ID")
        }
        event.preventDefault();
        if (doc && docMap[doc]) {
            navService.navigate({
                type: 'standard__namedPage',
                attributes: {
                    pageName: 'filePreview'
                },
                state: {
                    recordIds: docMap[doc],
                    selectedRecordId: docMap[doc]
                }
            });
        }
    },
    
})