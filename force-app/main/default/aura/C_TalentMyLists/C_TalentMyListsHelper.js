({
    loadMyListsData: function(component) {
        //console.log('Contact recordId in TalentMyListCMP----------------' + component.get('v.recordId'));
        var action = component.get('c.getTalentInMyLists');
        action.setParams({
            "contactId": component.get('v.recordId')
        });

        action.setCallback(this, $A.getCallback(function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var mylists = response.getReturnValue();
                var MyFirsName = JSON.stringify(mylists);
                console.log('xxxxxxxxxx' + JSON.stringify(mylists));
                component.set('v.myListsdata', mylists);
                component.set('v.currentlists', mylists);
                if (MyFirsName.includes('Full_Name__c')) {

                    component.set('v.showNameRtrnListZero', true);
                } else {
                    component.set('v.showNameRtrnListZero', false);
                }
                var loadingText = component.find('loading');
                $A.util.addClass(loadingText, 'show-loading');
                $A.util.removeClass(loadingText, 'hide-loading');
                // this.getReferenceData(component);
                if (mylists && MyFirsName.includes('Full_Name__c')) {
                    component.set('v.totalMyLists', 0);
                } else {
                    component.set('v.totalMyLists', mylists.length);
                }
                //console.log('Total Number of lists on landing page:' + mylists.length );
            } else if (state === "ERROR") {
                var errors = response.getError();
                //console.error(errors);
            }

        }));
        $A.enqueueAction(action);
    },

    filterContactLists: function(cmp) {
        var listscnt = cmp.get('v.totalMyLists')
        var lists = cmp.get('v.myListsdata');
        var filterText = cmp.get('v.filterText');
        if (listscnt) {
            if (filterText) {
                filterText = filterText.toUpperCase();
            } else {
                cmp.set('v.currentlists', lists);
                return;
            }

            var filteredList = lists.filter(function(myList) {
                if (myList && myList.Tag_Definition__r.Tag_Name__c.toUpperCase().includes(filterText)) {
                    return myList;
                }
            });

            cmp.set('v.currentlists', filteredList);
        } else {
           // console.log('No Data in TalentList to Search');
        }
    },
   
    deleteSelectedHelper: function(component, event, deleteRecordsIds) {
        //call apex class method
        var action = component.get('c.deleteContactTagRecords');
        // pass the all selected record's Id's to apex method 
        action.setParams({
            "lstRecordId": deleteRecordsIds
        });
        action.setCallback(this, function(response) {

            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
   
                // call the onLoad function for refresh the List view    
                this.loadMyListsData(component, event);
                component.set('v.selectedCount', '0');
                //This event is fired to clear Lists data on Insights card when we delete them from My Lists table.
                var appEvent = $A.get("e.c:E_RefreshChild");
                appEvent.fire();

            }
        });
        $A.enqueueAction(action);

    },
    
    saveUpdatedNotes: function(component,event,helper) {
        //console.log('Updated Note value----------------' + component.get('v.noteText'));
		//console.log('tagId----saveUpdatedNotes---'+component.get('v.tagId'));
        var action = component.get('c.setNotesInContactTag');
        action.setParams({
            "noteText": component.get('v.noteText'),
            "tagId": component.get('v.tagId')
        });

        action.setCallback(this, $A.getCallback(function(response) {
			//$A.get('e.force:refreshView').fire();
            var state = response.getState();
            if (state === "SUCCESS") {
				
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }

        }));
        $A.enqueueAction(action);
		 helper.loadMyListsData(component);
		 component.set("v.hideDelete",false);
    },
	deleteNote: function(component, event, helper) {
		//console.log('deleteRecordsIds Note value----------------' +component.get("v.deleteRecordsIds"));		
        var action = component.get('c.deleteContactTagNotes');
        action.setParams({
            "deleteRecordsIds": component.get('v.deleteRecordsIds')
        });

        action.setCallback(this, $A.getCallback(function(response) {
			//$A.get('e.force:refreshView').fire();
            var state = response.getState();
            if (state === "SUCCESS") {
				//$A.get('e.force:refreshView').fire();
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }

        }));
        $A.enqueueAction(action);
		 helper.loadMyListsData(component);
    },

});