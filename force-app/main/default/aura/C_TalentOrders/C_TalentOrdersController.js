({  doInit: function(cmp, event, helper) {
       helper.getResultAndCount(cmp);
    },
	setupDisplayFields : function(cmp, event, helper) {
        // console.log(component.get("v.records"));
        var records = cmp.get("v.records");
		if(!$A.util.isUndefinedOrNull(records)){
			for (var i = 0; i < records.length; i++) {
			 if (typeof records[i].Opportunity !== "undefined" && records[i].Opportunity !== "") {
                records[i].opptyCityState = helper.getAddressString([records[i].Opportunity.Account_City__c, records[i].Opportunity.Account_State__c]);
			 } else if (typeof records[i].ATS_Job__c !== "undefined" && records[i].ATS_Job__c !== "") {
                records[i].atsJobCityState = helper.getAddressString([records[i].ATS_Job__r.Account_City__c, records[i].ATS_Job__r.Account_State__c]);
				}
			}
		}
        
    },

    linkToViewMore : function(cmp, event, helper) {
        var tabHeader = document.getElementsByClassName("tabHeader");
        if(tabHeader !== undefined && tabHeader !== null){
            for(var i=0; i <tabHeader.length; i++){
                if(tabHeader[i].text === "Submittals"){
                    tabHeader[i].click(); 
                }
            }
        }
    },

    linkToATSJob: function(cmp, event, helper) {
        var recordID = event.currentTarget.dataset.recordid;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__JobSummary?jobId=" + recordID
        });
        urlEvent.fire();
    },
    
     //Neel summer 18 URL change-> This method not in use. Method calling code commented in cmp.
    linkToOppty: function(cmp, event, helper) {
       var recordID = event.currentTarget.dataset.recordid;
        //window.open('/one/one.app#/sObject/' + recordID, '_blank');
        var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
        var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
        var dURL;
        
        if(isFlagOn === "true") {
              dURL = ligtningNewURL+"/r/Opportunity/" + recordID +"/view";
        }
          else {
              dURL = ligtningNewURL+"/sObject/" + recordID +"/view";
        }
        
        window.open(dURL, '_blank');
    },
    loadData: function(cmp, event, helper) {
        cmp.set("v.loading",true);
        cmp.set("v.records",[]);
        cmp.set("v.recordCount",0);
		helper.getResultAndCount(cmp); 
    }
    
    ,updateLoading : function(cmp,event,helper){
        helper.updateLoading(cmp);
    }
    ,reloadData : function(cmp,event,helper) {
        var rd = cmp.get("v.reloadData");
        if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);
			helper.getResultAndCount(cmp); 
			cmp.set("v.reloadData", false);
        }

        
    },
    changeDispositinMatrix:function(component, event, helper){
     helper.makeDispositionPicklistValues(component);
    },

    clickPencilEdit : function(component, event, helper) {
        var arr = event.getSource().get("v.name").split("~~");
        helper.initStatusUpdate(component, arr);
    },

    dblClickStatus : function(component, event, helper) {
        var arr = event.srcElement.name.split("~~");
        helper.initStatusUpdate(component, arr);
    },

    saveStatusChange : function(component, event, helper) {
        helper.saveStatusUpdate(component);
        //helper.showError(component, "Req has OFCCP flag and requires an Application. No Application is found.  Talent cannot be submitted.", "Error", "Error");
    },
	saveMassUpdate : function(component, event, helper) {
        helper.saveMassUpdate(component);
    },
	saveMassDisposition : function(component, event, helper) {
        helper.saveMassDisposition(component);
    },
	massSendEmail : function(component, event, helper) {
        helper.massSendEmail(component);
    },
	massAddToCallSheet : function(component, event, helper) {
        helper.massAddToCallSheet(component);
    },
    cancelStatusChange: function(component, event, helper) {
       component.set("v.editId", '');
    },

	selectStatus : function(component, event, helper) {
		helper.selectStatus(component);
	},

	handleChangeCancel : function(component, event, helper) {
		helper.resetUpdateAttributes(component);
		component.set("v.reloadData", true);
	},

	handleChangeSave : function(component, event, helper) {
	   if(event.getParam("contactId") === component.get("v.recordId") ){ 
	     helper.resetUpdateAttributes(component);
		 component.set("v.reloadSubmittals", true);
	   }
		
	},

	onReloadSubmittals : function(component, event, helper) {
		if (component.get("v.reloadSubmittals") === true) {
			component.set("v.reloadData", true);
		}
	},
  onCheck : function(component, event, helper) {
		
		var val = event.getSource().get("v.value");
		var subid = event.getSource().get("v.text");
		var masslist = component.get("v.massList");
		var index  = masslist.indexOf(subid);
		if(val){
			if(index < 0 ){
				masslist.push(subid);
			}
		 }else{
			component.find("allcheckbox").set("v.value",false)
		    masslist.splice(index,1);
		}
		component.set("v.massList",masslist);

	},
	onCheckAll : function(component, event, helper) {
		
		var val = event.getSource().get("v.value");
		var masslist = component.get("v.massList"); 
		var index ;
		var records = component.get("v.records");
		 
		if(val){
			for (var i = 0; i < records.length; i++) {
					 if(masslist.indexOf(records[i].jpOrder.Id) < 0){
						masslist.push(records[i].jpOrder.Id);
						records[i].selected = true;
					 }
				}
		}else{
		   masslist = []; 
		   for (var i = 0; i < records.length; i++) {
					records[i].selected = false;
				} 
		}
		component.set("v.massList",masslist);
		component.set("v.records",records);

	},
	bringUpResumeModal: function(component, evt, helper) {
        helper.showTalentResume(component,evt, helper);
        
    },
    getDispReason: function(component, event, helper) {
        helper.getDispReason(component);
    },
    changeDisPositionsLoaded:function(component, event, helper){
        helper.getDispReason(component);
    },
})