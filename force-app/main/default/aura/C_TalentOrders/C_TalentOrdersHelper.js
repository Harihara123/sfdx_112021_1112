({
	
    getResultAndCount : function(cmp){
		cmp.set("v.massList",[]);
		cmp.set("v.showImage",false);
	    var compType = cmp.get("v.componentType");
		var isApplicationCard = compType.includes("application") ? true : false ;
		var listView = cmp.get("v.viewList");
				
		if(isApplicationCard){
			cmp.set("v.viewListIndex", 1); 
		}

        if(listView){
		   
		   if(compType === 'jobposting'){
			  var recordID = cmp.get("v.recordId");
			  if(recordID.startsWith("006")){
				cmp.set("v.recordIdObjValue",true);
			  }
			  var apexFunction = "getApplicantsFromJobPosting";
			  var actionParams = {'recordId':recordID};
				var bdata = cmp.find("bdhelper");

				bdata.callServer(cmp,'ATS','TalentOrderFunctions',apexFunction,function(response){
					cmp.set("v.records", response); 
					if($A.util.isUndefinedOrNull(response) || response.length === 0 ){
					   cmp.set("v.showImage",true);/*show the empty state image when there are no records - akshay (went with separate boolean instead of list.length property as whenever the list gets refreshed , the list lenth is zero until its gets repopulated and causes flickering*/
					}
					cmp.set("v.loadingData",false);
					cmp.set("v.loadingCount",false);
					cmp.set("v.loading",false);
                    cmp.set("v.reloadSubmittals", false); 
				},actionParams,false);
                // Loading the current User - S-104968
                bdata.callServer(cmp,'','',"getCurrentUserWithOwnership",function(response){
                    if(response && response.usr){
                        cmp.set('v.runningUser',response.usr);
						cmp.set('v.userCPCompany', response.userCPCompany);//Added by siva for story S-107087
                    }
                },null,true);

		   }else {
					var indexNo = cmp.get("v.viewListIndex");
					var recordID = cmp.get("v.recordId");
					var soql = listView[indexNo].soql;
					var countSoql = listView[indexNo].countSoql;

					soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
					countSoql = countSoql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
            
					var apexFunction = "c.getRowAndCount";
					var params = {
						"soql": soql,
						"countSoql" : countSoql,
						"maxRows": cmp.get("v.maxRows")
					};

					cmp.set("v.loadingData",true);
					cmp.set("v.loadingCount",true);
					var actionParams = {'recordId':recordID};
					if(params){
						if(params.length != 0){
							Object.assign(actionParams,params);
						}
					}
			
			var bdata = cmp.find("bdhelper");
            bdata.callServer(cmp,'','',apexFunction,function(response){
				cmp.set("v.records", response.records);
				cmp.set("v.recordCount", response.count);
                cmp.set("v.loadingData",false);
				cmp.set("v.loadingCount",false);
                        
                if(listView[indexNo].additional){
                    cmp.set("v.additional",listView[indexNo].additional);
				 }
				cmp.set("v.reloadSubmittals", false);            
            },actionParams,false);
		   }
        }
    },

    updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },

    refreshList : function(cmp) {
        cmp.set("v.reloadSubmittals", true);
    },

    getAddressString : function(fieldList) {
        var address = '';
        if (fieldList && fieldList.length > 0) {
            address = fieldList[0];
            for (var i = 1; i < fieldList.length; i++) {
                address = (fieldList[i] && fieldList[i].length > 0 ? (address && address.length > 0 ? (address + ", " + fieldList[i]) : fieldList[i]) : address); 
            }
        }
        return address;
    },

    initStatusUpdate : function(component, arrData) {
		
        component.set("v.editIndex", arrData[0]);
        component.set("v.editId", arrData[1]);
        component.set("v.editEventId", arrData[5]);
        component.set("v.submittal", component.get("v.records")[arrData[0]].jpOrder);

		var sHelp = component.find("submittalHelper");
        sHelp.getWorkflowStatuses(function(response) {
                component.set("v.statusList", response);
                component.set("v.newStatus", arrData[2]);
            }, {
                "opco" : arrData[3],
                "status" : arrData[2],
                "isAtsJob" : arrData[4] === "true"
            }
        );
        // Get Disposition all statuses
        if(component.get("v.simpleRecord.OFCCP_Flag__c")){
            var appIds = this.getMassEditDetails(component);
            var sHelp = component.find("submittalHelper");
            sHelp.getAllDispositionStatuses( function(response) {                
                component.set("v.dispoMatrix",response);

            },appIds);
        }
    },

    makeDispositionPicklistValues : function(component) {
      var matrix  = component.get("v.dispoMatrix"); 
      var codeOptionsMap = {};
      var codeOptions = [];
      for(var i in matrix){
          codeOptionsMap[matrix[i].Disposition_Code_Label__c] = matrix[i].Disposition_Code_Value__c;
      }
      var mapKeys = Object.keys(codeOptionsMap);
      for(var j in mapKeys ){
          codeOptions.push({label : mapKeys[j] , value : codeOptionsMap[mapKeys[j]]  });
      }
      component.set("v.codeOptions",codeOptions);
      var self = this;

      //By Default also load the disp reason.
      var records = component.get("v.records");
        if(!$A.util.isUndefinedOrNull(records)){
            var editIndex = component.get("v.editIndex");
            var rec = records[editIndex];
            if(!$A.util.isUndefinedOrNull(rec.event) && 
               !$A.util.isUndefinedOrNull(rec.event.Disposition_Reason__c)){
                  for(var coption in codeOptions){
                    if(codeOptions[coption].label == rec.event.Disposition_Code__c){
                        console.log(rec.event.Disposition_Code__c);
                        component.set("v.newDispCode",codeOptions[coption].value);
                        break;
                    }
                  }
            }
        }
      self.getDispReason(component);
    },

    saveStatusUpdate : function(component) {
		var spinner = component.find('spinner');
		$A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, 'slds-show');

        var sHelp = component.find("submittalHelper");
		var self = this;
        sHelp.updateStatus(function(response) {
                self.refreshList(component);
				self.resetUpdateAttributes(component);
            	//S-78898 - To Refresh Activity Timeline - Karthik
				var reloadEvt = $A.get("e.c:E_TalentActivityReload");
				reloadEvt.fire();
            }, component.get("v.editId"), component.get("v.newStatus"),
               component.get("v.editEventId"),component.get("v.newDispCode"),
               component.get("v.newDispReason")
        );
		
		$A.util.removeClass(spinner, 'slds-show');
		$A.util.addClass(spinner, 'slds-hide');
    },

	saveMassUpdate : function(component) {
	   if(this.validateMassUpdate(component)){
			var sHelp = component.find("submittalHelper");
		sHelp.massUpdateStatus( function(response) {
            },component.get("v.massList"));
	   }
		
		
    },
	saveMassDisposition : function(component) { 
         if(this.validateMassUpdate(component)){
			var appIds = this.getMassEditDetails(component);
            //this.getMassEditEventIds(component);
			var sHelp = component.find("submittalHelper");
		sHelp.massUpdateDisposition( function(response) {
            },appIds);
	   }
    },
	massSendEmail : function(component) {
        var appEdits = this.getMassEditDetails(component);
	   if(this.validateMassUpdate(component)){
			var sHelp = component.find("submittalHelper");
		sHelp.massSendEmail( function(response) {
            },appEdits);
	   }
		
		
    },
    // This method constructs all selected applications in a format.
    getMassEditDetails:function(cmp){
        var appEdits = [];
        var records = cmp.get("v.records");
        var massList = cmp.get("v.massList");
        for (var i = 0; i < records.length; i++) {
            if(massList.includes(records[i].jpOrder.Id)){
                appEdits.push(records[i]);
            }
        }
        return appEdits;
    },
	massAddToCallSheet : function(component) {
        var appEdits = this.getMassContacts(component);
	   if(this.validateMassUpdate(component)){
			var sHelp = component.find("submittalHelper");
		sHelp.massAddToCallSheet( component.get("v.simpleRecord.Name"),appEdits);
	   }
		
		
    },
    // This method constructs all selected applications in a format.
  
	getMassContacts:function(cmp){
        var appEdits = [];
        var records = cmp.get("v.records");
        var massList = cmp.get("v.massList");
        for (var i = 0; i < records.length; i++) {
            if(massList.includes(records[i].jpOrder.Id)){
                appEdits.push(records[i].jpOrder.ShipToContact);
            }
        }
        return appEdits;
    },

	getMassEditEventIds:function(cmp){
        var appEdits = [];
        var records = cmp.get("v.records");
        var massList = cmp.get("v.massList");
        for (var i = 0; i < records.length; i++) {
            if(massList.includes(records[i].jpOrder.Id)){
                appEdits.push(records[i].event.Id);
            }
        }
        return appEdits;
    },

	validateMassUpdate: function(component) {
	var valid = true;


	return valid;
	},

	resetUpdateAttributes : function(component) {
		component.set("v.editIndex", -1);
		component.set("v.editId", "");
        component.set("v.editEventId", "");
		component.set("v.statusList", []);
		component.set("v.newStatus", "");
        component.set("v.newDispCode", "");
        component.set("v.newDispReason", "");
		component.set("v.massList",[]);
	},

	selectStatus : function(component) {
		var sHelp = component.find("submittalHelper");
		var self = this;
        let type = component.get("v.newStatus")
        if (['Interviewing','Add New Interview','Edit Interview'].includes(type)) {
            var modalBody;
            $A.createComponent("c:C_SubmittalInterviewModal",
                {
                    submittal: component.getReference('v.submittal'),
                    modalPromise: component.getReference('v.modalPromise'),
                    editMode: type === 'Edit Interview',
                    targetStatus:type
                },
                function(content, status) {
                    if (status === "SUCCESS") {
                        modalBody = content;
                        let modalPromise = component.find('overlayLib').showCustomModal({
                            header: $A.get("$Label.c.ATS_INTERVIEW_MANAGEMENT"), //$Label.c.ATS_INTERVIEW_MANAGEMENT
                            body: modalBody,
                            showCloseButton: true,
                            cssClass: "slds-modal_medium",
                            closeCallback: function() {
                                self.resetUpdateAttributes(component);
                                component.set("v.reloadData", true);
                            }
                        })
                        component.set('v.modalPromise', modalPromise);
                    }
                });
        } else {
            sHelp.selectStatus(function(response) {
                }, component.get("v.editId"), component.get("v.newStatus"), component.get("v.submittal")
            );
        }
	},
    
    showError : function(component, message, messageType, theTitle){
        var title = theTitle;
        var errorMessage = message;
        var theType = messageType;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: theType
        });
        toastEvent.fire();
    },
	 showTalentResume: function(cmp, evt) {
        
		var arr = evt.getSource().get('v.name').split("~~");
        var modalEvt = $A.get("e.c:E_SearchResumeModal");
       
        modalEvt.setParams({
            "attachmentId" : arr[0],
            "resumeName" : arr[1],
            "destinationId" : "SubmittalHelper"
        });

        modalEvt.fire();

    },
    getDispReason : function(component) {
        var dsp = component.get("v.newDispCode");
        //component.find("InputSelectDynamic").get("v.value");
        //component.set("v.selectedValue",dsp);
        var reasonOptions = [];
        var matrix  = component.get("v.dispoMatrix"); 
        for(var i in matrix){
          if( matrix[i].Disposition_Code_Value__c === dsp &&  !$A.util.isUndefinedOrNull(matrix[i].Disposition_Reason_Label__c)){
             reasonOptions.push({label : matrix[i].Disposition_Reason_Label__c , value : matrix[i].Disposition_Reason_Value__c  });
          }
         }
         component.set("v.reasonOptions",reasonOptions);
    }
})