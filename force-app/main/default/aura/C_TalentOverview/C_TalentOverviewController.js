({
    doInit : function(cmp,event,helper){
		//alert("In Here"); -_-
        var conRecordId = cmp.get("v.recordId");
        var params = {"conId" : conRecordId}; 
        //Rajeesh check notes parser access.
		helper.checkAccess(cmp);
        helper.callServer(cmp, '', '', 'getTalentOverviewModel', function(response) {
            var state = response.record;
            cmp.set("v.runningUser", response.uoModal.usr);
			cmp.set("v.usrOwnership", response.uoModal.userOwnership); 
            cmp.set("v.runningUserCategory", response.uoModal.userCategory);
            cmp.set("v.talent",state);
			cmp.set("v.talentId",state.Id);
            if(response.talentBaseFields != null && response.talentBaseFields != '') {
                cmp.set("v.contactBaseFields",response.talentBaseFields);
                cmp.set("v.jobTitle",response.talentBaseFields.Title);
                }
                    
            cmp.set("v.currentSkills", helper.extractSkills(state.Skills__c));
            cmp.set("v.record", helper.extractPreferenceInternal(state.Talent_Preference_Internal__c));
                
                helper.setPicklistValues(cmp, response.currencyList);
                
                if((cmp.get('v.edit') == false) && ((typeof cmp.find("readdesiredrsalary") !== 'undefined' ) || (typeof cmp.find("readdesiredBonusId") !== 'undefined' ) || (typeof cmp.find("readdesiredAdditionalCompensationId") !== 'undefined' ))){
                   helper.calculateTotalComp(cmp); 
                }else if(cmp.get('v.edit') == true){
                   helper.calculateTotalComp(cmp); 
                }
                
            cmp.set("v.languageList", helper.extractLanguageList(response.languageMap));
                        helper.populateLanguage(cmp);
                        
                // G2 
                var g2Date = "";
                var day = new Date().getDate();
                var month = new Date().getMonth() + 1;
                var year = new Date().getFullYear();
                
                var getDate = day.toString();
                if (getDate.length == 1){ 
                    getDate = "0"+getDate;
                }
                var getMonth = month.toString();
                if (getMonth.length == 1){
                    getMonth = "0"+getMonth;
                }
                
                var todaysDate = year + "-" + getMonth + "-" + getDate;
                
                if(typeof state.G2_Completed_Date__c !== "undefined"){
                    var tempDate = state.G2_Completed_Date__c.split("T");
                    if(tempDate.length > 0){
                        g2Date = tempDate[0];
                        var todayInMilliSeconds = new Date(todaysDate).getTime(); 
                        var g2DateInMilliSeconds =  new Date(state.G2_Completed_Date__c).getTime();
                        cmp.set("v.isG2DateToday", (todayInMilliSeconds === g2DateInMilliSeconds));
                        var insightDate = new Date(g2Date.replace(/-/g, '\/')); 
                        cmp.set("v.g2CompletedDate", state.G2_Completed_Date__c);//g2DateInMilliSeconds
                        if (todayInMilliSeconds === g2DateInMilliSeconds) {
                            cmp.set("v.isG2Checked", true);
                        }
                    }
                }
				/*
					Rajeesh S-108773 - Ability to Restore Backup from 'refreshed' G2 on Inline Edit Card
					Feature enabled based on alias included in label. If label is set to GA, rolled out to everyone.
					Label contains comma seperated userids
				*/
				//console.log('overview cmp name'+cmp.getName());
				cmp.set("v.initOnly",true);
				//End Rajeesh
        },params,false);
		cmp.set("v.g2CommentsId",""); // need to clear to keep it blank when loaded
    },
	 doRefresh : function(cmp,event,helper){
		//Rajeesh fixing issue with client side backup on force refreshview.
		//cloned init and removed a line of code related to set value only on actual component init.
        var conRecordId = cmp.get("v.recordId");
        var params = {"conId" : conRecordId}; 
        //Rajeesh check notes parser access.
		helper.checkAccess(cmp);
        helper.callServer(cmp, '', '', 'getTalentOverviewModel', function(response) {
            var state = response.record;
            cmp.set("v.runningUser", response.uoModal.usr);
			cmp.set("v.usrOwnership", response.uoModal.userOwnership); 
            cmp.set("v.runningUserCategory", response.uoModal.userCategory);
            cmp.set("v.talent",state);
			cmp.set("v.talentId",state.Id);
            if(response.talentBaseFields != null && response.talentBaseFields != '') {
                cmp.set("v.contactBaseFields",response.talentBaseFields);
                cmp.set("v.jobTitle",response.talentBaseFields.Title);
                }
                    
            cmp.set("v.currentSkills", helper.extractSkills(state.Skills__c));
            cmp.set("v.record", helper.extractPreferenceInternal(state.Talent_Preference_Internal__c));
                
                helper.setPicklistValues(cmp, response.currencyList);
                
                if((cmp.get('v.edit') == false) && ((typeof cmp.find("readdesiredrsalary") !== 'undefined' ) || (typeof cmp.find("readdesiredBonusId") !== 'undefined' ) || (typeof cmp.find("readdesiredAdditionalCompensationId") !== 'undefined' ))){
                   helper.calculateTotalComp(cmp); 
                }else if(cmp.get('v.edit') == true){
                   helper.calculateTotalComp(cmp); 
                }
                
            cmp.set("v.languageList", helper.extractLanguageList(response.languageMap));
                        helper.populateLanguage(cmp);
                        
                // G2 
                var g2Date = "";
                var day = new Date().getDate();
                var month = new Date().getMonth() + 1;
                var year = new Date().getFullYear();
                
                var getDate = day.toString();
                if (getDate.length == 1){ 
                    getDate = "0"+getDate;
                }
                var getMonth = month.toString();
                if (getMonth.length == 1){
                    getMonth = "0"+getMonth;
                }
                
                var todaysDate = year + "-" + getMonth + "-" + getDate;
                
                if(typeof state.G2_Completed_Date__c !== "undefined"){
                    var tempDate = state.G2_Completed_Date__c.split("T");
                    if(tempDate.length > 0){
                        g2Date = tempDate[0];
                        var todayInMilliSeconds = new Date(todaysDate).getTime(); 
                        var g2DateInMilliSeconds =  new Date(state.G2_Completed_Date__c).getTime();
                        cmp.set("v.isG2DateToday", (todayInMilliSeconds === g2DateInMilliSeconds));
                        var insightDate = new Date(g2Date.replace(/-/g, '\/')); 
                        cmp.set("v.g2CompletedDate", state.G2_Completed_Date__c);//g2DateInMilliSeconds
                        if (todayInMilliSeconds === g2DateInMilliSeconds) {
                            cmp.set("v.isG2Checked", true);
                        }
                    }
                }
				/*
					Rajeesh S-108773 - Ability to Restore Backup from 'refreshed' G2 on Inline Edit Card
					Feature enabled based on alias included in label. If label is set to GA, rolled out to everyone.
					Label contains comma seperated userids
				*/
				//console.log('overview cmp name'+cmp.getName());
				//cmp.set("v.initOnly",true);
				//End Rajeesh
        },params,false);
    },
    launchNotesExtractor: function(cmp, event, helper) {
		//Rajeesh changing component name to C_NotesParser
		$A.createComponent(	"c:C_NotesParser",{"recId" : cmp.get("v.recordId"), "talentId" : cmp.get("v.talentId") },function(newButton, status, errorMessage){
			//Add the new button to the body array
			if (status === "SUCCESS") {
				var body = cmp.get("v.body");
				body.push(newButton);	
				cmp.set("v.body", body);
			}
			else if (status === "INCOMPLETE") {
				console.log("No response from server or client is offline.")
				// Show offline error
			}
			else if (status === "ERROR") {
				console.log("Error: " + errorMessage);
 
			// Show error message
			}
			else{
			} 
			});
	},    
    editTalentSummary :function(cmp, event, helper){
        var recordID = cmp.get("v.talentId");
        var contID = cmp.get("v.recordId");
        //var urlEvent = $A.get("e.c:E_TalentSummEdit");
		var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        urlEvent.setParams({
            "recordId": recordID,
            "recId": contID,
            "source": "Summary"
        });
        urlEvent.fire(); 
    },
    /*S-104786 - Neel -> Unused Method
	editCandidateSummary :function(cmp, event, helper) {
        helper.editCandidateSummary(cmp, event);
    },*/
    /*S-104786 - Neel -> Unused Method
	viewCandidateSummary :function(cmp, event, helper) {
        helper.viewCandidateSummary(cmp, event);
    },*/
    enableInlineEdit :function(cmp, event, helper) {
        cmp.set("v.edit",true);  
        $A.get('e.force:refreshView').fire();

		//Rajeesh start writing client side backup
		cmp.set('v.eraseBackup',false);
		cmp.set("v.initOnly",false);
    },
    disableInlineEdit :function(cmp, event, helper) {
        $A.get('e.force:refreshView').fire();
        cmp.set("v.edit",false);

		//Rajeesh stop writing client side backup
		cmp.set('v.eraseBackup',true);
		var dataBackup = cmp.find("clientSideBackup"); 
		dataBackup.clearDataBackup();
    },
    /*S-104786 - Neel -> Unused Method
	rateTypeChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
    S-104786 - Neel -> Unused Method
    rateChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },*/
    
    bonusPctChange : function (cmp, event, helper) {
        helper.calculateBonusAmountOnSalary(cmp);
        helper.calculateTotalComp(cmp);
    },
    
    bonusChange : function (cmp, event, helper) {
        helper.calculateBonusPctOnSalary(cmp);
        helper.calculateTotalComp(cmp);
    },
    
    AddlCompChange : function (cmp, event, helper) {
        helper.calculateTotalComp(cmp);
    },
    
    editCandidateSummary :function(cmp, event, helper) {
        
        helper.editCandidateSummary(cmp);
    },
    
    addNewLocation :function(cmp, event, helper) {//added for S-41845 akshay konijeti 10/31 start
        var locs = [];
        if(typeof cmp.get("v.record.geographicPrefs.desiredLocation") !== "undefined"){
            locs = cmp.get("v.record.geographicPrefs.desiredLocation");  
        }
        
        locs.push({country:'',state:'',city:''});
        cmp.set("v.record.geographicPrefs.desiredLocation",locs);
    },
    saveCandidateSummary :function(cmp, event, helper) {
        //Rajeesh S-66531 job title lookup
        //save contact fields
        var validTalent = true;
        validTalent = helper.validateSalary(cmp, validTalent);
        validTalent = helper.validateRate(cmp, validTalent); 
        validTalent = helper.validateTitleError(cmp, validTalent); 
        if(validTalent){
		  cmp.set("v.spinnerState", true);
          helper.saveTalentBaseFields(cmp);
        }

		//Rajeesh stop writing client side backup
		cmp.set('v.eraseBackup',true);
		var dataBackup = cmp.find("clientSideBackup"); 
		dataBackup.clearDataBackup();
    },
    removeLocation :function(cmp, event, helper) {
        var index = event.getSource().get('v.value');
        var locs = cmp.get("v.record.geographicPrefs.desiredLocation");
        locs.splice(index, 1);//deleting the element in the array
        cmp.set("v.record.geographicPrefs.desiredLocation",locs);
    },//added for S-41845 akshay konijeti 10/31 end
    backToTalent : function (cmp, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": cmp.get("v.talentId")//cmp.get("v.recordId")
        });
        navEvt.fire();
    },
    toggleexpcol : function (cmp, event, helper) {
        var buttonstate = cmp.get('v.expandcollapse');
        cmp.set('v.expandcollapse', !buttonstate);
    }, 

    updateMaxSalary: function (component, event, helper){
       helper.updateMaxSalary(component, event);
      }, 

    updateMax: function (component, event, helper){
       helper.updateMax(component, event);
      }, 

    calculateTotalComp: function(component, event, helper){
        helper.calculateBonusAmountOnSalary(component);
        helper.calculateBonusPctOnSalary(component);
        helper.calculateTotalComp(component, event);
      }
})