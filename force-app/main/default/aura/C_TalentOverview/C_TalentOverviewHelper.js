({
	saveTalentBaseFields:function(component,event){
		var talentBaseFields = component.get("v.contactBaseFields");
		talentBaseFields.Title = component.get("v.jobTitle");
		component.set("v.contactBaseFields",talentBaseFields);
		var parms = {"contact":talentBaseFields};
		this.callServer(component,'ATS','',"saveTalentBaseFields",function(resp){
			this.saveCandidateSummary(component);
			var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
			updateLMD.fire();
			$A.get('e.force:refreshView').fire();
			component.set("v.g2CommentsId",""); // need to clear to keep it blank when loaded
		},parms,false);
	} , 
	checkAccess:function(component){
	//Rajeesh checking access to notes parser.
		var action = component.get('c.hasAccessToNotesParser');
		action.setCallback(this,function(response){
			if(response.getState()=="SUCCESS")
			{
				var hasAccess = response.getReturnValue();
				console.log('user has access to notes parser'+hasAccess);
				if(hasAccess){
					component.set('v.hasAccess',true);
				}
			}
			else{
				console.log('something unexpected happened---'+response.getState());
			}
		});
		 $A.enqueueAction(action);
	},
   validateSalary: function(component, validTalent){

       var minSalaryField = component.find("desiredsalary"); 
       var maxSalaryField = component.find("desiredsalarymax");
	   var freqField = component.find("ratetypeId");
	   var freqValue = freqField.get("v.value");
	   var minField = component.find("desiredrateId");
	   var minValue = minField.get("v.value");
       var minSalary = minSalaryField.get("v.value");
       var maxSalary = maxSalaryField.get("v.value");
	   //Added RF check condition by akshay for S - 82338
	   if($A.util.isEmpty(freqValue) && !$A.util.isEmpty(minValue)){
        freqField.set("v.errors", [{message: "Please select Rate Frequency"}]);
        validTalent = false;
        this.showError("Please review the highlighted errors and try again.");
       }else{
        freqField.set("v.errors", null);
        freqField.set("v.isError", false);
		}

       if($A.util.isEmpty(minSalary) && !$A.util.isEmpty(maxSalary)){
        minSalaryField.set("v.isError",true);
        minSalaryField.set("v.errors", [{message:"Please enter Min. Salary"}]);
        validTalent = false;
        $A.util.addClass(minSalaryField,"slds-has-error show-error-message");
        this.showError("Not all fields passed validation.");
       }else{
        minSalaryField.set("v.isError",true);
        minSalaryField.set("v.errors", [{message:""}]);
        $A.util.removeClass(minSalaryField,"slds-has-error show-error-message");
       }

       if(minSalary > maxSalary && !$A.util.isEmpty(maxSalary)){
        maxSalaryField.set("v.errors", [{message:"Min salary cannot be greater than Max. Salary"}]);
        validTalent = false;
        maxSalaryField.set("v.isError",true);
        $A.util.addClass(maxSalaryField,"slds-has-error show-error-message");
        this.showError("Not all fields passed validation.");
       }else{
        maxSalaryField.set("v.errors", [{message:""}]);
        maxSalaryField.set("v.isError",false);
        $A.util.removeClass(maxSalaryField,"slds-has-error show-error-message");
       }
       return validTalent;

    },
	validateTitleError : function(component, validTalent) {
        var hiddenField = component.find("hiddenTitle");
        var hidden = hiddenField.get("v.value");
        if (!hidden || hidden === "") {
            validTalent = false;
            component.set("v.titleError", true);
        } else {
            component.set("v.titleError", false);
        }
        return validTalent;
    },
    validateRate: function(component, validTalent){

       var minField = component.find("desiredrateId"); 
       var maxField = component.find("desiredratemaxId");
       var minValue = minField.get("v.value");
       var maxValue = maxField.get("v.value");

       if($A.util.isEmpty(minValue) && !$A.util.isEmpty(maxValue)){
        minField.set("v.isError", true);
        minField.set("v.errors", [{message: "Please enter Min. Rate"}]);
        validTalent = false;
        $A.util.addClass(minField,"slds-has-error show-error-message");
        this.showError("Not all fields passed validation.");
       }else{
        minField.set("v.errors", [{message: ""}]);
        minField.set("v.isError", false);
        $A.util.removeClass(minField,"slds-has-error show-error-message");
       }
      
       if(minValue > maxValue && !$A.util.isEmpty(maxValue)){
        maxField.set("v.errors", [{message:"Min Rate cannot be greater than Max. Rate"}]);
        validTalent = false;
        maxField.set("v.isError", true);
        $A.util.addClass(maxField,"slds-has-error show-error-message");
        this.showError("Not all fields passed validation.");
       }else{
        maxField.set("v.errors", [{message:""}]);
        maxField.set("v.isError", false);
        $A.util.removeClass(maxField,"slds-has-error show-error-message");
       }  
       return validTalent;
    },

	extractSkills : function(model) {
		if(typeof model !== 'undefined') {
			return JSON.parse(model);
		}
		else {
			var skillstr = '{"skills":[]}';
			return JSON.parse(skillstr);
		}
	},

	extractPreferenceInternal : function(model) {
		if(model && model.length > 2) {
			return JSON.parse(model);
		}
		else {
			var ipjsonstr = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
			return JSON.parse(ipjsonstr);			
		}
	},

	extractLanguageList : function(model) {
		var opt = {};
		var key = '';
		for(key in model) {
			opt[key] = model[key];
		}
		return opt;
    },

    setPicklistValues : function(component, currencyList) {
        //similarly to BaseTalentPreferences populatePicklist
        var desiredCurrency = component.get("v.talent.Desired_Currency__c");
        component.set("v.desiredCurrency",desiredCurrency); 
        var CurrLevel = [];
        var CurrLevelMap = currencyList;
        for ( var key in CurrLevelMap ) {
            CurrLevel.push({value:key, key:key,selected: key === desiredCurrency});
        }
        component.set("v.CurrencyList", CurrLevel); 

	var natOpp =  component.get("v.record.geographicPrefs.nationalOpportunities");
        if(natOpp  != null){
            if(natOpp  == true){
                component.set("v.record.geographicPrefs.nationalOpportunities","Yes"); 
            }else{
                component.set("v.record.geographicPrefs.nationalOpportunities","No"); 
            }
        } else {
			component.set("v.record.geographicPrefs.nationalOpportunities","");
		}
        
        var reli = component.get("v.record.employabilityInformation.reliableTransportation");
        if(reli  != null){
            if(reli  == true){
                component.set("v.record.employabilityInformation.reliableTransportation","Yes"); 
            }else{
                component.set("v.record.employabilityInformation.reliableTransportation","No"); 
            }
        } else {
			component.set("v.record.employabilityInformation.reliableTransportation","");
		}
        /*var natOpp =  component.get("v.record.geographicPrefs.nationalOpportunities");
        if(natOpp  != null){
            if(natOpp  == true){
                component.set("v.nationalOpp","Yes"); 
            }else{
                component.set("v.nationalOpp","No"); 
            }
        }
     
        var reli = component.get("v.record.employabilityInformation.reliableTransportation");
        if(reli  != null){
            if(reli  == true){
                component.set("v.reliable","Yes"); 
            }else{
                component.set("v.reliable","No"); 
            }
        }*/
        
        var commuteType = component.get("v.record.geographicPrefs.commuteLength.unit");
        if(commuteType != null){
            component.set("v.commuteType",commuteType); 
        }
    }
})