({
	// Your renderer method overrides go here
	rerender : function(component, helper){
		try{	
    		/* Rajeesh S-108773 - Ability to Restore Backup from 'refreshed' G2 on Inline Edit Card
			 * create a track fields object with all the variables that need to be backed up 
    		 * and take a backup using the backup component.
			 * backup component creates a key for the card (using card number and contact id), and create a json and base 64 encode
			 * the string before storing in session storage.
			 */
			this.superRerender();
			var initOnly = component.get("v.initOnly");
			var eraseBackup = component.get('v.eraseBackup');
			//var usrHasAccess = component.get('v.usrHasAccess');
			//console.log('renderer userhasaccess'+usrHasAccess);
			if(!initOnly && !eraseBackup){
				var trackObj={};
				trackObj.talent=component.get('v.talent');
				trackObj.contactBaseFields = component.get('v.contactBaseFields');
				trackObj.jobTitle = component.get('v.jobTitle');
				trackObj.geoPrefComments = component.get('v.geoPrefComments');
				trackObj.contactRecord = component.get('v.contactRecord');

				/*
					Attributes from base talent preference Component
				*/
				trackObj.card				=	component.get('v.card');
				trackObj.talentId			=	component.get('v.talentId');
				trackObj.recordId			=	component.get('v.recordId');
				trackObj.edit				=	component.get('v.edit');
				trackObj.record				=	component.get('v.record');
				trackObj.currentSkills		=	component.get('v.currentSkills');
				trackObj.languageList		=	component.get('v.languageList');
				trackObj.currentLanguages	=	component.get('v.currentLanguages');
				trackObj.countries			=	component.get('v.countries');
				trackObj.CurrencyList		=	component.get('v.CurrencyList');
				trackObj.reliable			=	component.get('v.reliable');
				trackObj.nationalOpp		=	component.get('v.nationalOpp');
				trackObj.commuteType		=	component.get('v.commuteType');
				trackObj.desiredCurrency	=	component.get('v.desiredCurrency');
				trackObj.totalComp			=	component.get('v.totalComp');
				trackObj.totalCompMax		=	component.get('v.totalCompMax');
				trackObj.countryKey0		=	component.get('v.countryKey0');
				trackObj.countryKey1		=	component.get('v.countryKey1');
				trackObj.countryKey2		=	component.get('v.countryKey2');
				trackObj.countryKey3		=	component.get('v.countryKey3');
				trackObj.countryKey4		=	component.get('v.countryKey4');
				trackObj.countryValue		=	component.get('v.countryValue');
				trackObj.stateKey			=	component.get('v.stateKey');
				trackObj.stateValue			=	component.get('v.stateValue');
				trackObj.geoComments		=	component.get('v.geoComments');
				trackObj.isG2Checked		=	component.get('v.isG2Checked');
				trackObj.todayDate			=	component.get('v.todayDate');
				trackObj.g2Date				=	component.get('v.g2Date');
				trackObj.isG2DateToday		=	component.get('v.isG2DateToday');
				trackObj.runningUser		=	component.get('v.runningUser');
				trackObj.usrOwnership		=	component.get('v.usrOwnership');
				trackObj.isOPCOInDictionary	=	component.get('v.isOPCOInDictionary');
				trackObj.g2CommentsId		=	component.get('v.g2CommentsId');
				var dataBackup = component.find("clientSideBackup");
				dataBackup.setDataBackup(trackObj); 
			}
			//component.set("v.initOnly",false);
		}
		catch(e){
			console.log('exception in talent overview rerender'+e.message); 
		}
	}
	    
})