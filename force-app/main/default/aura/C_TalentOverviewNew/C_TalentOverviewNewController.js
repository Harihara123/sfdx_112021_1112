({
    doInit : function(cmp,event,helper){
        
        var conRecordId = cmp.get("v.recordId");
        var params = {"conId" : conRecordId};
        const viewOnly = cmp.get("v.viewOnly");
        //Rajeesh check notes parser access.
        helper.getUserDetails(cmp);
        helper.checkAccess(cmp);
        helper.checkAccessForN2PCan(cmp);
        var hlp = cmp.find("addEditBaseHelper");
        hlp.callServer(cmp, '', '', 'getTalentOverviewModel', function(response) {

            if(viewOnly){
                cmp.set("v.spinnerState", true);
            }
            var state = response.record; // record is account
            cmp.set("v.runningUser", response.uoModal.usr);
			cmp.set("v.usrOwnership", response.uoModal.userOwnership); 
			cmp.set("v.runningUser.userOwnership", response.uoModal.userOwnership);
            cmp.set("v.runningUserCategory", response.uoModal.userCategory);
            cmp.set("v.talent",state);
			cmp.set("v.talentId",state.Id);
            if(response.talentBaseFields != null && response.talentBaseFields != '') {
                cmp.set("v.contactBaseFields",response.talentBaseFields);
                cmp.set("v.jobTitle",response.talentBaseFields.Title);
				helper.consentPrefInfo(cmp, response.uoModal.hasUserConsentTextAccess);
            }
                    
            //cmp.set("v.currentSkills", helper.extractSkills(state.Skills__c));
            cmp.set("v.record", helper.extractPreferenceInternal(state.Talent_Preference_Internal__c));
                
              /*  helper.setPicklistValues(cmp, response.currencyList);
                
                if((cmp.get('v.edit') == false) && ((typeof cmp.find("readdesiredrsalary") !== 'undefined' ) || (typeof cmp.find("readdesiredBonusId") !== 'undefined' ) || (typeof cmp.find("readdesiredAdditionalCompensationId") !== 'undefined' ))){
                   helper.calculateTotalComp(cmp); 
                }else if(cmp.get('v.edit') == true){
                   helper.calculateTotalComp(cmp); 
                }*/
				/* cmp.set("v.languageList",helper.extractLanguageList(response.languageMap));
                helper.populateLanguage(cmp);*/
                // G2 
                var g2Date = "";
                var day = new Date().getDate();
                var month = new Date().getMonth() + 1;
                var year = new Date().getFullYear();
                
                var getDate = day.toString();
                if (getDate.length == 1){ 
                    getDate = "0"+getDate;
                }
                var getMonth = month.toString();
                if (getMonth.length == 1){
                    getMonth = "0"+getMonth;
                }
                
                var todaysDate = year + "-" + getMonth + "-" + getDate;
                
                if(typeof state.G2_Completed_Date__c !== "undefined"){
                    var tempDate = state.G2_Completed_Date__c.split("T");
                    if(tempDate.length > 0){
                        g2Date = tempDate[0];
                        var todayInMilliSeconds = new Date(todaysDate).getTime(); 
                        var g2DateInMilliSeconds =  new Date(state.G2_Completed_Date__c).getTime();
                        cmp.set("v.isG2DateToday", (todayInMilliSeconds === g2DateInMilliSeconds));
                        var insightDate = new Date(g2Date.replace(/-/g, '\/')); 
                        cmp.set("v.g2CompletedDate", state.G2_Completed_Date__c);//g2DateInMilliSeconds
                        if (todayInMilliSeconds === g2DateInMilliSeconds) {
                            cmp.set("v.isG2Checked", true);
                        }
                    }
                }

               /* if(cmp.get("v.talent.Desired_Placement_type__c")) {
                    let desiredPlacement = cmp.get("v.talent.Desired_Placement_type__c");

                    desiredPlacement = desiredPlacement.split(';');

                    cmp.set("v.desiredPlacementList", desiredPlacement);
                }*/
				/*
					Rajeesh S-108773 - Ability to Restore Backup from 'refreshed' G2 on Inline Edit Card
					Feature enabled based on alias included in label. If label is set to GA, rolled out to everyone.
					Label contains comma seperated userids
				*/
				//console.log('overview cmp name'+cmp.getName());
				cmp.set("v.initOnly",true);
				cmp.set('v.g2CommentsId','');//clear g2 field
				//End Rajeesh
				if (state.Skills__c) {
					try {
						cmp.set('v.talent.Skills__c_parsed',JSON.parse(state.Skills__c));
					} catch (parseExcpetion) {
						helper.showToast("Error", "This talent's skills data is corrupt. Please contact helpdesk for support before making any updates!", 'error');
					}
				}
				if (state.Tasks && state.Tasks[0]) {
					cmp.set("v.lastActivityDetails",state.Tasks[0]);
				}
				
				if (cmp.get("v.invokeSubComp") === true) {
					var locationSectionComp = cmp.find("locationSection");
					if (locationSectionComp) {
						locationSectionComp.invokeDoInit();
					}	
					var compWorkSection = cmp.find("compWorkSection");
					if (compWorkSection) {
						compWorkSection.invokeDoInit();
					}
					var overViewSection = cmp.find("overViewSection");
					if (overViewSection) {
						overViewSection.invokeDoInit();
					}
				}

				cmp.set('v.talentLoaded', true);
                if(viewOnly && cmp.get("v.talentLoaded")){
                    cmp.set("v.spinnerState", false);
                }
				
        },params,false);

        if (viewOnly) {
            cmp.set('v.expandcollapse', true);

            const g2LoadedEvt = cmp.getEvent("g2Loaded");
            g2LoadedEvt.setParam("g2Loaded", cmp.get("v.talentLoaded"));
            g2LoadedEvt.fire();
        }
        // Load user location
        navigator.geolocation.getCurrentPosition(function(position) {
            cmp.set("v.userLocation", position.coords.latitude + "," + position.coords.longitude);
        });
    },

    invokeSubComp : function(cmp, event, helper) {
            cmp.set("v.invokeSubComp",true)
    },
    launchNotesExtractor: function(cmp, event, helper) {
        //Rajeesh changing component name to C_NotesParser 
        console.log('---curr cmp ref---'+JSON.stringify(cmp.get("v.contactBaseFields")));
        $A.createComponent( "c:C_NotesParser",{"recId" : cmp.get("v.recordId"), "talentId" : cmp.get("v.talentId"),"parentCmp":cmp },function(newButton, status, errorMessage){
        //$A.createComponent(   "c:C_CanvasIntegrator",{"recordId" : cmp.get("v.recordId"), "talentId" : cmp.get("v.talentId"), "userLocation" : cmp.get("v.userLocation"),"contactBaseFields" : cmp.get("v.contactBaseFields"), "parentCmp":cmp},function(newButton, status, errorMessage){
            //Add the new button to the body array
            if (status === "SUCCESS") {
                var body = cmp.get("v.body");
                body.unshift(newButton);    
                cmp.set("v.body", body);
                cmp.set("v.enableN2PButton",false);
            }
            else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.");
                cmp.set("v.enableN2PButton",true);
                // Show offline error
            }
            else if (status === "ERROR") {
                console.log("Error: " + errorMessage);
                cmp.set("v.enableN2PButton",true);
            // Show error message
            }
            else{
            } 
            });
    }, 
    launchNotesExtractorCan: function(cmp, event, helper) {
        
        var runningUser = cmp.get("v.runningUser");
        var isG2 = cmp.get("v.isG2Checked");
        console.log('isG2----------------------'+isG2);

        //setTimeout($A.getCallback(helper.getUserDetails.bind(this, cmp)),0);

        var x = cmp.get('v.userDetails');

        /*
        if(isG2){
            var parsingModal = "G2";//Or it could be based on the other user specific details
        }else{
            var parsingModal = "Spacy";//Or it could be based on the other user specific details
        }   
        */
        var parsingModal = "Spacy"; // Regression deffect for 16.0 by phani
        // record id contact and talentId is the account id, runningUser is the user record, 

        $A.createComponent( "c:C_CanvasIntegrator",{"runningUser" : runningUser, "userDetails" : cmp.get("v.userDetails"), "recordId" : cmp.get("v.recordId"), "talentId" : cmp.get("v.talentId"), "userLocation" : cmp.get("v.userLocation"),"contactBaseFields" : cmp.get("v.contactBaseFields"), "parentCmp":cmp,"parsingModal":parsingModal,"runningUser":cmp.get("v.runningUser")},function(newButton, status, errorMessage){
            //Add the new button to the body array
            if (status === "SUCCESS") {
                var body = cmp.get("v.body");
                body.unshift(newButton);    
                cmp.set("v.body", body);
                cmp.set("v.enableN2PButton",false);
            }
            else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.");
                cmp.set("v.enableN2PButton",true);
                // Show offline error
            }
            else if (status === "ERROR") {
                console.log("Error: " + errorMessage);
                cmp.set("v.enableN2PButton",true);
            // Show error message
            }
            else{
            } 
            });
    },            
    editTalentSummary :function(cmp, event, helper){
        var recordID = cmp.get("v.talentId");
        var contID = cmp.get("v.recordId");
        //var urlEvent = $A.get("e.c:E_TalentSummEdit");
        var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        urlEvent.setParams({
            "recordId": recordID,
            "recId": contID,
            "source": "Summary",
            "consentPrefInfo" : cmp.get("v.consentPrefInfo")
        });
        urlEvent.fire(); 
    },
    /*S-104786 - Neel -> Unused Method
    editCandidateSummary :function(cmp, event, helper) {
        helper.editCandidateSummary(cmp, event);
    },*/
    /*S-104786 - Neel -> Unused Method
    viewCandidateSummary :function(cmp, event, helper) {
        helper.viewCandidateSummary(cmp, event);
    },*/
    enableInlineEdit :function(cmp, event, helper) {
        cmp.set("v.edit",true);  
       // $A.get('e.force:refreshView').fire();

        //Rajeesh start writing client side backup
        cmp.set('v.eraseBackup',false);
    },
    disableInlineEdit :function(cmp, event, helper) {
        // $A.get('e.force:refreshView').fire();
        cmp.set("v.edit",false);
         cmp.set("v.invokeSubComp",true);
          var a = cmp.get('c.doInit');
        $A.enqueueAction(a);
        $A.get('e.force:refreshView').fire();
        
        //Rajeesh stop writing client side backup
        cmp.set('v.eraseBackup',true);
        var dataBackup = cmp.find("clientSideBackup"); 
        dataBackup.clearDataBackup();
        cmp.set('v.g2CommentsId','');//clear g2 field
    },
    /*S-104786 - Neel -> Unused Method
    rateTypeChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },
    S-104786 - Neel -> Unused Method
    rateChange : function (cmp, event, helper) {
        helper.calculateBonusAmount(cmp);
        helper.calculateTotal(cmp);
    },*/
    
    bonusPctChange : function (cmp, event, helper) {
        helper.calculateBonusAmountOnSalary(cmp);
        helper.calculateTotalComp(cmp);
    },
    
    bonusChange : function (cmp, event, helper) {
        helper.calculateBonusPctOnSalary(cmp);
        helper.calculateTotalComp(cmp);
    },
    
    AddlCompChange : function (cmp, event, helper) {
        helper.calculateTotalComp(cmp);
    },
    
    editCandidateSummary :function(cmp, event, helper) {
        
        helper.editCandidateSummary(cmp);
    },
    
    addNewLocation :function(cmp, event, helper) {//added for S-41845 akshay konijeti 10/31 start
        var locs = [];
        if(typeof cmp.get("v.record.geographicPrefs.desiredLocation") !== "undefined"){
            locs = cmp.get("v.record.geographicPrefs.desiredLocation");  
        }
        
        locs.push({country:'',state:'',city:''});
        cmp.set("v.record.geographicPrefs.desiredLocation",locs);
    },
    saveCandidateSummary :function(cmp, event, helper) {
        //Rajeesh S-66531 job title lookup
        //save contact fields
       /* var validTalent = true;
        validTalent = helper.validateSalary(cmp, validTalent);
        validTalent = helper.validateRate(cmp, validTalent); 
        validTalent = helper.validateTitleError(cmp, validTalent); 
        if(validTalent){
		  cmp.set("v.spinnerState", true);
          helper.saveTalentBaseFields(cmp);
          $A.get('e.force:refreshView').fire();
        }*/
		

		var overViewSectionValid = cmp.find("overViewSection").validTalent();
		var compWorkSectionValid = cmp.find("compWorkSection").validTalent();
		if (overViewSectionValid && compWorkSectionValid) {
		    cmp.set("v.setValues", !cmp.get("v.setValues"));
			cmp.set("v.spinnerState", true);
			helper.saveTalentModal (cmp);
            cmp.set("v.edit",false);
			//helper.saveTalentBaseFields(cmp);			
		} 

		//Rajeesh stop writing client side backup
		cmp.set('v.eraseBackup',true);
		var dataBackup = cmp.find("clientSideBackup"); 
		dataBackup.clearDataBackup();
		
    },
	afterRender: function (cmp,event, helper) {
		/*var createdLog = cmp.get("v.singleLogCreated");
		var communityProfile =cmp.get("v.viewOnly");
		console.log('showSRI-----------'+communityProfile);
		if(communityProfile){	
	
		
				if(createdLog){			
					//console.log('Log created once Boolean----------------'+createdLog);
					//console.log('auraCmpU--after-if--------------'+cmp.get("v.showSRI"));
				}else{			
					//var auraCmpU = cmp.find("clogId").getLOGAPI();
					cmp.set("v.singleLogCreated",true);
					console.log('Log Created once-----------------'+cmp.get("v.singleLogCreated"));
					//console.log('auraCmpU--after---else------------'+cmp.get("v.showSRI"));
				}
			
		}*/
	},
	/*onRender: function (cmp, helper) {			
		//-----------------------
		//var auraCmp = cmp.find("clogId").getAPI;
		//var auraCmpF = cmp.find("clogId").getAPI();
		var auraCmpU = cmp.find("clogId").getLOGAPI();
		//console.log('auraCmp-----------------'+auraCmp);
		//console.log('auraCmpF-----------------'+auraCmpF);
		//console.log('auraCmpU-----------------'+auraCmpU);
		//var auraCmpAPI = auraCmp.getAPI();		
		//auraCmpF.logInfo(auraCmpF,{LogTypeId: 'ExternalData/Communities/loaded', Method: 'connectedCallback', Module: 'talentLanding'});
		//this.superAfterRender();
	},*/

    removeLocation :function(cmp, event, helper) {
        var index = event.getSource().get('v.value');
        var locs = cmp.get("v.record.geographicPrefs.desiredLocation");
        locs.splice(index, 1);//deleting the element in the array
        cmp.set("v.record.geographicPrefs.desiredLocation",locs);
    },//added for S-41845 akshay konijeti 10/31 end
    backToTalent : function (cmp, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": cmp.get("v.talentId")//cmp.get("v.recordId")
        });
        navEvt.fire();
    },
    toggleexpcol : function (cmp, event, helper) {
        var buttonstate = cmp.get('v.expandcollapse');
        cmp.set('v.expandcollapse', !buttonstate);
    }, 
    toggleSRI: function (cmp, e, h) {
        const cmpLoaded = e.getParam('contentLoaded');
        cmp.set("v.showSRI", cmpLoaded);
    },
   /* updateMaxSalary: function (component, event, helper){
       helper.updateMaxSalary(component, event);
      }, 

    updateMax: function (component, event, helper){
       helper.updateMax(component, event);
      }, 

    calculateTotalComp: function(component, event, helper){
        helper.calculateBonusAmountOnSalary(component);
        helper.calculateBonusPctOnSalary(component);
        helper.calculateTotalComp(component, event);
      },*/

	// Monika -- S-189738 -- Focus back on source after G2 modal is closed/cancel button is hit 
	focusonSource:function(component, event, helper) {
		const fieldId = event.getParam("fieldIdToGetFocus");
		if(fieldId == null) return;
		helper.setFocusToField(component, event, fieldId);
    }
})