({

	checkAccess:function(component){
	//Rajeesh checking access to notes parser.
		var action = component.get('c.hasAccessToNotesParser');
		action.setCallback(this,function(response){
			if(response.getState()=="SUCCESS")
			{
				var hasAccess = response.getReturnValue();
				//console.log('user has access to notes parser'+hasAccess);
				if(hasAccess){
					component.set('v.hasAccess',true);
				}
			}
			else{
				console.log('something unexpected happened---'+response.getState());
			}
		});
		 $A.enqueueAction(action);
	},
	checkAccessForN2PCan:function(component){
		var action = component.get('c.hasAccessToG2Parser');
		action.setCallback(this,function(response){
			if(response.getState() == "SUCCESS"){
				var hasAccess = response.getReturnValue();
				console.log('user has access: ' + hasAccess);
				if(hasAccess){
					component.set('v.hasAccessCan', true);
				}
			} else {
				console.log('Invalid Response: ' + response.getState());
			}
		});
		$A.enqueueAction(action);
	 },
	 saveTalentModal : function(component) {		
        var contact = component.get("v.contactBaseFields");
        var account = component.get("v.talent");  
        if(component.isValid()){
            //this.savePickList(component);
           // this.populateLanguageCode(component);
            
            component.set("v.talent.Talent_Preference_Internal__c", JSON.stringify(component.get("v.record")));
            component.set("v.talent.Skills__c", JSON.stringify(component.get("v.talent.Skills__c_parsed")));
			// Remove the Skills__c_parsed key from this object before saving to server since this is not a valid filed on Account.
			delete component.get("v.talent").Skills__c_parsed;
            
			var G2Comp = component.find("G2Section");
			var g2Comments = G2Comp.getG2Comments();

            if (component.get("v.isG2Checked")) {
                if (component.get("v.isG2DateToday")) {
                    component.set("v.talent.G2_Completed__c", false);
                } else {
                    component.set("v.talent.G2_Completed__c", true);
                }
            } else {
                component.set("v.talent.G2_Completed__c", false);
            }
            
            var dnc = component.get("v.talent.G2_Completed__c");
            
            var serverMethod = 'c.saveTalentRecordModal'; 
            var params = {
                "contact": contact, 
                "account": component.get("v.talent"),
                "g2Comments" : g2Comments,
				"g2Source": "inline-edit"
            };
            
            var acc = component.get("v.talent");
            var action = component.get(serverMethod);
            action.setParams(params);
            
            action.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") { 
					this.redirectCandidateSummary(component);
                    var updateLMD = $A.get("e.c:E_TalentActivityReload");
					updateLMD.fire();
                    var appEvent = $A.get("e.c:RefreshTalentHeader");
                    appEvent.fire();
                    this.showToast('Success', 'Talent has been updated!', 'success');

                } 
                else if (state === "ERROR") {
                    this.checkError(response, component);
                } 
				
				//$A.get('e.force:refreshView').fire();
				component.set("v.edit",false);
				component.set("v.spinnerState", false);
            });
            $A.enqueueAction(action);
        }
    },

    showToast: function(title, message, type) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },

    checkError : function(response, component){
        var errors = response.getError();
        if (errors) {
            //console.log("Errors", errors);
            if (errors[0].fieldErrors) {
                //this.showServerSideValidation(errors, component);
              //  var profileSectionComp = component.find("profileSection");
              //  profileSectionComp.showServerSideValidation(errors);
            } else if (errors[0] && errors[0].message) {
                this.showToast(errors[0].message, 'An error occured!', 'error');
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showToast(errors[0].fieldErrors[0].statusCode,"An error occured!", 'error');
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showToast(errors[0].pageErrors[0].statusCode,'An error occured!', 'error');
            }else{
                this.showToast('An unexpected error has occured. Please try again!', 'error');
            }
        } else {
            this.showToast(errors[0].message, 'error');
        }
    },

	extractPreferenceInternal : function(model) {
		if(typeof model !== 'undefined') {
			return JSON.parse(model);
		}
		else {
			var ipjsonstr = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
			return JSON.parse(ipjsonstr);			
		}
	},
   redirectCandidateSummary :function(cmp) {
       if(cmp.get("v.card")){
            $A.get('e.force:refreshView').fire();
			cmp.set("v.spinnerState", false);
            cmp.set("v.edit",false);
       }else{
        this.viewCandidateSummary(cmp);   
       }    
    },
    viewCandidateSummary :function(cmp) {
        
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        /*urlEvent.setParams({
                  "url": "/one/one.app#/sObject/" + recordID +"/view",
                  "isredirect":'true'
         });*/
        var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
        var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
        var sObj;
        var dURL;
        
        if(isFlagOn === "true") {
            if(recordID){
                if(recordID.substring(0, 3) === '001'){
                   sObj = 'Account';
                    
                }else if(recordID.substring(0, 3) === '003'){
                    sObj = 'Contact'; 
                }
            }
            dURL = ligtningNewURL+"/r/"+sObj+"/" + recordID +"/view";
        }
        else {
            dURL = ligtningNewURL+"/sObject/" + recordID +"/view";
        }
        
        urlEvent.fire();  
        
        
    },

    consentPrefInfo : function(component, hasUserConsentTextAccess) {
        let hasConsentPrefAccess = false;
        let contactRec =  component.get("v.contactBaseFields");
        
        if(hasUserConsentTextAccess && (contactRec.MailingCountry === 'USA' || contactRec.MailingCountry == 'United States')) {
            hasConsentPrefAccess = true;
        }
        
        let consentPref = {hasConsentPrefAccess : hasConsentPrefAccess,
                           PrefCentre_Aerotek_Mobile : contactRec.PrefCentre_Aerotek_OptOut_Mobile__c,
                           PrefCentre_Aerotek_Email : contactRec.PrefCentre_Aerotek_OptOut_Email__c };

        component.set("v.consentPrefInfo", consentPref);
    },

    getUserDetails : function(cmp) {

    
        var serverMethod = 'c.getUserDetailsN2P';
        var action = cmp.get(serverMethod);
        /*var params = {
            "userid" : runningUser.Id
        };

        action.setParams(params);*/
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = JSON.parse(JSON.stringify(response.getReturnValue()));
                console.log("Getting User Details");
                var ud = cmp.get('v.userDetails');
                ud.Office__c = result.Office__c;
                ud.Office_Code__c = result.Office_Code__c;
                ud.Name = result.Name;
                ud.Email = result.Email;
                cmp.set('v.userDetails', JSON.parse(JSON.stringify(ud)));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("Error Getting User Details");
                console.log(JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);

    },

    //Monika - Set focus to a particular field
    setFocusToField:function(component,event,cmpId){
        const fieldIdToGetFocus = component.find(cmpId);
        if(fieldIdToGetFocus == null) return;
        window.setTimeout(
            $A.getCallback(function() {
                // wait for element to render then focus
                fieldIdToGetFocus.focus();
            }), 100
        );
    },
})