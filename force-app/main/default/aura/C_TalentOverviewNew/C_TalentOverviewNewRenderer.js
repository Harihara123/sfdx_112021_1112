({
		rerender: function(component, helper) {
		/* Rajeesh S-108774 - Ability to Restore Backup from 'refreshed' G2 on Talent Summary Modal
			 * create a track fields object with all the variables that need to be backed up 
    		 * and take a backup using the backup component.
			 * backup component creates a key for the card (using card number and contact id), and create a json and base 64 encode
			 * the string before storing in session storage.

			 Rajeesh 1/29/2019: Refactoring After G2 changes.
			 The component is constructed using multiple child components, and data elements get modified in child components which will not trigger
			 parents rerender. Forcing it by passing a reference to parent's instance and updating a dummy variable in parent for every change that happens in any of the children,
		*/
		this.superRerender();
		var trackObj={};
		var initOnly = component.get("v.initOnly");
		var eraseBackup = component.get('v.eraseBackup');
		//var usrHasAccess = component.get('v.usrHasAccess');
		//console.log('renderer userhasaccess'+usrHasAccess);
		if(!initOnly && !eraseBackup){
            /*trackObj.edit				=	component.get('v.edit');
			trackObj.g2CommentsId		=	component.get('v.g2CommentsId');
			trackObj.lastActivityDetails			=	component.get('v.lastActivityDetails');
			trackObj.talentLoaded				=	component.get('v.talentLoaded');
			trackObj.toggleLayout			=	component.get('v.toggleLayout');
			trackObj.context				=	component.get('v.context');
			trackObj.radioOptions				=	component.get('v.radioOptions');
			trackObj.spinnerState		=	component.get('v.spinnerState');
			trackObj.expandcollapse		=	component.get('v.expandcollapse');
			trackObj.contactRecord	=	component.get('v.contactRecord');
			trackObj.desiredPlacementList			=	component.get('v.desiredPlacementList');
			trackObj.geoPrefComments		=	component.get('v.geoPrefComments');
			trackObj.jobTitle			=	component.get('v.jobTitle');
			trackObj.contactBaseFields		=	component.get('v.contactBaseFields');
			trackObj.talent		=	component.get('v.talent');
			trackObj.hasAccess	=	component.get('v.hasAccess');
			trackObj.card			=	component.get('v.card');
			*/
            trackObj.edit				=	component.get('v.edit');
            trackObj.g2CommentsId		=	component.get('v.g2CommentsId');
			trackObj.talentId			=	component.get('v.talentId');
			trackObj.talent				=	component.get('v.talent');
			trackObj.card			=	component.get('v.card');
			trackObj.recordId			=	component.get('v.recordId');
			trackObj.edit				=	component.get('v.edit');
			trackObj.record				=	component.get('v.record');
			trackObj.currentSkills		=	component.get('v.currentSkills');
			trackObj.languageList		=	component.get('v.languageList');
			trackObj.currentLanguages	=	component.get('v.currentLanguages');
			trackObj.countries			=	component.get('v.countries');
			trackObj.CurrencyList		=	component.get('v.CurrencyList');
			trackObj.reliable			=	component.get('v.reliable');
			trackObj.nationalOpp		=	component.get('v.nationalOpp');
			trackObj.commuteType		=	component.get('v.commuteType');
			trackObj.desiredCurrency	=	component.get('v.desiredCurrency');
			trackObj.totalComp			=	component.get('v.totalComp');
			trackObj.totalCompMax		=	component.get('v.totalCompMax');
			trackObj.countryKey0		=	component.get('v.countryKey0');
			trackObj.countryKey1		=	component.get('v.countryKey1');
			trackObj.countryKey2		=	component.get('v.countryKey2');
			trackObj.countryKey3		=	component.get('v.countryKey3');
			trackObj.countryKey4		=	component.get('v.countryKey4');
			trackObj.countryValue		=	component.get('v.countryValue');
			trackObj.stateKey			=	component.get('v.stateKey');
			trackObj.stateValue			=	component.get('v.stateValue');
			trackObj.geoComments		=	component.get('v.geoComments');
			trackObj.isG2Checked		=	component.get('v.isG2Checked');
			trackObj.todayDate			=	component.get('v.todayDate');
			trackObj.g2Date				=	component.get('v.g2Date');
			trackObj.isG2DateToday		=	component.get('v.isG2DateToday');
			trackObj.runningUser		=	component.get('v.runningUser');
			trackObj.usrOwnership		=	component.get('v.usrOwnership');
			trackObj.isOPCOInDictionary	=	component.get('v.isOPCOInDictionary');
			
			trackObj.modalTitle			=	component.get('v.modalTitle');
			//trackObj.C_TalentAddEdit	=	component.get('v.C_TalentAddEdit');
			//trackObj.C_TalentResumePreview=	component.get('v.C_TalentResumePreview');
			//trackObj.C_TalentStatusBadge=	component.get('v.C_TalentStatusBadge');
			trackObj.contactRecord		=	component.get('v.contactRecord');
			trackObj.countriesMap		=	component.get('v.countriesMap');
			trackObj.presetLocation		=	component.get('v.presetLocation');
			trackObj.resumeLoaded		=	component.get('v.resumeLoaded');
			trackObj.jobTitle			=	component.get('v.jobTitle');
			trackObj.contactBaseFields			=	component.get('v.contactBaseFields');
			trackObj.account			=	component.get('v.account');

			var dataBackup = component.find("clientSideBackup"); 
			dataBackup.setDataBackup(trackObj); 
		}
		//component.set("v.initOnly",false);
		//helper.setReRenderAttributes(component);
		//return this.superRerender();
	} 
})