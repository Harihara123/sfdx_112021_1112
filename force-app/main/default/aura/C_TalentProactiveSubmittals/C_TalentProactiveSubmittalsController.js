({  
    doInit: function(cmp, event, helper) {
        var conrecord = cmp.get("v.contactrecord");
        cmp.set("v.talentId",conrecord.fields.AccountId.value);
        var recordID = cmp.get("v.talentId");
        helper.getResults(cmp,helper);
        helper.getResultCount(cmp);  
        
    },
    editRecord : function(component, event, helper) {
        var recordID = event.getSource().get("v.value");
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": recordID
        });
        editRecordEvent.fire();
        
    },
    /*setupDisplayFields : function(cmp, event, helper) {
        
        var records = cmp.get("v.records");
    },*/
    linkToViewMore : function(cmp, event, helper) {
		var relatedListEvent = $A.get("e.force:navigateToRelatedList");
		relatedListEvent.setParams({
			"relatedListId": "Opportunities",
			"parentRecordId": cmp.get("v.talentId")
	        });
        relatedListEvent.fire();
    },
    linkToOppty: function(cmp, event, helper) {
    	var recordID = event.currentTarget.dataset.recordid;
       	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/" + recordID
        });
        urlEvent.fire();
    },
    linkProactiveSubmittal : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/skuid__ui?page=New_Guided_Req_Talent_Opportunity&accid=" + recordID
        });
        urlEvent.fire();
    },
  	loadData: function(cmp, event, helper) {
        cmp.set("v.loading",true);
        cmp.set("v.records",[]);
        cmp.set("v.recordCount",0);


        helper.getResults(cmp,helper);
        helper.getResultCount(cmp);
    }
    
    ,updateLoading : function(cmp,event,helper){
        helper.updateLoading(cmp);
    }
    ,reloadData : function(cmp,event,helper) {
        var rd = cmp.get("v.reloadData");
        if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);

            helper.getResults(cmp,helper);
            helper.getResultCount(cmp);

            cmp.set("v.reloadData", false);
        }

        
    }
})