({
	getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.talentId");
            var soql = listView[indexNo].soql;
			
            soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
  
                        var apexFunction = "c.getRows";

                        var params = {
                            "soql": soql,
                            "maxRows": cmp.get("v.maxRows")
                        };

                        cmp.set("v.loadingData",true);
                        var actionParams = {'recordId':recordID};
                        if(params){
                            if(params.length != 0){
                             Object.assign(actionParams,params);
                            }
                        }

                        var bdata = cmp.find("bdhelper");
                            bdata.callServer(cmp,'','',apexFunction,function(response){
                            cmp.set("v.records", response);
                            cmp.set("v.loadingData",false);
                        
                            if(listView[indexNo].additional){
                                cmp.set("v.additional",listView[indexNo].additional);
                            }
                            
                        },actionParams,false);
                    }
                
        }
    ,getResultCount : function(cmp){
        var listView = cmp.get("v.viewList");
        var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.talentId");
            var countSoql = listView[indexNo].countSoql;    
            
                    countSoql = countSoql.replace("%%%parentRecordId%%%","'"+recordID+"'")
                    var params = {"soql": countSoql};
                    cmp.set("v.loadingCount",true);
                    
                    var bdata = cmp.find("bdhelper");
                        bdata.callServer(cmp,'','','getRowCount',function(response){
                            cmp.set("v.recordCount", response);
                            cmp.set("v.loadingCount",false);
                        
                    },params,false);

        
    },

    updateLoading: function(cmp) {
       if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },

    refreshList : function(cmp) {
        cmp.set("v.reloadData", true);
    }
    /*getAddressString : function(fieldList) {
        var address = '';
        if (fieldList && fieldList.length > 0) {
            address = fieldList[0];
            for (var i = 1; i < fieldList.length; i++) {
                address = (fieldList[i] && fieldList[i].length > 0 ? (address && address.length > 0 ? (address + ", " + fieldList[i]) : fieldList[i]) : address); 
            }
        }
        return address;
	}*/
})