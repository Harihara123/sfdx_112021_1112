({
	doInit: function(component, event, helper) {
        var mode = "edit";
        if(mode != "edit"){    
        	var recommendationPicklist = "Salutation,Quality__c,Workload__c,Relationship__c,Relationship_Duration__c,Interaction_Frequency__c,Talent_Job_Duration__c";
            recommendationPicklist = recommendationPicklist + ',Rehire_Status__c,Competence__c,Leadership__c,Collaboration__c,Trustworthy__c,Likeable__c,Outlook__c,Team_Player__c,Professionalism__c';
            helper.initWrapper(component);
            helper.getAllPicklist(component, recommendationPicklist, 'salutation');   
        }
        else{
            helper.loadReference(component, "a224E0000010afoQAA");
        }
    },     
    callAddModal : function (component, event, helper) {
        helper.toggleClass(component,'backdropAddDocument','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
    },
    addTalentReference : function(component, event, helper) {
        helper.validateAndSaveReferences(component);
    },

    closeModal: function(component, event, helper) {
        // console.log('closing the model');
		helper.toggleClassInverse(component,'backdropAddDocument','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialogAddDocument','slds-fade-in-');
        var docModalCloseEvt = component.getEvent("documentModalCloseEvent");
        docModalCloseEvt.fire();
  
    }
})