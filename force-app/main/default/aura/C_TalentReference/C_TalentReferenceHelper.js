({
	showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    
    initWrapper : function(component){
    	var action = component.get('c.initWrapper');
 		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.reference', response.getReturnValue());
            }
		});
        $A.enqueueAction(action);
    },
    getAllPicklist: function(component, fieldName, elementId) {
        var serverMethod = 'c.getAllPicklistValues';
        var params = {
            "objectStr": "Contact, Talent_Recommendation__c",
            "fld": fieldName
        };
        var action = component.get(serverMethod);
        action.setParams(params);

        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 				var picklist = allValues["Contact.Solution"];
                var idName = "salutation";
                
                this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Quality__c"];
                idName = "qualityWork";
                this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Workload__c"];
                idName = "qualityWorkload";
                this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Relationship__c"];
                idName = "referenceRelationship";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Relationship_Duration__c"];
                idName = "relationshipDuration";
                this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Interaction_Frequency__c"];
                idName = "interactionFrequency";
                this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Talent_Job_Duration__c"];
                idName = "positionTime";
                this.setPicklistValues(component, picklist, idName);
                
                
                picklist = allValues["Recommendation.Rehire_Status__c"];
                idName = "rehire";
                this.setPicklistValues(component, picklist, idName);
               
                picklist = allValues["Recommendation.Competence__c"];
                idName = "ability";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Leadership__c"];
                idName = "initiative";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Collaboration__c"];
                idName = "Communication";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Trustworthy__c"];
                idName = "Reliability";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Likeable__c"];
                idName = "Likeability";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Outlook__c"];
                idName = "Outlook";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Team_Player__c"];
                idName = "teamMember";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Professionalism__c"];
                idName = "professionalism";
                this.setPicklistValues(component, picklist, idName);
            }
        });
        $A.enqueueAction(action);
    },
    setPicklistValues : function(component, picklist, fildName){
        console.log("#### Values ####");
        console.log(picklist);
        console.log(fildName);
        var opts = [];
        if (picklist != undefined && picklist.length > 0) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
        }
        for (var i = 0; i < picklist.length; i++) {
            opts.push({
                class: "optionClass",
                label: picklist[i],
                value: picklist[i]
            });
        }
        component.find(fildName).set("v.options", opts);
    },
    validateAndSaveReferences : function(component) {
        var mode = component.get("v.mode"); 
        if (this.validTalent(component))
            this.saveReference(component);   
    },
    validTalent: function(component) {
        var validTalent = true;
        validTalent = this.validateFirstName (component, validTalent);
        validTalent = this.validateLastName (component, validTalent);
        validTalent = this.validatePhoneEmailLinkedIn (component, validTalent);
        
        return validTalent;
    },
    validateFirstName : function(component, validTalent) {
        var firstNameField = component.find("firstName");
        var firstName = firstNameField.get("v.value");
        if (!firstName || firstName === "") {
            validTalent = false;
            firstNameField.set("v.errors", [{message: "Please enter a valid First Name"}]);
            firstNameField.set("v.isError",true);
            $A.util.addClass(firstNameField,"slds-has-error show-error-message");
        } else {
            firstNameField.set("v.errors", []);
            firstNameField.set("v.isError", false);
            $A.util.removeClass(firstNameField,"slds-has-error show-error-message");
        }
        return validTalent;
    },    

    validateLastName : function(component, validTalent) {
        var lastNameField = component.find("lastName");
        var lastName = lastNameField.get("v.value");
        if (!lastName || lastName === "") {
            validTalent = false;
            lastNameField.set("v.errors", [{message: "Please enter a valid Last Name"}]);
            lastNameField.set("v.isError",true);
            $A.util.addClass(lastNameField,"slds-has-error show-error-message");
        } else {
            lastNameField.set("v.errors", []);
            lastNameField.set("v.isError", false);
            $A.util.removeClass(lastNameField,"slds-has-error show-error-message");
        }
        return validTalent;
    },
    
    validatePhoneEmailLinkedIn : function(component, validTalent) {
        var mobilePhoneField = component.find("mobilePhone");
        var mobilePhone = mobilePhoneField.get("v.value");
        if(!this.validateMobilePhone(mobilePhone)) {
           mobilePhoneField.set("v.errors", [{message: "Please enter valid Mobile Phone."}]);
           mobilePhoneField.set("v.isError",true);
           validTalent = false;
        }

        var homePhoneField = component.find("homePhone");
        var homePhone = homePhoneField.get("v.value");
        if(!this.validateMobilePhone(homePhone)) {
           homePhoneField.set("v.errors", [{message: "Please enter valid Home Phone."}]);
           homePhoneField.set("v.isError",true);
           validTalent = false;
        }

        var workPhoneField = component.find("workPhone");
        var workPhone = workPhoneField.get("v.value");
        if(!this.validateMobilePhone(workPhone)) {
           workPhoneField.set("v.errors", [{message: "Please enter valid Work Phone."}]);
           workPhoneField.set("v.isError",true);
           validTalent = false;
        }
		
        var emailField = component.find("Email");
        var email = emailField.get("v.value");
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: "Please enter valid Email."}]);
            emailField.set("v.isError",true);
            validTalent = false;
        }

       if (!mobilePhone && !homePhone && !workPhone && !email) {
                validTalent = false;
                this.showError("One of the contact method either Phone or Email is required.");
        } 
        return validTalent;
    },       

     validateMobilePhone : function(mobilePhone) {
        if(mobilePhone && mobilePhone.length > 0) {
            var mp = mobilePhone.replace(/\s/g,'');
            if(mp.length >= 10 && mp.length <= 17) {
                
                var regex = new RegExp("^[0-9-.+()]+$");
                var isValid = mp.match(regex);
                
                if(isValid != null) {
                    return true; 
                }
                else{
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true; 
    },   

   validateEmail : function(emailID){
        if(emailID && emailID.trim().length > 0) {
            
            var isValidEmail = emailID.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

            if(isValidEmail != null) {
                return true;
            }
            else {
                return false;
            } 

        }
        return true;
    },
    saveReference : function(component){
    	var referenceWrap = component.get("v.reference");
        console.log("### Wrapper Values2 ###");
        console.log(referenceWrap);
        
        var serverMethod = 'c.saveTalentReference';	
        var params = {
            "contact": referenceWrap.contact,
            "Recommendation" : referenceWrap.recommendation,
            "talentAccount" : referenceWrap.account
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        
       action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                console.log(response);
            } else if (state === "ERROR") {
                console.log(response);
            } 
            
        });
        $A.enqueueAction(action);  
    },
    loadReference : function(component, referenceId){
    	var referenceWrap = component.get("v.reference");
        
        var serverMethod = 'c.getTalentReference';	
        var params = {
            "referenceId": referenceId
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        
       action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                console.log("EDIT");
                console.log(response.getReturnValue());
                component.set("v.reference",response.getReturnValue());
            } else if (state === "ERROR") {
                console.log(response);
            } 
            
        });
        $A.enqueueAction(action);  
    }
})