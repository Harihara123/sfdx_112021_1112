({
	doInit : function(cmp, event, helper){
        var params = null;
        var recordId = cmp.get("v.recordId") ;
         params = {recordId: recordId };

        var cmps = 'C_Candidate_Reference_v2';
        var cmpEvent = cmp.getEvent("createCmpEventReference");
        cmpEvent.setParams({
                "componentName" : 'c:' + cmps
                , "params":params
                ,"targetAttributeName": 'v.' + cmps });
           cmpEvent.fire();

    }, 
    handleCreateEvent : function(cmp, event, helper){
        var componentName = event.getParam("componentName");
        var targetAttributeName = event.getParam("targetAttributeName");
        var params = event.getParam("params");

        helper.createComponent(cmp, componentName, targetAttributeName,params);
    }
        
   
})