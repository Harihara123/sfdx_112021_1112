({
    downloadTEK : function(component, event, helper) {
        var downloadRefEvent = component.getEvent("downloadRefEvent");
        downloadRefEvent.setParams({ "itemId" : component.get("v.itemId")});
        downloadRefEvent.setParams({ "opco" : "TEK"});
		downloadRefEvent.fire();
	},
    
    downloadAerotek : function(component, event, helper) {
        var downloadRefEvent = component.getEvent("downloadRefEvent");
        downloadRefEvent.setParams({ "itemId" : component.get("v.itemId")});
        downloadRefEvent.setParams({ "opco" : "ONS"});
		downloadRefEvent.fire();
	},
    
    downloadAC : function(component, event, helper) {
        var downloadRefEvent = component.getEvent("downloadRefEvent");
        downloadRefEvent.setParams({ "itemId" : component.get("v.itemId")});
        downloadRefEvent.setParams({ "opco" : "AC"});
		downloadRefEvent.fire();
	},

    downloadStamfordGroup : function(component, event, helper) {
        var downloadRefEvent = component.getEvent("downloadRefEvent");
        downloadRefEvent.setParams({ "itemId" : component.get("v.itemId")});
        downloadRefEvent.setParams({ "opco" : "SG"});
        downloadRefEvent.fire();
    },

    downloadIND : function(component, event, helper) {                                   
        var downloadRefEvent = component.getEvent("downloadRefEvent");
        downloadRefEvent.setParams({ "itemId" : component.get("v.itemId")});
        downloadRefEvent.setParams({ "opco" : $A.get("$Label.c.Aerotek")});                                              
        downloadRefEvent.fire();
    },
    
    downloadSJA : function(component, event, helper) {
        var downloadRefEvent = component.getEvent("downloadRefEvent");
        downloadRefEvent.setParams({ "itemId" : component.get("v.itemId")});
        downloadRefEvent.setParams({ "opco" : $A.get("$Label.c.Aston_Carter")});
        downloadRefEvent.fire();
    },

    downloadEAS : function(component, event, helper) {
        var downloadRefEvent = component.getEvent("downloadRefEvent");
        downloadRefEvent.setParams({ "itemId" : component.get("v.itemId")});
        downloadRefEvent.setParams({ "opco" : $A.get("$Label.c.Newco")});
        downloadRefEvent.fire();
    },

    

})