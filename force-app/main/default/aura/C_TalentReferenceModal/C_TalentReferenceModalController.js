({
	doInit: function(component, event, helper) {
		//helper.focusBtn(component,"modalCloseBtn"); 
    },
	handleChange: function(component, event, helper) {
		console.log(component.get('v.reference'))  
    },
	handleMouseDown: function(component, event, helper) {
	    event.stopPropagation();
		helper.handleMouseDown(component, event); 
    },
	
	swapDesigns: function(component, event, helper) {
		component.set('v.oldDesign', !component.get('v.oldDesign'));
		//if (component.get('v.oldDesign'))  {
        
		//component.set("v.showSearchPopup",false);
        //helper.startSpinner(component, event, helper);
        //helper.createResumePreviewCmp(component, event);
        //helper.resetAccessSettings(component, event, helper); TOBE REMOVED
        //helper.addEditFormModal(component, event, helper, "Add");		
		//}
    },
	searchChange: function(component, event, helper) {
		component.set("v.isClientAccountBlank", true);  
    },


    addNewReference : function(component, event, helper) {
        
        var params = event.getParam('arguments');
		helper.clearReferenceObject(component);
        
        component.set( "v.referenceSearchValues" , params.searchParams );
		component.set("v.showSearchPopup",false);
		component.set("v.isSearchFlow",false);
        helper.startSpinner(component, event, helper);
        helper.createResumePreviewCmp(component, event);
        //helper.resetAccessSettings(component, event, helper); TOBE REMOVED
        helper.addEditFormModal(component, event, helper, "Add");

        
 	},
    editReference : function(component, event, helper) {
        
        component.set("v.showSearchPopup",false);
        component.set("v.isSearchFlow",false);
		
        var params = event.getParam('arguments');
        var ShouldUpdate = params.talentContactId;
        // This code snippet is used when user search for dupe, gets the result and select the dupe.
        // Variable names needs to change
        // Conddion /ShouldUpdate !== "parameter 1"/ is to check if we need to update the record or not
        if (ShouldUpdate !== "parameter 1") {
            var theRecordId = params.talentContactId;
 			var theSelectedContactId = params.selectedContactId;
			
            var theSelectedContactId1 = params.buttonClicked;
            var theSelectedContactId2 = params.talentrecommendationId;
            component.set('v.shouldSearchForDuplicate', false );
            component.set("v.useSelectedReference", true);
            component.set("v.recordId", theRecordId);
            component.set("v.selectedContactId", theSelectedContactId);
            component.set("v.recommendation", theSelectedContactId);
            
        }
        helper.startSpinner(component, event, helper);
        helper.addEditFormModal(component, event, helper, "Edit");
        
 	},
    
    callAddModal : function (component, event, helper) {
       
        helper.toggleClass(component,'backdropAddDocument','slds-backdrop--');
        helper.toggleClass(component,'modaldialogAddDocument','slds-fade-in-');
    },
	showMore: function(component, event, helper) {
		let toggle = {
			less: 'more',
			more: 'less'
		}
		component.set('v.workExpState', toggle[component.get('v.workExpState')])

	},

    saveReference : function(component, event, helper) {
        //component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));
        component.set("v.setValues", !component.get("v.setValues"));
        helper.startSpinner(component, event, helper);
        // Only for "Use Selected Contact"

        if(helper.validTalent(component) && component.get("v.useSelectedReference")){
            helper.userSelectedReference(component, event,helper, "Save");
        }
        else{
            helper.validateAndSaveReferences(component, event, helper, "Save");
        }
    },
	saveAndCompleteReference : function(component, event, helper) {
        component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));
        helper.startSpinner(component, event, helper);
        helper.validateAndSaveReferences(component, event, helper, "SaveAndComplete");
    },
    
    closeModal: function(component, event, helper) {
	    //component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));
        helper.closeModal(component, event, helper);  
        helper.clearAddEditReferenceForm(component, event, helper);
		helper.focusSource(component);
    },
	//S-83793: Function to handle closed/opened state of a section. Andy Satraitis
	toggleSection: function(cmp, event, helper) {
		var toggle = {
			'utility:chevronright': 'utility:chevrondown',
			'utility:chevrondown': 'utility:chevronright',
			show: 'hide',
			hide: 'show'
		}
		var acc = cmp.get("v.acc");
				
		var accordionId = event.currentTarget.id;
		var icon = acc[accordionId].icon;
		var className = acc[accordionId].className;		
				
		cmp.set(`v.acc[${accordionId}].icon`, toggle[icon]);
		cmp.set(`v.acc[${accordionId}].className`, toggle[className]);		

	},

    clearErrors: function(component, event, helper) {
        helper.clearDateErrors(component, event, helper);
    },

    disableEndDate: function(component, event, helper) {
        helper.disableEndDate(component, event, helper);
    },

    disableStillInPosition: function(component, event, helper) {
        helper.disableStillInPosition(component, event, helper);
    },
    
    handleRecordUpdated : function(cmp, event, helper) {
        helper.loadCmps(cmp);
    },    
    activityReference : function(component, event, helper) {
        helper.loadCmps(component);
    },
    actvitySaved:function(cmp,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The activity has been saved.",
            "type": "success"
        });
        toastEvent.fire();
        
        var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
        updateLMD.fire();
    },
    
    headerLoaded : function(cmp, event, helper){
        
        if (event.getParam("contactId") != cmp.get("v.recordId")) {
            // Do nothing if this is not the same contact
            return;
        }
        cmp.set("v.runningUser", event.getParam("runningUser"));
        cmp.set("v.runningUserCategory", event.getParam("runningUserCategory"));
        cmp.set("v.usrOwnership", event.getParam("usrOwnership"));
    }, 
    handleCreateEvent : function(cmp, event, helper){
        var componentName = event.getParam("componentName");
        var targetAttributeName = event.getParam("targetAttributeName");
        var params = event.getParam("params");
        helper.createResumePreviewCmp(cmp, event);
        //helper.createComponent(cmp, componentName, targetAttributeName,params);
    },
    
    refreshDocumentListAppEventHandler : function(cmp, event, helper) {
        var params = null;
        var recordId = cmp.get("v.recordId") ;
        var contactRecord = cmp.get("v.contactRecord");
        var timeToWait = helper.getTimeToWaitBeforeCallingResumingPreview(); 
        var runningUserCategory = cmp.get("v.runningUserCategory");
        var usrOwnership = cmp.get("v.usrOwnership");        
        params = {	recordId: recordId,
                  contactRecord : contactRecord,
                  runningUserCategory : runningUserCategory,
                  usrOwnership : usrOwnership,
                  refreshTimeOut :timeToWait }; 
        
        params["aura:id"]="previewleft";
        var cmpEvent1 = cmp.getEvent("createCmpEvent");
        cmpEvent1.setParams({
            "componentName" : 'c:C_TalentResumePreview',
            "params":params,
            "targetAttributeName": 'v.C_TalentResumePreview_leftpanel' });
        cmpEvent1.fire();
        
        var cmpEvent = cmp.getEvent("createCmpEvent");
        params["aura:id"] ="previewright";
        cmpEvent.setParams({
            "componentName" : 'c:C_TalentResumePreview',
            "params":params,
            "targetAttributeName": 'v.C_TalentResumePreview' });
        cmpEvent.fire();
        helper.openResumeCompareModal(cmp, event);
        
    },
    split_toggle : function(component, event, helper) {
	    // console.log('split invoked');
		component.set("v.splitState", !component.get("v.splitState"));
       
      let iconName=component.get("v.iconName");
      if(iconName == "right"){
         component.set("v.iconName","left"); 
      }else{
         component.set("v.iconName","right");
      }
	},
    /* 
     	Method Name : searchForExisting by Santosh C
        Purpose : This method is used to call dedupe services and should be called
                  only on "Save And Complete" button click as per Story.
        
    */    
    searchForExisting : function (component, event, helper) {

  //       helper.startSpinner(component, event, helper);
  //       // Only for "Use Selected Contact" 
  //       if(helper.validTalent(component) && component.get("v.useSelectedReference")){
  //           helper.userSelectedReference(component, event,helper, "saveAndComplete");
  //       }

  //       //For Search flow
  //       if (helper.validTalent(component) && component.get("v.shouldSearchForDuplicate") && !component.get("v.isSearchFlow")){
  //           // console.log("$$$$$$");
  //           // console.log( component.get("v.reference") );
  //               // console.log(component.find("projectDescription").get("v.value"));
  //           component.set('v.showSearchPopup', true );
  //           component.set('v.shouldSearchForDuplicate', false );
            
  //           helper.setMasterReferenceEventParameter(component, event,helper);
  //       }

  //       //For Non-search flow
  //       if (helper.validTalent(component) && component.get("v.isSearchFlow")){
  //           helper.startSpinner(component, event, helper);
  //           helper.validateAndSaveReferences(component, event, helper, "SaveAndComplete");
  //       }
		// if(!helper.validTalent(component)){
  //           helper.stopSpinner(component);
  //       }

        component.set("v.setValues", !component.get("v.setValues"));
        
        if( helper.validTalent(component) ){
            
            helper.startSpinner(component, event, helper);

            if( component.get("v.useSelectedReference")){
                helper.userSelectedReference(component, event,helper, "saveAndComplete");
            }else if( component.get("v.isSearchFlow")){ /*Non-search flow*/
                helper.validateAndSaveReferences(component, event, helper, "SaveAndComplete"); 
            }else if( component.get("v.shouldSearchForDuplicate") ){ /*Search For Existing*/
                component.set('v.showSearchPopup', true );
                component.set('v.shouldSearchForDuplicate', false );
                helper.setMasterReferenceEventParameter(component, event,helper);
            }
       }

        component.set('v.shouldSearchForDuplicate', true );

    },

    saveAndCompleteReferenceFromSearch : function(component, event, helper) {
        helper.addEditFormModal(component, event, helper, "Edit");
    },
    logValue: function(component, event, helper) {
    },
    handlerClick: function (component, event, helper) {
        event.stopPropagation();
        // console.log('click should be ignored')
    },

	focusBtnOnTabshiftTab: function(component, event, helper) { 	
	  /* Monika - S-200682 - focus close button on next tab of Save button */
		if ( event.keyCode == 9 ) {	
			helper.focusBtn(component,"modalCloseBtn");
		}	
		event.preventDefault();	

   		/*Monika - S-200682 - As the focus is getting trapped between save and close buttons, set focus on Save and Complete button on shift+tab from save*/
		if (event.shiftKey && event.keyCode == 9) {
			helper.focusBtn(component,"saveAndComplete");	
		}
	},

	/* Monika - S-200682 - getting focus on save button on reverse tab from close button  */	
	focusBackSaveFromClose : function(component,event,helper){
		if (event.shiftKey && event.keyCode == 9) {
			helper.focusBtn(component,"save");	
		}
	},

	focusCloseBtnOnModalOpen : function(component,event,helper){
		helper.focusBtn(component,"modalCloseBtn"); 
	}
})