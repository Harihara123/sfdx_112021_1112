({
    
    /****** Main Methods ******/
    addEditFormModal: function(component, event, helper, mode){
        
        var theVal = component.get("v.addModalTrigger")
        component.set("v.addModalTrigger", !theVal);
        component.set("v.mode", mode);
        var recommendationPicklist = "Salutation,Quality__c,Workload__c,Relationship__c,Relationship_Duration__c,Interaction_Frequency__c,Talent_Job_Duration__c";
        recommendationPicklist = recommendationPicklist + ',Rehire_Status__c,Competence__c,Leadership__c,Collaboration__c,Trustworthy__c,Likeable__c,Outlook__c,Team_Player__c,Professionalism__c,Reference_Type__c';
        
        if(component.get("v.mode") === "Add" || component.get("v.recommendation") === null || component.get("v.recommendation") === undefined)
            this.initWrapper(component);
        else{
            this.searchFlowInitWrapper(component,event, helper);   
        }  
		var self = this;
        setTimeout(()=>{
            self.getAllPicklist(component, event, helper, recommendationPicklist, 'salutation', mode);
        }, 200);
    },
    handleMouseDown: function(component, event, helper) {
        let target = document.getElementById('resumeWrap');
        let xPos1 = event.clientX;
        let width = target.clientWidth;
        let  move = (e) => {
            let xPos2 = e.clientX;
            let deltaXPos = xPos2 - xPos1;
            target.style.width = (width + +deltaXPos) + 'px';
            console.log(deltaXPos)

            //`calc(40vw + ${deltaXPos}px)`
        };
        let removeListeners = (e) => window.removeEventListener('mousemove', move)
        window.addEventListener('mousemove', move)
        window.addEventListener('mouseup', removeListeners)

        
          
    },
    createResumePreviewCmp : function(cmp,evt){
        var params = {recordId: cmp.get("v.recordId"),
                    showUploadButton:false,
                    showDownloadButton:true,
                    contactRecord :cmp.get("v.contactRecord")};
    $A.createComponent(
            "c:C_TalentResumePreview",
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set("v.C_TalentResumePreview", newComponent);
                }
                else if (status === "INCOMPLETE") {
                    
                }
                else if (status === "ERROR") {
                    
                }
            }
        );
        
    
    },
    

    handleError : function(response){
        
       // Use error callback if available
        if (typeof errCallback !== "undefined") {
            errCallback.call(this, response.getReturnValue());
            // return;
        }
        // Fall back to generic error handler
        var errors = response.getError();
        if (errors) {
            
            if (errors[0] && errors[0].message) {
                this.showError(errors[0].message);
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
            }else{
                this.showError('An unexpected error has occured. Please try again!');
            }
        } else {
            this.showError(errors[0].message);
            this.stopSpinner(component, event, helper);  

            //throw new Error("Unknown Error");
        }
    },            

    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

    
    initWrapper : function(component){
        
        var action = component.get('c.initWrapper');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.reference', response.getReturnValue());
                // Following code is added to set lookup values to null and avoid populating value from existing record
              
                component.get("v.reference").contact.Title = "";
                component.get("v.reference").recommendation.Talent_Job_Title__c = "";
                component.get("v.reference").recommendation.Opportunity__c = "";
                component.get("v.reference").recommendation.Organization_Name__c = "";
            }else if (state === "ERROR") {
                this.handleError(response);
            } 
        });
        $A.enqueueAction(action);
        
    },
    searchFlowInitWrapper : function(component,event, helper){
        
        var params = {recordId: component.get("v.recommendation")};
        var action = component.get('c.generateReferenceType');
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.reference', response.getReturnValue());
                var reference = response.getReturnValue(); 
                component.set("v.reference", reference );
                if(reference.disableContactField){
                   component.set("v.showMessage",true);  
                }
                // Following code is added to set lookup values to null and avoid populating value from existing record
                if(component.get("v.reference").contact.Title === "" || component.get("v.reference").contact.Title === undefined)
                    component.get("v.reference").contact.Title = "";
                component.get("v.reference").recommendation.Talent_Job_Title__c = "";
                component.get("v.reference").recommendation.Opportunity__c = "";
                component.get("v.reference").recommendation.Organization_Name__c = "";
                //TOBE REMOVED
                //  helper.editModeAccessSetter(component, event, helper, reference.contact, component.get("v.mode"));
            }else if (state === "ERROR") {
                this.handleError(response);
            }
        });
        $A.enqueueAction(action);

    },
    getAllPicklist: function(component, event, helper, fieldName, elementId, mode) {

        //this.clearAddEditReferenceForm(component, event, helper);
        // component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));


        var serverMethod = 'c.getAllPicklistValues';
        var params = {
            "objectStr": "Contact, Talent_Recommendation__c",
            "fld": fieldName
        };
        var action = component.get(serverMethod);
        action.setParams(params);

        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                var picklist = allValues["Contact.Solution"];               
                var idName = "salutation";
                
                this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Quality__c"];
                idName = "qualityWork";
                component.set('v.picklistValues.quality', picklist);
                // this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Workload__c"];
                idName = "qualityWorkload";
                component.set('v.picklistValues.qualityWorkload', picklist);
                // this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Relationship__c"];
                idName = "referenceRelationship";
                this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Relationship_Duration__c"];
                idName = "relationshipDuration";
                this.setPicklistValues(component, picklist, idName);
                picklist = allValues["Recommendation.Interaction_Frequency__c"];
                idName = "interactionFrequency";
                this.setPicklistValues(component, picklist, idName);
                                
                
                picklist = allValues["Recommendation.Rehire_Status__c"];
                idName = "rehire";
                this.setPicklistValues(component, picklist, idName);
               
                picklist = allValues["Recommendation.Competence__c"];
                idName = "ability";
                component.set('v.picklistValues.ability', picklist);
                // this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Leadership__c"];
                idName = "initiative";
                component.set('v.picklistValues.initiative', picklist);
                // this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Collaboration__c"];
                idName = "Communication";
                component.set('v.picklistValues.Communication', picklist);
                // this.setPicklistValues(component, picklist, idName);
                
                picklist = allValues["Recommendation.Trustworthy__c"];
                idName = "Reliability";
                component.set('v.picklistValues.Reliability', picklist);
                // this.setPicklistValues(component, picklist, idName);

                picklist = allValues["Recommendation.Reference_Type__c"];
                idName = "type";
                this.setPicklistValues(component, picklist, idName);

                if(mode == "Add"){                  
                    this.stopSpinner(component, event, helper);
                    this.clearAddEditReferenceForm(component, event, helper);
                    this.clearErrors(component);
                } else {
                    this.clearErrors(component);
                    if (component.get("v.row.Id")) {
                        // row ID is null when in the Edit flow. Don't try to load the reference.
                        this.loadReference(component, event, helper, component.get("v.row.Id"));
                    } else {
                        this.stopSpinner(component, event, helper);
                        this.disableEndDate(component, event, helper);
                        this.disableStillInPosition(component, event, helper);
                    }
                }
                
            }else if (state === "ERROR") {
                this.handleError(response);
            } 

            

        });
        $A.enqueueAction(action);
    },


    clearDateErrors: function(component, event, helper) {
        component.find("start_Date").set("v.errors", null);
        component.find("End_Date").set("v.errors", null);
    },
        
    disableEndDate: function(component, event, helper) {
        if (component.get("v.reference").recommendation.Talent_Current_Job__c) {
            component.set("v.isEndDateDisabled", "true");  
            component.find("End_Date").set("v.value", "");            
        } else {
            component.set("v.isEndDateDisabled", "false");  
        }
        this.clearDateErrors(component, event, helper);
    },

    disableStillInPosition: function(component, event, helper) {
        if (component.get("v.reference").recommendation.End_Date__c) {
            component.set("v.isStillInPositionDisabled", "true");
        } else {
            component.set("v.isStillInPositionDisabled", "false");
        }  
        this.clearDateErrors(component, event, helper);
    },


    setPicklistValues : function(component, picklist, fildName){
        var opts = [];
        var shouldDefault = false;
        if(fildName == "referenceRelationship" || fildName === 'type')
            shouldDefault = true;
        if (shouldDefault == false && picklist != undefined && picklist.length > 0) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
        }
        if(fildName == "referenceRelationship"){
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            opts.push({
                class: "optionClass",
                label: "Manager",
                value: "Manager"
            });
            
        }
        if(fildName == "type"){
            opts.push({
                class: "optionClass",
                label: "Standard",
                value: "Standard"
            });
            opts.push({
                class: "optionClass",
                label: "Exit",
                value: "Exit"
            });
            opts.push({
                class: "optionClass",
                label: "Peer",
                value: "Peer"
            });
        }
        if(fildName == "relationshipDuration"){
            opts.push({
                class: "optionClass",
                label: "1-3 years",
                value: "1-3 years"
            });
            opts.push({
                class: "optionClass",
                label: "4-5 years",
                value: "4-5 years"
            });
            opts.push({
                class: "optionClass",
                label: "5-10 years",
                value: "5-10 years"
            });
            opts.push({
                class: "optionClass",
                label: "10-15 years",
                value: "10-15 years"
            });
            opts.push({
                class: "optionClass",
                label: "15+ years",
                value: "15+ years"
            });
        }   
        
        for (var i = 0; i < picklist.length; i++) {
            if(picklist[i] != "Manager" && fildName != "relationshipDuration" && fildName != "type"){
                opts.push({
                    class: "optionClass",
                    label: picklist[i],
                    value: picklist[i]
                });
            }    
        }
        component.find(fildName).set("v.options", opts);
    },
    
        
    validateAndSaveReferences : function(component, event, helper, action) {
        var mode = component.get("v.mode"); 
        
        if (this.validTalent(component)){
            this.clearErrors(component);
            this.saveReference(component, event, helper, action);
            component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));
        } else {
            this.stopSpinner(component, event, helper);  
        }
    },
    
    saveReference : function(component, event, helper, buttonClicked){

        var referenceWrap = component.get("v.reference");
        if(component.get("v.isClientAccountBlank"))
            referenceWrap.recommendation.Client_Account__c = "";
        else
            referenceWrap.recommendation.Client_Account__c = component.find("companyNameShow").get("v.value");
        component.set("v.isClientAccountBlank", false); // Reset to false
       referenceWrap.recommendation.Opportunity__c = component.find("opportunityId").get("v.value");

        if(referenceWrap.recommendation.Opportunity__c == "" ){
            referenceWrap.recommendation.Opportunity__c = null;
            referenceWrap.recommendation.Opportunity__r = null;
        }

        
        if(referenceWrap.recommendation.Relationship__c == undefined)   
            referenceWrap.recommendation.Relationship__c = "Manager";
        if(referenceWrap.recommendation.Relationship_Duration__c == undefined)  
            referenceWrap.recommendation.Relationship_Duration__c = "<1 year";
        
        var recordId = component.get("v.recordId");
        var shouldInsert = true;
        if(component.get("v.mode") != "Add")
            shouldInsert = false;
        var serverMethod = 'c.saveTalentReference'; 
        var params = {
            "contact": referenceWrap.contact,
            "Recommendation" : referenceWrap.recommendation,
            "talentContactId" : recordId,
            "shouldInsert" : shouldInsert,
            "mode" : component.get("v.mode"),
            "action" : buttonClicked
        };

        var action = component.get(serverMethod);
        action.setParams(params);
     
        action.setCallback(this,function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
                var reference = response.getReturnValue();
                /*
                var parentComponent = component.get("v.parent");
                if(component.get("v.recommendation") === null) // temp logic remove it later    
                    parentComponent.reloadDatatable();
                */
                if(buttonClicked == "SaveAndComplete"){
                    var res = "Refresh Activity";
                    var evt = $A.get("e.c:E_TalentActivityUpdateEvent");
                    evt.setParams({ "result": event.getSource().getLocalId()});
                    evt.fire(); 
                    var evt2 = $A.get("e.c:E_TalentActivityReload");
                    evt2.setParams({ "result": event.getSource().getLocalId()});
                    evt2.fire(); 
                }
                
                this.showToast("Record has been saved", "success", "Success");
                this.closeModal(component, event, helper);
                this.clearAddEditReferenceForm(component, event, helper);
                this.relaodDatatableEvent(component, event, helper);
                // this.stopSpinner( component, event, helper );
            } else if (state === "ERROR") {
                this.handleError(response);
                this.closeModal(component, event, helper);

            }

            this.stopSpinner(component, event, helper);
        });

        $A.enqueueAction(action);
        //this.hideSearchExistingForms(component, event,helper);
    },
    loadReference : function(component, event, helper, referenceId){
        this.clearAddEditReferenceForm(component, event, helper);
        component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));

        var referenceWrap = component.get("v.reference");

        var serverMethod = 'c.getTalentReference';
        var params = {
            "referenceId": referenceId
        };

        var action = component.get(serverMethod);
        action.setParams(params);

        action.setCallback(this,function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var reference = response.getReturnValue();
                if(reference.recommendation.Client_Account__c != null){
                    reference.recommendation.Client_Account__c=reference.recommendation.Client_Account__r.Id;
                }

                if(reference.recommendation.Opportunity__c != null)
                    reference.recommendation.Opportunity__c = reference.recommendation.Opportunity__r.Name;
                if(reference.disableContactField){
                    component.set("v.showMessage",true);
                }

                component.set("v.reference",reference);

            } else if (state === "ERROR") {
                this.handleError(response);
            }

            this.stopSpinner(component, event, helper);
            this.disableEndDate(component, event, helper);
            this.disableStillInPosition(component, event, helper);

        });
        $A.enqueueAction(action);
    },


    /****** Field Validation Methods ******/
    validTalent: function(component) {

        var validTalent = true;
        validTalent = this.validateFields(component, validTalent, "firstName", $A.get("$Label.c.ATS_FIRST_NAME"));
        validTalent = this.validateFields(component, validTalent, "lastName", $A.get("$Label.c.ATS_LAST_NAME"));
        validTalent = this.validateFields(component, validTalent, "companyName", $A.get("$Label.c.ATS_COMPANY_NAME"));
        validTalent = this.validatePhoneEmailLinkedIn (component, validTalent);
        validTalent = this.validateDateFields (component, component.find("start_Date"), component.find("End_Date"), validTalent);
        return validTalent;
    },
    validateFields : function(component, validTalent, componentId, fieldName) {
        var currentFieldName = component.find(componentId);
        var validateField = currentFieldName.get("v.value");
        if (!validateField || validateField === "") {
            validTalent = false;
            var msg = $A.get("$Label.c.ATS_Please_enter_a_valid ")+fieldName;
            currentFieldName.set("v.errors", [{message: "Please enter a valid " + fieldName + " ."}]);
            currentFieldName.set("v.errors", [{message: $A.get("$Label.c.ATS_Please_enter_a_valid") + fieldName + " ."}]);
            currentFieldName.set("v.isError",true);
            $A.util.addClass(currentFieldName,"slds-has-error show-error-message");
			this.showToast(msg, "error", "Error");
			this.focusBtn(component,componentId);
        } else {
            currentFieldName.set("v.errors", []);
            currentFieldName.set("v.isError", false);
            $A.util.removeClass(currentFieldName,"slds-has-error show-error-message");
        }
        return validTalent;
    }, 
    
    validatePhoneEmailLinkedIn : function(component, validTalent) {
        var mobilePhoneField = component.find("mobilePhone");
        var mobilePhone = mobilePhoneField.get("v.value");
        if(!this.validateMobilePhone(mobilePhone)) {
           mobilePhoneField.set("v.errors", [{message: "Please enter valid Mobile Phone."}]);
           mobilePhoneField.set("v.isError",true);
           validTalent = false;
		   this.focusBtn(component,"mobilePhone");
        }

        var homePhoneField = component.find("homePhone");
        var homePhone = homePhoneField.get("v.value");
        if(!this.validateMobilePhone(homePhone)) {
           homePhoneField.set("v.errors", [{message: "Please enter valid Home Phone."}]);
           homePhoneField.set("v.isError",true);
           validTalent = false;
		   this.focusBtn(component,"homePhone");
        }

        var workPhoneField = component.find("phone");
        var workPhone = workPhoneField.get("v.value");
        var workPhoneValMessage = "Please enter a valid Work Phone.";

        if(!this.validateMobilePhone(workPhone)) {
           workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
           workPhoneField.set("v.isError",true);
           validTalent = false;
		   this.focusBtn(component,"phone");
        }
        var workExtensionField = component.find("workExtension");
        /* if (workExtensionField){
            var workExtension = workExtensionField.get("v.value");  farid replace 10062018 */
        var workExtension = workExtensionField.get("v.value");
        if (workExtension && workExtension.length > 0){ 
            if(workExtension != '' && workPhone == '' || workExtension != '' && workPhone == null ){
                workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
                workPhoneField.set("v.isError",true);
                validTalent = false;
				this.focusBtn(component,"workExtension");
                }
            
            else {
                workExtensionField.set("v.errors", null);
                workExtensionField.set("v.isError",false);
            }
        }
        
        var emailField = component.find("email");
        var email = emailField.get("v.value");
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: "Please enter valid Email."}]);
            emailField.set("v.isError",true);
            validTalent = false;
			this.focusBtn(component,"email");
        }

       if (!mobilePhone && !homePhone && !workPhone && !email) {
                validTalent = false;
                this.showToast($A.get("$Label.c.ADD_NEW_TALENT_CONTACTMETHOD"), "Error", "Error");
				this.focusBtn(component,"email");
        } 
        return validTalent;
    },       

     validateMobilePhone : function(mobilePhone) {
        if(mobilePhone && mobilePhone.length > 0) {
            var mp = mobilePhone.replace(/\s/g,'');
            if(mp.length >= 10 && mp.length <= 17) {
                
                var regex = new RegExp("^[0-9-.+()/]+$");
                var isValid = mp.match(regex);
                
                if(isValid != null) {
                    return true; 
                }
                else{
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true; 
    },   
     
    validateEmail : function(emailID){
        if(emailID && emailID.trim().length > 0) {
            
            var isValidEmail = emailID.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

            if(isValidEmail != null) {
                return true;
            }
            else {
                return false;
            } 

        }
        return true;
    },
    validateDateFields : function(component, startNewDate, endNewDate, validTalent){
        
        var sDateField = component.find("start_Date");
        var eDateField = component.find("End_Date");
              
        var stillInPositionExists = null;
       
        if(!$A.util.isUndefined( component.get("v.reference.recommendation")  ) || !$A.util.isEmpty( component.get("v.reference.recommendation")  ) ){
            stillInPositionExists = component.get("v.reference.recommendation.Talent_Current_Job__c");
        }

        var sDateExists = sDateField.get('v.value');
        var eDateExists = eDateField.get('v.value');
        var sDateStr, eDateStr;

        if (sDateExists && eDateExists) {
            var sDate = sDateField.get('v.value');
            var eDate = eDateField.get('v.value');
            
            if ((eDate!=""&&sDate!="") && (eDate < sDate)) {
                validTalent = false;
                eDateField.set("v.errors", [{message: "End date should be after start date!"}]);
                return;
            } else {
                eDateField.set("v.errors", null);
            }
        }
        
        if (eDateExists && !sDateExists) {
            validTalent = false;
            sDateField.set("v.errors", [{message: "Start date should not be blank!"}]);
            this.showToast("Start date should not be blank!", "Error", "Error");
			this.focusBtn(component,"start_Date");
            return;
        } else {
            sDateField.set("v.errors", null);
        }

        if (stillInPositionExists && !sDateExists) {
            validTalent = false;
            sDateField.set("v.errors", [{message: "Start date should not be blank!"}]);
            this.showToast("Start date should not be blank!", "Error", "Error");
			this.focusBtn(component,"start_Date");
            return;
        } else {
            sDateField.set("v.errors", null);
        }

        if (sDateExists && !eDateExists && !stillInPositionExists) {
            validTalent = false;
            eDateField.set("v.errors", [{message: "End date should be added or Please select Still in position!"}]);
            this.showToast("End date should be added or Please select Still in position!", "Error", "Error");
			this.focusBtn(component,"End_Date");
            return; 
        } else {
            eDateField.set("v.errors", null);
        }

        return validTalent;
    },
    /****** Miscellaneous Methods ******/
    clearAddEditReferenceForm : function(component, event, helper) {
        //Personal info
        component.set("v.showMessage",false);
        if(component.find("salutation") !== undefined){
            component.find("salutation").set("v.value", "");
            component.find("firstName").set("v.value", "");
            component.find("middleName").set("v.value", "");
            component.find("lastName").set("v.value", "");
            component.find("suffix").set("v.value", "");
            component.get("v.reference").contact.Title = "";
            
            //Method of contact
            component.find("phone").set("v.value", "");
            component.find("workExtension").set("v.value", "");
            component.find("mobilePhone").set("v.value", "");
            component.find("homePhone").set("v.value", "");
            component.find("email").set("v.value", "");
            
            // Additional info
            component.find("companyLookup").set("v.value", "");
            component.find("companyName").set("v.value", "");
            component.get("v.reference").recommendation.Opportunity__c = ""
            
            // Relation to Candidate
            component.find("start_Date").set("v.value", "");
            component.find("End_Date").set("v.value", "");
            component.find("interactionFrequency").set("v.value", "");
            component.find("referenceRelationship").set("v.value", "");
            component.get("v.reference").recommendation.Talent_Job_Title__c = "";
            component.get("v.reference").recommendation.Opportunity__c = "";
            component.find("rehire").set("v.value", "");
               
            // Quality of work  
            component.find("jobDuties").set("v.value", "");
            // component.find("qualityWorkload").set("v.value", "");
            component.find("projectDescription").set("v.value", "");
            // component.find("qualityWork").set("v.value", "");
            component.find("noTechSkills").set("v.value", "");
            component.find("culturalEnvironment").set("v.value", "");
            component.find("qualityWorkText").set("v.value", "");
            component.find("qualityWorkloadText").set("v.value", "");
            // component.find("ability").set("v.value", "");
            // component.find("initiative").set("v.value", "");
            component.find("abilityText").set("v.value", "");
            component.find("InitiativeText").set("v.value", "");
            // component.find("Communication").set("v.value", "");
            // component.find("Reliability").set("v.value", "");
            component.find("CommunicationText").set("v.value", "");
            component.find("ReliabilityText").set("v.value", "");
            component.find("strengths").set("v.value", "");
            component.find("growthOpportunity").set("v.value", "");
            component.find("additionalInformation").set("v.value", ""); 
            component.find("referenceInformation").set("v.value", ""); 
    
            component.set("v.isStillInPositionDisabled", "false");
            component.set("v.isEndDateDisabled", "false");
            // component.find("qualityWorkload").set("v.errors", null);
            if(  component.get("v.referenceSearchValues" ) != undefined ){
                component.set( "v.reference" , {"contact" : component.get("v.referenceSearchValues" ) } );
                component.set('v.isSearchFlow', true ); // To avoid recusive calling to deduple service
            }
        }    
//    	$A.get('e.force:refreshView').fire();
        
    },
    clearErrors : function (component) {
        //Personal info
        component.find("salutation").set("v.errors", null);
        component.find("firstName").set("v.errors", null);
        component.find("middleName").set("v.errors", null);
        component.find("lastName").set("v.errors", null);
        component.find("suffix").set("v.errors", null);
        component.find("talentJobTitle").set("v.value", "");
        
        //Method of contact
        component.find("phone").set("v.errors", null);
        component.find("mobilePhone").set("v.errors", null);
        component.find("homePhone").set("v.errors", null);
        component.find("email").set("v.errors", null);
        
        // Additional info
        component.find("companyLookup").set("v.errors", null);
        component.find("companyName").set("v.errors", null);
        component.find("taskOppLookup").set("v.errors", null);
        
        // Relation to Candidate
        component.find("start_Date").set("v.errors", null);
        component.find("End_Date").set("v.errors", null);
        component.find("interactionFrequency").set("v.errors", null);
        component.find("referenceRelationship").set("v.errors", null);
        component.find("clientJobTitle").set("v.value", "");
        component.find("rehire").set("v.errors", null);
           
        // Quality of work  
        component.find("jobDuties").set("v.errors", null);
        //component.find("qualityWorkload").set("v.errors", null);
        component.find("projectDescription").set("v.errors", null);
        //component.find("qualityWork").set("v.errors", null);
        component.find("noTechSkills").set("v.errors", null);
        component.find("culturalEnvironment").set("v.errors", null);
        component.find("qualityWorkText").set("v.errors", null);
        component.find("qualityWorkloadText").set("v.errors", null);
        //component.find("ability").set("v.errors", null);
        //component.find("initiative").set("v.errors", null);
        component.find("abilityText").set("v.errors", null);
        component.find("InitiativeText").set("v.errors", null);
        //component.find("Communication").set("v.errors", null);
        //component.find("Reliability").set("v.errors", null);
        component.find("CommunicationText").set("v.errors", null);
        component.find("ReliabilityText").set("v.errors", null);
        component.find("strengths").set("v.errors", null);
        component.find("growthOpportunity").set("v.errors", null);
        component.find("additionalInformation").set("v.errors", null);
    },

    startSpinner: function(component, event, helper){
        component.set("v.Spinner", true);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');   
    },
    stopSpinner: function(component, event, helper){
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');
        component.set("v.Spinner", false);   
    },
    closeModal: function(component, event, helper) {        
        component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));
        this.initWrapper(component); // Clearing Wrapper Object
        component.set("v.showMessage",false);
        this.toggleClassInverse(component,'backdropAddDocument','slds-backdrop--');
        this.toggleClassInverse(component,'modaldialogAddDocument','slds-fade-in-');
        var docModalCloseEvt = component.getEvent("documentModalCloseEvent");
        console.log('closing');
        component.set('v.modalClosed', true);
        
        component.set('v.modalClosed', true);
        
        docModalCloseEvt.fire();
    },   
    showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    
    createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    
                }
                    else if (status === "ERROR") {
                        
                    }
            }
        );
        
    },
    
    loadCmps : function(cmp) {
        var params = null;
        var recordId = cmp.get("v.recordId") ;
        var params = {recordId: recordId };
        var contactRecord = cmp.get("v.contactRecord");
        //var conrecordJSON = JSON.stringify(contactRecord);
        
        var runningUserCategory = cmp.get("v.runningUserCategory");
        var usrOwnership = cmp.get("v.usrOwnership");
        var params = {recordId: recordId
                      , contactRecord :contactRecord
                      , runningUserCategory : runningUserCategory
                      , usrOwnership : usrOwnership};        
        //Sandeep : needed two instances of C_TalentResumePreview component.
        var cmps = [
            'C_TalentResumePreview',
            'C_TalentResumePreview'
        ];
        var instanceCount_resumePreview=0;
        var usr = cmp.get("v.runningUser");
        
        for (var i=0;i<cmps.length;i++) {
            
            var cmpEvent = cmp.getEvent("createCmpEvent");
            /* for Talent Applications load TalentSubmittals card */
            if (cmps[i] === "C_TalentApplications") {
               
            } 
            else if(cmps[i] === "C_TalentResumePreview"){
                    //Sandeep: need two instances of Resume preview , add first one to C_TalentResumePreview attribute,
                    //2nd one to C_TalentResumePreview_leftpanel attribute
                    //Sandeep doc refresh fix,Aura Id given for resume preview instances.
                    let params = {
                        "recordId": recordId, 
                        "contactRecord": cmp.get("v.contactRecord")
                    };
                    
                    instanceCount_resumePreview ++;
                    let attributeName = "C_TalentResumePreview_leftpanel";
                    params["aura:id"]="previewleft";
                    if(instanceCount_resumePreview == 2){
                        attributeName = "C_TalentResumePreview";
                        params["aura:id"]="previewright";
                    }
                    cmpEvent.setParams({
                        "componentName": 'c:' + cmps[i],
                        "params": params,
                        "targetAttributeName": 'v.' + attributeName 
                    });
                    
                }
                    else {
                        cmpEvent.setParams({
                            "componentName": 'c:' + cmps[i],
                            "params": params,
                            "targetAttributeName": 'v.' + cmps[i] 
                        });
                    }
            cmpEvent.fire();
        } 
    } , 
    
    getTimeToWaitBeforeCallingResumingPreview : function() {
        
        var defaultInt = 9000; 
        
        var timeToWaitNum; 
        try {
            timeToWaitNum =  parseInt($A.get("$Label.c.ATS_TIME_TO_WAIT_RESUME_REFRESH"));
        } catch (error) {
            
            timeToWaitNum  = defaultInt; 
        }
        
        if (isNaN(timeToWaitNum)) {
            timeToWaitNum = defaultInt; 
        }
        
        return timeToWaitNum; 
        
    },
    openResumeCompareModal : function(component,event) {
        
        if(event.getParam('isUploadCompare') == true) {
            var uploadedDocumentID = event.getParam('uploadedDocID');
            var compareRecord = event.getParam('compareRecord');
            var talentId = component.get('v.talentId');
            var modalBody;
            var modalHeader;
            var modalFooter;
            var conrecord = component.get("v.contactRecord");
            
            $A.createComponents([
                ["c:C_TalentResumeCompareAndUpdateModal_Body_Contact",{"items":JSON.parse(compareRecord)}],
                ["c:C_TalentResumeCompareAndUpdateModal_Header",{"fName":component.get("v.contactRecord.fields.Name.value"),"lName":conrecord.LastName}],
                ["c:C_TalentResumeCompareAndUpdateModal_Footer",{"items":JSON.parse(compareRecord)}]
            ],
                                function(components, status){
                                    if (status === "SUCCESS") {
                                        modalBody = components[0];
                                        modalHeader = components[1];
                                        modalFooter = components[2]; 
                                        component.find('overlayLib').showCustomModal({
                                            header: modalHeader,
                                            body: modalBody,
                                            footer:modalFooter,
                                            cssClass: "ResumeParseModal",
                                            showCloseButton: true,
                                            closeCallback: function() {
                                                
                                            }
                                        });
                                    }
                                }
                               );//Create component end
        }
        
    },
    
    // Looks like a redudent method .. Should be removed.
  //   callDedupeService : function(component){
        
  //       var action = component.get('c.callDedupeService');
        // action.setParams({
  //           "firstName": "santosh",//component.get( "v.firstNameFieldValue"),
  //           "lastName" : "patil",//component.get( "v.lastNameFieldValue"),
  //           "email"    : "sam@gmail.com",//component.get( "v.emailFieldValue"),
  //           "phone"    : "7894561331" //component.get( "v.phoneFieldValue")
  //       });

  //       action.setCallback(this, $A.getCallback(function (response) {
  //           var state = response.getState();
  //           if (state === "SUCCESS") {
  //           	component.set("v.hideTable",false);
  //               var responseString = response.getReturnValue();
  //               var parsedSearches = JSON.parse(responseString);
                
  //               if(parsedSearches[0].hasOwnProperty('Message')){
  //                   component.set("v.searchTableData", null );
  //                   component.set("v.noResults", true );
  //               }else{
  //                   component.set("v.searchTableData",parsedSearches);  
  //                   component.set("v.noResults", false );  
  //               }

                
  //           }else if (state === "ERROR") {
  //               this.handleError(response);
  //           }

  //       }));
  //       $A.enqueueAction(action);
  //   },
    /*
        TOBE REMOVED
    
    editModeAccessSetter : function(component, event, helper, contactRecord, mode) {
        
        var ref = component.get('v.reference');
        if(ref.rwsRecord){
            contactRecord = component.get('v.reference').contact;
            // This can be optimize as loop is repeating
            for ( var key in contactRecord) {
                if(key !== "Id" && key !== undefined && key !== "AccountId" && key !== "RecordTypeId" && key !== "RecordType"){
                    helper.convertToComponentId(component, key, contactRecord[key]);
                }    
            }
        }else if(contactRecord.RecordType.Name !== "Reference"){
            for ( var key in contactRecord) {
                if(key !== "Id" && key !== "AccountId" && key !== "RecordTypeId" && key !== "RecordType"){
                    helper.convertToComponentId(component, key, contactRecord[key]);
                }    
            }    
        }
    }, 
    
    convertToComponentId : function(component, fieldName, theFieldVal){
        
        component.set('v.showMessage',true);
        if(theFieldVal !== ""){
            console.log("### Now Converting To ComponentId ###");
            fieldName = fieldName.charAt(0).toLowerCase() + fieldName.slice(1);;
            if(fieldName === "workExtension__c")
                fieldName = "workExtension";
            if(fieldName !== "title")
                component.find(fieldName).set("v.disabled", true);
            if(fieldName === "title")
                component.set('v.disableField',true);
        }
    },
    
    
    editModeAccessSetter : function(component, event, helper, reference, mode) {
        var theContactMap = [];
        var theContactField = ['Suffix','MobilePhone','Phone','HomePhone','WorkExtension__c','Salutation','Email','LastName','MiddleName',"FirstName"];
        var theContactComp = ["firstName",'middleName','lastName','email','salutation','workExtension','homePhone','workPhone','mobilePhone','suffix'];
        var theContact = reference.contact;
        
        var theReferenceMap = [];
        var theReferenceField = ['Relationship__c','Relationship_Duration__c','End_Date__c','Start_Date__c','Interaction_Frequency__c','Talent_Current_Job__c','Talent_Job_Title__c','Talent_Job_Duties__c','Opportunity__c','Client_Account__r.Name','Rehire_Status__c','Quality__c','Workload__c','Quality_Description__c','Workload_Description__c','Competence__c','Competence_Description__c','Leadership__c','Leadership_Description__c','Collaboration__c','Collaboration_Description__c','Trustworthy__c','Trustworthy_Description__c','Organization_Name__c','Recommendation__c','Growth_Opportunities__c','Project_Description__c','Non_technical_skills__c','Cultural_Environment__c','Additional_Reference_Notes__c','Reference_Type__c'];
        var theReferenceComp = ['type','additionalInformation','referenceInformation','culturalEnvironment','noTechSkills','projectDescription','growthOpportunity','strengths','companyName','ReliabilityText','Reliability','CommunicationText','Communication','InitiativeText','initiative','abilityText','ability','qualityWorkloadText','qualityWorkText','qualityWorkload','qualityWork','rehire','taskOppLookup','jobDuties','clientJobTitle','StillInPosition','interactionFrequency','start_Date','End_Date','companyLookup','relationshipDuration','referenceRelationship'];
        var theReference = reference.recommendation;
        
        helper.defineAccessMode(component, event, helper, theContact, theContactMap, theContactField, theContactComp, mode);
        helper.defineAccessMode(component, event, helper, theReference, theReferenceMap, theReferenceField, theReferenceComp, mode);
        if(component.find("talentJobTitle").get("v.value")){
          component.find("disableField").set("true"); 
        }
    },
    defineAccessMode :  function(component, event, helper, theReference, theReferenceMap, theReferenceField, theReferenceComp,mode){
        for(var i = 0; i < theReferenceField.length; i++){
              theReferenceMap.push({value:theReferenceComp[i], key:theReferenceField[i]});
        }
        
        for ( var key in theReferenceField) {
            var theVal = theReference[theReferenceField[key]];
            //Personal info
            if(theReferenceField[key] !== "Id" && theReferenceField[key] !== "AccountId" && theReferenceField[key] !== "Additional_Reference_Notes__c" && theReferenceField[key] !== "Reference_Type__c" && theReferenceField[key] !== "Client_Account__r" && theReferenceField[key] !== "Client_Account__c" && theReferenceField[key] !== "Title"){
                var theComp = theReferenceMap.pop(key).value;
                if(theVal && mode === "Edit"){
                    component.find(theComp).set("v.disabled", true);
                }else{
                    component.find(theComp).set("v.disabled", false);
                }
                
            }
        }
        document.getElementById('txt1').contentEditable = false;
    },
    
    resetAccessSettings : function(component, event, helper){
        //Personal info
        component.find("salutation").set("v.disabled", false);
        component.find("firstName").set("v.disabled", false);
        component.find("middleName").set("v.disabled", false);
        component.find("lastName").set("v.disabled", false);
        component.find("suffix").set("v.disabled", false);
        component.set('v.disableField',false);
        
        //Method of contact
        component.find("phone").set("v.disabled", false);
        component.find("mobilePhone").set("v.disabled", false);
        component.find("homePhone").set("v.disabled", false);
        component.find("workExtension").set("v.disabled", false);
        component.find("email").set("v.disabled", false);
        component.set('v.showMessage',false);
    },
    
    */
    
    /* 
        Method Name : userSelectedReference by Santosh C
        Purpose : insert only recommendation record with selected account & contact
        
    */
    userSelectedReference : function(component, event, helper, buttonClicked){
         
        var referenceWrap = component.get("v.reference");
        var action = component.get('c.connectTalentReferences');
        var recommendationId;
        if(referenceWrap.recommendation.Id === undefined)
            recommendationId = null;
        else
            recommendationId = referenceWrap.recommendation.Id;
        if(referenceWrap.recommendation.Relationship__c == undefined)   
            referenceWrap.recommendation.Relationship__c = "Manager";
        if(referenceWrap.recommendation.Relationship_Duration__c == undefined)  
            referenceWrap.recommendation.Relationship_Duration__c = "<1 year";
        
        action.setParams({
           "recommendationId":         recommendationId,
            "talentId" :                component.get('v.recordId'),
            "selectedrecommendation"  : referenceWrap.recommendation,
            "referenceFrom" : component.get('v.selectedContactId'),
            "referenceContact"  : referenceWrap.contact,
            "buttonClicked" : buttonClicked
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
           
            if (state === "SUCCESS") {

                var res = "Refresh Activity";
                var evt = $A.get("e.c:E_TalentActivityUpdateEvent");
                evt.setParams({ "result": event.getSource().getLocalId()});
                evt.fire(); 
                var evt2 = $A.get("e.c:E_TalentActivityReload");
                evt2.setParams({ "result": event.getSource().getLocalId()});
                evt2.fire();
                // this.stopSpinner(component, event, helper);
                //component.set('v.resetDropdownValues', !component.get('v.resetDropdownValues'));
                this.relaodDatatableEvent(component, event, helper);
                this.closeModal(component, event, helper);
                this.clearAddEditReferenceForm(component, event, helper);
            }else if (state === "ERROR") {
                this.handleError(response);
                this.closeModal(component, event, helper);
            
            }

            this.stopSpinner(component, event, helper);

        }));

        $A.enqueueAction(action);
    },

    setMasterReferenceEventParameter : function(component, event,helper) {
        //Get the event using event name. 
        
        var phoneVal = [];
        
        if(component.find("homePhone").get("v.value")){
            phoneVal.push(component.find("homePhone").get("v.value"));
        }
        if(component.find("mobilePhone").get("v.value")){
            phoneVal.push(component.find("mobilePhone").get("v.value"));
        }
        if(component.find("phone").get("v.value")){
            phoneVal.push(component.find("phone").get("v.value"));
        }
        
        var appEvent = $A.get("e.c:E_Talent_Reference_Master_Event"); 
        //Set event attribute value
        appEvent.setParams({
            "firstNameFieldValue" : component.find("firstName").get("v.value"),
            "lastNameFieldValue" : component.find("lastName").get("v.value"),
            "emailFieldValue" : component.find("email").get("v.value"),
            "phoneFieldValue" : phoneVal,
            "showPopup" : true,
            "talentContactId" : component.get("v.recordId"),
            "referenceRecord" : component.get("v.reference"),
            "recommendationRecord":component.get("v.reference.recommendation"), 
            "referenceContactRecord":component.get("v.reference.contact"),  
            "mode":component.get("v.mode"), 
            "shouldValidateSearch":true,
            "talentrecommendationId":component.get("v.reference.recommendation.Id"),
            "referenceContactId":component.get("v.reference.contact.Id"),  
            "hideAllSearchModal" : true,
            "IsSaveAndComplete": true
        }); 
        appEvent.fire();

        this.stopSpinner(component, event, helper);
        //this.closeModal(component, event, helper);
        //this.clearAddEditReferenceForm(component, event, helper);
    },

    saveReferenceFromSearch : function(component,event, helper){
        
        var params = {recordId: component.get("v.recommendation")};
        var action = component.get('c.getContactReferences');
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.reference', response.getReturnValue());
                var reference = response.getReturnValue(); 
                component.set("v.reference", reference );
                this.validateAndSaveReferences(component, event, helper, "Edit");
                
            }else if (state === "ERROR") {
                this.handleError(response);
            }
        });
        $A.enqueueAction(action);
    },

    // hideSearchExistingForms : function(component, event,helper) {
/*    TOBE REMOVED       
        var appEvent = $A.get("e.c:E_Talent_Reference_Master_Event"); 
        //Set event attribute value
        appEvent.setParams({
            "hideAllSearchModal" : false
        }); 
        appEvent.fire(); 
     */   
        //helper.closeModal(component, event, helper);
        //helper.clearAddEditReferenceForm(component, event, helper); TOBE REMOVED
    // },
    
    relaodDatatableEvent : function(component, event,helper) {
          
        
        var cmpEvent = component.getEvent("reloadDataTable");
        cmpEvent.setParams({
            "recordId" : component.get('v.recordId')
        });
        cmpEvent.fire();
        // this.hideSearchExistingForms(component, event,helper);
    },

	clearReferenceObject: function(component) {
		component.set("v.reference", {
			"account": {},
			"contact": {},
			"recommendation": {},
			"disableContactField": false,
			"rwsRecord": false
		});
	},

	/* Monika - focus a given fieldId - S- 200682 */
	focusBtn : function(component, fieldId) {
		
      const fieldIdToFocus = component.find(fieldId);
	  if(fieldIdToFocus == null) {	
		fieldIdToFocus = document.getElementById(fieldId);	
		if(fieldIdToFocus == null) return;	
	  }

		setTimeout(function(){ fieldIdToFocus.focus(); }, 200);
	},
	//End Monika

	focusSource : function(component){
		var cmpEventFromClose = $A.get('e.c:E_FocusSource');
		cmpEventFromClose.setParams({"fieldIdToGetFocus":"tlp-header-actions_AddReferences"});
		cmpEventFromClose.fire();
	}
})