({
    init: function (cmp, e, h)  {
        let randomID = Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
        cmp.set('v.randomID', randomID);
    },

    valueChange: function(cmp, e, h) {
        const getVal = cmp.find("select").get("v.value");
        cmp.set("v.value", getVal);
    }

    // resetValue: function(cmp, e, h) {
    //     let params = e.getParams();
    //     if (params.oldValue !== params.value) {
    //         const getSelect = cmp.find("select");
    //         getSelect.set("v.value", "");
    //     }
    // }
})