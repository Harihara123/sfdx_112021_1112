({
	doInit: function(component, event, helper) {
		helper.focusBtn(component,"modalCloseBtn"); 
    },

    showModal : function(component, event, helper) {
	    if(component.get("v.externalsource") == "TD"){
			 helper.showModal(component, event); 
		  }
		},
	
	showModalfromTS : function(component, event, helper) {
		helper.showModalfromTS(component, event);
	}, 
  
   hideModal : function(component, event, helper) {
		helper.hideModal(component);
	}, 
	updateSelectRows : function(component, event, helper) {
		helper.updateSelectRows(component, event);
    },

    linkJob: function(component, event, helper){
        helper.validateSelectedLinkJobs(component, event,helper);
    }, 
    
    submitJob: function(component, event, helper){
        //If there is no record found where OFCCP is true, then it will call the submitJob method.
        helper.validateSelectedJobs(component, event,helper);
        //helper.submitJob(component, event);
    }, 

	searchOnEnter : function(component, event, helper) {
        if (event.getParams().keyCode === 13) {
           	helper.linkJob(component, event);
        }
	},

    backToRecentlyViewed: function(component, event, helper){
	    helper.backToRecentlyViewed(component, event);
	    }, 

    ReqSearchModalResultsEventHandler: function(component, event, helper){
	   helper.reqsearchHandler(component, event);
		
    },
	updateReqRes: function(component, event, helper){
	   helper.updateReqRes(component, event);
	},
    //S-83479 added for link add applicant
     openSourceModal: function(component,event,helper){
        helper.openSourceModal(component);
    },
    //S-83479 added for link add applicant
    closeSourceModal: function(component,event,helper){
        helper.closeSourceModal(component);
    },

	/* Monika - S-202119 - focus close button on next tab of Cancel button */
	focusBtnOnTabshiftTab: function(component, event, helper) { 

		if ( event.keyCode == 9 ) {	
			helper.focusBtn(component,"modalCloseBtn");
		}

	event.preventDefault();	

	/*Monika - S-202119 - As the focus is getting trapped between cancel and close buttons, set focus on submit/reset button of Req Search on shift+tab from save*/
		if (event.shiftKey && event.keyCode == 9) {
			if(!component.get("v.disableButton"))
				helper.focusBtn(component,"submitButton");
			else if(component.get("v.disableButton"))
				{
					// focus reset button
					var appEventFromClose = $A.get('e.c:E_FocusResetBtn_ReqSearch');
					appEventFromClose.setParams({"fieldIdToGetFocus":"resetButtonId"});
					appEventFromClose.fire();
				}		
		}	
	},

	/* Monika - S-202119 - getting focus on cancel button on reverse tab from close button  */	
	focusBackCancelFromClose : function(component,event,helper){
		if (event.shiftKey && event.keyCode == 9) {
			if(component.get("v.runningUserOPCO") == "CATEGORY2")
				helper.focusBtn(component,"applicantCancel");
			else
				helper.focusBtn(component,"cancelButton");
		}
	},

	focusCloseBtnOnModalOpen : function(component,event,helper){
		helper.focusBtn(component,"modalCloseBtn"); 
	}
})