({
    toggleClass : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse : function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },
	updateReqRes: function(component, event){
	   var OppMap = component.get("v.ReqResList");
		var val = event.getParam("val");
		var req = event.getParam("reqID");
		if(val){
		   OppMap.push({reqID:req,orgID: event.getParam("orgID")});
		}else{
		     for(var i = 0; i < OppMap.length; i++) {
					if(OppMap[i].reqID === req) {
						OppMap.splice(i,1);
						break;
					}
				}
		
		}
		 
		 component.set("v.ReqResList",OppMap);

		 if( $A.util.isEmpty(OppMap) ){
		    component.set("v.disableButton",true);
			this.closeSourceModal(component);
		 }else{
		    component.set("v.disableButton",false);
		 }

    },

	 reqsearchHandler: function(component, event){
        var scrollTop = component.find("modalContent").getElement();
	    component.set("v.selectedRows",[]);
		component.set("v.ReqResList",[]);
	    if(event.getParam("sourcebutton") === "search"){
		    this.closeSourceModal(component);
		    component.set("v.showReqSearchResults", event.getParam("showReqSearchResults"));
			component.set("v.disableButton", event.getParam("disableLinkButton"));
            this.backToTop(component, scrollTop, 100);
		}else if(event.getParam("sourcebutton") === "refresh"){
		    this.destroyReqSearch(component);
			this.createReqSearch(component);
		}

    },

	backToRecentlyViewed: function(component, event){
	    component.set("v.selectedRows",[]);
		component.set("v.ReqResList",[]);
		component.set("v.disableButton", true);
		this.closeSourceModal(component);
	    this.destroyReqSearch(component);
        component.set("v.showReqSearchResults", false);
		this.createReqSearch(component);
    },

	createReqSearch : function (component) {
	var params = {externalsource : 'TD' , groupFilterOverride :'AGS,RPO,MLA,AP,AG_EMEA,AEROTEK,TEK' , displayResults : component.get('v.showReqSearchResults'),"aura:id":"reqsearch",accountId:component.get('v.talentId') };
	 $A.createComponent(
			"c:C_SearchContainerReq",
			params,
			function(newComponent, status, errorMessage){
				//Add the new button to the body array
				if (status === "SUCCESS") {
					component.set("v.C_SearchReqsContainer", newComponent);
				}
				else if (status === "INCOMPLETE") {
					// Show offline error
				}
				else if (status === "ERROR") {
					// Show error message
				}
			} 
		); 
	},

	destroyReqSearch : function (component) {
	 var rscmp = component.find("reqsearch");
	 if(!$A.util.isUndefinedOrNull(rscmp)){
	    if(rscmp.isValid()){
	    rscmp.destroy();
		}
	 }
	 
	},

	showModal : function(component, event, helper) {
		console.log('Inside ShowModal--------reqOpco----------TRSM-'+component.get("v.reqOpco"));
		console.log('Inside ShowModal--------recordId----------TRSM-'+component.get("v.recordId"));
		console.log('Inside ShowModal--------talentId----------TRSM-'+component.get("v.talentId"));
		console.log('Inside ShowModal--------ShipToContactId----------TRSM-'+component.get("v.record.ShipToContactId"));
		console.log('Inside ShowModal--------SubmittalId----------TRSM-'+component.get("v.record.Id"));
		
		var reqOpco=component.get("v.reqOpco");
		if(reqOpco != undefined){			
			//var position = reqOpco.indexOf('+');
		
			//if(position != -1){
			//console.log('Inside ShowModal--------inside position----------TRSM'+position);
				//var status = reqOpco.substringAfter('+');
			
				//var opco = reqOpco.SubStringBefore('+');
			//var opco = reqOpco.lastIndexOf('+');
			var opco1 = reqOpco.split("+")[0];
			var opco2 = reqOpco.split("+")[1];
			console.log('Inside ShowModal-------opco-split1--------TRSM-'+opco1);
			console.log('Inside ShowModal-------opco-split2---------TRSM-'+opco2);
			//var opco =reqOpco.substring(0, reqOpco.lastIndexOf('+'));
				console.log('Inside ShowModal--------inside position-opco---------TRSM-'+opco1);
				component.set("v.reqOpco",opco1);
				component.set("v.currentstatus",opco2);
			//}
		}
		console.log('Inside ShowModal-------ShipToContactId--'+component.get("v.record.ShipToContactId"));
        if (component.get("v.record") && component.get("v.record.ShipToContactId")) {
        	component.set("v.recordId", component.get("v.record.ShipToContactId"));
    	}
		//console.log('record.Name'+component.get("v.record.Name"));
		//console.log('record.Id'+component.get("v.record.Id"));
		/*ak changes if (typeof event.getParam("contactId") === "undefined" 
			|| (component.get("v.recordId") === event.getParam("contactId"))) {
                component.set("v.talentId",event.getParam("talentId"));
                component.set("v.record",event.getParam("record"));
                component.set("v.runningUserOPCO", event.getParam("category"));*/
				component.set("v.contactName", component.get("v.record.Name"));
				this.createReqSearch(component);
				this.initModal(component, event);  
		//}
	},
	showModalfromTS : function(component, event, helper) {
				console.log('Inside ShowModalFromTs-------------');
	            var modalobj = component.get("v.ReqSearchModalObj");
                component.set("v.talentId",modalobj.record.candidate_id);
				component.set("v.recordId",modalobj.record.contact_id);
                component.set("v.record",modalobj.record);
                component.set("v.runningUserOPCO", modalobj.runningUser.category);
				component.set("v.contactName", modalobj.record.given_name +" "+ modalobj.record.family_name);
				component.set("v.externalsource", "TS");
				this.createReqSearch(component);
				this.initModal(component, event); 
		
		         
         
    },
    initModal : function (component, event) {
	    component.set("v.selectedRows",[]);
		component.set("v.ReqResList",[]);
        this.toggleClass(component,'spin','slds-');
		console.log('RecordId in TalentReqSearchModal'+component.get("v.recordId"));
	    var OppMap = new Map();
        var action = component.get("c.getRecentReqs");

        action.setParams({ contactId : component.get("v.recordId"),
							maxRows: component.get("v.maxRows")});
		action.setStorable();
        action.setCallback(this, function(response) {
             this.toggleClassInverse(component,'spin','slds-');
            var res = [];
			res = response.getReturnValue();
			if(!$A.util.isEmpty(res)){
			  for (var i = 0; i < res.length; i++) {
			       res[i].Location = ($A.util.isUndefined(res[i].Req_Worksite_Street__c) ? '': res[i].Req_Worksite_Street__c)  + ',' + '\n'+ ($A.util.isUndefined(res[i].Req_Worksite_City__c) ? '': res[i].Req_Worksite_City__c)+ ','+ ($A.util.isUndefined(res[i].Req_Worksite_State__c) ? '': res[i].Req_Worksite_State__c)+ ',' + '\n' + ($A.util.isUndefined(res[i].Req_Worksite_Country__c) ? '': res[i].Req_Worksite_Country__c)+ ','+ ($A.util.isUndefined(res[i].Req_Worksite_Postal_Code__c) ? '': res[i].Req_Worksite_Postal_Code__c);
				    if(!$A.util.isUndefinedOrNull(res[i].Orders) ){
                       res[i].disabled = true;
				     }
				      OppMap[res[i].Id] = res[i];
                   

			  }
			  component.set("v.Opps",res);
			  component.set("v.OppMap",OppMap);
			}
			component.set("v.showReqSearchResults",false);
			
            this.toggleClass(component,'backdropTalentReqSearchDetail','slds-backdrop--');
            this.toggleClass(component,'modaldialogTalentReqSearchDetail','slds-fade-in-');
        });

        $A.enqueueAction(action);
    },

    hideModal : function (component) {
		if(component.get("v.externalsource") == "TD"){
		   component.destroy();
		}else{
			this.closeSourceModal(component);
			this.toggleClassInverse(component,'backdropTalentReqSearchDetail','slds-backdrop--');
			this.toggleClassInverse(component,'modaldialogTalentReqSearchDetail','slds-fade-in-');

			//component.set("v.recordId",null);
			//component.set("v.talentId",null);
			component.set("v.record",null);
			component.set("v.contactName",null);
			component.set("v.runningUserOPCO",null);
			component.set("v.Opps",null);
			component.set("v.OppMap",null);
			component.set("v.selectedRows",[]);
			component.set("v.disableButton",true);
			component.set("v.ReqResList",[]);
			this.destroyReqSearch(component);
		}

    },

	updateSelectRows : function(component, event) {
		
		var evt = event.getSource();
		var selRows = component.get("v.selectedRows");
		if(evt.get("v.value") === true){
		   selRows.push(evt.get("v.text"));
		}else{
		 var index = selRows.indexOf(evt.get("v.text"));
		 selRows.splice(index,1);
		}

		component.set("v.selectedRows",selRows);
		if(selRows.length > 0){
		   component.set("v.disableButton",false);
		}else{
		  component.set("v.disableButton",true);
		  this.closeSourceModal(component);
		}
    },
	validateLink : function (component,event) {
		if(component.get("v.showReqSearchResults")){
		   var reqIds = component.get("v.ReqResList");
		   if(reqIds.length >1){
				component.set("v.disableButton",true);
		   }
		}else{
		  var reqIds = component.get("v.selectedRows");
		  console.log('Inside---linkJob--else--TRSM--reqIds.length-----'+reqIds.length);
		  if(reqIds.length >1){
				component.set("v.disableButton",true);
		   }		
		}
	},
	validateSelectedLinkJobs :function (component,event,helper) {
        this.toggleClass(component,'spin','slds-');
        
        var Reqs = {};
        
        if(component.get("v.showReqSearchResults")){
		   var reqIds = component.get("v.ReqResList");
		   for(var i=0;i<reqIds.length;i++){
				Reqs[reqIds[i].reqID] = reqIds[i].orgID;
			
			}
		}else{
		  var reqIds = component.get("v.selectedRows");
		  var OppMap = component.get("v.OppMap");
		  for(var i=0;i<reqIds.length;i++){				
				Reqs[reqIds[i]] = OppMap[reqIds[i]].AccountId;			
			}		
		}
        
        var action = component.get("c.validateSubmittals");
		
		action.setParams({ reqs : Reqs })

		action.setCallback(this, function(response) {
            this.toggleClassInverse(component,'spin','slds-');
			var state = response.getState(); 
			if(state === 'SUCCESS'){
                if(response.getReturnValue() == 'true' || response.getReturnValue() == true)
                {
                    var toastEvent = $A.get("e.force:showToast");
                 	toastEvent.setParams({
                        "type":"error",
                        "message": "Please uncheck OFCCP Req's."

                    });
                    toastEvent.fire();
                }
                else{
				helper.linkJob(component,event);
				}
			}else{
                var toastEvent = $A.get("e.force:showToast");
			 toastEvent.setParams({
				    "type":"error",
					"message": "The Talent was not link to the Req(s). Please try again."
				});
                toastEvent.fire();
			}			
        });
        $A.enqueueAction(action);
    },
	
	linkJob : function (component,event) {
        this.toggleClass(component,'spin','slds-');
        console.log('reqOpco----TRSM'+component.get("v.reqOpco"));
		var Reqs = {};
        var applicantSource = component.find("applicantSource");
        var sourceType = "";
        var appStatus = "";
		var srcbutton = event.getSource().getLocalId();
		
		//S-83479 added for add as applicant
		if(srcbutton === "saveAsApplicantBtn" || (event.getParams().keyCode === 13)){
		console.log('Inside---linkJob--if--TRSM');
		   appStatus = "Applicant";
			if($A.util.isEmpty(applicantSource.get('v.value')))
				{
					applicantSource.set("v.errors",[{message:"Source is a required field."}]);
					this.toggleClassInverse(component,'spin','slds-');
					return;
				}
				else{
					applicantSource.set("v.errors",null);
				}
		}else{
		     appStatus = "Linked";
			console.log('Inside---linkJob--else--TRSM--appStatus'+appStatus);
		}
		if(component.get("v.showReqSearchResults")){
		   var reqIds = component.get("v.ReqResList");
		   for(var i=0;i<reqIds.length;i++){
				Reqs[reqIds[i].reqID] = reqIds[i].orgID;
			}
		}else{
		  var reqIds = component.get("v.selectedRows");
		  console.log('Inside---linkJob--else--TRSM--reqIds.length-----'+reqIds.length);
		  var OppMap = component.get("v.OppMap");
		  for(var i=0;i<reqIds.length;i++){
				
				Reqs[reqIds[i]] = OppMap[reqIds[i]].AccountId;			
			}		
		}
		var subId='';
		console.log('Inside TRSM appStatus-----'+appStatus);

		if(component.get("v.reqOpco") =='Proactive'){
			var opco=component.get("v.reqOpco");
			if(component.get("v.currentstatus") =='Offer Accepted'){
				appStatus='Offer Accepted';
			}else  if (['Interviewing','Add New Interview','Edit Interview'].includes(component.get("v.currentstatus"))) {
				appStatus='Interviewing';
			}
			appStatus = appStatus+'-'+opco;
			var submittalId=component.get("v.record.Id");
			appStatus = appStatus+'@'+submittalId;
		}
		console.log('Inside TRSM appStatus-After-merge----'+appStatus);
		console.log('recordId in TRSM---------------------------'+component.get("v.recordId"));
		console.log('SUbmitalrecordId in TRSM---------------------------'+component.get("v.record.Id"));
		var action = component.get("c.createSubmittals");
		action.setParams({ contactId : component.get("v.recordId"),
		                   reqs : Reqs, status: appStatus, source: component.get("v.applicantSource") })

		action.setCallback(this, function(response) {
            this.toggleClassInverse(component,'spin','slds-');
			
			var state = response.getState(); 
			var toastEvent = $A.get("e.force:showToast");
			if(state === 'SUCCESS'){
				var ssevt = $A.get("e.c:E_SubmittalStatusChangeSaved");
					ssevt.setParams({"contactId": component.get("v.recordId")});
					ssevt.fire();
					var message = "The Talent was successfully linked to the Req(s)";
					
					(appStatus === "Applicant")?message += " as an Applicant.":message +=".";
					 
				toastEvent.setParams({
				    "type":"success",
					"message": message
				});
				this.hideModal(component);
			}else{
			 toastEvent.setParams({
				    "type":"error",
					"message": "The Talent was not linked to the Req(s). Please try again."
				});
			}
			toastEvent.fire();
			   
			});

        $A.enqueueAction(action);
    },
      //Added by Krishna for validating selected talents for OFCCP
    validateSelectedJobs :function (component,event,helper) {
        this.toggleClass(component,'spin','slds-');
        
        var Reqs = {};
        
        if(component.get("v.showReqSearchResults")){
		   var reqIds = component.get("v.ReqResList");
		   for(var i=0;i<reqIds.length;i++){
				Reqs[reqIds[i].reqID] = reqIds[i].orgID;
			
			}
		}else{
		  var reqIds = component.get("v.selectedRows");
		  var OppMap = component.get("v.OppMap");
		  for(var i=0;i<reqIds.length;i++){				
				Reqs[reqIds[i]] = OppMap[reqIds[i]].AccountId;			
			}		
		}
        
        var action = component.get("c.validateSubmittals");
		
		action.setParams({ reqs : Reqs })

		action.setCallback(this, function(response) {
            this.toggleClassInverse(component,'spin','slds-');
			var state = response.getState(); 
			if(state === 'SUCCESS'){
                if(response.getReturnValue() == 'true' || response.getReturnValue() == true)
                {
                    var toastEvent = $A.get("e.force:showToast");
                 	toastEvent.setParams({
                        "type":"error",
                        "message": "Please uncheck OFCCP Req's."

                    });
                    toastEvent.fire();
                }
                else{
				helper.submitJob(component,event);
				}
			}else{
                var toastEvent = $A.get("e.force:showToast");
			 toastEvent.setParams({
				    "type":"error",
					"message": "The Talent was not submitted to the Req(s). Please try again."
				});
                toastEvent.fire();
			}			
        });
        $A.enqueueAction(action);
    },
	submitJob : function (component,event) {
        this.toggleClass(component,'spin','slds-');
        
		var Reqs = {};
        var applicantSource = component.find("applicantSource");
        var sourceType = "";
        var appStatus = "";
		var srcbutton = event.getSource().getLocalId();
		
		//S-83479 added for add as applicant
		if(srcbutton === "saveAsApplicantBtn" || (event.getParams().keyCode === 13)){
		   appStatus = "Applicant";
			if($A.util.isEmpty(applicantSource.get('v.value')))
				{
					applicantSource.set("v.errors",[{message:"Source is a required field."}]);
					this.toggleClassInverse(component,'spin','slds-');
					return;
				}
				else{
					applicantSource.set("v.errors",null);
				}
		}else{
		     appStatus = "Submitted";
		
		}

		if(component.get("v.showReqSearchResults")){
		   var reqIds = component.get("v.ReqResList");
		   for(var i=0;i<reqIds.length;i++){
				Reqs[reqIds[i].reqID] = reqIds[i].orgID;
			
			}
		}else{
		  var reqIds = component.get("v.selectedRows");
		  var OppMap = component.get("v.OppMap");
		  for(var i=0;i<reqIds.length;i++){
				
				Reqs[reqIds[i]] = OppMap[reqIds[i]].AccountId;
			
			}
		
		}
		
         



		var action = component.get("c.createSubmittals");
		
		action.setParams({ contactId : component.get("v.recordId"),
		                   reqs : Reqs, status: appStatus, source: component.get("v.applicantSource") })

		action.setCallback(this, function(response) {
            this.toggleClassInverse(component,'spin','slds-');
			
			var state = response.getState(); 
			var toastEvent = $A.get("e.force:showToast");
			if(state === 'SUCCESS'){
				var ssevt = $A.get("e.c:E_SubmittalStatusChangeSaved");
					ssevt.setParams({"contactId": component.get("v.recordId")});
					ssevt.fire();
					var message = "The Talent was successfully submitted to the Req(s)";
					
					(appStatus === "Applicant")?message += " as an Applicant.":message +=".";
					 
				toastEvent.setParams({
				    "type":"success",
					"message": message
				});
				this.hideModal(component);
			}else{
			 toastEvent.setParams({
				    "type":"error",
					"message": "The Talent was not submitted to the Req(s). Please try again."
				});
			}
			toastEvent.fire();
			   
			});

        $A.enqueueAction(action);
    },    
    //S-83479 added for link add applicant
    openSourceModal: function(component){
        var linkapplicantbtn = component.find('linkapplicantbtn');
        $A.util.addClass(linkapplicantbtn, 'slds-hide');

        var modalCancel = component.find('cancelButton'); 
        $A.util.addClass(modalCancel, 'slds-hide');
        
        var linkbtn = component.find('saveButton');
        $A.util.addClass(linkbtn, 'slds-hide');

		var submitbtn = component.find('submitButton');
        $A.util.addClass(submitbtn, 'slds-hide');

        var applicantSource = component.find('applicantSource');
        $A.util.removeClass(applicantSource, 'slds-hide');

        var applicantCancel = component.find('applicantCancel');
        $A.util.removeClass(applicantCancel, 'slds-hide');
        
        var savebtn = component.find('saveAsApplicantBtn');
        $A.util.removeClass(savebtn, 'slds-hide');

    },
    //S-83479 added for link add applicant
    closeSourceModal: function(component){
	    
		var modalCancel = component.find('cancelButton');
        var linkapplicantbtn = component.find('linkapplicantbtn');
		var linkbtn = component.find('saveButton');
		var submitbtn = component.find('submitButton');
		var applicantSource = component.find('applicantSource');
        var applicantCancel = component.find('applicantCancel');
        var savebtn = component.find('saveAsApplicantBtn');
        
		if(!$A.util.isUndefined(linkapplicantbtn)){
		    $A.util.removeClass(modalCancel, 'slds-hide');
            $A.util.removeClass(linkapplicantbtn, 'slds-hide'); 
            $A.util.removeClass(linkbtn, 'slds-hide');
			$A.util.removeClass(submitbtn, 'slds-hide');
			$A.util.addClass(applicantSource, 'slds-hide'); 
            $A.util.addClass(applicantCancel, 'slds-hide');
			$A.util.addClass(savebtn, 'slds-hide');
			 var appSource = component.find('applicantSource');
			appSource.set("v.errors",null);
			appSource.set("v.value",null);
		}
        
    },
    backToTop: function(component, element, duration) {

        var target = 0;

        var body = element;

        var scrollTo = function(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop;
                var perTick = difference / duration * 10;
       
                setTimeout(function() {
                    element.scrollTop = element.scrollTop + perTick;
               
                    if (element.scrollTop === to) return;
                    scrollTo(element, to, duration - 10);
                }, 10);
            };
      
        scrollTo(body, target, duration);
    },

	/* Monika - focus a given fieldId - S- 202119 */
	focusBtn : function(component, fieldId) {
		
      var fieldIdToFocus = component.find(fieldId);
	  if(fieldIdToFocus == null) {	
		fieldIdToFocus = document.getElementById(fieldId);	
		if(fieldIdToFocus == null) return;	
	  }

		setTimeout(function(){ fieldIdToFocus.focus(); }, 200);
	},
	//End Monika


})