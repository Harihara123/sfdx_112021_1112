({
   doInit:function(component,event,helper){
		/*var talentId = component.get('v.talentId');
		var talentDocumentId = component.get('v.talentDocumentId');
        var parsedTalent = component.get("c.fetchExistingNParsedTalent");
        parsedTalent.setParams({"documentID": talentDocumentId,"talentID":talentId,"functionalityName":"Contact"});
        parsedTalent.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                //UNCOMMENT THIS LINE WHEN MOVED TO MAYDEV.
                component.set("v.items", JSON.parse(response.getReturnValue()));
				//component.set("v.cmpLoaded",true);
                console.log('<<<<<<<<>>>>response.getReturnValue():'+response.getReturnValue());
 				//fire event to update footer
                var syncDataEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_ContactData");
		        syncDataEvt.setParams({
		            "items" : component.get('v.items')
		        });
		        syncDataEvt.fire();
            } else {
                console.log('Error while getting the parsed contact: response.getReturnValue():'+response.getReturnValue() + 'state:'+state);
            }
        });
        $A.enqueueAction(parsedTalent);*/
		/*var syncDataEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_ContactData");
		syncDataEvt.setParams({
		    "items" : component.get('v.items')
		});
		syncDataEvt.fire();*/
    },
	startSpinner: function(component,event,helper){
		component.set('v.saved',false);
	},
	stopSpinner: function(component,event,helper){
		component.set('v.saved',true);
	},
    updateItemsAndRerender: function(component,event,helper){
    	console.log('dropped field');
    	var fldMap = event.getParam('fldMap');
    	var items = component.get('v.items');
    	var flds  = items.Contact[0].record;
    	var updateIndex = fldMap.fldUpdateIndex;
		for(var i=0;i<Object.keys(items.Contact[0].record).length;i++){
			if(i==updateIndex){
				items.Contact[0].record[i].fldValue 	= fldMap.fldParsedValue;
				items.Contact[0].record[i].valueChanged = true;
			}

		}
		//This will trigger re-rendering
		component.set('v.items',items);

    },
    updateContactData:function(component,event,helper){
        //fire event
        var syncDataEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_ContactData");
        syncDataEvt.setParams({
            "items" : component.get('v.items')
        });
        syncDataEvt.fire();

    }

})