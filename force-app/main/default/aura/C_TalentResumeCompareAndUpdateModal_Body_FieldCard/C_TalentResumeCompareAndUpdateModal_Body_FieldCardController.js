({
    doInit: function(component,event,helper){
        //console.log('inside field init:!');
    },
    onDragStart : function(component, event, helper) {
        event.dataTransfer.dropEffect = "move";
        var cmpTarget = component.find("fldContainer");
        $A.util.addClass(cmpTarget, 'DragEffect');
        var fldMap = component.get('v.fieldMap');
        event.dataTransfer.setData('fldMap.fldName', fldMap.fldName);
        event.dataTransfer.setData('fldMap.fldValue', fldMap.fldValue);
        event.dataTransfer.setData('fldMap.fldParsedValue', fldMap.fldParsedValue);
		event.dataTransfer.setData('fldMap.cType', fldMap.cType);
    },
     allowDrop: function(component, event, helper) {
        event.preventDefault();
    },
    onDrop: function(component, event, helper) {
        event.preventDefault();
        var index=component.get('v.index');
             
        //$A.util.removeClass(cmpTarget, 'BorderlessInput');
        var fldName         = component.get('v.fieldMap').fldName;//event.dataTransfer.getData('fldMap.fldName');
        var fldValue        = component.get('v.fieldMap').fldValue;//event.dataTransfer.getData('fldMap.fldValue');
		//console.log('fldname---:'+fldName);
		//console.log('fldvalue---:'+fldValue);
        var fldParsedValue  = event.dataTransfer.getData('fldMap.fldParsedValue');
		var ctype            = event.dataTransfer.getData('fldMap.cType');
		//console.log('on drop:dragged type'+ctype);
        //console.log('current field type:'+component.get('v.fieldMap').cType);
		if(ctype!=component.get('v.fieldMap').cType){
            return;
        }
		var cmpTarget = component.find("fldContainer");
        $A.util.addClass(cmpTarget, 'DropEffect');  
        var fldMap={};
        fldMap.fldName = fldName;
        fldMap.fldValue= fldValue;
        fldMap.fldParsedValue=fldParsedValue;
        fldMap.fldUpdateIndex=component.get('v.index');
		fldMap.cType = ctype;
        var fieldDroppedEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_FieldDrop");
        fieldDroppedEvt.setParams({
            "fldMap" : fldMap
        });
        fieldDroppedEvt.fire();

        var undoMap = component.get('v.undoMap');
        var items = component.get('v.items');
        var flds  = items.Contact[0].record;
        var undoMapKey =fldName;
		if(fldValue==undefined || fldValue==null || fldValue=='null'){
			fldValue='';
		}
        if(undoMap.hasOwnProperty(undoMapKey)){
			//console.log('fldVal--->'+fldValue);
            undoMap[undoMapKey]=fldValue;
        }
        else{
			//console.log('fldVal--->>'+fldValue);
            undoMap[undoMapKey]=fldValue;
        }
		//console.log('undoMap'+undoMap);
		//for(var k in undoMap){console.log('key'+k+'val:'+undoMap[k]);}
        component.set('v.undoMap',undoMap);

    },
    undoUpdate: function(component,event,helper){
        var undoMap = component.get('v.undoMap');
        var fldMap      = component.get('v.fieldMap');
        var fldName = fldMap.fldName;
        var items = component.get('v.items');
        var flds = items.Contact[0].record;
        flds.forEach(function(elmnt){
            if(elmnt.fldName ==fldName){
				//console.log('inside undo:fldName'+fldName+'val:'+undoMap[fldName]);
				if(undoMap[fldName]==undefined || undoMap[fldName]==null || undoMap[fldName]=='null'){
					elmnt.fldValue = '';
				}
				else{
					elmnt.fldValue = undoMap[fldName];
				}
                elmnt.valueChanged = false;
            }
        });
        var cmpTarget = component.find("fldContainer");
        $A.util.removeClass(cmpTarget, 'DropEffect');
        component.set('v.items',items);
    },
    handleClick : function(component,event,helper){
        var fldMap = component.get('v.fieldMap');
        var undoMap = component.get('v.undoMap');
        var items = component.get('v.items');
        var flds  = items.Contact[0].record;
        var undoMapKey =fldMap.fldName;
        flds.forEach(function(elmnt){
            if(elmnt.fldName ==fldMap.fldName){
                undoMap[undoMapKey]=elmnt.fldValue;
            }
        });
        
        var cmpTarget = component.find("fldContainer");
        $A.util.addClass(cmpTarget, 'DropEffect');       

        var index=component.get('v.index');
        fldMap.fldUpdateIndex=component.get('v.index');

        var fieldDroppedEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_FieldDrop");
        fieldDroppedEvt.setParams({
            "fldMap" : fldMap
        });
        fieldDroppedEvt.fire();
        component.set('v.undoMap',undoMap);
    }
    
})