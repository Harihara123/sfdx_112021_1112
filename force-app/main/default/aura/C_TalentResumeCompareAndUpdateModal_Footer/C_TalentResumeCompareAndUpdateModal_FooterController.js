({
	doInit: function(component,event,helper){
		helper.setSelButtonStatus(component);
		helper.setAllButtonStatus(component);

	},
	saveAll:function(component,event,helper){
		var items = component.get('v.items');
		var flds = items.Contact[0].record;
		var successMsg = 'Updated all fields from resume that are different on the record';
		//fire event to start spinner
		var startSpinnerEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_SaveInit");
        startSpinnerEvt.fire();
		flds.forEach(function(elmnt) {
			if(elmnt.matched==false){
				//cannot update first name,middle name and last name
				//if((elmnt.fldName!='First Name' && elmnt.fldName!='Last Name' && elmnt.fldName!='Middle Name')){
				if(elmnt.cType!='Header' && !$A.util.isEmpty(elmnt.fldParsedValue)){	
					elmnt.valueChanged = true;
					elmnt.fldValue=elmnt.fldParsedValue;
				}
				else{
					elmnt.valueChanged = false;
				}
			}
		});
		helper.saveTalent(component,items,successMsg);
    },
	saveSelected:function(component,event,helper){
		var items = component.get('v.items');
		var successMsg = 'Updated selected fields from resume.';
		var startSpinnerEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_SaveInit");
        startSpinnerEvt.fire();
		helper.saveTalent(component,items,successMsg);
    },
	cancel:function(component,event,helper){
  		component.find("overlayLib").notifyClose();

    },
    updateContactData : function(component,event,helper){
    	var items = event.getParam("items");
		//console.log('updatecontactdata in footer'+JSON.stringify(items));
    	component.set('v.items',items);

		helper.setSelButtonStatus(component);
		helper.setAllButtonStatus(component);
    }
})