({
	saveTalent : function(component,items,successMsg) {
		var saveTalent = component.get("c.save");
		var respMsg;
		//console.log('json:'+JSON.stringify(items));
        saveTalent.setParams({"jsonStr": JSON.stringify(items) });
        saveTalent.setCallback(this, function(response) {
            var state = response.getState();
		    var toastEvent = $A.get("e.force:showToast");
			var closeSpinnerEvt = $A.get("e.c:E_TalentResumeCompareAndUpdateModal_SaveComplete");
			closeSpinnerEvt.fire();
            if(state === "SUCCESS") {
				respMsg = response.getReturnValue();
                //console.log('<<<<<<<<in helper>>>>response.getReturnValue():'+response.getReturnValue());
				if(respMsg =='Successful'){
					if (component.get('v.goTo')) {
						var sObjEvent = $A.get("e.force:navigateToSObject");
						sObjEvent.setParams({
							"recordId": component.get('v.goTo')
						});
						sObjEvent.fire();
					}
					component.find("overlayLib").notifyClose();
					toastEvent.setParams({
					  title: "SUCCESS",
					  message: successMsg,
					  mode: "dismissible",
					  type: "success"
					});
					var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
					updateLMD.fire();
				}
				else {
                    if(!respMsg){
                     	respMsg = "Update failed, Exact Same record.";   
                    }
					component.find("overlayLib").notifyClose();
					toastEvent.setParams({
					  title: "ERROR",
					  message: respMsg,
					  mode: "dismissible",
					  type: "error"
					});
				}
		        toastEvent.fire();
            } else {
                //console.log('Error while getting the parsed contact:+response.getReturnValue() '+response.getReturnValue()+'state:' + state);
                component.find("overlayLib").notifyClose();
		        toastEvent.setParams({
		          title: "ERROR",
		          message: "Could not update the values.",
		          mode: "dismissible",
		          type: "error"
		        });
		        toastEvent.fire();
            }
        });
        $A.enqueueAction(saveTalent);
	},
	setSelButtonStatus: function(component){
		var items = component.get('v.items');
		var flds = items.Contact[0].record;
		//rewriting with for - of loop (ES6) so that can break out of iteration when match found
		// for each loop doesnt allow breaking out of iteration.
		for (let elmnt of flds) {
		  if(elmnt.valueChanged){
				component.set('v.selButtonDisabled',false);
				break;
			}
			else{
				component.set('v.selButtonDisabled',true);
			}
		}
	},
	setAllButtonStatus: function(component){
		var items = component.get('v.items');
		var flds = items.Contact[0].record;
		//rewriting with for - of loop (ES6) so that can break out of iteration when match found
		// for each loop doesnt allow breaking out of iteration.
		for (let elmnt of flds) {
			if(!elmnt.matched){
				if($A.util.isEmpty(elmnt.fldParsedValue)){
					component.set('v.allButtonDisabled',true);
				}
				else{
					component.set('v.allButtonDisabled',false);
					break;
				}
			}
			else{
				component.set('v.allButtonDisabled',true);
			}
		}
	}
})