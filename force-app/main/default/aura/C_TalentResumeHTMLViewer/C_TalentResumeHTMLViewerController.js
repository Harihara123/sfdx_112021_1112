({
    doInit : function(component, event, helper) {
		var params = {
					'recordId' : component.get('v.recordId')         
				};
        component.set("v.isLoading", true);
		helper.callServer(component,'ATS','TalentDocumentFunctions',"getAttachmentHTML",function(response){
            if(component.isValid()){
               
                component.set("v.isLoading", false);

                if(response === 'ATSApexException'){
                    component.set("v.hasHtml", false);
                } else {
                    component.set("v.hasHtml", true);
                     // Strip out style tags from server resume
                    var regex = /<style\b[^<]*(?:(?!<\/style)<[^<]*)*<\/style>/gi;

                    response = response.replace(regex, "");
                    component.set("v.htmlStr", response);			
                }
				//console.log('html resp'+response);	              
            } else {
                 component.set("v.hasHtml", false);
            }
        },params,true);
       
        component.set("v.hasResume", (component.get("v.recordId")) ? true : false);
    
    },

    downLoadResume: function(cmp, e, h) {
       const url = window.location.origin.split('.')[0];
       const windowUrl = `${url}--c.documentforce.com/servlet/servlet.FileDownload?file=${cmp.get("v.recordId")}`;
       window.open(windowUrl, '_blank');
       const docModalCloseEvt = cmp.getEvent("closeResumeModal");
       docModalCloseEvt.fire();
    }
})