({
    changeDocument : function(component, event, helper) {
        helper.changeDocument(component, event, helper);
    },
    showCreateDocumentCreateModal : function(component, event, helper) { 
        
        var modalEvt = $A.get("e.c:E_CreateDocumentModal");
        modalEvt.setParams({'contactId' : component.get('v.talentId'),'source':'TalentResumePreview'});
        modalEvt.fire(); 

    },
    downloadDocument : function(cmp,event,helper){
        var additional = cmp.get("v.additional");
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": additional + "&apikey=nQ8QcjyLeoMA0ALpH7BJ1nYuVGxDdGeM"
        });
        urlEvent.fire();
	},
	showPreview : function(cmp,event,helper){
		cmp.set("v.showPreview", true);
	},
	doInit: function(component, event, helper) {
		var accID = component.get("v.contactRecord.fields.AccountId.value");
        if(!accID) {
            accID = component.get("v.talentId");
        }
        let refreshTimeout = component.get("v.refreshTimeOut");
        if(component.get('v.bInitialize')) {
        if(!$A.util.isUndefinedOrNull(accID) && refreshTimeout >= 0){
            setTimeout(function(){
                component.set("v.records",[]);
                component.set("v.recordCount",0);
                component.set("v.viewListIndex",0);
                helper.getInitialViewListLoad(component,helper,accID);   
            },refreshTimeout);
			}
        }
        else {
            helper.changeDocument(component, event, helper);
            component.set("v.loading",false);
            //Updated last resume date and fire event.
            helper.getLastResumeDate(component);
        }
    },
            
    loadData: function(cmp, event, helper) {
        cmp.set("v.loading",true);
        cmp.set("v.records",[]);
        cmp.set("v.recordCount",0);

		helper.getResultAndCount(cmp, helper);
    },
    
    updateLoading : function(cmp,event,helper){
        helper.updateLoading(cmp);
    },
    
    reloadData : function(cmp,event,helper) {
        if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);
			cmp.set("v.viewListIndex",0);
            var accID = cmp.get("v.contactRecord.fields.AccountId.value");
            helper.getInitialViewListLoad(cmp,helper,accID);
            cmp.set("v.reloadData", false);

        }
    },
    
    selectViewMenuItem : function(component, event, helper) {
        var triggerCmp = component.find("trigger");
        
        if (triggerCmp) {
            var indexValue = event.getParam("value").split('_');
            var views = component.get("v.viewList");
            var arrayLength = views.length;
            
            for (var i = 0; i < arrayLength; i++) {
                
                if(parseInt(indexValue[1]) === i) {
                    component.set("v.viewListIndex", i);
                }
            }
            
        }
	}, 
	// S-85594 added by N. Lockett 7/9/18
		downloadResumeEvent : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": component.get('v.additional') + "&apikey=" + $A.get("$Label.c.ATS_APIGEE_AZURE_API_KEY"),
            "isredirect":false
        });
        urlEvent.fire(); 
	},
    showModalUploadResumeAppEvent : function(component, event, helper) {
        /* Sandeep : C_TalentDocumentModal moved to Modalscontainer comp.Attribute switch will not work.
             * Firing documentModalOpenEvent */
        //var theVal = component.get("v.addTrigger")
        //component.set("v.addTrigger", !theVal);
            let docModelevt= $A.get("e.c:E_DocumentModalOpen");
            docModelevt.setParam("talentId",component.get('v.talentId'));
        	docModelevt.setParam("source","TalentResumePreview");
            docModelevt.fire();
            //console.log('docModelevt fired');
		//Rajeesh fixing modal going under global header
		//recordId="{!v.talentId}
/*		var modalBody;
		$A.createComponents([
                ["c:C_TalentDocumentModal",{"recordId":component.get('v.talentId')}]                
            ],
                function(components, status){
                    if (status === "SUCCESS") {
                        modalBody = components[0];
						console.log('modal body'+modalBody+':id:'+component.get('v.talentId'));
                        component.find('overlayLib').showCustomModal({
                            header: '',
                            body: modalBody,
                            footer:'',
                            showCloseButton: true,
                            closeCallback: function() {
                                console.log('closed the file upload modal');
                            }
                        });
                    }
                }
               );*/
    },
    
})