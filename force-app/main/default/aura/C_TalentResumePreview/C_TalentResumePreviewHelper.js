({
    
    getInitialViewListLoad : function(cmp,helper,accID){
        var awaitResultFromViewList = false;
        var recordId = accID;
        cmp.set("v.talentId", recordId);
        
        var viewListFunction = cmp.get("v.viewListFunction");

        if(viewListFunction){
            if(viewListFunction != ''){
                awaitResultFromViewList = true;
            }
        }
      
        if(awaitResultFromViewList){
            this.getViewListItems(cmp, viewListFunction, recordId, helper);
        }else{
			//this.getResults(cmp,helper);
            //this.getResultCount(cmp);
			this.getResultAndCount(cmp, helper);
        }
	
    }
    ,getViewFromApexFunction : function(cmp, apexFunction, recordId, params,helper){
        
        var className = '';
        var subClassName = '';
        var methodName = '';

        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }
        
        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordId, "maxRows": cmp.get("v.maxRows")};

            if(params){
                if(params.length != 0){
                 Object.assign(actionParams,params);
                }
            }
            var basedata = cmp.find("bdhelper");
            basedata.callServer(cmp,className,subClassName,methodName,function(response){
            //this.callServer(cmp,className,subClassName,methodName,function(response){
               
                cmp.set("v.records", response);
                cmp.set("v.loadingData",false);
                var recordCount = 0;
                if(response){
                    if(response.length > 0){
                        if(response[0].totalItems){
                            recordCount = response[0].totalItems;
                        }

                        if (response[0].presentRecordCount !== "undefined") {
                            cmp.set ("v.presentRecordCount", response[0].presentRecordCount);
                            cmp.set ("v.pastRecordCount", response[0].pastRecordCount);
                            cmp.set ("v.hasNextSteps", response[0].hasNextSteps);
                            cmp.set ("v.hasPastActivity", response[0].hasPastActivity);
                        }
                    }
                }

                if(recordCount == 0){
                    this.getResultCount(cmp);
                }

                cmp.set("v.recordCount", recordCount); // returnVal.length);
                cmp.set("v.loadingCount",false);
            },actionParams,false);
        }

    }
    ,getViewListItems : function(cmp, viewListFunction, recordId, helper) {

		cmp.set("v.viewList", []);
        cmp.set("v.loadingData",true);
        var actionParams = {'recordId':recordId};
        var className = '';
        var subClassName = '';
        var methodName = '';

        var arr = viewListFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }

        var basedata = cmp.find("bdhelper");
        var self = this;
        basedata.callServer(cmp,className,subClassName,methodName,function(response){
       
            var newArr = JSON.parse(response); 
            
            cmp.set("v.viewList", newArr);
            cmp.set("v.loadingData",false);
            self.getResultAndCount(cmp, self);
            self.getLastResumeDate(cmp); 
        },actionParams,false);

    }
	, getResultAndCount : function(cmp, helper) {
		var listView = cmp.get("v.viewList");
		if(listView) {
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.talentId");

			var soql = listView[indexNo].soql;
			if(soql != '') {
				var countSoql = listView[indexNo].countSoql;

				soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'"); 
				countSoql = countSoql.replace("%%%parentRecordId%%%","'"+ recordID +"'"); 
		
				var apexFunction = "c.getRowAndCount";

				var params = {
					"soql": soql,
					"countSoql": countSoql,
					"maxRows": cmp.get("v.maxRows")
				};

				cmp.set("v.loadingData",true);
				cmp.set("v.loadingCount",true);

				var actionParams = {'recordId':recordID};
				if(params){
					if(params.length != 0){
						Object.assign(actionParams,params);
					}
				}
				var basedata = cmp.find("bdhelper");
				basedata.callServer(cmp,'','',apexFunction,function(response){
					//console.log('server response ',response);
					cmp.set("v.records", response.records);
					cmp.set("v.recordCount", response.count);
					cmp.set("v.loadingData",false);
				        
					if(listView[indexNo].additional){
						cmp.set("v.additional",listView[indexNo].additional);
					}

					/* S-64611 Set additional info to blank when resume count is zero
						to prevent resume preview displaying when all resumes deleted */
					if(cmp.get("v.recordCount") === "0"){
						cmp.set("v.additional", ""); 
					}
					// END change added S-64611
					cmp.set("v.loadingCount",false);
                            
				},actionParams,false);

				cmp.set("v.loadingCount",false);
			}
			else {
				var apexFunc = listView[indexNo].apexFunction;
                if(apexFunc){
					var params = listView[indexNo].params;
                    this.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                }
            }
		}
	}

    ,getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");
        
        if(listView){
            //console.log('listView defined');
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.talentId");
            
            var soql = listView[indexNo].soql;
			
            if(soql){
                if(soql != ''){
                     //console.log('soql defined');
                    soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
  
                        var apexFunction = "c.getRows";

                        var params = {
                            "soql": soql,
                            "maxRows": cmp.get("v.maxRows")
                        };

                        cmp.set("v.loadingData",true);
                        var actionParams = {'recordId':recordID};
                        if(params){
                            if(params.length != 0){
                             Object.assign(actionParams,params);
                            }
                        }
                    	var basedata = cmp.find("bdhelper");
                        basedata.callServer(cmp,'','',apexFunction,function(response){
                        	//console.log('getResults server response ',response);
                            cmp.set("v.records", response);
                            cmp.set("v.loadingData",false);
                        	cmp.set("v.loading",false);		
                            if(listView[indexNo].additional){
                                cmp.set("v.additional",listView[indexNo].additional);
                            }
                            
                        },actionParams,false);
                    }
                }else{
                    var apexFunc = listView[indexNo].apexFunction;
                    if(apexFunc){
                        var params = listView[indexNo].params;
                        this.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                    }
                }
            }
        }
    ,getResultCount : function(cmp){
        var listView = cmp.get("v.viewList");

        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.talentId");
            var countSoql = listView[indexNo].countSoql;    
            if(countSoql){
                 if(countSoql != ''){
                    countSoql = countSoql.replace("%%%parentRecordId%%%","'"+recordID+"'")
                    var params = {"soql": countSoql};
                    cmp.set("v.loadingCount",true);
                    
                    var basedata = cmp.find("bdhelper");
                    basedata.callServer(cmp,'','','getRowCount',function(response){
                    //this.callServer(cmp,'','','getRowCount',function(response){
                            cmp.set("v.recordCount", response);
                            //console.log('Count:::'+response);

                            /* S-64611 Set additional info to blank when resume count is zero
                               to prevent resume preview displaying when all resumes deleted */
                            if(cmp.get("v.recordCount") === "0"){
                                cmp.set("v.additional", ""); 
                            }
                            // END change added S-64611
                            cmp.set("v.loadingCount",false);
                        
                    },params,false);

                 }else{
                    cmp.set("v.loadingCount",false);
                 }
            }
        }
        
    },

    updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },

    refreshList : function(cmp) {
        cmp.set("v.reloadData", true);
    },

    createComponent : function(cmp, componentName, targetAttributeName, params) {
         $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },

    getLastResumeDate : function(cmp){
        var listView = cmp.get("v.viewList");
        var recordID = cmp.get("v.talentId");
        var resumeDate; 
         var resumeEvent = $A.get("e.c:E_UpdateTalentInsightsResumeDate");
         if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var resumeData = listView[indexNo]; 
            if(resumeData !== null && resumeData !== undefined){
                //if(resumeData.label.split(":").length > 1){
                 //  resumeDate = resumeData.label.split(":")[0];
                resumeDate = resumeData.CreatedDate;
                if(resumeDate !==null && resumeDate !== undefined){
                   resumeEvent.setParams({
                     "recordId": cmp.get("v.recordId"),
                     "lastResumeUploadDate": resumeDate, 
                     "hasResume": true
                    });            
                }
          }else{
                   resumeEvent.setParams({
                     "recordId": cmp.get("v.recordId"),
                     "hasResume": false
            });  
          }
        }
        resumeEvent.fire(); 
      },

    changeDocument : function(component, event, helper) {
        var recordID = component.get("v.records[0].Id");
        
        // if(recordID){
            /*Rajeesh iframe replacement*/
			/*var iframeSrc = '/servlet/servlet.FileDownload?file=' + recordID;
            var componentName = "c:C_IFrameDisplay";
            var targetAttributeName = "v.C_IFrameDisplay";
            var params = {"pageURL": '/servlet/servlet.FileDownload?file=' + recordID,
                          "additionalClassName": 'resumeViewer'};*/
			var targetAttributeName = "v.C_TalentResumeHTMLViewer";
			var componentName = "c:C_TalentResumeHTMLViewer";//rajeesh
            var params={"recordId":recordID};//rajeesh
            
            this.createComponent(component, componentName, targetAttributeName, params);
        // }
        // else{
            //component.set("v.C_IFrameDisplay","");
		// 	component.set("v.C_TalentResumeHTMLViewer","");
        // }
      }

})