({
    getHighlightedResume : function(component) {
            var queryStringUrl = this.createResumeHighlightQueryString(component); 
            console.log("Resume Highlight queryStringUrl >>>>>>");
            console.log(queryStringUrl);
            var action = component.get("c.search"); 
            action.setParams({"serviceName": "Resume Highlight", 
                              "queryString": queryStringUrl});
            action.setCallback(this, function(response) {
                var errList = [];
                if (response.getState() === "SUCCESS") {
                    var searchTalentsResponse = response.getReturnValue();
                    var searchResponse = JSON.parse(searchTalentsResponse);
                    if (searchResponse.response.hits.hits["0"].highlight) {
                        var htmlResume = searchResponse.response.hits.hits["0"].highlight["resume_1" + ".html"]["0"];

                        var familyName = component.get("v.record.family_name") !== null ? component.get("v.record.family_name") : "";
                        var givenName = component.get("v.record.given_name") !== null ? component.get("v.record.given_name") : ""; 
                        var fullName = (familyName  + ', ' + givenName).replace(/['"]+/g, '\\\''); 
    					component.set("v.highlightedResume", htmlResume);	
    /*                    var x = window.open('', '_blank');

                        if (x != null){
                            x.document.body.innerHTML = htmlResume;
                            x.document.title = fullName;
                        } else {
                            var toastEvt = $A.get("e.force:showToast");

                            toastEvt.setParams({
                                title: "Opps, something went wrong!",
                                message: "In order to view resume, please allow pop-ups from this page.",
                                type: "error"
                            });

                            toastEvt.fire();
                           
                        }
    */                   
                    } else {
                        this.viewResume(component);
                    }
                    if (searchResponse.fault) {
                        errList.push(searchResponse.fault.faultstring);
                        searchCompletedEvt.setParams({"hasError" : true,
                                                  "errors" : errList});
                        console.log("Error!");
                    } else if (searchResponse.header.errors && searchResponse.header.errors.length > 0) {
                        var errors = searchResponse.header.errors;
                        for (var i=0; i<errors.length; i++) {
                            if (errors[i] && errors[i].message) {
                                errList.push(errors[i].message);
                            }
                        }
                        searchCompletedEvt.setParams({"hasError" : true,
                                                  "errors" : errList});
                        console.log("Error!");
                    } 
                } else {
                    var errors = response.getError();
                    if (errors) {
                        for (var i=0; i<errors.length; i++) {
                            if (errors[i] && errors[i].message) {
                                errList.push(errors[i].message);
                            }
                        }
                    }
                }
                component.set("v.errors", errList);
            });
            $A.enqueueAction(action); 
        },

    createResumeHighlightQueryString : function (component) {
        var parameters = {
                          q: "novdev woodlands", 
                          id: "0019E00000St0jIQAR", 
                          "highlight.fields": "resume_1" + ".html",
                         };
        return(this.buildUrl(parameters));
    },

    buildUrl : function (parameters) {
         var qs = "";
         for(var key in parameters) {
            var value = parameters[key];
            qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
         }
         if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
         }
         return qs;
    },
})