({
	getActivity: function (component, helper) {
        component.set('v.loading', true);
        var action = component.get('c.getRecentServiceActivity');

        action.setParams({talentId : component.get("v.talentId")});

        action.setCallback(this, function(response) {
            var state = response.getState();

            if(state === "SUCCESS") {
                component.set("v.activity", response.getReturnValue());
                component.set('v.loading', false);

            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set('v.loading', false);
            }
        });

        $A.enqueueAction(action);
	}
})