({
	changeColor : function(component, event, helper) {
		component.set("v.statusColor", helper.getStatusColor(component));
		helper.setLabel(component);
	}
})