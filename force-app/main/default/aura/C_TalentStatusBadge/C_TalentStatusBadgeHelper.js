({
	getStatusColor : function(component) {
		var candidateStatus = component.get("v.candidateStatus"),
			colorClass;
		
		if(candidateStatus !== null && candidateStatus !== undefined){

			candidateStatus = candidateStatus.toUpperCase();

			switch(candidateStatus){
				case 'FORMER':
					colorClass = 'AG_Status-Former';
					break;

				case 'PLACED':
					colorClass = 'AG_Status-Placed';
					break;

				case 'CURRENT':
					colorClass = 'AG_Status-Current';
					break;

				default:
					colorClass = 'AG_Status-Default';
					break;
			}
			
			return colorClass;

		} else {
			// If candidateStatus is undefined, always return a color
			colorClass = 'AG_Status-Default';
			return colorClass;
		}	
	},
	setLabel: function(component) {
		let candidateStatus = component.get("v.candidateStatus"),
			label;
		
		if(candidateStatus !== null && candidateStatus !== undefined){ 
			candidateStatus = candidateStatus.toUpperCase();
			if (candidateStatus === "FORMER"){			
					label = $A.getReference("$Label.c.ATS_STATUS_FORMER");					
			}
			if (candidateStatus === "CURRENT"){			
				label = $A.getReference("$Label.c.ATS_STATUS_CURRENT");					
			}
			if (candidateStatus === "PLACED"){			
				label = $A.getReference("$Label.c.ATS_STATUS_PLACED");					
			}
			if (candidateStatus === "CANDIDATE"){			
				label = $A.getReference("$Label.c.ATS_STATUS_CANDIDATE");					
			}
			if (candidateStatus === "INACTIVE"){
				label = $A.getReference("$Label.c.ATS_INACTIVE_STATUS");
			}
			component.set("v.badgeLabel", label);					
		} 
	}
})