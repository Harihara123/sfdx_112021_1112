({  doInit: function(cmp, event, helper) {
    	helper.checkTEKGroupPermission(cmp);
    	helper.checkAerotekGroupPermission(cmp);
        var isApplicationCard = cmp.get("v.isApplicationCard");
        if(isApplicationCard){
			cmp.set("v.viewListIndex", 1); 
		}
        else {
		helper.getResultAndCount(cmp);
        }
    },
    setupDisplayFields : function(cmp, event, helper) {
        // console.log(component.get("v.records"));
        var records = cmp.get("v.records");
        for (var i = 0; i < records.length; i++) {
            if (typeof records[i].Opportunity !== "undefined" && records[i].Opportunity !== "") {
                records[i].opptyCityState = helper.getAddressString([records[i].Opportunity.Account_City__c, records[i].Opportunity.Account_State__c]);
            } else if (typeof records[i].ATS_Job__c !== "undefined" && records[i].ATS_Job__c !== "") {
                records[i].atsJobCityState = helper.getAddressString([records[i].ATS_Job__r.Account_City__c, records[i].ATS_Job__r.Account_State__c]);
            }
        }
    },

    linkToViewMore : function(cmp, event, helper) {
        var tabHeader = document.getElementsByClassName("tabHeader");
        if(tabHeader !== undefined && tabHeader !== null){
            for(var i=0; i <tabHeader.length; i++){
                if(tabHeader[i].text === "Submittals"){
                    tabHeader[i].click(); 
                }
            }
        }

        // OLD view more code (just in case we scrap tabs :-)

        /*var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__Candidate_Submittal_v2?accountId=" + recordID
        });
        urlEvent.fire();*/
    },

    linkToATSJob: function(cmp, event, helper) {
        var recordID = event.currentTarget.dataset.recordid;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/c__JobSummary?jobId=" + recordID
        });
        urlEvent.fire();
    },
    
     //Neel summer 18 URL change-> This method not in use. Method calling code commented in cmp.
    linkToOppty: function(cmp, event, helper) {
       var recordID = event.currentTarget.dataset.recordid;
        //window.open('/one/one.app#/sObject/' + recordID, '_blank');
        var isFlagOn = $A.get("$Label.c.CONNECTED_Summer18URLOn");
        var ligtningNewURL = $A.get("$Label.c.CONNECTED_Summer18URL");
        var dURL;
        
        if(isFlagOn === "true") {
              dURL = ligtningNewURL+"/r/Opportunity/" + recordID +"/view";
        }
          else {
              dURL = ligtningNewURL+"/sObject/" + recordID +"/view";
        }
        
        window.open(dURL, '_blank');
        // console.log("linkToOppty RECORD ID: ", recordID); 
        // var urlEvent = $A.get("e.force:navigateToURL");
        // window.open('/one/one.app#/sObject/' + recordID, '_blank');
        /*urlEvent.setParams({
          "url": "/" + recordID
        });
         urlEvent.fire();*/
    },
    loadData: function(cmp, event, helper) {
        cmp.set("v.loading",true);
        cmp.set("v.records",[]);
        cmp.set("v.recordCount",0);


        //helper.getResults(cmp,helper);
        //helper.getResultCount(cmp);
		helper.getResultAndCount(cmp); 
    }
    
    ,updateLoading : function(cmp,event,helper){
        helper.updateLoading(cmp);
    }
    ,reloadData : function(cmp,event,helper) {
        var rd = cmp.get("v.reloadData");
        if (cmp.get("v.reloadData") === true){
            cmp.set("v.loading",true);
            cmp.set("v.records",[]);
            cmp.set("v.recordCount",0);

            //helper.getResults(cmp,helper);
            //helper.getResultCount(cmp);
			helper.getResultAndCount(cmp); 

            cmp.set("v.reloadData", false);
        }

        
    },

    clickPencilEdit : function(component, event, helper) {
        var arr = event.getSource().get("v.name").split("~~");
        helper.initStatusUpdate(component, arr);
    },

    dblClickStatus : function(component, event, helper) {
        var arr = event.srcElement.name.split("~~");
        helper.initStatusUpdate(component, arr);
    },

    saveStatusChange : function(component, event, helper) {
        //STORY S-98171 : Added if..else... - Santosh
		if(component.get("v.submittal").RecordType.DeveloperName != 'Proactive') {

			if(component.get("v.submittal").Opportunity.Req_OFCCP_Required__c && 
				!component.get("v.submittal").Has_Application__c && 
				((component.get("v.newStatus") == 'Submitted') || (component.get("v.newStatus") == 'Interviewing') || (component.get("v.newStatus") == 'Offer Accepted'))) {
				helper.showError(component, "Req has OFCCP flag and requires an Application. No Application is found.  Talent cannot be submitted.", "Error", "Error");
			} else {
				helper.saveStatusUpdate(component);
			}  
		} else {
			helper.saveStatusUpdate(component);
		}      
    },

    cancelStatusChange: function(component, event, helper) {
       component.set("v.editId", '');
    },

	selectStatus : function(component, event, helper) {
		component.set("v.oppModalFlag", false);
		helper.selectStatus(component);
	},

	handleChangeCancel : function(component, event, helper) {
		helper.resetUpdateAttributes(component);
		component.set("v.reloadData", true);
	},

	handleChangeSave : function(component, event, helper) {
	   if(event.getParam("contactId") === component.get("v.recordId") ){ 
	     helper.resetUpdateAttributes(component);
		 component.set("v.reloadSubmittals", true);
	   }
		
	},

	onReloadSubmittals : function(component, event, helper) {
		if (component.get("v.reloadSubmittals") === true) {
			component.set("v.reloadData", true);
		}
	},
	openModalConvertToOpp : function(component, event, helper) {
		if(!component.get("v.isApplicationCard")) {
			component.find("overlayLib").notifyClose();
			var sHelp = component.find("submittalHelper");
			//var self = this;
			sHelp.selectStatus(function(response) {
				}, event.getParam("submittalId"), event.getParam("targetStatus"), event.getParam("submittal"), component.get("v.isApplicationCard")
			);
		}
	},
	openCreateEnterpriseReq : function(component, event, helper){
        //Added w.r.t Story S-177138
        var ReqComponent="c:CreateEnterpriseReq";
        var tekGroupAccess = component.get("v.TEKPublicGroupAccessFlag");
        var aerotekGroupAccess = component.get("v.AerotekPublicGroupAccessFlag");
        if(component.get("v.runningUser.CompanyName") == "AG_EMEA"){
            ReqComponent="c:CreateEnterpriseEmeaReq";
        }
        //if(component.get("v.runningUser.CompanyName") == "TEKsystems, Inc." && tekGroupAccess){
        if(component.get("v.runningUser.CompanyName") == "TEKsystems, Inc."){
            ReqComponent = "c:CreateEnterpriseTekReq";
        }
        //if(component.get("v.runningUser.CompanyName") == "Aerotek, Inc" && aerotekGroupAccess){
        if(component.get("v.runningUser.CompanyName") == "Aerotek, Inc"){
            ReqComponent = "c:CreateEnterpriseAerotekReq_New";
        }
        /*else{
            ReqComponent="c:CreateEnterpriseReq";
        }*/
        
        let oppModalFlag = component.get("v.oppModalFlag");
        if(!component.get("v.isApplicationCard") && !oppModalFlag) {
                component.set("v.oppModalFlag", true);
                let modalBody;
                $A.createComponent(ReqComponent, {
                        "recordId": event.getParam("submittal").Id, 
                        "proactiveSubmittalStatus":event.getParam("targetStatus"),
                        "modalPromise": component.getReference('v.modalPromise')
                    },
                    function(content, status) {
                        if(status === "SUCCESS") {
                            modalBody = content;
                            let modalPromise = component.find('overlayLib').showCustomModal({
                                body : modalBody,
                                 cssClass: "slds-modal_large",
                                closeCallback : function() {
                                        helper.resetUpdateAttributes(component);
                                        component.set("v.reloadData", true);
                                }
                            });
                            component.set('v.modalPromise', modalPromise);
                            component.set("v.oppModalFlag", false);	//D-17220
                        }
                    }
                );
			}                   			
	},
	createProactiveSubmittal : function(cmp,event,helper){
        
        helper.navigateToProactiveSubmittal(cmp, event);
    }
})