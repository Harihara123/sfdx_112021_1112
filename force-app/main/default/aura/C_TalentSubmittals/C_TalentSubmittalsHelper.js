({
    getResults : function(cmp,helper){
		var isApplicationCard = cmp.get("v.isApplicationCard");
		var listView = cmp.get("v.viewList");
		if(isApplicationCard){
			cmp.set("v.viewListIndex", 1); 
		}
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var soql = listView[indexNo].soql;

            if(soql){
                if(soql != ''){
                    soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
  
                        var apexFunction = "c.getRows";

                        var params = {
                            "soql": soql,
                            "maxRows": cmp.get("v.maxRows")
                        };

                        cmp.set("v.loadingData",true);
                        var actionParams = {'recordId':recordID};
                        if(params){
                            if(params.length != 0){
                             Object.assign(actionParams,params);
                            }
                        }

                        var bdata = cmp.find("bdhelper");
                            bdata.callServer(cmp,'','',apexFunction,function(response){
							cmp.set("v.records", response);
                            cmp.set("v.loadingData",false);
                        
                            if(listView[indexNo].additional){
                                cmp.set("v.additional",listView[indexNo].additional);
                            }
							// Setting to false to mark the data as refreshed.
							cmp.set("v.reloadSubmittals", false);
                            
                        },actionParams,false);
                    }
            }
			else{
                    var apexFunc = listView[indexNo].apexFunction;
                    if(apexFunc){
                        var params = listView[indexNo].params;
                        this.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                    }
                }
            }
	},

    getResultAndCount : function(cmp){
		var listView = cmp.get("v.viewList");
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var soql = listView[indexNo].soql;
			var countSoql = listView[indexNo].countSoql;
            soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
			countSoql = countSoql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
            
			var apexFunction = "c.getRowAndCount";
            var params = {
                "soql": soql,
				"countSoql" : countSoql,
                "maxRows": cmp.get("v.maxRows")
            };

            cmp.set("v.loadingData",true);
			cmp.set("v.loadingCount",true);
            var actionParams = {'recordId':recordID};
            if(params){
                if(params.length != 0){
                    Object.assign(actionParams,params);
                }
            }

			
            var bdata = cmp.find("bdhelper");
            bdata.callServer(cmp,'','',apexFunction,function(response){
                cmp.set("v.records", response.records);
				cmp.set("v.recordCount", response.count);
                cmp.set("v.loadingData",false);
				cmp.set("v.loadingCount",false);
                        
                if(listView[indexNo].additional){
                    cmp.set("v.additional",listView[indexNo].additional);
        }
				// Setting to false to mark the data as refreshed.
				cmp.set("v.reloadSubmittals", false);            
            },actionParams,false);
        }
    },

    getResultCount : function(cmp){
        var listView = cmp.get("v.viewList");
		var isApplicationCard = cmp.get("v.isApplicationCard");
		if(isApplicationCard){
			cmp.set("v.viewListIndex", 1); 
		}
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.recordId");
            var countSoql = listView[indexNo].countSoql;
			
            if(countSoql){
                 if(countSoql != ''){
                    countSoql = countSoql.replace("%%%parentRecordId%%%","'"+recordID+"'")
                    var params = {"soql": countSoql};
                    cmp.set("v.loadingCount",true);
                    
                    var bdata = cmp.find("bdhelper");
                        bdata.callServer(cmp,'','','getRowCount',function(response){
                            cmp.set("v.recordCount", response);
                            cmp.set("v.loadingCount",false);
                        
                    },params,false);

                 }else{
                    cmp.set("v.loadingCount",false);
                 }
            }
        }
        
    },

    updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },

    refreshList : function(cmp) {
        // cmp.set("v.reloadData", true);
		cmp.set("v.reloadSubmittals", true);
    },

    getAddressString : function(fieldList) {
        var address = '';
        if (fieldList && fieldList.length > 0) {
            address = fieldList[0];
            for (var i = 1; i < fieldList.length; i++) {
                address = (fieldList[i] && fieldList[i].length > 0 ? (address && address.length > 0 ? (address + ", " + fieldList[i]) : fieldList[i]) : address); 
            }
        }
        return address;
    },

    initStatusUpdate : function(component, arrData) {
		
        component.set("v.editIndex", arrData[0]);
        component.set("v.editId", arrData[1]);
        component.set("v.submittal", component.get("v.records")[arrData[0]]);

		var opco;
		if(arrData[5] != 'Proactive') {
			opco = arrData[3];
		} else {
			opco = 'Proactive';
		}

		var sHelp = component.find("submittalHelper");
        sHelp.getWorkflowStatuses(function(response) {
                component.set("v.statusList", response);
                component.set("v.newStatus", arrData[2]);
            }, {
               // "opco" : arrData[3],
                 "opco" : opco,
				"status" : arrData[2],
                "isAtsJob" : arrData[4] === "true"
            }
        );
    },

    saveStatusUpdate : function(component) {

        if (component.get("v.newStatus") === 'Offer Accepted' && !this.validLocation(component)) {
            this.showError(
                component,
                'The talent record is missing location information. Please populate the location information before proceeding.',
                'error',
                'ERROR')
        } else {
            var spinner = component.find('spinner');
            $A.util.removeClass(spinner, "slds-hide");
            $A.util.addClass(spinner, 'slds-show');

            var sHelp = component.find("submittalHelper");
            var self = this;
            sHelp.updateStatus(function(response) {
                    self.refreshList(component);
                    self.resetUpdateAttributes(component);
                    //S-78898 - To Refresh Activity Timeline - Karthik
                    var reloadEvt = $A.get("e.c:E_TalentActivityReload");
                    reloadEvt.fire();
					$A.util.removeClass(spinner, 'slds-show');
					$A.util.addClass(spinner, 'slds-hide');
                }, component.get("v.editId"), component.get("v.newStatus")
            );

           /* $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide');*/
        }



    },

	resetUpdateAttributes : function(component) {
		component.set("v.editIndex", -1);
		component.set("v.editId", "");
		component.set("v.statusList", []);
		component.set("v.newStatus", "");
	},

	selectStatus : function(component) {
        let self = this;
        let type = component.get("v.newStatus")
        if (['Interviewing','Add New Interview','Edit Interview'].includes(type) ) {
            var modalBody;
			
			$A.createComponent("c:C_SubmittalInterviewModal",
                {
                    submittal: component.getReference('v.submittal'),
                    modalPromise: component.getReference('v.modalPromise'),
                    editMode: type === 'Edit Interview',
					targetStatus:type
                },
                function(content, status) {
                    if (status === "SUCCESS") {
                        modalBody = content;
                        let modalPromise = component.find('overlayLib').showCustomModal({
                            header: $A.get("$Label.c.ATS_INTERVIEW_MANAGEMENT"), //$Label.c.ATS_INTERVIEW_MANAGEMENT
                            body: modalBody,
                            showCloseButton: true,
                            cssClass: "slds-modal_medium",
                            closeCallback: function() {
                                self.resetUpdateAttributes(component);
                                component.set("v.reloadData", true);
                            }
                        })
                        component.set('v.modalPromise', modalPromise);
                    }
                });
        } else {
            if (type === 'Offer Accepted' && !this.validLocation(component)) {
			    this.showError(
                    component,
                    'The talent record is missing location information. Please populate the location information before proceeding.',
                    'error',
                    'ERROR')
            } else {
				//if(component.get("v.submittal").RecordType.DeveloperName == 'Proactive') {
					var sHelp = component.find("submittalHelper");				
					sHelp.selectStatus(function(response) {
					   }, //component.get("v.editId"), component.get("v.newStatus"), component.get("v.submittal")
						 component.get("v.editId"), component.get("v.newStatus"), component.get("v.submittal"), component.get("v.isApplicationCard")
					);
				//}
				/*else { 
					let modalBody;
					$A.createComponent("c:CreateEnterpriseReq", {
							"recordId": component.get("v.editId"), 
							"proactiveSubmittalStatus":component.get("v.newStatus"),
							"modalPromise": component.getReference('v.modalPromise')
						},
						function(content, status) {
							if(status === "SUCCESS") {
								modalBody = content;
								let modalPromise = component.find('overlayLib').showCustomModal({
									body : modalBody,
									closeCallback : function() {
										 self.resetUpdateAttributes(component);
										 component.set("v.reloadData", true);
									}
								});
								component.set('v.modalPromise', modalPromise);
							}
						}
					);
				}*/
                
            }

        }
	},
	openCreateEnterpriseReq : function(cmp,ctrl,helper){
		
		let modalBody;
		$A.createComponent("c:CreateEnterpriseReq", {
				"recordId": component.get("v.editId"), 
				"proactiveSubmittalStatus":component.get("v.newStatus"),
				"modalPromise": component.getReference('v.modalPromise')
			},
			function(content, status) {
				if(status === "SUCCESS") {
					modalBody = content;
					let modalPromise = component.find('overlayLib').showCustomModal({
						body : modalBody,
						closeCallback : function() {
								self.resetUpdateAttributes(component);
								component.set("v.reloadData", true);
						}
					});
					component.set('v.modalPromise', modalPromise);
				}
			}
		);
				
	},
    
    showError : function(component, message, messageType, theTitle){
        var title = theTitle;
        var errorMessage = message;
        var theType = messageType;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: theType
        });
        toastEvent.fire();
    },
    validLocation: function (cmp) {
        const talent = cmp.get('v.talentData');
        // const location = ['Street', 'Country', 'State', 'PostalCode', 'City']
        //
        let returnBoolean = true;
        if (!talent.fields) {
            returnBoolean = false;
        } else if(!(talent.fields.MailingCountry.value != null && (talent.fields.MailingState.value != null || talent.fields.MailingPostalCode.value != null))) {
            	returnBoolean= false;
            /*if (!talent.fields.MailingCountry.value && !talent.fields.MailingState.value) {
                            returnBoolean = false;
            }
            if (!talent.fields.MailingPostalCode.value && !talent.fields.MailingCountry.value) {
                            returnBoolean = false;
            }*/
        }
        //
        // location.forEach(loc => {
        //     if (talent.fields && !talent.fields[`Mailing${loc}`].value) {
        //         returnBoolean = false;
        //     }
        // })
        //
     return returnBoolean;

    },
	navigateToProactiveSubmittal : function(cmp, event) {
		var orderId = event.target.name;
		var pageReference = {
			type: 'standard__component',
			attributes: {
				"componentName" : "c__C_ProactiveSubmittalContainerForLWC"
            },
            state: {
                "c__recordType": "Order",
                "c__recordId": orderId, 
				"c__talentId": cmp.get("v.AccountId"),
                "c__runningUser": cmp.get("v.runningUser")
            }
        };
		cmp.set("v.pageReference", pageReference);
		
		var navService = cmp.find("navService");
		var pageReference = cmp.get("v.pageReference");
		event.preventDefault();
		navService.navigate(pageReference);
		this.delayedRefresh();
	},
      delayedRefresh : function(milliseconds){
		let ms = milliseconds || 100;
		window.setTimeout($A.getCallback(function(){
			$A.get('e.force:refreshView').fire();
		}),ms);
	},
    checkTEKGroupPermission:function(cmp)
    {
        console.log("checkTEKGroupPermission:");
        var action = cmp.get("c.checkTEKCreateGroupAccess");
		action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") 
            {
                var resFlag = response.getReturnValue();
                console.log("resFlag:"+resFlag);
                if(resFlag)
                {
                    cmp.set("v.TEKPublicGroupAccessFlag",true);
                    console.log("resFlag1:"+resFlag);
                }
            }
        });
        $A.enqueueAction(action);
    },
    checkAerotekGroupPermission:function(cmp)
    {
        console.log("checkAerotekGroupPermission:");
        var action = cmp.get("c.checkAerotekCreateGroupAccess");
		action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") 
            {
                var resFlag = response.getReturnValue();
                console.log("resFlag:"+resFlag);
                if(resFlag)
                {
                    cmp.set("v.AerotekPublicGroupAccessFlag",true);
                    console.log("resFlag1:"+resFlag);
                }
            }
        });
        $A.enqueueAction(action);
    }
})