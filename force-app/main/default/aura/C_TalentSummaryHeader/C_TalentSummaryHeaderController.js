({
   handleThsEvent: function(cmp, event, helper) {
    let CStatus = event.getParam("candidateStatus");    
    cmp.set('v.thsData', CStatus);  
   },
   copyURL: function(cmp) {
    let input = cmp.find('cURL').getElement();
    input.focus();
    input.select();
    var copyState = document.execCommand('copy');   
   }, 
   	copyTalentId : function(cmp) {
    let input = cmp.find('cTalentId').getElement();
    input.focus();
    input.select();
    var copyState = document.execCommand('copy');   
   }, 
   	

   toggleCorePopover: function(cmp, event, helper) {
   if ((event.type === 'mouseenter' || 'focus') && cmp.get('v.corePopoverState') === 'slds-hide') {
		cmp.set('v.corePopoverState', 'slds-show');
   } else if(event.type === 'mouseleave'){
		helper.hidecorePopover(cmp,event);	
   }
   else if(event.type=='blur')
   {
	if(!event.currentTarget.contains(event.relatedTarget))
		helper.hidecorePopover(cmp,event);
   }

//	if (event.type === 'mouseover' && cmp.get('v.corePopoverState') === 'slds-hide') {
//		cmp.set('v.corePopoverState', 'slds-show');
//		event.stopPropagation();
//	} else {
//		cmp.set('v.corePopoverState', 'slds-hide');
//		event.stopPropagation();
//	}
//	event.stopPropagation();		
   },

   Tabhandler : function(component,event,helper){
		var key = 'which' in event ? event.which : event.keyCode;

		if (event.shiftKey && event.keyCode == 9){
			var relatedContact=document.getElementById("relatedContact");
			relatedContact.focus();
			event.preventDefault();
		}
		else if (key == 9) { 
			helper.hidecorePopover(component,event);
			var status=document.getElementById("tlp-header-status");
			status.focus();
		}

	
   },

   doInit : function(cmp, event, helper) {				
       helper.splitViewDisplay(cmp, event);
	   helper.checkTFOGroupMember(cmp,event);   //S-224383
	   helper.checkTFODRZBoardGroup(cmp,event);
	            var conrecord = cmp.get("v.contactRecordFields");
		
					
				
	            //Andy: Setting a label for Deactivate/Activate Talent Action
                helper.checkForInactive(cmp, conrecord.Account.Talent_Ownership__c);

                let nameLength = conrecord.Name.length + (conrecord.Preferred_Name__c?conrecord.Preferred_Name__c.length+2:0) 
                if(nameLength > 35) {                       
                    helper.fontSize(cmp, nameLength)
                }

                cmp.set('v.currentURL', window.location.href)

                helper.tlpAddress(cmp, conrecord);          

                cmp.set("v.talentId",conrecord.AccountId);

                var talentStreetCityAddress = '';

                talentStreetCityAddress = helper.appendAddress(talentStreetCityAddress, conrecord.MailingStreet);
                talentStreetCityAddress = helper.appendAddress(talentStreetCityAddress, conrecord.MailingCity);

                cmp.set("v.addressStreetCity",talentStreetCityAddress);
                
                var talentStateCountryZipAddress = '';
                talentStateCountryZipAddress = helper.appendAddress(talentStateCountryZipAddress, conrecord.MailingState);
                talentStateCountryZipAddress = helper.appendAddress(talentStateCountryZipAddress, conrecord.MailingPostalCode);
                talentStateCountryZipAddress = helper.appendAddress(talentStateCountryZipAddress, conrecord.Talent_Country_Text__c);

                cmp.set("v.addressStatePostlCodeCountry",talentStateCountryZipAddress); 
               
                cmp.set("v.ldsAccountCallFlag",true);
                var isDnuAccessible = helper.checkDnuAccess(cmp);
       			var isTalentDeactivateAccessible = helper.checkTalentDeactivateAccess(cmp);

                var groupAPermissionsAction = cmp.get('c.checkGroupPermission');
                groupAPermissionsAction.setCallback(this, function(response) {
                    var response = response.getReturnValue();
                    cmp.set("v.userInGroups",response);             
                });
                $A.enqueueAction(groupAPermissionsAction);  
    }, 
	
    //S-95929 Rajeesh added for pipeline functionality....
    checkPipeline: function(component,event,helper){
        /*********************************************************************************************************************
        ***Rajeesh S-95929 pipeline feature:
           When a user chooses to add this talent to his pipeline, user's alias is stamped in one of the Target_Account_Stamp fields (0-5) on the account with :
           in front and back.
           Each stamp can accomodate 255/10 ~ 25 (8+2 seperators) a minimum of - 25x6=150 different users. 
           if more people try to add this contact to pipeline, then code will remove the oldest assignment first. (from the left side of the string that is constructed.)
           This design will not require an expensive query on junction object to figure out whether a talent is part of current users' pipeline to show the dynamic menu item 
           in landing page/summary header.
           Use LDS to read and update.
        */
        //rajeesh code refactoring 8/29/2018

        

        var plUsers = helper.getPLString(component,event);
        var usrPplSoftId= component.get("v.runningUser.Peoplesoft_Id__c");
        if($A.util.isEmpty(plUsers)){
            component.set("v.inPipeline","false");
        }
        else if(plUsers.indexOf(usrPplSoftId)>0){
            component.set("v.inPipeline","true");
        }
        else{
            component.set("v.inPipeline","false");
        }
        //console.log('inpipeline?:'+component.get("v.inPipeline"));
    },
    //S-95929 Rajeesh added for pipeline functionality.
    addToPipeline: function(component,event,helper){
        helper.appendToPipeline(component,event);
    },
    //S-95929 Rajeesh added for pipeline functionality.
    removeFromPipeline: function(component,event,helper){
        helper.removeFromPipeline(component,event);
    },
    showDissociateContactModal: function(component,event,helper){
        var modalBody;
        $A.createComponent("c:C_DissociateRelatedContactModal", {"recordId":component.get('v.recordId'), "item":"Client"},
            function(content, status) {
                if(status==="SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({                      
                        body : modalBody,
                        closeCallback : function() {
                            //alert('Modal Closed');
                            console.log('inside dissociate contact modal close');
                        }
                    });
                }
            }
        );
    },
    showRelateToContactModal : function(component, event, helper) {
        var appEvent = $A.get("e.c:E_RelateToContactModal");
        appEvent.setParams({"recordId": component.get("v.talentId")});
        appEvent.fire();
    },
    showAddToCallSheetModal : function(component, event, helper) {
        var appEvent = $A.get('e.c:E_AddToCallSheet');
        appEvent.fire();
    },
    doInitAccount : function(cmp, event, helper) {     
        //Rajeesh 8/29/2018 code merging issues- refactoring
        //call pipeline once runningUser is set.
        var runningUser = cmp.get('v.runningUser');
        if(!$A.util.isEmpty(runningUser)){
            cmp.checkPipeline();
        }
         var pageLoadedFlag = cmp.get("v.pageLoadedFlag");
                if (!pageLoadedFlag) {
                            cmp.set("v.pageLoadedFlag",true);
                            var action = cmp.get("c.getUserInfoTalentHeader");
                            action.setStorable();
                            action.setCallback(this, function(response) {
							//S-226585 - added 3 new Opco IND,SJA,EAS by Debasis
                                    var results = response.getReturnValue();
                                    if(results.usr.OPCO__c === 'ONS' 
										|| cmp.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aerotek")  
										|| cmp.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Aston_Carter")  
										|| cmp.get("v.runningUser.OPCO__c") === $A.get("$Label.c.Newco")) {
                                        cmp.set('v.isTextConsentAllowed', results.hasUserConsentTextAccess);
                                        //cmp.set('v.userHasSMS360Access',results.hasUserSMSAccess);                                      
                                    }
                                    cmp.set("v.runningUser", results.usr);
                                    cmp.set("v.UserCategory", results.userCategory);

									cmp.checkPipeline();//Rajeesh refactor                                	
                                    
                                	var traineeUserTitle	= 'recruiter trainee';
                                    var giveAccess = false; 
                                    if(results.usr.Profile.Name == 'System Administrator' || results.usr.Profile.Name == 'IS Admin')
                                        giveAccess = true;
                                    if(giveAccess == false && (results.userOwnership == 'AP' || results.userOwnership == 'MLA' || (results.userOwnership == 'RPO' && results.usr.Title != 'Recruitment Sourcing Manager' && (results.usr.Profile.Name == 'Single Desk 1' || results.usr.Profile.Name == 'ATS_Recruiter')) || (results.userOwnership == 'ONS' && results.usr && results.usr.Title && results.usr.Title.toLowerCase() == traineeUserTitle))){
                                        cmp.set("v.disableMerge", true);
                                    } else{
                                        cmp.set("v.disableMerge", false);
                                    } 
                     
                                    //results.haslinkedInSync will be controlled by Talent Role to Ownership metadata
                                    cmp.set("v.hasLinkedIn", results.haslinkedInSync);
                                    
                              });
                              $A.enqueueAction(action);
                  }


                   /* for Story S-86673 redirection fromTalent merge show toast message */
                if(window.location.href.indexOf("showToast") > -1) {
                    
                        var oldUrl = window.location.href;
                        var newUrl = oldUrl.replace("?showToast=true","");
                        helper.TalentMergeToast(cmp);
                        window.history.pushState({path:newUrl},'',newUrl); // remove showToast Param from URl
                        
                    }
                /*End of Story S-86673*/
                 var rwsId = cmp.get('v.accountRecordFields.Source_System_Id__c');
					
					if(rwsId != null && rwsId.includes('R.')){
					var slicedRws = rwsId.slice(2);
					
					cmp.set("v.rwsId",slicedRws);
					}else{
					cmp.set("v.rwsId",null);
					}
     }, 


  renderTalentStatusTags : function(component, event, helper) {     

     component.set("v.renderTalentStatusFlag",true);

  }, 

   displayHideRelatedContactPopup : function(component, event, helper) {        
        
        var isRelatedConActyLoaded = component.get ("v.isRelatedContactLoaded");
        if (isRelatedConActyLoaded) {
            var toggleText = component.find("tooltipRelatedContactId");
            $A.util.toggleClass(toggleText, "content-toggle"); 
        } else {
            helper.getRelatedContactActivity(component);
        }
        
    },
    
    addEngagmentRules : function(component, event, helper) {

        var actionParams = {'accountId': component.get("v.talentId")};
        var bdata = component.find("bdhelper");
        bdata.callServer(component,'ATS','TalentHeaderFunctions','getCandidateCurrentEmployer',function(response){
                var appEvent = $A.get("e.c:E_TalentEngagementRuleAdd");
                appEvent.setParams({ "recordId" : response.Current_Employer__c,
                                    "name":  response.Current_Employer__r.Name,
                                    "street":  response.Current_Employer__r.BillingStreet,
                                    "city":  response.Current_Employer__r.BillingCity,
                                    "state": response.Current_Employer__r.BillingState,
                                    "country":  response.Current_Employer__r.BillingCountry});
                appEvent.fire();
        },actionParams,false);
    },
    

    //added by akshay for code merge start
    editTalentSummary :function(cmp, event, helper){
        var recordID = cmp.get("v.talentId");
        var contID = cmp.get("v.recordId");
        var urlEvent = $A.get("e.c:E_TalentSummEdit");
        urlEvent.setParams({
            "recordId": recordID,
            "recId": contID,
            "source": "Profile"
        });
        urlEvent.fire(); 
    },
    //added by akshay for code merge end
    editCandidateContact : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");//cmp.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            //Neel summer 18 URL change->
            //"url": "/one/one.app#/n/Add_Edit_Talent?id=" + recordID
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Add_Edit_Talent?id=" + recordID  
        });
        urlEvent.fire();
    },
    
    mergeTalentRecord : function(component, event, helper) {

        // if(component.get("v.disableMerge") == true ){
        //     return;
        // }

        var talentCId = component.get("v.accountRecordFields.Talent_Id__c");

       var pageReference = {
            type: 'standard__component',
            attributes: {
                "componentName" : "c__C_TalentMergeComparison"
            },
            state: {
                "c__masterID": talentCId,
            }
        };
        component.set("v.pageReference", pageReference);
        
        var navService = component.find("navService");
        var pageReference = component.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);

/*        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Talent_Merge?masterID=" + talentCId ,
            "isredirect":'true'
        });
        urlEvent.fire();
*/    }
    ,linkCandidateToJob : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");//cmp.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        var reqModalOpen = $A.get("e.c:E_ReqSearchModalOpen");// S-82659 modal for req search
        
        var profile = cmp.get("v.runningUser.Profile.Name");
        if((profile == 'ATS_Recruiter')){
            urlEvent.setParams({
                "url":"/apex/c__Search_Jobs?accountId="+ recordID
            });  
            urlEvent.fire();
        }else{

        // S-82659 open req search modal
          reqModalOpen.setParams({"contactId": cmp.get("v.recordId"), "talentId": recordID,
                                  "record":cmp.get("v.contactRecordFields"), "runningUser": cmp.get("v.runningUser"),"category":cmp.get("v.UserCategory")});   
          reqModalOpen.fire();

        /* Previous flow for redirecting to req search. Replaced by changes from S-82659 

            urlEvent.setParams({
                //Neel summer 18 URL change->
                //"url":"/one/one.app#/n/Search_Reqs?accountId="+ recordID
                "url": $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Search_Reqs?accountId="+ recordID
            });
        urlEvent.fire(); */
        }
    },
    linkOwnership : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/c__Candidate_Ownership_Management?accountId=" + recordID
        });
        urlEvent.fire();
    },
    
    viewLinkedIn :function(cmp, event, helper) {
        var recordID = cmp.get("v.linkedInUrl");
        if (recordID != ''){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": recordID
            });
            urlEvent.fire();
        }
    },
    
    //D-05950 - Added by Karthik
    prefPhoneLink : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.Preferred_Phone_Value__c"));   
    },

    callMobilePhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.MobilePhone"));
    }, 
    
    callHomePhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.HomePhone"));
    }, 
    
    callPhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.Phone"));
    },
    
    callOtherPhone : function(cmp, event, helper) {
        helper.fireURLEventPhone(cmp, cmp.get("v.contactRecordFields.OtherPhone"));
    }, 

    emailMain : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Email"));
    },
    
    emailAlt : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Other_Email__c"));
    },
    
    emailWork : function(cmp, event, helper) {
        helper.fireURLEventEmail(cmp, cmp.get("v.contactRecordFields.Work_Email__c"));
    },
    //D-05950 - End 
    
    backToSearch : function(cmp, event, helper) {
        
        if (cmp.get("v.userInGroups.isInGroupB") === true) {
            history.back();
        } else {

            var recordID = cmp.get("v.talentId");
            var urlEvent = $A.get("e.force:navigateToURL");
            var apexFunction = "c.getRecord";
            var params = {
                "soql": "SELECT Id, Search_Params__c, Search_Params_Continued__c FROM User_Search_Log__c WHERE User__c = \'"+ cmp.get("v.runningUser").Id +"\' AND Type__c = \'AllLastSearches\' ORDER BY LastModifiedDate DESC"
            };
        
            var bdata = cmp.find("bdhelper");
            bdata.callServer(cmp,'','',apexFunction,function(response){
                if(response != undefined){
                    var allSearches = helper.joinSearchParams(response.Search_Params__c, response.Search_Params_Continued__c);

                    var lst = JSON.parse(allSearches);
                    if (lst.searches.length > 0) {
                        var lastSrch = JSON.parse(lst.searches[0]);
                        var url = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/ATS_CandidateSearchOneColumn?c__execIndex=0&c__savedType=AllLastSearches&c__type=" + lastSrch.type;
                    
                        if (lastSrch.type === "R" || lastSrch.type === "C2R") {
                            url = $A.get("$Label.c.CONNECTED_Summer18URL")+"/n/Search_Reqs?c__execIndex=0&c__savedType=AllLastSearches&c__type=" + lastSrch.type;
                        }

                        urlEvent.setParams({
                            "url": url
                        });
                        urlEvent.fire();
                    }
                }           
            },params,false);

        }
        
    },
    
    automatchToTalent : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");//cmp.get("v.recordId");

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": $A.get("$Label.c.CONNECTED_Summer18URL")
                    + "/n/ATS_CandidateSearchOneColumn?c__matchTalentId=" + recordID
                    + "&c__contactId=" + cmp.get("v.contactRecordFields.Id")
                    + "&c__srcName=" + cmp.get("v.contactRecordFields.Name")
                    + "&c__noPush=true"  //For Browser Back - Karthik
        });
        urlEvent.fire();
    },
    displayEditEndDate : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");
        var urlEvent = $A.get("e.c:E_CandidateHeaderDisplayEditEndDate");
        urlEvent.setParams({
            "recordId": recordID
        });
        urlEvent.fire(); 
    }, 
    addContactToList : function(cmp, event, helper) {
        var recordID = cmp.get("v.recordId");
        var urlEvent = $A.get("e.c:E_MassAddToContactList");
        urlEvent.setParams({
            "recordId": recordID
        });
        urlEvent.fire(); 
    },

    refreshTalentHeader : function(cmp, event, helper) {
        cmp.find('contactRecordData').reloadRecord(true);
        cmp.find('accountTalentData').reloadRecord(true);
    }, 

    talentSummaryUpdateLMD : function(cmp, event, helper) {
        cmp.find('accountTalentData').reloadRecord(true);
    },

    talentSummaryUpdateLMDForResumeUpload : function(cmp, event, helper) {
        setTimeout($A.getCallback(function(){
            if (cmp.find('accountTalentData')) {
            cmp.find('accountTalentData').reloadRecord(true);
            }
        }), 10000); 
    }, 
    SendJobPostingInvite : function(cmp, event, helper) {
        var recordID = cmp.get("v.talentId");
        if(cmp.get("v.contactRecordFields.Email") || cmp.get("v.contactRecordFields.Other_Email__c") || cmp.get("v.contactRecordFields.Work_Email__c")){
            var jobPostingModalOpen = $A.get("e.c:E_JobPostingModalEvent");
        
          jobPostingModalOpen.setParams({ "talentId": recordID,
                                  "record":cmp.get("v.contactRecordFields"), "runningUser": cmp.get("v.runningUser")});   
          jobPostingModalOpen.fire();
        }else{
          helper.displayNoEmailErrorToast('Talent must have at least one email to Initiate Job Posting Invite');
        }
    
    },
    toggleEditPopover: function (component, event, helper) {
        component.set("v.showLocationPopOver", "true");
    },

    hideEditPopOverHandler: function (component, event, helper) {
        component.set("v.showLocationPopOver", "false");
    },
    showConsentPreferencesModal : function (component, event, helper) { 
        var modalBody;
        $A.createComponent("c:C_ConsentPreferencesModal", {"modalOpeningSource":"TalentLandingPage", "contactID":component.get("v.recordId"), "recordId":component.get("v.recordId"), "isTextConsentAllowed":component.get("v.isTextConsentAllowed")},
            function(content, status) {
                if(status==="SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({                      
                        body : modalBody,
                        closeCallback : function() {
                            //alert('Modal Closed');
                        }
                    })
                }
            }
        );

    },

    editTalentSummaryNew :function(cmp, event, helper){
		helper.displayTalentSummaryModalNew(cmp, event, helper)
       /* var recordID = cmp.get("v.talentId");
        var contID = cmp.get("v.recordId");
        var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        urlEvent.setParams({
            "recordId": recordID,
            "recId": contID,
            "source": "Profile"
        });
        urlEvent.fire(); */
    },
    dnuModal: function (cmp, e, h) {
        $A.createComponent("c:C_DoNotUseModal", {
            recordId: cmp.get("v.recordId"),
                contactObject:cmp.get("v.contactRecordFields"),
                dnuModalPromise: cmp.getReference('v.dnuModalPromise')
            },
            function(content, status) {
                if(status==="SUCCESS") {
                    const modalBody = content;
                    const modalPromise = cmp.find('overlayLib').showCustomModal({
                        header: 'DO NOT USE FORM',
                        body : modalBody,
                        closeCallback : function() {
                            //alert('Modal Closed');
                        }
                    })
                    cmp.set('v.dnuModalPromise', modalPromise);
                }
            }
        );
    },
    /*SendSMS :function(cmp,event,helper){
        var aerotekMobileOptout = cmp.get("v.contactRecordFields.PrefCentre_Aerotek_OptOut_Mobile__c");
        if(aerotekMobileOptout && aerotekMobileOptout === 'No') {       
            var returnURL="/lightning/r/Contact/"+cmp.get("v.recordId");
            var pageName="tdc_tsw__SendSMS_SLDS?id";
            var modalBody;
            $A.createComponent("c:SendSMSContactComp", {
                "recordIdd": cmp.get("v.recordId"),
                "returnURL": returnURL,
                "pageName": pageName
             },
             function(content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    cmp.find('SMSoverlayLib').showCustomModal({
                           body: modalBody,
                           showCloseButton: true,
                           cssClass: "mymodal",
                       })
                   }
                 else{
                     var t ='test';
                     console.log('inside other- error');
                 }
               });
        }
        else if(aerotekMobileOptout && aerotekMobileOptout === 'Yes') { 
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                type: 'error',
                message: $A.get("$Label.c.SMS_Unsubscribe")
            });
            toastEvent.fire();
        }
        else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                type: 'error',
                message: $A.get("$Label.c.ATS_SMS_OBTAIN_CONSENT")
            });
            toastEvent.fire();
        }
    },
    */
    demoLookup : function(component,event,helper){
        console.log("LOAD LWC");
        var modalBody;
        $A.createComponent("c:C_DynamicLookUp", {},
            function(content, status) {
                if(status==="SUCCESS") {
                    modalBody = content;
                    component.find('demoLookUpOL').showCustomModal({                        
                        body : modalBody,
                        closeCallback : function() {
                            //alert('Modal Closed');
                        }
                    })
                }
            }
        );
    },
	createProactiveSubmittal : function(cmp,event,helper){
        helper.navigateToProactiveSubmittal(cmp, event);
    },
    toggleTalentActiveState: function(cmp, e, h) {
       h.toggleTalentActiveState(cmp, e);
    },

	// Monika -- S-189738 -- Focus back on source after G2 modal is closed/cancel button is hit 
	focusonSource:function(component, event, helper) {
		const fieldId = event.getParam("fieldIdToGetFocus");
		if(fieldId == null) return;
		helper.setFocusToField(component, event, fieldId);
    },
    //S-224383 
    showTalentFirstModal : function(component, event, helper){
        component.set("v.showTalentModalFlag", "true");        
    },
    closeTalentFirstModel : function (component, event, helper){
		component.set('v.drzBoardsForTfo', null);
        component.set('v.drzBoardId', null);
        component.set("v.showTalentModalFlag", "false");
    },
    createOpportunity : function (component, event, helper){
        component.set("v.showTalentModalFlag", "false");
		helper.createTalentFirstOpp(component);
    },
    handleDrzBoardsChange: function(cmp, e, h) {
        const data = e.getParams();
        cmp.set('v.drzBoardsForTfo', data.skills)    
        cmp.set('v.drzBoardId', data.skillsDataValues)           
    },
    handleDupeMsg: function(cmp, e, h) {
        h.showToast(`${e.getParams().val} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')        
    },
    handleSuggestedDrzBoardsChange: function(cmp, e, h) {
        const data = e.getParams();
        cmp.set('v.suggestedDrzBoards', data.pills);
    },
})