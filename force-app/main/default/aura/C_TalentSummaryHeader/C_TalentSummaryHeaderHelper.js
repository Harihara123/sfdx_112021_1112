({
    TalentMergeToast : function(component){
        var message ="{www.google.com}";
        
        var ToastType ="Success";
        var ToastTitle ="Your Talent records were Merged Succesfully!";
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'sticky',
            title: ToastTitle,
            message : message,
            messageTemplate: '{1}',
            messageTemplateData: ['Salesforce', {
                url: '/lightning/n/Talent_Merge',
                label: 'Click here to start another Merge',
            }],
            type: ToastType
        });
        toastEvent.fire();
    },
    fontSize: function(cmp, length) {
        let currentFontSize = cmp.get('v.fontSize');

        let reduceBy = 0.5;

        let newFontSize = currentFontSize - (reduceBy*(length - 35))
                    
        cmp.set('v.fontSize', newFontSize < 10 ?10:newFontSize);
    },
    tlpAddress: function(cmp, conrecord) {
                let USschema = ['City', 'State', 'PostalCode'];
                let nonUSSchema = ['Country', 'City', 'PostalCode']
                let tlpAddress = [];
                if (conrecord.MailingCountry === 'USA' || conrecord.MailingCountry === 'United States') {
                    for(let item of USschema) {
                        if (conrecord['Mailing'+item]) {
                            let obj = {
                                value: conrecord['Mailing'+item],
                                id: 'Mailing'+item
                            };
                            tlpAddress.push(obj);
                        }
                    }
                } else {
                    for(let item of nonUSSchema) {
                        if (conrecord['Mailing'+item]) {
                            let obj = {
                                value: conrecord['Mailing'+item],
                                id: 'Mailing'+item
                            };
                            tlpAddress.push(obj);
                        }
                    }           
                }
                    if(!tlpAddress && conrecord.MailingStreet) {
                        tlpAddress.push({
                            value: conrecord.MailingStreet,
                            id: MailingStreet
                        })
                    }
                    if (tlpAddress.length === 0) {tlpAddress = ['empty']}
                //console.log(tlpAddress)  
                cmp.set('v.cityStateZip', tlpAddress);      
    },
    getStreetCityAddress : function(cmp) {
        return getAddressString([cmp.get("v.contactRecordFields.MailingStreet"),
                                 cmp.get("v.contactRecordFields.MailingCity")]);
    },
    
    getStatePostlCodeCountryAddress : function(cmp) {
        return getAddressString([cmp.get("v.contactRecordFields.MailingState"),
                                 cmp.get("v.contactRecordFields.MailingPostalCode"),
                                 cmp.get("v.contactRecordFields.MailingCountry")]);
    },
    
    getAddress : function(cmp) {
        return getAddressString([cmp.get("v.contactRecordFields.MailingStreet"),
                                 cmp.get("v.contactRecordFields.MailingCity"),
                                 cmp.get("v.contactRecordFields.MailingState"),
                                 cmp.get("v.contactRecordFields.MailingPostalCode"),
                                 cmp.get("v.contactRecordFields.MailingCountry")]);
    },
    getCityStateZip: function(cmp) {
        return getAddressString([cmp.get("v.contactRecordFields.MailingCity"), cmp.get("v.contactRecordFields.MailingState"), cmp.get("v.contactRecordFields.MailingPostalCode")])
    },
    getAddressString : function(fieldList) {
        var address = '';
        if (fieldList && fieldList.length > 0) {
            address = fieldList[0];
            for (var i = 1; i < fieldList.length; i++) {
                address = (fieldList[i] && fieldList[i].length > 0 ? (address && address.length > 0 ? (address + ", " + fieldList[i]) : fieldList[i]) : address); 
            }
        }
        return address;
    },
    getRelatedContactActivity : function(cmp) {
        var relatedContactId  = cmp.get("v.contactRecordFields.Related_Contact__c");
        if(relatedContactId && relatedContactId != undefined && relatedContactId.length > 0) {
            
            var params = {"contactId": relatedContactId};
            var bdata = cmp.find("bdhelper");
            var self = this;
            bdata.callServer(cmp,'ATS','TalentHeaderFunctions','getRelatedActivity',function(response){
                if(cmp.isValid()){
                    self.constructRelatedContactLastActivity(cmp, response);
                }
            },params,false);
        } 
    }, 
    
    constructRelatedContactLastActivity : function(cmp, relatedContact){
        
        var activityList = [];
        
        var account = relatedContact[0].Account;
        var events = relatedContact[0].Events; 
        var tasks = relatedContact[0].Tasks; 
        
        if (events && events.length > 0) {
            activityList = events;
        }
        if (tasks && tasks.length > 0){
            
            activityList = activityList.concat(tasks);
            
        }
        
        
        if (relatedContact && relatedContact.length > 0) {
            
            var relatedConActivityHtml = "";
            
            var clientName 			= relatedContact[0].Name;
            var companyName 			= account.Name;
            var clientStatus			= relatedContact[0].Customer_Status__c;
            var clientDNC 			= account.Do_Not_Contact__c;
            
            var clientStatusLabel	= $A.get("$Label.c.ATS_CONTACT_STATUS");
            var clientDNCLabel 		= $A.get("$Label.c.ATS_DO_NOT_CONTACT");
            var companyNameLabel 	= $A.get("$Label.c.ATS_COMPANY_NAME");
            var activityLabel		= $A.get("$Label.c.ATS_ACTIVITY_LOG_HEADER");
            
            var RCMobileLabel		= $A.get("$Label.c.ATS_PHONE");
            var RCHomeLabel	     	= "Other Phone";
            var RCWorkLabel			= "Phone";
            var RCOtherLabel			= "OtherPhone";
            var RCEmailLabel			= $A.get("$Label.c.ATS_EMAIL");
            var RCWorkEmailLabel			= "Work Email"; 
            
            if (activityList && activityList.length > 0 ) {
                
                var activitiesDiv = "<div  class=\"tooltip__section activities__list slds-grid slds-wrap slds-grid--pull-padded\" >";
                activitiesDiv = activitiesDiv +   "<div class=\"bold slds-col slds-size--1-of-1 slds-p-horizontal--small\">" + activityLabel + "</div>";
                activityList.sort(this.compareActivityDate);
                activityList = activityList.slice(0, 5);
                for (var i = 0; i < activityList.length ; i++){
                    var dateLM = $A.localizationService.formatDate(activityList[i].LastModifiedDate, 'MM-DD-YYYY'); 
                    activitiesDiv = 
                        activitiesDiv + "<div class=\"slds-col slds-size--1-of-2 slds-m-bottom--small slds-p-horizontal--small\"><div class=\"\">"
                    + activityList[i].Activity_Type__c   + "</div> <div>" + dateLM + "</div></div>";
                }
                activitiesDiv  = activitiesDiv + "</div>";
                cmp.set("v.relatedContactActivity", activitiesDiv);
            }
            
            
            var clientNameDiv = "<div class=\"slds-text-align_center slds-text-heading_small slds-m-bottom--small slds-col slds-size--1-of-1 slds-p-horizontal--small\"><div>"
            +   (clientName ? clientName : "")  + "</div></div>";
            
            var companyNameDiv = "<div class=\"slds-col slds-size--1-of-2 slds-m-bottom--small slds-p-horizontal--small\"><div class=\"bold\">"
            + companyNameLabel + "</div><div>" + (companyName ? companyName : "")  + "</div></div>";
            
            var clientStatusDiv = "<div class=\"slds-col slds-size--1-of-2 slds-m-bottom--small slds-p-horizontal--small\"><div class=\"bold\">"
            + clientStatusLabel + "</div><div>"
            + (clientStatus ? clientStatus : "") + "</div></div>";
            
            var clientDNCDiv = (clientDNC ?  "<div class=\"slds-col slds-size--1-of-2 slds-p-horizontal--small tooltip__section\"><div class=\"bold\">"
                                + clientDNCLabel + "</div><div></div></div>" : "");
            //cmp.get("v.contactRecordFields.MailingPostalCode")
            
            
            //ATS- 3220 start - Akshay 
            
            
            var rcMphone = relatedContact[0].MobilePhone;
            var rcHphone = relatedContact[0].HomePhone;
            var rcWphone = relatedContact[0].Phone;
            var rcOphone = relatedContact[0].OtherPhone;
            var rcEmail =  relatedContact[0].Email;
            var rcOemail = relatedContact[0].Other_Email__c;
            var rcWemail = relatedContact[0].Work_Email__c;
            
            
            var rcphoneLabel = "";
            var rcphoneVal = "";
            
            if(typeof rcMphone !== "undefined"){
                rcphoneLabel = RCMobileLabel;
                rcphoneVal = rcMphone;
            }else{
                if(typeof rcWphone !== "undefined"){
                    rcphoneLabel = RCWorkLabel;
                    rcphoneVal = rcWphone; 
                }else if(typeof rcHphone !== "undefined"){
                    rcphoneLabel = RCHomeLabel;
                    rcphoneVal = rcHphone;
                }else if(typeof rcOphone !== "undefined"){
                    rcphoneLabel = RCOtherLabel;
                    rcphoneVal = rcOphone;
                }else{
                    rcphoneLabel = RCMobileLabel;
                    
                }
            }
            
            var rcPhoneDiv = "<div class=\"slds-col slds-size--1-of-2 slds-m-bottom--small slds-p-horizontal--small\"><div class=\"bold\">"
            + rcphoneLabel + "</div><div>" + (rcphoneVal ? rcphoneVal : "")  + "</div></div>";
            
            var rcemailVal = "";
            if(typeof rcEmail !== "undefined"){
                rcemailVal = rcEmail; 
            }else if(typeof rcOemail !== "undefined"){
                rcemailVal = rcOemail;
            }else if(typeof rcWemail !== "undefined"){
                rcemailVal = rcWemail;
            }
            
            var rcEmailDiv = "<div class=\"slds-col slds-size--1-of-2 slds-m-bottom--small slds-p-horizontal--small\"><div class=\"bold\">"
            + RCEmailLabel + "</div><div>" + (rcemailVal ? rcemailVal : "")  + "</div></div>";
            //ATS- 3220 end - Akshay
            
            relatedConActivityHtml =  "<div>"
            + "<div class=\"slds-grid slds-wrap slds-grid--pull-padded\">" + clientNameDiv + companyNameDiv + clientStatusDiv + clientDNCDiv  + rcPhoneDiv + rcEmailDiv +"</div>"
            + "</div>";
            
            
            cmp.set("v.relatedContactAccountInfo", relatedConActivityHtml);
            cmp.set("v.isRelatedContactLoaded", true);
            var toggleText = cmp.find("tooltipRelatedContactId");
            $A.util.toggleClass(toggleText, "content-toggle");             
        } 
        
        
    }, 
    
    compareActivityDate : function compare(a,b) {
        if (b.LastModifiedDate  < a.LastModifiedDate )
            return -1;
        if (b.LastModifiedDate  > a.LastModifiedDate )
            return 1;
        return 0;
    }, 
    
    /* updateInsights: function(component, event, helper){
      var headerEvent = $A.get("e.c:E_UpdateTalentInsightsFromHeader");
      headerEvent.setParams({"talentRecord": component.get("v.record")}); 
      headerEvent.fire();


    }, */
    
    appendAddress : function (address, appendAddress) {     
        if (appendAddress && appendAddress.length > 0 ) {
            address = ((address && address.length > 0 ) ? ( address + ', ' + appendAddress) : appendAddress);
        }  
        return address;
    },
    //S-95929 Rajeesh added for pipeline functionality..
    appendToPipeline: function(component,event){
        var plUsers = this.getPLString(component,event);//
        var usrAlias= component.get("v.runningUser.Alias");
        var usrPplSoftId= component.get("v.runningUser.Peoplesoft_Id__c");
        var plUsersNew;
        if($A.util.isEmpty(plUsers)){
            plUsersNew = ':'+usrPplSoftId+':';
        }
        else{
            plUsersNew = plUsers+usrPplSoftId+':';
        }
        if(plUsersNew.length>1530){
            //take last 1530 right after a : 
            while(plUsersNew.length>1530){
                plUsersNew = plUsersNew.substr(plUsersNew.indexOf(':')+1,plUsersNew.length);
            }
            
        }
        this.updatePipeline(component,plUsersNew,'Create');
    },
    updatePipeline: function(component,plUsersNew,oper){
        var now = new Date();
        //Update Account using LDS and then call apex action to create/delete record.
        var fldName = "v.accountRecordFields.Target_Account_Stamp_";
        var usrId= component.get("v.runningUser.Id");
        var conIds = [];
        conIds.push(component.get('v.recordId'));
        for(var i=0;i<=5;i++){
            component.set(fldName+i+'__c',plUsersNew.substr(i*254,254));
        }
        //console.log('date.now'+Date.now());
        component.set('v.accountRecordFields.Talent_Profile_Last_Modified_Date__c',now.toJSON());//Updating this fiedl to generate HRXML		
        component.find("accountTalentData").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                //console.log("saved to account using LDS. now call server side controller to create other records.");				
            }
        }));
        this.updPipelineRcd(component,conIds,usrId,oper);
    },
    updPipelineRcd: function(component,conIds,usrId,oper){
        var toastEvent = $A.get("e.force:showToast");
        var actionParams ='';
        var bdata = component.find("bdhelper");
        var self=this;
        if(oper=='Create'){
            actionParams = {'conIds': JSON.stringify(conIds),'userId': usrId};
            bdata.callServer(component,'ATS','ContactListFunctions','addToPipeline',function(response){					
                if(!response.indexOf('Success')){
                    toastEvent.setParams({
                        type: 'success',
                        message: $A.get("$Label.c.ATS_ADD_PIPELINE")
                    });
                    toastEvent.fire();
                }
                else{
                    toastEvent.setParams({
                        type: 'error',
                        message: $A.get("$Label.c.ATS_ADD_PIPELINE_ERROR")
                    });
                    toastEvent.fire();
                }
            },actionParams,false);
        }
        else{
            actionParams = {'conId': conIds[0],'userId': usrId};
            bdata.callServer(component,'ATS','ContactListFunctions','removeFromPipeline',function(response){
                console.log('called server for delete...!');
                if(response){
                    toastEvent.setParams({
                        type: 'success',
                        //message: 'The Talent was successfully removed from the Pipeline'
						message: $A.get("$Label.c.ATS_REMOVE_PIPELINE")
                    });
                    toastEvent.fire();
                }
                else{
                    toastEvent.setParams({
                        type: 'error',
                        message: $A.get("$Label.c.ATS_ADD_PIPELINE_ERROR")
                    });
                    toastEvent.fire();
                }
            },actionParams,false);
        }
        
    },
    removeFromPipeline: function(component,event){
        var plUsers = this.getPLString(component,event);
        var usrPplSoftId= component.get("v.runningUser.Peoplesoft_Id__c");
        var idx,stFirstStr,endFirstStr,stSecStr,endSecStr;
        var len = Number(usrPplSoftId.length)+1;
        if(!$A.util.isEmpty(plUsers)){
            idx = plUsers.indexOf(usrPplSoftId);
            stFirstStr = 0;
            endFirstStr= Number(idx)-1;
            stSecStr = Number(idx)+Number(usrPplSoftId.length);
            endSecStr = plUsers.length;
            if(idx>0)
            {
                //ex. :rothe:nkama: --
                plUsers = plUsers.substring(stFirstStr,endFirstStr)+plUsers.substring(stSecStr,endSecStr);
                if(plUsers==':') {
                    plUsers='';
                }
                this.updatePipeline(component,plUsers,'Delete');
            }
        }
    },
    getPLString: function(component,event){
        //get the pipeline string.
        var plUsers = '';
        var fldName = "v.accountRecordFields.Target_Account_Stamp_";
        for(var i=0;i<=5;i++){
            if(!$A.util.isEmpty(component.get(fldName+i+'__c'))){
                plUsers = plUsers+component.get(fldName+i+'__c');
            }
            
        }
        return plUsers;
    },
    //D-05950 - Added by Karthik
    fireURLEventPhone : function(component, phoneValue) {

		if (phoneValue !== null && phoneValue !== undefined){
			phoneValue = phoneValue.replace(/\s/g,'');
        
			var urlEvent = $A.get("e.force:navigateToURL");
			urlEvent.setParams({
				"url": "tel:" + phoneValue
			});
			urlEvent.fire();
		}
    },
    
    fireURLEventEmail : function(component, emailValue) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "mailto:" + emailValue
        });
        urlEvent.fire();
    },
    
    joinSearchParams : function(fld, fldContd) {
        if (!fldContd || fldContd == null) {
            return fld;
        } else {
            var len = fldContd.length;
            return fld.substring(1, 131071) + fldContd.substring(1, len-1);
        }
    },
    displayNoEmailErrorToast: function(msg){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: 'error',
            message: msg
        });
        toastEvent.fire();
    },

	checkDnuAccess : function(component){
		var action = component.get("c.checkPermissionSetAccess");
			action.setParams({ apiName : 'DNU_Access' });
			action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var dnuValue = response.getReturnValue();
				component.set("v.isDnuAccess", dnuValue);
            }

        });
        $A.enqueueAction(action);
    },
	navigateToProactiveSubmittal : function(cmp, event) {
		var pageReference = {
			type: 'standard__component',
			attributes: {
				"componentName" : "c__C_ProactiveSubmittalContainerForLWC"
            },
            state: {
                "c__recordId": cmp.get("v.recordId"), 
				"c__talentId": cmp.get("v.talentId"),
                "c__runningUser": cmp.get("v.runningUser")
            }
        };
		cmp.set("v.pageReference", pageReference);
		
		var navService = cmp.find("navService");
		var pageReference = cmp.get("v.pageReference");
		event.preventDefault();
		navService.navigate(pageReference);
		this.delayedRefresh();
	},
      delayedRefresh : function(milliseconds){
		let ms = milliseconds || 100;
		window.setTimeout($A.getCallback(function(){
			$A.get('e.force:refreshView').fire();
		}),ms);
	},
	displayTalentSummaryModalNew :function(component, event, helper){
        var recordID = component.get("v.talentId");
        var contID = component.get("v.recordId");
		let consentPrefInfo = this.consentPrefInfo(component);
		
        var urlEvent = $A.get("e.c:E_TalentSummEditNew");
        urlEvent.setParams({
            "recordId": recordID,
            "recId": contID,
            "source": "Profile",
			"consentPrefInfo" : consentPrefInfo
        });
        urlEvent.fire(); 
    },
	
	consentPrefInfo : function(component) {
		let hasConsentPrefAccess = false;
		let contactRec = component.get("v.contactRecordFields"); 
		
		if(component.get("v.isTextConsentAllowed") && (contactRec.MailingCountry === 'USA' || contactRec.MailingCountry == 'United States')) {
			hasConsentPrefAccess = true;
		}
		
		let consentPref = {hasConsentPrefAccess : hasConsentPrefAccess,
						   PrefCentre_Aerotek_Mobile : contactRec.PrefCentre_Aerotek_OptOut_Mobile__c,
						   PrefCentre_Aerotek_Email : contactRec.PrefCentre_Aerotek_OptOut_Email__c };

		return consentPref;
	},
    checkForInactive: function(cmp, ownership) {
        const normalizedOwnership = ownership.toUpperCase();
        if (normalizedOwnership === 'INACTIVE') {
            //set the label for menu to be "Activate Talent"
            const activateTalentLabel = $A.get("$Label.c.ATS_ACTIVATE_TALENT");
            cmp.set('v.ActiveInactiveLabel', activateTalentLabel)
            //cmp.set('v.renderTalentStatusFlag', false);
        } else {
            //set the label for menu to be "Deactivate Talent"
            const deactivateTalentLabel = $A.get("$Label.c.ATS_DEACTIVATE_TALENT");
            cmp.set('v.ActiveInactiveLabel', deactivateTalentLabel)
        }
    },
    toggleTalentActiveState: function(cmp, e) {
		const ownership = cmp.get('v.contactRecordFields').Account.Talent_Ownership__c;
		const talentRecordId = cmp.get('v.contactRecordFields').Id;
		var action = cmp.get("c.updateTalentStatus");
        if (ownership.toUpperCase() === 'INACTIVE') {
            cmp.set('v.showSpinner', true);
            action.setParams({ recordId : talentRecordId, activateTalent : true });
            //console.log('Re-activating an INACTIVE Talent');
        } else {
            cmp.set('v.showSpinner', true);
            action.setParams({ recordId : talentRecordId, activateTalent : false });
            //console.log('Deactivating a Talent');
        }
		
		action.setCallback(this, function(response) {
		var state = response.getState();
		if (state === "SUCCESS") {
		    if (cmp.get('v.ActiveInactiveLabel') === 'Activate Talent') {
		        //Activated talent
                this.checkForInactive(cmp, 'ACTIVE');
                this.showToast($A.get("$Label.c.ATS_TALENT_ACTIVATION_TOAST"), 'Success', 'Success');
            } else {
                this.checkForInactive(cmp, 'INACTIVE');
                this.showToast($A.get("$Label.c.ATS_TALENT_INACTIVATION_TOAST"), 'Success', 'Success');
		        //Deactivated talent
            }
            cmp.set('v.showSpinner', false);
			var responseValue = response.getReturnValue();
		} else {
            this.showToast( $A.get("$Label.c.ATS_UNEXPECTED_ERROR"), 'Error', 'Error');
        }

		});
        $A.enqueueAction(action);
    },
    checkTalentDeactivateAccess : function(component){
		var action = component.get("c.checkPermissionSetAccess");
			action.setParams({ apiName : 'Deactivate_ATS_Users' });
			action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var responseValue = response.getReturnValue();
				component.set("v.isTalentDeactivateAccess", responseValue);
            }

        });
        $A.enqueueAction(action);
    },
    showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },
    splitViewDisplay : function(component,event){
        if (window.location.href.indexOf("view?sv=") > -1) {
      		component.set("v.splitView",true);
    	}
    },
	hidecorePopover : function(component,event){
		setTimeout(()=>component.set('v.corePopoverState', 'slds-hide'), 200);
	},

	//Monika - Set focus to a particular field
	setFocusToField:function(component,event,cmpId){
		const fieldIdToGetFocus = document.getElementById(cmpId);
		if(fieldIdToGetFocus == null) return;
		window.setTimeout(
			$A.getCallback(function() {
				// wait for element to render then focus
				fieldIdToGetFocus.focus();
			}), 100
		);
	},
    //S-224383
    checkTFOGroupMember : function(component){
		var action = component.get("c.isTFOGroupMember");
			action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var responseValue = response.getReturnValue();
				component.set("v.isTFOGroupMember", responseValue);
            }

        });
        $A.enqueueAction(action);
    },
	checkTFODRZBoardGroup : function(component){
        var action = component.get("c.isTFOBoardMember");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
                component.set("v.isDRZTfoBoardMember", JSON.parse(responseValue));
            }            
        });
        $A.enqueueAction(action);
    },
	createTalentFirstOpp : function(component){
		let mymap = {};        
        mymap['ContactId'] = component.get("v.recordId");	
		if(component.get("v.drzBoardId").length > 0)
        mymap['DRZTFOBoardId'] = component.get("v.drzBoardId");
		var action = component.get("c.performServerCall");
			action.setParams({className: 'CRM',subClassName: 'TalentFirstOpportunity',methodName: 'CreateTalentFirstOpportunity',parameters: mymap});
			action.setCallback(this, function(response) {
            var state = response.getState();            
            if (state === "SUCCESS") {                
				var responseValue = response.getReturnValue();
                if(responseValue != undefined && responseValue != null && responseValue.length > 0){
                    this.showToast("Opportunity created Successfully!","Success","Success");
                    let optyUrl = "/lightning/r/Opportunity/"+responseValue[0]+"/view";
                	let urlEvent = $A.get("e.force:navigateToURL");        
        			urlEvent.setParams({
            			"url": optyUrl,
            			"isredirect": true
        			});
        			urlEvent.fire();
                }else{
                    this.showToast("Contact System Admin","Error","Error");
                }				
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    }
})