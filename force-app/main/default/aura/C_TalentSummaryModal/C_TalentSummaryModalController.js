({
  toggleSpinner : function(component, event, helper) {
    helper.toggleSpinner(component);
  },
  displayTalentSummForm : function(component, event, helper) {
    /*ak changes component.set("v.modalTitle", "Edit Talent");

    var recordID = event.getParam("recordId");
    component.set("v.talentId", recordID);
    
    var contId = event.getParam("recId");
    component.set("v.contactId", contId);

    var source = event.getParam("source");
    component.set("v.source", event.getParam("source"));

    console.log('displayTalentSummForm ' + component.get("v.currentLanguages"));
    console.log('displayTalentSummForm  2 ' + JSON.stringify(component.get("v.record.languages")));*/
	// var source = component.get("v.source");

    helper.populateLanguage(component);

    //console.log('displayTalentSummForm');

    // if(component.get("v.callServer")){
    //   if(source && source !== null){
    //     if(source === 'Summary'){
          // component.loadSummTab();
        // }
        // else if(source === 'Profile'){
          // component.loadProfileTab();
    //     }
    //   }
    // }
      //Sandeep: Load  Resume tab on display
    //helper.getInitialViewListLoad(component,helper,recordID);
	if(!component.get("v.resumeLoaded")){
	helper.createResumePreviewCmp(component, event);
	}
	
    //Show Modal....
    //Sandeep:workaround To show modal beyond header...
    
    
    if(component){
      //console.log('changing header z index');
      component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0!important}");  
    }
    /*ak changeshelper.toggleClass(component,'backdropAddEditSumm','slds-backdrop--');
    helper.toggleClass(component,'modaldialogAddEditSumm','slds-fade-in-');*/
  },
  updateJobTitle: function(component,event,helper){
    /*var contactRecord = component.get("v.contact");
    contactRecord.Title = component.get("v.jobTitle");
    component.set("v.contact",contactRecord);*/

    //console.log('title--->'+contactRecord.Title);
  },
  updateProfileTabJobTitle: function(component,event,helper) {
    var contactRecord = component.get("v.contact");

    /*if(event.getParam("newTitle")) {
        component.set("v.contact.Title", event.getParam("newTitle"));
    }*/

    //var form = Array.isArray(component.find("editTalent")) ? component.find("editTalent")[-1] : component.find("editTalent");
    var form = component.find("editTalent");
    //console.log(Array.isArray(component.find("editTalent")));

    if(form) {
        form.set('v.contact.Title', contactRecord.Title);
        //console.log('setting new contact title', contactRecord.Title);
    }
  },
  catchUpdatedTitle: function(component, event, helper) {
        //console.log('event handled');
        component.set("v.contact.Title", event.getParam("newTitle"));
  },

  closeModal : function(component, event, helper) {
    helper.closeModal(component);
  },

  // loadSummTab : function(component, event, helper){
  //   //console.log('Load Summ Tab');
  //   var recordID = component.get("v.talentId");
    
  //   if(recordID && recordID !== null){
  //     if(component.get("v.callServer")){
  //       helper.getTalentData(component, recordID);
  //     }

	 //  helper.getandpoplanguages(component);
  //   }
  // },

  // loadProfileTab : function(component, event, helper){
  //   //console.log('Load Prof Tab');

  //   var recordID = component.get("v.talentId");
  //    helper.getCurrentUser(component);
  //   if(component.get("v.callServer")){
  //       helper.getTalentData(component, recordID);
  //   }
   
  //   //helper.getPicklist(component, 'Salutation', 'salutation');
  //   var relatedContactElement = component.find("relatedClient");
  //   $A.util.toggleClass(relatedContactElement, "slds-hide");
  // },

 //Sandeep: resume tab removed no need of this method 
/* loadResumeTab : function(component, event, helper){
      //check to prevent loading the resume evey time user clicks on  Resume tab,
      //If resumeLoaded true , the loadResumeTab method lready called.
      if(!component.get("v.resumeLoaded")){
        var recordID = component.get("v.talentId");
        //console.log('loadResumeTab ',recordID);
      helper.getInitialViewListLoad(component,helper,recordID);
      }

  },*/ 

  hideModal : function(component, event, helper){
	//Rajeesh stop writing client side backup
	component.set('v.eraseBackup',true);
	var dataBackup = component.find("clientSideBackup"); 
	dataBackup.clearDataBackup();

    component.destroy();
	/*ak changes
    //Toggle CSS styles for hiding Modal
    component.set("v.callServer",true);
    component.set("v.cssStyle", "");
    helper.toggleClassInverse(component,'backdropAddEditSumm','slds-backdrop--');
    helper.toggleClassInverse(component,'modaldialogAddEditSumm','slds-fade-in-');*/

  },

  rateTypeChange : function (cmp, event, helper) {
    helper.calculateBonusAmount(cmp);
    helper.calculateTotal(cmp);
  },
   
  rateChange : function (cmp, event, helper) {
    helper.calculateBonusAmount(cmp);
    helper.calculateTotal(cmp);
  },
  
  bonusPctChange : function (cmp, event, helper) {
    // helper.calculateBonusAmount(cmp);
    helper.calculateBonusAmountOnSalary(cmp);
    helper.calculateTotalComp(cmp);
  },
  
  bonusChange : function (cmp, event, helper) {
    // helper.calculateBonusPct(cmp);
    helper.calculateBonusPctOnSalary(cmp);
    helper.calculateTotalComp(cmp);
  },
  
  AddlCompChange : function (cmp, event, helper) {
      helper.calculateTotalComp(cmp);
  },
  updateMailingCountryCode: function(cmp, event, helper) {
      helper.updateMailingCountryCode(cmp,event);
  },
  addNewLocation :function(cmp, event, helper) {
    var locs = [];
    if(typeof cmp.get("v.record.geographicPrefs.desiredLocation") !== "undefined"){
        locs = cmp.get("v.record.geographicPrefs.desiredLocation");  
    }
    locs.push({country:'',state:'',city:''});
    cmp.set("v.record.geographicPrefs.desiredLocation",locs);
  },

  removeLocation :function(cmp, event, helper) {
    var index = event.getSource().get('v.value');
    var locs = cmp.get("v.record.geographicPrefs.desiredLocation");
    locs.splice(index, 1);//deleting the element in the array
    cmp.set("v.record.geographicPrefs.desiredLocation",locs);
  },

  onDNCCheck : function(component, event, helper) {
    helper.onDNCCheck(component);
  },

  onMobilePhoneCheck : function(component, event, helper) {
    var checkCmp = component.find("homePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workPhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherPhonepreferred");
    checkCmp.set("v.value", false);  
  },

  onHomePhoneCheck : function(component, event, helper) {
    var checkCmp = component.find("mobilePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workPhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherPhonepreferred");
    checkCmp.set("v.value", false);    
  },

  onWorkPhoneCheck : function(component, event, helper) {
    var checkCmp = component.find("mobilePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("homePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherPhonepreferred");
    checkCmp.set("v.value", false);    
  },
    
  onOtherPhoneCheck : function(component, event, helper) {
    var checkCmp = component.find("mobilePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("homePhonepreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workPhonepreferred");
    checkCmp.set("v.value", false);    
  },  

  onEmailCheck : function(component, event, helper) {
    var checkCmp = component.find("otherEmailPreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workEmailPreferred");
    checkCmp.set("v.value", false);  
  },

  onOtherEmailCheck : function(component, event, helper) {
    var checkCmp = component.find("emailPreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("workEmailPreferred");
    checkCmp.set("v.value", false);  
  },

  
  onWorkEmailCheck : function(component, event, helper) {
    var checkCmp = component.find("emailPreferred");
    checkCmp.set("v.value", false);
    checkCmp = component.find("otherEmailPreferred");
    checkCmp.set("v.value", false);  
  },  
    
  saveCandidateSummary :function(cmp, event, helper) {
    //console.log('the button was clicked...');

    helper.setSummaryFields(cmp);
    //helper.validateAndSaveTalent(cmp);
    //var updateLMD = $A.get("e.c:E_TalentSummaryUpdateLMD");
    //updateLMD.fire();

    //TalentAddEdit now handles saving of talent data
  
   /* commented by akshay for g2defect fix 11/2/2018
   
   var saveEvent = $A.get("e.c:E_SaveTalentEditsFromModal");
    console.log('testing...');
    var g2Comm = cmp.find("g2CommentsId");
    if(g2Comm){
        saveEvent.setParams({"g2Comments": g2Comm.get("v.value")});
		
    }
	saveEvent.fire();*/
	//added  by akshay for g2defect fix 11/2/2018
	 var g2Comm = cmp.find("g2CommentsId") ? cmp.find("g2CommentsId").get("v.value") : '' ;
     var addEdit = cmp.find("editTalent");
     addEdit.validateAndSaveTalent(cmp.get("v.isG2Checked"), g2Comm );


	//Rajeesh stop writing client side backup
	cmp.set('v.eraseBackup',true);
	var dataBackup = cmp.find("clientSideBackup"); 
	dataBackup.clearDataBackup();
},

  deleteRelatedContact: function(component, event, helper) {
    //This method in the helper hides the div and make the related Client field empty
    helper.deleteRelatedContact(component);
    // Make the div visible again so that newly selected related client can be displayed
    var relatedContactElement = component.find("relatedClient");
    $A.util.toggleClass(relatedContactElement, "slds-hide");
  },

  sectionOne : function(component, event, helper) {
    helper.toggleSect(component,event,'articleOne');
    helper.toggleIcon(component);
  },
  
  sectionTwo : function(component, event, helper) {
    helper.toggleSect(component,event,'articleTwo');
    helper.toggleIcon(component);
  },
  
  sectionThree : function(component, event, helper) {
    helper.toggleSect(component,event,'articleThree');
    helper.toggleIcon(component);
  },
  
  sectionFour : function(component, event, helper) {
    helper.toggleSect(component,event,'articleFour');
    helper.toggleIcon(component);
  },

  sectionFive : function(component, event, helper) {
    helper.toggleSect(component,event,'articleFive');
    helper.toggleIcon(component);
  },
  
  sectionSix : function(component, event, helper) {
    helper.toggleSect(component,event,'articleSix');
    helper.toggleIcon(component);
  },
  sectionSeven : function(component, event, helper) {
    helper.toggleSect(component,event,'articleSeven');
    helper.toggleIcon(component);
  },
  sectionAllSummary : function (component, event, helper) {
      helper.expandCollapseAll(component,event,'articleOne');
      helper.openAll(component,event,'articleOne');
      helper.openAll(component,event,'articleTwo');
      helper.openAll(component,event,'articleThree');
      helper.openAll(component,event,'articleSix');
    helper.openAll(component,event,'articleSeven');      
  },

  sectionCloseAllSummary : function (component, event, helper) {
      helper.expandCollapseAll(component,event,'articleOne');
      helper.closeAll(component,event,'articleOne');
      helper.closeAll(component,event,'articleTwo');
      helper.closeAll(component,event,'articleThree');
      helper.closeAll(component,event,'articleSix');
      helper.closeAll(component,event,'articleSeven');
      
  },

  sectionAllProfile : function (component, event, helper) {
      helper.expandCollapseAll(component,event,'articleFour');
      helper.openAll(component,event,'articleFour');
      helper.openAll(component,event,'articleFive');
  },

  sectionCloseAllProfile : function (component, event, helper) {
      helper.expandCollapseAll(component,event,'articleFour');
      helper.closeAll(component,event,'articleFour');
      helper.closeAll(component,event,'articleFive');
  },
  
  expandCollapse : function(cmp,event,helper){
      var expanded = cmp.get("v.expanded");
      
      if(expanded){
          cmp.set("v.expanded", false);
      }else{
          cmp.set("v.expanded", true);
      }
  },

  changeDocument : function(component, event, helper) {
    var recordID = component.get("v.records[0].Id");
    component.set("v.attachmentId",recordID);

    if(recordID){
      var iframeSrc = '/servlet/servlet.FileDownload?file=' + recordID;
      var componentName = "c:C_IFrameDisplay";
          var targetAttributeName = "v.C_IFrameDisplay";
          var params = {"pageURL": '/servlet/servlet.FileDownload?file=' + recordID,
                "additionalClassName": 'resumeViewer'};

          helper.createComponent(component, componentName, targetAttributeName, params);
    }
    else{
      component.set("v.C_IFrameDisplay","");
    }
  },

  openNewWindow : function(component, event, helper) {
    var fileID = component.get("v.attachmentId");
    if(fileID !== undefined && fileID !== null){
      window.open('/servlet/servlet.FileDownload?file=' + fileID,'winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=500');
    }
  },

  /*downloadDocument : function(cmp,event,helper){
      var additional = cmp.get("v.additional");
      
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": additional + "&apikey=nQ8QcjyLeoMA0ALpH7BJ1nYuVGxDdGeM"
        });
        urlEvent.fire();
  },*/

  loadData: function(cmp, event, helper) {
    cmp.set("v.loading",true);
    cmp.set("v.records",[]);
    cmp.set("v.recordCount",0);

    helper.getResults(cmp,helper);
    helper.getResultCount(cmp);
  },

  updateLoading : function(cmp,event,helper){
    helper.updateLoading(cmp);
  },

  reloadData : function(cmp,event,helper) {
      var rd = cmp.get("v.reloadData");
      if (cmp.get("v.reloadData") === true){
          cmp.set("v.loading",true);
          cmp.set("v.records",[]);
          cmp.set("v.recordCount",0);

          helper.getResults(cmp,helper);
          helper.getResultCount(cmp);

          cmp.set("v.reloadData", false);
      }
  },
  //S-65378 - Added by KK - Update Mailing fields on location selection
  handleLocationSelection : function(component,event) {
	component.set("v.isGoogleLocation", true);
    var mailingstreet, streetnum, streetname, city, state, country,countrycode, postcode,countrymap,displayText;
	mailingstreet = streetnum = streetname = city = state = country = countrycode = postcode = displayText = '';
    var addr = event.getParam("addrComponents");
	var latlong = event.getParam("latlong");    //S-96255 - Update MailLat, MailLong fields on Contact - Karthik        
    countrymap = component.get("v.countriesMap"); 
    displayText = event.getParam("displayText");
    for (var i=0; i<addr.length; i++) {
        if (addr[i].types.indexOf("street_number") >= 0) {
            streetnum = addr[i].long_name;
        }
        if (addr[i].types.indexOf("route") >= 0) {
            streetname = addr[i].long_name;
        }        
        if (addr[i].types.indexOf("postal_code") >= 0) {
            postcode = addr[i].long_name;
        }
        if (addr[i].types.indexOf("locality") >= 0) {
            city = addr[i].long_name;
        }
        if (addr[i].types.indexOf("administrative_area_level_1") >= 0) {
            state = addr[i].long_name;
        }
        if (addr[i].types.indexOf("country") >= 0) {
            country = addr[i].long_name;
            countrycode = countrymap[addr[i].short_name];
        }        
    }
    //console.log('teststreet',streetnum,streetname);
    if(streetnum) {
        mailingstreet = streetnum;
    } 
    if(streetnum && streetname) {
        mailingstreet = mailingstreet + ' ';
    }
    if(streetname) {
        if(mailingstreet != ''){
            mailingstreet = mailingstreet + streetname;        
        } else {
            mailingstreet = streetname;
        }
    }
    //console.log(mailingstreet,city,state,countrycode,country,postcode);
    component.set("v.contact.MailingStreet",mailingstreet);
    component.set("v.contact.MailingCity",city);
    component.set("v.contact.MailingState",state);
    component.set("v.contact.MailingCountry",countrycode);
    component.set("v.contact.Talent_Country_Text__c",country);
    component.set("v.contact.MailingPostalCode",postcode);
    component.set("v.presetLocation",displayText);
	//S-96255 - Update MailLat, MailLong fields on Contact
	if(latlong && (mailingstreet || city || postcode)){
		//component.set("v.contact.GoogleMailingLatLong__Latitude__s",latlong.lat);
		//component.set("v.contact.GoogleMailingLatLong__Longitude__s",latlong.lng);
		component.set("v.contact.MailingLatitude",latlong.lat);
		component.set("v.contact.MailingLongitude",latlong.lng);	
	} else {
		component.set("v.contact.MailingLatitude",null);
		component.set("v.contact.MailingLongitude",null);	
	} 
  },
  showSpinner : function(component, event, helper) {
    var spin = component.get('v.showSpinner');

    if(spin) {
        component.set('v.showSpinner', false);
    } else {
        component.set('v.showSpinner', true);
    }

  },
  //S-65378 - Added by KK - Get country map data on component init
  doInit : function(component,event,helper) {
	/*
			Rajeesh S-108774 - Ability to Restore Backup from 'refreshed' G2 on summary modal
			Feature enabled based on alias included in label. If label is set to GA, rolled out to everyone.
			Label contains comma seperated userids
		*/

	component.set("v.initOnly",true);
	//End Rajeesh
   //S-59001 Neel Kamal 03/14/2018.
   helper.fetchCurrentUser(component);
   //Sandeep: Loading resume Preview 
   if(!component.get("v.resumeLoaded")){
  		helper.createResumePreviewCmp(component, event);
  	}
    var action = component.get("c.getCountryMappings");
    action.setStorable();
    action.setCallback(this,function(response) {
        component.set("v.countriesMap",response.getReturnValue());
    });
    $A.enqueueAction(action);

	//ak changes

    helper.populateLanguage(component);

    var recordID = component.get("v.talentId");
     helper.getCurrentUser(component);
    if(component.get("v.callServer")){
        helper.getTalentData(component, recordID);
    }
    helper.getandpoplanguages(component);

	if(!component.get("v.resumeLoaded")){
	helper.createResumePreviewCmp(component, event);
	}

    helper.toggleClass(component,'backdropAddEditSumm','slds-backdrop--');
    helper.toggleClass(component,'modaldialogAddEditSumm','slds-fade-in-');
  },
     //Sandeep : Split screen change 
  split_toggle : function(component, event, helper) {
        let maindiv = component.find("maindiv_split");
        $A.util.toggleClass(maindiv, "fullRight");
        $A.util.toggleClass(maindiv, "split");
        let iconName = component.get("v.iconName");
      if(iconName == "right"){
         component.set("v.iconName","left"); 
         component.set("v.toggleLayout", "oneColumn");
      }else{
         component.set("v.iconName","right");
         component.set("v.toggleLayout", "fourColumn");
      }
  },

  toggleSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronright': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronright',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
    },

    toggleSubSection: function(component, event, helper) {
        var toggle = {
            'utility:chevronup': 'utility:chevrondown',
            'utility:chevrondown': 'utility:chevronup',
            show: 'hide',
            hide: 'show'
        }
       
        helper.toggleAccordion(component, event, toggle);
    },

    toggleLayout: function(component, event, helper) {
        let newLayout = component.get("v.toggleLayout");
        component.set("v.toggleLayout", newLayout);
    },

    updateMaxSalary: function (component, event, helper){
       helper.updateMaxSalary(component, event);
      }, 

    updateMax: function (component, event, helper){
       helper.updateMax(component, event);
      }, 

    calculateTotalComp: function(component, event, helper){
        helper.calculateBonusAmountOnSalary(component);
        helper.calculateBonusPctOnSalary(component);
        helper.calculateTotalComp(component, event);
      }

})