({
    setSummaryFields : function (component) {
        //var form = Array.isArray(component.find("editTalent")) ? component.find("editTalent")[-1] : component.find("editTalent");
        var form = component.find("editTalent");
        //console.log('evaluating form');
        //console.log(form && true);
        //console.log(form);
        var acc = component.get('v.account');
        var rec = component.get('v.record');
        var langs = component.get('v.currentLanguages');
        var skills = component.get('v.currentSkills');

        this.savePickList(component);
        this.populateLanguageCode(component);

        form.set('v.account.Skill_Comments__c', acc.Skill_Comments__c);
        form.set('v.account.Talent_Overview__c', acc.Talent_Overview__c);
        form.set('v.account.Goals_and_Interests__c', acc.Goals_and_Interests__c );
        form.set('v.account.Desired_Rate__c', acc.Desired_Rate__c);
        form.set('v.account.Desired_Rate_Max__c', acc.Desired_Rate_Max__c);
        form.set('v.account.Desired_Rate_Frequency__c', acc.Desired_Rate_Frequency__c);
        form.set('v.account.Desired_Salary__c', acc.Desired_Salary__c);
        form.set('v.account.Desired_Salary_Max__c', acc.Desired_Salary_Max__c);
        form.set('v.account.Desired_Bonus__c', acc.Desired_Bonus__c);
        form.set('v.account.Desired_Bonus_Percent__c', acc.Desired_Bonus_Percent__c);
        form.set('v.account.Desired_Currency__c', acc.Desired_Currency__c);
        form.set('v.account.Desired_Placement_type__c', acc.Desired_Placement_type__c);
        form.set('v.account.Desired_Schedule__c', acc.Desired_Schedule__c);
        form.set('v.account.Talent_Security_Clearance_Type__c', acc.Talent_Security_Clearance_Type__c);
        form.set('v.account.Talent_FED_Agency_CLR_Type__c', acc.Talent_FED_Agency_CLR_Type__c);
        form.set('v.account.Willing_to_Relocate__c',acc.Willing_to_Relocate__c);
        form.set('v.record', rec);
        form.set('v.currentLanguages', langs);
        form.set('v.currentSkills', skills);



        var tst = component.get('v.record.languages');
         
        var lang = component.get("v.currentLanguages");



    },
    createProfileTab: function(component, helper) {
        //console.log('creating profile tab');
        var params = {contact: component.get("v.contact"),
                        account: component.get("v.account"),
                        talent: component.get("v.contact"),
                        accountId: component.get("v.accountId"),
                        showSpinner: component.get("v.showSpinner"),
                        reliable: component.get("v.reliable"),
                        countriesMap: component.get("v.countriesMap"),
                        countries: component.get("v.countries"),
                        nationalOpp: component.get("v.nationalOpp"),
                        commuteType: component.get("v.commuteType"),
                        desiredCurrency: component.get("v.desiredCurrency"),
                        totalComp: component.get("v.totalComp"),
                        totalCompMax: component.get("v.totalCompMax"),
                        countryKey0: component.get("v.countryKey0"),
                        countryKey1: component.get("v.countryKey1"),
                        countryKey2: component.get("v.countryKey2"),
                        countryKey3: component.get("v.countryKey3"),
                        countryKey4: component.get("v.countryKey4"),
                        countryValue: component.get("v.countryValue"),
                        stateKey: component.get("v.stateKey"),
                        stateValue: component.get("v.stateValue"),
                        geoComments: component.get("v.geoComments"),
                        isG2Checked: component.get("v.isG2Checked"),
                        todayDate: component.get("v.todayDate"),
                        g2Date: component.get("v.g2Date"),
                        isG2DateToday: component.get("v.isG2DateToday"),
                        homePhonePreferred: component.get("v.homePhonePreferred"),
                        mobilePhonePreferred: component.get("v.mobilePhonePreferred"),
                        workPhonePreferred: component.get("v.workPhonePreferred"),
                        otherPhonePreferred: component.get("v.otherPhonePreferred"),
                        emailPreferred: component.get("v.emailPreferred"),
                        otherEmailPreferred: component.get("v.otherEmailPreferred"),
                        workEmailPreferred: component.get("v.workEmailPreferred"),
                        presetLocation: component.get("v.presetLocation"),
                        streetAddress2: component.get("v.streetAddress2"),

                        "aura:id": "editTalent",
                        context: 'modal'

        };


        $A.createComponent(
            "c:C_TalentAddEdit",
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    /* if(componentName == 'C_TalentResumePreview'){ // Sandeep :for C_TalentResumePreview cmp assign instance to extra C_TalentResumePreview_leftpanel 
                        cmp.set("v.C_TalentResumePreview_leftpanel",newComponent);
                    }*/

                    component.set("v.C_TalentAddEdit", newComponent);

                    //console.log(component.find("editTalent") && true);
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    
    toggleSect : function(component,event,secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
            $A.util.toggleClass(acc[cmp], 'slds-hide'); 
            
        }
        
    },
    toggleIcon : function(component){
        var isExpanded = component.get("v.expanded");
        component.set("v.expanded", !isExpanded); 
    },
    openAll : function(component,event,secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
            $A.util.addClass(acc[cmp], 'slds-show');  
            $A.util.removeClass(acc[cmp], 'slds-hide');  
        }
    },
    closeAll : function(component,event,secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
            $A.util.removeClass(acc[cmp], 'slds-show');
            $A.util.addClass(acc[cmp], 'slds-hide');  
        }
    },
    expandCollapseAll : function(cmp,event){
        var expandedAll = cmp.get("v.expandAll");
        if(expandedAll){
            //console.log ("not expanded").
            cmp.set("v.expandAll", false);
        }else{
            //console.log ("expanded")
            cmp.set("v.expandAll", true);
        }
    },
    getCurrentUser : function(component) {

        var action = component.get("c.getCurrentUser");
        action.setStorable(); 
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var response = response.getReturnValue();
                var user = response.usr;

                component.set ("v.runningUser", user);
                
                /* S-97994 - Talent Page: Create Pick List for "Talent Source" */                
                component.set ("v.usrOwnership", response.userOwnership);
                
                /* Start for story S-79062 */
                component.set("v.usrCategory",response.userCategory);
                
                /* End for story S-79062 */


                //console.log(component.get("v.runningUser"));
                //Moved the logic to the <aura:if> in the markup
                /*if (response.Profile.Name === 'Single Desk 2' ||
                    response.Profile.Name === 'System Administrator') {
                    this.getAccountPicklist(component, 'Talent_Gender__c', 'Gender'); 
                    this.getAccountPicklist(component, 'Talent_Race__c', 'Race'); 
                    this.getAccountPicklist(component, 'Talent_Race_Information__c', 'RaceInfo'); 
                    this.getAccountPicklist(component, 'Talent_Gender_Information__c', 'GenderInfo'); 
                } 
                if (response.Profile.Name !== 'ATS_Recruiter') {
                    this.getAccountPicklist(component, 'Do_Not_Contact_Reason__c', 'Reason');
                }*/    
				if (!component.get("v.callServer") && component.get("v.contact") &&
				     component.get("v.contact.Talent_Country_Text__c").length == 0 &&
					 component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
					component.set("v.contact.Talent_Country_Text__c",'United States');
					component.set("v.MailingCountryKey",'US');
					component.set("v.parentInitialLoad",true);
		} 				
            }
        });
        $A.enqueueAction(action);
    },
    updateMailingCountryCode: function(cmp, event) {
        var conKey = cmp.get('v.MailingCountryKey');
        var countrymap = cmp.get("v.countriesMap");
        var countrycode ;
        if (conKey !== null && typeof conKey !== 'undefined') {
                countrycode = countrymap[conKey];
                if(countrycode !== null && typeof countrycode !== 'undefined'){
                    cmp.set("v.contact.MailingCountry",countrycode);
                }
            }
    },
    
    /*getAccountPicklist: function(component, fieldName, elementId) {
        var serverMethod = 'c.getPicklistValues';
        var params = {
            "objectStr": "Account",
            "fld": fieldName
        };

        var action = component.get(serverMethod);
        action.setParams(params);
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('response::'+allValues);
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                if(elementId == "Gender")
                    component.find("Gender").set("v.options", opts);
                if(elementId == "GenderInfo")
                    component.find("GenderInfo").set("v.options", opts);
                if(elementId == "Race")
                    component.find("Race").set("v.options", opts);
                if(elementId == "RaceInfo")
                    component.find("RaceInfo").set("v.options", opts);
                if(elementId == "Reason")
                    component.find("Reason").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },

    getPicklist: function(component, fieldName, elementId) {
        var serverMethod = 'c.getPicklistValues';
        var params = {
            "objectStr": "Contact",
            "fld": fieldName
        };
        var action = component.get(serverMethod);
        action.setParams(params);

        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }

                component.find("salutation").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },*/
    
    /*getParameterByName : function(cmp,event,name) {
        var urlV = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(urlV);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },*/
    
    getTalentData : function(component, accountId) {
        
        //console.log('getting data modal');
        component.set("v.callingServer", true);
        var account = component.get("v.account");
        account.Id = accountId;
        component.set("v.account", account); 
        var serverMethod = 'c.getTalentForModal';
        var params = {
            "accountId": accountId
        };
        
        var action = component.get(serverMethod);
        action.setParams(params);
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") { 
                var talent = response.getReturnValue();
                //console.log('talent::'+JSON.stringify(talent));
                this.translateTalent(component, talent, account);
                component.set("v.callServer",false);
                component.set("v.callingServer",false);
                this.createCandidateStatusBadge(component, event);
            } else {
                this.checkError(response, component);
                component.set("v.callingServer",false);
            }
        });
        $A.enqueueAction(action);
    },
    
    translateTalent : function(component, talent, account) {
        var g2Comm = component.find("g2CommentsId");
        if(g2Comm){
            g2Comm.set("v.value", null);
        }
        this.translateContact(component, talent);
        this.translateAccount(component, talent, account);
        //console.log('is array check: ', Array.isArray(component.find("editTalent")));
        //console.log(component.find("editTalent"));


        //console.log(component.get("v.callingServer"), 'Hello this is the log');
        if(!component.find("editTalent") && !component.get("v.callingServer")) {
            //console.log('creating profile tab...');
            component.set("v.ProfileTabCreated", true);
            this.createProfileTab(component, this);
        }
    },
    
    translateContact : function(component, talent) {
        try {
            var contact = component.get("v.contact");
            contact.Id = talent.Id;
            contact.FirstName = talent.FirstName ? talent.FirstName : '';
            contact.MiddleName = talent.MiddleName ? talent.MiddleName : '';
            contact.LastName = talent.LastName ? talent.LastName : '';
            contact.Preferred_Name__c = talent.Preferred_Name__c ? talent.Preferred_Name__c : '';
            contact.Other_Email__c = talent.Other_Email__c ? talent.Other_Email__c : '';
            contact.Work_Email__c = talent.Work_Email__c ? talent.Work_Email__c : '';
            contact.Email = talent.Email ? talent.Email : '';
            contact.Salutation = talent.Salutation ? talent.Salutation : '';
            contact.Suffix = talent.Suffix ? talent.Suffix : '';
            contact.LinkedIn_URL__c = talent.LinkedIn_URL__c ? talent.LinkedIn_URL__c : '';
            contact.Website_URL__c = talent.Website_URL__c ? talent.Website_URL__c : '';
            contact.Title = talent.Title ? talent.Title : '';
            contact.HomePhone = talent.HomePhone ? talent.HomePhone : '';
            contact.MobilePhone = talent.MobilePhone ? talent.MobilePhone : '';
            contact.Phone = talent.Phone ? talent.Phone : '';
			contact.WorkExtension__c = talent.WorkExtension__c ? talent.WorkExtension__c : '';
            contact.OtherPhone = talent.OtherPhone ? talent.OtherPhone : '';
            contact.MailingCountry = talent.MailingCountry ? talent.MailingCountry : '';
            contact.Talent_Country_Text__c = talent.Talent_Country_Text__c ? talent.Talent_Country_Text__c : '';
            contact.MailingState = talent.MailingState ? talent.MailingState : '';
            contact.Talent_State_Text__c = talent.Talent_State_Text__c ? talent.Talent_State_Text__c : '';
            contact.MailingStreet = talent.MailingStreet ? talent.MailingStreet : '';
            contact.MailingCity = talent.MailingCity ? talent.MailingCity : '';
            contact.MailingPostalCode = talent.MailingPostalCode ? talent.MailingPostalCode : '';
            contact.Related_Contact__c = talent.Related_Contact__c ? talent.Related_Contact__c : '';
            contact.Preferred_Email__c = talent.Preferred_Email__c;
            contact.Preferred_Phone__c = talent.Preferred_Phone__c;
            contact.RWS_DNC__c = talent.RWS_DNC__c;
			contact.Candidate_Status__c = talent.Candidate_Status__c;
            if (talent.Related_Contact__r) {
                contact.Related_Contact__r = talent.Related_Contact__r.Name;
            }

            //console.log('preferred values:');
            //console.log(talent.Preferred_Phone__c);
            //console.log(talent.Preferred_Email__c);
            
            if (talent.Preferred_Phone__c) {
                if (talent.Preferred_Phone__c === 'Home') {
                    component.set("v.homePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Mobile') {
                    component.set("v.mobilePhonePreferred", true);
                } else if (talent.Preferred_Phone__c === 'Work') {
                    component.set("v.workPhonePreferred", true);
                }
                    else if (talent.Preferred_Phone__c === 'Other') {
                        component.set("v.otherPhonePreferred", true);
                    }
            }
            
            if (talent.Preferred_Email__c) {            
                if (talent.Preferred_Email__c === 'Email') {
                    component.set("v.emailPreferred", true);
                } else if (talent.Preferred_Email__c === 'Alternate') {
                    component.set("v.otherEmailPreferred", true);
                } else if (talent.Preferred_Email__c === 'Work') {
                    component.set("v.workEmailPreferred", true);
                }
            }  
            
            if (talent.MailingState) {
                component.set("v.stateKey", talent.MailingState);
            } else {
                component.set("v.stateKey", '');
            }

            if(talent.Candidate_Status__c ){
                component.set("v.candidateStatus", talent.Candidate_Status__c);
            }
            /*if (talent.Talent_State_Text__c) {
                component.set("v.stateValue", talent.MailingState);
            } else {
                component.set("v.stateValue", '');
            }*/
            if (component.get("v.countriesMap") !== null && contact.Talent_Country_Text__c) {
				var countryCode = this.getKeyByValue(component.get("v.countriesMap"),contact.MailingCountry);
				component.set("v.parentInitialLoad",true);
				component.set("v.MailingCountryKey",countryCode);
			} else if(component.get("v.countriesMap") !== null && contact.Talent_Country_Text__c.length == 0 && component.get("v.runningUser.Country") && component.get("v.runningUser.Country") === 'United States'){
					component.set("v.contact.Talent_Country_Text__c",'United States');
					component.set("v.MailingCountryKey",'US');
					component.set("v.parentInitialLoad",true);
            }  
            
            // Added to split in Address 1 and 2
			var strs;
			if (contact.MailingStreet) {
				if( contact.MailingStreet.indexOf(',') != -1 ){
					/*strs = contact.MailingStreet.split(',');
				}
				if (strs) { */
                    console.log('reading address');
					component.set("v.presetLocation",contact.MailingStreet.substr(0,contact.MailingStreet.indexOf(',')));
					component.set("v.streetAddress2",contact.MailingStreet.indexOf(',')+1 != undefined ? contact.MailingStreet.substr(contact.MailingStreet.indexOf(',')+1).trim() : "");
				} else {
					component.set("v.presetLocation",contact.MailingStreet);
				}
			}   else {
				component.set("v.presetLocation","");
			}           
            component.set("v.contact", contact);
            
        } catch (error) {
            contact.Id = null;
            component.set("v.contact", contact);
            this.showError ("An unexpected error has occured, please contact helpdesk for support - " + error.message, 'Error');
            //console.log ("Error occured = " + error.message);
        }     
    },    
    
    translateAccount : function(component, talent, account) {
        try {
            account.Name = talent.Account.Name ? talent.Account.Name : '';
            account.Talent_Gender__c = talent.Account.Talent_Gender__c;
            account.Talent_Race__c = talent.Account.Talent_Race__c;
            account.Talent_Gender_Information__c = talent.Account.Talent_Gender_Information__c;
            account.Talent_Race_Information__c = talent.Account.Talent_Race_Information__c;
            account.Do_Not_Contact__c = talent.Account.Do_Not_Contact__c;
            /*if (talent.Account.Do_Not_Contact__c) {
                   this.onDNCCheck (component);
                }*/
            component.set("v.isChecked",talent.Account.Do_Not_Contact__c);
            
            account.Do_Not_Contact_Reason__c = talent.Account.Do_Not_Contact_Reason__c;
            account.Do_Not_Contact_Date__c = talent.Account.Do_Not_Contact_Date__c;
            account.Talent_Source_Other__c = talent.Account.Talent_Source_Other__c ? talent.Account.Talent_Source_Other__c : '';
            
            account.Talent_Overview__c = talent.Account.Talent_Overview__c;
            account.Skill_Comments__c = talent.Account.Skill_Comments__c;
            account.Goals_and_Interests__c = talent.Account.Goals_and_Interests__c;
            account.Desired_Currency__c = talent.Account.Desired_Currency__c;
            account.Desired_Rate__c = talent.Account.Desired_Rate__c;
            // Start - Fields added for S-58916 and S-76380 
            account.Desired_Rate_Max__c = talent.Account.Desired_Rate_Max__c;
            account.Desired_Salary__c = talent.Account.Desired_Salary__c;
            account.Desired_Salary_Max__c = talent.Account.Desired_Salary_Max__c;
            // End - Fields added for S-58916 and S-76380 
            account.Desired_Rate_Frequency__c = talent.Account.Desired_Rate_Frequency__c;
            account.Desired_Bonus__c = talent.Account.Desired_Bonus__c;
            account.Desired_Bonus_Percent__c = talent.Account.Desired_Bonus_Percent__c;
            account.Desired_Additional_Compensation__c = talent.Account.Desired_Additional_Compensation__c;
            account.Desired_Placement_type__c = talent.Account.Desired_Placement_type__c;
            account.Desired_Schedule__c = talent.Account.Desired_Schedule__c;
			account.Willing_to_Relocate__c = talent.Account.Willing_to_Relocate__c;
			if (talent.Account.Willing_to_Relocate__c == null) {
					account.Willing_to_Relocate__c = "";
			}
            account.G2_Completed_Date__c = talent.Account.G2_Completed_Date__c ? talent.Account.G2_Completed_Date__c : '';
            account.G2_Completed_By__c = talent.Account.G2_Completed_By__c ? talent.Account.G2_Completed_By__c : '';
            account.G2_Completed__c = talent.Account.G2_Completed__c;
            account.Talent_Security_Clearance_Type__c = talent.Account.Talent_Security_Clearance_Type__c;
            account.Talent_FED_Agency_CLR_Type__c = talent.Account.Talent_FED_Agency_CLR_Type__c;

            //console.log('checking bools...');
            //console.log(account.G2_Completed__c, account.G2_Completed_Date__c, account.Do_Not_Contact__c);
            
            //Summary Tab Fields
            if(talent){
                if(typeof talent.Account.Skills__c !== 'undefined'){
                    var skills = JSON.parse(talent.Account.Skills__c);
                    component.set("v.currentSkills",skills);
                }
                else{
                    var skillstr = '{"skills":[]}';
                    var skills  =  JSON.parse(skillstr);
                    component.set("v.currentSkills",skills)
                }
                
                if(typeof talent.Account.Talent_Preference_Internal__c !== 'undefined'){
                    var ipjson = JSON.parse(talent.Account.Talent_Preference_Internal__c);
                    component.set("v.record",ipjson);
                }else{
                    var ipjsonstr = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
                    var ipjson = JSON.parse(ipjsonstr);
                    component.set("v.record",ipjson );
                }
                
                this.populatePickList(component,talent);
                this.calculateTotalComp(component);
				this.getandpoplanguages(component);
                 
                
                // G2 
                var g2Date = "";
                var day = new Date().getDate();
                var month = new Date().getMonth() + 1;
                var year = new Date().getFullYear();
                
                var getDate = day.toString();
                if (getDate.length == 1){ 
                    getDate = "0"+getDate;
                }
                var getMonth = month.toString();
                if (getMonth.length == 1){
                    getMonth = "0"+getMonth;
                }
                
                var todaysDate = year + "-" + getMonth + "-" + getDate;
                
                if(typeof talent.Account.G2_Completed_Date__c !== "undefined"){
                    var tempDate = talent.Account.G2_Completed_Date__c.split("T");
                    if(tempDate.length > 0){
                        g2Date = tempDate[0];
                        var todayInMilliSeconds = new Date(todaysDate).getTime(); 
                        var g2DateInMilliSeconds =  new Date(talent.Account.G2_Completed_Date__c).getTime();
                        component.set("v.isG2DateToday", (todayInMilliSeconds === g2DateInMilliSeconds));
                        component.set("v.isG2Checked", (todayInMilliSeconds === g2DateInMilliSeconds));

                    }
                }
            }
            component.set("v.account", account);
        } 
        catch (error) {
            component.set("v.account", account);
            this.showError ("An unexpected error has occured, please contact helpdesk for support - " + error.message, 'Error');
            //console.log ("Error occured = " + error.message);
        }     
    },
    
	getandpoplanguages:function(component){
		this.callServer(component,'','',"getLanguages",function(response){
                    if(component.isValid()){
                        var opt = {};  
                        var key = '';
                        for(key in response) {     
                            opt[key] = response[key];
                        }
                        component.set("v.languageList", opt);
                        this.populateLanguage(component);
                    }
                },null,true);
	},
    populatePickList:function(cmp) {
        this.populateCurrencyType(cmp);
				
		var natOpp = cmp.get("v.record.geographicPrefs.nationalOpportunities");
		if (natOpp == null) {
			cmp.set("v.record.geographicPrefs.nationalOpportunities","");
		} 
		var reli = cmp.get("v.record.employabilityInformation.reliableTransportation");
        if(reli  == null){
			cmp.set("v.record.employabilityInformation.reliableTransportation","");
		}
        /*var natOpp =  cmp.get("v.record.geographicPrefs.nationalOpportunities");
        if(natOpp  != null){
            if(natOpp  == true){
                cmp.set("v.nationalOpp","Yes"); 
            }else{
                cmp.set("v.nationalOpp","No"); 
            }
        }
        
        var reli = cmp.get("v.record.employabilityInformation.reliableTransportation");
        if(reli  != null){
            if(reli  == true){
                cmp.set("v.reliable","Yes"); 
            }else{
                cmp.set("v.reliable","No"); 
            }
        }*/
        
        var commuteType = cmp.get("v.record.geographicPrefs.commuteLength.unit");
        if(commuteType != null){
            cmp.set("v.commuteType",commuteType); 
        }
    },
    
    populateCurrencyType : function(component) {
        var params = null;
        this.callServer(component,'ATS','TalentEmploymentFunctions','getCurrencyList',function(response){
            if(component.isValid()){
                var desiredCurrency = component.get("v.account.Desired_Currency__c");
                component.set("v.desiredCurrency",desiredCurrency); 
                var CurrLevel = [];
                var CurrLevelMap = response;
                for ( var key in CurrLevelMap ) {
                    CurrLevel.push({value:key, key:key,selected: key === desiredCurrency});
                }
                component.set("v.CurrencyList", CurrLevel); 
            }
        },params,false);
    },
    
    onDNCCheck : function(component) {
        var dncFlagField = component.find("dncFlag");
        if(dncFlagField){
            var dncFlag = dncFlagField.get("v.value");
        }
        
        //account.Do_Not_Contact__c = dncFlag;
        if (!dncFlag) {
            var account = component.get("v.account");
            account.Do_Not_Contact__c = "false";
            account.Do_Not_Contact_Reason__c = "";
            account.Do_Not_Contact_Date__c = "";
            component.set("v.account", account);
        }
        
        /*var dncElement = component.find("dncDivIdReason");
        if(dncElement){
            $A.util.toggleClass(dncElement, "slds-hide");    
        }
        
        var dncElement = component.find("dncDivIdExpDate");
        if(dncElement){
            $A.util.toggleClass(dncElement, "slds-hide");    
        }*/
        
    },
    
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: errorMessage,
            type: 'error'
        });
        toastEvent.fire();
    },

    toggleSpinner : function(component) {
        var show = component.get('v.showSpinner');
        var spinner = component.find('spinner');

        if(show) {
            $A.util.addClass(spinner, 'slds-show');
            $A.util.removeClass(spinner, 'slds-hide'); 
        } else {
            $A.util.removeClass(spinner, 'slds-show');
            $A.util.addClass(spinner, 'slds-hide'); 
        }
    },


    closeModal : function(component) {
        component.set("v.cssStyle", "");
        this.toggleClassInverse(component,'backdropAddEditSumm','slds-backdrop--');
        this.toggleClassInverse(component,'modaldialogAddEditSumm','slds-fade-in-');
        component.set("v.callServer",true);
        component.set("v.ProfileTabCreated", false);
        // var e = component.getEvent("refreshTalentHeaderComponent");
        var appEvent = $A.get("e.c:RefreshTalentHeader");
        appEvent.fire();
        
        $A.get('e.force:refreshView').fire();
        component.destroy();
    },
    
    checkError : function(response, component){
        var errors = response.getError();
        if (errors) {
            //console.log("Errors", errors);
            if (errors[0].fieldErrors) {
                this.showServerSideValidation(errors, component);   
            } else if (errors[0] && errors[0].message) {
                this.showError(errors[0].message, 'An error occured!');
                //throw new Error("Error" + errors[0].message);
            }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
            }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                this.showError(errors[0].pageErrors[0].statusCode,'An error occured!');
            }else{
                this.showError('An unexpected error has occured. Please try again!');
            }
        } else {
            this.showError(errors[0].message);
        }
    },    
    
    showServerSideValidation : function(errors, component) {
        for (var fieldName in errors[0].fieldErrors) {
            if (fieldName === "LinkedIn_URL__c") {
                
                var linkedinUrlField = component.find("linkedinUrl");
                if(linkedinUrlField){
                    linkedinUrlField.set("v.errors", [{message: "Please enter the valid LinkedIn url."}]);
                    linkedinUrlField.set("v.isError",true);
                    $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");
                }
                
            } else if (fieldName === "Email") {
                
                
                var emailField = component.find("email");
                if(emailField){
                   /* Start for story S-79062 */
                var message = ''; 
                var thisFieldError = errors[0].fieldErrors[fieldName];
                for(var j=0; j < thisFieldError.length; j++) {
                                    message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
                                }
                   //console.log('Message::'+ message);
                      emailField.set("v.errors", [{message: message}]);
                     // before it was emailField.set("v.errors", [{message: "Please enter valid email address"}]);
                    /* End for story S-79062 */
                    emailField.set("v.isError",true);
                    $A.util.addClass(emailField,"slds-has-error show-error-message");
                }
                
            } else if (fieldName === "Other_Email__c") {
                
                var otherEmailField = component.find("otherEmail");
                if(otherEmailField){
                    otherEmailField.set("v.errors", [{message: "Please enter a valid email address."}]);
                    otherEmailField.set("v.isError",true);
                    $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
                }
                
            }  else if (fieldName === "Work_Email__c") {
                
                var workEmailField = component.find("workEmail");
                if(workEmailField){
                    workEmailField.set("v.errors", [{message: "Please enter a valid email address."}]);
                    workEmailField.set("v.isError",true);
                    $A.util.addClass(workEmailField,"slds-has-error show-error-message");
                }
                
            } else {
                this.showError (errors[0].fieldErrors[fieldName][0].message, errors[0].fieldErrors[fieldName][0].statusCode);
            }
        }  
        
    },   
    
    clearServerValidationError : function(component) {
        var linkedinUrlField = component.find("linkedinUrl");
        if(linkedinUrlField){
            linkedinUrlField.set("v.errors", []);
            linkedinUrlField.set("v.isError", false);
            $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");
        }
        
        var emailField = component.find("email");
        if(emailField){
            emailField.set("v.errors", []);
            emailField.set("v.isError", false);
            $A.util.addClass(emailField,"slds-has-error show-error-message");
        }
        
        var otherEmailField = component.find("otherEmail");
        if(otherEmailField){
            otherEmailField.set("v.errors", []);
            otherEmailField.set("v.isError", false);
            $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
        }
        
        var workEmailField = component.find("workEmail");
        if(workEmailField){
            workEmailField.set("v.errors", []);
            workEmailField.set("v.isError", false);
            $A.util.addClass(workEmailField,"slds-has-error show-error-message");
        }
    },  
    
    updatePreferredValues : function(contact, component) {
        if (component.get("v.homePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Home';
        } 
        else if (component.get("v.mobilePhonePreferred") === true) {
            contact.Preferred_Phone__c = 'Mobile';
        } 
            else if (component.get("v.workPhonePreferred") === true) {
                contact.Preferred_Phone__c = 'Work';
            }
                else if (component.get("v.otherPhonePreferred") === true) {
                    contact.Preferred_Phone__c = 'Other';
                }
                    else {
                        contact.Preferred_Phone__c = '';
                    }
        //Email
        if (component.get("v.emailPreferred") === true) {
            contact.Preferred_Email__c = 'Email';
        } 
        else if (component.get("v.otherEmailPreferred") === true) {
            contact.Preferred_Email__c = 'Alternate';
        }
            else if (component.get("v.workEmailPreferred") === true) {
                contact.Preferred_Email__c = 'Work';
            }
                else {
                    contact.Preferred_Email__c = '';
                }
        
        return contact;
    },
    
    updateState : function(contact, component) {
        contact.MailingState = component.get("v.stateKey");
        contact.Talent_State_Text__c = component.get("v.stateValue");
        return contact;
    },

    // for story S-96223 TEK or ONS user phone or email mandatory.
    validatePhoneEmailLinkedInForTEKONS: function (component , validTalent){
       var mobilePhoneField = component.find("mobilePhone");
	   if(mobilePhoneField){
			var mobilePhone = mobilePhoneField.get("v.value");
			if(!this.validateMobilePhone(mobilePhone)) {
			   mobilePhoneField.set("v.errors", [{message: "Please enter valid Mobile Phone."}]);
			   mobilePhoneField.set("v.isError",true);
			   validTalent = false;
			}
		}

        var homePhoneField = component.find("homePhone");
		if(homePhoneField){
			var homePhone = homePhoneField.get("v.value");
			if(!this.validateMobilePhone(homePhone)) {
			   homePhoneField.set("v.errors", [{message: "Please enter valid Home Phone."}]);
			   homePhoneField.set("v.isError",true);
			   validTalent = false;
			}
		}

        var workPhoneField = component.find("workPhone");
			if(workPhoneField){
			var workPhone = workPhoneField.get("v.value");
			var workPhoneValMessage = "Please enter a valid Work Phone.";
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if((workExtension !== '' && typeof workExtension !== 'undefined') && (workPhone === '' || typeof workPhone == 'undefined')){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						}
					
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			 if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			}		
		}
		var otherPhoneField = component.find("otherPhone");
		if(otherPhoneField){
			var otherPhone = otherPhoneField.get("v.value");
			if(!this.validateMobilePhone(otherPhone)) {
			   otherPhoneField.set("v.errors", [{message: "Please enter valid Other Phone."}]);
			   otherPhoneField.set("v.isError",true);
			   validTalent = false;
			}
		}
       
        var emailField = component.find("email");
		if(emailField){
			var email = emailField.get("v.value");
			if(!this.validateEmail(email)) {
				emailField.set("v.errors", [{message: "Please enter valid Email."}]);
				emailField.set("v.isError",true);
				validTalent = false;
			}
		}

        var otherEmailField = component.find("otherEmail");
			if(otherEmailField){
			var otherEmail = otherEmailField.get("v.value");
			if(!this.validateEmail(otherEmail)) {
				otherEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
				otherEmailField.set("v.isError",true);
				validTalent = false;
			}
		}
        var workEmailField = component.find("workEmail");
			if(workEmailField){
			var workEmail = workEmailField.get("v.value");
			if(!this.validateEmail(workEmail)) {
				workEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
				workEmailField.set("v.isError",true);
				validTalent = false;
			} 
		}
       

        

		if(mobilePhoneField && homePhoneField && workPhoneField  && otherPhoneField && emailField && otherEmailField  && workEmailField ){
            if (!mobilePhone && !homePhone && !workPhone  && !otherPhone
                && !email && !otherEmail && !workEmail) {
                validTalent = false;
                this.showError("One of the contact methods (Phone, Email ) are required to proceed.", "Error");
            }
        }
        return validTalent;  
    },
    
    /* Start for Strory S-79062 */
    validateEMEAUser : function(component,flag) {
       
              var UserCategory = component.get("v.usrCategory");
        
                //console.log('UserCategory::'+UserCategory);
                if(UserCategory === 'CATEGORY3'){
                    //console.log('Inside CATEGORY3 ');
                    var emailField = component.find("email");
                    var email = emailField.get("v.value");
                    //console.log('EMail value::'+email);
                    
                    var otherEmailField = component.find("otherEmail");
                    var otherEmail = otherEmailField.get("v.value");
                    //console.log('otherEmail value::'+otherEmail);
        
                    var workEmailField = component.find("workEmail");
                    var workEmail = workEmailField.get("v.value");
                    //console.log('workEmail value::'+workEmail);
                   
                    if((!email || email == " ")&&(!otherEmail || otherEmail == " ")&&(!workEmail || workEmail == " ") ){                  
                        //console.log('Inside if condition where all email blank ');
                      
                        flag = false;
                        emailField.set("v.errors", [{message: "Email mandatory for EMEA User."}]);
                        emailField.set("v.isError",true);
                        $A.util.addClass(emailField,"slds-has-error show-error-message");
                       this.showError("Email is mandatory for EMEA Users", "Error");
                        
                    }
                    else{
                           emailField.set("v.errors", []);
                           emailField.set("v.isError", false);
                           $A.util.removeClass(emailField,"slds-has-error show-error-message");
                    }
                }
              
        //console.log('Before Returning the value::'+ flag);
        
        return flag;
    },
    /* End for Strory S-79062 */
   
    validateZipAndState : function(component, validTalent) {
        var validZip = true;
        var validState = true;
        
        var zipField = component.find("MailingPostalCode");
        if(zipField){
            var zip = zipField.get("v.value");
            if (!zip || zip === "") {
                validZip = false;
            }     
        }
        //S-65378 - Added by KK: Updated to use Mailing State instead of Talent_State_Text__c
        var state =  component.get("v.contact.MailingState");
       // if(state){
            if (!state || state === "") {
                validState = false;
            }
        //}
        
        if (!validZip && !validState) {
            validTalent = false;
            this.showError("Please enter a valid Postal Code or State.", "Error");
        }
        return validTalent;
    },   


     validateStateCountry : function(component, validTalent) {

      var state    =      component.get("v.contact.MailingState");
      var country  =      component.get("v.contact.Talent_Country_Text__c");

      var mailingCountry = component.get("v.contact.MailingCountry");

     if ( $A.util.isEmpty(state) || $A.util.isEmpty(country) ) {
            validTalent = false; 
            this.showError("Please enter a valid Country and State.", "Error");
        } else if (mailingCountry === '' ) {
            var countryMap = component.get("v.countriesMap");
            var countryCode = component.get("v.MailingCountryKey");
            if (countryMap && countryCode && countryCode.length > 0) {
               try {
                   var countryValue = countryMap[countryCode];
                   if (countryValue && countryValue.length > 0 ) {
                       component.set("v.contact.MailingCountry", countryValue);
                   } else {
                     validTalent = false; 
                     this.showError("Please enter a valid Country and State.");
                   }
               } catch(error) {
                   console.error(error);
                   validTalent = false;
                    this.showError("Please enter a valid Country and State.");
               }
            } else {
                     validTalent = false;
                     this.showError("Please enter a valid Country and State."); 
                   }
      } 
      return validTalent;        
    },
	validateTitleError : function(component, validTalent) {
        var hiddenField = component.find("hiddenTitle");
        var hidden = hiddenField.get("v.value");
        if (!hidden || hidden === "") {
            validTalent = false;
            component.set("v.titleError", true);
			this.showError("Please enter a Job Title.");
        } else {
            component.set("v.titleError", false);
        }
        return validTalent;
    },
    validateFirstName : function(component, validTalent) {
        var firstNameField = component.find("firstName");
        if(firstNameField){
            var firstName = firstNameField.get("v.value");    
            if (!firstName || firstName === "") {
                validTalent = false;
                firstNameField.set("v.errors", [{message: "Please enter a valid First Name"}]);
                firstNameField.set("v.isError",true);
                $A.util.addClass(firstNameField,"slds-has-error show-error-message");
                this.showError("Not all fields on the Profile Tab passed validation. Please review your changes.", "Error");
            } else {
                firstNameField.set("v.errors", []);
                firstNameField.set("v.isError", false);
                $A.util.removeClass(firstNameField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },    
    
    validateLastName : function(component, validTalent) {
        var lastNameField = component.find("lastName");
        if(lastNameField){
            var lastName = lastNameField.get("v.value");
            if (!lastName || lastName === "") {
                validTalent = false;
                lastNameField.set("v.errors", [{message: "Please enter a valid Last Name"}]);
                lastNameField.set("v.isError",true);
                $A.util.addClass(lastNameField,"slds-has-error show-error-message");
                this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
            } else {
                lastNameField.set("v.errors", []);
                lastNameField.set("v.isError", false);
                $A.util.removeClass(lastNameField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },    
    
    validateExpirationDate : function(component, validTalent) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        
        if(dd<10) {
            dd = '0'+dd
        } 
        
        if(mm<10) {
            mm = '0'+mm
        } 
        
        today = yyyy + '-' + mm + '-' + dd;
        var expirationDateField = component.find("expirationDate");
        if(expirationDateField){
            var expirationDate = expirationDateField.get("v.value");    
        }
        
        if(expirationDate < today && expirationDate != "") {
            validTalent = false;
            expirationDateField.set("v.errors", [{message: "Expiration Date can not be a Past Date."}]);
            expirationDateField.set("v.isError",true);
            $A.util.addClass(expirationDateField,"slds-has-error show-error-message");
            this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
        } else {
            expirationDateField.set("v.errors", []);
            expirationDateField.set("v.isError", false);
            $A.util.removeClass(expirationDateField,"slds-has-error show-error-message");
        }
        return validTalent;
    },    
    
    validateLinkedInUrl : function(component, validTalent){

        var linkedinUrlField = component.find("linkedinUrl");
        if(linkedinUrlField){
            var linkedinUrl = linkedinUrlField.get("v.value");    
            linkedinUrlField.set("v.errors", []);
            linkedinUrlField.set("v.isError", false);
        }

    
        $A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
        
        if(linkedinUrl && linkedinUrl != ""){
            // /(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
            // var linkedInRegex = new RegExp(".*(linkedin.com/).*", "i");
            // var linkedInRegex = new RegExp(/(http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/);

            var linkedInRegex = new RegExp(/(http|https):\/\/\/?\S+$/);

            if (!linkedInRegex.test(linkedinUrl)) {
                validTalent = false;
                linkedinUrlField.set("v.errors", [{message: "Please enter valid LinkedIn url."}]);
                linkedinUrlField.set("v.isError",true);
                $A.util.addClass(linkedinUrlField,"slds-has-error show-error-message");

                if(!linkedinUrl.includes('http')){
                    this.showError("Update URL link to include https:// or http:// for LinkedIn.", "Error");
                }else{
                    this.showError("Please Enter Valid URL format for LinkedIn Url.", "Error");
                }
                
                
            } else {
                linkedinUrlField.set("v.errors", []);
                linkedinUrlField.set("v.isError", false);
                $A.util.removeClass(linkedinUrlField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },

    
    validateWebsiteUrl : function(component, validTalent){

        var websiteUrlField = component.find("Website_URL__c");
        // alert(websiteUrlField);
        if(websiteUrlField){
            var websiteUrl = websiteUrlField.get("v.value");    
            websiteUrlField.set("v.errors", []);
            websiteUrlField.set("v.isError", false); 
        }

        $A.util.removeClass(websiteUrlField,"slds-has-error show-error-message");
        
        if(websiteUrl && websiteUrl != ""){
            
            var websiteUrlRegex = new RegExp(/(http|https):\/\/\/?\S+$/);

            if (!websiteUrlRegex.test(websiteUrl)) {
                
                validTalent = false;
                websiteUrlField.set("v.errors", [{message: "Please enter valid Website url."}]);
                websiteUrlField.set("v.isError",true);
                $A.util.addClass(websiteUrlField,"slds-has-error show-error-message");

                if(!websiteUrl.includes('http')){
                    this.showError("Update URL link to include https:// or http:// for Website.", "Error");
                }else{
                    this.showError("Please Enter Valid URL format for website url.", "Error");
                }


            } else {
                websiteUrlField.set("v.errors", []);
                websiteUrlField.set("v.isError", false);
                $A.util.removeClass(websiteUrlField,"slds-has-error show-error-message");
            }
        }
        return validTalent;
    },
    
    
    validatePrefOptions : function(component, validTalent) {
        if (component.get("v.homePhonePreferred") === true) {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                var homePhone = homePhoneField.get("v.value");
                if(homePhone === "" || !homePhone){
                    validTalent = false;
                    homePhoneField.set("v.errors", [{message: "Please enter Home Phone."}]);
                    homePhoneField.set("v.isError",true);
                    $A.util.addClass(homePhoneField,"slds-has-error show-error-message");
                    this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.");
                }
                else {
                    homePhoneField.set("v.errors", []);
                    homePhoneField.set("v.isError", false);
                    $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var homePhoneField = component.find("homePhone");
            if(homePhoneField){
                homePhoneField.set("v.errors", []);
                homePhoneField.set("v.isError", false);
                $A.util.removeClass(homePhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.mobilePhonePreferred") === true) {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                var mobilePhone = mobilePhoneField.get("v.value");
                if(mobilePhone === "" || !mobilePhone){
                    validTalent = false;
                    mobilePhoneField.set("v.errors", [{message: "Please enter Mobile Phone."}]);
                    mobilePhoneField.set("v.isError",true);
                    $A.util.addClass(mobilePhoneField,"slds-has-error show-error-message");
                    this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
                }
                else {
                    mobilePhoneField.set("v.errors", []);
                    mobilePhoneField.set("v.isError", false);
                    $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var mobilePhoneField = component.find("mobilePhone");
            if(mobilePhoneField){
                mobilePhoneField.set("v.errors", []);
                mobilePhoneField.set("v.isError", false);
                $A.util.removeClass(mobilePhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.workPhonePreferred") === true) {
            var workPhoneField = component.find("workPhone");
			var workPhoneValMessage = "Please enter a valid Work Phone.";

            if(workPhoneField){
                var workPhone = workPhoneField.get("v.value");
                if(workPhone === "" || !workPhone){
                    validTalent = false;
                    workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
                    workPhoneField.set("v.isError",true);
                    $A.util.addClass(workPhoneField,"slds-has-error show-error-message");
                    this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
                }
                else {
                    workPhoneField.set("v.errors", []);
                    workPhoneField.set("v.isError", false);
                    $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
                }    
            }	
        }
        else {
            var workPhoneField = component.find("workPhone");
            if(workPhoneField){
                workPhoneField.set("v.errors", []);
                workPhoneField.set("v.isError", false);
                $A.util.removeClass(workPhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.otherPhonePreferred") === true) {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                var otherPhone = otherPhoneField.get("v.value");
                if(otherPhone === "" || !otherPhone){
                    validTalent = false;
                    otherPhoneField.set("v.errors", [{message: "Please enter Work Phone."}]);
                    otherPhoneField.set("v.isError",true);
                    $A.util.addClass(otherPhoneField,"slds-has-error show-error-message");
                    this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
                }
                else {
                    otherPhoneField.set("v.errors", []);
                    otherPhoneField.set("v.isError", false);
                    $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherPhoneField = component.find("otherPhone");
            if(otherPhoneField){
                otherPhoneField.set("v.errors", []);
                otherPhoneField.set("v.isError", false);
                $A.util.removeClass(otherPhoneField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.emailPreferred") === true) {
            var emailField = component.find("email");
            if(emailField){
                var email = emailField.get("v.value");
                if(email === "" || !email){
                    validTalent = false;
                    emailField.set("v.errors", [{message: "Please enter Email."}]);
                    emailField.set("v.isError",true);
                    $A.util.addClass(emailField,"slds-has-error show-error-message");
                    this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
                }
                else {
                    emailField.set("v.errors", []);
                    emailField.set("v.isError", false);
                    $A.util.removeClass(emailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var emailField = component.find("email");
            if(emailField){
                emailField.set("v.errors", []);
                emailField.set("v.isError", false);
                $A.util.removeClass(emailField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.otherEmailPreferred") === true) {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                var otherEmail = otherEmailField.get("v.value");
                if(otherEmail === "" || !otherEmail){
                    validTalent = false;
                    otherEmailField.set("v.errors", [{message: "Please enter Other Email."}]);
                    otherEmailField.set("v.isError",true);
                    $A.util.addClass(otherEmailField,"slds-has-error show-error-message");
                    this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
                }
                else {
                    otherEmailField.set("v.errors", []);
                    otherEmailField.set("v.isError", false);
                    $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var otherEmailField = component.find("otherEmail");
            if(otherEmailField){
                otherEmailField.set("v.errors", []);
                otherEmailField.set("v.isError", false);
                $A.util.removeClass(otherEmailField,"slds-has-error show-error-message");
            }
        }
        
        if (component.get("v.workEmailPreferred") === true) {
            var workEmailField = component.find("workEmail");
            if(workEmailField){
                var workEmail = workEmailField.get("v.value");
                if(workEmail === "" || !workEmail){
                    validTalent = false;
                    workEmailField.set("v.errors", [{message: "Please enter Other Email."}]);
                    workEmailField.set("v.isError",true);
                    $A.util.addClass(workEmailField,"slds-has-error show-error-message");
                    this.showError("Not all the fields on the Profile Tab passed validation. Please review your changes.", "Error");
                }
                else {
                    workEmailField.set("v.errors", []);
                    workEmailField.set("v.isError", false);
                    $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
                }    
            }
        }
        else {
            var workEmailField = component.find("workEmail");
            if(workEmailField){
                workEmailField.set("v.errors", []);
                workEmailField.set("v.isError", false);
                $A.util.removeClass(workEmailField,"slds-has-error show-error-message");
            }
        }
        
        return validTalent;
    },
    
    validatePhoneEmailLinkedIn : function(component, validTalent) {
        var mobilePhoneField = component.find("mobilePhone");
        if(mobilePhoneField){
            var mobilePhone = mobilePhoneField.get("v.value");    
        }
        
        if(!this.validateMobilePhone(mobilePhone)) {
            mobilePhoneField.set("v.errors", [{message: "Please enter valid Mobile Phone."}]);
            mobilePhoneField.set("v.isError",true);
            validTalent = false;
        }
        
        var homePhoneField = component.find("homePhone");
        if(homePhoneField){
            var homePhone = homePhoneField.get("v.value");    
        }
        if(!this.validateMobilePhone(homePhone)) {
            homePhoneField.set("v.errors", [{message: "Please enter valid Home Phone."}]);
            homePhoneField.set("v.isError",true);
            validTalent = false;
        }
        
        var workPhoneField = component.find("workPhone");
			if(workPhoneField){
				var workPhone = workPhoneField.get("v.value");
				var workPhoneValMessage = "Please enter a valid Work Phone.";
				var wpValid = this.validateMobilePhone(workPhone);
			
				var workExtensionField = component.find("workExtension");
				if (workExtensionField){
					var workExtension = workExtensionField.get("v.value");
					if((workExtension !== '' && typeof workExtension !== 'undefined') && (workPhone === '' || typeof workPhone == 'undefined')){
						workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
						workPhoneField.set("v.isError",true);
						validTalent = false;
						}
				
					else {
						workExtensionField.set("v.errors", null);
						workExtensionField.set("v.isError",false);
					}
				}
			 if(!wpValid) {
			   workPhoneField.set("v.isError",true);
			   workPhoneField.set("v.errors", [{message: workPhoneValMessage}]);
			   validTalent = false;
			}		
		}
        
        var otherPhoneField = component.find("otherPhone");
        if(otherPhoneField){
            var otherPhone = otherPhoneField.get("v.value");    
        }
        if(!this.validateMobilePhone(otherPhone)) {
            otherPhoneField.set("v.errors", [{message: "Please enter valid Work Phone."}]);
            otherPhoneField.set("v.isError",true);
            validTalent = false;
        }
        
        
        var emailField = component.find("email");
        if(emailField){
            var email = emailField.get("v.value");
        }
        if(!this.validateEmail(email)) {
            emailField.set("v.errors", [{message: "Please enter valid Email."}]);
            emailField.set("v.isError",true);
            validTalent = false;
        }
        
        var otherEmailField = component.find("otherEmail");
        if(otherEmailField){
            var otherEmail = otherEmailField.get("v.value");    
        }
        if(!this.validateEmail(otherEmail)) {
            otherEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
            otherEmailField.set("v.isError",true);
            validTalent = false;
        }
        
        var workEmailField = component.find("workEmail");
        if(workEmailField){
            var workEmail = workEmailField.get("v.value");
        }
        if(!this.validateEmail(workEmail)) {
            workEmailField.set("v.errors", [{message: "Please enter valid Email."}]);
            workEmailField.set("v.isError",true);
            validTalent = false;
        } 
        
        var linkedinUrlField = component.find("linkedinUrl");
        if(linkedinUrlField){
            var linkedinUrl = linkedinUrlField.get("v.value");    
        }
        
        
        if(mobilePhoneField && homePhoneField && workPhoneField  && otherPhoneField && emailField && otherEmailField  && workEmailField && linkedinUrlField){
            if (!mobilePhone && !homePhone && !workPhone  && !otherPhone
                && !email && !otherEmail && !workEmail && !linkedinUrl) {
                validTalent = false;
                this.showError("One of the contact methods (Phone, Email or LinkedIn URL) are required to proceed.", "Error");
            }
        }
        
        return validTalent;    
    }, 
    
    validateMobilePhone : function(mobilePhone) {
        if(mobilePhone && mobilePhone.length > 0) {
            var mp = mobilePhone.replace(/\s/g,'');
            if(mobilePhone && mp.length >= 10 && mp.length <= 17) {
                
                var regex = new RegExp("^[0-9-.+()/]+$");
                var isValid = mp.match(regex);
                
                if(isValid != null) {
                    return true; 
                }
                else{
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true; 
    },   
     
    
    validateEmail : function(emailID){
        if(emailID && emailID.trim().length > 0) {
            
            var isValidEmail = emailID.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            
            if(isValidEmail != null) {
                return true;
            }
            else {
                return false;
            } 
            
        }
        
        return true;
    },
    
    deleteRelatedContact : function(component) {
        var contact = component.get("v.contact");
        contact.Related_Contact__c = "";
        contact.Related_Contact__r = "";
        component.set("v.contact", contact);
        var relatedContactElement = component.find("relatedClient");
        if(relatedContactElement){
            $A.util.toggleClass(relatedContactElement, "slds-hide");    
        }
    },
    
    /* Sandeep: Below resume disply logic not needed.Pview cmp has this logic */
	/*getInitialViewListLoad : function(cmp,helper,accID){
        var awaitResultFromViewList = false;
        var recordId = accID;
        //cmp.set("v.talentId", recordId);
        cmp.set("v.accountId", recordId);
        
        var viewListFunction = cmp.get("v.viewListFunction");
        
        if(viewListFunction){
            if(viewListFunction != ''){
                awaitResultFromViewList = true;
            }
        }
        
        if(awaitResultFromViewList){
            this.getViewListItems(cmp, viewListFunction, recordId, helper);
        }else{
            this.getResults(cmp,helper);
            this.getResultCount(cmp);
        }
        //Setting resumeLoaded boolean true
        cmp.set("v.resumeLoaded",true);
    },
    
    getViewFromApexFunction : function(cmp, apexFunction, recordId, params,helper){
        var className = '';
        var subClassName = '';
        var methodName = '';
        
        var arr = apexFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0];
            subClassName = arr[1];
            methodName = arr[2];
        }
        
        if(methodName != ''){
            cmp.set("v.loadingData",true);
            var actionParams = {'recordId':recordId, "maxRows": cmp.get("v.maxRows")};
            
            if(params){
                if(params.length != 0){
                    Object.assign(actionParams,params);
                }
            }
            this.callServer(cmp,className,subClassName,methodName,function(response){
                
                cmp.set("v.records", response);
                cmp.set("v.loadingData",false);
                var recordCount = 0;
                if(response){
                    if(response.length > 0){
                        if(response[0].totalItems){
                            recordCount = response[0].totalItems;
                        }
                    }
                }
                
                if(recordCount == 0){
                    this.getResultCount(cmp);
                }
                
                cmp.set("v.recordCount", recordCount); 
                cmp.set("v.loadingCount",false);
            },actionParams,false);
        }
        
    },
    
    getViewListItems : function(cmp, viewListFunction, recordId, helper) {
        
        cmp.set("v.loadingData",true);
        var actionParams = {'recordId':recordId};
        var className = '';
        var subClassName = '';
        var methodName = '';
        
        var arr = viewListFunction.split('.');
        if(arr.length == 1){
            methodName = arr[0];
        }else if(arr.length == 2){
            className = arr[0];
            methodName = arr[1];
        }else if(arr.length == 3){
            className = arr[0]; 
            subClassName = arr[1];
            methodName = arr[2];
        }
        
        this.callServer(cmp,className,subClassName,methodName,function(response){
            cmp.set("v.loadingData",false);
            var returnVal = response;
            console.log('in  getViewListItems returnVal',returnVal);
            var newArr = JSON.parse(returnVal);
            
            //var newArr = ( new Function("return [" + returnVal + "];")() );
            
            cmp.set("v.viewList", newArr);
            cmp.set("v.loadingData",false);
            this.getResults(cmp, this);
            this.getResultCount(cmp, this);
        },actionParams,false);
        
    },
    
    getResults : function(cmp,helper){
        var listView = cmp.get("v.viewList");
        
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            var recordID = cmp.get("v.accountId");
            var soql = listView[indexNo].soql;
            
            if(soql){
                if(soql != ''){
                    soql = soql.replace("%%%parentRecordId%%%","'"+ recordID +"'");
                    
                    var apexFunction = "c.getRows";
                    
                    var params = {
                        "soql": soql,
                        "maxRows": cmp.get("v.maxRows")
                    };
                    
                    cmp.set("v.loadingData",true);
                    var actionParams = {'recordId':recordID};
                    if(params){
                        if(params.length != 0){
                            Object.assign(actionParams,params);
                        }
                    }
                    
                    this.callServer(cmp,'','',apexFunction,function(response){
                        cmp.set("v.records", response);
                        cmp.set("v.loadingData",false);
                        
                        if(listView[indexNo].additional){
                            cmp.set("v.additional",listView[indexNo].additional);
                        }
                        
                    },actionParams,false);
                }
            }else{
                var apexFunc = listView[indexNo].apexFunction;
                if(apexFunc){
                    var params = listView[indexNo].params;
                    this.getViewFromApexFunction(cmp, apexFunc, recordID, params,helper);
                }
            }
        }
    },
    
    getResultCount : function(cmp){
        var listView = cmp.get("v.viewList");
        
        if(listView){
            var indexNo = cmp.get("v.viewListIndex");
            //var recordID = cmp.get("v.talentId");
            var recordID = cmp.get("v.accountId");
            var countSoql = listView[indexNo].countSoql;    
            if(countSoql){
                if(countSoql != ''){
                    countSoql = countSoql.replace("%%%parentRecordId%%%","'"+recordID+"'")
                    var params = {"soql": countSoql};
                    cmp.set("v.loadingCount",true);
                    
                    this.callServer(cmp,'','','getRowCount',function(response){
                        cmp.set("v.recordCount", response);
                        cmp.set("v.loadingCount",false);
                    },params,false);
                    
                }else{
                    cmp.set("v.loadingCount",false);
                }
            }
        }
        
    },*/
	//Sandeep : new method to create Preview cmp -->
	createResumePreviewCmp : function(cmp,evt){
	    var params = {recordId: cmp.get("v.recordId"),
					showUploadButton:false,
					contactRecord :cmp.get("v.contactRecord"),
                    talentId : cmp.get("v.accountId")};
	$A.createComponent(
            "c:C_TalentResumePreview",
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    /* if(componentName == 'C_TalentResumePreview'){ // Sandeep :for C_TalentResumePreview cmp assign instance to extra C_TalentResumePreview_leftpanel 
                        cmp.set("v.C_TalentResumePreview_leftpanel",newComponent);
                    }*/
                    cmp.set("v.C_TalentResumePreview", newComponent);
					cmp.set("v.resumeLoaded",true)
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
        
    
	},

    createCandidateStatusBadge : function(cmp,evt){
        
        var params = {
                    candidateStatus: cmp.get("v.candidateStatus"),
                    class : "slds-m-right_x-small"
                };
        $A.createComponent(
                "c:C_TalentStatusBadge",
                params,
                function(newComponent, status, errorMessage){
                    
                    if (status === "SUCCESS") {
                        
                        cmp.set("v.C_TalentStatusBadge", newComponent);
                        
                    }
                    else if (status === "INCOMPLETE") {
                       // console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                    else if (status === "ERROR") {
                       // console.log("Error: " + errorMessage);
                        // Show error message
                    }
                }
            );
        
    
    },
    
    updateLoading: function(cmp) {
        if (!cmp.get("v.loadingData") && !cmp.get("v.loadingCount")) {
            cmp.set("v.loading",false);
        }
    },
    
    refreshList : function(cmp) {
        cmp.set("v.reloadData", true);
    },
    
    createComponent : function(cmp, componentName, targetAttributeName, params) {
        $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        //console.log("Error: " + errorMessage);
                        // Show error message
                    }
            }
        );
    },
    //Sandeep:set left div height based on right div height
    setRightDivheight : function(component){
        let renderCalled =false;
        if(!renderCalled){
            renderCalled =true;
        let screenheight = document.documentElement.clientHeight;
        //let screenheight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        console.log('screenheight ',screenheight);
        let modalContentdiv =   document.getElementById("modalcontent");
        let leftDiv = document.getElementById("leftdiv"); 
        let rightDiv = document.getElementById("rightdiv");
 
        if(modalContentdiv){
            modalContentdiv.style.height = screenheight-250 +'px'; 
            //console.log('modalContentdiv.style.height  ',modalContentdiv.style.height );
        }
        
        if(rightDiv){
            rightDiv.style.height = screenheight-260 +'px';
        }
        if(leftDiv){
            leftDiv.style.height = screenheight-260 +'px';
        } 
        }
    }, 

     validateSalary: function(component, validTalent){

       var minSalaryField = component.find("desiredsalary"); 
       var maxSalaryField = component.find("desiredsalarymax");
	   if(!$A.util.isUndefinedOrNull(minSalaryField) && !$A.util.isUndefinedOrNull(maxSalaryField)){
			  var minSalary = minSalaryField.get("v.value");
			  var maxSalary = maxSalaryField.get("v.value");

		   if($A.util.isEmpty(minSalary) && !$A.util.isEmpty(maxSalary)){
			minSalaryField.set("v.isError",true);
			minSalaryField.set("v.errors", [{message:"Please enter Min. Salary"}]);
			validTalent = false;
		   }else{
			minSalaryField.set("v.isError",true);
			minSalaryField.set("v.errors", [{message:""}]);
		   }

		   if(minSalary > maxSalary && !$A.util.isEmpty(maxSalary)){
			maxSalaryField.set("v.errors", [{message:"Min salary cannot be greater than Max. Salary"}]);
			validTalent = false;
			maxSalaryField.set("v.isError",true);
			$A.util.addClass(maxSalaryField,"slds-has-error show-error-message");
			this.showError("Not all the fields on the Summary Tab passed validation. Please review your changes.", "Error");
		   }else{
			maxSalaryField.set("v.errors", [{message:""}]);
			maxSalaryField.set("v.isError",false);
			$A.util.addClass(maxSalaryField,"slds-has-error show-error-message");
		   }
	   }
       
       return validTalent;

    },

    validateRate: function(component, validTalent){

       var minField = component.find("desiredrateId"); 
       var maxField = component.find("desiredratemaxId");
       var freqField = component.find("ratetypeId");
	   if(!$A.util.isUndefinedOrNull(minField)){
       var freqValue = freqField.get("v.value");
       var minValue = minField.get("v.value");
       var maxValue = maxField.get("v.value");
       //Added RF check condition by akshay for S - 82338
	   
		   if($A.util.isEmpty(freqValue) && !$A.util.isEmpty(minValue)){
			freqField.set("v.errors", [{message: "Please select Rate Frequency"}]);
			validTalent = false;
			this.showError("Please review the highlighted errors and try again.", "Error");
		   }else{
			freqField.set("v.errors", null);
			freqField.set("v.isError", false);
			}

		   if($A.util.isEmpty(minValue) && !$A.util.isEmpty(maxValue)){
			minField.set("v.isError", true);
			minField.set("v.errors", [{message: "Please enter Min. Rate"}]);
			validTalent = false;
			$A.util.addClass(maxField,"slds-has-error show-error-message");
			this.showError("Not all the fields passed validation.", "Error");
		   }else{
			minField.set("v.errors", [{message: ""}]);
			minField.set("v.isError", false);
		   }
      
		   if(minValue > maxValue && !$A.util.isEmpty(maxValue)){
			maxField.set("v.errors", [{message:"Min Rate cannot be greater than Max. Rate"}]);
			validTalent = false;
			maxField.set("v.isError", true);
			$A.util.addClass(maxField,"slds-has-error show-error-message");
			this.showError("Not all the fields on the Summary Tab passed validation. Please review your changes.", "Error");
		   }else{
			maxField.set("v.errors", [{message:""}]);
			maxField.set("v.isError", false);
			$A.util.removeClass(maxField,"slds-has-error show-error-message");
		   }  
		}
       return validTalent;     

    },

	getKeyByValue : function(map, searchValue) {
		for(var key in map) {
			if (map[key] === searchValue) {
				return key;
			}
		}
	},

	setReRenderAttributes : function (component) {
		component.set("v.parentInitialLoad",false);
	},

    toggleAccordion: function(component, event, toggleState) {
        var toggleState;

        var acc = component.get("v.acc");    
        var accordionId = event.currentTarget.id;
        var icon = acc[accordionId].icon;
        var className = acc[accordionId].className;     
                
        component.set(`v.acc[${accordionId}].icon`, toggleState[icon]);
        component.set(`v.acc[${accordionId}].className`, toggleState[className]); 
        event.stopPropagation();   
    }

})