({
	doInit: function (component, event, helper) {
		 helper.toggleClass(component,'backdropAddEditSumm','slds-backdrop--');
		 helper.toggleClass(component,'modaldialogAddEditSumm','slds-fade-in-');
		component.set("v.isModalOpen", true);
        
        /* Monika - focus close button on modal open */
      
      const modalCloseButton = component.find("modalCloseBtn");
        window.setTimeout(
            $A.getCallback(function() {
               // wait for element to render then focus
               modalCloseButton.focus();
            }), 100
        );
      
      //End Monika
	  /* Dasaradh - #user story #S-181984 Keyboard shortcut for G2 page save & close that is compatible with Windows and Mac os
                     ctrl+A should work for Windows
                     cmd+A should work for Mac.
                     Note: Could use custom labels for keycodes if necessary.
       */
       var Name = "Not known"; 
        if (navigator.appVersion.indexOf("Win") != -1) Name =  
          "Windows OS";
        if (navigator.appVersion.indexOf("Mac") != -1) Name =  
          "MacOS"; 
		  
		  var a = function(e){
		  if(component.isValid()){
            var modifier = e.ctrlKey || e.metaKey;
			var shiftkey = e.shiftKey;
           if(modifier && shiftkey && e.which == 90 && (Name == 'Windows OS' || Name == 'MacOS')){
               e.preventDefault();
			   helper.saveCandidateSummary(component, event, helper);
			   }
          }
       }
       window.addEventListener('keydown', $A.getCallback(a));//End Dasaradh  
//console.log('---name:'+component.getName()+'--:init timestamp:'+Date.now());
		 /*if (component.get("v.runningUser")) {
			helper.loadDncReasons(component);
		 }*/
		 if(!component.get("v.resumeLoaded")){
			helper.createResumePreviewCmp(component, event);
		 }

	},

	/*userDataReceived: function(component, event, helper) {
		helper.loadDncReasons(component);
	},*/

	showSpinner : function(component, event, helper) {
		var spin = component.get('v.showSpinner');

		if(spin) {
			component.set('v.showSpinner', false);
		} else {
			component.set('v.showSpinner', true);
		}
	},

	//Dasaradh - S-189680 to destroy the component-->
	destoryCmp: function(component, event, helper) {
	        component.destroy();
	},

  	hideModal : function(component, event, helper){
		//Rajeesh stop writing client side backup
		component.set('v.eraseBackup',true);
		var dataBackup = component.find("clientSideBackup"); 
		dataBackup.clearDataBackup();
		helper.focusSource(component);
		component.destroy();
	},

	closeModal : function(component, event, helper) {
		helper.closeModal(component);
		helper.focusSource(component);
   },

   split_toggle : function(component, event, helper) {
        let maindiv = component.find("maindiv_split");
        $A.util.toggleClass(maindiv, "fullRight");
        $A.util.toggleClass(maindiv, "split");
        let iconName = component.get("v.iconName");
      if(iconName == "right"){
         component.set("v.iconName","left"); 
         component.set("v.toggleLayout", "oneColumn");
      }else{
         component.set("v.iconName","right");
         component.set("v.toggleLayout", "fourColumn");
      }
  },
  toggleLayout: function(component, event, helper) {
        let newLayout = component.get("v.toggleLayout");
        component.set("v.toggleLayout", newLayout);
   },

   saveCandidateSummary : function(cmp, event, helper) {
	 //cmp.set('v.disableSave', true);
	 var g2Comm = cmp.find("g2CommentsId") ? cmp.find("g2CommentsId").get("v.value") : '' ;
	 var addEdit = cmp.find("talentAddEdit");
	 addEdit.validateAndSaveTalentOnEdit(cmp.get("v.isG2Checked"), g2Comm );

  },

  talentDataLoaded : function(component, event, helper) {
	component.set("v.isG2DateToday",event.getParam('isG2DateToday'));
	component.set("v.RWSDoNotCallFlag",event.getParam('RWSDoNotCallFlag'));
	helper.createCandidateStatusBadge(component, event);
  },

  openConsentPrefModal : function(component, event, helper) {
		let consentPrefAccess = component.get("v.consentPrefInfo").hasConsentPrefAccess;
		let modalBody;
		$A.createComponent("c:C_ConsentPreferencesModal", {"modalOpeningSource":"TalentLandingPage", "contactID":component.get("v.contactId"), "recordId":component.get("v.contactId"), "isTextConsentAllowed":consentPrefAccess},
			function(content, status) {
				if(status==="SUCCESS") {
					modalBody = content;
					component.find('overlayLib').showCustomModal({                      
						body : modalBody,
						 cssClass: "reviewconsmodal",
						closeCallback : function() {
							//alert('Modal Closed');
						}
					})
				}
			}
		);
   },	
  /* Monika - focus close button on next tab of Save button */	
  focusCloseBtnOnTab: function(component, event, helper) { 	
		if ( event.keyCode == 9 ) {	
      const modalCloseButton = component.find("modalCloseBtn");        	
       window.setTimeout(	
            $A.getCallback(function() {	
               // wait for element to render then focus	
               modalCloseButton.focus();	
            }), 100	
        );	
        }	
     event.preventDefault();	
   	/*Ashwini - as the focus is getting trapped between save and close 
	trying to set focus on cancel on shfttab from save*/
      if (event.shiftKey && event.keyCode == 9) {
			const cancelButton = component.find("btnCancel");
			window.setTimeout(
				$A.getCallback(function() {
					// wait for element to render then focus
					cancelButton.focus();
				}), 100
			);
		}
    },
    
    /* Ashwini - getting focus on save button on reverse tab from close button  */	
    focusSaveFromClose : function(component, event, helper) {
	//event.preventDefault();
        if (event.shiftKey && event.keyCode == 9) {
            	const saveButton = component.find("btnSave");
			window.setTimeout(
				$A.getCallback(function() {
					// wait for element to render then focus
					saveButton.focus();
				}), 100
			);
			event.preventDefault();
        }
    }
})