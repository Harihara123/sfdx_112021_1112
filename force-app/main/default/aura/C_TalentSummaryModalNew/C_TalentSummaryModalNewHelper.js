({
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },

	closeModal : function(component) {
        component.set("v.isModalOpen", false);
        this.toggleClassInverse(component,'backdropAddEditSumm','slds-backdrop--');
        this.toggleClassInverse(component,'modaldialogAddEditSumm','slds-fade-in-');
        component.set("v.callServer",true);
        component.set("v.ProfileTabCreated", false);
        // var e = component.getEvent("refreshTalentHeaderComponent");
        var appEvent = $A.get("e.c:RefreshTalentHeader");
        appEvent.fire();
        
        $A.get('e.force:refreshView').fire();
        component.destroy();
    },

	createResumePreviewCmp : function(cmp,evt){
	    var params = {recordId: cmp.get("v.recordId"),
					showUploadButton:false,
					contactRecord :cmp.get("v.contactRecord"),
                    talentId : cmp.get("v.accountId")};
		$A.createComponent(
            "c:C_TalentResumePreview",
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    /* if(componentName == 'C_TalentResumePreview'){ // Sandeep :for C_TalentResumePreview cmp assign instance to extra C_TalentResumePreview_leftpanel 
                        cmp.set("v.C_TalentResumePreview_leftpanel",newComponent);
                    }*/
                    cmp.set("v.C_TalentResumePreview", newComponent);
					cmp.set("v.resumeLoaded",true)
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
        
    
	},

    createCandidateStatusBadge : function(cmp,evt){
        
        var params = {
                    candidateStatus: evt.getParam("candidateStatus"),
                    class : "slds-m-right_x-small"
                };
        $A.createComponent(
                "c:C_TalentStatusBadge",
                params,
                function(newComponent, status, errorMessage){
                    
                    if (status === "SUCCESS") {
                        
                        cmp.set("v.C_TalentStatusBadge", newComponent);
                        
                    }
                    else if (status === "INCOMPLETE") {
                       // console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                    else if (status === "ERROR") {
                       // console.log("Error: " + errorMessage);
                        // Show error message
                    }
                }
            );
        
    
    },
	saveCandidateSummary : function(cmp, event, helper) {
	 //cmp.set('v.disableSave', true);
	 var g2Comm = cmp.find("g2CommentsId") ? cmp.find("g2CommentsId").get("v.value") : '' ;
	 var addEdit = cmp.find("talentAddEdit");
	 addEdit.validateAndSaveTalentOnEdit(cmp.get("v.isG2Checked"), g2Comm );

  },

  focusSource : function(component){
	var fieldToGetFocus = (component.get("v.source")=="Profile" ? "tlp-header-actions" : (component.get("v.source")=="Summary"?"TalentSummarySectionEditBtn":""));
	if(fieldToGetFocus=="" || fieldToGetFocus == null) return;
	var cmpEventFromClose = $A.get('e.c:E_FocusField');
	cmpEventFromClose.setParams({"fieldIdToGetFocus":fieldToGetFocus});
	cmpEventFromClose.fire();
  }
})