({
	onClick : function(cmp, event, helper) {
		var action = cmp.get("c.createOpp");
		var numberofRecords=  cmp.find("NumberOfRecords").get("v.value");
		var opco= cmp.find("Opco").get("v.value");
		action.setParams({
             length: numberofRecords,
             FName: cmp.get("v.RecordName"),
			 Opco:opco,
			 ObjName:JSON.stringify(cmp.get("v.ObjectName"))
        });

action.setCallback(this, function(response) {
            var state = response.getState();
           if (state === "SUCCESS"){
               var serverResponse = response.getReturnValue();
			   helper.showToast(cmp, event, helper);
			 }    
        });
		$A.enqueueAction(action);
	},
// this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
	handleChange: function (cmp, event) {
        // This will contain an array of the "value" attribute of the selected options
        cmp.set("v.ObjectName",event.getParam("value"));
        //alert("Option selected with value: '" + selectedOptionValue.toString() + "'");
    }
    
	

})