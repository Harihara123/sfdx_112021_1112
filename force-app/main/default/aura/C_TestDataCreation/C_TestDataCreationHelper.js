({
	showToast: function (component, event, helper) {
	var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Success!",
		 "type": "success",
        "message": "The records  will be created in few seconds."
    });
    toastEvent.fire();


	}
})