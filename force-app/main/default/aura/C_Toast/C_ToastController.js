({
    handleVisibility : function(component, event, helper) {
        var params = component.get('v.parameters');
        helper.setType(component);
        var display = component.get('v.display');
        var toastBody = component.find('toastBody');
        if($A.util.isUndefinedOrNull(params.type)){
            params.type="error";
            component.set('v.parameters',params);
        }
        if(display) {
            $A.util.removeClass(toastBody, 'slds-hide');

            if(params.mode == 'dismissible') {                
                window.setTimeout($A.getCallback(function() {
                    component.set('v.display', false);
                }), 3000);
            }
        }
        else {
            $A.util.addClass(toastBody, 'slds-hide');
        }
    },

    close : function(component, event, helepr) {
        component.set('v.display', false);
    }
})