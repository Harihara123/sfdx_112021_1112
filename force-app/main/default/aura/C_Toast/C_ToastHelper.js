({
    setType : function(component) {
        var type = component.get('v.parameters').type;

        if(type == 'error') {
            component.set('v.sldsTheme', 'slds-theme_error');
            component.set('v.sldsIconUtility', 'slds-icon-utility-error');
            component.set('v.sldsType', 'error');
        }
        else if(type == 'warning') {
            component.set('v.sldsTheme', 'slds-theme_warning');
            component.set('v.sldsIconUtility', 'slds-icon-utility-warning');
            component.set('v.sldsType', 'warning');
        }
        else if(type == 'success') {
            component.set('v.sldsTheme', 'slds-theme_success');
            component.set('v.sldsIconUtility', 'slds-icon-utility-success');
            component.set('v.sldsType', 'success');
        }
    }
})