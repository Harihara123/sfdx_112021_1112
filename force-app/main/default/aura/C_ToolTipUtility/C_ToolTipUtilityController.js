({
	display : function(component, event, helper) {
    	//helper.toggleHelper(component, event);
    	 var toggleText = component.find("tooltip");
	    $A.util.removeClass(toggleText, "slds-fall-into-ground");
        $A.util.addClass(toggleText, "slds-rise-from-ground");
	},

	displayOut : function(component, event, helper) {
		//helper.toggleHelper(component, event);
    	 var toggleText = component.find("tooltip");
	    $A.util.addClass(toggleText, "slds-fall-into-ground");
        $A.util.removeClass(toggleText, "slds-rise-from-ground");
        
	}
})