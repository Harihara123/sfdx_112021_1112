({
    init: function(component, event, helper) {
let trackingSwitch = component.get('c.getUserTrackingSwith');
trackingSwitch.setCallback(this, function(response) {
    let switchSetting = response.getReturnValue();

    if (!switchSetting.Global__c) {
		// tracking is off
    } else {
let groupAPermissions = component.get('c.checkGroupPermission');
groupAPermissions.setCallback(this, function(response) {
let permissionsEnabled = response.getReturnValue();

    if (permissionsEnabled || switchSetting.StudyGroup__c) {
	// window.onbeforeunload = function() {
    // saveFormData();
    // return null
	// };
        component.set('v.enabled', true);
        helper.init(component, event);
                            
                    } else {
                    //  tracking permission not found
                    }
        });
                
                 $A.enqueueAction(groupAPermissions);      
        }
                });
                
        $A.enqueueAction(trackingSwitch);

        // let data = component.get('v.state.session');
        //
        //                 function compare(a,b) {
  		// 				if (a.sequence < b.sequence)
		// 			    return -1;
		// 				if (a.sequence > b.sequence)
		// 			    return 1;
		// 				return 0;
		// 				}
        //
        //                 for (let inc = 0; inc < data.length; inc++) {
        //                     let interactions = data[inc].activity.interactions || [];
		// 					interactions.sort(compare);
		// 					data[inc].activity.interactions = interactions;
        //                 }
        //
        //                 component.set('v.state.session', data);
        
	// 	function saveFormData() {
    //     let action = component.get('c.saveRecord');
    //     action.setParams({"userActivityJSON": JSON.stringify(component.get('v.state'))});
    //     $A.enqueueAction(action);
    // }

    },
    downloadFile: function(component, e, helper) {
        helper.downloadFile(component, e);
    },

updateResults: function(component, e, helper) {
    if (component.get("v.windowURL") != window.location.href.split("?")[0]) {
        // push new page object
        component.set("v.windowURL", window.location.href.split("?")[0]);
		helper.setSessionLocation(component, event);
    }
	setTimeout(() => {
	let talentCards = [];
	let reversedDivsClicked = (((component.get('v.state.session')[component.get('v.state.session').length - 1] || {}).activity || {}).interactions || []).reverse();
	let currentPage = null;

    
    let i = 0;
    while (i < reversedDivsClicked.length) {
        if (reversedDivsClicked[i].dataTracker.includes('search_button') || reversedDivsClicked[i].dataTracker.includes('keywords_search')) {
          	currentPage = '1';
            break;
        } else if (reversedDivsClicked[i].dataTracker.includes('pagination--')) {
            if (reversedDivsClicked[i].dataTracker.includes('pagination--goToPage-')) { 
            currentPage = reversedDivsClicked[i].dataTracker.replace('pagination--goToPage-', '');
            break;
        } else if (reversedDivsClicked[i].dataTracker.includes('pagination--goToFirst')) {
			currentPage = '1';
            break;
        }
	}
    i++;
    }
        let resOutput = {};
        resOutput.resultsPage = currentPage || '1';
        resOutput.transactionId = component.get('v.transactionId');
        resOutput.subTransactionId = component.get('v.subTransactionId');
        resOutput.requestId = component.get('v.requestId');
	    resOutput.pageSize = component.get('v.pageSize');
	    resOutput.filters = {
            location: component.get('v.queryLocation'),
            engine: (component.get("v.engineType") || {}).engine,
            queryType: (component.get("v.engineType") || {}).type,
        };
    
	    if (resOutput.filters.queryType === 'match') {
        resOutput.filters.matchType = (component.get("v.engineType") || {}).matchType;
        resOutput.filters.eagernessFilter = component.get('v.eagernessFilter') ? 'ON' : 'OFF';            
    	}
    
	    Object.keys(component.get("v.filters")).forEach(filterKey => {
            resOutput.filters[filterKey] = component.get("v.filters")[filterKey]
        })
	    resOutput.timeStamp = new Date().getTime();
        resOutput.inViewport = [];
        resOutput.scrolledInViewport = [];
    	resOutput.resultsReturned = [];

        let notInViewport = [];
            let trackedItems = document.querySelectorAll('[data-tracker]');
            if (trackedItems.length) {
                for (let i = 0; i < trackedItems.length; i++ ) {
                    if (trackedItems[i].getAttribute('data-tracker').includes('talent_card')) {
                        talentCards.push(trackedItems[i]);
                    }
                }
            }
    		/** Log interest score for all results **/
		    for (let k = 0; k < talentCards.length; k++) {
                let id = talentCards[k].getAttribute('data-tracker').split('--')[1];
                if (!id) {
                    id = 'RECORD HAS NO ID';
                } 
                let pair = {};
                pair.id = id;
                pair['interestScore'] = talentCards[k].getAttribute('data-interest');
                resOutput.resultsReturned.push(pair);
            }    	
    	
            /** Check what results are immediately in viewport **/
            for (let k = 0; k < talentCards.length; k++) {
                let bounding = talentCards[k].getBoundingClientRect();
                if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
                    resOutput.inViewport.push(talentCards[k].getAttribute('data-tracker'));
                } else {
                    notInViewport.push(talentCards[k])
                };
            };
        
        let session = component.get('v.state.session');
        if (!session.length) {
		helper.setSessionLocation(component, event);
        }
    	
    	if (!session[session.length - 1].activity.resultsOutput) {
    		session[session.length - 1].activity.resultsOutput = [];
		}
 
 		let duplicate = session[session.length - 1].activity.resultsOutput.find(output => 
 	output.transactionId === resOutput.transactionId 
 && output.subTransactionId === resOutput.subTransactionId 
 && output.requestId === resOutput.requestId);

if (duplicate) {
    return
}

 		session[session.length - 1].activity.resultsOutput.push(resOutput);

		component.set('v.state.session', session);
		let scrolledItem = window;
		if (document.getElementsByClassName('results-output').length) {
    	scrolledItem = document.getElementsByClassName('results-output')[0].firstElementChild;
		}
        /** Check what results entered viewport on user scrolling **/
        scrolledItem.addEventListener('scroll', () => {
        for (let z = 0; z < notInViewport.length; z++) {
           let bound = notInViewport[z].getBoundingClientRect();
           if (bound.top >= 0 && bound.left >= 0 && bound.right <= (window.innerWidth || document.documentElement.clientWidth) && bound.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
            // let resOutput = {}; 
            // (resOutput.scrolledInViewport || []).push(notInViewport[z].getAttribute('data-tracker'));

            let sessionResultsOutput = session[session.length - 1].activity.resultsOutput[session[session.length - 1].activity.resultsOutput.length - 1] || {};	
               sessionResultsOutput.scrolledInViewport.push(notInViewport[z].getAttribute('data-tracker'));
               session[session.length - 1].activity.resultsOutput[session[session.length - 1].activity.resultsOutput.length - 1] = sessionResultsOutput;
               component.set('v.state.session', session);
               notInViewport.splice(z, 1);
           }
       }
       });
        
}, 1000);
    },
        
    handleTrackingEvent: function(component, event, helper) {
        	let enabled = component.get('v.enabled');
        	if (enabled) {
            var dataTracker = event.getParam("clickTarget");
        	var dataType = event.getParam("clickType");
            let input = event.getParam("inputValue");    
        	let session = component.get('v.state.session');    
                
            if (event.getParam("filter")) {
                let filter = event.getParam("filter");
                let existingFilters = component.get('v.filters');
				Object.keys(filter).forEach(key => {
				  existingFilters[key] = filter[key];
				});
                component.set('v.filters', existingFilters);
            }
		
			if (event.getParam("engineType")) {
            component.set("v.engineType", event.getParam("engineType"));        
            }

    if (!session.length && dataTracker) {
    let pageUrls = [
    {page: 'Home', urlSegment: 'home'},
    {page: 'Accounts', urlSegment: 'account'},
    {page: 'Contacts', urlSegment: 'contact'},
    {page: 'My Activities', urlSegment: 'my_activities'},
    {page: 'My Lists', urlSegment: 'my_lists'},
    {page: 'Opportunities', urlSegment: 'opportunity'},
    {page: 'Req Search', urlSegment: 'search_reqs'},
    {page: 'Talent Search', urlSegment: 'ats_candidatesearch'},
    {page: 'Find or Add Talent', urlSegment: 'find_or_add_candidate'},
    {page: 'Chatter', urlSegment: 'chatter'},
    {page: 'Dashboard', urlSegment: 'dashboard'},
    {page: 'Reports', urlSegment: 'report'}
    // TODO add all pages
	];
	let checkPage = (url) => {
    let currentPage = pageUrls.find(page => url.toLowerCase().includes(page.urlSegment));
    if (currentPage) {
        return currentPage.page;
    }
	};
     			    let startPage = checkPage(window.location.href);
				    let pageObj = {page: startPage || 'undefined', pageSeq: 1, activity: {}};
                    session.push(pageObj);
    				component.set('v.state.session', session);
 	}
                
	        if (!((session[session.length - 1] || {}).activity || {}).interactions && dataTracker) {
            session[session.length - 1].activity.interactions = [];
    	    }
        if (dataTracker) {
        	let divClick = {
				dataTracker: dataTracker.split(' |')[0],
                dataType: dataType,
                delayTime: new Date().getTime() - component.get('v.state.sessionStats.lastClickTime'),
                sequence: session[session.length - 1].activity.interactions.length + 1,
                coordinates: {},
                input: input || null
                };
        
        	session[session.length - 1].activity.interactions.push(divClick);
        	
        	component.set('v.state.session', session);
			component.set('v.state.sessionStats.lastClickTime', new Date().getTime());     
        } 
            } else {
             //   console.log('[TRACKING]: tracking is off, data logging cancelled');
            }
            },
                
	handleOpco: function(component, event, helper) {
    component.set('v.state.sessionStats.opco', (event.getParam("opco")));  
    component.set('v.state.sessionStats.userId', (event.getParam("userId")));  
    component.set('v.state.sessionStats.userGroups', (event.getParam("userGroups").join(', ')));
                },
	
	handleServerCallEvent:function(component, event, helper) {
        if (event.getParam("transaction")) {
             component.set("v.transactionId", event.getParam("transaction"));
             component.set("v.filters", {});
        }
        if (event.getParam("subTransactionId")) {
             component.set("v.subTransactionId", event.getParam("subTransactionId"));              
        }
	},
	
	updateRequest: function(component, event, helper) {
	component.set("v.requestId", event.getParam("request"));                   	
	},
        
    handlePageResize: function(component, event, helper) {
    component.set('v.pageSize', (event.getParam("pageSize")));        
    },
	eagernessHandler: function(component, event, helper) {
	component.set('v.eagernessFilter', event.getParam('eagernessFilter'));            
    },
	setQueryLocation: function(component, event, helper) {
    component.set('v.queryLocation', event.getParam('location'));
    }
})