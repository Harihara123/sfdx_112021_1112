({
    init: function(component, event, helper) {
        // save session params
        let sessionTime = new Date().getTime();
        component.set('v.state.sessionStats.sessionTime', sessionTime);
        let windowWidth = window.innerWidth;
        let windowHeight = window.innerHeight;
        component.set('v.state.sessionStats.screenSize.width', windowWidth);        
        component.set('v.state.sessionStats.screenSize.height', windowHeight);
        let browser = 'Unknown';
        if (!!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime)) {
            browser = 'Chrome';                
        } else if (navigator.userAgent.indexOf("Firefox") != -1 ) {
            browser = 'Firefox';  
        } else if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
            browser = 'Opera';
        } else if ((navigator.userAgent.indexOf("Safari") != -1)) {
            browser = 'Safari';
        } else if ((navigator.userAgent.indexOf("MSIE") != -1 )) {
            browser = 'IE';            
        } else if (!(navigator.userAgent.indexOf("MSIE") != -1 ) && !!window.StyleMedia) {
            browser = 'Edge';                        
        } 
        component.set('v.state.sessionStats.browser', browser);

        let body = document.querySelectorAll('body')[0];

        let visualizationMode = localStorage.getItem('visualizationMode');
        if (!visualizationMode) {
            localStorage.setItem('visualizationMode', 'ON');
        }

        if (visualizationMode === 'ON') {
            // THIS BLOCK IS NOT EXECUTED WHEN VISUALIZATION IS OFF
            let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            svg.setAttribute('id', 'tracking-canvas');
            svg.style.background = 'rgba(0,0,0,.2)';
            svg.style.position = 'fixed';
            svg.style.top = '0';
            svg.style.right = '0';
            svg.style.bottom = '100px';
            svg.style.left = '0';
            svg.style.width = '100%';
            svg.style.height = '100%';

            // cannot use a class here to toggle tracking canvas as it belongs to body object
            // hence setting a class in the tracking component will not help
            // toggle opacity + z-index instead

            let container = document.createElement('div');
            container.setAttribute('id', 'tracking-container');
            container.style.position = 'fixed';
            container.style.top = '0';
            container.style.right = '0';
            container.style.bottom = '100px';
            container.style.left = '0';
            container.style.width = '100%';
            container.style.height = '100%';
            container.style.opacity = '0';

            container.appendChild(svg);
            body.appendChild(container);

        }

        if (component.get('v.tracking') === 'plugin') {
            console.log('[TRACKING] Mode: plugin');
            component.set('v.state.sessionStats.lastClickTime', new Date().getTime());
            body.onkeydown = (e) => {           
                if (e.shiftKey && e.altKey && e.keyCode === 83) {
                    // Shift + Alt + S - toggle switch
                    let newValue = (localStorage.getItem('switchTracking') === null || localStorage.getItem('switchTracking') === 'false') ? 'true' : 'false';
                    localStorage.setItem('switchTracking', newValue);
                    } else if (e.shiftKey && e.altKey && e.keyCode == 68) {
                        // Shift + Alt + D - download file
                        var element = document.createElement('a');
                        
                        let data = component.get('v.state.session');
                        
                        function compare(a,b) {
  						if (a.sequence < b.sequence)
					    return -1;
						if (a.sequence > b.sequence)
					    return 1;
						return 0;
						}
                        
                        for (let inc = 0; inc < data.length; inc++) {
                            let interactions = data[inc].activity.interactions || [];
							interactions.sort(compare);
							data[inc].activity.interactions = interactions;                            
                        }
                        
                        component.set('v.state.session', data);
                        
                        
                        element.setAttribute('href', 'data:appliaction/json;charset=utf-8,' + encodeURIComponent(
                            JSON.stringify(component.get('v.state'))
                        ));
                        element.setAttribute('download', 'userMetrics.json');
                        element.style.display = 'none';
                        document.body.appendChild(element);
                        element.click();
                        document.body.removeChild(element);
                    }
                    if (e.shiftKey && e.altKey && e.keyCode == 72) {
                         return;
                         if (document) {
                         let trackedElements = document.querySelectorAll('[data-tracker]');
                             let pagination = document.querySelectorAll('[data-section="pagination"]')[0];

                         for (let h = 0; h < trackedElements.length; h++) {

                             if (trackedElements[h].style.boxShadow && trackedElements[h].style.boxShadow !== 'none') {
                                 trackedElements[h].style.boxShadow = 'none';
                                 trackedElements[h].style.borderRadius = '0px';
                             } else {
                                 trackedElements[h].style.borderRadius = '3px';
                                 trackedElements[h].style.boxShadow = '0 0 15px #c70039';
                             }
                         }}
                     }
                };
        
	let pageUrls = [
    {page: 'Home', urlSegment: 'home'},
    {page: 'Accounts', urlSegment: 'account'},
    {page: 'Contacts', urlSegment: 'contact'},
    {page: 'My Activities', urlSegment: 'my_activities'},
    {page: 'My Lists', urlSegment: 'my_lists'},
    {page: 'Opportunities', urlSegment: 'opportunity'},
    {page: 'Req Search', urlSegment: 'search_reqs'},
    {page: 'Talent Search', urlSegment: 'ats_candidatesearch'},
    {page: 'Find or Add Talent', urlSegment: 'find_or_add_candidate'},
    {page: 'Chatter', urlSegment: 'chatter'},
    {page: 'Dashboard', urlSegment: 'dashboard'},
    {page: 'Reports', urlSegment: 'report'}
    // TODO add all pages
	];
	let checkPage = (url) => {
    let currentPage = pageUrls.find(page => url.toLowerCase().includes(page.urlSegment));
    if (currentPage) {
        return currentPage.page;
    }
	};

            const handleInput =  (e) => {
            let session = component.get('v.state.session');
			if (session[session.length - 1]) {
(((session[session.length - 1].activity || {}).interactions || [])[((session[session.length - 1].activity || {}).interactions || []).length - 1] || {}).input = e.target.value;
component.set('v.state.session', session);
					}
            }
            const handleBlur = (e) => {
            //    console.log('removing all dynamic listeners');
                e.target.removeEventListener('input', handleInput);
                e.target.removeEventListener('input', handleBlur);

            }

				// create first page object, push to state
                // capture click
                body.onclick = (e) => {
    				let session = component.get('v.state.session');
				    if (!session.length) {
     			    let startPage = checkPage(window.location.href);
				    let pageObj = {page: startPage || 'undefined', pageSeq: 1, activity: {}};
                    session.push(pageObj);
    				component.set('v.state.session', session);
					component.set("v.windowURL", window.location.href.split("?")[0]);
 					} else {
 					let currentPageObj = session[session.length - 1];
						if (currentPageObj.page !== checkPage(window.location.href)) {
    					// new page transition happened

                        let seq = component.get('v.state.session').length + 1;
					    let newPageObj = {page: checkPage(window.location.href) || 'undefined', pageSeq: seq, activity: {}}
                        session.push(newPageObj);
    					component.set('v.state.session', session);
					}}
                    let clickCoordinates = [e.clientX, e.clientY, 10]; // -43 is for sandboxes to deduct height of the Sandbox name

					// NEW STRUCTURE
					let newClicks = session[session.length - 1].activity.clicks;
						if (!newClicks) {
						    newClicks = [];
						}
					newClicks.push(clickCoordinates)
					session[session.length - 1].activity.clicks = newClicks;
					component.set('v.state.session', session);

                    if (e.target) {
                        let limit = 7; // bubble up to 7 levels up
                        let target = e.target;
                        if (target.nodeName === 'path' || target.nodeName === 'svg') {
                            limit = 9; // for buttons with nested icons bubble up to 9 levels up
                        }
                        let i = 0;

                        if (
                            e.target.nodeName === 'BODY'
                            || (e.target.nodeName === 'A' && !e.target.innerHTML)
                            || e.target.getAttribute('id') === 'tracking-canvas'
                            || e.target.getAttribute('id') === 'tracking-container'
                            || e.target.getAttribute('data-tracker') === 'ignore'
                        ) {
                            component.set('v.state.sessionStats.lastClickTime', new Date().getTime());
                            return
                        } else if (e.target.nodeName !== 'INPUT'
                            && e.target.nodeName !== 'TEXTAREA'
                            || (e.target.getAttribute('data-type') === 'input-checkbox' && e.target.querySelector('input'))
                        ) {

                            if (!target.getAttribute('data-tracker') && target) {
                                while (i < limit && target && !target.getAttribute('data-tracker')) {
                                    target = target.parentElement;
                                    i++;
                                    if (target) {
                                        
                                        if (target.getAttribute('data-tracker')) {
                                            break
                                        }
                                    }
                                }
                            }

                            if (!target) {
				            	component.set('v.state.session.lastClickTime', new Date().getTime());
                                return
                            }

                            let targetDiv = target.getAttribute('data-tracker');
                            if (!targetDiv) {
                                component.set('v.state.sessionStats.lastClickTime', new Date().getTime());
                                return
                            }
                            
                            let cardNum = null;
                            let inc = 0;
                            let currentTarget = target;
                            while (inc < 10 && !cardNum && currentTarget) {
                                currentTarget = currentTarget.parentElement;
                                inc++;
                                if (currentTarget && currentTarget.getAttribute('data-voice') && currentTarget.getAttribute('data-voice').includes('talent_card--')) {
                                    cardNum = currentTarget.getAttribute('data-voice');
                                    break
                                }
                            }
							// 5/9 D-10134
			                // e.stopPropagation();
                            
                            let elem = document.querySelectorAll(`[data-tracker="${targetDiv}"]`)[0];
                            let pair = {x: null, y: null}
                            if (elem) {
                            let elWidth = elem.offsetWidth;
                            let elHeight = elem.offsetHeight;
                            var viewportOffset = elem.getBoundingClientRect();
                            let elX = viewportOffset.left;
                            let elY = viewportOffset.top;                                
                            pair = {x: elX + elWidth / 2, y: elY + elHeight / 2};
                            }
                            
                            let extractedValue = null;
                            if (e.target.getAttribute('data-type') === 'input-checkbox' && !e.target.querySelector('input')) {
								return
                            } else if (e.target.getAttribute('data-type') === 'input-checkbox' && e.target.querySelector('input')) {
                                extractedValue = e.target.querySelector('input').checked ? 'checked' : 'unchecked';
                            }
                            
                            let divClick = {
                                dataTracker: targetDiv.split(' |')[0],
                                cardNum: cardNum,
                                dataType: target.getAttribute('data-type'),
                                delayTime: new Date().getTime() - component.get('v.state.sessionStats.lastClickTime'),
                                sequence: (component.get('v.state.session')[component.get('v.state.session').length - 1].activity.interactions || []).length + 1,
                                coordinates: pair,
                                input: extractedValue
                            };

                            let session = component.get('v.state.session');
                            session[session.length - 1].activity.interactions = component.get('v.state.session')[component.get('v.state.session').length - 1].activity.interactions;
                            if (!session[session.length - 1].activity.interactions) {
                                session[session.length - 1].activity.interactions = [];
                            }
                            session[session.length - 1].activity.interactions.push(divClick);
                            component.set('v.state.session', session);                            
                            
                            component.set('v.state.sessionStats.lastClickTime', new Date().getTime());                            


                        } else if (

                            e.target.nodeName === 'INPUT'
                            || e.target.nodeName === 'TEXTAREA'
                            || (e.target.getAttribute('data-type') || '').split('-')[0] === 'input') {


                            let delayTime = new Date().getTime() - component.get('v.state.sessionStats.lastClickTime');

                            if (!target.getAttribute('data-tracker')) {
                                while (i < limit && target && !target.getAttribute('data-tracker')) {
                                    target = target.parentElement;

                                    i++;
                                    if (target.getAttribute('data-tracker')) {
                                        break
                                    }
                                }
                            }

                            if (!target) {
                                component.set('v.state.sessionStats.lastClickTime', new Date().getTime());
                                return
                            }
                            

                            let targetDiv = target.getAttribute('data-tracker');
                            if (!targetDiv) {
                                component.set('v.state.sessionStats.lastClickTime', new Date().getTime());
                                return
                            }
                            
                            let cardNum = null;
                            let inc = 0;
                            let currentTarget = target;
                            while (inc < 10 && !cardNum && currentTarget) {
                                currentTarget = currentTarget.parentElement;
                                inc++;
                                if (currentTarget && currentTarget.getAttribute('data-voice') && currentTarget.getAttribute('data-voice').includes('talent_card--')) {
                                    cardNum = currentTarget.getAttribute('data-voice');
                                    break
                                }
                            }
							// 5/9 D-10134
		             		// e.stopPropagation();
                            
                            let elem = document.querySelectorAll(`[data-tracker="${targetDiv}"]`)[0];
                            let pair = {x: null, y: null}
                            if (elem) {
                            let elWidth = elem.offsetWidth;
                            let elHeight = elem.offsetHeight;
                            var viewportOffset = elem.getBoundingClientRect();
                            let elX = viewportOffset.left;
                            let elY = viewportOffset.top;                                
                            pair = {x: elX + elWidth / 2, y: elY + elHeight / 2};
                            }

                            
                            // [UPD] Andy's solution for delayed input value fetching
                            let extractedValue = null;    
		                    if((e.target.type === 'text' && e.target.type !== 'checkbox') || e.target.nodeName === 'TEXTAREA') {
        	                e.target.addEventListener('input', handleInput);
            	            e.target.addEventListener('blur', handleBlur);
                		    }
                            
                            if (e.target.type == 'checkbox') {
                                extractedValue = e.target.checked;
                            }
                            let divClick = {
                                dataTracker: targetDiv.split(' |')[0],
                                cardNum: cardNum,
                                dataType: target.getAttribute('data-type'),
                                delayTime: delayTime,
                                sequence: (component.get('v.state.session')[component.get('v.state.session').length - 1].activity.interactions || []).length + 1,
                                coordinates: pair,
                                input: extractedValue
                            };

                            let session = component.get('v.state.session');
                            let newInteractions = session[session.length - 1].activity.interactions;
                            if (!newInteractions) {
                                newInteractions = [];
                            }
                            newInteractions.push(divClick);
                            session[session.length - 1].activity.interactions = newInteractions;
                            component.set('v.state.session', session);                                              
                            
                            component.set('v.state.sessionStats.lastClickTime', new Date().getTime());
                        }
                    }
                };

            }
 },

   

    downloadFile: function(component, e, helper) {
		e.stopPropagation();
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(
            // JSON.stringify(component.get('{!v.state.session}'))
            JSON.stringify(component.get('{!v.state}'))
        ));
        element.setAttribute('data-tracker', 'ignore');
		element.innerHTML = 'IGNORE';
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();

       document.body.removeChild(element);
    },
        
    setSessionLocation: function(component, event) {
    let session = component.get('v.state.session');        
            	let pageUrls = [
    			{page: 'Home', urlSegment: 'home'},
    			{page: 'Accounts', urlSegment: 'account'},
    			{page: 'Contacts', urlSegment: 'contact'},
    			{page: 'My Activities', urlSegment: 'my_activities'},
    			{page: 'My Lists', urlSegment: 'my_lists'},
    			{page: 'Opportunities', urlSegment: 'opportunity'},
    			{page: 'Req Search', urlSegment: 'search_reqs'},
    			{page: 'Talent Search', urlSegment: 'ats_candidatesearch'},
    			{page: 'Find or Add Talent', urlSegment: 'find_or_add_candidate'},
    			{page: 'Chatter', urlSegment: 'chatter'},
    			{page: 'Dashboard', urlSegment: 'dashboard'},
    			{page: 'Reports', urlSegment: 'report'}
    			// TODO add all pages
				];
				let checkPage = (url) => {
    			let currentPage = pageUrls.find(page => url.toLowerCase().includes(page.urlSegment));
    			if (currentPage) {
        		return currentPage.page;
    			}
				};
        let startPage = checkPage(window.location.href);
		let pageObj = {page: startPage || 'undefined', pageSeq: 1, activity: {}};
        session.push(pageObj);
    	component.set('v.state.session', session);            
        }    
  
})