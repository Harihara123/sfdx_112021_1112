({
    test: function(cmp, e, h) {
        const data = e.getParam('data');
        console.log('custom event from LWC', data.value)
        var trackingEvent = $A.get("e.c:TrackingEvent");
        trackingEvent.setParam('inputValue', data.value);
        trackingEvent.setParam('clickTarget', data.id);
        trackingEvent.fire();
    }
})