({
	doInit : function(component, event, helper) {
		let trackingSwitch = component.get('c.getUserTrackingSwith');
		trackingSwitch.setCallback(this, function(response) {
	    let switchSetting = response.getReturnValue();

        // checking for Global tracking setting only, no group permissions are needed
    	if ((switchSetting || {}).Global__c) {
    		component.set("v.enabled", true);
        helper.initTracking(component, event);
        }});
		$A.enqueueAction(trackingSwitch);
	},

	handleOpco: function(component, event, helper) {
		helper.handleOpco(component, event);
	},
	dispatchHandler: function(component, event, helper) {
		helper.dispatchHandler(component, event);
	},
	updateResults: function (component, event, helper) {
		helper.updateResults(component, event);
	},
	handleServerCallEvent: function(component, event, helper) {
		if (event.getParam("transaction")) {
            if (component.get("v.transactionId")) {
				component.set("v.filters", {});
            }
			component.set("v.transactionId", event.getParam("transaction"));
		}
		if (event.getParam("subTransactionId")) {
			component.set("v.subTransactionId", event.getParam("subTransactionId"));
		}
	},

	updateRequest: function(component, event) {
		component.set("v.requestId", event.getParam("request"));
	},

	handlePageResize: function(component, event) {
		component.set("v.pageSize", (event.getParam("pageSize")));
	},
	eagernessHandler: function(component, event) {
		component.set("v.eagernessFilter", event.getParam("eagernessFilter"));
	},
	setQueryLocation: function(component, event) {
		component.set("v.queryLocation", event.getParam("location"));
	},
	handleTrackingEvent: function(component, event, helper) {
		helper.handleTrackingEvent(component, event);
	}
})