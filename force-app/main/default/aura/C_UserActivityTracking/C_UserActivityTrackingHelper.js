({
    // #1.
	initTracking : function(component, event) {
        // #2.
        let  connectedTabId = component.get("v.connectedTabId");

        // #2.1.
        if (!connectedTabId) {
            connectedTabId = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            sessionStorage.setItem("connectedTabId", connectedTabId);
            component.set("v.connectedTabId", connectedTabId);
            // #2.2.
        }

        let browserData = this.getBrowserData(component);

            // #3.
            let sessionObject = this.retrieveSessionHelper(component);
            // #3.1.
            if (!sessionObject) {
                sessionObject = {};
                sessionObject.tabsOpen = [];
                sessionObject.tabs = [];
                sessionObject.sessionStats = {};
                sessionObject.sessionStats.sessionTime = new Date().getTime();
                sessionObject.sessionStats.lastClickTime = new Date().getTime();
                sessionObject.sessionStats.screenSize = {};
                sessionObject.sessionStats.screenSize.width = browserData.width;
                sessionObject.sessionStats.screenSize.height = browserData.height;
                sessionObject.sessionStats.browser = browserData.type;
				
                var perfEntries = performance.getEntriesByType("navigation");
                for (var i=0; i < perfEntries.length; i++) { 
                    var p = perfEntries[i];
                	sessionObject.sessionStats.domComplete = p.domComplete;
                }
                localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                component.set("v.sessionObject", sessionObject);
            }
                // #3.2.1.
                if (sessionObject.sessionStats.sessionClosed || (new Date().getTime() - sessionObject.sessionStats.lastClickTime > 2 * 3600 * 1000)) {
                    // #6.                    
                    this.dispatchData(component, event);
                }
                // #3.2.2.
                else {
                    // #3.3.
                    let thisTab = {};
                    thisTab.connectedTabId = connectedTabId;
                    thisTab.session = [];

                    let currentPage = {};
                    currentPage.page = this.getCurrentPage(window.location.href);
                    currentPage.pageSeq = thisTab.session.length + 1;

                    currentPage.activity = {};
                    currentPage.activity.interactions = [];
                    currentPage.activity.resultsOutput = [];
                    thisTab.session.push(currentPage);

                    thisTab.sessionStats = {};
                    thisTab.sessionStats.sessionTime = new Date().getTime();
                    thisTab.sessionStats.lastClickTime = new Date().getTime();
                    thisTab.sessionStats.screenSize = {};
                    thisTab.sessionStats.screenSize.width = browserData.width;
                    thisTab.sessionStats.screenSize.height = browserData.height;
                    thisTab.sessionStats.browser = browserData.type;
                    component.set("v.thisTab", thisTab);
                    if (!sessionObject.tabsOpen.find(tab => tab === connectedTabId)) {
                        sessionObject.tabsOpen.push(connectedTabId);
                        sessionObject.tabs.push(thisTab);
                        localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                    }

                    // #4.
                    document.querySelector("body").addEventListener("click", (ev) => this.registerClick(component, ev, false));
                    document.querySelector("body").addEventListener("contextmenu", (ev) => this.registerClick(component, ev, true));
                    window.addEventListener("beforeunload", (ev) => this.tabClosed(component, ev));

                    component.set("v.initComplete", true);

                // request user id and Opco when session is re-initialized
                component.set("v.fetchUserInfo", !component.get("v.fetchUserInfo"));
            }
	},
    dispatchData : function (component, event) {
        component.set("v.dispatch", true);
    },
    registerClick : function (component, ev, rightClick) {
        let thisTab = component.get("v.thisTab");
        let sessionObject = this.retrieveSessionHelper(component);
        if (thisTab && thisTab.session && thisTab.sessionStats && sessionObject) {
            let sessionObject = this.retrieveSessionHelper(component);
            if (new Date().getTime() - (sessionObject.sessionStats || {}).lastClickTime > 2 * 3600 * 1000) {
                this.dispatchData(component, event);
            } else {
                let lastPage = (thisTab.session || []).find(page => page.pageSeq === thisTab.session.length);
                if (!lastPage || lastPage.page !== this.getCurrentPage(window.location.href)) {
                    // add new page object
                    let currentPage = {};
                    currentPage.page = this.getCurrentPage(window.location.href);
                    currentPage.pageSeq = thisTab.session.length + 1;
                    currentPage.activity = {};
                    currentPage.activity.interactions = [];
                    thisTab.session.push(currentPage);
        
                    this.updateTabAndSession(component, thisTab);
                }
        
        
                if (ev.target) {
                    let limit = 7;
                    let target = ev.target;
                    if (target.nodeName === 'path' || target.nodeName === 'svg') {
                        limit = 9;
                    }
                    let i = 0;
        
                    if (
                        ev.target.nodeName === 'BODY'
                        || (ev.target.nodeName === 'A' && !ev.target.innerHTML)
                        || ev.target.getAttribute('id') === 'tracking-canvas'
                        || ev.target.getAttribute('id') === 'tracking-container'
                        || ev.target.getAttribute('data-tracker') === 'ignore'
                    ) {
                        thisTab.sessionStats.lastClickTime = new Date().getTime();
                        sessionObject.sessionStats.lastClickTime = (thisTab.sessionStats || {}).lastClickTime;
                        localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                        return
        
                    } else if (ev.target.nodeName !== 'INPUT'
                        && ev.target.nodeName !== 'TEXTAREA'
                        || (ev.target.getAttribute('data-type') === 'input-checkbox' && ev.target.querySelector('input'))
                    ) {
        
                        if (!target.getAttribute('data-tracker') && target) {
                            while (i < limit && target && !target.getAttribute('data-tracker')) {
                                target = target.parentElement;
                                i++;
                                if (target) {
        
                                    if (target && target.getAttribute('data-tracker')) {
                                        break
                                    }
                                }
                            }
                        }
        
                        if (!target) {
                            thisTab.sessionStats.lastClickTime = new Date().getTime();
                            sessionObject.sessionStats.lastClickTime = (thisTab.sessionStats || {}).lastClickTime;
                            localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                            return
                        }
        
                        let targetDiv = target.getAttribute('data-tracker');
                        if (!targetDiv) {
                            thisTab.sessionStats.lastClickTime = new Date().getTime();
                            sessionObject.sessionStats.lastClickTime = (thisTab.sessionStats || {}).lastClickTime;
                            localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                            return
                        }
        
                        let cardNum = null;
                        let inc = 0;
                        let currentTarget = target;
                        while (inc < 10 && !cardNum && currentTarget) {
                            currentTarget = currentTarget.parentElement;
                            inc++;
                            if (currentTarget && currentTarget.getAttribute('data-voice') && currentTarget.getAttribute('data-voice').includes('_card--')) {
                                cardNum = currentTarget.getAttribute('data-voice');
                                break
                            }
                        }
        
                        let elem = document.querySelectorAll(`[data-tracker="${targetDiv}"]`)[0];
                        let pair = {x: null, y: null}
                        if (elem) {
                            let elWidth = elem.offsetWidth;
                            let elHeight = elem.offsetHeight;
                            var viewportOffset = elem.getBoundingClientRect();
                            let elX = viewportOffset.left;
                            let elY = viewportOffset.top;
                            pair = {x: elX + elWidth / 2, y: elY + elHeight / 2};
                        }
        
                        let extractedValue = null;
                        if (ev.target.getAttribute('data-type') === 'input-checkbox' && !ev.target.querySelector('input')) {
                            return
                        } else if (ev.target.getAttribute('data-type') === 'input-checkbox' && ev.target.querySelector('input')) {
                            extractedValue = ev.target.querySelector('input').checked ? 'checked' : 'unchecked';
                        }
        
                        let lastPageIndex = thisTab.session.findIndex(page => page.pageSeq === thisTab.session.length);
        
                        let divClick = {
                            dataTracker: targetDiv.split(' |')[0],
                            cardNum: cardNum,
                            dataType: target.getAttribute('data-type'),
                            delayTime: new Date().getTime() - (((thisTab ||{}).sessionStats || {}).lastClickTime || 0),
                            sequence: thisTab.session[lastPageIndex].activity.interactions.length + 1,
                            // coordinates: pair,
                            input: extractedValue
                        };

                        if (rightClick) {
                            divClick.clickType = "right-click";
                        }
        
                        thisTab.session[lastPageIndex].activity.interactions.push(divClick);
                        thisTab.sessionStats.lastClickTime = new Date().getTime();
                        sessionObject.sessionStats.lastClickTime = (thisTab.sessionStats || {}).lastClickTime;;
        
                        this.updateTabAndSession(component, thisTab);
        
                    }
        
                    else if (
        
                        ev.target.nodeName === 'INPUT'
                        || ev.target.nodeName === 'TEXTAREA'
                        || (ev.target.getAttribute('data-type') || '').split('-')[0] === 'input') {
        
        
                        // let delayTime = new Date().getTime() - ((thisTab.sessionStats || {}).lastClickTime || 0);
        
                        if (!target.getAttribute('data-tracker')) {
                            while (i < limit && target && !target.getAttribute('data-tracker')) {
                                target = target.parentElement;
        
                                i++;
                                if (target && target.getAttribute('data-tracker')) {
                                    break
                                }
                            }
                        }
        
                        if (!target) {
                            thisTab.sessionStats.lastClickTime = new Date().getTime();
                            sessionObject.sessionStats.lastClickTime = (thisTab.sessionStats || {}).lastClickTime;;
                            localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                            return
                        }
        
        
                        let targetDiv = target.getAttribute('data-tracker');
                        if (!targetDiv) {
                            thisTab.sessionStats.lastClickTime = new Date().getTime();
                            sessionObject.sessionStats.lastClickTime = (thisTab.sessionStats || {}).lastClickTime;;
                            localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                            return
                        }
        
                        let cardNum = null;
                        let inc = 0;
                        let currentTarget = target;
                        while (inc < 10 && !cardNum && currentTarget) {
                            currentTarget = currentTarget.parentElement;
                            inc++;
                            if (currentTarget && currentTarget.getAttribute('data-voice') && currentTarget.getAttribute('data-voice').includes('talent_card--')) {
                                cardNum = currentTarget.getAttribute('data-voice');
                                break
                            }
                        }
        
                        let elem = document.querySelectorAll(`[data-tracker="${targetDiv}"]`)[0];
                        let pair = {x: null, y: null}
                        if (elem) {
                            let elWidth = elem.offsetWidth;
                            let elHeight = elem.offsetHeight;
                            var viewportOffset = elem.getBoundingClientRect();
                            let elX = viewportOffset.left;
                            let elY = viewportOffset.top;
                            pair = {x: elX + elWidth / 2, y: elY + elHeight / 2};
                        }
        
                        let extractedValue = null;
                        if((ev.target.type === 'text' && ev.target.type !== 'checkbox') || ev.target.nodeName === 'TEXTAREA') {
                            ev.target.addEventListener('input', (event) => {
                                this.handleInput(event, component)
                            });
                            ev.target.addEventListener('blur', (event) => {
                                this.handleBlur(event, component)
                            });
                        }
        
                        if (ev.target.type == 'checkbox') {
                            extractedValue = ev.target.checked;
                        }
        
                        let lastPageIndex = thisTab.session.findIndex(page => page.pageSeq === thisTab.session.length);
        
                        let divClick = {
                            dataTracker: targetDiv.split(' |')[0],
                            cardNum: cardNum,
                            dataType: target.getAttribute('data-type'),
                            delayTime: new Date().getTime() - (((thisTab || {}).sessionStats || {}).lastClickTime || 0),
                            sequence: ((thisTab.session[lastPageIndex].activity || {}).interactions || []).length + 1,
                            // coordinates: pair,
                            input: extractedValue
                        };

                        if (rightClick) {
                            divClick.clickType = "right-click";
                        }
        
                        thisTab.session[lastPageIndex].activity.interactions.push(divClick);
                        thisTab.sessionStats.lastClickTime = new Date().getTime();
                        sessionObject.sessionStats.lastClickTime = (thisTab.sessionStats || {}).lastClickTime;;
        
                        this.updateTabAndSession(component, thisTab);
                    }
                }
            }
        } else {
            this.initTracking(component, event);
        }
    },
    retrieveSessionHelper: function (component) {
        let sessionObject = localStorage.getItem("sessionObject") || component.get("v.sessionObject");
        if (sessionObject && typeof sessionObject !== "object") {
            sessionObject = JSON.parse(sessionObject)
        }
        return sessionObject || null;
    },
    handleOpco: function (component, event) {
	    let checkInit = () => {
            if (!component.get("v.initComplete")) {
                setTimeout(() => {
                    checkInit()
                }, 100);
            } else {
                // set sessionStats for each session AND for central object
                let thisTab = component.get("v.thisTab");
                let thisTabChanged = false;
                if (!thisTab.sessionStats.opco) {
                    thisTab.sessionStats.opco = event.getParam("opco");
                    thisTabChanged = true;
                }
                if (!thisTab.sessionStats.userId) {
                    thisTab.sessionStats.userId = event.getParam("userId");
                    thisTabChanged = true;
                }
                if (!thisTab.sessionStats.userGroups) {
                    thisTab.sessionStats.userGroups = event.getParam("userGroups").join(', ');
                    thisTabChanged = true;
                }
                if (thisTabChanged) {
                    component.set("v.thisTab", thisTab);
                }

                let sessionObject = this.retrieveSessionHelper(component);
                let sessionObjectChanged = false;
                if (!sessionObject.sessionStats.opco) {
                    sessionObject.sessionStats.opco = event.getParam("opco");
                    sessionObjectChanged = true;
                }
                if (!sessionObject.sessionStats.userId) {
                    sessionObject.sessionStats.userId = event.getParam("userId");
                    sessionObjectChanged = true;
                }
                if (!sessionObject.sessionStats.userGroups) {
                    sessionObject.sessionStats.userGroups = event.getParam("userGroups").join(', ');
                    sessionObjectChanged = true;
                }
                if (sessionObjectChanged) {
                    localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
                }
            }
        }
        checkInit();
    },
    // when PubSubHelper receives callback for the last call, component must re-initialize
    dispatchHandler: function (component, event) {
        if (component.get("v.dispatch") == false) {
            localStorage.setItem("sessionObject", "");
            component.set("v.sessionObject", "");
            component.set("v.connectedTabId", "");
            component.set("v.thisTab", {});
            this.initTracking(component, event);
        }
    },
    tabClosed: function(component, event) {
	    let sessionObject = this.retrieveSessionHelper(component);
	    let openTabIndex = sessionObject.tabsOpen.findIndex(tab => tab === component.get("v.connectedTabId"));
        if (openTabIndex !== -1) {
            sessionObject.tabsOpen.splice(openTabIndex, 1);
        }
        if (sessionObject && sessionObject.tabsOpen && !sessionObject.tabsOpen.length) {
            sessionObject.sessionStats.sessionClosed = true;
        }
        localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
        component.set("v.sessionObject", sessionObject);
        sessionStorage.setItem("connectedTabId", "");
        return null
    },
    getBrowserData: function(component) {
        let windowWidth = window.innerWidth;
        let windowHeight = window.innerHeight;
        let browser = 'Unknown';
        if (!!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime)) {
            browser = 'Chrome';
        } else if (navigator.userAgent.indexOf("Firefox") != -1 ) {
            browser = 'Firefox';
        } else if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
            browser = 'Opera';
        } else if ((navigator.userAgent.indexOf("Safari") != -1)) {
            browser = 'Safari';
        } else if ((navigator.userAgent.indexOf("MSIE") != -1 )) {
            browser = 'IE';
        } else if (!(navigator.userAgent.indexOf("MSIE") != -1 ) && !!window.StyleMedia) {
            browser = 'Edge';
        }
        let browserData = {};
        browserData.width = windowWidth;
        browserData.height = windowHeight;
        browserData.type = browser;
        return browserData;
    },
    getCurrentPage: function(url) {
        let pageUrls = [
            {page: 'Home', urlSegment: 'home'},
            {page: 'Accounts', urlSegment: 'account'},
            {page: 'Contacts', urlSegment: 'contact'},
            {page: 'My Activities', urlSegment: 'my_activities'},
            {page: 'My Lists', urlSegment: 'my_lists'},
            {page: 'Opportunities', urlSegment: 'opportunity'},
            {page: 'Req Search', urlSegment: 'search_reqs'},
            {page: 'Talent Search', urlSegment: 'ats_candidatesearch'},
            {page: 'Find or Add Talent', urlSegment: 'find_or_add_candidate'},
            {page: 'Chatter', urlSegment: 'chatter'},
            {page: 'Dashboard', urlSegment: 'dashboard'},
            {page: 'Reports', urlSegment: 'report'}
        ];
            let currentPage = pageUrls.find(page => url.toLowerCase().includes(page.urlSegment));
            if (currentPage) {
                return currentPage.page;
            }
    },
    handleInput: function (event, component) {
        let thisTab = component.get("v.thisTab");
        if (thisTab.session) {
            // let sessionObject = this.retrieveSessionHelper(component);
            // let connectedTabId = component.get("v.connectedTabId");
            let currentPageIndex = thisTab.session.findIndex(page => page.pageSeq === thisTab.session.length);
            if (currentPageIndex !== -1) {
                let currentInteractionIndex = ((thisTab.session[currentPageIndex].activity || {}).interactions || []).findIndex(interaction => interaction.sequence === ((thisTab.session[currentPageIndex].activity || {}).interactions || []).length);
                if (currentInteractionIndex !== -1) {
                    ((thisTab.session[currentPageIndex].activity || {}).interactions || [])[currentInteractionIndex].input = event.target.value;
                }
            }
            this.updateTabAndSession(component, thisTab);
        }
    },
    handleBlur: function (event) {
        event.target.removeEventListener('input', this.handleInput);
        event.target.removeEventListener('input', this.handleBlur);
    },
    updateResults: function(component, event, temp) {
        // let existingFilters = component.get('v.filters');
	    let checkInitializationStatus = () => {
	        if (!component.get("v.initComplete")) {
	            setTimeout(() => {
                    checkInitializationStatus();
                }, 100);
            } else {

                if(event.getParam("sourceId")){
                    component.set("v.sourceId", event.getParam("sourceId"));
                }
                
                let thisTab = component.get("v.thisTab");
                if (thisTab.session) {
                    if (!thisTab.session.length || thisTab.session[thisTab.session.length - 1].page !== this.getCurrentPage(window.location.href)) {
                        let currentPage = {};
                        currentPage.page = this.getCurrentPage(window.location.href);
                        currentPage.pageSeq = thisTab.session.length + 1;
                        currentPage.activity = {};
                        currentPage.activity.interactions = [];
                        thisTab.session.push(currentPage);
                        component.set("v.thisTab", thisTab);
                        this.updateTabAndSession(component, thisTab);
                    }
    
                    setTimeout(() => {
                        let talentCards = [];
                        let reversedDivsClicked = (((thisTab.session[thisTab.session.length - 1] || {}).activity || {}).interactions || []).reverse();
                        let currentPage = null;
    
                        let i = 0;
                        while (i < reversedDivsClicked.length) {
                            if (reversedDivsClicked[i].dataTracker.includes('search_button') || reversedDivsClicked[i].dataTracker.includes('keywords_search')) {
                                currentPage = '1';
                                break;
                            } else if (reversedDivsClicked[i].dataTracker.includes('pagination--')) {
                                if (reversedDivsClicked[i].dataTracker.includes('pagination--goToPage-')) {
                                    currentPage = reversedDivsClicked[i].dataTracker.replace('pagination--goToPage-', '');
                                    break;
                                } else if (reversedDivsClicked[i].dataTracker.includes('pagination--goToFirst')) {
                                    currentPage = '1';
                                    break;
                                }
                            }
                            i++;
                        }
                    if (!temp) {
                        // console.log("[UPD RESULTS: CAUGHT RESPONSE]");
                      	let resOutput = {};
                        resOutput.resultsPage = currentPage || '1';
                        resOutput.transactionId = component.get('v.transactionId');
                        resOutput.subTransactionId = component.get('v.subTransactionId');
                        resOutput.requestId = component.get('v.requestId');
                        resOutput.pageSize = component.get('v.pageSize');
                        resOutput.filters = {
                            location: component.get('v.queryLocation'),
                            engine: (component.get("v.engineType") || {}).engine,
                            queryType: (component.get("v.engineType") || {}).type,
                        };
    
                            resOutput.sourceId = "";

                            if (resOutput.filters.queryType === 'match') {
                                resOutput.filters.matchType = (component.get("v.engineType") || {}).matchType;
                                resOutput.filters.eagernessFilter = component.get('v.eagernessFilter') ? 'ON' : 'OFF';
                                
                                resOutput.sourceId = component.get("v.sourceId");
                            }
                            
              
                            resOutput.timeStamp = new Date().getTime();
                            resOutput.inViewport = [];
                            resOutput.scrolledInViewport = [];
                            resOutput.resultsReturned = [];

                     
                           let notInViewport = [];
                           let trackedItems = document.querySelectorAll("[data-tracker]");


                           if (trackedItems.length) {
                               let trackedItemType;
                               if (resOutput.filters.matchType){
                                   trackedItemType = (resOutput.filters.matchType === 'C2R' || resOutput.filters.matchType === 'R') ? "req_card" : "talent_card";
                               } else {
                                   trackedItemType = (this.getCurrentPage(window.location.href) === "Req Search") ? "req_card" : "talent_card";
                               }
                             for (let i = 0; i < trackedItems.length; i++) {  
                                if (trackedItems[i].getAttribute("data-tracker").includes(trackedItemType)) {
                                  talentCards.push(trackedItems[i]);
                               }
                             }
                           }
                          
                           /** Log interest score for all results **/
                           for (let k = 0; k < talentCards.length; k++) {
                             let id = talentCards[k].getAttribute("data-tracker").split("--")[1];
                             if (!id) { 
                               id = "RECORD HAS NO ID";
                             }
                             let pair = {};
                             pair.id = id;
                             pair["interestScore"] = talentCards[k].getAttribute("data-interest");
                             resOutput.resultsReturned.push(pair);
                           }

                           /** Check what results are immediately in viewport **/
                           for (let k = 0; k < talentCards.length; k++) {
                             let bounding = talentCards[k].getBoundingClientRect();
                             if ( bounding.top >= 0 && bounding.left >= 0 && bounding.right <=
                                 (window.innerWidth || document.documentElement.clientWidth) &&
                               bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)
                             ) {
                               resOutput.inViewport.push(
                                 talentCards[k].getAttribute("data-tracker")
                               );
                             } else {
                               notInViewport.push(talentCards[k]);
                             }
                           }
                           
                           // let sessionObject = this.retrieveSessionHelper(component);
                        
                        if (!thisTab.session[thisTab.session.length - 1].activity.resultsOutput) {
                             thisTab.session[thisTab.session.length - 1].activity.resultsOutput = [];
                           }

                           let duplicate = thisTab.session[thisTab.session.length - 1].activity.resultsOutput.find(
                             (output) =>
                               output.transactionId ===
                                 resOutput.transactionId &&
                               output.subTransactionId ===
                                 resOutput.subTransactionId &&
                               output.requestId === resOutput.requestId
                           );

                           if (duplicate) {
                             return;
                           }

                           let tabsResults = thisTab.session[thisTab.session.length - 1].activity.resultsOutput;
                           let tempObj = tabsResults[tabsResults.length - 1];
                           if ( (tempObj || {}).requestSent && !tempObj.resultsPage ) {
                             resOutput.requestProcessingTime = ((new Date().getTime() - tempObj.requestSent) / 1000 ).toFixed(2) + "s";
                             thisTab.session[thisTab.session.length - 1].activity.resultsOutput[tabsResults.length - 1] = resOutput;
                             //                      thisTab.session[thisTab.session.length - 1].activity.resultsOutput.push(resOutput);
                             this.updateTabAndSession(component, thisTab);

                             component.set("v.requestId", null);

                             let scrolledItem = window;
                             //  If scrolledItem is window, remove height of elements that arent results
                             let displacedHeight =  254;

                            if (document.getElementsByClassName("results-output").length) {
                               scrolledItem = document.getElementsByClassName("results-output")[0].firstElementChild;
                               displacedHeight = 0;
                             } 

                             let scrollListener = () => {
                               for (let z = 0; z < notInViewport.length; z++) {
                                 let bound = notInViewport[z].getBoundingClientRect();
                                 if (
                                   bound.top >= 0 &&
                                   bound.left >= 0 &&
                                   bound.right <=
                                     (window.innerWidth ||
                                       document.documentElement.clientWidth) &&
                                   bound.bottom <=
                                     (window.innerHeight - displacedHeight ||
                                         document.documentElement.clientHeight - displacedHeight)
                                 ) {
                                   if (
                                     thisTab.session && 
                                     thisTab.session.length && 
                                     (thisTab.session[thisTab.session.length - 1 ].activity || {}).resultsOutput &&
                                     ((thisTab.session[thisTab.session.length - 1].activity || {}).resultsOutput || []).length && 
                                     thisTab.session[thisTab.session.length - 1].activity.resultsOutput[thisTab.session[thisTab.session.length - 1].activity.resultsOutput.length - 1].hasOwnProperty('scrolledInViewport')
                                   ) { 
                                     thisTab.session[thisTab.session.length - 1].activity.resultsOutput[thisTab.session[thisTab.session.length - 1].activity.resultsOutput.length - 1]
                                     .scrolledInViewport.push(
                                       notInViewport[z].getAttribute("data-tracker")
                                     );
                                     this.updateTabAndSession(component,thisTab);

                                     notInViewport.splice(z, 1);

                                     if (!notInViewport.length) {
                                       scrolledItem.removeEventListener("scroll", scrollListener);
                                     }
                                   }
                                 }
                               }
                             };
                             scrolledItem.addEventListener("scroll", scrollListener);
                           }
                            
                           /** Check what results entered viewport on user scrolling **/
                         } else {
							// console.log("[UPD RESULTS: CAUGHT REQ]");
							//S-189728
							let engineAtt;
							if(component.get("v.engineType") != undefined ) {
								engineAtt = component.get("v.engineType").engine;
							} else {
								engineAtt = 'ES';
							}
							let resOutput = {
								requestSent: new Date().getTime(),
								queryType: event.getParam("requestSent"),
								//engine: "ES"
								engine:engineAtt
							};
                            if (!thisTab.session[thisTab.session.length - 1].activity) {
								thisTab.session[thisTab.session.length - 1].activity = {};
                        }
                        if (!thisTab.session[thisTab.session.length - 1].activity.resultsOutput) {
                            thisTab.session[thisTab.session.length - 1].activity.resultsOutput = [];
                        }                        
	                        thisTab.session[thisTab.session.length - 1].activity.resultsOutput.push(resOutput);
    	                    this.updateTabAndSession(component, thisTab);                            
					}
                        
                    }, 1000);
                }
            }
        }

        checkInitializationStatus();
    },
    updateTabAndSession: function (component, thisTab) {
	    let sessionObject = this.retrieveSessionHelper(component);

        let tabIndexBeforeUpdate = sessionObject.tabs.findIndex(tab => tab.connectedTabId === thisTab.connectedTabId);
        if (tabIndexBeforeUpdate !== -1) {
            sessionObject.tabs[tabIndexBeforeUpdate] = thisTab;
        }
        component.set("v.thisTab", thisTab);
        localStorage.setItem("sessionObject", JSON.stringify(sessionObject));
    },
    handleTrackingEvent: function(component, event) {
        if (event.getParam("requestSent")) {
			this.updateResults(component, event, true);
        }
        if (event.getParam("filter")) {
            let filter = event.getParam("filter");
            let existingFilters = component.get('v.filters');
            if (typeof existingFilters == "string") {
                existingFilters = JSON.parse(existingFilters);
            }
            Object.keys(filter).forEach(key => {
                existingFilters[key] = filter[key];
            });
            component.set('v.filters', existingFilters);
        }

        if (event.getParam("engineType")) {
            component.set("v.engineType", event.getParam("engineType"));
        }
	    let checkInitializationStatus = () => {
	        if (!component.get("v.initComplete")) {
	            setTimeout(() => {
                    checkInitializationStatus();
                }, 100)
            } else {
                let enabled = component.get('v.enabled');
                let thisTab = component.get("v.thisTab");
                if (enabled && thisTab && thisTab.session) {
                    var dataTracker = event.getParam("clickTarget");
                    var dataType = event.getParam("clickType");
                    let input = event.getParam("inputValue");
                    // let sessionObject = this.retrieveSessionHelper(component);

                    if (dataTracker) {
                        let divClick = {
                            dataTracker: dataTracker.split(' |')[0],
                            dataType: dataType,
                            delayTime: new Date().getTime() - (((thisTab || {}).sessionStats || {}).lastClickTime || 0),
                            sequence: (((thisTab.session[thisTab.session.length - 1] || {}).activity || {}).interactions || []).length + 1,
                            coordinates: {},
                            input: input || null
                        };

                        ((thisTab.session[thisTab.session.length - 1].activity || {}).interactions || []).push(divClick);
                        thisTab.sessionStats.lastClickTime = new Date().getTime();
                        this.updateTabAndSession(component, thisTab);

                    }
                }
            }
        }
        checkInitializationStatus();
    }
})