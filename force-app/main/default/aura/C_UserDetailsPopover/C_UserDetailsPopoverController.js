({
    doInit : function(component, event, helper) {
        component.set("v.popoverLoading", true);
        helper.getUserDetails(component);
    }
})