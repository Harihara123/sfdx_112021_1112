({
    getUserDetails: function (cmp) {
        const bdata = cmp.find("baseDataHelper");

        let params = {
            userId: cmp.get("v.userId")
        };

        bdata.callServer(cmp, 'ATS', 'TalentActivityFunctions', 'getUserDetails', function (response) {
            if (response) {
                cmp.set('v.userDetails', response.user);
                cmp.set("v.popoverLoading", false);
            } else {
                cmp.set('v.userDetails', {});
            }
        }, params, false);
    }
})