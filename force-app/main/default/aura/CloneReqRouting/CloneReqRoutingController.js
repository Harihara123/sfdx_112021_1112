({
    doInit : function(component, event, helper) {
        var action = component.get("c.isGroupMember");
        action.setCallback(this,function(response){
            var resState = response.getState();
            if(resState == 'SUCCESS')
            {
                var responseVal = response.getReturnValue();
                component.set("v.isGroupMember",responseVal);
            }
             //alert('#isGroupMember#'+component.get("v.isGroupMember"));
            
            
            var navCmp = component.find("navService");
            var accid=component.get("v.recordId");
            var tgsId = '';
            var soppid= '';
            var OppRecTypeID = '';
            var rectype = component.get("v.opp.RecordTypeId"); 
            if(rectype === '012U0000000DWp4IAG'){
                //console.log('tgs oppty');
                tgsId = component.get("v.recordId");
                accid = component.get("v.opp.AccountId"); 
                soppid = component.get("v.oppId");
                OppRecTypeID = rectype ;
            }
            
            var companyName = component.get("v.opp.OpCo__c");
            var isInGroup = component.get("v.isGroupMember")
            var redirectComponent;
            //alert('#companyName Aerotek, Inc#'+companyName);
           // if(companyName === 'Aerotek, Inc' && isInGroup===true){	//S-223083            
            if(companyName === 'Aerotek, Inc'){
                redirectComponent = 'c__CreateEnterpriseAerotekReq_New';
            // }else if(companyName === 'TEKsystems, Inc.' && isInGroup===true){	//S-223083
            }else if(companyName === 'TEKsystems, Inc.'){
                redirectComponent = 'c__CreateEnterpriseTEKReq';
            }else if(companyName === 'AG_EMEA'){
                redirectComponent = 'c__CreateEnterpriseEmeaReq';
            }else{
                redirectComponent = 'c__CreateEnterpriseReq';
            }            
            
            if(redirectComponent) {
                var pageReference = {
                    "type": "standard__component",
                    "attributes": {
                        "componentName": redirectComponent
                    }, 
                    "state": {
                        "c__recId" : accid,
                        "c__tgsOppId" : tgsId,
                        "c__soppId" : soppid,
                        "c__oppRecId" :OppRecTypeID
                    }
                    
                    
                };
                
                
                component.set("v.pageReference", pageReference);
                var navCmp = component.find("navCmp");
                var pageReference = component.get("v.pageReference");
                // event.preventDefault();
                //console.log('Hareesh');
                navCmp.navigate(pageReference);
            } else {
                $A.get("e.force:closeQuickAction").fire();
            }
            
            
        });
        $A.enqueueAction(action);
    },
    
	navigateToCreateComponent : function(component, event, helper) {
                      
   } ,
    reInit: function(component, event, helper) {      
        $A.get('e.force:refreshView').fire();
       
   }
        
        
       

})