({
    showDropdown: function (component, event, helper) {
        let options = document.getElementById('options-' + component.get('v.randomID'));
        if (!options.classList.contains('hidden-menu')) {
            helper.hideDropdown(component, event);
        } else {
            helper.showDropdown(component, event)
        }
    },
    
    init: function (component, event, helper) {
        let randomID = Math.floor((1 + Math.random()) * 0x10000000000).toString(16).substring(1);
        component.set('v.randomID', randomID);
        
        let html = document.querySelector('html');
        html.onclick = () => {

            // clicking outside of a dropdown menu closes it and rotates caret back to normal position
            let options = document.getElementsByClassName('custom-menu-options');
            let carets = document.getElementsByClassName('stretch');
            if (options.length) {
                for (let i = 0; i < options.length; i++) {
                    if (!options[i].classList.contains('hidden-menu')) {

                        options[i].classList.add('hidden-menu');

                    }
                }
            }
        };
        
        
    },

    valueChange: function(component) {
        let itemSelected = document.getElementById('item-selected-' + component.get('v.randomID'));
		if (itemSelected) {
			if (component.get('v.value')) {
				// let newVal = component.get('v.value').toUpperCase();
				itemSelected.innerHTML = component.get('v.value');
			} else {
				itemSelected.innerHTML = 'Select';
			}
		}
    },

    itemsChange: function(component, event, helper) {
        let options = document.getElementById('options-' + component.get('v.randomID'));
        while (options.firstChild) {
            options.removeChild(options.firstChild);
        }
        let optionsObj = [...component.get('v.options')];
        optionsObj.unshift('Select');
        optionsObj.map(el => {
            let option = document.createElement('span');
            option.innerHTML = el;
            option.classList.add('custom-option');
            option.onclick = (event) => {
                event.stopPropagation();
                let itemSelected = document.getElementById('item-selected-' + component.get('v.randomID'));
                if (el !== 'Select') {
                    component.set('v.value', el);
                    itemSelected.innerHTML = component.get('v.value');
                } else {
                    component.set('v.value', null);
                    itemSelected.innerHTML = 'Select';
                }
                helper.hideDropdown(component, event, helper);
            };
            options.classList.add('hidden-menu');
            options.appendChild(option);

        })
    },

    hideDropdown: function (component, event, helper) {
        helper.hideDropdown(component, event);
    },

    resetValue: function (component, event, helper) {
        let itemSelected = document.getElementById('item-selected-' + component.get('v.randomID'));
        let params = event.getParams();
        if (params.oldValue !== params.value) {
            itemSelected.innerHTML = 'Select';
        }
    }
})