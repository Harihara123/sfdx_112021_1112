({
    showDropdown: function (component, event) {
        event.stopPropagation();


        let allOptions = document.getElementsByClassName('custom-menu-options');
        let carets = document.getElementsByClassName('stretch');
        for (let i = 0; i < allOptions.length; i++) {
            if (!allOptions[i].classList.contains('hidden-menu')) {
                allOptions[i].classList.add('hidden-menu');
                // carets[i].style.transform = 'rotate(360deg)';
            }
        }

        let options = document.getElementById('options-' + component.get('v.randomID'));
        options.classList.toggle('hidden-menu');
        let valueSelected = document.getElementById('item-selected-' + component.get('v.randomID'));
        let caret = document.getElementById('caret-' + component.get('v.randomID'));
        // caret.style.transform = 'rotate(180deg)';
        // options.style.transform = 'translate(' + (valueSelected.offsetLeft - options.offsetLeft) + 'px, 5px)';

    },
    hideDropdown: function (component, event) {
        event.stopPropagation();
        let options = document.getElementById('options-' + component.get('v.randomID'));
        options.classList.toggle('hidden-menu');
        let caret = document.getElementById('caret-' + component.get('v.randomID'));
        // caret.style.transform = 'rotate(360deg)';
    }
})