({
    afterRender: function(component, helper) {
        document.getElementById('value-selected-' + component.get('v.randomID')).style.transform = 'translateY(3px)';
               
        if (component.get('v.value')) {
            let itemSelected = document.getElementById('item-selected-' + component.get('v.randomID'));
            itemSelected.style.paddingRight = '5px';
            itemSelected.innerHTML = component.get('v.value');
        } else {
            let itemSelected = document.getElementById('item-selected-' + component.get('v.randomID'));
            itemSelected.innerHTML = 'Select';
            itemSelected.style.paddingRight = '5px';
        }

        let options = document.getElementById('options-' + component.get('v.randomID'));
        options.style.transform = 'translateY(5px)';
                while (options.firstChild) {
                    options.removeChild(options.firstChild);
                }
                if (component.get('v.options')) {
                    let optionsObj = [...component.get('v.options')];
                    optionsObj.unshift('Select');
                    optionsObj.map(el => {
                        let option = document.createElement('span');
                        option.innerHTML = el;
                        option.classList.add('custom-option');
                        option.onclick = (event) => {
                            event.stopPropagation();
                            let itemSelected = document.getElementById('item-selected-' + component.get('v.randomID'));
                            if (el !== 'Select') {
                                component.set('v.value', el);
                                itemSelected.innerHTML = component.get('v.value');
                            } else {
                                component.set('v.value', null);
                                itemSelected.innerHTML = 'Select';
                            }
                            helper.hideDropdown(component, event, helper);
                        };
                        options.classList.add('hidden-menu');
                        options.appendChild(option);

                    })
                }
        this.superAfterRender();
    }
})