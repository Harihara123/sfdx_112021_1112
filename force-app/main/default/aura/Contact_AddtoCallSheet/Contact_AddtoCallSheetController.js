({
	doInit : function(component, event, helper) {
		 helper.initialize(component);
		 var recs = component.get("v.selectedRecords");
		 console.log('Selected contacts '+recs);
	},
    submitSearchAction : function (component, event, helper) {
        helper.addToCallSheet(component,event, helper);
    },
    closeModalBox : function (component, event, helper) {
		var returnLocation = component.get("v.returnVal");
        if(returnLocation == true)
			var urlre='https://'+window.location.hostname+'/lightning/n/Contact_Topics';
        else
        	var urlre='https://'+window.location.hostname+'/lightning/o/Contact/home';
        console.log('URL is---->'+urlre);
        window.location=urlre;
	}
})