({
	doInit : function(component, event, helper) {
	   helper.initializeEnterpriseReqModal(component);
	},
    onOpcoChange : function(component, event, helper) {
       helper.loadBusinessUnitValues(component,event,'None');
	},
    
    toggleRedZone : function(component, event, helper) {
        
        var selectValue = component.find("stageId").get("v.value");
     
        if(selectValue != 'Draft'){
            component.set("v.disable_red_zone", false);
            component.find("redzoneCheck").set("v.value", false);
        }else{
            component.set("v.disable_red_zone", true);
            component.find("redzoneCheck").set("v.value", false);
        }
      	    
    },
    
    
     onBusinessunitChange : function(component, event, helper) {
       helper.EnterpriseReqSegmentModal(component,event);
	},
     onSegmentChange : function(component, event, helper) {
       helper.EnterpriseReqJobcodeModal(component,event);
	},
    onJobCodeChange : function(component, event, helper) {
       helper.EnterpriseReqCategoryModal(component,event);
	},
    onCategoryChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillModal(component,event);
	},
    
    onMainSkillChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillid(component,event);
	},
    addAltRow : function(component, event, helper) {
        helper.addAltRecord(component, event);
    },      
    removeAltRow: function(component, event, helper) {
        var supportRequestList = component.get("v.supportRequestList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        supportRequestList.splice(index, 1);
        component.set("v.supportRequestList", supportRequestList);
    },    
    onAltDeliveryChange : function(component, event, helper) {
      var selectedOp = component.find("altDeliveryList");
        console.log("Alternate Delivery ",selectedOp.get("v.value"));
       if(selectedOp.get("v.value") === "Yes"){
          component.set("v.altDeliveryVisibility","true");  
        }
    },   
    
    onSkillsValidated : function(component, event, helper) {
       helper.topSkillsValidation(component,event);
	},
    onProductPicklistChange : function(component, event, helper) {
    	component.set("v.ProductVarNamePicklist", "testing");
    },
    onProductChange : function(component, event, helper) {
    var selected = component.get("v.req.Req_Product__c");
    var isclone  = component.get("v.isCloneReq");
        //do something else
       
       if(selected === "Contract"){
            component.set("v.isContract", "true");
            component.set("v.isPermanent", "false");
            component.set("v.isCTH", "false");
            
            if(isclone == "true"){
               var salMin = component.get("v.req.Req_Salary_Min__c");
               var salMax = component.get("v.req.Req_Salary_Max__c");
               var bonus = component.get("v.req.Req_Bonus__c");
               var feepercent = component.get("v.req.Req_Fee_Percent__c");
               var includebonus = component.get("v.req.Req_Include_Bonus__c");
               var fltfee = component.get("v.req.Req_Flat_Fee__c");
               var otherComp = component.get("v.req.Req_Other_Compensations__c");
               var payTerms = component.get("v.req.Req_Payment_terms__c");
               var addFirstYear = component.get("v.req.Req_Additional_1st_Year_Compensation__c");
               var retainerFee = component.get("v.req.Retainer_Fee__c");
               var firstYearRev = component.get("v.req.Req_Include_in_Revenue_1st_Year_Comp__c");
               var curr = component.get("v.req.Currency__c");
                
                if(salMin != null && salMin != ''){
                    component.set("v.req.Req_Salary_Min__c", null);
                }
                if(curr != null && curr != ''){
                   // component.set("v.req.Currency__c",'--None--');
                }
                if(salMax != null && salMax != ''){
                    component.set("v.req.Req_Salary_Max__c", null);
                }
                if(bonus != null && bonus != ''){
                    component.set("v.req.Req_Bonus__c", null);
                }
                if(feepercent != null && feepercent != ''){
                    component.set("v.req.Req_Fee_Percent__c", null);
                }
                if(includebonus != null && includebonus !=''){
                    component.set("v.req.Req_Include_Bonus__c", false);
                }
                if(fltfee != null && fltfee !=''){
                    component.set("v.req.Req_Flat_Fee__c", null);
                }
                if(otherComp != null && otherComp != ''){
                    component.set("v.req.Req_Other_Compensations__c", null);
                }
                if(payTerms != null && payTerms != ''){
                    component.set("v.req.Req_Payment_terms__c", null);
                }
                if(addFirstYear != null && addFirstYear != ''){
                    component.set("v.req.Req_Additional_1st_Year_Compensation__c", null);
                }
                if(retainerFee != null && retainerFee != ''){
                    component.set("v.req.Retainer_Fee__c", null);
                }
                if(firstYearRev != null && firstYearRev != ''){
                    component.set("v.req.Req_Include_in_Revenue_1st_Year_Comp__c", false);
                }
                console.log('test currency val '+component.get("v.req.Currency__c"));
            }
        }else if(selected === "Permanent"){
            component.set("v.isContract", "false");
            component.set("v.isPermanent", "true");
            component.set("v.isCTH", "false");
             if(isclone == "true"){
               var durn = component.get("v.req.Req_Duration__c");
               var durUnit = component.get("v.req.Req_Duration_Unit__c");
               var brMin = component.get("v.req.Req_Bill_Rate_Min__c");
               var brMax = component.get("v.req.Req_Bill_Rate_Max__c");
               var prMin = component.get("v.req.Req_Pay_Rate_Min__c");
               var prMax = component.get("v.req.Req_Pay_Rate_Max__c");
               var sBurd = component.get("v.req.Req_Standard_Burden__c");
               var rFee = component.get("v.req.Retainer_Fee__c");
                var curry = component.get("v.req.Currency__c");
                 
                 if(durn != null && durn !=''){
                     component.set("v.req.Req_Duration__c",null);
                 }
                 if(durUnit != null && durUnit != ''){
                     component.set("v.req.Req_Duration_Unit__c",null);
                 }
                 if(brMin != null && brMin !=''){
                     component.set("v.req.Req_Bill_Rate_Min__c",null);
                 }
                 if(brMax != null && brMax != ''){
                     component.set("v.req.Req_Bill_Rate_Max__c",null);
                 }
                 if(prMin != null && prMin != ''){
                     component.set("v.req.Req_Pay_Rate_Min__c",null);
                 }
                 if(prMax != null && prMax != ''){
                     component.set("v.req.Req_Pay_Rate_Max__c",null);
                 }
                 if(sBurd != null && sBurd != ''){
                     component.set("v.req.Req_Standard_Burden__c",null);
                 }
                 if(rFee != null && rFee != ''){
                     component.set("v.req.Retainer_Fee__c", null);
                 }
                if(curry != null && curry != ''){
                   // component.set("v.req.Currency__c",'--None--');
                }
             }
        }else if(selected === "Contract to Hire"){
            component.set("v.isContract", "false");
            component.set("v.isPermanent", "false");
            component.set("v.isCTH", "true");
            if(isclone == "true"){
               var salMin = component.get("v.req.Req_Salary_Min__c");
               var salMax = component.get("v.req.Req_Salary_Max__c");
               var bonus = component.get("v.req.Req_Bonus__c");
               var feepercent = component.get("v.req.Req_Fee_Percent__c");
               var includebonus = component.get("v.req.Req_Include_Bonus__c");
               var fltfee = component.get("v.req.Req_Flat_Fee__c");
               var otherComp = component.get("v.req.Req_Other_Compensations__c");
               var payTerms = component.get("v.req.Req_Payment_terms__c");
               var addFirstYear = component.get("v.req.Req_Additional_1st_Year_Compensation__c");
               var retainerFee = component.get("v.req.Retainer_Fee__c");
               var firstYearRev = component.get("v.req.Req_Include_in_Revenue_1st_Year_Comp__c");
                var curcy = component.get("v.req.Currency__c");
                
                if(salMin != null && salMin != ''){
                    component.set("v.req.Req_Salary_Min__c", null);
                }
                if(salMax != null && salMax != ''){
                    component.set("v.req.Req_Salary_Max__c", null);
                }
                if(curcy != null && curcy != ''){
                   // component.set("v.req.Currency__c",'--None--');
                }
                if(bonus != null && bonus != ''){
                    component.set("v.req.Req_Bonus__c", null);
                }
                if(feepercent != null && feepercent != ''){
                    component.set("v.req.Req_Fee_Percent__c", null);
                }
                if(includebonus != null && includebonus !=''){
                    component.set("v.req.Req_Include_Bonus__c", false);
                }
                if(fltfee != null && fltfee !=''){
                    component.set("v.req.Req_Flat_Fee__c", null);
                }
                if(otherComp != null && otherComp != ''){
                    component.set("v.req.Req_Other_Compensations__c", null);
                }
                if(payTerms != null && payTerms != ''){
                    component.set("v.req.Req_Payment_terms__c", null);
                }
                if(addFirstYear != null && addFirstYear != ''){
                    component.set("v.req.Req_Additional_1st_Year_Compensation__c", null);
                }
                if(retainerFee != null && retainerFee != ''){
                    component.set("v.req.Retainer_Fee__c", null);
                }
                if(firstYearRev != null && firstYearRev != ''){
                    component.set("v.req.Req_Include_in_Revenue_1st_Year_Comp__c", false);
                }
            }
        }else {
            component.set("v.isContract", "false");
            component.set("v.isPermanent", "false");
            component.set("v.isCTH", "false");
            if(isclone == "true"){

               var durn = component.get("v.req.Req_Duration__c");
               var durUnit = component.get("v.req.Req_Duration_Unit__c");
               var brMin = component.get("v.req.Req_Bill_Rate_Min__c");
               var brMax = component.get("v.req.Req_Bill_Rate_Max__c");
               var prMin = component.get("v.req.Req_Pay_Rate_Min__c");
               var prMax = component.get("v.req.Req_Pay_Rate_Max__c");
               var sBurd = component.get("v.req.Req_Standard_Burden__c");
               var rFee = component.get("v.req.Retainer_Fee__c");
               var salMin = component.get("v.req.Req_Salary_Min__c");
               var salMax = component.get("v.req.Req_Salary_Max__c");
               var bonus = component.get("v.req.Req_Bonus__c");
               var feepercent = component.get("v.req.Req_Fee_Percent__c");
               var includebonus = component.get("v.req.Req_Include_Bonus__c");
               var fltfee = component.get("v.req.Req_Flat_Fee__c");
               var otherComp = component.get("v.req.Req_Other_Compensations__c");
               var payTerms = component.get("v.req.Req_Payment_terms__c");
               var addFirstYear = component.get("v.req.Req_Additional_1st_Year_Compensation__c");
               var retainerFee = component.get("v.req.Retainer_Fee__c");
               var firstYearRev = component.get("v.req.Req_Include_in_Revenue_1st_Year_Comp__c");
               var curncy = component.get("v.req.Currency__c");
                
                if(salMin != null && salMin != ''){
                    component.set("v.req.Req_Salary_Min__c", null);
                }
                if(salMax != null && salMax != ''){
                    component.set("v.req.Req_Salary_Max__c", null);
                }
                if(curncy != null && curncy != ''){
                  //  component.set("v.req.Currency__c",'--None--');
                }
                if(bonus != null && bonus != ''){
                    component.set("v.req.Req_Bonus__c", null);
                }
                if(feepercent != null && feepercent != ''){
                    component.set("v.req.Req_Fee_Percent__c", null);
                }
                if(includebonus != null && includebonus !=''){
                    component.set("v.req.Req_Include_Bonus__c", false);
                }
                if(fltfee != null && fltfee !=''){
                    component.set("v.req.Req_Flat_Fee__c", null);
                }
                if(otherComp != null && otherComp != ''){
                    component.set("v.req.Req_Other_Compensations__c", null);
                }
                if(payTerms != null && payTerms != ''){
                    component.set("v.req.Req_Payment_terms__c", null);
                }
                if(addFirstYear != null && addFirstYear != ''){
                    component.set("v.req.Req_Additional_1st_Year_Compensation__c", null);
                }
                if(retainerFee != null && retainerFee != ''){
                    component.set("v.req.Retainer_Fee__c", null);
                }
                if(firstYearRev != null && firstYearRev != ''){
                    component.set("v.req.Req_Include_in_Revenue_1st_Year_Comp__c", false);
                }
				
				 if(durn != null && durn !=''){
                     component.set("v.req.Req_Duration__c",null);
                 }
                 if(durUnit != null && durUnit != ''){
                     component.set("v.req.Req_Duration_Unit__c",null);
                 }
                 if(brMin != null && brMin !=''){
                     component.set("v.req.Req_Bill_Rate_Min__c",null);
                 }
                 if(brMax != null && brMax != ''){
                     component.set("v.req.Req_Bill_Rate_Max__c",null);
                 }
                 if(prMin != null && prMin != ''){
                     component.set("v.req.Req_Pay_Rate_Min__c",null);
                 }
                 if(prMax != null && prMax != ''){
                     component.set("v.req.Req_Pay_Rate_Max__c",null);
                 }
                 if(sBurd != null && sBurd != ''){
                     component.set("v.req.Req_Standard_Burden__c",null);
                 }
            }
        }
        
    },
    onOpCoChange :function (component,event,helper){
      var selected = component.get("v.req.OpCo__c");  
      // console.log(selected);
       
        if(selected === "Aerotek, Inc"){
            
        }else if(selected === "TEKsystems, Inc."){
            
        }
        
    },
    onTopSkillsReset : function(component, event, helper) {
       // console.log('reset flag skills');
        component.set("v.istopSkillsMessage", "false");
	},
    //Save Enterprise Req
    saveEnterpriseReqOpp : function(component, event, helper){
        component.set("v.isDisabled", true);
        //component.set("v.isSaveShown", false);
       
        component.set("v.errorMessageVal","");
        helper.saveEnterpriseReq(component, event, helper);
    },
    cancelAction : function(component, event, helper){
        component.set("v.errorMessageVal","");
		if(component.get("v.isProactiveSubmittal")) {
			helper.redirectForProactiveSubmittal(component, event, helper);
		}
		else {
			helper.redirectToStartURL(component, event, helper);
		}
    },
     reInit : function(component, event, helper) {
        console.log('reinit fired 2');
        $A.get('e.force:refreshView').fire();
    },
    handleChangeJobTitle : function(component, event, helper) {
        console.log('Handle Change Job Title');
        component.set('v.suggestedjobTitle',component.get('v.req.Req_Job_Title__c'));
        
    },
    onTGSChange : function(component, event, helper){
        var OpCoValue = component.get("v.req.OpCo__c");
        var TGSReqValue = component.find("isTGSReqId").get("v.value");
        if(OpCoValue == 'TEKsystems, Inc.' && TGSReqValue == 'Yes'){
            component.set("v.isTGSTrue", "true");
        }else if(OpCoValue == 'TEKsystems, Inc.' && (TGSReqValue == 'No' || TGSReqValue == '--None--')){
			component.set("v.isTGSTrue", "true");
			component.find("isBackfillId").set("v.value", 'false');
            component.find("isNationalId").set("v.value", 'false');
			component.find("isTGSPEId").set("v.value", '--None--');
            component.set("v.req.Practice_Engagement__c","--None--");
            component.find("isTGSLocationId").set("v.value", '--None--');
            component.set("v.req.GlobalServices_Location__c","--None--");
            component.find("isEmploymentAlignmentId").set("v.value", '--None--');
            component.set("v.req.Employment_Alignment__c","--None--");
            component.find("isFinaDecMakId").set("v.value", '--None--');
            component.set("v.req.Final_Decision_Maker__c","--None--");
            component.find("isInterNationalId").set("v.value", '--None--');
            component.set("v.req.Is_International_Travel_Required__c","--None--");
            component.find("isInternalHireId").set("v.value", '--None--');
            component.set("v.req.Internal_Hire__c","--None--");
			component.set("v.isTGSPEName","");
            component.set("v.isTGSLocationName","");
            component.set("v.isTGSEmpAlignName","");
            component.set("v.isFinaDecMakName","");
            component.set("v.isIntNationalName","");
            component.set("v.isInternalName","");
            component.set("v.isTGSTrue", "false");
        }        
    },
    
    onInsideIR35Change : function(component, event, helper) {
      var selectedOp = component.find("insideIR35");
      component.set("v.req.inside_IR35__c", selectedOp.get("v.value"));  
    },
    
    
})