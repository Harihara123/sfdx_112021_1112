({
    initializeEnterpriseReqModal : function(component,event,helper){
        /*var skillAction = component.get("c.getAccessDetailsForSkillSet");
        skillAction.setCallback(this, function(response) {
            if (response.getState() == 'SUCCESS') 
            {
                var skillAccess = response.getReturnValue();
                //alert('skillAccess::'+skillAccess);
                component.set("v.skillFlag",skillAccess);
            }
        });
        $A.enqueueAction(skillAction);*/
        var action = component.get("c.getEnterpriseReqPicklists");
        var UrlQuery = window.location.href;
        
        var myPageRef = component.get("v.pageReference");
        var tgsId ='';
		var oppId;
        
         if(myPageRef){
             oppId  = myPageRef.state.c__recId;
			 tgsId = myPageRef.state.c__tgsOppId;
             // Code freeze fix
             var paSubStatus = myPageRef.state.c__proactiveSubmittalStatus;
             if(paSubStatus!=undefined){
                 component.set("v.proactiveSubmittalStatus",paSubStatus);
             } // Code freeze fix
             if(oppId !== null && oppId !== undefined){
				component.set("v.recordId", oppId);
             }
			 if(myPageRef.state.c__source !== null && myPageRef.state.c__source !== undefined) {
				component.set("v.source", myPageRef.state.c__source);
			 }
        }
		component.set("v.workAddressFlag",true);
        var recrdId = component.get("v.recordId");
        console.log('OPP NEW ID'+recrdId);
		console.log('TGS ID'+tgsId);
        action.setParams({"recId":recrdId,"tgsOppId":tgsId});
         // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();
                component.set("v.account",model.act);
                component.set("v.contact",model.cnt);
                component.set("v.organization",model.office);
                console.log('loaded obj '+model.sobjType);
				component.set("v.sobjType",model.sobjType);
                //alert('model.isSkillVisible::'+model.isSkillVisible);
                component.set("v.skillFlag",model.isSkillVisible);
				
                     
                
               //Opp is Cloned
                if(model.opp != null){
                    console.log('opp is clone');
                    component.set("v.req",model.opp); 
                    component.set("v.isCloneReq","true");
                    var regex= new RegExp('/^[^a-zA-Z]*$/');
                    var epSkills = model.opp.EnterpriseReqSkills__c;
                    if(regex.test(epSkills)){
                        component.set("v.skills",epSkills);
                    }
                    component.set("v.req.Description",model.opp.req_job_description__c);
                    component.set("v.reqVMS",(model.opp.Req_VMS__c && model.opp.StageName=='Staging'));
                    model.opp.Originating_Opportunity__c=null;
                    var eclt = model.endClient;
                    if(eclt == null || typeof eclt=="undefined" || eclt==''){
                        console.log('which opco '+model.opp.OpCo__c);
                    }else{
                        console.log('end client '+eclt);
                        component.set("v.req.Req_End_Client__c",eclt.Id);
                        component.set("v.endClientName",eclt.Name);
                    }
                    
                    //Display Opco Specific Template
                     if(model.opp.OpCo__c === "AG_EMEA"){
                           component.set("v.isOpcoEmea", "true");
                           component.set("v.isOpcoRegular", "false");
                           component.set("v.isOpcoAerotek","false");
                           component.set("v.isOpcoTek","false");
                    }else if(model.opp.OpCo__c === "Allegis Group, Inc." || model.opp.OpCo__c === "Allegis Global Solutions, Inc." || 
                             model.opp.OpCo__c === "Allegis Partners, LLC" || model.opp.OpCo__c === "Major, Lindsey & Africa, LLC"){
                           component.set("v.isOpcoRegular", "true");
                           component.set("v.isOpcoEmea", "false");
                           component.set("v.isOpcoAerotek","false");
                           component.set("v.isOpcoTek","false");
                    }else if(model.opp.OpCo__c=='Aerotek, Inc'){
                        component.set("v.isOpcoAerotek",true);
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoTek","false");
						var reqOPTArry = [];
                        var optMap = model.ReqOptMappings;
                        reqOPTArry.push({value:'--None--', key:'--None--'});  
                        for (var key in optMap) {
                            reqOPTArry.push({value:optMap[key], key:key});
                        }
                        component.set("v.ReqOPTList", reqOPTArry);
                    }else if(model.opp.OpCo__c=='TEKsystems, Inc.'){
                         component.set("v.isOpcoTek",true);
                         component.set("v.isOpcoEmea", "false");
                         component.set("v.isOpcoRegular", "false");
                         component.set("v.isOpcoAerotek","false");
                    }
                    
					var reqComp = model.opp.Req_Compliance__c;
                    if(model.opp.Req_Export_Controls_Questionnaire_Reqd__c === true){
					  component.set("v.req.Req_Compliance__c",reqComp + '  ' +'Export Controls Questionnaire Required;');
					}else{
					  component.set("v.req.Req_Compliance__c",reqComp);
					}

                    component.set("v.req.Req_Export_Controls_Questionnaire_Reqd__c",model.opp.Req_Export_Controls_Questionnaire_Reqd__c);
                    component.set("v.req.Req_OFCCP_Required__c",model.opp.Req_OFCCP_Required__c);
                    
                    if(model.Organization_Office__c != null){
                        component.set("v.organization.Id",model.opp.Organization_Office__c);
                        component.set("v.organization.Name",model.opp.Organization_Office__r.Name);
                    }
                    component.set("v.req.EnterpriseReqSkills__c",model.opp.EnterpriseReqSkills__c);
                    
                    var oppList;
                    if(model.skillset){
                       oppList  = JSON.parse(model.skillset);
                    }
                    console.log('SKILLS'+oppList);
                    var finalSkills = [];
                    
                    if(model.opp.OpCo__c === "Allegis Group, Inc." || model.opp.OpCo__c === "Allegis Global Solutions, Inc." || 
                       model.opp.OpCo__c === "Allegis Partners, LLC" || model.opp.OpCo__c === "Major, Lindsey & Africa, LLC"){
                        finalSkills = [];
                    }else if(oppList){
                        for (var i = 0; i < oppList.length; i++) { 
                            if(oppList[i].favorite){
                                console.log('inside'+oppList[i].name);
                               finalSkills.push({ 
                                   "term" : oppList[i].name,
                                    "rawWeight" : 1,
                                    "required" : true
                                });
                            }else{
                                finalSkills.push({
                                    "term" : oppList[i].name,
                                    "rawWeight" : 0,
                                    "required" : false
                                });
                            }
                        }
                    }
                    
                    var finalOppSkills = finalSkills;
                    component.set("v.rawSkills",finalOppSkills);
                    
                    var stageArray = [];
                    stageArray.push({value:'--None--', key:'--None--'});
                    var stgName = "Draft";
                     var isRegOpco = component.get("v.isOpcoRegular");
                    var stageMap = model.stageMappings;
                    for ( var key in stageMap) {
                        if((model.opp.OpCo__c=='Aerotek, Inc'|| model.opp.OpCo__c=='AG_EMEA' || model.opp.OpCo__c == 'TEKsystems, Inc.') && (key=='Draft' || key=='Qualified') ){
                        	stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }else if(isRegOpco == 'true'){
                            stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }
                    }
                    component.set("v.stageList", stageArray);
                    component.set("v.req.StageName",stgName);
                      
                    var opcoListArray = [];
                    var opcoVal = model.opp.OpCo__c;
                    console.log('opp value'+opcoVal);
                    opcoListArray.push({value:'--None--', key:'--None--'}); 
                    var opcoListMap = model.opcoMappings;
                    for ( var key in opcoListMap) {
                        opcoListArray.push({value:opcoListMap[key], key:key, selected: key === opcoVal});
                    }

					var opcoCloneList = opcoListArray;
                    opcoCloneList.sort(function compare(a,b){
                                  if (a.key > b.key)
                                    return -1;
							      if (a.key < b.key)
                                    return 1;
                                  return 0;
                                });

				    opcoCloneList.reverse();

                    component.set("v.opcoList", opcoCloneList);
                    component.set("v.req.OpCo__c",opcoVal);

					console.log('CLONED DISPO '+model.dispositionMappings);
					console.log('CLONED SINGLE DISPO '+model.opp.Req_Disposition__c);

					/*Commented the cloning logic as part of S-124559*/
					/*if(model.opp.OpCo__c=='Aerotek, Inc' ||  model.opp.OpCo__c == 'TEKsystems, Inc.'){
					    var clonedispositionArray = [];
						var dispositionMap = model.dispositionMappings;
						var dispositionInfo = model.opp.Req_Disposition__c; 
                        clonedispositionArray.push({value:'--None--', key:'--None--'});  
							for (var key in dispositionMap) {
									clonedispositionArray.push({value:dispositionMap[key], key:key, selected: key === dispositionInfo});
								}
						component.set("v.dispositionList", clonedispositionArray);
					 }*/
					if(model.opp.OpCo__c == 'TEKsystems, Inc.'){
                        var IsTGSReqArray = [];
                        var IsTGSMap = model.TGSReqListMappings;
                        //var IsTGSInfo = model.opp.Req_TGS_Requirement__c; 
                        IsTGSReqArray.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSReqArray.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.isTGSReqList", IsTGSReqArray);
                        
                        var IsTGSPracEng = [];
                        var IsTGSMap = model.PracEngListMappings;
                        IsTGSPracEng.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSPracEng.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSPracEngList", IsTGSPracEng);
                        
                        var IsTGSIntTravel = [];
                        var IsTGSMap = model.IsIntTravelListMappings;
                        IsTGSIntTravel.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSIntTravel.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSIntTravelList", IsTGSIntTravel);
                        
                        var IsTGSInternalHire = [];
                        var IsTGSMap = model.InternalHireMappings;
                        IsTGSInternalHire.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSInternalHire.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSInternalHireList", IsTGSInternalHire);
                        
                        var IsTGSEmpAlign = [];
                        var IsTGSMap = model.EmploymentAlignmentMappings;
                        IsTGSEmpAlign.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSEmpAlign.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSEmpAlignList", IsTGSEmpAlign);
                        
                        var IsTGSFnlDecMkr = [];
                        var IsTGSMap = model.FinalDecisionMakerMappings;
                        IsTGSFnlDecMkr.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSFnlDecMkr.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSFnlDecMkrList", IsTGSFnlDecMkr);
                        
                        var IsTGSLocation = [];
                        var IsTGSMap = model.GlobalServiceLocationMappings;
                        IsTGSLocation.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSLocation.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSLocationList", IsTGSLocation);
                    }
                 
                    var productArray = [];
                    var productInfo = model.opp.Req_Product__c; 
                    var productMap = model.productMappings;
                    console.log('product value testing'+productInfo);
                    productArray.push({value:'--None--', key:'--None--'});  
                    for ( var key in productMap) {
                            productArray.push({value:productMap[key], key:key,selected: key ===productInfo});
                        }
                    component.set("v.productList", productArray);
                    component.set("v.req.Req_Product__c", productInfo);
                    
                    var productPicklistArray = [];
                    var productPicklistInfo = model.opp.Product__c;
                    var productPicklistMap = model.productPicklistMappings;
                    console.log('product value testing'+productInfo);
                    productPicklistArray.push({value:'--None--', key:'--None--'});
                    for ( var key in productPicklistMap) {
                    	productPicklistArray.push({value:productPicklistMap[key], key:key,selected: key ===productInfo});
                    }
                    component.set("v.productListPicklist", productPicklistArray);
                    component.set("v.req.Product__c", productPicklistInfo);
                                        
                    var currencyArray = [];
                    var curr = component.get("v.req.Currency__c");
                    currencyArray.push({value:'--None--', key:'--None--'}); 
                    var currencyMap = model.currencyMappings;
                    for ( var key in currencyMap) {
                        currencyArray.push({value:currencyMap[key], key:key, selected: key ===curr});
                    }
                    component.set("v.currencyList", currencyArray);
                    component.set("v.req.Currency__c",curr);
                   
                    var durUnit = component.get("v.req.Req_Duration_Unit__c");
                    var durationUnitArray = [];
                    durationUnitArray.push({value:'--None--', key:'--None--'}); 
                    var durationUnitMap = model.durationUnitMappings;
                    for ( var key in durationUnitMap) {
                        durationUnitArray.push({value:durationUnitMap[key], key:key, selected: key === durUnit});
                    }
                    component.set("v.durationUnitList", durationUnitArray);
                    component.set("v.req.Req_Duration_Unit__c",durUnit);
                    
                    var lossWashReasonArray = [];
                    var lossWash = model.opp.Req_Loss_Wash_Reason__c;
                    lossWashReasonArray.push({value:'--None--', key:'--None--'}); 
                    var lossWashReasonMap = model.lossWashReasonMappings;
                    for ( var key in lossWashReasonMap) {
                        lossWashReasonArray.push({value:lossWashReasonMap[key], key:key, selected: key === lossWash});
                    }
                    component.set("v.lossWashReasonList", lossWashReasonArray);
                    component.set("v.req.Req_Loss_Wash_Reason__c",lossWash);
                    
                    var workRemoteArray = [];
                    var workRem = model.opp.Req_Work_Remote__c;
                    workRemoteArray.push({value:'--None--', key:'--None--'}); 
                    var workRemoteMap = model.workRemoteMappings;
                    for ( var key in workRemoteMap) {
                        workRemoteArray.push({value:workRemoteMap[key], key:key, selected: key === workRem});
                    }
                    component.set("v.workRemoteList", workRemoteArray);
                    component.set("v.req.Req_Work_Remote__c",workRem);
                    
                     var minEducationArray = [];
                     var minEdu = model.opp.Req_Minimum_Education_Required__c;
                      minEducationArray.push({value:'--None--', key:'--None--'}); 
                      var minEducationMap = model.minimumEducationMappings;
                       for ( var key in minEducationMap) {
                          minEducationArray.push({value:minEducationMap[key], key:key,selected: key === minEdu });
                       }
                      component.set("v.minEducationList", minEducationArray);
                      component.set("v.req.Req_Minimum_Education_Required__c",minEdu);
                    
                    var practiceAreaArray = [];
                    var pracArea = model.opp.Req_Practice_Area__c;
                    practiceAreaArray.push({value:'--None--', key:'--None--'}); 
                    var practiceAreaMap = model.practiceAreaMappings;
                    for ( var key in practiceAreaMap) {
                        practiceAreaArray.push({value:practiceAreaMap[key], key:key,selected: key === pracArea});
                    }
                    component.set("v.practiceAreaList", practiceAreaArray);
                    component.set("v.req.Req_Practice_Area__c",pracArea);
                    
                    var experienceLevelArray = [];
                    var expLevel = model.opp.Req_Job_Level__c;
                    experienceLevelArray.push({value:'--None--', key:'--None--'}); 
                    var experienceLevelMap = model.experienceLevelMappings;
                    for (var key in experienceLevelMap) {
                        experienceLevelArray.push({value:experienceLevelMap[key], key:key, selected: key === expLevel});
                    }
                    component.set("v.experienceLevelList", experienceLevelArray);
                    component.set("v.req.Req_Job_Level__c",expLevel);
                    
                    var paymentTermsArray = [];
                    var payterm = model.opp.Req_Payment_terms__c;
                    paymentTermsArray.push({value:'--None--', key:'--None--'}); 
                    var paymentTermsMap = model.paymentTermsMappings;
                    for ( var key in paymentTermsMap) {
                        paymentTermsArray.push({value:paymentTermsMap[key], key:key,selected: key === payterm});
                    }
                    component.set("v.paymentTermsList", paymentTermsArray);
                    component.set("v.req.Req_Payment_terms__c",payterm);
                    
                    var termsofEngageArray = [];
                    var terms = model.opp.Req_Terms_of_engagement__c;
                    termsofEngageArray.push({value:'--None--', key:'--None--'}); 
                    var termsofEngageMap = model.termsOfEngagementMappings;
                    for ( var key in termsofEngageMap) {
                        termsofEngageArray.push({value:termsofEngageMap[key], key:key, selected: key === terms});
                    }
                    component.set("v.termsEngageList", termsofEngageArray);
                    component.set("v.req.Req_Terms_of_engagement__c",terms);
                   
                    var whyPositionArray = [];
                    var whyPosn = model.opp.Req_Why_is_Position_Open__c;
                    whyPositionArray.push({value:'--None--', key:'--None--'}); 
                    var positionListMap = model.whyPositionMappings;
                    for (var key in positionListMap){
                        whyPositionArray.push({value:positionListMap[key], key:key,selected: key === whyPosn });
                    }
                    component.set("v.whyPositionList", whyPositionArray);
                    component.set("v.req.Req_Why_is_Position_Open__c",whyPosn);
                    
                    //Req_Rate_Frequency__c
                    var rateFrequencyArray=[];
                    var rateFreq = model.opp.Req_Rate_Frequency__c
                       rateFrequencyArray.push({value:'--None--', key:'--None--'});
                       var rateFrequencyMap = model.rateFrequencyMappings;
                       for(var key in rateFrequencyMap){
                            if(rateFrequencyMap[key]!='Salary'){
                                rateFrequencyArray.push({value:rateFrequencyMap[key], key:key, selected: key === rateFreq});
                            }
                        }
                    
                    //Req Client Working on Req
                    var reqClientWorkingArray =[];
                    var clientWork = model.opp.Req_Client_working_on_Req__c;
                       reqClientWorkingArray.push({value:'--None--', key:'--None--'});
                       var clientWorkMap = model.ClientReqWorkingMappings;
                       console.log('REQ CLIENT WORK PLIST'+clientWorkMap);
                       for(var key in clientWorkMap){
                                reqClientWorkingArray.push({value:clientWorkMap[key], key:key, selected: key === clientWork});
                        }
                    
                    component.set("v.reqClientWorkList",reqClientWorkingArray);
                    component.set("v.rateFrequencyList",rateFrequencyArray);
                    component.set("v.req.Req_Rate_Frequency__c",rateFreq);
                    component.set("v.req.Description",model.opp.Req_Job_Description__c);
                    
                    var otMultiArray = [];
                    var otMultiplier = model.opp.OT_Multiplier__c;
                    otMultiArray.push({value:'--None--', key:'--None--'}); 
                    var otmultiListMap = model.otmultiplierMappings;
                    for (var key in otmultiListMap){
                        otMultiArray.push({value:otmultiListMap[key], key:key,selected: key === otMultiplier });
                    }
                    component.set("v.otmultitplierList", otMultiArray);
                    component.set("v.req.OT_Multiplier__c",otMultiplier);
                    
                    var alterDeliveryArray = [];
                    var alterDelivery = model.opp.Req_Delivery_Type__c;
                    alterDeliveryArray.push({value:'--None--', key:'--None--'}); 
                    var alterDeliveryListMap = model.alternateDeliveryTypeMappings;
                    for (var key in alterDeliveryListMap){
                        alterDeliveryArray.push({value:alterDeliveryListMap[key], key:key,selected: key === alterDelivery });
                    }
                    component.set("v.altdelivertyList", alterDeliveryArray);
                    component.set("v.req.Req_Delivery_Type__c",alterDelivery);
                    
                    var clientWorkingArray = [];
                    var clientWorking = model.opp.Req_Client_working_on_Req__c;
                    clientWorkingArray.push({value:'--None--', key:'--None--'});
                    var clientWorkingListMap = model.clientWorkingOnReqMappings;
                    for (var key in clientWorkingListMap){
                    	 clientWorkingArray.push({value:clientWorkingListMap[key], key:key,selected: key === clientWorking });
                    }
                    component.set("v.clientworkOnReqList", clientWorkingArray);
                    component.set("v.req.Req_Client_working_on_Req__c",clientWorking);
                    
                    
                    var draftReasonArray = [];
                    var draftReason = model.opp.Req_Draft_Reason__c;
                    draftReasonArray.push({value:'--None--', key:'--None--'});
                    var draftReasonListMap = model.draftReasonMappings;
                    for (var key in draftReasonListMap){
                          draftReasonArray.push({value:draftReasonListMap[key], key:key,selected: key === draftReason });
                    }
                    component.set("v.draftReasonList", draftReasonArray);
                    component.set("v.req.Req_Draft_Reason__c",draftReason);
                    
                    var serviceProductArray = [];
                    var serviceProduct = model.opp.Req_Service_Product_Offering__c;
                    serviceProductArray.push({value:'--None--', key:'--None--'}); 
                    var serviceProductListMap = model.serviceProductOfferingMappings;
                    for ( var key in serviceProductListMap) {
                        serviceProductArray.push({value:serviceProductListMap[key], key:key, selected: key === serviceProduct});
                    }
                    component.set("v.serviceproductOfferList", serviceProductArray);
                    component.set("v.req.Req_Service_Product_Offering__c",serviceProduct);
                    
                    var deliveryOfficeArray = [];
                    var deliveryOffice=model.opp.Req_Secondary_Delivery_Office__c;
                    deliveryOfficeArray.push({value:'--None--', key:'--None--'}); 
                    var deliviryOfficeListMap = model.alternativeDeliveryOfficeMappings;
                    for (var key in deliviryOfficeListMap){
                        deliveryOfficeArray.push({value:deliviryOfficeListMap[key], key:key,selected: key === deliveryOffice });
                    }
                    component.set("v.deliveryOfficeList", deliveryOfficeArray);
                    component.set("v.req.Req_Secondary_Delivery_Office__c",deliveryOffice);
                    
                    var oppLOBEITArray = [];
                    var oppLOBEIT=model.opp.REQ_Opportunity_driven_mostly_by_LOB__c;
                    oppLOBEITArray.push({value:'--None--', key:'--None--'});
                    var oppLOBEITListMap = model.oppLOBEITMapping;
                    for (var key in oppLOBEITListMap){
                          oppLOBEITArray.push({value:oppLOBEITListMap[key], key:key,selected: key === oppLOBEIT});
                    }
                    component.set("v.oppLOBEITList", oppLOBEITArray);
                    component.set("v.req.REQ_Opportunity_driven_mostly_by_LOB__c",oppLOBEIT);
                    
                    
                    //Product or Placement Type Rendering
                    if(productInfo === "Contract"){
                            component.set("v.isContract", "true");
                            component.set("v.isPermanent", "false");
                            component.set("v.isCTH", "false");
                      }else if(productInfo === "Permanent"){
                            component.set("v.isContract", "false");
                            component.set("v.isPermanent", "true");
                            component.set("v.isCTH", "false");
                      }else if(productInfo === "Contract to Hire"){
                            component.set("v.isContract", "false");
                            component.set("v.isPermanent", "false");
                            component.set("v.isCTH", "true");
                      }
                    
                    if(model.opp.Legacy_Product__r != null && model.opp.Legacy_Product__r.Name != null){
                       component.set("v.cloneProductId",model.opp.Legacy_Product__r.Name); 
                       component.set("v.isCloneMainSkill",true);
                    }
                    
                    component.set("v.req.Req_Total_Positions__c",model.opp.Req_Total_Positions__c);
                    component.set("v.req.Req_Include_Bonus__c",model.opp.Req_Include_Bonus__c);
                    
                    component.set("v.req.Req_Worksite_Street__c", model.opp.Req_Worksite_Street__c); 
                    component.set("v.req.StreetAddress2__c",model.opp.StreetAddress2__c);
                    component.set("v.req.Req_Worksite_City__c", model.opp.Req_Worksite_City__c); 
                    component.set("v.req.Req_Worksite_State__c", model.opp.Req_Worksite_State__c); 
                    component.set("v.req.Req_Worksite_Postal_Code__c", model.opp.Req_Worksite_Postal_Code__c); 
                    component.set("v.req.Req_Worksite_Country__c", model.opp.Req_Worksite_Country__c);
                    
                    if(model.act!=null){
                        component.set("v.masterGlobalAccountId",model.act.Master_Global_AccountID_formula_field__c);
                    }else{
                        component.set("v.masterGlobalAccountId",'NO-MASTER-ID');
                    }
                   
                    component.set("v.inputOffCCPCompliant",model.opp.Req_OFCCP_Required__c);
                    component.set("v.Ownership",model.ownershipOpco);
                    component.set("v.recordType","Req");
                    
                    var businessUnitArray = [];
                    var busUnit = model.opp.BusinessUnit__c;
                    var opcoDivi = model.opp.Req_Division__c;
                    var busArray = [];
                    businessUnitArray.push({value:'--None--', key:'--None--'});
                    var businessUnitMap = model.businessUnitMappings;
                     console.log('bus unit model'+model.businessUnitMappings);
                    for ( var key in businessUnitMap) {
                        if(key != 'VMS')
                        {
                        businessUnitArray.push({value:businessUnitMap[key], key:key, selected: key === opcoDivi});
                        busArray.push(key);
                    }
                    }
					
					var oppBusinessUnitList = businessUnitArray;
                     oppBusinessUnitList.sort(function compare(a,b){
                                  if (a.key > b.key)
                                    return -1;
							      if (a.key < b.key)
                                    return 1;
                                  return 0;
                                });

				     oppBusinessUnitList.reverse();
					 

                    component.set("v.businessUnitList", oppBusinessUnitList);
                    component.set("v.businessList", busArray);
                    component.set("v.req.Req_Division__c",opcoDivi);
                    
                    
                    var opcoDivisionArray=[];
                    var opcoDivsionMap=model.opcoDivsionDepdentMappings;
                    for(var key in opcoDivsionMap){
                      opcoDivisionArray.push({value:opcoDivsionMap[key], key:key,selected: key === opcoDivi});
                    }
                    component.set("v.opcoDivisionList",opcoDivisionArray);
                   
                    
                     if(opcoVal=='Aerotek, Inc'|| opcoVal=='TEKsystems, Inc.'){
                         component.set("v.MainskillOpco",true);
                          this.EnterpriseReqSegmentModal(component,event,helper);
                      }
                      else{
                         component.set("v.MainskillOpco",false);
                      }
                    if(opcoVal=='TEKsystems, Inc.'){
                        component.find("isTGSReqId").set("v.value", '--None--');
                        component.set("v.isTGSTrue", "true");
                        component.find("isTGSPEId").set("v.value", '--None--');
                        component.find("isTGSLocationId").set("v.value", '--None--');
                        component.find("isEmploymentAlignmentId").set("v.value", '--None--');
                        component.find("isFinaDecMakId").set("v.value", '--None--');
                        component.find("isInterNationalId").set("v.value", '--None--');
                        component.find("isInternalHireId").set("v.value", '--None--');
                        component.find("isBackfillId").set("v.value", 'false');
                        component.find("isNationalId").set("v.value", 'false');
                        component.set("v.isTGSTrue", "false");
                    }
                }else{
                    // Create New Req
                    
                    console.log('opco'+model.opco);
                   //Opco ---- UI --- Show -- Hide
                   if(model.opco === "AG_EMEA"){
                        component.set("v.isOpcoEmea", "true");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoTek",false);
                        component.set("v.isOpcoAerotek",false);
                    }else if(model.opco === "Allegis Group, Inc." || model.opco === "Allegis Global Solutions, Inc." || 
                             model.opco === "Allegis Partners, LLC" || model.opco === "Major, Lindsey & Africa, LLC"){
                        component.set("v.isOpcoRegular", "true");
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoTek","false");
                        component.set("v.isOpcoAerotek","false");
                    }else if(model.opco=='Aerotek, Inc'){
                        component.set("v.isOpcoAerotek",true);
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoTek","false");
                    }else if(model.opco=='TEKsystems, Inc.'){
                         component.set("v.isOpcoTek",true);
                         component.set("v.isOpcoEmea", "false");
                         component.set("v.isOpcoRegular", "false");
                         component.set("v.isOpcoAerotek","false");
                    }
                    
					//Proactive Submittal - S-143480
					if(model.proactiveSubmittal != null && model.proactiveSubmittal !== 'undefined') {
						component.set("v.proactiveSubmittal", model.proactiveSubmittal);
						component.set("v.req.Req_Job_Title__c", model.proactiveSubmittal.Job_Title__c);
						component.set("v.isProactiveSubmittal", true);
						component.set("v.psSkills", model.skillset);
					}
					
					if(model.exportControlsReqd === true){
					  component.set("v.req.Req_Compliance__c",'Export Controls Questionnaire Required;');
					}
                    component.set("v.req.Req_Export_Controls_Questionnaire_Reqd__c",model.exportControlsReqd);
                    component.set("v.req.Req_OFCCP_Required__c",model.ofccpReqd);
					
					if(model.isTgsReq === true){
					   component.set("v.isTgsParentReq", model.isTgsReq);
					   component.set("v.req.Req_Opportunity__c",model.tgsOpptyId);
					}
                    
                    var productArray = [];
                    var productMap = model.productMappings;
                    console.log('product value testing'+productInfo);
                    productArray.push({value:'--None--', key:'--None--'});  
                    for (var key in productMap) {
                            productArray.push({value:productMap[key], key:key});
                        }
                    component.set("v.productList", productArray);
					var productPicklistArray = [];
                    var productPicklistMap = model.productPicklistMappings;
                    console.log('product value testing'+productInfo);
                    productPicklistArray.push({value:'--None--', key:'--None--'});
                    for (var key in productPicklistMap) {
                    productPicklistArray.push({value:productPicklistMap[key], key:key});
                    }
                    component.set("v.productListPicklist", productPicklistArray);
					 /*Commented the creation for Req Dispotion field as part of S-124559*/
					/*if(model.opco=='Aerotek, Inc' ||  model.opco == 'TEKsystems, Inc.'){
					    var dispositionArray = [];
						var dispositionMap = model.dispositionMappings;
						dispositionArray.push({value:'--None--', key:'--None--'});  
						for (var key in dispositionMap) {
								dispositionArray.push({value:dispositionMap[key], key:key});
							}
						component.set("v.dispositionList", dispositionArray);
					 }*/
					
                    if(model.opco == 'TEKsystems, Inc.'){
					    var isTGSReqArray = [];
						var isTGSReqMap = model.TGSReqListMappings;
						var oppRecTypeId = component.get("v.oppRecTypeId");
                        var tgsValue ;
                        if(oppRecTypeId !='' && oppRecTypeId !=null && typeof oppRecTypeId !='undefined'){
                            tgsValue = 'Yes';
                            component.set("v.req.Req_TGS_Requirement__c", "Yes")
                            component.set("v.isTGSTrue", "true");
                        } else {
                            tgsValue = '--None--';
                        }
						isTGSReqArray.push({value:'--None--', key:'--None--'});  
						for (var key in isTGSReqMap) {
								isTGSReqArray.push({value:isTGSReqMap[key], key:key, selected: key === tgsValue});
							}
						component.set("v.isTGSReqList", isTGSReqArray);
                        
                        var IsTGSPracEng = [];
                        var IsTGSMap = model.PracEngListMappings;
                        IsTGSPracEng.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSPracEng.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSPracEngList", IsTGSPracEng);
                        
                        var IsTGSIntTravel = [];
                        var IsTGSMap = model.IsIntTravelListMappings;
                        IsTGSIntTravel.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSIntTravel.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSIntTravelList", IsTGSIntTravel);
                        
                        var IsTGSInternalHire = [];
                        var IsTGSMap = model.InternalHireMappings;
                        IsTGSInternalHire.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSInternalHire.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSInternalHireList", IsTGSInternalHire);
                        
                        var IsTGSEmpAlign = [];
                        var IsTGSMap = model.EmploymentAlignmentMappings;
                        IsTGSEmpAlign.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSEmpAlign.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSEmpAlignList", IsTGSEmpAlign);
                        
                        var IsTGSFnlDecMkr = [];
                        var IsTGSMap = model.FinalDecisionMakerMappings;
                        IsTGSFnlDecMkr.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSFnlDecMkr.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSFnlDecMkrList", IsTGSFnlDecMkr);
                        
                        var IsTGSLocation = [];
                        var IsTGSMap = model.GlobalServiceLocationMappings;
                        IsTGSLocation.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSLocation.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSLocationList", IsTGSLocation);
					 }
                    
                    if(model.opco=='Aerotek, Inc'){
                        var reqOPTArry = [];
                        var optMap = model.ReqOptMappings;
                        reqOPTArry.push({value:'--None--', key:'--None--'});  
                        for (var key in optMap) {
                            reqOPTArry.push({value:optMap[key], key:key});
                        }
                        component.set("v.ReqOPTList", reqOPTArry);
                    }
					
                     var stageArray = [];
                     var defaultStage = "Draft";
                    stageArray.push({value:'--None--', key:'--None--'}); 
                    var isRegOpco = component.get("v.isOpcoRegular");
                    var stageMap = model.stageMappings;
                    var soppid=component.get("v.soppId");
                    var index=0;
                    for ( var key in stageMap) {
                        console.log('stage'+key+''+model.opco);
                        if((model.opco=='Aerotek, Inc' || model.opco == 'AG_EMEA' || model.opco == 'TEKsystems, Inc.') && (key=='Draft' || key=='Qualified') ){
                        	stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }else if(isRegOpco == 'true'){
                            stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }
                        index=index+1;
                        if(key=='Draft'){
                             if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){ //Strategic Opportunity redirection
                                 var obj=stageArray[index];
                                 obj.selected="true"; 
                             }
                        }
                        
                    }
                    component.set("v.stageList", stageArray);
                  // component.set("v.req.StageName","Draft");
                    
                    var currencyArray = [];
                    var defCurrency = "USD - U.S Dollar";
                    currencyArray.push({value:'--None--', key:'--None--'}); 
                    var currencyMap = model.currencyMappings;
                    for ( var key in currencyMap) {
                        currencyArray.push({value:currencyMap[key], key:key, selected: key === defCurrency});
                    }
                    component.set("v.currencyList", currencyArray);
                    component.set("v.req.Currency__c",'USD - U.S Dollar');
                    
                    var durationUnitArray = [];
                    durationUnitArray.push({value:'--None--', key:'--None--'}); 
                    var durationUnitMap = model.durationUnitMappings;
                    for ( var key in durationUnitMap) {
                        durationUnitArray.push({value:durationUnitMap[key], key:key});
                    }
                    component.set("v.durationUnitList", durationUnitArray);
                    component.set("v.req.Req_Duration_Unit__c","--None--");
                    
                     var lossWashReasonArray = [];
                    lossWashReasonArray.push({value:'--None--', key:'--None--'}); 
                    var lossWashReasonMap = model.lossWashReasonMappings;
                    for ( var key in lossWashReasonMap) {
                        lossWashReasonArray.push({value:lossWashReasonMap[key], key:key});
                    }
                    component.set("v.lossWashReasonList", lossWashReasonArray);
                    
                    var workRemoteArray = [];
                    workRemoteArray.push({value:'--None--', key:'--None--'}); 
                    var workRemoteMap = model.workRemoteMappings;
                    for ( var key in workRemoteMap) {
                        workRemoteArray.push({value:workRemoteMap[key], key:key});
                    }
                    component.set("v.workRemoteList", workRemoteArray);
                    
                     var minEducationArray = [];
                      minEducationArray.push({value:'--None--', key:'--None--'}); 
                      var minEducationMap = model.minimumEducationMappings;
                       for ( var key in minEducationMap) {
                          minEducationArray.push({value:minEducationMap[key], key:key});
                       }
                      component.set("v.minEducationList", minEducationArray);
                    
                    var practiceAreaArray = [];
                    practiceAreaArray.push({value:'--None--', key:'--None--'}); 
                    var practiceAreaMap = model.practiceAreaMappings;
                    for ( var key in practiceAreaMap) {
                        practiceAreaArray.push({value:practiceAreaMap[key], key:key});
                    }
                    component.set("v.practiceAreaList", practiceAreaArray);
                    
                    var experienceLevelArray = [];
                    experienceLevelArray.push({value:'--None--', key:'--None--'}); 
                    var experienceLevelMap = model.experienceLevelMappings;
                    for ( var key in experienceLevelMap) {
                        experienceLevelArray.push({value:experienceLevelMap[key], key:key});
                    }
                    component.set("v.experienceLevelList", experienceLevelArray); 
                    
                    var paymentTermsArray = [];
                    paymentTermsArray.push({value:'--None--', key:'--None--'}); 
                    var paymentTermsMap = model.paymentTermsMappings;
                    for ( var key in paymentTermsMap) {
                        paymentTermsArray.push({value:paymentTermsMap[key], key:key});
                    }
                    component.set("v.paymentTermsList", paymentTermsArray);
                    
                    var termsofEngageArray = [];
                    termsofEngageArray.push({value:'--None--', key:'--None--'}); 
                    var termsofEngageMap = model.termsOfEngagementMappings;
                    for ( var key in termsofEngageMap) {
                        termsofEngageArray.push({value:termsofEngageMap[key], key:key});
                    }
                    component.set("v.termsEngageList", termsofEngageArray);
                    
                    var opcoListArray = [];
                    opcoListArray.push({value:'--None--', key:'--None--'}); 
                    var opcoListMap = model.opcoMappings;
                    var userOpco = model.opco;
                    for ( var key in opcoListMap) {
                      opcoListArray.push({value:opcoListMap[key], key:key, selected: key === userOpco}); 
                    }

					var opcoValList = opcoListArray;
                    opcoValList.sort(function compare(a,b){
                                  if (a.key > b.key)
                                    return -1;
							      if (a.key < b.key)
                                    return 1;
                                  return 0;
                                });

				    opcoValList.reverse();

                    component.set("v.opcoList", opcoValList);
                    
                    var whyPositionArray = [];
                    whyPositionArray.push({value:'--None--', key:'--None--'}); 
                    var positionListMap = model.whyPositionMappings;
                    for (var key in positionListMap){
                        whyPositionArray.push({value:positionListMap[key], key:key});
                    }
                    component.set("v.whyPositionList", whyPositionArray);
                    
                    var businessUnitArray = [];
                    var busArray = [];
                    businessUnitArray.push({value:'--None--', key:'--None--'}); 
                    var businessUnitMap = model.businessUnitMappings;
                    for ( var key in businessUnitMap) {
                        businessUnitArray.push({value:businessUnitMap[key], key:key});
                        busArray.push(key);
                    }

                    component.set("v.businessUnitList", businessUnitArray);
                    component.set("v.businessList", busArray);
                    
                     var rateFrequencyArray=[];
                       rateFrequencyArray.push({value:'--None--', key:'--None--'});
                       var rateFrequencyMap = model.rateFrequencyMappings;
                       for(var key in rateFrequencyMap){
                            if(rateFrequencyMap[key]!='Salary'){
                                rateFrequencyArray.push({value:rateFrequencyMap[key], key:key});
                            }
                        }
                    component.set("v.rateFrequencyList",rateFrequencyArray);
                    
                    //Req Client Working on Req
                       var reqClientWorkingArray =[];
                       reqClientWorkingArray.push({value:'--None--', key:'--None--'});
                       var clientWorkMap = model.ClientReqWorkingMappings;
                       for(var key in clientWorkMap){
                                reqClientWorkingArray.push({value:clientWorkMap[key], key:key});
                        }
                    
                    component.set("v.reqClientWorkList",reqClientWorkingArray);
                    
                    var opcoDivisionArray=[];
                    var opcoDivsionMap=model.opcoDivsionDepdentMappings;
                    for(var key in opcoDivsionMap){
                      opcoDivisionArray.push({value:opcoDivsionMap[key], key:key});
                    }
                    component.set("v.opcoDivisionList",opcoDivisionArray);
                    
                    
                     var rateFrequencyArray=[];
                       rateFrequencyArray.push({value:'--None--', key:'--None--'});
                       var rateFrequencyMap = model.rateFrequencyMappings;
                       for(var key in rateFrequencyMap){
                            if(rateFrequencyMap[key]!='Salary'){
                                rateFrequencyArray.push({value:rateFrequencyMap[key], key:key});
                            }
                        }
                    component.set("v.rateFrequencyList",rateFrequencyArray);
                    
                    var opcoDivisionArray=[];
                    var opcoDivsionMap=model.opcoDivsionDepdentMappings;
                    for(var key in opcoDivsionMap){
                      opcoDivisionArray.push({value:opcoDivsionMap[key], key:key});
                    }
                    component.set("v.opcoDivisionList",opcoDivisionArray);
                    
                    
                    
                    var otMultiArray = [];
                  
                    otMultiArray.push({value:'--None--', key:'--None--'}); 
                    var otmultiListMap = model.otmultiplierMappings;
                    for (var key in otmultiListMap){
                        otMultiArray.push({value:otmultiListMap[key], key:key });
                    }
                    component.set("v.otmultitplierList", otMultiArray);
                    
                    
                    var alterDeliveryArray = [];
                   
                    alterDeliveryArray.push({value:'--None--', key:'--None--'}); 
                    var alterDeliveryListMap = model.alternateDeliveryTypeMappings;
                    for (var key in alterDeliveryListMap){
                        alterDeliveryArray.push({value:alterDeliveryListMap[key], key:key });
                    }
                    component.set("v.altdelivertyList", alterDeliveryArray);
                    
                    var deliveryOfficeArray = [];
                    deliveryOfficeArray.push({value:'--None--', key:'--None--'}); 
                    var deliviryOfficeListMap = model.alternativeDeliveryOfficeMappings;
                    for (var key in deliviryOfficeListMap){
                        deliveryOfficeArray.push({value:deliviryOfficeListMap[key], key:key });
                    }
                    component.set("v.deliveryOfficeList", deliveryOfficeArray);
                   
                    
                    var clientWorkingArray = [];
                   
                    clientWorkingArray.push({value:'--None--', key:'--None--'});
                    var clientWorkingListMap = model.clientWorkingOnReqMappings;
                    for (var key in clientWorkingListMap){
                    	 clientWorkingArray.push({value:clientWorkingListMap[key], key:key });
                    }
                    component.set("v.clientworkOnReqList", clientWorkingArray);
                    
                    
                    
                    var draftReasonArray = [];
                   
                    draftReasonArray.push({value:'--None--', key:'--None--'});
                    var draftReasonListMap = model.draftReasonMappings;
                    for (var key in draftReasonListMap){
                        draftReasonArray.push({value:draftReasonListMap[key], key:key});
                    }
                    component.set("v.draftReasonList", draftReasonArray);
                    component.set("v.req.Req_Draft_Reason__c",'--None--');
                    
                    
                     var oppLOBEITArray = [];
                   
                    oppLOBEITArray.push({value:'--None--', key:'--None--'});
                    var oppLOBEITListMap = model.oppLOBEITMapping;
                    for (var key in oppLOBEITListMap){
                          oppLOBEITArray.push({value:oppLOBEITListMap[key], key:key});
                    }
                    component.set("v.oppLOBEITList", oppLOBEITArray);
                    
                    var serviceProductArray = [];
                    serviceProductArray.push({value:'--None--', key:'--None--'}); 
                    var serviceProductListMap = model.serviceProductOfferingMappings;
                    for ( var key in serviceProductListMap) {
                        serviceProductArray.push({value:serviceProductListMap[key], key:key});
                    }
                    component.set("v.serviceproductOfferList", serviceProductArray);
                    
                    component.set("v.req.Req_Total_Positions__c",1);
                    component.set("v.req.Req_Include_Bonus__c",true);
                    
                    component.set("v.req.Req_Worksite_Street__c",model.act.Account_Street__c); 
                    component.set("v.req.Req_Worksite_City__c",model.act.Account_City__c); 
                    component.set("v.req.Req_Worksite_State__c",model.act.Account_State__c); 
                    component.set("v.req.Req_Worksite_Postal_Code__c",model.act.Account_Zip__c); 
                    component.set("v.req.Req_Worksite_Country__c",model.act.Account_Country__c);
                    
                     //alert('Account Country'+model.act.Account_Country__c);
                
                   
                    //End of Create New Req
                    
                    component.set("v.masterGlobalAccountId",model.act.Master_Global_AccountID_formula_field__c);
                    component.set("v.Ownership",model.ownershipOpco);
                    component.set("v.req.OpCo__c",model.opco);
                    component.set("v.recordType","Req");
                    component.set("v.inputOffCCPCompliant",model.act.OFCCP_Compliant__c);
                    component.set("v.req.Req_End_Client__c",null);
                   
                   this.loadBusinessUnitValues(component,event,model.opco);
				   
				   var opcoDivison = '--None--';
				   var businessUnitArray = [];
                   var businessUnitMap = component.get("v.businessUnitList");;
                     
                   for (var key in businessUnitMap) {
                       if(key != 'VMS')
                        {
                      businessUnitArray.push({value:businessUnitMap[key].key, key:businessUnitMap[key].key, selected: businessUnitMap[key].key === opcoDivison});
                   }
                   }
					
					var oppBusinessUnitList = businessUnitArray;
                    oppBusinessUnitList.sort(function compare(a,b){
                                  if (a.key > b.key)
                                    return -1;
							      if (a.key < b.key)
                                    return 1;
                                  return 0;
                                });

				    oppBusinessUnitList.reverse();

                    component.set("v.businessUnitList", oppBusinessUnitList);
				    component.set("v.req.Req_Division__c",opcoDivison);
                }
                
            }  
            
            
            //Strategic Opportunity
            var soppid=component.get("v.soppId");
            if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){ //Strategic Opportunity redirection
            	component.set("v.req.StageName",'Draft');
        	}
            
        });
        
        $A.enqueueAction(action);
       
    },    
    addAltRecord : function(component,event,helper) {
        var supportRequestList = component.get("v.supportRequestList");
        supportRequestList.push({
            'sobjectType' : 'Support_Request__c',
            'Service_Product_Offering__c' : '',
            'Alternate_Delivery_Type__c' : '',
            'Alternate_Delivery_Shared_Positions__c' : '',
            'Alternate_Delivery_Office__c' : ''
        });
        component.set("v.supportRequestList", supportRequestList);
    },
    getQueryString : function(field,url){
        var href = url ? url : window.location.href;
        var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        var string = reg.exec(href);
        return string ? string[1] : null;
    },
    
    EnterpriseReqSegmentModal : function(component,event,helper){
        var action = component.get("c.getsegmentReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
      // console.log('check opco change '+event.getsource());
        action.setParams({"Opco": component.get("v.req.OpCo__c"),"Division": component.get("v.req.Req_Division__c")});

        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var segmentmodel = data.getReturnValue();
                
                var segmentarray = [];
                segmentarray.push({value:'--None--', key:'--None--'}); 
                var segmentmap = segmentmodel.SegmentMappings;
                // console.log('Size of Segment Mappings--->'+segmentmap.length);
                for ( var key in segmentmap) {
                    if(key != 'VMS')
                    {
                    segmentarray.push({value:segmentmap[key], key:key});
                }
                }
                component.set("v.segmentList", segmentarray);
                
            }
            });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },
    
    EnterpriseReqJobcodeModal : function(component,event,helper){
        var action = component.get("c.getJobcodeReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
       
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value") });
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var jobcodemodel = data.getReturnValue();
                
                var jobcodearray = [];
                jobcodearray.push({value:'--None--', key:'--None--'}); 
                var jobcodemap = jobcodemodel.JobCodeMappings;
                // console.log('jobcodemap'+jobcodemap);
                for ( var key in jobcodemap) {
                    jobcodearray.push({value:jobcodemap[key], key:key});
                                    }
                // console.log('harry'+jobcodearray);
                component.set("v.jobcodeList", jobcodearray);
                
            }
            component.set("v.workAddressFlag",true);
            });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },
    
    EnterpriseReqCategoryModal : function(component,event,helper){
        var action = component.get("c.getCategoryReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
       
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value"),"Jobcode" :this.findComp(component,"JobcodeId").get("v.value")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var categorymodel = data.getReturnValue();
                
                var categoryarray = [];
                categoryarray.push({value:'--None--', key:'--None--'}); 
                var categorymap = categorymodel.CategoryMappings;
                // console.log('categorymap'+categorymap);
                for ( var key in categorymap) {
                    categoryarray.push({value:categorymap[key], key:key});
                                    }
                // console.log('harry'+categoryarray);
                component.set("v.CategoryList", categoryarray);
                
            }
            });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },    
    findComp : function(component,auraid){
        
        var inst=component.find(auraid);
        
        var flag=Array.isArray(inst);
        
        var retVal;
        
        if(flag ){
            retVal= inst[0];
        }else{
         	retVal=inst;
        }
        return retVal;
    },    
    EnterpriseReqMainSkillModal : function(component,event,helper){
        var action = component.get("c.getMainskillReqPicklists");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
        
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value"),"Jobcode" :this.findComp(component,"JobcodeId").get("v.value"),"Category":this.findComp(component,"CategoryId").get("v.value")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var MainSkillmodel = data.getReturnValue();
                
                var MainSkillarray = [];
                MainSkillarray.push({value:'--None--', key:'--None--'}); 
                var MainSkillmap = MainSkillmodel.MainSkillMappings;
                // console.log('MainSkillmap'+MainSkillmap);
                for ( var key in MainSkillmap) {
                    MainSkillarray.push({value:MainSkillmap[key], key:key});
                                    }
                // console.log('harry'+MainSkillarray);
                component.set("v.MainSkillList", MainSkillarray);
                
            }
            });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },
    
    EnterpriseReqMainSkillid : function(component,event,helper){
        var action = component.get("c.getMainskillid");
      // Var Divisionval = component.find("businessUnitId").get("v.value");
       // Var Opcoval = component.find("opcoId").get("v.value");
      
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value"),"Jobcode" :this.findComp(component,"JobcodeId").get("v.value"),"Category":this.findComp(component,"CategoryId").get("v.value"),"MainSkill":this.findComp(component,"MainSkillId").get("v.value")});
        
            action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var MainSkillid = data.getReturnValue();
                component.set("v.MainSkillProductid", MainSkillid);
                // console.log('mainskillid'+ MainSkillid)
            }
          });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    }, 
   
    EnterpriseReqReSet : function (component,event,helper){
        
        var opco=component.get("v.req.OpCo__c");
        
        if(component.find("businessUnitId")!=null && opco != 'AG_EMEA' && this.findComp(component,"businessUnitId").get("v.value")=='--None--'){
          	  
          	  this.findComp(component,"segmentId").set("v.value",'--None--');
        }
        
        if(component.find("segmentId")!=null && this.findComp(component,"segmentId").get("v.value")=='--None--'){
          	  // console.log('Value of segmentId id '+(component.find("segmentId").get("v.value")));
              this.findComp(component,"JobcodeId").set("v.value",'--None--');
            
        }
        
        if(component.find("JobcodeId")!=null && this.findComp(component,"JobcodeId").get("v.value")=='--None--'){
          	  // console.log('Value of JobcodeId id '+(component.find("JobcodeId").get("v.value")));
              this.findComp(component,"CategoryId").set("v.value",'--None--');
        }
        
         if(component.find("CategoryId")!=null && this.findComp(component,"CategoryId").get("v.value")=='--None--'){
          	  // console.log('Value of CategoryId id '+(component.find("CategoryId").get("v.value")));
              this.findComp(component,"MainSkillId").set("v.value",'--None--');
        }
        
        if(component.find("MainSkillId")!=null && this.findComp(component,"MainSkillId").get("v.value")=='--None--'){
          	  // console.log('Value of MainSkillId id '+(component.find("MainSkillId").get("v.value")));
              //component.find("MainSkillId").set("v.value",'--None--');
        }
        
    },
    
    handleSaveClick : function(component,event,helper){
       component.set("v.isDisabled", true);
       this.saveEnterpriseReq(component,event,helper);
   },
    
    saveEnterpriseReq : function(component,event,helper){
        component.set("v.isDisabled", true);
        
        if(component.get("v.inputOffCCPCompliantElse")!=null && component.get("v.inputOffCCPCompliantElse")==true){
        	component.set("v.req.Req_OFCCP_Required__c",component.get("v.inputOffCCPCompliantElse"));    
        }
        
        var oppReq = component.get("v.req");

		 if(oppReq.OpCo__c =='AG_EMEA'){
             console.log('test ag emea');
		 component.set("v.req.Name","REQ-Opportunity");
         }


        var startDatevar=component.get("v.req.Start_Date__c");

         if(startDatevar=='' ||typeof startDatevar =='undefined'){
            oppReq.Start_Date__c=null;
        }
        
        if(oppReq.OpCo__c =='Aerotek, Inc'){
            var reqOpt = component.get("v.req.Req_OPT__c");
            if(reqOpt =='' ||typeof reqOpt =='undefined'){
               oppReq.Req_OPT__c=null; 
            }
        }
        
        var reqDateOpenVar=component.get("v.req.Req_Date_Client_Opened_Position__c");

         if(reqDateOpenVar=='' ||typeof reqDateOpenVar =='undefined'){
            oppReq.Req_Date_Client_Opened_Position__c=null;
        }
        
        var reqInterviewDateVar=component.get("v.req.Req_Interview_Date__c");

        if(reqInterviewDateVar=='' ||typeof reqInterviewDateVar =='undefined'){
            oppReq.Req_Interview_Date__c=null;
        }
        
        var reqOpenDateVar=component.get("v.req.Req_RRC_Open_Date__c");
        
        if(reqOpenDateVar=='' ||typeof reqOpenDateVar =='undefined'){
            oppReq.Req_RRC_Open_Date__c=null;
        }
        
        
        var closeDatevar=component.get("v.req.CloseDate");
        if(closeDatevar=='' ||typeof closeDatevar =='undefined'){
            oppReq.CloseDate=null;
        }
        
        //Strategic Opportunity
        var soppid=component.get("v.soppId");
        var oppRecTypeId = component.get("v.oppRecTypeId");
        if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){
            if(oppRecTypeId == '012U0000000DWp4IAG'){
				oppReq.Req_Opportunity__c = component.get("v.soppId");
        	}else{
            	oppReq.Originating_Opportunity__c = component.get("v.soppId");
            }
        }
        
       
        var oppty = JSON.stringify(oppReq);
        var accStg = component.get("v.account");
        var acctName = JSON.stringify(accStg);
        var hStg = component.get("v.contact");
        var mgr = JSON.stringify(hStg);
        var prodid=component.get("v.MainSkillProductid");
        
        var flagval=this.validateEnterpriseReq(component, event, helper);
         
        //Validation for Enterprise Req call.
        if(flagval){
            
        var oppClosedate = component.get("v.req.CloseDate");
        var cdatefield = component.find("inputCloseDate");
        //Close Date
        var pillData = component.get("v.automatchSkills");
        var pillList = [];
        var favSkills ='';
        var regularSkills ='';
        var finalSkills ='';
        if(pillData!=null){
               for (var j=0; j<pillData.length; j++) {
                if(pillData[j].required === "true" || pillData[j].required === true){
                    favSkills += pillData[j].key + ',';
                    var jsonFavSkill = { "name": pillData[j].key, "favorite":true };
                    pillList.push(jsonFavSkill);
                 }else{
                    regularSkills += pillData[j].key + ',';
                    var jsonSkill = { "name": pillData[j].key, "favorite":false };
                    pillList.push(jsonSkill);
                }   
            }
        }
            
        //Alternate Delivery
        var supportRequestList = component.get("v.supportRequestList");
        var altDeliveryList = JSON.stringify(supportRequestList);
        
        finalSkills = JSON.stringify(pillList);
        component.set("v.oppSkills",finalSkills);
            
        var officeInfoId = component.get("v.organization.Id");
        //job title
        var jtitle = component.get("v.newJobTitle");
            // console.log("new job"+jtitle);
        //Create Enterprise Req
                
                    /*console.log(component.get("v.oppSkills"));
                    console.log('passed');
                    console.log(finalSkills);*/
                    var optyStr = component.get("v.oppSkills");
                    var reqNewSkills = component.get("v.oppSkills");
                    let txSkills=component.get("v.skills");
                    reqNewSkills=JSON.stringify(txSkills);
            		if(oppReq.OpCo__c =='AG_EMEA'){
             			let txSkills=component.get("v.skills");
                        reqNewSkills=JSON.stringify(txSkills);
		                component.set("v.skills","");
         			}
                    
					//Spinner Starting
					component.set("v.spinner", true);
					let spinner = component.find("reqSpinner");
					$A.util.toggleClass(spinner, "slds-hide");
					//End Spinner Starting
                    
                    let proactiveSubmittal = component.get("v.proactiveSubmittal");
					if(component.get("v.isProactiveSubmittal")) {
						proactiveSubmittal.Status = component.get("v.proactiveSubmittalStatus");
					}
                    var actionopp = component.get("c.saveEnterpriseReq");
                    actionopp.setParams({"oppStr":  oppty,
                                        "accName": acctName,
                                        "hiringMgr": mgr,
                                         "skills":reqNewSkills,"Mainskillid":prodid, "officeId":officeInfoId,
                                        "altDeliveryList":altDeliveryList,
										"proactiveSubmittal":proactiveSubmittal});
                                    
                      actionopp.setCallback(this, function(response) {
							//Spinner Stopping
							$A.util.toggleClass(spinner, "slds-hide");
							component.set("v.spinner", false);
							//End Spinner Stopping

                           var oppId = response.getReturnValue();
                           component.set("v.isDisabled", true);
                            // Opp successfully created.
                            if (response.getState() === "SUCCESS") {
                                // console.log('Oppid--->'+oppId);
                                var errors = response.getError();
                                console.log(errors);
                                var opptyId = response.getReturnValue();
                                component.set("v.newOpportunityId",opptyId);
                                let istxOpcoEmea=component.get("v.isOpcoEmea");
        						if(istxOpcoEmea!=null && istxOpcoEmea==true){
                                    component.set("v.skills",null);
                                    component.set("v.txSkill",null);
                                    component.set("v.suggestedjobTitle",null);
                                    component.set("v.normalizedData",null);
                                    component.set("v.rawData",null);
                           		}    
                                
                                /*
                                  $A.util.addClass(component.find("MainModel"), "slds-modal slds-fade-in-hide");
                                  $A.util.removeClass(component.find("MainModel"), "slds-modal slds-fade-in-open");        
                                  $A.util.addClass(component.find("modalBackdrop"),  "slds-backdrop slds-backdrop--hide"); 
                                  $A.util.removeClass(component.find("modalBackdrop"),  "slds-backdrop slds-backdrop--open");
                                
                                var spinner = component.find('spinner');
                                var evt = spinner.get("e.toggle");
                                evt.setParams({ isVisible : false });
                                evt.fire(); 
                                */
                                if(component.get("v.isProactiveSubmittal")) {
									 this.showSuccessToast(component);
									 this.redirectForProactiveSubmittal(component, event, helper);
									 
								}
								else {
									this.redirectToOpportunityURL(component, event, helper);
								}
                                
								  
                             }else if (response.getState() === "ERROR") {
                                 
                                 component.set('v.isDisabled', false);
                                  var errors = response.getError();
                                  if (errors) {
                                        if (errors[0] && errors[0].message) {
                                            //this.showError(errors[0].message);
                                           component.set("v.errorMessageVal",errors[0].message);
                                        }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                                            //this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                                            component.set("v.errorMessageVal",errors[0].fieldErrors[0].statusCode);
                                        }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                                            //this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                                             component.set("v.errorMessageVal",errors[0].pageErrors[0].statusCode);
                                        }else{
                                            //this.showError('An unexpected error has occured. Please Contact System Administrator!');
                                            component.set("v.errorMessageVal",'An unexpected error has occured. Please Contact System Administrator!');
                                        }
                                      /*
                                        var spinner = component.find('spinner');
                                        var evt = spinner.get("e.toggle");
                                        evt.setParams({ isVisible : false });
                                        evt.fire(); */
                                 }
                             }
                          });
                              component.set('v.isDisabled', true);
                               $A.enqueueAction(actionopp); 
              return ;
        }
                               component.set("v.isDisabled", false);
                              /*
                               var spinner = component.find('spinner');
                                var evt = spinner.get("e.toggle");
                                evt.setParams({ isVisible : false });
                                evt.fire();  */
    },
    
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'sticky',
            title: title,
            message: errorMessage,
            duration:' 5000',
            type: 'error'
        });
        toastEvent.fire();
    },
    
    validateEnterpriseReq : function (component, event, helper) {
        var validated = true;
        
        var recordType= component.get("v.recordType");
        var aname = component.get("v.account.Name");
        var oppClosedate = component.get("v.req.CloseDate");
        var cdatefield = this.findComp(component,"inputCloseDate");
        var cname= component.get("v.contact.Name");
        var stageValue = component.get("v.req.StageName");
        var oppname= component.get("v.req.Name");
        var oppfield = this.findComp(component,"inputOppName");
        var productValue= component.get("v.req.Req_Product__c");
        var termOfEngagementValue= component.get("v.req.Req_Terms_of_engagement__c");
        var currencyValue=component.get("v.req.Currency__c");
        var duratiVarName=component.get("v.req.DurationVarName");
        var durationField=this.findComp(component,"inputDurationName");
        var durationValue=component.get("v.req.Req_Duration__c")
        var durationunitValue=component.get("v.req.Req_Duration_Unit__c");
		var alternatedeliverySharedPosns = component.get("v.req.Req_RRC_Positions__c");
        var totalPositionsValue=component.get("v.req.Req_Total_Positions__c");
        var totalPositionField=this.findComp(component,"inputTotalPosn");
        var jobtitle=component.get("v.req.Req_Job_Title__c");
        var lengthExceededQualification=component.get("v.req.Req_Qualification__c");
     	var requiredSkillPills = component.get("v.req.EnterpriseReqSkills__c");

        var salaryMinValue=component.get("v.req.Req_Salary_Min__c");
        var salaryMinValueField=this.findComp(component,"inputSalaryMin");
        
        var salaryMaxValue=component.get("v.req.Req_Salary_Max__c");
        var salaryMaxValueField=this.findComp(component,"inputSalaryMax");
        
        var bonusValue=component.get("v.req.Req_Bonus__c");
        var bonusField=this.findComp(component,"inputBonus");
        
        var worksitestreetValue=component.get("v.req.Req_Worksite_Street__c");
        var worksitestreetField=this.findComp(component,"inputworkSt");
        
        var worksitecityValue=component.get("v.req.Req_Worksite_City__c");
        var worksitecityField=this.findComp(component,"inputworkCity");
        
        var worksiteStateValue=component.get("v.req.Req_Worksite_State__c");
        var worksiteStateField=this.findComp(component,"inputworkState");
        
        var worksitepostalcodeValue=component.get("v.req.Req_Worksite_Postal_Code__c");
        var worksiteZipField=this.findComp(component,"inputworkZip");
        
        var worksitecountryValue=component.get("v.req.Req_Worksite_Country__c");
        var worksitecountryField=this.findComp(component,"inputworkCountry");
        
        var officeValue= component.get("v.organization.Name");
        var apexContext=false;
        
        var reqTotalFilled=component.get("v.req.Req_Total_Filled__c");
        var reqTotalPositions=component.get("v.req.Req_Total_Positions__c");
        
        var requiredBillRateMax=component.get("v.req.Req_Bill_Rate_Max__c");
        var requiredBillRateMin=component.get("v.req.Req_Bill_Rate_Min__c");
        
        var percentageFee=component.get("v.req.Req_Fee_Percent__c");
        var flatFee=component.get("v.req.Req_Flat_Fee__c");
        
        var jobDescription=component.get("v.req.Description");
        var payrateMax=component.get("v.req.Req_Pay_Rate_Max__c");
        var payrateMin=component.get("v.req.Req_Pay_Rate_Min__c");
        var requiredQualification=component.get("v.req.Req_Qualification__c");
        var reqStandardBurden=component.get("v.req.Req_Standard_Burden__c");
        var rateFrequency=component.get("v.req.Req_Rate_Frequency__c");
        var recordType=component.get("v.recordType");
        
        var whyPosition = component.get("v.req.Req_Why_Position_Open_Details__c");
        var skillDetails = component.get("v.req.Req_Skill_Details__c");
        var workEnv = component.get("v.req.Req_Work_Environment__c");
        var reqEvp = component.get("v.req.Req_EVP__c");
        var addnlInfo = component.get("v.req.Req_Additional_Information__c");
        var reqComp = component.get("v.req.Req_Compliance__c");
        var extJobDesc = component.get("v.req.Req_External_Job_Description__c");
        var busChlge = component.get("v.req.Req_Business_Challenge__c");
        var intExtCust = component.get("v.req.Req_Internal_External_Customer__c");
        var impactExt = component.get("v.req.Req_Impact_to_Internal_External_Customer__c");
        var compInfo = component.get("v.req.Req_Competition_Info__c");
        var appvlProcess = component.get("v.req.Req_Approval_Process__c");
        var intervwInfo = component.get("v.req.Req_Interview_Information__c");
        var altDeliType = component.get("v.req.Req_Delivery_Type__c");
		var altDeliOffice = component.get("v.req.Req_Secondary_Delivery_Office__c");

        var opco=component.get("v.req.OpCo__c");
        var opcoValVar=component.get("v.opcoVarName");

        var businessUnit=component.get("v.req.Req_Division__c");
        var businessUnitValVar=component.get("v.req.businessUnitVarName");
        
        var descriptionId=this.findComp(component,"inputSkillDescription");
        var mainskill=this.findComp(component,"MainSkillId");
        var jobAuraDesc = this.findComp(component,"inputDescription");
        
         var qualfield = this.findComp(component,"inputQualification");
         var inpPosnfield = this.findComp(component,"inputPositiondetails");
         var inpSkillDescfield = this.findComp(component,"inputSkillDescription");
         var workEnvfield = this.findComp(component,"workEnvironment");
         var evpfield = this.findComp(component,"evp");
         var addInfofield = this.findComp(component,"addInfo");
         var ipCompfield = this.findComp(component,"inputCompliance");
         var extCommfield = this.findComp(component,"extCommunities");
         var busChalfield  =  this.findComp(component,"inputbusChallenge");
         var intextfield = this.findComp(component,"intExternal");
         var impactIntfield = this.findComp(component,"impactIntExt");
         var compInfofield = this.findComp(component,"reqCompetitionInfo");
         var approvfield = this.findComp(component,"reqApprovalInfo");
         var IntInfofield = this.findComp(component,"intInfo");
		 var altOfficefield = this.findComp(component, "reqAltDelOffice");
        // Dafaulting values to zero for Req_AtleastOneSubmittal_Reqd_Interview validation rule.
        
        var reqSubInterviewing=0;
        var reqSubOfferExtended=0;
        var reqSubNotProceeding=0;
        var reqSubLPQInProcess=0;
        var reqSubReferening=0;
        var reqSubOnHold=0;
        var reqSubOfferAccepted=0;
        var toastMessages=[];
        
        this.validateAddress(component,event);
        var addfieldMap=component.get("v.addFieldMessagedMap");
        var isTGSReq = component.get("v.req.Req_TGS_Requirement__c");
        var isTGSPracEng = component.get("v.req.Practice_Engagement__c"); //component.find("isTGSPEId").get("v.value");
        var isGSLocation = component.get("v.req.GlobalServices_Location__c"); //component.find("isTGSLocationId").get("v.value");
        var isEmpAlign = component.get("v.req.Employment_Alignment__c"); //component.find("isEmploymentAlignmentId").get("v.value");
        var isFnDsMk = component.get("v.req.Final_Decision_Maker__c"); //component.find("isFinaDecMakId").get("v.value");
        var isIntTvReq = component.get("v.req.Is_International_Travel_Required__c"); //component.find("isInterNationalId").get("v.value");
        var isInternalHire = component.get("v.req.Internal_Hire__c"); //component.find("isInternalHireId").get("v.value");
        var isBackfill = component.get("v.req.Backfill__c");
        var isNational = component.get("v.req.National__c");
        if(typeof addfieldMap!='undefined' && addfieldMap!=null && JSON.stringify(addfieldMap)!="{}"){
            validated = false;
        }
        
        var worksiteStreetValMsg= component.get("v.req.Req_Worksite_Street__c");
        var worksiteCityValMsg = component.get("v.req.Req_Worksite_City__c");
        var worksiteStateValMsg = component.get("v.req.Req_Worksite_State__c");
        var worksitePostalValMsg = component.get("v.req.Req_Worksite_Postal_Code__c");
        var worksiteCountryValMsg = component.get("v.req.Req_Worksite_Country__c");
        
       if(aname == null || typeof aname=="undefined" || aname==''){
            validated = false;
            component.set("v.AccountVarName","Account Name cannot be blank.");
       }else{
            component.set("v.AccountVarName","");
       }
        
        if(component.get("v.req.isOpcoRegular") === "true"){
           if(oppClosedate == null || oppClosedate==''){
              validated = false;
              component.set("v.closeDateVarName","Close Date cannot be blank.");
             }else{
               component.set("v.closeDateVarName","");
             } 
        }

        //S-145401 Validation of Required fields for the PA Submittal Form --- 

        if(cname == null || typeof cname=="undefined" || cname==''){
            validated = false;
            component.set("v.HiringManagerVarName","Hiring Manager cannot be blank.");
        }else{
            component.set("v.HiringManagerVarName","");
        }		
        

        if(stageValue == null || typeof stageValue=="undefined" || stageValue=='' || stageValue == '--None--'){
            validated=false;
            component.set("v.StageVarName","Stage cannot be blank or None.");
        }else{
             component.set("v.StageVarName","");
        }

        // console.log('OpCo value--->'+(typeof opco == "undefined"));

        if(opco=='--None--' || typeof opco == "undefined"){
            validated=false;
            component.set("v.opcoVarName","OpCo cannot be blank.");
        }else{
             component.set("v.opcoVarName","");
        }

        console.log('businessUnit value--->'+(typeof businessUnit == "undefined"));
		
        if(businessUnit=='--None--' || typeof businessUnit == "undefined"){
            validated=false;
            component.set("v.businessUnitVarName","Division cannot be blank.");
        }else{
             component.set("v.businessUnitVarName","");
        }
        if(opco == 'TEKsystems, Inc.'){
            var productPicklistUnit=component.get("v.req.Product__c");
            if(productPicklistUnit=='--None--' || typeof productPicklistUnit == "undefined"){
                validated=false;
                component.set("v.ProductPicklistUnitVarName","Product cannot be blank.");
            }else{
                component.set("v.ProductPicklistUnitVarName","");
            }
        }
          // console.log('OPP ++');
         console.log(oppname);
        if((opco!='Aerotek, Inc' && opco!='TEKsystems, Inc.' && opco!='AG_EMEA') && (oppname == null || oppname=='' || typeof oppname == "undefined")){
            validated = false;
            component.set("v.oppVarName","Opportunity Name cannot be blank.");
        }else{
            component.set("v.oppVarName", "");
        }
        
        if(productValue == '--None--' || typeof productValue == "undefined"){
            validated = false;
            component.set("v.ProductVarName","Placement Type cannot be blank.");
        }else{
            component.set("v.ProductVarName","");
        }
            //console.log('-isTGSReq-'+isTGSReq);
        if(opco == 'TEKsystems, Inc.'){
            if(typeof isTGSReq == "undefined" || isTGSReq == '--None--'){
				validated = false;
                component.set("v.isTGSReqVarName","You must indicate if this is a TGS Requirement.");
            } else {
                component.set("v.isTGSReqVarName", "")
            }
            
            var TGSReqVal = component.find("isTGSReqId").get("v.value");
            /*if(TGSReqVal == '--None--'){
                var CloneVal = component.get("v.isCloneReq");
                if(CloneVal == 'true'){
					validated = false;
                    component.set("v.isTGSReqVarCloneName","You must indicate if this is a TGS Requirement.");
                } else {
                    component.set("v.isTGSReqVarCloneName", "")
                }
            }else{
                component.set("v.isTGSReqVarCloneName", "")
            }*/
            
            if(TGSReqVal == 'Yes'){
                if((typeof stageValue == "undefined" || stageValue == '--None--' || stageValue != 'Draft') && (typeof isTGSPracEng == "undefined" || isTGSPracEng == '--None--')){
                    validated = false;
                    component.set("v.isTGSPEName","Select a Practice Engagement value.");
                }else{
                    component.set("v.isTGSPEName","");
                }
                if(typeof stageValue == "undefined" || stageValue == '--None--' || stageValue !=''){
                    if(typeof isGSLocation == "undefined" || isGSLocation == '--None--'){
                        validated = false;
                        component.set("v.isTGSLocationName","Select a Location value.");
                    }else{
                        component.set("v.isTGSLocationName","");
                    }
                    if(typeof isEmpAlign == "undefined" || isEmpAlign == '--None--'){
                        validated = false;
                        component.set("v.isTGSEmpAlignName","Select an Employment Alignment value.");
                    }else{
                        component.set("v.isTGSEmpAlignName","");
                    }
                    if(typeof isFnDsMk == "undefined" || isFnDsMk == '--None--'){
                        validated = false;
                        component.set("v.isFinaDecMakName","Select a Final Decision Maker value.");
                    }else{
                        component.set("v.isFinaDecMakName","");
                    }
                    if(typeof isIntTvReq == "undefined" || isIntTvReq == '--None--'){
                        validated = false;
                        component.set("v.isIntNationalName","Indicate if International Travel is required.");
                    }else{
                        component.set("v.isIntNationalName","");
                    }
                    if(typeof isInternalHire == "undefined" || isInternalHire == '--None--'){
                        validated = false;
                        component.set("v.isInternalName","Indicate if this is an Internal Hire.");
                    }else{
                        component.set("v.isInternalName","");
                    }
                }
            }else if(TGSReqVal == 'No' || TGSReqVal == '--None--'){
                component.set("v.isTGSTrue", "true");
                component.find("isTGSPEId").set("v.value", '--None--');
                component.set("v.req.Practice_Engagement__c","--None--");
                component.find("isTGSLocationId").set("v.value", '--None--');
                component.set("v.req.GlobalServices_Location__c","--None--");
                component.find("isEmploymentAlignmentId").set("v.value", '--None--');
                component.set("v.req.Employment_Alignment__c","--None--");
                component.find("isFinaDecMakId").set("v.value", '--None--');
                component.set("v.req.Final_Decision_Maker__c","--None--");
                component.find("isInterNationalId").set("v.value", '--None--');
                component.set("v.req.Is_International_Travel_Required__c","--None--");
                component.find("isInternalHireId").set("v.value", '--None--');
                component.set("v.req.Internal_Hire__c","--None--");
                component.set("v.isTGSPEName","");
                component.set("v.isTGSLocationName","");
                component.set("v.isTGSEmpAlignName","");
                component.set("v.isFinaDecMakName","");
                component.set("v.isIntNationalName","");
                component.set("v.isInternalName","");
                component.set("v.isTGSTrue", "false");
            }
        }
        var externalComm=component.get("v.req.Req_External_Job_Description__c");
        if(opco=='TEKsystems, Inc.' && stageValue=='Qualified' && (externalComm==null || externalComm == '' || typeof externalComm == "undefined")){
            validated = false;
            component.set("v.externalCommVarName","External Communities Job Description is required.");
        }else{
            component.set("v.externalCommVarName","");
        }
        
        
        if((opco!='Aerotek, Inc' && opco!='TEKsystems, Inc.' ) &&(termOfEngagementValue == '--None--' || typeof termOfEngagementValue == "undefined")){
            validated = false;
            component.set("v.ReqTermEngagementVarName","Terms of Engagement cannot be blank.");
        }else{
            component.set("v.ReqTermEngagementVarName","");
        }
        
        if(currencyValue == '--None--' || typeof currencyValue == "undefined"){
            validated = false;
            component.set("v.CurrencyVarName","Currency cannot be blank.");
        }else{
            component.set("v.CurrencyVarName","");
        }
        
        if(totalPositionsValue == null || typeof totalPositionsValue == "undefined"){
            validated =false;
            component.set("v.reqTotalVarPositions", "Total Positions cannot be blank.");
        }else{
            component.set("v.reqTotalVarPositions", "");
        }
        
        if(jobtitle == null ||  jobtitle == '' || typeof jobtitle == "undefined"){
            validated = false;
            component.set("v.jobTitleErrorVarName","Job Title cannot be blank.");
        }else{
            component.set("v.jobTitleErrorVarName","");
        }

		let experLevel = component.get("v.req.Req_Job_Level__c");
		if( recordType === 'Req' && stageValue !== 'Draft' && (experLevel === null || experLevel === '' || experLevel === '--None--' || typeof experLevel === 'undefined')) {
			validated = false;
            component.set("v.experLevel","Experience level is required for Qualified and beyond stages.");
		}
		else {
			component.set("v.experLevel","");
		}

	    /*
        if(worksitestreetValue == null || worksitestreetValue=='' ||  typeof worksitestreetValue == "undefined"){
            validated =false; 
           // worksitestreetField.set("v.errors", [{message:"Worksite Street cannot be blank."}]);
           // $A.util.toggleClass(worksitestreetField, 'boldtext');
        }else{
            //worksitestreetField.set("v.errors", null);
        }
        
        if(worksitecityValue == null || worksitecityValue=='' ||  typeof worksitecityValue == "undefined"){
            validated =false; 
            //worksitecityField.set("v.errors", [{message:"Worksite City cannot be blank."}]);
        }else{
            //worksitecityField.set("v.errors", null);
        }
        
        if(worksiteStateValue == null || worksiteStateValue=='' ||  typeof worksiteStateValue == "undefined"){
            validated =false; 
            //worksiteStateField.set("v.errors", [{message:"Worksite State/Providence cannot be blank."}]);
        }else{
             //worksiteStateField.set("v.errors", null);
        }
        
        if(worksitepostalcodeValue == null || worksitepostalcodeValue=='' ||  typeof worksitepostalcodeValue == "undefined"){
            validated =false; 
            //worksiteZipField.set("v.errors", [{message:"Worksite Zip/Postal Code cannot be blank."}]);
        }else{
            //worksiteZipField.set("v.errors", null);
        }
        
        if(worksitecountryValue == null || worksitecountryValue=='' ||  typeof worksitecountryValue == "undefined"){
            validated =false; 
            //worksitecountryField.set("v.errors", [{message:"Worksite Country cannot be blank."}]);
        }else{
             //worksitecountryField.set("v.errors", null);
        }
       */
        if(officeValue == null || officeValue=='' ||  typeof officeValue == "undefined"){
            validated = false;
            component.set("v.organizationVarName","Office cannot be blank.");
        }else{
            component.set("v.organizationVarName","");
        }
        
        var perfExpect= component.get("v.req.Req_Performance_Expectations__c");
        
        if(perfExpect!=null && perfExpect!='' && perfExpect.length>2000){
            // console.log('inside jiobdescripiotn');
             	//descriptionId.set("v.errors", [{message:"Max Length can not exceed 32000"}]);  	
             component.set("v.perFormVarName","Performance Expectation can not exceed 2000.");
            validated = false;
        }else {
            component.set("v.perFormVarName","");
        }
        
        
        var compInfo= component.get("v.req.Req_Competition_Info__c");
        
        if(compInfo!=null && compInfo!='' && compInfo.length>2000){
            component.set("v.competitionVarName","Competition info can not exceed 2000.");
            validated = false;
        }else {
            component.set("v.competitionVarName","");
        }
        
        var reqInt= component.get("v.req.Req_Interview_Information__c");
        
        if(reqInt!=null && reqInt!='' && reqInt.length>2000){
            component.set("v.InterviewInfoVarName","Interview Information can not exceed 2000.");
            validated = false;
        }else {
            component.set("v.InterviewInfoVarName","");
        }
        
        
        //var extJobDesc= component.get("v.req.Req_External_Job_Description__c");
        
        /*if((extJobDesc!=null || extJobDesc!='') && stageValue=='Qualified' && opco=='TEKsystems, Inc.' ){
             component.set("v.extJobDescVarName","External Communities Job Description is required.");
            validated = false;
        }else {
            component.set("v.extJobDescVarName","");
        }*/
        
        var evp= component.get("v.req.Req_EVP__c ");
        
        if(evp!=null && evp!='' && evp.length>2000){
             component.set("v.evpVarName","EVP can not exceed 2000.");
            validated = false;
        }else {
            component.set("v.evpVarName","");
        }
        
        var complianceVarName= component.get("v.req.Req_Compliance__c");
        
        if(complianceVarName!=null && complianceVarName!='' && complianceVarName.length>255){
             component.set("v.complianceVarName","Compliance can not exceed 255.");
            validated = false;
        }else {
            component.set("v.complianceVarName","");
        }
        
        
        
        if(lengthExceededQualification!=null && lengthExceededQualification!='' && lengthExceededQualification.length>2000){
            component.set("v.qualificationLengthVarName","Additional Skills & Qualifications cannot be greater than 2000 characters.");
            qualfield.set("v.errors", [{message:"Additional Skills & Qualifications cannot be greater than 2000 characters"}]);
            validated = false;
        }else {
            component.set("v.qualificationLengthVarName","");
             qualfield.set("v.errors", null);
        }
        
        var startDate=component.get("v.req.Start_Date__c");
        console.log('start date check'+validated);
        if(validated && (stageValue=='Qualified')
          && recordType=='Req' && apexContext==false  && 
          (startDate==null || startDate=='' || typeof startDate == "undefined")){
            component.set("v.startDateValMsg","Start Date is Required");
            validated=false;
        }else{
            component.set("v.startDateValMsg","");
        } 
  
        //Req_Job_Description_Required  validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
           && recordType=='Req' && apexContext==false && 
           (jobDescription==null || jobDescription=='' || typeof jobDescription == "undefined")){
            console.log('desc not given');
            component.set("v.descriptionVarName", "Description is required.");
            validated=false;
        }
        else if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
           && recordType=='Req' && apexContext==false && 
           (jobDescription!=null && jobDescription!='' && typeof jobDescription != "undefined" && jobDescription.length< 25)){
            console.log('desc size less than 25');
            component.set("v.descriptionVarName", "Job Description should have at least 25 characters.");
            validated=false;
        }else{
            component.set("v.descriptionVarName", "");
        }
        
        var pillData = component.get("v.automatchSkills");
        console.log('pill data outside'+JSON.stringify(pillData));
        let istxOpcoEmea=component.get("v.isOpcoEmea");
        console.log('istxOpcoEmea::'+istxOpcoEmea);
        console.log('istxOpcoEmeaValues::'+JSON.stringify(component.get("v.skills")));
        pillData=component.get("v.skills");
        if(istxOpcoEmea!=null && istxOpcoEmea=="true"){
            pillData=component.get("v.skills");
        }
        if(validated   && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
          && recordType=='Req' && apexContext==false && (productValue=='Contract' || productValue=='Contract to Hire' || productValue=='Permanent') && 
          (pillData==null || pillData=='' || typeof pillData == "undefined")){
            console.log('pill data rule '+pillData);
            component.set("v.skillPillsRequiredMsg","Skills are required.");
            validated=false;
        }else{
            component.set("v.skillPillsRequiredMsg","");
        } 
        
        if(validated==false){
            component.set("v.errorMessageVal","Please fill in all mandatory fields /Correct errors.");
        }else{
            //start of c val
             
       var criteriaValidate=true;
       
        //EMEA Validations
        var opcoEmea = component.get("v.isOpcoEmea");
        var teksystemFlag=component.get("v.isOpcoTek");
            if(opcoEmea == 'true' || teksystemFlag==true){
               
                if(whyPosition !=null && whyPosition!='' && whyPosition.length>255){
                  inpPosnfield.set("v.errors", [{message:"Why is Position Open Details cannot be greater than 255 characters"}]);
                  criteriaValidate=false;
                }else {
                  inpPosnfield.set("v.errors", null);
                }
                
                if(skillDetails !=null && skillDetails!='' && skillDetails.length>32000){
                  inpSkillDescfield.set("v.errors", [{message:"Skill Details cannot be greater than 32000 characters"}]);
                  criteriaValidate=false;
                }else {
                  inpSkillDescfield.set("v.errors", null);
                }
                
                if(workEnv !=null && workEnv!='' && workEnv.length>3000){
                  workEnvfield.set("v.errors", [{message:"Work Environment cannot be greater than 3000 characters"}]);
                  criteriaValidate=false;
                }else {
                  workEnvfield.set("v.errors", null);
                }
                
                if(reqEvp !=null && reqEvp!='' && reqEvp.length>2000){
                  evpfield.set("v.errors", [{message:"EVP cannot be greater than 2000 characters"}]);
                  criteriaValidate=false;
                }else {
                  evpfield.set("v.errors", null);
                }
                
                if(addnlInfo !=null && addnlInfo!='' && addnlInfo.length>131702){
                  addInfofield.set("v.errors", [{message:"Additional Information cannot be greater than 131702 characters"}]);
                  criteriaValidate=false;
                }else {
                  addInfofield.set("v.errors", null);
                }
                
                if(reqComp !=null && reqComp!='' && reqComp.length>255){
                  ipCompfield.set("v.errors", [{message:"Compliance cannot be greater than 255 characters"}]);
                  criteriaValidate=false;
                }else {
                  ipCompfield.set("v.errors", null);
                }
                
                if(extJobDesc !=null && extJobDesc!='' && extJobDesc.length>500){
                  extCommfield.set("v.errors", [{message:"External Job Description cannot be greater than 500 characters"}]);
                  criteriaValidate=false;
                }else {
                  extCommfield.set("v.errors", null);
                }
                
                if(busChlge !=null && busChlge!='' && busChlge.length>4000){
                  busChalfield.set("v.errors", [{message:"Business Challenge cannot be greater than 4000 characters"}]);
                  criteriaValidate=false;
                }else {
                  busChalfield.set("v.errors", null);
                }
               
                if(intExtCust !=null && intExtCust!='' && intExtCust.length>2000){
                  intextfield.set("v.errors", [{message:"Who is internal/external Customer cannot be greater than 2000 characters"}]);
                  criteriaValidate=false;
                }else {
                  intextfield.set("v.errors", null);
                }
                
                if(impactExt !=null && impactExt!='' && impactExt.length>2000){
                  impactIntfield.set("v.errors", [{message:"Impact to internal/external Customer cannot be greater than 2000 characters"}]);
                  criteriaValidate=false;
                }else {
                  impactIntfield.set("v.errors", null);
                }
                
                if(compInfo !=null && compInfo!='' && compInfo.length>2000){
                  compInfofield.set("v.errors", [{message:"Competition Info cannot be greater than 2000 characters"}]);
                  criteriaValidate=false;
                }else {
                  compInfofield.set("v.errors", null);
                }
                
                if(appvlProcess !=null && appvlProcess!='' && appvlProcess.length>255){
                  approvfield.set("v.errors", [{message:"Approval Process cannot be greater than 255 characters"}]);
                  criteriaValidate=false;
                }else {
                  approvfield.set("v.errors", null);
                }
                
                if(intervwInfo !=null && intervwInfo!='' && intervwInfo.length>2000){
                  IntInfofield.set("v.errors", [{message:"Interview Information cannot be greater than 2000 characters"}]);
                  criteriaValidate=false;
                }else {
                  IntInfofield.set("v.errors", null);
                }
             
            }
            
            var aerotekflag=component.get("v.isOpcoAerotek");
            
            if(aerotekflag==true){
                if(workEnv !=null && workEnv!='' && workEnv.length>3000){
                  workEnvfield.set("v.errors", [{message:"Work Environment cannot be greater than 3000 characters"}]);
                  criteriaValidate=false;
                }else {
                  workEnvfield.set("v.errors", null);
                }


  				if(busChlge !=null && busChlge!='' && busChlge.length>4000){
                  busChalfield.set("v.errors", [{message:"Business Challenge cannot be greater than 4000 characters"}]);
                  criteriaValidate=false;
                }else {
                  busChalfield.set("v.errors", null);
                }
                
                if(extJobDesc !=null && extJobDesc!='' && extJobDesc.length>500){
                  extCommfield.set("v.errors", [{message:"External Job Description cannot be greater than 500 characters"}]);
                  criteriaValidate=false;
                }else {
                  extCommfield.set("v.errors", null);
                }
			/** Commenting Alternate Delivery validation S-130391
				if(((altDeliType != null && altDeliType == 'SRC') || (altDeliType != null && altDeliType == 'Global Talent Aqn Center')) && (altDeliOffice == null || altDeliOffice=='' || typeof altDeliOffice == "undefined" || altDeliOffice =="--None--")){
				   altOfficefield.set("v.errors", [{message:"Please select an Alternate Delivery Office."}]);
				   criteriaValidate=false;
				 } else{
				    altOfficefield.set("v.errors", null);
				}  **/
            }
            
        
        //Req_AtleastOneSubmittal_Reqd_Interview  validation rule.
        if(validated && (stageValue=='Interviewing' || stageValue=='Closed Won') && apexContext==false &&
          (recordType=='Req' || recordType=='Proactive') && ( reqSubInterviewing>0 || reqSubOfferExtended>0 || reqSubNotProceeding>0 || 
            reqSubLPQInProcess>0 || reqSubReferening>0 || reqSubOnHold>0) ){
            // console.log('Req_AtleastOnePlaced_Required_ClosedPart  validation rule');
           // toastMessages.push('At least one Candidate must be in Interview status before moving to â€˜Interviewingâ€™ stage or above.If you are on the edit page, please save Req in \'Qualified\' stage, change one more Candidates to Interviewing. Then, update stage to \'Interviewing\' or above.');
            component.set("v.StageVarName","At least one Candidate must be in Interview status before moving to â€˜Interviewingâ€™ stage or above.If you are on the edit page, please save Req in \'Qualified\' stage, change one more Candidates to Interviewing. Then, update stage to \'Interviewing\' or above.");
            criteriaValidate=false;
        }else if(validated){
            component.set("v.StageVarName","");
            //criteriaValidate=true;
        }
			/** Commenting Alternate Delivery validation S-130391	
		//AlternateDeliverySharedPositions cannot be greater than Total Positions.
		if(validated && (alternatedeliverySharedPosns > totalPositionsValue) && apexContext==false && recordType=='Req'){
            // console.log('Req_AtleastOnePlaced_Required_ClosedPart  validation rule');
           // toastMessages.push('At least one Candidate must be in Interview status before moving to â€˜Interviewingâ€™ stage or above.If you are on the edit page, please save Req in \'Qualified\' stage, change one more Candidates to Interviewing. Then, update stage to \'Interviewing\' or above.');
            component.set("v.AlernateDelShrdPos","Alternate Delivery Shared Positions cannot be greater than Total Positions");
            criteriaValidate=false;
        }else if(validated){
            component.set("v.AlernateDelShrdPos","");
            //criteriaValidate=true;
        }
		**/
		

        //Req_AtleastOneSubmittal_Reqd_Pend_Start  validation rule.
        if(validated && (stageValue=='Pending Start' || stageValue=='Closed Won') && apexContext==false &&
          (recordType=='Req') && (reqSubOfferAccepted>0 || reqTotalFilled>0) ){
            // console.log('Req_AtleastOneSubmittal_Reqd_Pend_Start  validation rule');
           // toastMessages.push('At least one Candidate must be in Interview status before moving to â€˜Interviewingâ€™ stage or above.If you are on the edit page, please save Req in \'Qualified\' stage, change one more Candidates to Interviewing. Then, update stage to \'Interviewing\' or above.');
            component.set("v.StageVarName","At least one submittal should be in a Interview status before stage can be moved to Interviewing.");
            criteriaValidate=false;
        }else if(validated){
            component.set("v.StageVarName","");
            //criteriaValidate=true;
        }
        
        // console.log('requiredBillRateMax------>'+requiredBillRateMax);
        
        //Req_Bill_Rate_Max_Required validation rule.
        if(validated  && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
          && recordType=='Req' && apexContext==false && (productValue=='Contract' || productValue=='Contract to Hire') && 
          (requiredBillRateMax==null || requiredBillRateMax=='' || typeof requiredBillRateMax == "undefined")){
            // console.log('Req_Bill_Rate_Max_Required validation rule.');
            //toastMessages.push('Bill Rate Max is required.');
           component.set("v.billRateMaxValMsg","Bill Rate Max is required.");
            criteriaValidate=false;
        }else{
            component.set("v.billRateMaxValMsg","");
            //criteriaValidate=true;
        }
        
        //Req_Bill_Rate_Min_Required validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') 
           && apexContext==false && (recordType=='Req' || recordType=='Proactive') && (productValue=='Contract' || productValue=='Contract to Hire') 
           && (requiredBillRateMin==null || requiredBillRateMin=='' || typeof requiredBillRateMin == "undefined") ){
           // console.log('Req_Bill_Rate_Min_Required validation rule'); 
           //toastMessages.push('Bill Rate Min is required.'); 
          component.set("v.billRateMinValMsg","Bill Rate Min is required.");
           criteriaValidate=false;
        }else{
            component.set("v.billRateMinValMsg","");
          // criteriaValidate=true;
        }
        
        var draftReason=component.get("v.req.Req_Draft_Reason__c");
        
         if(validated  && (stageValue=='Draft') && (opco=='Aerotek, Inc' || opco=='TEKsystems, Inc.') && 
            (draftReason==null || draftReason==''||typeof draftReason=="undefined" || draftReason=='--None--')
          && recordType=='Req' && apexContext==false ){
            component.set("v.drafReasonVarName","Draft Reason is required");
            criteriaValidate=false;
        }else{
            component.set("v.drafReasonVarName","");
            //criteriaValidate=true;
        }
        
        //Req_DurationUnit_Required  validation rule.
         if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') 
            && apexContext==false && (recordType=='Req' || recordType=='Proactive') && (productValue=='Contract' || productValue=='Contract to Hire') 
            && (durationunitValue==null || durationunitValue=='' || durationunitValue == "--None--") ){
             // console.log('Req_DurationUnit_Required  validation rule');
             //toastMessages.push('Duration Unit is required.'); 
            component.set("v.durationUnitValMsg","Duration Unit is required.");
            criteriaValidate=false;
         }else{
              component.set("v.durationUnitValMsg","");
           // criteriaValidate=true;
         }
        
        
         //Req_Duration_Required  validation rule.
         if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') 
            && apexContext==false && (recordType=='Req' || recordType=='Proactive') && (productValue=='Contract' || productValue=='Contract to Hire') 
            && (durationValue==null || durationValue=='' || typeof durationValue == "undefined") ){
             // console.log('Req_Duration_Required  validation rule');
            // toastMessages.push('Duration  is required.'); 
             component.set("v.durationVarName","Duration  is required.");
             criteriaValidate=false;
         }else{
             component.set("v.durationVarName","");
            // criteriaValidate=true;
         }
        
        //Req_Fee_Flat_FeePercent_Required validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
          && apexContext==false && recordType=='Req' && productValue=='Permanent' && ((percentageFee==null || percentageFee=='') && 
            (flatFee==null || flatFee=='' || typeof flatFee == "undefined"))){
            // console.log('Req_Fee_Flat_FeePercent_Required validation rule.');
           // toastMessages.push('Flat Fee or Fee % is required.'); 
            component.set("v.flatFeeValMsg","Flat Fee or Fee % is required.");
            criteriaValidate=false;
        }else{
            component.set("v.flatFeeValMsg","");
           // criteriaValidate=true;
        }
        
        //inputDescription
        if(validated && apexContext==false && (jobDescription!=null && jobDescription!='' && jobDescription.length>32000)){
            component.set("v.descriptionVarName","Job Description cannot exceed 32000.");
            criteriaValidate=false;
        }else {
            component.set("v.descriptionVarName","");
        }
        
        //Req_Pay_Rate_Max_Required  validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') &&  apexContext==false
           && recordType=='Req' && (productValue=='Contract' || productValue=='Contract to Hire') && 
            (payrateMax==null || payrateMax=='' || typeof payrateMax == "undefined" )){
             // console.log('Req_Pay_Rate_Max_Required  validation rule.');
             //toastMessages.push('Pay Rate Max is required.');
             component.set("v.payRateMaxValMsg","Pay Rate Max is required.");
             criteriaValidate=false;
        }else{
            component.set("v.payRateMaxValMsg","");
           //  criteriaValidate=true;
        }
        
   		if( component.get("v.req.OpCo__c") == 'AG_EMEA' ||  component.get("v.req.OpCo__c") == 'AG EMEA'){
        
            var insideIR35 = component.get("v.req.inside_IR35__c");
            if( validated && (stageValue=='Qualified'|| stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Closed Lost' || stageValue=='Closed Won' || stageValue=='Closed Won (Partial)' )){
                
                if(insideIR35 == '' || insideIR35 ==null || typeof insideIR35 == 'undefined' ){
                    component.set("v.ReqInsideIRError","Inside IR35 cannot be blank.");
                    criteriaValidate=false;
                    validated=false;
                }
            }else{
                component.set("v.ReqInsideIRError","");
            } 
        }
         
        
        //Req_Pay_Rate_Min_Required  validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') && apexContext==false
           && (recordType=='Req' || recordType=='Proactive') && (productValue=='Contract' || productValue=='Contract to Hire') && 
           (payrateMin==null || payrateMin=='' || typeof payrateMin == "undefined")){
            // console.log('Req_Pay_Rate_Min_Required  validation rule.');
            //toastMessages.push('Pay Rate Min is required.');
            component.set("v.payRateMinValMsg","Pay Rate Min is required.");
            criteriaValidate=false;
        }else{
            component.set("v.payRateMinValMsg","");
          //  criteriaValidate=true;
        }
        
        //Req_Qualification_Required validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') && recordType=='Req'
           && apexContext==false && (requiredQualification==null || requiredQualification=='' || typeof requiredQualification == "undefined")){
            // console.log('Req_Qualification_Required validation rule.');
			//toastMessages.push('Qualification is required.');
			component.set("v.qualificationVarName","Additional Skills & Qualifications are required.");
            criteriaValidate=false;
        }else{
            component.set("v.qualificationVarName","");
          //  criteriaValidate=true;
        }
        
        //Req_Salary_Max_Required validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') && apexContext==false
           && recordType=='Req' && productValue=='Permanent' && (salaryMaxValue==null || salaryMaxValue=='' || typeof salaryMaxValue == "undefined")){
            // console.log('Req_Salary_Max_Required validation rule.');
			//toastMessages.push('Salary Max is required.');  
            component.set("v.salaryMaxValMsg","Salary Max is required.");
            criteriaValidate=false;
        }else{
             component.set("v.salaryMaxValMsg","");
           //  criteriaValidate=true;
        }
        
        //Req_Salary_Min_Required  validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') && apexContext==false
           && recordType=='Req' && productValue=='Permanent' && (salaryMinValue==null || salaryMinValue=='' || typeof salaryMinValue == "undefined")){
            // console.log('Req_Salary_Min_Required  validation rule.');
            //toastMessages.push('Salary Min is required.'); 
            component.set("v.salaryMinValMsg","Salary Min is required.");     
            criteriaValidate=false;
        }else{
            component.set("v.salaryMinValMsg",""); 
          //  criteriaValidate=true;
        }
        
      
        //Req_Standard_Burden_Required  validation rule.
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
          && apexContext==false && recordType=='Req' && apexContext==false && (productValue=='Contract' || productValue=='Contract to Hire') && 
           (reqStandardBurden==null || reqStandardBurden=='' || typeof reqStandardBurden == "undefined")){
            // console.log('Req_Standard_Burden_Required  validation rule.');
            //toastMessages.push('Standard Burden % is required.');
            component.set("v.standardBurdenValMsg","Total Burden % is required.");
            criteriaValidate=false;
        }else {
            component.set("v.standardBurdenValMsg","");
          //  criteriaValidate=true;
        }
        
		
		var segVar=component.find("segmentId");
        var jobVar=component.find("JobcodeId");
        var catVar=component.find("CategoryId");
		if(validated && apexContext==false && recordType=='Req'   && 
           (opco=='TEKsystems, Inc.' || opco=='Aerotek, Inc') && (
             (businessUnit=='--None--' || typeof businessUnit == "undefined") || (mainskill!=null && (mainskill.get("v.value")=='--None--' || typeof mainskill.get("v.value")=="undefined" ) )
           || ( segVar!=null && (segVar.get("v.value")=='--None--' || typeof segVar.get("v.value") == "undefined" )) || (jobVar!=null && (jobVar.get("v.value")=='--None--' || typeof jobVar.get("v.value") == "undefined")) 
          || (catVar!=null && (catVar.get("v.value")=='--None--' || typeof catVar.get("v.value") == "undefined"))                                                                                                    
            )){
            // console.log('Req_Standard_Burden_Required  validation rule.');
            component.set("v.productHireValMsg","Please complete product hierarchy.");
            criteriaValidate=false;
        }else{
            component.set("v.productHireValMsg","");
            //criteriaValidate=true;
        }
        
      
        //Req_RateFrequency_Required validation rule.
        if(validated && apexContext==false
           && (recordType=='Req' || recordType=='Proactive') && (opco!='TEKsystems, Inc.' && opco!='Aerotek, Inc')   &&
           (productValue=='Contract' || productValue=='Contract to Hire') && ((rateFrequency=='--None--' || typeof rateFrequency == "undefined") && 
             ((requiredBillRateMax!=null  || requiredBillRateMax>0 )
          || (requiredBillRateMin!=null  ||  requiredBillRateMin>0 ) || 
          (payrateMin!=null  || payrateMin>0 ) || (payrateMax!=null || payrateMax>0)))){
           component.set("v.rateFrequencyValMsg","Rate Frequency is Required.");
            criteriaValidate=false;
        }else {
            component.set("v.rateFrequencyValMsg","");
        }
        
        if(validated && apexContext==false && oppname != null && oppname !='' && oppname.length > 120){
            component.set("v.oppVarName","Max length can not exceed 120.");
            criteriaValidate = false;
         }else {
             if(validated){
            	component.set("v.oppVarName","");
             }
        }
        
        console.log('RATE FREQ '+rateFrequency);
        if(validated && apexContext==false && (opco!='TEKsystems, Inc.' && opco!='Aerotek, Inc')   && (recordType=='Req' || recordType=='Proactive') && ((stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Closed Won'))
          && (productValue=='Contract' || productValue=='Contract to Hire') && (rateFrequency=='--None--' || typeof rateFrequency == "undefined")){
            console.log('RF Val RUle.'+validated + 'rate freqw'+rateFrequency);
            component.set("v.rateFrequencyValMsg","Please Select Rate Frequency");
            criteriaValidate=false;
        }else if(criteriaValidate){
            component.set("v.rateFrequencyValMsg","");
        }
            if(criteriaValidate==false){
        	 component.set("v.errorMessageVal","Please fill in required information.");
           }
            //End of criteria val
        }
       
        return (validated && criteriaValidate);
        
    },
    
    showErrorToast : function(message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'You\'ve missed something while creating the req.',
            message: message,
            messageTemplate: '',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'sticky'
        });
        toastEvent.fire();
    },

    showSuccessToast : function(component) {
		let msg = '';
		if(component.get("v.isOpcoTek") || component.get("v.isOpcoAerotek")) {
			msg =  "New opportunity successfully created. Please allow 15 minutes before creating an ESF.";
		}
		else {
			msg = "New opportunity successfully created."
		}
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Success!",
			"message": msg,
			"duration": 7000,
			"type": 'success',
		});
		toastEvent.fire();
    },

    topSkillsValidation : function(component,event){
       // console.log('handled');
        var isFavSkillsSelected = event.getParam("isSelectionDone");
        var isErrorShown = component.get("v.istopSkillsMessage");
        // console.log('event param'+isFavSkillsSelected);
        if(isFavSkillsSelected === true){
            // console.log('valid one');
            component.set("v.istopSkillsMessage", "true");
            console.log(component.get("v.automatchSkills"));
        }
    },
    
    redirectToOpportunityURL : function (component, event, helper) {
		let optyUrl;
		let proactiveSubmittal = component.get("v.proactiveSubmittal");
        event.preventDefault();
		
        optyUrl = "/lightning/r/Opportunity/"+component.get("v.newOpportunityId")+"/view";
		if(proactiveSubmittal !== null && proactiveSubmittal !== undefined) {
			optyUrl = "/lightning/r/Contact/"+component.get("v.proactiveSubmittal.ShipToContactId")+"/view";
		}
        let urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": optyUrl,
			  "isredirect": true
            });
            urlEvent.fire();
     },
    
    redirectToStartURL : function (component, event, helper){
        var objAPI =  component.get("v.sobjType");
		var isTgsreq = component.get("v.isTgsParentReq");
		 var strtUrl;
         var soppid=component.get("v.soppId");
        	
        console.log('soppid---->'+soppid);
		 console.log('start obj v1 '+objAPI);
         event.preventDefault();

		 if(isTgsreq){
		   strtUrl ="/lightning/r/Opportunity/"+component.get("v.req.Req_Opportunity__c")+"/view";
		 }else if(objAPI === 'Account'){
		   strtUrl ="/lightning/r/Account/"+component.get("v.recordId")+ "/view";
		 }else if(objAPI === 'Contact'){
		   strtUrl ="/lightning/r/Contact/"+component.get("v.recordId")+"/view";
		 }else{
		    strtUrl ="/lightning/r/Opportunity/"+component.get("v.recordId")+"/view";
		 }
        
        var soppid=component.get("v.soppId");
        if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){ //Strategic Opportunity redirection
            strtUrl ="/lightning/r/Opportunity/"+soppid+"/view";
        }
		 
             var urlEvent = $A.get("e.force:navigateToURL");
             urlEvent.setParams({
                 "url": strtUrl,
				 "isredirect":true
             });
         urlEvent.fire();
    },
    
    
    loadBusinessUnitValues : function(component,event,opcoValue){
        var opcoVal;
        
        if(opcoValue=='None'){
        	opcoVal = event.getSource().get("v.value"); 
        }else{
            opcoVal=opcoValue;
        }
        
        var opco = component.get("v.req.OpCo__c");
        if(opco=='Aerotek, Inc'){
            component.set("v.isOpcoAerotek",true);
            component.set("v.isOpcoEmea", "false");
            component.set("v.isOpcoRegular", "false");
            component.set("v.isOpcoTek","false");
         }else if(opco=='TEKsystems, Inc.'){
            component.set("v.isOpcoTek",true);
            component.set("v.isOpcoEmea", "false");
            component.set("v.isOpcoRegular", "false");
            component.set("v.isOpcoAerotek","false");
         }else if(opco=='AG_EMEA' || opco=='AG EMEA'){
            component.set("v.isOpcoEmea", "true");
            component.set("v.isOpcoRegular", "false");
            component.set("v.isOpcoTek",false);
            component.set("v.isOpcoAerotek",false);
         }else{
            component.set("v.isOpcoRegular", "true");
            component.set("v.isOpcoEmea", "false");
            component.set("v.isOpcoTek","false");
            component.set("v.isOpcoAerotek","false");
        }
        
        var aerotekArray = [];
        var tekArray = [];
        var allegisArray = [];
        var agsArray = [];
        var apArray = [];
        var mlaArray = [];
        
        component.set("v.MainSkillProductid",null);
        
        
        var busUnitMap = [];
        if(opcoVal=='Aerotek, Inc'|| opcoVal=='TEKsystems, Inc.'){
            component.set("v.MainskillOpco",true);
            
            
        }
        else{
            component.set("v.MainskillOpco",false);
        }
        
        
        var opArray=component.get("v.opcoDivisionList");
        
        // console.log('Load Business Unit Values--->'+opArray);
        
        for (var i=0; i<opArray.length; i++) {
            
            /*console.log('Values from dependent picklists-->'+opArray[i].value);
            console.log('Values from dependent picklists-->'+opArray[i].key);
            console.log('OPCO String--->'+opcoVal);
            console.log('opcoVal-->'+opArray[i].key ==opcoVal);*/
            
            if(opcoVal=='AG_EMEA'){
                opcoVal='AG EMEA';
            }
            
            if(opArray[i].key ==opcoVal){
                var res=opArray[i].value;
                var spltArry=res.toString().split(",");
                /*console.log('Split Result is--->'+res);
                console.log('Split array is--->'+spltArry);*/
                for(var j=0;j<spltArry.length;j++){
                    if(res[j] != 'VMS')
                    {
                    busUnitMap.push(res[j]);
                }
                }
            }
          //  break;
        }
        
          var busUnitArray = [];
                busUnitArray.push({value:'--None--', key:'--None--'});
                for ( var key in busUnitMap) {
                   if(busUnitMap[key]!='' || typeof busUnitMap[key]!='undefined')
                    busUnitArray.push({value: busUnitMap[key], key:busUnitMap[key]});
                }
          
		  /*
		  var oppBusinessUnitList = busUnitArray;
                     oppBusinessUnitList.sort(function compare(a,b){
                                  if (a.key > b.key)
                                    return -1;
							      if (a.key < b.key)
                                    return 1;
                                  return 0;
                                });

				oppBusinessUnitList.reverse();
				*/
                //component.set("v.opcoBusDiviList", oppBusinessUnitList);
               component.set("v.businessUnitList", busUnitArray);
                var busUnitId = this.findComp(component,"businessUnitId");
                
                if(busUnitId !=''  || typeof busUnitId!='undefined'){
                    busUnitId.set("v.value", '--None--');
                }
               
        
    },
    validateAddress:function(cmp,event){
        var oppAddress=cmp.get("v.req");
        var fieldMessages=[];
        var addFielMessagedMap={};
        var opcoName=oppAddress.OpCo__c;
        var OpCoValue = cmp.get("v.req.OpCo__c");        
        if(OpCoValue == 'TEKsystems, Inc.'){
			var TGSReqValue = cmp.find("isTGSReqId").get("v.value");
            if(TGSReqValue == 'No' || TGSReqValue == '--None--'){
                if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
                    // cmp.set("v.streetErrorVarName","Street cannot be blank.");
                    // fieldMessages.push("Street cannot be blank.");
                    addFielMessagedMap['streetErrorVarName']="Street cannot be blank.";
                }else{
                    cmp.set("v.streetErrorVarName","");
                }                                
                if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
                    // cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
                    // fieldMessages.push("City cannot be blank.");
                    addFielMessagedMap['cityTypeErrorVarName']="City cannot be blank.";
                }else{
                    cmp.set("v.cityTypeErrorVarName","");
                }                
                var countryManFlag=false;
                if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
                    //cmp.set("v.countryVarName","Country cannot be blank.");
                    //fieldMessages.push("Country cannot be blank.");
                    //addFielMessagedMap.set("countryVarName","Country cannot be blank.");
                    addFielMessagedMap['countryVarName']="Country cannot be blank.";
                }else{
                    countryManFlag=true;
                    cmp.set("v.countryVarName","");
                }                                
                if((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && countryManFlag && (oppAddress.Req_Worksite_Country__c=='United States' || oppAddress.Req_Worksite_Country__c=='Canada')) {
                    //cmp.set("v.stateErrorVarName","State cannot be blank.");
                    //fieldMessages.push("State cannot be blank.");
                    addFielMessagedMap['stateErrorVarName']="State cannot be blank.";
                }else{
                    cmp.set("v.stateErrorVarName","");
                }                                                
                if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
                    //cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
                    //fieldMessages.push("Postal Code cannot be blank.");
                    //addFielMessagedMap.set("zipErrorVarName","Postal Code cannot be blank.");
                    addFielMessagedMap['zipErrorVarName']="Postal Code cannot be blank.";
                }else{
                    cmp.set("v.zipErrorVarName","");
                }                                
                if(typeof addFielMessagedMap!='undefined' && addFielMessagedMap!=null && addFielMessagedMap.length>0){
                    cmp.set("v.insertFlag",true);
                    // cmp.set("v.addFieldMessage",fieldMessages);
                    cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
                    var testmap=cmp.get("v.addFieldMessagedMap");
                    console.log('Field Map--->'+testmap);
                }else{
                    cmp.set("v.insertFlag",false);
                    //cmp.set("v.addFieldMessage",fieldMessages);
                    cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
                }
            }else if(TGSReqValue == 'Yes'){
                cmp.set("v.insertFlag",false);
                //cmp.set("v.addFieldMessage",fieldMessages);
                cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
            }
        }else if(OpCoValue != 'TEKsystems, Inc.'){
            if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
                // cmp.set("v.streetErrorVarName","Street cannot be blank.");
                // fieldMessages.push("Street cannot be blank.");
                addFielMessagedMap['streetErrorVarName']="Street cannot be blank.";
            }else{
                cmp.set("v.streetErrorVarName","");
            }                        
            if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
                // cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
                // fieldMessages.push("City cannot be blank.");
                addFielMessagedMap['cityTypeErrorVarName']="City cannot be blank.";
            }else{
                cmp.set("v.cityTypeErrorVarName","");
            }            
            var countryManFlag=false;
            if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
                //cmp.set("v.countryVarName","Country cannot be blank.");
                //fieldMessages.push("Country cannot be blank.");
                //addFielMessagedMap.set("countryVarName","Country cannot be blank.");
                addFielMessagedMap['countryVarName']="Country cannot be blank.";
            }else{
                countryManFlag=true;
                cmp.set("v.countryVarName","");
            }                        
            if((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && countryManFlag && (oppAddress.Req_Worksite_Country__c=='United States' || oppAddress.Req_Worksite_Country__c=='Canada')) {
                //cmp.set("v.stateErrorVarName","State cannot be blank.");
                //fieldMessages.push("State cannot be blank.");
                addFielMessagedMap['stateErrorVarName']="State cannot be blank.";
            }else{
                cmp.set("v.stateErrorVarName","");
            }                                    
            if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
                //cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
                //fieldMessages.push("Postal Code cannot be blank.");
                //addFielMessagedMap.set("zipErrorVarName","Postal Code cannot be blank.");
                addFielMessagedMap['zipErrorVarName']="Postal Code cannot be blank.";
            }else{
                cmp.set("v.zipErrorVarName","");
            }                        
            if(typeof addFielMessagedMap!='undefined' && addFielMessagedMap!=null && addFielMessagedMap.length>0){
                cmp.set("v.insertFlag",true);
                // cmp.set("v.addFieldMessage",fieldMessages);
                cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
                var testmap=cmp.get("v.addFieldMessagedMap");
                console.log('Field Map--->'+testmap);
            }else{
                cmp.set("v.insertFlag",false);
                //cmp.set("v.addFieldMessage",fieldMessages);
                cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
            }
        }
    },
	redirectForProactiveSubmittal : function(component, event, helper) {
		if(component.get("v.source") === 'skuid') {
			this.redirectToOpportunityURL(component, event, helper);
		}
		else {
			let promise = component.get('v.modalPromise');
			promise.then(
				function (modal) {
					//var evt = component.getEvent("statusCanceledEvt");
					//evt.fire();
					modal.close();
				}
			);
		}
	}, 
})