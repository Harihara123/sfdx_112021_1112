({
	doInit : function(component, event, helper) {
	   helper.initializeEnterpriseReqModal(component);
	},
    onOpcoChange : function(component, event, helper) {
       helper.loadBusinessUnitValues(component,event,'None');
	},
    
    toggleRedZone : function(component, event, helper) {
        
        var selectValue = component.find("stageId").get("v.value");
     
        if(selectValue != 'Draft'){
            component.set("v.disable_red_zone", false);
            component.find("redzoneCheck").set("v.value", false);
        }else{
            component.set("v.disable_red_zone", true);
            component.find("redzoneCheck").set("v.value", false);
        }
      	    
    },
    //Adding w.r.t S-228902 primaryBuyer w.r.t Req_division    
    onBusinessunitChange : function(component, event, helper) {
       //helper.EnterpriseReqSegmentModal(component,event);       
        //component.set("v.req.Req_Skill_Specialty__c","--None--");
        if(component.get("v.req.Req_Division__c") != '--None--' && component.get("v.req.Req_Division__c") != undefined){
            helper.EntSkillSpecialtyModal(component, event, helper);
        }else if(component.get("v.req.Req_Division__c") == '--None--'){
            helper.SkillSpecialtyDependsDivision(component, event,null);
        }
        //Adding w.r.t S-235321 PrimaryBuyer dependent on division
        if(component.get("v.req.Req_Division__c") != undefined){
            var model=component.get("v.EnterpriseReqInitializedModel");
            helper.PrimaryBuyerDependsDivision(component, event, model);
        }
        //End of Adding w.r.t S-235321        
	},
    //End of Adding S-228902
    onSegmentChange : function(component, event, helper) {
       helper.EnterpriseReqJobcodeModal(component,event);
	},
    onJobCodeChange : function(component, event, helper) {
       helper.EnterpriseReqCategoryModal(component,event);
	},
    onCategoryChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillModal(component,event);
	},
    
    onMainSkillChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillid(component,event);
	},
    addAltRow : function(component, event, helper) {
        helper.addAltRecord(component, event);
    },      
    removeAltRow: function(component, event, helper) {
        var supportRequestList = component.get("v.supportRequestList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        supportRequestList.splice(index, 1);
        component.set("v.supportRequestList", supportRequestList);
    },    
    onAltDeliveryChange : function(component, event, helper) {
      var selectedOp = component.find("altDeliveryList");
        //console.log("Alternate Delivery ",selectedOp.get("v.value"));
       if(selectedOp.get("v.value") === "Yes"){
          component.set("v.altDeliveryVisibility","true");  
        }
    },   
    
    onSkillsValidated : function(component, event, helper) {
       helper.topSkillsValidation(component,event);
	},
    onProductPicklistChange : function(component, event, helper) {
    	component.set("v.ProductVarNamePicklist", "testing");
    },
    onProductChange : function(component, event, helper) {
        helper.onProductChanges(component,event,helper);   
    },
    onOpCoChange :function (component,event,helper){
      var selected = component.get("v.req.OpCo__c");  
      // console.log(selected);
       
        if(selected === "Aerotek, Inc"){
            
        }else if(selected === "TEKsystems, Inc."){
            
        }
        
    },
    onTopSkillsReset : function(component, event, helper) {
       // console.log('reset flag skills');
        component.set("v.istopSkillsMessage", "false");
	},
    
    //Save Enterprise Req
    saveEnterpriseReqOpp : function(component, event, helper){
        
        var reqValidator 	= component.find('validator');
        var validation 		= reqValidator.validateRequiredFields(component.find("stageId").get("v.value"),component.find("productId").get("v.value"));
        //validateRequiredFields(cmp.get("v.StageName"),cmp.get("v.placeType"))
        
        //validation.error_count
        //validation.isvalid
        
        component.set("v.validationResponse", validation );
        component.set("v.isDisabled", true);
        component.set("v.errorMessageVal","");
        helper.saveEnterpriseReq(component, event, helper );
        
    },
    
    cancelAction : function(component, event, helper){
        component.set("v.errorMessageVal","");
		if(component.get("v.isProactiveSubmittal")) {
			helper.redirectForProactiveSubmittal(component, event, helper);
		}
		else {
			helper.redirectToStartURL(component, event, helper);
		}
    },
     reInit : function(component, event, helper) {
        //console.log('reinit fired 2');
        $A.get('e.force:refreshView').fire();
    },
    handleChangeJobTitle : function(component, event, helper) {
        //console.log('Handle Change Job Title');
		let jobTitle = component.get('v.req.Req_Job_Title__c');
        component.set('v.suggestedjobTitle',jobTitle);	
        //S-233946 starts                          			        
		helper.skillSpecialtyClassifierCall(component, event);        
        //S-233946 ends        
    },
    onTGSChange : function(component, event, helper){
        var OpCoValue = component.get("v.req.OpCo__c");
        var TGSReqValue = component.find("isTGSReqId").get("v.value");
        if(OpCoValue == 'TEKsystems, Inc.' && TGSReqValue == 'Yes'){
            component.set("v.isTGSTrue", "true");
        }else if(OpCoValue == 'TEKsystems, Inc.' && (TGSReqValue == 'No' || TGSReqValue == '--None--')){
			component.set("v.isTGSTrue", "true");
			component.find("isBackfillId").set("v.value", 'false');
            component.find("isNationalId").set("v.value", 'false');
			component.find("isTGSPEId").set("v.value", '--None--');
            component.set("v.req.Practice_Engagement__c","--None--");
            component.find("isTGSLocationId").set("v.value", '--None--');
            component.set("v.req.GlobalServices_Location__c","--None--");
            component.find("isEmploymentAlignmentId").set("v.value", '--None--');
            component.set("v.req.Employment_Alignment__c","--None--");
            component.find("isFinaDecMakId").set("v.value", '--None--');
            component.set("v.req.Final_Decision_Maker__c","--None--");
            component.find("isInterNationalId").set("v.value", '--None--');
            component.set("v.req.Is_International_Travel_Required__c","--None--");
            component.find("isInternalHireId").set("v.value", '--None--');
            component.set("v.req.Internal_Hire__c","--None--");
			component.set("v.isTGSPEName","");
            component.set("v.isTGSLocationName","");
            component.set("v.isTGSEmpAlignName","");
            component.set("v.isFinaDecMakName","");
            component.set("v.isIntNationalName","");
            component.set("v.isInternalName","");
            component.set("v.isTGSTrue", "false");
        }        
    },
    
    onInsideIR35Change : function(component, event, helper) {
      var selectedOp = component.find("insideIR35");
      component.set("v.req.inside_IR35__c", selectedOp.get("v.value"));  
    },
    
    dateChange : function(component, event, helper) {
       // alert(event.targetId);
        var dateSource = event.getSource();
		var dateFieldId = dateSource.getLocalId();
        var dateField = component.find(dateFieldId);
        var dateValue = dateField.get("v.value");
       
        var cmpTarget = '';
        if(dateFieldId == 'inputOpenPositionDate'){
            cmpTarget = component.find('label-dateOpnPos');
        }else if(dateFieldId == 'inputStartDate'){
            cmpTarget = component.find('label-start-date');
        }else if(dateFieldId == 'inputExpInterview'){
            cmpTarget = component.find('label-inputExpInterview');
        }
       
        var matches = /^(\d{4})[-\/](\d{1,2})[-\/](\d{1,2})$/.exec(dateValue);
       
        if (matches == null){
            $A.util.addClass(cmpTarget, 'boldtext slds-text-color_error');
            $A.util.removeClass(cmpTarget, 'slds-form-element__label');
        } else{
            $A.util.removeClass(cmpTarget, 'boldtext slds-text-color_error');
            $A.util.addClass(cmpTarget, 'slds-form-element__label');
        }
    },
    onStageChange : function(component, event, helper){
        var reqValidator 	= component.find('validator');
    	reqValidator.validateRequiredEditFields(component.find("stageId").get("v.value"),component.find("productId").get("v.value"));
    },
    
  handleTopSkillsChange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.topSkills', data.skills)
      const secSkills = cmp.get('v.secondarySkills');
      const dupes = data.skills.filter(skill => secSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.topSkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
      cmp.set('v.topSkills', data.skills)
      h.constructFinalSkills(cmp)      
      h.skillSpecialtyClassifierCall(cmp,e);
    },
    handleSecSkillsChange: function(cmp, e, h) {
        const data = e.getParams();
        const topSkills = cmp.get('v.topSkills');
        const dupes = data.skills.filter(skill => topSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                const suggestedSkills = cmp.get('v.simpleSuggestedSkills')
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.secondarySkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
        cmp.set('v.secondarySkills', data.skills)
        h.constructFinalSkills(cmp)
    },
    handlesuggestedskillschange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.simpleSuggestedSkills', data.pills);
    },
    setSuggestedSkills: function(cmp, e, h) {
        const suggestedSkills = cmp.get('v.suggestedSkills');
        if (suggestedSkills) {
            const suggestedSkillsList = suggestedSkills.map(skill => skill.name);
            cmp.set('v.simpleSuggestedSkills', suggestedSkillsList);
        }
    },
    handleDupeMsg: function(cmp, e, h) {
        h.showToast(`${e.getParams().val} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')        
    },  
    handleSuggestedTitlesChange:function(cmp,event,helper) {
		
		const data = event.getParams();
		cmp.set("v.isUpdatedRelatedTitles",true);
		cmp.set("v.updatedSuggestedJobTitles",data.skills);	
		//call suggested skills api
		if(!$A.util.isUndefined(data.skills) || !$A.util.isEmpty(data.skills)) {
			cmp.set("v.suggTitlesForSkills",data.skills.join());                        
            helper.skillSpecialtyClassifierCall(cmp,event);            
		}	
	},
	changeTitleHandler:function(cmp,event,helper) {
		let jobTitle = cmp.get('v.suggestedjobTitle');
		let reqOpco = cmp.get("v.req.OpCo__c");
       
		if(jobTitle != null) {
			//Related titles
			var relatedTitles = cmp.find("relatedTitles");
            if(relatedTitles != undefined){
			relatedTitles.fetchRelatedTerms(jobTitle,"AG_EMEA");
			}else{
                $A.get('e.force:refreshView').fire();	
            }
		}
	},
    destroyComp: function(component, event, helper) {
        if(component.isRendered()==true && component.find('container')!= undefined){
            component.find('container').destroy();
        }        
    },
	relatedTitleHandler: function(cmp,event,helper) {
		console.log('suggJobTitles'+cmp.get('v.suggJobTitles'));
		let titles = cmp.get('v.suggJobTitles')
		if(titles != []) {
			
			cmp.set("v.suggTitlesForSkills",titles.join());
			
		}
	},
    setSkills : function(cmp,event,helper) {
        const allSkills = cmp.get('v.skills');
        cmp.set('v.allSelectedSkills', allSkills);
    },
    onJobDescChange : function(component, event, helper){
		var jobTitle = component.get("v.req.Req_Job_Title__c");
        var jobDesc = component.get("v.req.Description");
        //S-233946 starts
		if(typeof jobDesc != undefined && jobDesc != null && jobDesc != ""){			
            helper.skillSpecialtyClassifierCall(component, event);
		}        
        //S-233946 ends
	},
    onReqQualificationsChange : function(component, event, helper){
		var reqQualifications = component.get("v.req.Req_Qualification__c");
        if(typeof reqQualifications != 'undefined' && reqQualifications != null && reqQualifications != ""){            
			helper.skillSpecialtyClassifierCall(component, event);
		}        
	},
    //Adding w.r.t Story S-234527
    SSblured: function(component, event, helper){        
        helper.ManuallyUpdatedFields(component, event, helper,"Req_Skill_Specialty__c");        
    },
    FNFblured: function(component, event, helper){        
        helper.ManuallyUpdatedFields(component, event, helper,"Functional_Non_Functional__c");        
    },
    EXPblured: function(component, event, helper){        
        helper.ManuallyUpdatedFields(component, event, helper,"Req_Job_Level__c");         
    },
    SkillSetblured: function(component, event, helper){        
        helper.ManuallyUpdatedFields(component, event, helper,"Skill_Set__c");        
    }
	//End of Adding w.r.t Story S-234527
})