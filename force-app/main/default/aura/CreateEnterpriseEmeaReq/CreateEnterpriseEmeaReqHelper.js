({
    initializeEnterpriseReqModal : function(component,event,helper){
        
        var action = component.get("c.getEnterpriseReqPicklists");
        var UrlQuery = window.location.href;
        
        var myPageRef = component.get("v.pageReference");
        var tgsId ='';
        var oppId;

        //TFO Clone Changes Start
        let TFOFlag = '';
        let TFOacc = '';
        let TFOcon = ''; 
        let TFOaddr = '';       
        //TFO Clone Changes ends
        
        if(myPageRef){
            oppId  = myPageRef.state.c__recId;
            tgsId = myPageRef.state.c__tgsOppId;

            //TFO Changes start
            TFOFlag = myPageRef.state.c__TFOFlag;
            TFOacc = myPageRef.state.c__accId;
            TFOcon = myPageRef.state.c__conId;   
            TFOaddr = myPageRef.state.c__address;              
            if(TFOFlag) {
                component.set("v.TFOFlag", true);
            }
            //TFO changes end

            // Code freeze fix
            var paSubStatus = myPageRef.state.c__proactiveSubmittalStatus;
            if(paSubStatus!=undefined){
                component.set("v.proactiveSubmittalStatus",paSubStatus);
            } // Code freeze fix
            if(oppId !== null && oppId !== undefined){
                component.set("v.recordId", oppId);
            }
            if(myPageRef.state.c__source !== null && myPageRef.state.c__source !== undefined) {
                component.set("v.source", myPageRef.state.c__source);
            }
        }
        component.set("v.workAddressFlag",true);
        var recrdId = component.get("v.recordId");
        //console.log('OPP NEW ID'+recrdId);
        //console.log('TGS ID'+tgsId);
        action.setParams({"recId":recrdId,"tgsOppId":tgsId});
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();

                component.set("v.EnterpriseReqInitializedModel",model);               
                component.set("v.isPGMember", model.isPGMember);
                //TFO changes start
                if(TFOFlag) {
                    if(TFOacc && TFOcon) {
                        component.set("v.account.Id",TFOacc.Id);
                        component.set("v.account.Name",TFOacc.Name);
                        component.set("v.contact.Id",TFOcon.Id);
                        component.set("v.contact.Name",TFOcon.Name);
                    }
                    else {
                        component.set("v.account.Id","");
                        component.set("v.account.Name","");
                        component.set("v.contact.Id","");
                        component.set("v.contact.Name","");
                        component.set("v.req.Req_Worksite_Street__c","" );
                        component.set("v.req.Req_Worksite_City__c","" );
                        component.set("v.req.Req_Worksite_State__c","" );
                        component.set("v.req.Req_Worksite_Postal_Code__c","" );
                        component.set("v.req.Req_Worksite_Country__c","" );                        
                    }
                } else {
                    component.set("v.account",model.act);
                    component.set("v.contact",model.cnt);
                }                
                //component.set("v.account",model.act);
                //component.set("v.contact",model.cnt);
                //TFO changes end
                
                component.set("v.organization",model.office);
                component.set("v.sobjType",model.sobjType);
                component.set("v.skillFlag",model.isSkillVisible);
                
                
                var selected = component.get("v.req.Req_Product__c");
                var stage = component.get("v.req.StageName"); 
				
                //Opp is Cloned
                if(model.opp != null){
					stage = 'Draft';
                    //console.log('opp is clone');
                    component.set("v.req",model.opp); 
                    component.set("v.isCloneReq","true");
                    var regex= new RegExp('/^[^a-zA-Z]*$/');
                    var epSkills = model.opp.EnterpriseReqSkills__c;
                    if(regex.test(epSkills)){
                        component.set("v.skills",epSkills);
                    }
                    component.set("v.req.Description",model.opp.req_job_description__c);
                    component.set("v.reqVMS",(model.opp.Req_VMS__c && model.opp.StageName=='Staging'));
                    component.set("v.req.Req_VMS__c","false");
                    component.set("v.req.Req_Merged_with_VMS_Req__c","false");

                    //TFO changes start
                    if(TFOFlag) {                        
                        model.opp.Originating_Opportunity__c=oppId;
                    } else {
                        model.opp.Originating_Opportunity__c=null;
                    }
                    //TFO changes end
                    
                    var eclt = model.endClient;
                    if(eclt == null || typeof eclt=="undefined" || eclt==''){
                        //console.log('which opco '+model.opp.OpCo__c);
                    }else{
                        //console.log('end client '+eclt);
                        component.set("v.req.Req_End_Client__c",eclt.Id);
                        component.set("v.endClientName",eclt.Name);
                    }
                    
                    //TFO changes start                    
                    if(TFOFlag && !model.opp.OpCo__c){
                        model.opp.OpCo__c = 'AG_EMEA';                        
                    }                                            
                    //TFO changes end

                    //Display Opco Specific Template
                    if(model.opp.OpCo__c === "AG_EMEA"){
                        component.set("v.isOpcoEmea", "true");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoAerotek","false");
                        component.set("v.isOpcoTek","false");
                        this.skillSpecValues(component, event,model);
                        this.FunctionalValues(component,event,model);
                        this.skillSetValues(component,event,model);
                    }else if(model.opp.OpCo__c === "Allegis Group, Inc." || model.opp.OpCo__c === "Allegis Global Solutions, Inc." || 
                        model.opp.OpCo__c === "Allegis Partners, LLC" || model.opp.OpCo__c === "Major, Lindsey & Africa, LLC"){
                        component.set("v.isOpcoRegular", "true");
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoAerotek","false");
                        component.set("v.isOpcoTek","false");
                    }else if(model.opp.OpCo__c=='Aerotek, Inc'){
                        component.set("v.isOpcoAerotek",true);
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoTek","false");
                        var reqOPTArry = [];
                        var optMap = model.ReqOptMappings;
                        reqOPTArry.push({value:'--None--', key:'--None--'});  
                        for (var key in optMap) {
                            reqOPTArry.push({value:optMap[key], key:key});
                        }
                        component.set("v.ReqOPTList", reqOPTArry);
                        
                        var mspArray = [];
                        mspArray.push({value:'--None--', key:'--None--'}); 
                        var mspMap = model.mspMappings;
                        for ( var key in mspMap) {
                            mspArray.push({value:mspMap[key], key:key});
                        }
                        component.set("v.mspList", mspArray);
                        component.find("mspId").set("v.value", '--None--');
                        var mspName=component.find("mspId").get("v.value");
                        
                        var governmentWorkArray = [];
                        governmentWorkArray.push({value:'--None--', key:'--None--'}); 
                        var governmentWorkMap = model.governmentWorkMappings;
                        for ( var key in governmentWorkMap) {
                            governmentWorkArray.push({value:governmentWorkMap[key], key:key});
                        }
                        component.set("v.governmentWorkList", governmentWorkArray);
                        component.find("governmentWorkId").set("v.value", '--None--');
                        var mspName=component.find("governmentWorkId").get("v.value");
                        
                    }else if(model.opp.OpCo__c=='TEKsystems, Inc.'){
                        component.set("v.isOpcoTek",true);
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoAerotek","false");
                    }
                    
                    var reqComp = model.opp.Req_Compliance__c;
                    if(model.opp.Req_Export_Controls_Questionnaire_Reqd__c === true){
                        component.set("v.req.Req_Compliance__c",reqComp + '  ' +'Export Controls Questionnaire Required;');
                    }else{
                        component.set("v.req.Req_Compliance__c",reqComp);
                    }
                    
                    component.set("v.req.Req_Export_Controls_Questionnaire_Reqd__c",model.opp.Req_Export_Controls_Questionnaire_Reqd__c);
                    component.set("v.req.Req_OFCCP_Required__c",model.opp.Req_OFCCP_Required__c);
                    
                    if(model.Organization_Office__c != null){
                        component.set("v.organization.Id",model.opp.Organization_Office__c);
                        component.set("v.organization.Name",model.opp.Organization_Office__r.Name);
                    }
                    component.set("v.req.EnterpriseReqSkills__c",model.opp.EnterpriseReqSkills__c);
                    
                    var oppList;
                    if(model.skillset){
                        oppList  = JSON.parse(model.skillset);
                    }
                    //console.log('SKILLS'+oppList);
                    var finalSkills = [];
                    
                    if(model.opp.OpCo__c === "Allegis Group, Inc." || model.opp.OpCo__c === "Allegis Global Solutions, Inc." || 
                       model.opp.OpCo__c === "Allegis Partners, LLC" || model.opp.OpCo__c === "Major, Lindsey & Africa, LLC"){
                        finalSkills = [];
                    }else if(oppList){
                        if(component.get("v.TFOFlag")) {                                                          
                            let topSkillsList = [];
                            for(let i=0; i<oppList['skills'].length; i++) {
                                topSkillsList.push({
                                    'name': oppList['skills'][i],
                                    'favorite': true,
                                    'index': i,
                                    'suggestedSkill': false                                    
                                });
                            }
                            oppList = topSkillsList;
                        }
                        this.skillsAutoPopulate(component,oppList); 
                        
                       /* let topSkills = [];
                        let secSkills = [];

                        for (var i = 0; i < oppList.length; i++) { 
                            if(oppList[i].favorite){
                                topSkills.push(oppList[i].name)                                
                            }else{
                                secSkills.push(oppList[i].name)                                
                            }
                        }

                        component.set('v.topSkills', topSkills)
                        component.set('v.secondarySkills', secSkills)

                        this.constructFinalSkills(component)*/
                    }

                    if(component.get("v.TFOFlag") && TFOaddr) {
                        let addressList = TFOaddr.split(';');
                        for(let i=0; i<addressList.length; i++) {
                            if(addressList[i].toUpperCase() == 'NA' || addressList[i].toLowerCase() == 'null' || addressList[i].toLowerCase() == 'undefined') {
                                addressList[i] = "";
                            }
                        }
                        component.set("v.req.Req_Worksite_Street__c", addressList[0]);
                        component.set("v.req.Req_Worksite_City__c", addressList[1]);
                        component.set("v.req.Req_Worksite_State__c", addressList[2]);
                        component.set("v.req.Req_Worksite_Country__c", addressList[3]);
                        component.set("v.req.Req_Worksite_Postal_Code__c", addressList[4]);
                    }
                    
                    var finalOppSkills = finalSkills;
                    component.set("v.rawSkills",finalOppSkills);
                    
                    var stageArray = [];
                    stageArray.push({value:'--None--', key:'--None--'});
                    var stgName = "Draft";
                    var isRegOpco = component.get("v.isOpcoRegular");
                    var stageMap = model.stageMappings;
                    for ( var key in stageMap) {
                        if((model.opp.OpCo__c=='Aerotek, Inc'|| model.opp.OpCo__c=='AG_EMEA' || model.opp.OpCo__c == 'TEKsystems, Inc.') && (key=='Draft' || key=='Qualified') ){
                            stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }else if(isRegOpco == 'true'){
                            stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }
                    }
                    component.set("v.stageList", stageArray);
                    component.set("v.req.StageName",stgName);
                    
                    var opcoListArray = [];
                    var opcoVal = model.opp.OpCo__c;
                    //console.log('opp value'+opcoVal);
                    opcoListArray.push({value:'--None--', key:'--None--'}); 
                    var opcoListMap = model.opcoMappings;
                    for ( var key in opcoListMap) {
                        opcoListArray.push({value:opcoListMap[key], key:key, selected: key === opcoVal});
                    }
                    
                    var opcoCloneList = opcoListArray;
                    opcoCloneList.sort(function compare(a,b){
                        if (a.key > b.key)
                            return -1;
                        if (a.key < b.key)
                            return 1;
                        return 0;
                    });
                    
                    opcoCloneList.reverse();
                    
                    component.set("v.opcoList", opcoCloneList);
                    component.set("v.req.OpCo__c",opcoVal);
                    
                    //console.log('CLONED DISPO '+model.dispositionMappings);
                    //console.log('CLONED SINGLE DISPO '+model.opp.Req_Disposition__c);
                    
                    if(model.opp.OpCo__c == 'TEKsystems, Inc.'){
                        var IsTGSReqArray = [];
                        var IsTGSMap = model.TGSReqListMappings;
                        //var IsTGSInfo = model.opp.Req_TGS_Requirement__c; 
                        IsTGSReqArray.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSReqArray.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.isTGSReqList", IsTGSReqArray);
                        
                        var IsTGSPracEng = [];
                        var IsTGSMap = model.PracEngListMappings;
                        IsTGSPracEng.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSPracEng.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSPracEngList", IsTGSPracEng);
                        
                        var IsTGSIntTravel = [];
                        var IsTGSMap = model.IsIntTravelListMappings;
                        IsTGSIntTravel.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSIntTravel.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSIntTravelList", IsTGSIntTravel);
                        
                        var IsTGSInternalHire = [];
                        var IsTGSMap = model.InternalHireMappings;
                        IsTGSInternalHire.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSInternalHire.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSInternalHireList", IsTGSInternalHire);
                        
                        var IsTGSEmpAlign = [];
                        var IsTGSMap = model.EmploymentAlignmentMappings;
                        IsTGSEmpAlign.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSEmpAlign.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSEmpAlignList", IsTGSEmpAlign);
                        
                        var IsTGSFnlDecMkr = [];
                        var IsTGSMap = model.FinalDecisionMakerMappings;
                        IsTGSFnlDecMkr.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSFnlDecMkr.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSFnlDecMkrList", IsTGSFnlDecMkr);
                        
                        var IsTGSLocation = [];
                        var IsTGSMap = model.GlobalServiceLocationMappings;
                        IsTGSLocation.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSLocation.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSLocationList", IsTGSLocation);
                    }
                    
                    //TFO changes start
                    if(TFOFlag) {
                        model.opp.Req_Product__c = 'Contract';
                    }
                    //TFO changes end

                    var productArray = [];
                    var productInfo = model.opp.Req_Product__c; 
                    var productMap = model.productMappings;
                    //console.log('product value testing'+productInfo);
                    productArray.push({value:'--None--', key:'--None--'});  
                    for ( var key in productMap) {
                        productArray.push({value:productMap[key], key:key,selected: key ===productInfo});
                    }
                    component.set("v.productList", productArray);
                    component.set("v.req.Req_Product__c", productInfo);
					selected = model.opp.Req_Product__c; 
                    this.onProductChanges(component,event,helper);
                    
                    
                    //var productPicklistArray = [];
                    //var productPicklistInfo = model.opp.Product__c;
                    
                    /*var productPicklistMap = model.productPicklistMappings;
                    //console.log('product value testing'+productInfo);
                    productPicklistArray.push({value:'--None--', key:'--None--'});
                    for ( var key in productPicklistMap) {
                        productPicklistArray.push({value:productPicklistMap[key], key:key,selected: key ===productInfo});
                    }
                    component.set("v.productListPicklist", productPicklistArray);*/
                    
                    this.PrimaryBuyerDependsDivision(component,event,model);
                    
                    this.SkillSpecialtyDependsDivision(component,event,model);                    
                    
                    var currencyArray = [];
                    var curr = component.get("v.req.Currency__c");
                    currencyArray.push({value:'--None--', key:'--None--'}); 
                    var currencyMap = model.currencyMappings;
                    for ( var key in currencyMap) {
                        currencyArray.push({value:currencyMap[key], key:key, selected: key ===curr});
                    }
                    component.set("v.currencyList", currencyArray);
                    component.set("v.req.Currency__c",curr);
                    
                    var durUnit = component.get("v.req.Req_Duration_Unit__c");
                    var durationUnitArray = [];
                    durationUnitArray.push({value:'--None--', key:'--None--'}); 
                    var durationUnitMap = model.durationUnitMappings;
                    for ( var key in durationUnitMap) {
                        durationUnitArray.push({value:durationUnitMap[key], key:key, selected: key === durUnit});
                    }
                    component.set("v.durationUnitList", durationUnitArray);
                    component.set("v.req.Req_Duration_Unit__c",durUnit);
                    
                    var lossWashReasonArray = [];
                    var lossWash = model.opp.Req_Loss_Wash_Reason__c;
                    lossWashReasonArray.push({value:'--None--', key:'--None--'}); 
                    var lossWashReasonMap = model.lossWashReasonMappings;
                    for ( var key in lossWashReasonMap) {
                        lossWashReasonArray.push({value:lossWashReasonMap[key], key:key, selected: key === lossWash});
                    }
                    component.set("v.lossWashReasonList", lossWashReasonArray);
                    component.set("v.req.Req_Loss_Wash_Reason__c",lossWash);
                    
                    var workRemoteArray = [];
                    var workRem = model.opp.Req_Work_Remote__c;
                    workRemoteArray.push({value:'--None--', key:'--None--'}); 
                    var workRemoteMap = model.workRemoteMappings;
                    for ( var key in workRemoteMap) {
                        workRemoteArray.push({value:workRemoteMap[key], key:key, selected: key === workRem});
                    }
                    component.set("v.workRemoteList", workRemoteArray);
                    component.set("v.req.Req_Work_Remote__c",workRem);
                    
                    var minEducationArray = [];
                    var minEdu = model.opp.Req_Minimum_Education_Required__c;
                    minEducationArray.push({value:'--None--', key:'--None--'}); 
                    var minEducationMap = model.minimumEducationMappings;
                    for ( var key in minEducationMap) {
                        minEducationArray.push({value:minEducationMap[key], key:key,selected: key === minEdu });
                    }
                    component.set("v.minEducationList", minEducationArray);
                    component.set("v.req.Req_Minimum_Education_Required__c",minEdu);
                    var practiceAreaArray = [];
                    var pracArea = model.opp.Req_Practice_Area__c;
                    practiceAreaArray.push({value:'--None--', key:'--None--'}); 
                    var practiceAreaMap = model.practiceAreaMappings;
                    for ( var key in practiceAreaMap) {
                        practiceAreaArray.push({value:practiceAreaMap[key], key:key,selected: key === pracArea});
                    }
                    component.set("v.practiceAreaList", practiceAreaArray);
                    component.set("v.req.Req_Practice_Area__c",pracArea);
                    
                    var experienceLevelArray = [];
                    var expLevel = model.opp.Req_Job_Level__c;
                    experienceLevelArray.push({value:'--None--', key:'--None--'}); 
                    var experienceLevelMap = model.experienceLevelMappings;
                    for (var key in experienceLevelMap) {
                        experienceLevelArray.push({value:experienceLevelMap[key], key:key, selected: key === expLevel});
                    }
                    component.set("v.experienceLevelList", experienceLevelArray);
                    component.set("v.req.Req_Job_Level__c",expLevel);
                    
                    var paymentTermsArray = [];
                    var payterm = model.opp.Req_Payment_terms__c;
                    paymentTermsArray.push({value:'--None--', key:'--None--'}); 
                    var paymentTermsMap = model.paymentTermsMappings;
                    for ( var key in paymentTermsMap) {
                        paymentTermsArray.push({value:paymentTermsMap[key], key:key,selected: key === payterm});
                    }
                    component.set("v.paymentTermsList", paymentTermsArray);
                    component.set("v.req.Req_Payment_terms__c",payterm);
                    
                    if(model.opp.Req_Terms_of_engagement__c != null){
                        var opts = [];
                        var termVal = model.opp.Req_Terms_of_engagement__c;
                        
                        if(termVal == null || termVal === "" || typeof(termVal) == "undefined"){
                            opts.push( { value: "", label: "--None--",selected:true });
                            opts.push( { value: "Contingent", label: "Contingent" });
                            opts.push( { value: "Retained", label: "Retained" });
                        }else if(termVal == 'Retained'){
                            opts.push( { value: "", label: "--None--" });
                            opts.push( { value: "Contingent", label: "Contingent" });
                            opts.push( { value: "Retained", label: "Retained",selected:true });
                        }else if(termVal == 'Contingent'){
                            opts.push( { value: "", label: "--None--" });
                            opts.push( { value: "Contingent", label: "Contingent",selected:true });
                            opts.push( { value: "Retained", label: "Retained" });
                        }else if(termVal != 'Retained' && termVal != 'Contingent' ){
                            opts.push( { value: "", label: "--None--" });
                            opts.push( { value: "Contingent", label: "Contingent" });
                            opts.push( { value: "Retained", label: "Retained" });
                            opts.push({ value: termVal, label: termVal,selected:true });
                        }
                        component.set("v.options", opts);
                    }else{
                        this.defaultTermsOfEngagement(component,event,helper);
                    }
                    
                    var termsofEngageArray = [];
                    var terms = model.opp.Req_Terms_of_engagement__c;
                    termsofEngageArray.push({value:'--None--', key:'--None--'}); 
                    var termsofEngageMap = model.termsOfEngagementMappings;
                    for ( var key in termsofEngageMap) {
                        termsofEngageArray.push({value:termsofEngageMap[key], key:key, selected: key === terms});
                    }
                    component.set("v.termsEngageList", termsofEngageArray);
                    component.set("v.req.Req_Terms_of_engagement__c",terms);
                    
                    var whyPositionArray = [];
                    var whyPosn = model.opp.Req_Why_is_Position_Open__c;
                    whyPositionArray.push({value:'--None--', key:'--None--'}); 
                    var positionListMap = model.whyPositionMappings;
                    for (var key in positionListMap){
                        whyPositionArray.push({value:positionListMap[key], key:key,selected: key === whyPosn });
                    }
                    component.set("v.whyPositionList", whyPositionArray);
                    component.set("v.req.Req_Why_is_Position_Open__c",whyPosn);
                    
                    //Req_Rate_Frequency__c
                    var rateFrequencyArray=[];
                    var rateFreq = model.opp.Req_Rate_Frequency__c
                    rateFrequencyArray.push({value:'--None--', key:'--None--'});
                    var rateFrequencyMap = model.rateFrequencyMappings;
                    for(var key in rateFrequencyMap){
                        if(rateFrequencyMap[key]!='Salary'){
                            rateFrequencyArray.push({value:rateFrequencyMap[key], key:key, selected: key === rateFreq});
                        }
                    }
                    
                    //Req Client Working on Req
                    var reqClientWorkingArray =[];
                    var clientWork = model.opp.Req_Client_working_on_Req__c;
                    reqClientWorkingArray.push({value:'--None--', key:'--None--'});
                    var clientWorkMap = model.ClientReqWorkingMappings;
                    //console.log('REQ CLIENT WORK PLIST'+clientWorkMap);
                    for(var key in clientWorkMap){
                        reqClientWorkingArray.push({value:clientWorkMap[key], key:key, selected: key === clientWork});
                    }
                    
                    component.set("v.reqClientWorkList",reqClientWorkingArray);
                    component.set("v.rateFrequencyList",rateFrequencyArray);
                    component.set("v.req.Req_Rate_Frequency__c",rateFreq);
                    component.set("v.req.Description",model.opp.Req_Job_Description__c);
                    
                    var otMultiArray = [];
                    var otMultiplier = model.opp.OT_Multiplier__c;
                    otMultiArray.push({value:'--None--', key:'--None--'}); 
                    var otmultiListMap = model.otmultiplierMappings;
                    for (var key in otmultiListMap){
                        otMultiArray.push({value:otmultiListMap[key], key:key,selected: key === otMultiplier });
                    }
                    component.set("v.otmultitplierList", otMultiArray);
                    component.set("v.req.OT_Multiplier__c",otMultiplier);
                    
                    var alterDeliveryArray = [];
                    var alterDelivery = model.opp.Req_Delivery_Type__c;
                    alterDeliveryArray.push({value:'--None--', key:'--None--'}); 
                    var alterDeliveryListMap = model.alternateDeliveryTypeMappings;
                    for (var key in alterDeliveryListMap){
                        alterDeliveryArray.push({value:alterDeliveryListMap[key], key:key,selected: key === alterDelivery });
                    }
                    component.set("v.altdelivertyList", alterDeliveryArray);
                    component.set("v.req.Req_Delivery_Type__c",alterDelivery);
                    
                    var clientWorkingArray = [];
                    var clientWorking = model.opp.Req_Client_working_on_Req__c;
                    clientWorkingArray.push({value:'--None--', key:'--None--'});
                    var clientWorkingListMap = model.clientWorkingOnReqMappings;
                    for (var key in clientWorkingListMap){
                        clientWorkingArray.push({value:clientWorkingListMap[key], key:key,selected: key === clientWorking });
                    }
                    component.set("v.clientworkOnReqList", clientWorkingArray);
                    component.set("v.req.Req_Client_working_on_Req__c",clientWorking);
                    
                    
                    var draftReasonArray = [];
                    var draftReason = model.opp.Req_Draft_Reason__c;
                    draftReasonArray.push({value:'--None--', key:'--None--'});
                    var draftReasonListMap = model.draftReasonMappings;
                    for (var key in draftReasonListMap){
                        draftReasonArray.push({value:draftReasonListMap[key], key:key,selected: key === draftReason });
                    }
                    component.set("v.draftReasonList", draftReasonArray);
                    component.set("v.req.Req_Draft_Reason__c",draftReason);
                    
                    var serviceProductArray = [];
                    var serviceProduct = model.opp.Req_Service_Product_Offering__c;
                    serviceProductArray.push({value:'--None--', key:'--None--'}); 
                    var serviceProductListMap = model.serviceProductOfferingMappings;
                    for ( var key in serviceProductListMap) {
                        serviceProductArray.push({value:serviceProductListMap[key], key:key, selected: key === serviceProduct});
                    }
                    component.set("v.serviceproductOfferList", serviceProductArray);
                    component.set("v.req.Req_Service_Product_Offering__c",serviceProduct);
                    
                    var deliveryOfficeArray = [];
                    var deliveryOffice=model.opp.Req_Secondary_Delivery_Office__c;
                    deliveryOfficeArray.push({value:'--None--', key:'--None--'}); 
                    var deliviryOfficeListMap = model.alternativeDeliveryOfficeMappings;
                    for (var key in deliviryOfficeListMap){
                        deliveryOfficeArray.push({value:deliviryOfficeListMap[key], key:key,selected: key === deliveryOffice });
                    }
                    component.set("v.deliveryOfficeList", deliveryOfficeArray);
                    component.set("v.req.Req_Secondary_Delivery_Office__c",deliveryOffice);
                    
                    var oppLOBEITArray = [];
                    var oppLOBEIT=model.opp.REQ_Opportunity_driven_mostly_by_LOB__c;
                    oppLOBEITArray.push({value:'--None--', key:'--None--'});
                    var oppLOBEITListMap = model.oppLOBEITMapping;
                    for (var key in oppLOBEITListMap){
                        oppLOBEITArray.push({value:oppLOBEITListMap[key], key:key,selected: key === oppLOBEIT});
                    }
                    component.set("v.oppLOBEITList", oppLOBEITArray);
                    component.set("v.req.REQ_Opportunity_driven_mostly_by_LOB__c",oppLOBEIT);
                    
                    
                    //Product or Placement Type Rendering
                    if(productInfo === "Contract"){
                        component.set("v.isContract", "true");
                        component.set("v.isPermanent", "false");
                        component.set("v.isCTH", "false");
                    }else if(productInfo === "Permanent"){
                        component.set("v.isContract", "false");
                        component.set("v.isPermanent", "true");
                        component.set("v.isCTH", "false");
                    }else if(productInfo === "Contract to Hire"){
                        component.set("v.isContract", "false");
                        component.set("v.isPermanent", "false");
                        component.set("v.isCTH", "true");
                    }
                    
                    if(model.opp.Legacy_Product__r != null && model.opp.Legacy_Product__r.Name != null){
                        component.set("v.cloneProductId",model.opp.Legacy_Product__r.Name); 
                        component.set("v.isCloneMainSkill",true);
                    }
                    
                    component.set("v.req.Req_Total_Positions__c",model.opp.Req_Total_Positions__c);
                    component.set("v.req.Req_Include_Bonus__c",model.opp.Req_Include_Bonus__c);
                    
                    component.set("v.req.Req_Worksite_Street__c", model.opp.Req_Worksite_Street__c); 
                    component.set("v.req.StreetAddress2__c",model.opp.StreetAddress2__c);
                    component.set("v.req.Req_Worksite_City__c", model.opp.Req_Worksite_City__c); 
                    component.set("v.req.Req_Worksite_State__c", model.opp.Req_Worksite_State__c); 
                    component.set("v.req.Req_Worksite_Postal_Code__c", model.opp.Req_Worksite_Postal_Code__c); 
                    component.set("v.req.Req_Worksite_Country__c", model.opp.Req_Worksite_Country__c);
                    
                    if(model.act!=null){
                        component.set("v.masterGlobalAccountId",model.act.Master_Global_AccountID_formula_field__c);
                    }else{
                        component.set("v.masterGlobalAccountId",'NO-MASTER-ID');
                    }
                    
                    component.set("v.inputOffCCPCompliant",model.opp.Req_OFCCP_Required__c);
                    component.set("v.Ownership",model.ownershipOpco);
                    component.set("v.recordType","Req");
                    
                    var businessUnitArray = [];
                    var busUnit = model.opp.BusinessUnit__c;
                    var opcoDivi = model.opp.Req_Division__c;
                    var busArray = [];
                    businessUnitArray.push({value:'--None--', key:'--None--'});
                    var businessUnitMap = model.businessUnitMappings;
                    //console.log('bus unit model'+model.businessUnitMappings);
                    for ( var key in businessUnitMap) {
                        businessUnitArray.push({value:businessUnitMap[key], key:key, selected: key === opcoDivi});
                        busArray.push(key);
                    }
                    
                    
                    var oppBusinessUnitList = businessUnitArray;
                    oppBusinessUnitList.sort(function compare(a,b){
                        if (a.key > b.key)
                            return -1;
                        if (a.key < b.key)
                            return 1;
                        return 0;
                    });
                    
                    oppBusinessUnitList.reverse();
                    
                    
                    component.set("v.businessUnitList", oppBusinessUnitList);
                    component.set("v.businessList", busArray);
                    component.set("v.req.Req_Division__c",opcoDivi);
                    
                    
                    var opcoDivisionArray=[];
                    var opcoDivsionMap=model.opcoDivsionDepdentMappings;
                    for(var key in opcoDivsionMap){
                        opcoDivisionArray.push({value:opcoDivsionMap[key], key:key,selected: key === opcoDivi});
                    }
                    component.set("v.opcoDivisionList",opcoDivisionArray);
                    
                    
                    if(opcoVal=='Aerotek, Inc'|| opcoVal=='TEKsystems, Inc.'){
                        component.set("v.MainskillOpco",true);
                        this.EnterpriseReqSegmentModal(component,event,helper);
                    }
                    else{
                        component.set("v.MainskillOpco",false);
                    }
                    if(opcoVal=='TEKsystems, Inc.'){
                        component.find("isTGSReqId").set("v.value", '--None--');
                        component.set("v.isTGSTrue", "true");
                        component.find("isTGSPEId").set("v.value", '--None--');
                        component.find("isTGSLocationId").set("v.value", '--None--');
                        component.find("isEmploymentAlignmentId").set("v.value", '--None--');
                        component.find("isFinaDecMakId").set("v.value", '--None--');
                        component.find("isInterNationalId").set("v.value", '--None--');
                        component.find("isInternalHireId").set("v.value", '--None--');
                        component.find("isBackfillId").set("v.value", 'false');
                        component.find("isNationalId").set("v.value", 'false');
                        component.set("v.isTGSTrue", "false");
                    }
                    //S-228899 starts
                    model.opp.Req_Manually_Updated_Fields__c='';
                    component.set("v.req.Req_Manually_Updated_Fields__c", model.opp.Req_Manually_Updated_Fields__c);
                    if(model.opp.Description && model.opp.EnterpriseReqSkills__c && model.opp.Req_Job_Title__c) {
                        this.skillSpecialtyClassifierCall(component, event);
                    } else {
                        component.set("v.req.Req_Skill_Specialty__c", "");
                        component.set("v.req.Functional_Non_Functional__c", "");
                        component.set("v.req.Req_Job_Level__c", "");
                        component.set("v.req.Skill_Set__c", "");
                        component.set("v.req.Req_RVT_Occupation_Code__c", "");
                    }
                    //S-228899 ends
                }else{
                    // Create New Req
                    
                    //console.log('opco'+model.opco);
                    //Opco ---- UI --- Show -- Hide
                    if(model.opco === "AG_EMEA"){
                        component.set("v.isOpcoEmea", "true");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoTek",false);
                        component.set("v.isOpcoAerotek",false);
                        this.skillSpecValues(component, event,model);
                        this.FunctionalValues(component,event,model);
                        this.skillSetValues(component,event,model);
                    }else if(model.opco === "Allegis Group, Inc." || model.opco === "Allegis Global Solutions, Inc." || 
                             model.opco === "Allegis Partners, LLC" || model.opco === "Major, Lindsey & Africa, LLC"){
                        component.set("v.isOpcoRegular", "true");
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoTek","false");
                        component.set("v.isOpcoAerotek","false");
                    }else if(model.opco=='Aerotek, Inc'){
                        component.set("v.isOpcoAerotek",true);
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoTek","false");
                    }else if(model.opco=='TEKsystems, Inc.'){
                        component.set("v.isOpcoTek",true);
                        component.set("v.isOpcoEmea", "false");
                        component.set("v.isOpcoRegular", "false");
                        component.set("v.isOpcoAerotek","false");
                    }
                    
                    //Proactive Submittal - S-143480
                    if(model.proactiveSubmittal != null && model.proactiveSubmittal !== 'undefined') {
                        component.set("v.proactiveSubmittal", model.proactiveSubmittal);
                        component.set("v.req.Req_Job_Title__c", model.proactiveSubmittal.Job_Title__c);
                        component.set("v.isProactiveSubmittal", true);
                        component.set("v.psSkills", model.skillset);
                        if(model.skillset){
                        	var oppList  = JSON.parse(model.skillset);
                            this.skillsAutoPopulate(component,oppList);
                    	}                        
                    }
                    
                    if(model.exportControlsReqd === true){
                        component.set("v.req.Req_Compliance__c",'Export Controls Questionnaire Required;');
                    }
                    component.set("v.req.Req_Export_Controls_Questionnaire_Reqd__c",model.exportControlsReqd);
                    component.set("v.req.Req_OFCCP_Required__c",model.ofccpReqd);
                    
                    if(model.isTgsReq === true){
                        component.set("v.isTgsParentReq", model.isTgsReq);
                        component.set("v.req.Req_Opportunity__c",model.tgsOpptyId);
                    }
                    var productInfo = 'Contract';//model.opp.Req_Product__c; 
                    var productArray = [];
                    var productMap = model.productMappings;
                    //console.log('product value testing'+productInfo);
                    productArray.push({value:'--None--', key:'--None--'});  
                    for (var key in productMap) {
                        productArray.push({value:productMap[key], key:key,selected: key ===productInfo});
                    }
                    component.set("v.productList", productArray);
					component.set("v.req.Req_Product__c", productInfo);
					selected = productInfo;
					this.onProductChanges(component,event,helper);
                    
                    /*var productPicklistArray = [];
                    var productPicklistMap = model.productPicklistMappings;
                    //console.log('product value testing'+productInfo);
                    productPicklistArray.push({value:'--None--', key:'--None--'});
                    for (var key in productPicklistMap) {
                        productPicklistArray.push({value:productPicklistMap[key], key:key});
                    }
                    component.set("v.productListPicklist", productPicklistArray);
                    component.set("v.req.Product__c","");*/
                    this.PrimaryBuyerDependsDivision(component,event,null);
                    
                    this.SkillSpecialtyDependsDivision(component,event,null);                    
                    
                    if(model.opco == 'TEKsystems, Inc.'){
                        var isTGSReqArray = [];
                        var isTGSReqMap = model.TGSReqListMappings;
                        var oppRecTypeId = component.get("v.oppRecTypeId");
                        var tgsValue ;
                        if(oppRecTypeId !='' && oppRecTypeId !=null && typeof oppRecTypeId !='undefined'){
                            tgsValue = 'Yes';
                            component.set("v.req.Req_TGS_Requirement__c", "Yes")
                            component.set("v.isTGSTrue", "true");
                        } else {
                            tgsValue = '--None--';
                        }
                        isTGSReqArray.push({value:'--None--', key:'--None--'});  
                        for (var key in isTGSReqMap) {
                            isTGSReqArray.push({value:isTGSReqMap[key], key:key, selected: key === tgsValue});
                        }
                        component.set("v.isTGSReqList", isTGSReqArray);
                        
                        var IsTGSPracEng = [];
                        var IsTGSMap = model.PracEngListMappings;
                        IsTGSPracEng.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSPracEng.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSPracEngList", IsTGSPracEng);
                        
                        var IsTGSIntTravel = [];
                        var IsTGSMap = model.IsIntTravelListMappings;
                        IsTGSIntTravel.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSIntTravel.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSIntTravelList", IsTGSIntTravel);
                        
                        var IsTGSInternalHire = [];
                        var IsTGSMap = model.InternalHireMappings;
                        IsTGSInternalHire.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSInternalHire.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSInternalHireList", IsTGSInternalHire);
                        
                        var IsTGSEmpAlign = [];
                        var IsTGSMap = model.EmploymentAlignmentMappings;
                        IsTGSEmpAlign.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSEmpAlign.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSEmpAlignList", IsTGSEmpAlign);
                        
                        var IsTGSFnlDecMkr = [];
                        var IsTGSMap = model.FinalDecisionMakerMappings;
                        IsTGSFnlDecMkr.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSFnlDecMkr.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSFnlDecMkrList", IsTGSFnlDecMkr);
                        
                        var IsTGSLocation = [];
                        var IsTGSMap = model.GlobalServiceLocationMappings;
                        IsTGSLocation.push({value:'--None--', key:'--None--'});  
                        for (var key in IsTGSMap) {
                            IsTGSLocation.push({value:IsTGSMap[key], key:key});
                        }
                        component.set("v.IsTGSLocationList", IsTGSLocation);
                    }
                    
                    if(model.opco=='Aerotek, Inc'){
                        var reqOPTArry = [];
                        var optMap = model.ReqOptMappings;
                        reqOPTArry.push({value:'--None--', key:'--None--'});  
                        for (var key in optMap) {
                            reqOPTArry.push({value:optMap[key], key:key});
                        }
                        component.set("v.ReqOPTList", reqOPTArry);
                    }
                    
                    var stageArray = [];
                    var defaultStage = "Draft";
                    stageArray.push({value:'--None--', key:'--None--'}); 
                    var isRegOpco = component.get("v.isOpcoRegular");
                    var stageMap = model.stageMappings;
                    var soppid=component.get("v.soppId");
                    var index=0;
                    for ( var key in stageMap) {
                        //console.log('stage'+key+''+model.opco);
                        if((model.opco=='Aerotek, Inc' || model.opco == 'AG_EMEA' || model.opco == 'TEKsystems, Inc.') && (key=='Draft' || key=='Qualified') ){
                            stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }else if(isRegOpco == 'true'){
                            stageArray.push({value:stageMap[key], key:key,selected: key === stgName});
                        }
                        index=index+1;
                        if(key=='Draft'){
                            if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){ //Strategic Opportunity redirection
                                var obj=stageArray[index];
                                obj.selected="true"; 
                            }
                        }
                        
                    }
                    component.set("v.stageList", stageArray);
                    // component.set("v.req.StageName","Draft");
                    
                    var currencyArray = [];
                    var defCurrency = "USD - U.S Dollar";
                    currencyArray.push({value:'--None--', key:'--None--'}); 
                    var currencyMap = model.currencyMappings;
                    for ( var key in currencyMap) {
                        currencyArray.push({value:currencyMap[key], key:key, selected: key === defCurrency});
                    }
                    component.set("v.currencyList", currencyArray);
                    component.set("v.req.Currency__c",'USD - U.S Dollar');
                    
                    var durationUnitArray = [];
                    durationUnitArray.push({value:'--None--', key:'--None--'}); 
                    var durationUnitMap = model.durationUnitMappings;
                    for ( var key in durationUnitMap) {
                        durationUnitArray.push({value:durationUnitMap[key], key:key});
                    }
                    component.set("v.durationUnitList", durationUnitArray);
                    component.set("v.req.Req_Duration_Unit__c","--None--");
                    
                    var lossWashReasonArray = [];
                    lossWashReasonArray.push({value:'--None--', key:'--None--'}); 
                    var lossWashReasonMap = model.lossWashReasonMappings;
                    for ( var key in lossWashReasonMap) {
                        lossWashReasonArray.push({value:lossWashReasonMap[key], key:key});
                    }
                    component.set("v.lossWashReasonList", lossWashReasonArray);
                    
                    var workRemoteArray = [];
                    workRemoteArray.push({value:'--None--', key:'--None--'}); 
                    var workRemoteMap = model.workRemoteMappings;
                    for ( var key in workRemoteMap) {
                        workRemoteArray.push({value:workRemoteMap[key], key:key});
                    }
                    component.set("v.workRemoteList", workRemoteArray);
                    
                    var minEducationArray = [];
                    minEducationArray.push({value:'--None--', key:'--None--'}); 
                    var minEducationMap = model.minimumEducationMappings;
                    for ( var key in minEducationMap) {
                        minEducationArray.push({value:minEducationMap[key], key:key});
                    }
                    component.set("v.minEducationList", minEducationArray);
                    var governmentWorkArray = [];
                    governmentWorkArray.push({value:'--None--', key:'--None--'}); 
                    var governmentWorkMap = model.governmentWorkMappings;
                    for ( var key in governmentWorkMap) {
                        governmentWorkArray.push({value:governmentWorkMap[key], key:key});
                    }
                    component.set("v.governmentWorkList", governmentWorkArray);
                    
                    var mspArray = [];
                    mspArray.push({value:'--None--', key:'--None--'}); 
                    var mspMap = model.mspMappings;
                    for ( var key in mspMap) {
                        mspArray.push({value:mspMap[key], key:key});
                    }
                    component.set("v.mspList", mspArray);
                    
                    var practiceAreaArray = [];
                    practiceAreaArray.push({value:'--None--', key:'--None--'}); 
                    var practiceAreaMap = model.practiceAreaMappings;
                    for ( var key in practiceAreaMap) {
                        practiceAreaArray.push({value:practiceAreaMap[key], key:key});
                    }
                    component.set("v.practiceAreaList", practiceAreaArray);
                    
                    var experienceLevelArray = [];
                    experienceLevelArray.push({value:'--None--', key:'--None--'}); 
                    var experienceLevelMap = model.experienceLevelMappings;
                    for ( var key in experienceLevelMap) {
                        experienceLevelArray.push({value:experienceLevelMap[key], key:key});
                    }
                    component.set("v.experienceLevelList", experienceLevelArray); 
                    
                    //Terms of Engagement
                    this.defaultTermsOfEngagement(component,event,helper);
                    /*var opts = [
                        { value: "", label: "--None--" },
                        { value: "Contingent", label: "Contingent" },
                        { value: "Retained", label: "Retained" }
                    ];
                    component.set("v.options", opts);*/
                    
                    var paymentTermsArray = [];
                    paymentTermsArray.push({value:'--None--', key:'--None--'}); 
                    var paymentTermsMap = model.paymentTermsMappings;
                    for ( var key in paymentTermsMap) {
                        paymentTermsArray.push({value:paymentTermsMap[key], key:key});
                    }
                    component.set("v.paymentTermsList", paymentTermsArray);
                    
                    var termsofEngageArray = [];
                    termsofEngageArray.push({value:'--None--', key:'--None--'}); 
                    var termsofEngageMap = model.termsOfEngagementMappings;
                    for ( var key in termsofEngageMap) {
                        termsofEngageArray.push({value:termsofEngageMap[key], key:key});
                    }
                    component.set("v.termsEngageList", termsofEngageArray);
                    
                    var opcoListArray = [];
                    opcoListArray.push({value:'--None--', key:'--None--'}); 
                    var opcoListMap = model.opcoMappings;
                    var userOpco = model.opco;
                    for ( var key in opcoListMap) {
                        opcoListArray.push({value:opcoListMap[key], key:key, selected: key === userOpco}); 
                    }
                    
                    var opcoValList = opcoListArray;
                    opcoValList.sort(function compare(a,b){
                        if (a.key > b.key)
                            return -1;
                        if (a.key < b.key)
                            return 1;
                        return 0;
                    });
                    
                    opcoValList.reverse();
                    
                    component.set("v.opcoList", opcoValList);
                    
                    var whyPositionArray = [];
                    whyPositionArray.push({value:'--None--', key:'--None--'}); 
                    var positionListMap = model.whyPositionMappings;
                    for (var key in positionListMap){
                        whyPositionArray.push({value:positionListMap[key], key:key});
                    }
                    component.set("v.whyPositionList", whyPositionArray);
                    
                    var businessUnitArray = [];
                    var busArray = [];
                    businessUnitArray.push({value:'--None--', key:'--None--'}); 
                    var businessUnitMap = model.businessUnitMappings;
                    for ( var key in businessUnitMap) {
                        businessUnitArray.push({value:businessUnitMap[key], key:key});
                        busArray.push(key);
                    }
                    
                    component.set("v.businessUnitList", businessUnitArray);
                    component.set("v.businessList", busArray);
                    
                    var rateFrequencyArray=[];
                    rateFrequencyArray.push({value:'--None--', key:'--None--'});
                    var rateFrequencyMap = model.rateFrequencyMappings;
                    for(var key in rateFrequencyMap){
                        if(rateFrequencyMap[key]!='Salary'){
                            rateFrequencyArray.push({value:rateFrequencyMap[key], key:key});
                        }
                    }
                    component.set("v.rateFrequencyList",rateFrequencyArray);
                    
                    //Req Client Working on Req
                    var reqClientWorkingArray =[];
                    reqClientWorkingArray.push({value:'--None--', key:'--None--'});
                    var clientWorkMap = model.ClientReqWorkingMappings;
                    for(var key in clientWorkMap){
                        reqClientWorkingArray.push({value:clientWorkMap[key], key:key});
                    }
                    
                    component.set("v.reqClientWorkList",reqClientWorkingArray);
                    
                    var opcoDivisionArray=[];
                    var opcoDivsionMap=model.opcoDivsionDepdentMappings;
                    for(var key in opcoDivsionMap){
                        opcoDivisionArray.push({value:opcoDivsionMap[key], key:key});
                    }
                    component.set("v.opcoDivisionList",opcoDivisionArray);
                    
                    
                    var rateFrequencyArray=[];
                    rateFrequencyArray.push({value:'--None--', key:'--None--'});
                    var rateFrequencyMap = model.rateFrequencyMappings;
                    for(var key in rateFrequencyMap){
                        if(rateFrequencyMap[key]!='Salary'){
                            rateFrequencyArray.push({value:rateFrequencyMap[key], key:key});
                        }
                    }
                    component.set("v.rateFrequencyList",rateFrequencyArray);
                    
                    var opcoDivisionArray=[];
                    var opcoDivsionMap=model.opcoDivsionDepdentMappings;
                    for(var key in opcoDivsionMap){
                        opcoDivisionArray.push({value:opcoDivsionMap[key], key:key});
                    }
                    component.set("v.opcoDivisionList",opcoDivisionArray);
                    
                    
                    
                    var otMultiArray = [];
                    
                    otMultiArray.push({value:'--None--', key:'--None--'}); 
                    var otmultiListMap = model.otmultiplierMappings;
                    for (var key in otmultiListMap){
                        otMultiArray.push({value:otmultiListMap[key], key:key });
                    }
                    component.set("v.otmultitplierList", otMultiArray);
                    
                    
                    var alterDeliveryArray = [];
                    
                    alterDeliveryArray.push({value:'--None--', key:'--None--'}); 
                    var alterDeliveryListMap = model.alternateDeliveryTypeMappings;
                    for (var key in alterDeliveryListMap){
                        alterDeliveryArray.push({value:alterDeliveryListMap[key], key:key });
                    }
                    component.set("v.altdelivertyList", alterDeliveryArray);
                    
                    var deliveryOfficeArray = [];
                    deliveryOfficeArray.push({value:'--None--', key:'--None--'}); 
                    var deliviryOfficeListMap = model.alternativeDeliveryOfficeMappings;
                    for (var key in deliviryOfficeListMap){
                        deliveryOfficeArray.push({value:deliviryOfficeListMap[key], key:key });
                    }
                    component.set("v.deliveryOfficeList", deliveryOfficeArray);
                    
                    
                    var clientWorkingArray = [];
                    
                    clientWorkingArray.push({value:'--None--', key:'--None--'});
                    var clientWorkingListMap = model.clientWorkingOnReqMappings;
                    for (var key in clientWorkingListMap){
                        clientWorkingArray.push({value:clientWorkingListMap[key], key:key });
                    }
                    component.set("v.clientworkOnReqList", clientWorkingArray);
                    
                    
                    
                    var draftReasonArray = [];
                    
                    draftReasonArray.push({value:'--None--', key:'--None--'});
                    var draftReasonListMap = model.draftReasonMappings;
                    for (var key in draftReasonListMap){
                        draftReasonArray.push({value:draftReasonListMap[key], key:key});
                    }
                    component.set("v.draftReasonList", draftReasonArray);
                    component.set("v.req.Req_Draft_Reason__c",'--None--');
                    
                    
                    var oppLOBEITArray = [];
                    
                    oppLOBEITArray.push({value:'--None--', key:'--None--'});
                    var oppLOBEITListMap = model.oppLOBEITMapping;
                    for (var key in oppLOBEITListMap){
                        oppLOBEITArray.push({value:oppLOBEITListMap[key], key:key});
                    }
                    component.set("v.oppLOBEITList", oppLOBEITArray);
                    
                    var serviceProductArray = [];
                    serviceProductArray.push({value:'--None--', key:'--None--'}); 
                    var serviceProductListMap = model.serviceProductOfferingMappings;
                    for ( var key in serviceProductListMap) {
                        serviceProductArray.push({value:serviceProductListMap[key], key:key});
                    }
                    component.set("v.serviceproductOfferList", serviceProductArray);
                    
                    component.set("v.req.Req_Total_Positions__c",1);
                    component.set("v.req.Req_Include_Bonus__c",true);
                    
                    component.set("v.req.Req_Worksite_Street__c",model.act.Account_Street__c); 
                    component.set("v.req.Req_Worksite_City__c",model.act.Account_City__c); 
                    component.set("v.req.Req_Worksite_State__c",model.act.Account_State__c); 
                    component.set("v.req.Req_Worksite_Postal_Code__c",model.act.Account_Zip__c); 
                    component.set("v.req.Req_Worksite_Country__c",model.act.Account_Country__c);
                    
                    //End of Create New Req
                    
                    component.set("v.masterGlobalAccountId",model.act.Master_Global_AccountID_formula_field__c);
                    component.set("v.Ownership",model.ownershipOpco);
                    component.set("v.req.OpCo__c",model.opco);
                    component.set("v.recordType","Req");
                    component.set("v.inputOffCCPCompliant",model.act.OFCCP_Compliant__c);
                    component.set("v.req.Req_End_Client__c",null);
                    
                    this.loadBusinessUnitValues(component,event,model.opco);
                    
                    var opcoDivison = '--None--';
                    var businessUnitArray = [];
                    var businessUnitMap = component.get("v.businessUnitList");;
                    
                    for (var key in businessUnitMap) {
                        businessUnitArray.push({value:businessUnitMap[key].key, key:businessUnitMap[key].key, selected: businessUnitMap[key].key === opcoDivison});
                    }
                    
                    
                    var oppBusinessUnitList = businessUnitArray;
                    oppBusinessUnitList.sort(function compare(a,b){
                        if (a.key > b.key)
                            return -1;
                        if (a.key < b.key)
                            return 1;
                        return 0;
                    });
                    
                    oppBusinessUnitList.reverse();
                    
                    component.set("v.businessUnitList", oppBusinessUnitList);
                    component.set("v.req.Req_Division__c",opcoDivison);
                }
                
            }  
            
            
            //Strategic Opportunity
            var soppid=component.get("v.soppId");
            if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){ //Strategic Opportunity redirection
                component.set("v.req.StageName",'Draft');
            }
            
        });
        
        $A.enqueueAction(action);
        
    },    
    addAltRecord : function(component,event,helper) {
        var supportRequestList = component.get("v.supportRequestList");
        supportRequestList.push({
            'sobjectType' : 'Support_Request__c',
            'Service_Product_Offering__c' : '',
            'Alternate_Delivery_Type__c' : '',
            'Alternate_Delivery_Shared_Positions__c' : '',
            'Alternate_Delivery_Office__c' : ''
        });
        component.set("v.supportRequestList", supportRequestList);
    },
    getQueryString : function(field,url){
        var href = url ? url : window.location.href;
        var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        var string = reg.exec(href);
        return string ? string[1] : null;
    },
    
    EnterpriseReqSegmentModal : function(component,event,helper){
        var action = component.get("c.getsegmentReqPicklists");
        // Var Divisionval = component.find("businessUnitId").get("v.value");
        // Var Opcoval = component.find("opcoId").get("v.value");
        // console.log('check opco change '+event.getsource());
        action.setParams({"Opco": component.get("v.req.OpCo__c"),"Division": component.get("v.req.Req_Division__c")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var segmentmodel = data.getReturnValue();
                
                var segmentarray = [];
                segmentarray.push({value:'--None--', key:'--None--'}); 
                var segmentmap = segmentmodel.SegmentMappings;
                // console.log('Size of Segment Mappings--->'+segmentmap.length);
                for ( var key in segmentmap) {
                    segmentarray.push({value:segmentmap[key], key:key});
                }
                component.set("v.segmentList", segmentarray);
                
            }
        });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },
    
    EnterpriseReqJobcodeModal : function(component,event,helper){
        var action = component.get("c.getJobcodeReqPicklists");
        // Var Divisionval = component.find("businessUnitId").get("v.value");
        // Var Opcoval = component.find("opcoId").get("v.value");
        
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value") });
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var jobcodemodel = data.getReturnValue();
                
                var jobcodearray = [];
                jobcodearray.push({value:'--None--', key:'--None--'}); 
                var jobcodemap = jobcodemodel.JobCodeMappings;
                // console.log('jobcodemap'+jobcodemap);
                for ( var key in jobcodemap) {
                    jobcodearray.push({value:jobcodemap[key], key:key});
                }
                // console.log('harry'+jobcodearray);
                component.set("v.jobcodeList", jobcodearray);
                
            }
            component.set("v.workAddressFlag",true);
        });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },
    
    EnterpriseReqCategoryModal : function(component,event,helper){
        var action = component.get("c.getCategoryReqPicklists");
        // Var Divisionval = component.find("businessUnitId").get("v.value");
        // Var Opcoval = component.find("opcoId").get("v.value");
        
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value"),"Jobcode" :this.findComp(component,"JobcodeId").get("v.value")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var categorymodel = data.getReturnValue();
                
                var categoryarray = [];
                categoryarray.push({value:'--None--', key:'--None--'}); 
                var categorymap = categorymodel.CategoryMappings;
                // console.log('categorymap'+categorymap);
                for ( var key in categorymap) {
                    categoryarray.push({value:categorymap[key], key:key});
                }
                // console.log('harry'+categoryarray);
                component.set("v.CategoryList", categoryarray);
                
            }
        });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },    
    findComp : function(component,auraid){
        
        var inst=component.find(auraid);
        
        var flag=Array.isArray(inst);
        
        var retVal;
        
        if(flag ){
            retVal= inst[0];
        }else{
            retVal=inst;
        }
        return retVal;
    },    
    EnterpriseReqMainSkillModal : function(component,event,helper){
        var action = component.get("c.getMainskillReqPicklists");
        // Var Divisionval = component.find("businessUnitId").get("v.value");
        // Var Opcoval = component.find("opcoId").get("v.value");
        
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value"),"Jobcode" :this.findComp(component,"JobcodeId").get("v.value"),"Category":this.findComp(component,"CategoryId").get("v.value")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var MainSkillmodel = data.getReturnValue();
                
                var MainSkillarray = [];
                MainSkillarray.push({value:'--None--', key:'--None--'}); 
                var MainSkillmap = MainSkillmodel.MainSkillMappings;
                // console.log('MainSkillmap'+MainSkillmap);
                for ( var key in MainSkillmap) {
                    MainSkillarray.push({value:MainSkillmap[key], key:key});
                }
                // console.log('harry'+MainSkillarray);
                component.set("v.MainSkillList", MainSkillarray);
                
            }
        });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    },
    
    EnterpriseReqMainSkillid : function(component,event,helper){
        var action = component.get("c.getMainskillid");
        // Var Divisionval = component.find("businessUnitId").get("v.value");
        // Var Opcoval = component.find("opcoId").get("v.value");
        
        action.setParams({"Opco": this.findComp(component,"opcoId").get("v.value"),"Division": this.findComp(component,"businessUnitId").get("v.value"),"segment": this.findComp(component,"segmentId").get("v.value"),"Jobcode" :this.findComp(component,"JobcodeId").get("v.value"),"Category":this.findComp(component,"CategoryId").get("v.value"),"MainSkill":this.findComp(component,"MainSkillId").get("v.value")});
        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var MainSkillid = data.getReturnValue();
                component.set("v.MainSkillProductid", MainSkillid);
                // console.log('mainskillid'+ MainSkillid)
            }
        });
        this.EnterpriseReqReSet(component,event,helper);
        $A.enqueueAction(action);
    }, 
    
    EnterpriseReqReSet : function (component,event,helper){
        
        var opco=component.get("v.req.OpCo__c");
        
        if(component.find("businessUnitId")!=null && opco != 'AG_EMEA' && this.findComp(component,"businessUnitId").get("v.value")=='--None--'){
            
            this.findComp(component,"segmentId").set("v.value",'--None--');
        }
        
        if(component.find("segmentId")!=null && this.findComp(component,"segmentId").get("v.value")=='--None--'){
            // console.log('Value of segmentId id '+(component.find("segmentId").get("v.value")));
            this.findComp(component,"JobcodeId").set("v.value",'--None--');
            
        }
        
        if(component.find("JobcodeId")!=null && this.findComp(component,"JobcodeId").get("v.value")=='--None--'){
            // console.log('Value of JobcodeId id '+(component.find("JobcodeId").get("v.value")));
            this.findComp(component,"CategoryId").set("v.value",'--None--');
        }
        
        if(component.find("CategoryId")!=null && this.findComp(component,"CategoryId").get("v.value")=='--None--'){
            // console.log('Value of CategoryId id '+(component.find("CategoryId").get("v.value")));
            this.findComp(component,"MainSkillId").set("v.value",'--None--');
        }
        
        if(component.find("MainSkillId")!=null && this.findComp(component,"MainSkillId").get("v.value")=='--None--'){
            // console.log('Value of MainSkillId id '+(component.find("MainSkillId").get("v.value")));
            //component.find("MainSkillId").set("v.value",'--None--');
        }
        
    },
    
    handleSaveClick : function(component,event,helper){
        component.set("v.isDisabled", true);
        this.saveEnterpriseReq(component,event,helper);
    },
    
    saveEnterpriseReq : function(component,event,helper){
        component.set("v.isDisabled", true);
      
        if(component.get("v.inputOffCCPCompliantElse")!=null && component.get("v.inputOffCCPCompliantElse")==true){
            component.set("v.req.Req_OFCCP_Required__c",component.get("v.inputOffCCPCompliantElse"));    
        }
        
        if(component.get("v.req.Req_Skill_Specialty__c")=='--None--'){
            component.set("v.req.Req_Skill_Specialty__c","");
        }
       
        var oppReq = component.get("v.req");
        
        if(oppReq.OpCo__c =='AG_EMEA'){
            //console.log('test ag emea');
            component.set("v.req.Name","REQ-Opportunity");
			//Related titles
			//this.prepareRelatedTitles(component,event);
			var prepareRelatedTitles = component.find("relatedTitles");
			prepareRelatedTitles.prepareRelatedTerms();
			let relatedTitles = component.get("v.finalRelatedTitlesList");	
			if(relatedTitles.length > 0){
				oppReq.Suggested_Job_Titles__c = JSON.stringify(relatedTitles);
			}
        }
        
        var terms = component.get("v.selectedValue");
        oppReq.Req_Terms_of_engagement__c = terms ;
        
        var startDatevar=component.get("v.req.Start_Date__c");
        
        if(startDatevar=='' ||typeof startDatevar =='undefined'){
            oppReq.Start_Date__c=null;
        }
        
        if(oppReq.OpCo__c =='Aerotek, Inc'){
            var reqOpt = component.get("v.req.Req_OPT__c");
            if(reqOpt =='' ||typeof reqOpt =='undefined'){
                oppReq.Req_OPT__c=null; 
            }
        }
       
        var reqDateOpenVar=component.get("v.req.Req_Date_Client_Opened_Position__c");
        
        if(reqDateOpenVar=='' ||typeof reqDateOpenVar =='undefined'){
            oppReq.Req_Date_Client_Opened_Position__c=null;
        }
        
        var reqInterviewDateVar=component.get("v.req.Req_Interview_Date__c");
        
        if(reqInterviewDateVar=='' ||typeof reqInterviewDateVar =='undefined'){
            oppReq.Req_Interview_Date__c=null;
        }
      
        var reqOpenDateVar=component.get("v.req.Req_RRC_Open_Date__c");
        
        if(reqOpenDateVar=='' ||typeof reqOpenDateVar =='undefined'){
            oppReq.Req_RRC_Open_Date__c=null;
        }
        
        
        var closeDatevar=component.get("v.req.CloseDate");
        if(closeDatevar=='' ||typeof closeDatevar =='undefined'){
            oppReq.CloseDate=null;
        }
        
        //Strategic Opportunity
        var soppid=component.get("v.soppId");
        var oppRecTypeId = component.get("v.oppRecTypeId");
        if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){
            if(oppRecTypeId == '012U0000000DWp4IAG'){
                oppReq.Req_Opportunity__c = component.get("v.soppId");
            }else{
                oppReq.Originating_Opportunity__c = component.get("v.soppId");
            }
        }
        
        
        var oppty = JSON.stringify(oppReq);
        var accStg = component.get("v.account");
        var acctName = JSON.stringify(accStg);
        var hStg = component.get("v.contact");
        var mgr = JSON.stringify(hStg);
        var prodid=component.get("v.MainSkillProductid");
       
        var flagval=this.validateEnterpriseReq(component, event, helper);
        
        //Validation for Enterprise Req call.
        if(flagval){
            
            var oppClosedate = component.get("v.req.CloseDate");
            var cdatefield = component.find("inputCloseDate");
            //Close Date
            var pillData = component.get("v.automatchSkills");
            var pillList = [];
            var favSkills ='';
            var regularSkills ='';
            var finalSkills ='';
            if(pillData!=null){
                for (var j=0; j<pillData.length; j++) {
                    if(pillData[j].required === "true" || pillData[j].required === true){
                        favSkills += pillData[j].key + ',';
                        var jsonFavSkill = { "name": pillData[j].key, "favorite":true };
                        pillList.push(jsonFavSkill);
                    }else{
                        regularSkills += pillData[j].key + ',';
                        var jsonSkill = { "name": pillData[j].key, "favorite":false };
                        pillList.push(jsonSkill);
                    }   
                }
            }
            
            //Alternate Delivery
            var supportRequestList = component.get("v.supportRequestList");
            var altDeliveryList = JSON.stringify(supportRequestList);
            
            finalSkills = JSON.stringify(pillList);
            component.set("v.oppSkills",finalSkills);
            
            var officeInfoId = component.get("v.organization.Id");
            //job title
            var jtitle = component.get("v.newJobTitle");
            // console.log("new job"+jtitle);
            //Create Enterprise Req
            
            /*console.log(component.get("v.oppSkills"));
                    console.log('passed');
                    console.log(finalSkills);*/
            var optyStr = component.get("v.oppSkills");
            var reqNewSkills = component.get("v.oppSkills");
            let txSkills=component.get("v.skills");
            reqNewSkills=JSON.stringify(txSkills);
            if(oppReq.OpCo__c =='AG_EMEA'){
                let txSkills=component.get("v.skills");
                reqNewSkills=JSON.stringify(txSkills);
                component.set("v.skills","");
            }
            
            //Spinner Starting
            component.set("v.spinner", true);
            let spinner = component.find("reqSpinner");
            $A.util.toggleClass(spinner, "slds-hide");
            //End Spinner Starting
            
            let proactiveSubmittal = component.get("v.proactiveSubmittal");
            if(component.get("v.isProactiveSubmittal")) {
                proactiveSubmittal.Status = component.get("v.proactiveSubmittalStatus");
            }
            var actionopp = component.get("c.saveEnterpriseReq");
            actionopp.setParams({"oppStr":  oppty,
                                 "accName": acctName,
                                 "hiringMgr": mgr,
                                 "skills":component.get('v.stringSkills'),"Mainskillid":prodid, "officeId":officeInfoId,
                                 "altDeliveryList":altDeliveryList,
                                 "proactiveSubmittal":proactiveSubmittal});
            
            actionopp.setCallback(this, function(response) {
                //Spinner Stopping
                $A.util.toggleClass(spinner, "slds-hide");
                component.set("v.spinner", false);
                //End Spinner Stopping
                
                var oppId = response.getReturnValue();
              
                component.set("v.isDisabled", true);
                // Opp successfully created.
                if (response.getState() === "SUCCESS") {
                    // console.log('Oppid--->'+oppId);
                    var errors = response.getError();
                    //console.log(errors);
                    var opptyId = response.getReturnValue();
                    
                    component.set("v.newOpportunityId",opptyId);
                    
                    //Adding w.r.t S-228904
                    if(component.get("v.reqMetrics.Skill_Speciality_From_Model__c")!=undefined && component.get("v.reqMetrics.F_NF_From_Model__c")!=undefined){
                        var reqMetrics = component.find('reqMetrics');
            			component.set("v.reqMetrics.OpptyId__c",opptyId);
        				reqMetrics.saveMetricDataOnCreate(); 
                    }                                       
                    //End of Adding w.r.t S-228904
                    
                    let istxOpcoEmea=component.get("v.isOpcoEmea");
                    if(istxOpcoEmea!=null && istxOpcoEmea==true){
                        component.set("v.skills",null);
                        component.set("v.txSkill",null);
                        component.set("v.suggestedjobTitle",null);
                        component.set("v.normalizedData",null);
                        component.set("v.rawData",null);
                    }    
                    
                   
                                if(component.get("v.isProactiveSubmittal")) {
                                    this.showSuccessToast(component);
                                    this.redirectForProactiveSubmittal(component, event, helper);
                                    
                                }
                                else {
                                    this.redirectToOpportunityURL(component, event, helper);
                                }
                                
                                
                            }else if (response.getState() === "ERROR") {
                                
                                component.set('v.isDisabled', false);
                                var errors = response.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        //this.showError(errors[0].message);
                                        component.set("v.errorMessageVal",errors[0].message);
                                    }else if(errors[0] && errors[0].fieldErrors[0] && errors[0].fieldErrors[0].message){
                                        //this.showError(errors[0].fieldErrors[0].statusCode,"An error occured!");
                                        component.set("v.errorMessageVal",errors[0].fieldErrors[0].statusCode);
                                    }else if(errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message){
                                        //this.showError(errors[0].pageErrors[0].statusCode,"An error occured!");
                                        component.set("v.errorMessageVal",errors[0].pageErrors[0].statusCode);
                                    }else{
                                        //this.showError('An unexpected error has occured. Please Contact System Administrator!');
                                        component.set("v.errorMessageVal",'An unexpected error has occured. Please Contact System Administrator!');
                                    }
                                   
                                  }
                             }
                      });
            component.set('v.isDisabled', true);
            $A.enqueueAction(actionopp); 
            return ;
        }
        component.set("v.isDisabled", false);
        
    },
    
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'sticky',
            title: title,
            message: errorMessage,
            duration:' 5000',
            type: 'error'
        });
        toastEvent.fire();
    },
    
    validateEnterpriseReq : function (component, event, helper, validation=true ) {
        
        var error_count = component.get("v.validationResponse.error_count");       
        var validated 	= component.get("v.validationResponse.isvalid");
        
        if(!validated){
			if(typeof(component.get("v.focus_id")) != "undefined")
            document.getElementById( component.get("v.focus_id") ).scrollIntoView({ behavior: 'instant', block: 'center' });
        }
        
        var recordType= component.get("v.recordType");
        var aname = component.get("v.account.Name");
        var oppClosedate = component.get("v.req.CloseDate");
        var cdatefield = this.findComp(component,"inputCloseDate");
        var cname= component.get("v.contact.Name");
        var stageValue = component.get("v.req.StageName");
        var oppname= component.get("v.req.Name");
        var oppfield = this.findComp(component,"inputOppName");
        var productValue= component.get("v.req.Req_Product__c");
        var termOfEngagementValue= component.get("v.req.Req_Terms_of_engagement__c");
        var mspName= component.get("v.req.MSP__c");
        var governmentWorkName= component.get("v.req.Government_Work__c");        
        var currencyValue=component.get("v.req.Currency__c");
        var duratiVarName=component.get("v.req.DurationVarName");
        var durationField=this.findComp(component,"inputDurationName");
        var durationValue=component.get("v.req.Req_Duration__c")
        var durationunitValue=component.get("v.req.Req_Duration_Unit__c");
        var alternatedeliverySharedPosns = component.get("v.req.Req_RRC_Positions__c");
        var totalPositionsValue=component.get("v.req.Req_Total_Positions__c");
        var totalPositionField=this.findComp(component,"inputTotalPosn");
        var jobtitle=component.get("v.req.Req_Job_Title__c");
        var lengthExceededQualification=component.get("v.req.Req_Qualification__c");
        var requiredSkillPills = component.get("v.req.EnterpriseReqSkills__c");
        
        var salaryMinValue=component.get("v.req.Req_Salary_Min__c");
        var salaryMinValueField=this.findComp(component,"inputSalaryMin");
        
        var salaryMaxValue=component.get("v.req.Req_Salary_Max__c");
        var salaryMaxValueField=this.findComp(component,"inputSalaryMax");
        
        var bonusValue=component.get("v.req.Req_Bonus__c");
        var bonusField=this.findComp(component,"inputBonus");
        
        var worksitestreetValue=component.get("v.req.Req_Worksite_Street__c");
        var worksitestreetField=this.findComp(component,"inputworkSt");
        
        var worksitecityValue=component.get("v.req.Req_Worksite_City__c");
        var worksitecityField=this.findComp(component,"inputworkCity");
        
        var worksiteStateValue=component.get("v.req.Req_Worksite_State__c");
        var worksiteStateField=this.findComp(component,"inputworkState");
        
        var worksitepostalcodeValue=component.get("v.req.Req_Worksite_Postal_Code__c");
        var worksiteZipField=this.findComp(component,"inputworkZip");
        
        var worksitecountryValue=component.get("v.req.Req_Worksite_Country__c");
        var worksitecountryField=this.findComp(component,"inputworkCountry");
        
        var officeValue= component.get("v.organization.Name");
        var apexContext=false;
        
        var reqTotalFilled=component.get("v.req.Req_Total_Filled__c");
        var reqTotalPositions=component.get("v.req.Req_Total_Positions__c");
        
        var requiredBillRateMax=component.get("v.req.Req_Bill_Rate_Max__c");
        var requiredBillRateMin=component.get("v.req.Req_Bill_Rate_Min__c");
        
        var percentageFee=component.get("v.req.Req_Fee_Percent__c");
        var flatFee=component.get("v.req.Req_Flat_Fee__c");
        var jobDescription=component.get("v.req.Description");
        var payrateMax=component.get("v.req.Req_Pay_Rate_Max__c");
        var payrateMin=component.get("v.req.Req_Pay_Rate_Min__c");
        var requiredQualification=component.get("v.req.Req_Qualification__c");
        var reqStandardBurden=component.get("v.req.Req_Standard_Burden__c");
        var rateFrequency=component.get("v.req.Req_Rate_Frequency__c");
        var recordType=component.get("v.recordType");
        
        var whyPosition = component.get("v.req.Req_Why_Position_Open_Details__c");
        var skillDetails = component.get("v.req.Req_Skill_Details__c");
        var workEnv = component.get("v.req.Req_Work_Environment__c");
        var reqEvp = component.get("v.req.Req_EVP__c");
        var addnlInfo = component.get("v.req.Req_Additional_Information__c");
        var reqComp = component.get("v.req.Req_Compliance__c");
        var extJobDesc = component.get("v.req.Req_External_Job_Description__c");
        var busChlge = component.get("v.req.Req_Business_Challenge__c");
        var intExtCust = component.get("v.req.Req_Internal_External_Customer__c");
        var impactExt = component.get("v.req.Req_Impact_to_Internal_External_Customer__c");
        var compInfo = component.get("v.req.Req_Competition_Info__c");
        var appvlProcess = component.get("v.req.Req_Approval_Process__c");
        var intervwInfo = component.get("v.req.Req_Interview_Information__c");
        var altDeliType = component.get("v.req.Req_Delivery_Type__c");
        var altDeliOffice = component.get("v.req.Req_Secondary_Delivery_Office__c");
        var opco=component.get("v.req.OpCo__c");
        var opcoValVar=component.get("v.opcoVarName");
        
        var businessUnit=component.get("v.req.Req_Division__c");
        var businessUnitValVar=component.get("v.req.businessUnitVarName");
        
        var descriptionId=this.findComp(component,"inputSkillDescription");
        var mainskill=this.findComp(component,"MainSkillId");
        var jobAuraDesc = this.findComp(component,"inputDescription");
        
        var qualfield = this.findComp(component,"inputQualification");
        var inpPosnfield = this.findComp(component,"inputPositiondetails");
        var inpSkillDescfield = this.findComp(component,"inputSkillDescription");
        var workEnvfield = this.findComp(component,"workEnvironment");
        var evpfield = this.findComp(component,"evp");
        var addInfofield = this.findComp(component,"addInfo");
        var ipCompfield = this.findComp(component,"inputCompliance");
        var extCommfield = this.findComp(component,"extCommunities");
        var busChalfield  =  this.findComp(component,"inputbusChallenge");
        var intextfield = this.findComp(component,"intExternal");
        var impactIntfield = this.findComp(component,"impactIntExt");
        var compInfofield = this.findComp(component,"reqCompetitionInfo");
        var approvfield = this.findComp(component,"reqApprovalInfo");
        var IntInfofield = this.findComp(component,"intInfo");
        var altOfficefield = this.findComp(component, "reqAltDelOffice");
        // Dafaulting values to zero for Req_AtleastOneSubmittal_Reqd_Interview validation rule.
        
        var reqSubInterviewing=0;
        var reqSubOfferExtended=0;
        var reqSubNotProceeding=0;
        var reqSubLPQInProcess=0;
        var reqSubReferening=0;
        var reqSubOnHold=0;
        var reqSubOfferAccepted=0;
        var toastMessages=[];
        
        this.validateAddress(component,event);
        var addfieldMap=component.get("v.addFieldMessagedMap");
        var isTGSReq = component.get("v.req.Req_TGS_Requirement__c");
        var isTGSPracEng = component.get("v.req.Practice_Engagement__c"); //component.find("isTGSPEId").get("v.value");
        var isGSLocation = component.get("v.req.GlobalServices_Location__c"); //component.find("isTGSLocationId").get("v.value");
        var isEmpAlign = component.get("v.req.Employment_Alignment__c"); //component.find("isEmploymentAlignmentId").get("v.value");
        var isFnDsMk = component.get("v.req.Final_Decision_Maker__c"); //component.find("isFinaDecMakId").get("v.value");
        var isIntTvReq = component.get("v.req.Is_International_Travel_Required__c"); //component.find("isInterNationalId").get("v.value");
        var isInternalHire = component.get("v.req.Internal_Hire__c"); //component.find("isInternalHireId").get("v.value");
        var isBackfill = component.get("v.req.Backfill__c");
        var isNational = component.get("v.req.National__c");
        if(typeof addfieldMap!='undefined' && addfieldMap!=null && JSON.stringify(addfieldMap)!="{}"){
            validated = false;
        }
        
        var worksiteStreetValMsg= component.get("v.req.Req_Worksite_Street__c");
        var worksiteCityValMsg = component.get("v.req.Req_Worksite_City__c");
        var worksiteStateValMsg = component.get("v.req.Req_Worksite_State__c");
        var worksitePostalValMsg = component.get("v.req.Req_Worksite_Postal_Code__c");
        var worksiteCountryValMsg = component.get("v.req.Req_Worksite_Country__c");
        
        if(component.get("v.req.isOpcoRegular") === "true"){
            if(oppClosedate == null || oppClosedate==''){
                validated = false;
                error_count++;
                component.set("v.closeDateVarName","Close Date cannot be blank.");
            }else{
                component.set("v.closeDateVarName","");
            } 
        }
       
        

		if(productValue == '--None--' || typeof productValue == "undefined"){
            validated = false;
            component.set("v.ProductVarName","Placement Type cannot be blank.");
        }else{
            component.set("v.ProductVarName","");
        }
		
        var reqInt= component.get("v.req.Req_Interview_Information__c");
        
        if(reqInt!=null && reqInt!='' && reqInt.length>2000){
            component.set("v.InterviewInfoVarName","Interview Information can not exceed 2000.");
            validated = false;
            error_count++;
        }else {
            component.set("v.InterviewInfoVarName","");
        }
        
        var evp= component.get("v.req.Req_EVP__c ");
        
        if(evp!=null && evp!='' && evp.length>2000){
            component.set("v.evpVarName","EVP can not exceed 2000.");
            validated = false;
            error_count++;
        }else {
            component.set("v.evpVarName","");
        }
        
        
        
       	if( !($A.util.isUndefined( lengthExceededQualification ) || $A.util.isEmpty( lengthExceededQualification ) ) ){
            
            if( lengthExceededQualification.length>2000){
                component.set("v.qualificationVarName","Additional Skills & Qualifications cannot be greater than 2000 characters.");
                //qualfield.set("v.errors", [{message:"Additional Skills & Qualifications cannot be greater than 2000 characters"}]);
                validated = false;
                error_count++;
            }else {
                component.set("v.qualificationVarName","");
                //qualfield.set("v.errors", null);
            }
        }
     
        
        
        if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
           && recordType=='Req' && apexContext==false && 
           (jobDescription!=null && jobDescription!='' && typeof jobDescription != "undefined" && jobDescription.length< 25)){
            component.set("v.descriptionVarName", "Job Description should have at least 25 characters.");
            validated=false;
            error_count++;
            
        }
        if(validated && stageValue=='Qualified' && recordType=='Req' && apexContext==false && typeof productValue != "undefined" && productValue == 'Permanent'){ 
            var percentaFeeValue = component.find("inputFeePerc").get("v.value");
            var flatFeeValue = component.find("inputFlatFee").get("v.value");
            if((typeof percentaFeeValue == "undefined" || percentaFeeValue == "") && (typeof flatFeeValue == "undefined" || flatFeeValue == "")){
                component.set("v.flatFeeValMsg", "Fee % or Flat Fee is required.");
                validated=false;
                error_count++;
            }else if((typeof flatFeeValue == "undefined" || flatFeeValue == "") && (typeof percentaFeeValue == "undefined" || percentaFeeValue == "")){
                component.set("v.flatFeeValMsg", "Fee % or Flat Fee is required.");
                validated=false;
                error_count++;
            }            
        }
        var pillData = component.get("v.automatchSkills");
        //console.log('pill data outside'+JSON.stringify(pillData));
        let istxOpcoEmea=component.get("v.isOpcoEmea");
        //console.log('istxOpcoEmea::'+istxOpcoEmea);
        //console.log('istxOpcoEmeaValues::'+JSON.stringify(component.get("v.skills")));
        pillData=component.get("v.skills");
        if(istxOpcoEmea!=null && istxOpcoEmea=="true"){
            pillData=component.get("v.skills");
        }
       
       
        var criteriaValidate=true;
        if(validated==false){
            
        }else{
            //start of c val
            
            //EMEA Validations
            var opcoEmea = component.get("v.isOpcoEmea");
            var teksystemFlag=component.get("v.isOpcoTek");
            if(opcoEmea == 'true' || teksystemFlag==true){
                
                
                
                if(reqEvp !=null && reqEvp!='' && reqEvp.length>2000){
                    evpfield.set("v.errors", [{message:"EVP cannot be greater than 2000 characters"}]);
                    criteriaValidate=false;
                    error_count++;
                }else {
                    evpfield.set("v.errors", null);
                }
               
                
                
                if(extJobDesc !=null && extJobDesc!='' && extJobDesc.length>500){
                    extCommfield.set("v.errors", [{message:"External Job Description cannot be greater than 500 characters"}]);
                    criteriaValidate=false;
                    error_count++;
                }else {
                    extCommfield.set("v.errors", null);
                }
               
                if(busChlge !=null && busChlge!='' && busChlge.length>4000){
                    busChalfield.set("v.errors", [{message:"Business Challenge cannot be greater than 4000 characters"}]);
                    criteriaValidate=false;
                    error_count++;
                }else {
                    busChalfield.set("v.errors", null);
                }
                
                if(intervwInfo !=null && intervwInfo!='' && intervwInfo.length>2000){
                    IntInfofield.set("v.errors", [{message:"Interview Information cannot be greater than 2000 characters"}]);
                    criteriaValidate=false;
                    error_count++;
                }else {
                    IntInfofield.set("v.errors", null);
                }
                
            }
            
           
            //Req_AtleastOneSubmittal_Reqd_Interview  validation rule.
            if(validated && (stageValue=='Interviewing' || stageValue=='Closed Won') && apexContext==false &&
               (recordType=='Req' || recordType=='Proactive') && ( reqSubInterviewing>0 || reqSubOfferExtended>0 || reqSubNotProceeding>0 || 
                                                                  reqSubLPQInProcess>0 || reqSubReferening>0 || reqSubOnHold>0) ){
                // console.log('Req_AtleastOnePlaced_Required_ClosedPart  validation rule');
                // toastMessages.push('At least one Candidate must be in Interview status before moving to â€˜Interviewingâ€™ stage or above.If you are on the edit page, please save Req in \'Qualified\' stage, change one more Candidates to Interviewing. Then, update stage to \'Interviewing\' or above.');
                component.set("v.StageVarName","At least one Candidate must be in Interview status before moving to â€˜Interviewingâ€™ stage or above.If you are on the edit page, please save Req in \'Qualified\' stage, change one more Candidates to Interviewing. Then, update stage to \'Interviewing\' or above.");
                criteriaValidate=false;
                error_count++;
            }else if(validated){
                component.set("v.StageVarName","");
                //criteriaValidate=true;
            }
           
           
            
            
            //Req_AtleastOneSubmittal_Reqd_Pend_Start  validation rule.
            if(validated && (stageValue=='Pending Start' || stageValue=='Closed Won') && apexContext==false &&
               (recordType=='Req') && (reqSubOfferAccepted>0 || reqTotalFilled>0) ){
                // console.log('Req_AtleastOneSubmittal_Reqd_Pend_Start  validation rule');
                // toastMessages.push('At least one Candidate must be in Interview status before moving to â€˜Interviewingâ€™ stage or above.If you are on the edit page, please save Req in \'Qualified\' stage, change one more Candidates to Interviewing. Then, update stage to \'Interviewing\' or above.');
                component.set("v.StageVarName","At least one submittal should be in a Interview status before stage can be moved to Interviewing.");
                criteriaValidate=false;
                error_count++;
            }else if(validated){
                component.set("v.StageVarName","");
                //criteriaValidate=true;
            }
           
            // console.log('requiredBillRateMax------>'+requiredBillRateMax);
            
            //Req_Bill_Rate_Max_Required validation rule.
            if(validated  && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
               && recordType=='Req' && apexContext==false && (productValue=='Contract' || productValue=='Contract to Hire') && 
               (requiredBillRateMax==null || requiredBillRateMax=='' || typeof requiredBillRateMax == "undefined")){
                // console.log('Req_Bill_Rate_Max_Required validation rule.');
                //toastMessages.push('Bill Rate Max is required.');
                component.set("v.billRateMaxValMsg","Bill Rate Max is required.");
                criteriaValidate=false;
                error_count++;
            }else{
                component.set("v.billRateMaxValMsg","");
                //criteriaValidate=true;
            }
            
         
           
            var draftReason=component.get("v.req.Req_Draft_Reason__c");
            
            if(validated  && (stageValue=='Draft') && (opco=='Aerotek, Inc' || opco=='TEKsystems, Inc.') && 
               (draftReason==null || draftReason==''||typeof draftReason=="undefined" || draftReason=='--None--')
               && recordType=='Req' && apexContext==false ){
                component.set("v.drafReasonVarName","Draft Reason is required");
                criteriaValidate=false;
                error_count++;
            }else{
                component.set("v.drafReasonVarName","");
                //criteriaValidate=true;
            }
            
            //Req_DurationUnit_Required  validation rule.
            if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') 
               && apexContext==false && (recordType=='Req' || recordType=='Proactive') && (productValue=='Contract' || productValue=='Contract to Hire') 
               && (durationunitValue==null || durationunitValue=='' || durationunitValue == "--None--") ){
              
                component.set("v.durationUnitValMsg","Duration Unit is required.");
                criteriaValidate=false;
                error_count++;
            }else{
                component.set("v.durationUnitValMsg","");
                
            }
            
            //Req_Duration_Required  validation rule.
            if(validated && stageValue=='Qualified'  
               && recordType=='Req'  && (productValue=='Contract' || productValue=='Contract to Hire') 
               && (durationValue==null || durationValue=='' || typeof durationValue == "undefined") ){
               
                component.set("v.durationVarName","Duration  is required.");
                criteriaValidate=false;
                error_count++;
            }else{
                component.set("v.durationVarName","");
                
            }
            
            
            
            //inputDescription
            if(validated && apexContext==false && (jobDescription!=null && jobDescription!='' && jobDescription.length>32000)){
                component.set("v.descriptionVarName","Job Description cannot exceed 32000.");
                criteriaValidate=false;
                error_count++;
            }else {
                component.set("v.descriptionVarName","");
            }
            
            //Req_Pay_Rate_Max_Required  validation rule.
            if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') &&  apexContext==false
               && recordType=='Req' && (productValue=='Contract' || productValue=='Contract to Hire') && 
               (payrateMax==null || payrateMax=='' || typeof payrateMax == "undefined" )){
                // console.log('Req_Pay_Rate_Max_Required  validation rule.');
                //toastMessages.push('Pay Rate Max is required.');
                component.set("v.payRateMaxValMsg","Pay Rate Max is required.");
                criteriaValidate=false;
                error_count++;
            }else{
                component.set("v.payRateMaxValMsg","");
                //  criteriaValidate=true;
            }
            
           
            //Req_Salary_Max_Required validation rule.
            if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won') && apexContext==false
               && recordType=='Req' && (productValue=='Permanent' || productValue=='Contract to Hire')  && (salaryMaxValue==null || salaryMaxValue=='' || typeof salaryMaxValue == "undefined")){
                // console.log('Req_Salary_Max_Required validation rule.');
                //toastMessages.push('Salary Max is required.');  
                component.set("v.salaryMaxValMsg","Salary Max is required.");
                criteriaValidate=false;
                error_count++;
            }else{
                component.set("v.salaryMaxValMsg","");
                //  criteriaValidate=true;
            }
            
            //Req_Salary_Min_Required  validation rule.
            
            
            
            //Req_Standard_Burden_Required  validation rule.
            if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
               && apexContext==false && recordType=='Req' && apexContext==false && (productValue=='Contract' || productValue=='Contract to Hire') && 
               (reqStandardBurden==null || reqStandardBurden=='' || typeof reqStandardBurden == "undefined")){
                // console.log('Req_Standard_Burden_Required  validation rule.');
                //toastMessages.push('Standard Burden % is required.');
                component.set("v.standardBurdenValMsg","Total Burden % is required.");
                criteriaValidate=false;
                error_count++;
            }else {
                component.set("v.standardBurdenValMsg","");
                //  criteriaValidate=true;
            }
            
            if(validated && (stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Pending Start' || stageValue=='Closed Won')
               && apexContext==false && recordType=='Req' && apexContext==false && productValue=='Permanent'  && 
               (termOfEngagementValue==null || termOfEngagementValue=='' || typeof termOfEngagementValue == "undefined" || termOfEngagementValue == '--None--')){
                // console.log('Req_Standard_Burden_Required  validation rule.');
                //toastMessages.push('Standard Burden % is required.');
                component.set("v.EngagementVarName","Terms of Engagements is required.");
                criteriaValidate=false;
                error_count++;
            }else {
                component.set("v.EngagementVarName","");
                //  criteriaValidate=true;
            }
            
            var segVar=component.find("segmentId");
            var jobVar=component.find("JobcodeId");
            var catVar=component.find("CategoryId");
            
                       
            if(validated && apexContext==false && oppname != null && oppname !='' && oppname.length > 120){
                component.set("v.oppVarName","Max length can not exceed 120.");
                criteriaValidate = false;
                error_count++;
            }else {
                if(validated){
                    component.set("v.oppVarName","");
                }
            }
            
            /* 
 			Showket - Cannot find it in form , so commenting it out, please review and uncomment if necessery           
			*/
            //console.log('RATE FREQ '+rateFrequency);
            if(validated && apexContext==false && (opco!='TEKsystems, Inc.' && opco!='Aerotek, Inc')   && (recordType=='Req' || recordType=='Proactive') && ((stageValue=='Qualified' || stageValue=='Presenting' || stageValue=='Interviewing' || stageValue=='Closed Won'))
               && (productValue=='Contract' || productValue=='Contract to Hire') && (rateFrequency=='--None--' || typeof rateFrequency == "undefined")){
                //console.log('RF Val RUle.'+validated + 'rate freqw'+rateFrequency);
                component.set("v.rateFrequencyValMsg","Please Select Rate Frequency");
                //console.log('********** - Rate Frequency not on the layout - **********' );
                criteriaValidate=false;
                error_count++;
            }else if(criteriaValidate){
                component.set("v.rateFrequencyValMsg","");
            }
  
        }
        
        if(validated == false || criteriaValidate == false ){
            var message = 'There are ' + error_count + ' errors that need to be addressed before creating this req in a ' + component.get("v.req.StageName") + ' state.';
            this.showToast(message, 'Req Validation Error', 'error');
        }
        
        return (validated && criteriaValidate);
        
    },
    
    showErrorToast : function(message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'You\'ve missed something while creating the req.',
            message: message,
            messageTemplate: '',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'sticky'
        });
        toastEvent.fire();
    },
    
    showSuccessToast : function(component) {
        let msg = '';
        if(component.get("v.isOpcoTek") || component.get("v.isOpcoAerotek")) {
            msg =  "New opportunity successfully created. Please allow 15 minutes before creating an ESF.";
        }
        else {
            msg = "New opportunity successfully created."
        }
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": msg,
            "duration": 7000,
            "type": 'success',
        });
        toastEvent.fire();
    },
    
    topSkillsValidation : function(component,event){
        // console.log('handled');
        var isFavSkillsSelected = event.getParam("isSelectionDone");
        var isErrorShown = component.get("v.istopSkillsMessage");
        // console.log('event param'+isFavSkillsSelected);
        if(isFavSkillsSelected === true){
            // console.log('valid one');
            component.set("v.istopSkillsMessage", "true");
            //console.log(component.get("v.automatchSkills"));
        }
    },
    
    redirectToOpportunityURL : function (component, event, helper) {
        let optyUrl;
        let proactiveSubmittal = component.get("v.proactiveSubmittal");
        event.preventDefault();
        optyUrl = "/lightning/r/Opportunity/"+component.get("v.newOpportunityId")+"/view";
        if(proactiveSubmittal !== null && proactiveSubmittal !== undefined) {
            optyUrl = "/lightning/r/Contact/"+component.get("v.proactiveSubmittal.ShipToContactId")+"/view";
        }        
        let urlEvent = $A.get("e.force:navigateToURL");        
        urlEvent.setParams({
            "url": optyUrl,
            "isredirect": true
        });
        urlEvent.fire();        
		component.find('container').destroy();
    },
    
    redirectToStartURL : function (component, event, helper){
        var objAPI =  component.get("v.sobjType");
        var isTgsreq = component.get("v.isTgsParentReq");
        var strtUrl;
        var soppid=component.get("v.soppId");
        
        //console.log('soppid---->'+soppid);
        //console.log('start obj v1 '+objAPI);
        event.preventDefault();
        
        if(isTgsreq){
            strtUrl ="/lightning/r/Opportunity/"+component.get("v.req.Req_Opportunity__c")+"/view";
        }else if(objAPI === 'Account'){
            strtUrl ="/lightning/r/Account/"+component.get("v.recordId")+ "/view";
        }else if(objAPI === 'Contact'){
            strtUrl ="/lightning/r/Contact/"+component.get("v.recordId")+"/view";
        }else{
            strtUrl ="/lightning/r/Opportunity/"+component.get("v.recordId")+"/view";
        }
        
        var soppid=component.get("v.soppId");
        if(soppid!='' && soppid!=null && typeof soppid!='undefined' ){ //Strategic Opportunity redirection
            strtUrl ="/lightning/r/Opportunity/"+soppid+"/view";
        }
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": strtUrl,
            "isredirect":true
        });
        urlEvent.fire();
    },
    
    
    loadBusinessUnitValues : function(component,event,opcoValue){
        var opcoVal;
        
        if(opcoValue=='None'){
            opcoVal = event.getSource().get("v.value"); 
        }else{
            opcoVal=opcoValue;
        }
        
        var opco = component.get("v.req.OpCo__c");
        if(opco=='Aerotek, Inc'){
            component.set("v.isOpcoAerotek",true);
            component.set("v.isOpcoEmea", "false");
            component.set("v.isOpcoRegular", "false");
            component.set("v.isOpcoTek","false");
        }else if(opco=='TEKsystems, Inc.'){
            component.set("v.isOpcoTek",true);
            component.set("v.isOpcoEmea", "false");
            component.set("v.isOpcoRegular", "false");
            component.set("v.isOpcoAerotek","false");
        }else if(opco=='AG_EMEA' || opco=='AG EMEA'){
            component.set("v.isOpcoEmea", "true");
            component.set("v.isOpcoRegular", "false");
            component.set("v.isOpcoTek",false);
            component.set("v.isOpcoAerotek",false);
        }else{
            component.set("v.isOpcoRegular", "true");
            component.set("v.isOpcoEmea", "false");
            component.set("v.isOpcoTek","false");
            component.set("v.isOpcoAerotek","false");
        }
        
        var aerotekArray = [];
        var tekArray = [];
        var allegisArray = [];
        var agsArray = [];
        var apArray = [];
        var mlaArray = [];
        
        component.set("v.MainSkillProductid",null);
        
        
        var busUnitMap = [];
        if(opcoVal=='Aerotek, Inc'|| opcoVal=='TEKsystems, Inc.'){
            component.set("v.MainskillOpco",true);
            
            
        }
        else{
            component.set("v.MainskillOpco",false);
        }
        
        
        var opArray=component.get("v.opcoDivisionList");
        
        // console.log('Load Business Unit Values--->'+opArray);
        
        for (var i=0; i<opArray.length; i++) {
            
            /*console.log('Values from dependent picklists-->'+opArray[i].value);
            console.log('Values from dependent picklists-->'+opArray[i].key);
            console.log('OPCO String--->'+opcoVal);
            console.log('opcoVal-->'+opArray[i].key ==opcoVal);*/
            
            if(opcoVal=='AG_EMEA'){
                opcoVal='AG EMEA';
            }
            
            if(opArray[i].key ==opcoVal){
                var res=opArray[i].value;
                var spltArry=res.toString().split(",");
                /*console.log('Split Result is--->'+res);
                console.log('Split array is--->'+spltArry);*/
                for(var j=0;j<spltArry.length;j++){
                    busUnitMap.push(res[j]);
                }
                
            }
            //  break;
        }
        
        var busUnitArray = [];
        busUnitArray.push({value:'--None--', key:'--None--'});
        for ( var key in busUnitMap) {
            if(busUnitMap[key]!='' || typeof busUnitMap[key]!='undefined')
                busUnitArray.push({value: busUnitMap[key], key:busUnitMap[key]});
        }
        

       
        component.set("v.businessUnitList", busUnitArray);
        var busUnitId = this.findComp(component,"businessUnitId");
        
        if(busUnitId !='' && typeof busUnitId!='undefined'){
            busUnitId.set("v.value", '--None--');
        }
        
        
    },
    validateAddress:function(cmp,event){
        var oppAddress=cmp.get("v.req");
        var fieldMessages=[];
        var addFielMessagedMap={};
        var opcoName=oppAddress.OpCo__c;
        var OpCoValue = cmp.get("v.req.OpCo__c");        
        if(OpCoValue == 'TEKsystems, Inc.'){
            var TGSReqValue = cmp.find("isTGSReqId").get("v.value");
            if(TGSReqValue == 'No' || TGSReqValue == '--None--'){
                if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
                    // cmp.set("v.streetErrorVarName","Street cannot be blank.");
                    // fieldMessages.push("Street cannot be blank.");
                    addFielMessagedMap['streetErrorVarName']="Street cannot be blank.";
                }else{
                    cmp.set("v.streetErrorVarName","");
                }                                
                if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
                    addFielMessagedMap['cityTypeErrorVarName']="City cannot be blank.";
                }else{
                    cmp.set("v.cityTypeErrorVarName","");
                }                
                var countryManFlag=false;
                if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
                    
                    addFielMessagedMap['countryVarName']="Country cannot be blank.";
                }else{
                    countryManFlag=true;
                    cmp.set("v.countryVarName","");
                }                                
                //if((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && countryManFlag && (oppAddress.Req_Worksite_Country__c=='United States' || oppAddress.Req_Worksite_Country__c=='Canada')) {
                //Adding w.r.t Story S-179373
                if((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && (oppAddress.Req_Worksite_Country__c=='United States' || oppAddress.Req_Worksite_Country__c=='Canada')) {
                    addFielMessagedMap['stateErrorVarName']="State cannot be blank.";
                }else{
                    cmp.set("v.stateErrorVarName","");
                }                                                
                if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
                    
                    addFielMessagedMap['zipErrorVarName']="Postal Code cannot be blank.";
                }else{
                    cmp.set("v.zipErrorVarName","");
                }
                
                
                if(typeof addFielMessagedMap!='undefined' && addFielMessagedMap!=null && Object.keys(addFielMessagedMap).length>0){
                    cmp.set("v.insertFlag",true);
                    // cmp.set("v.addFieldMessage",fieldMessages);
                    // Adding w.r.t Story S-170613
                    addFielMessagedMap['locationErrorVarName']="Location not found you need to add it manually or lookup another location.";
                    //End of Adding w.r.t Story S-170613
                    cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
                    var testmap=cmp.get("v.addFieldMessagedMap");
                    //console.log('Field Map--->'+testmap);
                }else{
                    cmp.set("v.insertFlag",false);
                    //cmp.set("v.addFieldMessage",fieldMessages);
                    cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
                }
            }else if(TGSReqValue == 'Yes'){
                cmp.set("v.insertFlag",false);
                //cmp.set("v.addFieldMessage",fieldMessages);
                cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
            }
        }else if(OpCoValue != 'TEKsystems, Inc.'){
            if(oppAddress.Req_Worksite_Street__c == null || oppAddress.Req_Worksite_Street__c == ''){
                // cmp.set("v.streetErrorVarName","Street cannot be blank.");
                // fieldMessages.push("Street cannot be blank.");
                addFielMessagedMap['streetErrorVarName']="Street is required.";
            }else{
                cmp.set("v.streetErrorVarName","");
            }                        
            if(oppAddress.Req_Worksite_City__c == null || oppAddress.Req_Worksite_City__c == ''){
                // cmp.set("v.cityTypeErrorVarName","City cannot be blank.");
                // fieldMessages.push("City cannot be blank.");
                addFielMessagedMap['cityTypeErrorVarName']="City is required.";
            }else{
                cmp.set("v.cityTypeErrorVarName","");
            }            
            var countryManFlag=false;
            if(oppAddress.Req_Worksite_Country__c == null || oppAddress.Req_Worksite_Country__c == ''){
                //cmp.set("v.countryVarName","Country cannot be blank.");
                //fieldMessages.push("Country cannot be blank.");
                //addFielMessagedMap.set("countryVarName","Country cannot be blank.");
                addFielMessagedMap['countryVarName']="Country is required.";
            }else{
                countryManFlag=true;
                cmp.set("v.countryVarName","");
            }                        
            //if((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && countryManFlag && (oppAddress.Req_Worksite_Country__c=='United States' || oppAddress.Req_Worksite_Country__c=='Canada')) {
            //Adding w.r.t Story S-179373
                if((oppAddress.Req_Worksite_State__c == null || oppAddress.Req_Worksite_State__c == '') && (oppAddress.Req_Worksite_Country__c=='United States' || oppAddress.Req_Worksite_Country__c=='Canada')) {
                addFielMessagedMap['stateErrorVarName']="State is required.";
            }else{
                cmp.set("v.stateErrorVarName","");
            }                                    
            if(oppAddress.Req_Worksite_Postal_Code__c == null || oppAddress.Req_Worksite_Postal_Code__c == ''){
                //cmp.set("v.zipErrorVarName","Postal Code cannot be blank.");
                //fieldMessages.push("Postal Code cannot be blank.");
                //addFielMessagedMap.set("zipErrorVarName","Postal Code cannot be blank.");
                addFielMessagedMap['zipErrorVarName']="Postal Code is required.";
            }else{
                cmp.set("v.zipErrorVarName","");
            }                        
            //if(typeof addFielMessagedMap!='undefined' && addFielMessagedMap!=null && addFielMessagedMap.length>0){
            if(typeof addFielMessagedMap!='undefined' && addFielMessagedMap!=null && Object.keys(addFielMessagedMap).length>0){
                cmp.set("v.insertFlag",true);
                // cmp.set("v.addFieldMessage",fieldMessages);
                // Adding w.r.t Story S-170613
                addFielMessagedMap['locationErrorVarName']="Location not found you need to add it manually or lookup another location.";
                //End of Adding w.r.t Story S-170613
                cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
                var testmap=cmp.get("v.addFieldMessagedMap");
                //console.log('Field Map--->'+testmap);
            }else{
                cmp.set("v.insertFlag",false);
                //cmp.set("v.addFieldMessage",fieldMessages);
                cmp.set("v.addFieldMessagedMap",addFielMessagedMap);
            }
        }
    },
    redirectForProactiveSubmittal : function(component, event, helper) {
        if(component.get("v.source") === 'skuid') {
            this.redirectToOpportunityURL(component, event, helper);
        }
        else {
            let promise = component.get('v.modalPromise');
            promise.then(
                function (modal) {
                    //var evt = component.getEvent("statusCanceledEvt");
                    //evt.fire();
                    modal.close();
                }
            );
        }
    }, 
    onProductChanges : function(component, event, helper) {
       
        var selected = component.get("v.req.Req_Product__c");
        var isclone  = component.get("v.isCloneReq");
        var stage = component.get("v.req.StageName");   
    	        
        //do something else
       
       if(selected === "Contract"){
            component.set("v.isContract", "true");
            component.set("v.isPermanent", "false");
            component.set("v.isCTH", "false");
            
            if(isclone == "true"){
               var salMin = component.get("v.req.Req_Salary_Min__c");
               var salMax = component.get("v.req.Req_Salary_Max__c");
               var bonus = component.get("v.req.Req_Bonus__c");
               var feepercent = component.get("v.req.Req_Fee_Percent__c");
               var includebonus = component.get("v.req.Req_Include_Bonus__c");
               var fltfee = component.get("v.req.Req_Flat_Fee__c");
               var otherComp = component.get("v.req.Req_Other_Compensations__c");
               var payTerms = component.get("v.req.Req_Payment_terms__c");
               var addFirstYear = component.get("v.req.Req_Additional_1st_Year_Compensation__c");
               var retainerFee = component.get("v.req.Retainer_Fee__c");
               var firstYearRev = component.get("v.req.Req_Include_in_Revenue_1st_Year_Comp__c");
               var curr = component.get("v.req.Currency__c");
                
               
                
                if(salMin != null && salMin != ''){
                    component.set("v.req.Req_Salary_Min__c", null);
                }
                if(curr != null && curr != ''){
                   // component.set("v.req.Currency__c",'--None--');
                }
                if(salMax != null && salMax != ''){
                    component.set("v.req.Req_Salary_Max__c", null);
                }
                if(bonus != null && bonus != ''){
                    component.set("v.req.Req_Bonus__c", null);
                }
                if(feepercent != null && feepercent != ''){
                    component.set("v.req.Req_Fee_Percent__c", null);
                }
                if(includebonus != null && includebonus !=''){
                    component.set("v.req.Req_Include_Bonus__c", false);
                }
                if(fltfee != null && fltfee !=''){
                    component.set("v.req.Req_Flat_Fee__c", null);
                }
                if(otherComp != null && otherComp != ''){
                    component.set("v.req.Req_Other_Compensations__c", null);
                }
                if(payTerms != null && payTerms != ''){
                    component.set("v.req.Req_Payment_terms__c", null);
                }
                if(addFirstYear != null && addFirstYear != ''){
                    component.set("v.req.Req_Additional_1st_Year_Compensation__c", null);
                }
                if(retainerFee != null && retainerFee != ''){
                    component.set("v.req.Retainer_Fee__c", null);
                }
                if(firstYearRev != null && firstYearRev != ''){
                    component.set("v.req.Req_Include_in_Revenue_1st_Year_Comp__c", false);
                }
                //console.log('test currency val '+component.get("v.req.Currency__c"));
                               
            }
        }else if(selected === "Permanent"){
            component.set("v.isContract", "false");
            component.set("v.isPermanent", "true");
            component.set("v.isCTH", "false");
             if(isclone == "true"){
               var durn = component.get("v.req.Req_Duration__c");
               var durUnit = component.get("v.req.Req_Duration_Unit__c");
               var brMin = component.get("v.req.Req_Bill_Rate_Min__c");
               var brMax = component.get("v.req.Req_Bill_Rate_Max__c");
               var prMin = component.get("v.req.Req_Pay_Rate_Min__c");
               var prMax = component.get("v.req.Req_Pay_Rate_Max__c");
               var sBurd = component.get("v.req.Req_Standard_Burden__c");
               var rFee = component.get("v.req.Retainer_Fee__c");
                var curry = component.get("v.req.Currency__c");
                 
                 if(durn != null && durn !=''){
                     component.set("v.req.Req_Duration__c",null);
                 }
                 if(durUnit != null && durUnit != ''){
                     component.set("v.req.Req_Duration_Unit__c",null);
                 }
                 if(brMin != null && brMin !=''){
                     component.set("v.req.Req_Bill_Rate_Min__c",null);
                 }
                 if(brMax != null && brMax != ''){
                     component.set("v.req.Req_Bill_Rate_Max__c",null);
                 }
                 if(prMin != null && prMin != ''){
                     component.set("v.req.Req_Pay_Rate_Min__c",null);
                 }
                 if(prMax != null && prMax != ''){
                     component.set("v.req.Req_Pay_Rate_Max__c",null);
                 }
                 if(sBurd != null && sBurd != ''){
                     component.set("v.req.Req_Standard_Burden__c",null);
                 }
                 if(rFee != null && rFee != ''){
                     component.set("v.req.Retainer_Fee__c", null);
                 }
                if(curry != null && curry != ''){
                   // component.set("v.req.Currency__c",'--None--');
                }
             }
        }else if(selected === "Contract to Hire"){
            component.set("v.isContract", "false");
            component.set("v.isPermanent", "false");
            component.set("v.isCTH", "true");
            if(isclone == "true"){
               var salMin = component.get("v.req.Req_Salary_Min__c");
               var salMax = component.get("v.req.Req_Salary_Max__c");
               var bonus = component.get("v.req.Req_Bonus__c");
               var feepercent = component.get("v.req.Req_Fee_Percent__c");
               var includebonus = component.get("v.req.Req_Include_Bonus__c");
               var fltfee = component.get("v.req.Req_Flat_Fee__c");
               var otherComp = component.get("v.req.Req_Other_Compensations__c");
               var payTerms = component.get("v.req.Req_Payment_terms__c");
               var addFirstYear = component.get("v.req.Req_Additional_1st_Year_Compensation__c");
               var retainerFee = component.get("v.req.Retainer_Fee__c");
               var firstYearRev = component.get("v.req.Req_Include_in_Revenue_1st_Year_Comp__c");
                var curcy = component.get("v.req.Currency__c");
                
                if(salMin != null && salMin != ''){
                    component.set("v.req.Req_Salary_Min__c", null);
                }
                if(salMax != null && salMax != ''){
                    component.set("v.req.Req_Salary_Max__c", null);
                }
                if(curcy != null && curcy != ''){
                   // component.set("v.req.Currency__c",'--None--');
                }
                if(bonus != null && bonus != ''){
                    component.set("v.req.Req_Bonus__c", null);
                }
                if(feepercent != null && feepercent != ''){
                    component.set("v.req.Req_Fee_Percent__c", null);
                }
                if(includebonus != null && includebonus !=''){
                    component.set("v.req.Req_Include_Bonus__c", false);
                }
                if(fltfee != null && fltfee !=''){
                    component.set("v.req.Req_Flat_Fee__c", null);
                }
                if(otherComp != null && otherComp != ''){
                    component.set("v.req.Req_Other_Compensations__c", null);
                }
                if(payTerms != null && payTerms != ''){
                    component.set("v.req.Req_Payment_terms__c", null);
                }
                if(addFirstYear != null && addFirstYear != ''){
                    component.set("v.req.Req_Additional_1st_Year_Compensation__c", null);
                }
                if(retainerFee != null && retainerFee != ''){
                    component.set("v.req.Retainer_Fee__c", null);
                }
                if(firstYearRev != null && firstYearRev != ''){
                    component.set("v.req.Req_Include_in_Revenue_1st_Year_Comp__c", false);
                }
            }
        }else {
            component.set("v.isContract", "false");
            component.set("v.isPermanent", "false");
            component.set("v.isCTH", "false");
            if(isclone == "true"){

               var durn = component.get("v.req.Req_Duration__c");
               var durUnit = component.get("v.req.Req_Duration_Unit__c");
               var brMin = component.get("v.req.Req_Bill_Rate_Min__c");
               var brMax = component.get("v.req.Req_Bill_Rate_Max__c");
               var prMin = component.get("v.req.Req_Pay_Rate_Min__c");
               var prMax = component.get("v.req.Req_Pay_Rate_Max__c");
               var sBurd = component.get("v.req.Req_Standard_Burden__c");
               var rFee = component.get("v.req.Retainer_Fee__c");
               var salMin = component.get("v.req.Req_Salary_Min__c");
               var salMax = component.get("v.req.Req_Salary_Max__c");
               var bonus = component.get("v.req.Req_Bonus__c");
               var feepercent = component.get("v.req.Req_Fee_Percent__c");
               var includebonus = component.get("v.req.Req_Include_Bonus__c");
               var fltfee = component.get("v.req.Req_Flat_Fee__c");
               var otherComp = component.get("v.req.Req_Other_Compensations__c");
               var payTerms = component.get("v.req.Req_Payment_terms__c");
               var addFirstYear = component.get("v.req.Req_Additional_1st_Year_Compensation__c");
               var retainerFee = component.get("v.req.Retainer_Fee__c");
               var firstYearRev = component.get("v.req.Req_Include_in_Revenue_1st_Year_Comp__c");
               var curncy = component.get("v.req.Currency__c");
                
                if(salMin != null && salMin != ''){
                    component.set("v.req.Req_Salary_Min__c", null);
                }
                if(salMax != null && salMax != ''){
                    component.set("v.req.Req_Salary_Max__c", null);
                }
                if(curncy != null && curncy != ''){
                  //  component.set("v.req.Currency__c",'--None--');
                }
                if(bonus != null && bonus != ''){
                    component.set("v.req.Req_Bonus__c", null);
                }
                if(feepercent != null && feepercent != ''){
                    component.set("v.req.Req_Fee_Percent__c", null);
                }
                if(includebonus != null && includebonus !=''){
                    component.set("v.req.Req_Include_Bonus__c", false);
                }
                if(fltfee != null && fltfee !=''){
                    component.set("v.req.Req_Flat_Fee__c", null);
                }
                if(otherComp != null && otherComp != ''){
                    component.set("v.req.Req_Other_Compensations__c", null);
                }
                if(payTerms != null && payTerms != ''){
                    component.set("v.req.Req_Payment_terms__c", null);
                }
                if(addFirstYear != null && addFirstYear != ''){
                    component.set("v.req.Req_Additional_1st_Year_Compensation__c", null);
                }
                if(retainerFee != null && retainerFee != ''){
                    component.set("v.req.Retainer_Fee__c", null);
                }
                if(firstYearRev != null && firstYearRev != ''){
                    component.set("v.req.Req_Include_in_Revenue_1st_Year_Comp__c", false);
                }
				
				 if(durn != null && durn !=''){
                     component.set("v.req.Req_Duration__c",null);
                 }
                 if(durUnit != null && durUnit != ''){
                     component.set("v.req.Req_Duration_Unit__c",null);
                 }
                 if(brMin != null && brMin !=''){
                     component.set("v.req.Req_Bill_Rate_Min__c",null);
                 }
                 if(brMax != null && brMax != ''){
                     component.set("v.req.Req_Bill_Rate_Max__c",null);
                 }
                 if(prMin != null && prMin != ''){
                     component.set("v.req.Req_Pay_Rate_Min__c",null);
                 }
                 if(prMax != null && prMax != ''){
                     component.set("v.req.Req_Pay_Rate_Max__c",null);
                 }
                 if(sBurd != null && sBurd != ''){
                     component.set("v.req.Req_Standard_Burden__c",null);
                 }
            }
        }
       
        window.setTimeout(
            $A.getCallback(function() {
                var reqValidator;
                var productId;
                if(component.find("productId")) {
                    productId=component.find("productId").get("v.value");                    
                } else {
                    if(component.get("v.TFOFlag")) {
                        productId="Contract";
                    }
                }
                if(component.find('validator') !=undefined){
                    reqValidator 	= component.find('validator');
                //reqValidator.validateRequiredEditFields(component.find("stageId").get("v.value"),component.find("productId").get("v.value"));
            	//reqValidator.validateRequiredEditFields(component.find("productId").get("v.value"),'No');
                reqValidator.validateRequiredEditFields(productId,'No');
                }else{
                    $A.get('e.force:refreshView').fire();	//siva
                }
                }), 500
        );
        
    },
    showToast : function(message, title, toastType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title: title,
            message: message,
            type: toastType
        });
        toastEvent.fire();
    },
    defaultTermsOfEngagement : function(component, event, helper){
        var opts = [
                        { value: "", label: "--None--" },
                        { value: "Contingent", label: "Contingent" },
                        { value: "Retained", label: "Retained" }
                    ];
         component.set("v.options", opts);
    },
    constructFinalSkills: function(cmp) {
        const topSkills = cmp.get('v.topSkills');
        //D-17220 starts
        //const topSkillsCMP = cmp.find('crmSkillV').getElement()
        var topSkillsCMP;
        if(cmp.find('crmSkillV') != undefined){
            topSkillsCMP = cmp.find('crmSkillV').getElement();
        }
        //D-17220 ends
        const topSuggestedSkills = topSkillsCMP ? topSkillsCMP.inUseSuggestedSkills : []; //lwcReqSkills stores suggested skills in this variable
        const formattedTopSkills = topSkills.map((skill, i) => { //iterating over top skills and checking against suggested skills to mark them true
            const normalizedSuggested = topSuggestedSkills.map(s => s.toLowerCase());
            return {
                name: skill,
                favorite: true,
                index: i,
                suggestedSkill: normalizedSuggested.includes(skill.toLowerCase())
            }
            // if (normalizedSuggested.includes(skill.toLowerCase())) {
            //     //{"name":skillName,"favorite":false,"index":i,"suggestedSkill":true};
            //     return {name: skill, favorite: true, index: i, suggestedSkill: true }
            // }
            // return {name: skill, favorite: true, index: i, suggestedSkill: false}
        })

        const secSkills = cmp.get('v.secondarySkills');
        //D-17220 starts
        //const secSkillsCMP = cmp.find('lwcSecSkills').getElement();
        var secSkillsCMP;
        if(cmp.find('lwcSecSkills') != undefined){
            secSkillsCMP = cmp.find('lwcSecSkills').getElement();
        }
        //D-17220 ends
        const secSuggestedSkills = secSkillsCMP ? secSkillsCMP.inUseSuggestedSkills : [];
        const formattedSecSkills = secSkills.map((skill, i) => {
            const normalizedSuggested = secSuggestedSkills.map(s => s.toLowerCase());
            return {
                name: skill,
                favorite: false,
                index: topSkills.length + i,
                suggestedSkill: normalizedSuggested.includes(skill.toLowerCase())
            }
            // if (normalizedSuggested.includes(skill.toLowerCase())) {
            //     //{"name":skillName,"favorite":false,"index":i,"suggestedSkill":true};
            //     return {name: skill, favorite: false, index: topSkills.length + i, suggestedSkill: true }
            // }
            // return {name: skill, favorite: false, index: topSkills.length + i, suggestedSkill: false}
        })

        const formattedSkills = [...formattedTopSkills, ...formattedSecSkills]
        cmp.set('v.stringSkills', JSON.stringify(formattedSkills));
		//set skills to make sure selected skills are excluded
		cmp.set('v.skills', formattedSkills);
    },
    skillsAutoPopulate: function(component,oppList) {
    	let topSkills = [];
        let secSkills = [];
		for (var i = 0; i < oppList.length; i++) { 
        	if(oppList[i].favorite){
            	topSkills.push(oppList[i].name);
            }else{
            	secSkills.push(oppList[i].name);
            }
        }
		component.set('v.topSkills', topSkills);
        component.set('v.secondarySkills', secSkills);
        this.constructFinalSkills(component);
    },    
	FunctionalValues : function(component, event,model){
		var funcPositionArray = []; 
        funcPositionArray.push({value:'', key:'--None--', selected: true }); 
        var funcPositionMap = model.funcPositionMappings;
        for ( var key in funcPositionMap) {
			funcPositionArray.push({value:funcPositionMap[key], key:key});
        }
        component.set("v.funcPositionList", funcPositionArray);
        component.set("v.req.Functional_Non_Functional__c",'');
	},
	skillSpecValues : function(component, event,model){
		var skillArray = [];
        var skillMap = model.skillPicklistMappings;
        skillArray.push({value:'--None--', key:'--None--',selected: true});  
        for ( var key in skillMap) {
			skillArray.push({value:skillMap[key], key:key});
        }
        component.set("v.specialityList", skillArray);        
	},
    //End of Adding S-228902
    //Adding w.r.t S-228902 skill specialty w.r.t Req_division 
    SkillSpecialtyDependsDivision : function(component, event,model){                           
        var skillSpecialtyArray = [];
        skillSpecialtyArray.push({value:'--None--', key:'--None--',selected:true});
        if(component.get("v.req.Req_Division__c") != '--None--' && component.get("v.req.Req_Division__c") != undefined){
        		var skillSpecialtyPicklistMap = model.skillSpecialtyDivisionDepdentMappings[component.get("v.req.Req_Division__c")];
                for (var key in skillSpecialtyPicklistMap) {
                    skillSpecialtyArray.push({value:skillSpecialtyPicklistMap[key], key:skillSpecialtyPicklistMap[key],selected: key ===(model.opp!== undefined)?model.opp.Req_Skill_Specialty__c:''});
                }
        }                    
        component.set("v.specialityList", skillSpecialtyArray);  
        if(component.get("v.SSApiModel")) 
            this.updateSkillSpecialtyList(component, event, component.get("v.SSApiModel")); 
    },
    EntSkillSpecialtyModal : function(component,event,helper){
        var action = component.get("c.getSkillSpecialtyPicklists");        
        action.setCallback(this, function(response) {
            var data = response;
            if (data.getState() == 'SUCCESS') {
                var model = data.getReturnValue();                
                this.SkillSpecialtyDependsDivision(component, event,model);               
            }
        });        
        $A.enqueueAction(action);
    },
    //End of Adding S-228902
    skillSetValues: function(component, event, model) {
        var skillSetArray = []; 
        skillSetArray.push({value:'', key:'--None--', selected: true }); 
        var skillSetMap = model.skillSetMappings;
        for ( var key in skillSetMap) {
			skillSetArray.push({value:skillSetMap[key], key:key});
        }
        component.set("v.skillSetList", skillSetArray);
        component.set("v.req.Skill_Set__c",'');
    },
    skillSpecialtyClassifierCall : function(component,event){
        console.log('classifier called ::::');
        var callAPI=component.get("v.isCloneReq")=="true" && component.get("v.SSCAPIinProgress")==true?false:true;
        console.log('classifier calledAPI ::::'+ component.get("v.topSkills"));
        console.log('component.get("v.req.Description")' +component.get("v.req.Description"));
        console.log('component.get("v.req.Req_Job_Title__c")' +component.get("v.req.Req_Job_Title__c"));
        let skills = "";
        for(let x=0; x<component.get("v.topSkills").length; x++) {
            skills += component.get("v.topSkills")[x] + ",";
        }
        if(component.get("v.callSkillSpecialtyAPI")==true && callAPI &&
            component.get('v.req.Req_Job_Title__c') && component.get("v.req.Description") &&
            component.get("v.topSkills") != '' && component.get("v.topSkills") != null && typeof component.get("v.topSkills") != undefined){
            var action = component.get("c.fetchSkillSpecialtyWrapper"); 
            let payload = {"req_job_title__c": component.get('v.req.Req_Job_Title__c'),
                            "req_job_description__c": component.get("v.req.Description"),
                            "req_qualification__c": component.get("v.req.Req_Qualification__c"),                            
                            "enterprisereqskills__c":skills.substring(0, skills.length - 1),
                            "req_skill_details__c": component.get("v.req.Req_Skill_Details__c"),
                            "opco__c": component.get("v.req.OpCo__c"),
                            "suggested_job_titles__c": component.get("v.suggTitlesForSkills")
                        };       
            console.log(payload);
            action.setParams({"payload":JSON.stringify(payload)});
            component.set("v.SSCAPIinProgress",true);
            action.setCallback(this, function(response) {
                var data = response;			
                if (data.getState() == 'SUCCESS'){
                    var model = data.getReturnValue();  
                    component.set("v.SSApiModel", model);                  
                    console.log(model);
                    var updatedFields=component.get("v.req.Req_Manually_Updated_Fields__c");
                    //Adding w.r.t Story S-234527
                    if(updatedFields == undefined || !updatedFields.includes('Req_Skill_Specialty__c')){
                        this.updateSkillSpecialtyList(component, event, model);
                    }
                    if(updatedFields == undefined || !updatedFields.includes('Functional_Non_Functional__c')){
                    	this.updateFunctional(component, event, model);
                    }
                    if(updatedFields == undefined || !updatedFields.includes('Req_Job_Level__c')){
                    	this.updateExpLevel(component, event, model);
                    }
                    if(updatedFields == undefined || !updatedFields.includes('Skill_Set__c')){
                    	this.updateSkillSet(component, event, model);
                    }
                    if (model.onet_job_code) {
                        component.set("v.req.Req_ONet_SOC_Description__c", model.onet_job_code.onet_soc_description);
                        component.set("v.req.Soc_onet__c", model.onet_job_code.onet_soc_code);    
                    }
                    if (model.RVT && model.RVT.RVT_detail){
                        component.set("v.req.Req_RVT_Premium_Skills__c", model.RVT.RVT_detail.premium_fields);
                        component.set("v.req.Req_RVT_Occupation_Code__c",model.RVT.Occupation_Code);
                        component.set("v.req.Req_RVT_Occupation_Code_Confidence__c",model.RVT.Occupation_confidence_value);
                        component.set("v.req.Req_RVT_Experience_Level_Confidence__c",model.RVT.Experience_level_confidence_value);
                    }
                    //End of Adding w.r.t Story S-234527                    
                    //Adding w.r.t S-228904
					this.setReqMetricsData(component, event);
                    //End of Adding w.r.t S-228904			                
                }
                component.set("v.SSCAPIinProgress",false);
            });
			$A.enqueueAction(action);
		}
	},
    updateSkillSpecialtyList : function(component, event,model){
        if(model.skill_specialty.label) {
            var skillSpecialtyArray = [];                         
            let containSS = false;
            for(let x=0; x<component.get("v.specialityList").length; x++) {
                if(model.skill_specialty.label == component.get("v.specialityList")[x].value) {
                    component.get("v.specialityList")[x].selected = true;
                    containSS = true;                             
                } else {
                    component.get("v.specialityList")[x].selected = false;
                }
                skillSpecialtyArray.push(component.get("v.specialityList")[x]);
            }
            if(!containSS) {
                skillSpecialtyArray.push({value:model.skill_specialty.label, key:model.skill_specialty.label, selected: true});
            } 
            component.set("v.specialityList", skillSpecialtyArray);
            component.set("v.req.Req_Skill_Specialty__c", model.skill_specialty.label);
            console.log('skillSpecialtyArray' + skillSpecialtyArray); 		
        }
	},
    updateFunctional : function(component, event,model){
        if(model.functional.functional) {
            var funcPositionArray = [];                                 
            for(let x=0; x<component.get("v.funcPositionList").length; x++) {
                if(model.functional.functional == 'Not-Functional' && component.get("v.funcPositionList")[x].value == 'Non-Functional') {
                    console.log('funcPositionArray'+component.get("v.funcPositionList")[x]);
                    component.get("v.funcPositionList")[x].selected = true;   
                    component.set("v.req.Functional_Non_Functional__c",'Non-Functional');                                         
                } else if(model.functional.functional == 'Functional' && component.get("v.funcPositionList")[x].value == 'Functional') {
                    component.get("v.funcPositionList")[x].selected = true;
                    component.set("v.req.Functional_Non_Functional__c",'Functional');
                }else {
                    component.get("v.funcPositionList")[x].selected = false;
                }
                funcPositionArray.push(component.get("v.funcPositionList")[x]);
            }                
                    
            component.set("v.funcPositionList", funcPositionArray);                
            console.log('funcPositionArray'+model.functional);
        }
	},
    updateExpLevel: function(component, event, model) {
        if(model.RVT.Experience_level) {
            if(model.RVT.Experience_level == '0') {
                component.set("v.req.Req_Job_Level__c",'Entry Level');	
            } else if(model.RVT.Experience_level == '1') {
                component.set("v.req.Req_Job_Level__c",'Intermediate Level');	
            } else if(model.RVT.Experience_level == '2') {
                component.set("v.req.Req_Job_Level__c",'Expert Level');	
            } else {
                component.set("v.req.Req_Job_Level__c",model.RVT.Experience_level);	
            }
        }
        
    },
    updateSkillSet: function(component, event, model) {
        let MaxSkillSet = model.skillset.skillset_classifier.maxSkillsetClassifier;
        if(MaxSkillSet) {
            var skillSetList = [];
            for(let x=0; x<component.get("v.skillSetList").length; x++) {
                if(MaxSkillSet.toUpperCase() == component.get("v.skillSetList")[x].value.toUpperCase()) {
                    component.get("v.skillSetList")[x].selected = true;                    
                } else {
                    component.get("v.skillSetList")[x].selected = false;
                }
                skillSetList.push(component.get("v.skillSetList")[x]);
            }
            component.set("v.skillSetList", skillSetList);
            component.set("v.req.Skill_Set__c", MaxSkillSet);
            console.log('skillSpecialtyArray' + skillSetList); 		
        }
    },
    //Adding w.r.t S-228904
	setReqMetricsData : function(component, event){
        //Adding w.r.t Story S-234527
        var model=component.get("v.SSApiModel");        
        let ExpLevel;
        if(model.RVT.Experience_level) {
            	if(model.RVT.Experience_level == '0') {
                	ExpLevel='Entry Level';	
            	} else if(model.RVT.Experience_level == '1') {
                	ExpLevel='Intermediate Level';	
            	} else if(model.RVT.Experience_level == '2') {
                	ExpLevel='Expert Level';	
            	} else {
                	ExpLevel=model.RVT.Experience_level;	
            	}
        }
        //End of Adding w.r.t Story S-234527
        component.set("v.reqMetrics.Skill_Speciality_From_Model__c", model.skill_specialty.label);
        component.set("v.reqMetrics.F_NF_From_Model__c", model.functional.functional == 'Functional'? 'Functional':'Non-Functional');        
        component.set("v.reqMetrics.Skillset_From_Model__c", model.skillset.skillset_classifier.maxSkillsetClassifier);
        component.set("v.reqMetrics.Experience_Level_From_Model__c", ExpLevel);        
        component.set("v.reqMetrics.Job_Title__c", component.get("v.req.Req_Job_Title__c"));
        component.set("v.reqMetrics.Job_Description__c", component.find("jobDescription").get("v.value"));        
    },
	//End of Adding w.r.t S-228904 
	//Adding w.r.t Story S-234527  
	ManuallyUpdatedFields:   function(component, event, helper,APIName){        
    	var updatedFields=component.get("v.req.Req_Manually_Updated_Fields__c");
        if(updatedFields != undefined && updatedFields != null && !updatedFields.includes(APIName)){
        	updatedFields=updatedFields+','+APIName;
        }
        if(updatedFields == undefined || updatedFields == null || updatedFields == ''){
        	updatedFields=APIName;
        }
        component.set("v.req.Req_Manually_Updated_Fields__c", updatedFields);
    },
  	//End of Adding w.r.t Story S-234527
  	//Adding w.r.t S-235321 PrimaryBuyer dependent on division
  	PrimaryBuyerDependsDivision : function(component, event,model){                           
        var productPicklistArray = [];
        var productPicklistInfo;
        if(model == null){
            productPicklistInfo = "";
        }else{
            productPicklistInfo = (model.opp != null && model.opp != undefined)? model.opp.Product__c : '';
        }        
        productPicklistArray.push({value:'--None--', key:'--None--',selected:true});
        if(component.get("v.req.Req_Division__c") != '--None--' && component.get("v.req.Req_Division__c") != undefined){
        	let productPicklistMap = model.PrimaryBuyerMapping;            
            for (var key in productPicklistMap[component.get("v.req.Req_Division__c")]) {
            	productPicklistArray.push({value:productPicklistMap[component.get("v.req.Req_Division__c")][key], key:productPicklistMap[component.get("v.req.Req_Division__c")][key],selected: productPicklistMap[component.get("v.req.Req_Division__c")][key] ===productPicklistInfo});
            }          
        }
        component.set("v.productListPicklist", productPicklistArray);                    
        component.set("v.req.Product__c", productPicklistInfo);
    }
    //End of Adding w.r.t S-235321
})