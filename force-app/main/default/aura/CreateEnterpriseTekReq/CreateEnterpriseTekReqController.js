({
	doInit : function(component, event, helper) {
	   helper.initializeEnterpriseReqModal(component);	   	   
	},
    onOpcoChange : function(component, event, helper) {
       helper.loadBusinessUnitValues(component,event,'None');
	},
    
    toggleRedZone : function(component, event, helper) {
        
        var selectValue = component.find("tekdetailStage").get("v.value");
     
        if(selectValue != 'Draft'){
            component.set("v.disable_red_zone", false);
            component.find("redzoneCheck").set("v.value", false);
        }else{
            component.set("v.disable_red_zone", true);
            component.find("redzoneCheck").set("v.value", false);
        }
      	    
    },
    
    
    onBusinessunitChange : function(component, event, helper) {
        component.find("segmentId").set("v.value","--None--");
        component.find("JobcodeId").set("v.value","--None--");
        component.find("CategoryId").set("v.value","--None--");
        component.find("MainSkillId").set("v.value","--None--");
       helper.EnterpriseReqSegmentModal(component,event);
	},
    onSegmentChange : function(component, event, helper) {
       component.find("JobcodeId").set("v.value","--None--");
       component.find("CategoryId").set("v.value","--None--");
       component.find("MainSkillId").set("v.value","--None--"); 
       helper.EnterpriseReqJobcodeModal(component,event);
	},
    onJobCodeChange : function(component, event, helper) {
 	   component.find("CategoryId").set("v.value","--None--");
       component.find("MainSkillId").set("v.value","--None--");  
       helper.EnterpriseReqCategoryModal(component,event);
	},
    onCategoryChange : function(component, event, helper) {
       component.find("MainSkillId").set("v.value","--None--"); 
       helper.EnterpriseReqMainSkillModal(component,event);
	},
    
    onMainSkillChange : function(component, event, helper) {
       helper.EnterpriseReqMainSkillid(component,event);
	},
    addAltRow : function(component, event, helper) {
        helper.addAltRecord(component, event);
    },      
    removeAltRow: function(component, event, helper) {
        var supportRequestList = component.get("v.supportRequestList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        supportRequestList.splice(index, 1);
        component.set("v.supportRequestList", supportRequestList);
    },    
    onAltDeliveryChange : function(component, event, helper) {
      var selectedOp = component.find("altDeliveryList");
        //console.log("Alternate Delivery ",selectedOp.get("v.value"));
       if(selectedOp.get("v.value") === "Yes"){
          component.set("v.altDeliveryVisibility","true");  
        }
    },   
    
    onSkillsValidated : function(component, event, helper) {
       helper.topSkillsValidation(component,event);
	},
    onProductPicklistChange : function(component, event, helper) {
    	component.set("v.ProductVarNamePicklist", "testing");
    },
    onProductChange : function(component, event, helper) {
        helper.onProductChanges(component,event,helper);   
    },
    /*onOpCoChange :function (component,event,helper){
      var selected = component.get("v.req.OpCo__c");  
      // console.log(selected);
       
        if(selected === "Aerotek, Inc"){
            
        }else if(selected === "TEKsystems, Inc."){
            
        }
        
    },*/
    onTopSkillsReset : function(component, event, helper) {
       // console.log('reset flag skills');
        component.set("v.istopSkillsMessage", "false");
	},
    
    //Save Enterprise Req
    saveEnterpriseReqOpp : function(component, event, helper){
        //Adding w.r.t REDZONE
        var QualifyingStage=component.find("tekdetailStage").get("v.value");
        if(QualifyingStage == "RedZone")
            QualifyingStage="Qualified";
        
        var reqValidator 	= component.find('validator');
        //var validation 		= reqValidator.validateRequiredFields(component.find("tekdetailStage").get("v.value"),component.find("productId").get("v.value"),component.find("isTGSReqId").get("v.value"));
        var validation 		= reqValidator.validateRequiredFields(QualifyingStage,component.find("productId").get("v.value"),component.find("isTGSReqId").get("v.value"));
        //validateRequiredFields(cmp.get("v.StageName"),cmp.get("v.placeType"))        
        
        //var validation={'isvalid': true , 'error_count':0};
        component.set("v.validationResponse", validation );
        component.set("v.isDisabled", true);
        component.set("v.errorMessageVal","");
        helper.saveEnterpriseReq(component, event, helper );
        
    },
    
    cancelAction : function(component, event, helper){
        component.set("v.errorMessageVal","");
		if(component.get("v.isProactiveSubmittal")) {
			helper.redirectForProactiveSubmittal(component, event, helper);
		}
		else {
			helper.redirectToStartURL(component, event, helper);
		}
    },
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    handleChangeJobTitle : function(component, event, helper) {
        let jobTitle = component.get('v.req.Req_Job_Title__c');
        component.set('v.suggestedjobTitle',jobTitle);
        helper.skillSpecialtyClassifierCall(component, event);        	        
    },
    onTGSChange : function(component, event, helper){
        var OpCoValue = component.get("v.req.OpCo__c");
        var TGSReqValue = component.find("isTGSReqId").get("v.value");
        if(OpCoValue == 'TEKsystems, Inc.' && TGSReqValue == 'Yes'){
            component.set("v.isTGSTrue", "true");
            helper.setRequiredFieldsOnTGSChange(component, event);           
        }else if(OpCoValue == 'TEKsystems, Inc.' && (TGSReqValue == 'No' || TGSReqValue == '--None--')){			
            if(component.get("v.isTGSTrue") == "true"){
                helper.setRequiredFieldsOnTGSChange(component, event);                
            }            
            //component.find("isBackfillId").set("v.value", 'false');
            //component.find("isNationalId").set("v.value", 'false');
			//component.find("isTGSPEId").set("v.value", '--None--');
            component.set("v.req.Practice_Engagement__c","--None--");
            //component.find("isTGSLocationId").set("v.value", '--None--');
            component.set("v.req.GlobalServices_Location__c","--None--");
            //component.find("isEmploymentAlignmentId").set("v.value", '--None--');
            component.set("v.req.Employment_Alignment__c","--None--");
            //component.find("isFinaDecMakId").set("v.value", '--None--');
            component.set("v.req.Final_Decision_Maker__c","--None--");
            //component.find("isInterNationalId").set("v.value", '--None--');
            component.set("v.req.Is_International_Travel_Required__c","--None--");
            //component.find("isInternalHireId").set("v.value", '--None--');
            component.set("v.req.National__c", false);
            component.set("v.req.Backfill__c", false);
            //component.find("isNationalId").set("v.value", 'false');
            component.set("v.req.Internal_Hire__c","--None--");
            component.set("v.req.Req_Disposition__c","--None--");
			component.set("v.isTGSPEName","");
            component.set("v.isTGSLocationName","");
            component.set("v.isTGSEmpAlignName","");
            component.set("v.isFinaDecMakName","");
            component.set("v.isIntNationalName","");
            component.set("v.isInternalName","");
            component.set("v.isTGSTrue", "false");
        } 
        var tgsReqWorksiteLoc 	= component.find('locationId');
    	tgsReqWorksiteLoc.TGSReqForTek();
    },
    
    onInsideIR35Change : function(component, event, helper) {
      var selectedOp = component.find("insideIR35");
      component.set("v.req.inside_IR35__c", selectedOp.get("v.value"));  
    },
    
    dateChange : function(component, event, helper) {
       // alert(event.targetId);
        var dateSource = event.getSource();
		var dateFieldId = dateSource.getLocalId();
        var dateField = component.find(dateFieldId);
        var dateValue = dateField.get("v.value");
       
        var cmpTarget = '';
        if(dateFieldId == 'inputOpenPositionDate'){
            cmpTarget = component.find('label-dateOpnPos');
        }else if(dateFieldId == 'inputStartDate'){
            cmpTarget = component.find('label-start-date');
        }else if(dateFieldId == 'inputExpInterview'){
            cmpTarget = component.find('label-inputExpInterview');
        }
       
        var matches = /^(\d{4})[-\/](\d{1,2})[-\/](\d{1,2})$/.exec(dateValue);
       
        if (matches == null){
            $A.util.addClass(cmpTarget, 'boldtext slds-text-color_error');
            $A.util.removeClass(cmpTarget, 'slds-form-element__label');
        } else{
            $A.util.removeClass(cmpTarget, 'boldtext slds-text-color_error');
            $A.util.addClass(cmpTarget, 'slds-form-element__label');
        }
    },
    onStageChange : function(component, event, helper){
        //Adding w.r.t REDZONE
        var QualifyingStage=component.find("tekdetailStage").get("v.value");
        if(QualifyingStage == "RedZone")
            QualifyingStage="Qualified";
        
        var reqValidator 	= component.find('validator');
    	//reqValidator.validateRequiredEditFields(component.find("tekdetailStage").get("v.value"),component.find("productId").get("v.value"),component.find("isTGSReqId").get("v.value"));
        reqValidator.validateRequiredEditFields(QualifyingStage,component.find("productId").get("v.value"),component.find("isTGSReqId").get("v.value"));
    },
    
  handleTopSkillsChange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.topSkills', data.skills)
      const secSkills = cmp.get('v.secondarySkills');
      const dupes = data.skills.filter(skill => secSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.topSkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
      cmp.set('v.topSkills', data.skills)
      h.constructFinalSkills(cmp);      
    },
    handleSecSkillsChange: function(cmp, e, h) {
        const data = e.getParams();
        const topSkills = cmp.get('v.topSkills');
        const dupes = data.skills.filter(skill => topSkills.map(s => s.toLowerCase()).includes(skill.toLowerCase()));
        if (dupes.length) {
            const dupeIndex = data.skills.indexOf(dupes[0]);
            if (dupeIndex !== -1) {
                const suggestedSkills = cmp.get('v.simpleSuggestedSkills')
                data.skills.splice(dupeIndex, 1);
                cmp.set('v.secondarySkills', data.skills)
                h.showToast(`${dupes[0]} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')
            }
        }
        cmp.set('v.secondarySkills', data.skills)
        h.constructFinalSkills(cmp)
    },
    handlesuggestedskillschange: function(cmp, e, h) {
      const data = e.getParams();
      cmp.set('v.simpleSuggestedSkills', data.pills);
    },
    setSuggestedSkills: function(cmp, e, h) {
        const suggestedSkills = cmp.get('v.suggestedSkills');
        if (suggestedSkills) {
            const suggestedSkillsList = suggestedSkills.map(skill => skill.name);
            cmp.set('v.simpleSuggestedSkills', suggestedSkillsList);
        }
    },
    handleDupeMsg: function(cmp, e, h) {
        h.showToast(`${e.getParams().val} ${$A.get("$Label.c.ATS_ALREADY_EXISTS")}`,'Error','error')        
    },  
    handleSuggestedTitlesChange:function(cmp,event,helper) {
		
		const data = event.getParams();
		cmp.set("v.isUpdatedRelatedTitles",true);
		cmp.set("v.updatedSuggestedJobTitles",data.skills);	
		//call suggested skills api
		if(!$A.util.isUndefined(data.skills) || !$A.util.isEmpty(data.skills)) {
		 //console.log('test')
			cmp.set("v.suggTitlesForSkills",data.skills.join());            
		}	
	},
	changeTitleHandler:function(cmp,event,helper) {
		let jobTitle = cmp.get('v.suggestedjobTitle');
		let reqOpco = cmp.get("v.req.OpCo__c");
       
		if(jobTitle != null) {
			//Related titles
			var relatedTitles = cmp.find("relatedTitles");
            //S-216852 starts
            if(relatedTitles != undefined){
                relatedTitles.fetchRelatedTerms(jobTitle,"TEKsystems, Inc.");
            }else{
                $A.get('e.force:refreshView').fire();	
            }
			//S-216852 ends
			console.log('suggJobTitles'+cmp.get('v.suggJobTitles'));
			
		}
	},
    destroyComp: function(component, event, helper) {
        if(component.isRendered()==true && component.find('container')!= undefined){
            component.find('container').destroy();
        }        
    },
	relatedTitleHandler: function(cmp,event,helper) {
		console.log('suggJobTitles'+cmp.get('v.suggJobTitles'));
		let titles = cmp.get('v.suggJobTitles')
		if(titles != []) {			
			cmp.set("v.suggTitlesForSkills",titles.join());			
		}
	},	
        setSkills : function(cmp,event,helper) {
        const allSkills = cmp.get('v.skills');
        cmp.set('v.allSelectedSkills', allSkills);
    },
	onJobDescChange : function(component, event, helper){
		//var jobTitle = component.find("jobtitleid").get("v.value");
		var jobTitle = component.get("v.req.Req_Job_Title__c");
        var jobDesc = component.get("v.req.Description");
		if(jobDesc != null && jobDesc != "" && typeof jobDesc != undefined){
			helper.skillSpecialtyClassifierCall(component, event);
		}
	},
    SSblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Req_Skill_Specialty__c");        
    },
    FNFblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Functional_Non_Functional__c");        
    },
    EXPblured: function(component, event, helper){
        helper.ManuallyUpdatedFields(component, event, helper,"Req_Job_Level__c");        
    },
})