({
    doInit : function(component, event, helper) {
       // alert('test115');
       component.set("v.Spinner", true);
        var UrlQuery = window.location.href;        
        var myPageRef = component.get("v.pageReference");
        var tgsId ='';
        var pId ='';
        var oppId;
        var accName = '';
        var isClone = '';
      //  var officeVal='';
        //alert('test-1');
        if(myPageRef){
            //alert('test-2');
            oppId  = myPageRef.state.c__recId;
            tgsId = myPageRef.state.c__tgsOppId;
            pId = myPageRef.state.c__pId;
            accName = myPageRef.state.c__accName;
            isClone = myPageRef.state.c__isClone;
            component.set("v.tgsOppId", tgsId);
            //officeVal = myPageRef.state.c__office;
            //alert('test-3');
            if(isClone) {
                component.set("v.isClone", true);
                var action = component.get("c.fetchClonedOppDetails");
                action.setParams({ recordId : tgsId });        
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log(response.getReturnValue());
                        component.set("v.opp", response.getReturnValue());                    
                        component.set("v.account.Id",component.get("v.opp.Account.Id"));
                        component.set("v.account.Name",component.get("v.opp.Account.Name"));
                        //component.find("salesId").set("v.value", component.get("v.opp.Sales_Organization__c"));   
                            component.set("v.opp.IsClone__c", "Yes");
                            component.set("v.opp.Practice_Engagement__c", "");
                            component.set("v.opp.OSG_Approver__c", "");
                            component.set("v.opp.Client_Relationship__c","Existing GS Client");
                        if(component.get("v.opp.StageName") != 'Won') {                              
                            helper.showToastMessages("", "You can only clone a TGS opportunity from Closed Won Stage.", "error");   
                            helper.redirectToOriginatingOppURL(component, event, helper);
                        }               
                    }
                    component.set("v.Spinner", false);
                    if(component.find("salesId") && component.get("v.opp.Sales_Organization__c")) {
                        component.find("salesId").set("v.value", component.get("v.opp.Sales_Organization__c"));
                    }
                }); 
                $A.enqueueAction(action);            
            } else {
                component.set("v.Spinner", false);
            }
            if(oppId !== null && oppId !== undefined){
                component.set('v.AccountVarName',oppId); 
                component.set("v.recordId", oppId);
                component.set("v.parentrecordId", pId);
                component.set("v.account.Id",oppId);
                component.set("v.account.Name",accName);
               // component.find("ofcId").set("v.value",officeVal);
                //component.set("v.account.Name",accName);
                var opts = [
                    { "class": "slds-input slds-combobox__input ", label: "Interest", value: "Interest", selected: "true" }
                ];  
                //alert('test-4');
                //component.find("StageID").set("v.options", opts);
                component.set("v.options", opts); 
                component.set("v.StageValue", "Interest");
                //alert('test-5');
            }            
        }            
        //alert('test-1');
    },
    recordUpdate : function(component, event, helper) {        
    // var OfficeName = component.get("v.record.Office_Code__c");
	// component.set("v.officeVal",OfficeName);
    },    
		//component.find("controllerFld").set("v.value", CmpName);
    onPicklistChange : function(component, event, helper){        
       // var StageVal = component.find("StageID").get("v.value");
       // component.set("v.StageValue",StageVal);
    },
    PracticeVal : function(component, event, helper){        
        //var StageVal = component.find("StageID").get("v.value");
        //component.set("v.StageValue",StageVal);  SalesOrgVal
    },
    SalesOrgVal : function(component, event, helper){        
        //var StageVal = component.find("StageID").get("v.value");
        //component.set("v.StageValue",StageVal);  SalesOrgVal
    },
    saveValidation : function(component,event,helper){
        var SaveBtVal = event.getSource().getLocalId();
        component.set("v.BtnVal",SaveBtVal);        
    },
    onSubmit : function(component,event,helper){ 
		event.preventDefault();
        component.set("v.isDisabled", true);        
        var fieldMessages=[];
        var TGSOpp = event.getParam("fields"); 
        var AccName = component.get('v.AccountVarName');//component.find("acctId").get("v.value");
       // alert('AccName::'+AccName);
        if(AccName == null || AccName ==''){            
            component.set("v.accVarName","Account Name is required.");
            fieldMessages.push("Account Name is required.");
        }else{
            TGSOpp.AccountId = AccName;
            component.set("v.accVarName","");
        }
        
        var OppNameVal = component.find("inputoppName").get("v.value");
        //alert('OppNameVal:'+OppNameVal);
        if(OppNameVal == null || OppNameVal ==''){
            component.set("v.oppVarName","Complete this field.");
            fieldMessages.push("Complete this field.");
        }else{
            TGSOpp.Name = OppNameVal;
            component.set("v.oppVarName","");
        }
        //var OppStage = component.find("StageID").get("v.value");
        var OppStage = component.get("v.StageValue");
        //alert('OppStage:'+OppStage);
        if(OppStage == null || OppStage ==''){
            component.set("v.StageValue","Stage cannot be blank.");
            fieldMessages.push("Stage cannot be blank.");            
        }else{
                TGSOpp.StageName = component.get("v.StageValue");
                component.set("v.StageValue",""); 
        }
        var OfficeName = component.find("ofcId").get("v.value");
        if(OfficeName == null || OfficeName =='')
        {            
            component.set("v.organizationVarName","Office is required.");
            fieldMessages.push("Office is required.");
        }
        else
        {
            TGSOpp.Organization_Office__c  = OfficeName;
            component.set("v.organizationVarName","");
        }       
        //alert('v.opptyOwner.Id:'+component.get("v.opptyOwner.Id"));
        /*var opptyOwnerId = component.find("OwId").get("v.value");
        //alert('opptyOwnerId::'+opptyOwnerId);
        if(opptyOwnerId == null || opptyOwnerId =='')
        {            
            component.set("v.opptyOwnerName","Opportunitty Owner Name cannot be blank.");
            fieldMessages.push("Opportunitty Owner Name cannot be blank.");
        }
        else
        {
            TGSOpp.Owner = opptyOwnerId;
            component.set("v.opptyOwnerName","");
        }*/ 
        
		TGSOpp.Probability_TGS__c = '0%';
        var OppcloseDate = component.find("closeDateVal").get("v.value");
        if(OppcloseDate  == null || OppcloseDate =='')
        {
            component.set("v.closedDateVal","Please enter a date.");
            fieldMessages.push("Please enter a date.");
        }else{
            TGSOpp.CloseDate = OppcloseDate;
            component.set("v.closedDateVal","");
        } 
        var OppOpCo = component.find("OpcoId").get("v.value");
        if(OppOpCo  == null || OppOpCo  =='')
        {
           component.set("v.OpCoVal","OpCo cannot be blank.");
            fieldMessages.push("OpCo cannot be blank.");
        }else{
            TGSOpp.OpCo__c = OppOpCo;
            component.set("v.OpCoVal","");
        }   
        var OppBU = component.find("BusinessUnitID").get("v.value");
        if(OppBU == null || OppBU =='')
        {
             component.set("v.BuVal","Business Unit cannot be blank.");
            fieldMessages.push("Business Unit cannot be blank.");
        }else{
            TGSOpp.BusinessUnit__c = OppBU;
            component.set("v.BuVal","");
        } 
        if(component.get("v.parentrecordId") != null){
            var CntID = component.get("v.parentrecordId");
            if(CntID.startsWith('003'))
            TGSOpp.Req_Hiring_Manager__c = component.get("v.parentrecordId");
        }
        if(component.get("v.isClone")==true) {
            let opp = component.get("v.opp");
            for(let key in opp) {
                if(key != 'StageName') {
                    TGSOpp[key] = opp[key];
                }
            } 
            TGSOpp['Originating_Opportunity__c'] = component.get("v.tgsOppId");           
        }
        if(fieldMessages.length > 0){
            component.set("v.isDisabled", false);
            component.set("v.validationMessagesList",fieldMessages);
        } else {             
            component.find('TGSOppID').submit(TGSOpp);
            component.set("v.Spinner", true);
          // alert('Submit done:');
        }        
        //alert('Submit done:-1');        
    },
    handleSuccess : function(component, event, helper) { 
        //alert('handleSuccess:');
        var newOppId = event.getParams().response;
        //alert('newOppId:'+newOppId);
        //console.log(newOppId.id);
        if(newOppId !=null){
            if(component.get("v.BtnVal") == 'saveButton'){
                helper.showToastMessages('Success!', 'Opportunity saved successfully.', 'success');
                component.set("v.newOpportunityId",newOppId.id);
                helper.redirectToOpportunityURL(component, event, helper);
            }else{
                helper.showToastMessages('Success!', 'Opportunity saved successfully.', 'success');
                $A.get('e.force:refreshView').fire();
            }
        }
    },
    handleError : function(cmp,event,helper){
        //alert('handleError:');
        var params = event.getParams();
        var validationMessages=[];
        
        if(params.output!=null && params.output.fieldErrors!=null ){            
            var stgErrors=params.output.fieldErrors;            
            Object.keys(stgErrors).forEach(function(key){
                var value = stgErrors[key];
                var checkFlag=Array.isArray(value);
                console.log(key + ':' + value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;
                validationMessages.push(fmessage);                
            });            
        }
        
        if(params.output!=null && params.output.errors!=null ){            
            var stgErrors=params.output.errors;            
            Object.keys(stgErrors).forEach(function(key){
                var value = stgErrors[key];
                console.log(key + ':' + value);
                console.log(key + ':' + JSON.stringify(value));
                var checkFlag=Array.isArray(value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;
                validationMessages.push(fmessage);                
            });            
        }
        
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null && event.getParams().error.data.output!=null && event.getParams().error.data.output.fieldErrors!=null){
            var fieldErrors=event.getParams().error.data.output.fieldErrors;
            Object.keys(fieldErrors).forEach(function(key){
                var value = fieldErrors[key];
                console.log(key + ':' + value);
                console.log(key + ':' + JSON.stringify(value));
                var checkFlag=Array.isArray(value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;                
                validationMessages.push(fmessage);                                
            });
        } 
        if(validationMessages!=null && validationMessages.length==0 && event.getParams().error.data!=null  && event.getParams().error.data.output!=null && event.getParams().error.data.output.errors!=null) {            
            var fieldErrors=event.getParams().error.data.output.errors;            
            Object.keys(fieldErrors).forEach(function(key){
                var value = fieldErrors[key];
                console.log(key + ':' + value);
                console.log(key + ':' + JSON.stringify(value));
                var checkFlag=Array.isArray(value);
                var fmessage=(checkFlag==true)?value[0].message:value.message;
                validationMessages.push(fmessage);                                
            });            
        }
        cmp.set("v.validationMessagesList",validationMessages);
    },
    cancelAction : function(component, event, helper){
        component.set("v.accVarName","");
        component.set("v.oppVarName","");
        component.set("v.stageVarName","");
        component.set("v.OpCoVal","");
        component.set("v.BuVal","");
        component.set("v.closedDateVal","");
        component.set("v.validationMessagesList",null);
        event.preventDefault();
        if(component.get("v.isClone")==true) {            
            helper.redirectToOriginatingOppURL(component, event, helper);
        } else {            
            helper.redirectToStartURL(component, event, helper);
        }        
    }, 
    reInit : function(component, event, helper) {
        helper.reInit(component, event, helper); 
    },

})