({
    redirectToOpportunityURL : function (component, event, helper) {
        var optyUrl = "/lightning/r/Opportunity/"+component.get("v.newOpportunityId")+"/view";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": optyUrl,
            "isredirect": true
        });
        urlEvent.fire();
    },
    redirectToStartURL : function (component, event, helper){      
        var strtUrl ="/lightning/r/Account/"+component.get("v.parentrecordId")+ "/view";        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": strtUrl,
            "isredirect":true
        });
        urlEvent.fire();        
    },
    redirectToOriginatingOppURL : function (component, event, helper){      
        var optyUrl = "/lightning/r/Opportunity/"+component.get("v.tgsOppId")+"/view";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": optyUrl,
            "isredirect": true
        });
        urlEvent.fire();        
    },
    reInit : function(component, event, helper){
          $A.get('e.force:refreshView').fire();
    },
    showToastMessages : function( title, message, type ) {
        var toastEvent = $A.get( 'e.force:showToast' );
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });        
        toastEvent.fire();
    }
})