({
	utility :function(cmp, event, helper) {
        var action = cmp.get("c.getRedzoneLink");
     
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
             
               let urlis =  response.getReturnValue();
              
                cmp.set('v.redzoneurl',urlis);
              	helper.openUrl(cmp,event,helper);
     
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
     
	},
    openUrl:function(cmp,event,helper){
        let urlis =   cmp.get('v.redzoneurl');
         window.open(urlis,'_blank');
    	helper.closeUtilityBar(cmp); 
    },
    closeUtilityBar:function(component){
        setTimeout(()=>{
       
         var utilityBarAPI = component.find("utilitybar");
       	 utilityBarAPI.minimizeUtility();     
        },200)
       
    }
})