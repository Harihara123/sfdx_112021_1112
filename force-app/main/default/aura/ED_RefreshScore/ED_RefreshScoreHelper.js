({
    updateCheck11_helper : function(c,e,h) {
        //alert('aaaa');
        c.set("v.btnDisable",true);
        var save_action = c.get("c.updateCheck");
        save_action.setParams({"recordId": c.get("v.recordId")
            });
         // Create a callback that is executed after
        // the server-side action returns
        save_action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned
                // from the server
                //alert("From server: " + response.getReturnValue());
                c.set("v.btnDisable",false);
 				$A.get('e.force:refreshView').fire();
 				//location.reload();

                // You would typically fire a event here to trigger
                // client-side notification that the server-side
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                //alert("incomplete");
                c.set("v.btnDisable",false);
            }
            else if (state === "ERROR") {
                c.set("v.btnDisable",false);
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(save_action);
    }

})