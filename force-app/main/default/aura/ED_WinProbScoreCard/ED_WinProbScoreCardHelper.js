({
	getWinProbDashboardId : function(component, event, helper) 
    {
        var recordId = component.get('v.recordId');
        var oppDetail = component.get("v.simpleRecord");
        component.set("v.Division",oppDetail.Req_Division__c);
        console.log("Division Name is " +oppDetail.Req_Division__c);
        
        var action = component.get("c.getDashboardId");
        action.setParams({ 
           "Division": oppDetail.Req_Division__c
         });
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') { 
                var model = data.getReturnValue();
                component.set("v.DashboardId",model.Dashboard_Id__c);
                 component.set("v.DataSetName",model.DataSet_Name__c);
                console.log("DashboardId is" +component.get("v.DashboardId"));
               
            }else if (data.getState() == 'ERROR'){
                
            }
        });
        $A.enqueueAction(action); 
    }
})