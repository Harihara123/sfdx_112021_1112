({
	doInit : function(component, event, helper){
		var recId = component.get("v.recordId");
        var action = component.get("c.getEnterpriseCurrentsJSON");
        component.set('v.talentcolumns', [
            {label: 'Division', fieldName: 'division', type: 'text'},
                {label: 'Owner', fieldName: 'ownerId', type: 'url', typeAttributes: {label: { fieldName: 'ownerName' }, target: '_blank'}},
                {label: 'Hiring Manager', fieldName: 'hiringmanagerId', type: 'url', typeAttributes: {label: { fieldName: 'hiringmanager' }, target: '_blank'}},
                {label: 'Talent Name', fieldName: 'candidateId', type: 'url', typeAttributes: {label: { fieldName: 'candidatename' }, target: '_blank'}},
                {label: 'Job Title', fieldName: 'jobtitle', type: 'text '},
                {label: 'Start Date', fieldName: 'startdate', type: 'text '},
                {label: 'End Date', fieldName: 'enddate', type: 'text '},
                {label: 'Bill Rate', fieldName: 'billrate', type: 'text '}
            ]);
        action.setParams({"acctId":  recId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log('===='+dataObj);
                var res; 
                var talentList = [];
                var talentFormerList = [];
                var currentTalents = [];
                var formerTalents = [];
                res = JSON.parse(dataObj);
                currentTalents = res.entcurrentTalents;
                formerTalents = res.entformerTalents;
                component.set("v.masterGlobalAccountId",res.masterGlobalAccountId);
                if(currentTalents !== null && currentTalents.length > 0){
                     for (var i = 0; i < currentTalents.length; i++){
                         talentList.push(currentTalents[i]);
                     }
                     talentList.forEach(function(talent){
                          talent.ownerId = '/'+talent.ownerId;
                          talent.hiringmanagerId = '/'+talent.hiringmanagerId;
                          talent.candidateId = '/'+talent.candidateId;
                     });
                    component.set("v.displayedCurrents","showcurrent");
                    component.set("v.currents",talentList);
                }
                if(formerTalents !== null && formerTalents.length > 0){
                    for (var i = 0; i < formerTalents.length; i++){
                         talentFormerList.push(formerTalents[i]);
                     }
                     talentFormerList.forEach(function(talent){
                          talent.ownerId = '/'+talent.ownerId;
                          talent.hiringmanagerId = '/'+talent.hiringmanagerId;
                          talent.candidateId = '/'+talent.candidateId;
                     });
                    component.set("v.displayedFormers","showformer");
                    component.set("v.formers",talentFormerList);
                }
               component.set("v.IsEntSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    
    viewDetails : function(component, event, helper) {
		var accId = component.get("v.masterGlobalAccountId");
        var urllink = '/lightning/r/Report/00O1p000004lMzpEAE/view?t=1479844235107&fv1='+accId;
        var urlEvent = $A.get("e.force:navigateToURL");
        window.open(urllink, '_blank');
	},
    
    viewFormerDetails: function(component, event, helper) {
		var accId = component.get("v.masterGlobalAccountId");
        var urllink = '/lightning/r/Report/00O1p000004lMzqEAE/view?t=1479844235107&fv1='+accId;
        var urlEvent = $A.get("e.force:navigateToURL");
        window.open(urllink, '_blank');
	}
})