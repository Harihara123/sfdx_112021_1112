({
	handleClick: function(component, event, helper) {
        var btnClicked = event.getSource();         // the button
        var btnMessage = btnClicked.get("v.label"); // the button's label
        console.log("handleClick: Message: " + btnMessage);

        component.set("v.message", btnMessage);     // update our message
        
    },
    
    handleClick2: function(component, event, helper) {
    var newMessage = event.getSource().get("v.label");
    component.set("v.message", newMessage);
},

    handleClick3: function(component, event, helper) {
    component.set("v.message", event.getSource().get("v.label"));
},
    
    handleSelect : function(component, event, helper){
        var selectedMenuItemValue = event.getParam("value");
        var reportLabel = '';
        
        if(selectedMenuItemValue === 'GlobalAccountPlans'){
            reportLabel = 'Global Account Plans';
        }else if(selectedMenuItemValue === 'GlobalAccountEvents'){
            reportLabel = 'Global Account Events';
        }else if(selectedMenuItemValue === 'GlobalAccountOpportunities'){
            reportLabel = 'Global Account Opportunities';
        }else if(selectedMenuItemValue === 'AssociatedContacts'){
            reportLabel = 'Associated Contacts';
        }else if(selectedMenuItemValue === 'StartedCandidates'){
            reportLabel = 'Started Candidates';
        }else{ 
        }
        
        var action = component.get("c.geturlGlobalAccountPlans");  
          action.setParams({
            "accId": component.get("v.recordId"),"ReportLabel": reportLabel, "isEnterprise" : component.get("v.accountEnterprise")
        });
        
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS'){
                var urllink = data.getReturnValue();
                if(urllink != 'none'){
                    window.open(urllink, '_blank');
                }       
            }  
        });
        
         $A.enqueueAction(action);
    }
})