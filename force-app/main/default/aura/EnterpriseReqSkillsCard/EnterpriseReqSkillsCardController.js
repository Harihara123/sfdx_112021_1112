({
	doInit : function(component, event, helper) {
        var opptyId = component.get("v.recordId");
        console.log('final');
        console.log(opptyId);
		var action = component.get("c.getEnterpriseReqSkills");
          action.setParams({"oppId":  opptyId});
         action.setCallback(this, function(response) {
                            // Opp successfully created.
                            if (response.getState() === "SUCCESS") {
                                var model = response.getReturnValue();
                                console.log(model);
                                var oppList = model.skillList;
                                oppList.sort(function compare(a,b){
                                  if (a.favorite > b.favorite)
                                    return -1;
                                  if (a.favorite < b.favorite)
                                    return 1;
                                  return 0;
                                });
                                console.log(oppList);
                                component.set("v.Ownership",model.Opco);
                                component.set("v.isSkillsPresent", "true");
                                component.set("v.skills",oppList);
                             }
                          });
           $A.enqueueAction(action); 
   },
    handleEditClick : function(component, event, helper){
        component.set("v.isSkillsPresent",false);
        console.log(event);
        var finalSkills = [];
        var skillList = component.get("v.skills");
        console.log(skillList);
        console.log('looper 6');
        for (var i = 0; i < skillList.length; i++) { 
            if(skillList[i].favorite){
               finalSkills.push({
					"term" : skillList[i].name,
					"rawWeight" : 1,
					"required" : true
				});
            }else{
                finalSkills.push({
					"term" : skillList[i].name,
					"rawWeight" : 0,
					"required" : false
				});
            }
        }
        console.log(finalSkills);
        component.set("v.isSkillsSave","true");
        component.set("v.editSkills",finalSkills);
    },
    handleCancelClick :function(component, event, helper){
        component.set("v.isSkillsPresent", "true");
        component.set("v.isSkillsSave","false");
                               
    },
    handleSaveClick : function(component, event, helper){
        console.log('save'); 
         var pillData = component.get("v.automatchSkills");
            var favSkills ='';
            var regularSkills ='';
            var finalSkills ='';
            var pillList = [];
               for (var j=0; j<pillData.length; j++) {
                if(pillData[j].required === "true" || pillData[j].required === true){
                    favSkills += pillData[j].key + ',';
                    var jsonFavSkill = {"name": pillData[j].key, "favorite":true};
                    pillList.push(jsonFavSkill);
                 }else{
                    regularSkills += pillData[j].key + ',';
                    var jsonSkill = { "name": pillData[j].key, "favorite":false };
                    pillList.push(jsonSkill);
                }   
            }
        
           finalSkills = JSON.stringify(pillList);
        
        console.log('finalk skills');
        console.log(finalSkills);
        component.set("v.oppSkills",finalSkills);
        
        var reqEditedSkills = component.get("v.oppSkills");
        
        var opptyId = component.get("v.recordId");
        console.log('saving updating');
        console.log(opptyId);
        
        var action = component.get("c.updateSkillsforReq");
          action.setParams({"oppId":  opptyId, "skills":reqEditedSkills});
         action.setCallback(this, function(response) {
                            // Opp successfully created.
                            if (response.getState() === "SUCCESS") {
                                console.log(response.getReturnValue()); 
                                var model = response.getReturnValue();
                                var oppList = model.skillList;
                                console.log(oppList);
                                oppList.sort(function compare(a,b){
                                  if (a.favorite > b.favorite)
                                    return -1;
                                  if (a.favorite < b.favorite)
                                    return 1;
                                  return 0;
                                });
                                
                                component.set("v.Ownership",model.Opco);
                                component.set("v.isSkillsPresent", "true");
                                component.set("v.isSkillsSave","false");
                                component.set("v.skills",oppList);
                             }
                          });
           $A.enqueueAction(action);
        
    }
})