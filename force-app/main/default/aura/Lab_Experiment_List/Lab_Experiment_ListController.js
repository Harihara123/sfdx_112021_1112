({
    doInit: function(component, event, helper) {
        // Fetch the account list from the Apex controller
        helper.getExperimentList(component);
    },
    goToRecord:function(component, event, helper)
    {
        var id = event.getSource().get("v.class");
        var sobjectEvent=$A.get("e.force:navigateToSObject");
        sobjectEvent.setParams({
            "recordId": id
        });
        sobjectEvent.fire();
        
    }
})