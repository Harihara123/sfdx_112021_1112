({
      // Fetch the accounts from the Apex controller
      getExperimentList: function(component) {
        var soql = 'select Id, name, Description__c, Short_Description__c, LastModifiedDate, LastModifiedBy.Name, External_Image_Url__c, External_URL__c from Lab_Experiment__c order by name';
        var serverMethod = 'c.performServerCall';
        var params = {"soql": soql, "maxRows": "100"};
         
        var actionParams = {'className':'',
                            'subClassName':'',
                            'methodName': 'getRows',
                            'parameters': params};

    
        var action = component.get(serverMethod);
        action.setParams(actionParams);
          
        // Set up the callback
        action.setCallback(this, function(actionResult) {
         	component.set('v.experiments', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
      }
})