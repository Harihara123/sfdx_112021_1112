({
    createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Lab_Experiment__c"
        });
        createRecordEvent.fire();
    }
})