({
	onFocus : function(component, event, helper) {

        if (event.type === 'keydown' && event.keyCode == 13) {
            event.preventDefault();
            console.log('HERE GOES OUT SEARCH');
            // invoke search action
            return;
        }
        
        if (event.type === 'keydown' && event.keyCode == 8) {
            console.log('deleting');
            helper.onCut(component);
            return;
        }
        
        let textarea = document.getElementById(component.get('v.id'));
        let rows = textarea.getAttribute('rows');
        let containerWidth = document.getElementById(component.get('v.data-id')).clientWidth;
    	textarea.style.width = containerWidth - 8 + 'px';
    	while (textarea.scrollHeight > textarea.offsetHeight) {

      rows = +rows + 1;
      if (rows > component.get('v.maxRows')) {
        textarea.style.overflowY = 'scroll';  
        rows = component.get('v.maxRows');
        textarea.setAttribute('rows', rows);
        break;
      }
        textarea.setAttribute('rows', rows);
    }            
        let container = document.getElementById(component.get('v.data-id'));
        let truncatedClass = container.classList.contains('truncated-container');
        if (truncatedClass) {
            container.classList.remove('truncated-container')
        }
        let focusedClass = container.classList.contains('focused-div-container');
        if (!focusedClass) {
            container.classList.add('focused-div-container')
        }
        let txtcontainer = document.getElementById(component.get('v.id')).parentNode;
        let truncatedContainerClass = txtcontainer.classList.contains('truncated-container');
        if (truncatedContainerClass) {
            txtcontainer.classList.remove('truncated-container')
        } 
	},
    
	onBlur : function(component, event, helper) {
        let textarea = document.getElementById(component.get('v.id'));
        textarea.style.overflow = 'hidden';
        textarea.setAttribute('rows', 1);
        
        let txtcontainer = document.getElementById(component.get('v.id')).parentNode;
		let rows = textarea.getAttribute('rows');
		while (textarea.scrollHeight > textarea.offsetHeight) {
		rows = +rows + 1;
		if (rows > component.get('v.minRows')) {
		textarea.style.overflowY = 'scroll';  
		rows = component.get('v.minRows');
		txtcontainer.classList.add('truncated-container');
		let containerWidth = document.getElementById(component.get('v.data-id')).clientWidth;
		textarea.style.width = containerWidth - 8 + 'px';

		textarea.setAttribute('rows', rows);
		break;
		}
		textarea.setAttribute('rows', rows);
		} 
        document.getElementById(component.get('v.data-id')).classList.toggle('focused-div-container');

    },
    
    onCut: function(component, event, helper) {
        helper.onCut(component);
    },
    
    minChanged: function(cmp, event, helper) {
let textarea = document.getElementById(cmp.get('v.id'));
let rows = textarea.getAttribute('rows');
while (textarea.scrollHeight > textarea.offsetHeight) {
rows = +rows + 1;
if (rows > cmp.get('v.minRows')) {
textarea.style.overflowY = 'scroll';  
rows = cmp.get('v.minRows');
let txtcontainer = document.getElementById(cmp.get('v.id')).parentNode;
txtcontainer.classList.add('truncated-container');
let containerWidth = document.getElementById(cmp.get('v.data-id')).clientWidth;
textarea.style.width = containerWidth - 8 + 'px';

textarea.setAttribute('rows', rows);
break;
}
textarea.setAttribute('rows', rows);
}            
    }
})