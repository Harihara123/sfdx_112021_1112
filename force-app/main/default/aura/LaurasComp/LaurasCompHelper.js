({
	onCut : function(cmp) {
		setTimeout(() => {
		let textarea = document.getElementById(cmp.get('v.id'));
		textarea.style.overflow = 'hidden';
        textarea.setAttribute('rows', 1);
        
        let txtcontainer = document.getElementById('text-area-container');
		let rows = textarea.getAttribute('rows');
		while (textarea.scrollHeight > textarea.offsetHeight) {
		rows = +rows + 1;
		if (rows > 10) {
		textarea.style.overflowY = 'scroll';  
		rows = 10;
		txtcontainer.classList.add('truncated-container');
		let containerWidth = document.getElementById(cmp.get('v.data-id')).clientWidth;
		textarea.style.width = containerWidth - 8 + 'px';

		textarea.setAttribute('rows', rows);
		break;
		}
		textarea.setAttribute('rows', rows);
		}
    }, 0);
	}
})