({
	doInit : function (component, event, helper) {
		helper.loadTable(component);
		helper.loadComponentGeneralInfo(component);
		//helper.loadResume(component, event);
		
	},
	handleEdit: function (component, event, helper) {

	},
	handleRowAction : function(component, event, helper) {
		console.log('Event Details:'+JSON.stringify(event.getParam('rowDetails')));
		let cells = event.getParam('rowDetails').cells;

		helper.handleRowAction(component, cells);
		
	},
	handelFireEvent : function(component, event, helper) {
		console.log('tableData Details:'+JSON.stringify(event.getParam('tableData')));
	}
})