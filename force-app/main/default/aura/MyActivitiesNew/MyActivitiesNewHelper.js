({
	loadTable: function (component) {
		/*let tableCol = [
							{label:'Contact', field:'contactName',sort: true},
							{label:'Type', field:'type',sort: true},
							{label:'Due Date', field:'dueDate', sort: true},
							{label:'Subject', field:'subject', sort: true},					
							{label:'Related To', field:'relatedTo',sort: true},
							{label:'Status', field:'status', sort: true},
							{label:'Priority', field:'priority',sort: true},
							{label:'Contact Type', field:'contactType', sort: true}
						  ];*/
		//console.log('tableCol'+JSON.stringify(tableCol));					
		let actions = [
						{action: 'view', icon: 'utility:summarydetail'}
					  ];
		console.log('actions'+JSON.stringify(actions));
		//component.set("v.tableCol", tableCol);
		component.set("v.actions", actions);
		this.getTableData(component);
	},
	//use try-catch
	getTableData : function (component) {
		let action =component.get("c.getMyActivities");

		 action.setCallback(this, function(response) {
			let state = response.getState();
            if(state === 'SUCCESS'){
				let dtResult = response.getReturnValue();
				

				let tmpTableData = [];

				for(let i=0; i < dtResult.length; i++) {
					let tmpTableRecord = {id:dtResult[i].Id, 
										  cells : [{value:dtResult[i].contactName, navItem: {object: 'Contact', id: dtResult[i].contactId, name:'contactName'}},
												  {value:dtResult[i].type, name:'type'},
												  {value:dtResult[i].dueDate, name:'dueDate'},
												  {value:dtResult[i].subject, name:'subject'},												  
												  {value:dtResult[i].relatedTo, navItem: {object: 'Account', id: dtResult[i].relatedToId}, name:'relatedTo'},
												  {value:dtResult[i].status, name:'status'},
												  {value:dtResult[i].priority, name:'priority'},												  
												  //{value:dtResult[i].assignedTo, navItem: {object: 'User', id: dtResult[i].assignedToId, name:'assignedTo'}},
												  {value:dtResult[i].contactType, name:'contactType'}]
										}
					tmpTableData.push(tmpTableRecord);
				}
				if(tmpTableData.length > 0) {
					component.set("v.tableData", tmpTableData);
					this.handleRowAction(component, tmpTableData[0].cells);
					console.log('results==='+JSON.stringify(tmpTableData));
				} else {
					component.set("v.tableData", "");
				}
			}
		 });
		 $A.enqueueAction(action);
	},
	//use try-catch
	loadComponentGeneralInfo : function(cmp) { //will recheck again if this method required here or Activity timeline
		let action = cmp.get("c.currentUserWithOwnership");
		
		action.setCallback(this, function(response) {
			let state = response.getState();
			if(state === 'SUCCESS'){
				cmp.set('v.runningUser',response.usr);
				cmp.set('v.runningUserCategory', response.userCategory);
				cmp.set('v.usrOwnership', response.userOwnership);						
			}
		 });
		  $A.enqueueAction(action);
	},

	handleRowAction : function(component, cells) {
		for(let i=0; i<cells.length; i++) {
			if(cells[i].navItem) {
				if(cells[i].navItem.object === 'Account') {
					let accId = cells[i].navItem.id;
					if(accId.substring(0, 3) === '001') {
						component.set("v.accountId",cells[i].navItem.id); 					
					}
					
				}
				if(cells[i].navItem.object === 'Contact') {
					let accId = cells[i].navItem.id;
					if(accId.substring(0, 3) === '003') {
						component.set("v.contactId",cells[i].navItem.id); 					
					}
					
				}
			}
			if(cells[i].name && cells[i].name === 'contactType') {
				component.set("v.contactType", cells[i].value);
				
			}
		}
		if(component.get("v.accountId")) {
			let contactRec = {"fields":{"AccountId":{"value":component.get("v.accountId")}}};
			component.set("v.contactRecord", contactRec);
			component.set("v.talentId",component.get("v.contactId"));
			component.set("v.enableActivity","true");

			//set Resume for talent
			if(component.get('v.contactType') === 'Talent') {
				this.createTalentResumePanel(component,'v.C_TalentResumePreview');        
				
			} 
		}
	},
	createComponent : function(cmp, componentName, targetAttributeName, params) {
         $A.createComponent(
            componentName,
            params,
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    cmp.set(targetAttributeName, newComponent);
                }
                else if (status === "INCOMPLETE") {
                    //console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    //console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
	loadResume : function(component, recordId) {
        //var recordID = '00PP0000001upMGMAY';
        console.log('recordID:'+recordId);
        if(recordId){
            
			var targetAttributeName = "v.C_TalentResumeHTMLViewer";
			var componentName = "c:C_TalentResumeHTMLViewer";//rajeesh
            var params={"recordId":recordId};//rajeesh
            
            this.createComponent(component, componentName, targetAttributeName, params);
        }
        else{
            //component.set("v.C_IFrameDisplay","");
			component.set("v.C_TalentResumeHTMLViewer","");
        }
      },
	  createTalentResumePanel: function(component,targetAttribute){
        var recordId = component.get("v.accountId");
        var params = {'recordId' : recordId};

		var serverMethod = 'c.getAttachmentDetails';
        var actionParams = {'parameters': params};

    
        let getResumeDetails = component.get(serverMethod);
        getResumeDetails.setParams(actionParams);
		getResumeDetails.setCallback(this, function(response) {
			let state = response.getState();
			if(state === 'SUCCESS'){
				console.log('Response:'+JSON.stringify(response.getReturnValue()));
				let result = response.getReturnValue();
				if(result !='' || result != null) {
					if(result.recordAndCount != null ) {
						if(result.recordAndCount.records.length > 0)
							this.loadResume(component,result.recordAndCount.records[0].Id);
						else	
							this.loadResume(component,'');
					}
				}
				//cmp.set('v.runningUser',response.usr);
				//cmp.set('v.runningUserCategory', response.userCategory);
				//cmp.set('v.usrOwnership', response.userOwnership);						
			}
		 });
		  $A.enqueueAction(getResumeDetails);
        
    }

})