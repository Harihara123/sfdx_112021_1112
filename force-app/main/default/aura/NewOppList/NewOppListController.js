({
    doInit : function(component, event, helper) {
        helper.getTekRecTypeAccess(component, event, helper);
        helper.getOfficeDetails(component, event, helper);
        helper.getObjName(component, event, helper);
        helper.getModalOpen(component, event, helper);
        //helper.getIsGrpMember(component, event, helper);      //S-223083
        //helper.getIsAerotekGrpMember(component, event, helper);       //S-223083
    },                        
    setRadioValue : function(component, event, helper){
        var radioBtn = event.target.value;
        if(radioBtn !=null){
            component.set("v.isDisabled", false);
            //component.set("v.isError", false);
        }
        console.log('button selected '+radioBtn);
        component.set("v.radioSelected" , radioBtn);         
    },    
    hideOpportunityModal : function(component, event, helper) {
        helper.toggleClassInverse(component,'opptySelectionModal','slds-fade-in-');
        $A.get("e.force:closeQuickAction").fire();
        console.log('closed comp');
    },
    navigateToOppRecPage : function(component, event, helper){        
        if(component.get("v.radioSelected") == 'reqOpp'){
            //if(component.get("v.companyName")== 'TEKsystems, Inc.' && component.get("v.isGroupMember") == true){    //S-223083        
            if(component.get("v.companyName")== 'TEKsystems, Inc.'){                
                helper.navigateToTEKComponent(component, event, helper);
            //}else if(component.get("v.companyName")== 'Aerotek, Inc' && component.get("v.isAerotekGroupMember") == true){     //S-223083
            }else if(component.get("v.companyName")== 'Aerotek, Inc'){
                helper.navigateToNewAerotekComponent(component, event, helper);
            }else{
                helper.navigateToMyComponent(component, event, helper);
            }            
        }else if(component.get("v.radioSelected") == 'ONSreqOpp'){
            helper.navigateToAerotekComponent(component, event, helper);
        }else if(component.get("v.radioSelected") == 'EMEAreqOpp'){
            helper.navigateToEMEAComponent(component, event, helper);
        }else if(component.get("v.radioSelected") == 'strategicOpp'){
            helper.navigateToFlexITComponent(component, event, helper);
        }else if(component.get("v.radioSelected") == 'strategicFlex'){
            helper.navigateToFlexITComponent(component, event, helper);
        }else if(component.get("v.radioSelected") == 'tgsOpp'){
             helper.navigateToTGSComponent(component, event, helper);
        }else if(component.get("v.radioSelected") == 'eASiOpp'){
            helper.createRecord(component, event, helper);
        }        
    }
})