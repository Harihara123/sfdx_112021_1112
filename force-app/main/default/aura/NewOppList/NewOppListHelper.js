({
    getOfficeDetails : function(component, event, helper) {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') { 
                var model = data.getReturnValue();
                component.set("v.officeName",model.Office_Code__c);
                component.set("v.companyName",model.CompanyName);
            }else if (data.getState() == 'ERROR'){
                
            }
        });
        $A.enqueueAction(action);
    },
    getModalOpen : function(component, event, helper){
        this.toggleClass(component,'opptySelectionModal','slds-fade-in-');
    },
    
    getTekRecTypeAccess : function(component,event,helper)
    {
        var action = component.get("c.getRTAccessDetailsForProfiles");
        action.setCallback(this,function(response){
            var resState = response.getState();
            if(resState == 'SUCCESS')
            {
                var responseVal = response.getReturnValue();
                component.set("v.TekSystemFlag",responseVal);
            }
        });
        $A.enqueueAction(action);
    },
    //S-223083 starts
    // getIsGrpMember : function(component,event,helper)
    // {
    //     var action = component.get("c.isGroupMember");
    //     action.setCallback(this,function(response){
    //         var resState = response.getState();
    //         if(resState == 'SUCCESS')
    //         {
    //             var responseVal = response.getReturnValue();
    //             component.set("v.isGroupMember",responseVal);
    //             console.log(responseVal);
    //         }
    //     });
    //     $A.enqueueAction(action);
    // },
    
    // getIsAerotekGrpMember : function(component,event,helper)
    // {
    //     var action = component.get("c.isAerotekGroupMember");
    //     action.setCallback(this,function(response){
    //         var resState = response.getState();
    //         if(resState == 'SUCCESS')
    //         {
    //             var responseVal = response.getReturnValue();
    //             component.set("v.isAerotekGroupMember",responseVal);
    //             console.log(responseVal);
    //         }
    //     });
    //     $A.enqueueAction(action);
    // },
    //S-223083 ends
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    getObjName : function(component, event, helper){
        var action = component.get("c.getObjName"); 
        if (component.get("v.recordId")){
            action.setParams({"recordId": component.get("v.recordId")}); 
        }
        action.setCallback(this, function(response) {  
            var state = response.getState(); 
            if(component.isValid() && state === 'SUCCESS'){
                var objName =  response.getReturnValue();
                if (objName != null){
                    component.set('v.objName',objName[0]);
                    component.set('v.objNameVal',objName[1]);
                }
            }
        });
        $A.enqueueAction(action);        
    },
    navigateToFlexITComponent : function(component, event, helper) {
        var tgsOppId;
        if(component.get("v.radioSelected") == 'strategicFlex')
            tgsOppId = 'Flexible Capacity';
        else
            tgsOppId = 'NonFlex';
        var navCmp = component.find("navService");
        var accid = component.get("v.objName");
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__CRM_CreateFlexITReq"
            }, 
            "state": {
                "c__recId" : accid,
                "c__tgsOppId" : tgsOppId,
                "c__pId" : component.get("v.recordId"),
                "c__accName" : component.get("v.objNameVal")
            }
        };        
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");
        navCmp.navigate(pageReference);
        
    },
    navigateToMyComponent : function(component, event, helper) {        
        var navCmp = component.find("navService");
        var accid=component.get("v.recordId");        
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__CreateEnterpriseReq"
            }, 
            "state": {
                "c__recId" : accid,
                "c__tgsOppId" : ''
            }
        };        
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");
        //event.preventDefault();
        navCmp.navigate(pageReference);
    },
    
    navigateToAerotekComponent : function(component, event, helper) {        
        var navCmp = component.find("navService");
        var accid=component.get("v.recordId");        
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__CreateEnterpriseAerotekReq"
            }, 
            "state": {
                "c__recId" : accid,
                "c__tgsOppId" : ''
            }
        };        
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");
        //event.preventDefault();
        navCmp.navigate(pageReference);
    },
    
    navigateToEMEAComponent : function(component, event, helper) {        
        var navCmp = component.find("navService");
        var accid=component.get("v.recordId");        
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__CreateEnterpriseEmeaReq"
            }, 
            "state": {
                "c__recId" : accid,
                "c__tgsOppId" : ''
            }
        };        
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");
        //event.preventDefault();
        navCmp.navigate(pageReference);
    },
    
   navigateToTGSComponent : function(component, event, helper) {        
        var navCmp = component.find("navService");
       var accid=component.get("v.objName"); 
       var tgsOppId = '';  
       var officeVal =component.get("v.officeName")
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__Create_TGS_Opportunity"
            }, 
            "state": {
                "c__recId" : accid,
                "c__tgsOppId" : 'TGSOpp',
                "c__pId" : component.get("v.recordId"),
                "c__accName" : component.get("v.objNameVal"), 
                "c__office" : ''
            }
        };        
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");

        navCmp.navigate(pageReference);
    },
    navigateToTEKComponent : function(component, event, helper) {        
        var navCmp = component.find("navService");
        var accid=component.get("v.recordId");        
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__CreateEnterpriseTEKReq"
            }, 
            "state": {
                "c__recId" : accid,
                "c__tgsOppId" : ''
            }
        };        
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");        
        navCmp.navigate(pageReference);
    },
    
    navigateToNewAerotekComponent : function(component, event, helper) {        
        var navCmp = component.find("navService");
        var accid=component.get("v.recordId");        
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__CreateEnterpriseAerotekReq_New"
            }, 
            "state": {
                "c__recId" : accid,
                "c__tgsOppId" : ''
            }
        };        
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");        
        navCmp.navigate(pageReference);
    },
    createRecord: function(component, event, helper) {                
        var createRecordEvent = $A.get("e.force:createRecord");
        var accid=component.get("v.recordId");        
        var  aid=component.get("v.simpleNewContact.AccountId");    
        var conId ;
        if(aid!=null && aid!='' && typeof aid!="undefined"){
            accid=aid;
            conId = component.get("v.recordId");
        }else{
            accid=component.get("v.recordId");
            //conId = '';
        }        
        //helper.getOfficeDetails(component, event, helper); 
        console.log(accid);        
        //var selectedRadioOption = component.find("r4");
        //var isTgsOpp = selectedRadioOption.get("v.value");
        var recType ;
		var defaultOpCo;
        var defaultBU;
        var defaultStage;
        var defaultProbability;
        if(component.get("v.radioSelected") == 'tgsOpp'){
            recType = "012U0000000DWp4"; //TEKsystems Global Services Opportunity record type
			defaultOpCo = 'TEKsystems, Inc.';
            defaultBU = 'TEK Global Services'
            defaultStage = 'Interest';
            defaultProbability = 0
        }else if(component.get("v.radioSelected") == 'eASiOpp'){
            recType = "012U0000000DWp3"; //EASi Services Opportunity record type
        }
        createRecordEvent.setParams({
            "entityApiName": "Opportunity", // using account standard object for this sample
            "recordTypeId": recType, // Optionally Specify Record Type Id            
            "defaultFieldValues": {
                'AccountId' : accid,
                'OpCo__c' : component.get("v.companyName"),
                'Organization_Office__c' : component.get("v.officeName"),
                'Req_Hiring_Manager__c' : conId,
                'OpCo__c' : defaultOpCo,
                'BusinessUnit__c' : defaultBU,
                'StageName' : defaultStage,
                'Probability' : defaultProbability                
            }            
        });
        
        console.log('closing'); 
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        
        createRecordEvent.fire();
        component.destroy();         
    }
})