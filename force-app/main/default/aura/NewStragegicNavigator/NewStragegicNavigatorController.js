({	    
	navigateToMyComponent : function(component, event, helper) {        
       	var  aid=component.get("v.simpleNewOpportunity.AccountId");
      	var flag = helper.validateComponent(component);
        if(flag){
            helper.getOfficeDetails(component, event, helper);        	         		
        }
	},
	reInit : function(component, event, helper) {
        helper.reInit(component, event, helper); 
    }   
})