({
	validateComponent : function(component){
        var stage=component.get("v.simpleNewOpportunity.StageName"); //Interest,Qualifying,Closed Lost,Closed Wash
        var recTypeid=component.get("v.simpleNewOpportunity.RecordTypeId");
        //alert('recTypeid11::'+recTypeid);
        if(recTypeid == '012U0000000DWp4IAG'|| recTypeid == '012U0000000DWp3IAG'){
            return true;
        }else{
            if(stage=='Interest' || stage=='Qualifying' || stage=='Closed Lost' || stage=='Closed Wash' || stage=='Closed No Bid' || stage=='Closed No Pursue' ){
                this.redirectToStartURL(component);
                this.showError('You cannot create req opportunity from a Strategic Opportunity at Interest, Qualifying, or any Closed stages except Closed Won.','Error');
                return false; 
            }else{
                return true;
            }
        }
    },
    showError : function(errorMessage, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'sticky',
            title: title,
            message: errorMessage,
            duration:' 5000',
            type: 'error'
        });
        toastEvent.fire();
    }, 
    redirectToStartURL : function (component){
      	var strtUrl ="/lightning/r/Opportunity/"+component.get("v.oppId")+ "/view";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": strtUrl,
            "isredirect":true
        });
         urlEvent.fire();
    },
    reInit : function(component, event, helper){
        $A.get('e.force:refreshView').fire();
    },
    navigateToMyComp : function(component, event, helper) {
        var navCmp = component.find("navService");
        var soppid=component.get("v.oppId");
        var recId=component.get("v.recId");
        var OppRecTypeID = component.get("v.simpleNewOpportunity.RecordTypeId");
        if(OppRecTypeID =='012U0000000DWp4IAG' || OppRecTypeID == '012U0000000DWp3IAG'){                 
        }else{
        	OppRecTypeID = '';
        }
        var pageReference = {
        	"type": "standard__component",
            "attributes": {
            	"componentName": "c__CreateEnterpriseReq"
            }, 
            "state": {
            	"c__recId" :recId,
                "c__soppId" :soppid,
				"c__oppRecId" :OppRecTypeID
            }
        };
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");        
        navCmp.navigate(pageReference);
    },
    navigateToEMEAComponent : function(component, event, helper) {        
        var navCmp = component.find("navService");
        var soppid=component.get("v.oppId");
        var recId=component.get("v.recId");
        var OppRecTypeID = component.get("v.simpleNewOpportunity.RecordTypeId");
        if(OppRecTypeID =='012U0000000DWp4IAG' || OppRecTypeID == '012U0000000DWp3IAG'){                 
        }else{
        	OppRecTypeID = '';
        }
        var pageReference = {
        	"type": "standard__component",
            "attributes": {
            	"componentName": "c__CreateEnterpriseEmeaReq"
            },             
            "state": {
            	"c__recId" :recId,
                "c__soppId" :soppid,
                "c__oppRecId" :OppRecTypeID
            }            
        };
        component.set("v.pageReference", pageReference);
        var pageReference = component.get("v.pageReference");        
        navCmp.navigate(pageReference);
    },
    getOfficeDetails : function(component, event, helper) {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') { 
                var model = data.getReturnValue();
                //component.set("v.officeName",model.Office_Code__c);
                component.set("v.companyName",model.CompanyName);
                if(model.CompanyName == "AG_EMEA"){                    
                    this.navigateToEMEAComponent(component, event, helper);
                }else{
                    this.navigateToMyComp(component, event, helper);
                }
            }else if (data.getState() == 'ERROR'){
                
            }
        });
        $A.enqueueAction(action);
    }    
})