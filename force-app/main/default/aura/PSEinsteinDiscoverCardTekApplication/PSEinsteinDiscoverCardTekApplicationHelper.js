({
    round : function(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        var outputHTML = "";
        
        console.log('value1: ' + value);
        console.log("#####" + Math.round(value * multiplier) / multiplier);
        multiplier = Math.round(value * multiplier) / multiplier;
        return multiplier;
    },
    
    getColor : function(value, ranges) {
        
        var pass = 1;
        var color = 'score-grey';
        
        for (var i=0; i<ranges.length; i++)
        {
            console.log('value='+value+'range='+ranges[i]);
            var tmpStr = ranges[i].split(',');
            var lower = parseInt(tmpStr[0].trim());
            var upper = parseInt(tmpStr[1].trim());
            
            if (pass == 1)
            {
                if (value >= lower && value <= upper )
                {
                    color = 'score-red';
                    break;
                }
            }
            else
            {
                if (value > lower && value <= upper )
                {
                    if (pass == 2) color = 'score-yellow';
                    if (pass == 3) color = 'score-green';
                    break;
                }
            }
            pass++;
        }
        
        return color;
    },
    populateTable : function(daysOpen,totalmeetings,input, params) {
        console.log('initial input: ', input);
        if (!input || input.length == 0) {
            console.log('input is undefined');
            input = 'No recommendations detected'
        }
        console.log('input: ', input);
        var inputArr = input.split('<br>');
        console.log('inputArr: ', inputArr);
        
        var outputHTML = "";
        
        var limit = params.limit;
        if (limit != null && limit <= 0)
        {
            limit = null;
        }
        
        
        //ME: New variables
        var total_meetings = 0;
        var time_to_interview = 0;
        var time_to_submittal = 0;
        var tti_optimalTime = 99;
        var tti_scoreStr;
        var tti_scoreNum;
        var tti_newTarget;
        var t1s_optimalTime = 99;
        var t1s_scoreStr;
        var t1s_scoreNum;
        var t1s_newTarget;
        var hmm_optimalNumber = 99;
        var hmm_scoreStr;
        var hmm_scoreNum;
        var hmm_newTarget;
        
        //ME: Loop to get optimal values
 		for (var i = 0; i < inputArr.length; ++i) {
        
        	var cleanStr = inputArr[i].replace(/%/g,'').replace(/__c/g,'').replace(/_/g,' ').replace('+ ','+').replace('- ','-').trim();
            cleanStr = cleanStr.replace(/\b[a-z]/g,function(f){return f.toUpperCase();})
            var scoreStr = cleanStr.substr(0, cleanStr.indexOf(' ')).replace('+','+ ').replace('-','- ');
            var scoreNum = parseFloat(scoreStr.replace('+ ','').replace('- ','-'));
            
            var fis_time_to_interview = cleanStr.includes("Interview");
            var fis_total_meetings = cleanStr.includes("Total Meetings");
            var fis_time_to_submittal = cleanStr.includes("Submittal");
            
            if(fis_time_to_interview){
            	var recLast = cleanStr.substr(cleanStr.indexOf(' ') + 1).replace('Because','').replace('If You Change','').replace('Time To Interview To ','');
                recLast = Number(recLast.substr(recLast.indexOf('To')+3,2));
                if(recLast > daysOpen && recLast < tti_optimalTime){
                    tti_optimalTime = recLast;
                    tti_scoreStr = scoreStr;
                    tti_scoreNum = scoreNum;
                    tti_newTarget = Number(tti_optimalTime) - Number(daysOpen);
                }
            }
            if(fis_time_to_submittal){
            	var recLast = cleanStr.substr(cleanStr.indexOf(' ') + 1).replace('Because','').replace('If You Change','').replace('Time To First Submittal To ','');
                recLast = Number(recLast.substr(recLast.indexOf('To')+3,2));
                if(recLast > daysOpen && recLast < t1s_optimalTime){
                    t1s_optimalTime = recLast;
                    t1s_scoreStr = scoreStr;
                    t1s_scoreNum = scoreNum;
                    t1s_newTarget = Number(t1s_optimalTime) - Number(daysOpen);
                }
            }
            if( fis_total_meetings){
                    var recLast = cleanStr.substr(cleanStr.indexOf(' ') + 1).replace('Because','').replace('If You Change','').replace(' Total Meetings With Hiring Manager To ','');
                    //alert("recLast: " + recLast);
                	recLast = Number(recLast.substring(0,recLast.indexOf('To')-1))+1;
                    if (recLast > totalmeetings && recLast < hmm_optimalNumber){                        
                        hmm_optimalNumber = recLast;
                        hmm_scoreStr = scoreStr;
                        hmm_scoreNum = scoreNum;
                        hmm_newTarget = Number(hmm_optimalNumber) - Number(totalmeetings);
                    }
            }
            
        }
        
        
        
        for (var i = 0; i < inputArr.length; ++i) {
            
            if (limit != null && i >= limit) break;
            
            var scoreStr;
            var scoreNum;
            var desc;
            var is_total_meetings;
            var is_time_to_interview;
            var is_time_to_submittal;
            var is_exclusive;
            var is_red_zone;
            var color = '';
            var recLast;
       
            
            // cleans up the strings by removing any system field traits
            var cleanStr = inputArr[i].replace(/%/g,'').replace(/__c/g,'').replace(/_/g,' ').replace('+ ','+').replace('- ','-').trim();
            cleanStr = cleanStr.replace(/\b[a-z]/g,function(f){return f.toUpperCase();})
            console.log("Clean String: " + cleanStr);
            //alert("Clean String: " + cleanStr);
            
            //ME: new variables that are used when displaying the custom verbiage
            is_total_meetings = cleanStr.includes("Total Meetings");
            is_time_to_interview = cleanStr.includes("Interview");
            is_time_to_submittal = cleanStr.includes("Submittal");
            is_exclusive = cleanStr.includes("Exclusive");
            is_red_zone = cleanStr.includes("Red Zone");
           
            
            // handle format of other smaller phrases
            if(cleanStr.indexOf('other smaller') > 0) {
                scoreStr = cleanStr.substr(0,1) + ' ' + cleanStr.substr(cleanStr.lastIndexOf(' '));
                scoreNum = parseFloat(scoreStr);
                desc = cleanStr.substr(1,cleanStr.lastIndexOf(' '));
                color = this.getColor(scoreNum,params.ranges);
            }
            else if(cleanStr.startsWith('From The Baseline') > 0){
                scoreStr = cleanStr.split(',')[1].replace('+','+ ').replace('-','- ').replace(' + ','+ ').replace(' - ','- ');
                scoreNum = parseFloat(scoreStr.replace('+ ','').replace('- ','-'));
                console.log('baseline score: ' + scoreStr);
                desc = cleanStr.split(',')[0]
                color = this.getColor(scoreNum,params.ranges);
            }
            //ME: added for Hiring Manager Meetings
                else if( is_total_meetings){
                    recLast = cleanStr.substr(cleanStr.indexOf(' ') + 1).replace('Because','').replace('If You Change','').replace(' Contact.Total Meetings To ','');
                    recLast = recLast.substr(0,2);
                    //alert("recLast: " + recLast + " totalmeetings: " + totalmeetings);
                    if (total_meetings == 0 && hmm_newTarget >0 && hmm_optimalNumber < 99){                        
                        scoreStr = hmm_scoreStr;
                		scoreNum = hmm_scoreNum;
                		desc = "Meet with Hiring Manager " + hmm_newTarget + " more time(s)";
                        color = this.getColor(scoreNum,params.ranges);
                        total_meetings = 1;
                    }
                    else
                        continue;
            }
            // ME: Added for Time to Interview
            else if( is_time_to_interview){
                    if (time_to_interview == 0 && tti_newTarget>0 && tti_optimalTime < 99){
                        scoreStr = tti_scoreStr;
                		scoreNum = tti_scoreNum;
                		desc = "Target new Interview within the next " + tti_newTarget + " days";
                        color = this.getColor(scoreNum,params.ranges);
                        time_to_interview = 1;
                    }
                    else
                        continue;
            }
            //ME: Added for Time to First Submittal
            else if( is_time_to_submittal){
                    if (time_to_submittal == 0 && t1s_newTarget>0 && t1s_optimalTime < 99){
                        scoreStr = t1s_scoreStr;
                		scoreNum = t1s_scoreNum;
                		desc = "Deliver First Submittal within the next " + t1s_newTarget  + " days";
                        color = this.getColor(scoreNum,params.ranges);
                        time_to_submittal = 1;
                    }
                    else
                        continue;
            }
            //ME: Added for Exclusive
            else if( is_exclusive){
                    if(cleanStr.includes("True")){
                        scoreStr = cleanStr.substr(0, cleanStr.indexOf(' ')).replace('+','+ ').replace('-','- ');
                        scoreNum = parseFloat(scoreStr.replace('+ ','').replace('- ','-'));
                        desc = "Make Req Exclusive";//cleanStr.substr(cleanStr.indexOf(' ') + 1).replace('Because','').replace('If You Change','Change');
                        color = this.getColor(scoreNum,params.ranges);
                        }
                else
                    continue;
                   
            }
            //ME: Added for Red Zone
            else if( is_red_zone){
                if(cleanStr.includes("True")){
                        scoreStr = cleanStr.substr(0, cleanStr.indexOf(' ')).replace('+','+ ').replace('-','- ');
                        scoreNum = parseFloat(scoreStr.replace('+ ','').replace('- ','-'));
                        desc = "Add Req to Red Zone";//cleanStr.substr(cleanStr.indexOf(' ') + 1).replace('Because','').replace('If You Change','Change');
                        color = this.getColor(scoreNum,params.ranges);
                }
                else
                    continue;
                   
            }
            
                else {
                    scoreStr = cleanStr.substr(0, cleanStr.indexOf(' ')).replace('+','+ ').replace('-','- ');
                    scoreNum = parseFloat(scoreStr.replace('+ ','').replace('- ','-'));
                    desc = cleanStr.substr(cleanStr.indexOf(' ') + 1).replace('Because','').replace('If You Change','Change');
                    color = this.getColor(scoreNum,params.ranges);
                }
            console.log('score: ' + scoreStr);
            
            /*if(score.startsWith('-')){
                color = 'ac-sdd-text-color--green';
            }*/
            
            if(scoreStr.startsWith('N')){
                outputHTML += '<div class="slds-truncate slds-text-body--regular slds-m-vertical--xx-small slds-text-color--weak">' + scoreStr + (params.space ? ' ' : '') + params.unit + ' ' + desc + '</div>'
            } else{
                outputHTML += '<div class="slds-item--label ac-sdd-left-col slds-truncate slds-text-body--regular slds-m-vertical--xx-small ' 
                              + color + '">' + scoreStr + (params.space ? ' ' : '') + params.unit + '</div>';
                outputHTML += '<div class="slds-item--detail ac-sdd-right-col slds-truncate slds-text-body--regular slds-m-vertical--xx-small slds-text-color--weak">' + desc + '</div>'
            }
        }
        return outputHTML;
    }
 })