({
	subscribe : function(component, event, helper) {
        /*
       var empApi = component.find("empApi");        
        // Error handler function that prints the error to the console.
        var errorHandler = function (message) {
            //console.log("Received error ", message);
        }.bind(this);
        
        // Register error listener and pass in the error handler function.
        empApi.onError(errorHandler);
        
        var channel='/event/RWS_Posting__e';
        var sub;
        
        // new events
        var replayId=-1;
        
        var callback = function (message) {
            
            //console.log('Response from RWS web service --->'+message.data.payload.RWSResponse__c);
            component.set("v.userId",message.data.payload.UserId__c);
            
            var cUserId=$A.get('$SObjectType.CurrentUser.Id');
            
            //console.log('UserId from Event --->'+message.data.payload.UserId__c);
            
            // console.log('cUserId  --->'+cUserId);
            
            //console.log('Compare UserId-->'+(cUserId==message.data.payload.UserId__c));
            
            var tmessages=message.data.payload.RWSResponse__c.split('<br/>');
            //console.log('Returned Messages after Spilt are--->'+tmessages);
            var currURL=window.location.href;
      		var includeFlag=currURL.includes('/lightning/o/Job_Posting__c/list?filterName=00B1w000000Ma39EAC');
			
			var subchannelFlag=false;            
            var subchannel=component.get("v.sub");
            //console.log('Inside update Location  subchannel--->'+subchannel);
            if(subchannel!=null && subchannel!='' && typeof subchannel!='undefined'){
                subchannelFlag=true;
              //  console.log('inside updatelocation subchannel id----->'+component.get("v.sub")["id"]);
            }
            
            var x;
           // console.log('Before If Condition----cUserId------>'+cUserId);
           // console.log('Before If Condition----message.data.payload.UserId__c------>'+message.data.payload.UserId__c);
            //console.log('Before If Condition----includeFlag------>'+includeFlag);
            if(cUserId==message.data.payload.UserId__c && subchannelFlag){
           // console.log('Helper IF condition subchannelFlag------>'+subchannelFlag);
           // console.log('Inside if condition to show toast');
             for(x in tmessages){
              // console.log('Toast Message ---tmessages[x]---->'+tmessages[x]);
              // console.log('Toast Message -tmessages[x]!=null------>'+tmessages[x]!=null);
               //console.log('Toast Message -typeof tmessages[x] !=undefined------>'+(typeof tmessages[x] !='undefined'));
               if(tmessages[x]!='' && tmessages[x]!=null && typeof tmessages[x] !='undefined'){
                    component.find('notifLib').showToast({
                    "title": message.data.payload.SUBCODE__c,
                    "message": tmessages[x],
                     "mode": 'dismissible',
                     "variant":message.data.payload.SUBCODE__c 
                    });   
                   
                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : message.data.payload.SUBCODE__c,
                        message: tmessages[x],
                        messageTemplate: '',
                        duration:'5000',
                        key: 'info_alt',
                        type:message.data.payload.SUBCODE__c ,
                        mode: 'dismissible'
                    });
        			toastEvent.fire();
                   
                   
                   alert('Debugging toast isssue-->'+tmessages[x]);
                    
                }
              }
           }
        }.bind(this);

        empApi.subscribe(channel, replayId, callback).then(function(value) {
            //console.log("Subscribed to channel " + channel);
            sub = value;
            component.set("v.sub", sub);
        });
        
        
		 */
	},
    unsubscribe : function(component, event, helper) {
        /*
               var subchannel=component.get("v.sub");
        
        if(subchannel!=null && subchannel!='' &&typeof subchannel!='undefined'){
        
          // Get the empApi component.
                var empApi = component.find("empApi");
                // Get the channel from the input box.
                var channel ='/event/RWS_Posting__e';

                // Callback function to be passed in the subscribe call.
                var callback = function (message) {
                     //console.log("Unsubscribed from channel " + channel);
                }.bind(this);

                // Error handler function that prints the error to the console.
                var errorHandler = function (message) {
                    //console.log("Received error ", message);
                }.bind(this);

               // Object that contains subscription attributes used to
               // unsubscribe.
                //console.log('inside unsubscribe----->'+component.get("v.sub")["id"]);
                var sub = {"id": component.get("v.sub")["id"],
                           "channel":channel };

                // Register error listener and pass in the error handler function.
                empApi.onError(errorHandler);

                // Unsubscribe from the channel using the sub object.
                empApi.unsubscribe(sub, callback);
        }
        */
    }
})