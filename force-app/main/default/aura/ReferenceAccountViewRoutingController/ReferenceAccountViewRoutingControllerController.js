({
	doInit : function(component, event, helper) {
        
		 var action = component.get("c.getContactId");
        action.setParams({ idValue : component.get("v.recordId") });
        debugger;
        action.setCallback(this, function(response) {
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                var result =  response.getReturnValue();
                //alert("From server: " + result);
                if(result !== null){
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": result
                    });
                    navEvt.fire();  
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!!",
                        "message": "Could not find the appropriate contact for the Account.",
                        "type":"error",
                        "mode":"pester"
                    });
                    toastEvent.fire();  
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
	}
})