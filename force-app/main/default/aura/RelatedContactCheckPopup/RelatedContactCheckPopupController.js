({
	doInit : function(component, event, helper) {
        // For Story S-75078 Start
        if(window.location.href.split("ClientFlow=")[1] !== undefined){
           component.set("v.theMassage", "Would you like to add this contact as a candidate?"); 
            component.set("v.PopupHeaderMessage","Add as Candidate");
        }    
        //  For Story S-75078 End
        
        // Get a reference to the getContactReference() function defined in the Apex controller
		
        var action = component.get("c.getContactReference");
        action.setParams({
            "contactId": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var retVal = data.getReturnValue();
                if(retVal == null)
                {
                    component.set("v.clientcontact",data.getReturnValue());
                     component.set("v.hasReferenceContact", false);
                    
                }else
                {
                   component.set("v.clientcontact",data.getReturnValue());
                   component.set("v.hasReferenceContact", true);
                } 
            }  
        });
        // Invoke the service
           $A.enqueueAction(action);
    },
    
    //Dup Check Redirect
    getDupCheckUrl : function(component, event, helper) {
        var action = component.get("c.getDuplicateCheckUrl");
        action.setParams({
            "contactId": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var modalBody;
            var data = response;
			// Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                var dupCheckurl = data.getReturnValue();
                if(dupCheckurl != 'none')
                {
                   var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                                              "url": dupCheckurl,
                                              "isredirect" :true
                                          });
                     urlEvent.fire();  
                }      
            } 
        });
        // Invoke the service
        $A.enqueueAction(action);
    },

	openRelateTalentQuickAction: function(component, event, helper) {
		component.set("v.yesClicked", false);
		//component.set("v.yesClicked", true);
        	var attribs = {
			"aura:id": "relateTalentQuickAction",
			"recordId": component.get("v.recordId")
		};
        $A.createComponent("c:C_RelateToTalentQuickAction", attribs,
        function(content, status) {
                if (status === "SUCCESS") {
                    component.find('overlayLib').showCustomModal({
                    header: "Relate to Contact",
                    body: content, 
                    showCloseButton: true,
                    closeCallback: function() {
                        //console.log('closed the relate talent modal');
						component.set("v.yesClicked", false);
                        helper.hidePopup(component);
					}
                })
            } 
        }); 
	},
    
    //Close Modal Function
    closeAndUpdateFlag : function(component, event, helper){
        helper.closeModal(component);
  
    }
    
})