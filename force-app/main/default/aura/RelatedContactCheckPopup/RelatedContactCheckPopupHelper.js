({
	closeModal : function(component) {
        // Get a reference to the savePopupFlag() function defined in the Apex controller
		var action = component.get("c.savePopupFlag");
       // For Story S-75078 Start
        if (window.location.href.split("ClientFlow=")[1] !== undefined) {
            action.setParams({
				"contactId": null
            });
        } else {
            action.setParams({
                "contactId": component.get("v.recordId")
            });
        }
       //console.log("yyy"+window.location.href.split("ClientFlow=")[1]);
        // Story S-75078 End
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() === 'SUCCESS') {
                this.hidePopup(component);
            }  
        });

		// Invoke the service
        $A.enqueueAction(action);

	},
    
    hidePopup : function(component){
        $A.util.addClass(component.find("MainModel"), "slds-modal slds-fade-in-hide");
        $A.util.removeClass(component.find("MainModel"), "slds-modal slds-fade-in-open");        
        $A.util.addClass(component.find("modalBackdrop"),  "slds-backdrop slds-backdrop--hide"); 
        $A.util.removeClass(component.find("modalBackdrop"),  "slds-backdrop slds-backdrop--open");
    }

})