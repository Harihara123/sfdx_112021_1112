({
   RelatedAccountActivitiesURLClick : function(component, event, helper) {
        console.log('preetham start');
          var splanId = component.get("v.recordId");
          console.log('plan id'+splanId);
         var action = component.get("c.getStrategicReportUrl");
        action.setParams({
            "spId": component.get("v.recordId"),"ReportLabel": event.getSource().get("v.label")
        });
          
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                console.log('success one');
                var urllink = data.getReturnValue();
                console.log('final one'+urllink);
                if(urllink != 'none')
                {
                  var urlEvent = $A.get("e.force:navigateToURL");
                    window.open(urllink, '_blank');
                }       
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    
     RelatedAccountReqs : function(component, event, helper) {
        console.log('preetham start');
          var splanId = component.get("v.recordId");
          console.log('plan id'+splanId);
         var action = component.get("c.getRelatedAccountsWithReqsUrl");
        action.setParams({
            "spId": component.get("v.recordId"),"ReportLabel": event.getSource().get("v.label")
        });
          
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                console.log('success one');
                var urllink = data.getReturnValue();
                console.log('final one'+urllink);
                if(urllink != 'none')
                {
                  var urlEvent = $A.get("e.force:navigateToURL");
                    window.open(urllink, '_blank');
                }       
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    TeamMemberActivitiesURLClick : function(component, event, helper) {
        console.log('start');
          var splanId = component.get("v.recordId");
          console.log('plan id'+splanId);
         var action = component.get("c.getTeamMembersActivityUrl");
        action.setParams({
            "spId": component.get("v.recordId"),"ReportLabel": event.getSource().get("v.label")
        });
          
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                console.log('success reply');
                var urllink = data.getReturnValue();
                console.log('url result'+urllink);
                if(urllink != 'none')
                {
                  var urlEvent = $A.get("e.force:navigateToURL");
                    window.open(urllink, '_blank');
                }       
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    TeamMemberActivitiesAtAccountsURLClick : function(component, event, helper) {
        console.log('start');
          var splanId = component.get("v.recordId");
          console.log('plan id'+splanId);
         var action = component.get("c.getTeamMembersActivityAtAccountsUrl");
        action.setParams({
            "spId": component.get("v.recordId"),"ReportLabel": event.getSource().get("v.label")
        });
          
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                console.log('success reply');
                var urllink = data.getReturnValue();
                console.log('url result'+urllink);
                if(urllink != 'none')
                {
                  var urlEvent = $A.get("e.force:navigateToURL");
                    window.open(urllink, '_blank');
                }       
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    
   RelatedAccountOppReportURLClick : function(component, event, helper) {
        console.log('start');
          var splanId = component.get("v.recordId");
          console.log('plan id'+splanId);
         var action = component.get("c.getRelatedAccountOppReportUrl");
        action.setParams({
            "spId": component.get("v.recordId"),"ReportLabel": event.getSource().get("v.label")
        });
          
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                console.log('success one');
                var urllink = data.getReturnValue();
                console.log('final one'+urllink);
                if(urllink != 'none')
                {
                  var urlEvent = $A.get("e.force:navigateToURL");
                    window.open(urllink, '_blank');
                }       
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },    
      OppOwnedbyTeamMembersURLClick : function(component, event, helper) {
        console.log('start');
          var splanId = component.get("v.recordId");
          console.log('plan id'+splanId);
         var action = component.get("c.OppsOwnedURL");
        action.setParams({
            "spId": component.get("v.recordId"),"ReportLabel": event.getSource().get("v.label")
        });
          
        // Register the callback function
        action.setCallback(this, function(response) {
            var data = response;
            // Set the component attributes using values returned by the API call
            if (data.getState() == 'SUCCESS') {
                console.log('success one');
                var urllink = data.getReturnValue();
                console.log('final one'+urllink);
                if(urllink != 'none')
                {
                  var urlEvent = $A.get("e.force:navigateToURL");
                    window.open(urllink, '_blank');
                }       
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },     
     toggleSection : function(component, event, helper) {
		var isCollapsed = component.get("v.isCollapsed");
         if(isCollapsed === true){
            component.set("v.isCollapsed", false);
         }else{
             component.set("v.isCollapsed", true);
         }
         
	}

})