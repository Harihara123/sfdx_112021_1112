({
	afterScriptsLoaded : function(component, event, helper){
        helper.doInit(component,event,helper);
    },
    viewTalent : function(component, event, helper){
        helper.viewTalentDetails(component, event, helper);
    },
    handleReqChartEvent: function(component, event, helper){
       var opptyId  = event.getParam("oppId");
       var recrdId = component.get("v.recordId");
        console.log('event id'+opptyId);
        if(opptyId === recrdId){
            helper.doInit(component,event,helper);
        }
    }
})