({
	doInit : function(component, event, helper){
        var recId = component.get("v.recordId");
        var action = component.get("c.getchartJSON");
        action.setParams({"oppId":  recId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                var cnt = 0;
                var isStarted = false;
                console.log('===='+dataObj);
                var res = []; 
                var submitalList = [];
                res = JSON.parse(dataObj);
                console.log('json dt'+res.length);
                 for (var i = 0; i < res.length; i++){
                    var subArry = [];
                     if(res[i].status === 'Started' && isStarted === false){
                         isStarted = true;
                         component.set("v.displayedStarts","showstart");
                         component.set("v.starts",res[0].stOrds);
                         console.log('statrs'+res[0].stOrds);
                     }
                    subArry.push(res[i].status ,res[i].count);
                     cnt = cnt + res[i].count;
                     submitalList.push(subArry);
                }
                component.set("v.submittalCount",cnt);
                component.set("v.chartList",submitalList);
                component.set("v.data",dataObj);
                if(cnt > 0){
                    if(res[0].opco === 'AG_EMEA'){
                       component.set("v.isEmea",true)
                    }
                    component.set("v.hassubmittalCount",true);
                    component.set("v.displayedSection","chart");
                    helper.donutchart(component,event,helper);
                }
            }
        });
        $A.enqueueAction(action);
    },
    donutchart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        var dataObj = JSON.parse(jsonData);
        var finalList = component.get("v.chartList");
        console.log('JSON'+dataObj);
        var subCount = component.get("v.submittalCount");
        new Highcharts.Chart({
            chart: {
                renderTo: component.find("chart").getElement(),
                type: 'pie',
                marginTop: 2
            },
            plotOptions: {
                pie: {
                    //innerSize: 100,
                    //depth: 45,
                borderWidth: 6,
                startAngle: 90,
                innerSize: '55%',
                size: '100%',
                shadow: true,
                dataLabels: false,
                stickyTracking: false,
                states: {
                    hover: {
                        enabled: false
                    }
                },
                point: {
                events: {
                    mouseOver: function(){
					//S-113153 Oppty Donut Text Alignment Issues done by Revathi
                    	document.getElementsByClassName('submittalInfoPercentage')[0].innerHTML = Math.ceil(this.options.y/subCount * 100)+'%';
                        document.getElementsByClassName('submittalCount')[0].innerHTML = this.options.y;
                        document.getElementsByClassName('submittalStatus')[0].innerHTML = this.options.name;
                    }, 
                    mouseOut: function(){
					//S-113153 Oppty Donut Text Alignment Issues done by Revathi                    
						document.getElementsByClassName('submittalInfoPercentage')[0].innerHTML = Math.ceil(this.options.y/subCount * 100)+'%';
                        document.getElementsByClassName('submittalCount')[0].innerHTML = this.options.y;
                        document.getElementsByClassName('submittalStatus')[0].innerHTML = this.options.name;
                    }
                  }
                 }
                }
            },
             title: {
                 text: '<span style="font-size: 14px">'+'Count :' + subCount +'</span>',
                align: 'center',
                y: 360 //  this to move y-coordinate of title to desired location
            },
            credits: {
              enabled: false
            },
            exporting: {
                   enabled: false
           },
            series: [{
                name: 'Count',
                type: 'pie',
                data:finalList,
                size: '80%',
                innerSize: '65%',
                showInLegend:false,
                dataLabels: {
                    enabled: false
                }
            }]
 
        },
        function(chart) { // on complete
            var xpos = '50%';
            var ypos = '53%';
            var circleradius = 102;
            // S-113153 Oppty Donut Text Alignment Issues done by Revathi
            var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
			var textY = chart.plotTop  + (chart.plotHeight * 0.5);
			var chartInfoContainer = document.createElement('DIV');
			chartInfoContainer.id="chartInfoContainer";
            chartInfoContainer.innerHTML ="div{position: absolute; top: 0px; left: 0px}";
			var chartTextContainer = document.createElement('SPAN');
			chartTextContainer.id="chartTextContainer";
			chartTextContainer.style.position = 'absolute';
			chartTextContainer.style.font = '16px "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif';
			chartTextContainer.style.color = '#4572A7';
			var lineBreak = document.createElement('br');
			var chartElement = document.getElementsByClassName('highcharts-container')[0];
			chartElement.appendChild(chartInfoContainer);   
			chartInfoContainer.appendChild(chartTextContainer);
			var node = document.createElement("SPAN");
            node.className = 'submittalInfoPercentage';
			var textnode = document.createTextNode(Math.ceil(chart.series[0].data[0].y/subCount * 100)+'%');
			node.appendChild(textnode);
            node.style.font ='32px "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif';
			chartTextContainer.appendChild(node);
			chartTextContainer.appendChild(lineBreak);
			node = document.createElement("SPAN");
            node.className = 'submittalCount';
			textnode = document.createTextNode(chart.series[0].data[0].y);
			lineBreak = document.createElement('br');
			node.appendChild(textnode);
			chartTextContainer.appendChild(node);
			chartTextContainer.appendChild(lineBreak);
			node = document.createElement("SPAN");
            node.className = 'submittalStatus';
			textnode = document.createTextNode(chart.series[0].data[0].name);
			node.appendChild(textnode);
			chartTextContainer.appendChild(node);
			chartTextContainer.style.left = (textX + (chartTextContainer.offsetWidth * -0.5)) + 'px';
			chartTextContainer.style.top = (textY + (chartTextContainer.offsetHeight * -0.5)) + 'px';
      });
    },
    viewTalentDetails : function(component,event,helper){
       var oppId = component.get("v.recordId");
        var submUrl = "/one/one.app#/alohaRedirect/apex/Req_Submittal_Detail?Id="+oppId;
           var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": '/one/one.app#/alohaRedirect/apex/Req_Submittal_Detail?Id='+oppId
            });
            urlEvent.fire();
    }
})