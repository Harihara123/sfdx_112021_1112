({
    showTalentStatusTags: function (component, event, helper) {
          helper.initializeEndDateModal(component);
    },


    displayHideDonotRecruitPopup : function(component, event, helper) {
        var toggleText = component.find("tooltip");
        $A.util.toggleClass(toggleText, "content-toggle"); 
        
    },
    
    displayHidePeopleSoftPopup : function(component, event, helper) {
        
        var toggleText = component.find("PeopleSofttooltip");
        $A.util.toggleClass(toggleText, "content-toggle"); 
        
    },

    refreshDNR : function(component, event, helper) {
        helper.initializeEndDateModal(component);
    },
    handleOwnershipPopover: function (cmp, event) {
        let toggle = {
            'slds-hide': 'slds-show',
            'slds-show': 'slds-hide'
        }
        cmp.set('v.ownershipPopoverState', toggle[cmp.get('v.ownershipPopoverState')]);
        event.stopPropagation();
    }

})