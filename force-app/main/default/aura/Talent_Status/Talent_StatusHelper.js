({
    initializeEndDateModal : function(component) {

    //console.log("farid : initializeEndDateModal");

        var recordId = component.get("v.recordId"); 
        var talentId = component.get("v.talentId");
		
		let thsEvent = component.getEvent("thsEvent"); 


        var currentEmployer = component.get("v.currentEmployer"); 
        var companyName = component.get("v.runingUserCompanyName"); 
        var isMLA = false; 
        if (companyName ===  "Major, Lindsey & Africa, LLC" || companyName === "Allegis Partners, LLC") {
          isMLA = true; 
        }

        var action = component.get("c.getTalentHeaderStatusModel");
        action.setParams({ "recordId" : recordId, "accountId" : talentId, "clientAccountId" : currentEmployer, "isMLA" : isMLA});

        action.setCallback(this, function(response) {


            var state = response.getState();

            if (state === "SUCCESS") {
				thsEvent.setParams({'candidateStatus': {response: response.getReturnValue(), showTalentStatus: true}});
				thsEvent.fire(); 

                 component.set("v.doNotRecruitFlag", response.getReturnValue().doNotRecruit);
                 component.set("v.engagmentAddress", response.getReturnValue().engagementAddress);
                 component.set("v.doNotRecruitERList", response.getReturnValue().engagementRuleList);
                 component.set("v.talentOwnershipFlag", response.getReturnValue().talentOwnership);
                 component.set("v.talentOwnwershipText", response.getReturnValue().talentOwnershipDetailsText);
                 if (response.getReturnValue().talentOwnershipDetails && response.getReturnValue().talentOwnershipDetails.Expiration_Date__c) {
                     component.set("v.talentOwnershipDetailsExpiration_Date__c", response.getReturnValue().talentOwnershipDetails.Expiration_Date__c);
                 }

                component.set("v.latestFinishCode",         response.getReturnValue().latestFinishCode);
                component.set("v.latestFinishReason",       response.getReturnValue().latestFinishReason);


                component.set("v.showTalentStatus",true);  
               
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
        
    }

})