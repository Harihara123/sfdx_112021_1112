({
    doInit : function(component) {
        var vfOrigin = "https://" + component.get("v.vfHost");
                //console.log('init is being called');
        window.addEventListener("message", function(event) {
            if ( (event.data.type) && (event.data.type=='EventFromVF') ) {
                if (event.origin !== vfOrigin) {
                    // Not the expected origin: Reject the message!
                   // console.log('coming here in other cases');
                    return;
                } else {
                   // console.log('is the origin a vf page?');
                    // Handle the message
                    var toastEvent = $A.get('e.force:showToast');
                    toastEvent.setParams({
                        type: event.data.toasttype,
                        message: event.data.message,                
                        mode: 'dismissible',
                    });
                    toastEvent.fire();

                    //console.log(event.data);

                }
            }
        }, false);
    }

})