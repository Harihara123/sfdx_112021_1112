({
	fetchUserInfo : function(component, event) {
		let action = component.get('c.getTrackingUserInfo');
        let compEvent = component.getEvent("OpcoEvent");
        action.setCallback(this, function(response) {
            let results = response.getReturnValue();
            compEvent.setParam('opco', results.userOwnership.usr.OPCO__c);
			compEvent.setParam('userId', results.userOwnership.usr.Id);

			let groups = [];
            if (results.userGroups['isInGroupA']) {
                groups.push('A');
            }
            if (results.userGroups['isInGroupB']) {
                groups.push('B');
            }
            if (results.userGroups['isInGroupC']) {
                groups.push('C');
            }

			compEvent.setParam('userGroups', groups);
			compEvent.fire();
        });
        $A.enqueueAction(action);
	}
})