public with sharing class ATSBaseController {
    public static Object performServerCall(String subClassName, String methodName, Map<String, Object> parameters){
        Object result = null;

        // Need to handle exceptions  S-109119
        try{
            //Call the relevant performServerCall method in the specified subClass.
            if(subClassName == 'TalentActivityFunctions'){
                result = ATSTalentActivityFunctions.performServerCall(methodName,parameters);
            }else if (subClassName == 'TalentHeaderFunctions'){
                result = ATSTalentHeaderFunctions.performServerCall(methodName,parameters);
            }else if(subClassName == 'TalentEmploymentFunctions'){
                result = ATSTalentEmploymentFunctions.performServerCall(methodName,parameters);
            }else if(subClassName == 'TalentEducationFunctions'){
                result = ATSTalentEducationFunctions.performServerCall(methodName,parameters);
            }else if(subClassName == 'TalentCertificationFunctions'){
                result = ATSTalentCertificationFunctions.performServerCall(methodName,parameters);
            }else if(subClassName == 'TalentDocumentFunctions'){
                result = ATSTalentDocumentFunctions.performServerCall(methodName,parameters);
            }else if(subClassName == 'TalentEngagementFunctions'){
                result = ATSTalentEngagementFunctions.performServerCall(methodName,parameters);
            }else if(subClassName == 'TalentMarketingEmailFunctions'){
                result = ATSTalentMarketingEmailFunctions.performServerCall(methodName,parameters);
            //Added by KK on 3/19/2018 for S-70726  
            }else if(subClassName == 'TalentOrderFunctions'){
                result = ATSTalentOrderFunctions.performServerCall(methodName,parameters);
            //Added by KK on 3/19/2018 for S-70726  
            }else if(subClassName == 'ContactListFunctions'){
                result =  ATSContactListFunctions.performServerCall(methodName,parameters);
            //end of code additions - KK
            }else{
                //If no subclass provided, call the method within this class
                if(methodName == 'getTalentContactId'){
                    result = getTalentContactId((String)parameters.get('recordId'));
                }else if(methodName == 'saveTalentExperience'){
                    result = saveTalentExperience((String)parameters.get('talentExperience'));
                //S-35422 Added by Karthik
                }else if(methodName == 'saveTalentPreferences'){
                    result = saveTalentPreferences((parameters.get('talent')),((String)parameters.get('G2Comments')));            
                }//S-66531 Rajeesh - to get contact fields
                else if(methodName == 'getTalentBaseFields'){
                    result = getTalentBaseFields((String)(parameters.get('conId')));
                }
                else if(methodName == 'saveTalentBaseFields'){
					result = saveTalentBaseFields(parameters.get('contact'));
                }
				else if (methodName == 'addAndRelateTalent') {
					result = DupeSearchController.addAndRelateTalent((String) parameters.get('clientContactId'));
				}
				else if (methodName == 'getSuggestedSkillsByTitleOpco') {
					result = getSuggestedSkillsByTitleOpco(parameters);
				}
                /*else if(methodName == 'saveTalentPrefModal'){
                    result = saveTalentPrefModal((parameters.get('talent')),(parameters.get('cont')),((String)parameters.get('G2Comments')));            
                }*/
            }
            //Integer a = 10/0; // Testing component error. - DO NOT COMMIT

        }catch(Exception ex){
            
            String errorDesc = ex.getMessage();
            result = 'ATSApexException';
            System.debug('ATSBaseController PerformServer Call Exception '+errorDesc);
            Core_Data.logRecord(0,0,'ATSBaseController Exception',errorDesc);
        }
        return result;
    }
    
    private static Contact getTalentBaseFields(String conId){
        Contact con=[Select FirstName, LastName,Other_Email__c, Email,Salutation, LinkedIn_URL__c,Website_URL__c, Title,MiddleName,HomePhone  , Phone , MobilePhone,MailingCountry, MailingStreet, 
					 MailingCity,MailingState,MailingPostalCode,Talent_State_Text__c , Talent_Country_Text__c , OtherPhone, Work_Email__c,Preferred_Email__c , Preferred_Phone__c , Preferred_Name__c,
					 PrefCentre_Aerotek_OptOut_Mobile__c, PrefCentre_Aerotek_OptOut_Email__c from contact where id =: conId ];
        return con;
    }
    private static Id saveTalentBaseFields(Object con){
        //system.debug('con====>'+con);
        Contact c = (Contact)JSON.deserialize(String.valueOf(JSON.serialize(con)),Contact.class);
        upsert c;
        return c.Id;
    } 
    private static string getTalentContactId(string recordId){
        String contactId;
        if (recordId != null && recordId != '') {
            recordId = String.escapeSingleQuotes(recordId);
            Contact contact = [SELECT Id from Contact where AccountId =: recordId Limit 1];
            contactId = contact.Id;
        }
        
        return contactId;
    }

    private static Id saveTalentExperience(String talentExperience) {
        Talent_Experience__c t = (Talent_Experience__c)JSON.deserialize(talentExperience, Talent_Experience__c.class);
        upsert t;
        return t.Id;
    }
    
    //S-35422 Added by Karthik
    //S-47724 Added by Akshay...
    private static Id saveTalentPreferences(Object talent, String G2Comments) {
        Account t = (Account)JSON.deserialize(String.valueOf(JSON.serialize(talent)), Account.class);
        //Removing Varriage return from String
        if(String.isNotBlank(t.Skill_Comments__c)) {
            t.Skill_Comments__c = t.Skill_Comments__c.replace('\r', '');
        }
        //Removing Varriage return from String
        if(String.isNotBlank(t.Goals_and_Interests__c)) {
            t.Goals_and_Interests__c = t.Goals_and_Interests__c.replace('\r', '');
        }
        //Removing Varriage return from String
        if(String.isNotBlank(t.Talent_Overview__c)) {
            t.Talent_Overview__c = t.Talent_Overview__c.replace('\r', '');
        }
        
        String contactId = getTalentContactId(t.Id);
        String activitySubject = ''; 
        String userName = Userinfo.getFirstName() + ' ' + UserInfo.getLastName();
        t.Talent_Summary_Last_modified_by__c = UserInfo.getUserId();
        t.Talent_Profile_Last_Modified_By__c = UserInfo.getUserId();
        t.Talent_Summary_Last_Modified_date__c = System.now();
        t.Talent_Profile_Last_Modified_Date__c = System.now();
        
        //Removing Varriage return from String
        if(String.isNotBlank(G2Comments)) {
            G2Comments = G2Comments.replace('\r', '');
        }
        if(t.G2_Completed__c == true){
            activitySubject = 'Candidate Summary/G2 Completed by ' + userName;
            t.G2_Completed_By__c = UserInfo.getUserId();
            t.G2_Completed_Date__c = Date.Today();
            
            Task tsk = new Task();
            tsk.OwnerID = Userinfo.getUserId();
            tsk.Subject = activitySubject;
            tsk.Type = 'G2';
            tsk.Priority = 'Normal';
            tsk.WhatId = t.Id;
            tsk.WhoId = contactId;
            tsk.ActivityDate = Date.Today();
            tsk.Status = 'Completed';
            tsk.Description = G2Comments;

            insert tsk;
        }
        else if(t.G2_Completed__c == false && G2Comments != null){
            activitySubject = 'Candidate Summary/G2 Edited by ' + userName;

            Task tsk = new Task();
            tsk.OwnerID = Userinfo.getUserId();
            tsk.Subject = activitySubject;
            tsk.Type = 'G2';
            tsk.Priority = 'Normal';
            tsk.WhatId = t.Id;
            tsk.WhoId = contactId;
            tsk.ActivityDate = Date.Today();
            tsk.Status = 'Completed';
            tsk.Description = G2Comments;

            insert tsk;
        }


        update t;
        return t.Id;
    }

    //S-57729 - Added by Karthik -- Save is in TalentAddEditController
    /*private static Id saveTalentPrefModal(Object talent, Object cont, String G2Comments) {
        Account t = (Account)JSON.deserialize(String.valueOf(JSON.serialize(talent)), Account.class);
        System.debug(t);

        Contact c = (Contact)JSON.deserialize(String.valueOf(JSON.serialize(cont)), Contact.class);
        System.debug(c);


        String contactId = getTalentContactId(t.Id);
        String activitySubject = ''; 
        String userName = Userinfo.getFirstName() + ' ' + UserInfo.getLastName();
        t.Talent_Summary_Last_modified_by__c = UserInfo.getUserId();
        t.Talent_Profile_Last_Modified_By__c = UserInfo.getUserId();
        t.Talent_Summary_Last_Modified_date__c = System.now();
        t.Talent_Profile_Last_Modified_Date__c = System.now();
       
        if(t.G2_Completed__c == true){
            activitySubject = 'Candidate Summary/G2 Completed by ' + userName;
            t.G2_Completed_By__c = UserInfo.getUserId();
            t.G2_Completed_Date__c = Date.Today();
            
            Task tsk = new Task();
            tsk.OwnerID = Userinfo.getUserId();
            tsk.Subject = activitySubject;
            tsk.Type = 'G2';
            tsk.Priority = 'Normal';
            tsk.WhatId = t.Id;
            tsk.WhoId = contactId;
            tsk.ActivityDate = Date.Today();
            tsk.Status = 'Completed';
            tsk.Description = G2Comments;

            insert tsk;
        }
        else if(t.G2_Completed__c == false && G2Comments != null){
            activitySubject = 'Candidate Summary/G2 Edited by ' + userName;

            Task tsk = new Task();
            tsk.OwnerID = Userinfo.getUserId();
            tsk.Subject = activitySubject;
            tsk.Type = 'G2';
            tsk.Priority = 'Normal';
            tsk.WhatId = t.Id;
            tsk.WhoId = contactId;
            tsk.ActivityDate = Date.Today();
            tsk.Status = 'Completed';
            tsk.Description = G2Comments;

            insert tsk;
        }

        t.Name = c.FirstName + ' ' + c.LastName; 
        t.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        t.Talent_Committed_Flag__c = true;
        t.Talent_Visibility__c ='Private';
        try {
            upsert t;
        } catch (DmlException ex) {
            System.debug('Failed account validation --- ' + ex.getMessage());
            throw ex;
        }
        
        c.AccountId = t.Id;
        c.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        try {
            upsert c;
        } catch (DmlException ex) {
            System.debug('Failed contact validation --- ' + ex.getMessage());
            throw ex;
        }

        return t.Id;
    }*/

    private static string getSuggestedSkillsByTitleOpco(Map<String, Object> parameters) {
        string opco = (string)parameters.get('domain');

        if (String.isBlank(opco)) {
            String uId = UserInfo.getUserId();
            User u = [SELECT Id, OPCO__c, CompanyName FROM User WHERE Id = :uId LIMIT 1];
            
            opco = u.CompanyName == 'AG_EMEA' ? u.CompanyName : u.OPCO__c;
        }

        if (opco == 'TEK')
            return '{}';

        string title = (string)parameters.get('title');
        string terms = (string)parameters.get('terms');
        string max = (string)parameters.get('max');
        string domain = opco;

        // Get Url from Custom Settings
        ApigeeOAuthSettings__c connSettings = ApigeeOAuthSettings__c.getValues('SkillSearch');
        String url = connSettings.Service_URL__c;

        // Build URL
        PageReference pageRef = new PageReference(url);
        pageRef.getParameters().put('max', max);
        pageRef.getParameters().put('terms', terms);
        pageRef.getParameters().put('domain', domain);

        // Build request
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + connSettings.OAuth_Token__c);
        req.setHeader('Content-Length', '0');
        req.setMethod('GET');
        req.setEndpoint(pageRef.getUrl());
        
        // Send request
        String responseBody;
        try {
            HttpResponse response = connection.send(req);
            responseBody = response.getBody();
        } catch (Exception ex) {
            throw ex;
        }
        
        return responseBody;
    }
}