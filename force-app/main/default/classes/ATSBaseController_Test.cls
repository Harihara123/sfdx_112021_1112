@isTest
private class ATSBaseController_Test {
    @isTest static void test_performServerCall() {
        //No Params Entered....
        Object r = ATSBaseController.performServerCall('','',null);
        System.assertEquals(r,null);

        List<String> subClassNames = new List<String>();
        subClassNames.add('TalentActivityFunctions');
        subClassNames.add('TalentHeaderFunctions');
        subClassNames.add('TalentEmploymentFunctions');
        subClassNames.add('TalentEducationFunctions');
        subClassNames.add('TalentCertificationFunctions');
        subClassNames.add('TalentDocumentFunctions');
        subClassNames.add('TalentEngagementFunctions');
        subClassNames.add('TalentMarketingEmailFunctions');
        //Added by KK on 3/19/2018 for S-70726        
        subClassNames.add('ContactListFunctions');

        for(String sc : subClassNames){
            Object rSC = ATSBaseController.performServerCall(sc,'',null);
            System.assertEquals(rSC,null);
        }
        
    }

    @isTest static void test_method_getTalentContactId(){
        Account newAcc = BaseController_Test.createTalentAccount('');
        Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
                                                                   
        Test.startTest();
        Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newAcc.Id);
        String t1 = (String)ATSBaseController.performServerCall('','getTalentContactId',p);
        Test.stopTest();
        System.assertEquals(newC.Id, t1);
    }

    @isTest static void test_method_saveTalentExperience(){
        Account newAcc = BaseController_Test.createTalentAccount('');

        Test.startTest();
        Talent_Experience__c te = new Talent_Experience__c(Type__c='Work', Title__c='Test Title', Talent__c=newAcc.Id);

        Map<String,Object> p = new Map<String,Object>();
        p.put('talentExperience', JSON.serialize(te));
        Id t1 = (Id)ATSBaseController.performServerCall('','saveTalentExperience',p);
        Test.stopTest();

        System.assertNotEquals(null, t1);
    }
    
     @isTest static void test_method_saveTalentPreferences(){
        Account newAcc = BaseController_Test.createTalentAccount('');
        Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
                                                                   
        Test.startTest();
        Map<String,Object> p = new Map<String,Object>();
        //p.put('recordId', newAcc.Id); changed parameters by akshay for Nov 2017 release
        //p.put('JSON', '');
        p.put('talent', newAcc); 
        p.put('G2Comments','');
        Id t1 = (Id)ATSBaseController.performServerCall('','saveTalentPreferences',p);
        newAcc.G2_Completed__c = true;//added by akshay for Nov 2017 release
        Id t2 = (Id)ATSBaseController.performServerCall('','saveTalentPreferences',p);//added by akshay for Nov 2017 release
        Test.stopTest();
        System.assertNotEquals(null, t1);
    }
    
    //Added by Karthik for SummaryModal
    /*@isTest static void test_method_saveTalentPrefModal(){
        Account newAcc = BaseController_Test.createTalentAccount('');
        Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
                                                                   
        Test.startTest();
        Map<String,Object> p = new Map<String,Object>();
        //p.put('recordId', newAcc.Id); changed parameters by akshay for Nov 2017 release
        //p.put('JSON', '');
        p.put('talent', newAcc);
        p.put('cont', newC);
        p.put('G2Comments','');
        Id t1 = (Id)ATSBaseController.performServerCall('','saveTalentPrefModal',p);
        newAcc.G2_Completed__c = true;
        Id t2 = (Id)ATSBaseController.performServerCall('','saveTalentPrefModal',p);
        Test.stopTest();
        System.assertNotEquals(null, t1);
    }*/
}