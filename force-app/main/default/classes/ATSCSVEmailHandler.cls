public class ATSCSVEmailHandler {
    public static void exportRecords(){

        List<String> csvRecords = new List<String>();
        for(Contact cont : [Select Id, Name from Contact limit 2]){
            String csvRow = cont.id + ',' + cont.Name;
            csvRecords.add(csvRow);
        }
        String csvData = 'Id,Name\n';
        csvData = csvData + String.join(csvRecords, '\n');

        try{
            
            String fileName = 'UTMTestData.csv';
            /*
            ContentVersion file = new ContentVersion(
            title = fileName,
            versionData = Blob.valueOf(csvData),
            pathOnClient = '/'+fileName
            );        
            insert file;
            System.debug(' file ' + file);*/
            

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();    
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(fileName);
            efa.setBody(Blob.valueOf(csvData));
            email.setSubject('UTM Test Data');
            email.setToAddresses( new String[] {'psammeta@allegisgroup.com'} );
            email.setPlainTextBody( 'UTM Test Data.' );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            Messaging.SendEmailResult [] r =  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            System.debug('r ' + r);
        }catch(Exception ex){
            System.debug(ex.getStackTraceString());
        }
    }
    
    public static void sendEmail(Blob data, String[] emails){
        try{
            
            String fileName = 'UTMTestData.csv';
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();    
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(fileName);
            efa.setBody(data);
            email.setSubject('UTM Test Data');
            //email.setToAddresses( new String[] {'psammeta@allegisgroup.com'} );
            email.setToAddresses(emails);
            email.setPlainTextBody( 'UTM Test Data.' );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            Messaging.SendEmailResult [] r =  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            System.debug('r ' + r);
        }catch(Exception ex){
            System.debug('sendEmail Ex ' +ex.getStackTraceString());
        }
    }
}