global class ATSCandidate  {
     
    
    global String vendorId { get; set; } //VendorId
    global String sourceId { get; set; }
    global String sfId { get; set; } // Salesforce Id. It could be a contact or an accoutn id. 
	global String MDMID {get;set;}
	global String jbSourceId {get;set;} //Job Board Name
    global String userId {get;set;} // S-200573 - added for labs to set createdBy
    global String leadUpdatedDate {get;set;}//S-235378
    global ATSNameDetails  nameDetails{ get; set; }
    global ATSEmailDetails emailDetails { get; set; }
    global ATSPhoneDetails phoneDetails { get; set; }
    global ATSAddressDetails mailingAddress { get; set; }
    global String jobTitle { get; set; }
    global String securityClearance { get; set; }
    global String workEligibility { get; set; }
    global String highestEducation { get; set; }
    global String geoPrefComments { get; set; }
    global String desiredRate { get; set; }
    global String desiredSalary { get; set; }
    global String talentUpdate {get; set;}
    global String textConsent {get;set;}
    global String emailConsent {get;set;}
    global String transactionsource {get;set;}
	//Added w.r.t D-12143 EMEA(Parse resume via email)
	global String currentEmployer {get;set;}
    global String talentPrefInternal {get;set;}
	global String talentOverview {get;set;}   

    global String linkedInURL { get; set; }
    //S-169348 - EMEA Applications - Mapping Source ID to Account object.
    global String talentSource{ get; set; }
	
	// for story S-215279 passing recruiter information to append in task subject
	global String recruiterFirstName { get; set; }
	global String recruiterLastName { get; set; }
	global String recruiterEmail { get; set; }
    
	global class ATSNameDetails{   
        global String firstName { get; set; }
        global String lastName { get; set; }
        global String suffix { get; set; }
    }

    global class ATSPhoneDetails{   
        global String phone { get; set; }
        global String mobile { get; set; }
        global String workPhone { get; set; }
		global String otherPhone { get; set; }

		//Adding below fields for S-139433
		global String Home_Phone_Invalid { get; set; }
		global String Mobile_Phone_Invalid { get; set; }
		global String Work_Phone_Invalid { get; set; }
		global String Other_Phone_Invalid { get; set; }
    }

    global class ATSEmailDetails{   
        global String email { get; set; }
        global String workEmail { get; set; }
        global String otherEmail { get; set; }

		//Adding below fields for S-139433
		global String Email_Invalid { get; set; }
		global String Work_Email_Invalid { get; set; }
		global String Other_Email_Invalid { get; set; }
    }

    global class ATSAddressDetails{   
        global String street { get; set; }
        global String city { get; set; }
        global String state { get; set; }
        global String country { get; set; }
        global String postalCode { get; set; }
    }
}