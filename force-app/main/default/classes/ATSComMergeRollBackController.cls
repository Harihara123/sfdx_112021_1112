@RestResource(urlMapping='/talent/atscommergerollback/*')
global class ATSComMergeRollBackController {

   private static Integer numDays  = Integer.valueOf(TalentMerge__c.getInstance('Param').NumDays__c);

   private static final String TOBE_MERGED = 'TO_BE_MERGED';
   private final static String PostStr = 'Post'; 


    @HttpPost
    global static String doPost(Id masterAccountId, Id duplicateAccountId,Id masterContactId,Id duplicateContactId,Id mergeMappingId) {
        Account duplicateAccount = new Account(Id = duplicateAccountId);
        Contact duplicateContact = new Contact(Id = duplicateContactId);
        
       Account masterAccount = [select Merge_State__c, Merge_Id__c, Source_System_Id__c, talent_Ownership__c, TC_Talent_Profile_Last_Modified_By__c, TC_Talent_Profile_Last_Modified_Date__c, Talent_Profile_Last_Modified_By__c, Talent_Profile_Last_Modified_Date__c from Account where Id =:masterAccountId ]; 

       Contact masterContact =[select  About_Me__c, Email , FirstName , LastName , LinkedIn_URL__c, MailingCity , MailingCountry , MailingLatitude , MailingLongitude , MailingPostalCode , MailingState , MailingStreet  , MiddleName , Phone , Suffix , Title, TC_About_Me__c ,TC_Email__c ,TC_FirstName__c ,TC_LastName__c ,TC_LinkedIn_URL__c ,TC_MailingCity__c ,TC_MailingCountry__c ,TC_MailingLatitude__c ,TC_MailingLongitude__c ,TC_MailingPostalCode__c ,TC_MailingState__c ,TC_MailingStreet__c ,TC_Mailing_Address__c ,TC_Middle_Name__c  ,TC_Phone__c ,TC_Suffix__c ,TC_Title__c from Contact where Id=:masterContactId ];

        TriggerStopper.isMergeEvent = true;//setting this static variable so it bypasses most trigger activities.
        

       Talent_Merge_Mapping__c tmm = [select Id, Fetched__c, Result_Reason__c, Result__c, Account_Merge_Status__c, Contact_Merge_Status__c, Source_System_Id__c, Communities_Talent_Ownership__c from Talent_Merge_Mapping__c where id =: mergeMappingId]; 
        
        savePoint sp1  = Database.setSavepoint();
       
        try{
            Database.UndeleteResult ud1 = Database.undelete(duplicateAccountId);
            if(ud1.isSuccess()){
                Database.UndeleteResult ud2 = Database.undelete(duplicateContactId);
                if(ud2.isSuccess()){
                    
                    tmm.Result__c = 'ROLLBACK';
                    tmm.Result_Reason__c = null; 
                    tmm.Account_Merge_Status__c =  TOBE_MERGED;
                    tmm.Contact_Merge_Status__c =  TOBE_MERGED;
                    tmm.Fetched__c  = false; 

                    masterAccount.Merge_State__c = null;
                    masterAccount.Merge_Id__c = null;
                    masterAccount.Source_System_Id__c   = tmm.Source_System_Id__c; 
                    masterAccount.talent_Ownership__c = tmm.Communities_Talent_Ownership__c; 
                    masterAccount.Talent_Profile_Last_Modified_By__c = masterAccount.TC_Talent_Profile_Last_Modified_By__c; 
                    masterAccount.Talent_Profile_Last_Modified_Date__c = masterAccount.TC_Talent_Profile_Last_Modified_Date__c; 

                    Update masterAccount;

                    masterContact.Source_System_Id__c   = tmm.Source_System_Id__c; 
                    masterContact.Merge_State__c = null;


                    masterContact.About_Me__c =   masterContact.TC_About_Me__c; 
                   
                    masterContact.FirstName =   masterContact.TC_FirstName__c; 
                    masterContact.LastName =   masterContact.TC_LastName__c;  
                    masterContact.LinkedIn_URL__c =   masterContact.TC_LinkedIn_URL__c;  
                    masterContact.MailingCity =   masterContact.TC_MailingCity__c;  
                    masterContact.MailingCountry =   masterContact.TC_MailingCountry__c;  
                    masterContact.MailingLatitude =   masterContact.TC_MailingLatitude__c;  
                    masterContact.MailingLongitude =   masterContact.TC_MailingLongitude__c;  
                    masterContact.MailingPostalCode =   masterContact.TC_MailingPostalCode__c; 
                    masterContact.MailingState =   masterContact.TC_MailingState__c;  
                    masterContact.MailingStreet =   masterContact.TC_MailingStreet__c;  
                    masterContact.MiddleName =   masterContact.TC_Middle_Name__c;  
                    masterContact.Suffix =   masterContact.TC_Suffix__c;  
                    masterContact.Title =   masterContact.TC_Title__c;  
                
                    update masterContact; 

                    duplicateContact.AccountId = duplicateAccountId;
                    duplicateContact.Merge_State__c = null; 
                    duplicateAccount.Merge_State__c = null; 

                    update duplicateAccount; 
                    Update duplicateContact; 


                    List<Talent_Document__c> tds = Database.query('SELECT Id FROM Talent_Document__c where LastModifiedDate = LAST_N_DAYS:' + numDays +   ' AND  Document_Type__c !=  \'communities\'  AND Talent__c=:masterAccountId');
                    for(Talent_Document__c rec : tds){
                        rec.Talent__c = duplicateAccountId;
                    } 
                    if(!tds.isEmpty())
                        update tds;
                    
                    List<Talent_Experience__c> tex =  Database.query('SELECT Id FROM Talent_Experience__c where LastModifiedDate = LAST_N_DAYS:' + numDays +   ' AND PersonaIndicator__c = \'Recruiter\'  AND Talent__c=:masterAccountId');
                    for(Talent_Experience__c rec : tex){
                        rec.Talent__c = duplicateAccountId;
                    }
                    if(!tex.isEmpty())
                        update tex;

                     List<Talent_work_history__c> twh =  Database.query('SELECT Id FROM Talent_work_history__c where LastModifiedDate = LAST_N_DAYS:' + numDays +   '   AND Talent__c=:masterAccountId AND SourceId__c like \'R.%\'');
                    for(Talent_work_history__c rec : twh){
                        rec.Talent__c = duplicateAccountId;
                    }
                    if(!twh.isEmpty())
                        update twh;
                    
                    //contact related objects
                    
                    List<order> subs =  Database.query('SELECT Id,  Opportunity.Name, Opportunity.Id  FROM order where LastModifiedDate = LAST_N_DAYS:' + numDays +   '   AND shiptocontactid=:masterContactId');
                    for(order rec : subs){
                        rec.shiptocontactid = duplicateContactId;
                        rec.Unique_Id__c = String.valueOf(duplicateContactId) + String.valueOf(rec.Opportunity.Id);
                    }
                    if(!subs.isEmpty())
                        update subs;
                    
                    List<event> evt =  Database.query('SELECT Id FROM event where LastModifiedDate = LAST_N_DAYS:' + numDays +   '   AND whoid=:masterContactId ALL ROWS ');
                    for(event rec : evt){
                        rec.whoid = duplicateContactId;
                    }
                    if(!evt.isEmpty())
                        update evt;
                    
                    List<task> tasks =  Database.query('SELECT Id FROM task where LastModifiedDate = LAST_N_DAYS:' + numDays +   '   AND whoid=:masterContactId ALL ROWS ');
                    for(task rec : tasks){
                        rec.whoid = duplicateContactId;
                    }
                    if(!tasks.isEmpty())
                        update tasks;
                    
                    List<Contact_Tag__c> lists =  Database.query('SELECT Id FROM Contact_Tag__c where LastModifiedDate = LAST_N_DAYS:' + numDays +   '   AND Contact__c=:masterContactId');
                    for(Contact_Tag__c rec : lists){
                        rec.Contact__c = duplicateContactId;
                    }
                    if(!lists.isEmpty())
                        update lists;

                    List<Assessment__c> listsA =  Database.query('SELECT Id FROM Assessment__c where LastModifiedDate = LAST_N_DAYS:' + numDays +   '   AND Talent__c=:masterContactId');
                    for(Assessment__c rec : listsA){
                        rec.Talent__c = duplicateContactId;
                    }
                    if(!listsA.isEmpty())
                        update listsA;


                    List<Talent_Recommendation__c> listsR =  Database.query('SELECT Id FROM Talent_Recommendation__c where LastModifiedDate = LAST_N_DAYS:' + numDays +   '   AND Talent_Contact__c=:masterContactId');
                    for(Talent_Recommendation__c rec : listsR){
                        rec.Talent_Contact__c = duplicateContactId;
                    }
                    if(!listsR.isEmpty())
                        update listsR;



                   List<AccountTeamMember> listAT =  Database.query('  SELECT Id FROM AccountTeamMember Where LastModifiedDate = LAST_N_DAYS:' + numDays +   ' AND AccountId=:masterAccountId');
                    for(AccountTeamMember rec : listAT){
                        rec.AccountId = duplicateAccountId;
                    }
                    if(!listAT.isEmpty())
                        update listAT;

                     

                    //end restore
                }else{
                    
                    tmm.Result__c = 'FAIL-ROLLBACK-CONTACT';
                    tmm.Result_Reason__c = String.valueOf(ud2.errors[0]);
                     Database.rollback(sp1);
                }
            }else{
               
                tmm.Result__c = 'FAIL-ROLLBACK-ACCOUNT';
                tmm.Result_Reason__c = String.valueOf(ud1.errors[0]);
                 Database.rollback(sp1);
            } 
        update tmm;
        }catch(Exception e){
            Database.rollback(sp1);
            tmm.Result__c='FAIL-ROLLBACK - EXCEPTION';
            tmm.Result_Reason__c = e.getMessage();
            tmm.Account_Merge_Status__c =  PostStr;
            tmm.Contact_Merge_Status__c =  PostStr;
            update tmm;
            
        }
        return String.valueOf(duplicateContactId)+'-'+tmm.Result__c;
    }
}