/**
* @author Kaavya Karanam 
* @date 03/19/2018
*
* @group ATS - CRM
* @group-content ATSContactListFunctions.html
*
* @description Apex controller to get or save 'My Contact Lists' information
*/
public with sharing class ATSContactListFunctions  {

    private static String requestId = '';
    private static String transactionId = '';
	
    public static Object performServerCall(String methodName, Map<String, Object> parameters){
        Object result = null;
        Map<String, Object> p = parameters;

        //Call the method within this class
        if(methodName == 'getContactLists'){
            result = getContactLists((String)p.get('recordId'));
        }else if(methodName == 'saveContactLists'){
            requestId = (String)p.get('requestId');
            transactionId = (String)p.get('transactionId');		
            result = saveContactLists((String)p.get('recordId'),
                                        (Map<Object, Object>)p.get('clist'));
        }      
        else if(methodName == 'getAllContactLists') {
            result = getAllContactLists((List<String>)p.get('recordIds'));
        }
        else if(methodName == 'getTagNamesCount') { 
            result = getTagNamesCount(); 
        }  			 		
        else if(methodName == 'getContactRecords') {
            result = getContactRecords((String)p.get('contactIds')); 
        }
        else if(methodName == 'getTagDefinitions') { 
            result = getTagDefinitions(); 
        }                  
		else if(methodName == 'saveAllContactLists') {
            requestId = (String)p.get('requestId');
            transactionId = (String)p.get('transactionId');	
            result = saveAllContactLists((String)p.get('contactRecords'),(Map<Object, Object>)p.get('clist'),(Map<Object, Object>)p.get('cTagList'));
        }   
		else if(methodName=='addToPipeline'){
			//requestId = (String)p.get('requestId');
            //transactionId = (String)p.get('transactionId');	
			//System.JSON.deserializeStrict(jsonString,
			//System.debug('conIds before serializing'+(String)p.get('conIds'));
			List<String> conIds = (List<String>)System.JSON.deserialize((String)p.get('conIds'), List<String>.Class);	
			System.debug('conIds'+conIds);
            result = addToPipeline(conIds,(String)p.get('userId'));
		}
		else if(methodName=='removeFromPipeline'){
			//requestId = (String)p.get('requestId');
            //transactionId = (String)p.get('transactionId');		
			
            result = removeFromPipeline((String)p.get('conId'),(String)p.get('userId'));
		}
        return result;
    }

    private static List<Contact_Tag__c> getContactLists(String contactId) {
        List<Contact_Tag__c> mcList = new List<Contact_Tag__c>();
        if(contactId != null){
            mcList = [SELECT id,Contact__c,Contact__r.AccountId, Tag_Definition__r.Tag_Name__c,Tag_Definition__r.Id,Tag_Definition__r.Candidate_Count__c FROM Contact_Tag__c WHERE (Contact__c =: contactId OR Contact__r.AccountId =: contactId) AND CreatedbyId =:UserInfo.getUserID() Order by CreatedDate ];
        }
		//add the tag_definition count using the wrapper class
        return mcList;
    }

    private static List<Contact_Tag__c> getAllContactLists(List<String> recordIds) {
        List<Contact_Tag__c> contactTags = new List<Contact_Tag__c>();
        if(!recordIds.isEmpty()) {
            contactTags = [SELECT Id
                                , Contact__c
                                , Contact__r.AccountId
                                
								, Tag_Definition__r.Tag_Name__c
                                , Tag_Definition__c
                                , Tag_Definition__r.Candidate_Count__c
                            FROM Contact_Tag__c
                            WHERE (Contact__c IN: recordIds OR Contact__r.AccountId IN: recordIds) 
                                AND CreatedbyId =:UserInfo.getUserId() 
                            ORDER BY CreatedDate];
        }
        return contactTags;
    }
    

	private static List<Tag_Definition__c> saveAllContactLists(String contactList,Map<Object, Object> cMap, Map<Object, Object> cTagMap) {
		List<Tag_Definition__c> TagList = new List<Tag_Definition__c>();
        List<Tag_Definition__c> TagListNew = new List<Tag_Definition__c>();
		Map<String,Id> tagListMap = new Map<String,Id>();
        List<Contact_Tag__c> ConTagList = new List<Contact_Tag__c>();
		for(Object cl : cMap.keySet()){
            Tag_Definition__c Td = new Tag_Definition__c();
            String cMapVal = String.valueOf(cMap.get(cl));
            if(String.valueOf(cl).equalsIgnoreCase(cMapVal)){
                Td.Tag_Name__c = cMapVal;
                TaglistNew.add(Td);
            }
        }      
        insert TagListNew;
		for(Tag_Definition__c tg: TagListNew) {
			tagListMap.put(tg.Tag_Name__c, tg.Id);
		}

		for(Object cl: cTagMap.keySet()) {
				Tag_Definition__c Td = new Tag_Definition__c();
				String cMapVal = String.valueOf(cTagMap.get(cl));
				if(String.valueOf(cl).equalsIgnoreCase(cMapVal)){					
					Td.id = tagListMap.get(cMapVal); 
				} else {		
					Td.id = Id.valueOf(String.valueOf(cl));
				}
                Td.Tag_Name__c = cMapVal;
                TagList.add(Td);	
		}	 
		List<String> selectedIdsList = (List<String>)System.JSON.deserialize(contactList, List<String>.class);
		Map<Id,List<Id>> existingContactTagMap = new Map<Id,List<Id>>();
		for(Contact_Tag__c ctag:getAllContactLists(selectedIdsList)) {
			if(!existingContactTagMap.containsKey(ctag.Contact__c)) {
				existingContactTagMap.put(ctag.Contact__c,new List<Id>());	
			}
			existingContactTagMap.get(ctag.Contact__c).add(ctag.Tag_Definition__c);
		}
		System.debug('existing contact tag map'+existingContactTagMap);
		for(String c: selectedIdsList) {
			for(Tag_Definition__c Td : TagList) {
				Id contactId = Id.valueOf(c);				
				if(existingContactTagMap.isEmpty() || 
				(!existingContactTagMap.isEmpty() &&
				((existingContactTagMap.get(contactId)==null) || 
				((existingContactTagMap.get(contactId)!=null) && 
				!existingContactTagMap.get(contactId).contains(Td.Id))))) {
					Contact_Tag__c Ctag = new Contact_Tag__c();
					Ctag.Contact__c = contactId;
					Ctag.Tag_Definition__c  = Td.Id;
					Ctag.RequestID__c  = requestId;
					Ctag.TransactionID__c  = transactionId;
					ConTagList.add(CTag);				
				}			 
			}
        }
        insert ConTagList;
		return TagList;
	}

    private static List<Tag_Definition__c> saveContactLists(String contactId,Map<Object, Object> cMap) {
        Set<Contact> contactSet = new Set<Contact>([SELECT Id FROM Contact WHERE (Id =: contactId OR AccountId =: contactId)]);
		return createContactLists(contactSet,cMap);		
	}

	private static List<Tag_Definition__c> createContactLists(Set<Contact> contactsSet, Map<Object,Object> cMap) {
        List<Tag_Definition__c> TagList = new List<Tag_Definition__c>();
        List<Tag_Definition__c> TagListNew = new List<Tag_Definition__c>();
        List<Contact_Tag__c> ConTagList = new List<Contact_Tag__c>();
        
        for(Object cl : cMap.keySet()){
            Tag_Definition__c Td = new Tag_Definition__c();
            String cMapVal = String.valueOf(cMap.get(cl));
            if(String.valueOf(cl).equalsIgnoreCase(cMapVal)){
                Td.Tag_Name__c = cMapVal;
                TaglistNew.add(Td);
            }else{
                Td.id = Id.valueOf(String.valueOf(cl));
                Td.Tag_Name__c = cMapVal;
                TagList.add(Td);
            }
           }
        
        insert TagListNew;
        TagList.addAll(TagListNew);		       
        for(Tag_Definition__c Td : TagList) {
			for(Contact c: contactsSet) {
				Contact_Tag__c Ctag = new Contact_Tag__c();
				Ctag.Contact__c = c.Id;
				Ctag.Tag_Definition__c  = Td.Id;
                Ctag.RequestID__c  = requestId;
                Ctag.TransactionID__c  = transactionId;
				ConTagList.add(CTag);			
			}
        }
        insert ConTagList;
        return TagList;	
	}
	
    private static List<Contact> getContactRecords(String contactIdsString) {
		List<String> selectedIdsList = getContactIdsFromJSONString('[', ']',contactIdsString,', ');
        return [select Name, FirstName, LastName, AccountId, RecordTypeId, RecordType.Name from Contact where (Id In:selectedIdsList OR AccountId In:selectedIdsList) and RecordType.Name IN ('Talent','Client')];
    }

	private static List<String> getTagNamesCount() {
        List<String> tagNameList = new List<String>();
		for(Tag_Definition__c tagName : [select Name, Tag_Name__c from Tag_Definition__c where OwnerId =:UserInfo.getUserID() Order by CreatedDate]){
			tagNameList.add(tagName.Tag_Name__c);
		}
		return tagNameList;
    }  
    private static List<Tag_Definition__c> getTagDefinitions() {
        return [select Id, Name, Tag_Name__c,Candidate_Count__c,Legacy_List__c, OwnerId from Tag_Definition__c where OwnerId =:UserInfo.getUserID() Order by Tag_Name__c];
    }

	private static List<String> getContactIdsFromJSONString(String subStringStart,String subStringEnd,String contactIdsString,String splitParam) {
		String contactRecords = (String)JSON.deserialize(contactIdsString, String.class);
		List<String> selectedIdsList = new List<String>();
		String selectedList = contactRecords;
		selectedList = selectedList.substringBetween(subStringStart,subStringEnd);
		selectedIdsList = selectedList.split(splitParam);	
		return selectedIdsList; 
	} 
	@TestVisible
	Private Static String addToPipeline(List<String> conIds,String userId){
	/* S-95929 - Ability to add a talent to a pipeline on Talent Detail- Rajeesh
		1. Pipeline is the junction object between user and contact (talent).
		2. BAU - takes one con id. But bulkify code for other scenarios.
		3. if userid is blank, take running user's id.
		4. if pipeline doesn't exist for user, create one.

	*/
		String retMsg = '';
		if(conIds.size()==0) return 'Error:Nothing to add!'; //Nothing to add.

		//If user info is blank take the id from current user.
		if(String.isBlank(userId)){
			userId = UserInfo.getUserId();
		}
		List<Pipeline__c> plToInsert = new List<Pipeline__c>();
		Integer noOfExpDays = Integer.valueOf(Label.Pipeline_Expiration_Days);
		for(String cid: conIds){
			Pipeline__c pl = new Pipeline__c(
				Contact__c=cid,
				User__c=userId,
				Expiry_Date__c = System.today().addDays(noOfExpDays)
				);
			plToInsert.add(pl);
		}
		if(plToInsert.size()>0){
			try{
				insert plToInsert;
				retMsg = 'Success:The Talent was successfully added to the Pipeline';
			}
			catch(DmlException dmlEx){
				System.debug('unable to insert pipeline.:'+dmlEx.getMessage());
				retMsg = 'Exception:The Talent was not added to the Pipeline, please contact the support desk';
			}
		}
		return retMsg;
	}
	@TestVisible
	Private Static Boolean removeFromPipeline(String conId,String userId){
		//Allow removal of only one record at a time.
		if(String.isBlank(userId)){
			userId = UserInfo.getUserId();
		}
		try{
			List<Pipeline__c> removeTalent = [Select id from Pipeline__c where contact__c=:conId and user__c=:userId];
			if(removeTalent.size()>0){
				delete removeTalent;
			}
		}
		catch(System.DmlException dmlEx){
			//couldnt delete.
			return false;
		}
		return true;
	}
}