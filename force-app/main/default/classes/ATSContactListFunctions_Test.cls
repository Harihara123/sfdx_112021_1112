/**
* @author Kaavya Karanam 
* @date 03/19/2018
*
* @group ATS - CRM
* @group-content ATSContactListFunctions.html
*
* @description Test class for ATSContactListFunctions.cls
*/
@isTest 
private class ATSContactListFunctions_Test {

    @isTest static void test_performServerCall() {
        //No Params Entered
        Object r = ATSContactListFunctions.performServerCall('',null);
        System.assertEquals(r,null);
    }

    @isTest static void test_method_getContactLists(){
        Account newAcc = BaseController_Test.createClientAccount();
        Contact newCon = BaseController_Test.createClientContact(newAcc.Id);

        Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newCon.Id);

        List<Contact_Tag__c> r = (List<Contact_Tag__c>)ATSContactListFunctions.performServerCall('getContactLists',p);
    
    }
    
    @isTest static void test_method_getAllContactLists() {
     /*   Account talentAccount = CreateTalentTestData.createTalent();
        //Contact talentContact = CreateTalentTestData.createTalentContact(talentAccount);
        Contact talentContact = CreateTalentTestData.getTalentContact(talentAccount);
	*/
	List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account talentAccount = new Account(Name='Test Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(talentAccount);
		
		Contact talentContact = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=talentAccount.Id, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(talentContact);


        Tag_Definition__c myList = new Tag_Definition__c(Tag_Name__c = 'myList');
        insert myList;
        Contact_Tag__c newConTag = new Contact_Tag__c(Contact__c = talentContact.Id, Tag_Definition__c = myList.Id);
        insert newConTag;
        
        Test.startTest();
        Map<string, Object> parameters = new Map<String, Object>();
        parameters.put('recordIds', new List<String>{talentAccount.Id});        
        List<Contact_Tag__c> results = (List<Contact_Tag__c>)ATSContactListFunctions.performServerCall('getAllContactLists', parameters);
        Test.stopTest();
        System.assertEquals(1, results.size(), 'should only find one');
    }

    @isTest static void test_method_saveContactLists(){
        Account newAcc = BaseController_Test.createClientAccount();
        Contact newCon = BaseController_Test.createClientContact(newAcc.Id);
        
        Tag_Definition__c TagDef = new Tag_Definition__c (Tag_Name__c = 'TagTest');
        insert TagDef;

        Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newCon.Id);
        Map<Object, Object> cMap = new Map<Object, Object> ();
        cMap.put('test','test');
        cMap.put(TagDef.Id,TagDef.Name);
        p.put('clist', cMap);
        p.put('requestId', '4fbf793d-e03e-07cb-95f5-b4955dede2cd');
        p.put('transactionId', '27b54f43-fed3-9c88-90fd-aa391615fe03');

        List<Tag_Definition__c> r = (List<Tag_Definition__c>)ATSContactListFunctions.performServerCall('saveContactLists',p);
    }

    @isTest static void test_method_saveAllContactLists(){
        Account newAcc = BaseController_Test.createClientAccount();
        Contact newCon = BaseController_Test.createClientContact(newAcc.Id);
        
        Tag_Definition__c TagDef = new Tag_Definition__c (Tag_Name__c = 'TagTest');
        insert TagDef;

        Map<String,Object> p = new Map<String,Object>();
        p.put('contactRecords', '["'+newCon.Id+'"]');
        Map<Object, Object> cMap = new Map<Object, Object> ();
        cMap.put('test','test');
        p.put('clist', cMap);
        p.put('requestId', '4fbf793d-e03e-07cb-95f5-b4955dede2cd');
        p.put('transactionId', '27b54f43-fed3-9c88-90fd-aa391615fe03');
        Map<Object, Object> cTagMap = new Map<Object, Object> ();
        cTagMap.put(TagDef.Id,TagDef.Name);
		p.put('cTagList',cTagMap);
        List<Tag_Definition__c> r = (List<Tag_Definition__c>)ATSContactListFunctions.performServerCall('saveAllContactLists',p);
    }

	@isTest static void test_method_getContactRecords(){
        Account newAcc = BaseController_Test.createClientAccount();
        Contact newCon = BaseController_Test.createClientContact(newAcc.Id);
        
        Map<String,Object> p = new Map<String,Object>();
		
        p.put('contactIds','"['+newCon.Id+']"');
        
        List<Contact> r = (List<Contact>)ATSContactListFunctions.performServerCall('getContactRecords',p);
    }

	@isTest static void test_method_getTagDefinitions(){
        Tag_Definition__c myList = new Tag_Definition__c(Tag_Name__c = 'myList');
        insert myList;

		Test.startTest();
        List<Tag_Definition__c> tagDefs = (List<Tag_Definition__c>)ATSContactListFunctions.performServerCall('getTagDefinitions',null);
		System.assertEquals(1, tagDefs.size(), 'should only find one');
		Test.stopTest();
    }

	@isTest static void test_getTagNamesCount() {
        Account newAcc = BaseController_Test.createClientAccount();
        Contact newCon = BaseController_Test.createClientContact(newAcc.Id);

        Tag_Definition__c myList = new Tag_Definition__c(Tag_Name__c = 'myList');
        insert myList;

		Test.startTest();
		Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newCon.Id);
        List<String> count = (List<String>)ATSContactListFunctions.performServerCall('getTagNamesCount',p);
		System.assertEquals(1, count.size(), 'should only find one');
		Test.stopTest();
    }
	 @isTest static void test_method_PipelineActions() {
		
		List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(newAcc);
		
		Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=newAcc.Id, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(newC);



		 Profile p = [select id from profile where name='System Administrator']; 
         long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
         Integer random = Math.Round(Math.Random() * timeDiff );
		User u = new User(alias = 'standt', email='user@allegisgroup.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', username='xyz'+random+'@allegisgroup.com');
			System.debug('contact====>'+newC);
		insert u;
			System.debug('user====>'+u);
        Test.startTest();
		ATSContactListFunctions.addToPipeline(new List<String>{(String)newC.Id},(String)u.Id);
		ATSContactListFunctions.removeFromPipeline((String)newC.Id,(String)u.Id);
        Test.stopTest();
    }

}