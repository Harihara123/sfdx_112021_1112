global class ATSEmeaApplicationSchedular implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<Application_Staging__c > staginglist = [SELECT id,Status__c from Application_Staging__c where  Status__c = 'New'];

     for(Application_Staging__c stg :staginglist ){

        ApplicationProcessController.processApplication(stg.id);
       
    }
    }
}