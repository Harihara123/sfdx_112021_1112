@RestResource(urlMapping = '/Person/Application/V1/*')
global without sharing class ATSOrderRESTService {
	public static final String ATS_EXCEPTION_JOBPOSTING_NOTAVAILABLE = 'This Job Posting is not available in Connected.';
	public static final String ATS_EXCEPTION_OPPORTUNITY_DELETED = 'The Opportunity for this Job Posting is deleted in Connected.';

	@HttpPost
	global Static Map<String, String> doPost(ATSSubmittal submittal) {

		Map<String, String> result = new Map<String, String> ();
		String status = 'Success';
		Savepoint savePoint = null;
		try {
			savePoint = Database.setSavepoint();
			OrderResponse orderResp = insertOrder(submittal);
			Order order = orderResp.order;
			// Generate an application event
			Event event = generateEvent(orderResp, submittal);
			result.put('SubmittalId', '' + orderResp.order.Id);
			result.put('appEventId', '' + event.Id);
			// Send email to recruiter S-86384
			//sendEmail(orderResp,submittal,event);
		} catch(Exception e) {
			if (savePoint != null) {
				Database.rollback(savePoint);
			}
			status = 'Fail';
			result.put('description', e.getMessage());
		}
		result.put('Status', status);

		return result;
	}

	private static OrderResponse insertOrder(ATSSubmittal submittal) {

		OrderResponse orderResponse = null;
		Order order = null;
		try {

			/***
			 **
			  1. Get the Job_Posting__c based on the postingId.
			  2. Get the Opportunity based on the Job_Posting__c.
			  3. Get other detials like Account etc from the Opportunity.
			 ***/


			Job_Posting__c jp = getJobPosting(submittal);


			// Check if there is any order already existing for this user for the jobposting id.
			order = findExistingOrder(submittal.talentId, jp.Opportunity__r.Id);
			System.debug('AccID ' + jp.Opportunity__r.AccountId);
			if (order == null) {
				order = new Order();
				order.AccountId = jp.Opportunity__r.AccountId;
				order.OpportunityId = jp.Opportunity__r.Id;
				order.ShipToContactId = submittal.talentId;
				order.RecordTypeId = Utility.getRecordTypeId('Order', 'OpportunitySubmission');
				order.Unique_Id__c = String.valueOf(submittal.talentId) + String.valueOf(jp.Opportunity__r.Id);
				order.Status = 'Applicant';
				if (String.isNotBlank(submittal.transactionsource)) {
					Datetime dt = null;
					if (submittal.appliedDate != null && submittal.appliedDate != '') {
						dt = (DateTime) JSON.deserialize('"' + submittal.appliedDate + '"', DateTime.class);
						order.EffectiveDate = dt != null ? dt.date() : Date.today();
					}
				} else {
					order.EffectiveDate = submittal.appliedDate != null ? Date.parse(submittal.appliedDate) : Date.today();
				}

			}

			order.Has_Application__c = true; //submittal.isApplicantSubmittal;

			// soruce system id will be populated by RWS.
			//order.Source_system_id__c = submittal.externalSourceId;
			/*commented by akshay 11/28/18 , please refer to kishan regarding discepancy of payload from soa
			  if(submittal.desiredSalary != null){
			  order.Salary__c = Decimal.valueOf(submittal.desiredSalary);
			  }
			  if(submittal.desiredRate != null){
			  order.Pay_Rate__c = Decimal.valueOf(submittal.desiredRate);
			  }*/


			upsert order;

			orderResponse = new OrderResponse();
			orderResponse.order = order;
			orderResponse.jobPosting = jp;

			//Update Talent last modified data S-107094
			updateTalentModifiedDetails(submittal);

		} catch(Exception e) {
			System.debug('insertOrder >> ' + e.getMessage());
			throw e;
		}
		return orderResponse;
	}

	/**
	 *** This method generates an application event.
	 **/
	private static Event generateEvent(OrderResponse orderResp, ATSSubmittal submittal) {

		Event event = null;
		Datetime dt = null;

		try {

			event = new Event();
			event.WhatId = orderResp.order.Id;
			event.WhoId = submittal.talentId;
			event.Application_Id__c = submittal.vendorApplicationId;
			event.Inviter_Full_Name__c = submittal.inviterFullName;
			event.JB_Source_ID__c = submittal.jbSourceId;
			event.Vendor_Application_ID__c = submittal.vendorApplicationId;
			event.Vendor_Segment_ID__c = submittal.vendorSegmentId;
			event.Job_Posting__c = orderResp.jobPosting.Id;
			event.DurationInMinutes = 1;
			event.StartDateTime = DateTime.now();
			event.Subject = orderResp.order.Status;
			event.Type = orderResp.order.Status;
			event.Activity_Type__c = orderResp.order.Status;
			event.Source__c = submittal.vendorName;
			event.Feed_Source_ID__c = submittal.vendorId;
			event.ECID__c = submittal.ecid;
			event.ICID__c = submittal.icid;
			//STORY S-113884 Changes Owner Id to Talent Owner from jobPosting OwnerId by Siva 1/23/2019
			//S-160033  the Owner ID has been hard coded.Change this to use the user ID invoking the API (Order Upsert)
			//event.OwnerId = '005240000038GjfAAE';//orderResp.jobPosting.OwnerId;
			if (String.isBlank(submittal.transactionsource))
			{
				event.OwnerId = Label.User_Talent_Owner;
			}
			event.Job_Posting_Owner_Email__c = orderResp.jobPosting.Owner.email; //STORY S-113884 Added Job posting Owner's Email to event Email by Siva 1/23/2019
			// Set default disposition status for OFCCP Job Postings S-106121
			if (orderResp.jobPosting.OFCCP_Flag__c) {
				event.Disposition_Code__c = getDefualtDispositionReasonCode();
			}
			//S-156724
			if (!String.isBlank(submittal.transactionsource) && submittal.transactionsource.equalsIgnoreCase('PP')) {

				if (!String.isBlank(submittal.inviterEmail)) {
					event.InviterEmail__c = submittal.inviterEmail;
				}
				if (!String.isBlank(submittal.ecvid)) {
					event.Ecvid__c = submittal.ecvid;
				}
				if (!String.isBlank(submittal.acceptedTnc)) {
					event.Accepted_Tnc__c = Boolean.ValueOf(submittal.acceptedTnc);
				}
				if (!String.isBlank(submittal.talentDocumentId)) {
					event.TalentDocumentId__c = submittal.talentDocumentId;
				}
				if (!String.isBlank(submittal.transactionId)) {
					event.ApplicationTransactionId__c = submittal.transactionId; //S-154867
				}
				if (!String.isBlank(submittal.integrationId)) {
					event.IntegrationId__c = submittal.integrationId; //S-154867
				}

			}
			
			if (String.isNotBlank(submittal.appliedDate)) {
				dt = (DateTime) JSON.deserialize('"' + submittal.appliedDate + '"', DateTime.class);
				event.Application_DateTime__c = dt;
			}

			insert event;
			/*S-155146*/
			Job_Posting__c jp = new Job_Posting__c();
			jp.Id = event.Job_Posting__c;
			jp.Applications_Count__c = [Select Id from Event where Job_Posting__c = :event.Job_Posting__c].Size();
			Update jp;
		}
		catch(Exception e) {
			System.debug('generateEvent >> ' + e.getMessage());
			throw e;
		}
		return event;
	}

	/**
	 *** Get the disposition picklist values API name.
	 **/
	private static String getDefualtDispositionReasonCode() {
		String dispReasonCode;

		Schema.DescribeFieldResult fields = Event.Disposition_Code__c.getDescribe();
		for (Schema.PicklistEntry pe : fields.getPicklistValues()) {
			if (pe.getLabel() == 'Applied - Not Reviewed') {
				dispReasonCode = pe.getValue();
				break;
			}
		}

		return dispReasonCode;
	}
	private static Boolean updateTalentModifiedDetails(ATSSubmittal submittal) {
		Boolean updateStatus = true;
		try {
			Contact talent = [Select Account.Talent_Profile_Last_Modified_By__c, Account.Talent_Profile_Last_Modified_Date__c from Contact where id = :submittal.talentId];
			talent.Account.Talent_Profile_Last_Modified_By__c = UserInfo.getUserId();
			talent.Account.Talent_Profile_Last_Modified_Date__c = Datetime.now();
			update talent;
		} catch(Exception ex) {
			System.debug('updateTalentModifiedDetails Exception ' + ex.getMessage());
		}
		return updateStatus;
	}
	/**
	 *** This method sends an email to the jobposting owner.
	 **/
	/*
	  private static void sendEmail(OrderResponse orderResp, ATSSubmittal submittal, Event evnt){
	 
	  try{
	  List<String> toList= new List<String>();
	  toList.add(orderResp.jobPosting.Owner.email);
	 
	  List<EmailTemplate> listTemplates=[Select Id, Subject, Body from EmailTemplate where name = 'Recruiters Notifications for Application'];  
	  System.debug('listTemplates '+listTemplates);
	  if(listTemplates.size()>0){
	 
	  Contact talent = [Select firstName, lastName from Contact where id =:submittal.talentId];
	  EmailTemplate et= listTemplates[0];
	  Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(et.Id, orderResp.jobPosting.Owner.Id, evnt.Id);
	  //new Messaging.SingleEmailMessage();
	  mail.setToAddresses(toList);
	 
	  mail.setSubject('Job Application from ' + talent.FirstName + ' ' + talent.LastName);
	  //mail.setHtmlBody(et.Body);
	 
	  mail.saveAsActivity = false;
	  Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { mail });
	  }          
	  }catch(Exception e){
	  System.debug('sendEmail error >> '+e.getMessage());
	  //throw e;
	  }
	  }
	 */
	/***
	 ** This method check if there is any existing order for given candiate id and opp id
	 ***/
	private static Order findExistingOrder(String candidateId, String oppId) {
		Order order = null;
		try {
			List<Order> orders = [Select Id, Status, Source_system_id__c,
			                      EffectiveDate, Has_Application__c
			                      from Order
			                      where ShipToContactId = :candidateId AND OpportunityId = :oppId];

			if (orders.size() > 0) {
				order = orders[0];
			}
		} catch(Exception e) {
			System.debug(' Exception in findExistingOrder ' + e.getMessage());
		}
		return order;
	}
	@TestVisible
	private static Job_Posting__c getJobPosting(ATSSubmittal submittal) {

		Job_Posting__c jp = null;
		List<Job_Posting__c> jpList = new List<Job_Posting__c> ();
		try {
			/*Neel :  03/18/21:: S-216764 : OrderUpsert changes - Handle Board Instance ID
			  Removed code related to BroadBean. As BB decommissioned. Previously JP was checking on the basis of Source System ID.
			  Now Job_Posting__c.Board_Posting__c will used to get JP either JP-Number or Board Instance Id data.
			  If Payload from Phenom then submittal.jobPostingId will have JP number
			  If Payload from EQuest then submittal.jobPostingId will have Board Instance Id.
			  eQuest payload is for EMEA.
			 */
			String jpNumber;
			if (submittal.jobPostingId.startsWithIgnoreCase('jp-')) {
				jpNumber = submittal.jobPostingId;
			}
			else {
				jpNumber = [select Id, Job_Posting_Id__r.Name from Job_Posting_Channel_Details__c where Board_Posting_Id__c = :submittal.jobPostingId LIMIT 1] ?.Job_Posting_Id__r.Name;
			}

			jpList = [SELECT Id, Name, OwnerId, Owner.email, Opportunity__c,
			          Opportunity__r.Id, Opportunity__r.AccountId, OFCCP_Flag__c
			          FROM Job_Posting__c
			          where Name = :jpNumber LIMIT 1];

			if (jpList.size() > 0) {
				jp = jpList.get(0);
				Id oppId = [Select Id from Opportunity where Id = :jp.Opportunity__c and IsDeleted = false] ?.Id;

				// S-109516 - Need to throw a custom exception if the JP opportunity is deleted.  
				if (oppId == null) {
					throw new AllegisException(ATS_EXCEPTION_OPPORTUNITY_DELETED);
				}
			}
			else {
				throw new AllegisException(ATS_EXCEPTION_JOBPOSTING_NOTAVAILABLE);
			}
		} catch(Exception exp) {
			ConnectedLog.LogException('Submittal/REST', 'ATSOrderRESTService', 'getJobPosting', exp);
			throw exp;
		}
		return jp;
	}

	class OrderResponse {
		Order order;
		Job_Posting__c jobPosting;
	}
}