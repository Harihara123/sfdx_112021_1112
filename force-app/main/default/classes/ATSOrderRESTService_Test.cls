@IsTest
public class ATSOrderRESTService_Test {

	@TestSetup
	public static void testDataSetup() {
		User user = CreateTalentTestData.createUser('System Integration', 'Aerotek, Inc.', 'Application');
		System.runAs(user) {
			JobPostingTestDataFactory.northAmericaTestDataSetup();
			JobPostingTestDataFactory.emeaChannelListTestData();
			JobPostingTestDataFactory.emeaTestDataSetup();
			Account talAcc = CreateTalentTestData.createTalent();
			Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
		}
	}

	static testMethod void insertSubmittal() {
		Test.startTest();
		User user = CreateTalentTestData.createUser('System Integration', 'Aerotek, Inc.', 'Application');
		System.runAs(user) {
			String sourceSystmId = '73066296';
			Contact talCont = [Select Id from Contact Limit 1];
			

			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();

			ATSSubmittal submittal = new ATSSubmittal();
			submittal.ecid = '';
			submittal.icid = '';
			submittal.inviterFullName = 'test name';
			submittal.vendorSegmentId = 'ved1111';
			submittal.vendorApplicationId = 'CS_Application_233';

			submittal.vendorId = 'vendor101';
			submittal.feedSourceId = 'feed11';
			submittal.jobPostingId = sourceSystmId;
			submittal.talentId = talCont.Id;
			submittal.externalSourceId = 'Talent_Submittal_111';
			submittal.isApplicantSubmittal = true;
			submittal.appliedDate = '06/05/2020';

			submittal.desiredSalary = '1222';
			submittal.desiredRate = '122';
			submittal.transactionId = '12345677';
			submittal.integrationId = '12345678';
			submittal.talentDocumentId = '';
			submittal.acceptedTnc = 'true';
			submittal.transactionsource = 'PP';
			submittal.inviterEmail = 'test@demo.com';
			submittal.ecvid = '12233';


			req.requestURI = '/services/apexrest/Person/Application/V1/';
			req.httpMethod = 'POST';
			RestContext.request = req;
			RestContext.response = res;
			ATSOrderRESTService.doPost(submittal);
			submittal.appliedDate = '2020-04-04T05:45:47Z';
			ATSOrderRESTService.doPost(submittal);
			
		}
		Test.stopTest();
	}

	@IsTest
	public static void getJobPostingPhenom_Test() {
		Test.startTest();
		String boardPostingId = [Select id, Name from Job_Posting__c where Job_Title__c = 'Test Engineer' LIMIT 1]?.Name;
		Contact talCont = [Select Id from Contact Limit 1];

		ATSSubmittal submittal = new ATSSubmittal();
		submittal.vendorId = '6';
		submittal.feedSourceId = 'feed11';
		submittal.jobPostingId = boardPostingId;
		submittal.talentId = talCont.Id;

		Job_Posting__c jp = ATSOrderRESTService.getJobPosting(submittal);
		System.assertNotEquals(jp.Opportunity__c, null);
		Test.stopTest();
	}
	
	@IsTest
	public static void getJobPosting_Test() {
		Test.startTest();
		String boardPostingId = '73066296';
		Contact talCont = [Select Id from Contact Limit 1];

		ATSSubmittal submittal = new ATSSubmittal();
		submittal.vendorId = '6';
		submittal.feedSourceId = 'feed11';
		submittal.jobPostingId = boardPostingId;
		submittal.talentId = talCont.Id;

		Job_Posting__c jp = ATSOrderRESTService.getJobPosting(submittal);
		System.assertNotEquals(jp.Opportunity__c, null);
		Test.stopTest();
	}

	@IsTest
	public static void getJobPostingExp_Test() {
		Test.startTest();
		String boardPostingId = '73099299';
		Contact talCont = [Select Id from Contact Limit 1];

		ATSSubmittal submittal = new ATSSubmittal();
		submittal.vendorId = '6';
		submittal.feedSourceId = 'feed11';
		submittal.jobPostingId = boardPostingId;
		submittal.talentId = talCont.Id;
		try {
			Job_Posting__c jp = ATSOrderRESTService.getJobPosting(submittal);
		} catch(Exception exp) {
			System.assertEquals(ATSOrderRESTService.ATS_EXCEPTION_OPPORTUNITY_DELETED, exp.getMessage());
		}
		Test.stopTest();

	}

	@IsTest
	public static void getJobPostingNoJPExp_Test() {
		Test.startTest();
		String boardPostingId = '1200112';
		Contact talCont = [Select Id from Contact Limit 1];

		ATSSubmittal submittal = new ATSSubmittal();
		submittal.vendorId = '6';
		submittal.feedSourceId = 'feed11';
		submittal.jobPostingId = boardPostingId;
		submittal.talentId = talCont.Id;
		try {
			Job_Posting__c jp = ATSOrderRESTService.getJobPosting(submittal);
		} catch(Exception exp) {
			System.assertEquals(ATSOrderRESTService.ATS_EXCEPTION_JOBPOSTING_NOTAVAILABLE, exp.getMessage());
		}
		Test.stopTest();

	}
}