public without sharing class ATSPrintRecordController {

	public Boolean optionChecked {get; set;}
    public Boolean showCustomizedFields {get; set;}
    public List<PrintRecord> printSettings {get;set;}
    public List<PrintRecord> allPrintSettings {get;set;}
    public List<String> listDefaultFieldIds;
    public List<PrintRecordWrapper> prWrappers {get;set;}
	public Map<String,List<PrintRecordWrapper>> prWrappersMap{get;set;}
    map<string,Schema.SObjectField> fieldList;
    public List<String> forceFields = new List<String>{'Req_End_Client__c','Req_Hiring_Manager__c'};
    // Give all json fields in the small case letters
    public List<String> jsonFields = new List<String>{'enterprisereqskills__c'};
    String strAllFields = '';
    public String oppTitle {get;set;}
	//Boolean Field for select or deselect all field
	public Boolean selectAllFields {get; set;}
	public List<String> keylabelSet {get;set;}
	public Boolean isPreviewEnabled {get; set;}
	public Map<String,List<String>> sectionFieldMap{get;set;}
	public Map<String,List<PrintRecord>> customizeFieldMap{get;set;}
	 public Map<String,Decimal> sectionOrder{get;set;}
	// Enable or Disable Preview Button
	public Boolean showPreviewButton {get; set;}

	public Map<String,Boolean> sectionHide{get;set;}
	
	//Preferences
	public Boolean setAsPref {get; set;}
	public String callJSToastMsg {get; set;}
	public static final String PRINT_PREFERENCES = 'Print Preferences';
	public String domainUrl {get;set;}
	//variable for close Modal
	public String callCloseModal {get; set;}
	// Variables for Cancel Button 
	public List<SObject> oppAllFieldValues {get;set;}
	public String allStringFields {get;set;}
	public String queryFields {get;set;}

    public void updateValue(){
        this.optionChecked = this.optionChecked ? false : true;
    }
	public PageReference verifySelectedRecords(){
			List<PrintRecord> notSelectedPS = new List<PrintRecord>();
            for(PrintRecord prec : allPrintSettings){
                if(prec.fieldSelected == false){
					notSelectedPS.add(prec);
                }
            }
			if(notSelectedPS.size() == allPrintSettings.size()){
				showPreviewButton = false;
			}else{
				updateCustomizeFieldValue();
			}
		return null;
	}

    /*public PageReference verifySelectedRecords1(){
           // List<PrintRecord> notSelectedPS = new List<PrintRecord>();
           // for(PrintRecord prec : allPrintSettings){
           //     if(prec.fieldSelected == false){
           //         notSelectedPS.add(prec);
           //     }
           // }
           // if(notSelectedPS.size() == allPrintSettings.size()){
            //    showPreviewButton = false;
           // }else{
			   showCustomizedFields = false;
			   removeSelectedRecords();
           // }
        return null;
    }*/
	public PageReference cancelToPreview(){
		List<String> userPreferenceFieldList = new List<String>();
		List<User_Search_Log__c> userPreferenceList = new List<User_Search_Log__c>();

		userPreferenceList = getUserPreferences();
		String fieldsToQuery = '';
		if(userPreferenceList.size() > 0) {
			//parsing the user preferences to a list
			userPreferenceFieldList = parseUserPreferenceJSON(userPreferenceList);
		}
		if(userPreferenceFieldList.size() > 0){
			for(String userPreferenceField:userPreferenceFieldList) {
				//Checks if all user preference field list are available in metadata field list
				if(strAllFields.contains(userPreferenceField)) {
					if(fieldsToQuery == '')
						fieldsToQuery = userPreferenceField ;
					else
						fieldsToQuery = fieldsToQuery+','+userPreferenceField;
				}
			}
		
		} 
		if(!String.isEmpty(fieldsToQuery) ) {
			queryFields = fieldsToQuery;
		}
		callJSToastMsg = '';
		showCustomizedFields = false;
		makeCustomizeFieldSet(oppAllFieldValues,queryFields,allStringFields);
        populatePRWrapper(allPrintSettings);
		return null;
	}

    public PageReference cancelTempView(){           
        showPreviewButton = false;
        return null;
    }    
	public PageReference removeSelectedRecords(){    
       
		return null;
	}
    public PageReference updateCustomizeFieldValue(){
		showPreviewButton = true;
		callJSToastMsg = '';
		callCloseModal ='';
        if(this.showCustomizedFields){
            // Add selected fields into the defualt list
            List<PrintRecord> selectedPS = new List<PrintRecord>();
            List<PrintRecord> allPS = new List<PrintRecord>();
            for(PrintRecord prec : allPrintSettings){
                if(prec.fieldSelected){
                    selectedPS.add(prec);
                }else{
                    allPS.add(prec);
                }
            }
            printSettings = selectedPS;
            allPS = allPrintSettings;
            populatePRWrapper(allPrintSettings);			
        }
        this.showCustomizedFields = this.showCustomizedFields ? false : true;
        return null;
    }
	//Called when Select All is link is clicked
	public PageReference selectAll(){
		callJSToastMsg = '';
		callCloseModal ='';
		selectAllFields = true;
		selectDeselect();
		return null;
	}
	//Called when DeSelect All is link is clicked
	public PageReference deselectAll(){
		callJSToastMsg = '';
		callCloseModal='';
		selectAllFields = false;
		selectDeselect();
		return null;
	}
	public PageReference selectDeselect(){
		List<PrintRecord> notSelectedPS = new List<PrintRecord>();
        if(this.showCustomizedFields ){
            // Add selected fields into the defualt list
            List<PrintRecord> selectedPS = new List<PrintRecord>();
            List<PrintRecord> allPS = new List<PrintRecord>();
            for(PrintRecord prec : allPrintSettings){
                
				If(selectAllFields)	 prec.fieldSelected = true ;
				 else
				 prec.fieldSelected = false;
				
				if(prec.fieldSelected == false){
					notSelectedPS.add(prec);
                }
            }
            printSettings = selectedPS;
			
			if(notSelectedPS.size() == allPrintSettings.size()){
				showPreviewButton = false;
				
			}else{
				showPreviewButton = true;
			}
            populatePRWrapper(allPrintSettings);
        }
		this.showCustomizedFields=true;
        return null;
    }
	public PageReference hidePreview(){
		List<PrintRecord> notSelectedPS = new List<PrintRecord>();
            for(PrintRecord prec : allPrintSettings){
                if(prec.fieldSelected == false){
					notSelectedPS.add(prec);
                }
            }
			if(notSelectedPS.size() == allPrintSettings.size()){
				showPreviewButton = false;
			}else{
				showPreviewButton = true;
			}
            populatePRWrapper(allPrintSettings);
		this.showCustomizedFields=true;
        return null;
    }
    public ATSPrintRecordController(){
         strAllFields = '';
		showPreviewButton = false;
		sectionFieldMap = new Map<String,List<String>>();
		printSettings = new List<PrintRecord> ();
        showCustomizedFields = false;
		selectAllFields = false;
		sectionOrder = new Map<String,Decimal>();
		//Preferences
		setAsPref = false;
		callJSToastMsg = '';
		callCloseModal='';
		List<String> userPreferenceFieldList = new List<String>();
		List<User_Search_Log__c> userPreferenceList = new List<User_Search_Log__c>();
        String oppId = System.currentPageReference().getParameters().get('oppId');
        try{
            if(oppId != null){
                Opportunity opp = [SELECT Id,OpCo__c FROM Opportunity WHERE Id = :oppId];
				//fetch user preferences
				userPreferenceList = getUserPreferences();
				if(userPreferenceList.size() > 0) {
					//parsing the user preferences to a list
					userPreferenceFieldList = parseUserPreferenceJSON(userPreferenceList);
				}
                List<Print_Setting__mdt> allDBPrintSettings = [SELECT Available_Fields__c,Default_Fields__c,Id,Section_Name__c,Section_Order__c FROM Print_Setting__mdt WHERE Opco__c =:opp.OpCo__c];
				String fieldsToQuery = '';
                for(Print_Setting__mdt ps : allDBPrintSettings){
					if(sectionOrder.containsKey(ps.Section_Name__c)){
						sectionOrder.put(ps.Section_Name__c,ps.Section_Order__c);	
					}else{
						sectionOrder.put(ps.Section_Name__c,ps.Section_Order__c);
					}
					List<String> allFieldList = New List<String> ();
                    //Create Map with Section Name as Key and All Available Fields as list
					if(sectionFieldMap.containsKey(ps.Section_Name__c)){
						allFieldList=sectionFieldMap.get(ps.Section_Name__c);
						list<String> fields=new List<String>(ps.Available_Fields__c.split(','));
						allFieldList.addAll(fields);
						sectionFieldMap.put(ps.Section_Name__c,allFieldList);
					}else{
						allFieldList = ps.Available_Fields__c.split(',');
						sectionFieldMap.put(ps.Section_Name__c,allFieldList);
					}			
					if(ps.Default_Fields__c != null && ps.Default_Fields__c.length()>0){
                        if(fieldsToQuery == '' && strAllFields==''){
						
						if(userPreferenceFieldList.size() <= 0) // fetch default fields only if no user preferences
							fieldsToQuery = ps.Default_Fields__c;

                        strAllFields = ps.Available_Fields__c;
                        }
                        else{
							if(userPreferenceFieldList .size() <= 0) // fetch default fields only if no user preferences
								fieldsToQuery = fieldsToQuery + ',' + ps.Default_Fields__c;

							strAllFields = strAllFields +','+ ps.Available_Fields__c;
                        }
                    }
					//if the default Field is Blank then putting the Available fields 
					else{
						strAllFields = strAllFields +','+ ps.Available_Fields__c;
					}
                }

				//Populate user preferred fields to fieldsToQuery variable
				if(userPreferenceFieldList.size() > 0) {					
					for(String userPreferenceField:userPreferenceFieldList) {
						//Checks if all user preference field list are available in metadata field list
						if(strAllFields.contains(userPreferenceField)) {
							if(fieldsToQuery == '')
								fieldsToQuery = userPreferenceField ;
							else
								fieldsToQuery = fieldsToQuery+','+userPreferenceField;
						}
					}
					
				}
							
				if (strAllFields.startsWith(',')) {
				  strAllFields = strAllFields.substring(1);
				}
				if (strAllFields.endsWith(',')) {
				  strAllFields = strAllFields.substring(0, strAllFields.length() - 1);
				}
                // Query all the fields
                String selectQuery = 'Select ' + strAllFields + ' from Opportunity where id = \'' + oppId + '\'';
                List<SObject> oppAllFields = Database.query(selectQuery);
                printSettings = this.mapToFields(oppAllFields,fieldsToQuery);
				//get back up for Cancel Button
				oppAllFieldValues = new List<SObject>(oppAllFields);
				//allStringFields = new String();
				allStringFields=strAllFields;
				//queryFields = new String();
				queryFields=fieldsToQuery;


                // Make full print settigns
                makeCustomizeFieldSet(oppAllFields,fieldsToQuery,strAllFields);
                populatePRWrapper(allPrintSettings);
            }
        }catch(Exception e){
            System.debug('exception '+e.getMessage()+e.getLineNumber());
        }		
    }
    public void makeCustomizeFieldSet(List<SObject> oppAllFields,String defualtFields, String allFields){
       // Add defualt field
       Opportunity opp = (Opportunity)oppAllFields[0];
       List<String> defualtFieldList = defualtFields.split(',');
       List<String> allFieldList = allFields.split(',');
       map<string,Schema.SObjectField> fieldList = schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
       allPrintSettings = new List<PrintRecord>();
	   
  
       for(String fieldAPI : allFieldList){
            if(!forceFields.contains(fieldAPI)){
                String fieldAPI1 = fieldAPI.toLowerCase(); // JSON fields to validate, in lower case format
                Schema.SObjectField objField = fieldList.get(fieldAPI);

                if(objField != null){
                    DescribeFieldResult describe = objField.getDescribe();
                    PrintRecord printRec = new PrintRecord();
                    printRec.fieldAPIName =  fieldAPI;
                    printRec.fieldDisplayName =  describe.getLabel();
                    Object fieldValue = opp.get(fieldAPI);

                    if(jsonFields.contains(fieldAPI) || 
                        jsonFields.contains(fieldAPI1)){
                        String skills=parseJSONString(fieldValue);
                        printRec.fieldDisplayValue = skills;
                    }
                    else if(fieldValue instanceOf DateTime){
                        DateTime dateValue = (DateTime)fieldValue;
                        fieldValue = dateValue.format('MMM-dd-yyyy');
                        printRec.fieldDisplayValue = String.valueOf(fieldValue);
                    }else{
                        printRec.fieldDisplayValue = String.valueOf(fieldValue);
                    }
                    printRec.isDefualtField =  defualtFieldList.contains(fieldAPI);
                    printRec.fieldSelected =  defualtFieldList.contains(fieldAPI);
                    allPrintSettings.add(printRec);
					
                }
            }                         
       }
	    
    }

    private String parseJSONString(Object objValue){
        List<String> skills = new List<String>();
        try{
            List<Map<String,String>> listRecords = (List<Map<String,String>>)JSON.deserialize((String)objValue,List<Map<String,String>>.class);
            for(Map<String,String> recMap : listRecords){
                String skillName = recMap.get('name');
                if(skillName!=null){
                    skills.add(skillName);
                }
            }
        }catch(Exception e){
            System.debug('Parese String  exception - '+e.getMessage());
        }

        return String.join(skills,',');
    }
    public List<PrintRecord> mapToFields(List<SObject> oppAllFields,String fieldsToQuery){
        listDefaultFieldIds = new List<String>();
        List<PrintRecord> result = new List<PrintRecord>();
        List<String> defualtFields = fieldsToQuery.split(',');
        Opportunity opp = (Opportunity)oppAllFields[0];
        map<string,Schema.SObjectField> fieldList1 = schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
        fieldList = new map<string,Schema.SObjectField>();

        for(String mapKey : fieldList1.keySet()){
           mapKey = mapKey.toLowerCase();
           fieldList.put(mapKey,fieldList1.get(mapKey)); 
        }
        PrintRecord printRec = null;
        oppTitle = opp.Name;
        List<String> strTokens = null;
        for(String fieldAPI : defualtFields){
            if(!forceFields.contains(fieldAPI)){
                String apiName = fieldAPI;
                 String relObjName = '';         
            fieldAPI = fieldAPI.toLowerCase();
            Schema.SObjectField objField = fieldList.get(fieldAPI);
            if(objField != null){
                DescribeFieldResult describe = objField.getDescribe();
                printRec = new PrintRecord();
                printRec.fieldAPIName =  apiName;
                printRec.fieldDisplayName =  describe.getLabel();
                Object fieldValue = null;
                if(apiName == fieldAPI){
                     fieldValue = opp.get(apiName);
                }else{                        
                      fieldValue = opp.getSobject(relObjName).get(strTokens[1]);
                }

                if(jsonFields.contains(fieldAPI)){
                    fieldValue=parseJSONString(fieldValue);                       
                }
                else if(fieldValue instanceOf DateTime){
                    DateTime dateValue = (DateTime)fieldValue;
                    fieldValue = dateValue.format('MMM-dd-yyyy');
                }
                printRec.fieldDisplayValue = String.valueOf(fieldValue);
                printRec.isDefualtField =  true;
                printRec.fieldSelected =  true;
                result.add(printRec);
            }else{
                System.debug(' No Field value' + fieldAPI);
            }
            }
        } 
        return result;
    }

    public void populatePRWrapper(List<PrintRecord> listPrintRecs){
		for(String key:sectionFieldMap.keySet()){
			for(String getFieldName:sectionFieldMap.get(key)){
				for(PrintRecord pr:listPrintRecs){
					if(pr.fieldAPIName == getFieldName){
						pr.sectionName = key;
					}
				}
			}
		}
        List<PrintRecordWrapper> prwrapper = new List<PrintRecordWrapper>();
		prWrappersMap = new Map<String,List<PrintRecordWrapper>> ();
		//keylabelSet = new List<String>(sectionOrder.keySet().size());
		customizeFieldMap = new Map<String,List<PrintRecord>>();

        if(listPrintRecs!=null && listPrintRecs.size()>0){
            for(Integer prc=0;prc<listPrintRecs.size();prc=prc+3){

                PrintRecordWrapper prw = new PrintRecordWrapper();
                if(prc<listPrintRecs.size()){
                    prw.record1 = listPrintRecs.get(prc);
                }
                if((prc+1)<listPrintRecs.size()){
                    prw.record2 = listPrintRecs.get(prc+1);
                }
                if((prc+2)<listPrintRecs.size()){
                    prw.record3 = listPrintRecs.get(prc+2);
                }
                prwrapper.add(prw);
            }

			for(String k:sectionFieldMap.keySet()){
				//List<PrintRecord> allPRList = New List<PrintRecord>();
				for(PrintRecord p: listPrintRecs){
					List<PrintRecord> allPRList = New List<PrintRecord>();
					if(!customizeFieldMap.containsKey(k) ){
						allPRList = New List<PrintRecord>();	
						customizeFieldMap.put(k,allPRList);						
					}

					if(p.sectionName.equalsIgnoreCase(k) && customizeFieldMap.containsKey(k)){
						allPRList=customizeFieldMap.get(k);
						allPRList.add(p);
						customizeFieldMap.put(k,allPRList);
						allPRList = New List<PrintRecord>();
					}
				}
				
			}
				sectionHide = new Map<String,Boolean>();
			for(String k:sectionFieldMap.keySet()){
				List<PrintRecord> notSelectedPS = new List<PrintRecord>();
				for(PrintRecord pr:customizeFieldMap.get(k)){
				
					if(pr.fieldSelected == false){
						notSelectedPS.add(pr);
					}	
				}
				System.debug('notSelectedPS---'+notSelectedPS.size()+'Customize fieldMap'+customizeFieldMap.get(k).size()+'value of K is --'+k);
				if(notSelectedPS.size() == customizeFieldMap.get(k).size()){
					sectionHide.put(k,false);	
				}else{
					sectionHide.put(k,true);
				}
			}
			System.debug('sectionOrder.key size---'+sectionOrder.keySet());

            // Setting up the order of the secions.
            // Its not an optimized solution, needs to be improved.
			keylabelSet = new List<String>();
			for(integer i =0; i < sectionOrder.keySet().size(); i++){
				keylabelSet.add('');
			}
                        
			for (String sec:sectionOrder.keySet()){
				Integer ord= Integer.valueOf(sectionOrder.get(sec));
				ord = ord - 1;
				if(ord < keylabelSet.size()){
					keylabelSet.remove(ord);
				}
                if(ord>=keylabelSet.size()){
                    keylabelSet.add(sec);
                }else{
                    keylabelSet.add(ord,sec);
                }				
			}  
			prWrappers = prwrapper;
        }
    }

	//Save User Preferences when user clicks on preferences
	public pagereference saveUserPreferences() {	
		try {	
		if(setAsPref) {
			String userPrefJSON = getUserPrefJSON();
			// Query user search log objects on current user to see if preference already exists
			List<User_Search_Log__c> existingPrefs = getUserPreferences();

			if(existingPrefs.size() > 0) { //update Preferences if already existing
				for(User_Search_Log__c existingPref:existingPrefs) {

					existingPref.Search_Params__c = userPrefJSON;

				}
				update existingPrefs;
				
			} else { //create Preferences if no preference already exists
				User_Search_Log__c userPrefObj = new User_Search_Log__c();
				userPrefObj.Search_Params__c = userPrefJSON;
				userPrefObj.User__c = UserInfo.getUserId();
				userPrefObj.Type__c = PRINT_PREFERENCES;				
				insert userPrefObj;				
			}
		}
		callJSToastMsg = '<script>sendToLC();</script>';
		setAsPref = false;
		} catch(Exception e) {}
		return null;
	}

	public String getUserPrefJSON() {
		List<String> fieldAPIList = new List<String>();
		ATSUserPreferencesJSON userPrefs = new ATSUserPreferencesJSON();
		List<ATSUserPreferencesJSON.UserPrintPreferences> printPrefList = new List<ATSUserPreferencesJSON.UserPrintPreferences>();
		ATSUserPreferencesJSON.UserPrintPreferences printPref = new ATSUserPreferencesJSON.UserPrintPreferences();
		printPref.ObjectName = 'Opportunity';
		for (List<PrintRecord> printRecords: customizeFieldMap.values()){	
			for(PrintRecord printRecord: printRecords) {
				if(printRecord.fieldSelected) {
					fieldAPIList.add(printRecord.fieldAPIName);
				}
			}		
		}
		printPref.PrintPreferences = fieldAPIList;
		printPrefList.add(printPref);
		userPrefs.UserPreferences = printPrefList;	
		//Serialize JSON
		String userPrefJson = JSON.serialize(userPrefs);
		return userPrefJson;
	}

	public List<User_Search_Log__c> getUserPreferences() {
		List<User_Search_Log__c> userPrefList = [SELECT Id,Name,OwnerId,Search_Params__c,Type__c,User__c FROM User_Search_Log__c WHERE User__c =: UserInfo.getUserId() AND Type__c = 'Print Preferences' ];
		return userPrefList;
	}
	private List<String> parseUserPreferenceJSON(List<User_Search_Log__c> userPrefList) {
		ATSUserPreferencesJSON parsedJson = (ATSUserPreferencesJSON)JSON.deserialize(userPrefList[0].Search_Params__c, ATSUserPreferencesJSON.class);
		System.debug('ParsedJson:'+parsedJson);
		return parsedJson.UserPreferences[0].PrintPreferences;		
	}	 
    public class PrintRecordWrapper{
        public PrintRecord record1{get;set;}
        public PrintRecord record2{get;set;}
        public PrintRecord record3{get;set;}
    }
    public class PrintRecord{
        public String fieldDisplayName {get;set;}
        public String fieldAPIName {get;set;}
		public String sectionName {get;set;}
        public String fieldDisplayValue {get;set;}
        public Boolean fieldSelected {get;set;}
        public Boolean isDefualtField {get;set;}

    }
}