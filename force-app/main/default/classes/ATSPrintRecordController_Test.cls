@isTest
public class ATSPrintRecordController_Test  {
    public static Opportunity opp = null;
    static testmethod void testParameters(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
		Account Acc1 = CreateTalentTestData.createTalent();
        Contact talCont = CreateTalentTestData.createTalentContact(Acc1);
        Acc1.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
		Update Acc1;

		Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
		Name = 'TIVOLI',
		Category_Id__c = 'FIN_RM',
		Category__c = 'Finance Resource Management',
		Job_Code__c = 'Developer',
		Jobcode_Id__c	='700009',
		OpCo_Id__c='ONS',
		Segment_Id__c	='A_AND_E',
		Segment__c = 'Architecture and Engineering',
		OpCo_Status__c = 'A',
		Skill_Id__c	='TIVOLI',
		Skill__c = 'Tivoli' );
		insert prd;
		
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        String accName = 'New Opportunity'+Acc1.name;
        opp = new Opportunity( Name =  accName, LDS_Account_Name__c = Acc1.id, Accountid = Acc1.id,                
                    RecordTypeId = ReqRecordTypeId, 
                    Formal_Decision_Criteria__c = 'Defined', 
                    Opco__c ='Aerotek, Inc',
					Division__c = 'EASi Engineering',
					Legacy_Product__c = prd.Id,
                    BusinessUnit__c ='EASI',
                    Response_Type__c = 'Formal RFx',
                    stagename = 'Draft',
                    Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                    Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                    Impacted_Regions__c='Option1',
                    Access_To_Funds__c = 'abc', 
                    Aligned_to_defined_GS_Objective__c = 'Yes',
                    Compelling_Event__c = 'Solution is required, pain is verified and felt',
                    Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                    Sales_Resource_Requirements__c = 'Less than standard effort/light investment',
                    CloseDate = system.today()+1,Req_Job_Description__c = 'Test',
                    Req_Qualification__c = 'Test',
                    Req_Product__c = 'Contract',
                    Req_Bill_Rate_Max__c=10,
                    Req_Bill_Rate_Min__c=1,
                    Req_Duration_Unit__c = 'Day(s)',
                    Req_Duration__c=10,
                    Req_Pay_Rate_Max__c = 10,
                    Req_Pay_Rate_Min__c = 1,
                    Req_Standard_Burden__c = 10,
                    Req_Rate_Frequency__c='Hourly');
         insert opp;

		 List<Print_Setting__mdt> allDBPrintSettings = [SELECT Available_Fields__c,Default_Fields__c,Id,Section_Name__c,Section_Order__c FROM Print_Setting__mdt WHERE Opco__c =:opp.OpCo__c];
		 System.debug('all DB PrintSettings:'+allDBPrintSettings);
         Test.startTest();
		 ApexPages.currentPage().getParameters().put('oppId',opp.Id);
         ATSPrintRecordController pc = new ATSPrintRecordController();
         //System.assert(pc.printSettings != null);
		 pc.showCustomizedFields = true;
		 pc.updateCustomizeFieldValue();
		 pc.deselectAll();
		 pc.hidePreview();
		 pc.selectAll();
		 pc.setAsPref = true;
		 pc.saveUserPreferences();		 
		 pc.verifySelectedRecords();
		 Test.stopTest();
    }
}