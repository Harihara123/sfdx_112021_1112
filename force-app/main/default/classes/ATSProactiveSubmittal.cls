public class ATSProactiveSubmittal  {
	
	@AuraEnabled(cacheable=true)
	public static ProactiveSubmittalInfo getSubmittalFormInfo(ID contactId) {
		ProactiveSubmittalInfo submittalInfo = getJobTitleNSkills(contactId);

		return submittalInfo;
	}
	/**
		This method pull all the details which are needed to show a Proactive Submittal form for a given proactive submittal record/Order Id.
	 */
	@AuraEnabled(cacheable=false)
	public static ProactiveSubmittalInfo getPASubmittalRecordDetails(ID orderId) {
		ProactiveSubmittalInfo submittalInfo = getPAOrderDetails(orderId);
		return submittalInfo;
	}
	private static ProactiveSubmittalInfo getPAOrderDetails(ID orderId) {
		ProactiveSubmittalInfo proSubmittal = new ProactiveSubmittalInfo();
		try{
			Order orderRec = [select Id, ShipToContactId,ShipToContact.Name,Job_Title__c,Point_of_Contact_AM__c,  Type, Point_of_Contact_AM__r.Name, Skills__c, AccountId,Account.Name, Hiring_Manager__c,Hiring_Manager__r.Name, Talent_Qualifications__c,Description, Pay_Rate__c, Rate_Frequency__c,Salary__c, Proactive_Submittal_Currency__c, Status from Order where Id=:orderId];
			if(orderRec != null) {
				proSubmittal.jobTitle = orderRec.Job_Title__c;
				proSubmittal.skills = orderRec.Skills__c;

				proSubmittal.orderId = orderRec.Id;
				proSubmittal.accountId = orderRec.AccountId;
				proSubmittal.accountName = orderRec.Account.Name;

				proSubmittal.hiringManagerId = orderRec.Hiring_Manager__c;
				proSubmittal.hiringManagerName = orderRec.Hiring_Manager__r.Name;


				proSubmittal.accountManagerId = orderRec.Point_of_Contact_AM__c;
				proSubmittal.accountManagerName = orderRec.Point_of_Contact_AM__r.Name;

				proSubmittal.shipToContactId = orderRec.ShipToContactId;
				proSubmittal.shipToContactName = orderRec.ShipToContact.Name;

				proSubmittal.submissionType = orderRec.type;
				proSubmittal.talentQualification = orderRec.Talent_Qualifications__c;

				proSubmittal.description = orderRec.Description;

				proSubmittal.payRate = orderRec.Pay_Rate__c;

				proSubmittal.rateFreq = orderRec.Rate_Frequency__c;
				proSubmittal.psStatus = orderRec.Status;

				proSubmittal.salary = orderRec.Salary__c;
				proSubmittal.submittalCurrency = orderRec.Proactive_Submittal_Currency__c;

				
			}
		}catch(Exception ex){
			System.debug('getPAOrderDetails '+ex.getCause());
		}			
		return proSubmittal;
	}
	/*@AuraEnabled(cacheable=true)
	public static String getJobTitle(ID contactId) {
	System.debug('contactId---'+contactId);
		String jobTitle = null;
		ID conId = contactId;
		if(conId != null) {
			List<Contact> contacts = [select title FROM Contact WHERE id=: conId];
			jobTitle = contacts.get(0).title;
			//Id accountId = [select accountid from cotact WHERE id =: conId];
		}
		System.debug('jobTitle---'+jobTitle);
		return jobTitle;
	}

	@AuraEnabled(cacheable=true)
	public static List<String> getJobTitleList(String jobTitleKey) {
		List<String> jobTitleList = new List<String>();
		return jobTitleList;
	}

	@AuraEnabled(cacheable=true)
	public static List<User> getAccountManager(string acManagerName) {
		System.debug('acManagerName---------'+acManagerName);
		String key = '%' + acManagerName + '%';
		return [select id, Name from user where Profile_Name__c = 'Single Desk 1' and isActive = true and name like :key limit 5];
	}

	@AuraEnabled(cacheable=true)
	public static String getSkill(ID contactId) {//need to delete it
		return '';
	}*/
	
	private static ProactiveSubmittalInfo getJobTitleNSkills(ID contactId) {
		ProactiveSubmittalInfo proSubmittal = new ProactiveSubmittalInfo();
		Contact ct = [select Id, Title, Account.skills__c from contact where Id=:contactId];
		if(ct != null) {
			proSubmittal.jobTitle = ct.Title;
			proSubmittal.skills = ct.Account.Skills__c;
		}
		
		return proSubmittal;
	}


	
	/*@AuraEnabled(cacheable=true)
	public static List<String> getSkillList(String skillKey) {
		List<String> skillList = new List<String>();
		return skillList;
	}

	@AuraEnabled(cacheable=true)
	public static List<String> getAccountList(String accountKey) {
		List<String> accountList = new List<String>();
		return accountList;
	}

	@AuraEnabled(cacheable=true)
	public static List<String> getHiringManagerList(String hiringMgrKey) {
		List<String> hiringMgrList = new List<String>();
		return hiringMgrList;
	}*/

	@AuraEnabled
	public static String saveProactiveSubmittal(ProactiveSubmittalModal psModal, String orderId) {
		String status = 'SUCCESS';
		System.debug('==================='+psModal);
		try {
			if(orderId != null && orderId.length() > 0){
				Order proactiveSubmittal = [select Id,Job_Title__c,Point_of_Contact_AM__c,Skills__c,AccountId,Hiring_Manager__c,Type,Talent_Qualifications__c,Description,Pay_Rate__c,Rate_Frequency__c,Salary__c,Proactive_Submittal_Currency__c,Unique_Id__c  from Order where Id=:orderId];
				proactiveSubmittal.Job_Title__c = psModal.jobTitle;
				proactiveSubmittal.Point_of_Contact_AM__c = psModal.accountMgrID; //which field of Submittal
				proactiveSubmittal.Skills__c = psModal.skills;
				proactiveSubmittal.AccountId = psModal.accountId;			
				proactiveSubmittal.Hiring_Manager__c = psModal.hiringMgrId;
				proactiveSubmittal.Type = psModal.submittalType;
				proactiveSubmittal.Talent_Qualifications__c = psModal.talentQualification;
				proactiveSubmittal.Description = psModal.descRole;
				proactiveSubmittal.Pay_Rate__c = psModal.payRate;
				proactiveSubmittal.Rate_Frequency__c = psModal.rateType;
				proactiveSubmittal.Salary__c = psModal.desireSalary;
				proactiveSubmittal.Proactive_Submittal_Currency__c = psModal.submittalCurrency;
				proactiveSubmittal.Unique_Id__c = getProSubmUniqueNum(psModal.accountId,  psModal.contactId);
				//proactiveSubmittal.Status = 'Submitted';
				//proactiveSubmittal.EffectiveDate = System.today();
				//proactiveSubmittal.Is_Created_As_Promarket__c = true;
				upsert proactiveSubmittal;
			}else{
				Order proactiveSubmittal = new Order();
				proactiveSubmittal.RecordTypeId = Utility.getRecordTypeId('Order', 'Proactive');
				proactiveSubmittal.ShipToContactId = psModal.contactId;
				proactiveSubmittal.Job_Title__c = psModal.jobTitle;
				proactiveSubmittal.Point_of_Contact_AM__c = psModal.accountMgrID; //which field of Submittal
				proactiveSubmittal.Skills__c = psModal.skills;
				proactiveSubmittal.AccountId = psModal.accountId;			
				proactiveSubmittal.Hiring_Manager__c = psModal.hiringMgrId;
				proactiveSubmittal.Type = psModal.submittalType;
				proactiveSubmittal.Talent_Qualifications__c = psModal.talentQualification;
				proactiveSubmittal.Description = psModal.descRole;
				proactiveSubmittal.Pay_Rate__c = psModal.payRate;
				proactiveSubmittal.Rate_Frequency__c = psModal.rateType;
				proactiveSubmittal.Salary__c = psModal.desireSalary;
				proactiveSubmittal.Proactive_Submittal_Currency__c = psModal.submittalCurrency;
				proactiveSubmittal.Unique_Id__c = getProSubmUniqueNum(psModal.accountId,  psModal.contactId);
				proactiveSubmittal.Status = 'Submitted';
				proactiveSubmittal.EffectiveDate = System.today();
				proactiveSubmittal.Is_Created_As_Promarket__c = true;

				insert proactiveSubmittal;
			}			
		}catch(Exception ex) {
			String relatedId = psModal.accountId!=null ? String.valueOf(psModal.accountId) : '';
			Core_Log.logException('ATSProactiveSubmittal', ex.getMessage(), 'saveProactiveSubmittal',relatedId, 'Order', 'Saving proactive submittal modal');
			System.debug(ex.getMessage() );
			System.debug(ex.getStackTraceString());
			status =  'ERROR';
		}
		return status;
	}

	

	public static String getProSubmUniqueNum(ID accountId, ID contactId) {
		
		List<AggregateResult> aggResults =  [select COUNT(ID) from Order where Is_Created_As_Promarket__c = true];
		String uniqueSeq;
		for(AggregateResult ar : aggResults) {
			uniqueSeq =accountId + '_' + Integer.valueOf(ar.get('expr0')) + 1;
		}
		return uniqueSeq;
	}

	//Convert the proactive submittal to submittal after creation of Opportunity during interview update or Offer accepted.
	public static String updateProactiveSubmittal(Order proactiveSubmittal, Id opportunityID) {
		String updateStatus = 'SUCCESS';
		ID opportunitySubmissionRecTypeId = Utility.getRecordTypeId('Order', 'OpportunitySubmission'); 
		proactiveSubmittal.RecordTypeId = opportunitySubmissionRecTypeId;
		proactiveSubmittal.Unique_Id__c = String.valueOf(proactiveSubmittal.ShipToContactId) +  String.valueOf(opportunityID);
		proactiveSubmittal.OpportunityId = opportunityID;
		try {
			update proactiveSubmittal;
		}catch (Exception ex) {
			String relatedId = proactiveSubmittal.id !=null ? String.valueOf(proactiveSubmittal.id) : '';
			Core_Log.logException('ATSProactiveSubmittal', ex.getMessage(), 'updateProactiveSubmittal',relatedId, 'Order', 'Converting Proactive Submittal to Submittal after Oppoertunity creation');
			System.debug(ex.getMessage() );
			System.debug(ex.getStackTraceString());
			updateStatus = 'ERROR';
		}

		return updateStatus;
	}

	public class ProactiveSubmittalModal {
		@AuraEnabled public  String jobTitle{get;Set;}
		@AuraEnabled public  String skills{get;set;}
		@AuraEnabled public  String orderId{get;set;}
		@AuraEnabled public  String accountMgrID{get;set;}
		@AuraEnabled public  String hiringMgrId{get;set;}
		@AuraEnabled public  String accountId{get;set;}
		@AuraEnabled public  String contactId{get;set;}
		@AuraEnabled public  String submittalType{get;set;}
		@AuraEnabled public  String talentQualification{get;set;}
		@AuraEnabled public  String descRole{get;set;}
		@AuraEnabled public  Decimal payRate{get;set;}
		@AuraEnabled public  Decimal desireSalary{get;set;}
		@AuraEnabled public  String rateType{get;set;}
		@AuraEnabled public  String submittalCurrency{get;set;}
	}

	/*@AuraEnabled(cacheable=true)
	public static PicklistWrapper getPicklistValues() {
		PicklistWrapper pw = new PicklistWrapper();
		pw.submittalTypeOptions = Utilities.picklistValues('Order', 'type');
		pw.rateTypeOptions = Utilities.picklistValues('Order', 'Rate_Frequency__c');
		pw.submittalCurrencyOptions = Utilities.picklistValues('Order', 'Proactive_Submittal_Currency__c');

		return pw;
	}

	public class PicklistWrapper {
		@AuraEnabled public List<String>submittalTypeOptions{get;Set;}
		@AuraEnabled public List<String>rateTypeOptions{get;Set;}
		@AuraEnabled public List<String>submittalCurrencyOptions{get;Set;}
	}*/

	public class ProactiveSubmittalInfo {
		@AuraEnabled public String jobTitle{get;set;}
		@AuraEnabled public String skills{get;set;}
        
		@AuraEnabled public Id orderId{get;set;}
        @AuraEnabled public Id accountId{get;set;}
		@AuraEnabled public String accountName{get;set;}
		
		@AuraEnabled public Id accountManagerId{get;set;}
		@AuraEnabled public String accountManagerName{get;set;}

		@AuraEnabled public Id hiringManagerId{get;set;}
		@AuraEnabled public String hiringManagerName{get;set;}

		@AuraEnabled public Id shipToContactId{get;set;}
		@AuraEnabled public String shipToContactName{get;set;}

		@AuraEnabled public String submissionType{get;set;}
		@AuraEnabled public String talentQualification{get;set;}
		@AuraEnabled public String description{get;set;}
		@AuraEnabled public Decimal payRate{get;set;}
		@AuraEnabled public String rateFreq{get;set;}
		@AuraEnabled public Decimal salary{get;set;} // Desired rate
		@AuraEnabled public String submittalCurrency{get;set;}
		@AuraEnabled public String psStatus{get;set;}
	}
	
}