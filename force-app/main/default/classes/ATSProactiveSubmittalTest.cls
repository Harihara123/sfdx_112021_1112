@isTest
public class ATSProactiveSubmittalTest  {
    
	@TestSetup 
	static void datasetup() {
		Account acc = CreateTalentTestData.createTalent();
		acc.Skills__c = '{"skills":["Salesforce.com","Marketing Cloud"]}';
		update acc;
		Contact ct = CreateTalentTestData.createTalentContact(acc);

		User usr = TestDataHelper.createUser('Single Desk 1');
        insert usr; 

		Account acc1 = TestData.newAccount(1, 'Client');
		insert acc1;
		Contact ct1 = TestData.newContact(acc1.id, 3, 'Client') ;
		insert ct1;
      
	}

	@IsTest
	public static void getSubmittalFormInfoTest() {
		Test.startTest();
			Contact ct = [select id from contact where RecordType.name='Talent' limit 1];
			ATSProactiveSubmittal.getSubmittalFormInfo(ct.id);
		Test.stopTest();
	}
    
    @isTest
    public static void getPASubmittalRecordDetailsTest() {
        Test.startTest();
            user ur = [select id from User limit 1];
            Contact hmgr = [select id from Contact where RecordType.name = 'Client' Limit 1];
            Account acc1 = [select id from Account where RecordType.name='Client' Limit 1];
            Contact talent = [select id from Contact where RecordType.name='Talent' Limit 1];
            
            
            Order sub = TestData.newOrder(acc1.id, hmgr.id, ur.id, 'Proactive');
            insert sub;
            
            Order sub1 = [select id, Job_Title__c from Order where RecordType.name='Proactive' Limit 1];
        	ATSProactiveSubmittal.getPASubmittalRecordDetails(sub1.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void getPASubmittalRecordDetailsNullTest() {
        Test.startTest();
        	Id n = Null;
        	ATSProactiveSubmittal.getPASubmittalRecordDetails(n);
        Test.stopTest();
    }

	@isTest
	public static void saveProactiveSubmittalTest() {
		user ur = [select id from User limit 1];
		Contact hmgr = [select id from Contact where RecordType.name = 'Client' Limit 1];
		Account acc1 = [select id from Account where RecordType.name='Client' Limit 1];
		Contact talent = [select id from Contact where RecordType.name='Talent' Limit 1];
		ATSProactiveSubmittal.ProactiveSubmittalModal modal = new ATSProactiveSubmittal.ProactiveSubmittalModal();

		modal.jobTitle = 'Architect';
		modal.skills = '{"skills":["Salesforce.com";"Marketing Cloud"]}';
		//modal.accountMgrID = ur.id;
		modal.hiringMgrId = hmgr.id;
		modal.accountId = acc1.id;
		modal.contactId = talent.id;
		modal.submittalType = 'Proactive';
		modal.talentQualification = 'Master';
		modal.descRole = 'Awesome';
		modal.payRate = 90;
		modal.desireSalary = 30000000;
		modal.rateType = 'Weekly';
		modal.submittalCurrency = '';

		Test.startTest();
			ATSProactiveSubmittal.saveProactiveSubmittal(modal,null);
			Order od = [Select id, Job_Title__c, ShipToContactId from order where RecordType.name='Proactive' limit 1 ];
			System.debug('!!!!!!!!  ID: ' + od.id);
			System.assertEquals(od.ShipToContactId, talent.id);
		Test.stopTest();

	}
    
    	@isTest
	public static void editProactiveSubmittalTest() {
        
        user ur = [select id from User limit 1];
		Contact hmgr = [select id from Contact where RecordType.name = 'Client' Limit 1];
		Account acc1 = [select id from Account where RecordType.name='Client' Limit 1];
		Contact talent = [select id from Contact where RecordType.name='Talent' Limit 1];
        
        
        Order sub = TestData.newOrder(acc1.id, hmgr.id, ur.id, 'Proactive');
        insert sub;
        
        Order sub1 = [select id, Job_Title__c from Order where RecordType.name='Proactive' Limit 1];
		ATSProactiveSubmittal.ProactiveSubmittalModal modal = new ATSProactiveSubmittal.ProactiveSubmittalModal();

		modal.jobTitle = 'Architect';
		modal.skills = '{"skills":["Salesforce.com";"Marketing Cloud"]}';
		//modal.accountMgrID = ur.id;
		modal.hiringMgrId = hmgr.id;
		modal.accountId = acc1.id;
		modal.contactId = talent.id;
		modal.submittalType = 'Proactive';
		modal.talentQualification = 'Master';
		modal.descRole = 'Awesome';
		modal.payRate = 90;
		modal.desireSalary = 30000000;
		modal.rateType = 'Weekly';
		modal.submittalCurrency = '';
		
        // Create a new Order record and pass the id to the save method.

		Test.startTest();
			ATSProactiveSubmittal.saveProactiveSubmittal(modal, sub1.Id);
			Order od = [select id, Job_Title__c, Skills__c from Order where RecordType.name='Proactive' Limit 1];
			System.debug('Job Title: ' + od.Job_Title__c);
			System.assertEquals(modal.JobTitle, od.Job_Title__c);
        	System.assertEquals(modal.skills, od.Skills__c);
		Test.stopTest();
    }
}
