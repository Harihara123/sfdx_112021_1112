@RestResource(urlMapping='/reqalertresults/v1old/*') 
global without sharing class ATSReqSearchAlertResultsService{
    //static final String ATS_RESULTS_POSTING_SUCCESSFUL = 'Results successfully posted.';
    static final String ATS_RESULTS_POSTING_UNSUCCESSFUL = 'Error in result posting.';
    public class ATSApplicationAPIException extends Exception{}

   /**
     * Sample request body JSON For Req 
     * { 
     * "alertId" : "a3o0Q000000CeNeQAK", 
     * "ownerId" : "0051p00000ABMmnAAH", 
     * "stage" : "Draft", 
     * "jobTitle" : "Production Damager",
     * "duration" : "6 Months", 
     * "billRate" : "NA", 
     * "city" : "Charleston", 
     * "state" : "South Carolina", 
     * "country" : "United States", 
     * "createdDate" : "2019-01-24T15:08:24.000Z", 
     * "skills" : ["Salesforce Architect", "Agile", "Enterprise Architecture", "Sfdc"], 
     * "opportunityId" : "0061k000007YDCOAA4", 
     * "alertMatchTimestamp" : "2019-01-24T15:08:24.000Z",
     * "resumeLastModifiedDate" : "2019-01-24T15:08:24.000Z",
     * "alertTrigger" : ["Req Created" ],
     * "remote" : true,
     * "subVendor" : true,
     * "accName" : "ABC Ltd",
     * "accId" : "0011k00000aRUXV",
     * "reqNumber" : "O-2806869"
     * "reqOwner" : "Test User"
     * }
*/
    @HttpPost
  global Static void doPost(){
        String status;
        String errStr = null;
        List<String> sList = new List<String>();
        List<Id> alertIds = new List<Id>();
        RestResponse response = RestContext.response;
       
        try{
            String getStrParam  = RestContext.request.requestBody.toString();
                ReqSearchAlertResultsModel searchResults = (ReqSearchAlertResultsModel) JSON.deserialize(getStrParam, ReqSearchAlertResultsModel.class);
                System.debug(searchResults);  
                if (searchResults != null) {
                    System.enqueueJob(new ReqSearchAlertResultsQueueable(searchResults));        
                    
                  }
              
        }
        catch(Exception e){
            status = ATS_RESULTS_POSTING_UNSUCCESSFUL + ' - ' + e.getMessage() + '\n' + e.getStackTraceString();
        }

    status = 'Successful inserts/updates = \n\n ' + status;
    RestContext.response.responseBody = Blob.valueOf(status); 
    }
}