@RestResource(urlMapping='/Person/ResumeApplicationStatus/V1/*')
/*
{

"applicationVendor": "eQuest",

"applicationSource":"JobServe",

"base64CV":"asdsa",
"fileName":"Resume.docx",
"documentType":"application/pdf",

"postingId":"2342",

"dateApplied":"2021-03-10T11:00:00Z"

}
*/
Global with sharing class  ATSResumeApplicationStatusAPI {
	 @HttpPost
    Global Static void doPost(){
        RestRequest req = RestContext.request;
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestJSONBody = body.toString();
        try{
            serviceWrapper wrapObject = (serviceWrapper) System.JSON.deserialize(requestJSONBody, serviceWrapper.class);
            
            List<Job_Posting_Channel_Details__c> postingList =[SELECT Id, Board_Id__c, Job_Posting_Id__c, Board_Name__c, 
                                               Job_Posting_Id__r.Req_Division__c,Job_Posting_Id__r.OwnerId,
                                               Job_Posting_Id__r.Owner.Name,Job_Posting_Id__r.Owner.Email,
                                               Job_Id__c, Board_Posting_Id__c 
                                               FROM Job_Posting_Channel_Details__c 
                                               where Board_Posting_Id__c =: wrapObject.postingId  with SECURITY_ENFORCED limit 1];
            
            Datetime applicationDate = system.now();
            if(wrapObject.dateApplied != '' && wrapObject.dateApplied != null){
              		applicationDate = (DateTime)JSON.deserialize('"' + wrapObject.dateApplied + '"', DateTime.class);    
            }
            Application_Staging__c stagingObject = new Application_Staging__c();
            stagingObject.Recruiter_Name__c = postingList[0].Job_Posting_Id__r.Owner.Name;
            stagingObject.Source__c ='EMEA_Application';
            stagingObject.Talent_Source__c= wrapObject.applicationSource;
            stagingObject.Vendor_ID__c = wrapObject.applicationVendor;
            stagingObject.Status__c = 'New';
            stagingObject.RecruiterEmail__c = postingList[0].Job_Posting_Id__r.Owner.Email;
            stagingObject.Recruiter__c = postingList[0].Job_Posting_Id__r.OwnerId;
            stagingObject.PostingID__c = postingList[0].Job_Posting_Id__c;
            stagingObject.OPCO__c = postingList[0].Job_Posting_Id__r.Req_Division__c;
            stagingObject.Application_Date__c = applicationDate;
            stagingObject.Candidate_First_Name__c=wrapObject.firstname;
            stagingObject.Candidate_Last_Name__c=wrapObject.lastname;
            stagingObject.Candidate_Email__c=wrapObject.emailId;
            stagingObject.Candidate_Phone__c=wrapObject.phone;
            insert stagingObject;
            
            Attachment att = new Attachment();
            att.ParentId = stagingObject.Id;
            att.Name =wrapObject.fileName;
            att.Body= EncodingUtil.base64Decode(wrapObject.base64CV);
            att.ContentType= String.valueOf(wrapObject.documentType);
            insert att;
            
            stagingObject.Status__c='Exception';
            update stagingObject;
            
            response.statusCode = 200;
        }
        Catch(JSONException e){
            System.debug('Exception::' + e.getMessage() + '::line Number::' + e.getLineNumber());
            ConnectedLog.LogException('ResumeFailureDetail', 'ATSResumeApplicationStatusAPI', 'doPost', e);
                response.statusCode = 400;
        }
        Catch(DMLException e){
            System.debug('Exception::' + e.getMessage() + '::line Number::' + e.getLineNumber());
            ConnectedLog.LogException('ResumeFailureDetail', 'ATSResumeApplicationStatusAPI', 'doPost', e);
            response.statusCode = 400;
        }
    }
     private class serviceWrapper{
        public String applicationVendor;
        public String applicationSource;
        public String base64CV;
        public String fileName;
        public String documentType;
        public String postingId;
        public String dateApplied;
        public String firstname;
        public String lastname;
        public String emailId;
        public String phone;
    }
}