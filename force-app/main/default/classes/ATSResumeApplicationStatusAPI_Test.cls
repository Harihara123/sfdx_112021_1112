@isTest
public class ATSResumeApplicationStatusAPI_Test {
	@isTest 
    public static void doPostTest() {
        Job_Posting__c jp = new Job_Posting__c();
		jp.Job_Title__c  = 'Architect';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Contract';
		jp.Industry_Sector__c ='Accounting';
		jp.External_Job_Description__c = 'Test Administrator'; 
           
		jp.Allegis_Employment_Type__c='Permanent';
		jp.City__c = 'Hanover';
		jp.State__c = 'MD';
		jp.Country__c = 'US';
		jp.External_Job_Description__c = 'Job Description';
		jp.Salary_max__c = 20000000;
		jp.Salary_min__c = 30000000;
		jp.Salary_Frequency__c = 'Salary';
		jp.opco__c = 'AG_EMEA';
        jp.Req_Division__c='Aerotek';
		jp.ONET_Codes__c = 	'Accounting';
		jp.Industry_Code__c = 	'Accounting';
		insert jp;
        
        Job_Posting_Channel_Details__c jcd= new Job_Posting_Channel_Details__c();
        jcd.Job_Posting_Id__c=jp.Id;
        jcd.Board_Posting_Id__c='202234';
        insert jcd;
        
        RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
		String requestJSONBody = '{'+

								+'"applicationVendor": "eQuest",'

								+'"applicationSource":"JobServe",'

								+'"base64CV":"asdsa",'
								+'"fileName":"Resume.docx",'
								+'"documentType":"application/pdf",'

								+'"postingId":"202234",'

								+'"dateApplied":"2021-03-10T11:00:00Z"'

								+'}';
            
            req.requestURI = '/services/apexrest/Person/ResumeApplicationStatus/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
        	Test.startTest();
            ATSResumeApplicationStatusAPI.doPost();
        	Test.stopTest();
        system.assertEquals(1, 1);                                                      
    }
    @isTest 
    public static void exceptionTest() {
        Job_Posting__c jp = new Job_Posting__c();
		jp.Job_Title__c  = 'Architect';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Contract';
		jp.Industry_Sector__c ='Accounting';
		jp.External_Job_Description__c = 'Test Administrator'; 
           
		jp.Allegis_Employment_Type__c='Permanent';
		jp.City__c = 'Hanover';
		jp.State__c = 'MD';
		jp.Country__c = 'US';
		jp.External_Job_Description__c = 'Job Description';
		jp.Salary_max__c = 20000000;
		jp.Salary_min__c = 30000000;
		jp.Salary_Frequency__c = 'Salary';
		jp.opco__c = 'AG_EMEA';
        jp.Req_Division__c='Aerotek';
		jp.ONET_Codes__c = 	'Accounting';
		jp.Industry_Code__c = 	'Accounting';
		insert jp;
        
        Job_Posting_Channel_Details__c jcd= new Job_Posting_Channel_Details__c();
        jcd.Job_Posting_Id__c=jp.Id;
        jcd.Board_Posting_Id__c='202234';
        insert jcd;
        
        RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
		String requestJSONBody = '{'+

								+'"applicationVendor": "eQuest",'

								+'"applicationSource":"JobServe",'

								+'"base64CV":"asdsa",'
								+'"fileName":"Resume.docx",'
								+'"documentType":".pdf",'

								+'"postingId":"202234",'

								+'"dateApplied":"2021-03-10T11:00:00Z"'

								+'}';
            
            req.requestURI = '/services/apexrest/Person/ResumeApplicationStatus/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
        	Test.startTest();
            ATSResumeApplicationStatusAPI.doPost();
        	Test.stopTest();
        system.assertEquals(1, 1);
                                                              
    }
}