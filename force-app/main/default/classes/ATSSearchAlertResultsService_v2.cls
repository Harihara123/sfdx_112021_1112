@RestResource(urlMapping='/alertresults/v1/*')
global without sharing class ATSSearchAlertResultsService_v2 {
    // static final String ATS_RESULTS_POSTING_SUCCESSFUL = 'Results successfully posted.';
    static final String ATS_RESULTS_POSTING_UNSUCCESSFUL = 'Error in result posting.';
    public class ATSApplicationAPIException extends Exception{}

    @HttpPost
	global Static void doPost(){
        String status;
		String errStr = null;
		
		RestResponse response = RestContext.response;
        
        try{
			String getStrParam  = RestContext.request.requestBody.toString();
            
            Boolean isReqAlert = getStrParam.contains('"jobId"');
            if(isReqAlert){
                ReqSearchAlertResultsModel searchResults = (ReqSearchAlertResultsModel) JSON.deserialize(getStrParam, ReqSearchAlertResultsModel.class);
                
                if (searchResults != null) {
                    processReqResults(searchResults);
                 }
                
            }else{
                SearchAlertResultsModel searchResults = (SearchAlertResultsModel) JSON.deserialize(getStrParam, SearchAlertResultsModel.class);
				if (searchResults != null) {
                     processTalentResults(searchResults); 
                }
            }
			
				
			
		}
		catch(Exception e){
            system.debug('Message::'+ e.getMessage() + ' stack trace::' + e.getStackTraceString());
            status = ATS_RESULTS_POSTING_UNSUCCESSFUL + ' - ' + e.getMessage() + '\n' + e.getStackTraceString();
            
            ConnectedLog.LogException('SearchAlertResults','ATSSearchAlertResultsService_v2', 'doPost', e);
        }

		status = 'Successful inserts/updates = \n\n' + status;
		RestContext.response.responseBody = Blob.valueOf(status); 
    }

    public static void processTalentResults(SearchAlertResultsModel searchResults){
      List<Search_Alert_Result__c> alertResults = new List<Search_Alert_Result__c>();
        
      for (SearchAlertResultsModel.SearchAlertResultModel resultEntry : searchResults.results) {
				  String key = resultEntry.alertId+'-'+resultEntry.candidateId+'-'+String.valueOf(Date.today());

					Search_Alert_Result__c result = new Search_Alert_Result__c();
					result.Alert_Criteria__c = resultEntry.alertId;
					result.Results__c = JSON.serialize(resultEntry);
					result.Date__c = Date.today();
				    result.Combination_Key__c=key;
				    result.Version__c='v2';
					alertResults.add(result);
			}

			if (alertResults.size() > 0) {
				Schema.SObjectField f = Search_Alert_Result__c.Fields.Combination_Key__c;
				Database.UpsertResult[] urList = Database.upsert(alertResults, f, false);			
			}
        
    }
    	

    public static void processReqResults(ReqSearchAlertResultsModel searchResults){
      List<Search_Alert_Result__c> alertResults = new List<Search_Alert_Result__c>();
        
      for (ReqSearchAlertResultsModel.ReqSearchAlertResultModel resultEntry : searchResults.results) {
          String key = resultEntry.alertId+'-'+resultEntry.jobId+'-'+String.valueOf(Date.today());

          Search_Alert_Result__c result = new Search_Alert_Result__c();
          result.Alert_Criteria__c = resultEntry.alertId;
          result.Results__c = JSON.serialize(resultEntry);
          result.Date__c = Date.today();
          result.Combination_Key__c=key;
          result.Version__c='v2';
          alertResults.add(result);
      }

      if (alertResults.size() > 0) {
        Schema.SObjectField f = Search_Alert_Result__c.Fields.Combination_Key__c;
        Database.UpsertResult[] urList = Database.upsert(alertResults, f, false);      
      }
  }

}