global class ATSSubmittal  {

    global String jobPostingId { get; set; }
    global String talentId { get; set; }
    global String externalSourceId { get; set; }
    global String jbSourceId { get; set; }
    global String inviterFullName { get; set; }
    global String vendorSegmentId { get; set; }
    global String vendorName { get; set; }
    global Boolean isApplicantSubmittal { get; set; }
    global String effectiveDate { get; set; }
    global String desiredRate { get; set; }
    global String desiredSalary { get; set; }
    global String appliedDate { get; set; }
    global String vendorApplicationId { get; set; }
    global String ecid { get; set; }
    global String icid { get; set; }
    global String feedSourceId { get; set; }

    global String vendorId { get; set; }
    global String inviterEmail { get; set; }
    global String ecvid { get; set; }
    global String acceptedTnc { get; set; }
    global String talentDocumentId { get; set; }
    global String transactionId {get; set; }
    global string integrationId {get; set; }
    global String transactionsource {get;set;}

}