@RestResource(urlMapping='/Person/SurvivorshipMatch/V1')
global without sharing class ATSSurvivorshipMatch {
   //Custom settings stored values of talent status like "'Current', 'Former', 'Placed'.
    Public static Map<String, Talent_Status__c> ts = Talent_Status__c.getAll();
    Public static Set<String> talentStatus = ts.keySet();
    //Custom settings stored values of Activities__c types like "Client Interview','Finish','G2','Internal Interview '..etc'.
    Public static Map<String, Survivorship_Activities__c> atype = Survivorship_Activities__c.getAll();
    Public static Set<String> activityType = atype.keySet();
    Public static set<String> opcoset1 = new set<String>{'MLA','AP'};
    Public static set<String> opcoset2 = new set<String>{'ONS','TEK','RWS'};
    
    Public static set<String> talentIDSet;
    Public static ATSSurvivorshipMatch.ResponseWrapper responseWrapObject;
    
    @HttpGet
    global static void getSurvivorshipTalents() 
    {   
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        Map<String, String> params = RestContext.request.params;
        String param1 = params.get('ID1');
        String param2 = params.get('ID2');
        List<Contact> talents = getContactDetails(param1,param2);
        Map<String, String> paramMap = new Map<String, String>();
        
        try{
            for (Contact c : talents) {
                if (c.Id == param1 || c.AccountId == param1) {
                    paramMap.put(c.Id, param1);
                } else if (c.Id == param2 || c.AccountId == param2) {
                    paramMap.put(c.Id, param2);
                }
            }
           Contact talent1 = talents[0];
           Contact talent2 = talents[1];
           response.responseBody = Blob.valueOf(preCheckRules(talent1,talent2,paramMap));
		}catch(Exception ex){
            responseWrapObject = new ATSSurvivorshipMatch.ResponseWrapper();
            responseWrapObject.msgCode ='Survivorship/UserCheck/RequiredIDFieldMissing';
            responseWrapObject.validationMsg ='MasterID/VictimID fields are missing';  
            response.responseBody = Blob.valueOf(jsonResponse(responseWrapObject, new Map<String,String>()));
        }
    }
    
    //This method fetching corresponding talent details.
    public static List<Contact> getContactDetails(string param1,string param2){
        String validationMsg = '';
        set<String> ids = new set<String>{param1,param2};
        List<Contact> obj = new List<Contact>();
        obj = [Select Id,accountId,Talent_Id__c,Talent_Ownership__c, Peoplesoft_ID__c,Candidate_Status__c,
               Account.G2_Last_Modified_Date__c,Account.Talent_Profile_Last_Modified_Date__c,
               LastModifiedDate,CreatedDate,
               (SELECT Id, Name FROM Users WHERE UserType='CspLitePortal')
               FROM Contact where Id IN:ids or accountId IN:ids ];
        return obj;
    }
    
    //This method checking survivorship rules 
    public static String preCheckRules(Contact talent1, Contact talent2, Map<String,String> paramMap)
    {  
       	String userTalent;
        responseWrapObject = new ATSSurvivorshipMatch.ResponseWrapper();
        talentIDSet = new set<String>();
        talentIDSet.add(talent1.id);
        talentIDSet.add(talent2.id);
       
      if(talent1.Talent_Ownership__c == 'INACTIVE'|| talent2.Talent_Ownership__c == 'INACTIVE'){
          responseWrapObject.validationMsg = 'Master/Duplicate Talent cannot be an INACTIVE Talent.';
          responseWrapObject.msgCode = 'Survivorship/TalentCheck/InactiveTalent';
       }else if(talent1.Users.size() > 0 && talent2.Users.size() > 0){//If both records have User, abandon the rules
		  responseWrapObject.validationMsg = 'Talents with user records cannot be merged';
          responseWrapObject.msgCode = 'Survivorship/TalentCheck/BothEntitiesLinkedWithUserRecord';
       }else if ((talent1.Users.size() > 0) || (talent2.Users.size() > 0) || 
                 (talent1.Users.size() == 0 && talent2.Users.size()== 0 )){
		  // If one of the talent has a user record excute the rule1,rule2,rule3,rule4 or both talents with out user	
          if(talent1.Users.size() > 0){
               userTalent = talent1.Id;
          }else if(talent2.Users.size() > 0){
               userTalent = talent2.Id;
          }
         //Rule 1: Ownership Conflict:MLA and AP ownerships can be merged with themselves,
         //Rest of the ownerships can be merged with themselves and MLA/AP cannot be merged
         // with rest of the ownerships and vice versa.
          if(!ATSSurvivorshipMatch.checkTalentOwnership(talent1, talent2)){ //Rule 2: PSID check
                ATSSurvivorshipMatch.checkPSId(talent1, talent2);
          }// talent ownership
     
          if(responseWrapObject.masterId == userTalent || (talent1.Users.size()<=0 && talent2.Users.size()<=0 
                                                             && String.isBlank(responseWrapObject.validationMsg))){
            responseWrapObject.validationMsg = 'Master and Victim talents successfully returned for the given pair';
            responseWrapObject.msgCode = '';
            responseWrapObject.success = 'true';   
          }else if(String.isBlank(responseWrapObject.validationMsg)){
            responseWrapObject.victimId = '';
            responseWrapObject.masterId = '';
            responseWrapObject.validationMsg ='Talent with user record cannot be the victim. Merge is not possible with given talents';
            responseWrapObject.msgCode = 'Survivorship/UserCheck/VictimTalentWithUserRecord';
          }
     	}//user check
     	return jsonResponse(responseWrapObject, paramMap); //generate the jason body
    }
    
    
    public static Boolean checkTalentOwnership(Contact talent1 ,Contact talent2){
        if(opcoset1.contains(talent1.Talent_Ownership__c.toUpperCase()) != opcoset1.contains(talent2.Talent_Ownership__c.toUpperCase()))
            {
              responseWrapObject.validationMsg = 'Survivor/Victim cannot be determined as the talentownerships conflict';
              responseWrapObject.msgCode = 'Survivorship/TalentCheck/TalentOwnerShipConflict';
              return true;
             }
        return false;
    }
    
    public static void checkPSId(Contact talent1 ,Contact talent2){
        if (talent1.Peoplesoft_ID__c != null && talent2.Peoplesoft_ID__c == null)
            {
  			   responseWrapObject.masterId = talent1.id;
               responseWrapObject.victimId = talent2.id; 
            }else if (talent1.Peoplesoft_ID__c == null && talent2.Peoplesoft_ID__c != null){
               responseWrapObject.victimId  = talent1.id;
               responseWrapObject.masterId = talent2.id;
            }else if(talent1.Peoplesoft_ID__c != talent2.Peoplesoft_ID__c){//If both talents have different PS IDs tied
              responseWrapObject.validationMsg ='Survivor/Victim cannot be determined as both the talents are tied to different psids';
              responseWrapObject.msgCode = 'Survivorship/PSIDCheck/BothEntitiesLinkedWithDiffPSID';
            }else if(talent1.Peoplesoft_ID__c == talent2.Peoplesoft_ID__c){ //If both talents have the same PS ID or have no PS IDs
             //If one of the Talent statuses is in (Current, Former, Placed).
               if(talentStatus.contains(talent1.Candidate_Status__c) && !talentStatus.contains(talent2.Candidate_Status__c))
                {
                  responseWrapObject.masterId = talent1.id;
                  responseWrapObject.victimId = talent2.id;
                }else if(!talentStatus.contains(talent1.Candidate_Status__c) && talentStatus.contains(talent2.Candidate_Status__c)){
                    responseWrapObject.victimId = talent1.id;
                    responseWrapObject.masterId = talent2.id;
                }else if((talent1.Candidate_Status__c == talent2.Candidate_Status__c)
                     ||(!talentStatus.contains(talent1.Candidate_Status__c)==(!talentStatus.contains(talent2.Candidate_Status__c)) )){ 
                	//If both the talent have the same status, 
                    //check to see if one of the Talent has a most recent G2_Last_Modified_Date__c.
                  	ATSSurvivorshipMatch.mostRecentG2Activity(talent1,talent2);
           		}//same status
          }
    }
    
    public static void mostRecentG2Activity(Contact talent1 ,Contact talent2){
        if(talent1.Account.G2_Last_Modified_Date__c > talent2.Account.G2_Last_Modified_Date__c){
            responseWrapObject.masterId = talent1.id;
            responseWrapObject.victimId = talent2.id;
        }else if(talent1.Account.G2_Last_Modified_Date__c < talent2.Account.G2_Last_Modified_Date__c){
            responseWrapObject.victimId = talent1.id;
            responseWrapObject.masterId = talent2.id;
        }else{
            //If both the talents have the same G2 last mod date, 
            //check to see if one of the talents has most recent Talent_Profile_Last_Modified_Date__c (Account Object).
            ATSSurvivorshipMatch.mostRecentLastModifiedDate(talent1,talent2);
         }//g2 last modified
    }
    
    public static void mostRecentLastModifiedDate(Contact talent1, Contact talent2){
        //If both the talents have the same G2 last mod date, 
            //check to see if one of the talents has most recent Talent_Profile_Last_Modified_Date__c (Account Object).
            if(talent1.Account.Talent_Profile_Last_Modified_Date__c > talent2.Account.Talent_Profile_Last_Modified_Date__c){
                responseWrapObject.masterId = talent1.id;
                responseWrapObject.victimId = talent2.id;
            }else if(talent1.Account.Talent_Profile_Last_Modified_Date__c < talent2.Account.Talent_Profile_Last_Modified_Date__c){
                responseWrapObject.victimId = talent1.id;
                responseWrapObject.masterId = talent2.id;
            }else{
                //If both talents have the same profile last mod date, 
                //check to see if one of the talents has an activity created in the last 180 days.
                ATSSurvivorshipMatch.mostRecentActivity(talent1,talent2);
            }//both same last modifidate
    }
    
    public static void mostRecentActivity(Contact talent1, Contact talent2){
        //If both talents have the same profile last mod date, 
                //check to see if one of the talents has an activity created in the last 180 days.
                List<Contact> contactList = [Select Id,
                                        (Select Id, IsTask,whoid,ActivityDate, ActivityType, CallType 
                                         From ActivityHistories where ActivityDate >= LAST_N_DAYS:180 
                                         AND ActivityType IN : activityType ORDER BY ActivityDate DESC LIMIT 1) 
                                        From Contact c where Id IN:talentIDSet];
                
                ActivityHistory contactActivity1 = null;
                if (contactList[0].ActivityHistories.size()>0){
                    contactActivity1 = contactList[0].ActivityHistories;
                }
                ActivityHistory contactActivity2 = null;
                if (contactList[1].ActivityHistories.size()>0){
                    contactActivity2 = contactList[1].ActivityHistories;
                }
                
                if(contactActivity1 !=null && contactActivity2 !=null){
                    if(contactActivity1.ActivityDate > contactActivity2.ActivityDate){
                        responseWrapObject.masterId = contactActivity1.whoid;
                        responseWrapObject.victimId = contactActivity2.whoid;
                    }else if(contactActivity1.ActivityDate < contactActivity2.ActivityDate){
                        responseWrapObject.victimId = contactActivity1.whoid;
                        responseWrapObject.masterId = contactActivity2.whoid;
                    }
                }else if((contactActivity1 == null && contactActivity2 == null) ||
                         (contactActivity1 == null || contactActivity2 == null) || 
                         (contactActivity1.ActivityDate == contactActivity2.ActivityDate)){
                             
                       ATSSurvivorshipMatch.mostRecentTalentDocument(talent1,talent2);
                }//activity.
    }
    
    public static void mostRecentTalentDocument(Contact talent1, Contact talent2){
        //If both talents do not have an activity in the last 180 days or have activity with the same date, 
        //check to see if one of the talents has a most recent resume created date.
        
        Map<String, Datetime> talentDocumentMap = new Map<String, Datetime>();
        
        List<Talent_Document__c> talentDocumentList =[SELECT LastModifiedDate,CreatedDate,Talent__c FROM Talent_Document__c  
                                                      where Talent__c IN: (new List<ID> {talent1.AccountId , talent2.AccountId}) 
                                                      order by CreatedDate desc];
        
        for(Talent_Document__c td : talentDocumentList){
            talentDocumentMap.put(td.Talent__c, td.CreatedDate);
        }
        
        if(talentDocumentMap.get(talent1.AccountId) > talentDocumentMap.get(talent2.AccountId)){
            responseWrapObject.masterId = talent1.id;
            responseWrapObject.victimId = talent2.id;
        }else if(talentDocumentMap.get(talent1.AccountId) < talentDocumentMap.get(talent2.AccountId)){
            responseWrapObject.victimId = talent1.id;
            responseWrapObject.masterId = talent2.id;                                    
        }else{
            //there is no resume
            if(talent1.CreatedDate < talent2.CreatedDate){
                responseWrapObject.masterId = talent1.id;
                responseWrapObject.victimId = talent2.id;
            }else if(talent1.CreatedDate > talent2.CreatedDate){
                responseWrapObject.victimId = talent1.id;
                responseWrapObject.masterId = talent2.id;  
            }else{
                responseWrapObject.validationMsg = 'Survivor/Victim cannot be determined with the available Talent details';
                responseWrapObject.msgCode = 'Survivorship/PSIDCheck/CannotDeterminedTalentDetails';
            }
        }//no resume
    }
    public static String jsonResponse(ATSSurvivorshipMatch.ResponseWrapper responseWrapObject, Map<String,String> paramMap){
        Map<String, String> responseData = new Map<String, String>();
        responseData.put('Message', responseWrapObject.validationMsg);
        responseData.put('MessageCode', responseWrapObject.msgCode);
        responseData.put('Success', responseWrapObject.success);
        if(paramMap.containsKey(responseWrapObject.victimId)){ 
        	responseData.put('VictimID', paramMap.get(responseWrapObject.victimId));
        }
        if(paramMap.containsKey(responseWrapObject.masterId)){ 
            responseData.put('MasterID', paramMap.get(responseWrapObject.masterId));
        } 
       
        return JSON.serialize(responseData);    
    }
    
    public class ResponseWrapper{
        public string victimId;
        public string masterId;
        public string validationMsg;
        public string msgCode;
        public string success;
        public ResponseWrapper(){
            this.victimId ='';
            this.masterId = '';
            this.validationMsg = '';
            this.msgCode ='';
            this.success='false';
        }
	}
    
 }