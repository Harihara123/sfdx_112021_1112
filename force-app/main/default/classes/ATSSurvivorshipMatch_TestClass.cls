@isTest
public class ATSSurvivorshipMatch_TestClass {
    
     private static String domainURL = '/services/apexrest/Person/SurvivorshipMatch/V1';
     private static String methodType = 'GET'; 
     private static RestRequest req = new RestRequest(); 
     private static RestResponse res = new RestResponse();
     static {
        req.httpMethod = methodType; 
        req.requestURI = domainURL;  
        }
   
    static testMethod void testGetOrderEventNoParam() {
         Set<String> customerUserTypes = new Set<String> {'CspLitePortal'};
         Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
         User thisUser = [SELECT Id,Peoplesoft_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
         long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
         Integer random = Math.Round(Math.Random() * timeDiff );
         Account talent = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'testAccount',
            //Talent_Preference_Internal__c = createPreferenceJSON(),
            Desired_Rate_Frequency__c = 'hourly',
            Talent_Ownership__c = 'ONS',
            Desired_Rate__c =200
         );
         insert talent;
        Account talent1 = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'testAccount2',
            //Talent_Preference_Internal__c = createPreferenceJSON(),
            Desired_Rate_Frequency__c = 'hourly',
            Talent_Ownership__c = 'ONS',
            Desired_Rate__c =200
         );
         insert talent1;
          Contact con = new contact();
          con.AccountId = talent.id;
          con.LastName = 'testContact1';
          insert con;
          Event event = new Event(
                            Whoid = con.id,
                            Whatid = talent.id, 
                            Subject = 'Test Event',
                            Type = 'Client Interview', 
                            startDateTime =  system.Now(),
                            EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today()
                            //DurationInMinutes  = 10
                             );
            insert event;
            
            Task task = new Task(
                                Whoid = con.id, 
                                Subject = 'Test G2',
                                Type = 'G2',
                                Status = 'Completed' ,
                                ActivityDate =  system.today() );
                 insert task;
                 Contact con1 = new Contact(LastName='testContact2', accountId=talent1.Id);
                  insert con1;
                 Event event1 = new Event(
                                Whoid = con1.id,
                                Whatid = talent1.id, 
                                Subject = 'Test Event',
                                Type = 'Client Interview', 
                                startDateTime =  system.Now(),
                                EndDateTime =   system.Now().addMinutes(10),
                                ActivityDate =  system.today()
                            //DurationInMinutes  = 10
                             );
               insert event1;
              Task task1 = new Task(
                            Whoid = con1.id, 
                            Subject = 'Test G2',
                            Type = 'G2',
                            Status = 'Completed' ,
                            ActivityDate =  system.today() );
               insert task1;
        
          UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];      
          User user1 ;
          System.runAs (thisUser) {
          user1= new User(
          Username = 'testusr'+random+'@allegisgroup.com.dev',
          ContactId = con.Id,
          ProfileId = p.Id,
          Alias = 'test123',
          Email = 'test12345@test.com',
          EmailEncodingKey = 'UTF-8',
          LastName = ' User'+random,
          CommunityNickname = 'test12345',
          TimeZoneSidKey = 'America/Los_Angeles',
          LocaleSidKey = 'en_US',
          LanguageLocaleKey = 'en_US',
          OPCO__c = 'AP',
          Region__c= 'Aerotek MidAtlantic',
          Region_Code__c = 'test',
                Office_Code__c = '10001',
                Peoplesoft_Id__c = talent.Peoplesoft_ID__c,
                User_Application__c = 'CRM',
          Talent_Preference__c = 'test preferences'
          );
          insert user1;      
          req.addParameter('ID1', con.Id);
		  req.addParameter('ID2', con1.Id);
          RestContext.request = req;
          RestContext.response = res;
        ATSSurvivorshipMatch atShipmatch = new ATSSurvivorshipMatch();
    	ATSSurvivorshipMatch.getSurvivorshipTalents();
          test.startTest();
          con.Peoplesoft_ID__c = '123456';
          update con;
          con1.Peoplesoft_ID__c = null;
          update con1;
          ATSSurvivorshipMatch.getSurvivorshipTalents();
         
          con.Peoplesoft_ID__c = null;
          update con;
          con1.Peoplesoft_ID__c = '123456';
          update con1;
          ATSSurvivorshipMatch.getSurvivorshipTalents();
          con.Peoplesoft_ID__c = '123456';
          update con;
          con1.Peoplesoft_ID__c = '123457';
          update con1;
          ATSSurvivorshipMatch.getSurvivorshipTalents();
          talent.Candidate_Status__c ='Current';
          update talent;
          con.Peoplesoft_ID__c = '123457';
          update con;
          talent1.Candidate_Status__c ='Current';
          update talent1;
          con1.Peoplesoft_ID__c = '123457';
          update con1;
          ATSSurvivorshipMatch.getSurvivorshipTalents();
          List<Account> acc = new List<Account>();
          talent.G2_Last_Modified_Date__c = system.Datetime.now();
          talent.Talent_Profile_Last_Modified_Date__c = system.Datetime.now();
          talent1.G2_Last_Modified_Date__c = system.Datetime.now();
          talent1.Talent_Profile_Last_Modified_Date__c = system.Datetime.now();
          acc.add(talent);
          acc.add(talent1);
          update acc;
           ATSSurvivorshipMatch.getSurvivorshipTalents();
          acc.clear();
          talent.G2_Last_Modified_Date__c = system.Datetime.now()+1;
          talent.Talent_Profile_Last_Modified_Date__c = system.Datetime.now()+1;
          talent1.G2_Last_Modified_Date__c = system.Datetime.now();
          talent1.Talent_Profile_Last_Modified_Date__c = system.Datetime.now();
          acc.add(talent);
          acc.add(talent1);
          update acc;
           ATSSurvivorshipMatch.getSurvivorshipTalents();
          
        ATSSurvivorshipMatch.getSurvivorshipTalents();
              talent.Talent_Ownership__c = 'MLA';
              talent1.Talent_Ownership__c = 'AP' ;
              talent.Candidate_Status__c = 'Current';
              talent1.Candidate_Status__c = 'XYZ';
              Update talent;
              Update talent1;
               ATSSurvivorshipMatch.getSurvivorshipTalents();
              talent.Talent_Ownership__c = 'MLA';
              talent1.Talent_Ownership__c = 'RWS';
              update talent;
              update talent1;
              ATSSurvivorshipMatch.getSurvivorshipTalents();
                                         
            Talent_Document__c  td1 = new Talent_Document__c( Talent__c = talent.Id, Document_Name__c = 'test.doc',Document_Type__c = 'Resume', Default_Document__c = true, Internal_Document__c = true,HTML_Version_Available__c=True,mark_for_deletion__c=false,Committed_Document__c = true);

             insert td1;
               talent.Talent_Ownership__c = 'MLA';
              talent.Peoplesoft_ID__c = '12345';
              talent.Candidate_Status__c = 'Abc' ;  
               talent.G2_Last_Modified_Date__c = null;
              talent.Talent_Profile_Last_Modified_Date__c = null;
              
              talent1.Talent_Ownership__c = 'AP'; 
              talent1.Peoplesoft_ID__c = '12345';
              talent1.Candidate_Status__c = 'Abc' ; 
              talent1.G2_Last_Modified_Date__c = null;
              talent1.Talent_Profile_Last_Modified_Date__c = null;
               Talent_Document__c td = new  Talent_Document__c(Document_Name__c ='test1.doc',Document_Type__c = 'Resume',Default_Document__c = true,Internal_Document__c = true,Talent__c = talent1.id );
              insert td;
              Update talent1;
              Update talent;
              con.AccountId = talent.id;
              con1.AccountId = talent1.id;
              update con;
              update con1;
                ATSSurvivorshipMatch.getSurvivorshipTalents();
              talent.Talent_Profile_Last_Modified_Date__c = System.Now();
              Update talent;
               ATSSurvivorshipMatch.getSurvivorshipTalents();
              talent1.Talent_Profile_Last_Modified_Date__c = System.Now();
              Update talent1;
                ATSSurvivorshipMatch.getSurvivorshipTalents();
      		req.addParameter('ID2', null);
            ATSSurvivorshipMatch.getSurvivorshipTalents();  
          test.stopTest();
     
        	
    }
   }
    
    
    
}