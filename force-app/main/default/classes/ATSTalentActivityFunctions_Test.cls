@isTest
private class ATSTalentActivityFunctions_Test {
  
   @isTest static void test_performServerCall() {
		//No Params Entered....
		Object r = ATSTalentActivityFunctions.performServerCall('',null);
		System.assertEquals(r,null);
	}
	
	@isTest//(seeAllData=true) 
    static void test_method_getTalentActivitiesTasks() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		DateTime now = DateTime.now();
        Date datenow = Date.newInstance(now.year(),now.month(),now.day());
		List<Task> newTasks = new List<Task> ();
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		p.put('maxItems', '30');
		
        Test.startTest();
		
        List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
		Task newTask1 = (Task)t1[0];
		newTask1.Subject='Test Task';
        newTasks.add(newTask1);

        List<String> types = new List<String>{'Task','Attempted Contact','BizDev  Call'
												,'BD Call','Maintenance Call','Tickler Call','Correspondence'
												,'Email','To Do'};
                                                    
        for(String typ : types){
            Task newTask = new Task();
            newTask.OwnerId = UserInfo.getUserId();
            newTask.WhoId = newCon.Id;
            newTask.ActivityDate =  datenow;
            newTask.Subject='Test Task';
            newTask.Status = 'Completed';
            newTask.ActivityDate = date.Today().addDays(10);
            newTask.Type=typ;
            newTasks.add(newTask);
            
        }

        Database.insert(newTasks);
        
        
        Map<String,Object> p2 = new Map<String,Object>();
		p2.put('recordId', newCon.Id);
		p2.put('maxItems', '30');
		
		List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p2);
		Test.stopTest();
        //System.assertNotEquals(0, r.size());
	}

	@isTest//(seeAllData=true) 
    static void test_method_getTalentActivitiesBlank() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);		
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		p.put('maxItems', '5');
		Test.startTest();
		List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p);
		Test.stopTest();
        System.assertEquals(0, r.size());
	}
    
    @isTest
    static void test_method_getTalentActivitiesBlank_Filter() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		p.put('maxItems', '30');
        p.put('filterCriteria', 'test,filter,text,IsTaskOrEvent:');
        
		Test.startTest();
		List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
		Task newTask = (Task)t1[0];
		newTask.Subject='Test Task';
		Database.insert(newTask);

		List<Object> t2 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
			Task newTask2 = (Task)t2[0];
			newTask2.Subject='Test Task';
			newTask2.Status = 'Completed';
			newTask2.ActivityDate = date.Today().addDays(-1);
			newTask2.Type='Task';
			Database.insert(newTask2);
        
		List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p);
		Test.stopTest();
	}
    
    @isTest
    static void test_method_getTalentActivitiesBlank_Filter_NoTaskOrEvent() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		p.put('maxItems', '30');
        p.put('filterCriteria', 'test,filter,text');
        
		Test.startTest();
		List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
		Task newTask = (Task)t1[0];
		newTask.Subject='Test Task';
		Database.insert(newTask);

		List<Object> t2 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
			Task newTask2 = (Task)t2[0];
			newTask2.Subject='Test Task';
			newTask2.Status = 'Completed';
			newTask2.ActivityDate = date.Today().addDays(-1);
			newTask2.Type='Task';
			Database.insert(newTask2);
        
		List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p);
		Test.stopTest();
	}
    
    @isTest
    static void test_method_updateTask_Simple() {
	    Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		p.put('maxItems', '30');
        p.put('sc', 'Completed');
        p.put('des', 'test_method_updateTask_Simple');
		p.put('contactId', newCon.Id);
		Test.startTest();
		List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
		List<Object> t12 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewContactTask',p);
		Task newTask = (Task)t1[0];
		newTask.Subject='Test Task';
		Database.insert(newTask);
	    
        p.put('recId', newTask.Id);
        
		String result = (String)ATSTalentActivityFunctions.performServerCall('saveTask',p);
		Test.stopTest();
        System.assertEquals(result, 'Updated');
	}
	@isTest
    static void test_method_updateEvent_Simple() {
        
        Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
        p.put('maxItems', '30');
		p.put('post', 'test_method_updateEvent_Simple');
        
           Test.startTest();
			List<Object> e1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);
			Event newEvent = (Event)e1[0];
			newEvent.StartDateTime = DateTime.newInstance(2017,1,1,0,0,0);
			newEvent.EndDateTime = DateTime.newInstance(2017,1,2,0,0,0);
			newEvent.Subject = 'Test Event';
			newEvent.Pre_Meeting_Notes__c = 'test';
			newEvent.Post_Meeting_Notes__c = 'test';
			Database.insert(newEvent);
        Test.stopTest();
        p.put('recId', newEvent.Id);
        
        String result = (String)ATSTalentActivityFunctions.performServerCall('saveEvent',p);
        System.assertEquals(result, 'Updated');
    }
    @isTest
    static void test_method_getShortDate_Today(){
        String result = 'No Due Date';
        result = ATSTalentActivityFunctions.getShortDate(Date.today());
        System.assertEquals('Today', result);
    }
    @isTest
    static void test_method_getShortDate_PreviousDay(){
        String result = 'No Due Date';
        result = ATSTalentActivityFunctions.getShortDate(date.Today().addDays(-1));
        System.assertEquals('Yesterday', result);
    }
    @isTest
    static void test_method_getShortDate_FutureDay(){
        String result = 'No Due Date';
        result = ATSTalentActivityFunctions.getShortDate(date.Today().addDays(2));
        System.assert(result != 'Today' && result != 'Yesterday');
    }
    @isTest
    static void test_method_getIconNameActivity(){
        List<String> taskActivities = new List<String>{'Attempted Contact','BizDev  Call','BD Call','Maintenance Call',
            'Call','Correspondence','Email','To Do','allOther'};
        List<String> meetingActviities = new List<String>{'Meeting','Breakfast','Lunch','Dinner','Meal','Out of Office',
            'Networking','Contractor Meeting','Other','allOther'};
        Map<String,List<String>> inputMap = new Map<String,List<String>>();
        inputMap.put('Task', taskActivities);
        inputMap.put('NonTask', meetingActviities);
        String result = 'valid result';
        for(String key : inputMap.keySet()){
            List<String> valueList = inputMap.get(key);
            for(String inputString : valueList){
                result = ATSTalentActivityFunctions.getIconNameActivity(key, inputString);
                if(result == ''){
                    break;
                }
            }
            if(result == ''){
                    break;
                }
        }
        System.assert(result != '');
    }
	@isTest//(seeAllData=true) 
    static void test_method_getTalentActivitiesEvents() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		p.put('maxItems', '30');
		p.put('contactId', newCon.Id);
		Test.startTest();
			List<Object> e1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);
			List<Object> e12 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewContactEvent',p);
			Event newEvent = (Event)e1[0];
			newEvent.StartDateTime = DateTime.newInstance(2017,1,1,0,0,0);
			newEvent.EndDateTime = DateTime.newInstance(2017,1,2,0,0,0);
			newEvent.Subject = 'Test Event';
			newEvent.Pre_Meeting_Notes__c = 'test';
			newEvent.Post_Meeting_Notes__c = 'test';
			Database.insert(newEvent);
			
	
			List<String> types = new List<String>{'Meeting','Breakfast','Lunch'
									,'Dinner','Meal','Out of Office'
									,'Networking','Contractor Meeting','Other','Event'};
	
			List<Event> evtList = new List<Event>();
			for(String typ : types){
				List<Object> e2 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);
				Event newEvent2 = (Event)e2[0];
				newEvent2.Subject = 'Test Event';
				newEvent2.Pre_Meeting_Notes__c = 'test';
				newEvent2.Post_Meeting_Notes__c = 'test';
				newEvent2.Type = typ;
				evtList.add(newEvent2);
			}
			Database.insert(evtList);
			
			List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p);
		Test.stopTest();
        //System.assertNotEquals(0, r.size());
	}
	@isTest
    static void test_method_getTalentActivitiesEvents_Contact()
    {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		p.put('maxItems', '30');
		
		Test.startTest();
			List<Object> e1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);
			Event newEvent = (Event)e1[0];
			newEvent.StartDateTime = DateTime.newInstance(2017,1,1,0,0,0);
			newEvent.EndDateTime = DateTime.newInstance(2017,1,2,0,0,0);
			newEvent.Subject = 'Test Event';
			newEvent.Pre_Meeting_Notes__c = 'test';
			newEvent.Post_Meeting_Notes__c = 'test';
			Database.insert(newEvent);
			
	
			List<String> types = new List<String>{'Meeting','Breakfast','Lunch'
									,'Dinner','Meal','Out of Office'
									,'Networking','Contractor Meeting','Other','Event'};
	
			List<Event> evtList = new List<Event>();
        	Id tempContId = newCon.Id;
			for(String typ : types){
				List<Object> e2 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);
				Event newEvent2 = (Event)e2[0];
				newEvent2.Subject = 'Test Event';
                tempContId = newEvent2.WhoId;
				newEvent2.Pre_Meeting_Notes__c = 'test';
				newEvent2.Post_Meeting_Notes__c = 'test';
				newEvent2.Type = typ;
				evtList.add(newEvent2);
			}
			Database.insert(evtList);
			p.put('recordId', tempContId);
			List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p);
		Test.stopTest();
        //System.assertNotEquals(0, r.size());
	}
    
	@isTest static void test_method_getNewAccountTask() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		Test.startTest();
		List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
		Test.stopTest();
		Boolean result = t1[0] instanceof Task;
		System.assertEquals(true, result);
		System.assertEquals(BaseController.getCurrentUser(), t1[1]);
	}

	@isTest static void test_method_putAccountTask() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		Test.startTest();
		List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
		Task newTask = (Task)t1[0];
		newTask.Subject='Test Task';

		p.clear();
		p.put('newTask', JSON.serialize(newTask));
		
		String r = (String)ATSTalentActivityFunctions.performServerCall('putAccountTask',p);
        Test.stopTest();
		System.assertEquals('Inserted', r);
	}

	@isTest static void test_method_putAccountEvent() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		Test.startTest();
		List<Object> e1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);
		Event newEvent = (Event)e1[0];
		newEvent.StartDateTime = DateTime.newInstance(2017,1,1,0,0,0);
		newEvent.EndDateTime = DateTime.newInstance(2017,1,2,0,0,0);
		newEvent.Subject = 'Test Event';
		newEvent.Pre_Meeting_Notes__c = 'test';
		newEvent.Post_Meeting_Notes__c = 'test';

		p.clear();
		p.put('newEvent', JSON.serialize(newEvent));
		
		String r = (String)ATSTalentActivityFunctions.performServerCall('putAccountEvent',p);
		Test.stopTest();
        System.assertEquals('Inserted', r);
	}

	@isTest static void test_method_getNewAccountEvent() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);

		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);

		List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);

		Boolean result = t1[0] instanceof Event;
		System.assertEquals(true, result);
		System.assertEquals(BaseController.getCurrentUser(), t1[1]);	
	}

	@isTest static void test_method_getAccountTask() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		Test.startTest();
		List<Object> t1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountTask',p);
		Task newTask = (Task)t1[0];
		newTask.Subject='Test Task';

		Database.insert(newTask);

		p.clear();
		p.put('itemID', newTask.Id);
		
		List<Object> r = (List<Object>)ATSTalentActivityFunctions.performServerCall('getAccountTask',p);
        Test.stopTest();
		Task theTask = (Task)r[0];
		System.assertEquals(newTask.Id, theTask.Id);	
	}

	@isTest static void test_method_getAccountEvent() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		Test.startTest();
		List<Object> e1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent',p);
		Event newEvent = (Event)e1[0];

		newEvent.StartDateTime = DateTime.newInstance(2017,1,1,0,0,0);
		newEvent.EndDateTime = DateTime.newInstance(2017,1,2,0,0,0);
		newEvent.Subject = 'Test Event';
		newEvent.Pre_Meeting_Notes__c = 'test';
		newEvent.Post_Meeting_Notes__c = 'test';
		newEvent.WhatId = newAcc.Id;
		Database.insert(newEvent);

		p.clear();
		p.put('itemID', newEvent.Id);
		
		List<Object> r = (List<Object>)ATSTalentActivityFunctions.performServerCall('getAccountEvent',p);
        Test.stopTest();
		Event theEvent = (Event)r[0];
		System.assertEquals(newEvent.Id, theEvent.Id);	
	}
    static testmethod void test_getTaskStatusList(){
       	Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id); 
        Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		Test.startTest();
		 Map<String, String> statusMappings = (Map<String, String>)ATSTalentActivityFunctions.performServerCall('getTaskStatusList',p);
        Test.stopTest();
    }
    static testmethod void test_getTalentActivities() {
	    Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);		
		Task newTask = new Task();
		newTask.Subject='Test Task';
        newTask.WhoId=newCon.Id;
        newTask.Type='Internal Interview';
        Insert newTask;
        Event evt = new Event ();
        evt.whoId =newCon.Id;
        evt.Description = 'Test description';
        evt.DurationInMinutes = 5;
        evt.ActivityDateTime = system.now();
        evt.Type='Internal Interview';
        Insert evt;
        
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newCon.Id);
		p.put('maxItems', '5');
		Test.startTest();
		List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p);
		Test.stopTest();
        System.assertEquals(0, r.size());
	}
	@isTest(seeAllData=true)
    static void test_getTalentActivitiesNew() {
	    Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);   
		
		
		List<Task> tskLst=new List<Task>();		
		Task newTask = new Task();
		newTask.Subject='Meeting';		
        newTask.WhoId=newCon.Id; 
        newTask.WhatId=newAcc.Id;		
        newTask.status='Not Started';
        newTask.Type='Meeting';
		newTask.TaskSubtype='Task';
		newTask.Target_Account_Activity__c='Non-Target';		
		newTask.Completed_Flag__c=false;
		newTask.Activity_Type__c='Meeting';			
		newTask.ActivityDate=date.today();
		tskLst.add(newTask);

		Task CompletedTask = new Task();
		CompletedTask.Subject='Meeting';		
        CompletedTask.WhoId=newCon.Id; 
        CompletedTask.WhatId=newAcc.Id;		
        CompletedTask.status='Completed';
        CompletedTask.Type='Meeting';
		CompletedTask.TaskSubtype='Task';
		CompletedTask.Target_Account_Activity__c='Non-Target';		
		CompletedTask.Completed_Flag__c=false;
		CompletedTask.Activity_Type__c='Meeting';			
		CompletedTask.ActivityDate=date.today();			
        tskLst.add(CompletedTask);
		Insert tskLst;

		newCon.Source_System_Id__c='R.';
		update newCon;	
        
        Event evt = new Event ();
        evt.whoId =newCon.Id;
        //evt.AccountId=newAcc.Id;
        evt.Description = 'Test description';
        evt.DurationInMinutes = 5;
        evt.ActivityDateTime = system.now();
        evt.Type='Internal Interview';
        Insert evt;
        
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newCon.Id);
		p.put('maxItems', '20');
		p.put('filterCriteria', 'Meeting,Internal Interview,IsTaskOrEvent:');	    

		Test.startTest();
        List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentActivityFunctions.performServerCall('getTalentActivities',p);
		Test.stopTest();        
	}
    static testmethod void testErrorUserMethod(){
		Test.startTest();
		ATSTalentActivityFunctions.saveErrorLog('error','error','error','excp');		
		Test.stopTest();
    }
	@isTest(seeAllData=true)
    static void test_getActivitiesForInsights() {
	    Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		
		List<Task> tskLst=new List<Task>();		
		Task newTask = new Task();
		newTask.Subject='G2';		
        newTask.WhoId=newCon.Id; 
        newTask.WhatId=newAcc.Id;		
        newTask.status='Completed';
        newTask.Type='G2';
		newTask.Target_Account_Activity__c='Non-Target';		
		newTask.Activity_Type__c='G2';			
		newTask.ActivityDate=date.today();
		tskLst.add(newTask);		
		Insert tskLst;

		newCon.Source_System_Id__c='R.';
		update newCon;

		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newCon.Id);
		p.put('maxItems', '20');
		p.put('filterCriteria', 'G2,IsTaskOrEvent:');		     
        
		Test.startTest();
        Object obj = ATSTalentActivityFunctions.performServerCall('getActivitiesForInsights',p);
		Test.stopTest();        
	}
}