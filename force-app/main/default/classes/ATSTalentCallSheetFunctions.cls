public with sharing  class ATSTalentCallSheetFunctions {
    @AuraEnabled
    public static void saveCandidateToCallSheet(String recordIdCSV,String tasks){
    	
    	
    	try{
	        recordIdCSV = recordIdCSV.removeEnd(',');
	        List<Task> newTasks = new List<Task>();
	        
	        string[] recordArray = recordIdCSV.split(',');
	        List<Contact> recordList = [select Id, AccountId, Email, Other_Email__c, Preferred_Email__c, HomePhone, MobilePhone, Phone from Contact where AccountId in :recordArray];
	        Task tsk=null;
	        if(String.isNotBlank(tasks)){
				  tsk = (Task)JSON.deserialize(tasks,Task.class);
	        }
	        
	        //Default Values
	        string currentUser = UserInfo.getUserId();
	        
	        Date dueDate = Date.today();
	        string comments = '';
	        
	        for (Contact record : recordList) {
	            Task newTask = new Task();
	            newTask.OwnerId = currentUser;
	            newTask.Description = comments;
	            newTask.CreatedById = currentUser;
	            newTask.ActivityDate = tsk.ActivityDate;
	            newTask.WhoId = record.Id;
	            newTask.Priority = tsk.priority;
	            newTask.WhatId = record.AccountId;
	            newTask.Status = tsk.status;
	            newTask.Subject = tsk.subject;
                newTask.TransactionID__c = tsk.TransactionID__c;
                newTask.RequestID__c = tsk.RequestID__c;
	            newTask.type=tsk.type;
	            newTasks.add(newTask);
	        } 

	       	insert newTasks;
	     	newTasks=null;  	
    	}catch(Exception e){
    		
    		 throw new AuraHandledException('Were sorry, Please Contact System Administrator: ' + e.getMessage() + ''+ e.getCause());
       
                System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
                System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
                System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
                System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
                System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
    		
    	}
       // System.debug('Task id is ####--->'+newTasks.get(0).id);
        
    }
    
    
    
    @AuraEnabled
    public static CallSheet initialize(){
    	
    	CallSheet model=new CallSheet();
    	
    	
    	Map<string,string> typeMap = new Map<string,string>();
        Schema.DescribeFieldResult field2 = Task.Type.getDescribe();
    	 List<Schema.PicklistEntry> typeList = field2.getPicklistValues();
        for (Schema.PicklistEntry types: typeList) {
            typeMap.put(types.getValue(), types.getLabel());
        }
        model.typeMapping = typeMap;
        
        Map<string,string> statusMap = new Map<string,string>();
        Schema.DescribeFieldResult field3 = Task.Status.getDescribe();
    	List<Schema.PicklistEntry> statusList = field3.getPicklistValues();
        for (Schema.PicklistEntry status: statusList) {
            statusMap.put(status.getValue(), status.getLabel());
        }
        model.statusMapping = statusMap;
        
        
        Map<string,string> priorityMap = new Map<string,string>();
        Schema.DescribeFieldResult field4 = Task.priority.getDescribe();
    	List<Schema.PicklistEntry> priorityList = field4.getPicklistValues();
        for (Schema.PicklistEntry priority: priorityList) {
            priorityMap.put(priority.getValue(), priority.getLabel());
        }
        model.priorityMapping = priorityMap;
	    
        model.taskObj=new Task();
        model.taskObj.ActivityDate=Date.Today();
        model.taskObj.type='Call';
        model.taskObj.Status='Not Started';
        model.taskObj.Priority='Normal';
        
    	return model;
    }

	@AuraEnabled
	public static List<Contact> getContactList(List<Id> accountIds) {
		List<Contact> contactIds = new List<Contact>();
		try {
			for(Contact con:[SELECT Id FROM Contact WHERE AccountId in :accountIds]) {
				contactIds.add(con);
			}
		} catch(Exception e){
    		
    		 throw new AuraHandledException('Were sorry, Please Contact System Administrator: ' + e.getMessage() + ''+ e.getCause());
       
                System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
                System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
                System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
                System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
                System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
    		
    	}
		return contactIds;
	}

	@AuraEnabled
	public static User getUserDetails() {
		User userDetails = new User();
		userDetails = (User)BaseController.performServerCall('', '', 'getCurrentUser', null);
		return userDetails;
	}
    
    public class CallSheet{
    	@AuraEnabled
    	public Task taskObj{get;set;}
    	@AuraEnabled
    	public Map<String, String> typeMapping {get;set;}
    	@AuraEnabled 
    	public Map<String, String> statusMapping{get;set;}
    	@AuraEnabled
    	public Map<String, String> priorityMapping{get;set;}    	
    	
    }
}