@isTest
public class ATSTalentCallSheetFunctionsTest {
	
	@isTest
	static void testInitialize() {
		Test.startTest();
		ATSTalentCallSheetFunctions.CallSheet results = ATSTalentCallSheetFunctions.initialize();
		Test.stopTest();
		System.assert(results.taskObj != null, 'taskObj is not empty');
		System.assert(results.taskObj.ActivityDate == Date.today(), 'default date should be today');
		System.assert(results.taskObj.Type == 'Call', 'default type should be Call');
		System.assert(results.taskObj.Status == 'Not Started', 'default status should be Not Started');
		System.assert(results.taskObj.Priority == 'Normal', 'default priority should be Normal');
	}

	@isTest
	static void testSaveCandidateToCallSheet() {
		Account talentAccount = CreateTalentTestData.createTalentAccount();
		Contact talentContact = CreateTalentTestData.createTalentContact(talentAccount);
		Date newActivityDate = Date.today().addDays(7);
		//comma removal is accounted for in function
		String testRecordIdCSV = talentAccount.Id + ',';

		//JSON
		Task updatedTask = createTask();
		//this value should be ignored, dueDate is set in the method
		updatedTask.activityDate = newActivityDate;
		String testTasks = JSON.serialize(createTask());

		Test.startTest();
		ATSTalentCallSheetFunctions.saveCandidateToCallSheet(testRecordIdCSV, testTasks);
		Test.stopTest();
		//should only be one task
		Task results = [SELECT Id, ActivityDate FROM Task LIMIT 1];
		System.assert(results.activityDate == Date.today(), 'new activity date should not have been saved, method sets duedate');
	}	

	public static Task createTask() {
		Task newTask = new Task(
			ActivityDate = Date.today()
			, Type = 'Call'
			, STatus = 'Not Started'
			, Priority = 'Normal'
            , TransactionID__c = 'b0db184b-b713-6d15-c683-2926edff2dc7'
            , RequestID__c = '7655b019-cd0d-aa6f-a7e7-efb13a1bc3c0'
			);
		return newTask;
	}
}