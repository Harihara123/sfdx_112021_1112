public with sharing  class ATSTalentCandidateListFunctions {

	public static Object performServerCall(String methodName, Map<String, Object> parameters){
		Object result = null;
		Map<String, Object> p = parameters;

		//Call the method within this class
		if(methodName == 'getCandidateLists'){
			result = getCandidateLists((String)p.get('recordId'));
		}else if(methodName == 'saveCandidateLists'){
            result = saveCandidateLists((String)p.get('recordId'),
                                        (Map<Object, Object>)p.get('clist'));
		}
		
		return result;
	}


	private static List<Contact_Tag__c> getCandidateLists(String accountId) {
		Contact c = [SELECT Id FROM Contact WHERE AccountId =: accountId LIMIT 1];
		List<Contact_Tag__c> mcList = new List<Contact_Tag__c>();

		String contactId = (String)c.Id;

		if(contactId != ''){
			mcList = [SELECT id,Contact__c, Tag_Definition__r.Tag_Name__c FROM Contact_Tag__c WHERE Contact__c =: contactId AND CreatedbyId =:UserInfo.getUserID() Order by CreatedDate ];
        }

		return mcList;
	}
    
    private static List<Tag_Definition__c> saveCandidateLists(String accountId,Map<Object, Object> cMap) {
        System.debug(cMap);
        List<Tag_Definition__c> TagList = new List<Tag_Definition__c>();
        List<Tag_Definition__c> TagListNew = new List<Tag_Definition__c>();
        List<Contact_Tag__c> ConTagList = new List<Contact_Tag__c>();
        Contact c = [SELECT Id FROM Contact WHERE AccountId =: accountId LIMIT 1];
        
        for(Object cl : cMap.keySet()){
            Tag_Definition__c Td = new Tag_Definition__c();
            String cMapVal = String.valueOf(cMap.get(cl));
            if(String.valueOf(cl).equalsIgnoreCase(cMapVal)){
                Td.Tag_Name__c = cMapVal;
                TaglistNew.add(Td);
            }else{
                Td.id = Id.valueOf(String.valueOf(cl));
                Td.Tag_Name__c = cMapVal;
                TagList.add(Td);
            }
           }
        
        insert TagListNew;
        TagList.addAll(TagListNew);
       
        for(Tag_Definition__c Td : TagList){
            Contact_Tag__c Ctag = new Contact_Tag__c();
            Ctag.Contact__c = c.Id;
            Ctag.Tag_Definition__c  = Td.Id;
            ConTagList.add(CTag);
        }
        insert ConTagList;
		

		return TagList;
	}
}