@isTest
public class ATSTalentCandidateListFunctions_Test {


	@isTest static void test_performServerCall() {
		//No Params Entered
		Object r = ATSTalentCandidateListFunctions.performServerCall('',null);
		System.assertEquals(r,null);
	}

	@isTest static void test_method_getCandidateLists(){
		Account newAcc = BaseController_Test.createTalentAccount('');
        Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);

		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);

		List<Contact_Tag__c> r = (List<Contact_Tag__c>)ATSTalentCandidateListFunctions.performServerCall('getCandidateLists',p);
	
	}
    
    @isTest static void test_method_saveCandidateLists(){
		Account newAcc = BaseController_Test.createTalentAccount('');
        Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
        
        
        
        Tag_Definition__c TagDef = new Tag_Definition__c (Tag_Name__c = 'TagTest');
        insert TagDef;

		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
        Map<Object, Object> cMap = new Map<Object, Object> ();
        cMap.put('test','test');
        cMap.put(TagDef.Id,TagDef.Name);
        p.put('clist', cMap);

		List<Tag_Definition__c> r = (List<Tag_Definition__c>)ATSTalentCandidateListFunctions.performServerCall('saveCandidateLists',p);
	
		//System.assertEquals(cert.Id, r[0].RecordId);List<Tag_Definition__c> saveCandidateLists(String accountId,Map<Object, Object> cMap) {
	}
}