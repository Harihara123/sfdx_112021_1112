public with sharing class ATSTalentCertificationFunctions {
    public static Object performServerCall(String methodName, Map<String, Object> parameters){
        Object result = null;
        Map<String, Object> p = parameters;

        //Call the method within this class
        if(methodName == 'getCertifications'){
            result = getCertifications((String)p.get('recordId'), (String)p.get('maxRows'), (String)p.get('personaIndicator'));
        }
        
        return result;
    }

    private static List<BaseTimelineModel> getCertifications(String recordId, String maxRows, String personaIndicator)
    {

        Integer iMax = Integer.valueof(maxRows);
        Integer valueQuery = (iMax >= 5000) ? 5000 : iMax;

        List<BaseTimelineModel> returnList = new List<BaseTimelineModel>();

		List<Talent_Experience__c> talentExperienceList = new List<Talent_Experience__c>();
        Integer total = 0;
        boolean eval = false;
		//S-111810:Removed TalentMergeIndicator usage by Siva 1/10/2019
        //Boolean mergeIndicator = String.valueOf(Label.TalentMergeIndicator) == 'True'?true:false;
        //if(mergeIndicator){
            total = [SELECT Count() from talent_experience__c where type__c = 'Training' and talent__r.Id =:recordId and personaIndicator__c =:personaIndicator ];
        /*}else{
            total = [SELECT Count() from talent_experience__c where type__c = 'Training' and talent__r.Id =:recordId];
        }*/
        if(total > 0){
			//S-111810:Removed TalentMergeIndicator usage by Siva 1/10/2019
            //if(mergeIndicator){
                talentExperienceList = [select id, graduation_year__c, notes__c, certification__c 
                     from talent_experience__c 
                     where type__c = 'Training' and talent__r.Id =:recordId 
                     and personaIndicator__c =:personaIndicator
                     ORDER BY graduation_year__c DESC  LIMIT :valueQuery];
            /*}else{
                talentExperienceList = [select id, graduation_year__c, notes__c, certification__c 
                     from talent_experience__c 
                     where type__c = 'Training' and talent__r.Id =:recordId 
                     ORDER BY graduation_year__c DESC  LIMIT :valueQuery];
            }*/
            if(talentExperienceList.size() > 0)
            {
                for(Talent_Experience__c t: talentExperienceList)
                {
                    BaseTimelineModel certItem = new BaseTimelineModel();
                    
                    certItem.RecordId = t.Id;
                    certItem.ParentRecordId = recordId;
                    certItem.TimelineType = 'Certification';
                    certItem.Year = t.graduation_year__c;
                    certItem.Certification = t.certification__c;
                    certItem.Notes = t.Notes__c;
                    certItem.showDeleteItem = true;
                    returnList.add(certItem);
                 }
                 returnList[0].totalItems = total;
             } 
         } 
        return returnList;
   }
}