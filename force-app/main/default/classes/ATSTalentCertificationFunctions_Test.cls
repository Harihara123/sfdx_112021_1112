@isTest
private class ATSTalentCertificationFunctions_Test {
    @isTest static void test_performServerCall() {
        //No Params Entered
        Object r = ATSTalentCertificationFunctions.performServerCall('',null);
        System.assertEquals(r,null);
    }

    @isTest static void test_method_getCertifications(){
        Account newAcc = BaseController_Test.createTalentAccount('');

        Talent_Experience__c cert = new Talent_Experience__c(certification__c = 'Test Cert'
                                                            ,graduation_year__c='2017'
                                                            ,type__c = 'Training'
															,PersonaIndicator__c = 'Recruiter'//S-82159:Added by siva 12/6/2018
                                                            ,Talent__c = newAcc.Id);
        Database.insert(cert);


        Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newAcc.Id);
        p.put('maxRows','5000');
		p.put('personaIndicator', 'Recruiter');//S-82159:Added by siva 12/6/2018
        List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentCertificationFunctions.performServerCall('getCertifications',p);
    
        System.assertEquals(cert.Id, r[0].RecordId);
    }
}