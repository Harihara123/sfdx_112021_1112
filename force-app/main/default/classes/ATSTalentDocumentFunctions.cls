public with sharing class ATSTalentDocumentFunctions {
    public static Object performServerCall(String methodName, Map<String, Object> parameters){
        Object result = null;
        Map<String, Object> p = parameters;
        //Call the method within this class
        if(methodName == 'updateDefaultResumeFlag'){
            result = updateDefaultResumeFlag((String)p.get('recordId'),(String)p.get('talentDocumentId'));
        }else if(methodName == 'deleteDocumentRecord'){
            result = deleteDocumentRecord((String)p.get('talentDocumentId'));
        }else if(methodName == 'getTalentRecord'){
            result = talentDocuments((String)p.get('recordId'));
        }else if(methodName == 'talentDocuments'){
            result = getTalentRecord((String)p.get('recordId'));
        }else if(methodName == 'getDocumentTypeList'){ 
            result = getDocumentTypeList();
        }else if(methodName == 'getResumeCount'){
            result = getResumeCount((String)p.get('recordId'));
        }else if(methodName == 'getResumeDubbedCount'){ //S-40390 Added by Karthik
            result = getResumeDubbedCount((String)p.get('recordId'));
        }else if(methodName == 'getResumeListToReplace'){
            result = getResumeListToReplace((String)p.get('recordId'));
        }else if(methodName == 'getResumeDubbedListToReplace'){ //S-40390 Added by Karthik
            result = getResumeDubbedListToReplace((String)p.get('recordId'));
        }else if(methodName == 'performPostUploadAction'){
            result = performPostUploadAction((String)p.get('talentDocumentId'),(String)p.get('replaceResumeId'));
        }else if(methodName == 'parseResumeAndCreateTalent'){
            result = parseResumeAndCreateTalent((String)p.get('talentDocumentId'));
        }else if(methodName == 'createTalentDocument'){
            result = createTalentDocument((String)p.get('recordId'),(String)p.get('fileName'),(String)p.get('documentType'),(String)p.get('replaceResume'),(Boolean)p.get('defaultFlag'));
        }else if(methodName == 'getDocumentList'){
            result = getDocumentList((String)p.get('recordId'));
        }
        //S-56277 - Summary Modal Added by Karthik
        else if(methodName == 'getDocumentListModal'){
            result = getDocumentListModal((String)p.get('recordId'));
        }//S-56277 - End
		else if(methodName == 'getCoreContactCompareRecord') {
			result = getCoreContactCompareRecord((String)p.get('talentDocumentId'), (String)p.get('replaceResumeId'));
		}
        else if(methodName == 'getTalentResumeModel') {
            result = getTalentResumeModel((String)p.get('recordId'));
        }   
		else if(methodName == 'getAttachmentHTML') {
            result = getAttachmentHTML((String)p.get('recordId'));
        }       
        return result;
    }

    private static string getDocumentList(string recordID) {
        string listView = '';
		/* Sandeep : get all resumes except Other type */
        List<Talent_Document__c> sobjList = [SELECT Id, Azure_URL__c, Azure_Blobpath__c, Document_Name__c, CreatedDate,Default_Document__c 
											FROM Talent_Document__c 
											WHERE Talent__c =: recordID AND mark_for_deletion__c = false  AND 
											Document_Type__c != 'Other'  AND HTML_Version_Available__c = True  ORDER BY Default_Document__c DESC,CreatedDate DESC];
            /*  Removed this condition from above query  AND Document_Type__c != 'communities'   - As per S-176093     */
        Integer docNo = 1;

        for(Talent_Document__c t : sobjList){
            if(listView != ''){
                listView = listView + ',';
            }
            listView = listView + generateJSONRow(t);
            docNo = docNo + 1;
        }
        
        listView = '['+listView + ']'; 
        
        if(listView == '[]'){
            listView = '['+'{"label" : \"'+System.Label.ATS_Resume_Previews+' \","soql":\"\","countSoql":\"SELECT Count() FROM Talent_Document__c WHERE Talent__c =%%%parentRecordId%%% AND Document_Type__c != \'Other\'  AND HTML_Version_Available__c = True AND mark_for_deletion__c = false\"}'+']';
			//AND Document_Type__c != \'communities\' - Removed as per S-176093
        }
        return listView;
    }
            
    private static String generateJSONRow(Talent_Document__c t) { 
            DateTime dT = t.CreatedDate;
            Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
            
            String tempLabelName  = t.Document_Name__c;
            try {
                tempLabelName  = tempLabelName.replaceAll('[^a-zA-Z0-9\\s-_.]', '');
            } catch(Exception e){
                 system.debug('Fail to remvoe Special Chars from Resume name: ' + e.getMessage());
            }
            
            Integer indexDot = tempLabelName.lastIndexOf('.');
            if (indexDot > 0) {
                tempLabelName = tempLabelName.substring(0, indexDot);
            }
            string label = myDate.format()+ ': ' + tempLabelName;
           
            if(t.Default_Document__c){
                label = label  + ' (Default)';
            }
			
			//Added w.r.t US 132051
			String Additional = t.Azure_Blobpath__c;
			if(Additional.contains('\"')){
				Additional = JSON.serialize(Additional);
            }else{
                Additional = '\"'+Additional+'\"';
            }
			
        //return '{"label":\"' + label + '\","soql":\"SELECT Id from attachment where ParentId = \'' + t.Id + '\'\","countSoql":\"SELECT Count() FROM Talent_Document__c WHERE Talent__c =%%%parentRecordId%%% AND Document_Type__c != \'Other\' AND Document_Type__c != \'communities\' AND HTML_Version_Available__c = True AND mark_for_deletion__c = false\","additional":\"' +t.Azure_Blobpath__c + '\","CreatedDate":\"' + t.CreatedDate + '\"}';	
		return '{"label":\"' + label + '\","soql":\"SELECT Id from attachment where ParentId = \'' + t.Id + '\'\","countSoql":\"SELECT Count() FROM Talent_Document__c WHERE Talent__c =%%%parentRecordId%%% AND Document_Type__c != \'Other\'  AND HTML_Version_Available__c = True AND mark_for_deletion__c = false\","additional":' +Additional + ',"CreatedDate":\"' + t.CreatedDate + '\"}';	
		//AND Document_Type__c != \'communities\' 
    }

	public class TalentResumeModel {
		@AuraEnabled
		public String viewList;
		@AuraEnabled
		public BaseController.ListAndCount recordAndCount;

		Public TalentResumeModel() {}
        }
        
    private static TalentResumeModel getTalentResumeModel (String recordId) {
        TalentResumeModel model = new TalentResumeModel();
        model.viewList = '';
        
        List<Talent_Document__c> sobjList = [SELECT Id, Azure_URL__c, Azure_Blobpath__c, Document_Name__c, CreatedDate,Default_Document__c 
                                                FROM Talent_Document__c 
                                                WHERE Talent__c =: recordID AND mark_for_deletion__c = false  AND 
                                                Document_Type__c != 'Other' AND  HTML_Version_Available__c = True  ORDER BY Default_Document__c DESC,CreatedDate DESC];
       //Document_Type__c != 'communities' AND ---S-176093    
        for(Talent_Document__c t : sobjList){
            if(model.viewList != ''){
                model.viewList = model.viewList + ',';
            }
	        model.viewList = model.viewList + generateJSONRow(t);     
        }
        
        model.viewList = '['+model.viewList + ']'; 

        String parseSoql = '';
        String parseCountSoql = '';
        if(model.viewList == '[]'){ 
            //model.viewList = '['+'{"label" : \"Resume Previews \","soql":\"\","countSoql":\"SELECT Count() FROM Talent_Document__c WHERE Talent__c =%%%parentRecordId%%% AND Document_Type__c != \'Other\' AND Document_Type__c != \'communities\' AND HTML_Version_Available__c = True AND mark_for_deletion__c = false\"}'+']';
            model.viewList = '['+'{"label" : \"'+System.Label.ATS_Resume_Previews+' \","soql":\"\","countSoql":\"SELECT Count() FROM Talent_Document__c WHERE Talent__c =%%%parentRecordId%%% AND Document_Type__c != \'Other\'  AND HTML_Version_Available__c = True AND mark_for_deletion__c = false\"}'+']';
			//AND Document_Type__c != \'communities\'
		}
        else {
            parseSoql = 'SELECT Id from attachment where ParentId = \'' + sobjList[0].Id + '\'';
            parseCountSoql = 'SELECT Count() FROM Talent_Document__c WHERE Talent__c = \'' + recordId + '\' AND Document_Type__c != \'Other\'  AND HTML_Version_Available__c = True AND mark_for_deletion__c = false';
			//AND Document_Type__c != \'communities\'
	    }

        model.recordAndCount = (BaseController.ListAndCount)BaseController.performServerCall('', '', 'getRowAndCount', new Map<String, String>{'soql' => parseSoql, 'countSoql' => parseCountSoql, 'maxRows' => '5'});
        return model;
    }

    //S-56277 - Summary Modal Added by Karthik
    private static string getDocumentListModal(string recordID) {
        string listView = '';
        List<Talent_Document__c> sobjList = [select Id, Azure_URL__c, Azure_Blobpath__c, Document_Name__c, CreatedDate,Default_Document__c from Talent_Document__c WHERE Talent__c =: recordID AND mark_for_deletion__c = false  AND Document_Type__c = 'Resume' AND HTML_Version_Available__c = True  ORDER BY Default_Document__c DESC,CreatedDate DESC];
            
        Integer docNo = 1;

        for(Talent_Document__c t : sobjList){
            if(listView != ''){
                listView = listView + ',';
            }
            
            DateTime dT = t.CreatedDate;
            Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
            
            //Farid - need to relplace speical chars from resumes name 
            String tempLabelName  = t.Document_Name__c;
            //system.debug(logginglevel.WARN, 'tempLabelName: ' + t);
            try {
                tempLabelName  = tempLabelName.replaceAll('[^a-zA-Z0-9\\s-_.]', '');
                //system.debug(logginglevel.WARN, 'tempLabelName after remove special charas: ' + tempLabelName);
            } catch(Exception e){
                 system.debug('Fail to remvoe Special Chars from Resume name: ' + e.getMessage());
            }
            
            Integer indexDot = tempLabelName.lastIndexOf('.');
            if (indexDot > 0) {
                tempLabelName = tempLabelName.substring(0, indexDot);
            }
            string label = myDate.format()+ ': ' + tempLabelName;
           
            
            if(t.Default_Document__c){
                label = label  + ' (Default)';
            }
			
			//Added w.r.t US 132051
			String Additional = t.Azure_Blobpath__c;
			if(Additional.contains('\"')){
				Additional = JSON.serialize(Additional);
			}else{
                Additional = '\"'+Additional+'\"';
            }
			
            //listView = listView + '{"label":\"' + label + '\","soql":\"SELECT Id from attachment where ParentId = \'' + t.Id + '\'\","countSoql":\"SELECT Count() FROM Talent_Document__c WHERE Talent__c =%%%parentRecordId%%% AND Document_Type__c = \'Resume\' AND HTML_Version_Available__c = True AND mark_for_deletion__c = false\","additional":\"' +t.Azure_Blobpath__c + '\"}';    
			listView = listView + '{"label":\"' + label + '\","soql":\"SELECT Id from attachment where ParentId = \'' + t.Id + '\'\","countSoql":\"SELECT Count() FROM Talent_Document__c WHERE Talent__c =%%%parentRecordId%%% AND Document_Type__c = \'Resume\' AND HTML_Version_Available__c = True AND mark_for_deletion__c = false\","additional":' +Additional + '}';    
            docNo = docNo + 1;         
        }
        
        listView = '['+listView + ''+']';

        if(listView == '[]'){
            listView = '['+'{"label":\"\","soql":\"\","countSoql":\"\"}'+']';
        }

        return listView;
    }
    //S-56277 - End

   private static String updateDefaultResumeFlag(String recordId, String talentDocumentId) {
        List<Talent_Document__c> talentDocumentList = [select Id from Talent_Document__c 
                                                       WHERE Talent__c = :recordId 
                                                       and mark_for_deletion__c = false and committed_document__c = true 
                                                       and document_type__c = 'Resume' and Default_Document__c = true]; 

        if(talentDocumentList.size() > 0){
            for (Talent_Document__c t : talentDocumentList){
                t.Default_Document__c = false;
                update(t);
            }
        }                                     


        Talent_Document__c talentDocument = [SELECT Id FROM Talent_Document__c where Id =:talentDocumentId];
        talentDocument.Default_Document__c = true;
        update (talentdocument);
        return talentDocument.id;
    }

    private static Map<String, String> getDocumentTypeList() {
        Map<String, String> documentTypeMappings = new Map<String,String>();
        Schema.DescribeFieldResult field = Talent_Document__c.Document_Type__c.getDescribe();
        List<Schema.PicklistEntry> documentTypeList = field.getPicklistValues();
        for (Schema.PicklistEntry entry: documentTypeList) {
            documentTypeMappings.put(entry.getValue(), entry.getLabel());
        }
        return documentTypeMappings;
    }

    private static Integer getResumeCount(String recordId) {
        //Sandeep: handling null value for recordId. if null , count will be zero.
        Integer resumeCount = 0;
        if(null != recordId){
        	System.debug('recordId '+recordId);
        	Account account = [select Talent_Internal_Resume_Count__c from Account where id = :recordId];
        	resumeCount = Integer.valueOf(account.Talent_Internal_Resume_Count__c);
        }
        return resumeCount;
    }
    //S-40390 Added by Karthik
    private static Integer getResumeDubbedCount(String recordId) {
        List<Talent_Document__c> tds = [SELECT Document_Name__c,Document_Type__c,Id,IsDeleted,Name FROM Talent_Document__c WHERE Talent__c = :recordId 
                                    AND Document_Type__c = 'Resume - Dubbed' AND Internal_Document__c = True AND Mark_For_Deletion__c = False AND Committed_Document__c = True];
        Integer resumeDubbedCount = tds.size();                                     
        return resumeDubbedCount;
    }

    private static List<Talent_Document__c> getResumeListToReplace(String recordId) {
        List<Talent_Document__c> talentDocumentList = [select Id, Document_Name__c, lastmodifieddate,
                                              (Select Id, Name from Attachments) from Talent_Document__c 
                                                       WHERE Talent__c = :recordId 
                                                       and mark_for_deletion__c = false and committed_document__c = true 
                                                       and document_type__c = 'Resume'
                                              order by lastmodifieddate DESC]; 
        return talentDocumentList;
    }
    //S-40390 Added by Karthik
    private static List<Talent_Document__c> getResumeDubbedListToReplace(String recordId) {
        List<Talent_Document__c> talentDocumentList = [select Id, Document_Name__c, lastmodifieddate,
                                              (Select Id, Name from Attachments) from Talent_Document__c 
                                                       WHERE Talent__c = :recordId 
                                                       and mark_for_deletion__c = false and committed_document__c = true 
                                                       and document_type__c = 'Resume - Dubbed'
                                              order by lastmodifieddate DESC]; 
        return talentDocumentList;
    }

    private static string performPostUploadAction(String talentDocumentId, String replaceResumeId) {
        
		Talent_Document__c talentDocument = [SELECT Id, Committed_Document__c FROM Talent_Document__c where id = :talentDocumentId];
        talentDocument.Committed_Document__c = true;
        update (talentdocument);

        if (replaceResumeId != '' && replaceResumeId != null) {
            String[] parts = replaceResumeId.split('-');
            String oldTalentDocumentId = parts[0];
            String attachmentId = parts[1];
            Attachment attachment = new Attachment(Id = attachmentId); 
            delete attachment;
            
            talentDocument = [SELECT Id FROM Talent_Document__c where Id =:oldTalentDocumentId];
            talentDocument.Mark_For_Deletion__c = true;
            update (talentdocument);
        }
		
        return 'Success';
    }

    private static String parseResumeAndCreateTalent(String talentDocumentId) {
        List<String> contact = null;
        // Get attachment ID
        Attachment[] atts = [SELECT Id, Name from Attachment WHERE ParentId = :talentDocumentId];
        if (atts != null && atts.size() > 0) {
            List<String> attList = new List<String>();
            attList.add(String.valueOf(atts[0].Id));
            // Returns 1 item in list of string since it was originally built as Invocable. Could be refactored?
            try {
                contact = InvocableAttachmentParser.parseAttachmentForData(attList);
            } catch (Exception e) {
                return 'errorparsingresume';
            }
            if (contact != null && contact.size() > 0) {
                return contact[0];
            }
        }
        return 'Failure';
    }

    private static String createTalentDocument(String recordId, String fileName, String documentType, String replaceResume, Boolean defaultFlag) {
        Talent_Document__c talentDocument = null;                                           
        talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = recordId;

        try {
                fileName  = fileName.replaceAll('[^a-zA-Z0-9\\s-_.]', '');
        } catch(Exception e){
               system.debug('Fail to remvoe Special Chars from Resume name: ' + e.getMessage());
        }
        
        talentDocument.Document_Name__c = fileName;
        talentDocument.Document_Type__c = documentType; 
        talentDocument.Default_Document__c = defaultFlag;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = false;
        insert(talentDocument);
        return talentDocument.id;                                                  
    }
    
    
    private static List<Talent_Document__c> talentDocuments(String recordId){
        system.debug('@@@@@ Success');
        return [SELECT CreatedDate,Id,Document_Name__c, Azure_URL__c, Document_Type__c, Default_Document__c, (Select Id from Attachments) FROM Talent_Document__c WHERE mark_for_deletion__c = false and committed_document__c = true  and Talent__c =: recordId ORDER BY LastModifiedDate DESC];
	}
    
    private static string deleteDocumentRecord(String recordId){
        if(recordId != ''){
            Database.delete(recordId);
            return 'Success';
        }else{
            return 'No Record Id specified';
        }
    }
    
    private static Account getTalentRecord(String recordId){ //need to take care with LDS
        return [ SELECT Id, Name from Account where id =: recordId];
    }
	/**
	* @description This method returned compare JSON of parsed records and existing Talent
	**/
	private static String getCoreContactCompareRecord(String documentID, String replaceResumeId ) {
		
		String talentID =  (String)[SELECT Talent__c FROM Talent_Document__c WHERE Id = :documentID][0].Talent__c;
		System.debug('documentID---'+documentID+'---talentID---'+talentID+'---replaceResumeId---'+replaceResumeId);
		String compareJSON;
		try {
                compareJSON = ATS_UpdateTalentByParsedResume.fetchExistingNParsedTalent(documentID, talentID, 'Contact');
        } catch (Exception ex) {
			System.debug('ATS_UpdateTalentByParsedResume.fetchExistingNParsedTalent exception ::::');
			System.debug(ex);
            return 'Resume parsing error.';
        }
		performPostUploadAction(documentID, replaceResumeId) ;
		
		return compareJSON;
	}
	/**
	*Rajeesh S-101395 - iframe replacement
	**/ 
	@Testvisible
	private static String getAttachmentHTML(String attchId){
		Attachment att = [Select Body from Attachment where Id=:attchId]; 
        Blob attBody = (Blob)att.body;
        String attHTML=attBody.toString();
        System.debug('attHTML===>'+attHTML);
        return attHTML;
	}
}