@isTest
private class ATSTalentDocumentFunctions_Test {
	@isTest static void test_performServerCall() {
		//No Params Entered
		Object r = ATSTalentDocumentFunctions.performServerCall('',null);
		System.assertEquals(r,null);
	}
	
	@isTest static void test_method_updateDefaultResumeFlag(){
		Account newAcc = BaseController_Test.createTalentAccount('');

 		Talent_Document__c talentDocument = null;                                           
        talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.doc';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = true;
        talentDocument.Internal_Document__c = true;
        talentDocument.HTML_Version_Available__c=True;
        talentDocument.mark_for_deletion__c=false;
        talentDocument.Committed_Document__c = true;
        insert(talentDocument);
        
       	Map<String,Object> p = new Map<String,Object>();
		p.put('talentDocumentId', talentDocument.Id);
        p.put('recordId', newAcc.Id);

        String r = (String)ATSTalentDocumentFunctions.performServerCall('updateDefaultResumeFlag',p);

		Talent_Document__c talentDocument2 = [SELECT Id,Default_Document__c FROM Talent_Document__c where Id =:talentDocument.Id];
        
		System.assertEquals(true,talentDocument2.Default_Document__c);
	}

	@isTest static void test_method_getDocumentTypeList(){
		Map<String, String> r = (Map<String, String>)ATSTalentDocumentFunctions.performServerCall('getDocumentTypeList',null);
		System.assertNotEquals(r,null);
	}

	@isTest(SeeAllData=true) static void test_method_getResumeCount(){
		Account newAcc = BaseController_Test.createTalentAccount('');

 		Talent_Document__c talentDocument = null;                                           
        talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.doc';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = true;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        insert(talentDocument);
        
       	Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);

		Integer r = (Integer)ATSTalentDocumentFunctions.performServerCall('getResumeCount',p);
		System.assertEquals(1,r);
	}

	@isTest static void test_method_getResumeListToReplace(){
		Account newAcc = BaseController_Test.createTalentAccount('');

 		Talent_Document__c talentDocument = null;                                           
        talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.doc';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = true;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        insert(talentDocument);


       	Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);

		List<Talent_Document__c> talentDocumentList  = (List<Talent_Document__c>)ATSTalentDocumentFunctions.performServerCall('getResumeListToReplace',p);
		System.assertEquals(1,talentDocumentList.size());
	}

	@isTest(SeeAllData=true) static void test_method_performPostUploadAction(){
		Account newAcc = BaseController_Test.createTalentAccount('');

		Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        Database.insert(talentDocument);

		Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);


 		Talent_Document__c talentDocument2 = new Talent_Document__c();
        talentDocument2.Talent__c = newAcc.Id;
        talentDocument2.Document_Name__c = 'test2.txt';
        talentDocument2.Document_Type__c = 'Resume'; 
        talentDocument2.Default_Document__c = true;
        talentDocument2.Internal_Document__c = true;
        talentDocument2.Committed_Document__c = true;
        Database.insert(talentDocument2);


       	Map<String,Object> p = new Map<String,Object>();
		p.put('talentDocumentId', talentDocument2.Id);
		p.put('replaceResumeId', talentDocument.Id + '-' + attach.Id);


		String r  = (String)ATSTalentDocumentFunctions.performServerCall('performPostUploadAction',p);
		System.assertEquals('Success',r);	
	}

	@isTest(SeeAllData=true) static void test_method_createTalentDocument(){
		Account newAcc = BaseController_Test.createTalentAccount('');

		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId',newAcc.Id);
		p.put('fileName','test.doc');
		p.put('documentType','Resume');
		p.put('replaceResume','');
		p.put('defaultFlag', true);

		String r = (String)ATSTalentDocumentFunctions.performServerCall('createTalentDocument',p);
		System.assertNotEquals(r,null);
	}


	@isTest(SeeAllData=true) static void test_method_getDocumentList(){
		Account newAcc = BaseController_Test.createTalentAccount('');

		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId',newAcc.Id);
		p.put('fileName','test.doc');
		p.put('documentType','Resume');
		p.put('replaceResume','');
		p.put('defaultFlag', true);

		String r = (String)ATSTalentDocumentFunctions.performServerCall('createTalentDocument',p);

		p = new Map<String,Object>();
		p.put('recordId',newAcc.Id);
        string listView = '';
        
        Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.HTML_Version_Available__c=True;
        talentDocument.mark_for_deletion__c=false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
		talentDocument.Azure_URL__c = 'https://api-tst.allegistest.com/documentblob?blobpath=TalentCommunity/Account/'+newAcc.Id+'/Resume/TalentDocument/docx/test.txt.docx';
        //talentDocument.Azure_URL__c =null;
		Database.insert(talentDocument);

		Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);
        
		String dList = (String)ATSTalentDocumentFunctions.performServerCall('getDocumentList',p);
        
        String dListM = (String)ATSTalentDocumentFunctions.performServerCall('getDocumentListModal',p);

		System.assertNotEquals(dList,null);
		System.assertNotEquals(dList,'');
        System.assertNotEquals(dListM,null);
		System.assertNotEquals(dListM,'');
	}
    @isTest(SeeAllData=true) static void test_deleteDocumentRecord(){
		Account newAcc = BaseController_Test.createTalentAccount('');
        Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        Database.insert(talentDocument);
        Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);
		Map<String,Object> p = new Map<String,Object>();
		p.put('talentDocumentId',talentDocument.Id);
		String dList = (String)ATSTalentDocumentFunctions.performServerCall('deleteDocumentRecord',p);

	}
    @isTest(SeeAllData=true) static void test_getResumeDubbedCount(){
        Account newAcc = BaseController_Test.createTalentAccount('');
        Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        talentDocument.Mark_For_Deletion__c =  false;
        Database.insert(talentDocument);
        Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId',newAcc.Id);
		Integer cnt = (Integer)ATSTalentDocumentFunctions.performServerCall('getResumeDubbedCount',p);
    }
     @isTest(SeeAllData=true) static void test_talentDocuments(){
        Account newAcc = BaseController_Test.createTalentAccount('');
        Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        talentDocument.Mark_For_Deletion__c =  false;
        Database.insert(talentDocument);
        Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId',newAcc.Id);
		List<Talent_Document__c> docs = (List<Talent_Document__c>)ATSTalentDocumentFunctions.performServerCall('getTalentRecord',p);
    }
     @isTest(SeeAllData=true) static void test_getResumeDubbedListToReplace(){
        Account newAcc = BaseController_Test.createTalentAccount('');
        Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        talentDocument.Mark_For_Deletion__c =  false;
        Database.insert(talentDocument);
        Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId',newAcc.Id);
		List<Talent_Document__c> docs = (List<Talent_Document__c>)ATSTalentDocumentFunctions.performServerCall('getResumeDubbedListToReplace',p);
    }
	@isTest(SeeAllData=true) static void test_method_getAttachmentHTML(){ 
		List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        Database.insert(newAcc);
		
		Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=newAcc.Id, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(newC);
        Talent_Document__c tdoc =  new Talent_Document__c(Talent__c = newAcc.id,Document_Name__c = 'TestDocument.docx', Document_Type__c = 'Resume',HTML_Version_Available__c=true);
        insert tdoc;
		Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=tdoc.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);
        
        Test.startTest();
		String strHTML = ATSTalentDocumentFunctions.getAttachmentHTML(attach.id);
        Test.stopTest(); 
	}
    @isTest (SeeAllData=true) static void test_ParseResumeAndCreateTalent(){
        Account newAcc = BaseController_Test.createTalentAccount('');
        Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        talentDocument.Mark_For_Deletion__c =  false;
        Database.insert(talentDocument);
        Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        //attach.ContentType = 'text/plain';
        Database.insert(attach);
		Map<String,Object> p = new Map<String,Object>();
		p.put('talentDocumentId',talentDocument.id);
        p.put('recordId',newAcc.id);
        ATSTalentDocumentFunctions.performServerCall('getTalentResumeModel',p);
		String docs = (String)ATSTalentDocumentFunctions.performServerCall('parseResumeAndCreateTalent',p);
   
    }
    
}