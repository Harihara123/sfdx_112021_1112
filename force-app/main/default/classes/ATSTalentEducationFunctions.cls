public with sharing class ATSTalentEducationFunctions {
    
    public static Object performServerCall(String methodName, Map<String, Object> parameters){
        Object result = null;
        Map<String, Object> p = parameters;

        //Call the method within this class
        if(methodName == 'getEducations'){
        system.debug('------Final'+(String)p.get('personaIndicator'));
            result = getEducations((String)p.get('recordId'), (String)p.get('maxRows'), (String)p.get('personaIndicator'));
        }else if(methodName == 'getDegreeLevelList'){
            result = getDegreeLevelList();
        }
        
        return result;
    }


    private static List<BaseTimelineModel> getEducations(String recordId, String maxRows, string personaIndicator)
    {

        Integer iMax = Integer.valueof(maxRows);
        Integer valueQuery = (iMax >= 5000) ? 5000 : iMax;

        List<BaseTimelineModel> returnList = new List<BaseTimelineModel>();
        List<Talent_Experience__c> talentExperienceList = new List<Talent_Experience__c>();
        boolean eval = false;
		//S-111810:Removed TalentMergeIndicator usage by Siva 1/10/2019
        //Boolean mergeIndicator = String.valueOf(Label.TalentMergeIndicator) == 'True'?true:false;
        //if(mergeIndicator){
            talentExperienceList = [select id, graduation_year__c,Maxhire_Education_Value__c,PersonaIndicator__c , degree_level__c, Organization_Name__c, Major__c, Honor__c, Notes__c 
                     from talent_experience__c 
                     where type__c = 'Education' 
                     AND PersonaIndicator__c =: personaIndicator
                     and talent__r.Id =  :recordId 
                     ORDER BY graduation_year__c DESC LIMIT :valueQuery];
        /*}else{
            talentExperienceList = [select id, graduation_year__c,Maxhire_Education_Value__c,PersonaIndicator__c , degree_level__c, Organization_Name__c, Major__c, Honor__c, Notes__c 
                     from talent_experience__c 
                     where type__c = 'Education' 
                     and talent__r.Id =  :recordId 
                     ORDER BY graduation_year__c DESC LIMIT :valueQuery];
        }*/
        if(talentExperienceList.size() > 0)
        {
            for(Talent_Experience__c t: talentExperienceList)
            {
                BaseTimelineModel educationItem = new BaseTimelineModel();
                
                educationItem.RecordId = t.Id;
                educationItem.ParentRecordId = recordId;
                educationItem.TimelineType = 'Education';
                educationItem.Year = t.graduation_year__c;
                educationItem.Degree = t.degree_level__c;
                educationItem.SchoolName = t.Organization_Name__c;
                educationItem.Major = t.Major__c;
                educationItem.Honors = t.Honor__c;
                educationItem.Notes = t.Notes__c;
                educationItem.ParsedSchool = t.Maxhire_Education_Value__c;
                educationItem.showDeleteItem = true;
                returnList.add(educationItem);
             }

         }
        return returnList;
   }

   private static Map<String, String> getDegreeLevelList() {
        Map<String, String> degreeLevelMappings = new Map<String,String>();
        Schema.DescribeFieldResult field = Talent_Experience__c.Degree_Level__c.getDescribe();
        List<Schema.PicklistEntry> degreeLevelList = field.getPicklistValues();
        for (Schema.PicklistEntry entry: degreeLevelList) {
            degreeLevelMappings.put(entry.getValue(), entry.getLabel());
        }
        return degreeLevelMappings;
    }
}