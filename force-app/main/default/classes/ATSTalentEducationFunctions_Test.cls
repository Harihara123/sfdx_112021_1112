@isTest
private class ATSTalentEducationFunctions_Test {
    @isTest static void test_performServerCall() {
        //No Params Entered
        Object r = ATSTalentEducationFunctions.performServerCall('',null);
        System.assertEquals(r,null);
    }

    @isTest static void test_method_getEducations(){
        Account newAcc = BaseController_Test.createTalentAccount('');

        Talent_Experience__c cert = new Talent_Experience__c(graduation_year__c = '2017'
                                                            , degree_level__c = 'Bachelors'
                                                            , Organization_Name__c  = 'Univeristy'
                                                            , Major__c = 'Software Dev.'
                                                            , Honor__c = 'Computing'
                                                            , Notes__c ='Some Notes'
                                                            ,type__c = 'Education'
															,PersonaIndicator__c = 'Recruiter'//S-82159:Added by siva 12/6/2018
                                                            ,Talent__c = newAcc.Id);
        Database.insert(cert);


        Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newAcc.Id);
        p.put('maxRows','5000');
		p.put('personaIndicator', 'Recruiter');//S-82159:Added by siva 12/6/2018
        List<BaseTimelineModel> r = (List<BaseTimelineModel>)ATSTalentEducationFunctions.performServerCall('getEducations',p);
    
        System.assertEquals(cert.Id, r[0].RecordId);
    }

    @isTest static void test_method_getDegreeLevelList(){

        Map<String,Object> p = new Map<String,Object>();
        Map<String, String> r = (Map<String, String>)ATSTalentEducationFunctions.performServerCall('getDegreeLevelList',p);
    
        System.assertNotEquals(null,r);
    }
}