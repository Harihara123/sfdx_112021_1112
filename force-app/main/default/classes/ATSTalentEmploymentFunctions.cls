public with sharing class ATSTalentEmploymentFunctions {
    //Adding the wrapper class
    static integer twhcount = 0;
    static integer TEtotalcount = 0;
	static string personaIndicator;//S-82159:Added by Siva 12/6/2018
     static EmploymentWrapper ReturnEmpData;
    @TestVisible public class EmploymentWrapper{
         @AuraEnabled public String Ucategory = '';
         @AuraEnabled public Integer AWHCount = 0;
         @AuraEnabled public Integer AGWHCount = 0;
         @AuraEnabled public List<BaseTimelineModel>  AWHList  = new List<BaseTimelineModel>();
         @AuraEnabled public List<BaseTimelineModel> AGWHList = new List<BaseTimelineModel>();
    
    }

    //Adding the wrapper class
    public static Object performServerCall(String methodName, Map<String, Object> parameters){
        Object result = null;
        Map<String, Object> p = parameters;

        //Call the method within this class
        if(methodName == 'getWorkHistory'){
		    personaIndicator = (String)p.get('personaIndicator'); //S-82159:Added by Siva 12/6/2018
            result = getWorkHistory((String)p.get('recordId'), (String)p.get('maxRows') != '' ? Integer.valueOf(p.get('maxRows')) : 5);
        
		} else if(methodName == 'getCurrencyList'){
            result = getCurrencyList();
         }
        else if(methodName == 'getRateFrequencyList'){
            result = getRateFrequencyList();
        }
        return result;
    }

    //All Work History
    private static EmploymentWrapper getWorkHistory(string recordId, integer maxRows) {
        List<BaseTimelineModel> returnList = new List<BaseTimelineModel>();
        ReturnEmpData = new EmploymentWrapper ();

         ReturnEmpData.Ucategory =  BaseController.getUserCategory();

        if(ReturnEmpData.Ucategory == 'CATEGORY1'){
               
               returnList.addAll(getAllegisWorkHistory(recordId, maxRows));
               if(twhcount > 0){
                  ReturnEmpData.AGWHCount = twhcount;
                  ReturnEmpData.AWHCount = twhcount;
                  ReturnEmpData.AGWHList.addAll(returnList);
               }
               returnList.addAll(getAllegisWorkHistoryTalentExperience(recordId, maxRows));
               if(TEtotalcount > 0){
                  ReturnEmpData.AWHCount += TEtotalcount;
                  returnList.sort();
                  if(returnList.size() > maxRows){
                     for(Integer j=0;j < maxRows;j++){
                         ReturnEmpData.AWHList.add(returnList[j]);
                      }
                  }else{
                      ReturnEmpData.AWHList.addAll(returnList);
                  }
                  
               }else{
                      ReturnEmpData.AWHList.addAll(returnList);
               }
               
        }else {
               returnList.addAll(getAllegisWorkHistoryTalentExperience(recordId, maxRows));
               if(TEtotalcount > 0){
                  ReturnEmpData.AWHCount = TEtotalcount;
                  ReturnEmpData.AWHList.addAll(returnList);
                  ReturnEmpData.AGWHCount = ReturnEmpData.AGWHList.size();
               }
               
        }
        
        return ReturnEmpData;
    }

    //Allegis Work History
    private static List<BaseTimelineModel> getAllegisWorkHistory(string recordId, integer maxRows) {
        List<BaseTimelineModel> returnList = new List<BaseTimelineModel>();
        
        integer i=0;
        for (Talent_Work_History__c twh : [SELECT Talent__c, Start_Date__c, End_Date__c, Job_Title__c, Main_Skill__c, Pay_Rate__c, Currency_Type__c, Payment_Frequency_Type__c, DivisionName__c, Finish_Reason__c, Id, Organization_Name__c, Current_Assignment__c,Talent__r.Peoplesoft_Id__c, SourceCompany__c, SourceCompanyId__c, Finish_Code__c, Employee_Class__c,Current_Assignment_Formula__c,Talent__r.Candidate_Status__c,
                                               ESFId__c   FROM Talent_Work_History__c 
                                                  WHERE Talent__c = :recordId AND SourceId__c like 'R.%'
                                                  ORDER BY  End_Date__c DESC, Start_Date__c DESC NULLS LAST]) {
                             
                if(i < maxRows){
                   returnList.add(buildEmployment(twh));
                }
            i++;    
            }
        twhcount = i;
        
        return returnList;
    }
    //S-82159:Modified the query to use the personaIndicator parameter by Siva 12/4/2018
    private static List<BaseTimelineModel> getAllegisWorkHistoryTalentExperience(string recordId, integer maxRows) {
        List<BaseTimelineModel> returnList = new List<BaseTimelineModel>();
        integer i=0;
		List<Talent_Experience__c> talEx = new List<Talent_Experience__c>();
		//S-111810:Removed TalentMergeIndicator usage by Siva 1/10/2019
		//if(Label.TalentMergeIndicator.equalsIgnoreCase('True')) {
			talEx = [SELECT Talent__c, End_Date__c,Payrate__c,PersonaIndicator__c, Daily_Rate__c, Salary__c,Id,Organization_Name__c,Start_Date__c,Title__c, Allegis_Placement__c, Current_Assignment__c, Department__c, Bonus__c, Bonus_Percentage__c,Other_Compensation__c, Compensation_Total__c, Reason_For_Leaving__c, Notes__c,Currency__c,Rate_Frequency__c,Talent__r.Candidate_Status__c
                                                    FROM Talent_Experience__c 
                                                    WHERE Talent__c = :recordId  
                                                        AND Type__c = 'Work'
                                                        AND PersonaIndicator__c =:personaIndicator
                                                        ORDER BY  End_Date__c DESC, Start_Date__c DESC NULLS LAST];
		/*} else {
			talEx = [SELECT Talent__c, End_Date__c,Payrate__c, Daily_Rate__c, Salary__c,Id,Organization_Name__c,Start_Date__c,Title__c, Allegis_Placement__c, Current_Assignment__c, Department__c, Bonus__c, Bonus_Percentage__c,Other_Compensation__c, Compensation_Total__c, Reason_For_Leaving__c, Notes__c,Currency__c,Rate_Frequency__c,Talent__r.Candidate_Status__c
                                                    FROM Talent_Experience__c 
                                                    WHERE Talent__c = :recordId  
                                                        AND Type__c = 'Work'
                                                        ORDER BY  End_Date__c DESC, Start_Date__c DESC NULLS LAST];
		}*/
        for (Talent_Experience__c te : talEx) {
                
                
                if(i < maxRows){
                    returnList.add(buildEmployment(te));
                    if(ReturnEmpData.Ucategory != 'CATEGORY1'){
                       if(te.Allegis_Placement__c == true ){
                            ReturnEmpData.AGWHList.add(returnList[i]);
                        }
                    }
                    
                   }else if(ReturnEmpData.Ucategory != 'CATEGORY1'){
                       if(te.Allegis_Placement__c == true){
                            ReturnEmpData.AGWHList.add(buildEmployment(te));
                        }
                        
                    }
                    
                    i++;
                    
            }
        TEtotalcount = i;
        return returnList;
    }
    
    

     private static BaseTimelineModel buildEmployment(Talent_Experience__c t) {
        BaseTimelineModel workExItem = new BaseTimelineModel();
        
         workExItem.WorkType = 'TalentExperience';
         
         workExItem.ParentRecordId = t.Talent__c;
         workExItem.RecordId = t.Id;
         workExItem.TimelineType = 'Employment';
         workExItem.AllegisPlacement = t.Allegis_Placement__c;
         workExItem.CurrentAssignment = t.Current_Assignment__c;
         workExItem.OrgName = t.Organization_Name__c;
         workExItem.OrgId = '';
         workExItem.Department = t.Department__c;
         workExItem.JobTitle = t.Title__c;
         workExItem.EmploymentStartDate = t.Start_Date__c;
         workExItem.EmploymentEndDate = t.End_Date__c;
         workExItem.Salary = t.Salary__c;
         workExItem.Payrate = t.Payrate__c;
         workExItem.DailyRate = t.Daily_Rate__c;
         workExItem.Bonus = t.Bonus__c;
         workExItem.BonusPct = t.Bonus_Percentage__c;
         workExItem.OtherComp = t.Other_Compensation__c;
         workExItem.CompensationTotal = t.Compensation_Total__c;
         workExItem.ReasonForLeaving = t.Reason_For_Leaving__c;
         workExItem.FinishReason = t.Reason_For_Leaving__c;
         workExItem.Notes = t.Notes__c;
         workExItem.CurrencyType = t.Currency__c;
         workExItem.RateFrequency = t.Rate_Frequency__c;
         workExItem.PaymentFrequency = t.Rate_Frequency__c;
         workExItem.showDeleteItem = true;

         if(t.Talent__r.Candidate_Status__c != null){
            workExItem.candidateStatus = t.Talent__r.Candidate_Status__c;
        }
         return workExItem;
    }
    
    private static BaseTimelineModel buildEmployment(Talent_Work_History__c t) {
        BaseTimelineModel workExItem = new BaseTimelineModel();
         
        Boolean codeEmpty = (t.Finish_Code__c == null || t.Finish_Code__c == '');
        Boolean reasonEmpty = (t.Finish_Reason__c == null || t.Finish_Reason__c == '');
        
        //Work History is always Allegis Placement
        workExItem.AllegisPlacement = true;
        workExItem.WorkType = 'TalentWorkHistory';
        
        if(t.ESFId__c !='0'){ // adding condition for defect DEFECT D-11560
        	workExItem.esfId = t.ESFId__c; // for story S-140332    
        }        
        
        workExItem.PeoplesoftId = t.Talent__r.Peoplesoft_Id__c;
        workExItem.JobTitle = t.Job_Title__c;
        workExItem.MainSkill = t.Main_Skill__c;
        workExItem.Payrate = t.Pay_Rate__c;
        workExItem.CurrencyType = t.Currency_Type__c;
        workExItem.RateFrequency = t.Payment_Frequency_Type__c;
        workExItem.PaymentFrequency = t.Payment_Frequency_Type__c;
        workExItem.EmploymentStartDate = t.Start_Date__c;
        workExItem.EmploymentEndDate = t.End_Date__c;
        workExItem.Division = t.DivisionName__c;
        workExItem.BillRate = t.Employee_Class__c;
        workExItem.ParentRecordId = t.Talent__c;
        workExItem.RecordId = t.Id;
        workExItem.TimelineType = 'Employment';      
        //workExItem.CurrentAssignment = t.Current_Assignment__c; // D-05880 | Boolean field displays multiple placements as current even if placement ended | Natalie L 3/22/18
        workExItem.CurrentAssignment = t.Current_Assignment_Formula__c; 
        workExItem.OrgName = t.SourceCompany__c;
        workExItem.OrgId = t.SourceCompanyId__c;

        if(t.Talent__r.Candidate_Status__c != null){
            workExItem.candidateStatus = t.Talent__r.Candidate_Status__c;
        }
        if (codeEmpty && !reasonEmpty)
            workExItem.FinishReason = t.Finish_Reason__c;
        else if (!codeEmpty && reasonEmpty)
            workExItem.FinishReason = t.Finish_Code__c;
        else if (!codeEmpty && !reasonEmpty)
            workExItem.FinishReason = t.Finish_Code__c + ': ' + t.Finish_Reason__c;
        
        return workExItem;
    }
    
    private static Map<String, String> getCurrencyList() {
        Map<String, String> CurLevelMappings = new Map<String,String>();
        Schema.DescribeFieldResult field = Talent_Experience__c.Currency__c.getDescribe();
        List<Schema.PicklistEntry> CurLevelList = field.getPicklistValues();
        for (Schema.PicklistEntry entry: CurLevelList) {
            CurLevelMappings.put(entry.getValue(), entry.getLabel());
        }
        return CurLevelMappings;
    }

        private static Map<String, String> getRateFrequencyList() {
        
            Map<String, String> RateFrequencyMap = new Map<String,String>();
            Schema.DescribeFieldResult field = Talent_Experience__c.Rate_Frequency__c.getDescribe();
           
            List<Schema.PicklistEntry> rateFrequencyList = field.getPicklistValues();
            for (Schema.PicklistEntry value: rateFrequencyList) {
                RateFrequencyMap.put(value.getValue(), value.getLabel());
            }
            return RateFrequencyMap;
    }


}