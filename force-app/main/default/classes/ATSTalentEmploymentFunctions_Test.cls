@isTest
private class ATSTalentEmploymentFunctions_Test {
	@isTest static void test_performServerCall() {
		//No Params Entered
		Object r = ATSTalentEmploymentFunctions.performServerCall('',null);
		System.assertEquals(r,null);
	}

	@isTest static void test_method_getWorkHistory_category1(){
	   String profileName = 'Single Desk 1';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'testlst';
        testUser.Firstname = 'Testy';
        testUser.Alias = 'test';
        testUser.CommunityNickname = '12347';
        profile prof = [ select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
		testUser.OPCO__c = 'TEK';


        insert testUser;
            System.runAs(testUser) {
		Account newAcc = BaseController_Test.createTalentAccount('');
		BaseController_Test.createTalentEmployment(newAcc.Id, true);
        BaseController_Test.createTalentWorkHistory(newAcc.Id);

		Test.startTest();
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
        p.put('maxRows', '5000');
		p.put('personaIndicator', 'Recruiter');//S-82159:Added by siva 12/4/2018
		 ATSTalentEmploymentFunctions.EmploymentWrapper r = ( ATSTalentEmploymentFunctions.EmploymentWrapper)ATSTalentEmploymentFunctions.performServerCall('getWorkHistory',p);
		Test.stopTest();
		}
	}

	@isTest static void test_method_getWorkHistory_not_category1(){
	   String profileName = 'Single Desk 1';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'testlst';
        testUser.Firstname = 'Testy';
        testUser.Alias = 'test';
        testUser.CommunityNickname = '12347';
        profile prof = [ select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
		testUser.OPCO__c = 'MLA';


        insert testUser;
            System.runAs(testUser) {
		Account newAcc = BaseController_Test.createTalentAccount('');
		BaseController_Test.createTalentEmployment(newAcc.Id, true);
		BaseController_Test.createTalentEmployment(newAcc.Id, false);
		BaseController_Test.createTalentEmployment(newAcc.Id, true);
        Test.startTest();
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
        p.put('maxRows', '5000');
		p.put('personaIndicator', 'Recruiter');//S-82159:Added by siva 12/4/2018
		 ATSTalentEmploymentFunctions.EmploymentWrapper r = ( ATSTalentEmploymentFunctions.EmploymentWrapper)ATSTalentEmploymentFunctions.performServerCall('getWorkHistory',p);
		Test.stopTest();
		System.assertEquals(3,r.AWHCount);
		}
	}
   @isTest static void test_method_getCurrencyList(){
      Test.startTest();
        Map<String,Object> p = new Map<String,Object>();
		Map<String, String> r = (Map<String, String>)ATSTalentEmploymentFunctions.performServerCall('getCurrencyList',p);
		Test.stopTest();  
    }
    @isTest static void test_method_getRateFrequencyList(){
        Map<String,Object> p = new Map<String,Object>();
		Map<String, String> r = (Map<String, String>)ATSTalentEmploymentFunctions.performServerCall('getRateFrequencyList',p);
		 
    }
    
    @isTest static void test_method_getWorkHistory_category11(){
	   String profileName = 'Single Desk 1';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'testlst';
        testUser.Firstname = 'Testy';
        testUser.Alias = 'test';
        testUser.CommunityNickname = '12347';
        profile prof = [ select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
		testUser.OPCO__c = 'TEK';


        insert testUser;
            System.runAs(testUser) {
		Account newAcc = BaseController_Test.createTalentAccount('');
		BaseController_Test.createTalentEmployment(newAcc.Id, true);
        Talent_Work_History__c twh=BaseController_Test.createTalentWorkHistory(newAcc.Id);
                twh.SourceId__c = 'R.test';
                Database.update(twh);

		Test.startTest();
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
        p.put('maxRows', '50');
		p.put('personaIndicator', 'Recruiter');//S-82159:Added by siva 12/4/2018
		 ATSTalentEmploymentFunctions.EmploymentWrapper r = ( ATSTalentEmploymentFunctions.EmploymentWrapper)ATSTalentEmploymentFunctions.performServerCall('getWorkHistory',p);
		Test.stopTest();                
		}
	}
}