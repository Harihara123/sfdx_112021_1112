public with sharing class ATSTalentEngagementFunctions {
	public static Object performServerCall(String methodName, Map<String, Object> parameters){
		Object result = null;
		Map<String, Object> p = parameters;

		//Call the method within this class
		if(methodName == 'getScopeTypeList'){
			result = getScopeTypeList();
		}else if(methodName == 'createER'){
			result = createER((String)p.get('sd'),(String)p.get('ed'),(String)p.get('sc'),(String)p.get('des'),(String)p.get('recId'));
		}
		
		return result;
	}

	private static Map<String, String> getScopeTypeList() {
        Map<String, String> scopeTypeMappings = new Map<String,String>();
        Schema.DescribeFieldResult field = Engagement_Rule__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> scopeTypeList = field.getPicklistValues();
        for (Schema.PicklistEntry entry: scopeTypeList) {
            scopeTypeMappings.put(entry.getValue(), entry.getLabel());
        }
        return scopeTypeMappings;
    }

    private static Id createER(String sd,String ed,String sc,String des,String recId) {
       
        Engagement_rule__c ER = new Engagement_rule__c ();
        ER.Account__c = recId;
        ER.Start_Date__c = Date.valueOf(SD);
        ER.Expiration_Date__c = Date.valueOf(ED);
        ER.Type__c = Sc;
        ER.Description__c = Des;
        ER.Active__c = true;
        insert ER;
        return ER.Id;
    }
}