@isTest 
public class ATSTalentEngagementFunctions_Test {

    // Positive test method
    public static testMethod void atsTalentTestMethod() {
        // Insert Dummy Data
        // account in Engagement rule have validation rule for 'Client' recordType
        // get the recordtypeId 
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get('Client').getRecordTypeId();
        
        Date d = Date.today().addDays(2);
        Account newClientAcc = BaseController_Test.createClientAccount();
        Account newAcc = BaseController_Test.createTalentAccount(newClientAcc.Id);
        newAcc.recordtypeid=AccountRecordTypeInfo.get('Client').getRecordTypeId();
        update newAcc;
        // Maintain map
        Map<String, Object> stringObjectMap = new Map<String, Object>();
        stringObjectMap.put('sd','2017-08-21 00:00:00');
        stringObjectMap.put('ed','2017-08-31 00:00:00');
        stringObjectMap.put('sc','');
        stringObjectMap.put('des','');
        stringObjectMap.put('recId',newAcc.Id);
        
        Engagement_Rule__c engRule = new Engagement_Rule__c(Active__c = true
                            ,Description__c='Rule 1'
                            ,Type__c='Associate'
                            ,Account__c = newClientAcc.Id
                            ,Start_Date__c = d.addDays(-4)
                            ,expiration_Date__c=d);
        Test.startTest();
            Database.insert(engRule);
            Object newObject = ATSTalentEngagementFunctions.performServerCall('',null);
            ATSTalentEngagementFunctions.performServerCall('getScopeTypeList',null);
            ATSTalentEngagementFunctions.performServerCall('createER',stringObjectMap);
            System.assertEquals(newObject,null);
        Test.stopTest();
        
    }
    
}