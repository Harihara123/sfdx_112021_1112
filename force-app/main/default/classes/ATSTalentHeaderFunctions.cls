public with sharing class ATSTalentHeaderFunctions {
    public static Object performServerCall(String methodName, Map<String, Object> parameters){
        Object result = null;
        Map<String, Object> p = parameters;

        //Call the method within this class..
        if(methodName == 'getTalentHeaderModel'){
            result = getTalentHeaderModel((String)p.get('recordId'));
        } else if(methodName =='getRelatedActivity'){
            result = getRelatedActivity((String)p.get('contactId'));
        } else if(methodName == 'getDoNotRecruitAggregate'){
            result = getDoNotRecruitAggregate((String)p.get('clientAccountId'));
        } else if(methodName == 'getCandidateCurrentEmployer'){
            result = getCandidateCurrentEmployer((String)p.get('accountId'));
        }
        
        return result;
    }

    private static List<Contact> getRelatedActivity(String contactId) {
        List<Contact> listContact; 
        if (contactId != null && contactId != '') {
             contactId = String.escapeSingleQuotes(contactId);
             listContact =  [SELECT Id, Name,  Account.Customer_Status__c, Customer_Status__c, Account.Name, Account.Do_Not_Contact__c, MobilePhone, OtherPhone, Phone, HomePhone, Email, Other_Email__c, Work_Email__c,
                              (SELECT Id, Activity_Type__c, ActivityDate, LastModifiedDate, Subject FROM Tasks ORDER BY ActivityDate ASC NULLS LAST, LastModifiedDate DESC LIMIT 5),
                              (SELECT Id, Activity_Type__c, ActivityDate, LastModifiedDate, Subject FROM Events ORDER BY ActivityDate ASC NULLS LAST, LastModifiedDate DESC LIMIT 5)
                        FROM Contact where Id =:contactId and RecordType.Name = 'Client'];
        } 
        return listContact; 
    }

    private static ATSTalentHeaderModel getTalentHeaderModel(string recordId){
        ATSTalentHeaderModel thm = new ATSTalentHeaderModel();

        thm.profile = (User)BaseController.getCurrentUser();//
		thm.UserCategory = BaseController.getUserCategory();//D - 06629 Added by akshay on 5/7/18
        thm.contact = getCandidateHeader(recordId);
        thm.userOwnership = (String)BaseController.getCurrentUserWithOwnership().userOwnership;
        /*
        commented by akshay for s- 55186 
        thm.talentPlaced = getTalentPlacedFlag(thm.contact.AccountId);//(recordId);
        
        if (thm.talentPlaced) {
            thm.talentFormer = false;
        }
        else {
            thm.talentFormer = getTalentFormerFlag(recordId);
        }*/

        String talentStreetCityAddress = '';
        talentStreetCityAddress = appendAddress(talentStreetCityAddress, thm.contact.MailingStreet);
        talentStreetCityAddress = appendAddress(talentStreetCityAddress, thm.contact.MailingCity);
        thm.talentStreetCity = talentStreetCityAddress; 
        
        String talentStateCountryZipAddress = '';
        talentStateCountryZipAddress = appendAddress(talentStateCountryZipAddress, thm.contact.MailingState );
        talentStateCountryZipAddress = appendAddress(talentStateCountryZipAddress, thm.contact.MailingPostalCode );
        talentStateCountryZipAddress = appendAddress(talentStateCountryZipAddress, thm.contact.Talent_Country_Text__c );
        thm.talentStateCountryZip = talentStateCountryZipAddress;
        
        thm.doNotContact = thm.contact.Account.Do_Not_Contact__c;

        //Check to ensure that website & linkedin urls are https
        thm.contact.LinkedIn_URL__c = getHttpsUrl(thm.contact.LinkedIn_URL__c);
        thm.contact.Website_URL__c = getHttpsUrl(thm.contact.Website_URL__c);

        return thm;
    }


     @AuraEnabled
    public static ATSTalentHeaderStatusModel getTalentHeaderStatusModel(String recordId, string accountId, String clientAccountId, boolean isMLA){
        ATSTalentHeaderStatusModel thm = new ATSTalentHeaderStatusModel();

        if (isMLA) {
                    thm.talentOwnershipDetails = getTalentOwnershipDetails(accountId);
                    thm.talentOwnership =  (thm.talentOwnershipDetails != null ? true : false);

                    if (thm.talentOwnershipDetails != null) {
                        String duration = thm.talentOwnershipDetails.Duration__c;
                        String ownershipText = '';

                        if (duration == '180 days'){
                             ownershipText = Label.ATS_OWNER;
                         } else if (duration == '365 days') {
                             ownershipText =  Label.ATS_SPG_LFM;                    
                         } else {
                            ownershipText = duration + ': ';
                                }
                         thm.talentOwnershipDetailsText = ownershipText + ' : ' + thm.talentOwnershipDetails.User__r.Name + ', expires: '; 
                      
                    }
                    
                    List<Object> doNotRecruitAggregateList  = getDoNotRecruitAggregate(clientAccountId);
                    If (doNotRecruitAggregateList != null && doNotRecruitAggregateList.size() > 0) {
                         thm.EngagementRuleList =  (List<Object>) doNotRecruitAggregateList[0];
                         thm.EngagementAddress =  (String) (doNotRecruitAggregateList.size() > 1 ?  doNotRecruitAggregateList[1] : '');
                    } 
                    
                    thm.doNotRecruit =  (doNotRecruitAggregateList != null && doNotRecruitAggregateList.size() > 0 ? true : false);

        }

        thm.latestFinishCodeReason = getFinishStatus(accountId);

        return thm;
    }
    





    private static Account getCandidateCurrentEmployer(string accountId){

        Account c =[SELECT  Current_Employer__c, Current_Employer__r.Name, 
                            Current_Employer__r.BillingStreet, Current_Employer__r.BillingCity, 
                            Current_Employer__r.BillingState, Current_Employer__r.BillingCountry
                    FROM Account where Id=: accountId];
        return c;
    }



    private static Contact getCandidateHeader(string recordId){
        Contact c =[SELECT FirstName, LastName, Description, Title, 
                Phone, Email, MailingStreet, MailingCity, 
                MailingState, MailingPostalCode, MailingCountry , Talent_Country_Text__c,
                Suffix, Salutation, Website_URL__c, Full_Name__c, 
                Preferred_Phone__c, Preferred_Phone_Value__c, Preferred_Email__c, Preferred_Name__c,
                Preferred_Email_Value__c, HomePhone, MobilePhone, OtherPhone, Work_Email__c,
                Other_Email__c, AccountId, Account.Name, Id, Name, 
                LastModifiedBy.Name, LastModifiedDate, Related_Contact__c, 
                Related_Contact__r.Name, Related_Contact__r.Id, 
                Related_Contact__r.Account.Do_Not_Contact__c, Related_Contact__r.Company_Name__c, 
                Related_Contact__r.Account.Customer_Status__c, Related_Contact__r.AccountId, 
                Related_Contact__r.MobilePhone,Related_Contact__r.OtherPhone,Related_Contact__r.Phone,Related_Contact__r.Email,Related_Contact__r.Other_Email__c,//Added Related COntact fields for ATs-3220 - akshay
                Related_Contact__r.Account.Name, Related_Contact__r.Account.Id, Company_Name__c, 
                Account.Current_Employer__c,  Account.Current_Employer__r.Name, 
                Account.Current_Employer__r.BillingStreet, Account.Current_Employer__r.BillingCity, 
                Account.Current_Employer__r.BillingState, Account.Current_Employer__r.BillingCountry, 
                Last_activity_Date__c, Do_Not_Contact__c, Account.Id, Account.Customer_Status__c, 
                Account.Phone, Account.Do_Not_Contact__c, Account.Talent_Status__c, 
                Account.Talent_Id__c, Account.Candidate_Status__c,
                Account.Talent_Profile_Last_Modified_By__c, Account.Talent_Profile_Last_Modified_By__r.Name,  
                Account.Talent_Profile_Last_Modified_Date__c, Account.Talent_Source_Other__c, 
                Account.Talent_Race__c, Account.Talent_Gender__c, LinkedIn_URL__c, 
                Preferred_URL_Value__c, Account.Talent_Current_Employer_Text__c,  Account.Talent_End_Date__c,
                Account.Talent_Ownership__c,Account.Talent_Community_Status__c,Account.Peoplesoft_ID__c,RWS_DNC__c, // added by akshay for S - 55186
                LinkedIn_Person_URN__c
        FROM Contact where Id=: recordId];//changed by akshay to Id from AccountId 
        return c;
    }

    private static List<Object> getDoNotRecruitAggregate(String clientAccountId) {
        List<Object> resultList = new List<Object>();
        AggregateResult[] groupedResults = null; 
        List<Engagement_Rule__c> engagemeneRule; 
        if (clientAccountId != null && clientAccountId != '') {
               clientAccountId = String.escapeSingleQuotes(clientAccountId);
               groupedResults = [SELECT max(expiration_Date__c), Type__c  
                                            FROM Engagement_Rule__c
                                                    where Account__c =:clientAccountId 
                                                        and Active__c = true 
                                                        and expiration_Date__c > YESTERDAY
                                                    GROUP BY  Type__c];
            
            if (groupedResults != null){
                if(groupedResults.size() > 0 ) {
                    resultList.add(groupedResults);
                    engagemeneRule = [SELECT  Account_City__c, Account_Country__c, Account_State__c FROM Engagement_Rule__c
                                                        where Account__c =:clientAccountId 
                                                            and Active__c = true 
                                                            and expiration_Date__c > YESTERDAY
                                                            LIMIT 1];
                    if (engagemeneRule != null && engagemeneRule.size() > 0) {
                        String addressStr = '';
                        addressStr = appendAddress(addressStr, engagemeneRule[0].Account_City__c);
                        addressStr = appendAddress(addressStr, engagemeneRule[0].Account_Country__c);
                        addressStr = appendAddress(addressStr, engagemeneRule[0].Account_State__c);
                        resultList.add(addressStr);
                    }
                }
            }
            
        }
        return resultList;
    }

    private static Target_Account__c getTalentOwnershipDetails(string recordId){
        Target_Account__c ta = null;
        List<Target_Account__c> tl = [SELECT Duration__c,Expiration_Date__c, User__R.Name  
                                                    FROM Target_Account__c 
                                                            where Account__c =:recordId 
                                                                and  Expiration_Date__c > Yesterday Limit 1];   

        if(tl != null){
            if(tl.size() > 0){
                ta = tl[0];
            }
        } 

        return ta;     
    }
    /*
        commented by akshay for s- 55186 
    private static Boolean getTalentPlacedFlag(String recordId) {
        Integer c = [SELECT COUNT()  FROM Talent_Experience__c 
                        where Talent__c =:recordId 
                            and  Allegis_Placement__c = true 
                            and  Current_Assignment__c = true];               
        if(c > 0){
            return true;
        }else{
            return false;
        }
  }  

   private static Boolean getTalentFormerFlag(String recordId) {
        Integer formerCount = [SELECT COUNT()  FROM Talent_Experience__c 
                               where Talent__c =:recordId 
                               and Allegis_Placement__c = true
                               and Current_Assignment__c = false];
        

        if (formerCount > 0) {
            return true;
        }
        else {            
            return false; 
        }
    }
   */
    private static String getHttpsUrl(String url){
        if(url != null){
            if(url != ''){
                if(!url.startsWith('http')){
                    url = 'http://' + url;
                }
            }
        }
        return url;
    }
                   
    private static String appendAddress(String address, String appendAddress) {     
        if (String.isNotBlank(appendAddress)) {
            address = (String.isNotBlank(address) ? ( address + ', ' + appendAddress) : appendAddress);
        }  
        return address;
   }

   private static Map<string, string> getFinishStatus(string recordId) {
        Map<string, string> codeReason = new Map<string, string>();
        try {
            Talent_Work_History__c twh = [SELECT Id, End_Date__c, Finish_Code__c, Finish_Reason__c FROM Talent_Work_History__c WHERE Talent__c = :recordId ORDER BY End_Date__c DESC LIMIT 1];
            
            if (twh.Finish_Code__c != null && twh.Finish_Reason__c != null) {
                codeReason.put(twh.Finish_Code__c, twh.Finish_Reason__c);
            }
        }
        catch (Exception ex){
            
        }
        
        return codeReason;
    }    


    @AuraEnabled
    public static Talent_Work_History__c[] getCandidateWorkExEndDateInfo(string recordId) {
       
     //System.debug(logginglevel.WARN, 'recordId: ======================>>>>>>>:' + recordId);
     return [SELECT Id,SourceCompany__c,Job_Title__c, End_Date__c, Start_Date__c,PS_last_change_req_date__c
                                                    FROM Talent_Work_History__c 
                                                    WHERE Talent__c = :recordId and SourceId__c like 'R.%'
                                                    and (Current_Assignment_Formula__c = true OR finish_code__c = '.')
                                                    ORDER BY End_Date__c DESC limit 20];
    }   
    @AuraEnabled
    public static String invokePSEndDateChangeRequest(Id twhId,String endDate){
		return PSEndDateExtendController.sendPositionEndDateChangeRequest(twhId,endDate);
    } 
}