@isTest
private class ATSTalentHeaderFunctions_Test {
    @isTest static void test_performServerCall() {
        //No Params Entered...
        Object r = ATSTalentHeaderFunctions.performServerCall('',null);
        System.assertEquals(r,null);
    }
    
    @isTest(SeeAllData=true) static void test_method_getTalentHeaderModel(){
        Date d = Date.today().addDays(2);
        Account newClientAcc = BaseController_Test.createClientAccount();
        Account newAcc = BaseController_Test.createTalentAccount(newClientAcc.Id);
        Contact newC = BaseController_Test.createTalentContact(newAcc.Id);

        Engagement_Rule__c er = new Engagement_Rule__c(Active__c = true
                                                        ,Description__c='Rule 1'
                                                        ,Type__c='Associate'
                                                        ,Account__c = newClientAcc.Id
                                                        ,Start_Date__c = d.addDays(-4)
                                                        ,expiration_Date__c=d);
        Database.insert(er);

        Target_Account__c ta = new Target_Account__c(Account__c = newAcc.Id
                                                    ,Expiration_Date__c = d 
                                                    ,Duration__c='180 days'
                                                    ,User__c=UserInfo.getUserId());
        Database.insert(ta);

        Test.startTest();
        Map<String,Object> p = new Map<String, Object>();
        p.put('recordId', newC.Id);

    

        ATSTalentHeaderModel t = (ATSTalentHeaderModel)ATSTalentHeaderFunctions.performServerCall('getTalentHeaderModel',p);
        //System.assertNotEquals(null, t);

        Talent_Experience__c te = new Talent_Experience__c(Allegis_Placement__c = true
                                                        ,Talent__c=newAcc.Id
                                                        ,Current_Assignment__c = true);
        Database.insert(te);

        ta.Duration__c = '365 days';
        Database.Upsert(ta);


        t = (ATSTalentHeaderModel)ATSTalentHeaderFunctions.performServerCall('getTalentHeaderModel',p);
        System.assertNotEquals(null, t);

        delete ta;

        Target_Account__c ta3 = new Target_Account__c(Account__c = newAcc.Id
                                                    ,Expiration_Date__c = d 
                                                    ,User__c=UserInfo.getUserId());
        Database.insert(ta3);


        t = (ATSTalentHeaderModel)ATSTalentHeaderFunctions.performServerCall('getTalentHeaderModel',p);
        System.assertNotEquals(null, t);

        Test.stopTest();
    }
    
    
    @isTest
    public static void verifyCandidateStatusTest(){
    
         
       
         Account newAcc = BaseController_Test.createTalentAccount('');
         Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
        
         Account account = [Select Id, Candidate_Status__c from Account where Id=:newAcc.Id];
         
         System.assertNotEquals(null, account.Candidate_Status__c, 'Candidate Status should not be empty');
         System.assertEquals('Candidate', account.Candidate_Status__c);
    
    }
    @isTest
    public static void getDoNotRecruitAggregateTest(){
        Date d = Date.today().addDays(2);
        Account newClientAcc = BaseController_Test.createClientAccount();
		newClientAcc.BillingCity = 'Test City';
		newClientAcc.BillingCountry = 'Test USA';
		newClientAcc.BillingState = 'TD';
		update newClientAcc;
        Account newAcc = BaseController_Test.createTalentAccount(newClientAcc.Id);
        Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
        Engagement_Rule__c er = new Engagement_Rule__c(Active__c = true
                                                        ,Description__c='Rule 1'
                                                        ,Type__c='Associate'
                                                        ,Account__c = newClientAcc.Id
                                                        ,Start_Date__c = d.addDays(-4)
                                                        ,expiration_Date__c=d);
        Database.insert(er);

        Target_Account__c ta = new Target_Account__c(Account__c = newAcc.Id
                                                    ,Expiration_Date__c = d 
                                                    ,Duration__c='180 days'
                                                    ,User__c=UserInfo.getUserId());
		Database.insert(ta);

        Test.startTest();

        Map<String, Object> param = new Map<String,Object>();
        param.put('clientAccountId', newClientAcc.id);//getDoNotRecruitAggregate'
		Object r = ATSTalentHeaderFunctions.performServerCall('getDoNotRecruitAggregate',param);
        Test.stopTest();
        System.assertNotEquals(null,r);
        
    }
    @isTest
    public static void getTalentHeaderStatusModelTest(){
        Date d = Date.today().addDays(2);
        Account newClientAcc = BaseController_Test.createClientAccount();
        Account newAcc = BaseController_Test.createTalentAccount(newClientAcc.Id);
        Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
		Talent_Work_History__c twh = CreateTalentTestData.createWorkHistory(newAcc);
		twh.Finish_Code__c='testcode'; 
		twh.Finish_Reason__c='test';
		update twh;
		 Engagement_Rule__c er = new Engagement_Rule__c(Active__c = true
                                                        ,Description__c='Rule 1'
                                                        ,Type__c='Associate'
                                                        ,Account__c = newClientAcc.Id
                                                        ,Start_Date__c = d.addDays(-4)
                                                        ,expiration_Date__c=d);
        Database.insert(er);

        Target_Account__c ta = new Target_Account__c(Account__c = newAcc.Id
                                                    ,Expiration_Date__c = d 
                                                    ,Duration__c='180 days'
                                                    ,User__c=UserInfo.getUserId());
		Database.insert(ta);
        Test.startTest();
        ATSTalentHeaderStatusModel statusModal = ATSTalentHeaderFunctions.getTalentHeaderStatusModel(newC.id, newAcc.id, newClientAcc.id, true);
        Test.stopTest();
    }
    @isTest
    public static void getCandidateWorkExEndDateInfoTest(){
        Date d = Date.today().addDays(2);
        Account newClientAcc = BaseController_Test.createClientAccount();
        Account newAcc = BaseController_Test.createTalentAccount(newClientAcc.Id);
        Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
        Test.startTest();
        Talent_Work_History__c[] twhList = ATSTalentHeaderFunctions.getCandidateWorkExEndDateInfo(newC.id);
        Test.stopTest();

    }
}