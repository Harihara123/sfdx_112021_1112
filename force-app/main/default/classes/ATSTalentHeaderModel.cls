global class ATSTalentHeaderModel {
	@AuraEnabled
    public User profile {get;set;}

	@AuraEnabled
    public String UserCategory {get;set;}//D - 06629 Added by akshay on 5/7/18

    @AuraEnabled
    public Contact contact {get;set;}
    
    @AuraEnabled
    public Boolean doNotContact {get;set;}

	//@AuraEnabled
    //public Boolean talentPlaced {get;set;} //field is not in use

    //@AuraEnabled
    //public Boolean talentFormer {get;set;} //field is not in use
    
    @AuraEnabled
    public String talentStreetCity {get;set;}
    
    @AuraEnabled
    public String talentStateCountryZip {get;set;}

    //@AuraEnabled
    //public List<Contact> relatedActivity {get;set;} //field is not in use

    @AuraEnabled
    public String userOwnership {get;set;}
}