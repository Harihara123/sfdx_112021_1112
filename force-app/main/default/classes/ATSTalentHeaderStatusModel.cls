global class ATSTalentHeaderStatusModel {
    
    @AuraEnabled
    public List<Object> doNotRecruitAggregate {get;set;}
    
    @AuraEnabled
    public List<Object> engagementRuleList {get;set;} 
    
    @AuraEnabled
    public String  engagementAddress {get;set;}

    @AuraEnabled
    public Boolean doNotRecruit {get;set;}

    
    @AuraEnabled
    public Target_Account__c talentOwnershipDetails {get;set;}
    
    @AuraEnabled
    public String talentOwnershipDetailsText {get;set;}
    
  
    @AuraEnabled
    public Boolean talentOwnership {get;set;}


    @AuraEnabled
    public Map<string, string> latestFinishCodeReason {
        get;
        
        set{
            Map<string, string> newMap = value;
            latestFinishCodeReason = newMap;
            
            if (!newMap.isEmpty()) {
                for (string s : newMap.keySet()) {
                    latestFinishCode = s;
                    latestFinishReason = newMap.get(s);
                    break;
                }
            }
        }
    }
    
    @AuraEnabled
    public string latestFinishCode {get;set;}
    
    @AuraEnabled
    public string latestFinishReason {get;set;}
}