public with sharing class ATSTalentMarketingEmailFunctions {
	public static Object performServerCall(String methodName, Map<String, Object> parameters){
		Object result = null;
		Map<String, Object> p = parameters;

		//Call the method within this class
		if(methodName == 'getMarketingEmails'){
			result = getMarketingEmails((String)p.get('recordId'));
		}
		
		return result;
	}


	private static List<MarketingEmailSendsModel> getMarketingEmails(String accountId) {
		Contact c = [SELECT Id FROM Contact WHERE AccountId =: accountId LIMIT 1];
		List<MarketingEmailSendsModel> meList = new List<MarketingEmailSendsModel>();

		String contactId = (String)c.Id;

		if(contactId != ''){
			Integer totalItems = [SELECT Count() FROM et4ae5__IndividualEmailResult__c WHERE et4ae5__SendDefinition__c != '' AND et4ae5__Contact__c =: contactId];

			if(totalItems > 0){
				List<et4ae5__IndividualEmailResult__c> el = [	SELECT Id, et4ae5__DateSent__c,
							et4ae5__FromAddress__c,
							et4ae5__FromName__c,
							et4ae5__Opened__c,
							et4ae5__SubjectLine__c,
							Name,
							et4ae5__SendDefinition__c,
							et4ae5__SendDefinition__r.Name,
							et4ae5__SendDefinition__r.et4ae5__EmailName__c,
							et4ae5__SendDefinition__r.et4ae5__Campaign__r.Name,
							et4ae5__Contact__c
						FROM et4ae5__IndividualEmailResult__c 
						WHERE et4ae5__SendDefinition__c != '' AND et4ae5__Contact__c =: contactId LIMIT 5];	

				for(et4ae5__IndividualEmailResult__c e : el){
					MarketingEmailSendsModel mes = new MarketingEmailSendsModel();
					mes.contactId = contactId;
					mes.emailID = e.Id;
					mes.subjectLine = e.et4ae5__SubjectLine__c;
					mes.fromName = e.et4ae5__FromName__c;
					mes.fromEmail = e.et4ae5__FromAddress__c;
					mes.dateSent = e.et4ae5__DateSent__c;
					mes.emailSendNumber = e.et4ae5__SendDefinition__r.Name;
					mes.emailName = e.et4ae5__SendDefinition__r.et4ae5__EmailName__c;
					mes.campaignName = e.et4ae5__SendDefinition__r.et4ae5__Campaign__r.Name;
					
					Boolean eopened = e.et4ae5__Opened__c;
					if(eopened){
						mes.opened = 'Yes';
					}else{
						mes.opened = 'No';
					}

					meList.add(mes);
				}

				if(meList.size() > 0){
					meList[0].totalItems = totalItems;
				}
			}
		}

		return meList;
	}
}