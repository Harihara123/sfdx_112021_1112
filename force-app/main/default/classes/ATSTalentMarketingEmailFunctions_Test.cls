@isTest public class ATSTalentMarketingEmailFunctions_Test {
	@isTest static void test_performServerCall() {
		//No Params Entered
		Object r = ATSTalentMarketingEmailFunctions.performServerCall('',null);
		System.assertEquals(r,null);
	}

	@isTest static void test_method_getMarketingEmails(){
		Account newAcc = BaseController_Test.createTalentAccount('');
		Contact newC = BaseController_Test.createTalentContact(newAcc.Id);
		                                                           
		BaseController_Test.createTestMarketingEmails(newC.Id);


		Test.startTest();
		Map<String,Object> p = new Map<String,Object>();
		p.put('recordId', newAcc.Id);
		List<MarketingEmailSendsModel> t1 = (List<MarketingEmailSendsModel>)ATSTalentMarketingEmailFunctions.performServerCall('getMarketingEmails',p);
		Test.stopTest();
		System.assertEquals(1, t1.size());
	}
}