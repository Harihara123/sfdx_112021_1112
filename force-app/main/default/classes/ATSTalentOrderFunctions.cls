public class ATSTalentOrderFunctions  {
	
	public static Object performServerCall(String methodName, Map<String, Object> parameters){
		Object result = null;
		Map<String, Object> p = parameters;

		//Call the method within this class
		if(methodName == 'getApplicantsFromJobPosting'){
			result = getApplicantsFromJobPosting((String)p.get('recordId'));
		}else if(methodName == 'updateDisposition'){
			     updateDisposition((String)p.get('evtId'),(String)p.get('Code'),(String)p.get('Reason'));
		}
		
		return result;
	}

	private static List<ATSJPSubmittal> getApplicantsFromJobPosting(string JPId) {
        Map <Id,event> oid = new Map<Id,event> (); 
        Map<Id,Id> JobPostingId = new Map<Id,Id>();
        list<datetime> JpDate = new list<datetime>();
        String oppOpco;
        String oppReqDivision;
        Boolean isOFCCP;
        //Map <Id,String> orderJobTitles = new Map<Id,String>();
        if(JPId.startsWith('a1y')){
            Map <Id,ATSJPSubmittal> talAccMap;
            for(Event oe : [Select Id,WhatId,WhoId,JB_Source_Name__c,Application_Id__c,Vendor_Application_ID__c ,TalentDocumentId__c, Job_Posting__r.Job_Title__c,Feed_Source_ID__c,Job_Posting__r.Source_System_id__c,Job_Posting__r.Name,Job_Posting__r.createdBy.Id,Job_Posting__r.createdBy.FirstName, Job_Posting__r.createdBy.LastName, Job_Posting__r.createdBy.Email, Job_Posting__r.createdBy.phone,Job_Posting__r.createdBy.title, toLabel(Disposition_Code__c), toLabel(Disposition_Reason__c), Job_Posting__r.createddate, Job_Posting__r.OFCCP_Flag__c from Event where Job_Posting__c = :JPId AND isDeleted = false ALL ROWS]){
                oid.put(oe.WhatId,oe); 
                JpDate.add(oe.Job_Posting__r.createddate);                
                //orderJobTitles.put(oe.WhatId,oe.Job_Posting__r.Job_Title__c);
            }
            if(!oid.isEmpty()){
                talAccMap = new Map <Id,ATSJPSubmittal>();
                for(Order o : [SELECT Id, ShipToContactId,RecordTypeId, CreatedById,Display_LastModifiedDate__c, Status, Has_Application__c, EffectiveDate  ,ShipToContact.Name, ShipToContact.firstName, ShipToContact.lastName  ,ShipToContact.MailingStreet,ShipToContact.MailingCity,ShipToContact.MailingState,ShipToContact.MailingCountry,ShipToContact.MailingPostalCode, ShipToContact.Title,ShiptoContact.AccountId,ShipToContact.HomePhone,ShipToContact.Email,toLabel(ShipToContact.CS_Desired_Salary_Rate__c),ShipToContact.CS_Salary__c,Account.Name,Opportunity.Account.Name, Opportunity.Account_City__c, Opportunity.Account_State__c, Opportunity.Req_Job_Title__c, Opportunity.Req_Client_Job_Title__r.Name,Opportunity.RecordType.Name,  Opportunity.OpCo__c, Opportunity.Req_Division__c, Opportunity.Req_OpCo_Code__c, Opportunity.Req_Product__c, Opportunity.Currency__c, Opportunity.Req_Total_Filled__c, Opportunity.Req_Open_Positions__c, Opportunity.Req_Total_Positions__c,  Opportunity.Req_Total_Lost__c ,Opportunity.Req_Total_Washed__c,Opportunity.Req_OFCCP_Required__c,Opportunity.Owner.Id,Opportunity.Owner.FirstName, Opportunity.Owner.LastName, Opportunity.Owner.email, Opportunity.Owner.phone,Opportunity.Owner.title,(select Id,Source__c from Events), Email_Type__c, Unique_Id__c, RecordType.DeveloperName FROM Order WHERE Has_Application__c = true AND Id IN : oid.keySet() ORDER BY Display_LastModifiedDate__c DESC]){
                    Event event = oid.get(o.Id);
                    ATSJPSubmittal jpo = new ATSJPSubmittal ();
                    jpo.jpOrder = o;
                    jpo.talId = o.ShipToContact.AccountId;//talAccMap.get(tal);
                    jpo.event = event;
                    jpo.jobPostingId = JPId;
                    jpo.jobTitle = event.Job_Posting__r.Job_Title__c;
                    if(event.Job_Posting__r.OFCCP_Flag__c){
                        isOFCCP = true;
                    }else{
                        isOFCCP = false;
                    }
                    talAccMap.put(jpo.talId,jpo);
                    oppOpco = o.Opportunity.Opco__c;
                    oppReqDivision = o.Opportunity.Req_Division__c;
                }
                if(talAccMap != null){
                    // making common logic not checking for any specific resume type,
                    // if talent document is present in event object save it else pick the most recent talent document.
                   List<Talent_Document__c> talentDocumentList ;
                    if (isOFCCP) {
                        talentDocumentList =[select id, Talent__c, (SELECT id from Attachments LIMIT 1) 
                                                         from Talent_Document__c 
                                                         where  Talent__c IN :talAccMap.keySet() AND
                                                         (Document_Type__c ='Resume - OFCCP')         
                                                         AND HTML_Version_Available__c  = true WITH SECURITY_ENFORCED ORDER by CreatedDate ];
                    }
                    else{
                        talentDocumentList =[select id, Talent__c, (SELECT id from Attachments LIMIT 1) 
                                                         from Talent_Document__c 
                                                         where  Talent__c IN :talAccMap.keySet() AND
                                                         (Document_Type__c ='Resume - Job Board' OR Document_Type__c ='Resume - OFCCP')         
                                                         AND HTML_Version_Available__c  = true WITH SECURITY_ENFORCED ORDER by CreatedDate];
                    }
                    
                    for (Talent_Document__c tdatt : talentDocumentList){
                        if (isOFCCP) {
                            if(talAccMap.get(tdatt.Talent__c).event.TalentDocumentId__c == tdatt.id){   
                                talAccMap.get(tdatt.Talent__c).talDocId = tdatt.Attachments[0].Id;
                        	}
                        }    
                        else{
                            	talAccMap.get(tdatt.Talent__c).talDocId  = tdatt.Attachments[0].Id;
                        	}                               
                	}
                     
                    
                    for(task t : [select id, type, whatid 
                                  from task 
                                  where createddate >= : JpDate and whatid in : talAccMap.keySet() and createdbyId = : UserInfo.getUserId() and type != null
                                  order by LastModifiedDate desc]){
                                      talAccMap.get(t.whatid).task = t ;
                                  }
                }
                system.debug('--talAccMap.values()--'+talAccMap.values());
                return talAccMap.values();                
            }
        }else if(JPId.startsWith('006')){
            Map <Id,ATSJPSubmittal> talAccMap;
            list<id> JPIds = new list<id>();
            for(Job_Posting__c j : [select id, Opportunity__c from Job_Posting__c where Opportunity__c =:JPId]){
                JPIds.add(j.id);
            }
            for(Event oe : [Select Id,WhatId,WhoId,JB_Source_Name__c,Application_Id__c,Vendor_Application_ID__c ,TalentDocumentId__c, Job_Posting__r.Job_Title__c,Feed_Source_ID__c,Job_Posting__r.Source_System_id__c,Job_Posting__r.Name,Job_Posting__r.createdBy.Id,Job_Posting__r.createdBy.FirstName, Job_Posting__r.createdBy.LastName, Job_Posting__r.createdBy.Email, Job_Posting__r.createdBy.phone,Job_Posting__r.createdBy.title, toLabel(Disposition_Code__c), toLabel(Disposition_Reason__c), Job_Posting__r.createddate, Job_Posting__r.OFCCP_Flag__c from Event where Job_Posting__c IN:JPIds AND isDeleted = false ALL ROWS]){
                oid.put(oe.WhatId,oe); 
                JpDate.add(oe.Job_Posting__r.createddate);
                JobPostingId.put(oe.WhatId,oe.Job_Posting__c);
                //orderJobTitles.put(oe.WhatId,oe.Job_Posting__r.Job_Title__c);
            }
            system.debug('--oid--'+oid);
            if(!oid.isEmpty()){
                talAccMap = new Map <Id,ATSJPSubmittal>();
                for(Order o : [SELECT Id, ShipToContactId,RecordTypeId, CreatedById,Display_LastModifiedDate__c, Status, Has_Application__c, EffectiveDate  ,ShipToContact.Name, ShipToContact.firstName, ShipToContact.lastName  ,ShipToContact.MailingStreet,ShipToContact.MailingCity,ShipToContact.MailingState,ShipToContact.MailingCountry,ShipToContact.MailingPostalCode, ShipToContact.Title,ShiptoContact.AccountId,ShipToContact.HomePhone,ShipToContact.Email,toLabel(ShipToContact.CS_Desired_Salary_Rate__c),ShipToContact.CS_Salary__c,Account.Name,Opportunity.Account.Name, Opportunity.Account_City__c, Opportunity.Account_State__c, Opportunity.Req_Job_Title__c, Opportunity.Req_Client_Job_Title__r.Name,Opportunity.RecordType.Name,  Opportunity.OpCo__c, Opportunity.Req_Division__c, Opportunity.Req_OpCo_Code__c, Opportunity.Req_Product__c, Opportunity.Currency__c, Opportunity.Req_Total_Filled__c, Opportunity.Req_Open_Positions__c, Opportunity.Req_Total_Positions__c,  Opportunity.Req_Total_Lost__c ,Opportunity.Req_Total_Washed__c,Opportunity.Req_OFCCP_Required__c,Opportunity.Owner.Id,Opportunity.Owner.FirstName, Opportunity.Owner.LastName, Opportunity.Owner.email, Opportunity.Owner.phone,Opportunity.Owner.title,(select Id,Source__c from Events), Email_Type__c, Unique_Id__c, RecordType.DeveloperName FROM Order WHERE Has_Application__c = true AND OpportunityId =:JPId and Status = 'Applicant' ORDER BY Display_LastModifiedDate__c DESC]){
					if(oid.get(o.Id)!=null){
						Event event = oid.get(o.Id);
						ATSJPSubmittal jpo = new ATSJPSubmittal ();
						jpo.jpOrder = o;
						jpo.talId = o.ShipToContact.AccountId;//talAccMap.get(tal);
						jpo.event = event;
						jpo.jobPostingId = JobPostingId.get(o.id);
						jpo.jobTitle = event.Job_Posting__r.Job_Title__c;
						if(event.Job_Posting__r.OFCCP_Flag__c){
							isOFCCP = true;
						}else{
							isOFCCP = false;
						}
						talAccMap.put(jpo.talId,jpo);
						oppOpco = o.Opportunity.Opco__c;
						oppReqDivision = o.Opportunity.Req_Division__c;
					}
                }
                if(talAccMap != null){
                    /* commenting for defect D-13428
                    Boolean isPPEnabled = JobPostingController.isPPEnabledOpco(oppOpco, oppReqDivision);
                    if (isOFCCP && isPPEnabled) { 
                        for (Talent_Document__c tdatt : [select id, Talent__c, (SELECT id from Attachments LIMIT 1) 
                                                         from Talent_Document__c 
                                                         where Document_Type__c = 'Resume - OFCCP' 
                                                         AND Talent__c IN :talAccMap.keySet() 
                                                         AND HTML_Version_Available__c  = true]){
                                                             if(talAccMap.get(tdatt.Talent__c).event.TalentDocumentId__c == tdatt.id){
                                                                 talAccMap.get(tdatt.Talent__c).talDocId = tdatt.Attachments[0].Id;
                                                             }
                                                         }
                    } else {
                        for (Talent_Document__c  tdatt : [Select Id,Talent__c,HTML_Version_Available__c,(Select Id from Attachments LIMIT 1) 
                                                          from Talent_Document__c 
                                                          Where Document_Type__c ='Resume - Job Board'	
                                                          and Talent__c IN :talAccMap.keySet() 
                                                          AND HTML_Version_Available__c  = true]){
                                                              talAccMap.get(tdatt.Talent__c).talDocId  = tdatt.Attachments[0].Id;
                                                          }
                    }
                   */
                    // making common logic not checking for any specific resume type,
                    // if talent document is present in event object save it else pick the most recent talent document.
                     List<Talent_Document__c> talentDocumentList =[select id, Talent__c, (SELECT id from Attachments LIMIT 1) 
                                                         from Talent_Document__c 
                                                         where  Talent__c IN :talAccMap.keySet() AND
                                                         (Document_Type__c ='Resume - Job Board' OR Document_Type__c ='Resume - OFCCP')         
                                                         AND HTML_Version_Available__c  = true];
                   
                    for (Talent_Document__c tdatt : talentDocumentList){
                            if(talAccMap.get(tdatt.Talent__c).event.TalentDocumentId__c == tdatt.id){
                                talAccMap.get(tdatt.Talent__c).talDocId = tdatt.Attachments[0].Id;
                        	}else{
                            	talAccMap.get(tdatt.Talent__c).talDocId  = tdatt.Attachments[0].Id;
                        	}                               
                	}
                    
                    
                    for(task t : [select id, type, whatid 
                                  from task 
                                  where createddate >= : JpDate and whatid in : talAccMap.keySet() and createdbyId = : UserInfo.getUserId() and type != null
                                  order by LastModifiedDate desc]){
                                      talAccMap.get(t.whatid).task = t ;
                                  }
                }
                system.debug('--talAccMap.values()--'+talAccMap.values());
                return talAccMap.values();
            }
        }
        return null;
    }
    
    @future(Callout=true)
    public static void updateDisposition ( Id evtId,String Code,String Reason ){
    if(evtId != null){
       Event evt= [Select Id,Feed_Source_ID__c,Vendor_Application_ID__c from Event where Id=:evtId];
       System.debug(evt);
       List<SubmittalStatusController.MCEmail> mcemlist = new List<SubmittalStatusController.MCEmail>();
       SubmittalStatusController.MCEmail mcem = new SubmittalStatusController.MCEmail();
       mcem.vendorId=evt.Feed_Source_ID__c;
       mcem.applicationId=evt.Vendor_Application_ID__c;
       mcem.eventId = evtId;
       mcemlist.add(mcem);
       //SubmittalStatusController.saveMassDisposition(JSON.serialize(mcemlist),Code,Reason);
	   if(!Test.isRunningTest()){
		SubmittalStatusController.saveMassDisposition(JSON.serialize(mcemlist),Code,Reason);
       }
    }
    
    }

    public class ATSJPSubmittal {
        
         @AuraEnabled public Order jpOrder;
         @AuraEnabled public Id talDocId;
         @AuraEnabled public Id talId;
         @AuraEnabled public Event event ;
         @AuraEnabled public Id jobPostingId; 
         @AuraEnabled public String jobTitle;
         @AuraEnabled public Boolean selected;
         @AuraEnabled public Task task ;
    }
}