/*This class is called from email service : ATSTalentViaEmail
  This class gets attachment from email and save record to Application_Staging__c
  and Attachment.
  Current expectation is one resume per email.
*/
global class ATSTalentViaEmail implements Messaging.InboundEmailHandler {
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		String recruitEmail;
		List<Application_Staging__c> stagingRecordList = new List<Application_Staging__c> ();
		Set<String> jpSet = new Set<String>();
		List<Attachment> attachmentList = new List<Attachment> ();

		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
		if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
			


			for (integer i = 0; i<email.binaryAttachments.size(); i++) {
				String fileName = email.binaryAttachments[i].filename;
				Boolean ValidFileType = FileController.validateFileType(fileName);
				

				if (ValidFileType) {
					Application_Staging__c stagingObject = new Application_Staging__c();
					Attachment attachObject = new Attachment();
					if (fileName.containsIgnoreCase('-Covering_Letter.txt')) {
						
						continue;
					}
					try {

						stagingObject.File_Name__c = fileName.replaceAll('[^a-zA-Z0-9\\s-_.]', '');
						stagingObject.Content_Type__c = email.binaryAttachments[i].mimeTypeSubType;
						if (email.subject.containsIgnoreCase('CV Forwarded by')) {
							List<String> str = email.subject.split('/');
							List<String> str1 = str[0].toLowerCase().split('cv forwarded by');
							recruitEmail = str1[1].trim();
							stagingObject.RecruiterEmail__c = str1[1].trim();
							
						} else {
							
							for (String recAddress : email.toAddresses) {
								if (recAddress.containsIgnoreCase('__cv')) {
									stagingObject.RecruiterEmail__c = recAddress.remove('__cv');
									recruitEmail = recAddress.remove('__cv');
								}

							}

							if (String.isBlank(stagingObject.RecruiterEmail__c))
							{
								stagingObject.RecruiterEmail__c = envelope.fromAddress;
								recruitEmail = envelope.fromAddress;
							}
						}
						
						if (!String.isBlank(email.subject)) {
							//carrersite application email subject parsing for story S-220671
							if(email.subject.containsIgnoreCase('JP-')){
								
								List<String> s1 = email.subject.split('to:');
								List<String> s2 = s1[1].split('from');
								jpSet.add(s2[0].trim());
			
								stagingObject.PostingID__c = s2[0].trim();
								stagingObject.Talent_Source__c = s2[1].trim();
								
								
							}else{
								List<String> talentSource = email.subject.split('from');
								if (talentSource.size()> 1) {
									stagingObject.Talent_Source__c = talentSource[1] + ' - Job Posting';
								}
								else
								{
									List<String> tSource = email.subject.split('/');

									if (tSource.size() > 2) {
										System.debug(tSource[2]);
										stagingObject.Talent_Source__c = tSource[2].replaceAll('[^a-zA-Z0-9\\s+]', '') + ' - Job Posting';
									}
								}
							}
							

						}
						
						attachObject.Body = email.binaryAttachments[i].body;
						
						attachObject.Name = fileName.replaceAll('[^a-zA-Z0-9\\s-_.]', '');
						attachObject.ContentType = email.binaryAttachments[i].mimeTypeSubType;

					}
					catch(Exception excp) {
						
						ConnectedLog.LogException('ATSTalentViaEmail', 'handleInboundEmail', excp);
					}
					stagingRecordList.add(stagingObject);
					attachmentList.add(attachObject);
				} 
			}

			try {
				List<Job_Posting__c> jbList =[select Name, OwnerId, Owner.Email, Owner.Name from Job_Posting__c where Name IN: jpSet WITH SECURITY_ENFORCED];
				System.debug(jbList);
				Map<String,Job_Posting__c> jpMap = new Map<String,Job_Posting__c>();
				for(Job_Posting__c jp: jbList){
					jpMap.put(jp.Name,jp);
				}
				if(jbList.size()>0){
					for (Application_Staging__c appstg : stagingRecordList){
						if(jpMap.containsKey(appstg.PostingID__c)){
							appstg.Recruiter__c = jpMap.get(appstg.PostingID__c).OwnerId;
							appstg.RecruiterEmail__c = jpMap.get(appstg.PostingID__c).Owner.Email;
							appstg.Recruiter_Name__c = jpMap.get(appstg.PostingID__c).Owner.Name;
						}
					}
				}else{
					List<User> user = [select Id, FirstName, LastName, Email
				                   from user where IsActive = true and email = :recruitEmail limit 1];
					if (user != null && user.size() > 0) {
						for (Application_Staging__c appstg : stagingRecordList) {
							
								appstg.Recruiter__c = user[0].id;
								appstg.Recruiter_Name__c = user[0].firstName;
						}
					}
				}
				

				if (stagingRecordList.size() > 0) {
					insert stagingRecordList;
				}

				for (Attachment at : attachmentList) {
					for (Application_Staging__c appls : stagingRecordList) {
						if (appls.File_Name__c == at.Name) {
							at.ParentId = appls.id;
						}
					}
				}
				insert attachmentList;

				for (Application_Staging__c apps : stagingRecordList) {
					apps.Source__c = 'Email';
					apps.Status__c = 'New';
				}
				update stagingRecordList;

			} catch(Exception excp) {
				System.debug('Exception >>'+excp.getStackTraceString());
				ConnectedLog.LogException('ATSTalentViaEmail', 'handleInboundEmail', excp);
			}

		}

		return result;
	}
	@InvocableMethod(label = 'EMEANotifications' description = 'EMEA notificaions w.r.t parsing resume via email')
	public static void EMEANotifications(List<Id> ids) {
		
		List<Application_Staging__c> application = new List<Application_Staging__c> ();

		application = [SELECT Id, Status__c, Source__c, RecruiterEmail__c, Recruiter__c, OwnerId, Name FROM Application_Staging__c where id in :ids];
		if (application.size() > 0) {
			EmailTemplate et = [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName = 'EMEARecruiterNotification'];
			List<Attachment> attchBody = [Select id, Name, Body, ContentType from Attachment where parentId = :application[0].Id limit 1];
			for (Application_Staging__c appl : application) {
				
				List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage> ();

				Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(et.id, null, appl.Id);

				// If User record was determined earlier from email, use setTargetObjectId(). Else fallback to setToAddresses()
				if (appl.Recruiter__c != null) {
					mail.setTargetObjectId(appl.Recruiter__c);
					mail.setSaveAsActivity(false);
				} else {
					List<String> toAddressList = new List<String> ();
					if (appl.RecruiterEmail__c.contains('ewsdaxtra') && appl.RecruiterEmail__c.contains('@allegisgroup.co.uk')) {
						toAddressList.add(Label.DAXTRA_EMAIL_REPLACEMENT);
					} else {
						toAddressList.add(appl.RecruiterEmail__c);
					}
					mail.setToAddresses(toAddressList);
				}

				if (appl.Status__c.contains('Exception')) {
					Messaging.EmailFileAttachment emailAttachments = new Messaging.EmailFileAttachment();
					if (attchBody.size() > 0) {
						emailAttachments.setBody(attchBody[0].Body);
						emailAttachments.setFileName(attchBody[0].Name);
						emailAttachments.setContentType(attchBody[0].ContentType);
					}
					mail.setFileAttachments(new Messaging.EmailFileAttachment[] { emailAttachments });
				}
				emails.add(mail);

				if (emails.size() > 0) {
					Messaging.SendEmailResult[] EmailResults = Messaging.sendEmail(emails);
					
				}
			}
		}

	}
}