@isTest
public class ATSTalentViaEmailTest {
	public static final string fromName='FirstName LastName';
	public static final string fromAddress='someaddress@allegisgroup.com';
	public static final String  fileType='application/msword';
    public Static final String blobValue='Firstname: abcdert LastName: poiuyjklpoi JobTitle:Java Developer Skill: Java Phone Number : 0987654321 Email Id : poilkj@gmail.com';
	public Static final String toAddress ='someaddress__CV@allegisgroup.com';
	public Static final String recruitEmail ='recruiteremail__CV@allegisgroup.com';
	public static final string fileName='abc.doc';

	static testMethod void TestinBoundEmail()
    {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        //email.subject = 'Test Job from Allegis Group';
        email.subject = 'CV Forwarded by bgudiseva@allegisgroup.com / bgudiseva@__CV@allegisgroup.com - stefano volpe from Monster';
        email.fromname = fromName;
        List<String> strtoaddress = new List<String>{toAddress};
            email.toAddresses=strtoaddress;
        env.fromAddress = fromAddress;
        
        // add an Binary attachment
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('xyz1');
        attachment.fileName = fileName;
        attachment.mimeTypeSubType = fileType;
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
            // add an Text atatchment
            
            Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachment.body = blob.valueOf('abcdw');
        attachment.fileName = fileName;
        attachment.mimeTypeSubType = fileType;
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
            
            Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='doc',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
        insert glob;
        
        Test.startTest();
        Boolean result = FileController.validateFileType (fileName);
        System.assertEquals(true, result,'pass');
        Test.stopTest();
        
        Application_Staging__c Applstaging = new Application_Staging__c();
        ApplStaging.File_Name__c=fromName;
        ApplStaging.RecruiterEmail__c=recruitEmail;
        ApplStaging.Source__c = 'Email';
        ApplStaging.Status__c = 'New';
        insert ApplStaging;
        Attachment attachObject=new Attachment();
        attachObject.body = blob.valueOf(blobValue);
        attachObject.Name = fileName;
        attachObject.ContentType = fileType;
        attachObject.ParentId = ApplStaging.id;  
        insert attachObject;
        
        // call the email service class and test it with the data in the testMethod
        ATSTalentViaEmail emailProcess = new ATSTalentViaEmail();
        emailProcess.handleInboundEmail(email, env);
    }
    
    static testMethod void TestinBoundEmail2()
    {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        //email.subject = 'Test Job from Allegis Group';
        email.subject = 'Ad response to: JP-123456 from LinkedIn';
        email.fromname = fromName;
        List<String> strtoaddress = new List<String>{toAddress};
            email.toAddresses=strtoaddress;
        env.fromAddress = fromAddress;
        
        // add an Binary attachment
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = fileName;
        attachment.mimeTypeSubType = fileType;
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
            // add an Text atatchment
            
            Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachment.body = blob.valueOf('mnop');
        attachment.fileName = fileName;
        attachment.mimeTypeSubType = fileType;
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
            
            Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='doc',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
        insert glob;
        
        Test.startTest();
        Boolean result = FileController.validateFileType (fileName);
        System.assertEquals(true, result,'pass');
        Test.stopTest();
        
        Application_Staging__c Applstaging = new Application_Staging__c();
        ApplStaging.File_Name__c=fromName;
        ApplStaging.RecruiterEmail__c=recruitEmail;
        ApplStaging.Source__c = 'Email';
        ApplStaging.Status__c = 'New';
        insert ApplStaging;
        Attachment attachObject=new Attachment();
        attachObject.body = blob.valueOf(blobValue);
        attachObject.Name = fileName;
        attachObject.ContentType = fileType;
        attachObject.ParentId = ApplStaging.id;  
        insert attachObject;
        
        // call the email service class and test it with the data in the testMethod
        ATSTalentViaEmail emailProcess = new ATSTalentViaEmail();
        emailProcess.handleInboundEmail(email, env);
    }
    
}