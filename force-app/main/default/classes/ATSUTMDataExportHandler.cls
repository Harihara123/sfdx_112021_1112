global class ATSUTMDataExportHandler implements Database.Batchable<SObject>,Database.Stateful{
    String query;
    Integer totalRecords=0;
    Integer maxPairs = 0;
    String[] emailList;
    DuplicateRecord dup;
    Boolean insertInUTMTable = false;
    String sourceName = 'ATS';// Can be passed as an input.
    String transactionId = '';
    Map<String,DuplicateRecord> allContacts = new Map<String,DuplicateRecord>();
    global ATSUTMDataExportHandler(){

    }
    global ATSUTMDataExportHandler(Integer maxRecords, String[] emails){
        maxPairs = maxRecords;
        emailList = emails;
    }
    global ATSUTMDataExportHandler(Integer maxRecords, String[] emails, Boolean insertIntoUTM,String source){
        maxPairs = maxRecords;
        emailList = emails;
        insertInUTMTable = insertIntoUTM;
        sourceName = source;
    }
    global ATSUTMDataExportHandler(Integer maxRecords, String[] emails, Boolean insertIntoUTM,String source, String transId){
        maxPairs = maxRecords;
        emailList = emails;
        insertInUTMTable = insertIntoUTM;
        sourceName = source;
        transactionId = transId;
    }
    public static List<Object> slice(List<Object> input, Integer ge, Integer l)
    {
        List<Object> output = input.clone();
        for (Integer i = 0; i < ge; i++) output.remove(0);
        Integer elements = l - ge;
        while (output.size() > elements) output.remove(elements);
        return output;
    }

   /*global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }*/
    /*
   global Iterable<AggregateResult> start(Database.BatchableContext BC){ 
        return (new AggregateResultIterable()); 
    } */
   global Database.QueryLocator start(Database.batchableContext info) {
       return Database.getQueryLocator([select Id,source_system_id__c from contact where record_type_name__c = 'Talent' and Talent_Committed_Flag__c = true and Talent_Ownership__c = 'TEK']);
   }
  global void execute(Database.BatchableContext BC, Contact[] scope){ 
      System.debug('---------- 1 ' + scope);  
      
      try{
          //Now aggregate and see for duplicate
          List<String> psids = new List<String>();
          for(AggregateResult  contacts : [select source_system_id__c ssid, count(id) recs from contact where Id in:scope group by source_system_id__c having count(id) >1]){
                //System.debug((String)contacts.get('ssid') + '=' +(String)contacts.get('recs'));               
                psids.add((String)contacts.get('ssid'));
           }
          Integer psIdCounter = 0;
          Integer incrCounter = 10;
          for(Integer i=0;i<psids.size();i=i+incrCounter){
              try{

                  List<String> subpsids = (List<String>)slice(psids,i,(i+incrCounter));            
                  for(Contact cont : [Select Id, AccountId,source_system_id__c from Contact where source_system_id__c  in : subpsids]){
              
                  if(allContacts.keySet().contains(cont.source_system_id__c)){
                        dup = allContacts.get(cont.source_system_id__c);
                        dup.contId2 = cont.id;
                        dup.account2 = cont.AccountId;                      
                  }else{
                        dup = new DuplicateRecord();
                        dup.psId = cont.source_system_id__c;
                        dup.contId1 = cont.id;
                        dup.account1 = cont.AccountId;                                           
                        allContacts.put(cont.source_system_id__c, dup);                  
                  }
              }
              }catch(Exception exe){
                  System.debug(exe);
              }
          }
          totalRecords = totalRecords + allContacts.size();
          if(allContacts.size()>=maxPairs){              
              // Insert into UTM talentmerge table              
              List<Unified_Merge__c> utmRecords = new List<Unified_Merge__c>();
              String csvData = 'ContactId1, AccountId1, ContactId2, AccountId2, SSID\n';
              for(String psid : allContacts.keySet()){
                  dup = allContacts.get(psid);
                  csvData = csvData + (dup.contId1 + ',' + dup.account1 + ','+dup.contId2 + ',' + dup.account2 + ','+dup.psId + '\n');
                  utmRecords.add(new Unified_Merge__c(Input_ID_1__c=dup.contId1,Input_ID_2__c=dup.contId2,Process_State__c  ='Ready',Request_Source__c=sourceName,Transaction_Batch_ID__c=transactionId));
              }
              if(insertInUTMTable){
                  insert utmRecords;
              }
              ATSCSVEmailHandler.sendEmail(Blob.valueOf(csvData),emailList);
              System.abortJob(BC.getJobId());
          }          
      }catch(Exception ex){
          System.debug('ex ' + ex);
      }      
    }
   
    global void finish(Database.BatchableContext BC){

      System.debug('finish....'+totalRecords);
      String csvData = 'ContactId1, AccountId1, ContactId2, AccountId2, SSID\n';
        for(String psid : allContacts.keySet()){
            dup = allContacts.get(psid);
            csvData = csvData + (dup.contId1 + ',' + dup.account1 + ','+dup.contId2 + ',' + dup.account2 + ','+dup.psId + '\n');
        }
        ATSCSVEmailHandler.sendEmail(Blob.valueOf(csvData),emailList);
        
    }
   public static void exportDuplicateTalents(){
       try{
           for(AggregateResult  contacts : [select source_system_id__c ssid,count(id) recs from contact where record_type_name__c = 'Talent' and Talent_Committed_Flag__c = true and Talent_Ownership__c = 'TEK' group by source_system_id__c having count(id) >1 limit 100]){
                System.debug((String)contacts.get('ssid') + '=' +(String)contacts.get('recs'));               
           }
       }catch(Exception ex){
           System.debug(' ex ' + ex);
       }
    }
    
    public class DuplicateRecord{
        String psId;
        String contId1;
        String contId2;
        String account1;
        String account2;        
    }
}