@isTest
public class ATSUTMDataExportHandlerTest {
	 static testMethod void  export1Recs(){
         
         Test.startTest();
         List<Id> recIds = new List<Id>();
         List<Contact> contacts = new List<Contact>();
         Account talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         Contact talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
             //new Contact();
         talCont1.FirstName = 'test one';
         talCont1.lastName = 'test last';
         talCont1.Email = 'testemail@testalg.com';
         talCont1.accountId = talAcc1.Id;
         talCont1.source_system_id__c = 'R.44862750';
         contacts.add(talCont1);
         recIds.add(talCont1.Id);
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test one1';
         talCont1.lastName = 'test last1';
         talCont1.source_system_id__c = 'R.44862750';         
         talCont1.Email = 'testemail@testalg.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);
         
         talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two';
         talCont1.lastName = 'test two last';
         talCont1.Email = 'testemail@test2.com';
         talCont1.source_system_id__c = 'R.44862751';         
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);         
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two2';
         talCont1.lastName = 'test two last2';
         talCont1.source_system_id__c = 'R.44862751';         
         talCont1.Email = 'testemail@test2.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         upsert contacts;
         
         // 3 pair
         talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two';
         talCont1.lastName = 'test two last';
         talCont1.Email = 'testemail@test2.com';
         talCont1.source_system_id__c = 'R.44862752';         
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);         
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two2';
         talCont1.lastName = 'test two last2';
         talCont1.source_system_id__c = 'R.44862752';                  
         talCont1.Email = 'testemail@test2.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         upsert contacts;
         // 4 pair
         talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two';
         talCont1.lastName = 'test two last';
         talCont1.source_system_id__c = 'R.44862753';                  
         talCont1.Email = 'testemail@test2.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);         
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two2';
         talCont1.lastName = 'test two last2';
         talCont1.Email = 'testemail@test2.com';
         talCont1.source_system_id__c = 'R.44862754';         
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         upsert contacts;
         
         recIds.add(talCont1.Id);
         
         Id batchInstanceId = Database.executeBatch(new ATSUTMDataExportHandler(1 ,new String[]{'psammeta@allegisgroup.com'},true,'ATS'), 20);
         Test.stopTest();
         List<Unified_Merge__c> utmRecs = [Select Id, Process_State__c from Unified_Merge__c where Input_ID_1__c in :recIds OR Input_ID_2__c in :recIds];
         System.debug('UTMExport Results ' + utmRecs);
         System.assert(utmRecs != null);
     }
    static testMethod void  export4Recs(){
         
         Test.startTest();
         List<Id> recIds = new List<Id>();
         List<Contact> contacts = new List<Contact>();
         Account talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         Contact talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
             //new Contact();
         talCont1.FirstName = 'test one';
         talCont1.lastName = 'test last';
         talCont1.Email = 'testemail@testalg.com';
         talCont1.accountId = talAcc1.Id;
         talCont1.source_system_id__c = 'R.44862750';
         contacts.add(talCont1);
         recIds.add(talCont1.Id);
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test one1';
         talCont1.lastName = 'test last1';
         talCont1.source_system_id__c = 'R.44862750';         
         talCont1.Email = 'testemail@testalg.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);
         
         talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two';
         talCont1.lastName = 'test two last';
         talCont1.Email = 'testemail@test2.com';
         talCont1.source_system_id__c = 'R.44862751';         
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);         
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two2';
         talCont1.lastName = 'test two last2';
         talCont1.source_system_id__c = 'R.44862751';         
         talCont1.Email = 'testemail@test2.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         upsert contacts;
         
         // 3 pair
         talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two';
         talCont1.lastName = 'test two last';
         talCont1.Email = 'testemail@test2.com';
         talCont1.source_system_id__c = 'R.44862752';         
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);         
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two2';
         talCont1.lastName = 'test two last2';
         talCont1.source_system_id__c = 'R.44862752';                  
         talCont1.Email = 'testemail@test2.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         upsert contacts;
         // 4 pair
         talAcc1 = CreateTalentTestData.createTalentAccount('TEK');
         talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two';
         talCont1.lastName = 'test two last';
         talCont1.source_system_id__c = 'R.44862753';                  
         talCont1.Email = 'testemail@test2.com';
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         recIds.add(talCont1.Id);         
		 talCont1 =  CreateTalentTestData.createTalentContact(talAcc1);
         talCont1.FirstName = 'test two2';
         talCont1.lastName = 'test two last2';
         talCont1.Email = 'testemail@test2.com';
         talCont1.source_system_id__c = 'R.44862754';         
         talCont1.accountId = talAcc1.Id;
         contacts.add(talCont1);
         upsert contacts;
         
         recIds.add(talCont1.Id);
         
         Id batchInstanceId = Database.executeBatch(new ATSUTMDataExportHandler(4 ,new String[]{'psammeta@allegisgroup.com'}), 20);
         Test.stopTest();
         List<Unified_Merge__c> utmRecs = [Select Id, Process_State__c from Unified_Merge__c where Input_ID_1__c in :recIds OR Input_ID_2__c in :recIds];
         System.debug('UTMExport Results ' + utmRecs);
         System.assert(utmRecs != null);
     }
}