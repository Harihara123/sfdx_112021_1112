public class ATSUTMHandler  {
	
	static final String baseURL = System.label.UTM_Heroku_Url;
	Static String CONNECTED_UTM_BATCH_LOG_TYPE_ID = 'UTM/Batch';
    Static String CONNECTED_UTM_BATCH_EXCEPTION_LOG_TYPE_ID = 'UTM/BatchException';
    Static String CONNECTED_LOG_CLASS_NAME = 'ATSUnifiedTalentMergeBatch';
	Static String CONNECTED_LOG_METHOD = 'UTMRestCall';

	@future(callout=true)
	public static void callUTMHeroku(Integer maxRecords, String sourceSystem) {

		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(baseURL);
		request.setMethod('POST');
		Organization env = [SELECT Name FROM Organization LIMIT 1];
		request.setHeader('Content-Type', 'application/json');
        String body;
        if(Test.isRunningTest()){
            body = '{"maxRecords":"'+5+'","sourceSystem":"'+''+'","targetOrg":"Test"}';
        }
        else{
            UTMBatchRecordLimit__c lim = UTMBatchRecordLimit__c.getValues('UTMBatchLimit');
			UTMBatchSettings__c settings = UTMBatchSettings__c.getValues('UTMSchedule');
            Integer recordNum = (maxRecords > (Integer)lim.BatchLimit__c) ? (Integer)lim.BatchLimit__c : maxRecords;
            body = '{"maxRecords":"'+ recordNum+'","sourceSystem":"'+''+'","targetOrg":"'+env.Name+'"}';
        }
        
		

		System.debug(body);
		request.setBody(body);

		HttpResponse response = http.send(request);
		if (response.getStatusCode() == 200) {
			System.debug('Success');
			System.debug(' API Response ' +response.getBody());
		}
		else {
			System.debug('Failed');
			System.debug('Status Code: ' + response.getStatusCode());
		}
		
	}

	@future(callout=true)
	public static void callUTMHerokuTransactionId(Integer maxRecords, String sourceSystem, String transactionId) {

		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(baseURL);
		request.setMethod('POST');
		request.setHeader('Content-Type', 'application/json;charset=UTF-8');
		Organization env = [SELECT Name FROM Organization LIMIT 1];
        String body;
        if(Test.isRunningTest()){
            body = '{"maxRecords":"'+ 5+'","sourceSystem":"'+''+'","targetOrg":"Test","transactionId":"'+'ATSTestClass'+'"}';
        }
        else{
            UTMBatchRecordLimit__c lim = UTMBatchRecordLimit__c.getValues('UTMBatchLimit');
            UTMBatchSettings__c settings = UTMBatchSettings__c.getValues('UTMSchedule');
            Integer recordNum = (maxRecords > (Integer)lim.BatchLimit__c) ? (Integer)lim.BatchLimit__c : maxRecords;
            body = '{"maxRecords":"'+ recordNum+'","sourceSystem":"'+''+'","targetOrg":"'+env.Name+'","transactionId":"'+transactionId+'"}';
        }
		
		System.debug(body);
		request.setBody(body);

		HttpResponse response = http.send(request);
		if (response.getStatusCode() == 200) {
			System.debug('Success');
			
			System.debug(response.getBody());
		}
		else {
			System.debug('Failed');
			System.debug('Status Code: ' + response.getStatusCode());
		}
		
	}
}