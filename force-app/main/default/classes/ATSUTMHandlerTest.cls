@isTest 
private class ATSUTMHandlerTest {

	@isTest
	private static void testCallUTMHeroku() {
		Test.setMock(HttpCalloutMock.class, new UTMHandlerMockCallout());
        Test.startTest();
			ATSUTMHandler.callUTMHeroku(5, 'ATS-Dev-Test');
        Test.stopTest();
		
	}

	@isTest
	private static void testCallUTMHerokuTransactionID() {
		Test.setMock(HttpCalloutMock.class, new UTMHandlerMockCallout());
        Test.startTest();
			ATSUTMHandler.callUTMHerokuTransactionId(5, 'ATS-Dev-Test', 'TestId');
        Test.stopTest();
	}
}