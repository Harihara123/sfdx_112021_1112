public class ATSUnifiedTalentMerge  {

    Static String processStatus = 'NA';
    Static String processStatusMessage = 'NA';
    Static String processRequestor = '';
    Static String processRequestReason = '';
    Static String transactionId = '';
    Static String errorType = 'ATS_UNIFIED_TALENT_MERGE_EXCEPTION';
    Static String CONNECTED_UTM_LOG_TYPE_ID = 'UTM/TalentMerge';
    Static String CONNECTED_UTM_EXCEPTION_LOG_TYPE_ID = 'UTM/TalentMergeException';
    Static String CONNECTED_UTM_SURVIVORSHIP_LOG_TYPE_ID = 'UTM/Survivorship';
    Static String CONNECTED_UTM_SURVIVORSHIP_EXCEPTION_LOG_TYPE_ID = 'UTM/SurvivorshipException';
    Static String CONNECTED_UTM_MERGE_LOG_TYPE_ID = 'UTM/Merge';
    Static String CONNECTED_UTM_MERGE_EXCEPTION_LOG_TYPE_ID = 'UTM/MergeException';
    Static String CONNECTED_LOG_CLASS_NAME = 'ATSUnifiedTalentMerge';
    Static String CONNECTED_LOG_SRURVIVORSHIP_RESPONSE= 'SurviviourShip response';
    Static String CONNECTED_LOG_MERGE_RESPONSE = 'Merge response';
    Static String UTM_PROCESS_STATUS_READY = 'Ready';
    Static String UTM_PROCESS_STATUS_SURVIVOR_SELECTED = 'Survivor Selected';
    Static String UTM_PROCESS_STATUS_FAILED_SURVIVIOURSHIP = 'Failed Survivorship';
    Static String UTM_PROCESS_STATUS_MERGED = 'Merged';
    Static String UTM_PROCESS_STATUS_FAILED_MERGE = 'Failed Merge';
    static String UTM_LOG_DATETIME_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss.SSSZ';
    UnifiedTalMerge unifiedMergeObj = null;
    static Boolean enableConnectedLog = true;
    public enum UTMProcessLevel {SURVIVOUR_ONLY, MERGE_ONLY, FULLPROCESS}

    /**
     *** inputs utmId is the Unified_Merge__c  record id,  input1 and input2 are either contact or account Ids, 
     *** reqSource is the source system who loaded these reocrds and transId is the transaction Id.
     **/
    public static String processUTM(Id utmId,String input1,String input2,String reqSource, String transId,String batchJobId,UTMProcessLevel processLevel){

        ATSUnifiedTalentMerge utmMerger = new ATSUnifiedTalentMerge();
        String talAccId1 = null;
        String talAccId2 = null;
        String talContId1 = null;
        String talContId2 = null;
        processStatusMessage = '';

        utmMerger.unifiedMergeObj = new UnifiedTalMerge();
        utmMerger.unifiedMergeObj.requestSource = reqSource;
        utmMerger.unifiedMergeObj.transactionId = transId;
        utmMerger.unifiedMergeObj.recordProcStartTime = Datetime.now();
        utmMerger.unifiedMergeObj.batchJobId = batchJobId;
        Boolean isDataSourceSF = false;
        processStatus = UTM_PROCESS_STATUS_READY;
        utmMerger.unifiedMergeObj.moreLogDetails = new Map<String,String>();
        utmMerger.unifiedMergeObj.moreLogDetails.put('Input ID 1',input1);
        utmMerger.unifiedMergeObj.moreLogDetails.put('Input ID 2',input2);

        try{
            List<String> accIds = null;
            List<String> inputIds = new List<String>{input1,input2};
            List<Contact> listContacts = utmMerger.doContactsExist(inputIds);
            // Match the Talent Ids in case of any past merge made one of the given contact ids as victims
            if(listContacts == null || listContacts.size() < inputIds.size())
            {
                Map<String, Object> mappingResult = utmMerger.matchContactIds(listContacts,input1,input2);
                listContacts = (List<Contact>)mappingResult.get('resultConts');
            }

            if(listContacts != null && listContacts.size() == inputIds.size()){
                // Assume here order of the 1 and 2 records is not a problem.
                talContId1 = listContacts[0].Id;
                talContId2 = listContacts[1].Id;
                talAccId1 = listContacts[0].AccountId;
                talAccId2 = listContacts[1].AccountId;

                utmMerger.unifiedMergeObj.moreLogDetails.put('talent contact 1',talContId1);
                utmMerger.unifiedMergeObj.moreLogDetails.put('talent contact 2',talContId2);
                utmMerger.unifiedMergeObj.moreLogDetails.put('talent account 1',talAccId1);
                utmMerger.unifiedMergeObj.moreLogDetails.put('talent account 2',talAccId2);

                accIds = new List<String>{talAccId1,talAccId2};
                processStatusMessage = '';
                Map<String, String> contAccMap = new Map<String, String>();// ContactId = Account Id
                Map<String, Contact> contTalentIdMap = new Map<String, Contact>(); // AccId = Contact
                contTalentIdMap.put(listContacts[0].AccountId,listContacts[0]);
                contTalentIdMap.put(listContacts[1].AccountId,listContacts[1]);
                contAccMap.put(listContacts[0].Id,listContacts[0].AccountId);
                contAccMap.put(listContacts[1].Id,listContacts[1].AccountId);
                System.debug('processLevel >>> '+processLevel);
                if(processLevel != null && processLevel == UTMProcessLevel.MERGE_ONLY){
                    // Handle Merge only Flow
                    ConnectedLog.EventTimer logTimer= new ConnectedLog.EventTimer(ATSUnifiedTalentMerge.CONNECTED_UTM_LOG_TYPE_ID,ATSUnifiedTalentMerge.CONNECTED_LOG_CLASS_NAME, 'processUTM', 'UTM Survivor Timer');
                    //System.debug(processStatus+'===1='+processStatusMessage);
                    //utmMerger.mergeTalents(listContacts[0].Talent_Id__c,listContacts[1].Talent_Id__c,processStatus,processStatusMessage,utmMerger);
                    //System.debug(processStatus+'===2='+processStatusMessage);
                    String mergeResponse = TalentMergeController.talentMerge(listContacts[0].Talent_Id__c,listContacts[1].Talent_Id__c);
                    utmMerger.unifiedMergeObj.moreLogDetails.put(CONNECTED_LOG_MERGE_RESPONSE,mergeResponse);
                    Map<String,Object> respMap = (Map<String, Object>)JSON.deserializeUntyped(mergeResponse);
                    if(respMap != null && respMap.get('Success') != null){
                        processStatus = (((String)respMap.get('Success')) == 'true')?UTM_PROCESS_STATUS_MERGED:UTM_PROCESS_STATUS_FAILED_MERGE;
                    }else{
                        processStatus = UTM_PROCESS_STATUS_FAILED_MERGE;
                        processStatusMessage = (respMap.get('Message') != null ? (String)respMap.get('Message'): '');
                    }
                    logTimer.LogTimer();
                }else{
                    ConnectedLog.EventTimer logTimer= new ConnectedLog.EventTimer(ATSUnifiedTalentMerge.CONNECTED_UTM_LOG_TYPE_ID,ATSUnifiedTalentMerge.CONNECTED_LOG_CLASS_NAME, 'processUTM', 'UTM Survivor Timer');
                    String surAPIRes = ATSSurvivorshipMatch.preCheckRules(listContacts[0],listContacts[1],contAccMap);
                    logTimer.LogTimer();
                    utmMerger.unifiedMergeObj.moreLogDetails.put(CONNECTED_LOG_SRURVIVORSHIP_RESPONSE,(surAPIRes!=null ? (surAPIRes.left(surAPIRes.length()-1)) : surAPIRes));
                    Map<String,Object> respMap = (Map<String,Object>)JSON.deserializeUntyped(surAPIRes);

                    if(respMap != null && respMap.get('Success') != null){
                        String survAPIStatus = (String)respMap.get('Success');
                        utmMerger.unifiedMergeObj.survivourShipRule = (String)respMap.get('Message');
                        processStatusMessage = (survAPIStatus == 'true' ? 'Survivour API Success' : 'Survivour API Failed');
                        if(survAPIStatus == 'true'){
                            processStatus = UTM_PROCESS_STATUS_SURVIVOR_SELECTED;
                            try{
                                

                                String masterAccId = (String) respMap.get('MasterID');
                                String victimAccId = (String) respMap.get('VictimID');

                                utmMerger.unifiedMergeObj.survivourAccId = masterAccId;
                                utmMerger.unifiedMergeObj.victimAccId = victimAccId;
                                Contact mastContact = (Contact)contTalentIdMap.get(masterAccId);
                                Contact victContact = (Contact)contTalentIdMap.get(victimAccId);
                                utmMerger.unifiedMergeObj.contId1 = mastContact.Id;
                                utmMerger.unifiedMergeObj.contId2 = victContact.Id;
                                String masterContTalId = mastContact.Talent_Id__c;
                                String victimContTalId = victContact.Talent_Id__c;
                                utmMerger.unifiedMergeObj.survivourContId = mastContact.Id;
                                utmMerger.unifiedMergeObj.victimContId = victContact.id;
                                // Call Merge for process

                                if(processLevel != null && processLevel != UTMProcessLevel.SURVIVOUR_ONLY){
                                    processStatusMessage = '';
                                    respMap = null;
                                    ConnectedLog.EventTimer logTimerMerge= new ConnectedLog.EventTimer(ATSUnifiedTalentMerge.CONNECTED_UTM_LOG_TYPE_ID,ATSUnifiedTalentMerge.CONNECTED_LOG_CLASS_NAME, 'processUTM', 'UTM Merge Timer');
                                    //utmMerger.mergeTalents(masterContTalId,victimContTalId,processStatus,processStatusMessage,utmMerger);


                                    String mergeResponse = TalentMergeController.talentMerge(masterContTalId,victimContTalId);
                                    utmMerger.unifiedMergeObj.moreLogDetails.put(CONNECTED_LOG_MERGE_RESPONSE,mergeResponse);
                                    respMap = (Map<String, Object>)JSON.deserializeUntyped(mergeResponse);
                                    if(respMap != null && respMap.get('Success') != null){
                                        processStatus = (((String)respMap.get('Success')) == 'true')?UTM_PROCESS_STATUS_MERGED:UTM_PROCESS_STATUS_FAILED_MERGE;
                                    }else{
                                        processStatus = UTM_PROCESS_STATUS_FAILED_MERGE;
                                        processStatusMessage = (respMap.get('Message') != null ? (String)respMap.get('Message'): '');
                                    }
                                    logTimerMerge.LogTimer();
                                }
                            }catch(Exception ex){                            
                                processStatus = UTM_PROCESS_STATUS_FAILED_MERGE;
                                utmMerger.unifiedMergeObj.moreLogDetails.put(CONNECTED_UTM_MERGE_EXCEPTION_LOG_TYPE_ID,'Merge/'+ex.getStackTraceString());
                                processStatusMessage = (respMap != null)?(String)respMap.get('Message'):('Merge Exception ' + ex.getStackTraceString());
                                if(ATSUnifiedTalentMerge.enableConnectedLog){
                                    ConnectedLog.LogException(CONNECTED_UTM_MERGE_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME, 'processUTM', utmId,'','', ex);
                                    }
                            }
                        }else{
                            processStatus = UTM_PROCESS_STATUS_FAILED_SURVIVIOURSHIP;
                        }
                    }else{
                        processStatus = UTM_PROCESS_STATUS_FAILED_SURVIVIOURSHIP;
                    }
                }
            }else{
                // One or more given contact Ids are not found in the system.
				if( input1 == input2 ){
					processStatusMessage = 'Duplicate records';
					utmMerger.unifiedMergeObj.survivourShipRule = 'Survivor/Victim cannot be determine the talents are tied to the same IDs';
				}
				else {
					processStatusMessage = 'Given Input Ids are not found.';
				}
                processStatus = UTM_PROCESS_STATUS_FAILED_SURVIVIOURSHIP;
                utmMerger.unifiedMergeObj.moreLogDetails.put('RecordsNotFound','Given input reocrds are not found.');
            }
        }catch(Exception ex){
            utmMerger.unifiedMergeObj.moreLogDetails.put(CONNECTED_UTM_SURVIVORSHIP_EXCEPTION_LOG_TYPE_ID,'Survivorship/'+ex.getStackTraceString());
            processStatus = UTM_PROCESS_STATUS_FAILED_SURVIVIOURSHIP;
            processStatusMessage = ex.getMessage();
            if(ATSUnifiedTalentMerge.enableConnectedLog){
                ConnectedLog.LogException(CONNECTED_UTM_SURVIVORSHIP_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME, 'processUTM', utmId,'','', ex);
            }
        }
        utmMerger.unifiedMergeObj.recordProcEndTime = Datetime.now();
        utmMerger.unifiedMergeObj.moreLogDetails.put('StartTime',utmMerger.unifiedMergeObj.recordProcStartTime.format(UTM_LOG_DATETIME_FORMAT));
        utmMerger.unifiedMergeObj.moreLogDetails.put('EndTime',utmMerger.unifiedMergeObj.recordProcEndTime.format(UTM_LOG_DATETIME_FORMAT));
        utmMerger.unifiedMergeObj.failureReason = processStatusMessage;
        utmMerger.unifiedMergeObj.processState = processStatus;
        utmMerger.insertUnifiedMerges(utmId,utmMerger.unifiedMergeObj);
        return JSON.serialize(utmMerger.unifiedMergeObj);
    }

    private void mergeTalents(String masterContTalId,String victimContTalId,String processStatus, String processStatusMessage,ATSUnifiedTalentMerge utmMerger){
        String mergeResponse = TalentMergeController.talentMerge(masterContTalId,victimContTalId);
        utmMerger.unifiedMergeObj.moreLogDetails.put(CONNECTED_LOG_MERGE_RESPONSE,mergeResponse);
        Map<String, Object> respMap = (Map<String, Object>)JSON.deserializeUntyped(mergeResponse);
        if(respMap != null && respMap.get('Success') != null){
            processStatus = (((String)respMap.get('Success')) == 'true')?UTM_PROCESS_STATUS_MERGED:UTM_PROCESS_STATUS_FAILED_MERGE;
        }else{
            processStatus = UTM_PROCESS_STATUS_FAILED_MERGE;
            processStatusMessage = (respMap.get('Message') != null ? (String)respMap.get('Message'): '');
        }
    }
    // Inputs - One or more contact record Id's

    private List<Contact> doContactsExist(List<String> inputIds)
    {
        List<Contact> contactRecords = null;
        contactRecords = [Select Id,accountId,Talent_Id__c,Talent_Ownership__c, Peoplesoft_ID__c,Candidate_Status__c,Account.G2_Last_Modified_Date__c,Account.Talent_Profile_Last_Modified_Date__c, LastModifiedDate,CreatedDate FROM Contact where Id IN:inputIds OR accountId In:inputIds];
        return contactRecords;
    }

    // For given Contact Id's find if there were any matching in the UTM
    private Map<String, Object> matchContactIds(List<Contact> listContacts,String talContId1,String talContId2){
		System.debug('*** 0 '+listContacts);
        System.debug('*** 0 '+talContId1);
        System.debug('*** 0 '+talContId2);
        Map<String,Object> resultMap = new Map<String,Object>();
        List<Contact> resultConts = null;
        Map<String, Contact> contTalentIdMap = new Map<String, Contact>();
        String talAccId = null;
        for (Contact c : listContacts) {
            contTalentIdMap.put(c.Id,c);
        }
        if(!contTalentIdMap.keySet().contains(talContId1)){
            // Find if the talContId1 has any UTM record as victim
            Contact cont = this.getUTMSurvivourContact(talContId1);
            
            if(cont != null){
                talAccId = cont.AccountId;
                contTalentIdMap.put(cont.Id,cont);
                talContId1 = cont.Id;               
            }else{
              System.debug('*** 1 '+talContId1);
              System.debug('*** 2 '+cont);  
            }
        }

        if(!contTalentIdMap.keySet().contains(talContId2)){
            // Find if the talContId2 has any UTM record as victim
            Contact cont = this.getUTMSurvivourContact(talContId2);
            if(cont != null){
                talAccId = cont.AccountId;
                contTalentIdMap.put(cont.Id,cont);
                talContId2 = cont.Id;
            }
        }

        resultConts = contTalentIdMap.values();
        resultMap.put('resultConts',resultConts);
        return resultMap;
    }

    private Contact getUTMSurvivourContact(String victimContId){
        Contact survCont = null;       
        try{
			List<Unified_Merge__c> utmList = [Select Id,Survivor_Contact_ID__c from Unified_Merge__c where (Victim_Contact_ID__c=:victimContId) Order by createddate desc limit 1];
            if(utmList.size() > 0 ){
				survCont = [Select Id,accountId,Talent_Id__c,Talent_Ownership__c, Peoplesoft_ID__c,Candidate_Status__c,Account.G2_Last_Modified_Date__c,Account.Talent_Profile_Last_Modified_Date__c, LastModifiedDate,CreatedDate FROM Contact where id =:utmList[0].Survivor_Contact_ID__c];
			}
        }catch(Exception ex){
            if(ATSUnifiedTalentMerge.enableConnectedLog){
                ConnectedLog.LogException(CONNECTED_UTM_SURVIVORSHIP_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'getUTMSurvivourContact',victimContId ,'','', ex);
            }
        }
        return survCont;
    }
    private void updateLogInformation(Id recId,UnifiedTalMerge unifiedMerge){
        try{
            String moreDetails = JSON.serialize(unifiedMerge.moreLogDetails);
            moreDetails = (moreDetails!=null ? (moreDetails.left(moreDetails.length()-1)) : moreDetails);
            ConnectedLog.LogInformation(ATSUnifiedTalentMerge.CONNECTED_UTM_LOG_TYPE_ID,ATSUnifiedTalentMerge.CONNECTED_LOG_CLASS_NAME,'updateLogInformation',String.valueOf(recId),unifiedMerge.transactionId,'',unifiedMerge.failureReason,(new Map<String,String>{'OtherDetails'=>moreDetails,'BatchJobId'=>unifiedMerge.batchJobId}));
        }catch(Exception ex){
            ConnectedLog.LogException(CONNECTED_UTM_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'updateLogInformation',recId ,'','', ex);
        }
    }
    private boolean insertUnifiedMerges(Id utmIdnew,UnifiedTalMerge unifiedMerge){
        Boolean insertStatus = false;
        List<Unified_Merge__c> listUnifMerges = new List<Unified_Merge__c>();
        try{            
                Unified_Merge__c unfMerg = new Unified_Merge__c();
                unfMerg.id = utmIdnew;
                unfMerg.Survivor_Contact_ID__c = unifiedMerge.contId1;              
                unfMerg.Victim_Contact_ID__c = unifiedMerge.contId2;
                unfMerg.Failure_Reason__c = unifiedMerge.failureReason;
                unfMerg.Process_State__c = unifiedMerge.processState;                
                unfMerg.Survivor_Account_ID__c = unifiedMerge.survivourAccId;               
                unfMerg.Victim_Account_ID__c = unifiedMerge.victimAccId;
                unfMerg.Survivorship_Rule__c = unifiedMerge.survivourShipRule;  
				if(unifiedMerge.requestSource != null && unifiedMerge.requestSource.length()>0){
                 unfMerg.Request_Source_Name__c = unifiedMerge.requestSource;   
                }   
                upsert unfMerg;
        }catch(Exception ex){
            unifiedMerge.moreLogDetails.put(CONNECTED_UTM_EXCEPTION_LOG_TYPE_ID,'Update UTM recod/'+ex.getStackTraceString());
            if(ATSUnifiedTalentMerge.enableConnectedLog){
                ConnectedLog.LogException(CONNECTED_UTM_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME, 'insertUnifiedMerges', utmIdnew,'','', ex);
            }
        }
        if(ATSUnifiedTalentMerge.enableConnectedLog){
            this.updateLogInformation(utmIdnew,unifiedMerge);
        }
        return insertStatus;
    }
     class UnifiedTalMerge{
        
        String requestSource = '';
        String requestReason = '';
        String ruleAttributes = '';
        String survivourShipRule = '';

        String transactionId = '';
        String contId1;
        String contId2;
        String survivourAccId = '';
        String victimAccId = '';
        String survivourContId = '';
        String victimContId = '';
        String batchJobId = '';
        String processState = '';
        String failureReason = '';
        DateTime recordProcStartTime = null;
        DateTime recordProcEndTime = null;
        Map<String,String> moreLogDetails = null;
    }

}