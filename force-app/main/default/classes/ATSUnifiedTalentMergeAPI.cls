@RestResource(urlMapping='/UTM/MergeAPI/Merge/*')
global with sharing class ATSUnifiedTalentMergeAPI {
    private static Map<String,ATSUnifiedTalentMerge.UTMProcessLevel> processLevels = new Map<String,ATSUnifiedTalentMerge.UTMProcessLevel>{'SurvOnly' => ATSUnifiedTalentMerge.UTMProcessLevel.SURVIVOUR_ONLY,
                                                                                                                            'MergeOnly' => ATSUnifiedTalentMerge.UTMProcessLevel.MERGE_ONLY,
                                                                                                                            'Full' => ATSUnifiedTalentMerge.UTMProcessLevel.FULLPROCESS};
	@HttpPost
    global static void doPost(Id utmId,String input1,String input2,String reqSource, String transId,String batchJobId,String processLevel) {
        ATSUnifiedTalentMerge.UTMProcessLevel UTMProcessLevel = ATSUnifiedTalentMerge.UTMProcessLevel.FULLPROCESS;
        if(processLevel != null && processLevels.get(processLevel)!=null){
            UTMProcessLevel = processLevels.get(processLevel);
            ConnectedLog.LogInformation('UTM/UTMAPI','ATSUnifiedTalentMergeAPI','doPost',String.valueOf(utmId),transId,'','',(new Map<String,String>()));
            RestContext.response.addHeader('Content-Type', 'application/json');
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            String resp = ATSUnifiedTalentMerge.processUTM(utmId, input1, input2,reqSource,transId,batchJobId,UTMProcessLevel);
            system.debug(resp);
            gen.writeObjectField('mergeResponse', resp); 
            gen.writeEndObject();
            RestContext.response.responseBody = Blob.valueOf(gen.getAsString()); 
        }
    }
}