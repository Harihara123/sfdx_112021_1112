@isTest
public class ATSUnifiedTalentMergeAPITest {
    static testMethod void  callPostMethod(){
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/UTM/MergeAPI/Merge/*';
        req.httpMethod = 'POST';
//Id utmId,String input1,String input2,String reqSource, String transId,String batchJobId,String processLevel) {
		
        //Insert a UTM record
        Account talAcc1 = CreateTalentTestData.createTalentAccount();
        Account talAcc2 = CreateTalentTestData.createTalentAccount();
        
        talAcc1.Talent_Ownership__c = 'TEK';
        talAcc2.Talent_Ownership__c = 'TEK';        
        update (new List<Account>{talAcc1,talAcc2});
        
        Contact talCont1 =  new Contact();
        talCont1.FirstName = 'test one';
        talCont1.Peoplesoft_ID__c = '123456';
        talCont1.Phone = '1122334455';
        talCont1.LastName = 'contact';
        talCont1.Email = 'testemail@testalg.com';
        talCont1.accountId = talAcc1.Id;
        talCont1.MailingStreet = 'Ancells Road';
        talCont1.MailingState = 'New Jersey';
        talCont1.MailingCity = 'Fair Lawn';
        talCont1.MailingPostalCode = '07410';
        talCont1.Title = 'Java Architect';
        
        String sourceSystmId = '54534544';

        Contact talCont2 =  new Contact();
        //CreateTalentTestData.createTalentContact(talAcc2);               
        talCont2.Email = 'testemail2@testalg.com';
        talCont2.FirstName = 'test one 2';
        talCont2.Phone = '1122334455';
        talCont2.Peoplesoft_ID__c = '123457';
        talCont2.LastName = 'contact 2';
        talCont2.accountId = talAcc2.Id;
        talCont2.MailingStreet = 'Ancells Road';
        talCont2.MailingState = 'New Jersey';
        talCont2.MailingCity = 'Fair Lawn';
        talCont2.MailingPostalCode = '07410';
        talCont2.Title = 'Java Architect';                        
        insert (new List<Contact>{talCont1,talCont2});
        
        Unified_Merge__c utm = new Unified_Merge__c();
        utm.Input_ID_1__c = String.valueOf(talCont1.Id);
        utm.Input_ID_2__c = String.valueOf(talCont2.Id);
        utm.Process_State__c = 'Ready';
        utm.Transaction_Batch_ID__c = 'TR1';
        utm.Request_Source_Name__c = 'ATS';
        insert utm;
        
        RestContext.request = req;
        RestContext.response= res;
        ATSUnifiedTalentMergeAPI.doPost(utm.Id,String.valueOf(talCont1.Id),String.valueOf(talCont2.Id),'ATS','TR1','','Full');
        System.debug(res);
        System.assert(res!=null);
        Test.stopTest();
    }
}