global class ATSUnifiedTalentMergeBatch implements Database.Batchable<SObject>,Database.Stateful,Database.AllowsCallouts{    
    Static String CONNECTED_UTM_BATCH_LOG_TYPE_ID = 'UTM/Batch';
    Static String CONNECTED_UTM_BATCH_EXCEPTION_LOG_TYPE_ID = 'UTM/BatchException';
    Static String CONNECTED_LOG_CLASS_NAME = 'ATSUnifiedTalentMergeBatch';
    static String UTM_LOG_DATETIME_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss.SSSZ';
    ConnectedLog.EventTimer logTimer = null;
    Integer totalRecCount = 0;
    Integer totalSucessRecCount = 0;
    Integer totalFailRecCount = 0;
    Map<String,Object> moreLogDetails = null;
    List<Map<String,String>> childJobDetails = null;
    Map<String,String> jobDetails = null;
    String curRecordProcStatus = '';
    String batchJobId = '';
    DateTime startTime;
    global final String query;
    Map<String,ATSUnifiedTalentMerge.UTMProcessLevel> processLevels = new Map<String,ATSUnifiedTalentMerge.UTMProcessLevel>{'SurvOnly' => ATSUnifiedTalentMerge.UTMProcessLevel.SURVIVOUR_ONLY,
                                                                                                                            'MergeOnly' => ATSUnifiedTalentMerge.UTMProcessLevel.MERGE_ONLY,
                                                                                                                            'Full' => ATSUnifiedTalentMerge.UTMProcessLevel.FULLPROCESS};
    ATSUnifiedTalentMerge.UTMProcessLevel UTMProcessLevel = ATSUnifiedTalentMerge.UTMProcessLevel.FULLPROCESS;
    global ATSUnifiedTalentMergeBatch(String q){
        query = q;
        this.setUpClass();
    }
    global ATSUnifiedTalentMergeBatch(String q,String processLevel){
        query = q;
        if(processLevel != null && this.processLevels.get(processLevel)!=null){
            this.UTMProcessLevel = this.processLevels.get(processLevel);
        }
        this.setUpClass();
    }
    public void setUpClass(){
        this.logTimer = new ConnectedLog.EventTimer(CONNECTED_UTM_BATCH_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME, 'ATSUnifiedTalentMergeBatch', 'ATS Unified Batch Timer');
        this.moreLogDetails = new Map<String,Object>();
        this.childJobDetails = new List<Map<String,String>>();
        this.startTime = Datetime.now();
        this.moreLogDetails.put('batchStartTime',this.startTime.format(UTM_LOG_DATETIME_FORMAT));
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
      this.batchJobId = BC.getJobId();
      this.moreLogDetails.put('batchJobId',batchJobId);
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Unified_Merge__c> unifiedRecords){
     this.jobDetails = new Map<String,String>();
     //this.jobDetails.put('childJobId',BC.getChildJobId());
     this.jobDetails.put('childJobSize',String.valueOf(unifiedRecords.size()));
     this.jobDetails.put('jobStartTime',Datetime.now().format(UTM_LOG_DATETIME_FORMAT));
     this.totalRecCount = this.totalRecCount + unifiedRecords.size();
     Integer jobSuccessRecords = 0;
     for(Unified_Merge__c mr : unifiedRecords){
        this.curRecordProcStatus = '';  	 
         try{
	         ATSUnifiedTalentMerge.processUTM(mr.id, mr.Input_ID_1__c, mr.Input_ID_2__c,mr.Request_Source__c,mr.Transaction_Batch_ID__c,this.batchJobId,this.UTMProcessLevel);
             this.curRecordProcStatus = 'Processing Success';
             jobSuccessRecords = jobSuccessRecords + 1;
             this.totalSucessRecCount = this.totalSucessRecCount+1;
         }
         catch(Exception ex){
            this.totalFailRecCount = this.totalFailRecCount + 1;
            ConnectedLog.LogException(CONNECTED_UTM_BATCH_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME, 'execute', mr.Id,'','', ex);
            this.curRecordProcStatus = 'Processing Failed';
            this.logTimer.SetExceptionOccured();
         }finally{
            this.jobDetails.put('childJobRecord',String.valueOf(mr.Id)+' ' + this.curRecordProcStatus);
         }
     }
     this.jobDetails.put('jobEndTime',Datetime.now().format(UTM_LOG_DATETIME_FORMAT));
     this.jobDetails.put('jobSuccessRecords',String.valueOf(jobSuccessRecords));
     this.childJobDetails.add(this.jobDetails);
    }

   global void finish(Database.BatchableContext BC){
        try{
            DateTime endTime = Datetime.now();
            this.moreLogDetails.put('batchAllJobs',this.childJobDetails);
            this.moreLogDetails.put('RowsSucceeded',(this.totalSucessRecCount));
            this.moreLogDetails.put('RowsFailed',(this.totalFailRecCount));
            this.moreLogDetails.put('batchEndTime',endTime.format(UTM_LOG_DATETIME_FORMAT));  
            this.moreLogDetails.put('totalRecordsProcessed',(this.totalRecCount));
            String moreDetails = JSON.serialize(this.moreLogDetails);
            moreDetails = (moreDetails!=null ? (moreDetails.left(moreDetails.length()-1)) : moreDetails);
            this.logTimer.AddAdditionalData('OtherDetails',moreDetails);
            this.logTimer.AddAdditionalData('RowsSucceeded',(this.totalSucessRecCount));
            this.logTimer.AddAdditionalData('RowsFailed',(this.totalFailRecCount));
            this.logTimer.AddAdditionalData('totalRecordsProcessed',(this.totalRecCount));
            this.logTimer.AddAdditionalData('BatchJobId',this.batchJobId);
            //this.logTimer.AddAdditionalData('duration',String.valueOf((endTime.getTime()-this.startTime.getTime())));
        }catch(Exception ex){            
            ConnectedLog.LogException(CONNECTED_UTM_BATCH_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'finish','','',ex);
            this.logTimer.SetExceptionOccured();
        }finally{
            this.logTimer.LogTimer();
        }
   }
}