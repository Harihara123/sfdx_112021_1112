@istest
public class ATSUnifiedTalentMergeTest  {
    
    static testMethod void  startUTMProcess_DiffPSIDs(){
        User user = TestDataHelper.createUser('System Integration');
        System.runAs(user) {
            try{
                String sourceSystmId = '54555334';

                Test.startTest();            
                Account talAcc1 = CreateTalentTestData.createTalentAccount();
                Account talAcc2 = CreateTalentTestData.createTalentAccount();

                talAcc1.Talent_Ownership__c = 'TEK';
                talAcc2.Talent_Ownership__c = 'TEK';

                update (new List<Account>{talAcc1,talAcc2});

                Contact talCont1 =  new Contact();
                talCont1.FirstName = 'test one';
                talCont1.Peoplesoft_ID__c = '123456';
                talCont1.Phone = '1122334455';
                talCont1.LastName = 'contact';
                talCont1.Email = 'testemail@testalg.com';
                talCont1.accountId = talAcc1.Id;
                talCont1.MailingStreet = 'Ancells Road';
                talCont1.MailingState = 'New Jersey';
                talCont1.MailingCity = 'Fair Lawn';
                talCont1.MailingPostalCode = '07410';
                talCont1.Title = 'Java Architect';


                //CreateTalentTestData.createTalentContact(talAcc1);               

                sourceSystmId = '54534544';

                Contact talCont2 =  new Contact();
                //CreateTalentTestData.createTalentContact(talAcc2);               
                talCont2.Email = 'testemail2@testalg.com';
                talCont2.FirstName = 'test one 2';
                talCont2.Phone = '1122334455';
                talCont2.Peoplesoft_ID__c = '123457';
                talCont2.LastName = 'contact 2';
                talCont2.accountId = talAcc2.Id;
                talCont2.MailingStreet = 'Ancells Road';
                talCont2.MailingState = 'New Jersey';
                talCont2.MailingCity = 'Fair Lawn';
                talCont2.MailingPostalCode = '07410';
                talCont2.Title = 'Java Architect';



                insert (new List<Contact>{talCont1,talCont2});


                Unified_Merge__c utm = new Unified_Merge__c();
                utm.Input_ID_1__c = String.valueOf(talCont1.Id);
                utm.Input_ID_2__c = String.valueOf(talCont2.Id);
                utm.Process_State__c = 'Ready';
				utm.Request_Source_Name__c = 'ATS';
                insert utm;

                talCont1.LastName = talCont1.LastName + 'updated';
                update talCont1;



                system.debug('************* DBUG');
                List<String> accIds = new List<String>{talAcc1.Id,talAcc2.Id};
                System.debug(accIds);



                ATSUnifiedTalentMerge.processUTM(utm.Id,''+talCont1.Id,''+talCont2.Id,'','','processUTM',ATSUnifiedTalentMerge.UTMProcessLevel.SURVIVOUR_ONLY);

                Test.stopTest();

                // Check if yousee any records in the 
                List<Unified_Merge__c> mergeResults =  [SELECT Id,Survivor_Account_ID__c FROM Unified_Merge__c WHERE Survivor_Account_ID__c in :(new List<String>{talAcc1.Id,talAcc2.Id})];

                System.assert(mergeResults.size()==0);
            }catch(Exception ex){
                System.debug(ex.getStackTraceString());
            }

      }
    }
 
    static testMethod void  startUTMProcess(){
        User user = TestDataHelper.createUser('System Integration');
        System.runAs(user) {
           // try{
                String sourceSystmId = '54555334';

                Test.startTest();            
                Account talAcc1 = CreateTalentTestData.createTalentAccount();
                Account talAcc2 = CreateTalentTestData.createTalentAccount();

                talAcc1.Talent_Ownership__c = 'TEK';
                talAcc2.Talent_Ownership__c = 'TEK';

                update (new List<Account>{talAcc1,talAcc2});

                Contact talCont1 =  new Contact();
                talCont1.FirstName = 'test one';
                talCont1.Peoplesoft_ID__c = '123456';
                talCont1.LastName = 'contact';
                talCont1.Phone = '1122334455';
                talCont1.Email = 'testemail@testalg.com';
                talCont1.accountId = talAcc1.Id;
                talCont1.MailingStreet = 'Ancells Road';
                talCont1.MailingState = 'New Jersey';
                talCont1.MailingCity = 'Fair Lawn';
                talCont1.MailingPostalCode = '07410';
                talCont1.Title = 'Java Architect';


                //CreateTalentTestData.createTalentContact(talAcc1);               

                sourceSystmId = '54534544';

                Contact talCont2 =  new Contact();
                //CreateTalentTestData.createTalentContact(talAcc2);               
                talCont2.Email = 'testemail2@testalg.com';
                talCont2.FirstName = 'test one 2';
                talCont2.Peoplesoft_ID__c = '123456';
                talCont2.Phone = '1122334455';
                talCont2.LastName = 'contact 2';
                talCont2.accountId = talAcc2.Id;
                talCont2.MailingStreet = 'Ancells Road';
                talCont2.MailingState = 'New Jersey';
                talCont2.MailingCity = 'Fair Lawn';
                talCont2.MailingPostalCode = '07410';
                talCont2.Title = 'Java Architect';



                insert (new List<Contact>{talCont1,talCont2});

                Unified_Merge__c utm = new Unified_Merge__c();
                utm.Input_ID_1__c = String.valueOf(talCont1.Id);
                utm.Input_ID_2__c = String.valueOf(talCont2.Id);
                utm.Process_State__c = 'Ready';
				utm.Request_Source_Name__c = 'ATS';
                insert utm;

                Test.stopTest();

                Test.setCreatedDate(talCont2.Id,(Datetime.now().addDays(-1)));

                system.debug('************* DBUG');
                List<String> accIds = new List<String>{talAcc1.Id,talAcc2.Id};
                System.debug(accIds);



                ATSUnifiedTalentMerge.processUTM(utm.id,''+talCont1.Id,''+talCont2.Id,'','','',ATSUnifiedTalentMerge.UTMProcessLevel.FULLPROCESS);

                // Check if yousee any records in the 
                List<Unified_Merge__c> mergeResults =  [SELECT Id, Survivor_Account_ID__c FROM Unified_Merge__c WHERE Survivor_Account_ID__c in :(new List<String>{talAcc1.Id,talAcc2.Id})];

                System.assert(mergeResults.size()>=0);
            /*}catch(Exception ex){
                System.debug(ex.getStackTraceString());
            }*/

      }
    }
    static testMethod void  startUTMBatchProcess(){
         User user = TestDataHelper.createUser('System Integration');
        System.runAs(user) {
                String sourceSystmId = '54555334';
            
                Test.startTest();            
                Account talAcc1 = CreateTalentTestData.createTalentAccount();
                Account talAcc2 = CreateTalentTestData.createTalentAccount();

                talAcc1.Talent_Ownership__c = 'TEK';
                talAcc2.Talent_Ownership__c = 'TEK';

                update (new List<Account>{talAcc1,talAcc2});

                Contact talCont1 =  new Contact();
                talCont1.FirstName = 'test one';
                talCont1.Peoplesoft_ID__c = '123456';
                talCont1.LastName = 'contact';
                talCont1.Phone = '1122334455';
                talCont1.Email = 'testemail@testalg.com';
                talCont1.accountId = talAcc1.Id;
                talCont1.MailingStreet = 'Ancells Road';
                talCont1.MailingState = 'New Jersey';
                talCont1.MailingCity = 'Fair Lawn';
                talCont1.MailingPostalCode = '07410';
                talCont1.Title = 'Java Architect';

                sourceSystmId = '54534544';
                Contact talCont2 =  new Contact();
                talCont2.Email = 'testemail2@testalg.com';
                talCont2.FirstName = 'test one 2';
                talCont2.Peoplesoft_ID__c = '123456';
                talCont2.Phone = '1122334455';
                talCont2.LastName = 'contact 2';
                talCont2.accountId = talAcc2.Id;
                talCont2.MailingStreet = 'Ancells Road';
                talCont2.MailingState = 'New Jersey';
                talCont2.MailingCity = 'Fair Lawn';
                talCont2.MailingPostalCode = '07410';
                talCont2.Title = 'Java Architect';
            	List<Contact> contactList = new List<Contact>{talCont1,talCont2};
                insert (contactList);
            
            	// Create a record in UTM
            	String query = 'SELECT Id,Input_ID_1__c,Input_ID_2__c,Transaction_Batch_ID__c,Request_Source__c,Request_Source_Name__c  FROM Unified_Merge__c WHERE Process_State__c =  ' + '\'Ready\'';
            	List<Unified_Merge__c> utms = CreateTalentTestData.insertUnifiedMergeRecrds(new Map<String,String>{contactList[0].Id => contactList[1].id},'Ready',null,null, null,null);
				Id batchInstanceId = Database.executeBatch(new ATSUnifiedTalentMergeBatch(query,'Full'), 5);
            	Test.stopTest();
            
				Unified_Merge__c utmsRes = [Select Id, Process_State__c from Unified_Merge__c where Id =: utms[0].Id];
            	System.debug('utmsRes ' + utmsRes);
            	System.assert(utmsRes != null);
        }
    }
  static testMethod void  mergeOfMergesUTMBatchProcess(){
      User user = TestDataHelper.createUser('System Integration');
        System.runAs(user) {
                String sourceSystmId = '54555334';
            
                Test.startTest();
                Account talAcc1 = CreateTalentTestData.createTalentAccount();
                Account talAcc2 = CreateTalentTestData.createTalentAccount();

                talAcc1.Talent_Ownership__c = 'TEK';
                talAcc2.Talent_Ownership__c = 'TEK';

                update (new List<Account>{talAcc1,talAcc2});

                Contact talCont1 =  new Contact();
                talCont1.FirstName = 'test one';
                talCont1.Peoplesoft_ID__c = '123456';
                talCont1.LastName = 'contact';
                talCont1.Phone = '1122334455';
                talCont1.Email = 'testemail@testalg.com';
                talCont1.accountId = talAcc1.Id;
                talCont1.MailingStreet = 'Ancells Road';
                talCont1.MailingState = 'New Jersey';
                talCont1.MailingCity = 'Fair Lawn';
                talCont1.MailingPostalCode = '07410';
                talCont1.Title = 'Java Architect';

                sourceSystmId = '54534544';
                Contact talCont2 =  new Contact();
                talCont2.Email = 'testemail2@testalg.com';
                talCont2.FirstName = 'test one 2';
                talCont2.Peoplesoft_ID__c = '123456';
                talCont2.Phone = '1122334455';
                talCont2.LastName = 'contact 2';
                talCont2.accountId = talAcc2.Id;
                talCont2.MailingStreet = 'Ancells Road';
                talCont2.MailingState = 'New Jersey';
                talCont2.MailingCity = 'Fair Lawn';
                talCont2.MailingPostalCode = '07410';
                talCont2.Title = 'Java Architect';
            	List<Contact> contactList = new List<Contact>{talCont1,talCont2};
                insert (contactList);
            
            	
            	String query = 'SELECT Id,Input_ID_1__c,Input_ID_2__c,Transaction_Batch_ID__c,Request_Source__c,Request_Source_Name__c  FROM Unified_Merge__c WHERE Process_State__c =  ' + '\'Ready\'';
            	List<Unified_Merge__c> utms = CreateTalentTestData.insertUnifiedMergeRecrds(new Map<String,String>{contactList[0].Id => contactList[1].id},'Ready',null,null, null,null);
				Id batchInstanceId = Database.executeBatch(new ATSUnifiedTalentMergeBatch(query), 5);
            	//Test.stopTest();
	            Unified_Merge__c utmsRes = [Select Id, Process_State__c,Victim_Contact_ID__c,Survivor_Contact_ID__c from Unified_Merge__c where Id =: utms[0].Id];
            	System.assert(utmsRes != null);
            	
            	//Test.startTest();
				 utmsRes = [Select Id, Process_State__c,Victim_Contact_ID__c,Survivor_Contact_ID__c from Unified_Merge__c where Id =: utms[0].Id];
            	System.debug('mergeOfMergesUTMBatchProcess ' + utmsRes);
            	//System.assert(utmsRes != null && utmsRes.Process_State__c != 'Ready');
            	sourceSystmId = '545343390';
                Contact talCont3 =  new Contact();
                talCont3.Email = 'testemail3@testalg.com';
                talCont3.FirstName = 'test one 3';
                talCont3.Peoplesoft_ID__c = '123456';
                talCont3.Phone = '1122334455';
                talCont3.LastName = 'contact 3';
                talCont3.accountId = talAcc2.Id;
                talCont3.MailingStreet = 'Ancells Road';
                talCont3.MailingState = 'New Jersey';
                talCont3.MailingCity = 'Fair Lawn';
                talCont3.MailingPostalCode = '07410';
                talCont3.Title = 'Java Architect';
				contactList = new List<Contact>{talCont3};
                insert (contactList);
	            utms = CreateTalentTestData.insertUnifiedMergeRecrds(new Map<String,String>{utmsRes.Victim_Contact_ID__c => talCont3.Id},'Ready',null,null,null,null);
            	// Make another merge with lost record
            	batchInstanceId = Database.executeBatch(new ATSUnifiedTalentMergeBatch(query), 5);
            	//Test.stopTest();

				//Test.startTest();            
				Unified_Merge__c utmsRes3 = [Select Id, Process_State__c,Victim_Contact_ID__c,Survivor_Contact_ID__c from Unified_Merge__c where Id =: utms[0].Id];
            	System.debug('mergeOfMergesUTMBatchProcess 3 ' + utmsRes);
            	//System.assert(utmsRes != null && utmsRes.Process_State__c != 'Ready');
            	//
            	//delete talCont1;
            	sourceSystmId = '500943390';
                Contact talCont4 =  new Contact();
                talCont4.Email = 'testemail4@testalg.com';
                talCont4.FirstName = 'test one 4';
                talCont4.Peoplesoft_ID__c = '123456';
                talCont4.Phone = '1122334455';
                talCont4.LastName = 'contact 4';
                talCont4.accountId = talAcc2.Id;
                talCont4.MailingStreet = 'Ancells Road';
                talCont4.MailingState = 'New Jersey';
                talCont4.MailingCity = 'Fair Lawn';
                talCont4.MailingPostalCode = '07410';
                talCont4.Title = 'Java Architect';
				contactList = new List<Contact>{talCont4};
                insert (contactList);
				//delete talCont4;
	            utms = CreateTalentTestData.insertUnifiedMergeRecrds(new Map<String,String>{utmsRes3.Victim_Contact_ID__c => talCont4.Id},'Ready',null,null,null,null);
            	// Make another merge with lost record
            	batchInstanceId = Database.executeBatch(new ATSUnifiedTalentMergeBatch(query), 5);
            	Test.stopTest();            
				utmsRes = [Select Id, Process_State__c,Victim_Contact_ID__c from Unified_Merge__c where Id =: utms[0].Id];
            	System.debug('mergeOfMergesUTMBatchProcess 4 ' + utmsRes);
            	System.assert(utmsRes != null);
        }
  }
    static testMethod void  mergeOfMergesUTMBatchTestBypassBatch(){
        User user = TestDataHelper.createUser('System Integration');
        System.runAs(user) {
                String sourceSystmId = '54555334';
            
                Test.startTest();
                Account talAcc1 = CreateTalentTestData.createTalentAccount();
                Account talAcc2 = CreateTalentTestData.createTalentAccount();

                talAcc1.Talent_Ownership__c = 'TEK';
                talAcc2.Talent_Ownership__c = 'TEK';

                update (new List<Account>{talAcc1,talAcc2});

                Contact talCont1 =  new Contact();
                talCont1.FirstName = 'test one';
                talCont1.Peoplesoft_ID__c = '123456';
                talCont1.LastName = 'contact';
                talCont1.Phone = '1122334455';
                talCont1.Email = 'testemail@testalg.com';
                talCont1.accountId = talAcc1.Id;
                talCont1.MailingStreet = 'Ancells Road';
                talCont1.MailingState = 'New Jersey';
                talCont1.MailingCity = 'Fair Lawn';
                talCont1.MailingPostalCode = '07410';
                talCont1.Title = 'Java Architect';

                sourceSystmId = '54534544';
                Contact talCont2 =  new Contact();
                talCont2.Email = 'testemail2@testalg.com';
                talCont2.FirstName = 'test one 2';
                talCont2.Peoplesoft_ID__c = '123456';
                talCont2.Phone = '1122334455';
                talCont2.LastName = 'contact 2';
                talCont2.accountId = talAcc2.Id;
                talCont2.MailingStreet = 'Ancells Road';
                talCont2.MailingState = 'New Jersey';
                talCont2.MailingCity = 'Fair Lawn';
                talCont2.MailingPostalCode = '07410';
                talCont2.Title = 'Java Architect';
            	List<Contact> contactList = new List<Contact>{talCont1,talCont2};
                insert (contactList);
            
            	// Insert a record in UTM and mark it has been completed the merge

           		String query = 'SELECT Id,Input_ID_1__c,Input_ID_2__c,Transaction_Batch_ID__c,Request_Source__c,Request_Source_Name__c  FROM Unified_Merge__c WHERE Process_State__c =  ' + '\'Ready\'';
            	List<Unified_Merge__c> utms = CreateTalentTestData.insertUnifiedMergeRecrds(new Map<String,String>{contactList[0].Id => contactList[1].id},'Ready',talAcc1.Id,talAcc2.Id, talCont1.Id,talCont2.Id);
                delete talCont2;
            
            	Contact talCont3 =  new Contact();
                talCont3.Email = 'testemail3@testalg.com';
                talCont3.FirstName = 'test one 3';
                talCont3.Peoplesoft_ID__c = '123456';
                talCont3.Phone = '1122334455';
                talCont3.LastName = 'contact 3';
                talCont3.accountId = talAcc2.Id;
                talCont3.MailingStreet = 'Ancells Road';
                talCont3.MailingState = 'New Jersey';
                talCont3.MailingCity = 'Fair Lawn';
                talCont3.MailingPostalCode = '07410';
                talCont3.Title = 'Java Architect';
                insert talCont3;
                utms = CreateTalentTestData.insertUnifiedMergeRecrds(new Map<String,String>{talCont3.Id => talCont2.id},'Ready',talAcc2.Id,talAcc2.Id, talCont2.Id,talCont2.Id);
            
				Id batchInstanceId = Database.executeBatch(new ATSUnifiedTalentMergeBatch(query), 5);
            	Test.stopTest();
            
                Unified_Merge__c utmsRes = [Select Id, Input_ID_1__c,Input_ID_2__c,Process_State__c,Victim_Contact_ID__c,Survivor_Contact_ID__c,Request_Source_Name__c from Unified_Merge__c where Id =: utms[0].Id];
            	System.debug('mergeOfMergesUTMBatchTestBypassBatch  ' + utmsRes);
            	System.assert(utmsRes != null);
        }
    }
}