global class ATS_AGS_BatchChatterPost implements Schedulable {
    global static Id chatterGroupId;
    global void execute (SchedulableContext sc){   
        List<Case> Ccases = [Select id,CaseNumber,Description__c,IsVisibleInSelfService,AccountId,Recordtype.Name from Case where AccountId = null AND IsVisibleInSelfService = true AND Recordtype.Name = 'JobApplication' ];
        List<CollaborationGroup> chatterGroup = [select id from CollaborationGroup where Name ='AGS Lloyds Web Applications' ];
        
        if(!chatterGroup.isEmpty()){
            chatterGroupId = chatterGroup[0].id;   
            
            List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
            
            for (Case cc : Ccases) {
                if(cc.Description__c != null){
                    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                    
                    messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
                    
                    mentionSegmentInput.id = chatterGroupId;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                    textSegmentInput.text = ' Case # '+cc.CaseNumber+'. '+ cc.Description__c;
                    messageBodyInput.messageSegments.add(textSegmentInput);
                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = chatterGroupId;
                    ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
                    batchInputs.add(batchInput);
                    
                }
                cc.IsVisibleInSelfService = false;
            }
            ConnectApi.ChatterFeeds.postFeedElementBatch(null, batchInputs);
            Update Ccases;
        }
    }
    
    public static void schedule() {
        ATS_AGS_BatchChatterPost ags =  new ATS_AGS_BatchChatterPost();
        if(Test.isRunningTest()){
            System.schedule('test AGS LLOYD Hourly Unsuccessful Case Chatter Post Update', '0 0 * * * ?', ags);
        }else{
            System.schedule('AGS LLOYD Hourly Unsuccessful Case Chatter Post Update', '0 0 * * * ?', ags);
        }
    }
    
}