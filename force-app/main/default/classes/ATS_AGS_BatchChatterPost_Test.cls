@isTest(seeAlldata=true)

public class ATS_AGS_BatchChatterPost_Test {

    public static testMethod void schedultestmethod(){
       CollaborationGroup cg = new CollaborationGroup(Name='AGS Lloyds Web Applications', CollaborationType='Public');
        Insert cg;
        
        List<CollaborationGroup> chatterGroup = [select id from CollaborationGroup where Name ='AGS Lloyds Web Applications' ];
        system.debug('chatterGroup '+chatterGroup);
        Case tCase = new Case(
                              RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('JobApplication').getRecordTypeId(),
                              Origin = 'Web',
                              IsVisibleInSelfService = true,
                              Description__c='Test');
        insert tCase;
        
        ATS_AGS_BatchChatterPost.schedule();
    }
}