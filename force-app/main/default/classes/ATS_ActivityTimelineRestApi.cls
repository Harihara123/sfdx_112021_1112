@RestResource(urlMapping='/activitytimeline/*')
global class ATS_ActivityTimelineRestApi {
    
    public class RestApiException extends Exception {}
    
    @HttpGet
    global static Contact getContactWithActivities() {
        Map<String,String> params = RestContext.request.params;
        
        try {
            return  getContact(params); 
        } catch (Exception e) {
            throw new RestApiException('Failed to Construct the query');
        }
    }

    private static Contact getContact(Map<String, String> params) {
        String queryStr = '';
        
        try {
              queryStr = getQueryStr(params);
        } catch (Exception e) {
              throw new RestApiException('Failed to Construct the query');
        }
        
        List<SObject> contactList = Database.query(queryStr);
         
        return ((contactList != null && !contactList.isEmpty()) ? ((Contact) contactList[0]) : null); 
      
    }
    
    private static String getQueryStr(Map<String, String> params) {

        String accountId = params.get('accountid');
        String dataObjectToken = params.get('dataobjecttoken');
        String sortOrderToken = params.get('sortordertoken');
        String loadMoreToken = params.get('loadmoretoken');
        String queryFilterToken = params.get('queryFilterToken');
        
        //System.Debug(logginglevel.warn, '>>>> the value of queryFilterToken  is ' + queryFilterToken);
        
        queryFilterToken = ' WHERE ActivityType NOT IN (\'Linked\', \'Submitted\', \'Interviewing\', \'Offer Accepted\',\'Offer Extended\' ,\'Not Proceeding\',\'Started\', \'Applicant\', \'Screening\',\'Interview Requested\',\'Pursuing\',\'LPQ in Process\',\'Internal Interviewing\', \'On Hold\',\'Network Referral\',\'Referencing\',\'Anonymous Promo\',\'Carve-Out\') ' + 
            (String.isNotBlank(queryFilterToken) ? ' AND ' + queryFilterToken : ''); 
              
        if (dataObjectToken == null || dataObjectToken == '') dataObjectToken = 'ActivityHistories';
        if (sortOrderToken == null || sortOrderToken == '') sortOrderToken = 'DESC';
        if (loadMoreToken == null || loadMoreToken == '') loadMoreToken = '3';
        
        System.Debug(logginglevel.warn, '>>>> the value of accountId is ' + accountId );
        if (accountId != null && accountId != '') accountId = String.escapeSingleQuotes(accountId);
        else {
            throw new RestApiException('accountid parameter must contain a valid Account Id.');
        }
       // Escape single quotes        
        dataObjectToken = String.escapeSingleQuotes(dataObjectToken);
        sortOrderToken = String.escapeSingleQuotes(sortOrderToken);
        loadMoreToken = String.escapeSingleQuotes(loadMoreToken);

        String query =  ' SELECT  ( SELECT Id, ActivityDate, Completed__c, StartDateTime, EndDateTime, ActivityType, Description, IsAllDayEvent, ' +
            ' Owner.Name, LastModifiedDate, IsClosed, IsTask, Location, Priority, Status, Subject, ' + 
            ' Pre_Meeting_Notes__c FROM ' + dataObjectToken + ' ' + queryFilterToken + ' '  + 
            ' ORDER BY ActivityDate '+sortOrderToken+' NULLS LAST, LastModifiedDate DESC LIMIT '+loadMoreToken+ ' )' +
            ' FROM Contact WHERE AccountId = \'' + accountId + '\'' ; 

        System.Debug(logginglevel.warn, '>>>> the value of query  is ' + query);
        return query; 

    }
}