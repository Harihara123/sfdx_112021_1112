global class ATS_BaseTimelineModel implements Comparable {
    //Used Only For Displaying Next Steps Message On Activities Timeline
    @AuraEnabled
    public Boolean hasNextSteps {get;set;}

    @AuraEnabled
    public Boolean hasPastActivity {get;set;}

    @AuraEnabled
    public string sprite {get;set;}
    
    @AuraEnabled
    public string icon {get;set;}
    
    @AuraEnabled
    public string iconTimeLineColor {get;set;}
    
    @AuraEnabled
    public string lineColor {get;set;}

    @AuraEnabled
    public Id ParentRecordId {get;set;}
    
    @AuraEnabled
    public Id RecordId {get;set;}

    @AuraEnabled
    public String CreatedBy {get;set;}

    @AuraEnabled
    public Boolean showDeleteItem {get;set;}
    
    @AuraEnabled
    public DateTime LastModifiedDate {get;set;}
        
    @AuraEnabled
    public String TimelineType {get;set;}
    
    @AuraEnabled
    public String ActivityType {get;set;}
    
    @AuraEnabled
    public Boolean NextStep {get;set;}

    @AuraEnabled
    public String Subject {get;set;}
    
    @AuraEnabled
    public String Detail {get;set;}

    @AuraEnabled
    public String Status {get;set;}

    @AuraEnabled
    public DateTime EventStartDateTime {get;set;}

    @AuraEnabled
    public DateTime EventEndDateTime {get;set;}
    
    // Text form of event duration
    @AuraEnabled
    public String EventTime {get;set;}
    
    // Task related to, Email "to", or list of attendees for events
    @AuraEnabled
    public String Recipients {get;set;}
    
    // Task/Event Asignee, or Email From
    @AuraEnabled
    public String Assigned {get;set;}
    
    // For indicating task completion
    @AuraEnabled
    public boolean Complete {get;set;}

    // Short event
    @AuraEnabled
    public String ShortDate {get;set;}

    @AuraEnabled
    public String PreMeetingNotes {get;set;}

    @AuraEnabled
    public String PostMeetingNotes {get;set;}

    @AuraEnabled
    public Boolean AllegisPlacement {get;set;}

    @AuraEnabled
    public Boolean CurrentAssignment {get;set;}

    @AuraEnabled
    public String OrgName {get;set;}

    @AuraEnabled
    public String Department {get;set;}

    @AuraEnabled
    public String JobTitle {get;set;}

    @AuraEnabled
    public Date EmploymentStartDate {get;set;}

    @AuraEnabled
    public Date EmploymentEndDate {get;set;}

    @AuraEnabled
    public Decimal Salary {get;set;}
    
    @AuraEnabled
    public Decimal Payrate {get;set;}
    
    @AuraEnabled
    public Decimal Bonus {get;set;}
    
    @AuraEnabled
    public Decimal BonusPct {get;set;}
    
    @AuraEnabled
    public Decimal OtherComp {get;set;}
    
    @AuraEnabled
    public Decimal CompensationTotal {get;set;}
    
    @AuraEnabled
    public String ReasonForLeaving {get;set;}
    
    @AuraEnabled
    public String Notes {get;set;}
    
    @AuraEnabled
    public String Year {get;set;}

    @AuraEnabled
    public String Certification {get;set;}
    
    @AuraEnabled
    public String Degree {get;set;}

    @AuraEnabled
    public String SchoolName {get;set;}
    
    @AuraEnabled
    public String Major {get;set;}



    @AuraEnabled
    public String Honors {get;set;}

    public DateTime ActualDate {get;set;}

    global Integer compareTo(Object objToCompare)
    {
        DateTime otherActualDate = objToCompare != null ? ((ATS_BaseTimelineModel)objToCompare).ActualDate : System.now();

        return (this.ActualDate.getTime() - otherActualDate.getTime()).intValue();
        
      
    }
    
}