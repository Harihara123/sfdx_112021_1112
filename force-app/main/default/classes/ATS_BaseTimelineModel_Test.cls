@isTest
public class ATS_BaseTimelineModel_Test {
    @isTest
    static void unitTest1(){
        List<ATS_BaseTimelineModel> timeLineList =  new List<ATS_BaseTimelineModel>();
        for(Integer i=0;i<10;i++){
            ATS_BaseTimelineModel timelynmodel = new ATS_BaseTimelineModel();
            timelynmodel.hasNextSteps=true;
            timelynmodel.hasPastActivity= true;
            timelynmodel.sprite='test';
            timelynmodel.icon='test';
            timelynmodel.iconTimeLineColor='green';
            timelynmodel.lineColor='green';
            timelynmodel.ParentRecordId='0016E00000R9DU0MMY';
            timelynmodel.RecordId='0016E00000R9DU0MMY';
            timelynmodel.CreatedBy='Admin';
            timelynmodel.showDeleteItem=true;
            timelynmodel.LastModifiedDate=System.now();
            timelynmodel.TimelineType='no type';
            timelynmodel.ActivityType='no type';
            timelynmodel.NextStep=true;
            timelynmodel.Subject='test Subject';
            timelynmodel.Detail='Tet Detail';
            timelynmodel.Status ='test';
            timelynmodel.EventStartDateTime=System.now().addMinutes(10);
            timelynmodel.EventEndDateTime=System.now().addMinutes(40);
            timelynmodel.EventTime='time in string';
            timelynmodel.Recipients='test@test.com';
            timelynmodel.Assigned='test@test.com';
            timelynmodel.Complete=true;
            timelynmodel.ShortDate='test short date';
            timelynmodel.PreMeetingNotes ='test PreMeetingNotes';
            timelynmodel.PostMeetingNotes='test PostMeetingNotes';
            timelynmodel.AllegisPlacement=true;
            timelynmodel.CurrentAssignment=true;
            timelynmodel.OrgName='';
			timelynmodel.Department ='';
            timelynmodel.JobTitle ='';
            timelynmodel.EmploymentStartDate =system.now().date();
            timelynmodel.EmploymentEndDate=system.now().date();
            timelynmodel.Salary=0.00;
            timelynmodel.Payrate=0.00;
             timelynmodel.Bonus=0.00;
            timelynmodel.BonusPct=0.00;
            timelynmodel.OtherComp =0.00;
            timelynmodel.CompensationTotal =0.00;
            timelynmodel.ReasonForLeaving='';
            timelynmodel.Notes='';
            timelynmodel.Year='18';
            timelynmodel.Certification='';
            timelynmodel.Degree='';
            timelynmodel.SchoolName ='';
            timelynmodel.Major ='yup';
            timelynmodel.Honors='';
            timelynmodel.ActualDate=System.now().addDays(i);
            timeLineList.add(timelynmodel);
            
        }
        timeLineList.sort();
    }
}