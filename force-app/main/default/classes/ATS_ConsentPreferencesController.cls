/***************************************************************************************************************************************
* Description - This controller fetching the logged in user's company and division and get list of C&P metadata. 
On basis of metadata dynamically selecting C&P field of contact set value in wrapper class to display on UI.
The way its displaying sae it dynamically selecting field C&P field of contact and updating the C&P fields
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Neel Kamal                  10/03/2018              Created
*****************************************************************************************************************************************/

public class ATS_ConsentPreferencesController  {
    private static Boolean isTextConsentAllwoed = false;
    private static  final  String CP_FIELDS =	'Account.BillingCountry,RecordTypeId,RecordType.Name,MailingCountry,Do_Not_Contact__c, PrefCentre_Aerotek_OptOut_Channels__c,PrefCentre_Aerotek_OptOut_Email__c,PrefCentre_Aerotek_OptOut_Mobile__c,PrefCentre_Aerotek_SMS_Channels__c,' +
        'PrefCentre_AGS_OptOut_Channels__c,PrefCentre_AGS_OptOut_Email__c,PrefCentre_AGS_OptOut_Mobile__c,PrefCentre_AGS_SMS_Channels__c, ' +
        'PrefCentre_Allegis_OptOut_Channels__c,PrefCentre_Allegis_OptOut_Email__c,PrefCentre_Allegis_OptOut_Mobile__c,PrefCentre_Allegis_SMS_Channels__c, ' +
        'PrefCentre_AllegisPartners_OptOut_Channe__c,PrefCentre_AllegisPartners_OptOut_Email__c,PrefCentre_AllegisPartners_OptOut_Mobile__c,PrefCentre_AllegisPartners_SMS_Channels__c, ' +
        'PrefCentre_AstonCarter_OptOut_Channels__c,PrefCentre_AstonCarter_OptOut_Email__c,PrefCentre_AstonCarter_OptOut_Mobile__c,PrefCentre_AstonCarter_SMS_Channels__c, ' +
        'PrefCentre_EASi_OptOut_Channels__c,PrefCentre_EASi_OptOut_Email__c,PrefCentre_EASi_OptOut_Mobile__c,PrefCentre_EASi_SMS_Channels__c, ' +
        'PrefCentre_MLA_OptOut_Channels__c,PrefCentre_MLA_OptOut_Email__c,PrefCentre_MLA_OptOut_Mobile__c,PrefCentre_MLA_SMS_Channels__c, ' +
        'PrefCentre_TEKsystems_OptOut_Channels__c,PrefCentre_TEKsystems_OptOut_Email__c,PrefCentre_TEKsystems_OptOut_Mobile__c,PrefCentre_TEKsystems_SMS_Channels__c, ' +
        'PrefCentre_TGS_OptOut_Channels__c,PrefCentre_TGS_OptOut_Email__c,PrefCentre_TGS_OptOut_Mobile__c,PrefCentre_TGS_SMS_Channels__c ';
    @AuraEnabled											
    public static ConsentPreferencesModelWrapper getConsentPreferencesModelProperties(ID contactID) {
        ConsentPreferencesModelWrapper modelProperties= new ConsentPreferencesModelWrapper();
        Map<String, Object> consentPrefMap = new Map<String, String>();
        User user = [select id, CompanyName, Division__c from User where id = :Userinfo.getUserId()];
        String companyName = getUserCompany(user);
        List<Consent_Preferences_Fields_Mapping__mdt> cpFieldList = getCPFieldMapping(); //get field mapping from metadata
        List<Contact> contactList = Database.query('select id,'+CP_FIELDS+' from contact where id = : contactID');
        List<ConsentPreferencesField> conPrefList = new List<ConsentPreferencesField>();
        
        //This block dynmically selecting contact's C&P field and setting value into wrapper class.
        //Need to get the value of 4 CP field out of 36.
        if(!contactList.isEmpty() && !cpFieldList.isEmpty()) {
            String fldValue;
            Contact con = contactList.get(0);
            ConsentPreferencesField conPref = null;
            
            for(Consent_Preferences_Fields_Mapping__mdt cpMDT : cpFieldList) {
                conPref = new ConsentPreferencesField();
                conPref.fieldLabel = cpMDT.Field_UI_Label__c;
                conPref.fieldValue = flipConsentValues(String.valueOf(con.get(cpMDT.CP_API_Field__c)));
                conPref.fieldType = cpMDT.Field_Type__c;
                conPref.fieldOrder = cpMDT.Fields_Order__c;
                conPref.isDisable = 'false';
				conPref.fieldDisplay = cpMDT.Field_Display__c;
				conPref.isConsentAllowed = cpMDT.Is_Request_Consent_Allowed__c;
                if('No'.equals(conPref.fieldValue)){
                    conPref.isDisable = 'true';
                }
                conPrefList.add(conPref);
            }
        }
        modelProperties.showSendConsentButton = sendConsentPreferenceEmailAllowed(contactID,Label.Consent_and_Preferences_Period_Hours,companyName);
        modelProperties.consentPreferencesFieldList =conPrefList;
		modelProperties.isRequestTextConsentAllowed = isTextConsentAllwoed;
        System.debug('modelProperties '+modelProperties);
        return modelProperties;
    }
    private static String flipConsentValues(String consentValue) {
		if(String.isNotBlank(consentValue)) {
			if(consentValue.equalsIgnoreCase('Yes')) {
				return 'No';
			}
			else if(consentValue.equalsIgnoreCase('No')) {
				return 'Yes';
			}
		}
		return consentValue;
	}
    @auraEnabled
    public static String saveConsentPref(String consPrefDTOJSON, ID contactID) {
        List<ConsentPreferencesField> consPrefList = (List<ConsentPreferencesField>) JSON.deserialize(consPrefDTOJSON,List<ConsentPreferencesField>.class);
        String status = 'SUCCESSFUL';
        //User user = [select id, OPCO__c from User where id = :Userinfo.getUserId()];
        List<Consent_Preferences_Fields_Mapping__mdt> cpFieldList = getCPFieldMapping();
        
        Map<String, String> fldLabelAPIMap = new Map<String, String>();
        
        for(Consent_Preferences_Fields_Mapping__mdt cp_mdt: cpFieldList){
            fldLabelAPIMap.put(cp_mdt.Field_UI_Label__c, cp_mdt.CP_API_Field__c);
        }
        
        
        Contact con = new Contact(Id=contactID);
        //getting the 4 fields out of 36 field which need to update 
        for(ConsentPreferencesField cpDTO: consPrefList) {
            if(cpDTO.fieldLabel.equalsIgnoreCase('Do Not Contact')) continue;//This field never will be updated on UI so skiping it.
            //System.debug('cpDTO.fieldLabel '+cpDTO.fieldLabel +' cpDTO.fieldValue '+cpDTO.fieldValue+' api name '+fldLabelAPIMap.get(cpDTO.fieldLabel));
            con.put(fldLabelAPIMap.get(cpDTO.fieldLabel), flipConsentValues(cpDTO.fieldValue));
        }
        
        //System.debug('Contact for update---'+con);
        try{
            update con;
        }catch(Exception exp){
            logException(Core_Log.logException('ATS_ConsentPreferencesController', exp.getMessage(), 'saveConsentPref', con.Id, 'Contact', 'Saving Consent & Preferences'));
            throw new AuraHandledException(Label.Error);			
        }	
        return status;
    }
    @AuraEnabled
    public static Consent_And_Preferences_Banner_Object__mdt getConsentPreferencesBannerProperties(ID contactID){
        String country;
        String recType;
        String textVal;
        String emailVal;
        String recordTypeVal;
        User user = [select id, CompanyName, Division__c from User where id = :Userinfo.getUserId()];
        String companyName= getUserCompany(user);
        List<MC_Email__c> recent_emails = new List<MC_Email__c>();
        //System.debug('cpFieldList '+cpFieldList);
        Contact contact= Database.query('select id,'+CP_FIELDS+' from contact where id = : contactID');
		List<Consent_Preferences_Fields_Mapping__mdt> cpFieldList = getCPFieldMapping(); //get field mapping from metadata
        List<ConsentPreferencesField> conPrefList = new List<ConsentPreferencesField>();
        List<Consent_And_Preferences_Banner_Object__mdt> bannerMsgs = new List<Consent_And_Preferences_Banner_Object__mdt> ();
        
        
        recordTypeVal=contact.RecordType.Name;          
        String  contactCountry  = (recordTypeVal=='Talent')? contact.MailingCountry : contact.Account.BillingCountry;
		
		country = 'OTH';
        if(String.isNotEmpty(contactCountry)) { // Commented by Santosh - this change is giving component error			
			if(isCanadaCountryCode(contactCountry)) {
				Country = 'CAN';
			}            
        }    
        
        //This block dynmically selecting contact's C&P field and setiing value into wrapper class.
        //Need to get the value of 4 CP field out of 36.
       for(Consent_Preferences_Fields_Mapping__mdt cpMDT : cpFieldList) {
            if(cpMDT.Fields_Order__c==1 ){//field order 1 is email preference
				if(cpMDT.field_display__c) {
					emailVal = flipConsentValues(String.valueOf(contact.get(cpMDT.CP_API_Field__c)));
				}
				else {
					emailVal = 'Yes';
				}
                
            }else if(cpMDT.Fields_Order__c==2){//field order 2 is text preference
				if(cpMDT.field_display__c) {
					textVal= flipConsentValues(String.valueOf(contact.get(cpMDT.CP_API_Field__c)));
				}
				else {
					textVal ='Yes';
				}
                
            }
        }

		bannerMsgs = [select Allow_Resend__c,Display_Send_Consent_Button__c,is_Talent__c,Non_US_Location__c,
                      Result_Email__c,Result_Message__c,Result_Text__c,Type__c,Message__c,Result_Text_Message__c 
                      from Consent_And_Preferences_Banner_Object__mdt 
                      where Country__c=:country and RecordType__c=:recordTypeVal and Text__c=:textVal and Email__c=:emailVal LIMIT 1];
        //Sandeep:S-106760
        if(!bannerMsgs.IsEmpty()){
            bannerMsgs.get(0).Allow_Resend__c= sendConsentPreferenceEmailAllowed(contactID,Label.Consent_and_Preferences_Period_Hours,companyName);
            return bannerMsgs.get(0);
        }
        return null;
        
    }

	private static Boolean  isCanadaCountryCode(String countryCode) {
		
		List<String> canCountryList = Label.CANADA_COUNTRY_CODE.split(',');
		Boolean flag = false;
		for(String cCode : canCountryList) {
			if(countryCode.equalsIgnoreCase(cCode)) {
				flag = true;
				break;
			}
		}
		return flag;
	}
    //Sandeep: sendConsentPrefEmail method will create record in MC Email object.
    //Marketing cloud will get this data for every 15 minutes and send the email.
    @auraEnabled 
    public static Boolean sendConsentPreferenceEmail(Id conId, String consentType){
        Contact contactDetails  =   [Select id,Salutation,FirstName,LastName,Email,MobilePhone,MailingState,MailingCountry From Contact where id=:conId];
        User    usertDetails    =   [Select id,Name,Email,Title,Phone, CompanyName, Division__c From User where id=:UserInfo.getUserId()];
        String companyName= getUserCompany(usertDetails);
        try {
            
            MC_Email__c mcEmail = new  MC_Email__c();
            mcEmail.UniqueID__c = contactDetails.Id + companyName+String.valueOf(System.now()); 
            mcEmail.UserID__c = usertDetails.Id; 
            mcEmail.Salutation__c = contactDetails.Salutation; 
            mcEmail.FirstName__c = contactDetails.FirstName; 
            mcEmail.LastName__c = contactDetails.LastName; 
            mcEmail.ContactID__c = contactDetails.Id;
            mcEmail.UserFullName__c = usertDetails.Name; 
            mcEmail.UserEmail__c = usertDetails.Email; 
            mcEmail.UserTitle__c = usertDetails.Title; 
            mcEmail.UserPhone__c = usertDetails.Phone; 
            mcEmail.OpCo__c  = companyName; 
            mcEmail.Contactemail__c = contactDetails.Email;
            mcEmail.Contactmobile__c = contactDetails.MobilePhone; 
            mcEmail.ContactState__c = contactDetails.MailingState; 
            mcEmail.ContactCountry__c = contactDetails.MailingCountry;
			if(consentType.equals('Email')) {
				mcEmail.ResponseEmailID__c = Label.ATS_Consent_Response;
			}
			else {
				mcEmail.ResponseEmailID__c = Label.ATS_Text_Consent_Response;
			}
			
            
            insert mcEmail;
        }catch(Exception ex){
            logException(Core_Log.logException('ATS_ConsentPreferencesController', ex.getMessage(), 'sendConsentPreferenceEmail', contactDetails.Id, 'MC_Email__c', 'Saving Consent & Preferences email record'));
        }
        
        return null;
    }
    public static String getUserCompany(User user){
        String userCompany;
        List<Consent_Preferences_Company_Division_Map__mdt> cpCompDivisionList = new List<Consent_Preferences_Company_Division_Map__mdt>();
        
        //if(user.companyName.equalsIgnoreCase('AG_EMEA')) {
        cpCompDivisionList = [select CP_Company__c from Consent_Preferences_Company_Division_Map__mdt where Company_Name__c =: user.CompanyName and Division__c =: user.Division__c];	
		if(!cpCompDivisionList.isEmpty()) {
			userCompany = cpCompDivisionList.get(0).CP_Company__c;
        }else{
            userCompany = user.companyName;    
        }
        return userCompany;
    }  
    //Sandeep:S-106760
    public static Boolean sendConsentPreferenceEmailAllowed(Id contactId,String timePeriod,String companyName){
        List<MC_Email__c> recent_emails = new List<MC_Email__c>();
        DateTime lastNhours = DateTime.now().addHours(Integer.valueof(timePeriod) * -1);
        String query ='Select id From MC_Email__c Where ContactID__c=:contactID and OpCo__c=:companyName and CreatedDate >= :lastNhours';//;Label.Resend_Consent_Period_Days;
        recent_emails = Database.query(query);
        return recent_emails.isEmpty();
    }
    //Wrapper class
    public class ConsentPreferencesModelWrapper {
        @auraEnabled public Boolean showSendConsentButton{get;set;}
		@auraEnabled public Boolean isRequestTextConsentAllowed{get;set;}
        @auraEnabled public List<ConsentPreferencesField> consentPreferencesFieldList{get;set;}
        
    }
    public class ConsentPreferencesField{
        @AuraEnabled public String fieldLabel{get;set;}
        @AuraEnabled public String fieldValue{get;set;}
        @AuraEnabled public String fieldType{get;set;}
        @AuraEnabled public Decimal fieldOrder{get;set;}
        @AuraEnabled public String isDisable{get;set;}        
        @AuraEnabled public Boolean fieldDisplay{get;set;}
		@AuraEnabled public Boolean isConsentAllowed{get;set;}
    }
    
	/*
	   This method have fucntionality to get CP metadata on user's Company and Division. 
		If user's company is not AG_EMEA then just need to fetch CP field on basis of company. 

		If user's company is AG_EMEA then first need to decide which company will get CP field. 
		After getting company then go as the first step.

		It also handling which Consent field(Email and text) need to display on GUI (field_display__c).
		//S-109361 If User's Company = 'Aerotek, Inc' and Division='Aston Cartor' then CP Company = 'Aston cartor'
		This kind of record need special handling to display Consent Field.  Fields Display_Text_Consent__c and 
		Display_Email_Consent__c will override the field field_display__c.
	*/
    private static List<Consent_Preferences_Fields_Mapping__mdt> getCPFieldMapping() {
        User user = [select id, CompanyName, Division__c from User where id = :Userinfo.getUserId()];
        List<Consent_Preferences_Fields_Mapping__mdt> cpFieldList = new List<Consent_Preferences_Fields_Mapping__mdt>();
        
        String cpCompany = user.CompanyName;	
        String division = user.Division__c;
		Boolean displayTextConsant;
		Boolean displayEmailConsant;
        if(String.isBlank(user.Division__c)) {
            division = 'BLANK';
        }
        List<Consent_Preferences_Company_Division_Map__mdt> cpCompDivisionList = [select CP_Company__c, Display_Email_Consent__c, Display_Text_Consent__c from Consent_Preferences_Company_Division_Map__mdt where Company_Name__c =: cpCompany and Division__c =: division];
        if(!cpCompDivisionList.isEmpty()) {
            cpCompany =  cpCompDivisionList.get(0).CP_Company__c;  
			displayTextConsant = cpCompDivisionList.get(0).Display_Text_Consent__c;
			displayEmailConsant = cpCompDivisionList.get(0).Display_Email_Consent__c;
        } 
        
        
        for(Consent_Preferences_Fields_Mapping__mdt cpfm : [select CP_API_Field__c, Field_UI_Label__c, Field_Type__c, Fields_Order__c, field_display__c, Is_Request_Consent_Allowed__c from Consent_Preferences_Fields_Mapping__mdt 
															where (Company_Name__c =: cpCompany OR Company_Name__c ='ALL') order by Fields_Order__c ASC]) {
			if(!cpCompDivisionList.isEmpty()) {
				if(cpfm.Field_UI_Label__c.contains('Email') &&  cpfm.Field_Type__c.equals('Picklist')) {
					cpfm.field_display__c = displayEmailConsant;
				}
				else if(cpfm.Field_UI_Label__c.contains('Text') && cpfm.Field_Type__c.equals('Picklist')) {
					cpfm.field_display__c = displayTextConsant;
				}
			}
			if(cpfm.Field_UI_Label__c.contains('Text') && cpfm.Field_Type__c.equals('Picklist') && cpfm.Is_Request_Consent_Allowed__c) {
				isTextConsentAllwoed = true;
			}
			cpFieldList.add(cpfm);
		}

        return cpFieldList;
    }
    
    private static void logException(log__c logEx) {
        List<log__c> lstLogs = new List<log__c>();
        lstLogs.add(logEx);
        Database.insert(lstLogs, false);
    }
    
    private class ConsentBannerProperties{
        @auraEnabled
        string customMetaDataValue{get;set;}
        @auraEnabled
        Boolean anyrecentConsentMail{get; set;}
    }
    public class ConsentandPreferencesException extends exception{}
}