@isTest
public class ATS_ConsentPreferencesController_Test  {
    static testMethod void consentPrefDatatest() {
        Test.startTest();
        User usr = TestDataHelper.createUser('Single Desk 1');
        usr.opco__c = 'Test';
        usr.CompanyName='TEKsystems, Inc.';
        insert usr;  
        //System.debug('User'+ usr.companyName + '---'+usr.division__c);
        System.runAs(usr) {
            //System.debug('usr detail--'+[select id, profile.name, opco__c from user where id =: UserInfo.getUserID()]);
            Contact contact = CreateTalentTestData.createTalentContact(CreateTalentTestData.createTalentAccount());
            contact.PrefCentre_TEKsystems_OptOut_Channels__c = '';
            contact.PrefCentre_TEKsystems_OptOut_Email__c = 'No';
            contact.PrefCentre_TEKsystems_OptOut_Mobile__c='Yes';
            contact.PrefCentre_TEKsystems_SMS_Channels__c = '';
            
            update contact;
            
            ID conID = contact.id;
            
            ATS_ConsentPreferencesController.ConsentPreferencesModelWrapper resultWraper = ATS_ConsentPreferencesController.getConsentPreferencesModelProperties(contact.id);
            //System.debug('dtoList---'+dtoList);
            for(ATS_ConsentPreferencesController.ConsentPreferencesField fld : resultWraper.consentPreferencesFieldList ){
                if(fld.fieldOrder == 1) {
                    System.assertEquals(fld.fieldLabel,'TEKsystems Emails');
                    break;
                }
            }
            //System.assertEquals(dtoList.size(), 5);
            
            String jsonStr = JSON.serialize(resultWraper.consentPreferencesFieldList);
            String testSave = ATS_ConsentPreferencesController.saveConsentPref(jsonStr, conID);
            System.assertEquals(testSave, 'SUCCESSFUL');
        }
        
        
        
        Test.stopTest();
        
    }
    
    static testMethod void consentPrefAG_EMEA() {
        Test.startTest();
        User usr = TestDataHelper.createUser('Single Desk 1');
        usr.opco__c = 'Test';
        usr.CompanyName='AG_EMEA';
        usr.division__c='Applications';
        insert usr;
        //System.debug('User'+ usr.companyName + '---'+usr.division__c);
        System.runAs(usr) {
            //System.debug('usr detail--'+[select id, profile.name, opco__c from user where id =: UserInfo.getUserID()]);
            Contact contact = CreateTalentTestData.createTalentContact(CreateTalentTestData.createTalentAccount());
            contact.PrefCentre_TEKsystems_OptOut_Channels__c = '';
            contact.PrefCentre_TEKsystems_OptOut_Email__c = 'No';
            contact.PrefCentre_TEKsystems_OptOut_Mobile__c='Yes';
            contact.PrefCentre_TEKsystems_SMS_Channels__c = '';
            
            update contact;
            
            ID conID = contact.id;
            
            ATS_ConsentPreferencesController.ConsentPreferencesModelWrapper result = ATS_ConsentPreferencesController.getConsentPreferencesModelProperties(contact.id);
            
            String jsonStr = JSON.serialize(result.consentPreferencesFieldList);
            String testSave = ATS_ConsentPreferencesController.saveConsentPref(jsonStr, conID);
            System.assertEquals(testSave, 'SUCCESSFUL');
        }
        Test.stopTest();
        
    }
    static testMethod void sendConsentPreferenceEmail(){
        User usr = TestDataHelper.createUser('Single Desk 1');
        usr.opco__c = 'Test';
        usr.CompanyName='AG_EMEA';
        usr.division__c='Applications';
        insert usr;
        System.runAs(usr){
            Contact contact = CreateTalentTestData.createTalentContact(CreateTalentTestData.createTalentAccount());
			Test.startTest();
			ATS_ConsentPreferencesController.sendConsentPreferenceEmail(contact.Id, 'Email');
			ATS_ConsentPreferencesController.sendConsentPreferenceEmail(contact.Id, 'Text');
			Test.stopTest();
        }
    }
    static testMethod void getBannerMsg(){
        User usr = TestDataHelper.createUser('Single Desk 1');
        usr.opco__c = 'AstonCarter';
        usr.CompanyName='AG_EMEA';
        usr.division__c='Applications';
        insert usr;
        System.runAs(usr){
             Contact contact = CreateTalentTestData.createTalentContact(CreateTalentTestData.createTalentAccount());
        Test.startTest();
        ATS_ConsentPreferencesController.getConsentPreferencesBannerProperties(contact.Id);
        Test.stopTest();
        }
    }
    
}