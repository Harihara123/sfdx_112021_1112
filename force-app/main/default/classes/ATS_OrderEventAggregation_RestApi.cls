@RestResource(urlMapping='/ordereventaggregatedcount/*')
  global class ATS_OrderEventAggregation_RestApi {
    
    public class RestApiException extends Exception {}
      
      @HttpGet    
      global static Map<String, Map<String, String>> getOrderEventAggregation() {
        
        Map<String,String> params = RestContext.request.params;
        
        try {
          return getEventOrderAggregation(params);
        } catch (Exception e) {
          throw new RestApiException('Failed to construct the query');
        }
        
      }
      
      private static Map<String, Map<String, String>> getEventOrderAggregation(Map<String, String> params) {
        
        Map<String, Map<String, String>> eventAggregatedMap = new Map<String, Map<String, String>>();
        
        List<Order> listJobOrders = getListOfJobOrders(params); 
        
        if (listJobOrders != null && listJobOrders.size() > 0) {
          List<String> listOrderIds = getOrderIdString(listJobOrders); 
          List<String> listOrderStatus = getListOfOrderStatus();
          getEventAggregateStatus(listOrderStatus, listOrderIds, eventAggregatedMap);
        }
        return eventAggregatedMap;
        
      }
      
      private static void getEventAggregateStatus(List<String> listStatus, List<String> listOrderIds, Map<String, Map<String, String>> eventAggregatedMap) {
        
        List<AggregateResult> result =  [SELECT Type, COUNT_distinct(whoid), max(lastmodifieddate)
        FROM Event
        where whatid in :listOrderIds 
        and Type in :listStatus
        GROUP BY type];               
        
        if (!result.isEmpty()) {
          
          for (AggregateResult r : result) {
            
            Map<String, String> eventMap = new Map<String, String>();
            
            String status = String.valueOf(r.get('Type'));
            
            eventMap.put('count', String.valueOf(r.get('expr0')));
            eventMap.put('lastModDate',  String.valueOf(r.get('expr1')) + '.000Z');
            System.Debug(logginglevel.warn, '>>>> the value of lastModDate *******=======>>>> ' + String.valueOf(r.get('expr1')));
            System.Debug(logginglevel.warn, '>>>> the value of stats *******=======>>>> ' + status );
            //getLocalTimeTest(String.valueOf(r.get('expr1')));
            eventAggregatedMap.put(status, eventMap);
            
          }
        }
        
      }
      
      private static List<Order> getListOfJobOrders(Map<String, String> params) {
        
        String jobId = params.get('jobid');
        
        if (jobId != null && jobId != '') {
          jobId = String.escapeSingleQuotes(jobId);
        } else {
          throw new RestApiException('******* =>  parameter must contain a valid Job Id.');
        }
        
        String orderListQuery = 'Select Id, status from Order where Recordtype.DeveloperName=\'OpportunitySubmission\' AND ATS_Job__c = \'' + jobId + '\'';//added RecType Condition by akshay ATS - 3344 
        
        List<Order> orderList = Database.query(orderListQuery);
        
        return orderList; 
      }
      
      private static List<String> getListOfOrderStatus() {
        List<String> listOrderStatusName = new List<String>();
          
        Schema.DescribeFieldResult fieldResult = Order.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            if(f.isActive()){
        		listOrderStatusName.add(f.getValue());
            }
        } 
          
        //String orderStatusQuery = 'Select status from Order GROUP BY status'; 
        //List<AggregateResult> result = Database.query(orderStatusQuery);
        //if (!result.isEmpty()) {
        //  for (AggregateResult r : result) {
        //    listOrderStatusName.add(String.valueOf(r.get('Status')));
        //  }
        //}
        
        return listOrderStatusName; 
        
      }
      
      private static List<String> getOrderIdString(List<Order> orderList) {
        
        string listOrderIdStr = ''; 
        List<String> listOrderIds = new List<String>();
        
        for (Order order : orderList) {
          listOrderIds.add(order.Id);
        }
        
        return listOrderIds; 
      }
      
      private static void getLocalTimeTest(String stringDate) {
        
        try {
          Datetime myDate = Datetime.valueOf(stringDate);
          System.Debug('myDate:' + myDate);
        } catch (Exception e) {
          throw new RestApiException('Failed to Construct DATE *****************:' + e);
        }
        
      }
      
    }