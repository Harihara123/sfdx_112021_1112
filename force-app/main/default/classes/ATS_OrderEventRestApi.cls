@RestResource(urlMapping='/orderevent/*')
global class ATS_OrderEventRestApi {
    
    public class RestApiException extends Exception {}
    
    @HttpGet    
    global static Event getOrderEvent() {

           Map<String,String> params = RestContext.request.params;
            
            try {
                   return  getEvent(params); 
            } catch (Exception e) {
                  throw new RestApiException('Failed to Construct the query');
            }
            
    }
    

    private static Event getEvent(Map<String, String> params) {
    
      
        String queryStr = '';
        
        try {
              queryStr = getQueryStr(params);
        } catch (Exception e) {
              throw new RestApiException('Failed to Construct the query');
        }
        
        List<SObject> eventList = Database.query(queryStr);
        
        //System.Debug(logginglevel.warn, '>>>> the value of eventList is ' + eventList.size() );
         
        return ((eventList  != null && !eventList .isEmpty()) ? ((Event) eventList[0]) : null); 
      
    }
    

    private static String getQueryStr(Map<String, String> params) {

        String OrderId = params.get('orderId');
  
        //System.Debug(logginglevel.warn, '>>>> the value of Order is ' + OrderId );
        
        if (OrderId != null && OrderId != '') OrderId = String.escapeSingleQuotes(OrderId);
        else {
            throw new RestApiException('orderId parameter must contain a valid order Id.');
        }
       

      String queryStr = 'SELECT  Id, WhatId, Description, Not_Proceeding_Reason__c, Interview_Type__c, Interview_Status__c, Interview_Client_Comments__c, ' +
      'Interview_Candidate_Comments__c, Submittal_Interviewers__c, StartDateTime, LastModifiedDate, Type, Source__c ' + 
        ' FROM Event where WhatId = \''  + orderId + '\' order by LastModifiedDate DESC LIMIT 1 ';
      
      System.Debug(logginglevel.warn, '>>>> the value of QueryStr is'  +  queryStr );
      
      return queryStr; 
                   
     

    }
}