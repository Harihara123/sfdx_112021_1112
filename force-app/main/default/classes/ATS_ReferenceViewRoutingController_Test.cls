@isTest
public class ATS_ReferenceViewRoutingController_Test {


    @isTest
    public static void getContactIdReferenceAccountTest(){
   
         
          ID talentContactId = ReferenceViewRoutingController.getContactIdReferenceAccount('');
          
          System.assertEquals(null, talentContactId);
          
         
          
          Contact contactTalent = createTalentRecord();
          Contact contactRef = createTalentReferenceRecord();
          Talent_Recommendation__c tR = createTalentRecommendation(contactTalent, contactRef);
          
          talentContactId = ReferenceViewRoutingController.getContactIdReferenceAccount(contactRef.accountId);
          
          
          
          System.assertEquals(talentContactId, contactTalent.Id);
    
    }
    
    
    
    /* @isTest
    public static void getContactIdReferenceContactTest(){
    
    
       ID talentContactId = ReferenceViewRoutingController.getContactIdReferenceContact('');
          
       System.assertEquals(null, talentContactId);
       
       
        Contact contactTalent = createTalentRecord();
        Contact contactRef = createTalentReferenceRecord();
        Talent_Recommendation__c tR = createTalentRecommendation(contactTalent, contactRef);
          
        talentContactId = ReferenceViewRoutingController.getContactIdReferenceContact(contactRef.Id);
          
       
          
        System.assertEquals(talentContactId, contactTalent.Id);
    
    
    
    }*/
    
    
    private static Contact createTalentRecord() {
    
        Account newAcc = BaseController_Test.createTalentAccount('');
        return  BaseController_Test.createTalentContact(newAcc.Id);
        
    }
    
    private static Contact createTalentReferenceRecord() {
    
         List<RecordType> types = [SELECT Id from RecordType where Name = 'Reference' And SobjectType = 'Account'];
         TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
         String jsonTP = JSON.serialize(tp);
         Account newAcc = new Account(Name='Test Talent Reference', RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);

         Database.insert(newAcc);
         
         
          Contact newC = new Contact(Firstname='TestRef', LastName='TalentReference'
                  ,AccountId=newAcc.Id, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tastrefernce@talentref.com');
          Database.insert(newC);
    
         
         
         return newC;
  }
        
   
    
    private static Talent_Recommendation__c createTalentRecommendation(Contact contact, Contact contactRef){
    
    
        
         System.debug(LoggingLevel.WARN, 'contact id: ' + contact.Id);
         System.debug(LoggingLevel.WARN, 'contact account Id : ' + contact.accountId);
         System.debug(LoggingLevel.WARN, 'contactRef Id: ' + contactRef.Id);
        Talent_Recommendation__c tR; 
        try {
             tR = new Talent_Recommendation__c(Talent_Contact__c = contact.Id, 
                                                                    Recommendation_From__c = contactRef.Id,  
                                                                    
                                                                    Relationship__c = 'Manager', 
                                                                    Relationship_Duration__c = '1-3 years',
                                                                    Talent_Job_Duration__c = '4-5 years' ); 
             insert tR;
        } catch (Exception e) {
            System.debug(LoggingLevel.WARN, 'exception Message : ' + e.getCause());
        }
        
         System.debug(LoggingLevel.WARN, 'tR : ');
         System.debug(LoggingLevel.WARN, tR);
      
       return tR;
    
    
    }
    




}