with sharing public class ATS_SMStrackingController  {
	@AuraEnabled
	public static Integer getSMSCount() {
		
		Datetime laTime = System.now().addDays(-30) ;		
		Integer smsCount = 0;
		/*
		List<SMS_Alert_Info__c> smsAlertInfoList = [select Id, User__c, Last_Accessed_Time__c from SMS_Alert_Info__c where User__c =: UserInfo.getUserId()];
		SMS_Alert_Info__c smsAlertInfo;
		if(smsAlertInfoList != null && smsAlertInfoList.size() >0) {
			smsAlertInfo = smsAlertInfoList.get(0);
			laTime = smsAlertInfoList.get(0).Last_Accessed_Time__c; 
			smsAlertInfo.Last_Accessed_Time__c = System.now();
		}
		else {
			smsAlertInfo = new SMS_Alert_Info__c(User__c = UserInfo.getUserId(), Last_Accessed_Time__c = System.now());
		}

		List<AggregateResult> smsCountList = [select COUNT(id) from tdc_tsw__Message__c where tdc_tsw__Read__c = FALSE AND Name = 'Incoming' AND owner.id =: UserInfo.getUserId() and LastModifiedDate >=: laTime];
		for(AggregateResult ar : smsCountList) {
			smsCount = (Integer)ar.get('expr0');
		}
		Upsert smsAlertInfo;
		*/
		System.debug(smsCount);
		return smsCount;
	}

	@AuraEnabled
	public static Boolean userHasSMS360Access() {		
		return Utilities.userHasAccessPermission('SMS');
	}
}