global class ATS_TalentHeaderModel {
	@AuraEnabled
    public User profile {get;set;}

    @AuraEnabled
    public Contact contact {get;set;}

    @AuraEnabled
    public List<Object> doNotRecruitAggregate {get;set;}
    
    @AuraEnabled
    public List<Object> engagementRuleList {get;set;} 
    
    @AuraEnabled
    public String  engagementAddress {get;set;}

    @AuraEnabled
    public Boolean doNotRecruit {get;set;}
    
    @AuraEnabled
    public Boolean doNotContact {get;set;}

	@AuraEnabled
    public Boolean talentPlaced {get;set;}
    
    @AuraEnabled
    public Target_Account__c talentOwnershipDetails {get;set;}
    
    @AuraEnabled
    public String talentOwnershipDetailsText {get;set;}
    
    @AuraEnabled
    public String talentStreetCity {get;set;}
    
    @AuraEnabled
    public String talentStateCountryZip {get;set;}

    @AuraEnabled
    public Boolean talentOwnership {get;set;}

    @AuraEnabled
    public List<Contact> relatedActivity {get;set;}
       
}