@isTest 
private class ATS_TalentOrderFunctions_Test {

  @testSetup
  private static void testmethodDataSetup() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
     User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
    Account tA1 = CreateTalentTestData.createTalentAccount();
    Contact tC1 = CreateTalentTestData.createTalentContact(tA1);
    TestData TdAcc = new TestData(1);
    List<Account> cA1 = TdAcc.createAccounts();
    Opportunity COpp1 = TestData.newOpportunity(cA1[0].Id,1);
    insert COpp1;
        COpp1.OpCo__c = 'Aerotek, Inc';
        COpp1.Req_Division__c = 'Aston Carter';
        update COpp1;
    Order tO1 = new Order(Name = 'New Order1', EffectiveDate = system.today()+1,
                              Status ='Applicant',AccountId = cA1[0].Id,
                OpportunityId = COpp1.Id,shiptocontactid=tC1.id,
                              recordtypeid = Utility.getRecordTypeId('Order','OpportunitySubmission'),
                Has_Application__c = true );
     insert tO1;
    
    Job_Posting__c jP1 = new Job_Posting__c(Currency_Code__c='111', 
            Delete_Retry_Flag__c=false, Job_Title__c = 'testJobTitle',Opportunity__c=COpp1.Id, Posting_Detail_XML__c='Sample Description',
            Private_Flag__c=false,RecordTypeId=Utility.getRecordTypeId('Job_Posting__c','Posting'),Source_System_id__c='R.112233',State__c='MD');

            insert jP1; 

    Event e1 = new Event (WhatId = tO1.Id,
                WhoId = tC1.id,
                Job_Posting__c = jP1.Id,
                DurationInMinutes = 1,
                StartDateTime = DateTime.now(),
                              Subject = tO1.Status,
                              Type = tO1.Status,
                              Activity_Type__c  = tO1.Status);
                
    insert e1;
    
     }
  }
  
  @IsTest
  public static void  getAppswithoutTD(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
  Test.startTest();
    List<Job_Posting__c> jp1 = [SELECT Id FROM Job_Posting__c ];
    System.assertEquals(1,jp1.size());
    Map<String,Object> pMap= new Map<String,Object>();
    pMap.put('recordId',jP1[0].Id);
    Object apps = ATSTalentOrderFunctions.performServerCall('getApplicantsFromJobPosting',pMap);
  Test.stopTest();
  }

  public static testMethod void updateDisposition() {
     DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Test.startTest();
        ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c();
        serviceSettings.Client_Id__c = '121111';
        serviceSettings.Name = 'saveDisposition';
        serviceSettings.Service_Http_Method__c = 'POST';
        serviceSettings.Service_URL__c = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';
        serviceSettings.Token_URL__c = '--';
        insert serviceSettings;
        Test.setMock(HttpCalloutMock.class,new RWSInteractionsCalloutMock());
    List<Event> jp1 = [SELECT Id FROM Event limit 1];
        Map<String,Object> pMap2= new Map<String,Object>();
    pMap2.put('evtId',jP1[0].Id);
        pMap2.put('Code','12375');
        pMap2.put('Reason','');
    Object apps = ATSTalentOrderFunctions.performServerCall('updateDisposition',pMap2);
        Test.stopTest();
        
    }
  public static testMethod void  getAppFromJobPosting(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
  Test.startTest();
    List<Job_Posting__c> jp1 = [SELECT Id FROM Job_Posting__c ];
    List<Opportunity> COpp1 = [SELECT Id FROM Opportunity limit 1];
    System.assertEquals(1,jp1.size());
    Map<String,Object> pMap= new Map<String,Object>();
    pMap.put('recordId',COpp1[0].Id);
    Object apps = ATSTalentOrderFunctions.performServerCall('getApplicantsFromJobPosting',pMap);
  Test.stopTest();
  }

}