public class ATS_TestDataCreation  {
      
  
@AuraEnabled
	public static void createOpp(Integer length,String FName,String Opco,String ObjName) {
	for (integer i=0;i<length;i++){
		ID jobID = System.enqueueJob(new AsyncTestDataCreate(FName,Opco,ObjName));
   		system.debug('JobId '+jobID);
		}
   }

@AuraEnabled
 public static void   createOpportunity(Integer length,String FName,String ownership,String ObjName ) {
 List<Account> testAccountList = new List<Account>();
  List<Contact> testContactList = new List<Contact>();
 if(ObjName.contains('Account')){
    Integer randomNumber = Integer.valueof((Math.random() * 5));
    for (Integer i=0; i<length;i++){
        Account testAccount = new Account(
            RecordTypeId = '01224000000FFqQAAW',
            Name = FName+i+'TalentAccount',
            Talent_Preference_Internal__c = createPreferenceJSON1(),
            Talent_Ownership__c = ownership,
			Peoplesoft_ID__c='R.'+randomNumber,
			Skills__c='{"skills":["Green Building"]}',
			Skill_Comments__c='Green Building Architect',
			Talent_Security_Clearance_Type__c='Confidential Or R',
			Willing_to_Relocate__c='Yes',
			Goals_and_Interests__c='His goal is to become Green Building Architect',
			Shift__c='First;Second;Third;Weekend'
        );
		testAccountList.add(testAccount);
     
    }
	   insert testAccountList;
    }

	if(ObjName.contains('Contact')){
	for (Integer i = 0; i < length; i++){
			Contact testContact = new Contact(
            RecordTypeId = '01224000000FFqVAAW',
            AccountId = testAccountList[i].Id,
			Email = FName.deleteWhitespace()+'@email.com',
            LastName = FName+i,
            FirstName = FName,
            Title = 'Green Building Architect',
            MobilePhone = '9696969696',
            HomePhone = '1234569870',
            Phone = '9876543210',
            OtherPhone = '5555555550',
            Other_Email__c = FName.deleteWhitespace()+'@gmail.com',
            MailingStreet = '3142 Normandy Woods Dr',
            MailingCity = 'Ellicott City',
            MailingState = 'Maryland',
            MailingCountry = 'United States',
            MailingLatitude = 39.267326,
            MailingLongitude = -76.798309
        );
		testContactList.add(testContact);
		}
        insert testContactList;
        }
		
        
   
    List<Talent_Experience__c> testTalentExperienceListEdu = new List<Talent_Experience__c>();
 	if(ObjName.contains('TalentEducation'))
	{
		for (Integer i = 0; i < length; i++){
	    Talent_Experience__c texpedu = new Talent_Experience__c(
				Current_Assignment__c = false,
				Notes__c='Testing Education	',
				PersonaIndicator__c	='Recruiter',
				Talent__c=testAccountList[i].Id,
				Graduation_Year__c	 = '2019',
				Allegis_Placement__c = false,
                Degree_Level__c='Diploma',
                //Education_Organization__c = 'a1v55000002eD0QAAU',
                Honor__c='Communication',
				Major__c='Electronics Communications',
				Organization_Name__c='SRM University',
				Talent_Committed_Flag__c=true,
				Type__c='Education'
				 );
				 testTalentExperienceListEdu.add(texpedu);
				 }
				insert testTalentExperienceListEdu;
	}

    List<Talent_Experience__c> testTalentExperienceListCert = new List<Talent_Experience__c>();
	 if(ObjName.contains('TalentCertification')){
    for (Integer i = 0; i < length; i++){
			
				 Talent_Experience__c texpcert = new Talent_Experience__c(
				Current_Assignment__c = false,
				Notes__c='Testing Education	',
				PersonaIndicator__c	='Recruiter',
				Talent__c=testAccountList[i].Id,
				Graduation_Year__c	 = '2019',
				Allegis_Placement__c = false,
                Certification__c='BAR',
                //Education_Organization__c = 'a1v55000002eD0QAAU',
                Honor__c='Communication',
				Major__c='Electronics Communications',
				Organization_Name__c='SRM University',
				Talent_Committed_Flag__c=true,
				Type__c='Training'
				 );
				 testTalentExperienceListCert.add(texpcert);
				 }
				 
			insert testTalentExperienceListCert;
			}
        	List<Account> testClientAccountList = new List<Account>();
			if(ObjName.contains('Opportunity') || ObjName.contains('Submittal') || ObjName.contains('Reference') || ObjName.contains('WorkHistory')){
			for (Integer i=0; i<length;i++){
			 Account testClientAccount = new Account(
            RecordTypeId = '01224000000FFqL',
            Name = FName+i+'ClientAccount'
           /*ShippingCity='Ellicott City',
		   ShippingState='MD',
		   ShippingCountry='United States',
		   ShippingStreet='3142 Normandy Woods Dr',
		   ShippingPostalCode='21043'
		   */
        );
		testClientAccountList.add(testClientAccount);
     
    }
	   		insert testClientAccountList;
      }
    		List<Contact> testClientContactList = new List<Contact>();
				if(ObjName.contains('Opportunity') || ObjName.contains('Submittal') || ObjName.contains('Reference') || ObjName.contains('WorkHistory')){
			for (Integer i = 0; i < length; i++){
			Contact testClientContact = new Contact(
            RecordTypeId = '012U0000000DWoy',
            AccountId = testClientAccountList[i].Id,
			FirstName=FName+i,
			LastName=FName+ i,
			Email=FName.deleteWhitespace()+'@gmail.com',
			MobilePhone='1234567890',
			MailingStreet = '3142 Normandy Woods Dr',
            MailingCity = 'Ellicott City',
            MailingState = 'Maryland',
            MailingCountry = 'United States',
            MailingLatitude = 33.386532,
            MailingLongitude = -112.215578,
           Title='Java / J2ee Developer'
        );
		testClientContactList.add(testClientContact);
		}
        	insert testClientContactList;
    }

    if (Test.isRunningTest())
	{
	insert new DRZSettings__c(Name = 'OpCo', Value__c='Aerotek, Inc;TEKsystems, Inc.');
	}
    List<Opportunity> testOpportunityList = new List<Opportunity>();
	if(ObjName.contains('Opportunity') || ObjName.contains('Submittal') || ObjName.contains('Reference')){
		for (Integer i = 0; i < length; i++){
			Opportunity testOpportunity = new Opportunity(
            RecordTypeId = '01224000000kMQ4',
			AccountId=testClientAccountList[i].Id,
            StageName='Draft',
			Req_Product__c='Contract',
			Req_Total_Positions__c=1,
			Req_Worksite_City__c='Ellicott',
			Req_Worksite_State__c='Maryland',
			Req_Worksite_Country__c='United States',
			Req_Worksite_Street__c='3142 Normandy Woods Dr',
			Req_Worksite_Postal_Code__c='21043',
			Req_Job_Title__c='Green Building Architect',
			Req_Hiring_Manager__c=testClientContactList[i].id,
			Currency__c='USD - U.S Dollar',
			Req_Division__c='Applications',
			OpCo__c='TEKsystems, Inc.',
			Req_Draft_Reason__c='Qualifying',
			Req_Rate_Frequency__c='Hourly',
			Req_TGS_Requirement__c='No',
			Legacy_Product__c='01tU0000000yc6uIAA',
			Name=FName+' - Req',
			Req_GeoLocation__Latitude__s=39.267326,
			Req_GeoLocation__Longitude__s=-76.798309,
			EnterpriseReqSkills__c='[{"name":"Green Building","favorite":false,"index":0,"suggestedSkill":false}]',
			CloseDate=System.today() + 5,
			Req_Rest_of_Skills__c='Green Building',
			isDRZUpdate__c = False
        );
		testOpportunityList .add(testOpportunity);
		}
        insert testOpportunityList;
		}
   /*     List<Order> testOrderList = new List<Order>();
 		for (Integer i = 0; i < length; i++){
        Order ord = new Order(
                 RecordTypeId ='01224000000kOSEAA2',
            	 AccountId=testAccountList[i].Id,
				 Status='Submitted',
                 OpportunityId = testOpportunityList[i].Id,
                 ShipToContactId=testContactList[i].Id,
                 EffectiveDate=System.today()
            );
			testOrderList.add(ord);
    }
		insert testOrderList;*/

		RecordType contactRecordType = [Select Id, Name, sobjecttype from RecordType where sobjecttype = 'Contact' and Name = 'Reference'];
        RecordType accountRecordType = [Select Id, Name, sobjecttype from RecordType where sobjecttype = 'Account' and Name = 'Reference'];
		RecordType referenceRecordType = [Select Id, Name, sobjecttype from RecordType where sobjecttype = 'Talent_Recommendation__c' and Name = 'Reference'];

		 List<Talent_Recommendation__c> testReferenceList = new List<Talent_Recommendation__c>();
		 List<Account> testReferenceAccountList = new List<Account>();
		 List<Contact> testReferenceContactList = new List<Contact>();
		 if(ObjName.contains('Reference')){
		 for (Integer i=0; i<length;i++){
        Account testReferenceAccount = new Account(
            RecordTypeId = accountRecordType.Id,
			//Record_Type_Name__c='Reference',
            Name = FName+'TalentReferenceAccount'+' '+FName+i+'TalentReferenceAccount',
            Talent_Ownership__c = ownership,
			Talent_Visibility__c = 'Private'
			
		);
		testReferenceAccountList.add(testReferenceAccount);
     
    }
	   insert testReferenceAccountList;
    
    
	for (Integer i = 0; i < length; i++){
			Contact testReferenceContact = new Contact(
			//Record_Type_Name__c='Reference',
            RecordTypeId = contactRecordType.Id,
            AccountId = testReferenceAccountList[i].Id,
			Email = FName.deleteWhitespace()+'@email.com',
            LastName = FName+i+'TalentReferenceAccount',
            FirstName = FName+'TalentReferenceAccount',
            Title = 'Green Building Architect',
            HomePhone = '1234569870',
            Other_Email__c = FName.deleteWhitespace()+'@gmail.com'
            
        );
		testReferenceContactList.add(testReferenceContact);
		}
        insert testReferenceContactList;
        


	for (Integer i = 0; i < length; i++){
			Talent_Recommendation__c testTalentRecommendation = new Talent_Recommendation__c(
            RecordTypeId = referenceRecordType.Id,
			Collaboration__c='Excellent',
            Additional_Reference_Notes__c = 'Test',
			Comments__c='Test',
			Competence__c='	Excellent',
			Interaction_Frequency__c='Daily',
			Leadership__c='	Excellent',
			Opportunity__c=testOpportunityList[i].Id,
			Organization_Name__c='Test',
			Quality_Description__c='Test',
			Quality__c='Excellent',
			Reference_Type__c='	Standard',
			Rehire_Status__c='Pending',
			Relationship_Duration__c='4-5 years',
			Relationship__c='Manager',
			Client_Account__c=testClientAccountList[i].Id,
			Recommendation_From__c=testReferenceContactList[i].Id,
			Talent_Contact__c=testContactList[i].Id


		);
		testReferenceList.add(testTalentRecommendation);
		}
		}
        insert testReferenceList;

    List<Talent_Work_History__c> testWorkHistoryList = new List<Talent_Work_History__c>();
 		 if(ObjName.contains('WorkHistory')){
		for (Integer i = 0; i < length; i++){
		//Integer randomNumber = Integer.valueof((Math.random() * 10));
	    Talent_Work_History__c twh = new Talent_Work_History__c(
                 Start_Date__c = System.today(),
                End_Date__c = System.today() + 5,
                Talent_Recruiter__c = 'peoplesoftId',
                 Talent_Account_Manager__c = 'peoplesoftId2',
                 Job_Title__c = 'Green Building Architect',
				 Main_Skill__c='Green Building',
                 FInish_Code__c = '',
                 Talent__c = testAccountList[i].Id,
				SourceCompany__c='APPLUS TECHNOLOGIES INC',
				SourceCompanyId__c='270255',
				Currency_Type__c='USD - U.S Dollar',
				SourceId__c=testAccountList[i].Peoplesoft_ID__c
				//uniqueId__c=String.valueOf(randomNumber)
            );
			testWorkHistoryList.add(twh);
    }
		insert testWorkHistoryList;
		}
    List<Event> IntEvent = new List<Event> ();
	 if(ObjName.contains('Activities')){
		for (Integer i = 0; i < length; i++){

         IntEvent.add(new Event(
                            Whoid = testContactList[i].id, 
                            Whatid = testAccountList[i].id,
                            Subject = 'Internal Interview', 
							Type='Internal Interview',
                            startDateTime =  system.Now(),
							 EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
							Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10));
        
        
					 IntEvent.add(new Event(
                            Whoid = testContactList[i].id, 
                            Whatid = testAccountList[i].id,
                            Subject = 'Meal', 
							Type='Meal',
                            startDateTime =  system.Now(),
							 EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
							Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10));


        					 IntEvent.add(new Event(
                            Whoid = testContactList[i].id, 
                            Whatid = testAccountList[i].id,
                            Subject = 'Other', 
							Type='Other',
                            startDateTime =  system.Now(),
							 EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
							Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10));

         IntEvent.add(new Event(
                            Whoid = testContactList[i].id, 
                            Whatid = testAccountList[i].id,
                            Subject = 'Out of Office', 
							Type='Out of Office',
                            startDateTime =  system.Now(),
							 EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
							Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10));

							IntEvent.add(new Event(
                            Whoid = testContactList[i].id, 
                            Whatid = testAccountList[i].id,
                            Subject = 'Performance Feedback', 
							Type='Performance Feedback',
                            startDateTime =  system.Now(),
							 EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
							Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10));


							IntEvent.add(new Event(
                            Whoid = testContactList[i].id, 
                            Whatid = testAccountList[i].id,
                            Subject = 'Presentation', 
							Type='Presentation',
                            startDateTime =  system.Now(),
							 EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
							Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10));

							IntEvent.add(new Event(
                            Whoid = testContactList[i].id, 
                            Whatid = testAccountList[i].id,
                            Subject = 'Service Touchpoint', 
							Type='Service Touchpoint',
                            startDateTime =  system.Now(),
							 EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
							Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10));
							}

        Database.insert(IntEvent,false);
    List<Task> IntTask = new List<Task>();
		for (Integer i = 0; i < length; i++){
		
		IntTask.add( new Task(
                            Whoid = testContactList[i].id, 
							Status='Completed',
							Subject='Attempted Contact',
                            Activity_Type__c='Attempted Contact',
							Type='Attempted Contact',
                            ActivityDate =  system.today()));
        
        
        
        IntTask.add( new Task(
                            Whoid = testContactList[i].id, 
							Status='Completed',
							Subject='BD Call',
                            Activity_Type__c='BD Call',
							Type='BD Call',
                            ActivityDate =  system.today()));
        
		IntTask.add( new Task(
                            Whoid = testContactList[i].id, 
							Status='Completed',
							Subject='Email',
                            Activity_Type__c='Email',
							Type='Email',
                            ActivityDate =  system.today()));
					}

        Database.insert(IntTask,false);
        }
       if(ObjName.contains('Submittal')){
	   List<Job_Posting__c> testJobPostingList = new List<Job_Posting__c>();

 		for (Integer i = 0; i < length; i++){
        Job_Posting__c tjp = new Job_Posting__c(
                 RecordTypeId = '01224000000LUJzAAO',
                Opportunity__c = testOpportunityList[i].id,
                Private_Flag__c = false,
                 Source_System_id__c = 'R.6414011',
                 Source_System_Name__c = 'RWS',
                 Applications_Count__c = 0.0,
                 BROADBEAN_ACCOUNT_ID__c = 2.0,
				Delete_Retry_Flag__c=false,
				Invitations_Count__c=0.0,
				OFCCP_Flag__c=false
			);
			testJobPostingList.add(tjp);
    }
		insert testJobPostingList;
     
      List<Order> testOrderhasaplicationList = new List<Order>();
	   
     Integer randomNumber1 = Integer.valueof((Math.random() * 5));
 		for (Integer i = 0; i < length; i++){
        Order ordhasapllication = new Order(
                 RecordTypeId ='01224000000kOSEAA2',
            	 AccountId=testAccountList[i].Id,
				 Status='Applicant',
                 OpportunityId = testOpportunityList[i].Id,
                 ShipToContactId=testContactList[i].Id,
                 EffectiveDate=System.today(),
            	 Unique_Id__c=randomNumber1+'abc',
            	 Has_Application__c=true
            );
			testOrderhasaplicationList.add(ordhasapllication);
    }
		insert testOrderhasaplicationList;
		}
   /* List<Event> testEventList = new List<Event>();
        Job_Posting__c Jp;
        Event tevt;
    for(integer i=0;i<length;i++)
{
    system.debug('SELECT id FROM Event where AccountId='+testAccountList[i].Id+' and type=\'Applicant\'');
    tevt = [SELECT id FROM Event where AccountId=:testAccountList[i].Id and type='Applicant'];
    tevt.Job_Posting__c=testJobPostingList[i].id;
       testEventList.add(tevt);             
}
        update testEventList;
   */  
   /*
     List<Data_Extraction_EAP__c> testDataExtractionList = new List<Data_Extraction_EAP__c>();
	    if(ObjName.contains('Account') || ObjName.contains('Submittal') || ObjName.contains('Opportunity')){
		   for (Integer i=0; i<length;i++){
        Data_Extraction_EAP__c testDataExtractionAccount = new Data_Extraction_EAP__c(
            Object_Name__c = 'Account',
            Object_Ref_ID__c=testClientAccountList[i].Id
			);
		testDataExtractionList.add(testDataExtractionAccount);
     
    }
	insert testDataExtractionList;
     
	 }
     List<Data_Extraction_EAP__c> testDataExtractionListcon = new List<Data_Extraction_EAP__c>();
	 if(ObjName.contains('Account') || ObjName.contains('Submittal') || ObjName.contains('Opportunity')|| ObjName.contains('WorkHistory')){
		   for (Integer i=0; i<length;i++){
        Data_Extraction_EAP__c testDataExtractionAccount = new Data_Extraction_EAP__c(
            Object_Name__c = 'Contact',
            Object_Ref_ID__c=testClientContactList[i].Id
			);
		testDataExtractionListcon.add(testDataExtractionAccount);
     
    }
	insert testDataExtractionListcon;
}
*/
}

@AuraEnabled
public static String createPreferenceJSON1() {
        return '{"languages":["en","fr"],"geographicPrefs":{"nationalOpportunities":true,"geoComments":"Open Any where in United states","desiredLocation":[{"country":"United States","state":"Maryland","city":"Elicott City"}],"commuteLength":{"unit":"minutes","distance":100}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":true,"eligibleIn":["United States"],"drugTest":null,"backgroundCheck":null}}';
}
	}