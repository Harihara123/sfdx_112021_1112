@isTest
public class ATS_Test_Batch_Retry_Talent_Hrxml  {

 private static Map<Id,String> errorMap = new Map<Id, String>();

 @testSetup 
 static void setup() {
        errorMap = createTestData();
        TalentRetryLog.logTalentRetryAction(TalentRetryLog.HRXML, errorMap);     
 }


 @isTest
 public static void Batch_Retry_Talent_HrxmlTest(){

        Test.startTest();

        Batch_Retry_Talent_Hrxml brTalentHrxml = new Batch_Retry_Talent_Hrxml();
        DataBase.executeBatch(brTalentHrxml);

        Test.stopTest();


        List<String> listRecord = new List<String>();

         for (Id recordId : errorMap.keySet() ) {
           listRecord.add(String.valueOf(recordId));
         }

         Integer count  = [SELECT count() FROM Talent_Retry__c 
                       WHERE Action_Record_Id__c in: listRecord 
                       and Action_Name__c = 'HRXML' 
                       and Status__c  in ('Failed', 'Success' )
                       and Description__c = 'Failed HRXML Testing only']; 

         System.assertEquals(errorMap.size(), count);
 }




 private static Map<Id,String> createTestData(){

       return BaseController_Test.CreateTalentForBatchRetryHrxml();


  }


}