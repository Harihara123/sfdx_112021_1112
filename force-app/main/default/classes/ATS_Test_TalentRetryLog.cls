@isTest
public class ATS_Test_TalentRetryLog  {


  @isTest
  public static void logTalentRetryActionTest(){

    Map<Id,String> errorMap = createTestData();

     List<String> listRecord = new List<String>();

    for (Id recordId : errorMap.keySet() ){
       listRecord.add(String.valueOf(recordId));
    }

     TalentRetryLog.logTalentRetryAction(TalentRetryLog.HRXML, errorMap);

     Integer count  = [SELECT count() FROM Talent_Retry__c 
                       WHERE Action_Record_Id__c in: listRecord 
                       and Action_Name__c = 'HRXML' and Status__c = 'Open' and Description__c = 'Failed HRXML Testing only']; 

     System.assertEquals(errorMap.size(), count);

  }

  @isTest
  public static void logRetryRunTest(){


         Map<Id,String> errorMap = createTestData();
          

         TalentRetryLog.logTalentRetryAction(TalentRetryLog.HRXML, errorMap);

         List<Id> talentRetryIdsList = new List<Id>();

         List<String> listRecord = new List<String>();

         for (Id recordId : errorMap.keySet() ){
           listRecord.add(String.valueOf(recordId));
         }

         for (Talent_Retry__c tr : [SELECT Id FROM Talent_Retry__c 
                       WHERE Action_Record_Id__c in: listRecord 
                       and Action_Name__c = 'HRXML' and Status__c = 'Open' and Description__c = 'Failed HRXML Testing only'] ) {

               talentRetryIdsList.add(tr.Id);

        }


         TalentRetryLog.logRetryRun(TalentRetryLog.STATUS_FAILED, talentRetryIdsList);


          Integer count  = [SELECT count() FROM Talent_Retry__c 
                       WHERE Action_Record_Id__c in: listRecord 
                       and Action_Name__c = 'HRXML' and Status__c = 'Failed' and Description__c = 'Failed HRXML Testing only']; 

         System.assertEquals(errorMap.size(), count);

     }



  private static Map<Id,String> createTestData(){

      return BaseController_Test.CreateTalentForBatchRetryHrxml();

  }




  


}