/**
* @author Neel Kamal 
* @date 04/09/2018
*
* @group LandingScreen
* @group-content ATS_UpdateTalentByParsedResume.html
*
* @description This class generating JSON having compare record of parsed resume and current talnet core contact. 
			   It also saving updating record.
*/

public with sharing class ATS_UpdateTalentByParsedResume  {
    
    public class ObjectInfo {
        String recordNum;
        List<fieldsInfo>  record;
        public ObjectInfo(String recNum, List<fieldsInfo>  fldList) {
            recordNum = recNum;
            record = fldList.clone();
        }
    }
    
    public class FieldsInfo {
	    String rowID {get;set;}
        String fldName {get;set;}
        String fldValue {get;set;}
        String fldParsedValue {get;set;}
        String cType{get;set;}
        Boolean matched {get;set;}
        Boolean valueChanged {get;set;}

		FieldsInfo(Id id, string fLabel, String fValue, String fParsedValue, Boolean fFlag, String customType) {
			this.rowID = id;
			this.fldName = fLabel;
			this.fldValue = fValue;
			this.fldParsedValue = fParsedValue;
			this.matched = fFlag;
			this.cType = customType;
		}
    }

	/*
	* @description This method getting parsed resume contact info and call method modalToJSONGenerator and compareTalentNParsedResumeRecord. 
	* @param documentID and				   
	*/
    @AuraEnabled
    public static String fetchExistingNParsedTalent(String documentID, String talentID, String functionalityName) {
        Attachment[] atts = [SELECT Id, Name from Attachment WHERE ParentId = :documentId];
        String response;
        if (atts != null && atts.size() > 0) {
            InvocableAttachmentParser.ParsedInfo pInfo = InvocableAttachmentParser.parseResumeForTalentUpdate(atts[0].id);
            if(functionalityName.equalsIgnoreCase('Contact')) {
                InvocableAttachmentParser.ContactInfo contactInfo = pInfo.contactInfo;
                response = modalToJSONGenerator(compareTalentNParsedResumeRecord(talentID, contactInfo), functionalityName);
				System.debug('Talent core JSON ::: \n' + response);
            }
            /*else if(functionalityName.equalsIgnoreCase('EducationHistory')) {
                List<Talent_Experience__c> eduParsedInfo = pInfo.educationList;
                response = modalToJSONGenerator(educationInfo(talentID, eduParsedInfo), functionalityName);
                System.debug('Education JSON ::: \n' + response);
            }*/
            
        }
        
        return response;
    }
	/**
	*@description compare record of current contact and parsed resume record.
	*/
    @TestVisible private static List<ObjectInfo>  compareTalentNParsedResumeRecord(String talentID, InvocableAttachmentParser.ContactInfo contactInfo) {
        List<Contact> contactList = [select Id, FirstName, LastName, MiddleName, MailingStreet, MailingCity, MailingState, Talent_State_Text__c, MailingPostalCode, 
                                     MailingCountry, Talent_Country_Text__c, MobilePhone, HomePhone, Phone, OtherPhone, Email, Work_Email__c, Other_Email__c, Website_URL__c, LinkedIn_URL__c from Contact where Accountid =: talentID];
        
		String conInfoJSON = JSON.serialize(contactInfo);
		System.debug('compareTalentNParsedResumeRecord::conInfoJSON:::'+conInfoJSON);
		Map<String,Object> conInfoMap =  (Map<String,Object>) JSON.deserializeUntyped(conInfoJSON);
		List<FieldsInfo> fldList = new List<FieldsInfo>();

		String extValue = '';
		String parsedValue = '';

        
		conInfoJSON = JSON.serialize(contactInfo);
		conInfoMap =  (Map<String,Object>) JSON.deserializeUntyped(conInfoJSON);

		if(contactList != null  && contactList.size() > 0) {
			Contact con = contactList.get(0);

			for(UI_Field_Label_Mapping__mdt flm : [select API_Name__c, Custom_Type__c, StdLabel__c, UI_Label__c, Variable__c, Position__c from UI_Field_Label_Mapping__mdt 
															where Object__c='Contact' order by Position__c]) 
				{
				extValue = '';
				parsedValue = '';
				if(flm.Variable__c.contains('.')) { 
					String var1 = flm.Variable__c.substring(0, flm.Variable__c.indexOf('.'));
					String var2 = flm.Variable__c.substring(flm.Variable__c.indexOf('.')+1);						
					extValue =  (String)con.get(flm.API_Name__c);
					if(conInfoMap.get(var1) != null) {
						Map<String, Object> stMap = (Map<String, Object>)conInfoMap.get(var1);
						if((String)stMap.get(var2) != null ) {
							parsedValue  = ((String)stMap.get(var2)).trim();

						}
					}
					fldList.add(new FieldsInfo(con.Id, flm.UI_Label__c, extValue, parsedValue, match(extValue, parsedValue), flm.Custom_Type__c));
				}
				else {
					extValue =  (String)con.get(flm.API_Name__c);
					if((String)conInfoMap.get(flm.Variable__c) != null) {
						parsedValue = ((String)conInfoMap.get(flm.Variable__c)).trim();
					}
					fldList.add(new FieldsInfo(con.Id, flm.UI_Label__c, extValue, parsedValue, match(extValue, parsedValue), flm.Custom_Type__c));
				}
                
			}
		}
       
        ObjectInfo objInfo = new ObjectInfo('record1', fldList);

        List<ObjectInfo> records = new List<ObjectInfo>();
        records.add(objInfo);
		System.debug(records);
        return records;
    }

    @AuraEnabled
    public static String save(String jsonStr) {
		System.debug('Save::jsonStr::::'+jsonStr);
        JSONParser parser = JSON.createParser(jsonStr);
        List<fieldsInfo> fieldInfoList = new List<FieldsInfo>();
		String resultMsg;
        while (parser.nextToken() != null) {
            // Start at the array of invoices.
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        ObjectInfo objInfor = (ObjectInfo)parser.readValueAs(ObjectInfo.class);
                        if(objInfor != null && objInfor.record != null && objInfor.record.size() > 0) {
                            fieldInfoList = objInfor.record;
                            resultMsg = updateContact(fieldInfoList);
                        }
                        
                        System.debug('ObjectInfo parsed:::'+objInfor.record);
                        // Skip the child start array and start object markers.
                        parser.skipChildren();

                    }
                }
            }
        }
        //System.debug(objInfoList);

        return resultMsg;
    }

	//Updating contact
    private static String updateContact(List<fieldsInfo> fieldInfoList) {
		String resultMsg;
        Contact ct = null;
		Map<String, UI_Field_Label_Mapping__mdt> fieldLblAPIMap = new Map<String, UI_Field_Label_Mapping__mdt>();
		for(UI_Field_Label_Mapping__mdt flm : [select API_Name__c, Custom_Type__c, StdLabel__c, UI_Label__c, Variable__c from UI_Field_Label_Mapping__mdt 
                                                          where Object__c='Contact' order by Position__c]) {
			fieldLblAPIMap.put(flm.UI_Label__c, flm);
		}

        for(fieldsInfo fi : fieldInfoList) {
            
            if(fi.valueChanged != null && fi.valueChanged) {
                if(ct == null) {
                    ct = new Contact(id=fi.rowID);
                }
				if(fi.fldName.equals('State')) {//On UI state code is not changing on change of state.
					ct.put('Talent_State_Text__c', getHeaderParsedValue(fieldInfoList, 'Talent_State_Text'));//API field name contain test but it have code.
					//Country is not displaying on UI so if State will chnage then also chnage the country.
					ct.put('Talent_Country_Text__c',getHeaderParsedValue(fieldInfoList, 'Talent_Country_Text'));
					ct.put('MailingCountry',getHeaderParsedValue(fieldInfoList, 'Country'));
				}
				
                System.debug('Field API---'+fi.fldName+'---'+fieldLblAPIMap.get(fi.fldName).API_Name__c);
                ct.put(fieldLblAPIMap.get(fi.fldName).API_Name__c, fi.fldValue);                                            
            }
        }

        if(ct != null) {
			System.debug('updateContact::Contact before update::::'+ct);
			Database.SaveResult sr = Database.update(ct, false);
			if (sr.isSuccess()) {
				resultMsg = 'Successful';
			}
			else {
				// Operation failed, so get all errors                
				for(Database.Error err : sr.getErrors()) {						
					resultMsg = err.getStatusCode() + ': ' + err.getMessage();	
										
					System.debug('The following error has occurred.');                    
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Fields that affected this error: ' + err.getFields());
				}
			}		
		}

		return resultMsg;
    }

	private static String getHeaderParsedValue(List<fieldsInfo> fieldInfoList, String fldLabel) {
		String pValue = '';
		for(fieldsInfo fi : fieldInfoList) {
			if(fi.fldName.equals(fldLabel)) {
				pValue = fi.fldParsedValue;
			}
		}
		return pValue;
	}

    private static Boolean match(String value, String pValue) {
        if(String.isBlank(value) && String.isBlank(pValue)) {
            return true;
        }
        else if(String.isBlank(value) || String.isBlank(pValue)) {
            return false;
        }
        else if(value.equalsIgnoreCase(pValue)) {
            return true;
        }
        return false;
    }

    @TestVisible private static String modalToJSONGenerator(List<ObjectInfo>  records, String objName){
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        jsonGen.writeFieldName(objName);
        jsonGen.writeObject(records);
        jsonGen.writeEndObject();

        return jsonGen.getAsString();
    }

    /*private static List<ObjectInfo>  educationInfo(String talentID, List<Talent_Experience__c> parsedEduInfoList) {
        
        List<ObjectInfo> objInfoList = new List<ObjectInfo>();
        List<FieldsInfo> fldList = new List<FieldsInfo>();
        List<String> processedDegree = new List<String>();
        FieldsInfo flInfo = null;
        Boolean flag = false;
        ObjectInfo objInfo = null;
        Integer recNum = 0;
        List<Talent_Experience__c> currEduInfoList = [select id, Degree_Level__c, Graduation_Year__c, Major__c, Notes__c, Type__c, Education_Organization__c 
                                                      from Talent_Experience__c where Talent__c =: talentID]; 
        
        if(currEduInfoList != null && currEduInfoList.size() > 0){
            for(Talent_Experience__c edu : currEduInfoList ) {
                flag = false;
                if(parsedEduInfoList != null  && parsedEduInfoList != null ) {
                    for(Talent_Experience__c parsedEdu : parsedEduInfoList) {
                        if(parsedEdu.Degree_Level__c.equalsIgnoreCase(edu.Degree_Level__c) ) {
                            fldList = createEduModal(edu, parsedEdu);
                            flag = true;    
                            processedDegree.add(parsedEdu.Degree_Level__c);//This need to maintain because need to check if any records op resume didn't processed.
                            break;                      
                        }
                    }
                }
                if(!flag) {
                    fldList = createEduModal(edu, null);                    
                }

                recNum++;
                objInfo =  new ObjectInfo();
                objInfo.record = fldList;
                objInfo.recordNum = 'record'+recNum;
                objInfoList.add(objInfo);
            }
        }

        if(parsedEduInfoList != null  && parsedEduInfoList != null ) {
            for(Talent_Experience__c parsedEdu : parsedEduInfoList) {
                if(processedDegree != null && processedDegree.size() > 0) {
                    fldList = createEduModal(null, parsedEdu);

                    recNum++;
                    objInfo =  new ObjectInfo();
                    objInfo.record = fldList;
                    objInfo.recordNum = 'record'+recNum;
                    objInfoList.add(objInfo);
                    
                }
            }
        }

        return objInfoList;
    }

    private static List<FieldsInfo> createEduModal(Talent_Experience__c currEdu, Talent_Experience__c parsedEdu) {
        List<FieldsInfo> fldList = new List<FieldsInfo>();
        FieldsInfo flInfo = null;
        String currEduID;
        String currEduDegreelevel;
        String currEduGradyear;
        String currEduMajor; 
        String currEduNote;
        String currEduType;
        String parsedEduDegreeLevel;
        String parsedEduGradyear;
        String parsedEduMajor; 
        String parsedEduNote;
        String parsedEduType ; 
        if(currEdu != null && parsedEdu != null) {
            currEduID = currEdu.Id;
            currEduDegreelevel = currEdu.Degree_Level__c;
            currEduGradyear = currEdu.Graduation_Year__c;
            currEduMajor = currEdu.Major__c; 
            currEduNote = currEdu.Notes__c;
            currEduType  = currEdu.Type__c;

            parsedEduDegreeLevel = parsedEdu.Degree_Level__c;
            parsedEduGradyear = parsedEdu.Graduation_Year__c;
            parsedEduMajor = parsedEdu.Major__c; 
            parsedEduNote = parsedEdu.Notes__c;
            parsedEduType  = parsedEdu.Type__c;
        }
        else if(currEdu != null) {
            currEduID = currEdu.Id;
            currEduDegreelevel = currEdu.Degree_Level__c;
            currEduGradyear = currEdu.Graduation_Year__c;
            currEduMajor = currEdu.Major__c; 
            currEduNote = currEdu.Notes__c;
            currEduType  = currEdu.Type__c;

            parsedEduDegreeLevel = null;
            parsedEduGradyear = null;
            parsedEduMajor = null; 
            parsedEduNote = null;
            parsedEduType  = null;
        }
        else if(parsedEdu != null) {
            currEduID = null;
            currEduDegreelevel = null;
            currEduGradyear = null;
            currEduMajor = null; 
            currEduNote = null;
            currEduType  = null;

            parsedEduDegreeLevel = parsedEdu.Degree_Level__c;
            parsedEduGradyear = parsedEdu.Graduation_Year__c;
            parsedEduMajor = parsedEdu.Major__c; 
            parsedEduNote = parsedEdu.Notes__c;
            parsedEduType  = parsedEdu.Type__c;
        }
        flInfo = createFieldInfo(currEduID, 'Degree', currEduDegreelevel, parsedEduDegreeLevel, match(currEduDegreelevel, parsedEduDegreeLevel),'');
        fldList.add(flInfo);

        flInfo = createFieldInfo(currEduID, 'Year', currEduGradyear, parsedEduGradyear, match(currEduGradyear, parsedEduGradyear),'');
        fldList.add(flInfo);

        flInfo = createFieldInfo(currEduID, 'Major', currEduMajor, parsedEduMajor, match(currEduMajor, parsedEduMajor),'');
        fldList.add(flInfo);

        flInfo = createFieldInfo(currEduID, 'Notes', currEduNote, parsedEduNote, match(currEduNote, parsedEduNote),'');
        fldList.add(flInfo);

        return fldList;
    }*/
}