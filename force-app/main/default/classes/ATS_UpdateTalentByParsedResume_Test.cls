@isTest
public class ATS_UpdateTalentByParsedResume_Test  {
    
    testmethod static void testFetchExistingNParsedTalent_Contact(){
        
        Account talent = CreateTalentTestData.createTalentAccount();
        Contact talentContact = CreateTalentTestData.createTalentContact(talent);

		Talent_Document__c tdoc = CreateTalentTestData.createAttResumeDocument(talent, true, false, true);
        
        ApigeeOAuthSettings__c settings = new ApigeeOAuthSettings__c(Name='Resume Parser',Client_Id__c='testid',Client_Secret__c='testsecretid',OAuth_Token__c='testoauthtoken',Service_Header_1__c='test:sh',Service_Header_2__c='test:sh2',Service_Header_3__c='test:sh3',Service_Http_Method__c='testshm',Service_URL__c='www.test.com/test',Token_Expiration__c=null,Token_URL__c='www.test.com/test');
        insert settings;
            
        
        // Attachment attachment = ATS_UpdateTalentByParsedResume_Test.insertAResume(talent.Id);
       
        Test.startTest();
			InvocableAttachmentParser.ContactInfo contactInf = createContactInfo();
			Test.setMock(HttpCalloutMock.class, new Test_InvocableAttachmentParser());
			ATS_UpdateTalentByParsedResume.fetchExistingNParsedTalent(tdoc.Id,talent.id,'Contact');
			ATS_UpdateTalentByParsedResume.fetchExistingNParsedTalent(tdoc.Id,talent.id,'Contact');
			String jsonResponse = ATS_UpdateTalentByParsedResume.modalToJSONGenerator(ATS_UpdateTalentByParsedResume.compareTalentNParsedResumeRecord(talent.id, contactInf), 'Contact');
			
        Test.stopTest();
    }
	
	

	private static InvocableAttachmentParser.ContactInfo createContactInfo() {
		InvocableAttachmentParser.State st = new InvocableAttachmentParser.State();
		st.code = 'Maryland';
		st.text = 'MD';


		InvocableAttachmentParser.ContactInfo cinfo = new InvocableAttachmentParser.ContactInfo();
	
		cinfo.firstname = 'Roger';
		cinfo.lastname = 'Federer';
		cinfo.addressline = '1111 test avenue';
		cinfo.city = 'Testville';
		cinfo.state = st;
		cinfo.homephone = '1234567890';
		cinfo.workphone = '1234567890';
		cinfo.mobilephone = '1234567890';
		cinfo.otherphone = '1234567890';
		cinfo.email = 'RFederer@atest.com';
		cinfo.otheremail = 'Other@atest.com';
		cinfo.websiteurl = 'www.test.com';
		cinfo.linkedinurl = '';

		return cinfo;
	}
	
    
   testmethod static void testSaveMethod(){
    	
        Test.startTest();
			//String resp = ATS_UpdateTalentByParsedResume.save(jsonStr);
			String resp = ATS_UpdateTalentByParsedResume.save(jsonStrSaveAll);			
			//Just saving street and state
			resp = ATS_UpdateTalentByParsedResume.save(jsonStr);
		Test.stopTest();

    }

	static String jsonStr = '{ "Contact" : [ { "recordNum" : "record1", "record" : [ { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Saptagiri", "fldParsedValue" : "Saptagiri", "fldName" : "First Name", "cType" : "Header" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Middle Name", "cType" : "Header" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Lingala", "fldParsedValue" : "Lingala", "fldName" : "Last Name", "cType" : "Header" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : false, "fldValue" : "1111 Wheaton Way", "fldParsedValue" : " 1111 Wheaton Way", "fldName" : "Street Address", "cType" : "Address-Street" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Ellicott City", "fldParsedValue" : "Ellicott City", "fldName" : "City", "cType" : "Address-City" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Maryland", "fldParsedValue" : "Maryland", "fldName" : "State", "cType" : "Address-State" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "98005", "fldParsedValue" : "98005", "fldName" : "Postal Code", "cType" : "Address-PostalCode" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : false, "fldValue" : "+1 425-241-3496", "fldParsedValue" : "+1 425-241-3435", "fldName" : "Mobile Phone", "cType" : "Phone" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Home Phone", "cType" : "Phone" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Business Phone", "cType" : "Phone" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Other Phone", "cType" : "Phone" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "lingala.saptagiri@gmail.com", "fldParsedValue" : "lingala.saptagiri@gmail.com", "fldName" : "Email", "cType" : "Email" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Work Email", "cType" : "Email" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Other Email", "cType" : "Email" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Website URL", "cType" : "WebsiteURL" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "LinkedIn URL", "cType" : "LinkedinUrl" },{ "valueChanged" : null, "rowID" : "0034E00000h91w3QAA", "matched" : false, "fldValue" : null, "fldParsedValue" : "MD", "fldName" : "Talent_State_Text", "cType" : "Header" } ] } ] }';
	static String jsonStrSaveAll = '{ "Contact" : [ { "recordNum" : "record1", "record" : [ { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Saptagiri", "fldParsedValue" : "Saptagiri", "fldName" : "First Name", "cType" : "Header" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Middle Name", "cType" : "Header" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Lingala", "fldParsedValue" : "Lingala", "fldName" : "Last Name", "cType" : "Header" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : false, "fldValue" : "3120 Wheaton Way", "fldParsedValue" : " 3120 Wheaton Way", "fldName" : "Street Address", "cType" : "Address-Street" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Ellicott City", "fldParsedValue" : "Ellicott City", "fldName" : "City", "cType" : "Address-City" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "Maryland", "fldParsedValue" : "Maryland", "fldName" : "State", "cType" : "Address-State" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "98005", "fldParsedValue" : "98005", "fldName" : "Postal Code", "cType" : "Address-PostalCode" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : false, "fldValue" : "+1 425-241-3496", "fldParsedValue" : "+1 425-241-3435", "fldName" : "Mobile Phone", "cType" : "Phone" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Home Phone", "cType" : "Phone" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "+1 425-241-3435", "fldParsedValue" : "+1 425-241-3435", "fldName" : "Business Phone", "cType" : "Phone" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "999-999-9999", "fldParsedValue" : "999-999-9999", "fldName" : "Other Phone", "cType" : "Phone" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "lsaptagiri@gmail.com", "fldParsedValue" : "lsaptagiri@gmail.com", "fldName" : "Email", "cType" : "Email" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Work Email", "cType" : "Email" }, { "valueChanged" : null, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : null, "fldParsedValue" : null, "fldName" : "Other Email", "cType" : "Email" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "www.neel.com", "fldParsedValue" : "www.neel.com", "fldName" : "Website URL", "cType" : "WebsiteURL" }, { "valueChanged" : true, "rowID" : "0034E00000fiaq4QAA", "matched" : true, "fldValue" : "www.usertest.com", "fldParsedValue" : "www.usertest.com", "fldName" : "LinkedIn URL", "cType" : "LinkedinUrl" },{ "valueChanged" : null, "rowID" : "0034E00000h91w3QAA", "matched" : false, "fldValue" : null, "fldParsedValue" : "MD", "fldName" : "Talent_State_Text", "cType" : "Header" } ] } ] }';
}