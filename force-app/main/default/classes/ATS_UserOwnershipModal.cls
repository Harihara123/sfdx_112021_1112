public class ATS_UserOwnershipModal  {
	@AuraEnabled
	public User usr{get;set;}

	@AuraEnabled
	public String userOwnership{get;set;}

	@AuraEnabled
	public Boolean haslinkedInSync{get;set;}
      /* Start for story S-79062 */
    @AuraEnabled
    public String userCategory {get;set;}
      /* End for story S-79062 */
	/* Start for story S-107087 added by siva*/
	@AuraEnabled
	public String userCPCompany {get; set;}
	/* End for story S-107087 added by siva*/

	@AuraEnabled
	public Boolean hasUserSMSAccess {get; set;} //Neel - S-140117 

	@AuraEnabled
	public Boolean hasUserConsentTextAccess {get; set;} //Neel - S-140117 
}