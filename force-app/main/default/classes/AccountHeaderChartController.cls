public with sharing class AccountHeaderChartController{
/*
// This method calls the Tibco Webservice to fetch the Header Information. 
    @AuraEnabled   
    public Static String getHeader(String accountId){
       // String ReqID, String OPCO, String EmployeeID, List<string>ContactSiebelID       
       String SiebelId = '';
       String OPCO = 'TEK';
       String EmployeeID = '00033695';
       String GlobalAccountID ='';
       
       //Soql
        list<account> tempList = [select  Siebel_ID__c, Master_Global_Account_Id__c from Account where id =: accountId];
            if(tempList.size()>0){
              SiebelId = tempList[0].Siebel_ID__c;
              GlobalAccountID = tempList[0].Master_Global_Account_Id__c;
            }
                  
       List<ChartWrapper> JsonLstWrapper = new List<ChartWrapper>();
       List<WS_Header_Service.getSFDCHeaderResponseType> Header = new List<WS_Header_Service.getSFDCHeaderResponseType>();
          
                   try{                    
                    if(Test.isRunningTest()) {    
                        WS_Header_Service.getSFDCHeaderResponseRoot_element RespRoot= new WS_Header_Service.getSFDCHeaderResponseRoot_element();
                        WS_Header_Service.getSFDCHeaderRequestType ReqType = new WS_Header_Service.getSFDCHeaderRequestType();
                        WS_Header_Service.Fault_element fltType = new WS_Header_Service.Fault_element();
                        WS_Header_Service.getSFDCHeaderResponseType Head = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head.FillRatio= '12345';
                        Head.WentDirectClient= '123451234';
                        Head.ProducerSpread= '123';
                        Head.OpCoSpread= '12354';
                        Head.GlobalAccountSpread = '12358';
                        Header.add(Head);
                        WS_Header_Service.getSFDCHeaderResponseType Head1 = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head1.FillRatio= '12345';
                        Head1.WentDirectClient= '123451234';
                        Head1.ProducerSpread= '123';
                        Head1.OpCoSpread= '12354';
                        Head1.GlobalAccountSpread = '12358';
                        Header.add(Head1); 
                        WS_Header_Service.getSFDCHeaderResponseType Head2 = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head2.FillRatio= '1234-5';
                        Header.add(Head2); 
                    }
                    else{
                        WS_Header_ServiceType.GetSFDCHeaderEndpoint1 WSHeader = new WS_Header_ServiceType.GetSFDCHeaderEndpoint1 ();                        
                        WSHeader.timeout_x = 30000;
                        //String GlobalAccountID;
                    
                        try {                               
                                Header = WSHeader.getHeaders(SiebelId ,GlobalAccountID ,OPCO ,EmployeeID ,null);
                            }
                             catch(CalloutException ex) {                    
                                //exceptionDescription = ex.getMessage();
                              // Header = Null;
                          }
                    }
                 if(Header <> Null && Header.size() > 0){                  
                    ChartWrapper obj;
                    
                    for(WS_Header_Service.getSFDCHeaderResponseType respRecord : Header){
                        obj = new ChartWrapper();
                        
                        if(respRecord.FillRatio <> NULL && respRecord.FillRatio <> ''){
                        obj.FillRatio = (decimal.valueOf(respRecord.FillRatio)*100).setScale(2);
                        } 
                                               
                        obj.WentDirectClient = respRecord.WentDirectClient;
                        
                        if(respRecord.ProducerSpread <> NULL && respRecord.ProducerSpread <> ''){
                        obj.ProducerSpread = (decimal.valueOf(respRecord.ProducerSpread)).setScale(0);
                        }
                        if(respRecord.OpCoSpread <> NULL && respRecord.OpCoSpread <> ''){
                        obj.OpCoSpread = (decimal.valueOf(respRecord.OpCoSpread)).setScale(0);}
                        
                        if(respRecord.GlobalAccountSpread <> NULL && respRecord.GlobalAccountSpread <> ''){
                        obj.GlobalAccountSpread = (decimal.valueOf(respRecord.GlobalAccountSpread)).setScale(0); 
                        }    
                                           
                        JsonLstWrapper.add(obj);                       
                    }
                 }
            } catch(CalloutException ex) {                
                //exceptionDescription = ex.getMessage();
            }catch(Exception ex)  {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, ex.getMessage()));
                //exceptionDescription = ex.getMessage();  
            }
            
            return Json.serialize(JsonLstWrapper);
    } */
}