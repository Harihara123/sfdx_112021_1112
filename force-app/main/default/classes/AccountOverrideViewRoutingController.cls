public class AccountOverrideViewRoutingController
{
   private final ApexPages.StandardController controller;
   private static Map<Id,String> accountRecordTypeNameCache = null;
   public string url {get;set;}

   @testVisible 
   private Boolean isSalesforce1 = UserInfo.getUiThemeDisplayed() == 'Theme4t';
   
   public AccountOverrideViewRoutingController(ApexPages.StandardController controller) {
        this.controller = controller;
        url = '';
   }
   
    public void onLoad() 
    {
        Account c = [Select id, recordtypeid,Talent_Ownership__c From Account Where Id = :ApexPages.currentPage().getParameters().get('id')];
        
        if(c.recordtypeid == System.Label.CRM_ACCOUNTCLIENTRECTYPE || (c.Talent_Ownership__c != null && c.Talent_Ownership__c.contains('Community'))) 
        {
            url = '/' + c.id +  '?nooverride=1';
        }
        else if(c.recordtypeid == System.Label.CRM_TALENTACCOUNTRECTYPE && (c.Talent_Ownership__c != null && !c.Talent_Ownership__c.contains('Community')))
        {
            // For salesforce 1 mobile app, send to old skuid candidate profile page.
            if (isSalesforce1) {
                //url = '/apex/c__CandidateSummary?id=' +  c.id;
                try {
                  Contact talentContact = [SELECT Id FROM Contact WHERE AccountId =: c.Id LIMIT 1];
                  url = '/' + talentContact.id;
                }
                catch(Exception e) {
                  //something really bad happened if we get here
                }
            } 
            else {
                //url = '/one/one.app#/sObject/' + c.id;
                url = System.Label.CONNECTED_Summer18URL;                
                String urlChangesOn = System.Label.CONNECTED_Summer18URLOn;
                if(urlChangesOn != null && (urlChangesOn.equalsIgnoreCase('true'))){
                    url = url + '/r/Account/' + c.id;
                }else{
                    url = url + '/sObject/' + c.id;
                }
            }
        }
        else if(c.recordtypeid == System.Label.CRM_REFERENCEACCOUNTRECTYPE) 
        {
              List<Talent_Recommendation__c> recs = [select Talent_Contact__r.AccountId from Talent_Recommendation__c where  Recommendation_From__r.AccountId = :c.id LIMIT 1];
              if(recs.size() > 0)
              {
                    url = '/' + recs[0].Talent_Contact__r.AccountId;
              }
              else
              {
                   url = '';
              }
        }
        else
        {
              url = '';
        }  
    }
}