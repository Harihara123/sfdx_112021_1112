@isTest
Public class AccountOverrideViewRoutingControllerTest{
  @isTest static void test_RoutingForTalentRecordType(){
    Profile p = [select id from profile where name='System Administrator']; 
    //User as System Admin
    User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
     System.runAs(u)
     {
           Account a =  new Account(RecordTypeId = System.Label.CRM_TALENTACCOUNTRECTYPE,
                                    Name = 'bwiIadTestAccount' + String.Valueof(Math.random()),
                                    Talent_Ownership__c = 'RPO');
              
           Test.startTest();   
             Insert a; 
           Test.stopTest();  
            
            PageReference pref =  Page.CandidateSummary;
            pref.getParameters().put('id', a.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController acctCtrl = new ApexPages.StandardController(a);
            AccountOverrideViewRoutingController ctrl = new AccountOverrideViewRoutingController(acctCtrl);
            
           // PageReference pageRef = ctrl.getRedir();
            //System.assertNotEquals(null, pageRef);
            
            ctrl.onLoad();
               }
            }

  @isTest static void test_RoutingForClientRecordType(){
       Profile p = [select id from profile where name='System Administrator']; 
       //User as System Admin
       User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
      System.runAs(u){
           List<RecordType> recTypes = [select Id from recordtype where name ='Client' and SobjectType = 'Account' LIMIT 1];
           Account c =  new Account(RecordTypeId = recTypes[0].Id,
                                    Name = 'BWITechAccount' + String.Valueof(Math.random()));
              
           Test.startTest();   
             Insert c; 
           Test.stopTest();  
            
            PageReference pref =  Page.CandidateSummary;
            pref.getParameters().put('id', c.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController acctCtrl = new ApexPages.StandardController(c);
            AccountOverrideViewRoutingController ctrl = new AccountOverrideViewRoutingController(acctCtrl);
            
            //PageReference pageRef = ctrl.getRedir();
            //System.assertNotEquals(null, pageRef);
             ctrl.onLoad();
           }
    } 
    
    @isTest static void test_RoutingForReferenceRecordType(){
       Profile p = [select id from profile where name='System Administrator']; 
       //User as System Admin
       User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
      System.runAs(u){
           Account t =  new Account(RecordTypeId = System.Label.CRM_REFERENCEACCOUNTRECTYPE,
                                    Name = 'TestBWAccount' + String.Valueof(Math.random()));
          Insert t;
          Contact c = new Contact(RecordTYpeId = System.label.CRM_TALENTCONTACTRECTYPE, 
                               FirstName = 'bwiIad' + String.valueof(Math.random()),
                               LastName = 'TestAccount' + String.valueOf(Math.random()),
                               Email = 'bwiIadTestAccount@test.com',
                               AccountId = t.Id);
           Test.startTest();   
             
          	 insert c;
             Talent_Recommendation__c talent = new Talent_Recommendation__c();
             //talent.Recommended_By__c = t.Id;
             talent.Talent_Contact__c = c.Id;
             
             Insert talent;
           Test.stopTest();  
            
            PageReference prf =  Page.CandidateSummary;
            prf.getParameters().put('id', t.Id);
            Test.setCurrentPage(prf);
            ApexPages.StandardController acctCtrl = new ApexPages.StandardController(t);
            AccountOverrideViewRoutingController ctrl = new AccountOverrideViewRoutingController(acctCtrl);
            
           // PageReference pageRf = ctrl.getRedir();
            //System.assertNotEquals(null, pageRf);
             ctrl.onLoad();
          }
         }

    @isTest static void test_Salesforce1RoutingTalentRecordType() {
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = [SELECT Id FROM User WHERE ProfileId =: p.Id AND isActive = true LIMIT 1];
      System.runAs(u) {
        Account a =  new Account(RecordTypeId = System.Label.CRM_TALENTACCOUNTRECTYPE,
                                Name = 'bwiIadTestAccount' + String.Valueof(Math.random()),
                                Talent_Ownership__c = 'RPO');  
        Insert a; 

        Contact c = new Contact(RecordTYpeId = System.label.CRM_TALENTCONTACTRECTYPE, 
                               FirstName = 'bwiIad' + String.valueof(Math.random()),
                               LastName = 'TestAccount' + String.valueOf(Math.random()),
                               Email = 'bwiIadTestAccount@test.com',
                               AccountId = a.Id);
        Insert c;       
        
        PageReference pref =  Page.CandidateSummary;
        pref.getParameters().put('id', a.id);
        Test.setCurrentPage(pref);
        Test.startTest();
        ApexPages.StandardController acctCtrl = new ApexPages.StandardController(a);
        AccountOverrideViewRoutingController ctrl = new AccountOverrideViewRoutingController(acctCtrl);
        ctrl.isSalesforce1 = true;
        ctrl.onLoad();
        Test.stopTest();
        System.assertEquals(ctrl.isSalesforce1, true, 'salesforce1 was not set properly');
        System.assertEquals(ctrl.url, '/' + c.Id, 'URL should redirect to contact record');
      }
    }


}