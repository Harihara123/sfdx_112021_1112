global class AccountSearchControlBatch implements Database.Batchable<sObject>  {

    global String query;
    //global Map<String,String> AccountSearchControlMap = new Map<String,String>();
    global List<Contact> toBeUpdated;
    
    //Constructor
    global AccountSearchControlBatch (String s)  
    {  
         //Constructing dynamic query         
        query = s;
        
    }  

    // Batch class start method
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {  
        return Database.getQueryLocator(query);
    }  

    // Batch Class execute method
    global void execute(Database.BatchableContext BC,List<Contact> batch)  
    {   
        toBeUpdated = new List<Contact>();
        for(Contact a : batch){
            if(a.Account_Search__c != a.Account.name){
                a.Account_Search__c = a.Account.name;
                toBeUpdated.add(a);
            }
        }        
        Database.update(toBeUpdated);
    }
    
    //Batch Class finish method
    global void finish(Database.BatchableContext BC)  
    {  
    } 
    
}