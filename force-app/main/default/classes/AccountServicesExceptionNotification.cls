//***************************************************************************************************************************************/
//* Name        - AccountServicesExceptionNotification
//* Description - Class used to Identify Accounts and Reqs that have failed more than X number of times in the last Y days and send an
//*               email notification to the Admin team member. 
//*               
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Pratz                       02/27/2013            Created
//* Pratz                       03/05/2013            Updated with Code Fix to ensure that once Accounts/Reqs are fixed, they don't appear in 
//*                                                    the next Email Notification
//*****************************************************************************************************************************************/

global class AccountServicesExceptionNotification implements Schedulable {
   
    //Declare Variables
    global static Integer NoOfAccountsFailed = 0 ;
    global static Integer NoOfReqsFailed = 0;
    global static String strListOfFailedAccounts = '';
    global static String strListOfFailedReqs = '';
    global Date d;
    List<Log__c> errors = new List<Log__c>();
    List<String> lstIRIds = new List<String>();
    
    //Logic that will Execute when the Scheduled job runs
    global void execute(SchedulableContext sc) {
    try{    
          //Date variable to store the Date (Y days before today's date)
          d = system.today().addDays(-(Integer.valueOf((Label.NumberOfDaysUsedForRetryFailureAlert).trim())));
          system.debug('Date: '+d);
          
          //Declare an Integer variable to store how many times an Account/Req has to fail to appear in the Error email notification
          Integer t = Integer.valueOf((Label.Record_Failure_Threshold).trim());
          
          /*****************************************************START CODE FIX:3/4/2013********************************************************************/
          
          /********************************************NOT REQUIRED*********************************************/
          //DateTime variable to reset the Last Time Job ran DateTime in the Custom Setting
          //DateTime newJobRunTime;
          /********************************************NOT REQUIRED*********************************************/
                
          //String variable to store the Status
          String strStatus = 'Success'; 
          
          
          /********************************************NOT REQUIRED*********************************************/
          //Get the ASNotificationJobStartDateTime Custom Setting Record
          //ASNotificationJobStartDateTime__c dt = ASNotificationJobStartDateTime__c.getValues('AccountServicesExceptionNotification');                   
          //Store the Last time the Job was Run in a variable
          //DateTime dtLastBatchRunTime = dt.Job_Run_Date_Time__c;
          //system.debug('Last Job Run Date Time: '+dt);
          /********************************************NOT REQUIRED*********************************************/
          
          //Query the Retry records which have been created since the last Y number of Days 
          String QueryRetry = 'SELECT Id, Id__c, CreatedDate FROM Retry__c WHERE CreatedDate > :d';
          
          //Create a Retry Map using the Query result
          Map<Id, Retry__c> RetryMap = new Map<Id,Retry__c>((List<Retry__c>)Database.query(QueryRetry));
          system.debug('Map RetryMap: '+RetryMap);   
    
          //Declare another Retry Map in the format RetryMap<Id__c, Retry__c>
          Map<String, Retry__c> RetryMapRefactored = new Map<String, Retry__c>();
          
          //Loop through the Retry Map to populate the new Retry Map in the form of Map<Id__c, Retry__c>          
          for(Id RetryId : RetryMap.keyset()){
               system.debug('string.valueOf(RetryMap.get(RetryId).Id__c) :'+ string.valueOf(RetryMap.get(RetryId).Id__c));
               system.debug('RetryMap.get(RetryId) :'+RetryMap.get(RetryId));              
               RetryMapRefactored.put(string.valueOf(RetryMap.get(RetryId).Id__c),RetryMap.get(RetryId));
          }                 

          system.debug('RetryMapRefactored :' + RetryMapRefactored);     
          
          /***************************************************************************************************************************************************/                            
          
          //Loop through the Retry Records that were created in the Last 'Y' days and which have failed more than 'X' number of times
          for(AggregateResult aggRes:[Select Id__c, COUNT(Id) NoOfFailures From Retry__c WHERE CreatedDate > :d GROUP BY Id__c Having Count(Id) >:t])
          {
                lstIRIds.add((string)aggRes.get('Id__c'));
          }
          system.debug(lstIRIds); 
          
           
          //Declare a new IR Map in the format <Object__Id__c, DateTime> 
          Map<String, DateTime> IRMapRefactored = new Map<String, DateTime>();
          
          //Identify IR Ids that have been created within last 'Y' days and have status = Success                                      
          for(AggregateResult aggIRRes : [SELECT MAX(CreatedDate)MxCreatedDate, Object_Id__c FROM Integration_Response__c WHERE Status__c =: strStatus AND (CreatedDate > :d AND Object_Id__c IN :lstIRIds) GROUP BY Object_Id__c]){  //TESTING                                    
             //Add the Object Id to the IRMapRefactored Map
             IRMapRefactored.put((string)aggIRRes.get('Object_Id__c'),(Datetime)aggIRRes.get('MxCreatedDate'));
          } 
          system.debug('lstIRIds :' + lstIRIds);     
                         
          system.debug('IRMapRefactored :' + IRMapRefactored);      
          /*****************************************************END CODE FIX:3/4/2013********************************************************************/                

          //Aggregate Query on Retry Records Created in the last Y days to identify the Accounts and Reqs that have failed atleast 'X'
          //number of times
          for(AggregateResult aggRes : [Select Id__c, COUNT(Id) NoOfFailures From Retry__c WHERE CreatedDate > :d GROUP BY Id__c Having Count(Id) >:t]) {            
          system.debug('No of Failures:'+aggRes.get('NoOfFailures'));
                                
               /*****************************************************START CODE FIX:3/4/2013********************************************************************/  
               //Declare a string variable to store get the Id__c from the AggregateResult records
               String strId1 = (string)aggRes.get('Id__c');
               system.debug('strId1: '+ strId1);               
               
               //Get the CreatedDate of the current Retry Record
               DateTime  dt1=  RetryMapRefactored.get(strId1).CreatedDate;
               system.debug('dt1 :'+dt1);
 
               //If there is an Integration Response Record with Status = Success, corresponding to the Id stored in the Variable strId1
               if(IRMapRefactored.containsKey(strId1)){    
                                   
                   //Get the Object_Id__c of the Integration Response records from IRMapRefactored                      
                   //String strId2 = IRMapRefactored.get(strId1).Object_Id__c;
                   
                   //Get the CreatedDate Datetime value from the IRMapRefactored                   
                   DateTime dt2= IRMapRefactored.get(strId1);
                   system.debug('dt2 :'+dt2);
                                 
                   //Check if the Id of the Integration Response Record and the Aggregate Result record match 
                   // And the Object_Last_Modified__c value for the successful IR record is after the Retry record's created date
                   //If this condition is true then it indicates that this Retry record has been successfully retried after its recent 'X' failures                        
                   //if(strId1.substring(0,14).equals(strId2.substring(0,14)) &&  (dt2 > dt1))
                   if(dt2 > dt1)
                   {
                      //DO NOTHING
                   }
                   else
                   {   
                      //Increment Account/Req Failure Count & Append the Error Notification with Account/Req Failure Record Ids
                      //IncrementFailureRecordCount(aggRes,strListOfFailedAccounts,strListOfFailedReqs,NoOfAccountsFailed,NoOfReqsFailed);
                      IncrementFailureRecordCount(aggRes);                      
                   }
                 }
                 
                 else
                 {   
                     //Increment Account/Req Failure Count & Append the Error Notification with Account/Req Failure Record Ids              
                     //IncrementFailureRecordCount(aggRes,strListOfFailedAccounts,strListOfFailedReqs,NoOfAccountsFailed,NoOfReqsFailed);
                     IncrementFailureRecordCount(aggRes);                       
                 }  
               
                   
             /*****************************************************END CODE FIX:3/4/2013********************************************************************/                             
       }       
       
       
       system.debug('Failed Accounts :'+strListOfFailedAccounts);
       system.debug('Failed Reqs :'+strListOfFailedReqs);
       system.debug('Total Number of Accounts Failed :' + NoOfAccountsFailed);
       system.debug('Total Number of Reqs Failed :' + NoOfReqsFailed);
       
       //Send an Email to an Admin Team member (email address stored in a custom label) 
       //if there is atleast one Account OR Req that fails more than 'X' number of times
       if(NoOfAccountsFailed > 0 || NoOfReqsFailed > 0)
       {
           //Send Email notification for Account/Req Failures           
           SendEmail(false,'');
       }
       
       
        
        /********************************************NOT REQUIRED*********************************************/
        //Update the Custom Setting's Job_Run_Date_Time__c with the New Time Stamp to store the last time the job ran
        //ASNotificationJobStartDateTime__c CustomSetting1 = ASNotificationJobStartDateTime__c.getValues('AccountServicesExceptionNotification');   
        
        //Get the current System DateTime to stamp on the Custom Setting
        //newJobRunTime = System.Now();                              
        //CustomSetting1.Job_Run_Date_Time__c = newJobRunTime ;       
        //System.debug('New Job Run DateTime: '+newJobRunTime);
        
        //Update Custom Setting
        //if(CustomSetting1 != null)
        //    update CustomSetting1;
        /********************************************NOT REQUIRED*********************************************/
        
    
    }
  catch (System.Exception e)
  {
          //Dump the error records to Log__c object
          //database.insert(errors,false);
          //errors.add(Core_Log.CustomException(e.getMessage()));
          
          //Send Email notification for Exception          
          SendEmail(true,e.getMessage());
  
  }         
 }      
     
     /*****************************************************START CODE FIX:3/4/2013********************************************************************/   
     //Method to Increment Failure Count & Append the Error Notification with Failure Record Ids 
     //public static void IncrementFailureRecordCount(AggregateResult aggRes, String strListOfFailedAccounts, String strListOfFailedReqs, Integer NoOfAccountsFailed, Integer NoOfReqsFailed){
     public static void IncrementFailureRecordCount(AggregateResult aggRes){
                                
           //If the Retry record is for an Account
           If(string.valueOf((Id)aggRes.get('Id__c')).startsWith(Label.AccountSFIdInitialCharachters))
           {
                //Prepare a string of Account Ids Failed more than 'X' number of times
                strListOfFailedAccounts = strListOfFailedAccounts+ '\n'+ string.valueOf((Id)aggRes.get('Id__c'))+ ';';
                     
                //Increment the number accounts failed more than 'X' number of times 
                NoOfAccountsFailed++;
           }
                
           //If the Retry record is for a Req   
           If(string.valueOf((Id)aggRes.get('Id__c')).startsWith(Label.ReqSFIdInitialCharachters))
           {
                //Prepare a string of Req Ids Failed more than 'X' number of times
                strListOfFailedReqs = strListOfFailedReqs+ '\n' +string.valueOf((Id)aggRes.get('Id__c'))+ ';';
                     
                //Increment the number Reqs failed more than 'X' number of times
                NoOfReqsFailed++;
           }     

     }
     
     /*****************************************************END CODE FIX:3/4/2013********************************************************************/        
     //Method to send an Email to Admin Team member 
     //public static void SendEmail(Integer NoOfAccountsFailed, Integer NoOfReqsFailed, String strListOfFailedAccounts, String strListOfFailedReqs, Boolean IsException){
     public static void SendEmail(Boolean IsException, String strErrorMessage){
                                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  
                mail.setToAddresses(toAddresses); 
                String strEmailBody; 
                
                //If there is no Exception
                if(IsException == false)
                {
                    mail.setSubject('Repeated Account Req Retry Failure Warnings'); 
                
                    //Prepare the body of the email
                    strEmailBody = 'The scheduled Apex job processed and found '+ NoOfAccountsFailed +' Accounts and '+ NoOfReqsFailed +' Reqs that failed more than '+Label.Record_Failure_Threshold+' times in the last '+Label.NumberOfDaysUsedForRetryFailureAlert+' days and which have not been fixed yet.\n\n' + 'Account Failures: \n' + strListOfFailedAccounts +' \n\nReq Failures:\n' +  strListOfFailedReqs;                  
                }
                
                //If there is an exception
                else
                {
                    mail.setSubject('Repeated Account Req Retry Failure Warnings : Exception'); 
                    //Prepare the body of the email
                    strEmailBody = 'The scheduled Apex Job failed to process. There was an exception during execution.\n'+ strErrorMessage; 
                    
                }
                
                mail.setPlainTextBody(strEmailBody);   
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  

    }

    
}