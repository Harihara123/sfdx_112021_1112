/*******************************************************************
Name        : AccountStrategicInitTriggerHandler
Created By  : Vivek Ojha (Appirio)
Date        : 04 June 2015
Story/Task  : S-321244 / T-407647
Purpose     : This trigger is invoked for all contexts
              and delegates control to AccountStrategicInitTriggerHandler
********************************************************************/
public with sharing class AccountStrategicInitTriggerHandler {
//comment test
String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    Map<Id, Account_Strategic_Initiative__c> oldMap = new Map<Id, Account_Strategic_Initiative__c>();
    Map<Id, Account_Strategic_Initiative__c> newMap = new Map<Id, Account_Strategic_Initiative__c>();
    
     //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Account_Strategic_Initiative__c> accountStratRec) {
        
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, Account_Strategic_Initiative__c>newMap) {
       // populatePrimaryAccount(accountStratRec); Commenting as it is not needed -Nidish.
       StrategicPlanUpdatePriorityDate(oldmap, newMap);
       AccountSP_UpdateAssociatedPrimary (afterInsert, oldMap, newMap);
        
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Account_Strategic_Initiative__c>oldMap, Map<Id, Account_Strategic_Initiative__c>newMap) {
       
        
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Account_Strategic_Initiative__c>oldMap, Map<Id, Account_Strategic_Initiative__c>newMap) {   
        AccountSP_UpdateAssociatedPrimary (afterUpdate, oldMap, newMap);
        
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Account_Strategic_Initiative__c>oldMap) {
    PrimaryAccountDelete(beforeDelete, oldMap, newMap,oldMap.values());   
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Account_Strategic_Initiative__c>oldMap) {
        StrategicPlanUpdatePriorityDateDel(oldmap);

    }


    //-------------------------------------------------------------------------
    // On after insert trigger method to populate Primary_Account__c on Strategic_Initiative__c if its null.
    // Commenting code as it is not needed. -Nidish.
    //-------------------------------------------------------------------------
 /*   public void populatePrimaryAccount(List<Account_Strategic_Initiative__c> accountStratRec) {
        Map<Id,Id> stratIds = new Map<Id,Id>();
        List<Strategic_Initiative__c> stratRecList = new List<Strategic_Initiative__c>();
        for(Account_Strategic_Initiative__c rec : accountStratRec){
            stratIds.put(rec.Strategic_Initiative__c,rec.Account__c);
            
        }
        for(Strategic_Initiative__c stratRec : [Select Id , Primary_Account__c  FROM Strategic_Initiative__c Where ID IN : stratIds.keyset() and Primary_Account__c = null]){
            stratRec.Primary_Account__c = stratIds.get(stratRec.ID);
            stratRecList.add(stratRec);
        }
        update stratRecList;
    } */
    
    //--------------------------------------------------------------------
    // method to uncheck primary when primary account is changed on Account plan -Nidish
    //----------------------------------------------------------------------
    
    public void StrategicPlanUpdatePriorityDate (Map<Id, Account_Strategic_Initiative__c>oldMap, Map<Id, Account_Strategic_Initiative__c>newMap) {
        
        List<Strategic_Initiative__c> SPsToUpdatePriority = new List<Strategic_Initiative__c>();
        List<Log__c> errorLogs = new List<Log__c>();
        set<string> strPlans = new set<string>();
       // List<Account_Strategic_Initiative__c> TriggerNewVal = newMap.values();
        //system.debug('TriggerNewVal' +TriggerNewVal);
        for(Account_Strategic_Initiative__c rec :newMap.values()){
            system.debug('before the if');
            system.debug('recordtype'+rec.Strategic_Initiative__r.recordtype.name );
             
           
                system.debug('in the if');
                   strplans.add(rec.Strategic_Initiative__c);
                system.debug('strplans '+ strplans);
             
        }
    
        
        //list<Strategic_Initiative__c> StrQuery =[select id, Priority_Changed_Date__c from Strategic_Initiative__c where id in:strplans];
        
        for(Strategic_Initiative__c strtoupdate: [select id, Priority_Changed_Date__c from Strategic_Initiative__c where id in:strplans and recordtype.name='Account Plan'] ){
            
            strtoupdate.Priority_Changed_Date__c=date.today();
            SPsToUpdatePriority.add(strtoupdate);
            
        }
        
        system.debug('strp' + SPsToUpdatePriority);
        
         if(SPsToUpdatePriority.size() > 0)
        {
            for(database.saveResult result : database.update(SPsToUpdatePriority,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        }

        // insert error logs
        if(errorLogs.size() > 0)
            database.insert(errorLogs,false);
    
        
        
        
    }
    
    
   public void StrategicPlanUpdatePriorityDateDel(Map<Id, Account_Strategic_Initiative__c>oldMap){
        List<Strategic_Initiative__c> SPsToUpdatePriority = new List<Strategic_Initiative__c>();
        List<Log__c> errorLogs = new List<Log__c>();
        set<string> Actids = new set<string>();
       set<string>ownerids =new set<string>();
       set<string> strids =new set<string>();
       
       for(Account_Strategic_Initiative__c rec :oldmap.values()){
           
           actids.add(rec.Account__c);
           ownerids.add(rec.Strategic_Initiative__r.ownerid);
           system.debug('ownerids'+ ownerids);
       }
       
       for(Account_Strategic_Initiative__c Actstrtoupdate: [select id, Strategic_Initiative__c from Account_Strategic_Initiative__c where Account__c  in:actids and Strategic_Initiative__r.recordtype.name='Account Plan' ] ){
           
           
       strids.add(Actstrtoupdate.Strategic_Initiative__c);
       
       
            }
       for(Strategic_Initiative__c strtoupdate:[Select id, 	Priority_Changed_Date__c from Strategic_Initiative__c where id IN:strids] ){
           strtoupdate.Priority_Changed_Date__c= date.today();
           SPsToUpdatePriority.add(strtoupdate);
       }
    
        if(SPsToUpdatePriority.size() > 0)
        {
            for(database.saveResult result : database.update(SPsToUpdatePriority,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        }

        // insert error logs
        if(errorLogs.size() > 0)
            database.insert(errorLogs,false);
       
   }
    
        public void AccountSP_UpdateAssociatedPrimary (String Context, Map<Id, Account_Strategic_Initiative__c>oldMap, Map<Id, Account_Strategic_Initiative__c>newMap) {

        List<Log__c> errorLogs = new List<Log__c>();
        set<string> associatedSPs = new set<string>();
        List<Strategic_Initiative__c> SPsToUpdate = new List<Strategic_Initiative__c>();
        List<Account_Strategic_Initiative__c> associationRecordsToUpdate = new List<Account_Strategic_Initiative__c>();

        //loop over the contact association records and check if there are any marked as primary
        List<Account_Strategic_Initiative__c> TriggerNew = newMap.values();
        for(Account_Strategic_Initiative__c rec : TriggerNew)
        {
            
            boolean addToList = true;
            if(!Context.equals(afterInsert))
            {
                
                
                addToList = false;
                if(rec.Primary__c && oldMap.get(rec.Id).Primary__c != rec.Primary__c)
                     addToList = true;
            }
            else
            {
                if(!rec.Primary__c)
                    addToList = false;
            }
            if(addToList)
                 associatedSPs.add(rec.Strategic_Initiative__c);
        }

        //query the contact and its associated AccountContact Association records
        if(associatedSPs.size() > 0)
        {
            for(Strategic_Initiative__c SP : [select Primary_Account__c,(select Primary__c from Account_Strategic_Initiatives__r order by LastModifiedDate desc) from Strategic_Initiative__c where id in :associatedSPs])
            {
                boolean isPrimary = false;
                // Loop over the child records
                for(Account_Strategic_Initiative__c recs : SP.Account_Strategic_Initiatives__r)
                {
                    boolean isCurrentRecord = false;
                    if(newMap.get(recs.Id) != Null) //current record
                    {
                        SP.Primary_Account__c = newMap.get(recs.Id).Account__c;
                        if(newMap.get(recs.Id).Primary__c)
                            isPrimary = true;
                        isCurrentRecord = true;
                    }
                    if(isPrimary)
                    {
                        if(!isCurrentRecord)
                        {
                            recs.Primary__c = false;
                            associationRecordsToUpdate.add(recs);
                        }
                    }

                }
                SPsToUpdate.add(SP);
            }
        }
        system.debug('associationRecordsToUpdate:'+associationRecordsToUpdate);
        //update the SP records accordingly
      /*  if(SPsToUpdate.size() > 0)
        {
            for(database.saveResult result : database.update(SPsToUpdate,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        } */
        //update the records accordingly
        if(associationRecordsToUpdate.size() > 0)
        {
            for(database.saveResult result : database.update(associationRecordsToUpdate,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        }

        // insert error logs
        if(errorLogs.size() > 0)
            database.insert(errorLogs,false);
    } 
    
    //--------------------------------------------------------------------
    // Validation method to check for primary account before delete -Nidish
    //----------------------------------------------------------------------
       public static void PrimaryAccountDelete(String Context, Map<Id, Account_Strategic_Initiative__c>oldMap, Map<Id, Account_Strategic_Initiative__c>newMap,List<Account_Strategic_Initiative__c> triggerOldRec){
           if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) {
           for(Account_Strategic_Initiative__c PA:triggerOldRec){
              if(PA.Primary__c)
                oldMap.get(PA.Id).addError(Label.AccountContactAssociation_DeleteError,false);
           }
           }
       }
    
}