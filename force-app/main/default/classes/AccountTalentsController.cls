public class AccountTalentsController {

@AuraEnabled
 public static String getAccountCurrentsJSON(string acctId){
     
     talentwrapper talentData = new talentwrapper();
     List<DataWrapper> currents = new List<DataWrapper>();
     List<DataWrapper> formers = new List<DataWrapper>();
     List<DataWrapper> placedCandidates = new List<DataWrapper>();
     List<Talent_Work_History__c> submittallist = new List<Talent_Work_History__c>();
     List<Talent_Work_History__c> formerList = new List<Talent_Work_History__c>();
     List<Talent_Work_History__c> placedList = new List<Talent_Work_History__c>();
     try{
         /* Opening the component in contact detail page - Using Hiring Manager Id */
         if(string.valueOf((acctId)).startsWith('003')){
                   
         	submittallist = [SELECT DivisionName__c, Hiring_Manager_Contact_ID__c,
                                  Hiring_Manager_Contact_ID__r.Name ,Talent__c, Talent__r.Name, Job_Title__c, 
                                  Start_Date__c, End_Date__c, Bill_rate__c, OrderId__r.Opportunity.OwnerId, OrderId__r.OpportunityId,
                                  OrderId__r.Opportunity.Owner.Name, OrderId__r.Opportunity.OpCo__c from Talent_Work_History__c
                                  where Hiring_Manager_Contact_ID__c =: acctId and End_Date__c >= LAST_N_DAYS:8 
                                  ORDER BY  End_Date__c DESC LIMIT 10];    
         
         	formerList = [SELECT DivisionName__c, Hiring_Manager_Contact_ID__c,
                                  Hiring_Manager_Contact_ID__r.Name ,Talent__c, Talent__r.Name, Job_Title__c, 
                                  Start_Date__c, End_Date__c, Bill_rate__c, OrderId__r.Opportunity.OwnerId, OrderId__r.OpportunityId,
                                  OrderId__r.Opportunity.Owner.Name, OrderId__r.Opportunity.OpCo__c from Talent_Work_History__c
                                  where Hiring_Manager_Contact_ID__c =: acctId and End_Date__c < LAST_N_DAYS:8 
                                  ORDER BY  End_Date__c DESC LIMIT 10];  
           placedList = [SELECT DivisionName__c, Hiring_Manager_Contact_ID__c,
                                  Hiring_Manager_Contact_ID__r.Name ,Talent__c, Talent__r.Name, Job_Title__c, 
                                  Start_Date__c, End_Date__c, Bill_rate__c, OrderId__r.Opportunity.OwnerId, OrderId__r.OpportunityId,
                                  OrderId__r.Opportunity.Owner.Name, OrderId__r.Opportunity.OpCo__c from Talent_Work_History__c
                                  where Hiring_Manager_Contact_ID__c =: acctId and Talent__r.Candidate_Status__c = 'Placed' 
                                  ORDER BY  End_Date__c DESC LIMIT 10];   
         }
         else{
         	submittallist = [SELECT DivisionName__c, Hiring_Manager_Contact_ID__c,
                                  Hiring_Manager_Contact_ID__r.Name ,Talent__c, Talent__r.Name, Job_Title__c, 
                                  Start_Date__c, End_Date__c, Bill_rate__c, OrderId__r.Opportunity.OwnerId, OrderId__r.OpportunityId,
                                  OrderId__r.Opportunity.Owner.Name, OrderId__r.Opportunity.OpCo__c from Talent_Work_History__c
                                  where Client_Account_ID__c =: acctId and End_Date__c >= LAST_N_DAYS:8 
                                  ORDER BY  End_Date__c DESC LIMIT 10];    
         
         	formerList = [SELECT DivisionName__c, Hiring_Manager_Contact_ID__c,
                                  Hiring_Manager_Contact_ID__r.Name ,Talent__c, Talent__r.Name, Job_Title__c, 
                                  Start_Date__c, End_Date__c, Bill_rate__c, OrderId__r.Opportunity.OwnerId, OrderId__r.OpportunityId,
                                  OrderId__r.Opportunity.Owner.Name, OrderId__r.Opportunity.OpCo__c from Talent_Work_History__c
                                  where Client_Account_ID__c =: acctId and End_Date__c < LAST_N_DAYS:8 
                                  ORDER BY  End_Date__c DESC LIMIT 10];   
            
             placedList = [SELECT DivisionName__c, Hiring_Manager_Contact_ID__c,
                                  Hiring_Manager_Contact_ID__r.Name ,Talent__c, Talent__r.Name, Job_Title__c, 
                                  Start_Date__c, End_Date__c, Bill_rate__c, OrderId__r.Opportunity.OwnerId, OrderId__r.OpportunityId,
                                  OrderId__r.Opportunity.Owner.Name, OrderId__r.Opportunity.OpCo__c from Talent_Work_History__c
                                  where Client_Account_ID__c =: acctId and Talent__r.Candidate_Status__c = 'Placed'  
                                  ORDER BY  End_Date__c DESC LIMIT 10];
         }
         
         
      if(submittallist.size() > 0){
          for (Talent_Work_History__c ord: submittallist){
            DataWrapper dw = new DataWrapper();
              dw.opco = String.Valueof(ord.OrderId__r.Opportunity.OpCo__c);
              dw.ownerId = String.Valueof(ord.OrderId__r.Opportunity.OwnerId);
              dw.ownerName = String.Valueof(ord.OrderId__r.Opportunity.Owner.Name);
              dw.opportunityId = String.ValueOf(ord.OrderId__r.OpportunityId);
              dw.division = String.Valueof(ord.DivisionName__c);
              dw.hiringmanager = String.Valueof(ord.Hiring_Manager_Contact_ID__r.Name);
              dw.hiringmanagerId = String.Valueof(ord.Hiring_Manager_Contact_ID__c);
              dw.candidatename = String.Valueof(ord.Talent__r.Name);
              dw.candidateId = String.Valueof(ord.Talent__c);
              dw.jobtitle =  String.isBlank(ord.Job_Title__c) ? '': String.Valueof(ord.Job_Title__c);
              dw.startdate = ord.Start_Date__c == null ? '': String.Valueof(ord.Start_Date__c);
              dw.enddate = ord.End_Date__c == null ? '' : String.Valueof(ord.End_Date__c);
              dw.billrate = String.Valueof(ord.Bill_rate__c);
              currents.add(dw);
          }
          talentData.currentTalents = currents;
      }
         
       if(formerList.size() > 0){
          for (Talent_Work_History__c ord: formerList){
            DataWrapper dw = new DataWrapper();
              dw.opco = String.Valueof(ord.OrderId__r.Opportunity.OpCo__c);
              dw.ownerId = String.Valueof(ord.OrderId__r.Opportunity.OwnerId);
              dw.ownerName = String.Valueof(ord.OrderId__r.Opportunity.Owner.Name);
              dw.opportunityId = String.ValueOf(ord.OrderId__r.OpportunityId);              
              dw.division = String.Valueof(ord.DivisionName__c);
              dw.hiringmanager = String.Valueof(ord.Hiring_Manager_Contact_ID__r.Name);
              dw.hiringmanagerId = String.Valueof(ord.Hiring_Manager_Contact_ID__c);
              dw.candidatename = String.Valueof(ord.Talent__r.Name);
              dw.candidateId = String.Valueof(ord.Talent__c);
              dw.jobtitle =  String.isBlank(ord.Job_Title__c) ? '': String.Valueof(ord.Job_Title__c);
              dw.startdate = ord.Start_Date__c == null ? '': String.Valueof(ord.Start_Date__c);
              dw.enddate = ord.End_Date__c == null ? '' : String.Valueof(ord.End_Date__c);
              dw.billrate = String.Valueof(ord.Bill_rate__c);
              formers.add(dw);
          }
         talentData.formerTalents = formers;
      }
         
      if(placedList.size() > 0){
          for (Talent_Work_History__c ord: placedList){
            DataWrapper dw = new DataWrapper();
              dw.opco = String.Valueof(ord.OrderId__r.Opportunity.OpCo__c);
              dw.ownerId = String.Valueof(ord.OrderId__r.Opportunity.OwnerId);
              dw.ownerName = String.Valueof(ord.OrderId__r.Opportunity.Owner.Name);
              dw.opportunityId = String.ValueOf(ord.OrderId__r.OpportunityId);              
              dw.division = String.Valueof(ord.DivisionName__c);
              dw.hiringmanager = String.Valueof(ord.Hiring_Manager_Contact_ID__r.Name);
              dw.hiringmanagerId = String.Valueof(ord.Hiring_Manager_Contact_ID__c);
              dw.candidatename = String.Valueof(ord.Talent__r.Name);
              dw.candidateId = String.Valueof(ord.Talent__c);
              dw.jobtitle =  String.isBlank(ord.Job_Title__c) ? '': String.Valueof(ord.Job_Title__c);
              dw.startdate = ord.Start_Date__c == null ? '': String.Valueof(ord.Start_Date__c);
            //  dw.enddate = ord.End_Date__c == null ? '' : String.Valueof(ord.End_Date__c);
              dw.billrate = String.Valueof(ord.Bill_rate__c);
              placedCandidates.add(dw);
          }
         talentData.placedTalents = placedCandidates;
		}	   
         
     }catch(Exception e)
    {
      System.debug('Error Response: ' + e.getMessage());
    }
   return System.json.serialize(talentData);
 }  
    
 public class DataWrapper{
       @AuraEnabled
       public String opco;
       @AuraEnabled
       public String ownerId;
       @AuraEnabled
       public String ownerName;
       @AuraEnabled
       public String opportunityId;
       @AuraEnabled
       public String division;
       @AuraEnabled
       public String hiringmanager;
       @AuraEnabled
       public String hiringmanagerId;
       @AuraEnabled
       public String candidatename;
       @AuraEnabled
       public String candidateId;
       @AuraEnabled
       public String jobtitle;
       @AuraEnabled
       public String startdate;
       @AuraEnabled
       public String enddate;
       @AuraEnabled
       public String billrate;
       @AuraEnabled
       public String lastactivitydate;
   }
    
 public class talentwrapper{
        @AuraEnabled
        public List<DataWrapper> currentTalents;
        @AuraEnabled
        public List<DataWrapper> formerTalents;
      	@AuraEnabled
        public List<DataWrapper> placedTalents;
    }
}