//Class that contains all of the functionality called by the AccountTrigger. All contexts should be in this class.
public with sharing class AccountTriggerHandler {


    Static String ATS_DATS_MIGRATION = 'ATS Data Migration';
    Static String TALENT = 'Talent'; 
    Static ID TALENT_RECORD_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(TALENT).getRecordTypeId(); 
    static ID REFERNCE_RECORDTYE_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Reference').getRecordTypeId(); 
    private static string validOpcosForPlatformEvent = 'ONS,TEK,ASI,IND,SJA,EAS';
    private static boolean hasBeenProccessed = false;
    private static boolean hasBeenProccessedForPlatformEvent = false;
    private static string talentOwnerId = '005240000038GjfAAE';


    
    // On before insert trigger method
    public void OnBeforeInsert(List<Account> accounts) {
        
        AccountBeforeInsertTrigger(accounts);
        SumScoreCard_UpdateTrigger(accounts);
        stampTalentG2SummaryOnInsert(accounts);
        UpdateAccountSource(Accounts);
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            UpdateTalentOwnershipForRole(accounts);
        }
        DunsNumberUpdateTrigger(accounts);
    }
    
    // On after insert trigger method
    public void OnAfterInsert(List<Account> accounts) {
        
        if (!Test.isRunningTest()){
            if (accounts != null) {
                for (Account a : accounts) {
                    TimemachineActionEvent tae = new TimemachineActionEvent();
                    tae.RaiseObjectChangeEvent('Create', 'Account', a.Id, a);
                }
            }
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'AccountTriggerHandler', 'OnAfterInsert', 
                        'Triggered ' + accounts.size() + ' Account records: ' + CDCDataExportUtility.joinObjIds(accounts));
                }
                PubSubBatchHandler.insertDataExteractionRecord(accounts, 'Account');
                hasBeenProccessed = true;
            }
       
        }
        /*
            ---Start--- 
            Generating platform event and saveing records into a custom object. 
            This PE is generating for sending account ids to Muelsoft when a new account created.
        */
        List<Account_Platform_Event__c> accPEList = new List<Account_Platform_Event__c>();
        User usr = [select opco__c,Alias from user where id = :userInfo.getUserId() limit 1];
        List<ConnectedToRWSCandidateSync__e> pe = new List<ConnectedToRWSCandidateSync__e>();
        for (Account a : accounts) {
            if  (!hasBeenProccessedForPlatformEvent  &&   (a.Record_Type_Name__c == TALENT && a.Talent_Committed_Flag__c == true && a.Source_System_Id__c == null)
                &&((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) && !String.isBlank(usr.Opco__c) && validOpcosForPlatformEvent.contains(usr.Opco__c))
                   //(usr.OPCO__c  == 'ONS' || usr.OPCO__c  == 'TEK' || usr.OPCO__c  == 'ASI'))
                 || (usr.Alias == 'jinte'  || usr.Alias == 'cinte') )   {
                        ConnectedToRWSCandidateSync__e peObj = new ConnectedToRWSCandidateSync__e();
                        peObj.Id__c = a.Id;
                        peObj.UserId__c = a.CreatedById;
                        pe.add(peObj);
                   
                        Account_Platform_Event__c platformEvent = new Account_Platform_Event__c();
                        platformEvent.Account_ID__c = a.Id;
                        platformEvent.User__c       = a.CreatedById;  
                        accPEList.add(platformEvent);
                    }   
        }
        boolean isPublished = false;
        if (pe.size() > 0) {
            system.debug('OnAfterInsert:::Platform event for Sending account ids to mulesoft' + pe);
            List<Database.SaveResult> results = EventBus.publish(pe);
            
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                   isPublished = true;
                } else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('OnAfterInsert::::Error returned: ' +  err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }       
            }
        }

        if (isPublished && accPEList.size() > 0) 
        {
            try {
                insert(accPEList);
            } catch(DmlException dexp) {
                ConnectedLog.LogException('AccountTriggerHandler', 'OnAfterInsert', dexp);
            }
          
            hasBeenProccessedForPlatformEvent = true;
        }
       //---end----
    } 
    
    // On before update trigger method
    public void OnBeforeUpdate(Map<Id, Account>oldMap, Map<Id, Account>newMap) {
        if(!triggerstopper.AccountBeforeUpdateTrigger){
            triggerstopper.AccountBeforeUpdateTrigger=true;
            AccountBeforeUpdateTrigger(oldMap, newMap);
        }

        stampTalentG2SummaryOnUpdate(oldMap, newMap);
        

        if(!triggerstopper.sumscorecardstopper){
            triggerstopper.sumscorecardstopper=true;
            SumScoreCard_UpdateTrigger(newMap.values());
        }
        DunsNumberUpdateTrigger(newMap.values());
    }
    
    // On after update trigger method
    public void OnAfterUpdate (Map<Id, Account>oldMap, Map<Id, Account>newMap) {        
        if(!TriggerStopper.AccountIntelHistory){
            TriggerStopper.AccountIntelHistory = true;
            createHistoryRecord(oldMap, newMap);
        }
        
        if (!Test.isRunningTest()){
            if (newMap != null) {
                for (Account a : newMap.values()) {
                    TimemachineActionEvent tae = new TimemachineActionEvent();
                    tae.RaiseObjectChangeEvent('Update', 'Account', a.Id, a);
                }
            } 
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'AccountTriggerHandler', 'OnAfterUpdate', 
                        'Triggered ' + newMap.values().size() + ' Account records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
                }
                PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'Account');
                hasBeenProccessed = true;
           } 
        }

       

       

    }
    
    // On before delete trigger method
    public void OnBeforeDelete (Map<Id, Account>oldMap) {
        AccountMerge_ReqPushToIR(oldMap);
    }
    
    // On after delete trigger method
    public void OnAfterDelete (Map<Id, Account>oldMap) {
         if (!Test.isRunningTest()){
            if (oldMap != null) {
                for (Account a : oldMap.values()) {
                    TimemachineActionEvent tae = new TimemachineActionEvent();
                    tae.RaiseObjectChangeEvent('Delete', 'Account', a.Id, a);
                }
                Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
                if (config.Data_Extraction_Insert_Flag__c) {
                    if (config.Enable_Trace_Logging__c) {
                        ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'AccountTriggerHandler', 'OnAfterDelete', 
                            'Triggered ' + oldMap.values().size() + ' Account records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
                    }
                    PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Account');  
                }
            }
        }
    }   


    // On after insert Async Trigger method
    public void OnAfterInsertAsync (Set<String> accIds) {
    //  System.debug('error test');
        
        List<Account> AccList = new List<Account>();
        for (Account entry : [SELECT Recordtypeid,ownerid,Talent_Committed_Flag__c, id FROM Account where id in :accIds]) {
            if (entry.OwnerId != talentOwnerId
                    && entry.Talent_Committed_Flag__c
                    && (entry.Recordtypeid == TALENT_RECORD_ID || entry.Recordtypeid == REFERNCE_RECORDTYE_ID)) {
                entry.OwnerId = talentOwnerId;
                AccList.add(entry);
            }
        }
        try {
            upsert AccList;
        } catch (Exception ex) {
            ConnectedLog.LogException('AccountTriggerHandler', 'OnAfterInsertAsync', ex);
        }
    }



    
    // New method to filter only client Accounts and call the Utility class
    public void createHistoryrecord(map<id,Account> oldmap,map<id, Account> newmap){
        Map<id,Account> newmapfiltered = new map<id,Account>();
        for(Account act :newmap.values()){
            
            if(act.recordtypeid==Label.Account_Client_RecordTypeID){
                newmapfiltered.put(act.id,act);
            }
        }
        
        if(newmapfiltered.size()>0){
            Utility.createHistoryRecord(oldMap, newmapfiltered, 'Account');
        }  
    }
    
    public void UpdateAccountSource(List<Account> accounts) {
        if (UserInfo.getName() != 'Account Integration'){
            for (Account p : accounts){  
                /* The bellow functionality is only for Data.com */
                if( Utilities.objectHasField('DunsNumber', p ) ){
                    if(p.recordtypeid == Label.Account_Client_RecordTypeID){
                        if(p.jsImpacts__Added_from_Data_com__c==1){
                            p.ShippingStreet=p.BillingStreet;
                            p.ShippingCity=p.BillingCity;
                            p.ShippingCountry=p.BillingCountry;
                            p.ShippingState=p.BillingState;
                            p.ShippingPostalCode=p.BillingPostalCode;
                            p.AccountSource='Jigsaw';
                            p.BillingStreet='';
                            p.BillingCity='';
                            p.BillingCountry='';
                            p.BillingState='';
                            p.BillingPostalCode ='';   
                        }  
                    }
                }
            }
        }
    }
    
     public void DunsNumberUpdateTrigger(List<Account> accounts) {
        for (Account d : accounts){
            
            if(d.recordtypeid == Label.Account_Client_RecordTypeID){
                
                if( Utilities.objectHasField('iSell__OSKeyID__c', d) ){
                    
                    if( d.DUNS__c != null && d.DUNS__c != ''){
                        d.iSell__OSKeyID__c = d.DUNS__c;
                    }else{
                       // d.DUNS__c = d.DunsNumber;
                        //d.NAICS_Code__c = d.NaicsCode;
                        //d.NAICS_Description__c = d.NaicsDesc;
                        //d.Tradestyle__c = d.Tradestyle;
                        //d.YearStarted__c = d.YearStarted; 
                        //d.iSell__OSKeyID__c = d.DunsNumber;
                    }
                        
                    
                }
            }
        }
    }
    
    // Captures the current DateTime in AccountServicesChange__c field on Account object record for new account create event to Siebel relevant fields
    public void AccountBeforeInsertTrigger(List<Account> accounts) {
        for (Account p : accounts){
            
            if(p.recordtypeid == Label.Account_Client_RecordTypeID){ // added condition to fire this only Client Accounts - Nidish
                
                if( Utilities.objectHasField('DunsNumber', p) ){
                    
                    if( p.DUNS__c != null && p.DUNS__c != ''){
                        //p.DunsNumber = p.DUNS__c;
                    }else{//
                        //p.DUNS__c = p.DunsNumber;
                        //p.NAICS_Code__c = p.NaicsCode;
                        //p.NAICS_Description__c = p.NaicsDesc;
                        //p.Tradestyle__c = p.Tradestyle;
                        //p.YearStarted__c = p.YearStarted; 
                        //p.iSell__OSKeyID__c = p.DunsNumber;
                    }
                        
                    
                }
                
                if (UserInfo.getName() != 'Account Integration'){
                    p.AccountServicesChange__c = System.now();            
                }
 
                // Update contact Phone with correct Format...Fix Phone and Fax format for newly created Data.com Accounts
                if (p.Phone != NULL)
                {
                    if (p.ShippingCountry == 'United States' || p.ShippingCountry== 'USA' || p.ShippingCountry== 'US' || p.ShippingCountry== 'Canada' || p.ShippingCountry== 'CA'||p.BillingCountry == 'United States' || p.BillingCountry== 'USA' || p.BillingCountry== 'US' || p.BillingCountry== 'Canada' || p.BillingCountry== 'CA')
                    {
                        if (p.Phone.startsWith('+1'))
                        {
                            p.Phone = p.Phone.substring(3);
                            p.Phone = p.Phone.replace('.', '-');
                            p.Phone = p.Phone.replaceFirst('(^[0-9][0-9][0-9]).','($1) ');
                        }
                    }
                    p.Phone = p.Phone.replace('.', '-');
                }
                if (p.Fax != NULL)
                {
                    if (p.ShippingCountry == 'United States' || p.ShippingCountry== 'USA' || p.ShippingCountry== 'US' || p.ShippingCountry== 'Canada' || p.ShippingCountry== 'CA'||p.BillingCountry == 'United States' || p.BillingCountry== 'USA' || p.BillingCountry== 'US' || p.BillingCountry== 'Canada' || p.BillingCountry== 'CA')
                    {
                        if (p.Fax.startsWith('+1'))
                        {
                            p.Fax= p.Fax.substring(3);
                            p.Fax = p.Fax.replace('.', '-');
                            p.Fax= p.Fax.replaceFirst('(^[0-9][0-9][0-9]).','($1) ');
                        }
                    }
                    p.Fax = p.Fax.replace('.', '-');
                }
            }
        }
    }
    
    // Captures the current DateTime in AccountServicesChange__c field on Account object record
    public void AccountBeforeUpdateTrigger(Map<Id, Account>oldMap, Map<Id, Account>newMap) {
        Boolean IsValidFieldChange = False;
        String QueryCustomSetting;
        Map<Id, AccountUpdateAPIFields__c> AccountUpdateAPIFieldsMap = new Map<Id,AccountUpdateAPIFields__c>();
        
        QueryCustomSetting = 'SELECT Account_API_Field_Name__c FROM AccountUpdateAPIFields__c';
        AccountUpdateAPIFieldsMap = new Map<Id, AccountUpdateAPIFields__c>((List<AccountUpdateAPIFields__c>)Database.query(QueryCustomSetting));
        
        List<Account> TriggerNew = newMap.values();
        for(Account a : TriggerNew){
            if(a.recordtypeid == Label.Account_Client_RecordTypeID){ // only Client Accounts
                Account oldAccount = oldMap.get(a.ID);
                
                //Loop through the Custom Setting Fields
                for(Id FieldId :AccountUpdateAPIFieldsMap.keyset())
                {
                    //If atleast one Valid field has been updated then set IsValidFieldChange = True
                    if(string.valueOf(a.get(AccountUpdateAPIFieldsMap.get(FieldId).Account_API_Field_Name__c)) != string.valueOf(oldAccount.get(AccountUpdateAPIFieldsMap.get(FieldId).Account_API_Field_Name__c)))
                    {
                        IsValidFieldChange = True;
                        break;
                    }
                }

                if( (IsValidFieldChange && (UserInfo.getName() != 'Account Integration'))  || (UserInfo.getProfileId().substring(0,15) == Label.Profile_IS_Admin || UserInfo.getProfileId().substring(0,15) == Label.Profile_System_Admin ))
                {
                    a.AccountServicesChange__c = System.now();
                }
            }
        }
    }
    
    
    // Process Req Push on Account Merge for Losing Accounts
    // This job will blanket update all Reqs for which Account relationship was updated from losing
    // Account to the Winning Account. Trigger was neccesary since WF was not firing Req after Account merge
    public void AccountMerge_ReqPushToIR(Map<Id, Account>oldMap) {
        
        Set<Id> accIdSet = new Set<Id>();
        List<Account> TriggerOld = oldMap.values();
        for (Account acc : TriggerOld)
        {
            if(acc.recordtypeid == Label.Account_Client_RecordTypeID){ // added condition to fire this only Client Accounts - Nidish
                accIdSet.add(acc.Id);
            }
        }
        if(accIdset.size()>0){
            List<Id> ListOfModifiedReqs = new List<Id>();
            Reqs__c[] allModifiedReqs  = [select Id from reqs__c where Account__r.Id IN :accIdSet];
            
            for (Reqs__c req : allModifiedReqs) {
                ListOfModifiedReqs.add(req.Id);
            }
            if (ListOfModifiedReqs.size() > 0) {
                AsyncREQPushToIRClass.WS_AsyncREQPushToIR(ListOfModifiedReqs);
            }
        }
    }
    
    // Before insert and before update trigger to update Sum Score Card
    public void SumScoreCard_UpdateTrigger(List<Account> accounts){
        integer SumScoreCard = 0;
        for(Account a : accounts){
            if(a.recordtypeid == Label.Account_Client_RecordTypeID){ // added condition to fire this only Client Accounts - Nidish
                if(a.Staffing_Services_Models_Used__c <> NULL && a.Staffing_Services_Models_Used__c <>''){ SumScoreCard+=1;}
                if(a.Total_num_Contractors__c <> NULL && a.Total_num_Contractors__c > 0){ SumScoreCard+=1;}
                if(a.End_Customers__c <> NULL && a.End_Customers__c <>''){ SumScoreCard+=1;}
                if(a.VMS_Technology__c <> NULL && a.VMS_Technology__c <>''){ SumScoreCard+=1;}
                if(a.MSP_Provider__c <> NULL && a.MSP_Provider__c <>''){ SumScoreCard+=1;}
                if(a.Upcoming_Client_Projects__c<> NULL && a.Upcoming_Client_Projects__c<>''){ SumScoreCard+=1;}
                if(a.Top_Business_Initiatives__c <> NULL && a.Top_Business_Initiatives__c <>''){ SumScoreCard+=1;}
                if(a.Competitive_landscape__c <> NULL && a.Competitive_landscape__c <>''){ SumScoreCard+=1;}
                if(a.Client_Business_Pain__c <> NULL && a.Client_Business_Pain__c <>''){ SumScoreCard+=1;}
                if(a.Value_Proposition__c <> NULL && a.Value_Proposition__c <>''){ SumScoreCard+=1;}
                if(a.Annual_Staffing_Spend__c <> NULL && a.Annual_Staffing_Spend__c > 0){ SumScoreCard+=1;}
                if(a.Annual_Services_Budget__c <> NULL && a.Annual_Services_Budget__c >0){ SumScoreCard+=1;}
                if(a.Divisions__c <> NULL && a.Divisions__c <>''){ SumScoreCard+=1;}
                if(a.Product_Skill_Set_Details__c <> NULL && a.Product_Skill_Set_Details__c <>''){ SumScoreCard+=1;}
                a.Sum_Score_Card__c = SumScoreCard;
                SumScoreCard = 0;
            }
        }  
    }
    
    // Before insert to set Talent_Ownership__c based on current user role
    public void UpdateTalentOwnershipForRole (List<Account> accounts) {
        Map<String, String> roleOwnershipMap = new Map<String, String>();
        for (Talent_Role_to_Ownership_Mapping__mdt entry : [SELECT Opco_Code__c, Talent_Ownership__c from Talent_Role_to_Ownership_Mapping__mdt]) {
            roleOwnershipMap.put(entry.Opco_Code__c, entry.Talent_Ownership__c);
        }
        User currentUser = [SELECT Opco__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1][0];
        
        List<RecordType> rtypeList = [SELECT Id FROM RecordType WHERE Name IN ('Talent', 'Reference') AND SObjectType='Account'];
        for (Account a : accounts) {
            Boolean updateOwnership = false;
            for(RecordType rtype : rtypeList) {
                if (a.RecordTypeId == rtype.Id) {
                    updateOwnership = True; 
                    break;
                }
            }
            
            if (String.isBlank(a.Talent_Ownership__c) && updateOwnership && (!String.isBlank(roleOwnershipMap.get(currentUser.Opco__c)))) {
                a.Talent_Ownership__c = roleOwnershipMap.get(currentUser.Opco__c);
            } 
        }
    }
    

    /** Farid 10/11/2018
    **  This method handles populating Account Fields G2_Last_Modified_By__c and G2_Last_Modified_Date__c
    **  when there is change to the G2/Summary information by the users
    **  these two fields are used by SOA to determine whether they should overwrite the G2/Summary section of a talent 
    **/ 
   private static void stampTalentG2SummaryOnUpdate(Map<Id, Account>oldMap, Map<Id, Account>newMap) {

       if((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) || (UserInfo.getName() == ATS_DATS_MIGRATION)) {

            for(Account account :newmap.values()) {

                  if(account.recordtypeid == TALENT_RECORD_ID && UserInfo.getUserType() == 'Standard') {

                        boolean isG2Modified = false;

                        if(account.G2_Completed__c != oldmap.get(account.id).G2_Completed__c) {isG2Modified = true;}

                        else if(account.Skills__c    != oldmap.get(account.id).Skills__c    ) {isG2Modified = true;}
                        else if(account.Talent_Preference_Internal__c != oldmap.get(account.id).Talent_Preference_Internal__c) {isG2Modified = true;}

                        else if(account.Do_Not_Contact__c != oldmap.get(account.id).Do_Not_Contact__c) {isG2Modified = true;}

                        else if(account.Desired_Total_Compensation__c != oldmap.get(account.id).Desired_Total_Compensation__c) {isG2Modified = true;}
                        else if(account.Desired_Bonus_Percent__c != oldmap.get(account.id).Desired_Bonus_Percent__c) {isG2Modified = true;}
                        else if(account.Desired_Bonus__c != oldmap.get(account.id).Desired_Bonus__c) {isG2Modified = true;}
                        else if(account.Desired_Currency__c != oldmap.get(account.id).Desired_Currency__c) {isG2Modified = true;}
                        else if(account.Desired_Placement_type__c != oldmap.get(account.id).Desired_Placement_type__c) {isG2Modified = true;}
                        else if(account.Desired_Rate_Frequency__c != oldmap.get(account.id).Desired_Rate_Frequency__c) {isG2Modified = true;}
                        else if(account.Desired_Rate_Max__c != oldmap.get(account.id).Desired_Rate_Max__c) {isG2Modified = true;}
                        else if(account.Desired_Rate__c != oldmap.get(account.id).Desired_Rate__c) {isG2Modified = true;}
                        else if(account.Desired_Salary_Max__c != oldmap.get(account.id).Desired_Salary_Max__c) {isG2Modified = true;}
                        else if(account.Desired_Salary__c != oldmap.get(account.id).Desired_Salary__c) {isG2Modified = true;}
                        else if(account.Desired_Schedule__c != oldmap.get(account.id).Desired_Schedule__c) {isG2Modified = true;}
                        else if(account.Desired_Additional_Compensation__c  != oldmap.get(account.id).Desired_Additional_Compensation__c) {isG2Modified = true;}

                        else if(account.Goals_and_Interests__c != oldmap.get(account.id).Goals_and_Interests__c) {isG2Modified = true;}

                        else if(account.Talent_Security_Clearance_Type__c != oldmap.get(account.id).Talent_Security_Clearance_Type__c) {isG2Modified = true;}
                       else if(account.Talent_FED_Agency_CLR_Type__c != oldmap.get(account.id).Talent_FED_Agency_CLR_Type__c) {isG2Modified = true;}

                        else if(account.Talent_Overview__c != oldmap.get(account.id).Talent_Overview__c) {isG2Modified = true;}
                        else if(account.Skill_Comments__c != oldmap.get(account.id).Skill_Comments__c) {isG2Modified = true;}

                        else if(account.Willing_to_Relocate__c != oldmap.get(account.id).Willing_to_Relocate__c) {isG2Modified = true;}

                        else if(account.Talent_Race__c != oldmap.get(account.id).Talent_Race__c) {isG2Modified = true;}
                        else if(account.Talent_Gender__c != oldmap.get(account.id).Talent_Gender__c) {isG2Modified = true;}
                       

                        if(isG2Modified) {
                                account.G2_Last_Modified_By__c = UserInfo.getUserId();
                                account.G2_Last_Modified_Date__c = System.now();
                        }

                      
                 }
          } 
      }

   } 

    /** Farid 10/11/2018
    **  This method handles populating Account Fields G2_Last_Modified_By__c and G2_Last_Modified_Date__c
    **  when there is new Talent Account created
    **  these two fields are used by SOA to determine whether they should overwrite the G2/Summary section of a talent 
    **/ 
    private static void stampTalentG2SummaryOnInsert(List<Account> accounts) {

       if((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) || (UserInfo.getName() == ATS_DATS_MIGRATION)) {

            for(Account account :accounts) {

                  if(account.recordtypeid == TALENT_RECORD_ID) {  

                                account.G2_Last_Modified_By__c = UserInfo.getUserId();
                                account.G2_Last_Modified_Date__c = System.now();
                 }
          } 
      }
   }   
}