@isTest 
private class AccountTriggerHandlerTest {

	static testMethod void testName() {
	  
        user newUser;
		Profile userProfile = [select Name from Profile where Name = 'System Administrator'];
             
		long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
		Integer random = Math.Round(Math.Random() * timeDiff );
		newUser = new User(alias = 'cinte', 
						email='testuser@allegisgroup.com',
						emailencodingkey='UTF-8', 
						lastname= 'System Administrator User', 
						languagelocalekey='en_US',
						localesidkey='en_US', 
						profileid = userProfile.Id,
						timezonesidkey='America/Los_Angeles', 
						OPCO__c = 'ONS',
						Region__c= 'Aerotek MidAtlantic',
						Region_Code__c = 'test',
						Office_Code__c = '10001',
						Peoplesoft_Id__c = string.valueOf(random * 143),
						User_Application__c = 'CRM',
						CompanyName = 'Aerotek',
						username='testusr'+random+'@allegisgroup.com.dev');             
           
		system.runAs(newUser) { 
			Account accnt = TestData.newAccount(1);
			accnt.Name = 'testAccount';
			accnt.Talent_Committed_Flag__c = true;
			accnt.Source_System_Id__c = null;
			insert accnt; 
			System.assertEquals('1','1');
		}
	}

	//S-226639 - test method for 3 new opcos 
	static testMethod void testOpcoIND() {
	  
        user newUser;
		Profile userProfile = [select Name from Profile where Name = 'System Administrator'];
             
		long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
		Integer random = Math.Round(Math.Random() * timeDiff );
		newUser = new User(alias = 'cinte', 
						email='testuser@allegisgroup.com',
						emailencodingkey='UTF-8', 
						lastname= 'System Administrator User', 
						languagelocalekey='en_US',
						localesidkey='en_US', 
						profileid = userProfile.Id,
						timezonesidkey='America/Los_Angeles', 
						OPCO__c = 'IND',
						Region__c= 'Aerotek MidAtlantic',
						Region_Code__c = 'test',
						Office_Code__c = '10001',
						Peoplesoft_Id__c = string.valueOf(random * 143),
						User_Application__c = 'CRM',
						CompanyName = 'Aerotek',
						username='testusr'+random+'@allegisgroup.com.dev');             
           
		system.runAs(newUser) { 
			Account accnt = TestData.newAccount(1);
			accnt.Name = 'testAccount';
			accnt.Talent_Committed_Flag__c = true;
			accnt.Source_System_Id__c = null;
			insert accnt; 
			System.assertEquals('1','1');
		}
	}

	static testMethod void testOpcoSJA() {
	  
        user newUser;
		Profile userProfile = [select Name from Profile where Name = 'System Administrator'];
             
		long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
		Integer random = Math.Round(Math.Random() * timeDiff );
		newUser = new User(alias = 'cinte', 
						email='testuser@allegisgroup.com',
						emailencodingkey='UTF-8', 
						lastname= 'System Administrator User', 
						languagelocalekey='en_US',
						localesidkey='en_US', 
						profileid = userProfile.Id,
						timezonesidkey='America/Los_Angeles', 
						OPCO__c = 'SJA',
						Region__c= 'Aerotek MidAtlantic',
						Region_Code__c = 'test',
						Office_Code__c = '10001',
						Peoplesoft_Id__c = string.valueOf(random * 143),
						User_Application__c = 'CRM',
						CompanyName = 'Aerotek',
						username='testusr'+random+'@allegisgroup.com.dev');             
           
		system.runAs(newUser) { 
			Account accnt = TestData.newAccount(1);
			accnt.Name = 'testAccount';
			accnt.Talent_Committed_Flag__c = true;
			accnt.Source_System_Id__c = null;
			insert accnt; 
			System.assertEquals('1','1');
		}
	}

	static testMethod void testOpcoEAS() {
	  
        user newUser;
		Profile userProfile = [select Name from Profile where Name = 'System Administrator'];
             
		long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
		Integer random = Math.Round(Math.Random() * timeDiff );
		newUser = new User(alias = 'cinte', 
						email='testuser@allegisgroup.com',
						emailencodingkey='UTF-8', 
						lastname= 'System Administrator User', 
						languagelocalekey='en_US',
						localesidkey='en_US', 
						profileid = userProfile.Id,
						timezonesidkey='America/Los_Angeles', 
						OPCO__c = 'EAS',
						Region__c= 'Aerotek MidAtlantic',
						Region_Code__c = 'test',
						Office_Code__c = '10001',
						Peoplesoft_Id__c = string.valueOf(random * 143),
						User_Application__c = 'CRM',
						CompanyName = 'Aerotek',
						username='testusr'+random+'@allegisgroup.com.dev');             
           
		system.runAs(newUser) { 
			Account accnt = TestData.newAccount(1);
			accnt.Name = 'testAccount';
			accnt.Talent_Committed_Flag__c = true;
			accnt.Source_System_Id__c = null;
			insert accnt; 
			System.assertEquals('1','1');
		}
	}
}