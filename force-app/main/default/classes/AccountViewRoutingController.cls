public class AccountViewRoutingController{
    private static Map<Id,String> accountRecordTypeNameCache = null;
    
    public AccountViewRoutingController(ApexPages.StandardController controller) {
        this.controller = controller;
    }

    public PageReference getRedir() {
        Account c = [Select id, recordtypeid,Talent_Ownership__c  From Account Where Id = :ApexPages.currentPage().getParameters().get('id')];
        PageReference newPage;
        if(c.recordtypeid == System.Label.CRM_TALENTACCOUNTRECTYPE && !c.Talent_Ownership__c.contains('Community')) {
              newPage = Page.CandidateSummary;
              newPage.getParameters().put('id', c.id);
        }else if(c.recordtypeid == System.Label.CRM_REFERENCEACCOUNTRECTYPE) {
           List<Talent_Recommendation__c> recs = [select Talent_Contact__r.AccountId from Talent_Recommendation__c where Recommendation_From__r.AccountId = :c.id LIMIT 1];
              if(recs.size() > 0){
                     newPage = Page.CandidateSummary;
                     newPage.getParameters().put('id', recs[0].Talent_Contact__r.AccountId);
              }else{
                   newPage = null;
              }
              
        }else
        {
             newPage = null;
        }
        return newPage != null ? newPage.setRedirect(true): null;
    }
    
    @AuraEnabled
    public static Id pageRedirect(String AccId){
        List<Contact> con = [Select Id from contact where AccountId =: Id.valueOf(AccId) ] ;
        
        if(con.size() == 1){
           return con[0].Id; 
        }
        
     return null;   
    }

    @AuraEnabled
    public static Id communitiesPageRedirect(String AccId){
        try {
            List<Account> accPSId = [Select Peoplesoft_ID__c from Account where Id =: Id.valueOf(AccId) ] ;
            if(accPSId.size() == 1){
                
                //Adding case for Community EMEA Talent, which starts with E. - Alex Thomas
                String psId = '';
                if (accPSId[0].Peoplesoft_ID__c.startsWith('E.')){
                    psId = accPSId[0].Peoplesoft_ID__c.substring(2);
                }
                else {
                    psId = 'R.' + accPSId[0].Peoplesoft_ID__c;
                }

                List<Account> acc = [Select Id from Account where Peoplesoft_ID__c =: psId];
                if(acc.size() == 1){
                    List<Contact> con = [Select Id from contact where AccountId =: acc[0].Id ] ;
                    if(con.size() == 1){
                      return con[0].Id; 
                    }
                }
            }
        } catch (Exception e) {
            System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
            System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
            System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
            System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
            System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
        }      
     return null;   
    }
    
    private final ApexPages.StandardController controller;
}