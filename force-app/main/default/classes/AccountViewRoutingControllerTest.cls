@isTest
Public class AccountViewRoutingControllerTest{
 @isTest static void test_RedirectionForTalentRecType(){
    Profile p = [select id from profile where name='System Administrator']; 
    //User as System Admin
    User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
     System.runAs(u){
           Account a =  new Account(RecordTypeId = System.Label.CRM_TALENTACCOUNTRECTYPE,
                                    Name = 'bwiTestAccount' + String.Valueof(Math.random()),
                                    Talent_Ownership__c = 'RPO');
              
           Test.startTest();   
             Insert a; 
           Test.stopTest();  
            
            PageReference pref =  Page.CandidateSummary;
            pref.getParameters().put('id', a.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController acctCtrl = new ApexPages.StandardController(a);
            AccountViewRoutingController ctrl = new AccountViewRoutingController(acctCtrl);
            
            PageReference pageRef = ctrl.getRedir();
            System.assertNotEquals(null, pageRef);
               }
            }
            
    @isTest static void test_RedirectionForClientRecType(){
       Profile p = [select id from profile where name='System Administrator']; 
       //User as System Admin
       User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
      System.runAs(u){
           List<RecordType> recTypes = [select Id from recordtype where name ='Client' and SobjectType = 'Account' LIMIT 1];
           Account c =  new Account(RecordTypeId = recTypes[0].Id,
                                    Name = 'TestAccount' + String.Valueof(Math.random()));
              
           Test.startTest();   
             Insert c; 
           Test.stopTest();  
            
            PageReference pref =  Page.CandidateSummary;
            pref.getParameters().put('id', c.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController acctCtrl = new ApexPages.StandardController(c);
            AccountViewRoutingController ctrl = new AccountViewRoutingController(acctCtrl);
            
            PageReference pageRef = ctrl.getRedir();
            System.assertEquals(null, pageRef);
               }
    } 
    @isTest static void test_RedirectionForReferenceRecType(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
       Profile p = [select id from profile where name='System Administrator']; 
       //User as System Admin
       User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
      System.runAs(u){
           Account t =  new Account(RecordTypeId = System.Label.CRM_REFERENCEACCOUNTRECTYPE,
                                    Name = 'TestAccount' + String.Valueof(Math.random()));
           Test.startTest();   
             Insert t;

             List<RecordType> recTypes = [select Id from recordtype where name ='Reference' and SobjectType = 'Contact' LIMIT 1];
             Contact c = new Contact(RecordTypeId = recTypes[0].Id, LastName = 'test', Phone='999-999-9999',
                                           AccountId = t.Id  );  

                insert c;
             
             Talent_Recommendation__c talent = new Talent_Recommendation__c();
             talent.Recommendation_From__c = c.Id;
             talent.Talent_Contact__c = c.Id;
             Insert talent;
           Test.stopTest();  
            
            PageReference prf =  Page.CandidateSummary;
            prf.getParameters().put('id', t.Id);
            Test.setCurrentPage(prf);
            ApexPages.StandardController acctCtrl = new ApexPages.StandardController(t);
            AccountViewRoutingController ctrl = new AccountViewRoutingController(acctCtrl);
            
            PageReference pageRf = ctrl.getRedir();
            System.assertNotEquals(null, pageRf);
          }
    }     
}