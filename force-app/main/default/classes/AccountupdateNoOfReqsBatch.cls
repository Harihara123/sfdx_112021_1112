global class AccountupdateNoOfReqsBatch implements Database.Batchable<sObject>  {

    global String query;
    
    
    //Constructor
    global AccountupdateNoOfReqsBatch (String s)  
    {  
         //Constructing dynamic query         
        query = s;
        
    }  

    // Batch class start method
    global Iterable<sObject> start(Database.BatchableContext BC)  
    {  
        return Database.getQueryLocator(query);
    }  

    // Batch Class execute method
    global void execute(Database.BatchableContext BC,List<Account> batch)  
    {   
        
        List<Account> toBeUpdated = new List<Account>();
             
        for(Account a : batch)
        {
            integer i = 0;
            integer counter = 0; 
            for(Reqs__c r : a.reqs__r)
            {                        
                i = i+ integer.valueof(r.Open_Positions__c);
                counter++;
            }
            a.No_of_Open_Positions__c = i;
            a.No_of_Reqs__c = counter;
            toBeUpdated.add(a);
        }  
        Database.update(toBeUpdated,false);
    }
    
    //Batch Class finish method
    global void finish(Database.BatchableContext BC)  
    {  
    } 
    
}