/***************************************************************************
Name        : Acct_Cont_Assoc_TriggerHandler
Created By  : JFreese (Appirio)
Date        : 19 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : Class that contains all of the functionality called by the
              Acct_Cont_Assoc_Trigger. All contexts should be in this class.
*****************************************************************************/

public with sharing class Acct_Cont_Assoc_TriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    Map<Id, Account_Contact_Association__c> oldMap = new Map<Id, Account_Contact_Association__c>();

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Account_Contact_Association__c> newRecs) {
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, Account_Contact_Association__c>newMap) {
        AccountContactAssociation_UpdateAssociatedContact(afterInsert, oldMap, newMap);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Account_Contact_Association__c>oldMap,
                               Map<Id, Account_Contact_Association__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Account_Contact_Association__c>oldMap,
                               Map<Id, Account_Contact_Association__c>newMap) {
        AccountContactAssociation_UpdateAssociatedContact(afterUpdate, oldMap, newMap);
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Account_Contact_Association__c>oldMap) {
        AccountContactAssociation_PreventDeleteOfPrimaryAccountAssociation(oldMap);
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Account_Contact_Association__c>oldMap) {

    }

    /************************************************************************************************************
    Original Header: trigger AccountContactAssociation_UpdateAssociatedContact on Account_Contact_Association__c (after insert,after update)
    Apex Trigger Name: AccountContactAssociation_UpdateAssociatedContact
    Created Date     : 05/14/2012
    Function         : Trigger to update the asscoiated contact accordingly.

    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------
    * Pavan               05/14/2012                     Created
    ***************************************************************************************************************/
    public void AccountContactAssociation_UpdateAssociatedContact (String Context,
                                                                   Map<Id, Account_Contact_Association__c>oldMap,
                                                                   Map<Id, Account_Contact_Association__c>newMap) {

        string FORMER = 'Formerly With';

        // Skip when the current user profile is 'System Integration'
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) {
        List<Log__c> errorLogs = new List<Log__c>();
        set<string> associatedContacts = new set<string>();
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Account_Contact_Association__c> associationRecordsToUpdate = new List<Account_Contact_Association__c>();

        //loop over the contact association records and check if there are any marked as primary
        List<Account_Contact_Association__c> TriggerNew = newMap.values();
        for(Account_Contact_Association__c rec : TriggerNew)
        {
            boolean addToList = true;
            if(!Context.equals(afterInsert))
            {
                addToList = false;
                if(rec.Is_Primary_Account__c && oldMap.get(rec.Id).Is_Primary_Account__c != rec.Is_Primary_Account__c)
                     addToList = true;
            }
            else
            {
                if(!rec.Is_Primary_Account__c)
                    addToList = false;
            }
            if(addToList)
                 associatedContacts.add(rec.Contact__c);
        }

        //query the contact and its associated AccountContact Association records
        if(associatedContacts.size() > 0)
        {
            for(Contact con : [select AccountId,IsAllegisPlacedContact__c,(select Is_Primary_Account__c,Status__c,Contact_Moved_date__c, IsAllegisPlacedClientContact__c from Past_Contacts_Accounts__r order by LastModifiedDate desc) from Contact where id in :associatedContacts])
            {
                boolean isPrimary = false;
                // Loop over the child records
                for(Account_Contact_Association__c recs : con.Past_Contacts_Accounts__r)
                {
                    boolean isCurrentRecord = false;
                    if(newMap.get(recs.Id) != Null) //current record
                    {
                        con.AccountId = newMap.get(recs.Id).Account__c;
                        if(newMap.get(recs.Id).Is_Primary_Account__c)
                            isPrimary = true;
                        isCurrentRecord = true;
                    }
                    if(isPrimary)
                    {
                        if(!isCurrentRecord)
                        {
                            // change the status only when the triggering point is contact
                            if(TriggerStopper.triggeredFromContact)
                                recs.Status__c = FORMER;
                            recs.Is_Primary_Account__c = false;
                            if(recs.Contact_Moved_date__c == null)
                                recs.Contact_Moved_date__c = system.now();      //Assign the date moved to present time
                            associationRecordsToUpdate.add(recs);
                        }
                    }

                }
                
                con.IsPlacedFromSkuid__c = false;
                contactsToUpdate.add(con);
            }
        }
        system.debug('associationRecordsToUpdate:'+associationRecordsToUpdate);
        //update the contact records accordingly
        if(contactsToUpdate.size() > 0)
        {
            TriggerStopper.isContactUpdated = true;
            for(database.saveResult result : database.update(contactsToUpdate,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        }
        //update the records accordingly
        if(associationRecordsToUpdate.size() > 0)
        {
            for(database.saveResult result : database.update(associationRecordsToUpdate,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        }

        // insert error logs
        if(errorLogs.size() > 0)
            database.insert(errorLogs,false);
        }
    }

    /************************************************************************************************************
    Original Header: AccountContactAssociation_PreventDeleteOfPrimaryAccountAssociation on Account_Contact_Association__c (before delete)
    Apex Trigger Name: AccountContactAssociation_PreventDeleteOfPrimaryAccountAssociation
    Created Date     : 06/25/2012
    Function         : Trigger to prevent user from deleting primary account contact association record.

    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------
    * Pavan                   06/25/2012                 Created
    ***************************************************************************************************************/
    public void AccountContactAssociation_PreventDeleteOfPrimaryAccountAssociation (Map<Id, Account_Contact_Association__c>oldMap) {
         // Skip when the current user profile is 'System Integration'
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) {
            //loop over the records
            List<Account_Contact_Association__c> TriggerOld = oldMap.values();
            for(Account_Contact_Association__c associationRecord : TriggerOld)
            {
               //prevent those records that are marked primary from deletion
               if(associationRecord.Is_Primary_Account__c && !associationRecord.Merging_contact__c)                   
                   oldMap.get(associationRecord.Id).addError(Label.AccountContactAssociation_DeleteError);
            }
        }
    }
}