public with sharing class ActController 
{
    private ApexPages.StandardSetController standardController;

    public ActController(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
    }

    public PageReference MassAccounts()
    {     

integer countsuc =0;
        integer countfail =0;
        
        
        // Get the selected records (optional, you can use getSelected to obtain ID's and do your own SOQL)
        List<Account> selectedCases = (List<Account>) standardController.getSelected();
        List<Target_Account__c> Taglst=new List<Target_Account__c>();
        List<Id> listOfIds = new List<Id>();
        for(Account acct:selectedCases){
          Target_Account__c tac=new Target_Account__c();
            tac.Account__c=acct.id;
            tac.ownerid=UserInfo.getUserId();
            tac.user__c=UserInfo.getUserId();
            tac.role__c='Team Member';
            Taglst.add(tac);
        }
        Database.SaveResult[] srList = Database.insert(Taglst, false);
               for (Database.SaveResult sr : srList) {
                   
                   
    if (sr.isSuccess()) {
        
        countsuc=countsuc+1;
                        
                
    }
               
        else{
            countfail=countfail+1;
            
        }
               }
        
        
             system.debug(srlist);   
        
        if(countSuc>0) {
         ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'You have been succesfully added to the  selected '+CountSuc+ ' Accounts.'));
            
            }
        if(countfail>0){
         ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'You are already a member of the selected '+Countfail+ ' Accounts.'));  
            
        }
        
        if(countSuc==0 && countfail==0 )
        
        {
         ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'Please  select atleast one Account!'));   
        }
            
                        
       
                
            
               // Update records       
       // PageReference p;
        //p=new PageReference(Addtoaccountteam);
        PageReference pageRef = new PageReference('/apex/accountstest');

        return null;        
    }
    
    
    
    
    
    public PageReference Targetaccounts(){
        
        List<Account> selectedAccounts = (List<Account>) standardController.getSelected();
        List<Target_Account__c> Taglst=new List<Target_Account__c>();
        List<Target_Account__c> targetActs = new List<Target_Account__c>();
        List<Target_Account__c>  targetActsupdate = new List<Target_Account__c>();
        set<Id> SetOfIds = new Set<Id>();
        set<string> uniquekey = new set<string>();  
        integer TotalCount= selectedAccounts.size();
        string acct1;
        string userid;
        
        for(Account acct:selectedAccounts){
            SetOfIds.add(acct.id);
            acct1 = acct.id;
            userid= UserInfo.getUserId();
            uniquekey.add(acct1.substring(0,15)+'-'+userid.substring(0,15));
            
            
            system.debug('uniquekey'+uniquekey);
            
            
                    }
        
        targetActs =[select id,Account__c,Ownerid,User__c,role__c,Target__c from Target_Account__c  where Unique_Identifier__c in :uniquekey and Target__c=false];
        system.debug('target'+targetActs);
        
        
        if(targetActs.size()>0){
        for(Target_Account__c Tag :targetActs){
            Tag.target__c=True;
            targetActsupdate.add(Tag);
            
            
        }
        }
        if(targetActsupdate.size()>0){
        Database.SaveResult[] srListupdated = Database.update(targetActsupdate,false);
        //update targetActsupdate;
        }
        
        for(Account acct:selectedAccounts){
          Target_Account__c tac=new Target_Account__c();
            tac.Account__c=acct.id;
            tac.ownerid=UserInfo.getUserId();
            tac.user__c=UserInfo.getUserId();
            tac.role__c='Team Member';
            tac.Target__c=true;
            Taglst.add(tac);
        
        }
        
        Database.SaveResult[] srList = Database.insert(Taglst,false);
        
        if(TotalCount>0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,TotalCount+' Accounts have been Targeted Successfully'));
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'Please select at least one Account to Target!'));
        }
        
        return null;
    }
    
    
    public PageReference Deleteaccountsmsg(){
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'Are you sure you want to delete your self from the selected Accounts!'));
        
        return null;
    }
    
    
    
    
    
    public PageReference Deleteaccounts(){
        
        
        List<Account> selectedAccounts = (List<Account>) standardController.getSelected();
        List<Target_Account__c> targetActs = new List<Target_Account__c>();
        set<string> uniquekey = new set<string>();
        integer TotalCount= selectedAccounts.size();
        string acct1;
        string userid;
        
        for(Account acct:selectedAccounts){
            
            acct1 = acct.id;
            userid= UserInfo.getUserId();
            uniquekey.add(acct1.substring(0,15)+'-'+userid.substring(0,15));
            
            
            system.debug('uniquekey'+uniquekey);
            
            
                    }
        
        targetActs =[select id,Account__c,Ownerid,User__c,role__c,Target__c from Target_Account__c  where Unique_Identifier__c in :uniquekey ];
        
        if(TargetActs.size()>0){
            
        
        Database.DeleteResult[] srList = Database.Delete(TargetActs,false);
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'You have been Successfully removed from the selected Accounts!'));
            
        }
        
        if(TotalCount>0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'You have been Successfully removed from the selected Accounts!'));
            
        }
        
        if(TotalCount==0){
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Info,'Please select at least one Account!'));
            
        }
        
        PageReference cancel = standardController.cancel();
        
        return cancel;
        
    }
    
    
    

    public PageReference cancel()
    {       
        // Call StandardSetController 'save' method to update (optional, you can use your own DML)
        PageReference cancel = standardController.cancel();
        standardController.save();
        return cancel;
          
    }
}