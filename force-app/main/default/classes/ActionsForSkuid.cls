public class ActionsForSkuid {
    @invocablemethod(label = 'Skuid Exposed API')
    public static List < String > invokeApexAction(List<String> request) {
        System.debug(request[0]); // current Status
        System.debug(request[1]); // Opco
        System.debug(request[2]); // ATS_Job__c
        List<String> lst = new List<String>();
        
        if(request.size() > 2){
        
          if(request[2] == 'undefined'){
            Map<Id,Submittal_Status_Matrix__mdt> subStatuses = new Map<Id,Submittal_Status_Matrix__mdt>([SELECT Id,Label,OpCo__c,Business_Unit__c,Req_Product__c, 
                                                                                                       Current_Status__c,Available_Status__c 
                                                                                                       FROM Submittal_Status_Matrix__mdt 
                                                                                                       Where Current_Status__c =: request[0] and OpCo__c =: request[1]]);
            String Soql = '';
            for(Submittal_Status_Matrix__mdt key : subStatuses.values())
             {
                Soql += key.Available_Status__c;
             } 

            if(Soql.length() > 0){
              String formattedStr = Soql.substring(1,Soql.length()-1);
              List<String> lstStatuses = formattedStr.split(',');
              for(String s : lstStatuses){
                    lst.add(s);          
              }
            }
          }else{
            // Code to Handle ATS Jobs
            Map<Id,Submittal_Status_Matrix__mdt> subStatuses = new Map<Id,Submittal_Status_Matrix__mdt>([SELECT Id,Label,OpCo__c,Business_Unit__c,Req_Product__c, 
                                                                                                       Current_Status__c,Available_Status__c 
                                                                                                       FROM Submittal_Status_Matrix__mdt 
                                                                                                       Where Current_Status__c =: request[0] and OpCo__c = 'Allegis Global Solutions, Inc.']);
            String Soql = '';
            for(Submittal_Status_Matrix__mdt key : subStatuses.values())
             {
                Soql += key.Available_Status__c;
             } 

            if(Soql.length() > 0){
              String formattedStr = Soql.substring(1,Soql.length()-1);
              List<String> lstStatuses = formattedStr.split(',');
              for(String s : lstStatuses){
                    lst.add(s);          
              }
            }
          
          }
        }
        return lst;
    }
}