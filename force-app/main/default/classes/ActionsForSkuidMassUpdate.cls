/**
1. Get Submittal Ids from Squid
2. Query for related OpCo's and selected submittal statuses
3. Add selected submittal statuses to avstatuses
4. Assign strings to map, count the occurences and reorder the list
5. Pass back the list to squid
**/

public class ActionsForSkuidMassUpdate {
  @invocablemethod(label = 'Skuid Exposed API')
  public static List < String > invokeApexAction(List<String> request) {

    List<String> lst = new List<String>();
    List<String> retStatus = new List<String>();
    List<String> selStatuses = new List<String>();
    List<String> avStatuses = new List<String>();
    Set<String> opco = new Set<String>();
    List<String> opcoList = new List<String>();
    List<Submittal_Status_Matrix__mdt> avList = new List<Submittal_Status_Matrix__mdt>();
    
    if(request.size() > 0){
      
      //Bring out the list of orders
      List<Order> selOrders = new List<Order>([SELECT Opportunity.OpCo__c, ATS_Job__c, Status FROM Order WHERE Id IN:request]);
      
      System.Debug('Request'+request);
      
      //Add the list of selected values to a list and non-duplicate opco's to a set
      for(Order o:selOrders){
        selStatuses.add(o.Status);
          if(o.ATS_Job__c != null){
              opco.add('Allegis Global Solutions, Inc.');
          }
          else{
         	opco.add(o.Opportunity.OpCo__c);
          }
      }
      System.Debug('Selected Status'+selstatuses);
      System.Debug('OpCo '+opco);
        
      opcoList.addall(opco);
      
      for(Integer op=0;op<opcoList.size(); op++){
            avList.addAll([SELECT Available_Status__c FROM Submittal_Status_Matrix__mdt WHERE OpCo__c=:opcoList[op] AND Current_Status__c IN:selStatuses]);
        }
        
      System.Debug('List size for matrix -'+avList.size());
      
      String Soql = '';
      for(Submittal_Status_Matrix__mdt key : avList){
        Soql += key.Available_Status__c;
      } 

      if(Soql.length() > 0){
        String formattedStr = Soql.substring(1,Soql.length()-1);
        String removeMid = formattedStr.replace('""',',');
        avStatuses = removeMid.split(',');
      }
      
      /**TEST to check all available statuses
        for(Integer t=0;t<avStatuses.size();t++){
            System.Debug(t+' - '+avStatuses[t]);
        }
      **/
      
      System.Debug('Available Statuses '+avStatuses);
      
      Map<String, Integer> takeCount = new Map<String, Integer>();

      for(String val: avStatuses){
        if(!takeCount.containsKey(val)){
          takeCount.put(val,0);
        }
        
        Integer currentVal = takeCount.get(val) +1;
        takeCount.put(val,currentVal);
      }

      System.Debug('Total Count : '+takeCount);

      String master = [SELECT Available_Status__c FROM Submittal_Status_Matrix__mdt WHERE DeveloperName='MASTER'].Available_Status__c;
      System.Debug('MDM values '+master);

        //  String master = 'Applicant,Linked,Pursing,LPQ in Process,Screening,Allegis Interviewing,Submitted,Interview Requested,Interviewing,On Hold,Networking,Reference,Anonymous Promo,Carve-Out,Offer Extended,Offer Accepted,Started,Not Proceeding';
		// Master List contains all values in the subsequent order
		// Master List excludes exception values which aren't valid as an available status
		// exceptionList{'Linked','Interviewing','Offer Extended','Offer Accepted','Started'};
      List<String> masterList = master.split(',');
      for(Integer i=0; i<masterList.size(); i++){
        if(takeCount.get(masterList[i])==avList.size()){
         retStatus.add(masterList[i]);
       }
     }
       
       System.Debug(retStatus);
     
   }
   return retStatus;
 }
}