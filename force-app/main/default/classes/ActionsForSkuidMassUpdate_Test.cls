@isTest
public class ActionsForSkuidMassUpdate_Test {
static User user = TestDataHelper.createUser('system administrator');
    
    static testMethod void verifyMassUpdate(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
System.runAs(user) {
        List<Opportunity> lstOpportunity = new List<Opportunity>(); 
		TestData TdAcc = new TestData(1);
        testdata tdcont = new Testdata();
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

        for(Account Acc : TdAcc.createAccounts()) {                     
            Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly');  

			// Load opportunity data
            lstOpportunity.add(NewOpportunity);
        }                
		// Insert list of Opportunity
        insert lstOpportunity;
        string rectype='Talent';
        List<Contact> TalentcontLst=tdcont.createContacts(rectype);

        string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
		
        Order firstorder = new Order(Name = 'New Order', EffectiveDate = system.today()+1,
            Status = 'Linked',AccountId = lstOpportunity[0].AccountId, OpportunityId = lstOpportunity[0].Id,shiptocontactid=TalentcontLst[0].id,recordtypeid = OrderRecordType);
        insert firstorder;   
      
		
       /* Order secondorder = new Order(Name = 'New Order1', EffectiveDate = system.today()+1,
            Status = 'Submitted',AccountId = lstOpportunity[0].AccountId, OpportunityId = lstOpportunity[0].Id,shiptocontactid=TalentcontLst[0].id,recordtypeid = OrderRecordType);
        insert secondorder;*/
		
   
		//ActionsForSkuidMassUpdate massup= new  ActionsForSkuidMassUpdate();

        List<String> req = new List<String>();
        req.add(firstorder.Id);
        /*req.add(secondorder.Id);
        req.add(orderToInsert[0].Id);
        req.add(orderToInsert[1].Id);*/
        System.Debug(LoggingLevel.ERROR,'First Order ' +firstorder);
        System.Debug(LoggingLevel.ERROR,'Reqx' +req);
        
        List<Submittal_Status_Matrix__mdt> avList = new List<Submittal_Status_Matrix__mdt>([SELECT DeveloperName, Current_Status__c, Available_Status__c, OpCo__c FROM
                                            Submittal_Status_Matrix__mdt WHERE Current_Status__c='Submitted']);

        Test.startTest();
        ActionsForSkuidMassUpdate.invokeApexAction(req);
        Test.stopTest();
        
        /**
		List<String> onereq = new List<String>();
        onereq.add('Submitted');
        onereq.add('Aerotek, Inc.');
        onereq.add('undefined');
        onereq.add('test');
        ActionsForSkuid.invokeApexAction(onereq);
        **/
    }
    }

}