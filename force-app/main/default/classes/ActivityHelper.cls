public class ActivityHelper{
    
    final static string WHAT_ID = 'WhatId';
    final static string MASTER_GLOBAL_ACCOUNT = 'Master_Account_Id__c';
    final static string OWNER_OFFICE = 'Owner_s_Office_Name__c';
    final static string OWNER_PROFILE = 'Owners_Profile__c';
    final static string OWNER_REGION = 'Owner_s_Region__c';
    final static string CONTACT_TITLE = 'Related_Contact_s_Title__c';
    final static string WHO_ID = 'whoId';
    //final static set<string> ContRecSet =new set<string>{'012U0000000DWoyIAG','012U0000000DWoxIAG','012U0000000DWozIAG'};
   final static set<string> ContRecSet =new set<string>{'Client','CSA','Recruiter'};
   final static string Actrcdtypeid = string.valueof(label.Account_Client_RecordTypeID);
    
    public static map<id,contact> updateLastActivity(set<string> contactAssociations,map<id,contact> contmap)
    {
        // collection to store the exceptions
        List<Log__c> errorLogs = new List<Log__c>();
        List<event> lastevt =new List<event>();
        // Loadthe list of contact which needs to be update witht the last event date
        List<Contact> contacts = new List<contact>();
        Map<Id, Datetime> recentCustomActivityDates = new Map<Id, Datetime>();
 
        DateTime currentTime = system.now();

        // test add comment by Farid 
        
        // query for contacts and the associated events for newContactAssociations
        // Hareesh-Added extra record type condition as part of communities org merge
       // for(contact con : [select id,Title,Last_Event_OpCo__c,Last_Event__c,Total_Meals__c,Total_Meetings__c,AccountId,Account.Master_Global_Account_Id__c ,(select EndDateTime from Events where EndDateTime <= :currentTime order by EndDateTime desc limit 1) from contact where recordtype.developername IN :ContRecSet AND id in :contactAssociations limit :Limits.getLimitQueryRows()])
        lastevt=[select id, EndDateTime,whoid from Event where EndDateTime <= :currentTime and whoid in:contmap.keyset() order by EndDateTime desc Limit 1];
        for(contact con:contmap.values()) {
            for(event evt:lastevt){
                if(evt.whoid==con.id){
               //assign the last event date
               date lastEvent;
                    
                         
                  lastEvent = Date.newinstance(evt.EndDateTime.year(),evt.EndDateTime.month(),evt.EndDateTime.day());
                     con.Last_Event__c = lastEvent;
               }
            }
              
               contacts.add(con);
            contmap.put(con.id,con);
            }
            
            
            
            //Logic to populate the custom last activit date
            for(contact con:[SELECT Id, Custom_Last_Activity_date__c,
                                 (SELECT createddate FROM Tasks WHERE Type!='Attempted Contact' ORDER BY createddate DESC LIMIT 1) 
                                , (SELECT createddate FROM Events  ORDER BY createddate DESC LIMIT 1)
                                FROM contact WHERE Id IN: contmap.keyset()])  {
              DateTime taskDateTime, eventDateTime;
               if(!con.Tasks.isEmpty()) {
                taskDateTime = con.Tasks[0].createddate; 
                }
                
                 if(!con.Events.isEmpty()) {
                eventDateTime = con.Events[0].createddate;
            }   
            
            if(taskDateTime == null && eventDateTime != null) {
                recentCustomActivityDates.put(con.Id, eventDateTime);
            }
            else if(taskDateTime != null && eventDateTime == null) {
                recentCustomActivityDates.put(con.Id, taskDateTime);
            }
            else if(taskDateTime != null && eventDateTime != null) {
                recentCustomActivityDates.put(con.Id, eventDateTime > taskDateTime ? eventDateTime : taskDateTime);
            }
            
            }
            
            for(Contact cont:contmap.values()){
            if(recentCustomActivityDates.size()>0 && recentCustomActivityDates.containskey(cont.id)){
           datetime latestdate= recentCustomActivityDates.get(cont.id);
           date lastactivity= Date.newinstance(latestdate.year(),latestdate.month(),latestdate.day());
            cont.Custom_Last_Activity_date__c=lastactivity;
            
            contmap.put(cont.id,cont);
            }
                           
              }                  
        
        return contmap;
        //if(contacts.size() > 0)
           // database.update(contacts,false);
        
    }
    /**********************************************************************
    Method for Target Accounts
    ************************************************************************/
    public static void associateToTargetAccount(Map<string,List<SObject>> accountIds,Map<string,List<SObject>> ownerMap)
    {
           string TARGET_ACCOUNT_ACTIVITY = 'Target_Account_Activity__c';
           string MASTER_ACCOUNT_ID = 'Master_Account_Id__c';
           // get the account and target account information
           // Hareesh-Added extra record type condition to filter out community Accounts
           for(Account acc : [select Master_Global_Account_Id__c,(select Account__c, User__c from Target_Accounts__r where Target__c = TRUE) from Account where recordtypeid= :Actrcdtypeid AND id in :accountIds.keyset()]) // Replaced ownerid with User__c and added a filter target__c = TRUE - Nidish
           {
                  // populate the master global account Id
                  for(Sobject activity : accountIds.get(acc.Id))
                  {
                       activity.put(MASTER_ACCOUNT_ID,acc.Master_Global_Account_Id__c);
                  } 
                  // get the target accounts related to associated to accounts
                  // Skip when the current user profile is 'System Integration'
                  if(UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration)
                  {
                      for(Target_Account__c targetAccount : acc.Target_Accounts__r)
                      {
                           // check if the targetAccount owner is assigned a Event
                           if(ownerMap.get(targetAccount.User__c) != Null) // Replaced owner with User__c - Nidish
                           {
                                  // loop over the activities and assign the appropriate field values
                                  for(SObject activity : ownerMap.get(targetAccount.User__c)) // Replaced owner with User__c - Nidish
                                  {
                                      activity.put(TARGET_ACCOUNT_ACTIVITY,Label.Target_Account_Activity);
                                  }
                           } 
                      }
                  }
           }
    }
    
    
     /**********************************************************************
    Method for updating contacts's primary account on task/event
    ************************************************************************/
    public static void associateToPrimaryAccount(Map<id,List<Sobject>> associatedContacts)
    {
        //Hareesh-Added recordtype condition to bypass communities,ATS
        for(Contact con : [Select AccountId,Account.Master_Global_Account_Id__c from Contact where recordtype.developername IN :ContRecSet and id in :associatedContacts.keyset() limit :Limits.getLimitQueryRows()])
        {
            // process the associated activities accordingly
            for(SObject obj : associatedContacts.get(con.Id))
            {
               obj.put(WHAT_ID,con.AccountId);
               //assign the master global account id
               obj.put(MASTER_GLOBAL_ACCOUNT,con.Account.Master_Global_Account_Id__c);
            }
        }
    }
    /**************************************************************************************************
     Method to populate the owner's office and profile name
     ****************************************************************************************************/  
     public static void assignOwnerOffice(Map<string,List<SObject>> associatedProfiles)
     {
           for(User usr : [select Profile.Name,Office__c,Region__c from user where id in :associatedProfiles.keyset() limit :Limits.getLimitQueryRows()])
           {
                   //process the events accordingly
                   for(SObject Obj : associatedProfiles.get(string.valueOf(usr.Id)))
                   {
                         obj.put(OWNER_PROFILE,usr.Profile.Name); 
                         obj.put(OWNER_OFFICE,usr.Office__c);
                         obj.put(OWNER_REGION,usr.Region__c);
                   }
           }
     } 
   /****************************************************************************************
   Method to get the assocaited activities for a contact. This update can be a future method
   as the field is used only for reporting purpose
   ****************************************************************************************/  
   @future
   public static void updateAssociatedActivities(set<string> contactIds)
   {
       List<sObject> activity = new List<sObject>();
       //query the associated Events and tasks of a contact
       //Hareesh-Added Record type condition to bypass communities and ATS
       for(Contact con : [SELECT Title,
                          (SELECT Related_Contact_s_Title__c from Tasks),
                          (SELECT Related_Contact_s_Title__c from Events)
                          FROM Contact
                          WHERE recordtype.developername IN :ContRecSet and id in :contactIds
                          LIMIT :Limits.getLimitQueryRows()])
       {
           //assign the values accordingly by looping through the activities
           for(Task tsk : con.Tasks)
              activity.add(processActivity(tsk,con.Title));
           
           //loop over Events
           for(Event evnt : con.Events)
              activity.add(processActivity(evnt,con.Title));
       }
       if(activity.size() > 0)
           database.update(activity,false);                  
   } 
    /******************************************************************************
    utility method
    *******************************************************************************/
    private static SObject processActivity(SObject activity,string title)
    {
           SObject obj = activity;
           obj.put(CONTACT_TITLE,title);
           return obj;
    }
    /*******************************************************************************
    Method to assign the related contact title
    ********************************************************************************/
    public static void assignTitleToActivities(Map<id,List<SObject>> activitiesMap)
    {
         //get the associated Contact title
         //Hareesh-Added record type condition to by pass communities and ATS
         for(Contact con : [SELECT Title
                            FROM Contact
                            WHERE recordtype.developername IN :ContRecSet AND id in :activitiesMap.keyset()
                            Limit :Limits.getLimitQueryRows()])
          {
              //process the related activities
              for(SObject obj : activitiesMap.get(con.Id))
                 obj.put(CONTACT_TITLE,con.Title);
          }                  
    }
    /****************************************************************
    Method to populate the contacts and associated activities
    *****************************************************************/
    public static Map<id,List<SObject>> processAssociatedContactMap(Map<id,List<SObject>>activityMap, SObject activity)
    {
        
        if(activityMap.get(string.valueOf(activity.get(WHO_ID))) != Null)
           activityMap.get(string.valueOf(activity.get(WHO_ID))).add(activity);
        else
        {
             List<SObject> temp = new List<SObject>();
             temp.add(activity);
             activityMap.put(string.valueOf(activity.get(WHO_ID)),temp);
        }
        return activityMap;
    }

    public static void updateLastServiceDateOnContact(List<SObject> accountWrappers, Map<Id, Contact> contMap) {
        //used in events
        Map<Id, Contact> lastServiceDateOnContacts = getLastServiceDateOnContacts(accountWrappers, contMap);
        for(Id contactId : contMap.keySet()) {
            if(lastServiceDateOnContacts.get(contactId) != null) {
                contMap.get(contactId).Last_Service_Date__c = lastServiceDateOnContacts.get(contactId).Last_Service_Date__c;
            }
        }
    }

    public static void updateLastServiceDateOnContact(List<SObject> accountWrappers) {
        //used in tasks
        Map<Id, Contact> lastServiceDateOnContacts = getLastServiceDateOnContacts(accountWrappers, null);
        if(lastServiceDateOnContacts.values().size() > 0) {
            Database.update(lastServiceDateOnContacts.values(), false);
        }
    }

    public static Map<Id, Contact> getLastServiceDateOnContacts(List<SObject> accountWrappers, Map<Id, Contact> contMap) {
        //base work
        Map<Id, Contact> lastServiceDateOnContacts = new Map<Id, Contact>();
        
        Map<Id, Contact> accountIdToContacts = getAccountIdToContacts(accountWrappers, contMap);
        Map<Id, Datetime> recentServiceActivityDateTimes = ServiceTouchpointController.getRecentServiceActivityDateTimes(accountWrappers);
        if(accountWrappers != null) {
            for(SObject accountWrapper : accountWrappers) {
                Id talentAccountId = getAccountIdFromRecord(accountWrapper);
                Contact talentContact = accountIdToContacts.get(talentAccountId);
                if(talentContact != null) {
                Datetime mostRecentActivityDatetime = recentServiceActivityDateTimes.get(talentAccountId);
                if(mostRecentActivityDatetime != null){
                    if(mostRecentActivityDatetime.date() != talentContact.Last_Service_Date__c) {  
                        lastServiceDateOnContacts.put(talentContact.Id, new Contact(Id = talentContact.Id, Last_Service_Date__c = mostRecentActivityDatetime.date()));
                    }
                }
                else {
                    //last activity has been deleted and last service date is not null
                        if(talentContact.Last_Service_Date__c != null) {
                            lastServiceDateOnContacts.put(talentContact.Id, new Contact(Id = talentContact.Id, Last_Service_Date__c = null));
                        }
                    }
                }
            }
        }
        return lastServiceDateOnContacts;
    }

    private static Id getAccountIdFromRecord(SObject record) {
        if(record.getSObjectType().getDescribe().getName() == 'Contact') {
            return (Id)record.get('AccountId');
        } else {
            return (Id)record.get('WhatId');
        }
    }

    @TestVisible
    private static Map<Id, Contact> getAccountIdToContacts(List<SObject> accountWrappers, Map<Id, Contact> contMap) {

        Boolean listContainsActivities = false;
        Set<Id> accountIds = new Set<Id>();
        Map<Id, Contact> accountIdtoContacts = new Map<Id, Contact>();

        for(SObject accountWrapper : accountWrappers) {

            if(accountWrapper.getSObjectType().getDescribe().getName() == 'Contact') {
                accountIdtoContacts.put((Id)accountWrapper.get('AccountId'), (Contact) accountWrapper);
            } 
            //It's a task, check for whoId and whatId
            else { 
                Id wrapperWhatId = (Id)accountWrapper.get('WhatId');
                Id wrapperWhoId = (Id)accountWrapper.get('WhoId');     
                
                if((wrapperWhatId != null && wrapperWhatId.getSobjectType() == Schema.Account.SObjectType) ||
                    (wrapperWhoId != null && wrapperWhoId.getSObjectType() == Schema.Contact.SObjectType)) {
                        if(contMap != null) {
                        String key = (Id)accountWrapper.get('WhoId');
                        Contact value = contMap.get(key);
                        if(value != null) {
                            accountIdtoContacts.put(wrapperWhatId, value);
                        }
                    }
                    else if(wrapperWhatId != null && wrapperWhatId.getSobjectType() == Schema.Account.SObjectType){
                        listContainsActivities = true;
                        accountIds.add(wrapperWhatId);
                    }
                }
            }
        }

        if(listContainsActivities) {
        Id contactTalentRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
            for(Contact con : [SELECT Id, Last_Service_Date__c, AccountId FROM Contact WHERE AccountId IN: accountIds AND recordTypeId =: contactTalentRT]) {
                accountIdtoContacts.put(con.AccountId, con);
            }
        }

        return accountIdtoContacts;
    }

    // Create Task for given Cases
    @future
    public static void createTasks(String oldCasesString, String newCasesString, Boolean isAfterInsert){
		
        System.debug('caseObjects');
        
        System.debug(oldCasesString + '\n\n '+ newCasesString + '\n\n '+isAfterInsert);
        
         //Id talentRT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Talent').getRecordTypeId();
         //Id fsgRT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
        Id talentRT = [SELECT Id, Name, DeveloperName, sObjectType FROM RecordType WHERE DeveloperName = 'Talent' AND SobjectType = 'Case' LIMIT 1].Id;
        Id fsgRT = [SELECT Id, Name, DeveloperName, sObjectType FROM RecordType WHERE DeveloperName = 'FSG' AND SobjectType = 'Case' LIMIT 1].Id;

        try{
            List<Case> oldCases = null;
            if(!isAfterInsert){
                oldCases = (List<Case>)Json.deserialize(oldCasesString,List<Case>.class);
            }
            List<Case> newCases = (List<Case>)Json.deserialize(newCasesString,List<Case>.class);
           
            List<String> caseIds = new List<String>();
            List<Case> caseObjects = new List<Case>();
            if(isAfterInsert){
                for(Integer counter = 0; counter < newCases.size(); counter++){
                    Case newCase = newCases[counter];
                    if(newCase.RecordTypeId == talentRT && newCase.Create_Activity__c && (newCase.Status == 'Closed')) {                      
                        caseIds.add(newCase.id);                                             
                    }
                }
            }else{
                for(Integer counter = 0; counter < oldCases.size(); counter++){
                    Case oldCase = oldCases[counter];
                    Case newCase = newCases[counter];
                    if(oldCase.RecordTypeId == talentRT && newCase.Create_Activity__c && (newCase.Status == 'Closed')) {
                        caseIds.add(newCase.id);
                    }
                }
            }
            System.debug('caseObjects');
            System.debug(caseObjects);
            if(caseIds.size()>0){
              	caseObjects = [Select id, ContactId, Description, CaseNumber, Type, Status, (Select Id, CommentBody From CaseComments order by CreatedDate DESC) From Case where id in : caseIds];
                List<Task> tasks = new List<Task>();
                for(Case caseObj : caseObjects){
                    try{
                        tasks.add(ActivityHelper.mapCaseToTask(caseObj));
                    }catch(Exception ex){
                        System.debug('createTasks on Case ex -' + ex.getMessage());
                    }
                }
    
                if(tasks.size() > 0){
                    insert tasks;
                }  
            }
			
        }catch(Exception ex){System.debug('createTasks ex -' + ex.getMessage());}
    }
    public static Task mapCaseToTask(Case caseObject){
                    
        Task taskObj = new Task();
        taskObj.WhatId = caseObject.Id;
        taskObj.WhoId = caseObject.ContactId;
        taskObj.Status = 'Completed';
        taskObj.ActivityDate = Date.today();
        taskObj.Type = 'Call';
        String taskDesc = caseObject.Description;
        taskDesc = (taskDesc != null ? ('\n'+taskDesc) : '');
        System.debug('CaseComments');
		System.debug(caseObject.CaseComments);
        
        for(CaseComment caseComm : caseObject.CaseComments){
            if(caseComm.CommentBody != null && caseComm.CommentBody.length() > 0){
                taskDesc = taskDesc + '\n------------------------\n'  +  caseComm.CommentBody;
            }
        }
        String subj = 'Case#'+caseObject.CaseNumber + '-' + caseObject.Type + '-' + caseObject.Status;
            
        taskObj.Subject = subj;
        taskObj.Description = taskDesc;

        System.debug(taskObj);
        return taskObj;

    }
}