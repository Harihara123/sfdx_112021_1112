@isTest
public class ActivityHelperTest  {
    @TestSetup
    static void setup() {
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;
        talentAcc.G2_Completed__c = true;
        insert talentAcc;

        Contact ct = TestData.newContact(talentAcc.id, 1, 'Talent'); 
        ct.Email = 'test@allegisgroup.com';
        ct.Title = 'test title';
        ct.Talent_State_Text__c = 'test text';
        ct.MailingState = 'test text';
        insert ct;
    }

    @IsTest
    static void updateLastServiceDateOnContact_givenActivities_updateContact() {
        User systemIntegrationUser = TestDataHelper.createUser('System Integration');
        User singleDeskUser = TestDataHelper.createUser('Single Desk 1');
        Contact testContact = [SELECT Id, AccountId FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Task oldTask = new Task();
        
        System.runAs(systemIntegrationUser) {
            oldTask.Type = 'Service Touchpoint';
            oldTask.WhatId = testContact.AccountId;
            oldTask.Completed__c = System.Now().addDays(-1);
            oldTask.Completed_Flag__c = true;
            insert oldTask;
        }

        List<Task> taskTriggerContext = new List<Task>{oldTask};
        ActivityHelper.updateLastServiceDateOnContact(taskTriggerContext); 

        Task newTask = new Task();
        newTask.Type = 'Service Touchpoint';
        newTask.WhatId = testContact.AccountId;
        newTask.Completed_Flag__c = true;
        newTask.Completed__c = System.Now();

        List<Task> newTaskTriggerContext = new List<Task>{newTask};
        System.runAs(singleDeskUser) {
            Test.startTest();
                ActivityHelper.updateLastServiceDateOnContact(newTaskTriggerContext);      
            Test.stopTest();
        }
        
    }

    @IsTest
    static void testCreateTasks_Afterinsert() {
        User user = TestDataHelper.createUser('System Integration');
        System.runAs(user) {
            Account talAcc = CreateTalentTestData.createTalent();
            // create a talent contact
            Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
            //Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Talent').getRecordTypeId();
            Id recTypeId = [SELECT Id, Name, DeveloperName, sObjectType FROM RecordType WHERE DeveloperName = 'Talent' AND SobjectType = 'Case' LIMIT 1].Id;
            // Create a case
            Case caseObj = new Case();
            caseObj.AccountId = talAcc.Id;
            caseObj.ContactId = talCont.Id;
            caseObj.Description = 'Unit Test Case';
            caseObj.Type = 'Communities';
            caseObj.RecordTypeId = recTypeId;
            insert caseObj;
            // Update a case
            caseObj.Status = 'Closed';
            upsert caseObj;
        }
    }
}