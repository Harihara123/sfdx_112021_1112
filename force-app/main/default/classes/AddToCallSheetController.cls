public class AddToCallSheetController {
    
     @AuraEnabled
    public static CallSheet initialize(String recIdList){
        
        System.debug('Record id list--->'+recIdList);
        
       List<String> lstAlpha=new List<String>();
        if(!String.isBlank(recIdList)){
            String str=recIdList.removeStart('[');
            System.debug('removed str is-1-->'+str);
            str=str.removeEnd(']');
            System.debug('removed str is--2->'+str);
        	lstAlpha = str.split(',');
        }
        
        Account accounts=[select id,name from Account where id in (Select accountId from Contact where id  in:lstAlpha) limit 1];
        
        System.debug('Retrieved List=-->'+accounts);
    	
    	CallSheet model=new CallSheet();
        model.taskObj=new Task();
        model.contactIdList=lstAlpha;
        
        if(accounts!=null ){
            model.taskObj.Subject=accounts.name;
            model.accountId=accounts.id;
        }
    	
    	
    	Map<string,string> typeMap = new Map<string,string>();
        Schema.DescribeFieldResult field2 = Task.Type.getDescribe();
    	 List<Schema.PicklistEntry> typeList = field2.getPicklistValues();
        for (Schema.PicklistEntry types: typeList) {
            typeMap.put(types.getValue(), types.getLabel());
        }
        model.typeMapping = typeMap;
        
        Map<string,string> statusMap = new Map<string,string>();
        Schema.DescribeFieldResult field3 = Task.Status.getDescribe();
    	List<Schema.PicklistEntry> statusList = field3.getPicklistValues();
        for (Schema.PicklistEntry status: statusList) {
            statusMap.put(status.getValue(), status.getLabel());
        }
        model.statusMapping = statusMap;
        
        
        Map<string,string> priorityMap = new Map<string,string>();
        Schema.DescribeFieldResult field4 = Task.priority.getDescribe();
    	List<Schema.PicklistEntry> priorityList = field4.getPicklistValues();
        for (Schema.PicklistEntry priority: priorityList) {
            priorityMap.put(priority.getValue(), priority.getLabel());
        }
        model.priorityMapping = priorityMap;
	    
        
        model.taskObj.ActivityDate=Date.Today();
        model.taskObj.type='Call';
        model.taskObj.Status='Not Started';
        model.taskObj.Priority='Normal';
        
    	return model;
    }
    
	   @AuraEnabled
    public static void saveCandidateToCallSheet(String recordIdCSV,String tasks){
    	
        System.debug('Task save method called--1--'+recordIdCSV);
        System.debug('Task save method called--2--'+tasks);
    	
    	try{
	      //  recordIdCSV = recordIdCSV.removeEnd(',');
	        List<Task> newTasks = new List<Task>();
        
        
        	 List<String> lstAlpha=new List<String>();
             List<ID> lstAlpha1=new List<ID>();
       		 if(!String.isBlank(recordIdCSV)){
            	String str=recordIdCSV.removeStart('[');
            	System.debug('removed str is-1-->'+str);
            	str=str.removeEnd(']');
            	System.debug('removed str is--2->'+str);
        		lstAlpha = str.split(',');
        	 }
            
            System.debug('Size of array is--->'+lstAlpha.size());
            String finalStr='';
            for(String str:lstAlpha){
                String gb=str.trim();
                lstAlpha1.add(ID.ValueOf(gb));
                
            }
	        
            String query1='select Id, AccountId, Email, Other_Email__c, Preferred_Email__c, HomePhone, MobilePhone, Phone from Contact where id in :lstAlpha1';
            
            List<Contact> recordList = Database.query(query1);
        
	        Task tsk=null;
	        if(String.isNotBlank(tasks)){
				  tsk = (Task)JSON.deserialize(tasks,Task.class);
                System.debug('Task save method called--3--'+tsk);
	        }
       		
        	
	        
	        //Default Values
	        string currentUser = UserInfo.getUserId();
	        
	        Date dueDate = Date.today();
	        string comments = '';
	        
	        for (Contact record : recordList) {
	            Task newTask = new Task();
	            newTask.OwnerId = currentUser;
	            newTask.Description = comments;
	            newTask.CreatedById = currentUser;
	            newTask.ActivityDate = tsk.ActivityDate;
	            newTask.WhoId = record.Id;
	            newTask.Priority = tsk.priority;
	            newTask.WhatId = record.AccountId;
	            newTask.Status = tsk.status;
	            newTask.Subject = tsk.subject;
                newTask.TransactionID__c = tsk.TransactionID__c;
                newTask.RequestID__c = tsk.RequestID__c;
	            newTask.type=tsk.type;
	            newTasks.add(newTask);
	        } 

	       	Database.SaveResult[]  svr=Database.insert(newTasks,false);
            for (Database.SaveResult sr : svr) {
                    if (!sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields that affected this error: ' + err.getFields());
                        }
                    }
			}
            
	     	newTasks=null;  	
    	}catch(Exception e){
    		
    		// throw new AuraHandledException('Were sorry, Please Contact System Administrator: ' + e.getMessage() + ''+ e.getCause());
       
                System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
                System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
                System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
                System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
                System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
    		
    	}
       // System.debug('Task id is ####--->'+newTasks.get(0).id);
        
    }
	   
     public class CallSheet{
    	@AuraEnabled
    	public Task taskObj{get;set;}
    	@AuraEnabled
    	public Map<String, String> typeMapping {get;set;}
    	@AuraEnabled 
    	public Map<String, String> statusMapping{get;set;}
    	@AuraEnabled
    	public Map<String, String> priorityMapping{get;set;}    
        @AuraEnabled
        public List<String> contactIdList{get;set;}
        @AuraEnabled
        public String accountId{get;set;}
    	
    }

}