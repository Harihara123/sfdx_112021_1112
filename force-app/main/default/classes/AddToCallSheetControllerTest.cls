@isTest
public class AddToCallSheetControllerTest {
    
    @isTest
	static void testInitialize() {
		Test.startTest();
        Account talentAccount = CreateTalentTestData.createTalentAccount();
		Contact talentContact = CreateTalentTestData.createTalentContact(talentAccount);
		String str='['+talentContact.id+']';
		AddToCallSheetController.CallSheet results = AddToCallSheetController.initialize(str);
		Test.stopTest();
		System.assert(results.taskObj != null, 'taskObj is not empty');
		System.assert(results.taskObj.ActivityDate == Date.today(), 'default date should be today');
		System.assert(results.taskObj.Type == 'Call', 'default type should be Call');
 		System.assert(results.taskObj.Status == 'Not Started', 'default status should be Not Started');
		System.assert(results.taskObj.Priority == 'Normal', 'default priority should be Normal');
	}

	@isTest
	static void testSaveCandidateToCallSheet() {
        Test.startTest();
		Account talentAccount = CreateTalentTestData.createTalentAccount();
		Contact talentContact = CreateTalentTestData.createTalentContact(talentAccount);
		Date newActivityDate = Date.today().addDays(7);
		//comma removal is accounted for in function
		    
        String testRecordIdCSV='['+talentContact.Id+']';

		//JSON
		Task updatedTask = createTask();
		//this value should be ignored, dueDate is set in the method
		updatedTask.activityDate = newActivityDate;
		String testTasks = JSON.serialize(createTask());

		
		AddToCallSheetController.saveCandidateToCallSheet(testRecordIdCSV, testTasks);
        Task results = [SELECT Id, ActivityDate FROM Task LIMIT 1];
		Test.stopTest();
		//should only be one task
		
		System.assert(results.activityDate == Date.today(), 'new activity date should not have been saved, method sets duedate');
	}	

	public static Task createTask() {
		Task newTask = new Task(
			ActivityDate = Date.today()
			, Type = 'Call'
			, STatus = 'Not Started'
			, Priority = 'Normal'
            , TransactionID__c = 'b0db184b-b713-6d15-c683-2926edff2dc7'
            ,Subject='Test Subject'
			);
		return newTask;
	}

}