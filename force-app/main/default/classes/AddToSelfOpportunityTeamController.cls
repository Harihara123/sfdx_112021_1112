public without sharing class AddToSelfOpportunityTeamController {


@AuraEnabled
 public static OppTeamModel addSelfToOppTeam(String recIdList){
    
    OppTeamModel oppTeamModel = new OppTeamModel();
    System.debug('Record id list--->'+recIdList);
        
       List<String> lstOpps=new List<String>();
        if(!String.isBlank(recIdList)){
            String str=recIdList.removeStart('[');
            System.debug('removed str is-1-->'+str);
            str=str.removeEnd(']');
            System.debug('removed str is--2->'+str);
          lstOpps = str.split(',');
       }else{
         oppTeamModel.isMultipleSelection = false;
         oppTeamModel.isMemberAlreadyAdded = false;
         oppTeamModel.isNoSelection = true;
       }
       
       if(lstOpps.size() > 1){
         oppTeamModel.isMultipleSelection = true;
         oppTeamModel.isMemberAlreadyAdded = false;
         oppTeamModel.isNoSelection = false;
       }else if(lstOpps.size() > 0){
         
        List<OpportunityTeamMember> oTmList = [select Id, OpportunityId from OpportunityTeamMember where UserId =: UserInfo.getUserId() and OpportunityId =: lstOpps[0]];
        if(oTmList.size() > 0){
           oppTeamModel.isMemberAlreadyAdded = true;
           oppTeamModel.isMultipleSelection = false;
           oppTeamModel.isNoSelection = false;
           }else if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration){
            
             OpportunityTeamMember  OppTeamMember = new OpportunityTeamMember(userid=UserInfo.getUserId(),teammemberrole='Recruiter',opportunityid=lstOpps[0],OpportunityAccessLevel ='Edit');
              insert OppTeamMember;
              oppTeamModel.isMemberAlreadyAdded = false;
              oppTeamModel.isMultipleSelection = false;
              oppTeamModel.isNoSelection = false;
              oppTeamModel.recId = OppTeamMember.Id;
              List<Opportunity> opps = [select Id, Name, owner.Name, owner.email from Opportunity where Id =: lstOpps[0]];
              
             if(opps.size() > 0){
              System.debug('opps found'+opps[0].owner.email);
               List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
              //Send Email to Owner
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               List<String> sendTo = new List<String>();
               sendTo.add(opps[0].owner.email);
               mail.setToAddresses(sendTo);
    
              // Set who the email is sent from
              mail.setReplyTo('noreply@salesforce.com');
              mail.setSenderDisplayName('Allegis Group Salesforce Support');
              mail.setUseSignature(false);
    
              // Set email contents
              mail.setSubject('New Opportunity Team Member');
              String body = '<html>' +  'Dear ' + opps[0].Owner.Name + ', ' + '<br />';
              body += '<br />';
              body += 'The following user has added himself/herself as a Team Member of your Opportunity:' + '<br />';
              body += 'User: '+ UserInfo.getName() + '<br />';
              body += 'Opportunity: ';
              body +=  '<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+opps[0].id+'">'+opps[0].Name+'</a>' +'<br />';
              body += '<br />';
              body += '<p></p>';
              body += 'This is an auto generated email – please do not reply to it.';
              body +=  '</body></html>';
         
              mail.setHtmlBody(body);
              mails.add(mail);
              Messaging.sendEmail(mails);
           }
         
          }
       }
      return oppTeamModel;
 }

public class OppTeamModel{
     @AuraEnabled public Boolean isMemberAlreadyAdded{get;set;}
     @AuraEnabled public Boolean isMultipleSelection{get;set;}
     @AuraEnabled public Boolean isNoSelection{get;set;}
     @AuraEnabled public String recId {get;set;}
     
}

}