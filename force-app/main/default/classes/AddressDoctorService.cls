/*
  @author : Neel Kamal
  @date  : 01/08/2021
  @desc : This service class will be used to validate the address(city, state and country) by using callout to USPS API provided by Informatica Team.
  Valid status : 'V1, V2, V3, V4, C1, C2, C3, C4, I2, I3, I4'  

Code			Description
V4		The input data is correct. Address validation checked all postally relevant elements, and inputs matched perfectly.
V3		The input data is correct, but some or all elements were standardized. The input may contain outdated name data.
V2		The input data is correct, but some elements could not be verified because of incomplete reference data.
V1		The input data is correct, but user standardization has negatively impacted deliverability.
C4		Corrected. All postally relevant elements have been checked.
C3		Corrected. Some elements could not be checked.
C2		Corrected, but the delivery status is unclear due to absent reference data.
C1		Corrected, but the delivery status is unclear because user standardization has introduced errors.
I4		Data could not be corrected completely, but is very likely to be deliverable. There is a single match with an address in the reference data.
I3		Data could not be corrected completely, but is very likely to be deliverable. There are multiple matches with addresses in the reference data.
I2		Data could not be corrected. The address may be deliverable.
I1		Data could not be corrected and the address is not likely to be delivered.
*/
public without sharing class AddressDoctorService {
	
	
	public static String isAddressValid(String city, String State, String Country) {
	
	String nmdtvalue=getAddDctConfig();
	System.debug(nmdtvalue);
		Address_Doctor__mdt addDr = [Select Switch__c, Valid_Status__c, Timeout__c,UserName__c,Password__c from Address_Doctor__mdt where DeveloperName = :nmdtvalue WITH SECURITY_ENFORCED];

		String status = 'SWITCH_OFF';
		if (addDr.Switch__c) {
			MuleSoftOAuth_Connector.OAuthWrapper oAuthWrapper = MuleSoftOAuth_Connector.getAccessToken('AddressDr');
			Http connection = new Http();
			HttpRequest req = new HttpRequest();
			HttpResponse resp;

			req.setEndpoint(oAuthWrapper.endPointURL);
			req.setMethod('POST');
			req.setHeader('Content-Type', 'application/xml');
			req.setHeader('Authorization', 'Bearer ' + oAuthWrapper.token);
			//req.setHeader('Authorization', 'Basic YWRzdGFuZGFyZDpBZCR0QG5kYXJkRGV2');
			req.setBody(getBody(addDr.UserName__c,addDr.Password__c,city, state, country));
			req.setTimeout(Integer.valueOf(addDr.Timeout__c));

			try {
				resp = connection.send(req);

				if (resp.getStatusCode() == 200 && resp.getBody() != null) {
					status = 'INVALID';
					String body = null;
					if (Test.isRunningTest()) {
						body = '<tns:StatusInfoMatchCode>C4</tns:StatusInfoMatchCode>';
					}
					else {
						body = resp.getBody();
					}
					System.debug(body);
					String respStatus = body.substringBetween('<tns:StatusInfoMatchCode>', '</tns:StatusInfoMatchCode>');
					if (addDr.Valid_Status__c.contains(respStatus)) {
						status = 'VALID';
					}
				}

			} catch(CalloutException exp) {
				ConnectedLog.LogException('AddressDoctorService', 'AddressDoctorServce', 'isAddressValid', exp);
				String logInfo = 'Request :: ' + req.getBody();
				if (resp != null) {
					logInfo += ' ::Response:: ' + resp.getBody();
				}
				ConnectedLog.LogInformation('AddressDoctorService', 'AddressDoctorServce', 'isAddressValid', logInfo);
				status = 'INVALID';
			}
		}
		return status;
	}

	/**
    * @description Address Doctor Configuration values.
    * @param None 
    * @return String value indicating the Name of Sandbox
	* ;
    */     
    public static String getAddDctConfig(){
	String mdtvalue=null;
		String orgEnv=Utilities.getSFEnv();
		if (orgEnv.containsIgnoreCase('prod'))
		{
			mdtvalue ='JPD_Address_Dr_Prod';
		}
		else if(orgEnv.containsIgnoreCase('test'))
		{
			mdtvalue ='JPD_Address_Dr_Test';
		}
		else if(orgEnv.containsIgnoreCase('load')){
			mdtvalue ='JPD_Address_Dr_Load';
		}
		else if(orgEnv.containsIgnoreCase('dev')){
				mdtvalue ='JPD_Address_Dr_Test';
		}
		else {
				mdtvalue ='JPD_Address_Dr_Test';
		}
			return mdtvalue;
    }

	private static String getBody(String userName,String password,String city, String state, String country) {
		String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://www.informatica.com/dis/ws/"> ' +
		'<soapenv:Header>' +
		 '<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
		' <wsse:UsernameToken wsu:Id="UsernameToken-1">'+
		'<wsse:Username>' + userName + '</wsse:Username>'+
		' <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' + password + '</wsse:Password>'+
		'<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">V0VCU0VSVklDRV9VU0VSOldFQlNFUlZJQ0VAVFNU</wsse:Nonce>'+
		'<wsu:Created>2013-05-29T00:45:19.136Z</wsu:Created>'+
		'</wsse:UsernameToken>'+
		'</wsse:Security>'+
		'</soapenv:Header>'+
		'<soapenv:Body>' +
		'<ws:mplt_Standardize_US_CityState_Operation>' +
		'<!--Zero or more repetitions:-->' +
		'<ws:Group>' +
		'<!--Optional:-->' +

		'<!--Optional:-->' +
		'<ws:City>' + city + '</ws:City>' +
		'<ws:State>' + state + '</ws:State>' +
		'<!--Optional:-->' +
		'<ws:Country>' + country + '</ws:Country>' +
		'</ws:Group>' +
		'</ws:mplt_Standardize_US_CityState_Operation>' +
		'</soapenv:Body>' +
		'</soapenv:Envelope>';

		return body;
	}
}