@isTest
public class AddressDoctorService_Test {
	@isTest
	static void isValidAddressTest(){
	Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Failed Application',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                         	Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
		

		
		Mulesoft_OAuth_Settings__c adressDrService = new Mulesoft_OAuth_Settings__c(name ='AddressDr',
                                                                            Endpoint_URL__c='https://allegismulesoftdevtest.allegistest.com/allegis-dev-adstandardizewrapper/DataIntegrationService/WebService/ws_Standardize_US_CityState/',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                         	Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://login.microsoftonline.com/allegiscloud.onmicrosoft.com/oauth2/token'
                                                                        );
        insert adressDrService;

		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new MulesoftOAuthMockResponse());	
 			MuleSoftOAuth_Connector.OAuthWrapper oauthWrap = MuleSoftOAuth_Connector.getAccessToken(serviceSettings.Name);
        	system.debug(oauthWrap);
			

			/*String reponseXML = '<tns:StatusInfoMatchCode>C4</tns:StatusInfoMatchCode>';
			String contentType = 'application/XML';
			Integer statusCode = 200;
			calloutMockResponse mkResponse = new calloutMockResponse(reponseXML, contentType, statusCode);
			Test.setMock(HttpCalloutMock.class, mkResponse);*/
			String status = AddressDoctorService.isAddressValid('Ellicott City', 'MD', 'US');
			System.assertEquals('VALID', status);
		Test.stopTest();
	}
	

}