public without sharing class AddsSelfToOpportunityTeam{

@AuraEnabled
 public static teamModel createOpportunityTeamMember(String recordId){
  teamModel tM = new teamModel();
  List<OpportunityTeamMember> oTmList = [select Id, OpportunityId from OpportunityTeamMember where UserId =: UserInfo.getUserId() and OpportunityId =: recordId];
        if(oTmList.size() > 0){
           tm.recId = recordId;
           tm.isMemberAlreadyAdded = true;
         }else if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration){
            
           OpportunityTeamMember  OppTeamMember = new OpportunityTeamMember(userid=UserInfo.getUserId(),teammemberrole='Recruiter',opportunityid=recordId,OpportunityAccessLevel ='Edit');
           insert OppTeamMember;
           
           tm.recId = recordId;
           tm.isMemberAlreadyAdded = false;
           
           List<Opportunity> opps = [select Id, Name, owner.Name, RecordType.Name, owner.email from Opportunity where Id =: recordId];
           
           if(opps.size() > 0){
              System.debug('opps found'+opps[0].owner.email);
               List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
              //Send Email to Owner
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               List<String> sendTo = new List<String>();
               sendTo.add(opps[0].owner.email);
               mail.setToAddresses(sendTo);
    
              // Set who the email is sent from
              mail.setReplyTo('noreply@salesforce.com');
              mail.setSenderDisplayName('Allegis Group Salesforce Support');
              mail.setUseSignature(false);
    
              // Set email contents
              mail.setSubject('New Opportunity Team Member');
              String body = '<html>' +  'Dear ' + opps[0].Owner.Name + ', ' + '<br />';
              body += '<br />';
              body += 'The following user has added himself/herself as a Team Member of your Opportunity:' + '<br />';
              body += 'User: '+ UserInfo.getName() + '<br />';
              body += 'Opportunity Record Type: '+ opps[0].RecordType.Name+ '<br />';
              body += 'Opportunity Link: ';
              body +=  '<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+opps[0].id+'">'+opps[0].Name+'</a>' +'<br />';
              body += '<br />';
              body += '<p></p>';
              body += 'This is an auto generated email – please do not reply to it.';
              body +=  '</body></html>';
         
              mail.setHtmlBody(body);
              mails.add(mail);
              Messaging.sendEmail(mails);
           }
            
         }
       return tM;
}

    @AuraEnabled
    public static teamModel insertOpportunityTeamMember(String recordId, String OpportunityAccessLevel, String TeamMemberRole){
        teamModel tM = new teamModel();
              
        OpportunityTeamMember  OppTeamMember = new OpportunityTeamMember(
            userid=UserInfo.getUserId(),
            teammemberrole=TeamMemberRole,
            opportunityid=recordId,
            OpportunityAccessLevel=OpportunityAccessLevel,
            Source__c='Opportunity'
        );

        insert OppTeamMember;
           
           tm.recId = recordId;
           tm.isMemberAlreadyAdded = false;
           
           List<Opportunity> opps = [select Id, Name, owner.Name, RecordType.Name, owner.email from Opportunity where Id =: recordId];
           
           if(opps.size() > 0){
              System.debug('opps found'+opps[0].owner.email);
               List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
              //Send Email to Owner
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               List<String> sendTo = new List<String>();
               sendTo.add(opps[0].owner.email);
               mail.setToAddresses(sendTo);
    
              // Set who the email is sent from
              mail.setReplyTo('noreply@salesforce.com');
              mail.setSenderDisplayName('Allegis Group Salesforce Support');
              mail.setUseSignature(false);
    
              // Set email contents
              mail.setSubject('New Opportunity Team Member');
              String body = '<html>' +  'Dear ' + opps[0].Owner.Name + ', ' + '<br />';
              body += '<br />';
              body += 'The following user has added himself/herself as a Team Member of your Opportunity:' + '<br />';
              body += 'User: '+ UserInfo.getName() + '<br />';
              body += 'Opportunity Record Type: '+ opps[0].RecordType.Name+ '<br />';
              body += 'Opportunity Link: ';
              body +=  '<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+opps[0].id+'">'+opps[0].Name+'</a>' +'<br />';
              body += '<br />';
              body += '<p></p>';
              body += 'This is an auto generated email – please do not reply to it.';
              body +=  '</body></html>';
         
              mail.setHtmlBody(body);
              mails.add(mail);
              Messaging.sendEmail(mails);
           }
         
       return tM;
    }
    @AuraEnabled
     public static Opportunity getOpportunityRecord(String recordId){
         Opportunity o = [Select id, Name, recordtypeId, recordtype.Name from Opportunity where id =: recordId];
         system.debug('#####YTS'+o.recordtype.Name);
         return o;
    }
    
    // Return User along with Office Code and CompanyName
    @AuraEnabled
    public static User fetchUser(){
        
       /* System.debug('Inside FetchUser NewOppList_Controller');
        Id userId = UserInfo.getUserId();
        User_Organization__c office =null;
        List<User> usrList = [SELECT OPCO__c,Office_Code__c,CompanyName FROM User WHERE Id = :userId LIMIT 1];
        
        User usr=usrList[0];
        
        System.debug('usrList----->'+usrList.size());
        if(usrList.size() > 0 ){
         
         System.debug('usrList----->'+usrList[0].Office_Code__c);    
         
        /* List<Talent_Role_to_Ownership_Mapping__mdt> OwnershipList = [SELECT Opco_Code__c,Talent_Ownership__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: usrList[0].OPCO__c LIMIT 1];
         String ownershipOpco =OwnershipList.size() > 0 ?  OwnershipList[0].Talent_Ownership__c: '';
         String opco = usrList[0].CompanyName;*/
         /*  if(!String.IsBlank(usrList[0].Office_Code__c)){
              List<User_Organization__c> orgList = [select Id, Name from User_Organization__c where Office_Code__c =: usrList[0].Office_Code__c and OpCo_Code__c  =: usrList[0].OPCO__c LIMIT 1];
              
               office = orgList.size() > 0 ? orgList[0]: new User_Organization__c();
               usr.Office_Code__c=office.id;
           }
        }  */
      user usr =  NewOppList_Controller.fetchUser();
          System.debug('USer data '+ usr);
        return usr;        
    }
    
    
    public class teamModel{
        @AuraEnabled public Boolean isMemberAlreadyAdded{get;set;}
        @AuraEnabled public String recId {get;set;}
    }
	
	@AuraEnabled
	public static Map<String,Integer> saveAllocatedRecruiter(Object RecruiterData, String OppId) {        
        try {
            List<OpportunityTeamMember> OppTeamMemberList=new List<OpportunityTeamMember>();
            Map<Id,String> existingUserIds=new Map<Id,String>();
			List<OpportunityTeamMember> existingUsers=new List<OpportunityTeamMember>();
			System.debug('***********RecruiterData**'+RecruiterData);
            System.debug('***********OppId**'+OppId);
            if(RecruiterData != null){               
                List<Object> userObj=(List<Object>)RecruiterData;
                System.debug('***********userObj**'+userObj);
                List<Id> userIds=new List<Id>();
                if(userObj.size()>0){
                    for(Object obj: userObj){
                        Map<Object,Object> userData=(Map<Object,Object>)obj;
                        userIds.add((Id)userData.get('Id'));
                    }
                   
                    existingUsers=[Select UserId,User_Name__c,TeamMemberRole from OpportunityTeamMember where OpportunityId =:OppId and UserId in :userIds];
                    for(OpportunityTeamMember opt: existingUsers){
                        existingUserIds.put(opt.UserId,opt.User_Name__c );
                    }                   
                    for(Id usId: userIds){
                        if(!existingUserIds.containsKey(usId)){
                            OpportunityTeamMember OppTeamMember=new OpportunityTeamMember();
                            OppTeamMember.OpportunityAccessLevel='Edit';
                            OppTeamMember.TeamMemberRole='Allocated Recruiter';
                            OppTeamMember.UserId=usId;
                            OppTeamMember.OpportunityId=OppId;
                            OppTeamMember.Source__c='Req Routing';
                            OppTeamMemberList.add(OppTeamMember);
                    	}
                	}
                }
            }
            Map<String,Integer> results=new Map<String,Integer>();
            if(OppTeamMemberList.size()>0){
                System.debug('*****************OppTeamMemberList***'+OppTeamMemberList);
                Database.SaveResult[] srList = Database.insert(OppTeamMemberList, false);              
                Integer createdRecords=0;
                List<Id> successfulInserts = new List<Id>();
                for (Database.SaveResult sr : srList) {
                	if (sr.isSuccess()) {
                    	System.debug('*****************success ids***'+sr);
                        createdRecords=createdRecords+1;
                        successfulInserts.add(sr.getId());
                    }else{ 
                        System.debug('*****************failure ids***'+sr);
                    }
                }
                results.put('createdRecords',createdRecords);
                //return null;
            }
            if(existingUserIds.size() >0){
            	System.debug('*****************existingUserIds***'+existingUserIds.values());
					//------------------------------For Adding Existing Reqruiter to Allocated Requiter
				List<OpportunityTeamMember> OppTeamMemberListUpdate=new List<OpportunityTeamMember>();
				//OppTeamMemberListUpdate=existingUsers.clone();
				System.debug('*****************OppTeamMemberListUpdate***'+OppTeamMemberListUpdate);
				if(existingUsers.size()>0){                 
						for(OpportunityTeamMember OppTeamMember: existingUsers){
							if(OppTeamMember.TeamMemberRole=='Recruiter'){
								OppTeamMember.TeamMemberRole='Allocated Recruiter';							
								OppTeamMemberListUpdate.add(OppTeamMember);
								existingUserIds.remove(OppTeamMember.Id);
							}
                		}
				}
				if(OppTeamMemberListUpdate.size()>0){
                System.debug('*****************OppTeamMemberListUpdate***'+OppTeamMemberListUpdate);
                Database.SaveResult[] srList = Database.update(OppTeamMemberListUpdate, false);              
                Integer updatedRecords=0;
                List<Id> successfulUpdate = new List<Id>();
                for (Database.SaveResult sr : srList) {
                	if (sr.isSuccess()) {
                    	System.debug('*****************success ids***'+sr);
                        updatedRecords=updatedRecords+1;
                        successfulUpdate.add(sr.getId());
                    }else{ 
                        System.debug('*****************failure ids***'+sr);
                    }
                }
                results.put('updatedRecords',updatedRecords);
                //return null;
            }
				//------------------------
                results.put('ExistingRecords',existingUserIds.size());                                
            }
            if(results.size() >0){
                return results;
            }else{
                return null;
            }
        }catch(Exception ex) {
            System.debug('exception  '+ex.getMessage());
            return null;
        }  
    }
	// changes for S-235647 Ability to Un-allocate a Recruiter from Req Routing DB (Back-end) - Debasis
	//We just did OppTeamMember.TeamMemberRole='Recruiter'; from OppTeamMember.TeamMemberRole='Allocated Recruiter';
	@AuraEnabled
	public static Map<String,Integer> saveUnAllocatedRecruiter(String RecruiterId, String OppId) {        
        try {
            List<OpportunityTeamMember> OppTeamMemberList=new List<OpportunityTeamMember>();
            Map<Id,String> existingUserIds=new Map<Id,String>();
			System.debug('***********RecruiterId**'+RecruiterId);
            System.debug('***********OppId**'+OppId);
            if(RecruiterId != null && RecruiterId != '' && OppId != null && OppId != ''){               
                    List<OpportunityTeamMember> existingUsers=new List<OpportunityTeamMember>();
                    existingUsers=[Select Id,
					OpportunityAccessLevel,
					TeamMemberRole,
					UserId,
					User_Name__c,
					Source__c  
					from OpportunityTeamMember 
					where OpportunityId =:OppId and UserId=:RecruiterId];
                    if(existingUsers.size()>0){                 
						for(OpportunityTeamMember OppTeamMember: existingUsers){
						// blocking the other field update as they are not updatable 
							//OppTeamMember.OpportunityAccessLevel='Edit';
							OppTeamMember.TeamMemberRole='Recruiter';
							//OppTeamMember.UserId=RecruiterId;
							//OppTeamMember.OpportunityId=OppId;
							//OppTeamMember.Source__c='Req Routing';
							OppTeamMemberList.add(OppTeamMember); 
                		}
					}
                }
            
            Map<String,Integer> results=new Map<String,Integer>();
            if(OppTeamMemberList.size()>0){
                System.debug('*****************OppTeamMemberList***'+OppTeamMemberList);
                Database.SaveResult[] srList = Database.update(OppTeamMemberList, false);              
                Integer updatedRecords=0;
                List<Id> successfulUpdate = new List<Id>();
                for (Database.SaveResult sr : srList) {
                	if (sr.isSuccess()) {
                    	System.debug('*****************success ids***'+sr);
                        updatedRecords=updatedRecords+1;
                        successfulUpdate.add(sr.getId());
                    }else{ 
                        System.debug('*****************failure ids***'+sr);
                    }
                }
                results.put('updatedRecords',updatedRecords);
                //return null;
            }
            if(existingUserIds.size() >0){
            	System.debug('*****************existingUserIds***'+existingUserIds.values());
                results.put('ExistingRecords',existingUserIds.size());                                
            }
            if(results.size() >0){
                return results;
            }else{
                return null;
            }
        }catch(Exception ex) {
            System.debug('exception  '+ex.getMessage());
            return null;
        }  
    }
}