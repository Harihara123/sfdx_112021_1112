@isTest
public class AddsSelfToOpportunityTeam_Test {
    
    @isTest
    Static void createOpportunityTeamMemberTest(){
        
               
        Account acnt=TestData.newAccount(1);
        insert acnt;
		Opportunity oppr=TestData.newOpportunity(acnt.Id,1);
        insert oppr;
        
        User usr = TestDataHelper.createUser('Single Desk 1');
	    insert usr; 
        
        Test.startTest();
        System.runas(usr){
        	AddsSelfToOpportunityTeam.teamModel tm1 = AddsSelfToOpportunityTeam.createOpportunityTeamMember(oppr.id);
      		AddsSelfToOpportunityTeam.insertOpportunityTeamMember(oppr.id,'Edit','Recruiter');
            AddsSelfToOpportunityTeam.getOpportunityRecord(oppr.id);
         	user us1 =    AddsSelfToOpportunityTeam.fetchUser();
            system.assert( tm1 != null);          
        } 
        
        Test.stopTest(); 
    }
    @isTest
    Static void saveAllocatedRecruiterTest(){        
        Test.startTest();       
        Account acnt=TestData.newAccount(1);
        insert acnt;
		Opportunity oppr=TestData.newOpportunity(acnt.Id,1);
        insert oppr;        
        User usr = TestDataHelper.createUser('Single Desk 1');
        User recruiterUsr = TestDataHelper.createUser('Single Desk 1');
	    List<User> usrList=new List<User>();
        usrList.add(usr);
        usrList.add(recruiterUsr);
	    insert usrList;        
        System.runas(usr){
        	Map<Object,Object> userData=new Map<Object,Object>();
			userData.put('Id',usrList[1].Id);
			List<Object> userObj = new List<Object>();
			userObj.add(userData);
			Object RecruiterData;
            RecruiterData=userObj;
            AddsSelfToOpportunityTeam.saveAllocatedRecruiter(RecruiterData,oppr.Id);
        }
        Test.stopTest(); 
    }

}