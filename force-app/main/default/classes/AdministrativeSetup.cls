/*
Setup Class used for Flexible Search, MultiLevel Copy and Audit Trail.  
*/

global without sharing class AdministrativeSetup {
    
    public string objName{get;set;}
    public boolean isFlexi{get;set;}
    public boolean isMulti{get;set;}
    public boolean isAudit{get;set;}
    public boolean IsCreate{get;set;}
    public boolean isUpdate{get;set;}
    public boolean isDelete{get;set;}
    public boolean blockCascadeDelete{get;set;}
    public boolean SetupEnabled{get;set;}
    public boolean FieldSectionEnabled{get;set;}
    public boolean MultiLevelSection{get;set;}
    public boolean MultilevelFieldSec{get;set;}
    public list<SelectOption> childObjects{get;set;}
    public string ChildObjectName{get;set;}    
    public list<fieldContainer> ParentFields{get;set;}
    public list<fieldContainer> ChildFields{get;set;}
    public boolean CheckAllFlexible{get;set;}
    public boolean CheckAllMultiCopy{get;set;}
    public boolean CheckAllAudit{get;set;}
    public boolean CheckAllChilds{get;set;}
    List<SelectOption> options;
    public Boolean tablerender{get;set;}
    public integer size{get;set;}
    public Boolean objectselect{get;set;}
    public string objLabel{get;set;}
    
    private static final set<String> includedStandardObjects = new set<String>{'Account','Contact','Task','OpportunityTeamMember'};
    private static final set<String> excludedCustomObjects = new set<String>{'MultiLevel_Copy__c','MultiLevel_Copy_Child__c','AuditTrial__c','AuditTrialFields__c','AuditTrialSummary__c'};
    private static final set<string> excludedFieldTypes = new set<string>{'LOCATION','BASE64','ANYTYPE'};
    private static final set<String> excludedSysFields = new set<string>{'AccountSource','CreatedDate','CreatedById','Id','IsDeleted','LastActivityDate','LastModifiedDate','LastModifiedById','LastReferencedDate','LastViewedDate','Name','SicDesc','SystemModstamp'};
    
    public AdministrativeSetup()
    {
        isFlexi = false;
        isMulti = false;
        isAudit = false;
        SetupEnabled = false;
        FieldSectionEnabled = false;
        MultiLevelSection = false;
        MultilevelFieldSec = false;
        //CheckAllFlexible = false;
        CheckAllMultiCopy = false;
        CheckAllAudit = false;
        CheckAllChilds = false;
        
    }
     //This method is used to get all sObjects.
    public List<SelectOption> getobjects()
    {
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();    
        options = new List<SelectOption>();
        set<SelectOption> setoptions = new set<SelectOption>();  
        setoptions.add(new SelectOption('','--None--'));
        for(Schema.SObjectType f : gd)
        {
            if(!f.getDescribe().isCustomSetting()  && f.getDescribe().isQueryable() && f.getDescribe().getName().right(3) !='mdt' && f.getDescribe().getName().right(3) !='__e')
            {
                if((f.getDescribe().isCustom() || includedStandardObjects.contains(f.getDescribe().getName())) && 
                !excludedCustomObjects.contains(string.valueof(f.getDescribe().getName())))
                    setoptions.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getLabel()+'-['+f.getDescribe().getName()+']'));
            }
        }
        options.addAll(setoptions);
       this.options = sortSelectOptionList( this.options, 'asc' );
       // options.sort();
        return options;
    }

    @RemoteAction
    global static List<ResponseObjWrapper> getObjectsList(String query) {
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();
        List<SelectOption> optionsList = new List<SelectOption>();        
        for (Schema.SObjectType f : gd) {
            String objName = String.valueOf(f.getDescribe().getName());
            String objLabel = String.valueOf(f.getDescribe().getLabel());
            //if (!f.getDescribe().isCustomSetting() && f.getDescribe().isQueryable() && objLabel.containsIgnoreCase(query)) {
              if (!f.getDescribe().isCustomSetting() && f.getDescribe().isQueryable() && query.containsIgnoreCase(objLabel)) {
                Boolean shouldBeAdded = (f.getDescribe().isCustom() || includedStandardObjects.contains(f.getDescribe().getName())) && 
                !excludedCustomObjects.contains(string.valueof(f.getDescribe().getName()));
                if (shouldBeAdded) {
                    optionsList.add(new SelectOption(objName, objLabel + '-[' + objName + ']'));
                }                
            }
        }
        List<ResponseObjWrapper> response = new List<ResponseObjWrapper>();
        if (optionsList.isEmpty()) {
            response.add(new ResponseObjWrapper(null, 'No Records Found'));
        } else {
            optionsList = sortSelectOptionList(optionsList, 'asc');    
            for (SelectOption opt : optionsList) {
                response.add(new ResponseObjWrapper(opt.getValue(), opt.getLabel()));
            }
        }
        return response;
    }

    public void describeObject()
      {
        FieldSectionEnabled = false;
        MultiLevelSection = false;
        MultilevelFieldSec = false;
        isFlexi = false;
        isMulti = false;
        isAudit = false;
        //CheckAllFlexible = false;
        CheckAllMultiCopy = false;
        CheckAllAudit = false;
        CheckAllChilds = false;
        ChildObjectName = '';
                
        if(objName != null && objName != '')
        {
            objLabel = Schema.GetGlobalDescribe().get(objName.toLowerCase()).getDescribe().getLabel();
            
            
            Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objName.toLowerCase()).getDescribe().Fields.getMap(); 
            set<string> fieldslist = new set<string>();
            list<string> lstcolumn = new list<string>();
            ParentFields = new list<fieldContainer>();
            tablerender= true;
            objectselect = false;
            
            list<AuditTrialFields__c> ExistingFields4Audit = new list<AuditTrialFields__c>();
            Map<String,AuditTrialFields__c> ExistingFields4AuditMap = new Map<String,AuditTrialFields__c>();
                
            AuditTrial__c auditTrailRecord = new AuditTrial__c();
            try {
                auditTrailRecord = [select  Delete__c,
                                            New__c,Update__c
                                    from    AuditTrial__c
                                    where   Name = :ObjName
                                    limit   1];
                IsCreate = auditTrailRecord.New__c;
                isUpdate = auditTrailRecord.Update__c;
                isDelete = auditTrailRecord.Delete__c;
            } catch(Exception e) {
                auditTrailRecord = new AuditTrial__c();
                IsCreate = false;
                isUpdate = false;
                isDelete = false;
            }
            if( auditTrailRecord.Id != NULL) {
                ExistingFields4Audit = [select Id,Name,API_Name__c,DataType__c,AuditTrial__r.Name
                                        from AuditTrialFields__c
                                        where AuditTrial__c = :auditTrailRecord.Id];
            }
            if(!ExistingFields4Audit.isEmpty())
            {
                for(AuditTrialFields__c aud : ExistingFields4Audit)
                    ExistingFields4AuditMap.put(aud.API_Name__c,aud);
                              
            }  
            for (Schema.SObjectField ft : fMap.values())
            {           
                Schema.DescribeFieldResult fd = ft.getDescribe();
                if(!fd.isCalculated())
                {
                    if(!excludedFieldTypes.contains(String.valueOf(fd.getType())) && !excludedSysFields.contains(fd.getName()))
                    {
                        fieldContainer fc = new fieldContainer();
                        if(ExistingFields4AuditMap.containsKey(string.valueof(fd.getName())))
                            fc.IsAudit = true;
                        else
                            fc.IsAudit = false;
                        
                        fc.api = fd.getName();
                        fc.Label =  String.valueOf(fd.getLabel());
                        fc.Dtype = String.valueOf(fd.getType());                        
                        ParentFields.add(fc);
                    }
                }
               SetupEnabled = true;
               ParentFields.sort();
               integer i=1;
               for(fieldContainer fc : ParentFields)
               {
                    fc.index = i;i++;
               }
            }
        } 
        else
        {
            SetupEnabled = false;
        }
    }
    
    public void EnabledFieldSection()
    {
        If(isFlexi || isMulti || isAudit)
        {
            FieldSectionEnabled = true;
            if(isMulti)
            {
                MultiLevelSection = true;
                describeChildObjects();
            }
            else
            MultiLevelSection = false;
        }
        else
        {
            FieldSectionEnabled = false;
        }
    }
    
    public void describeChildObjects()
    {       
       if(objName != '' && objName != null)
       {
       List <Schema.ChildRelationship> childRelationships =Schema.getGlobalDescribe().get(objName.toLowerCase()).getDescribe().getChildRelationships(); 
                                 
          childObjects = new list<SelectOption>();      
          List <Schema.ChildRelationship> childRelationshipBatch = new List <Schema.ChildRelationship>(); 
          for (Integer childRelationshipIndex = 0; childRelationshipIndex < childRelationships.size(); childRelationshipIndex++)
          {    
              Schema.ChildRelationship childRelationship = childRelationships[childRelationshipIndex];
              
              if (childRelationship.getRelationshipName() != null && childRelationship.getField().getDescribe().isAccessible() && childRelationship.getField().getDescribe().isCreateable())//isUpdateable()
              {
                  childRelationshipBatch.add(childRelationship);
              }
          }
          childObjects.add(new SelectOption('','--None--'));
         for (Schema.ChildRelationship fd : childRelationshipBatch)
         {                 
            childObjects.add(new SelectOption(String.valueOf(fd.getChildSObject()),String.valueOf(fd.getChildSObject().getDescribe().getLabel()+'-['+fd.getChildSObject().getDescribe().getName()+']')));                                                               
         }
       }
    }
    
     public void describeChildFields()
      {
        
    }
    
   public void SelectAllFlexible()
    {
    
    }
    
    public void SelectAllMultiCopy()
    {
        for(fieldContainer fc:ParentFields)
        {
            fc.IsMulti = CheckAllMultiCopy;
        }
    }
        
    
    public void SelectAllAudit()
    {
        for(fieldContainer fc:ParentFields)
        {
            fc.IsAudit = CheckAllAudit;
        }
    }
    
    public void SelectAllChildFields()
    {
        for(fieldContainer fc:ChildFields)
        {
            fc.IsMulti = CheckAllChilds;
        }
    }
    public void SaveParentObject()
    {
        boolean isSave = false; 
        
        if(isAudit)
        {    
            list<AuditTrial__c> Masterobject = new list<AuditTrial__c>();
            list<AuditTrialFields__c> fieldslist = new list<AuditTrialFields__c>();        
            delete [select id,Name from AuditTrial__c where Name=:objName ];        
            AuditTrial__c Master = new AuditTrial__c();
            Master.Name = objName;
            Master.New__c = IsCreate;
            Master.Update__c= isUpdate;
            Master.Delete__c = isDelete;
            insert Master;
            
            for(fieldContainer fc:ParentFields)
            {
                if(fc.IsAudit == true)
                {
                    AuditTrialFields__c atf = new AuditTrialFields__c();
                    atf.Name = fc.Label;
                    atf.API_Name__c = fc.api;
                    atf.DataType__c = fc.Dtype;
                    atf.AuditTrial__c = Master.Id;                    
                    fieldslist.add(atf);
                }
            }
            if(fieldslist.size() > 0)
            {   
                AuditTrialFields__c atf = new AuditTrialFields__c(Name='Name',API_Name__c='Name',DataType__c='STRING',AuditTrial__c = Master.Id);
                fieldslist.add(atf);    
                insert fieldslist;             
            }
            isSave = true;
        }
        if(isSave)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Information saved successfully.'));
           return;
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Select at least one option before saving.'));
            return;
        }
    }
    
    public void SaveChildObject()
    {
        boolean isSave = false;
        string allfields = 'Name';
        for(fieldContainer fc:ChildFields)
        {
            if(fc.IsMulti == true)
            {
                  allfields += ','+fc.api;                    
                  isSave = true;
            }
        }
        if(isSave)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Information saved successfully.'));
           return;
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Select at least one option before saving.'));
            return;
        }
    }
    public PageReference HomePage()
    {
        PageReference pr = new PageReference('/apex/AdministrativeSetupPage').setredirect(true);
        return pr;
    }
    
    
    /* ---Dropdown Sorting Method-----*/
    @TestVisible
    private static List<SelectOption> sortSelectOptionList(List<SelectOption> items, String sortOrder)
    {
 
        List<SelectOption> results = new List<SelectOption>();
 
        //map of select options for sorting
        Map<String, SelectOption> objectMap = new Map<String, SelectOption>();
 
        for(SelectOption so : items){
            objectMap.put(so.getLabel(), so);
        }
 
        //Sort the keys and items in ascending order
        List<String> keys = new List<String>(objectMap.keySet());
        keys.sort();
 
        for(String k : keys){
            results.add(objectMap.get(k));
        }
 
        //clear container for return
        items.clear();
 
        //return items in ASC order
        if(sortOrder.toLowerCase() == 'asc'){
            for(SelectOption ob : results){
                items.add(ob);
            }
        }
        //return items in DESC order
        else{
            for(integer i = results.size()-1; i >= 0; i--){
                items.add(results[i]);
            }
        }
 
        return items;
    }
    
    
    public class fieldContainer implements Comparable
    {
        public Boolean IsFlexible{get;set;}
        public Boolean IsMulti{get;set;}
        public Boolean IsAudit{get;set;} 
        public integer index{get;set;}               
        public String api{get; set;}
        public String Label{get; set;}        
        public String Dtype{get; set;}
        public fieldContainer()
        {
               
        }
        public Integer compareTo(Object ObjToCompare) {
            return Label.CompareTo(((fieldContainer)ObjToCompare).Label);
        }
    }

    global class ResponseObjWrapper {
        public String value {get;set;}
        public String label {get;set;}
        public ResponseObjWrapper(String val, String lbl) {
            value = val;
            label = lbl;
        }
    }
}