global class AlertBatch implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts{
    Set<ID> inactiveUseIdSet = new Set<ID>();
    global Database.QueryLocator start (Database.BatchableContext BC) {
        String query = 'Select Id, ownerId, createdBy.isActive from Search_Alert_Criteria__c where Owner.isActive = false';
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, List<Search_Alert_Criteria__c> scope) {        
        Set<ID> successIdSet = new Set<ID>();
        for(Search_Alert_Criteria__c alert : scope){
            inactiveUseIdSet.add(alert.OwnerId);
        }        
        try {
             successIdSet = SearchController.RemoveAllAlertFromSOA(inactiveUseIdSet);
             List<Search_Alert_Criteria__c> lstAlertToDelete = new List<Search_Alert_Criteria__c>();
             for(Search_Alert_Criteria__c alert : scope){
                 for(Id ownerID : successIdSet){
                     if(ownerID == alert.ownerId){
                         lstAlertToDelete.add(alert); 
                     }
                 }
             }
             if(successIdSet.size() > 0)
                 delete [select id from User_Search_Log__c where ownerId IN : successIdSet];
             if(lstAlertToDelete.size() > 0)
                delete lstAlertToDelete;
        } catch (Exception e) {
            string responseBody = e.getMessage();
            SearchController.createLogForFailedAlert('delete', 'AlertBatch', responseBody);
        }
        
    }
    
    global void finish (Database.BatchableContext BC) {
       // SearchController.RemoveAllAlertFromSOA(inactiveUseIdSet);
    }
    
    global void execute (SchedulableContext SC) {
        Database.executeBatch(new AlertBatch());
    }
    
}