
public class AlertCriteriaParse {

	public class MatchVectors {
		public List<Skills_vector> skills_vector {get;set;} 
		public List<Skills_vector> title_vector {get;set;} 
		public List<Facets> acronym_vector {get;set;} 
		public List<Facets> keyword_vector {get;set;} 
		public List<Skills_vector> sf_vector {get;set;} 
		public Sf_domain_confidence sf_domain_confidence {get;set;} 

		public MatchVectors(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'skills_vector') {
							skills_vector = arrayOfSkills_vector(parser);
						} else if (text == 'title_vector') {
							title_vector = arrayOfSkills_vector(parser);
						} else if (text == 'acronym_vector') {
							acronym_vector = arrayOfFacets(parser);
						} else if (text == 'keyword_vector') {
							keyword_vector = arrayOfFacets(parser);
						} else if (text == 'sf_vector') {
							sf_vector = arrayOfSkills_vector(parser);
						} else if (text == 'sf_domain_confidence') {
							sf_domain_confidence = new Sf_domain_confidence(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'MatchVectors consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Facets {

		public Facets(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Facets consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Skills_vector {
		public String term {get;set;} 
		public Double weight {get;set;} 
		public Boolean required {get;set;} 

		public Skills_vector(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'term') {
							term = parser.getText();
						} else if (text == 'weight') {
							weight = parser.getDoubleValue();
						} else if (text == 'required') {
							required = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Skills_vector consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Sf_domain_confidence {
		public String skillfamily_domain {get;set;} 
		public Integer skillfamily_domain_confidence {get;set;} 

		public Sf_domain_confidence(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'skillfamily_domain') {
							skillfamily_domain = parser.getText();
						} else if (text == 'skillfamily_domain_confidence') {
							skillfamily_domain_confidence = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Sf_domain_confidence consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public Long execTime {get;set;} 
	public Integer offset {get;set;} 
	public Integer pageSize {get;set;} 
	public String type_Z {get;set;} // in json: type
	public String engine {get;set;} 
	public Criteria criteria {get;set;} 
	public Facets fdp {get;set;} 
	public Match match {get;set;} 
	public String httpVerb {get;set;} 
	public MatchVectors matchVectors {get;set;} 
	public String title {get;set;} 

	public AlertCriteriaParse(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'execTime') {
						execTime = parser.getLongValue();
					} else if (text == 'offset') {
						offset = parser.getIntegerValue();
					} else if (text == 'pageSize') {
						pageSize = parser.getIntegerValue();
					} else if (text == 'type') {
						type_Z = parser.getText();
					} else if (text == 'engine') {
						engine = parser.getText();
					} else if (text == 'criteria') {
						criteria = new Criteria(parser);
					} else if (text == 'fdp') {
						fdp = new Facets(parser);
					} else if (text == 'match') {
						match = new Match(parser);
					} else if (text == 'httpVerb') {
						httpVerb = parser.getText();
					} else if (text == 'matchVectors') {
						matchVectors = new MatchVectors(parser);
					} else if (text == 'title') {
						title = parser.getText();
					} else {
						System.debug(LoggingLevel.WARN, 'AlertCriteriaParse consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Data {
		public String srcLinkId {get;set;} 
		public String srcName {get;set;} 

		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'srcLinkId') {
							srcLinkId = parser.getText();
						} else if (text == 'srcName') {
							srcName = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Criteria {
		public Object multilocation {get;set;} 
		public Facets facets {get;set;} 
		public Facets filterCriteriaParams {get;set;} 

		public Criteria(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'multilocation') {
							multilocation = parser.readValueAs(Object.class);
						} else if (text == 'facets') {
							facets = new Facets(parser);
						} else if (text == 'filterCriteriaParams') {
							filterCriteriaParams = new Facets(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Criteria consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Match {
		public Data data {get;set;} 
		public String srcid {get;set;} 

		public Match(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'data') {
							data = new Data(parser);
						} else if (text == 'srcid') {
							srcid = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Match consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static AlertCriteriaParse parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new AlertCriteriaParse(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	







    private static List<Skills_vector> arrayOfSkills_vector(System.JSONParser p) {
        List<Skills_vector> res = new List<Skills_vector>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Skills_vector(p));
        }
        return res;
    }







    private static List<Facets> arrayOfFacets(System.JSONParser p) {
        List<Facets> res = new List<Facets>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Facets(p));
        }
        return res;
    }

}