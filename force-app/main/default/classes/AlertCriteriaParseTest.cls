
@IsTest
public class AlertCriteriaParseTest {
	
	// This test method should give 100% coverage

	static testMethod void testParse() {
		String json = '{\"execTime\":1589475790319,\"offset\":0,\"pageSize\":30,\"type\":\"C2C\",\"engine\":\"ES\",\"criteria\":{\"multilocation\":null,\"facets\":{},\"filterCriteriaParams\":{}},\"fdp\":{},\"match\":{\"data\":{\"srcLinkId\":\"0031p00001duCnVAAU\",\"srcName\":\"Vijay Reddy\"},\"srcid\":\"testerforalert56\"},\"httpVerb\":\"GET\",\"matchVectors\":{\"skills_vector\":[{\"term\":\"devops\",\"weight\":0.11497451,\"required\":false},{\"term\":\"computer network\",\"weight\":0.10177158,\"required\":false},{\"term\":\"ms office suite\",\"weight\":0.09394506,\"required\":false}],\"title_vector\":[{\"term\":\"salesforce administrator\",\"weight\":0.9933072,\"required\":false},{\"term\":\"technical lead developer\",\"weight\":0.9933071,\"required\":false},{\"term\":\"java technical lead\",\"weight\":0.9933071,\"required\":false},{\"term\":\"salesforce admin\",\"weight\":0.88904166,\"required\":false},{\"term\":\"SFDC admin\",\"weight\":0.8702654,\"required\":false},{\"term\":\"salesforce business analyst\",\"weight\":0.842522,\"required\":false},{\"term\":\"administrator\",\"weight\":0.72809416,\"required\":false},{\"term\":\"lead developer\",\"weight\":0.49665356,\"required\":false},{\"term\":\"technical lead\",\"weight\":0.40651092,\"required\":false},{\"term\":\"salesforce developer\",\"weight\":0.19511698,\"required\":false},{\"term\":\"administrative support\",\"weight\":0.0103832,\"required\":false},{\"term\":\"java developer\",\"weight\":0.00919371,\"required\":false},{\"term\":\"lead java developer\",\"weight\":0.00919365,\"required\":false}],\"acronym_vector\":[],\"keyword_vector\":[],\"sf_vector\":[{\"term\":\"CRM\",\"weight\":0.09232651,\"required\":false}],\"sf_domain_confidence\":{\"skillfamily_domain\":\"IT\",\"skillfamily_domain_confidence\":1}},\"title\":\"JavaT2T\"}';
		AlertCriteriaParse r = AlertCriteriaParse.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse.MatchVectors objMatchVectors = new AlertCriteriaParse.MatchVectors(System.JSON.createParser(json));
		System.assert(objMatchVectors != null);
		System.assert(objMatchVectors.skills_vector == null);
		System.assert(objMatchVectors.title_vector == null);
		System.assert(objMatchVectors.acronym_vector == null);
		System.assert(objMatchVectors.keyword_vector == null);
		System.assert(objMatchVectors.sf_vector == null);
		System.assert(objMatchVectors.sf_domain_confidence == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse.Facets objFacets = new AlertCriteriaParse.Facets(System.JSON.createParser(json));
		System.assert(objFacets != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse.Skills_vector objSkills_vector = new AlertCriteriaParse.Skills_vector(System.JSON.createParser(json));
		System.assert(objSkills_vector != null);
		System.assert(objSkills_vector.term == null);
		System.assert(objSkills_vector.weight == null);
		System.assert(objSkills_vector.required == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse.Sf_domain_confidence objSf_domain_confidence = new AlertCriteriaParse.Sf_domain_confidence(System.JSON.createParser(json));
		System.assert(objSf_domain_confidence != null);
		System.assert(objSf_domain_confidence.skillfamily_domain == null);
		System.assert(objSf_domain_confidence.skillfamily_domain_confidence == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse objAlertCriteriaParse = new AlertCriteriaParse(System.JSON.createParser(json));
		System.assert(objAlertCriteriaParse != null);
		System.assert(objAlertCriteriaParse.execTime == null);
		System.assert(objAlertCriteriaParse.offset == null);
		System.assert(objAlertCriteriaParse.pageSize == null);
		System.assert(objAlertCriteriaParse.type_Z == null);
		System.assert(objAlertCriteriaParse.engine == null);
		System.assert(objAlertCriteriaParse.criteria == null);
		System.assert(objAlertCriteriaParse.fdp == null);
		System.assert(objAlertCriteriaParse.match == null);
		System.assert(objAlertCriteriaParse.httpVerb == null);
		System.assert(objAlertCriteriaParse.matchVectors == null);
		System.assert(objAlertCriteriaParse.title == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse.Data objData = new AlertCriteriaParse.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.srcLinkId == null);
		System.assert(objData.srcName == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse.Criteria objCriteria = new AlertCriteriaParse.Criteria(System.JSON.createParser(json));
		System.assert(objCriteria != null);
		System.assert(objCriteria.multilocation == null);
		System.assert(objCriteria.facets == null);
		System.assert(objCriteria.filterCriteriaParams == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AlertCriteriaParse.Match objMatch = new AlertCriteriaParse.Match(System.JSON.createParser(json));
		System.assert(objMatch != null);
		System.assert(objMatch.data == null);
		System.assert(objMatch.srcid == null);
	}
}