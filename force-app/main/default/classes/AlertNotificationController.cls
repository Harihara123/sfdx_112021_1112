public class AlertNotificationController {
    /*
Json in Text Area field(results__c) in Result object
{
"results": [
{
"candidateId": "0030D000009F3YvQAK",
"givenName": "Valarie",
"familyName": "Peterson",
"candidateStatus": "Current",
"city": "Meridian",
"state": "Mississippi",
"country": "United States",
"createdDate": "2019-01-24T15:08:24.000Z",
"alertId": "a3r0D0000004TzSQAU",
"teaser": "This is a resume teaser for this alert match",
"alertTrigger": ["Talent Created"],
"alertMatchTimestamp": "2019-02-13T15:08:24.000Z"
}
]
}

*/
    // email body wrapper class
    public class EmailNotificationWrapper{
        public string alertName;
        public string userEmail;
        public string userName;
        public string subject;
        public integer totalRecordCount;
        public string userId;
        public list<SearchAlertResultsModel.SearchAlertResultModel> final_resultList;
        public list<ReqSearchAlertResultsModel.ReqSearchAlertResultModel> final_req_resultList;
        public EmailNotificationWrapper(){
            this.final_resultList= new List<SearchAlertResultsModel.SearchAlertResultModel>();
            this.final_req_resultList = new List<ReqSearchAlertResultsModel.ReqSearchAlertResultModel>(); 
        }
    }
    
    public static void sendNotification(List<Search_Alert_Result__c> lstSearchAlertResult){

        List<EmailNotificationWrapper> emailBodyWrapperList =new List<EmailNotificationWrapper>();
        if(lstSearchAlertResult.size()>0){
            
            for(Search_Alert_Result__c alert_result : lstSearchAlertResult){
                
                try{
                    if(alert_result.Alert_Criteria__r.Alert_Trigger__C == 'Talent Created'){
                        list<SearchAlertResultsModel.SearchAlertResultModel> temp_resultList = new list<SearchAlertResultsModel.SearchAlertResultModel>();
                   
                        EmailNotificationWrapper wrapObject = new EmailNotificationWrapper();
                        wrapObject.alertName = alert_result.Alert_Criteria__r.Name;
                        wrapObject.userEmail = alert_result.Alert_Criteria__r.Owner.Email;
                        wrapObject.userName = alert_result.Alert_Criteria__r.Owner.FirstName;
                        wrapObject.subject = alert_result.Alert_Criteria__r.Name +'- Talent Alert Results';
                        wrapObject.userId = alert_result.Alert_Criteria__r.OwnerId;
                        
                        SearchAlertResultsModel wrapResults = (SearchAlertResultsModel)JSON.deserialize(alert_result.Results__c, SearchAlertResultsModel.class);
                        // wrapObject.final_resultList.addAll(wrapResults.results);   
                        temp_resultList.addAll(wrapResults.results);
                        
                        // to reverse list to sort desc
                        
                        Integer index=0;
                        for(Integer i = temp_resultList.size()-1;i>=0;i--){
                            if(index<=9){
                                wrapObject.final_resultList.add(temp_resultList[i]);  
                                index++;  
                            }
                            else{
                                break; 
                            }
                            
                        }
                        //end
                        
                        wrapObject.totalRecordCount = temp_resultList.size();
                        emailBodyWrapperList.add(wrapObject);
                        } else {
                            list<ReqSearchAlertResultsModel.ReqSearchAlertResultModel> temp_resultList = new list<ReqSearchAlertResultsModel.ReqSearchAlertResultModel>();
                   
                            EmailNotificationWrapper wrapObject = new EmailNotificationWrapper();
                             wrapObject.alertName = alert_result.Alert_Criteria__r.Name;
                            wrapObject.userEmail = alert_result.Alert_Criteria__r.Owner.Email;
                            wrapObject.userName = alert_result.Alert_Criteria__r.Owner.FirstName;
                            wrapObject.subject = alert_result.Alert_Criteria__r.Name +'- Req Alert Results';
                            wrapObject.userId = alert_result.Alert_Criteria__r.OwnerId;

                            ReqSearchAlertResultsModel wrapResults = (ReqSearchAlertResultsModel)JSON.deserialize(alert_result.Results__c, ReqSearchAlertResultsModel.class);
                            // wrapObject.final_resultList.addAll(wrapResults.results);   
                            temp_resultList.addAll(wrapResults.results);
                            
                            // to reverse list to sort desc
                            
                            Integer index=0;
                            for(Integer i = temp_resultList.size()-1;i>=0;i--){
                                if(index<=9){
                                    wrapObject.final_req_resultList.add(temp_resultList[i]);  
                                    index++;  
                                }
                                else{
                                    break; 
                                }
                                
                            }
                            //end
                            
                            wrapObject.totalRecordCount = temp_resultList.size();
                            emailBodyWrapperList.add(wrapObject);
                        }
                    
                    
                }catch(Exception e){
                      system.debug('Exception__>'+e.getStackTraceString());
                      ConnectedLog.LogException('AlertNotificationController','sendNotification',e);
                }
                
            }
            
            AlertNotificationController.sendEmailMethod(emailBodyWrapperList);
        }
        
    }
    public static void sendEmailMethod(list<EmailNotificationWrapper> wrapObjectList){
            Date alertDate = (system.today()-1);
            String alertDateString = alertDate.format();
            EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where developername='Alert_Results_Notification' limit 1];
            
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            
            for(EmailNotificationWrapper wrapObject : wrapObjectList){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               // String[] toAddresses = new String[]{wrapObject.userEmail};
               //     mail.setToAddresses(toAddresses);     
                mail.setTargetObjectId(wrapObject.userId);           
                String htmlbody = emailTemplate.HtmlValue;    
                htmlbody = htmlbody.replace('{!Name}', wrapObject.userName); 
                htmlbody = htmlbody.replace('{!AlertName}', wrapObject.alertName); 
                htmlbody = htmlbody.replace('{!date}', alertDateString); 
                if(wrapObject.totalRecordCount > 10){
                    htmlbody = htmlbody.replace('{!displayCount}','10');
                    htmlbody = htmlbody.replace('{!totalCount}',String.valueof(wrapObject.totalRecordCount));       
                }else{
                    htmlbody = htmlbody.replace('{!displayCount}',String.valueof(wrapObject.totalRecordCount));
                    htmlbody = htmlbody.replace('{!totalCount}',String.valueof(wrapObject.totalRecordCount)); 
                }
                
                String resultsBody='';

                String reqString = 'Req';

                if(wrapObject.subject.contains(reqString)){
                   for(ReqSearchAlertResultsModel.ReqSearchAlertResultModel res : wrapObject.final_req_resultList){
                        resultsBody += AlertNotificationController.getReqResultBody(res);
                    }
                    htmlbody = htmlbody.replace('{!AlertType}', 'Req Search Alert');
                } else {
                    for(SearchAlertResultsModel.SearchAlertResultModel res : wrapObject.final_resultList){
                        resultsBody += AlertNotificationController.getResultBody(res);
                    }
                    htmlbody = htmlbody.replace('{!AlertType}', 'Talent Search Alert');
                }
                
                
                // system.debug('ReqResultsBody'+resultsBody);
                htmlbody = htmlbody.replace('{!results}', resultsBody); 
                //system.debug('htmlbody-->'+htmlbody);
                mail.setHtmlBody(htmlbody);
                mail.setSubject(wrapObject.subject);
                mail.saveAsActivity = false;
                mails.add(mail);    
            }
            
            if (!Test.isRunningTest()) {
                List<Messaging.sendEmailResult> results = Messaging.sendEmail(mails);
                
            }    
        
        
    }
    
    public static string getResultBody(SearchAlertResultsModel.SearchAlertResultModel res){
        String resultHtmlBody='';
        try{
                String contact_baseUrl = String.valueof(Label.ALert_Contact_URL);
                String candidateUrl ='';
                candidateUrl = contact_baseUrl.replace('{contactid}', res.candidateId);
            // Date createdDate = date.valueOf(res.createdDate);
            String dateString = '';
            String dateLabel ='';    
            String teaser = res.teaser;
            String name = res.givenName+' '+res.familyName;
            String address = res.city+', '+res.state+', '+res.country;
            if(name.length() > 100){
                name  = name.substring(0, 100)+'...';
            } 
            if(teaser =='...' || teaser.length() == 0){
                teaser = 'N/A';
            }
          
            if(res.resumeLastModifiedDate != null){
                Datetime resultDate = Datetime.valueOf(res.resumeLastModifiedDate);
                dateString  = resultDate.format('MMM d, YYYY');
                dateLabel='Resume Updated Date';
            }
            else if(res.createdDate != null){
                Datetime resultDate = Datetime.valueOf(res.createdDate);
                dateString = resultDate.format('MMM d, YYYY');
                dateLabel='Created Date';
                
            }
            /*  if(address.length()>25){
address = address.substring(0, 24)+'...' ;
                }
            */
            
            // resultHtmlBody +='<tr>'
            //     +'<td width=270 valign=top style="width:202.8pt;border-top:none;border-left:'
            //             +'solid #A5A5A5 1.0pt;border-bottom:solid #A5A5A5 1.0pt;border-right:none;'
            //     +'background:#F2F2F2;padding:13px 5.4px 5.4px;">'
            //     +'<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:200%">'
            //     +'<b><span title="'+res.givenName+' '+res.familyName+'" style="font-size:14.0pt;line-height:75%;color:#4472C4">'
            //     +'<a href="'+candidateUrl+'">'+ name +'</a></span></b> </p>'
            //     +'<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:75%">'
            //     +'<b><span style="font-size:12.0pt;line-height:210%">'+res.jobTitle+'</span></b></p>'
            //     +'<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:75%">'
            //             +'<span title="'+address+'" style="font-size:12.0pt;line-height:200%">'+address+'</span> </p></td>'
            //             +'<td width=78 style="width:58.5pt;border:none;border-bottom:solid #A5A5A5 1.0pt;background:#F2F2F2;padding:0in 5.4pt 0in 5.4pt">'
            //             +'<p class=MsoNormal align=center style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b>'+res.candidateStatus+' </b></p>'
            //     +'</td>'
            //     +'<td width=282 style="width:211.25pt;border-top:none;border-left:none;border-bottom:solid #A5A5A5 1.0pt;border-right:solid #A5A5A5 1.0pt;'
            //     +'background:#F2F2F2;padding:0in 5.4pt 0in 5.4pt">'
            //     +'<p class=MsoNormal align=right style="margin-bottom:0in;margin-bottom:.0001pt;text-align:right;line-height:normal">'+dateLabel+': <b>'+dateString+'</b> </p>'
            //     +'</td>'
            //     +'</tr>'
            //     +'<tr>'
            //     +'<td width=630 colspan=3 valign=top style="width:472.55pt;border:solid #A5A5A5 1.0pt;'
            //     +'border-top:none;padding:4px;">'
            //     +'<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal">'+teaser+' </p>'
            //     +'</td>'
            //     +'</tr>'
            //     +'<tr><td width="630" colspan="2" style="height:10px;background-color:#FFFFFF;border-top:none;border-bottom:solid #A5A5A5 1.0pt;border-left:none;border-right:none"></td></tr>';
            resultHtmlBody +='<tr> <td width="650" colspan="3" valign="center" style="width:202.8pt;border-top:solid #A5A5A5 1.0pt;border-left: solid #A5A5A5 1.0pt;border-right:solid #A5A5A5 1.0pt;border-bottom:none;background:#F2F2F2;padding:13px 5.4px 5.4px"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:100%"> <b><span title="'+res.givenName+' '+res.familyName+'" style="font-size:14.0pt;line-height:75%;color:#4472C4"> <a href="'+candidateUrl+'">'+name+' </a> </span></b> </p></td></tr><tr> <td width="282" style="width:211.25pt;border-top:0;border-bottom:0;border-left:solid #A5A5A5 1.0pt; background:#F2F2F2;padding:2.5px 5.4px"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:75%"> <b><span style="font-size:12.0pt;line-height:100%">'+res.jobTitle+'</span></b></p></td><td width="78" style="width:58.5pt;border:0;;background:#F2F2F2;padding:2.5px 5.4px"> <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b>'+res.candidateStatus+' </b></p></td><td width=282 style="width:211.25pt;border-top:0;border-left:0;border-bottom:0;border-right:solid #A5A5A5 1.0pt; background:#F2F2F2;padding:2.5px 5.4px"> <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:right;line-height:normal">'+dateLabel+' : <b>'+dateString+'</b> </p></td></tr><tr> <td width="650" colspan="3" valign="center" style="width:202.8pt;border-top:0;border-left: solid #A5A5A5 1.0pt;border-right:solid #A5A5A5 1.0pt;border-bottom:solid #A5A5A5 1.0pt;background:#F2F2F2;padding:2.5px 5.4px 5.4px"> <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:left;line-height:normal">'+ address +'</p></td></tr><tr> <td width="650" colspan="3" valign="top" style="width:472.55pt;border-top:none;border-bottom:solid #A5A5A5 1.0pt;border-left:solid #A5A5A5 1.0pt;border-right:solid #A5A5A5 1.0pt;padding:4px;"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal">'+teaser+' </p></td></tr><tr> <td width="650" colspan="3" style="height:10px;background-color:#FFFFFF;border:none"></td></tr>';
                    
        
            
        }Catch(Exception e){
            system.debug('Exception__>'+e.getStackTraceString());
            ConnectedLog.LogException('AlertNotificationController','getResultBody',e);
        }
        return resultHtmlBody;	
    }

    public static string getReqResultBody(ReqSearchAlertResultsModel.ReqSearchAlertResultModel res){
        String resultHtmlBody='';
        try{
            String oppty_baseUrl = String.valueof(Label.Alert_Oppty_URL);
            String opptyUrl ='';
            opptyUrl = oppty_baseUrl.replace('{jobId}', res.jobId);
            // Date createdDate = date.valueOf(res.createdDate);
            String dateString = '';
            String createdDate ='';    
            String skills = ' N/A';
            String name = res.positionTitle+'@ '+res.accountName;
            String truncatedName = name;
            String address = res.city+', '+res.state+', '+res.country;
            String jobOwner = ' '+res.jobOwner;
            String reqStage = ' '+res.jobStage;
            String duration = 'Duration: N/A';
            String billRate = 'N/A';
            String jobMinRate = 'N/A';
            String jobMaxRate = 'N/A';
            String workRemote = '';
            String subVendor = '';
            
            // System.debug('Res ==>'+res);
            
            if(name.length() > 100){
                truncatedName  = truncatedName.substring(0, 100) + '...';
            } 
            
            if(res.jobCreatedDate != null){
               
                Datetime resultDate = Datetime.valueOf(res.jobCreatedDate);
                
                dateString  = resultDate.format('MMM d, YYYY');
                 
                createdDate = 'Created: '+dateString;
            } else {
                createdDate = 'Created: N/A';
            }

            if(String.isNotBlank(res.skills)){
                skills = ' '+res.skills;
            } 
            if(String.isNotBlank(res.jobDuration)){
                duration = 'Duration: '+res.jobDuration+' '+res.jobDurationUnit;
            } 

            if(String.isNotBlank(res.jobMaxRate)){
                jobMaxRate = res.jobMaxRate;
            }

            if(String.isNotBlank(res.jobMinRate)){
                jobMinRate = res.jobMinRate;
            }

            if (jobMinRate == 'N/A' && jobMaxRate == 'N/A'){
                billRate = 'Bill Rate: N/A';
            } else if (jobMinRate == 'N/A'){
                billRate = 'Bill Rate: $'+res.jobMaxRate+'/hr';
            } else if (jobMaxRate == 'N/A') {
                billRate = 'Bill Rate: $'+res.jobMinRate+'/hr';
            } else {
                billRate = 'Bill Rate: $'+res.jobMinRate+' - $'+res.jobMaxRate+'/hr';
            }
            
            workRemote = res.workRemote ? 'Remote: Yes' : 'Remote: No';

            subVendor = res.canUseApprovedSubVendor ? 'Subvendor: Yes' : 'Subvendor: No';
             
            


            /*  if(address.length()>25){
address = address.substring(0, 24)+'...' ;
                }
            */
          
            resultHtmlBody += '<tr> <td width="650" colspan="2" style="width:650.8pt;border-top:solid #A5A5A5 1.0pt;border-left: solid #A5A5A5 1.0pt; border-bottom:none;border-right:solid #A5A5A5 1.0pt; background:#F2F2F2;padding:13px 5.4px 5.4px;"> <table style="border-spacing: 0px;margin:0px;border-collapse: collapse"> <tbody> <td align="left" width="521"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:100%"> <b> <span title="'+name+'" > <a style="font-size:16px; line-height:75%;color:#4472C4;width:521px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis" href="'+opptyUrl+'">'+truncatedName+'</a> </span> </b> </p></td><td align="right" width="110"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:100%"> <b> <span title="'+res.positionId+'" style="font-size:16px;line-height:75%;color:#4472C4;width:110px"> <a href="'+opptyUrl+'">'+res.positionId+'</a> </span> </b> </p></td></tbody> </table> </td></tr><tr> <td width="650" valign="top" colspan="2" style="width:650.8pt;border-top:none;border-left: solid #A5A5A5 1.0pt; border-bottom:none;border-right:solid #A5A5A5 1.0pt; background:#F2F2F2;padding:0in 5.4pt"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:75%"> <span title="address" style="font-size:12.0pt;line-height:100%">'+address+'</span> </p></td></tr><tr> <td width="315" valign="top" style="width:315pt;border-top:none;border-left: solid #A5A5A5 1.0pt;border-bottom:solid #A5A5A5 1.0pt;border-right:none; background:#F2F2F2;padding:0in 5.4pt 5.4pt 5.4pt"> <ul style="margin-top: 4px;margin-bottom: 8px;padding-left: 20px;"> <li style="margin-bottom: 0px"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;margin-top:0in;margin-top:0px;line-height:75%"> <span title="owner" style="font-size:12.0pt"> <b>Owner:</b>'+jobOwner+' </span> </p></li><li style="margin-bottom: 0px"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;margin-top:0in;margin-top:0px;line-height:75%"> <span title="address" style="font-size:12.0pt"> <b>Stage:</b>'+reqStage+' </span> </p></li></ul> </td><td width="315" valign="top" style="width:315pt;border-top:none;border-left:none;border-bottom:solid #A5A5A5 1.0pt;border-right:solid #A5A5A5 1.0pt; background:#F2F2F2;padding:0in 5.4pt 0in 5.4pt"> <p class="MsoNormal" align="left" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:left;line-height:normal"> <b>Skills:</b>'+skills+' </p></td></tr><tr> <td colspan="2" valign="middle" style="border-bottom:solid #A5A5A5 1.0pt;border-left:solid #A5A5A5 1.0pt;border-right:solid #A5A5A5 1.0pt;border-top:none;"> <table style="padding:0rem;border-collapse: collapse;" cellspacing="0"> <tbody> <tr> <td width="110" valign="middle" style="width:110.55pt;border-right:solid #A5A5A5 1.0pt;padding:2px;"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;font-size:9.5pt">'+createdDate+'</p></td><td width="110" valign="middle" style="width:110.55pt;border-right:solid #A5A5A5 1.0pt;padding:2px;"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;font-size:9.5pt">'+duration+'</p></td><td width="150" valign="middle" style="width:150.55pt;border-right:solid #A5A5A5 1.0pt;padding:2px;"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;font-size:9.5pt">'+billRate+'</p></td><td width="55" valign="middle" style="width:55.55pt;border-right:solid #A5A5A5 1.0pt;padding:2px;"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;font-size:9.5pt">'+workRemote+'</p></td><td width="70" valign="middle" style="width:70.55pt;padding:2px;"> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;font-size:9.5pt">'+subVendor+'</p></td></tr></tbody> </table> </td></tr><tr> <td width="650" colspan="2" style="height:10px;background-color:#FFFFFF;border:none"></td></tr>';
            
        
        }Catch(Exception e){
            system.debug('Exception__>'+e.getStackTraceString());
            ConnectedLog.LogException('AlertNotificationController','getReqResultBody',e);
        }
        
                
        return resultHtmlBody;
    }
}