@isTest 
private class AlertUtilizationBatchTest {

	@isTest
	private static void AlertUtil() {
	Test.startTest();
	List<User> usrs = TestDataHelper.createUsers(3, 'System Administrator');
	List<Search_Alert_Criteria__c> lstsrc= new List<Search_Alert_Criteria__c>();
	
	
	Search_Alert_Criteria__c srcobj= new Search_Alert_Criteria__c();
	srcobj.Alert_Criteria__c='{"execTime":1589824699744,"offset":0,"pageSize":30,"type":"C","engine":"ES","criteria":{"keyword":"LEANFT","facets":null,"filterCriteriaParams":null},"fdp":null,"title":"alertRyEi"}';
	srcobj.LastModifiedById=usrs.get(0).Id;
        lstsrc.add(srcobj);
        
    Search_Alert_Criteria__c srcobj1= new Search_Alert_Criteria__c();
	srcobj1.Alert_Criteria__c='{"execTime":1589824699744,"offset":0,"pageSize":30,"type":"C2C","engine":"ES","criteria":{"keyword":"LEANFT","facets":null,"filterCriteriaParams":null},"fdp":null,"title":"alertRyEi"}';
	srcobj1.LastModifiedById=usrs.get(0).Id;
        lstsrc.add(srcobj1);
        
        Search_Alert_Criteria__c srcobj2= new Search_Alert_Criteria__c();
	srcobj2.Alert_Criteria__c='{"execTime":1589824699744,"offset":0,"pageSize":30,"type":"R","engine":"ES","criteria":{"keyword":"LEANFT","facets":null,"filterCriteriaParams":null},"fdp":null,"title":"alertRyEi"}';
	srcobj2.LastModifiedById=usrs.get(0).Id;
        lstsrc.add(srcobj2);
        
    Search_Alert_Criteria__c srcobj3= new Search_Alert_Criteria__c();
	srcobj3.Alert_Criteria__c='{"execTime":1589824699744,"offset":0,"pageSize":30,"type":"C2R","engine":"ES","criteria":{"keyword":"LEANFT","facets":null,"filterCriteriaParams":null},"fdp":null,"title":"alertRyEi"}';
	srcobj3.LastModifiedById=usrs.get(0).Id;
        lstsrc.add(srcobj3);
        
    Search_Alert_Criteria__c srcobj4= new Search_Alert_Criteria__c();
	srcobj4.Alert_Criteria__c='{"execTime":1589824699744,"offset":0,"pageSize":30,"type":"R2C","engine":"ES","criteria":{"keyword":"LEANFT","facets":null,"filterCriteriaParams":null},"fdp":null,"title":"alertRyEi"}';
	srcobj4.LastModifiedById=usrs.get(0).Id;
        lstsrc.add(srcobj4);
        insert lstsrc;

        
	AlertUtilizationCalculatorBatch batch = new AlertUtilizationCalculatorBatch();
	Database.executeBatch(batch,5);

	Test.stopTest();

}
	
}