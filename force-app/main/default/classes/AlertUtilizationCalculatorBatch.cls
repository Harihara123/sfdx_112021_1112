global class AlertUtilizationCalculatorBatch implements Database.Batchable<sObject>, Schedulable {

	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */
	global Database.QueryLocator start(Database.BatchableContext context) {
		Integer days = Integer.valueOf(Label.Search_Utilization_Days);
		String soql =  'SELECT Alert_Criteria__c,CreatedById,CreatedBy.Name,CreatedBy.Office__c,CreatedBy.OPCO__c,CreatedBy.Division__c FROM Search_Alert_Criteria__c where CreatedDate = LAST_N_DAYS:'+days;
        system.debug(soql);
		return Database.getQueryLocator(soql);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */
	global void execute(Database.BatchableContext context, List<Search_Alert_Criteria__c> scope) {
		
		Search_Utilization__c srcUtilizn;

		Set<ID> aID = new Set<ID> ();
		for (Search_Alert_Criteria__c con : scope)
		{
			aID.add(con.CreatedById);
		}
		String RecordTypeID=Utility.getRecordTypeId('Search_Utilization__c', 'Alert Criteria');
		Integer days = Integer.valueOf(Label.Search_Utilization_Days);
		String soql='select User__c,Type_R__c,type_C2C__c,type_C2R__c,Type_R2C__c ,Type_C__c, Total__c from Search_Utilization__c where User__c in :aID and RecordType.Name IN (\'Alert Criteria\') and CreatedDate = LAST_N_DAYS:'+days;
		List<Search_Utilization__c> srcutilList =Database.query(soql);
		System.debug('values for search utilization tabel:'+srcutilList);
		List<Search_Utilization__c> srcUtilizationList = new List<Search_Utilization__c> ();
		Map<Id, Search_Utilization__c> srcutilmap = new Map<Id, Search_Utilization__c> ();

		for (Search_Alert_Criteria__c alertcriteria : scope) {
			String srcCriteria = getAlertCriteria(alertcriteria);
			if (srcutilmap.get(alertcriteria.CreatedById) != null) 
			{
				//System.debug('inside if loop');
				AlertUtilizationWrapper srcUtilizWrapper = alertUtilizationInfo(srcCriteria);
				srcUtilizn = srcutilmap.get(alertcriteria.CreatedById);
				srcUtilizn.recordTypeid = Utility.getRecordTypeId('Search_Utilization__c', 'Alert Criteria');
				srcUtilizn.User__c = alertcriteria.CreatedById;
				srcUtilizn.Office__c = alertcriteria.CreatedBy.Office__c;
				srcUtilizn.Opco__c = alertcriteria.CreatedBy.OPCO__c;
				srcUtilizn.Division__c = alertcriteria.CreatedBy.Division__c;
				srcUtilizn.Created_Date__c = System.today();
				srcUtilizn.Type_C__c = srcUtilizn.Type_C__c + srcUtilizWrapper.type_C;
				srcUtilizn.Type_R__c = srcUtilizn.Type_R__c + srcUtilizWrapper.type_R;
				srcUtilizn.Type_C2C__c = srcUtilizn.Type_C2C__c + srcUtilizWrapper.type_C2C;
				srcUtilizn.Type_C2R__c = srcUtilizn.Type_C2R__c + srcUtilizWrapper.type_C2R;
				srcUtilizn.Type_R2C__c = srcUtilizn.Type_R2C__c + srcUtilizWrapper.type_R2C;
				srcUtilizn.Total__c = srcUtilizWrapper.total + srcUtilizn.Total__c;
				srcutilmap.put(alertcriteria.CreatedById, srcUtilizn);
				System.debug('THis is map when updating the record'+srcutilmap);
			}
			else {
				//System.debug('inside else loop');
				String srcCriteria1 = getAlertCriteria(alertcriteria);

				AlertUtilizationWrapper srcUtilizWrapper = alertUtilizationInfo(srcCriteria1);
				srcUtilizn = new Search_Utilization__c();
				srcUtilizn.recordTypeid = Utility.getRecordTypeId('Search_Utilization__c', 'Alert Criteria');
				srcUtilizn.User__c = alertcriteria.CreatedById;
				srcUtilizn.Office__c = alertcriteria.CreatedBy.Office__c;
				srcUtilizn.Opco__c = alertcriteria.CreatedBy.OPCO__c;
				srcUtilizn.Division__c = alertcriteria.CreatedBy.Division__c;
				srcUtilizn.ExternalId__c = String.valueOf(alertcriteria.CreatedById)+String.valueOf(System.today());
				srcUtilizn.Created_Date__c = System.today();
				srcUtilizn.Type_C__c = srcUtilizWrapper.type_C;
				srcUtilizn.Type_R__c = srcUtilizWrapper.type_R;
				srcUtilizn.Type_C2C__c = srcUtilizWrapper.type_C2C;
				srcUtilizn.Type_C2R__c = srcUtilizWrapper.type_C2R;
				srcUtilizn.Type_R2C__c = srcUtilizWrapper.type_R2C;
				srcUtilizn.Total__c = srcUtilizWrapper.total;
				srcutilmap.put(alertcriteria.CreatedById, srcUtilizn);
				System.debug('THis is map when creating  the record'+srcutilmap);
			}
		}
		
		
		for (Search_Utilization__c src : srcutilList)
		
		{
		
		if (srcutilmap.get(src.User__c) != null) 
			{
		
				srcUtilizn = srcutilmap.get(src.User__c);
				srcUtilizn.Type_C__c = srcUtilizn.Type_C__c+src.Type_C__c ;
				srcUtilizn.Type_R__c = srcUtilizn.Type_R__c + src.Type_R__c;
				srcUtilizn.Type_C2C__c = srcUtilizn.Type_C2C__c + src.Type_C2C__c;
				srcUtilizn.Type_C2R__c = srcUtilizn.Type_C2R__c + src.Type_C2R__c;
				srcUtilizn.Type_R2C__c = srcUtilizn.Type_R2C__c + src.Type_R2C__c;
				srcUtilizn.Total__c = srcUtilizn.total__c + src.Total__c;
				srcutilmap.put(src.User__c, srcUtilizn);
				}
		}
		
		upsert srcutilmap.values() ExternalId__c ;
	}





	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */
	global void finish(Database.BatchableContext context) {

	}


	private static String getAlertCriteria(Search_Alert_Criteria__c logEntry) {

		if (logEntry.Alert_Criteria__c == null || logEntry.Alert_Criteria__c == '') {
			ConnectedLog.LogInformation('SearchUtilizationCalculatorBatch', 'searchUtilizationInfo', logEntry.Id, 'No Json Value for Alert Criteria for ');
		}
		else {
			String alertjson=logEntry.Alert_Criteria__c;
			String alerttype=alertjson.substringBetween('"type":"','"');
			return alerttype;
			}
		return logEntry.Alert_Criteria__c;
	}

	private AlertUtilizationWrapper alertUtilizationInfo(String jsonStr) {
		//System.debug('This is the input for Json String inside alertUtilization :' + jsonStr);
		AlertUtilizationWrapper srcWrapper = new AlertUtilizationWrapper();
		String searchType;
		if (String.isNotBlank(jsonStr)) {
			try {

				//AlertCriteriaParse objAlertCriteriaParse = new AlertCriteriaParse(System.JSON.createParser(jsonStr));
				//System.debug('after deserialization: ' + objAlertCriteriaParse.type_Z);
				searchType = jsonStr;
				switch on searchType {
					when 'C' {
						srcWrapper.type_C += 1;
						srcWrapper.total += 1;
					}
					when 'R' {
						srcWrapper.type_R += 1;
						srcWrapper.total += 1;
					}
					when 'C2C' {
						srcWrapper.type_C2C += 1;
						srcWrapper.total += 1;
					}
					when 'C2R' {
						srcWrapper.type_C2R += 1;
						srcWrapper.total += 1;
					}
					when 'R2C' {
						srcWrapper.type_R2C += 1;
						srcWrapper.total += 1;
					}
				}


			} catch(JSONException jExp) {
				System.debug(jExp);
				ConnectedLog.LogException('AlertUtilizationCalculatorBatch', 'alertUtilizationInfo', jExp);
			}

		}
		return srcWrapper;

	}

	class AlertUtilizationWrapper {
		public Integer total = 0;
		public Integer type_C = 0;
		public Integer type_R = 0;
		public Integer type_C2C = 0;
		public Integer type_C2R = 0;
		public Integer type_R2C = 0;
	}

	public void execute(SchedulableContext sc) {
		AlertUtilizationCalculatorBatch batch = new AlertUtilizationCalculatorBatch();
		Database.executeBatch(batch);
	}
}