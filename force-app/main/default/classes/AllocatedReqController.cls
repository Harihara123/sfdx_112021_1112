public without sharing class AllocatedReqController {

	public static string open = 'Open';
	public static string[] stageName = new string[]{'Qualified', 'RedZone'};
	public static string teamMemberRole = 'Allocated Recruiter';

    @AuraEnabled(cacheable=false)
    public static string getPermissions(){
        String outputJson = '{';
        try {
            // Get User
            SearchController.SearchUser searchUser = (SearchController.SearchUser)JSON.deserialize(SearchController.getCurrentUser(), SearchController.SearchUser.class);

            // Adding to display top matches when the permission set available 
			if(searchUser.userPermissionName.contains('ShowTopTalentsInWidget')){
				outputJson += '\'ShowTopTalentsInWidget\':\'true\',';
			}

            // Add check for 'Recently Applied' here

            return outputJson + '}';

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

	@AuraEnabled(cacheable = true)
	public static List<OpportunityTeamMember> getAllocatedOptions() {
        List<OpportunityTeamMember> lstOpptyTeamMembers = new List<OpportunityTeamMember>();

		try {
            if (Schema.SObjectType.OpportunityTeamMember.isAccessible()) {
                lstOpptyTeamMembers = [
                    SELECT UserId, User_Name__c, CreatedDate, 
                            Opportunity.Id, Opportunity.Name, Opportunity.Req_Job_Title__c, Opportunity.Req_GeoLocation__Latitude__s, Opportunity.Req_GeoLocation__Longitude__s,
                            Opportunity.Req_Worksite_City__c, Opportunity.Req_Worksite_State__c, Opportunity.Req_Worksite_Country__c
                    FROM OpportunityTeamMember 
                    WHERE TeamMemberRole =: teamMemberRole 
                    AND Opportunity.IsClosed = false
                    AND Opportunity.Req_Qualifying_Stage__c IN :stageName
                    AND Opportunity.Req_Open_Positions__c >=0 
                    AND userId = : userInfo.getuserId() 
                    WITH SECURITY_ENFORCED 
                    ORDER BY LastModifiedDate DESC];
            }
            else {
                throw new AuraHandledException(Label.CRM_NoAccess_OpportunityTeamMember);
            }
			return lstOpptyTeamMembers;
        } catch(Exception  e){
			throw new AuraHandledException(e.getMessage());
		}
	}

    @AuraEnabled(cacheable=false)
    public static String getTopMatches(String queryString) {
        // Get user details
        SearchController.SearchUser searchUser = (SearchController.SearchUser)JSON.deserialize(SearchController.getCurrentUser(), SearchController.SearchUser.class);

        // Add to queryString
        queryString += '&userId=' + searchUser.id;
        queryString += '&opco=' + searchUser.opco;
        queryString += '&flt.group_filter=' + searchUser.opcoSearchFilter;
        queryString += '&rescore=true';

        String talentMatches = SearchController.matchTalents(queryString, 'GET', '', '');

        // Do more stuff here
        // I was hoping to do all those subsequent calls here, instead of doing it all in the JS controller.

        return talentMatches;
    }

    @AuraEnabled(cacheable=false)
    public static string getTalentMatches(String queryString){
        try {
            // Get user details
            SearchController.SearchUser searchUser = (SearchController.SearchUser)JSON.deserialize(SearchController.getCurrentUser(), SearchController.SearchUser.class);
			//Adding to display top matches when the permission set available 
			if(!searchUser.userPermissionName.contains('ShowTopTalentsInWidget')){
				return null;
			}
            // Add to queryString
            queryString += '&userId=' + searchUser.id;
            queryString += '&opco=' + searchUser.opco;
            queryString += '&flt.group_filter=' + searchUser.opcoSearchFilter;

            // LTR
            //if (FeatureManagement.checkPermission('GroupB')) {
                queryString += '&rescore=true';
            /*}
            else {
                queryString += '&sort=relevancyV2:desc';
            }*/
            
            // Use existing search controller.
            // Current logic (on match page) has requestBody and location null always
            // 
            // SearchController.matchTalents(queryString, httpRequestType, requestBody, location)
            return SearchController.matchTalents(queryString, 'GET', '', '');
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=false)
    public static List<Contact> getContactActivity(string[] listOfContactIds){
        try {
            return [SELECT Id, (SELECT Id, ActivityDate, CreatedBy.Name FROM ActivityHistories ORDER BY ActivityDate DESC LIMIT 1) FROM Contact WHERE Id IN :listOfContactIds];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static Object getApplicants(String recordId) {
       
        Map<String, Object> p = new Map<String, Object>();
        p.put('recordId', recordId);

        return ATSTalentOrderFunctions.performServerCall('getApplicantsFromJobPosting', p);
    }
}