@isTest(seealldata = false)
public without sharing class AllocatedReqController_Test {
    public static User usr;
    public static Opportunity oppr;
    
    static void setup(String profile, String status, String teamMemeberRole) {
        // Insert test user
        usr = TestDataHelper.createUser(profile);
        insert usr;
        
        User u = [Select Id, Name, Email from User where id =: usr.Id];
        
        // Insert test account
        Account acnt = TestData.newAccount(1);
        insert acnt;
        
        // Insert test oppy
        oppr = TestData.newOpportunity(acnt.Id, 1);
        oppr.StageName = 'Qualifying';
        oppr.status__c = status;
        insert oppr;
        
        // Insert test OTM
        OpportunityTeamMember oppTeamMember = new OpportunityTeamMember(UserId = u.Id, teammemberrole = teamMemeberRole, opportunityid = oppr.Id, OpportunityAccessLevel = 'Edit');
        insert OppTeamMember;
    }
    
    @isTest
    Static void createOpportunityTeamMemberTest() {
        // Setup data
        setup('Single Desk 1', 'Open', 'Allocated Recruiter');

        // Ensure data was setup properly
        system.assertEquals(oppr.StageName, 'Qualifying');

        Test.startTest();
        System.runas(usr) {
            // Get Allocated reqs. Should return results.
            List<OpportunityTeamMember> options = AllocatedReqController.getAllocatedOptions();
            System.assert(options.size() > 0);
        }
        Test.stopTest();
    }

    @isTest
    Static void createOpportunityTeamMemberTest2() {
        // Setup data
        setup('Single Desk 1', 'Draft', 'Recruiter');

        // Ensure data was setup properly
        system.assertEquals(oppr.StageName, 'Qualifying');

        Test.startTest();
        System.runas(usr) {
            // Get Allocated reqs. Should return empty.
            List<OpportunityTeamMember> options = AllocatedReqController.getAllocatedOptions();
            System.assert(options.isEmpty());
        }
        Test.stopTest();
    }

    @isTest
    Static void createOpportunityTeamMemberTest3() {
        // Setup data
        setup('Allegis Chatter Only', 'Draft', 'Recruiter');

        // Ensure data was setup properly
        system.assertEquals(oppr.StageName, 'Qualifying');

        // Set assert compare
        Boolean isError = false;

        Test.startTest();
        System.runas(usr) {
            try {
                // Get Allocated reqs.
                // Should error, since 'Allegis Chatter Only doesn't have access to OpportunityTeamMember.
	            List<OpportunityTeamMember> options = AllocatedReqController.getAllocatedOptions();
                
                // If the test gets here, it shouldn't have. Fail the test.
                isError = false;
            }
            catch (Exception e) {
                // The above test should've errored. If it gets here, success!
                isError = true;
            }
        }

        // Assert
        System.assert(isError);
        Test.stopTest();
    }
    
    @isTest
    Static void getContactActivityTest() {
        // Setup data
        Contact c = TestDataHelper.createContact();
        
        String[] cIds = new String[]{c.Id};
        
        AllocatedReqController.getContactActivity(cIds);
    }
}