public class AlternateDeliveryCenterController 
{
    public Boolean accessFlag{get;set;}
    public AlternateDeliveryCenterController(ApexPages.StandardSetController controller)
    {
        accessFlag = false;
        List<GroupMember> groupMemberList = [SELECT Id,GroupId,UserOrGroupId 
                                             FROM GroupMember 
                                             WHERE Group.DeveloperName = 'Aerotek_Delivery_Queue_memebers' AND UserOrGroupId =: UserInfo.getUserId()];
        if(!groupMemberList.isEmpty())
        {
            accessFlag = true;
        }
    }
}