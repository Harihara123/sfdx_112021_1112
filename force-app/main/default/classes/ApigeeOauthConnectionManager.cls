/**
 * Author: Ajay Panachickal
 * 23-Jun-2016
 * 
 * Class manages OAuth connections to a given endpoint defined by the custom setting object name.
 */
public class ApigeeOauthConnectionManager {
    ApigeeOAuthSettings__c settings;

    /**
     * Constructor 
     */
    public ApigeeOauthConnectionManager(String settingKey) {
         settings = ApigeeOAuthSettings__c.getValues(settingKey);
    }
    
    /**
     * Method refreshes access token. Since we set expiry time to 75% of actual, this is still preemptive.
     */
    @Future(callout = true)
    private static void refreshAccessToken(String service) {
        ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues(service);
        // Create request and connect
        HttpRequest req = new HttpRequest();
		req.setEndpoint(serviceSettings.Token_URL__c);
		req.setMethod('POST');
        String authHeader = EncodingUtil.base64Encode(Blob.valueOf(serviceSettings.Client_Id__c + ':' + serviceSettings.Client_Secret__c));
        System.debug(authHeader);
        req.setHeader('Authorization', authHeader);
        
        HttpResponse resp = new Http().send(req);
        
        // Parse response JSON to get access token and expiry.
        String accessToken;
        JSONParser parser = JSON.createParser(resp.getBody());
        while (null != parser.nextToken()) {
            if (parser.getCurrentName() == 'accessToken') {
                parser.nextToken();
                accessToken = parser.getText();
                serviceSettings.OAuth_Token__c = accessToken;
            } else if (parser.getCurrentName() == 'expiresIn') {
                parser.nextToken();
                Integer seconds = Integer.valueOf(parser.getText());
                // Setting next refresh to 75% of actual received expiry time so we do not actually hit the point where the token is not valid.
                // Choice of 75% is just arbitrary.
                Datetime newExpiry = System.now().addSeconds(Integer.valueOf(seconds * 0.75));
                serviceSettings.Token_Expiration__c = newExpiry;
            }
        }
        
        // Update the access token and expiration to settings object.
        update serviceSettings;
    }
    
    public HttpRequest getServiceRequest() {
        if ((settings.Token_Expiration__c == null) || (settings.Token_Expiration__c < System.now())) {
            refreshAccessToken(settings.Name);
        }
        HttpRequest req = new HttpRequest();
		req.setEndpoint(settings.Service_URL__c);
		req.setMethod(settings.Service_Http_Method__c);
        
        req.setHeader('Authorization', 'Bearer ' + settings.OAuth_Token__c);
        req = setServiceHeader(req, settings.Service_Header_1__c);
        req = setServiceHeader(req, settings.Service_Header_2__c);
        req = setServiceHeader(req, settings.Service_Header_3__c);
        
        return req;
    }
    
    private HttpRequest setServiceHeader(HttpRequest req, String setting) {
        if (!String.isBlank(setting)) {
            String[] hdr = setting.split(':');
            req.setHeader(hdr[0], hdr[1]);
        }
        return req;
    }
}