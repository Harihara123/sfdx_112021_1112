public class AppPrioritizationEmailController  {

	public TalentFitment__c talentFitmentRec{get;set;}
	public Contact contactDetails {get;set;}
    Static String CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID = 'APPPRIORITY/AppPrioritizationEmailController';
	Static String CONNECTED_LOG_CLASS_NAME = 'AppPrioritizationEmailController';

	public TalentFitment__c getTalentFitmentDetails() {
		String skillDetails = '';
        List<TalentFitment__c> tft = new List<TalentFitment__c>();
        try {
            tft = [SELECt Id,ApplicantId__c,Matched_Candidate_Status__c,Matched_Desired_Pay__c,Matched_Last_Activity__c,Matched_Location_Preference__c,
                        Matched_Opportunities__c,Score__c,Status__c,API_Skills__c FROM TalentFitment__c WHERE Id =:talentFitmentRec.Id];
			if(tft.size() >0){
				if(String.isNotBlank(tft[0].API_Skills__c)) {
					List<String> skills = tft[0].API_Skills__c.split(';');
					for(String skill:skills){
						List<String> skillvalues = skill.split(':');	
						if(String.isNotBlank(skillDetails)) {
							skillDetails += ', '+skillvalues[0];
						} else {
    
							skillDetails = skillvalues[0];
						}
					}
				}
				tft[0].API_Skills__c = skillDetails;
			}
        } catch(Exception ex) {            
            ConnectedLog.LogException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'getTalentFitmentDetails',talentFitmentRec.Id ,'','', ex);	            
        }
		return tft[0];

	}

	
	public Event getEvent(){
		
		System.debug('ApplicationEmailTemplateController '+talentFitmentRec);
        List<Event> event = new List<Event>();
            try {
            event = [SELECT Id, Job_Posting__c,WhoId, who.FirstName,who.LastName,Job_Posting__r.Owner.Profile.Name,
                    Job_Posting__r.Job_Title__c, Job_Posting__r.Owner.Name,Job_Posting__r.Source_System_id__c,Job_Posting__r.Name,
                    Job_Posting__r.Opportunity__r.OpCo__c,Job_Posting__r.Opportunity__r.Name,Job_Posting__r.Opportunity__r.StageName,
                    Job_Posting__r.Opportunity__r.Req_Worksite_Street__c,Job_Posting__r.Opportunity__r.Req_Worksite_City__c,
                    Job_Posting__r.Opportunity__r.Req_Worksite_State__c,Job_Posting__r.Opportunity__r.Req_Worksite_Postal_Code__c,
                    Job_Posting__r.Opportunity__r.Req_Worksite_Country__c,Job_Posting__r.Opportunity__r.StreetAddress2__c,
                    Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c, Job_Posting__r.Opportunity__r.CreatedDate,
                    Job_Posting__r.Opportunity__r.Bill_Rate_max_Normalized__c,Job_Posting__r.Opportunity__r.Req_Duration__c,
                    Job_Posting__r.Opportunity__r.Req_Work_Remote__c,Job_Posting__r.Opportunity__r.Req_Can_Use_Approved_Sub_Vendor__c,
                    Job_Posting__r.Opportunity__r.Req_Job_Title__c,Job_Posting__r.Opportunity__r.Opportunity_Num__c,
                    Job_Posting__r.Opportunity__r.Account.Name,Job_Posting__r.Opportunity__r.Account.Desired_Salary__c,
                    Job_Posting__r.Opportunity__r.ORIGINATION_SYSTEM_OPPORTUNITY_NAME__c,Vendor_Application_ID__c,
					Job_Posting__r.Opportunity__r.Req_Division__c
                    FROM Event WHERE Id=: getTalentFitmentDetails().ApplicantId__c];
            System.debug('Event:'+event);
		    if(event.size() > 0 && event[0].WhoId <> null){
				contactDetails = [SELECT Id,AccountId,Name,CS_Salary__c,CS_Desired_Salary_Rate__c,CS_Geo_Pref_Comments__c FROM Contact WHERE Id=: event[0].whoId];
		    }

            for(Event evt:event) {
                System.debug('evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c'+evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c);
                if(String.isNotBlank(evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c) && evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c != '[]' ) {
                    //String skillsList ='{'+ evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c + '}';
                    List<EntSkillsWrapper> skillsList = (List<EntSkillsWrapper>)JSON.deserialize(evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c, List<EntSkillsWrapper>.class);
                    evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c = parseSkills(skillsList);
    
                }
            }
    
    
            System.debug('event[0]:'+event[0]);
            } catch(Exception ex) {
                
                ConnectedLog.LogException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'getEvent',talentFitmentRec.Id ,'','', ex);	            
                return new Event();
            }
		return event[0];
	}

	public List<Opportunity> getOppList() {
		List<Id> oppIds = new List<Id>();
		List<Opportunity> oppList = new List<Opportunity>();
        try {
            if(!String.isEmpty(getTalentFitmentDetails().Matched_Opportunities__c))
                oppIds = getTalentFitmentDetails().Matched_Opportunities__c.split(',');
    
            if(oppIds.size() > 0) {
                oppList = [Select Id,OpCo__c,Name,StageName,Req_Worksite_City__c,Req_Worksite_State__c,Req_Worksite_Postal_Code__c,
                        Req_Worksite_Country__c,EnterpriseReqSkills__c,CreatedDate,Bill_Rate_max_Normalized__c,Req_Duration__c,Req_Work_Remote__c,Req_Can_Use_Approved_Sub_Vendor__c,
                        Owner.Name,Opportunity_Num__c,Req_Job_Title__c,Account.Name,Req_Division__c FROM Opportunity WHERE Id IN : oppIds ];
            
                for(Opportunity opp:oppList) {
                    if(opp.EnterpriseReqSkills__c != null && (opp.EnterpriseReqSkills__c != '[]' && opp.EnterpriseReqSkills__c != '')) {
                    //if((opp.EnterpriseReqSkills__c != '[]' || opp.EnterpriseReqSkills__c != '')) {
                        //String skillsList ='{'+ evt.Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c + '}';					

                        List<EntSkillsWrapper> skillsList = (List<EntSkillsWrapper>)JSON.deserialize(opp.EnterpriseReqSkills__c, List<EntSkillsWrapper>.class);
                        //System.debug('Skill list in JSON-----'+skillsList);
						opp.EnterpriseReqSkills__c = parseSkills(skillsList);
    
                    } else {
                        opp.EnterpriseReqSkills__c = 'N/A';
                    }
                }		
                
            }
            System.debug('oppList:'+oppList);
        }catch(Exception ex) {
            
            ConnectedLog.LogException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'getOppList',talentFitmentRec.Id,'','', ex);	            
            return oppList;
        }
		return oppList;

	}

	public String parseSkills(List<EntSkillsWrapper> skillList) {
		String topSkills = '';
		String secondarySkills = '';
		String finalSkills = '';
		for(EntSkillsWrapper skill:skillList) {
			if(skill.favorite) {
				if(!String.isEmpty(topSkills) ) {
					topSkills += ', '+skill.name;
				}else {
					topSkills = skill.name;
				}

			} else {
				if(!String.isEmpty(secondarySkills) ) {
					secondarySkills += ', '+skill.name;
				}else {
					secondarySkills = skill.name;
				}
			}

		}
		if(!String.isEmpty(topSkills) && !String.isEmpty(secondarySkills))
			finalSkills = topSkills +', '+secondarySkills;
		else if(!String.isEmpty(topSkills) && String.isEmpty(secondarySkills))
			finalSkills = topSkills;
		else if(!String.isEmpty(secondarySkills) && String.isEmpty(topSkills))
			finalSkills = secondarySkills;
		else{
			finalSkills = 'N/A';
		}
		 return finalSkills;
	}

	public class EntSkillsWrapper {
	
		public String name;
		public Boolean favorite;

		
		public EntSkillsWrapper(String name, Boolean favorite) {
			this.name = name;
			this.favorite = favorite;
		}
	
	}
}