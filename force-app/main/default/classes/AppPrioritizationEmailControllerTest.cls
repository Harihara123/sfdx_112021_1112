@isTest

public class AppPrioritizationEmailControllerTest  {

	@testSetup static void testData() {
	DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        	insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='True');
        insert DRZSettings1;
		List<Opportunity> lstOpportunity = new List<Opportunity>(); 
		Account talAcc = CreateTalentTestData.createTalent();
		Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
		Job_Posting__c jobPosting = null;
        TestData TdAcc = new TestData(1);
        testdata tdcont = new Testdata();
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        String accName = '';
        Opportunity NewOpportunity = null;
		String sourceSystmId= '252525';
        for(Account Acc : TdAcc.createAccounts()) {                     
            accName = 'New Opportunity'+Acc.name;
            NewOpportunity = new Opportunity( Name =  accName, LDS_Account_Name__c = Acc.id, Accountid = Acc.id,                
            RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
            Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
            Customers_Application_or_Project_Scope__c = 'Precisely Defined',
            Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
            Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
            Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,
			Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly',
			EnterpriseReqSkills__c=	'[{\'name\':\'Biology\',\'favorite\':true},{\'name\':\'Quality control\',\'favorite\':false}]');  

            lstOpportunity.add(NewOpportunity);
            break;
		}
        insert lstOpportunity;
			
        // Ceate Job_Posting__c
        jobPosting = new Job_Posting__c(Currency_Code__c='111', 
        Delete_Retry_Flag__c=false, Job_Title__c = 'testJobTitle',Opportunity__c=NewOpportunity.Id, Posting_Detail_XML__c='Sample Description',
        Private_Flag__c=false,RecordTypeId=Utility.getRecordTypeId('Job_Posting__c','Posting'),Source_System_id__c='R.'+sourceSystmId,State__c='MD');

        insert jobPosting;
		//System.debug('Opportunity:'+jp.Opportunity__c);
		List<Account> accList = TdAcc.createAccounts();
		Order order = new Order();
		order.AccountId = accList[0].Id;
		order.OpportunityId = jobPosting.Opportunity__r.Id;
		order.ShipToContactId = talCont.Id;
		order.RecordTypeId = Schema.getGlobalDescribe().get('Order').getDescribe().getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
		order.Unique_Id__c = String.valueOf(talCont.Id)+String.valueOf(jobPosting.Opportunity__r.Id);
		order.Status = 'Applicant';
        
		order.EffectiveDate =  Date.today();
		order.Has_Application__c = true;
		

		//insert order;

		Event event = new Event();
		 
		//event.WhatId = order.Id;
		event.WhoId = talCont.Id;
		event.Application_Id__c = 'CS_Application_233';
		event.Inviter_Full_Name__c = 'Test Application';
		//event.JB_Source_ID__c = submittal.jbSourceId;
		event.Vendor_Application_ID__c = 'CS_Application_233';
		event.Vendor_Segment_ID__c = 'ved1111';
		event.Job_Posting__c = jobPosting.Id;
		event.DurationInMinutes = 1;
		event.StartDateTime = DateTime.now();
		event.Subject = order.Status;
		event.Type = order.Status;
		event.Activity_Type__c  = order.Status;
		event.Source__c = 'Test Name';
		//event.Feed_Source_ID__c = submittal.vendorId;
		event.ECID__c = '';
		event.ICID__c = '';			
		//event.OwnerId = '005240000038GjfAAE';            
		event.Job_Posting_Owner_Email__c = jobPosting.Owner.email;//STORY S-113884 Added Job posting Owner's Email to event Email by Siva 1/23/2019
        
		insert event; 

		TalentFitment__c tft = new TalentFitment__c();
		tft.ApplicantId__c = event.Id;
		tft.Status__c = 'Completed';
		tft.Recruiter__c = event.Job_Posting__r.OwnerId;
		tft.Applicant_Name__c = event.Who.Name;
		tft.Score__c = 0.81;
		tft.Matched_Opportunities__c = lstOpportunity[0].Id+','+lstOpportunity[0].Id;
		tft.API_Skills__c = 'cherry picker:0.23608594;forklift:0.12647332;clamp:0.12379681;material handling:0.10347272;pallet jack:0.09570494;ups system:0.08055955;stand up:0.05180998;fedex:0.04790829;shipping and receiving:0.04186243;inventory control:0.03796453';
		tft.Email_Subject__c = '(Good Fit For Req) Connected Application From amit testing';
		tft.Matched_Candidate_Status__c = 'Candidate';

		insert tft;

	}

	static testMethod void  testEmailMethod() {
		Test.startTest();
		List<TalentFitment__c> tftList = [SELECT Id,ApplicantId__c,Status__c,Recruiter__c,Applicant_Name__c,Score__c,Matched_Opportunities__c,
		API_Skills__c,Email_Subject__c,Matched_Candidate_Status__c FROM TalentFitment__c LIMIT 10];

		
		AppPrioritizationEmailController controller = new AppPrioritizationEmailController();
		List<AppPrioritizationEmailController.EntSkillsWrapper> skillList = new List<AppPrioritizationEmailController.EntSkillsWrapper>();
		AppPrioritizationEmailController.EntSkillsWrapper skill1 = new AppPrioritizationEmailController.EntSkillsWrapper('Java',true);
		skillList.add(skill1);
		AppPrioritizationEmailController.EntSkillsWrapper skill2 = new AppPrioritizationEmailController.EntSkillsWrapper('java script',false);
		skillList.add(skill2);
		AppPrioritizationEmailController.EntSkillsWrapper skill3 = new AppPrioritizationEmailController.EntSkillsWrapper('java Developer',true);
		skillList.add(skill3);
		AppPrioritizationEmailController.EntSkillsWrapper skill4 = new AppPrioritizationEmailController.EntSkillsWrapper('java8',false);
		skillList.add(skill4);

		
		controller.parseSkills(skillList);
		controller.talentFitmentRec = tftList[0];
		controller.getTalentFitmentDetails();
		Event evt = controller.getEvent();
		List<Opportunity> oppList = controller.getOppList();
		Test.stopTest();
		System.assertEquals(1,oppList.size());
		//System.assertEquals(evt.Id,event.Id);

	}

	static testMethod void testEmailLinkTracking() {
        Test.startTest();
		List<TalentFitment__c> tftList = [SELECT Id FROM TalentFitment__c LIMIT 10];
		List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 10];
		List<Contact> talentList = [SELECT Id FROM Contact LIMIT 10];

		
		AppPriorityEmailLinkTracking.saveUserEmailActions('Profile',tftList[0].Id,oppList[0].Id,talentList[0].Id);
		
		List<ApplicantPriorityTracking__c> appTracking = [SELECT Id FROM ApplicantPriorityTracking__c LIMIT 10];
		System.assertEquals(1,appTracking.size());
        Test.stopTest();
	}

	static testMethod void testApplicantTrackingTrigger() {
		
        Test.startTest();
		List<TalentFitment__c> tftList = [SELECT Id FROM TalentFitment__c LIMIT 10];
		List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 10];
		List<Contact> talentList = [SELECT Id FROM Contact LIMIT 10];
		List<Job_Posting__c> jobPosting = [SELECT Id FROM Job_Posting__c LIMIT 10];

		Test.enableChangeDataCapture();

		ApplicantPriorityTracking__c track = new ApplicantPriorityTracking__c();
		track.ClickType__c = 'Profile';
		track.ClickTimeStamp__c = System.now();
		track.TalentFitment__c = tftList[0].Id;
		track.JobPosting__c = jobPosting[0].Id;
		track.Opportunity__c = oppList[0].Id;
		
		insert track;
		Test.getEventBus().deliver();

		track.Recruiter__c = UserInfo.getUserId();
		update track;
		Test.getEventBus().deliver();

		delete track;
		Test.getEventBus().deliver();
        Test.stopTest();
	}

	static testMethod void testTalentFitmentTrigger() {
        Test.startTest();
		List<Event> evtList = [SELECT Id,Job_Posting__r.OwnerId,whoId,Who.Name FROM Event LIMIT 10];
		if(evtList.size() > 0) {
			TalentFitment__c tft = new TalentFitment__c();
			tft.ApplicantId__c = evtList[0].Id;
			tft.Status__c = 'New';
			tft.Recruiter__c = evtList[0].Job_Posting__r.OwnerId;
			tft.Applicant_Name__c = evtList[0].Who.Name;
			tft.Applicant_Source__c = 'ZIP';
			insert tft;

			tft.Applicant_Source__c = 'PhenomPeople';
			update tft;

			delete tft;
		}
        Test.stopTest();
	}
}