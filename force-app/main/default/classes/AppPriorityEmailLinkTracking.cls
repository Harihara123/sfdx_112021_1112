public class AppPriorityEmailLinkTracking  {
	Static String CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID = 'APPPRIORITY/AppPriorityEmailLinkTracking';
	Static String CONNECTED_LOG_CLASS_NAME = 'AppPriorityEmailLinkTracking';

	@AuraEnabled
	public static void saveUserEmailActions(String linkType,String tftId,String oppId,String talentId) {
		System.debug('linkType:'+linkType);
		List<ApplicantPriorityTracking__c> appTrackList = new List<ApplicantPriorityTracking__c>();
		Id tftIds= Id.valueOf(tftId);
		try {
			//Check if there are existing track records			
			Boolean isExisting = fetchTrackingDetailsCount(linkType,oppId,talentId,tftIds);			
			if(!isExisting) {
				ApplicantPriorityTracking__c appTrack = new ApplicantPriorityTracking__c();
				//Fetch Talent fitment & Applicant Details
				System.debug('tftId-----------------------'+tftIds);
				TalentFitment__c tftRec = fetchtalentFitmentDetails(tftIds);
				Event evt = fetchEventDetails(tftRec);

				appTrack.ClickType__c = linkType;
				appTrack.Opportunity__c = evt.Job_Posting__r.Opportunity__c;
				appTrack.Talent__c = evt.WhoId;
				appTrack.Recruiter__c = evt.Job_Posting__r.OwnerId;
				appTrack.TalentFitment__c = tftIds;
				appTrack.ClickTimeStamp__c = System.now();

				if(linkType == 'Profile' || linkType == 'Applicant' || linkType == 'JobPosting') {
					appTrack.JobPosting__c = evt.Job_Posting__c;
					appTrack.Submittal__c = evt.WhatId;
				}				
				
				appTrackList.add(appTrack);			
			}
			System.debug('appTrackList:'+appTrackList);
			if(appTrackList.size() > 0) {
				insert appTrackList;
			}
		} catch(Exception ex) {
			ConnectedLog.LogException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'saveUserEmailActions',tftIds,'', ex);	
		}
	}

	private static TalentFitment__c fetchtalentFitmentDetails(Id tftId) {

		TalentFitment__c tft = new TalentFitment__c();
        tft = [SELECT Id,ApplicantId__c FROM TalentFitment__c WHERE Id =:tftId];
		return tft;
	}

	private static Event fetchEventDetails(TalentFitment__c tftRec){
		List<Event> eventList = new List<Event>();

		eventList = [SELECT Id, WhatId,WhoId, who.FirstName,who.LastName,Job_Posting__r.Owner.Profile.Name,
				Job_Posting__r.Job_Title__c, Job_Posting__r.Owner.Name,Job_Posting__r.Source_System_id__c,Job_Posting__r.Name,
				Job_Posting__r.Opportunity__r.OpCo__c,Job_Posting__r.Opportunity__r.Name,Job_Posting__r.Opportunity__r.StageName,
				Job_Posting__r.Opportunity__r.Req_Worksite_Street__c,Job_Posting__r.Opportunity__r.Req_Worksite_City__c,
				Job_Posting__r.Opportunity__r.Req_Worksite_State__c,Job_Posting__r.Opportunity__r.Req_Worksite_Postal_Code__c,
				Job_Posting__r.Opportunity__r.Req_Worksite_Country__c,Job_Posting__r.Opportunity__r.StreetAddress2__c,
				Job_Posting__r.Opportunity__r.EnterpriseReqSkills__c, Job_Posting__r.Opportunity__r.CreatedDate,
				Job_Posting__r.Opportunity__r.Bill_Rate_max_Normalized__c,Job_Posting__r.Opportunity__r.Req_Duration__c,
				Job_Posting__r.Opportunity__r.Req_Work_Remote__c,Job_Posting__r.Opportunity__r.Req_Can_Use_Approved_Sub_Vendor__c,
				Job_Posting__r.Opportunity__r.Req_Job_Title__c,Job_Posting__r.Opportunity__r.Opportunity_Num__c,
				Job_Posting__r.Opportunity__r.Account.Name,Job_Posting__r.Opportunity__r.Account.Desired_Salary__c,
				Job_Posting__r.Opportunity__r.ORIGINATION_SYSTEM_OPPORTUNITY_NAME__c,Vendor_Application_ID__c,
				Job_Posting__r.Opportunity__r.Req_Division__c
				FROM Event WHERE Id=: tftRec.ApplicantId__c];
           
		if (eventList.size()>0) {
			return eventList[0];
		} else {
			return null;
		}
	}

	private static Boolean fetchTrackingDetailsCount(String linkType,String oppId,String talentId,String tftId) {
		Boolean isExisting = false;
		List<ApplicantPriorityTracking__c> trackRecords = new List<ApplicantPriorityTracking__c>();
		trackRecords = [SELECT Id FROM ApplicantPriorityTracking__c WHERE 
			ClickType__c =: linkType AND Opportunity__c =: oppId AND Talent__c =: talentId AND TalentFitment__c =: tftId LIMIT 1];
		
		if(trackRecords != null && trackRecords.size() > 0) {
			isExisting = true;
		} else {
			isExisting = false;
		}

		return isExisting;
	}
}