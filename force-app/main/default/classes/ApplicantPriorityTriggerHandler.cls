public class ApplicantPriorityTriggerHandler  {
	String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, ApplicantPriorityTracking__c> oldMap = new Map<Id, ApplicantPriorityTracking__c>();
    Map<Id, ApplicantPriorityTracking__c> newMap = new Map<Id, ApplicantPriorityTracking__c>();

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<ApplicantPriorityTracking__c> newRecs) {
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<ApplicantPriorityTracking__c> newRecs) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
		 if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
             if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ApplicantPriorityTriggerHandler', 'OnAfterInsert', 
                    'Triggered ' + newRecs.size() + ' ApplicantPriorityTracking__c records: ' + CDCDataExportUtility.joinObjIds(newRecs));
             }
            PubSubBatchHandler.insertDataExteractionRecord(newRecs, 'ApplicantPriorityTracking__c');
         }
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, ApplicantPriorityTracking__c>oldMap, Map<Id, ApplicantPriorityTracking__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, ApplicantPriorityTracking__c>oldMap, Map<Id, ApplicantPriorityTracking__c>newMap) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
       if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
           if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ApplicantPriorityTriggerHandler', 'OnAfterUpdate', 
                    'Triggered ' + newMap.values().size() + ' ApplicantPriorityTracking__c records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
           }
            PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'ApplicantPriorityTracking__c');
        }
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, ApplicantPriorityTracking__c>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, ApplicantPriorityTracking__c>oldMap) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ApplicantPriorityTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' ApplicantPriorityTracking__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'ApplicantPriorityTracking__c');
        }
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, ApplicantPriorityTracking__c>oldMap) {
    }

}