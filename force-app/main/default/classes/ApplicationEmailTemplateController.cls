public class ApplicationEmailTemplateController  {
    public Event eventRec{get;set;}
    public Contact getcontact(){
        System.debug('ApplicationEmailTemplateController '+eventRec);
       // Event evnt= [Select Who.Id from Event where Id=:eventRec];
        Contact contact = [Select Id, CS_Salary__c,CS_Desired_Salary_Rate__c,CS_Geo_Pref_Comments__c from Contact where Id =:eventRec.WhoId];
        return contact;
    }
}