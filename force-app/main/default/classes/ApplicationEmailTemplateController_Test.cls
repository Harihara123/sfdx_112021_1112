@isTest (SeeAllData=false)
public class ApplicationEmailTemplateController_Test  {
    public static testMethod void testPushDispositionDetailsToRWS() {

      Test.startTest();
      Contact contc = ApplicationManagementTestData.createCSContact();
      insert contc;
      Event evnt = ApplicationManagementTestData.createEvent(contc);
      insert evnt;

      ApplicationEmailTemplateController appEmail = new ApplicationEmailTemplateController();
      appEmail.eventRec = evnt;
      Contact contc1 = appEmail.getcontact();
      Test.stopTest();

      System.assert(contc1 != null);
    }
}