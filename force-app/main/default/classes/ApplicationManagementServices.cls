@RestResource(urlMapping='/Application/Disposition/V1/*')
global without sharing class ApplicationManagementServices  {

    global class DispositionResponse{
       public String hrEmplId;
       public String dispositionCode;
       public String dispositionReasonCode;
       public List<Map<String,Object>> applicationIdList;

       public DispositionResponse(String hreId, String disC, String disR, List<Map<String,Object>> appctions){
        this.hrEmplId = hreId;
        this.dispositionCode = disC;
        this.dispositionReasonCode = disR;
        this.applicationIdList = appctions;
       }
    }

    /***
      ** This method is called on an Http get method, expects an event id. It returns dispositino detials in a JSON string format.
    ***/
    @HttpGet
    global static void getDisposition() {
            
        Map<String, String> params = RestContext.request.params;
        String eventId = RestContext.request.params.get('eventId');
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(getDispositionDetails(eventId)));
     }

     private static DispositionResponse getDispositionDetails(String eventId){
        
        DispositionResponse result;
        String respStatus = 'Success';
        Map<String,Object> dispositionData = new Map<String,Object>();
        try{
           

            List<Event> events = [select id,Disposition_Code__c,Disposition_Reason__c,Vendor_Application_ID__c,Feed_Source_ID__c,LastModifiedById from Event where Id =:eventId];
            if(events != null && events.size()>0){
                List<Map<String,Object>> applications = new List<Map<String,Object>>();

                Event event = events[0];
                User user = [Select Peoplesoft_Id__c from User where id=:event.LastModifiedById];


                Map<String,String> application = new Map<String,String>();
                application.put('vendorApplicationId',(event.Vendor_Application_ID__c != null ? event.Vendor_Application_ID__c : ''));
                application.put('vendorId',(event.Feed_Source_ID__c != null ? event.Feed_Source_ID__c : ''));
                applications.add(application); 


                result = new DispositionResponse((user.Peoplesoft_Id__c != null ? user.Peoplesoft_Id__c : ''),
                            (event.Disposition_Code__c != null ? event.Disposition_Code__c : ''),
                            (event.Disposition_Reason__c != null ? event.Disposition_Reason__c : ''),
                            applications);
            }     
        }catch(Exception e){
            
            System.debug('getDispositionDetails '+e.getMessage());
            throw e;
        }
       // dispositionData.put('status',respStatus);
        return result;
    }


}