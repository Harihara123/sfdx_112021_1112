@isTest(seealldata = false)
public class ApplicationManagementTestData  {
    public static Job_Posting__c createJobPostingObject(String sourceSystmId) {
        Job_Posting__c jobPosting = null;
        try{
             if(sourceSystmId == null || sourceSystmId.length()==0){
                sourceSystmId= '121112';
             }
             List<Opportunity> lstOpportunity = new List<Opportunity>(); 
             TestData TdAcc = new TestData(1);
             testdata tdcont = new Testdata();
             string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
             String accName = '';
             Opportunity NewOpportunity = null;
             for(Account Acc : TdAcc.createAccounts()) {                     
                    accName = 'New Opportunity'+Acc.name;
                    NewOpportunity = new Opportunity( Name =  accName, LDS_Account_Name__c = Acc.id, Accountid = Acc.id,                
                    RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                    Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                    Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                    Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                    Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                    Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly');  

                    lstOpportunity.add(NewOpportunity);
                    break;
            }
            insert lstOpportunity;
            // Ceate Job_Posting__c
            jobPosting = new Job_Posting__c(Currency_Code__c='111', 
            Delete_Retry_Flag__c=false, Job_Title__c = 'testJobTitle',Opportunity__c=NewOpportunity.Id, Posting_Detail_XML__c='Sample Description',
            Private_Flag__c=false,RecordTypeId=Utility.getRecordTypeId('Job_Posting__c','Posting'),Source_System_id__c='R.'+sourceSystmId,State__c='MD');

            insert jobPosting;

            return jobPosting;
        }catch(Exception e){
            System.debug('Could not insert JOBPOSTINGOBJECT '+e.getMessage());
        }
        return jobPosting;
    }
    public static contact createCSContact()
     {
           return new Contact(CS_Desired_Salary_Rate__c='10040',CS_Geo_Pref_Comments__c='Elkridge, MD',CS_Highest_Education__c='',CS_Salary__c=100000,LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
     }
    public static Event createEvent(Contact contact){
        return new Event(subject='Meeting - ',DurationInMinutes = 1,ActivityDateTime = system.now()+1,WhoId=contact.Id);
    }
}