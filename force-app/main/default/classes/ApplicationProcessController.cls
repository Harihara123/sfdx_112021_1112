public class ApplicationProcessController {
public static final string aero='Aerotek';
public static final string tek='TEKsystems';
public static final string astonCarter='Aston Carter';
public static final string excp='Exception';
public static final string dedupeExcp='DeDupe Exception';
public static final string talUpExcp='Talent Upsert Exception';
public static final string resUpExcp='Resume Upload Exception';
public static final string talentUpserted='Talent Upserted';
public static final string resUploaded='Resume Uploaded';
public static final string ordEvIUpserted ='Order-Event Upserted';
public static final string ordeEvExcp='Order/Event Upsert Exception';

    @InvocableMethod(label='Process_Application' description='Process the payload stored in Application Staging Object')
    public static void getApplication(List<Id> ids) {
        for(Id i : ids){
            ApplicationProcessController.processApplication(String.valueOf(i));   
        }
        
    }

    
    @future(callout = true)
    public static void processApplication(String recordId){
        
        Application_Staging__c application;
		List<Application_Staging__c> applicationList  = new List<Application_Staging__c>();
        Attachment attach;
        InvocableAttachmentParser.ParsedInfo parsedData;
        String name = '';
        List<String> emails = new List<String>();
        List<String> phones = new List<String>();
        Job_Posting__c jp;
		String vendorId='';

        List<List<Sobject>> deDupeResult = new List<List<Sobject>>();
        
        try{
                application =[SELECT Id, Name, JSON_Payload__c, Content_Type__c, 
                                                            Exception_Logs__c, File_Name__c, Source__c, Candidate_Last_Name__c, Candidate_First_Name__c, 
                                                            Status__c, Candidate_Phone_Type__c, Candidate_Email__c, Candidate_Phone__c, 
                                                        Candidate_LinkedIN_URL__c, Found_Duplicate__c, DeDupe_TimeStamp__c, Vendor_ID__c, 
                                                        Duplicate_AccountID__c, Duplicate_ContactID__c, Talent_Upsert_Account_Id__c, Order_Id__c,
                                                        Talent_Upsert_TimeStamp__c, Talent_Upsert_Contact_Id__c, Talent_Document_Id__c, Event_Id__c,
                                                        Order_Upsert_TimeStamp__c, Resume_Upload_TimeStamp__c, Attachment_ID__c,Replay_Eligible__c,Talent_Source__c,
                                                        RecruiterEmail__c, Recruiter__c, OwnerID, PostingId__c 
														FROM Application_Staging__c
                                                        where id=: recordId with SECURITY_ENFORCED];
        
             attach=[Select id, parentId, Name, body, ContentType from Attachment where parentId = :recordId];
                 parsedData = InvocableAttachmentParser.parseResumeForTalentUpdate(attach.Id);
            
            
           
            if(application.Exception_Logs__c == null){
                application.Exception_Logs__c = '';
            }
        }        
        catch(Exception e){
            //System.debug(e);
            
            application.Status__c =excp;
            
            application.Exception_Logs__c +='\n Exception::'+System.now()+':\n';
            
            application.Exception_Logs__c +=e.getStackTraceString()+'\n';
            
            application.DeDupe_TimeStamp__c = system.now();
			applicationList = new List<Application_Staging__c>();
			applicationList.add(application);
			SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, applicationList, false);
			Update securityDecision.getRecords();
           // update application;
        }
        Savepoint sp = Database.setSavepoint(); 
        try{
			
            if(application.Status__c != excp){
                
				// for emea
                name = (parsedData.contactInfo.firstname != null? parsedData.contactInfo.firstname:'');
                if(parsedData.contactInfo.lastname != null){
                    name=(name != ''?name+' ':'')+parsedData.contactInfo.lastname;
                }
                //Get email parameter         
                if(parsedData.contactInfo.email != null){
                    emails.add(parsedData.contactInfo.email);
                }
                if(parsedData.contactInfo.otheremail != null){
                    emails.add(parsedData.contactInfo.otheremail);
                }
                if(parsedData.contactInfo.workemail != null){
                    emails.add(parsedData.contactInfo.workemail);
                }
                //Get phone parameter         
                if(parsedData.contactInfo.mobilephone != null){
                    phones.add(parsedData.contactInfo.mobilephone);
                }
                if(parsedData.contactInfo.homephone != null){
                    phones.add(parsedData.contactInfo.homephone);
                }
                if(parsedData.contactInfo.otherphone != null){
                    phones.add(parsedData.contactInfo.otherphone);
                }

            deDupeResult = ApplicationProcessController.checkforDuplicate(name,phones,emails);
            if(deDupeResult.size()>0){
                application.Duplicate_ContactID__c = String.valueof(deDupeResult[0][0].get('Id'));
                application.Duplicate_AccountID__c = String.valueof(deDupeResult[0][0].get('AccountId'));
                application.Found_Duplicate__c = true;
            }
            
            application.DeDupe_TimeStamp__c = system.now();
            }
        }Catch(Exception e){
            
            Database.rollback(sp);
            
            application.Status__c =dedupeExcp;
            
            application.Exception_Logs__c +='\n DedupeException::'+System.now()+':\n';
           
            application.Exception_Logs__c +=e.getStackTraceString()+'\n';
            
            application.DeDupe_TimeStamp__c = system.now();
			//update application;
			applicationList = new List<Application_Staging__c>();
            applicationList.add(application);
			SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, applicationList, false);
			Update securityDecision.getRecords();
			
        }
        try{
           if(application.Status__c != excp && application.Status__c != dedupeExcp)
           {
			if(application.PostingID__c != Null && application.PostingID__c !=''){
				jp = getJobPosting(application.PostingID__c);
				if(jp != Null){
					if(jp.Req_Division__c ==aero){
						vendorId ='19';
					}else if(jp.Req_Division__c ==astonCarter){
						vendorId ='20';
					}else if(jp.Req_Division__c ==tek){
						vendorId ='21';
					}
				}
				
			}
            Map<String,String> talentUpsertResult = new Map<String,String>();
               talentUpsertResult = Candidate.doPost(ApplicationProcessController.prepareCandidateWrapperObjectForEMEA(parsedData,application,vendorId));                
                
            if(talentUpsertResult.get('Status') == 'Success'){
                application.Status__c = talentUpserted;
                application.Talent_Upsert_Account_Id__c = talentUpsertResult.get('candidateAccId');
                application.Talent_Upsert_Contact_Id__c = talentUpsertResult.get('candidateId');
            }else if(talentUpsertResult.get('Status') == 'Fail'){
                
				Database.rollback(sp);
                                               
                application.Status__c =talUpExcp;
                
                application.Exception_Logs__c +='\n Talent Upsert Exception::'+System.now()+':\n';
                application.Exception_Logs__c +=talentUpsertResult.get('description') +'\n';
                
            }
            application.Talent_Upsert_TimeStamp__c = system.now(); 
           }
        }Catch(Exception e){
            
            Database.rollback(sp);
            
            application.Status__c =talUpExcp;
           
            application.Exception_Logs__c +='\n Talent Upsert Exception::'+System.now()+':\n';
            application.Exception_Logs__c +=e.getStackTraceString()+'\n';
           
            application.Talent_Upsert_TimeStamp__c = system.now();
            //update application;
			applicationList = new List<Application_Staging__c>();
			applicationList.add(application);
			SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, applicationList, false);
			Update securityDecision.getRecords();
        }
        try{
            String TalentDocId='';
            if(application.Status__c == talentUpserted){
                                
               
                if(application.Source__c == 'Email'){ 
                   
                    if(deDupeResult.size()>0 ){
                        
                        deleteExistingTalentDocument(String.valueof(deDupeResult[0][0].get('AccountId'))); 
                    }else{
						createTalentExperience(application.Talent_Upsert_Account_Id__c,parsedData);
					}
                    
                  
                    TalentDocId = CreateTalentDocument(application.Talent_Upsert_Account_Id__c,'',attach.Name,false);            
                   
                    String attachmentId =  reParentAttachment(TalentDocId,attach.Id);
                    application.Talent_Document_Id__c = TalentDocId;
                    application.Attachment_ID__c = attachmentId;
                    application.Candidate_First_Name__c=parsedData.contactInfo.firstname!=null?parsedData.contactInfo.firstname:'';
                    application.Candidate_Last_Name__c=parsedData.contactInfo.lastname!=null?parsedData.contactInfo.lastname:'';
                }
                
                application.Resume_Upload_TimeStamp__c = system.now();
                application.Status__c =resUploaded;
            }
           
        }Catch(Exception e){
            Database.rollback(sp);
            
            application.Status__c =resUpExcp;
            application.Exception_Logs__c +='\n Resume Upload Exception::'+System.now()+':\n';
            application.Exception_Logs__c +=e.getStackTraceString() +'\n';
            
            application.Resume_Upload_TimeStamp__c = system.now();
			applicationList = new List<Application_Staging__c>();
			applicationList.add(application);
			SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, applicationList, false);
			Update securityDecision.getRecords();
            //update application;           
        }
		try{
            if(application.Status__c == resUploaded && jp!= NULL){
					
					Map<String,String> orderUpsertResult = ATSOrderRESTService.doPost(ApplicationProcessController.prepareOrderServiceWrapperObject(application,vendorId));
                        
					if(orderUpsertResult.get('Status') == 'Success'){
						application.Status__c = ordEvIUpserted;
						application.Order_Id__c = orderUpsertResult.get('SubmittalId');
						application.Event_Id__c = orderUpsertResult.get('appEventId');                    
					}else if(orderUpsertResult.get('Status') == 'Fail'){

						application.Status__c =ordeEvExcp;
						
						application.Exception_Logs__c +='\n Order Upsert Exception::'+System.now()+':\n';
						application.Exception_Logs__c +=orderUpsertResult.get('description')+'\n';
						
					}
				
				            
                application.Order_Upsert_TimeStamp__c = system.now();			
            }            
            //update application;
			applicationList.add(application);
			SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, applicationList, false);
			Update securityDecision.getRecords();		   
		}Catch(Exception e){
           
            application.Status__c = ordeEvExcp;
            
            application.Exception_Logs__c +='\n Order Upsert Exception::'+System.now()+':\n';
            application.Exception_Logs__c +=e.getStackTraceString()+'\n';
            
            application.Order_Upsert_TimeStamp__c = system.now();
            //update application;
			applicationList = new List<Application_Staging__c>();
			applicationList.add(application);
			SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, applicationList, false);
			Update securityDecision.getRecords();
		}       
      }
    
    public static List<List<SObject>> checkforDuplicate(String Name, List<String> phones, List<String> emails ){
        List<List<Sobject>> deDupeResult = TalentDedupeSearch.postDedupTalents(Name,emails, phones, '', '', new List<String>{'Talent'}, 'ONE',true);
        return deDupeResult;
    }
    
    

    public static  ATSCandidate prepareCandidateWrapperObjectForEMEA(InvocableAttachmentParser.ParsedInfo parsedData,Application_Staging__c application, String vendorId){
        
            //Set name
            ATSCandidate.ATSNameDetails nameObject = new ATSCandidate.ATSNameDetails();
            nameObject.firstName = (parsedData.contactInfo.firstname != null? parsedData.contactInfo.firstname:'');
            nameObject.lastName = (parsedData.contactInfo.lastname != null? parsedData.contactInfo.lastname:'');                
            //Set email
            ATSCandidate.ATSEmailDetails emailObject = new ATSCandidate.ATSEmailDetails();
            emailObject.email = (parsedData.contactInfo.email != null? parsedData.contactInfo.email:'');
            emailObject.workEmail = (parsedData.contactInfo.workemail != null? parsedData.contactInfo.workemail:'');
            emailObject.otheremail = (parsedData.contactInfo.otheremail != null? parsedData.contactInfo.otheremail:'');                
            //Set Phone       
            ATSCandidate.ATSPhoneDetails phoneObject = new ATSCandidate.ATSPhoneDetails();                
            phoneObject.phone = (parsedData.contactInfo.homephone != null? parsedData.contactInfo.homephone:'');        
            phoneObject.mobile =  (parsedData.contactInfo.mobilephone != null? parsedData.contactInfo.mobilephone:'');
            phoneObject.otherPhone =  (parsedData.contactInfo.otherphone != null? parsedData.contactInfo.otherphone:'');              
            //Set Address         
            ATSCandidate.ATSAddressDetails addressObject = new ATSCandidate.ATSAddressDetails();
            addressObject.city = (parsedData.contactInfo.city != null? parsedData.contactInfo.city:'');
            addressObject.country = (parsedData.contactInfo.country!=null && parsedData.contactInfo.country.text != null? parsedData.contactInfo.country.text:'');
            addressObject.state = (parsedData.contactInfo.state!=null && parsedData.contactInfo.state.text != null? parsedData.contactInfo.state.text:'');
            addressObject.street = (parsedData.contactInfo.addressline != null? parsedData.contactInfo.addressline:'');
            addressObject.postalCode = (parsedData.contactInfo.postalcode != null? parsedData.contactInfo.postalcode:'');
            
            if(addressObject.city =='' && addressObject.country =='' && addressObject.state =='' && addressObject.street =='' && addressObject.postalcode ==''){
                addressObject = null;
            }
            
            ATSCandidate candWrapper = new  ATSCandidate();
			if(String.isBlank(vendorId)){
				candWrapper.transactionsource='Email';	
			}else{
				candWrapper.transactionsource='equest';
				candWrapper.vendorId= vendorId;
				candWrapper.jbSourceId= application.Talent_Source__c;
			}    
            
            candWrapper.nameDetails = nameObject;
            candWrapper.emailDetails = emailObject;
            candWrapper.phoneDetails = phoneObject;
            candWrapper.mailingAddress = addressObject;
            //candWrapper.vendorId = 'EMEA Broadbean';
           // if(deDupeResult.size()==0 || deDupeResult.size() == null ){
		   if(!String.isBlank(application.Duplicate_ContactID__c) && !String.isBlank(application.Duplicate_AccountID__c)){
                candWrapper.talentSource = application.Talent_Source__c;   
            }
            
            if(parsedData.talentPrefsInternal != null){
                candWrapper.talentPrefInternal = parsedData.talentPrefsInternal;
            }
            if(parsedData.currentEmployer != null){
                    candWrapper.currentEmployer = parsedData.currentEmployer;
            }
            if(parsedData.talentOverview != null){
                    candWrapper.talentOverview = parsedData.talentOverview;
            }
            candWrapper.linkedInURL = (parsedData.contactInfo.linkedinurl != null? parsedData.contactInfo.linkedinurl:'');
                    
            //if(deDupeResult.size()>0){
			if(!String.isBlank(application.Duplicate_ContactID__c) && !String.isBlank(application.Duplicate_AccountID__c)){        
                candWrapper.sfId = application.Duplicate_ContactID__c;
                candWrapper.talentUpdate = 'true';
            }else{
                candWrapper.talentUpdate = 'false';
            }        
            
            return candWrapper;
            
    }

    public static String CreateTalentDocument(String AccntId,String ExternalSystem,String docName,Boolean OFFCCPFlag){
        Talent_Document__c talentDocumentObject = new Talent_Document__c();
        talentDocumentObject.Talent__c = AccntId; 
        talentDocumentObject.Resume_External_System__c =ExternalSystem;
        talentDocumentObject.Document_Name__c = docName;
        talentDocumentObject.Committed_Document__c = true;
        if(OFFCCPFlag == true){
            talentDocumentObject.Document_Type__c ='Resume - OFCCP';
        }else{
            talentDocumentObject.Document_Type__c ='Resume - Job Board';
        }
        insert talentDocumentObject;    
        return talentDocumentObject.id;
    }

    //This method creates Talent_Experience records for emea
    //w.r.t work and education data of attached resume
    public static void createTalentExperience(String talentId,InvocableAttachmentParser.ParsedInfo parsedData){
        
        List<SObject> addRecords = new List<SObject>();
        
        if(parsedData.workExList != null){
            for(Talent_Experience__c experience : parsedData.workExList) {
                experience.Talent__c = talentId;
                experience.Talent_Committed_Flag__c=true;
                addRecords.add(experience);
            }
        }
        
        if(parsedData.educationList != null){
            for (Talent_Experience__c experience : parsedData.educationList) {
                experience.Talent__c = talentId;
                experience.Talent_Committed_Flag__c=true;
                addRecords.add(experience);
            }
        }          
          
        
        if(addRecords.size()>0){
                DBUtils.InsertRecords(addRecords, False);  
        }                      
        
    }
    
    //This method checks for latest talent document
    //w.r.t duplicate contacts and delete
    public static void deleteExistingTalentDocument(String contAccntId){        
        list<Talent_Document__c> talentDocument=[select id, Mark_For_Deletion__c from Talent_Document__c where Document_Type__c='Resume - Job Board' and Talent__c=:contAccntId  ORDER BY CreatedDate DESC ];
        //need to discuss if more than one record of this type found , all will be deleted or only the latest one  
        if(talentDocument.size()>0){
                 delete talentDocument;
          }        
    }

    //This method create new attachment record for EMEA
    //to map the new talent_Document_id 
    public static String reParentAttachment(String TalentDocId, String attachmentId){
         
            Attachment attch=[Select id,name,body,ContentType from Attachment where id=:attachmentId];
            Attachment AttchNew=new Attachment();
            AttchNew.Body=attch.Body;
            AttchNew.Name=attch.Name;
            AttchNew.parentId=TalentDocId;
         
            AttchNew.ContentType=attch.ContentType;
            insert AttchNew;
            return AttchNew.Id;
        
    }

	public static ATSSubmittal prepareOrderServiceWrapperObject(Application_Staging__c application, String vendorId){
        Datetime dt = System.now();
		
		ATSSubmittal submittalWrapperObj = new ATSSubmittal();
                
        submittalWrapperObj.talentId = application.Talent_Upsert_Contact_Id__c;
        
        submittalWrapperObj.jobPostingId = application.PostingId__c;
        submittalWrapperObj.jbSourceId = application.Talent_Source__c;
        submittalWrapperObj.vendorId = vendorId;
        submittalWrapperObj.effectiveDate = dt.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'','UTC');
        submittalWrapperObj.transactionsource = 'EQUEST';
		submittalWrapperObj.appliedDate =  dt.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'','UTC');
        //submittalWrapperObj.appliedDate = String.valueOf(dt.month())+'/'+String.valueOf(dt.day())+'/'+String.valueOf(dt.year());
		submittalWrapperObj.isApplicantSubmittal = true;
        submittalWrapperObj.vendorName ='Portal AG';
				               
        return 	submittalWrapperObj;
	}

	public static Job_Posting__c getJobPosting(String jpID){
		List<Job_Posting__c> jpList = [SELECT Id, Name, OwnerId, Owner.email, Opportunity__c,
			          Opportunity__r.Id, Opportunity__r.AccountId, OFCCP_Flag__c, Req_Division__c 
			          FROM Job_Posting__c
			          where Name = :jpID with SECURITY_ENFORCED LIMIT 1 ];
		return jpList[0];
	}
}