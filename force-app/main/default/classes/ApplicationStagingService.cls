@RestResource(urlMapping='/Application/StagingRequest/V1')
// URL: /services/apexrest/Application/StagingRequest/V1

global class ApplicationStagingService {
    
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        
        String responseMessage ='';
		String errorMessage ='The following mandatory field(s) are not supplied in the input request:';
		Boolean validationPassed = true;
		Boolean duplicateRequest = false;
		List<String> missingFields = new List<String>(); 
        try{
            Blob body = req.requestBody;
            String requestJSONBody = body.toString();
            // system.debug('requestJSONBody-->'+requestJSONBody);
            
            ApplicationWrapper wrapObject = ApplicationWrapper.parse(requestJSONBody);
            String combinationKey = wrapObject.ApplicationData.VendorCandidateId +'-'+wrapObject.ApplicationData.PostingId;
			List<Application_Staging__c> stagingRecordList =[select id,Combination_Key__c,CreatedDate from Application_Staging__c where Combination_Key__c=: combinationKey AND CreatedDate = LAST_N_DAYS:1];

            if(stagingRecordList.size()>0){
				validationPassed = false;
				duplicateRequest = true;
			}

            if(wrapObject.VendorData.VendorID == '' || wrapObject.VendorData.VendorID == null){
				validationPassed = false;
				missingFields.add(' VendorID');
				
			}
            if(wrapObject.CandidateData.LastName == '' || wrapObject.CandidateData.LastName == null){
                validationPassed = false;
				missingFields.add(' CandidateLastName');
				
            }
			 if((wrapObject.CandidateData.Email =='' && wrapObject.CandidateData.PhoneData.Phone =='') || (wrapObject.CandidateData.Email == null && wrapObject.CandidateData.PhoneData.Phone == null)){
                validationPassed = false;
				missingFields.add(' Email/Phone');
				
             }
			 if((wrapObject.CandidateData.AddressData.Country==''||wrapObject.CandidateData.AddressData.Country==null)){
				validationPassed = false;
				missingFields.add(' Country');
			 }
			 if((wrapObject.CandidateData.AddressData.PostalCode =='' || wrapObject.CandidateData.AddressData.PostalCode ==null)){
				validationPassed = false;
				missingFields.add(' PostalCode');
			 }

			if((wrapObject.CandidateData.AddressData.StateProvince  == '' || wrapObject.CandidateData.AddressData.StateProvince  == null )){
                validationPassed = false;
				missingFields.add(' State');
				
            }
			if(wrapObject.ApplicationData.VendorCandidateId == '' || wrapObject.ApplicationData.VendorCandidateId == null){
				validationPassed = false;
				missingFields.add(' VendorCandidateId');
				
			}
			if(wrapObject.ApplicationData.PostingId == '' || wrapObject.ApplicationData.PostingId == null){
				validationPassed = false;
				missingFields.add(' PostingId');
				
			}
			if(wrapObject.ApplicationData.RecentJobTitle == '' || wrapObject.ApplicationData.RecentJobTitle == null){
				validationPassed = false;
				missingFields.add(' RecentJobTitle');
				
			}
			if(wrapObject.ApplicationData.VendorApplicationId == '' || wrapObject.ApplicationData.VendorApplicationId == null){
				validationPassed = false;
				missingFields.add(' VendorApplicationId');
				
			}
			if(wrapObject.ApplicationData.JBSourceId == '' || wrapObject.ApplicationData.JBSourceId == null){
				validationPassed = false;
				missingFields.add(' JBSourceId');
				
			}
			if(wrapObject.ApplicationData.ApplicationDateTime == '' || wrapObject.ApplicationData.ApplicationDateTime == null){
				validationPassed = false;
				missingFields.add(' ApplicationDateTime');
				
			}
			if(wrapObject.ResumeData.Base64Encoded == '' || wrapObject.ResumeData.Base64Encoded == null){
				validationPassed = false;
				missingFields.add(' Base64Encoded');
				
			}
			if(wrapObject.ResumeData.FileType == '' || wrapObject.ResumeData.FileType == null){
				validationPassed = false;
				missingFields.add(' FileType');
				
			}
			if(validationPassed == true){
				Datetime dt = (DateTime)JSON.deserialize('"' + wrapObject.ApplicationData.ApplicationDateTime + '"', DateTime.class);

				Application_Staging__c stagingObj = new Application_Staging__c();
				stagingObj.JSON_Payload__c	 = JSON.serialize(wrapObject);
				stagingObj.Source__c='Phenom';
				stagingObj.Status__c='New';
				stagingObj.Candidate_First_Name__c = wrapObject.CandidateData.FirstName;
				stagingObj.Candidate_Last_Name__c = wrapObject.CandidateData.LastName;
				stagingObj.Candidate_LinkedIN_URL__c = wrapObject.CandidateData.LinkedinProfileURL;
				stagingObj.Candidate_Email__c = wrapObject.CandidateData.Email;
				stagingObj.Candidate_Phone_Type__c = wrapObject.CandidateData.PhoneData.PhoneType;
				stagingObj.Candidate_Phone__c = wrapObject.CandidateData.PhoneData.Phone;
				stagingObj.Transaction_ID__c = wrapObject.ApplicationData.TransactionID;
				stagingObj.Vendor_ID__c = wrapObject.VendorData.VendorID;
				stagingObj.Combination_Key__c = combinationKey;
				stagingObj.Application_Date__c =dt;
                stagingObj.VendorCandidateID__c = wrapObject.ApplicationData.VendorCandidateId;
                stagingObj.PostingID__c = wrapObject.ApplicationData.PostingId;
                insert stagingObj;

                res.statusCode = 200;
                responseMessage ='{"Message":"The Payload has been successfully staged",'+
                    '"StageId":"'+stagingObj.Id+'"}';    
            }else{
				res.statusCode = 400;
				if(duplicateRequest == true){
					errorMessage ='Candidate and Posting ID combination already exists. Duplicate applications for same Posting are not allowed';
				}else{
					for(String str : missingFields){
						errorMessage = errorMessage + str;
					}
				}

				
				responseMessage = '{"Message":"'+errorMessage+'"}'; 
			}
            
            res.responseBody = Blob.valueOf(responseMessage);        	
        }catch(Exception e){
			
            res.statusCode = 400;
			responseMessage = '{"Message":"The payload could not be parsed. Exception message: '+e.getMessage()+'"}';
            res.responseBody = Blob.valueOf(responseMessage);
        }
        
        
    }
}