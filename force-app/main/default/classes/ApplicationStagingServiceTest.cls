@isTest
public class ApplicationStagingServiceTest {

	static testMethod void  doPost(){
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{ "VendorData":{ "VendorID":"9" }, "ApplicationData":{ "TransactionID": "1234Xyc5", "DesiredSalary":"120.45", "VendorCandidateId":"16896636cfj", "PostingId":"6998776", "DesiredSalaryType":"Hourly", "ApplicationDateTime":"2019-10-07T13:40:02.942Z", "RecentJobTitle":"AI Scientist", "ICID":"em_aero_gen_whtppr_20170515_c7bcc0e1", "VendorSegmentId":"523", "ECID":"ot_aero_gen_nnrtcls-rtk_20170420_709afe7a", "EmailConsent":true, "InviterEmail":"", "VendorApplicationId":"46853491", "JBSourceId":"5387", "AdobeEcvid":"", "TextConsent":false }, "CandidateData":{ "FirstName":"Sobia", "LastName":"Liaqat", "Email":"sobia-89@hotmail.com", "PhoneData":{ "Phone":"3215435423", "PhoneType":"Cell" }, "AddressData":{ "Country":"USA", "StateProvince":"VA", "PostalCode":"20164", "Street":"", "City":"Sterling" }, "HighestEducation":"", "SecurityClearance":"", "WorkAuthorization":"", "LinkedinProfileURL":"" }, "ResumeData":{ "FileType":"doc", "Base64Encoded":"TkxIC0gMTk5Mi4NCg0KU3RkLiBYDQoNClN0Lkpvc2VwaHMgSGlnaGVyIFNlY29uZGFyeSBTY2hvb2wsDQpDaGVuZ2FscGF0dHUsIFRhbWlsTmFkdSwgSW5kaWENCg0KQWdncmVnYXRlOiA5MiAlDQoxOTg5ICAxOTkwLg==" }, "OFFCCPFlag":false, "OFCCPData":{ "OFCCPSignatureDate":"", "OFCCPEthnicityResponse":"", "OFCCPGenderResponse":"", "OFCCPSignatureName":"", "OFCCPVeteranStatus":"", "OFCCPDisabilityResponse":"" } }';
            
            req.requestURI = '/services/apexrest/Application/StagingRequest/V1/';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            ApplicationStagingService.doPost();
            
            requestJSONBody = '{ "VendorData":{ "VendorID":"" }, "ApplicationData":{ "TransactionID": "1234Xyc5", "DesiredSalary":"120.45", "VendorCandidateId":"", "PostingId":"", "DesiredSalaryType":"Hourly", "ApplicationDateTime":"", "RecentJobTitle":"", "ICID":"em_aero_gen_whtppr_20170515_c7bcc0e1", "VendorSegmentId":"523", "ECID":"ot_aero_gen_nnrtcls-rtk_20170420_709afe7a", "EmailConsent":true, "InviterEmail":"", "VendorApplicationId":"", "JBSourceId":"", "AdobeEcvid":"", "TextConsent":false }, "CandidateData":{ "FirstName":"Sobia", "LastName":"", "Email":"", "PhoneData":{ "Phone":"", "PhoneType":"Cell" }, "AddressData":{ "Country":"", "StateProvince":"", "PostalCode":"", "Street":"", "City":"Sterling" }, "HighestEducation":"", "SecurityClearance":"", "WorkAuthorization":"", "LinkedinProfileURL":"" }, "ResumeData":{ "FileType":"", "Base64Encoded":"" }, "OFFCCPFlag":false, "OFCCPData":{ "OFCCPSignatureDate":"", "OFCCPEthnicityResponse":"", "OFCCPGenderResponse":"", "OFCCPSignatureName":"", "OFCCPVeteranStatus":"", "OFCCPDisabilityResponse":"" } }';
            req.requestBody = Blob.valueOf(requestJSONBody);
            ApplicationStagingService.doPost();
           
            requestJSONBody = '{ "VendorData1":{ "VendorID":"" }}';
            req.requestBody = Blob.valueOf(requestJSONBody);
            ApplicationStagingService.doPost();
            Test.stopTest();
     }
    }
}