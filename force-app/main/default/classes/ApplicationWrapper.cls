public class ApplicationWrapper {

	

	public VendorData VendorData;
	public ApplicationData ApplicationData;
	public CandidateData CandidateData;
	public ResumeData ResumeData;
	public Boolean OFFCCPFlag;
	public OFCCPData OFCCPData;

	

	public class ApplicationData {
		public String TransactionID;
		public String DesiredSalary;
		public String VendorCandidateId;
		public String PostingId;
		public String DesiredSalaryType;
		public String ApplicationDateTime;
		public String RecentJobTitle;
		public String ICID;
		public String VendorSegmentId;
		public String ECID;
		public Boolean EmailConsent;
		public String InviterEmail;
		public String VendorApplicationId;
		public String JBSourceId;
		public String AdobeEcvid;
		public Boolean AcceptedTnC;
		public Boolean TextConsent;
	}

	public class CandidateData {
		public String FirstName;
		public String LastName;
		public String Email;
		public PhoneData PhoneData;
		public AddressData AddressData;
		public String HighestEducation;
		public String SecurityClearance;
		public String WorkAuthorization;
		public String LinkedinProfileURL;
	}

	public class PhoneData {
		public String Phone;
		public String PhoneType;
	}

	public class OFCCPData {
		public String OFCCPSignatureDate;
		public String OFCCPEthnicityResponse;
		public String OFCCPGenderResponse;
		public String OFCCPSignatureName;
		public String OFCCPVeteranStatus;
		public String OFCCPDisabilityResponse;
	}

	public class VendorData {
		public String VendorID;
	}
	
    public class AddressData {
		public String Country;
		public String StateProvince;
		public String PostalCode;
		public String Street;
		public String City;
	}
    public class ResumeData {
		public String FileType;
		public String Base64Encoded;
	}
	
	public static ApplicationWrapper parse(String json) {
		return (ApplicationWrapper) System.JSON.deserialize(json, ApplicationWrapper.class);
	}
}