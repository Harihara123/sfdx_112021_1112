global class AssociatedToLinkedBatch implements Database.Batchable<sObject> {
    
    private Enum CHANGE_TARGET {
        ORDER, 
        EVENT
    }

    String query;
    
    private AssociatedToLinkedBatch(CHANGE_TARGET inp) {
        if (inp == CHANGE_TARGET.EVENT) {
            query = 'SELECT Id, Type, Activity_Type__c, Subject FROM Event WHERE Type=\'Associated\'';
        } else if (inp == CHANGE_TARGET.ORDER) {
            query = 'SELECT Id, Status FROM Order WHERE Status = \'Associated\'';
        } 
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    public static void runthis() {
        AssociatedToLinkedBatch btEvent = new AssociatedToLinkedBatch(AssociatedToLinkedBatch.CHANGE_TARGET.EVENT);
		Database.executeBatch(btEvent);
        
        AssociatedToLinkedBatch btOrder = new AssociatedToLinkedBatch(AssociatedToLinkedBatch.CHANGE_TARGET.ORDER);
		Database.executeBatch(btOrder);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        try {
            for (SObject obj : scope) {
                if (obj.getSObjectType() == Event.sObjectType) {
                    Event e = (Event) obj;
                    e.Type = 'Linked';
                    e.Activity_Type__c = 'Linked';
                    e.Subject = 'Linked';
                    System.debug(LoggingLevel.WARN, e);
                } else if (obj.getSObjectType() == Order.SObjectType) {
                    Order o = (Order) obj;
                    o.Status = 'Linked';
                    System.debug(LoggingLevel.WARN, o);
                }
            }
            update scope;
        } catch (DMLException de) {
            Integer numErrors = de.getNumDml();
            System.debug(logginglevel.WARN,'getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                System.debug(logginglevel.WARN,'getDmlFieldNames=' + de.getDmlFieldNames(i));
                System.debug(logginglevel.WARN,'getDmlMessage=' + de.getDmlMessage(i));
            }
        } catch (Exception e) {
            System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
            System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
            System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
            System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
            System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext BC){
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('AssociatedToLinkedBatch ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}