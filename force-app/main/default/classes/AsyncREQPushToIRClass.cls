/***************************************************************************************************************************************
* Name        - AsyncREQPushToIRClass 
* Description - This class has a future webservice that updates Reqs passs as a List of Ids                 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       11/13/2012             Created
*****************************************************************************************************************************************/
global class AsyncREQPushToIRClass {

  @future (callout=true)
  static webservice void WS_AsyncREQPushToIR(List<Id> ListOfModifiedReqs) {
  
    // blanket update Reqs for the losing Account or losing Contact
    List<Reqs__c> allModifiedReqs = new List<Reqs__c>();
    for (Id ReqId : ListOfModifiedReqs)
    {
      Reqs__c ReqObject = new Reqs__c(Id = ReqId);
      allModifiedReqs.add(ReqObject);
    }     
    update allModifiedReqs; 
    
  }
}