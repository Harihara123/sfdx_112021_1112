public class AsyncTestDataCreate implements Queueable {
    String FName;
    String Opco;
	String ObjName;
    
    public AsyncTestDataCreate(String FName,String Opco,String ObjName)
    {
        this.FName=FName;
        this.Opco=Opco;
		this.ObjName=ObjName;

    }

    public void execute(QueueableContext context) {
        ATS_TestDataCreation.createOpportunity(1,FName,Opco,ObjName);

    }

}