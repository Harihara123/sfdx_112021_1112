//Generated by wsdl2apex

public class AsyncWS_TIBCO_Submittal {
    public class AsyncSubmittalLogServiceSFHTTPEndpoint {
    Integration_Endpoints__c SubmittalsEndpoint = Integration_Endpoints__c.getValues('TIBCO.Submittals');
        public String endpoint_x = SubmittalsEndpoint.Endpoint__c;
        public Map<String,String> inputHttpHeaders_x;
        Integration_Certificate__c SSLCertificate = Integration_Certificate__c.getValues('Certificate');
        public String clientCertName_x = SSLCertificate.Certificate_Name__c;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.allegisgroup.com/SubmittalSF', 'WS_TIBCO_Submittal', 'http://www.allegisgroup.com/AllegisServices/SharedResources/CDM/SubmittalLogNS', 'WS_TIBCO_SubmittalType', 'http://www.allegisgroup.com/AllegisServices/SharedResources/Schemas/Common/CommonNS', 'WS_TIBCO_SubmittalType'};
        public AsyncWS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2Future beginPullSubmittalLogSF(System.Continuation continuation,String PartnerReqSysID,String PartnerReqID,String ReqGUID,String Filter,String SortColumn,String SortDirection) {
            WS_TIBCO_SubmittalType.SubmittalLogSearchRequestType2 request_x = new WS_TIBCO_SubmittalType.SubmittalLogSearchRequestType2();
            request_x.PartnerReqSysID = PartnerReqSysID;
            request_x.PartnerReqID = PartnerReqID;
            request_x.ReqGUID = ReqGUID;
            request_x.Filter = Filter;
            request_x.SortColumn = SortColumn;
            request_x.SortDirection = SortDirection;
            return (AsyncWS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2Future) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2Future.class,
              continuation,
              new String[]{endpoint_x,
              'PullSubmittalLogSF',
              'http://www.allegisgroup.com/AllegisServices/SharedResources/CDM/SubmittalLogNS',
              'SubmittalLogSearchRequest2',
              'http://www.allegisgroup.com/AllegisServices/SharedResources/CDM/SubmittalLogNS',
              'SubmittalLogSearchResponse2',
              'WS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2'}
            );
        }
    }
}