//Merge Class apex script 
@RestResource(urlMapping='/talent/atscommerge/*')
global without sharing class AtsComTalentMergeController {


      
   @HttpPost
   global static String doPost(Id masterAccountId, Id duplicateAccountId,Id masterContactId,Id duplicateContactId,Id mergeMappingId) {

        return TalentMergeHandler.handleMerge(masterAccountId, duplicateAccountId, masterContactId, duplicateContactId, mergeMappingId);

    }
}