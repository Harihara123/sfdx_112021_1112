//Merge Class apex script 
@RestResource(urlMapping='/talent/update/accountcontact/*')
global without sharing class AtsComTalentMergeUpdateController  {

  private static final String ATSCOPIED = 'ATS_COPIED';


   @HttpPost
   global static String doPost(Id masterAccountId, Id duplicateAccountId,Id masterContactId,Id duplicateContactId,Id mergeMappingId) {

            TriggerStopper.isMergeEvent = true;
                            
            Talent_Merge_Mapping__c tmm = new Talent_Merge_Mapping__c(Id = mergeMappingId);

            savePoint sp1  = Database.setSavepoint();

            try {

                    Account msA = copyAccountRecords(masterAccountId, duplicateAccountId);
                    Contact msC = copyContactRecords(masterContactId, duplicateContactId);

                    List<SObject> msts = new List<SObject>();
                    msts.add(msA);
                    msts.add(msC);

                    update msts;

                   tmm.Account_Merge_Status__c = ATSCOPIED;
                   tmm.Contact_Merge_Status__c = ATSCOPIED;
                   tmm.Result__c= null; 
                   tmm.Result_Reason__c = null;
                   update tmm;

            } catch(Exception e) {
                    Database.rollback(sp1);
                    tmm.Result__c='FAIL-EXCEPTION';
                    tmm.Result_Reason__c = e.getMessage();
                    update tmm;
             }  

           return String.valueOf(masterAccountId) + '-' + tmm.Result__c;
    }


    private static Contact copyContactRecords(Id masterContactId, Id duplicateContactId){
       
         Contact objMaster; 
         Contact objDupe; 

         List<Id> listIds = new List<Id>{masterContactId,duplicateContactId};

         List<Contact> objs =  [select id, Peoplesoft_ID__c,ABOUT_ME__C,  TC_MAILINGLATITUDE__C, TC_MAILINGLONGITUDE__C, merge_state__C, cs_desired_salary_rate__c,cs_geo_pref_comments__c,cs_highest_education__c,cs_salary__c,cs_security_clearance_type__c,cs_work_eligibility__c,data_quality_record_flag__c,data_quality_resume_flag__c,do_not_contact__c,do_not_contact_account__c,do_not_contact_reason_account__c,do_not_recruit__c,do_not_recruit_talent__c,email,externalsourceid__c,firstname,has_related_contact__c,homephone,last_activity_date__c,lastname,linkedin_person_urn__c,linkedin_url__c,mailingaddress,mailingcity,mailingcountry,mailinggeocodeaccuracy,mailinglatitude,mailinglongitude,mailingpostalcode,mailingstate,mailingstreet,middlename,mobilephone,name,name__c,other_email__c,otherphone,ownerid,phone,phone_exists__c,portalag_assignments__c,portalag_profile__c,prefcentre_aerotek_optout_channels__c,prefcentre_aerotek_optout_email__c,prefcentre_aerotek_optout_mobile__c,prefcentre_aerotek_sms_channels__c,prefcentre_ags_optout_channels__c,prefcentre_ags_optout_email__c,prefcentre_ags_optout_mobile__c,prefcentre_ags_sms_channels__c,prefcentre_allegis_optout_channels__c,prefcentre_allegis_optout_email__c,prefcentre_allegis_optout_mobile__c,prefcentre_allegis_sms_channels__c,prefcentre_allegispartners_optout_channe__c,prefcentre_allegispartners_optout_email__c,prefcentre_allegispartners_optout_mobile__c,prefcentre_allegispartners_sms_channels__c,prefcentre_astoncarter_optout_channels__c,prefcentre_astoncarter_optout_email__c,prefcentre_astoncarter_optout_mobile__c,prefcentre_astoncarter_sms_channels__c,prefcentre_brand__c,prefcentre_captureddate__c,prefcentre_capturedsource__c,prefcentre_cmsource__c,prefcentre_contactmethodtype__c,prefcentre_contactmethodvalue__c,prefcentre_country__c,prefcentre_easi_optout_channels__c,prefcentre_easi_optout_email__c,prefcentre_easi_optout_mobile__c,prefcentre_easi_sms_channels__c,prefcentre_mla_optout_channels__c,prefcentre_mla_optout_email__c,prefcentre_mla_optout_mobile__c,prefcentre_mla_sms_channels__c,prefcentre_structured__c,prefcentre_teksystems_optout_channels__c,prefcentre_teksystems_optout_email__c,prefcentre_teksystems_optout_mobile__c,prefcentre_teksystems_sms_channels__c,prefcentre_tgs_optout_channels__c,prefcentre_tgs_optout_email__c,prefcentre_tgs_optout_mobile__c,prefcentre_tgs_sms_channels__c,prefcentre_update_error__c,preferred_email__c,preferred_email_value__c,preferred_first_name__c,preferred_name__c,preferred_phone__c,preferred_phone_value__c,preferred_url_value__c,profile_last_modified_by__c,profile_last_modified_date__c,related_contact__c,rws_dnc__c,salutation,source_system_id__c,source_type__c,suffix,talent_committed_flag__c,talent_country_text__c,talent_id__c,talent_ownership__c,talent_state_text__c,title,website_url__c,work_email__c,workextension__c,tc_email__c,tc_firstname__c,tc_lastname__c,tc_linkedin_url__c,tc_mailingcity__c,tc_mailingcountry__c, tc_mailingpostalcode__c,tc_mailingstate__c,tc_mailingstreet__c,tc_name__c,tc_phone__c,tc_title__c,tc_about_me__c,tc_middle_name__c,tc_suffix__c,Last_Service_Date__c  from Contact where Id In:listIds]; 

         if (objs[0].id == masterContactId ) {
                objMaster = objs[0];
                objDupe = objs[1];
         } else {
                objMaster = objs[1];
                objDupe = objs[0];
         }  

        //objMaster.TC_EMAIL__C= objMaster.EMAIL;
        //objMaster.Email = objDupe.Email;

        if (objDupe.Email != null) {
           if (objDupe.Other_Email__c == null ) {
                objDupe.Other_Email__c = objDupe.Email; 
           } else if (objDupe.Work_Email__c == null )  {
                objDupe.Work_Email__c = objDupe.Email; 
           }
        }

        objMaster.Work_Email__c = objDupe.Work_Email__c;
        objMaster.Other_Email__c = objDupe.Other_Email__c;

         
        // objMaster.TC_PHONE__C= objMaster.PHONE;

        if (objDupe.Phone != null && objDupe.OtherPhone == null) {
            objDupe.OtherPhone = objDupe.Phone; 
        }
        objMaster.Phone = (objMaster.Phone == null ? objDupe.Phone : objMaster.Phone); 
        objMaster.MobilePhone = (objMaster.MobilePhone == null ? objDupe.MobilePhone : objMaster.MobilePhone); 

        objMaster.OtherPhone = objDupe.OtherPhone;
        objMaster.HomePhone = objDupe.HomePhone;


        objMaster.TC_FIRSTNAME__C= objMaster.FIRSTNAME;
        objMaster.TC_LASTNAME__C= objMaster.LASTNAME;
        objMaster.TC_LINKEDIN_URL__C= objMaster.LINKEDIN_URL__C;
        objMaster.TC_MAILINGCITY__C= objMaster.MAILINGCITY;
        objMaster.TC_MAILINGCOUNTRY__C= objMaster.MAILINGCOUNTRY;
        objMaster.TC_MAILINGLATITUDE__C= objMaster.MAILINGLATITUDE;
        objMaster.TC_MAILINGLONGITUDE__C= objMaster.MAILINGLONGITUDE;
        objMaster.TC_MAILINGPOSTALCODE__C= objMaster.MAILINGPOSTALCODE;
        objMaster.TC_MAILINGSTATE__C= objMaster.MAILINGSTATE;
        objMaster.TC_MAILINGSTREET__C= objMaster.MAILINGSTREET;
        objMaster.TC_TITLE__C= objMaster.TITLE;
        objMaster.TC_ABOUT_ME__C= objMaster.ABOUT_ME__C;
        objMaster.TC_MIDDLE_NAME__C= objMaster.MIDDLENAME;
        objMaster.TC_SUFFIX__C= objMaster.SUFFIX;

        objMaster.merge_state__C = ATSCOPIED; 

         objMaster.CS_Desired_Salary_Rate__c = objDupe.CS_Desired_Salary_Rate__c;
         objMaster.CS_Geo_Pref_Comments__c = objDupe.CS_Geo_Pref_Comments__c;
         objMaster.CS_Highest_Education__c = objDupe.CS_Highest_Education__c;
         objMaster.CS_Salary__c = objDupe.CS_Salary__c;
         objMaster.CS_Security_Clearance_Type__c = objDupe.CS_Security_Clearance_Type__c;
         objMaster.CS_Work_Eligibility__c = objDupe.CS_Work_Eligibility__c;
         objMaster.Data_Quality_Record_Flag__c = objDupe.Data_Quality_Record_Flag__c;
         objMaster.Data_Quality_Resume_Flag__c = objDupe.Data_Quality_Resume_Flag__c;
         objMaster.Do_Not_Contact__c = objDupe.Do_Not_Contact__c;

         //objMaster.externalSourceId__c = objDupe.externalSourceId__c;
         objMaster.FirstName = objDupe.FirstName;

         objMaster.LastName = objDupe.LastName;
         objMaster.LinkedIn_Person_URN__c = objDupe.LinkedIn_Person_URN__c;
         objMaster.LinkedIn_URL__c = objDupe.LinkedIn_URL__c;
         objMaster.MailingCity = objDupe.MailingCity;
         objMaster.MailingCountry = objDupe.MailingCountry;
         objMaster.MailingGeocodeAccuracy = objDupe.MailingGeocodeAccuracy;
         objMaster.MailingLatitude = objDupe.MailingLatitude;
         objMaster.MailingLongitude = objDupe.MailingLongitude;
         objMaster.MailingPostalCode = objDupe.MailingPostalCode;
         objMaster.MailingState = objDupe.MailingState;
         objMaster.MailingStreet = objDupe.MailingStreet;
         objMaster.MiddleName = objDupe.MiddleName;
        

         
         objMaster.OwnerId = objDupe.OwnerId;
        
         objMaster.PrefCentre_Aerotek_OptOut_Channels__c = objDupe.PrefCentre_Aerotek_OptOut_Channels__c;
         objMaster.PrefCentre_Aerotek_OptOut_Email__c = objDupe.PrefCentre_Aerotek_OptOut_Email__c;
         objMaster.PrefCentre_Aerotek_OptOut_Mobile__c = objDupe.PrefCentre_Aerotek_OptOut_Mobile__c;
         objMaster.PrefCentre_Aerotek_SMS_Channels__c = objDupe.PrefCentre_Aerotek_SMS_Channels__c;
         objMaster.PrefCentre_AGS_OptOut_Channels__c = objDupe.PrefCentre_AGS_OptOut_Channels__c;
         objMaster.PrefCentre_AGS_OptOut_Email__c = objDupe.PrefCentre_AGS_OptOut_Email__c;
         objMaster.PrefCentre_AGS_OptOut_Mobile__c = objDupe.PrefCentre_AGS_OptOut_Mobile__c;
         objMaster.PrefCentre_AGS_SMS_Channels__c = objDupe.PrefCentre_AGS_SMS_Channels__c;
         objMaster.PrefCentre_Allegis_OptOut_Channels__c = objDupe.PrefCentre_Allegis_OptOut_Channels__c;
         objMaster.PrefCentre_Allegis_OptOut_Email__c = objDupe.PrefCentre_Allegis_OptOut_Email__c;
         objMaster.PrefCentre_Allegis_OptOut_Mobile__c = objDupe.PrefCentre_Allegis_OptOut_Mobile__c;
         objMaster.PrefCentre_Allegis_SMS_Channels__c = objDupe.PrefCentre_Allegis_SMS_Channels__c;
         objMaster.PrefCentre_AllegisPartners_OptOut_Channe__c = objDupe.PrefCentre_AllegisPartners_OptOut_Channe__c;
         objMaster.PrefCentre_AllegisPartners_OptOut_Email__c = objDupe.PrefCentre_AllegisPartners_OptOut_Email__c;
         objMaster.PrefCentre_AllegisPartners_OptOut_Mobile__c = objDupe.PrefCentre_AllegisPartners_OptOut_Mobile__c;
         objMaster.PrefCentre_AllegisPartners_SMS_Channels__c = objDupe.PrefCentre_AllegisPartners_SMS_Channels__c;
         objMaster.PrefCentre_AstonCarter_OptOut_Channels__c = objDupe.PrefCentre_AstonCarter_OptOut_Channels__c;
         objMaster.PrefCentre_AstonCarter_OptOut_Email__c = objDupe.PrefCentre_AstonCarter_OptOut_Email__c;
         objMaster.PrefCentre_AstonCarter_OptOut_Mobile__c = objDupe.PrefCentre_AstonCarter_OptOut_Mobile__c;
         objMaster.PrefCentre_AstonCarter_SMS_Channels__c = objDupe.PrefCentre_AstonCarter_SMS_Channels__c;
         objMaster.PrefCentre_Brand__c = objDupe.PrefCentre_Brand__c;
         objMaster.PrefCentre_CapturedDate__c = objDupe.PrefCentre_CapturedDate__c;
         objMaster.PrefCentre_CapturedSource__c = objDupe.PrefCentre_CapturedSource__c;
         objMaster.PrefCentre_CMSource__c = objDupe.PrefCentre_CMSource__c;
         objMaster.PrefCentre_ContactMethodType__c = objDupe.PrefCentre_ContactMethodType__c;
         objMaster.PrefCentre_ContactMethodValue__c = objDupe.PrefCentre_ContactMethodValue__c;
         objMaster.PrefCentre_Country__c = objDupe.PrefCentre_Country__c;
         objMaster.PrefCentre_EASi_OptOut_Channels__c = objDupe.PrefCentre_EASi_OptOut_Channels__c;
         objMaster.PrefCentre_EASi_OptOut_Email__c = objDupe.PrefCentre_EASi_OptOut_Email__c;
         objMaster.PrefCentre_EASi_OptOut_Mobile__c = objDupe.PrefCentre_EASi_OptOut_Mobile__c;
         objMaster.PrefCentre_EASi_SMS_Channels__c = objDupe.PrefCentre_EASi_SMS_Channels__c;
         objMaster.PrefCentre_MLA_OptOut_Channels__c = objDupe.PrefCentre_MLA_OptOut_Channels__c;
         objMaster.PrefCentre_MLA_OptOut_Email__c = objDupe.PrefCentre_MLA_OptOut_Email__c;
         objMaster.PrefCentre_MLA_OptOut_Mobile__c = objDupe.PrefCentre_MLA_OptOut_Mobile__c;
         objMaster.PrefCentre_MLA_SMS_Channels__c = objDupe.PrefCentre_MLA_SMS_Channels__c;
         objMaster.PrefCentre_Structured__c = objDupe.PrefCentre_Structured__c;
         objMaster.PrefCentre_TEKsystems_OptOut_Channels__c = objDupe.PrefCentre_TEKsystems_OptOut_Channels__c;
         objMaster.PrefCentre_TEKsystems_OptOut_Email__c = objDupe.PrefCentre_TEKsystems_OptOut_Email__c;
         objMaster.PrefCentre_TEKsystems_OptOut_Mobile__c = objDupe.PrefCentre_TEKsystems_OptOut_Mobile__c;
         objMaster.PrefCentre_TEKsystems_SMS_Channels__c = objDupe.PrefCentre_TEKsystems_SMS_Channels__c;
         objMaster.PrefCentre_TGS_OptOut_Channels__c = objDupe.PrefCentre_TGS_OptOut_Channels__c;
         objMaster.PrefCentre_TGS_OptOut_Email__c = objDupe.PrefCentre_TGS_OptOut_Email__c;
         objMaster.PrefCentre_TGS_OptOut_Mobile__c = objDupe.PrefCentre_TGS_OptOut_Mobile__c;
         objMaster.PrefCentre_TGS_SMS_Channels__c = objDupe.PrefCentre_TGS_SMS_Channels__c;
         objMaster.PrefCentre_Update_Error__c = objDupe.PrefCentre_Update_Error__c;
         objMaster.Preferred_Email__c = objDupe.Preferred_Email__c;
         objMaster.Preferred_Name__c = objDupe.Preferred_Name__c;
         objMaster.Preferred_Phone__c = objDupe.Preferred_Phone__c;
         objMaster.Profile_Last_Modified_By__c  = objDupe.Profile_Last_Modified_By__c ;
         objMaster.Profile_Last_Modified_Date__c = objDupe.Profile_Last_Modified_Date__c;
         objMaster.Related_Contact__c = objDupe.Related_Contact__c;
         objMaster.RWS_DNC__c = objDupe.RWS_DNC__c;
         objMaster.Salutation = objDupe.Salutation;
         objMaster.Source_System_Id__c = objDupe.Source_System_Id__c;
         objMaster.Source_Type__c = objDupe.Source_Type__c;
         objMaster.Suffix = objDupe.Suffix;
         objMaster.Talent_Country_Text__c = objDupe.Talent_Country_Text__c;
         objMaster.Talent_State_Text__c = objDupe.Talent_State_Text__c;
         objMaster.Title = objDupe.Title;
         objMaster.Website_URL__c = objDupe.Website_URL__c;

         objMaster.WorkExtension__c = objDupe.WorkExtension__c;

         // copy last service touch point
         if(objDupe.Last_Service_Date__c != null){
            objMaster.Last_Service_Date__c = objDupe.Last_Service_Date__c;
         }

         // Updated for S-124665
         if(objMaster.Peoplesoft_ID__c != null && objMaster.Peoplesoft_ID__c.startsWith('E.')){
            objMaster.Peoplesoft_ID__c = objDupe.Peoplesoft_ID__c;
         }
         return objMaster;
  }


  private static Account copyAccountRecords(Id masterAccountId, Id duplicateAccountId){

             Account objMaster; 
             Account objDupe; 

             List<Id> listIds = new List<Id>{masterAccountId,duplicateAccountId};
             List<Account> objs = [select id, Peoplesoft_ID__c,merge_state__C, Merge_Id__c, tc_talent_profile_last_modified_date__c,candidate_status__c,current_employer__c,desired_additional_compensation__c,desired_bonus__c,desired_bonus_percent__c,desired_currency__c,desired_placement_type__c,desired_rate__c,desired_rate_frequency__c,desired_rate_max__c,desired_salary__c,desired_salary_max__c,desired_schedule__c,desired_total_compensation__c,do_not_contact__c,do_not_contact_date__c,do_not_contact_reason__c,do_not_recruit__c,do_not_recruit_rules_count__c,do_not_recruit_talent__c,g2_completed__c,g2_completed_by__c,g2_completed_date__c,g2_last_modified_by__c,g2_last_modified_date__c,goals_and_interests__c,last_activity_date__c,lastactivitydate,name,ownerid,portalag_company_assignments_link__c,portalag_profile_link__c,rws_data_tag__c,skill_comments__c,skills__c,source_system_id__c,talent_committed_flag__c,talent_current_employer__c,talent_current_employer_text__c,talent_end_date__c,talent_end_date_new__c,talent_gender__c,talent_gender_information__c,talent_latest_submittal_status__c,talent_latest_submittal_timestamp__c,talent_overview__c,talent_ownership__c,talent_preference_internal__c,talent_profile_last_modified_by__c,talent_profile_last_modified_date__c,talent_security_clearance_type__c,talent_source__c,talent_source_other__c,talent_summary_last_modified_by__c,talent_summary_last_modified_date__c,talent_visibility__c,target_account_stamp_0__c,target_account_stamp_1__c,target_account_stamp_2__c,target_account_stamp_3__c,target_account_stamp_4__c,target_account_stamp_5__c,willing_to_relocate__c  from Account where Id In:listIds]; 

             if (objs[0].id == masterAccountId ) {
                objMaster = objs[0];
                objDupe = objs[1];
             } else {
                objMaster = objs[1];
                objDupe = objs[0];
             }

            objMaster.TC_TALENT_PROFILE_LAST_MODIFIED_BY__C = objMaster.TALENT_PROFILE_LAST_MODIFIED_BY__C;
            objMaster.TC_TALENT_PROFILE_LAST_MODIFIED_DATE__C = objMaster.TALENT_PROFILE_LAST_MODIFIED_DATE__C;

            objMaster.merge_state__C = ATSCOPIED; 
            objMaster.Merge_Id__c = duplicateAccountId; 

            objMaster.Candidate_Status__c= objDupe.Candidate_Status__c;
            objMaster.Current_Employer__c= objDupe.Current_Employer__c;
            objMaster.Desired_Additional_Compensation__c= objDupe.Desired_Additional_Compensation__c;
            objMaster.Desired_Bonus__c= objDupe.Desired_Bonus__c;
            objMaster.Desired_Bonus_Percent__c= objDupe.Desired_Bonus_Percent__c;
            objMaster.Desired_Currency__c= objDupe.Desired_Currency__c;
            objMaster.Desired_Placement_type__c= objDupe.Desired_Placement_type__c;
            objMaster.Desired_Rate__c= objDupe.Desired_Rate__c;
            objMaster.Desired_Rate_Frequency__c= objDupe.Desired_Rate_Frequency__c;
            objMaster.Desired_Rate_Max__c= objDupe.Desired_Rate_Max__c;
            objMaster.Desired_Salary__c= objDupe.Desired_Salary__c;
            objMaster.Desired_Salary_Max__c= objDupe.Desired_Salary_Max__c;
            objMaster.Desired_Schedule__c= objDupe.Desired_Schedule__c;
            objMaster.Do_Not_Contact__c = objDupe.Do_Not_Contact__c ;
            objMaster.Do_Not_Contact_Date__c = objDupe.Do_Not_Contact_Date__c ;
            objMaster.Do_Not_Contact_Reason__c = objDupe.Do_Not_Contact_Reason__c ;
            objMaster.G2_Completed__c= objDupe.G2_Completed__c;
            objMaster.G2_Completed_By__c= objDupe.G2_Completed_By__c;
            objMaster.G2_Completed_Date__c= objDupe.G2_Completed_Date__c;
            objMaster.G2_Last_Modified_By__c= objDupe.G2_Last_Modified_By__c;
            objMaster.G2_Last_Modified_Date__c= objDupe.G2_Last_Modified_Date__c;
            objMaster.Goals_and_Interests__c= objDupe.Goals_and_Interests__c;
            objMaster.Name= objDupe.Name;
            objMaster.OwnerId= objDupe.OwnerId;
            objMaster.RWS_DATA_TAG__c= objDupe.RWS_DATA_TAG__c;
            objMaster.Skill_Comments__c= objDupe.Skill_Comments__c;
            objMaster.Skills__c= objDupe.Skills__c;
            objMaster.Source_System_Id__c= objDupe.Source_System_Id__c;
            objMaster.Talent_Committed_Flag__c= objDupe.Talent_Committed_Flag__c;
            objMaster.Talent_Current_Employer_Text__c= objDupe.Talent_Current_Employer_Text__c;
            objMaster.Talent_End_Date_New__c= objDupe.Talent_End_Date_New__c;
            objMaster.Talent_Gender__c= objDupe.Talent_Gender__c;
            objMaster.Talent_Gender_Information__c= objDupe.Talent_Gender_Information__c;
            objMaster.Talent_Latest_Submittal_Status__c= objDupe.Talent_Latest_Submittal_Status__c;
            objMaster.Talent_Latest_Submittal_Timestamp__c= objDupe.Talent_Latest_Submittal_Timestamp__c;
            objMaster.Talent_Overview__c= objDupe.Talent_Overview__c;
            objMaster.Talent_Ownership__c= objDupe.Talent_Ownership__c;
            objMaster.Talent_Preference_Internal__c= objDupe.Talent_Preference_Internal__c;
            objMaster.Talent_Profile_Last_Modified_By__c= objDupe.Talent_Profile_Last_Modified_By__c;
            objMaster.Talent_Profile_Last_Modified_Date__c= objDupe.Talent_Profile_Last_Modified_Date__c;
            objMaster.Talent_Security_Clearance_Type__c= objDupe.Talent_Security_Clearance_Type__c;
            objMaster.Talent_Source__c= objDupe.Talent_Source__c;
            objMaster.Talent_Source_Other__c= objDupe.Talent_Source_Other__c;
            objMaster.Talent_Summary_Last_modified_by__c= objDupe.Talent_Summary_Last_modified_by__c;
            objMaster.Talent_Summary_Last_Modified_date__c= objDupe.Talent_Summary_Last_Modified_date__c;
            objMaster.Talent_Visibility__c= objDupe.Talent_Visibility__c;
            objMaster.Target_Account_Stamp_0__c= objDupe.Target_Account_Stamp_0__c;
            objMaster.Target_Account_Stamp_1__c= objDupe.Target_Account_Stamp_1__c;
            objMaster.Target_Account_Stamp_2__c= objDupe.Target_Account_Stamp_2__c;
            objMaster.Target_Account_Stamp_3__c= objDupe.Target_Account_Stamp_3__c;
            objMaster.Target_Account_Stamp_4__c= objDupe.Target_Account_Stamp_4__c;
            objMaster.Target_Account_Stamp_5__c= objDupe.Target_Account_Stamp_5__c;
            objMaster.Willing_to_Relocate__c= objDupe.Willing_to_Relocate__c;

            // Updated for S-124665
            if(objMaster.Peoplesoft_ID__c != null && objMaster.Peoplesoft_ID__c.startsWith('E.')){
                objMaster.Peoplesoft_ID__c = objDupe.Peoplesoft_ID__c;
            }

            return objMaster;
  }


}