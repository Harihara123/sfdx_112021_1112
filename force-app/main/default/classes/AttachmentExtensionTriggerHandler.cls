/*
@author : Neel Kamal
@Date : 03/09/2020
@Description : This hadnler will be use for handle the all trigger event.
*/
with sharing public class AttachmentExtensionTriggerHandler  {
	public void onBeforeInsert(List<Attachment_Extension__c> attExtList) {
		if(!(UserInfo.getName().equals('ATS Data Migration') || UserInfo.getName().equals('CloudStorage Integration'))) {
			updateStatus(attExtList);
		}
	}
	public void onBeforeUpdate(List<Attachment_Extension__c> attExtList) {
		if(!(UserInfo.getName().equals('ATS Data Migration') || UserInfo.getName().equals('CloudStorage Integration'))) {
			updateStatus(attExtList);
		}
	}
	public void onAfterInsert(Map<Id, Attachment_Extension__c> newMap) {
		//User ATS Data Migration and CloudUser Integration have not access to create plateform event. Both user already created, so Id can be used in condition
		if(!(UserInfo.getName().equals('ATS Data Migration')  || UserInfo.getName().equals('CloudStorage Integration')) && !TriggerStopper.attachementExtensionStopper) {
			generatePlateformEvent(newMap.values());
			//TriggerStopper.attachementExtensionStopper = true;//Neel - This has commented because its stopping trigger 2nd time.
		}
	}

	public void onAfterUpdate(Map<Id, Attachment_Extension__c> oldMap, Map<Id, Attachment_Extension__c> newMap){
		//User ATS Data Migration and CloudUser Integration have not access to create plateform event. Both user already created, so Id can be used in condition
		if(!(UserInfo.getName().equals('ATS Data Migration')  || UserInfo.getName().equals('CloudStorage Integration')) && !TriggerStopper.attachementExtensionStopper) {
			generatePlateformEvent(newMap.values());
			//TriggerStopper.attachementExtensionStopper = true; //Neel - This has commented because its stopping trigger 2nd time.
		}
		
	}

	//Status need to change sent for the record which PE generated.
	private static void updateStatus(List<Attachment_Extension__c> triggerNew) {
		for(Attachment_Extension__c attExt : triggerNew) {
			if(attExt.Object_Name__c.equalsIgnoreCase('Talent_Document__c') && attExt.Talent_Document_Committed__c)	{
				attExt.Status__c = 'Sent';
			}
		}		
	}

	//This method will generate plaeform event and also logging same data into log object. 
	//PE will be generated when Object name='Talent-Document__c and Talent_Document_Committed__c=true'
	private static void generatePlateformEvent(List<Attachment_Extension__c> triggerNew) {
		List<Send_Attachment_Notification__e> peList = new List<Send_Attachment_Notification__e>();
		List<Attachment_Extension_PE__c> aeList = new List<Attachment_Extension_PE__c>();
		

		for(Attachment_Extension__c attExt : triggerNew) {		
			if(attExt.Object_Name__c.equalsIgnoreCase('Talent_Document__c') && attExt.Talent_Document_Committed__c)	{
				Send_Attachment_Notification__e pe = new Send_Attachment_Notification__e();
				Attachment_Extension_PE__c attExtLog = new Attachment_Extension_PE__c();

				pe.Attachment_Extension_Id__c = attExt.Id;
				pe.Created_Date__c = attExt.CreatedDate;
				pe.Entity_Name__c = attExt.Entity_Name__c;
				pe.Event__c = attExt.Event__c;
				pe.Gparent_Id__c = attExt.Gparent_Id__c;
				pe.Last_Modified_Date__c = attExt.LastModifiedDate;
				pe.Object_Name__c = attExt.Object_Name__c;
				pe.Parent_Id__c = attExt.Parent_Id__c;
				pe.Processing_Priority__c = attExt.Processing_Priority__c;
				pe.Processing_Type__c = attExt.Processing_Type__c;
				pe.Record_Id__c = attExt.Record_Id__c;
				pe.SystemModstamp__c = attExt.SystemModstamp;

				attExtLog.Attachment_Extension__c = attExt.Id;
				attExtLog.Entity_Name__c = attExt.Entity_Name__c;
				attExtLog.Event__c = attExt.Event__c;
				attExtLog.Gparent_Id__c = attExt.Gparent_Id__c;
				attExtLog.Object_Name__c = attExt.Object_Name__c;
				attExtLog.Parent_Id__c = attExt.Parent_Id__c;
				attExtLog.Processing_Priority__c = attExt.Processing_Priority__c;
				attExtLog.Processing_Type__c = attExt.Processing_Type__c;
				attExtLog.Record_Id__c = attExt.Record_Id__c;
				//attExtLog.User__c = UserInfo.getUserId();

				peList.add(pe);
				aeList.add(attExtLog);
			}
			
		}
	
		Boolean isPublished = false;
		try {
			if(peList.size() > 0) {
				List<Database.SaveResult> results = EventBus.publish(peList);

				for (Database.SaveResult sr : results) {
					if (sr.isSuccess()) {
					   isPublished = true;
					} 
					else {
						for(Database.Error err : sr.getErrors()) {
							System.debug('OnAfterInsert::::Error returned: ' +  err.getStatusCode() + ' - ' + err.getMessage());
						}
					}       
				}
			}

			if(isPublished) {
				insert aeList;
			}

		}catch(DMLException exp) {
			
		}

	}
}