@isTest
public class AttachmentExtensionTrigger_Test {
	@TestSetup
	public static void testDataSetup() {
		CreateTalentTestData.createAcceptedFiletypeLOVs();

		Account account = CreateTalentTestData.createTalentAccount();
       
        Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.docx', 'Resume');
        List<Attachment> atts = [SELECT Id from Attachment where ParentId = :validAttachmentDocx.Id];
		
	}

	@isTest
	public static void beforeInsert_Test() {
		Test.startTest();
			List<Attachment_Extension__c> aExtList = [select id, Entity_Name__c,Event__c,Gparent_Id__c,	Object_Name__c,	Parent_Id__c,Processing_Priority__c,Processing_Type__c,	Record_Id__c, Talent_Document_Committed__c from Attachment_Extension__c];
			AttachmentExtensionTriggerHandler attExtHandler = new AttachmentExtensionTriggerHandler();
			attExtHandler.onBeforeInsert(aExtList);
			Attachment_Extension__c attExt = aExtList.get(0);
			List<Attachment_Extension_PE__c> attExPEList = [select id, Entity_Name__c, Object_Name__c from Attachment_Extension_PE__c];
			Attachment_Extension_PE__c attExPe = attExPEList.get(0);

			System.assertEquals(attExt.Entity_Name__c, attExPe.Entity_Name__c);
		Test.stopTest();
	}

	@isTest
	public static void afterInsert_Test() {
		Test.startTest();
			List<Attachment_Extension__c> aExtList = [select id, Entity_Name__c,Event__c,Gparent_Id__c,	Object_Name__c,	Parent_Id__c,Processing_Priority__c,Processing_Type__c,	Record_Id__c, Talent_Document_Committed__c from Attachment_Extension__c];
			Attachment_Extension__c attExtUpd = aExtList.get(0);
			attExtUpd.Processing_Priority__c = 8;

			update attExtUpd;

			List<Attachment_Extension_PE__c> attExPEList = [select id, Entity_Name__c, Object_Name__c from Attachment_Extension_PE__c];
			Attachment_Extension_PE__c attExPe = attExPEList.get(0);

			System.assertEquals(attExtUpd.Entity_Name__c, attExPe.Entity_Name__c);
		Test.stopTest();
	}
}