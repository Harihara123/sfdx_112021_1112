global class AttachmentService {
    final static Pattern tokenPattern = pattern.compile('[{][{](.*?)[}][}]');
    final static Pattern jsonTokenPattern = pattern.compile('[{][{](.*?)[\\.][J][s][o][n][2][S][t][r][i][n][g][(](.*?)[)][}][}]');
    static boolean lightningProfile = false;

    global class TalentAttachment {
        webservice String peoplesoftId;
        webservice String htmlAttachment;
        webservice String errorMessage;
        webservice Date lastModifiedDate;
       
    }
    
    webservice static TalentAttachment getAttachments(String peoplesoftId, string templateName, string locale) {
        lightningProfile = false;
        TalentAttachment attachment = new TalentAttachment();
        try {
            Map<String, String> tokenValueMap = new Map<String, String>();
            //Verify the Peoplesoft Id is not null
            verifyServiceInput(peoplesoftId);
            
            getContactDetails(peoplesoftId, tokenValueMap, templateName); 
            
            getAccountDetails(tokenValueMap.get('contact.accountId'), tokenValueMap);
            
            getTalentExperiences(tokenValueMap.get('contact.accountId'), tokenValueMap);
            
             getParsedResumeDetails(tokenValueMap.get('contact.accountId'), tokenValueMap);
            //updated: 10 Aug 2016 as per story COM-1483
            List<Document> documents = [Select body from Document where DeveloperName =: templateName];
            String html;            
            if(documents.size()>0) {
                Blob body = documents[0].body;
                html  = body.toString();
            }
            
            html = mergeJsonVariables(html, tokenValueMap);
            html = mergeVariables(html, tokenValueMap);
            if (lightningProfile == true) {                
                html = html.replace('removeCertificationLtng', 'style="display:none"');
                html = html.replace('showCertificationLtng', '');
            } else {
                html = html.replace('removeCertification', '');
                html = html.replace('showCertificationLtng', 'style="display:none"');
            }
            System.debug(html);
            attachment.htmlAttachment = EncodingUtil.base64Encode(Blob.valueOf(html));
            // attachment.htmlAttachment =html;
            attachment.peoplesoftId = peoplesoftId;
             //string dateoutput = Date.valueOf(tokenValueMap.get('account.talentProfileLastModifiedDate')).format();
            Date lastModifiedDate = Date.valueOf(tokenValueMap.get('account.talentProfileLastModifiedDate'));
            attachment.lastModifiedDate = lastModifiedDate;
           
        } catch (Exception ex) {
            attachment.errorMessage = ex.getMessage();
        }
        return attachment ;    
    }
    
    static void verifyServiceInput(String peoplesoftId) {
        if (String.isBlank(peoplesoftId)) {
            throw new SoapFaultException ('Peoplesoft Id is required.');
        }
    }
    
    static Map<String, String> getContactDetails(String  peopplesoftId, Map<String, String> tokenValueMap, String templateName) {
        List<Contact> contactList;
        if(templateName !='RWSG2Template'){
            contactList = [select name, email, HomePhone, phone, title, accountId, MailingCity, MailingCountry, 
            MailingPostalCode, MailingState, MailingStreet, About_Me__c, LinkedIn_URL__c, Facebook_URL__c, 
            Twitter_URL__c,Preferred_Name__c,Website_URL__c,Account.Merge_State__c,Account.ReadyforProfileSync__c,TC_Name__c,TC_About_Me__c,TC_LinkedIn_URL__c,TC_Email__c,
            TC_phone__c,TC_Title__c, TC_MailingCity__c, TC_MailingCountry__c, TC_MailingPostalCode__c,TC_MailingState__c,
            TC_MailingStreet__c from contact where Peoplesoft_ID__c= :peopplesoftId AND Account.ReadyforProfileSync__c = true];
        }else{
            contactList = [select name, email, HomePhone, phone, title, accountId, MailingCity, MailingCountry, 
                MailingPostalCode, MailingState, MailingStreet, About_Me__c, LinkedIn_URL__c, Facebook_URL__c, 
                Twitter_URL__c,Preferred_Name__c,Website_URL__c,Account.Merge_State__c,Account.ReadyforProfileSync__c,TC_Name__c,TC_About_Me__c,TC_LinkedIn_URL__c,TC_Email__c,
                TC_phone__c,TC_Title__c, TC_MailingCity__c, TC_MailingCountry__c, TC_MailingPostalCode__c,TC_MailingState__c,
                TC_MailingStreet__c from contact where Peoplesoft_ID__c= :peopplesoftId];
        }
        if (contactList.size() == 1) {
            if(contactList[0].Account.Merge_State__c != null && !String.isBlank(contactList[0].Account.Merge_State__c) && String.ValueOf(contactList[0].Account.Merge_State__c) =='Post'){
                tokenValueMap.put('contact.accountId' , contactList[0].accountId);
                tokenValueMap.put('contact.name' , contactList[0].TC_Name__c);
                tokenValueMap.put('contact.aboutMe' , contactList[0].TC_About_Me__c);
                tokenValueMap.put('contact.facebook' , contactList[0].Facebook_URL__c);
                tokenValueMap.put('contact.linkedin' , contactList[0].TC_LinkedIn_URL__c);
                tokenValueMap.put('contact.twitter' , contactList[0].Twitter_URL__c);
                tokenValueMap.put('contact.email' , contactList[0].email);
                tokenValueMap.put('contact.phone' , contactList[0].HomePhone);
                tokenValueMap.put('contact.title' , contactList[0].TC_title__c);
                tokenValueMap.put('contact.MailingCity' , contactList[0].TC_MailingCity__c);
                tokenValueMap.put('contact.MailingCountry' , contactList[0].TC_MailingCountry__c);
                tokenValueMap.put('contact.MailingPostalCode' , contactList[0].TC_MailingPostalCode__c);
                tokenValueMap.put('contact.MailingState' , contactList[0].TC_MailingState__c);
                tokenValueMap.put('contact.MailingStreet' , contactList[0].TC_MailingStreet__c);
                tokenValueMap.put('contact.preferredName', contactList[0].Preferred_Name__c);
                tokenValueMap.put('contact.websiteUrl', contactList[0].Website_URL__c);
            }else if(String.isBlank(contactList[0].Account.Merge_State__c)){
                tokenValueMap.put('contact.accountId' , contactList[0].accountId);
                tokenValueMap.put('contact.name' , contactList[0].name);
                tokenValueMap.put('contact.aboutMe' , contactList[0].About_Me__c);
                tokenValueMap.put('contact.facebook' , contactList[0].Facebook_URL__c);
                tokenValueMap.put('contact.linkedin' , contactList[0].LinkedIn_URL__c);
                tokenValueMap.put('contact.twitter' , contactList[0].Twitter_URL__c);
                tokenValueMap.put('contact.email' , contactList[0].email);
                tokenValueMap.put('contact.phone' , contactList[0].phone);
                tokenValueMap.put('contact.title' , contactList[0].title);
                tokenValueMap.put('contact.MailingCity' , contactList[0].MailingCity);
                tokenValueMap.put('contact.MailingCountry' , contactList[0].MailingCountry);
                tokenValueMap.put('contact.MailingPostalCode' , contactList[0].MailingPostalCode);
                tokenValueMap.put('contact.MailingState' , contactList[0].MailingState);
                tokenValueMap.put('contact.MailingStreet' , contactList[0].MailingStreet);
                tokenValueMap.put('contact.preferredName', contactList[0].Preferred_Name__c);
                tokenValueMap.put('contact.websiteUrl', contactList[0].Website_URL__c);
            }
        } else if (contactList.size() == 0) {
            throw new SoapFaultException ('No record found');
        } else {
            throw new SoapFaultException ('More than one matching record found');
        }
        return tokenValueMap;
    }

    
    @TestVisible
    static Map<String, Object> getEducationDetails(Id accountId, Map<String, String> tokenValueMap,List<Talent_Experience__c> talentExperiences) {
        String educationOutput = '';
       
        for (Talent_Experience__c te: talentExperiences) {
        string Grdyear=te.Graduation_Year__c;
        string Degree =te.Degree__c;
        string DegreeLevel = te.Degree_Level__c;
        string major  =te.Major__c;
        if(string.isblank(Grdyear)){
                 educationOutput += '<br/><span><b>Graduation Year</b>: ' +  + ' </span>';
             }else{
                 educationOutput += '<br/><span><b>Graduation Year</b>: ' + Grdyear + ' </span>';
             }
           
             
             educationOutput += '<br/><span><b>School</b>: ' + te.Organization_Name__c + '</span>';
             if(!string.isblank(DegreeLevel)){
                  educationOutput += '<br/><span><b>Degree</b>: ' + DegreeLevel + ' </span>';
             } else {
                if(string.isblank(Degree)){
                  educationOutput += '<br/><span><b>Degree</b>: ' +  + ' </span>';
                }
                else{
                     educationOutput += '<br/><span><b>Degree</b>: ' + Degree + ' </span>';
                }
             }
             
            if(string.isblank(major)){
                 educationOutput += '<br/><span><b>Major</b>: ' + + ' </span><br/>';
             }else{
                 educationOutput += '<br/><span><b>Major</b>: ' + major + ' </span><br/>';
             }
        
          /*  educationOutput += '<br/><span><b>Graduation Year</b>: ' + (te.Graduation_Year__c != ''?te.Graduation_Year__c:'') + ' </span>';
            educationOutput += '<br/><span><b>School</b>: ' + te.Organization_Name__c + '</span>';
            educationOutput += '<br/><span><b>Degree</b>: ' + (te.Degree__c != ''?te.Degree__c:'') + ' </span>';
            educationOutput += '<br/><span><b>Major</b>: ' + (te.Major__c != ''?te.Major__c:'') + ' </span><br/>'; */
            }
        System.debug(educationOutput);
        tokenValueMap.put('talentEducation', educationOutput);
        return tokenValueMap;
    }
    
    static Map<String, Object> getAccountDetails(Id accountId, Map<String, Object> tokenValueMap) {
        Account a = [select name, Talent_Preference__c,Merge_State__c,TC_Talent_Profile_Last_Modified_Date__c,ReadyforProfileSync__c,Talent_Profile_Last_Modified_Date__c from Account where Id = :accountId];
        tokenValueMap.put('account.name' , a.name);
       // Boolean mergeIndicator = String.valueOf(Label.TalentMergeIndicator) == 'True'?true:false;
        if(a != null && !String.isBlank(a.Merge_State__c) && String.ValueOf(a.Merge_State__c) =='Post'){
            tokenValueMap.put('account.talentProfileLastModifiedDate' , String.valueOf(a.TC_Talent_Profile_Last_Modified_Date__c));
        }else if(a != null && String.isBlank(a.Merge_State__c)){
            tokenValueMap.put('account.talentProfileLastModifiedDate' , String.valueOf(a.Talent_Profile_Last_Modified_Date__c));    
        }
        tokenValueMap.put('account.talentPreference' , a.Talent_Preference__c);
        a.ReadyforProfileSync__c = false;
        update a;
        return tokenValueMap;
    }
    @TestVisible
    static Map<String, String> getCertificationDetails(String accountId, Map<String, String> tokenValueMap, List<Talent_Experience__c> talentExperiences) {
        //Map<String, String> tokenValueMap = new Map<String, String>();
        String certificationOutput = '';
           for (Talent_Experience__c te: talentExperiences) {
        string Grdyear=te.Graduation_Year__c;
        string Notes=te.Notes__c;
        string Certification=te.Certification__c;
             if(string.isblank(Certification)){
                 certificationOutput += '<br/><span><b>Certification</b>: ' + + ' </span>';
             }else{
                 certificationOutput += '<br/><span><b>Certification</b>: ' + Certification + ' </span>';
             }
             if(string.isblank(Grdyear)){
                 certificationOutput += '<br/><span><b>Graduation Year</b>: ' + + ' </span>';
             }else{
                 certificationOutput += '<br/><span><b>Graduation Year</b>: ' + Grdyear + ' </span>';
             }
             if(string.isblank(Notes)){
                 certificationOutput += '<br/><span><b>Notes</b>: ' + + ' </span>';
             }else{
                 certificationOutput += '<br/><span><b>Notes</b>: ' + Notes + ' </span><br/>';
             }
             tokenValueMap.put('talentCertification', certificationOutput);
        
        }
        return tokenValueMap;
    }
    
    static Map<String, String> getTalentExperiences(String accountId, Map<String, String> tokenValueMap) {
        List<Talent_Experience__c> educationList = new List<Talent_Experience__c>();
        List<Talent_Experience__c> certList = new List<Talent_Experience__c>();
        List<Talent_Experience__c> talentExperiences = new List<Talent_Experience__c>(); 
           // Boolean mergeIndicator = String.valueOf(Label.TalentMergeIndicator) == 'True'?true:false;
            //if(!mergeIndicator){
                talentExperiences = [SELECT Degree_Level__c,Type__c, Degree__c,Notes__c,Graduation_Year__c,Major__c,CreatedDate,Organization_Name__c,PersonaIndicator__c,Certification__c FROM Talent_Experience__c WHERE Talent__c = :accountId AND PersonaIndicator__c = 'SelfService' ORDER BY CreatedDate DESC NULLS LAST];                        
           // }else{
            //    talentExperiences = [SELECT Degree_Level__c,Type__c, Degree__c,Notes__c,Graduation_Year__c,Major__c,CreatedDate,Organization_Name__c,Certification__c FROM Talent_Experience__c WHERE Talent__c = :accountId ORDER BY CreatedDate DESC NULLS LAST];
           // }
            for(Talent_Experience__c te: talentExperiences){
                if (te.type__c == 'Education'){
                    educationList.add(te);                  
                }else if (te.type__c == 'Training'){
                    lightningProfile = true;
                    certList.add(te);                   
                }
            }
        getEducationDetails(tokenValueMap.get('contact.accountId'), tokenValueMap,educationList);
        if (lightningProfile == true) {
            getCertificationDetails(tokenValueMap.get('contact.accountId'), tokenValueMap, certList);
        }        
        return tokenValueMap;
    }
    
    static Map<String, Object> getParsedResumeDetails(Id accountId, Map<String, Object> tokenValueMap) {
        String parsedResume = '';
        Integer startIndex, endIndex;
        List<Talent_Document__c> tdList = [SELECT Id FROM Talent_Document__c WHERE 
            Talent__c = :accountId and HTML_Version_Available__c = true and Mark_For_Deletion__c = false and (Document_Type__c = 'Resume' or Document_Type__c = 'communities') order by createddate desc limit 1];
        
        for (Talent_Document__c td : tdList ) {
            List<Attachment> a = [SELECT Body FROM Attachment WHERE parentId = :td.Id];
            if (a.size() > 1) {
                throw new SoapFaultException ('Multiple attachment exist for a talent document');
            } else if (a.size() == 1) {
                startIndex = a[0].body.toString().indexOf('<body>') + 6;
                endIndex = a[0].body.toString().indexOf('</body>');
                if (startIndex > 5 && endIndex > 0) {
                    parsedResume += '<div>' + a[0].body.toString().substring(startIndex, endIndex) + '</div>';
                }
            }
        }        
        tokenValueMap.put('ParsedResume', parsedResume);
        return tokenValueMap;   
    }
    
    static String mergeVariables(String template, Map<String, String> tokenValueMap) {       
        Matcher tokenMatcher = tokenPattern.matcher(template);
        String matchedToken, replaceWith;
        while (tokenMatcher.find()) { 
            matchedToken = tokenMatcher.group(0);
            replaceWith  = tokenValueMap.get(tokenMatcher.group(1));
            replaceWith = ((replaceWith != null) ? replaceWith : '');
            template = template.replace(matchedToken , replaceWith);
        }
        return template;
    }
    
    static String mergeJsonVariables(String template, Map<String, String> tokenValueMap) {
        Matcher tokenMatcher = jsonTokenPattern.matcher(template);
        String matchedToken, jsonField, parsedJsonKey;
        while (tokenMatcher.find()) { 
            jsonField = tokenMatcher.group(1);
            parsedJsonKey = jsonField + '.' + tokenMatcher.group(2);
            //System.debug(tokenMatcher.group(0) + '---' + tokenMatcher.group(1) + '--' + tokenMatcher.group(2));
            if(tokenValueMap.get(parsedJsonKey) == null) {
                parseJson(tokenMatcher.group(1), tokenValueMap);
            }
            template = template.replace(tokenMatcher.group(0) , 
                (tokenValueMap.get(parsedJsonKey)!= null)? tokenValueMap.get(parsedJsonKey) : '');
        }
       return template;
    }
    
    static void parseJson(String jsonField, Map<String, String> tokenValueMap) {
        if (tokenValueMap.get(jsonField) == null) {
            return;
        }
        JSONParser parser = JSON.createParser(tokenValueMap.get(jsonField));
        String fieldName, fieldValue;
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                if (!parser.getText().equals('data')) {
                    fieldName = parser.getText();
                    parser.nextToken();
                } else {
                    parser.nextToken();
                    fieldValue = parser.getText();
                    tokenValueMap.put(jsonField + '.' + fieldName, fieldValue);
                }                
            }
        }
    
    }
    
    
}