@isTest(seealldata=true)
public class AttachmentServiceTest {

    private static void setupData() {
      
        Account a = new Account();
        a.name = 'Talent Candidate';
        a.Talent_Preference__c = '{"skills":"My Skills","goals":"My Goals","interests":"My interests","certifications":"My certs"}';
        insert a;
      Id RecordTypeId = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
        Contact c = new Contact();
        c.accountId = a.Id;
        c.firstname = 'John';
        c.lastname = 'John';
        c.About_Me__c = 'Talent profile summary';
        c.Facebook_URL__c = 'facebook.com';
        c.LinkedIn_URL__c = 'linkedin.com';
        c.email = 'myemail@example.com';
        c.phone = '1234567890';
        c.title = 'my title';
        c.MailingCity = 'my city';
        c.MailingCountry = 'my country';
        c.MailingPostalCode = '60345';
        c.MailingState = 'NY';
        c.MailingStreet = 'My street';
        c.Peoplesoft_ID__c = '099999';
        c.RecordTypeId =RecordTypeId;
        insert c;
        
        Talent_Experience__c te = new Talent_Experience__c();
        te.Degree__c = 'some degree';
        te.Degree_Level__c = 'salesforce';
        te.Graduation_Year__c = '2004';
        te.Notes__c = 'xyz';
        te.Certification__c = 'Force';
        te.Major__c = 'Philosophy';
        te.Organization_Name__c = 'Stanford';
        te.Talent__c = a.Id;
        insert te;
        
         List<Document> existingDoc = [Select Id from Document where DeveloperName = 'TestResumeMergeTemplate'];
         
        if (existingDoc.size() <= 0) {
            List<Folder> fList = [SELECT Id FROM Folder where type='Document' limit 1];
                
            Document d = new Document();
            d.folderId= fList[0].Id;
            d.developerName = 'TestResumeMergeTemplate';
            d.name = 'TestResumeMergeTemplate';
            d.body = Blob.valueOf('<html>{{contact.aboutMe}}<br/>{{account.talentPreference.Json2String(skills)}}</html>');
            
            insert d;
            
            Document d2 = new Document();
            d2.folderId= fList[0].Id;
            d2.developerName = 'TestRWSG2Template';
            d2.name = 'TestRWSG2Template';
            d2.body = Blob.valueOf('<html>{{contact.aboutMe}}<br/>{{account.talentPreference.Json2String(skills)}}</html>');
            
            insert d2;
        } else {
            existingDoc[0].body = Blob.valueOf('<html>{{contact.aboutMe}}<br/>{{account.talentPreference.Json2String(skills)}}</html>');
            
            update existingDoc[0];
        }
       
        
    }
    
     @isTest
    static void testCreateAttachmentWithResume() {
        setupData();
        String peoplesoftId = '099999';
        AttachmentService.getAttachments(peoplesoftId,'TestResumeMergeTemplate','English (United States)');       
    }  
    
    @isTest
    static void testgetAttachmentWithoutResume() {
        setupData();
        String peoplesoftId = '099999';
        AttachmentService.getAttachments(peoplesoftId,'TestRWSG2Template','English (United States)');       
    }  

    @isTest
    static void testInvalidPeoplesoftId() {
        String peoplesoftId = 'aaaaaa';
        AttachmentService.getAttachments(peoplesoftId,'TestRWSG2Template','English (United States)');
    }
    
    @isTest
    static void testEmptyPeoplesoftId() {
        String peoplesoftId = '';
        AttachmentService.getAttachments(peoplesoftId,'TestRWSG2Template','English (United States)');
    }
    @isTest
    static void testEmptyPeoplesoftId123() {
         Account a = new Account();
        a.name = 'Talent Candidate';
        a.Talent_Preference__c = '{"skills":"My Skills","goals":"My Goals","interests":"My interests","certifications":"My certs"}';
        insert a;
        List<Talent_Experience__c> teExp = new List<Talent_Experience__c>(); 
        Talent_Experience__c te123 = new Talent_Experience__c();
        te123.Degree__c = 'some degree';
        te123.Degree_Level__c = 'salesforce';
        te123.Graduation_Year__c = '2004';
        te123.Notes__c = 'xyz';
        te123.Certification__c = 'Force';
        te123.Major__c = 'Philosophy';
        te123.Organization_Name__c = 'Stanford';
        te123.Talent__c = a.Id;
        insert te123;
        teExp.add(te123);
        Map<String, String> testMap = new map<String, String>();
        testMap.Put('364534537','65384734456');
        Id peoplesoftId = a.Id;
        AttachmentService.getEducationDetails(peoplesoftId,testMap,teExp);
        String peoplesoftIdstr = '00364534353';
        AttachmentService.getCertificationDetails(peoplesoftIdstr,testMap,teExp);
    }
    

}