public class AutomateApprovalProcessHelper{
    public static void submitForApproval(List<SObject> recs)
    {
       // Loop over the records and submit the same for approval
        for (SObject obj : recs) 
        {
           try
            {
                Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
                app.setObjectId(string.valueOf(obj.get('id')));
                Approval.ProcessResult result = Approval.process(app);
            }catch(Exception e)
            {}
        }
    }

}