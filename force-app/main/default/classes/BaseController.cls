public with sharing class BaseController {
    //@AuraEnabled is required for any method which will be called from the Lightning Components
    /*
        Standard method definitions:

        @AuraEnabled
        public static returnType methodName([params])
     */
    

    //This is the primary function call from Lightning JS controllers, usually via the BaseDataHelperComponentHelper.js class
    //but can be called from any controller.
    //The idea is to improve code readability and prevent issues with multiple developers changing base controllers,
    //by segmenting all methods into the relevant subclasses. This also allows the different teams (i.e. ATS / CRM / Communities)
    //to update their code with less impact on the others.
    @AuraEnabled
    public static Object performServerCall(String className, String subClassName, String methodName, Map<String, Object> parameters){
        //Set the result to null and handle on the JS controller
        Object result = null;

        System.debug(className);
        System.debug(methodName);
        System.debug('checking logging');
        //Call the relevant performServerCall method in the specified class
        if(className == 'ATS'){
            result = ATSBaseController.performServerCall(subClassName,methodName,parameters);
        }
        else if(className == 'CRM'){
           result = CRMBaseController.performServerCall(subClassName,methodName,parameters);
        }
        else if(className =='Lookup'){
            System.debug('AM I CALLING THE RIGHT SOQL MESSAGE');
            result = BaseLookupController.performServerCall(methodName,parameters);
        }else{
            //If no class specified, look for the method within this class
            Map<String, Object> p = parameters;
            if(methodName == 'getRows'){
                result = getRows((String)p.get('soql'), (String)p.get('maxRows'));
            }else if(methodName == 'getRowCount'){
                result = getRowCount((String)p.get('soql'));
            }else if(methodName == 'getRecord'){
                result = getRecord((String)p.get('soql'));
            }else if(methodName == 'getHRXMLRecord'){
                result = getHRXMLRecord((String)p.get('soql'));
            }else if(methodName == 'deleteRecord'){
                result = deleteRecord((String)p.get('recordId'));
            }else if(methodName == 'getCurrentUser'){
                result = getCurrentUser();
            }else if(methodName == 'getLanguages'){
                result = getLanguages();
            }else if(methodName == 'getUserCategory'){
                result = getUserCategory();
            //Added by KK on 3/19/2018 for S-70726
            }else if(methodName == 'getRecordTypeName'){
                result = getRecordTypeName((String)p.get('recordId'),(String)p.get('objName'));
            }
            // end of code additions
            else if(methodName == 'getCurrentUserWithOwnership'){
                result = getCurrentUserWithOwnership();
            }
			else if(methodName == 'getRowAndCount'){
				result = getRowAndCount((String)p.get('soql')
										, (String)p.get('countSoql')
										, (String)p.get('maxRows'));
			}
			else if(methodName == 'getTalentOverviewModel') {
				result = getTalentOverviewModel((String)p.get('conId'));
			}
			else if(methodName == 'getOrderRecords'){
                result = getOrderRecords((String)p.get('OrderContactRecordID'));
                system.debug('--result--'+result);
            } 

            else if(methodName == 'userHasAccessPermission'){
                return userHasAccessPermission((String)p.get('permissionName'));
            } 
        }
        return result;
    }


    //Gets the current user profile
	@AuraEnabled
    public static User getCurrentUser(){
		//S-95929 Rajeesh Adding alias in user obj for pipeline related story.
        // S-185932 Region__c Added by Rajib Maity
        User user = [select id, Name,Alias, Peoplesoft_Id__c, CompanyName, OPCO__c,Region__c, Country, Profile.Name, Profile.PermissionsModifyAllData, UserPermissionsMarketingUser from User where id = :Userinfo.getUserId()];
        System.debug('Base Cotroller::getCurrentUser:::'+user);
        return user;
    }

	public class TrackingUserInfo {
		@AuraEnabled public UserInGroups userGroups { get; set;}
		@AuraEnabled public ATS_UserOwnershipModal userOwnership { get; set;}
	}

	@AuraEnabled
    public static TrackingUserInfo getTrackingUserInfo() {
		TrackingUserInfo trackInfo = new TrackingUserInfo();
		trackInfo.userOwnership = getUserInfoTalentHeader();
		trackInfo.userGroups = checkGroupPermission();

		return trackInfo;
	}

    @AuraEnabled
    public static ATS_UserOwnershipModal getUserInfoTalentHeader() {

        ATS_UserOwnershipModal uoModal = new ATS_UserOwnershipModal();
		//S-95929 Rajeesh Adding alias in user obj for pipeline related story.
        User objUser = [select id, Name,Alias, Peoplesoft_Id__c, CompanyName, OPCO__c, Title, Country, Profile.Name, Profile.PermissionsModifyAllData,LocaleSidKey from User where id = :Userinfo.getUserId()];

        uoModal.usr = objUser;
		uoModal.hasUserSMSAccess = Utilities.userHasAccessPermission('SMS');
		uoModal.hasUserConsentTextAccess = Utilities.userHasAccessPermission('Text_Consent');

        if(!String.isBlank(objUser.OPCO__c) ) {
            Talent_Role_to_Ownership_Mapping__mdt[] Ownership = [SELECT Opco_Code__c,Talent_Ownership__c,LinkedIn_Sync__c, Ownership_Category__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: objUser.OPCO__c];      
            if(Ownership!=null && !Ownership.isEmpty()) {
                uoModal.userOwnership = Ownership[0].Talent_Ownership__c;
                uoModal.haslinkedInSync = Ownership[0].LinkedIn_Sync__c;   
                if (Ownership[0].Ownership_Category__c != null) {
                      uoModal.userCategory = Ownership[0].Ownership_Category__c; 
                } else{
                     uoModal.userCategory = 'NO_CATEGORY'; 
                }
            }
        }
        return uoModal;
    }


    @AuraEnabled
    public static ATS_UserOwnershipModal getUserInfo() {

     return getCurrentUserWithOwnership();
    }

    public static ATS_UserOwnershipModal getCurrentUserWithOwnership() {
        ATS_UserOwnershipModal uoModal = new ATS_UserOwnershipModal();
		//S-95929 Rajeesh Adding alias in user obj for pipeline related story.
		//S-100826 Neel- adding peoplesoft id in SOQL
		//S-107087 Siva - Adding Email, Phone, Division__c in SOQL
        User objUser = [select id, Name,Peoplesoft_Id__c, Alias, CompanyName, Division__c, OPCO__c, Title, Email, Phone, Profile.Name, Profile.PermissionsModifyAllData, UserPermissionsMarketingUser from User where id = :Userinfo.getUserId()];
        uoModal.usr = objUser;
		uoModal.hasUserSMSAccess = Utilities.userHasAccessPermission('SMS');
		uoModal.hasUserConsentTextAccess = Utilities.userHasAccessPermission('Text_Consent');

        if(!String.isBlank(objUser.OPCO__c) ) {
            Talent_Role_to_Ownership_Mapping__mdt[] Ownership = [SELECT Opco_Code__c,Talent_Ownership__c,LinkedIn_Sync__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: objUser.OPCO__c];      
            if(Ownership!=null && !Ownership.isEmpty()) {
                uoModal.userOwnership = Ownership[0].Talent_Ownership__c;
                uoModal.haslinkedInSync = Ownership[0].LinkedIn_Sync__c;            
            }
        }
        
        uoModal.userCategory = getUserCategory();

		/* Start for story S-107087 added by siva*/
		uoModal.userCPCompany = ATS_ConsentPreferencesController.getUserCompany(objUser);
		/* End for story S-107087 added by siva*/
        
        return uoModal;
    }


    //Get a Map of Languages
    public static Map<String, String> getLanguages(){
        Map<String, String> l = new Map<String, String>();
        List<Global_LOV__c> glist = [SELECT Text_Value_2__c, Text_Value__c FROM Global_LOV__c WHERE LOV_Name__c = 'LanguageCode'];

        for(Global_LOV__c g : glist){
            l.put(g.Text_Value__c, g.Text_Value_2__c);
            l.put(g.Text_Value__c.toUpperCase(), g.Text_Value_2__c);
        }

        return l;
    }

    //Get a list of sObjects based on provided soql query
    private static List<sObject> getRows(String soql, String maxRows) {
        if(soql != '') {
              Integer iMax = Integer.valueof(maxRows);
            String valueQuery = (iMax >= 5000) ? '5000' : string.valueof(iMax);
        
            List<sObject> sobjList = Database.query(soql + ' LIMIT ' + valueQuery);
       
            return sobjList;
        }else
        {
            return new List<sObject>();
        }  
    }

    //Get a count of rows based on provided soql query
    private static String getRowCount(String soql) {
        if(soql != ''){
            String countQuery = soql + ' LIMIT 5000';
            Integer i = Database.countQuery(countQuery);
            return string.valueof(i);
        }else{
            return '0';
        }
    }

	private static ListAndCount getRowAndCount(String soql
												, String countSoql
												, String maxRows) {
		//getRowCount: wants to return a maximum amount LIMIT 5000
		//getRows: maxRows is to get for displaying purposes
		return new ListAndCount(
						getRowCount(countSoql)
						, getRows(soql, maxRows));
	}

	public class ListAndCount {	
		@AuraEnabled	
		public String count;
		@AuraEnabled
		public List<SObject> records;
		public ListAndCount(String count, List<SObject> records) {
			this.count = count;
			this.records = records;
		}
	}

	private static TalentOverviewModel getTalentOverviewModel(String contactId) {
		return new TalentOverviewModel(
						getContactRecord(contactId)
						, getCurrentUserWithOwnership()
						, getLanguages()
						, ATSBaseController.performServerCall('','getTalentBaseFields', new Map<String, String>{'conId' => contactId}) 
                        , ATSTalentEmploymentFunctions.performServerCall('getCurrencyList', new Map<String, String>()) );
	}

	public class TalentOverviewModel {
		//record is the account Record related to the contact talent
		@AuraEnabled
		public SObject record;
		@AuraEnabled
		public ATS_UserOwnershipModal uoModal;
		@AuraEnabled
		public Map<String, String> languageMap;
		@AuraEnabled
		public Object talentBaseFields;
        @AuraEnabled
        public Object currencyList;

		public TalentOverviewModel(SObject record, ATS_UserOwnershipModal uoModal, Map<String, String> languageMap, Object talentBaseFields, Object currencyList) {
			this.record = record;
			this.uoModal = uoModal;
			this.languageMap = languageMap;
			this.talentBaseFields = talentBaseFields;
            this.currencyList = currencyList;
		}
	}

    //Get an sObjects based on provided soql query
	private static sObject getContactRecord(String contactId) {
		Id accountId = [select accountId from contact where id=:contactId].accountId;
		String soql = 'SELECT Id, Name, Talent_Preference_Internal__c, Talent_Summary_Last_modified_by__r.Name, Talent_Summary_Last_Modified_date__c, '+
					 'Talent_Overview__c, Skills__c, Skill_Comments__c, Willing_to_Relocate__c, Desired_Rate__c, Desired_Rate_Max__c, Desired_Salary__c, ' +
					 'Desired_Salary_Max__c, Desired_Rate_Frequency__c, Desired_Currency__c, Desired_Placement_type__c, Shift__c, Desired_Schedule__c, Desired_Bonus__c, '+
					 'Desired_Bonus_Percent__c, Desired_Additional_Compensation__c, Desired_Total_Compensation__c, Goals_and_Interests__c,G2_Completed_By__c, '+
					 'G2_Completed_Date__c, G2_Completed__c, Talent_Security_Clearance_Type__c,Talent_FED_Agency_CLR_Type__c, Account.Date_Available__c, Account.Do_Not_Contact__c, Account.Do_Not_Contact_Reason__c , Account.Do_Not_Contact_Date__c , Account.Talent_Current_Employer_Text__c, Account.Talent_Current_Employer__c, '+
					 '(select id, activityDate,Description, Owner.Name from tasks where WhatId = :accountId and Type = \'' + String.escapeSingleQuotes('G2') + '\' and status =\'' + String.escapeSingleQuotes('Completed') + '\' order by CreatedDate  desc limit 1) FROM Account WHERE Id =: accountId';
		
		sObject[] sobjList = Database.query(soql);
		if (sobjList.size() > 0){
            return sobjList[0];
        }else{
            return null;
        }
	}
    private static sObject getRecord(String soql) {
        if(soql != ''){
            
            sObject[] sobjList =  Database.query(soql + ' LIMIT 1');
            if (sobjList.size() > 0){
                return sobjList[0];
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    //Get a JSON based field based on provided soql query
    private static string getHRXMLRecord(String soql) {
        String hrxmlField = soql.subString(6, soql.indexOf('FROM')).Trim();
        sObject sobj = Database.query(soql + ' LIMIT 1');
        String result = '';
        if(sobj != null){
            Object field = sobj.get(hrxmlField);
            if(field != null)
            {
               result= String.valueOf(field).Trim();
            }
        }
        return result;
    }

    
    @AuraEnabled
    public static Account getRecords(String recordId){
        system.debug('@@@@@ Returning Record');
        return [ SELECT Id, Name from Account where id =: recordId];
    }

    @AuraEnabled
    public static String getUserCategory(){
     User CUser = [Select Id,OPCO__c from User where Id =:UserInfo.getUserId()];
     List<Talent_Role_to_Ownership_Mapping__mdt> OwnershipCategory = new List<Talent_Role_to_Ownership_Mapping__mdt>([SELECT Opco_Code__c,Ownership_Category__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: CUser.OPCO__c]);
     if(OwnershipCategory != null && OwnershipCategory.size() > 0){
        return OwnershipCategory[0].Ownership_Category__c; 
        }else{
            return 'NO_CATEGORY'; 
        }
    }
    
    @AuraEnabled
    public static void updateRecord(sObject record) {
        try {
            Database.update(record);
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
/*    @AuraEnabled
    public static List<Talent_Document__c> talentDocuments(String recordId){
        return [SELECT CreatedDate,Id, CreatedBy.Name, Document_Name__c, Azure_URL__c, Document_Type__c, Default_Document__c, (Select Id from Attachments) FROM Talent_Document__c WHERE mark_for_deletion__c = false and committed_document__c = true  and Talent__c =: recordId ORDER BY LastModifiedDate DESC];
    }
*/
    //Delete a record based on the provided recordId
    private static string deleteRecord(String recordId){
        if(recordId != ''){
            Database.delete(recordId);
            return 'Success';
        }else{
            return 'No Record Id specified';
        }
    }

    //Added by KK on 3/19/2018 for S-70726
    //Method to get Name of Recordtype based on the object name and record id
    // use dataservice to get this data instead of querying on quick action
    public static String getRecordTypeName(String recordId,String objName) {
        
        system.debug('@@@@@recordId'+recordId+objName);
        try {
            if(recordId != '' && recordId != null) {
                String query='select RecordTypeId,RecordType.DeveloperName from '+objName+' where Id =:recordId';
                sObject s = Database.query(query);  
                String recordTypeName = (String) s.getSobject('RecordType').get('DeveloperName');
                return recordTypeName;
            } else {
                return 'No record type name';
            }       
        } catch (Exception e) {
            return 'No record type name';
        }
    }
	
	@AuraEnabled 
    public static Order getOrderRecords(String recordId){
        system.debug('--record Id for OrderRecords--'+recordId);
        try{
            return [ SELECT Id, Submittal_Not_Proceeding_Reason__c, Comments__c, ShipToContactId,Display_LastModifiedDate__c, Status  
                from Order 
                where ShipToContactId =: recordId order by Display_LastModifiedDate__c desc limit 1];
           }catch(exception ex){
               system.debug(ex.getMessage());
               return null;
           } 
    }
	//S-157807 - Modified method to make it generic 
	@AuraEnabled
	public static Boolean checkPermissionSetAccess(String apiName){
		Boolean isDnu = false;
		List<PermissionSetAssignment> currentUserPerSet =  [SELECT Id, PermissionSet.Name,AssigneeId from PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() and PermissionSet.Name =:apiName ];
		if (currentUserPerSet.size() > 0){
			for (PermissionSetAssignment psa: currentUserPerSet){
				if(psa.PermissionSet.Name.equals(apiName)){
					isDnu = true;
				}
			
			}
		}

		return isDnu;
	}

	public class UserInGroups {
		@AuraEnabled public boolean isInGroupA { get; set;}
		@AuraEnabled public boolean isInGroupB { get; set;}
		@AuraEnabled public boolean isInGroupC { get; set;}
	}

	@AuraEnabled
    public static UserInGroups checkGroupPermission() {
		UserInGroups userGroup = new UserInGroups();
		userGroup.isInGroupA =FeatureManagement.checkPermission('GroupA');
		userGroup.isInGroupB =FeatureManagement.checkPermission('GroupB');
        userGroup.isInGroupC =FeatureManagement.checkPermission('GroupC');
        return userGroup;        
    }

   //@AuraEnabled 
   //public static Boolean userHasAccessPermission(String permissionName) {
   //               system.debug('permissionName : ' + permissionName);

   // Boolean hasAccess=false;
   // List<PermissionSetAssignment> usrPermSets = [Select Id, PermissionSet.Name,AssigneeId from PermissionSetAssignment where AssigneeId=:UserInfo.getUserId()];
   // for(PermissionSetAssignment psa: usrPermSets){
   //                   system.debug('psa.PermissionSet.Name : ' + psa.PermissionSet.Name);

   //     if(psa.PermissionSet.Name.equals(permissionName)) {
   //         hasAccess = true;
   //         //break; 
   //     }
   // }
   // return hasAccess;
   //}


    @AuraEnabled  
   public static Boolean userHasAccessPermission(String permissionName){
    Boolean hasAccess=false;
    Boolean hasCustomPermission = FeatureManagement.checkPermission(permissionName);
    System.debug('hasCustomPermission'+hasCustomPermission);
    return hasCustomPermission;
   }

    @AuraEnabled
    public static String callSearchDedupeService(String talentDocID, String Source, String contactDetails, String transactionId){
       // talentDocID  will be talent document id
       // Source from which this process initiated eg: Find_Add_Talent
       // ContactDetails will be ParsedResume json contact Json string, this will only have value for Find_Add_Talent flow.
        system.debug('callSearchDedupeService--talentDocID'+talentDocID);
        system.debug('callSearchDedupeService--Source'+Source);
        system.debug('callSearchDedupeService--contactDetails'+contactDetails);
		system.debug('callSearchDedupeService--transactionId'+transactionId);
        
        User CUser = [Select Id,OPCO__c from User where Id =:UserInfo.getUserId()];
        List<Data_Shielding_Search_Query_Filter__mdt> searchFilter = new List<Data_Shielding_Search_Query_Filter__mdt>([SELECT OPCO_Name__c,Filter_String__c FROM Data_Shielding_Search_Query_Filter__mdt Where OPCO_Name__c =: CUser.OPCO__c]);
        
        String AccountID ='';
        String responseBody ='';  
        String jsonBody='';
        String UnparssedBase64String='';
        String ServiceUrl = '';
        String firstName='';
        String lastName ='';
        String sourceSystemId='';
        String city='';
        String country='';
        String state ='';

        List<String> phoneList = new List<String>();
        List<String> emailList = new List<String>();
        
        String jsonEmailString ='[]';
        String jsonPhoneString='[]';
        
        List<Talent_Document__c> td = [SELECT Id, Name, Talent__c FROM Talent_Document__c WHERE id=:talentDocID];
        
        Attachment[] atts = [SELECT Id, Name, Body from Attachment WHERE ParentId = :talentDocID];
        UnparssedBase64String = EncodingUtil.base64Encode(atts[0].Body);
        
        ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues('Data Quality Service');
        ServiceUrl = serviceSettings.Service_URL__c;
        
        if(Source=='Find_Add_Talent' && contactDetails != '' && contactDetails != null && contactDetails !='"null"'){
                String parsedHRXML ='';
            	Map<String, Object> objEntry = (Map<String, Object>) JSON.deserializeUntyped(contactDetails);
                if(objEntry.containsKey('firstname')){
                    firstName = String.valueOf(objEntry.get('firstname'));
                }
                if(objEntry.containsKey('lastname')){
                    lastName = String.valueOf(objEntry.get('lastname')); 
                }
                if(objEntry.containsKey('parsedHRXML')){
                    parsedHRXML = String.valueOf(objEntry.get('parsedHRXML')); 
                }
               if(objEntry.containsKey('email')){
                    if(objEntry.get('email') != null && objEntry.get('email') != ''){
                        emailList.add(String.valueOf(objEntry.get('email')));
                    }
            	}
            	if(objEntry.containsKey('otheremail')){
                    if(objEntry.get('otheremail') != null && objEntry.get('otheremail') != ''){
                        emailList.add(String.valueOf(objEntry.get('otheremail')));
                    }
            	}
            	if(objEntry.containsKey('workemail')){
                    if(objEntry.get('workemail') != null && objEntry.get('workemail') != ''){
                        emailList.add(String.valueOf(objEntry.get('workemail')));
                    }
            	}
            	if(objEntry.containsKey('mobilephone')){
                    if(objEntry.get('mobilephone') != null && objEntry.get('mobilephone') != ''){
                        phoneList.add(String.valueOf(objEntry.get('mobilephone')));
                    }
            	}
            	if(objEntry.containsKey('workphone')){
                    if(objEntry.get('workphone') != null && objEntry.get('workphone') != ''){
                        phoneList.add(String.valueOf(objEntry.get('workphone')));
                    }
            	}
            	if(objEntry.containsKey('homephone')){
                    if(objEntry.get('homephone') != null && objEntry.get('homephone') != ''){
                        phoneList.add(String.valueOf(objEntry.get('homephone')));
                    }
            	}
            	jsonEmailString = json.serialize(emailList);
                jsonPhoneString = json.serialize(phoneList);
            
                
            jsonBody ='{'+
            '"sourceSystemId":"'+sourceSystemId+'",'+
            '"firstName":  "'+firstName+'",'+
            '"lastName":  "'+lastName+'",'+
            '"email":  '+jsonEmailString+','+
            '"phone":  '+jsonPhoneString+','+                     
            '"city":  "'+city+'",'+                     
            '"state":  "'+state+'",'+                       
            '"country":  "'+country+'",'+ 
            '"unparsedResume" :  "'+UnparssedBase64String+'"'+
			'}';        
            
        }else if(Source !='Find_Add_Talent'){
            List<Contact> conObject =[select Id, firstName, AccountID,lastName,
                                                Phone,MobilePhone,HomePhone,OtherPhone,
                                                Email, Work_Email__c, Other_Email__c, Account.Source_System_Id__c,
                                                MailingState,MailingCity,Talent_Country_Text__c from Contact where AccountID=: td[0].Talent__c ];
            
            
            
            if(conObject.size()>0){
                AccountID = conObject[0].AccountID;
                firstName = conObject[0].firstName;
                lastName = conObject[0].lastName; 
                sourceSystemId = conObject[0].Account.Source_System_Id__c;
                city =conObject[0].MailingCity;
                state =conObject[0].MailingState;
                country =conObject[0].Talent_Country_Text__c;
                if(conObject[0].HomePhone != null ){
                    phoneList.add(conObject[0].HomePhone);
                }
                if(conObject[0].MobilePhone != null){
                    phoneList.add(conObject[0].MobilePhone);
                }
                if(conObject[0].OtherPhone != null){
                    phoneList.add(conObject[0].OtherPhone);
                }
                if(conObject[0].Email != null ){
                    emailList.add(conObject[0].Email);
                }
                if(conObject[0].Other_Email__c != null){
                    emailList.add(conObject[0].Other_Email__c);
                }
                if(conObject[0].Work_Email__c != null){
                    emailList.add(conObject[0].Work_Email__c);
                }
                jsonEmailString = json.serialize(emailList);
                jsonPhoneString = json.serialize(phoneList);
            }
            // Adding candidateId new parameter to request when service being called from Talent Landing Page
            jsonBody ='{'+
            '"candidateId":  "'+AccountID+'",'+
            '"sourceSystemId":"'+sourceSystemId+'",'+
            '"firstName":  "'+firstName+'",'+
            '"lastName":  "'+lastName+'",'+
            '"email":  '+jsonEmailString+','+
            '"phone":  '+jsonPhoneString+','+
            '"city":  "'+city+'",'+                     
            '"state":  "'+state+'",'+                       
            '"country":  "'+country+'",'+                       
            '"unparsedResume" :  "'+UnparssedBase64String+'"'+
            '}';
        }
        
        
        //system.debug('Email List:'+emailList.size());
       	//system.debug('Phone List:'+phoneList.size());
        system.debug('JsonBody-->'+jsonBody);
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        try{
            
            Http http = new Http();
            request.setEndpoint(ServiceUrl+searchFilter[0].Filter_String__c);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setHeader('Authorization', 'Bearer '+ serviceSettings.OAuth_Token__c);
            request.setBody(jsonBody);
            request.setTimeout(120000);
            response = http.send(request);
            System.debug('Response-->'+ response.getStatusCode());
            if (response.getStatusCode() == 200 && (!Test.isRunningTest()))  {
                
                //system.debug('Response Body-->'+response.getBody());
             
              DataQualityServiceResponseWrapper  wrap =  DataQualityServiceResponseWrapper.parse(String.valueof(response.getBody()));
              system.debug('Wrap--'+wrap.response.duplicates);
			               
                if(wrap.response.duplicates.status=='FOUND'){
                    responseBody = JSON.serialize(wrap.response.duplicates.records);
                }else{
                 	responseBody = 'No_Result_Found';   
                }
            } else {
               // callInternalDedupe
              // createLogForFailedCallout( 'callSearchDedupeService', responseBody);
                
               List<List<SObject>> internalDedupeResult  = new List<List<SObject>>();
                
                if(phoneList.size()==0 && emailList.size()==0){
                    responseBody = 'No_Result_Found'; 
                }else{
                    internalDedupeResult = TalentDedupeSearch.postDedupTalents(firstName+' '+lastName, emailList, phoneList, '', '', new List<String>{'Talent'}, 'Many', true );
                    system.debug('internalDedupeResult-->'+internalDedupeResult);
                    if(internalDedupeResult.size() >0){
                        List <DataQualityServiceResponseWrapper.Records> wrapRecordList = new List <DataQualityServiceResponseWrapper.Records>();
                        for(integer i=0; i<internalDedupeResult[0].size(); i++){
                            DataQualityServiceResponseWrapper.Records wrapRecord = new DataQualityServiceResponseWrapper.Records();
                            DataQualityServiceResponseWrapper.DuplicateTalent dupeRecord = new DataQualityServiceResponseWrapper.DuplicateTalent();
                            dupeRecord.candidateId= String.valueof(internalDedupeResult[0][i].get('AccountId'));
                            dupeRecord.contactId = String.valueof(internalDedupeResult[0][i].get('Id'));
                            dupeRecord.firstName = String.valueof(internalDedupeResult[0][i].get('FirstName'));
                            dupeRecord.lastName = String.valueof(internalDedupeResult[0][i].get('LastName'));
                            dupeRecord.city = String.valueof(internalDedupeResult[0][i].get('MailingCity'));
                            dupeRecord.state = String.valueof(internalDedupeResult[0][i].get('MailingState'));
                            dupeRecord.country = String.valueof(internalDedupeResult[0][i].get('Talent_Country_Text__c'));
                            dupeRecord.resumeModifiedDate = String.valueOf(DateTime.valueOf(internalDedupeResult[0][i].get('LastModifiedDate')).date());
                            if(internalDedupeResult[0][i].get('LastActivityDate') != null){
                            	dupeRecord.qualificationsLastActivityDate = String.valueOf(Date.valueOf(internalDedupeResult[0][i].get('LastActivityDate')));    
                            }                            
                            dupeRecord.candidateStatus = String.valueof(internalDedupeResult[0][i].get('Candidate_Status__c'));
                            dupeRecord.talentId = String.valueof(internalDedupeResult[0][i].get('Talent_Id__c'));
                            dupeRecord.otherPhone = String.valueOf(internalDedupeResult[0][i].get('OtherPhone'));
                            dupeRecord.mobilePhone = String.valueOf(internalDedupeResult[0][i].get('MobilePhone'));
                            dupeRecord.workPhone = String.valueOf(internalDedupeResult[0][i].get('phone'));
                            dupeRecord.homePhone = String.valueof(internalDedupeResult[0][i].get('HomePhone'));
                            dupeRecord.otherEmail = String.valueof(internalDedupeResult[0][i].get('Other_Email__c'));
                            dupeRecord.personalEmail = String.valueof(internalDedupeResult[0][i].get('email'));
                            dupeRecord.workEmail = String.valueof(internalDedupeResult[0][i].get('Work_Email__c'));
                            dupeRecord.employmentPositionTitle = String.valueof(internalDedupeResult[0][i].get('title'));
                            wrapRecord.duplicateTalent = dupeRecord;
                            wrapRecord.similarity = 0.0;
                            wrapRecord.duplicateMatchFlags = new List<String>{'Internal_Dedupe_Match'};
                            wrapRecordList.add(wrapRecord);                            
                        } 
                       // system.debug('wrapRecordList-->'+wrapRecordList);
                        if(wrapRecordList.size()>0){
                           responseBody = JSON.serialize(wrapRecordList);
                		}else{
                 			responseBody = 'No_Result_Found';   
                		}
                        
                    }
                }
            }

			if(transactionId != '' && transactionId !=null){
				if(responseBody !='No_Result_Found'){
					logInformation(jsonBody,'Result Found',transactionId);
				}else{
					logInformation(jsonBody,responseBody,transactionId);
				}
				
			}
            
 
          return responseBody;
        }
        catch(System.StringException e){
            system.debug('Exception -->'+e.getMessage());
            //createLogForFailedCallout('callSearchDedupeService', e.getStackTraceString());
			ConnectedLog.LogException('BaseController','callSearchDedupeService',e);
            return null;
        }
      
    }

    /*
    public static void createLogForFailedCallout(String methodName, String exceptionString){
        Log__c callOutLog = new Log__c();
        callOutLog.Class__c = 'BaseController';
        callOutLog.Method__c = methodName;
        callOutLog.Description__c = exceptionString;           
        DateTime TodayDateTime = datetime.parse(system.now().format());         
        callOutLog.Log_Date__c = TodayDateTime;
        
        callOutLog.Subject__c = 'upload resume flow';
        database.insert(callOutLog);
        
    }
    */   

    @AuraEnabled
    public static Map<string,string> updateTalentStatus(String recordId, Boolean activateTalent){
        return (TalentDeactivateRESTService.doPost(recordId,activateTalent));
    }
    
    @AuraEnabled    
    public static Boolean checkTEKCreateGroupAccess() 
    {   
        List<GroupMember> members = [SELECT Id,GroupId,UserOrGroupId    
                                     FROM GroupMember   
                                     WHERE Group.DeveloperName = 'TEK_Create_New_Req_Access' AND UserOrGroupId =: UserInfo.getUserId()];    
        if(!members.isEmpty())  
            {   
                return TRUE;    
            }   
            return FALSE;   
      }
    
    @AuraEnabled    
    public static Boolean checkAerotekCreateGroupAccess() 
    {   
        List<GroupMember> members = [SELECT Id,GroupId,UserOrGroupId    
                                     FROM GroupMember   
                                     WHERE Group.DeveloperName = 'Aerotek_Create_New_Req_Access' AND UserOrGroupId =: UserInfo.getUserId()];    
        if(!members.isEmpty())  
            {   
                return TRUE;    
            }   
            return FALSE;   
      }

	  @AuraEnabled
		public static void logInformation(String request, String result, String transactionId){
			System.debug('LogInformation_request'+request);
			System.debug('LogInformation_result'+result);
			System.debug('LogInformation_transactionId'+transactionId);
			
			Map<String,String> infoMap = new Map<String,String>();
			//infoMap.put('TransactionId',transactionId);
			infoMap.put('Type','Resume Search');
			infoMap.put('Request',request);
			infoMap.put('Result',result);

			ConnectedLog.LogInformation('Find/Add Page','BaseController','callSearchDedupeService',transactionId,'','logging',infoMap);
		}

    //S-224383    
	@AuraEnabled
    public static Boolean isTFOGroupMember(){
        try{
            List<groupmember> groupMembers = [SELECT userorgroupid FROM groupmember 
                                            WHERE group.DeveloperName  = 'Talent_First_Opportunity_PG'
                                            AND userorgroupid =: UserInfo.getUserId() WITH SECURITY_ENFORCED];
            boolean isTFOGroupMember = (groupMembers.size() == 0)?false:true;            
            return isTFOGroupMember;
        }catch(Exception excp){
            ConnectedLog.LogException('BaseController','isTFOGroupMember',excp);
            return false;
        }       
    }
    //Added w.r.t Stroy S-238208
    @AuraEnabled
    public static Map<String,Object> getCaseCurrentUserTalentObj(String recId) {
        try{
            list<Contact>TalentRec=new List<Contact>();
        	TalentRec=[Select Id, Name, AccountId, Lead_Talent__c from Contact where id =:recId];
        	Map<String,Object> CurrentUserWithTalent=new Map<String,Object>();        
        	ATS_UserOwnershipModal uoModal = new ATS_UserOwnershipModal();
			uoModal= getCurrentUserWithOwnership();
        	CurrentUserWithTalent.put('UserModel',uoModal);        
        	CurrentUserWithTalent.put('ContactRec',TalentRec[0]);
        	return CurrentUserWithTalent;
        }catch(Exception excp){
            ConnectedLog.LogException('BaseController','getCaseCurrentUserTalentObj',excp);
            return null;
        }      
    }
    //End of Added w.r.t Stroy S-238208
	//S-248354
	@AuraEnabled
    public static string isTFOBoardMember(){
        Callable extension = (Callable) Type.forName('DRZ_ConnectedBridge').newInstance();
        Object result = extension.call('isTFOBoardMember', new Map<String, Object>());
        system.debug('-result-'+result);
        return JSON.serialize(result);
    }
}