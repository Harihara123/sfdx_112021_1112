@isTest
public class BaseController_Test {
    @isTest static void test_performServerCall() {
        //No Params Entered..
        Object r = BaseController.performServerCall('','','',null);
        System.assertEquals(r,null);

        //Test ClassName Routing
        sObject t1 = (sObject)BaseController.performServerCall('ATS','','', null);
        System.assertEquals(t1, null);

        sObject t2 = (sObject)BaseController.performServerCall('CRM','','', null);
        System.assertEquals(t2, null);
    }

    //This method can be called seperately from other classes without using the performServerCall
    @isTest static void test_getCurrentUser() {
        Test.startTest();
        User u = BaseController.getCurrentUser();
        Test.stopTest();
        
        System.assertEquals(u.Id, Userinfo.getUserId());
    }

    @isTest static void testuserHasAccessPermission() {
            Test.startTest();
            User u = BaseController.getCurrentUser();

            Boolean hasCustomPermission = FeatureManagement.checkPermission('SMS');
            Boolean hasSMSPermssionAccess = BaseController.userHasAccessPermission('SMS');
            System.assertEquals(hasCustomPermission, hasSMSPermssionAccess);

            hasSMSPermssionAccess = BaseController.userHasAccessPermission('SMS_2');
            System.assertEquals(hasSMSPermssionAccess, false);

            Test.stopTest();
    } 

    //This method can be called seperately from other classes without using the performServerCall
    @isTest static void test_getCurrentUserWithOwnership() {
        Test.startTest();
        ATS_UserOwnershipModal modal = BaseController.getCurrentUserWithOwnership();
        User usr = modal.usr;
        Test.stopTest();
        
        System.assertEquals(usr.Id, Userinfo.getUserId());
    }
    
    //This method can be called seperately from other classes without using the performServerCall
    @isTest(seeAllData=true) static void test_getLanguages() {
        Test.startTest();
        Map<String, String> l = (Map<String, String>)BaseController.performServerCall('','','getLanguages', null);
        Test.stopTest();
        System.assertEquals(false, l.isEmpty());
    }

    @isTest static void test_method_getCurrentUser() {
        Test.startTest();
        User u = (User)BaseController.performServerCall('','','getCurrentUser', null);
        Test.stopTest();
        System.assertEquals(u.Id, Userinfo.getUserId());
    }

    @isTest static void test_method_getUserCategory() {
        Test.startTest();
        String userCategory = '';
        String testUserCategory = ''; 
        User CUser = [Select Id,OPCO__c from User where Id =:UserInfo.getUserId()];
        List<Talent_Role_to_Ownership_Mapping__mdt> OwnershipCategory = new List<Talent_Role_to_Ownership_Mapping__mdt>([SELECT Opco_Code__c,Ownership_Category__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: CUser.OPCO__c]);
        if(OwnershipCategory != null && OwnershipCategory.size() > 0){
            userCategory = OwnershipCategory[0].Ownership_Category__c; 
           }
        testUserCategory = (String)BaseController.performServerCall('','','getUserCategory', null);
        Test.stopTest();
        System.assertEquals(userCategory, testUserCategory);
    }
    
    @isTest static void test_method_getRows(){
        String soql = 'SELECT Id FROM Talent_Recommendation__c';
        Map<String,Object> p = new Map<String,Object>();
        p.put('soql', soql);
        p.put('maxRows', '5');

        Test.startTest();
        List<Talent_Recommendation__c> t = Database.Query(soql + ' LIMIT 5');
        List<Talent_Recommendation__c> t2 = (List<Talent_Recommendation__c>)BaseController.performServerCall('','','getRows', p);

        System.assertEquals(t,t2);

        p.clear();
        p.put('soql', '');
        p.put('maxRows', '5');
        List<sObject> t3 = (List<sObject>)BaseController.performServerCall('','','getRows', p);
        Test.stopTest();
        System.assertEquals(t3,new List<sObject>());
    }

    @isTest static void test_method_getRowCount(){
        Test.startTest();
        String soql = 'SELECT count() FROM Talent_Recommendation__c';
        Map<String,Object> p = new Map<String,Object>();
        p.put('soql', soql);
        Integer t = Database.countQuery(soql);
        String t2 = (String)BaseController.performServerCall('','','getRowCount', p);

        System.assertEquals(string.valueof(t),t2);

        p.clear();
        p.put('soql', '');
        String t3 = (String)BaseController.performServerCall('','','getRowCount', p);
        Test.stopTest();
        System.assertEquals('0',t3);
    }

	@isTest static void test_getRowAndCount() {
        String soql = 'SELECT Id FROM Talent_Recommendation__c';
		String countSoql = 'SELECT count() FROM Talent_Recommendation__c';
        Map<String,Object> p = new Map<String,Object>{'soql' => soql
														, 'countSoql' => countSoql
														, 'maxRows' => '5'};

		Test.startTest();
		BaseController.ListAndCount rncModel = (BaseController.ListAndcount)BaseController.performServerCall('','','getRowAndCount', p);
		Test.stopTest();

		System.assertNotEquals(rncModel, null, 'there should be a model of row and count in one call');
	}

    @isTest static void test_method_getRecord(){
        Account newAcc = createTalentAccount('');

        Test.startTest();
        String soql = 'SELECT Id FROM Account WHERE Id =\'' + newAcc.Id + '\'';
        Account t = Database.Query(soql);
        Map<String,Object> p = new Map<String,Object>();
        p.put('soql', soql);
        Account t2 = (Account)BaseController.performServerCall('','','getRecord', p);

        System.assertEquals(t.Id, t2.Id);

        p.clear();
        p.put('soql','');
        sObject t3 = (sObject)BaseController.performServerCall('','','getRecord', p);
        System.assertEquals(t3, null);

        Test.stopTest();

    }

	@isTest static void test_getTalentOverviewModel() {
		Account talentAccount = createTalentAccount('');
		Contact talentContact = createTalentContact(talentAccount.Id);
		String soql = 'SELECT Id FROM Account WHERE Id =\'' + talentAccount.Id + '\'';
		Map<String, Object> p = new Map<String, Object>{'soql' => soql, 'conId' => talentContact.Id};
		Test.startTest();
		BaseController.TalentOverviewModel toModel = (BaseController.TalentOverviewModel)BaseController.performServerCall('', '', 'getTalentOverviewModel', p);
		Test.stopTest();
		System.assertNotEquals(toModel, null, 'toModel should exist');
	}

    @isTest static void test_method_getHRXMLRecord(){
        Account newAcc = createTalentAccount('');

        Test.startTest();
        Map<String,Object> p = new Map<String,Object>();
        p.put('soql', 'SELECT Talent_Preference_Internal__c FROM Account WHERE Id = \'' + newAcc.Id +'\'');
        String t2 = (String)BaseController.performServerCall('','','getHRXMLRecord', p);
        Test.stopTest();
        System.assertEquals(t2, newAcc.Talent_Preference_Internal__c);
    }

    @isTest static void test_method_deleteRecord(){
        Account newAcc = createTalentAccount('');

        Test.startTest();
        Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newAcc.Id);
        String t2 = (String)BaseController.performServerCall('','','deleteRecord', p);

        System.assertEquals(t2, 'Success');

        p.clear();
        p.put('recordId','');
        String t3 = (String)BaseController.performServerCall('','','deleteRecord', p);
        Test.stopTest();
        System.assertEquals(t3, 'No Record Id specified');
    }

    //Added by KK on 3/19/2018 for S-70726
    @isTest static void test_method_getRecordTypeName() {
        Account newAcc = createClientAccount();
        Contact newCon = createClientContact(newAcc.Id);
        Test.startTest();
        Map<String,Object> p = new Map<String,Object>();
        p.put('recordId', newAcc.Id);
        p.put('objName','Account');
        String t1 = (String)BaseController.performServerCall('','','getRecordTypeName', p);
        System.assertEquals(t1, 'Client');
        
        p.clear();
        p.put('recordId','');
        String t2 = (String)BaseController.performServerCall('','','getRecordTypeName', p);
        System.assertEquals(t2, 'No record type name');

        //call the static method directly without using the performServerCall.
        String t3 = BaseController.getRecordTypeName(newCon.Id,'Contact');
        System.assertEquals(t3, 'Client');
        Test.stopTest();
    }
    //end of code additions - KK
    //Test Data Methods
    public static Account createTalentAccount(String clientAccountID){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);

        Account newAcc = new Account(Name='Test Talent', RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);

        if(clientAccountID != ''){
            newAcc.Current_Employer__c = clientAccountID;
        }

        Database.insert(newAcc);
        return newAcc;
    }

    //Added by KK on 3/19/2018 for S-70726
    public static Account createClientAccount(){
        Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);

        return newClientAcc;
    }
    //end of code additions - KK
    @isTest static void setLinkedInMock(){
	LinkedInAuthHandlerForAts tHandler = new LinkedInAuthHandlerForAts(LinkedInEnvironment.DEV);
		LinkedInAuthHandlerForATS.AuthJSON mReturnJson = new LinkedInAuthHandlerForATS.AuthJSON();
    	mReturnJson.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
    	mReturnJson.expires_in = '1799';
    	LinkedInIntegrationHttpMock tMock = new LinkedInIntegrationHttpMock(mReturnJson, 200);
		Test.setMock(HttpCalloutMock.class, tMock);
	
	
	}
    public static Contact createTalentContact(String accountID){
	   
		List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Contact'];
        Contact newC = new Contact(Firstname='Test', LastName='Talent'
                                    ,AccountId=accountID, Website_URL__c='test.com'
                                    ,MailingStreet = 'Address St'
                                    ,MailingCity='City',
									RecordTypeId = types[0].Id
                                    ,Email='tast@talent.com');
        Database.insert(newC);
		setLinkedInMock();
        return newC;
    }
    
    //Added by KK on 3/19/2018 for S-70726
    public static Contact createClientContact(String accountID){
        Contact newC = new Contact(Firstname='Test', LastName='Client'
                                    ,AccountId=accountID, Website_URL__c='test.com'
                                    ,MailingStreet = 'Address St'
                                    ,MailingCity='City'
                                    ,Email='tast@client.com');
        Database.insert(newC);
        return newC;
    }
    //end of code additions - KK
    
    public static Talent_Experience__c createTalentEmployment(String accountID, Boolean allegisPlacement){
        Talent_Experience__c e = new Talent_Experience__c( Notes__c ='Some Notes'
                                                            ,Allegis_Placement__c = allegisPlacement
                                                            ,type__c = 'Work'
															,PersonaIndicator__c = 'Recruiter'//S-82159:Added by siva 12/6/2018
                                                            ,Talent__c = accountID);
        Database.insert(e);
        return e;
    }
	 
	@isTest static void test_allUncoveredMethods() {
        Test.startTest();
			BaseController.getUserInfo();
			BaseController.getTrackingUserInfo();
			BaseController.getUserInfoTalentHeader();
			BaseController.getUserInfoTalentHeader();
			Account talentAccount = createTalentAccount('');			
			BaseController.getRecords(talentAccount.Id);
			BaseController.updateRecord(talentAccount);			
			BaseController.checkPermissionSetAccess('BaseController');
			//BaseController.createLogForFailedCallout('getUserInfo', 'Test Class Check');
			Account newAcc1 = createTalentAccount('');
			Contact con = createTalentContact(newAcc1.Id);
			BaseController.updateTalentStatus(con.Id, false);

			Account newAccount = new Account(
				Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = '1-4W6J-1',
				Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
				ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
				ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
				BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
				BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
			);
			insert newAccount;       
			//string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
		   opportunity  TestOpp=new opportunity ();
		   date mydate = date.parse('12/31/2030');
		   TestOpp.name='TestOpp1';
			// TestOpp.RecordTypeId = reqRecordTypeId;
		   TestOpp.StageName='Test';
		   TestOpp.CloseDate=mydate;
		   TestOpp.AccountId = newAccount.Id; 
		   TestOpp.OpCo__c='Aerotek, Inc';
		   insert TestOpp;
		   Order testOrder = new Order();
		   testOrder.AccountId=newAccount.Id;
		   testorder.EffectiveDate= mydate;
		   testOrder.Status='Draft';
		   testOrder.isDRZUpdate__c=true;
		   testOrder.OpportunityId = TestOpp.Id;
		   testOrder.Bill_Rate__c = 123.0;
		   testOrder.ShipToContactId = con.Id;
		   insert testOrder;
		   BaseController.getOrderRecords(con.Id);
		   //--------------------------------------------------------
		   
        Test.stopTest();
        
    }
	@isTest static void test_callSearchDedupeService(){		
             				
		CreateTalentTestData.createAcceptedFiletypeLOVs();
		Account account = CreateTalentTestData.createTalentAccount(); 
		Account accountnew = CreateTalentTestData.createTalentAccount(); 
		Contact conta = createTalentContact(account.Id);
		conta.Other_Email__c ='teeeeest@test.com';
		conta.Work_Email__c ='teeeeest@test.com';
		conta.MobilePhone ='9090909056';
		conta.Phone ='9090909090';
		conta.HomePhone ='9090909090';
		update conta;
		Contact contanew = createTalentContact(accountnew.Id);
		contanew.Other_Email__c ='test@test.com';
		contanew.Work_Email__c ='test@test.com';
		contanew.MobilePhone ='9090909090';
		contanew.Phone ='9090909090';
		contanew.HomePhone ='9090909090';
		update contanew;
		Attachment attach=new Attachment();   	
    	attach.Name='Somefile.docx';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
		attach.parentId=account.id;
		insert attach;      
		Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.docx', 'Resume'); 
						
		   JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('firstname',conta.FirstName);
            gen.writeStringField('lastname', conta.LastName);
            gen.writeStringField('parsedHRXML', 'test');
            gen.writeStringField('email', conta.Email);
            gen.writeStringField('otheremail', conta.Other_Email__c);
            gen.writeStringField('workemail', conta.Work_Email__c);
            gen.writeStringField('mobilephone', conta.MobilePhone);
			gen.writeStringField('workphone', conta.Phone);
			gen.writeStringField('homephone', conta.HomePhone);			
            gen.writeEndObject();
            system.debug('***'+gen.getAsString());
			String contactDetails = gen.getAsString();

			ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c();
			serviceSettings.Client_Id__c = '121111';
			serviceSettings.Name = 'Data Quality Service';
			serviceSettings.Service_Http_Method__c = 'POST';
			serviceSettings.Service_URL__c = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';
			serviceSettings.Token_URL__c = '--';
			insert serviceSettings;
			//Test.setMock(HttpCalloutMock.class,new RWSInteractionsCalloutMock());
			Test.startTest();			
			BaseController.callSearchDedupeService( validAttachmentDocx.Id, 'Find_Add_Talent', contactDetails,'12345');
			Test.stopTest();			
	}
	@isTest static void test_callSearchDedupeServiceOther(){		
             				
		CreateTalentTestData.createAcceptedFiletypeLOVs();
		Account account = CreateTalentTestData.createTalentAccount(); 
		Account accountnew = CreateTalentTestData.createTalentAccount(); 
		Contact conta = createTalentContact(account.Id);
		conta.Other_Email__c ='teeeeest@test.com';
		conta.Work_Email__c ='teeeeest@test.com';
		conta.MobilePhone ='9090909056';
		conta.Phone ='9090909090';
		conta.HomePhone ='9090909090';
		update conta;
		Contact contanew = createTalentContact(accountnew.Id);
		contanew.Other_Email__c ='test@test.com';
		contanew.Work_Email__c ='test@test.com';
		contanew.MobilePhone ='9090909090';
		contanew.Phone ='9090909090';
		contanew.HomePhone ='9090909090';
		update contanew;
		Attachment attach=new Attachment();   	
    	attach.Name='Somefile.docx';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
		attach.parentId=account.id;
		insert attach;      
		Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.docx', 'Resume'); 
						
		   JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('firstname',conta.FirstName);
            gen.writeStringField('lastname', conta.LastName);
            gen.writeStringField('parsedHRXML', 'test');
            gen.writeStringField('email', conta.Email);
            gen.writeStringField('otheremail', conta.Other_Email__c);
            gen.writeStringField('workemail', conta.Work_Email__c);
            gen.writeStringField('mobilephone', conta.MobilePhone);
			gen.writeStringField('workphone', conta.Phone);
			gen.writeStringField('homephone', conta.HomePhone);			
            gen.writeEndObject();
            system.debug('***'+gen.getAsString());
			String contactDetails = gen.getAsString();

			ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c();
			serviceSettings.Client_Id__c = '121111';
			serviceSettings.Name = 'Data Quality Service';
			serviceSettings.Service_Http_Method__c = 'POST';
			serviceSettings.Service_URL__c = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';
			serviceSettings.Token_URL__c = '--';
			insert serviceSettings;
			
			Test.startTest();
			BaseController.callSearchDedupeService( validAttachmentDocx.Id, 'CodeCoverage', contactDetails,'123456');
			Test.stopTest();
	}
    public static Talent_Work_History__c createTalentWorkHistory(String accountID){
        Talent_Work_History__c e = new Talent_Work_History__c(Talent__c = accountID, uniqueId__c = 'test');
        Database.insert(e);
        return e;
    }

    public static void createTestMarketingEmails(Id contactId){
            List<et4ae5__configuration__c> cl = [SELECT Id FROM et4ae5__configuration__c WHERE et4ae5__Integration_Type__c='Enterprise2.0'];

            if(cl.size() == 0){
                et4ae5__configuration__c c = new et4ae5__configuration__c(et4ae5__Integration_Type__c='Enterprise2.0');
                Database.insert(c);
            } 

            List<et4ae5__Business_Unit__c> bul = [SELECT Id FROM et4ae5__Business_Unit__c WHERE Name = 'Allegis Group, Inc.'];
            et4ae5__Business_Unit__c bu = null;

            if(bul.size() == 0){
                bu = new et4ae5__Business_Unit__c(Name='Allegis Group, Inc.',
                                                  et4ae5__Business_Unit_ID__c ='7280184',
                                                  et4ae5__Enabled__c = true,
                                                  et4ae5__IsParentAccountMID__c = true
                                                 );
                Database.insert(bu);
            }else{
                bu = bul[0];
            }


            et4ae5__SendDefinition__c sd = new et4ae5__SendDefinition__c();
            sd.et4ae5__Business_Unit__r = bu;
            sd.et4ae5__Business_Unit_saved__c = ''; 
            sd.et4ae5__DateSent__c = Datetime.now();
            sd.et4ae5__EmailName__c = 'Test Email';
            sd.et4ae5__FromEmail__c = 'test@test.com';
            sd.et4ae5__FromName__c = 'Aerotek';
            sd.et4ae5__Subject__c = 'Test Subject';

            Database.insert(sd);

            et4ae5__IndividualEmailResult__c er = new et4ae5__IndividualEmailResult__c();
            er.et4ae5__DateSent__c = Datetime.Now();
            er.et4ae5__FromAddress__c = 'test@test.com';
            er.et4ae5__FromName__c = 'Aerotek';
            er.et4ae5__Opened__c = false;
            er.et4ae5__SubjectLine__c = 'Test Subject';
            er.Name = 'Test Email';
            er.et4ae5__SendDefinition__c = sd.Id;
            er.et4ae5__Contact__c = contactId;

            Database.insert(er);
        
    }


    public static Map<Id,String> CreateTalentForBatchRetryHrxml(){


       Map<Id,String> errorMap = new Map<Id, String>();

       Account newAcc1 = createTalentAccount('');
       Contact con = createTalentContact(newAcc1.Id);
       Account newAcc2 = createTalentAccount('');
       Contact con2 = createTalentContact(newAcc2.Id);

       Account newAcc3 = createTalentAccount('');
       Contact con3 = createTalentContact(newAcc3.Id);

       Account newAcc4 = createTalentAccount('');
       Contact con4 = createTalentContact(newAcc4.Id);


       errorMap.put(newAcc1.Id,  'Failed HRXML Testing only');
       errorMap.put(newAcc2.Id,  'Failed HRXML Testing only');
       errorMap.put(newAcc3.Id,  'Failed HRXML Testing only');
       errorMap.put(newAcc4.Id,  'Failed HRXML Testing only');


       return errorMap;


    }
    public static TestMethod void isTFOGroupMemberTest()
    {
        User usr=TestDataHelper.createUser('System Administrator');
        usr.Office_Code__c='01384';
        usr.OPCO__c='GB1';
        Insert usr;
            
        //Create Parent Group
        Group grp = new Group();
        grp.name = 'Talent_First_Opportunity_PG';
        grp.Type = 'Regular'; 
        Insert grp; 
        
        Test.startTest(); 
        //Create Group Member
        GroupMember grpMem = new GroupMember();
        grpMem.UserOrGroupId = Usr.Id;
        grpMem.GroupId = grp.Id;
        //Insert grpMem;   
        
        boolean retValue = BaseController.isTFOGroupMember();
        system.assertEquals(false, retValue);
        system.debug('######retValue '+retValue);
        Test.stopTest();
    }

}