public with sharing class BaseLookupController {
	public static Object performServerCall(String methodName, Map<String, Object> parameters){
		Object result = null;

        //System.assert(false, methodName );
		if(methodName == 'customLogicResponse'){			
			if ((String)parameters.get('type') == 'myAssignedReqs') {
				List<Object> addlFields = (List<Object>) JSON.deserializeUntyped( (String) parameters.get('addlReturnFields'));
				result = getMyAssignedReqs((String)parameters.get('searchString'),addlFields);
			}
		} else if(methodName == 'getCurrentValue'){
			result = getCurrentValue((String)parameters.get('type'),(String)parameters.get('value'));
		}else if(methodName == 'getStateList'){
			result = getStateList((String)parameters.get('parentKey'),(String)parameters.get('filterValue'));
    	}else if(methodName == 'getCountryList'){
			result = getCountryList((String)parameters.get('filterValue'));
    	}else if(methodName == 'searchSObject'){
            

			List<Object> addlFields = (List<Object>) JSON.deserializeUntyped( (String) parameters.get('addlReturnFields'));
			System.debug('wit Record Limit');
			if(String.isNotBlank((String)parameters.get('recordLimit'))) {
				
				result = searchSObject((String)parameters.get('type')
    									,(String)parameters.get('recTypeName')
    									,(String)parameters.get('nameFieldOverride')
										,(String)parameters.get('idFieldOverride')
    									,(String)parameters.get('searchString')
										,(String)parameters.get('sortCondition')
    									//,(List)parameters.get('addlReturnFields'));
										,addlFields
										,(String)parameters.get('recordLimit'));

			} else {
				result = searchSObject((String)parameters.get('type')
    								,(String)parameters.get('recTypeName')
    								,(String)parameters.get('nameFieldOverride')
                                    ,(String)parameters.get('idFieldOverride')
    								,(String)parameters.get('searchString')
                                    ,(String)parameters.get('sortCondition')
    								//,(List)parameters.get('addlReturnFields'));
                                    ,addlFields);
			}
    	}else if(methodName == 'querySObject'){

            List<Object> addlFields = (List<Object>) JSON.deserializeUntyped( (String) parameters.get('addlReturnFields'));
			if(String.isNotBlank((String)parameters.get('recordLimit'))) {
				result = querySObject((String)parameters.get('type')
										,(String)parameters.get('recTypeName')
										,(String)parameters.get('nameFieldOverride')
										,(String)parameters.get('idFieldOverride')
										,(String)parameters.get('searchString')
										,(Boolean)parameters.get('useCache')
										,(String)parameters.get('whereCondition')
										,(String)parameters.get('sortCondition')
										//,(List)parameters.get('addlReturnFields'));
										,addlFields
										,(String)parameters.get('recordLimit'));
			} else {

				result = querySObject((String)parameters.get('type')
                                    ,(String)parameters.get('recTypeName')
                                    ,(String)parameters.get('nameFieldOverride')
                                    ,(String)parameters.get('idFieldOverride')
                                    ,(String)parameters.get('searchString')
                                    ,(Boolean)parameters.get('useCache')
                                    ,(String)parameters.get('whereCondition')
                                    ,(String)parameters.get('sortCondition')
                                    //,(List)parameters.get('addlReturnFields'));
                                    ,addlFields);
			}
        }else if(methodName == 'getRecently'){
            
			List<Object> addlFields = (List<Object>) JSON.deserializeUntyped( (String) parameters.get('addlReturnFields'));

            result = getRecently((String)parameters.get('type')
    								,(String)parameters.get('recTypeName')
    								,(String)parameters.get('nameFieldOverride')
                                    ,(String)parameters.get('idFieldOverride')
                                    ,(String)parameters.get('sortCondition')
    								,addlFields);
        } else if (methodName == 'loadPicklist') {
			String fld = (String) parameters.get('picklistField');
			String[] fldArr = fld.split('\\.');

			List<String> picklistVals = TalentAddEditController.getPicklistValues(fldArr[0], fldArr[1]);
			List<SearchResult> lst = new List<SearchResult>();

			for (String val : picklistVals) {
				SearchResult sr = new SearchResult();
				sr.value = val;
				sr.id = val;
				sr.addlFields = new List<String>();
				lst.add(sr);
			}

			result = JSON.serialize(lst);
		}

		return result;
	}

    private static String getRecents(String uriString) {
        
       // String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm(); 
		//String restAPIURL = sfdcURL + '/services/data/v41.0/ui-api/lookups/Opportunity/AccountId'; 
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
        
       // req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId()); 
       // System.debug('UserInfo.getSessionID >>>>>>>> ' + UserInfo.getSessionID());       
		//req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID()); 
        
         req.setHeader('Authorization', 'OAuth ' +'00D9E00000012K1!AQUAQHjZCEW9ZOmuazogW6JifYTnAj2OWnof0AfSL7SYUanRqBUc0ULVx2.iIb.G_GUC0orbs8bPTAHHptP3LGLXFf0F9JuL');            
		req.setHeader('Authorization', 'Bearer ' + '00D9E00000012K1!AQUAQHjZCEW9ZOmuazogW6JifYTnAj2OWnof0AfSL7SYUanRqBUc0ULVx2.iIb.G_GUC0orbs8bPTAHHptP3LGLXFf0F9JuL'); 
        req.setEndpoint('https://allegisgroup--decdev.cs88.my.salesforce.com/services/data/v41.0/ui-api/lookups/Opportunity/AccountId');
        req.setMethod('GET');
        String responseBody;
        try {
            HttpResponse response = connection.send(req);
            System.debug('response >>>>>>>> ' + response);
            System.debug(response);
            responseBody = response.getBody();
            System.debug('responseBody >>>>>>>> ' + responseBody);
        } catch (Exception ex) {
            // Callout to apigee failed
            System.debug(LoggingLevel.ERROR, 'Call to ui-api failed! ' + ex.getMessage());
            throw ex;
        }
        
        if (String.isBlank(responseBody)) {
            System.debug(LoggingLevel.ERROR, 'Call to ui-api returned blank response!');
        }
        return responseBody;
    }
    
     private static List<RecentlyViewed> getRecently(String type, String recTypeName, String nameFieldOverride, 
                                String idFieldOverride,String sortCondition,  Object[] addlReturnFields) {
                  
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null){
            return null;
        }

        String addlFields = '';
        if (addlReturnFields != null) {
            for (Object fld : addlReturnFields) {
                addlFields += ', ' + (String) fld;
            }
        }

        String nameField = String.isBlank(nameFieldOverride)? getSobjectNameField(objType) : nameFieldOverride;                           
                                    
           List<RecentlyViewed> recents = new List<RecentlyViewed>();
            
           String query  = 'select '+ (String.isBlank(idFieldOverride) ? 'Id' : idFieldOverride)+','+ nameField + ' from RecentlyViewed where Type = '+ '\'' +type+ '\''+' AND recordType.Name = '+'\''+ recTypeName+'\''+ ' order by LastViewedDate DESC LIMIT 5';
           recents = Database.query(query);
 		   System.debug('******Recents*****');
           System.debug(recents);
			
           return recents;
         
    }

	private static String getCurrentValue(String type, String value){
        if(String.isBlank(type)){
            return null;
        }
        
        ID lookupId = null;
        try{   
        	lookupId = (ID)value;
        }catch(Exception e){
            return null;
        }
        
        if(String.isBlank(lookupId)){
            return null;
        }
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null){
            return null;
        }

        String nameField = getSobjectNameField(objType);
        String query = 'Select Id, '+nameField+' From '+type+' Where Id = \''+lookupId+'\'';
        System.debug('### Query: '+query);
        List<SObject> oList = Database.query(query);
        if(oList.size()==0) {
            return null;
        }
		return (String) oList[0].get(nameField);
    }

    /*
     * Searchs (using SOSL) for a given Sobject type
     */
    private static String searchSObject(String type, String recTypeName, String nameFieldOverride, 
                                String idFieldOverride, String searchString, String sortCondition, Object[] addlReturnFields){
          
        /*if(String.isBlank(type) || String.isBlank(searchString)){
            return null;
        }
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null){
            return null;
        }

        String addlFields = '';
        if (addlReturnFields != null) {
            for (Object fld : addlReturnFields) {
                addlFields += ', ' + (String) fld;
            }
        }

        String nameField = String.isBlank(nameFieldOverride)? getSobjectNameField(objType) : nameFieldOverride;
        searchString = '\"' + searchString + '*\"';
        String soslQuery = 'FIND :searchString IN ' + getSearchFields(type) + ' RETURNING '
                            + type 
                            + '(' + (String.isBlank(idFieldOverride) ? 'Id' : idFieldOverride) + ', ' 
                            + nameField + addlFields + getWhereClause(type, recTypeName) 
                            + (String.isBlank(sortCondition) ? '' : ' order by ' + sortCondition) + ') LIMIT 20';
		System.debug('SOSL QUERY: '+soslQuery);
        List<List<SObject>> results =  Search.query(soslQuery);
        System.debug('results***** ');
        System.debug(results);
        List<SearchResult> output = new List<SearchResult>();
        if(results.size()>0){
            for(SObject sobj : results[0]){
                output.add(translateResult(sobj, nameField, idFieldOverride, addlReturnFields));
            }
        }
        return JSON.serialize(output);*/
		return getSearchJSON(type,recTypeName,nameFieldOverride,idFieldOverride,searchString,sortCondition,addlReturnFields,'20');
    }

	//method overloaded with record limit parameter
	private static String searchSObject(String type, String recTypeName, String nameFieldOverride, 
                                String idFieldOverride, String searchString, String sortCondition, Object[] addlReturnFields, String recordLimit){
		return getSearchJSON(type,recTypeName,nameFieldOverride,idFieldOverride,searchString,sortCondition,addlReturnFields,recordLimit);
		
	}

	public static String getSearchJSON(String type, String recTypeName, String nameFieldOverride, 
                                String idFieldOverride, String searchString, String sortCondition, Object[] addlReturnFields, String recordLimit) {
		if(String.isBlank(type) || String.isBlank(searchString)){
            return null;
        }
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null){
            return null;
        }

        String addlFields = '';
        if (addlReturnFields != null) {
            for (Object fld : addlReturnFields) {
                addlFields += ', ' + (String) fld;
            }
        }

        String nameField = String.isBlank(nameFieldOverride)? getSobjectNameField(objType) : nameFieldOverride;
        searchString = '\"' + searchString + '*\"';
        String soslQuery = 'FIND :searchString IN ' + getSearchFields(type) + ' RETURNING '
                            + type 
                            + '(' + (String.isBlank(idFieldOverride) ? 'Id' : idFieldOverride) + ', ' 
                            + nameField + addlFields + getWhereClause(type, recTypeName) 
                            + (String.isBlank(sortCondition) ? '' : ' order by ' + sortCondition) + ') LIMIT '+recordLimit;
		System.debug('SOSL QUERY: '+soslQuery);
        List<List<SObject>> results =  Search.query(soslQuery);
        System.debug('results***** ');
        System.debug(results);
        List<SearchResult> output = new List<SearchResult>();
        if(results.size()>0){
            for(SObject sobj : results[0]){
                output.add(translateResult(sobj, nameField, idFieldOverride, addlReturnFields));
            }
        }
        return JSON.serialize(output);
    }
    
    /*
     * Query (using SOQL) for a given Sobject type
     */
    private static String querySObject(String type, String recTypeName, String nameFieldOverride, 
                                String idFieldOverride, String searchString,Boolean useCache, String whereCondition, String sortCondition, Object[] addlReturnFields){
       
        /*if((String.isBlank(type) || String.isBlank(searchString) )&& !useCache){
            return null;
        }
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null){
            return null;
        }

        String addlFields = '';
        if (addlReturnFields != null) {
            for (Object fld : addlReturnFields) {
                addlFields += ', ' + (String) fld;
            }
        }
      // string opco = 'MLA';
         String nameField = String.isBlank(nameFieldOverride)? getSobjectNameField(objType) : nameFieldOverride;
        //searchString = '\"' + searchString + '*\"';
        String soqlQuery = 'SELECT ' + (String.isBlank(idFieldOverride) ? 'Id' : idFieldOverride) + ', ' 
                            + nameField + addlFields 
                            + ' FROM ' + type
                            + getWhereClause(type, recTypeName) 
                            + (String.isBlank(searchString)? '' : (String.isBlank(recTypeName) ? ' WHERE ' : ' AND ') 
                               + nameField + ' LIKE \'' + String.escapeSingleQuotes(searchString) + '%\'')
                            + (String.isBlank(whereCondition) ? '' : ' AND ' + whereCondition)
                      //      + 'AND' + ' Text_Value_2__c' + ' LIKE \''+ opco + '%\''
                            + ' ORDER BY '+ (String.isBlank(sortCondition) ? nameField : sortCondition)
                            + (useCache ? ' LIMIT 500' : ' LIMIT 20');
                                    
                                    
        System.debug('SOQL QUERY: ' + soqlQuery);
        List<SObject> results =  Database.query(soqlQuery);
        List<SearchResult> output = new List<SearchResult>();
        if(results.size() > 0) {
            for(SObject sobj : results){
                output.add(translateResult(sobj, nameField, idFieldOverride, addlReturnFields));
            }
        }
        return JSON.serialize(output);*/
		return getQueryJSON(type, recTypeName, nameFieldOverride,idFieldOverride,searchString,useCache,whereCondition,sortCondition,addlReturnFields,'20');
    }

	//Method overloading for recordlimit parameter
	private static String querySObject(String type, String recTypeName, String nameFieldOverride, 
                                String idFieldOverride, String searchString,Boolean useCache, String whereCondition, String sortCondition, Object[] addlReturnFields, String recordLimit){
		return getQueryJSON(type, recTypeName, nameFieldOverride,idFieldOverride,searchString,useCache,whereCondition,sortCondition,addlReturnFields,recordLimit);
	}

	public static String getQueryJSON(String type, String recTypeName, String nameFieldOverride, 
                                String idFieldOverride, String searchString,Boolean useCache, String whereCondition, String sortCondition, Object[] addlReturnFields, String recordLimit) {
		
		if((String.isBlank(type) || String.isBlank(searchString) )&& !useCache){
            return null;
        }
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null){
            return null;
        }

        String addlFields = '';
        if (addlReturnFields != null) {
            for (Object fld : addlReturnFields) {
                addlFields += ', ' + (String) fld;
            }
        }
      // string opco = 'MLA';
         String nameField = String.isBlank(nameFieldOverride)? getSobjectNameField(objType) : nameFieldOverride;
        //searchString = '\"' + searchString + '*\"';
        String soqlQuery = 'SELECT ' + (String.isBlank(idFieldOverride) ? 'Id' : idFieldOverride) + ', ' 
                            + nameField + addlFields 
                            + ' FROM ' + type
                            + getWhereClause(type, recTypeName) 
                            + (String.isBlank(searchString)? '' : (String.isBlank(recTypeName) ? ' WHERE ' : ' AND ') 
                               + nameField + ' LIKE \'' + String.escapeSingleQuotes(searchString) + '%\'')
                            + (String.isBlank(whereCondition) ? '' : ' AND ' + whereCondition)
                      //      + 'AND' + ' Text_Value_2__c' + ' LIKE \''+ opco + '%\''
                            + ' ORDER BY '+ (String.isBlank(sortCondition) ? nameField : sortCondition)
                            + (useCache ? ' LIMIT 500' : ' LIMIT '+recordLimit);
                                    
                                    
        System.debug('SOQL QUERY: ' + soqlQuery);
        List<SObject> results =  Database.query(soqlQuery);
        List<SearchResult> output = new List<SearchResult>();
        if(results.size() > 0) {
            for(SObject sobj : results){
                output.add(translateResult(sobj, nameField, idFieldOverride, addlReturnFields));
            }
        }
        return JSON.serialize(output);
    }
    
 	 @AuraEnabled    
     public static Map<String, String> getCountryList(String filterValue) {
        filterValue = filterValue + '%'; 
        Map<String, String> countryMap = new Map<String,String>();
        for(Global_LOV__c entry : [SELECT Text_Value__c, Text_Value_3__c FROM Global_LOV__c WHERE LOV_Name__c = 'CountryList' and Text_Value_3__c != null and Text_Value_3__c like :filterValue order by Text_Value_3__c]){                                   
            countryMap.put(entry.Text_Value__c, entry.Text_Value_3__c);
        }
        return countryMap;
    }
    
     private static Map<String, String> getStateList(String parentKey, String filterValue) {
        parentKey = parentKey + '%'; 
        filterValue = filterValue + '%';  
        Map<String, String> stateMap = new Map<String,String>();
        for(Global_LOV__c entry : [SELECT Text_Value_2__c, Text_Value_3__c FROM Global_LOV__c WHERE LOV_Name__c = 'StateList' and Text_Value_3__c != null and Text_Value_2__c like :parentKey and (Text_Value_3__c like :filterValue or Text_Value_4__c like :filterValue) order by Text_Value_3__c]){                                                                   
            stateMap.put(entry.Text_Value_2__c, entry.Text_Value_3__c);
        }
        return stateMap;
    }

    /*
     * Utility class for search results
	*/
    public class SearchResult{
        public String value{get;Set;}
        public String id{get;set;}
        public String[] addlFields{get; set;}
    }

    /*
     * Translate a query result item into the a SearchResult object for return.
     */
    private static SearchResult translateResult(SObject sobj, String nameField, String idFieldOverride, Object[] addlReturnFields) {
        SearchResult sr = new SearchResult();
        sr.id = (String)sobj.get(String.isBlank(idFieldOverride) ? 'Id' : idFieldOverride);
        sr.value = (String)sobj.get(nameField);
        List<String> fldList = new List<String>();
        for (Object fld : addlReturnFields) {
            fldList.add(String.valueOf(sobj.get((String) fld)));
        }
        sr.addlFields = fldList;

        return sr;
    }
    
    /*
     * Returns the "Name" field for a given SObject (e.g. Case has CaseNumber, Account has Name)
	*/
    private static String getSobjectNameField(SobjectType sobjType){
        
        //describes lookup obj and gets its name field
        String nameField = 'Name';
        if(String.valueOf(sobjType) == 'Global_LOV__c'){
           nameField = 'Text_Value__c' ; 
        }else{
            Schema.DescribeSObjectResult dfrLkp = sobjType.getDescribe();
            for(schema.SObjectField sotype : dfrLkp.fields.getMap().values()){
                Schema.DescribeFieldResult fieldDescObj = sotype.getDescribe();
                if(fieldDescObj.isNameField() ){
                    nameField = fieldDescObj.getName();
                    break;
                }
            }
        }
        return nameField;
    }

    /*
     * Generate the where clause for the SOSL find.
     */
    private static String getWhereClause(String type, String recTypeName) {
        return String.isBlank(recTypeName) 
                    ? '' 
                    : (type == 'Global_LOV__c' ? ' WHERE LOV_Name__c=\'' : ' WHERE RecordType.Name=\'') 
                        + recTypeName + '\'';
    }

    /*
     * Get the search fields for the SOSL
     */
    private static String getSearchFields(String type) {
        return (type == 'Global_LOV__c') ? ' ALL FIELDS ' : ' NAME FIELDS ';
    }

	private static string getMyAssignedReqs(string searchString,Object[] addlReturnFields) {
		List<OpportunityTeamMember> openReqInOppTeam = [select opportunityId from OpportunityTeamMember where userId =:UserInfo.getUserId()];
		List<Id> OppIds = new List<Id>();
		For (OpportunityTeamMember OTM : openReqInOppTeam) {
			OppIds.add(OTM.OpportunityId);
		}
		String addlFields = '';
        if (addlReturnFields != null) {
            for (Object fld : addlReturnFields) {
                addlFields += ', ' + (String) fld;
            }
        }
		List<SearchResult> output = new List<SearchResult>();
		System.debug('searchString:' + searchString);
		string soqlQuery = 'SELECT Id,Name '+addlFields+' from Opportunity where isClosed = false and recordType.Name = \''+String.escapeSingleQuotes('Req')+'\' and ';
		soqlQuery+= '(OwnerId =\''+UserInfo.getUserId()+'\' OR Id in :OppIds ) and StageName != \''+String.escapeSingleQuotes('Staging')+'\' ';
		if (!String.isBlank(searchString)) {
			soqlQuery+= ' and ((Req_Job_Title__c LIKE \'%' + String.escapeSingleQuotes(searchString) + '%\')';
			soqlQuery+= ' OR (Opportunity_Num__c LIKE \'%' + String.escapeSingleQuotes(searchString) + '%\')';
			soqlQuery+= ' OR (Account_name_Fourmula__c LIKE \'%' + String.escapeSingleQuotes(searchString) + '%\')';
			soqlQuery+= ' OR (owner.Name LIKE \'%' + String.escapeSingleQuotes(searchString) + '%\'))';
        }
		soqlQuery+=' Order by createdDate desc'; 
		if (String.isBlank(searchString)) {
			soqlQuery+=' Limit 6';
		} else {
			soqlQuery+=' Limit 500';
		}                                                              
        System.debug('SOQL QUERY: ' + soqlQuery);
        List<Opportunity> myOpenOppty =  Database.query(soqlQuery);			
		if(myOpenOppty.size() > 0) {
            for(SObject sObj : myOpenOppty){
				 output.add(translateResult(sObj, 'Name', '', addlReturnFields));

            }        
		}
		return JSON.serialize(output);
	}
}