@isTest
global class BaseLookupControllerTest implements HttpCalloutMock{
   
    @testSetup
    static void setupTestData() {
        List<Account> accList = new List<Account>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
		accList.add(clientAcc);
        
		Insert accList;
		
        List<Contact> clientConList = new List<Contact>();
        
        for(Integer i=0; i<2 ; i++) {
            Contact ct = new Contact(
                FirstName = 'Firstname' + i,
                LastName = 'Lastname' + i,
                AccountId = accList[0].id,
                Email = 'testemail@test.com',
                MailingStreet = '123' + i + ' Main St',
                MailingCity = 'Hanover',
                MailingState = 'MD',
                MailingPostalCode = '55001',
                MailingCountry = 'USA',
                Other_Email__c = 'other'+i+'@testemail.com',
                Title = 'test title'+i,
                Talent_State_Text__c = 'test text'+i,
                recordTypeId =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId()
            );
            
            clientConList.add(ct);
		}
        insert clientConList;
        system.debug('Contact List---'+clientConList);
       
	} 
    
    global HTTPResponse respond(HTTPRequest req) {
        //System.assertEquals(req.getEndpoint(), actual)
        System.assertEquals('GET', req.getMethod());
		
        HttpResponse resp = new HTTpResponse();
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
		
        return res;
    }
    
    public static testMethod void testGetRecents() {
        Test.setMock(HttpCalloutMock.class, new BaseLookupControllerTest());
        
        //String resBody = BaseLookupController.getRecents('');
    }
    
    static testMethod void testGetCurrentValue() {
        Test.startTest();
        ID cid = [select id from Contact Limit 1].id;
        Map<String, Object> parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('value', cid);
        BaseLookupController.performServerCall('getCurrentValue', parmMap);
        
        parmMap = new Map<String, Object>();
        parmMap.put('type', '');
        parmMap.put('value', cid);
        BaseLookupController.performServerCall('getCurrentValue', parmMap);
        
        parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('value', 'erwerw343rfwer');
        BaseLookupController.performServerCall('getCurrentValue', parmMap);
        
        parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('value', '');
        BaseLookupController.performServerCall('getCurrentValue', parmMap);
       
        test.stopTest();
    }
    
    static testMethod void testGetStateList() {
        Test.startTest();
        
        Global_LOV__c gls = new Global_LOV__c(Lov_Name__c='StateList',Source_System_Id__c ='StateList.1230',Text_Value_2__c='TST.88', Text_Value_3__c ='Test State');
        insert gls;
        
        Map<String, Object> parmMapSt = new Map<String, Object>();
        parmMapSt.put('parentKey', 'TST');
        parmMapSt.put('filterValue', 'Test');        
        BaseLookupController.performServerCall('getStateList', parmMapSt);
        
        test.stopTest();
    }
    static testMethod void testGetCountryList() {
        Test.startTest();
        Global_LOV__c glc = new Global_LOV__c(Lov_Name__c='CountryList',Source_System_Id__c ='CountryList.1230',Text_Value_2__c='TST.88', Text_Value_3__c ='Test Country');
        insert glc;
        
        Map<String, Object> parmMapSt = new Map<String, Object>();
        parmMapSt.put('filterValue', 'Test');        
        BaseLookupController.performServerCall('getCountryList', parmMapSt);
        
        test.stopTest();
    }
    
     static testMethod void testSearchSObject() {
        Test.startTest();
        Map<String, Object> parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('recTypeName', 'Client');
        parmMap.put('nameFieldOverride', '');
        parmMap.put('idFieldOverride', '');
        parmMap.put('searchString','First');
        parmMap.put('sortCondition','');
        parmMap.put('addlReturnFields', '[]');        
        
        BaseLookupController.performServerCall('searchSObject', parmMap);
        
        test.stopTest();
    }

	static testMethod void testSearchSObjectRecordLimit() {
        Test.startTest();
        Map<String, Object> parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('recTypeName', 'Client');
        parmMap.put('nameFieldOverride', '');
        parmMap.put('idFieldOverride', '');
        parmMap.put('searchString','First');
        parmMap.put('sortCondition','');
        parmMap.put('addlReturnFields', '[]');  
		parmMap.put('recordLimit', '6');        
        
        BaseLookupController.performServerCall('searchSObject', parmMap);
        
        test.stopTest();
    }
    
    static testMethod void testQuerySObject() {
        Test.startTest();
        Map<String, Object> parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('recTypeName', 'Client');
        parmMap.put('nameFieldOverride', '');
        parmMap.put('idFieldOverride', '');
        parmMap.put('searchString','First');
        parmMap.put('useCache',False);
        parmMap.put('whereCondition','');
        parmMap.put('sortCondition','');
        parmMap.put('addlReturnFields','[]');        
        
        BaseLookupController.performServerCall('querySObject', parmMap);
        
        test.stopTest();
    }

	static testMethod void testQuerySObjectRecordLimit() {
        Test.startTest();
        Map<String, Object> parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('recTypeName', 'Client');
        parmMap.put('nameFieldOverride', '');
        parmMap.put('idFieldOverride', '');
        parmMap.put('searchString','First');
        parmMap.put('useCache',False);
        parmMap.put('whereCondition','');
        parmMap.put('sortCondition','');
        parmMap.put('addlReturnFields','[]');        
        parmMap.put('recordLimit', '6');   
        BaseLookupController.performServerCall('querySObject', parmMap);
        
        test.stopTest();
    }
    
    
    static testMethod void testGetRecently() {
        Test.startTest();
        Map<String, Object> parmMap = new Map<String, Object>();
        parmMap.put('type', 'Contact');
        parmMap.put('recTypeName', 'Client');
        parmMap.put('nameFieldOverride', '');
        parmMap.put('idFieldOverride', '');
        parmMap.put('addlReturnFields','[]');        
        
        BaseLookupController.performServerCall('getRecently', parmMap);
        
        test.stopTest();
    }

	static testMethod void testcustomLogicResponse() {
        Test.startTest();
        Map<String, Object> parmMap = new Map<String, Object>();
        parmMap.put('type', 'myAssignedReqs');        
        parmMap.put('searchString','First');
        parmMap.put('addlReturnFields','[]');        
        
        BaseLookupController.performServerCall('customLogicResponse', parmMap);
        
        test.stopTest();
    }

}