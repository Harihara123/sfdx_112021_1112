global with sharing class BaseTimelineModel implements Comparable {

    @AuraEnabled
    public String esfId {get;set;} // for story S-140332

    @AuraEnabled
    public Id AssignedToId {get;set;}

    @AuraEnabled
    public Integer totalItems {get;set;}

// Added for talent insights S-59445
    @AuraEnabled
    public Integer totalCountNDays {get;set;}

// Added by Santosh 
    @AuraEnabled
    public Integer presentRecordCount {get;set;}

// Added by Santosh
    @AuraEnabled
    public Integer pastRecordCount {get;set;}


    //Used Only For Displaying Next Steps Message On Activities Timeline
    @AuraEnabled
    public Boolean hasNextSteps {get;set;}

    @AuraEnabled
    public Boolean hasPastActivity {get;set;}

    @AuraEnabled
    public string sprite {get;set;}
    
    @AuraEnabled
    public string icon {get;set;}
    
    @AuraEnabled
    public string iconTimeLineColor {get;set;}
    
    @AuraEnabled
    public string lineColor {get;set;}

    @AuraEnabled
    public Id ParentRecordId {get;set;}
    
    @AuraEnabled
    public Id RecordId {get;set;}

    @AuraEnabled
    public String CreatedBy {get;set;}

    @AuraEnabled
    public Boolean showDeleteItem {get;set;}
    
    @AuraEnabled
    public DateTime LastModifiedDate {get;set;}
        
    @AuraEnabled
    public String TimelineType {get;set;}
    
    @AuraEnabled
    public String ActivityType {get;set;}
    
    @AuraEnabled
    public Boolean NextStep {get;set;}

    @AuraEnabled
    public String Subject {get;set;}
    
    @AuraEnabled
    public String Detail {get;set;}

    @AuraEnabled
    public String Status {get;set;}
    
    @AuraEnabled
    public String Priority {get;set;}

    @AuraEnabled
    public DateTime EventStartDateTime {get;set;}

    @AuraEnabled
    public DateTime EventEndDateTime {get;set;}
   
    /* Start for Story S-82799 */
    @AuraEnabled
    public String NotProceedingReason {get;set;}
    
   /* End for Story S-82799 */
	
    /* Start For STory S-69509 */
	@AuraEnabled
    public String SubmittalInterviewers{get;set;}
    
    @AuraEnabled
    public String InterviewStatus{get;set;}
    
    @AuraEnabled
    public Boolean SubmittalEvent{get;set;}
    
    @AuraEnabled
    public String SubmittalDescription {get;set;}
    /* End For STory S-69509 */
    // Text form of event duration
    @AuraEnabled
    public String EventTime {get;set;}
    
    // Task related to, Email "to", or list of attendees for events
    @AuraEnabled
    public String Recipients {get;set;}
    
    // Task/Event Asignee, or Email From
    @AuraEnabled
    public String Assigned {get;set;}
    
    // For indicating task completion
    @AuraEnabled
    public boolean Complete {get;set;}

    // Short event
    @AuraEnabled
    public String ShortDate {get;set;}

    @AuraEnabled
    public String PreMeetingNotes {get;set;}

    @AuraEnabled
    public String PostMeetingNotes {get;set;}

    @AuraEnabled
    public Boolean AllegisPlacement {get;set;}

    @AuraEnabled
    public Boolean CurrentAssignment {get;set;}

    @AuraEnabled
    public String OrgName {get;set;}
    
    @AuraEnabled
    public String OrgId {get;set;}

    @AuraEnabled
    public String Department {get;set;}

    @AuraEnabled
    public String JobTitle {get;set;}
    
    @AuraEnabled
    public Date EmploymentStartDate {get;set;}

    @AuraEnabled
    public Date EmploymentEndDate {get;set;}

    @AuraEnabled
    public Decimal Salary {get;set;}
    
    @AuraEnabled
    public String CurrencyType {get;set;}
    
    @AuraEnabled
    public Decimal Payrate {get;set;}

    @AuraEnabled
    public Decimal DailyRate {get;set;}
    
    @AuraEnabled
    public Decimal Bonus {get;set;}
    
    @AuraEnabled
    public Decimal BonusPct {get;set;}
    
    @AuraEnabled
    public Decimal OtherComp {get;set;}
    
    @AuraEnabled
    public Decimal CompensationTotal {get;set;}
    
    @AuraEnabled
    public String ReasonForLeaving {get;set;}
    
    @AuraEnabled
    public String Notes {get;set;}
    
    @AuraEnabled
    public String Year {get;set;}

    @AuraEnabled
    public String Certification {get;set;}
    
    @AuraEnabled
    public String Degree {get;set;}

    @AuraEnabled
    public String SchoolName {get;set;}
    
    @AuraEnabled
    public String ParsedSchool {get;set;}
    
    @AuraEnabled
    public String Major {get;set;}

    @AuraEnabled
    public String RateFrequency {get;set;}

	@AuraEnabled
    public String MainSkill {get;set;}

	@AuraEnabled
    public String Division {get;set;}    
    
    @AuraEnabled
    public String FinishReason {get;set;} 
    
    @AuraEnabled
    public String PeoplesoftId {get;set;} 
    
    @AuraEnabled
    public String BillRate {get;set;} 
    
    @AuraEnabled
    public String PaymentFrequency {get;set;} 
    
    @AuraEnabled
    public String WorkType {get;set;} 
    
    @AuraEnabled
    public String Honors {get;set;}

    public DateTime ActualDate {get;set;}

	@AuraEnabled
	public Date lastmeetingdate {get;set;}//added by akshay for S - 82631 5/21/18

	@AuraEnabled
	public String lastmeetingwith {get;set;}//added by akshay for S - 82631 5/21/18

    @AuraEnabled
    public String candidateStatus {get;set;}//added by Phani for S - 91079 7/17/18

    // Added for STORY S-133689

    @AuraEnabled
    public Integer totalOpenRecords {get;set;}
    @AuraEnabled
    public Integer totalPastRecords {get;set;}
    @AuraEnabled
    public Date taskStartDateTime {get;set;}

    global Integer compareTo(Object objToCompare)
    {  
	    if(this.TimelineType == 'Employment' ){
		   BaseTimelineModel compareToEmp = (BaseTimelineModel)objToCompare;
		   if((this.EmploymentEndDate == null) && (compareToEmp.EmploymentEndDate == null)){
		      return 0;
		   }else if((this.EmploymentEndDate != null) && (compareToEmp.EmploymentEndDate == null)){
		      return -1;
		   }else if((this.EmploymentEndDate == null) && (compareToEmp.EmploymentEndDate != null)){
		      return 1;
		   }else if((this.EmploymentEndDate != null) && (compareToEmp.EmploymentEndDate != null)){
		            if(this.EmploymentEndDate > compareToEmp.EmploymentEndDate){
						return -1;
					   }else if(this.EmploymentEndDate < compareToEmp.EmploymentEndDate){
						return 1;
						}else{
							if((this.EmploymentStartDate == null) && (compareToEmp.EmploymentStartDate == null)){
								return 0;
							}else if((this.EmploymentStartDate != null) && (compareToEmp.EmploymentStartDate == null)){
								return -1;
							}else if((this.EmploymentStartDate == null) && (compareToEmp.EmploymentStartDate != null)){
								return 1;
							}else if((this.EmploymentStartDate != null) && (compareToEmp.EmploymentStartDate != null)){
									if(this.EmploymentStartDate > compareToEmp.EmploymentStartDate){
									return -1;
								   }else if(this.EmploymentStartDate < compareToEmp.EmploymentStartDate){
									return 1;
									}else{
									return 0;
									}
							}else {
							  return 0;
							}
						}
		   }else{
		    return 0;
		   }
		
		}
		else{
		  DateTime otherActualDate = objToCompare != null ? ((BaseTimelineModel)objToCompare).ActualDate : System.now();

        return (this.ActualDate.getTime() - otherActualDate.getTime()).intValue();
        
		}
        
      
    }
    
}