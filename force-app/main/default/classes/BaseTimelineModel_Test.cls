@isTest
public class BaseTimelineModel_Test {
    @isTest
    //EmploymentEndDate is null for some records and not null for sum records
    static void unitTest1(){
        List<BaseTimelineModel> btList =  new List<BaseTimelineModel>();
        BaseTimelineModel btmodel_1 = new BaseTimelineModel();
        btmodel_1.TimelineType = 'Employment';
        
        BaseTimelineModel btmodel_2 = new BaseTimelineModel();
        btmodel_2.TimelineType = 'Employment';
        btmodel_2.EmploymentEndDate = System.today();
        
        btList.add(btmodel_1);
        btList.add(btmodel_2);
        
        btList.sort();
        //record with EmploymentEndDate will come last
        System.assertEquals(null, btList[1].EmploymentEndDate);
    }
    //  descending by EmploymentEndDate 
    @isTest
    static void unitTest2(){
        List<BaseTimelineModel> btList =  new List<BaseTimelineModel>();
        BaseTimelineModel btmodel_1 = new BaseTimelineModel();
        btmodel_1.TimelineType = 'Employment';
        
        BaseTimelineModel btmodel_2 = new BaseTimelineModel();
        btmodel_2.TimelineType = 'Employment';
        btmodel_2.EmploymentEndDate = System.today();
        
        BaseTimelineModel btmodel_3 = new BaseTimelineModel();
        btmodel_3.TimelineType = 'Employment';
        btmodel_3.EmploymentEndDate = System.today().addDays(1);
        
        btList.add(btmodel_1);
        btList.add(btmodel_2);
        btList.add(btmodel_3);
        
        btList.sort();
        //record sorted by EmploymentEndDate desc
        System.assertEquals( System.today().addDays(1), btList[0].EmploymentEndDate);
    }
    // no timeline type
    @isTest
    static void unitTest3(){
        List<BaseTimelineModel> btList =  new List<BaseTimelineModel>();
        BaseTimelineModel btmodel_1 = new BaseTimelineModel();
        btmodel_1.TimelineType = 'Employment';
        
        BaseTimelineModel btmodel_2 = new BaseTimelineModel();
        btmodel_2.TimelineType = 'Employment';
        btmodel_2.EmploymentEndDate = System.today();
        BaseTimelineModel btmodel_3 = new BaseTimelineModel();
        
        btList.add(btmodel_1);
        btList.add(btmodel_2);
        btList.add(btmodel_3);
        
        btList.sort();
        //record sorted by EmploymentEndDate desc
        //System.assertEquals( System.today().addDays(1), btList[0].EmploymentEndDate);
    }
    //  EmploymentEndDate are equal then go by EmploymentStartDate;
    @isTest
    static void unitTest4(){
        List<BaseTimelineModel> btList =  new List<BaseTimelineModel>();
        BaseTimelineModel btmodel_1 = new BaseTimelineModel();        	
        btmodel_1.TimelineType = 'Employment';
        btmodel_1.EmploymentEndDate = System.today().addDays(1);
        
        
        BaseTimelineModel btmodel_2 = new BaseTimelineModel();
        btmodel_2.TimelineType = 'Employment';
        btmodel_2.EmploymentEndDate = System.today().addDays(2);
        btmodel_2.EmploymentStartDate = System.today().addDays(1);
        
        BaseTimelineModel btmodel_3 = new BaseTimelineModel();
        btmodel_3.TimelineType = 'Employment';
        btmodel_3.EmploymentEndDate = System.today().addDays(2);
        btmodel_3.EmploymentStartDate = System.today();
        
        btList.add(btmodel_1);
        btList.add(btmodel_2);
        btList.add(btmodel_3);
        
        btList.sort();
        //record sorted by EmploymentEndDate desc
        //System.assertEquals( System.today().addDays(1), btList[0].EmploymentEndDate);
    }
    //coverage for class varibales
    @isTest 
    static void unitTest5(){
        List<BaseTimelineModel> btList =  new List<BaseTimelineModel>();
        BaseTimelineModel btmodel_1 = new BaseTimelineModel();
        btmodel_1.totalItems=10;
        btmodel_1.totalCountNDays = 10;
        btmodel_1.presentRecordCount =10;
        btmodel_1.pastRecordCount=10;
        btmodel_1.hasNextSteps= false;
        btmodel_1.hasPastActivity= false;
        btmodel_1.sprite='Test';
        btmodel_1.icon='Test';
        btmodel_1.iconTimeLineColor='Test';
        btmodel_1.lineColor='Test';
        btmodel_1.ParentRecordId='0016E00000R9DU0MMY';
        btmodel_1.RecordId='0016E00000R9DU0MMY';
        btmodel_1.CreatedBy='Me';
        btmodel_1.showDeleteItem=true;
        btmodel_1.LastModifiedDate=System.now();
        btmodel_1.TimelineType='test';
        btmodel_1.ActivityType='test';
        btmodel_1.NextStep=true;
        btmodel_1.Subject='test';
        btmodel_1.Detail='test detail';
        btmodel_1.Status='test';
        btmodel_1.Priority='high';
        btmodel_1.NotProceedingReason='just for fun';
        btmodel_1.SubmittalInterviewers='me';
        btmodel_1.InterviewStatus='unknown';
        btmodel_1.SubmittalEvent=true;
        btmodel_1.SubmittalDescription='test descri';
        btmodel_1.EventTime='test';
        btmodel_1.Recipients='test';
        btmodel_1.Assigned='test';
        btmodel_1.Complete=true;
        btmodel_1.ShortDate='test';
        btmodel_1.PostMeetingNotes='test';
        btmodel_1.AllegisPlacement=false;
        btmodel_1.CurrentAssignment=false;
        btmodel_1.OrgName='test';
        btmodel_1.OrgId='test';
        btmodel_1.Department='test';
        btmodel_1.JobTitle='test';
        btmodel_1.Salary=1234.56;
        btmodel_1.CurrencyType='USD';
        btmodel_1.Payrate=60.00;
        btmodel_1.DailyRate=10.00;
        btmodel_1.Bonus=11.11;
        btmodel_1.BonusPct=11.11;
        btmodel_1.OtherComp=11.11;
        btmodel_1.CompensationTotal=111.11;
        btmodel_1.ReasonForLeaving='fun';
        btmodel_1.Notes='nope';
        btmodel_1.Year='1234';
        btmodel_1.Certification='sfdc';
        btmodel_1.Degree='masters';
        btmodel_1.SchoolName='test';
        btmodel_1.ParsedSchool='test';
        btmodel_1.Major='test';
        btmodel_1.RateFrequency='test';
        btmodel_1.MainSkill='sfdc';
        btmodel_1.Division='test';
        btmodel_1.FinishReason='test';
        btmodel_1.PeoplesoftId='test123';
        btmodel_1.BillRate='test';
        btmodel_1.PaymentFrequency='daily';
        btmodel_1.WorkType='boring';
        btmodel_1.Honors='nop';
        btmodel_1.ActualDate=System.now();
        btmodel_1.lastmeetingdate=System.today();
        btmodel_1.lastmeetingwith='president';
        
    }
    
}