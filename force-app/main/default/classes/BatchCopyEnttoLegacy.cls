Global class BatchCopyEnttoLegacy implements Database.Batchable<sObject>, Database.stateful
{
//Variables
global DateTime dtLastBatchRunTime;
global Datetime latestBatchRunTime;
global String QueryReq; 
//Current DateTime stored in a variable
 Global DateTime newBatchRunTime;
Global Boolean isReqJobException = False;
global String strErrorMessage = '';
Global Set<Id> setoppIds;
Global Set<Id> setParentOppIds = new Set<Id>();
Global Set<Id> errorParentOppIds = new Set<Id>();
Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
Global List<String> exceptionList = new List<String>();

//Constructor
global BatchCopyEnttoLegacy()
{
       latestBatchRunTime = System.now();
       String strJobName = 'BatchEntReqLastrunTime';
       //Get the ProcessLegacyReqs Custom Setting
       ProcessLegacyReqs__c p = ProcessLegacyReqs__c.getValues(strJobName);
       SYSTEM.DEBUG('i AM HERE');
       //Store the Last time the Batch was Run in a variable
       dtLastBatchRunTime = p.LastExecutedBatchTime__c;
       newBatchRunTime = latestBatchRunTime;
       String strCustomSettingName = 'ProcessEntReqs';
       //Get the ProcessOpportunityRecords Custom Setting
       ApexCommonSettings__c s = ApexCommonSettings__c.getValues(strCustomSettingName);
       QueryReq = System.Label.ProcessEntReqs;
    
}

global database.QueryLocator start(Database.BatchableContext BC)  
 {  
    //Create DataSet of oPPS to Batch
     return Database.getQueryLocator(QueryReq);
 } 
 
global void execute(Database.BatchableContext BC, List<sObject> scope)
 {
        List<Log__c> errors = new List<Log__c>(); 
        setoppids = new Set<Id>();
        
        List<opportunity> lstQueriedopp = (List<opportunity>)scope;
         try{
              for(opportunity opp: lstQueriedopp){
                   if (opp.Id != null){
                     setoppids.add(opp.Id);
                   } 
                  }
                System.Debug('opportunities'+setoppIds);  
                if(setoppIds.size() > 0){
                   system.debug('setoppid'+setoppIds);
                   EnterprisetolegacyUtil reqUtil = new EnterprisetolegacyUtil(setoppids);
                   syncErrors = reqUtil.CopyEnterprisetoLegacy();
                 }
            }Catch(Exception e){
             //Process exception here and dump to Log__c object
             exceptionList.add(e.getMessage());
              if(exceptionList.size() > 0)
                    Core_Data.logInsertBatchRecords('Ent-Legacy Runtime', exceptionList);
             syncErrors.errorList.addall(exceptionList);
             isReqJobException = True;
             strErrorMessage = e.getMessage();
        }

  }
  
global void finish(Database.BatchableContext BC)
 {
        // Send an Email on Exception
         // Query the AsyncApexJob object to retrieve the current job's information.
        if(syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0  || Test.isRunningTest()){
           String strOppId = ' \r\n';
           String strOppMessage ='';
           String strEmailBody = '';
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Copy Enterprise to Legacy');
           
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with following failures.';
         
             for(Id oppId : syncErrors.errorMap.keyset()){
                if(oppId != null){
                   strOppId += 'Opp Id: '+ string.valueof(oppId) +' ' + 'Error Reason: ' + syncErrors.errorMap.get(oppId) + ' \r\n';
                }
             }
                 
           strEmailBody += strOppId;
           
           for(String err: syncErrors.errorList){
                if(!String.IsBlank(err)){
                   strOppMessage += ' \r\n' +  err + ' \r\n' ;
                }
             }
           
           strEmailBody += strOppMessage;
           
           errorText += strEmailBody;                           
           mail.setPlainTextBody(errorText);
           
           //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
       
        //Daisy Job Chaining -- Call Batch to Create Req Team Members
       
           BatchCopyEnttoLegacyTeamMembers batch = new BatchCopyEnttoLegacyTeamMembers();
           Database.executeBatch(batch, 50);
        
         //Update the Custom Setting's LastExecutedTime__c with the New Time Stamp for the last time the job ran
            ProcessLegacyReqs__c reqSetting = ProcessLegacyReqs__c.getValues('BatchEntReqLastrunTime');                  
            reqSetting.LastExecutedBatchTime__c = latestBatchRunTime;
              if(reqSetting != null) 
                 update reqSetting;
     
         //If Exception, Logs are created
         //Reset the Boolean variable and the Error Message string variable
             isReqJobException = False;
             strErrorMessage = '';     
 }

}