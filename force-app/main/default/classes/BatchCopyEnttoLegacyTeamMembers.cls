Global class BatchCopyEnttoLegacyTeamMembers implements Database.Batchable<sObject>, Database.stateful{

//Variables
global DateTime dtLastBatchRunTime;
global String QueryReq; 
//Current DateTime stored in a variable
Global DateTime newBatchRunTime;
Global Boolean isReqJobException = False;
global String strErrorMessage = '';
Global Set<Id> oppIds;
Global Set<Id> childTeamMemberIdSet  = new Set<Id>();
Global Set<Id> errorTeamMemberSet  = new Set<Id>();
Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
Global List<String> bugList = new List<String>();

//Constructor
global BatchCopyEnttoLegacyTeamMembers(){
       String strJobName = 'BatchEntReqLastrunTime';
       //Get the ProcessLegacyReqs Custom Setting
       ProcessLegacyReqs__c p = ProcessLegacyReqs__c.getValues(strJobName);
       //Store the Last time the Batch was Run in a variable
       dtLastBatchRunTime = p.LastExecutedBatchTime__c;
       
       String strCustomSettingName = 'ProcessEntReqs';
       //Get the ProcessOpportunityRecords Custom Setting
       ApexCommonSettings__c s = ApexCommonSettings__c.getValues(strCustomSettingName);
       QueryReq = 'Select Id,OpportunityId,Opportunity.Req_Legacy_Req_Id__c,userid, teammemberrole , Opportunity.Req_origination_System_Id__c from OpportunityTeammember where Id != null and SystemModstamp > :dtLastBatchRunTime and lastmodifiedby.name != \'Batch Integration\'';
}

global database.QueryLocator start(Database.BatchableContext BC){  
    //Create DataSet of Opps to Batch
     return Database.getQueryLocator(QueryReq);
}

global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Log__c> errors = new List<Log__c>(); 
        Set<Id> setoppIds = new Set<Id>();
        List<opportunityTeamMember> lstQueriedoppTeamMembers = (List<opportunityTeamMember>)scope;
         try{
               if(lstQueriedoppTeamMembers.size() > 0){
                   EnterprisetolegacyTeamMembersUtil reqTeamMembersUtil = new EnterprisetolegacyTeamMembersUtil(lstQueriedoppTeamMembers);
                   syncErrors = reqTeamMembersUtil.CopyEnterprisetoLegacyMembers(); 
                }   
            }Catch(Exception e){
             //Process exception here and dump to Log__c object
            bugList.add(e.getMessage());
            if(bugList.size() > 0)
                    Core_Data.logInsertBatchRecords('EntTeam-LegTeam Runtime', bugList);
             syncErrors.errorList.addall(bugList);
             isReqJobException = True;
             strErrorMessage = e.getMessage();
        }
}

global void finish(Database.BatchableContext BC){
          // Send an Email on Exception
         // Query the AsyncApexJob object to retrieve the current job's information.
        if(syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0  || Test.isRunningTest()){
           String strOppId = ' \r\n';
           String strOppMessage ='';
           String strEmailBody = '';
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Copy Ent-Legacy Team');
           
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with following failures.';
         
             for(Id oppId : syncErrors.errorMap.keyset()){
                if(oppId != null){
                   strOppId += 'Opp Id: '+ string.valueof(oppId) +' ' + 'Error Reason: ' + syncErrors.errorMap.get(oppId) + ' \r\n';
                }
             }
                 
           strEmailBody += strOppId;
           
           for(String err: syncErrors.errorList){
                if(!String.IsBlank(err)){
                   strOppMessage += ' \r\n' +  err + ' \r\n' ;
                }
             }
           
           strEmailBody += strOppMessage;
           
           errorText += strEmailBody;                           
           mail.setPlainTextBody(errorText);
           
           //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
        //Reset the Boolean variable and the Error Message string variable
        isReqJobException = False;
        strErrorMessage = '';
        //Start Copy Ent To Legacy Batch after 30 minutes
        // Start again in half an hour
         if(System.label.StopEntToLegacyBatchFromChildBatch != '' && system.label.StopEntToLegacyBatchFromChildBatch.tolowercase() == 'true')
          {
		    List<AsyncApexJob> flexQueuelst = [SELECT Id FROM AsyncApexJob WHERE Status = 'Holding' FOR UPDATE];
            if(flexQueuelst.size() >= Integer.valueOf(Label.Scheduled_Job_Limit)){
				// Send an email to the Apex job's submitter notifying of job completion.
			   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			   String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
			   mail.setToAddresses(toAddresses);
			   mail.setSubject('Important: Scheduled Req Sync job is about to reach Org limit.');
			   String errorText = 'Req sync job is about to get aborted please go to Apex jobs in Salesforce and check whether  L>E and E>L jobs are running, if they are not running please restart the jobs.';                   
			   mail.setPlainTextBody(errorText);
               //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
		   }
          if(!System.Test.isRunningTest()){
          
            system.scheduleBatch(new BatchCopyEnttoLegacy(),'Batch Enterprise-Legacy',30);
          }
          }
 }
 
}