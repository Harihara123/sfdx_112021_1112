global class BatchFocusNonFocusUpdate implements Database.Batchable<sObject> {
set<id> strownerid=new set<id>();
set<id> AccountStrIdset= new set<id>();

List<Account_Strategic_Initiative__c> StrLIst = new List<Account_Strategic_Initiative__c>();
    
    FocusNonFocusBatchDate__c mycs1 = FocusNonFocusBatchDate__c.getValues('BatchDate');
   Public Date myDate = mycs1.BatchranDate__c;
    
    Focus_NonFocus__c myCS2 = Focus_NonFocus__c.getValues('Event Query');
public String eventQuery = myCS2.Query__c;
    
    public integer lt=Limits.getLimitQueryRows();
   
         
         
       global Database.QueryLocator start(Database.BatchableContext BC) {
           
                       
       StrList = database.query('select id,Strategic_Initiative__r.Ownerid, Account__c from Account_Strategic_Initiative__c where Strategic_Initiative__r.Priority_Changed_Date__c >=:mydate limit :lt');
        
        for(Account_Strategic_Initiative__c SI :StrList){
            
            strownerid.add(SI.Strategic_Initiative__r.Ownerid);
            AccountStrIdset.add(SI.Account__c);
            
        }
        
        
        String query = eventQuery;
        system.debug(query);
        return Database.getQueryLocator(query);
        
    }
   
    global void execute(Database.BatchableContext BC, List<Event> scope) {
         DateTime TodayDateTime;
       List<Log__c> errlogs = new List<Log__c>();      

      List<Database.SaveResult> updateResults;
        updateResults = Database.update(scope, false);  
        
        for(Integer i=0;i<updateResults.size();i++){
            if (!updateResults.get(i).isSuccess()){
                
                 Database.Error error = updateResults.get(i).getErrors().get(0);
                String failedDML = error.getMessage();
                
            Log__c excep = new  Log__c();
                
         excep.Description__c = failedDML;
         excep.Subject__c = 'Account Plan Priority Exception';
         TodayDateTime = datetime.parse(system.now().format());         
         excep.Log_Date__c = TodayDateTime;
         excep.Integration_System__c = 'FocusNonFocusBatchJob';
         excep.Related_Object__c = 'Event';
         excep.Source_System__c = 'Salesforce';
                
                errlogs.add(excep);
            }
        }
        
insert errlogs;
        
        

}    
        
      
    global void finish(Database.BatchableContext BC) {
        
        
       FocusNonFocusBatchDate__c BatchDates=FocusNonFocusBatchDate__c.getValues('BatchDate');
        BatchDates.BatchranDate__c=system.today();
        update BatchDates;
        
    }
}