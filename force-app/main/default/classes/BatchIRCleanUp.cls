/***************************************************************************************************************************************
* Name        - BatchIRCleanUp 
* Description - This class holds the logic for cleaning up Integration Response records older than 30 days 
                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       1/25/2013             Created
*****************************************************************************************************************************************/

 global class BatchIRCleanUp implements Database.Batchable<Sobject>{
        
        global String query;
        global Date d;
        //global String email;
                
        //constructor
        global BatchIRCleanUp(){
        
        
        }
                    
        //Method to get the data to be proceesed  
        global database.Querylocator start(Database.BatchableContext context){
            
            d = system.today().addDays(-30);            
            return Database.getQueryLocator(query);          
        }
            
         
        //Method to execute the batch
        global void execute(Database.BatchableContext context , Sobject[] scope){
            
            List<Log__c> errors = new List<Log__c>();
            try{
                                  
                   
                   List<Integration_Response__c> IRsTobeDeleted = new List<Integration_Response__c>();  
            
                   for(Sobject s : scope){ 
                       Integration_Response__c ir = (Integration_Response__c)s;
                       IRsTobeDeleted.Add(ir);                                             
                   }
            
                   delete IRsTobeDeleted;
            
            }
            catch (Exception e) 
            {
                // Process exception here
                // Dump to Log__c object
                   errors.add(Core_Log.logException(e));
                   database.insert(errors,false);
            }  
            
        }
         
         //Method to be called after the excute
         global void finish(Database.BatchableContext context){
             // send an email
             //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             //mail.setToAddresses(new String[] {email});
             //mail.setReplyTo('batch@acme.com');
             //mail.setSenderDisplayName('Integration Response Clean Up Processing');
             //mail.setSubject('Integration Response Clean Up Batch Process Completed');
             //mail.setPlainTextBody('Integration Response Clean Up Batch Process is Completed');
             //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
         }
}