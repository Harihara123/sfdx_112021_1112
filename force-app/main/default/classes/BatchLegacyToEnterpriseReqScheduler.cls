/***************************************************************************************************************************************
* Name        - BatchLegacyToEnterpriseReqScheduler
* Description - Class used to invoke Batch_CopyLegacyReqToEnterpriseReq class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Preetham Uppu              06/07/2017               Created
*****************************************************************************************************************************************/

global class BatchLegacyToEnterpriseReqScheduler implements Schedulable
{
   global void execute(SchedulableContext sc){
       Batch_CopyLegacyReqToEnterpriseReq  batchJob = new Batch_CopyLegacyReqToEnterpriseReq();
       database.executebatch(batchJob,150);
  }
}