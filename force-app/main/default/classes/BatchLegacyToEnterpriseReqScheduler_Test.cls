@isTest
public class BatchLegacyToEnterpriseReqScheduler_Test {
    
    testmethod static void coverBatch() {
        
        ProcessLegacyReqs__c plr = new ProcessLegacyReqs__c();
        plr.Name = 'BatchLegacyReqLastRunTime';
        plr.LastExecutedBatchTime__c = System.Now().addDays(-10);
        insert plr;

            
        BatchLegacyToEnterpriseReqScheduler batchObj = new BatchLegacyToEnterpriseReqScheduler();
        String sch = '23 30 8 10 2 ?';
        system.schedule( 'Test Schedule', sch, batchObj );
        
    }

}