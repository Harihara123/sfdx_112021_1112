/***************************************************************************************************************************************
* Name        - BatchProcessOpportunityRecordsScheduler
* Description - Class used to invoke Batch_ProcessOpportunityRecords class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Preetham                  11/09/2016               Created
*****************************************************************************************************************************************/

global class BatchProcessOpportunityRecordsScheduler implements Schedulable
{
   global void execute(SchedulableContext sc){
       Batch_ProcessOpportunityRecords  batchJob = new Batch_ProcessOpportunityRecords();
       database.executebatch(batchJob,200);
  }
}