@isTest
public class BatchProcessOpportunityRecords_Test {
    
    testmethod static void coverBatch() {
        
        ProcessOpportunityRecords__c p = new ProcessOpportunityRecords__c();
        p.LastExecutedTime__c  = System.now();
        p.Name = 'SyncOpportunityLastRunTime';
        
        insert p;
        
        ApexCommonSettings__c a = new ApexCommonSettings__c();
        a.SoqlQuery__c  = 'Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtLastBatchRunTime';
        a.Name = 'ProcessOppRecordsQuery';
        insert a;
        
        BatchProcessOpportunityRecordsScheduler batchObj = new BatchProcessOpportunityRecordsScheduler();
        String sch = '23 30 8 10 2 ?';
        system.schedule( 'Test Schedule', sch, batchObj );
        
    }

}