public class BatchUpdateCompanyDataHelperclass 
{
    public static String errormsgs;
     
     
     
    public static List<Log__c> processaccnts(set<String> setAccountId, list<string> fieldnames,datetime Old180DaysDateTime,SET<string> setCompanyName,Boolean isUpdateCompanyJobException,Map<Id,Account> nonUpdatedAccountMap,Map<String,New_Company_Names__mdt> mapNewCompanyNames)  
    {  
        //query account with the current and former fields of companies
        List<Log__c> errorLogs = new List<Log__c>();
        
        try
        {
            string queryAccount = 'select id, ' + String.join(fieldnames, ', ') + ' from account where id in:setAccountId';
            List<account> lstAccount = Database.query(queryAccount);

            for(Account ac: lstAccount)
            {
                nonUpdatedAccountMap.put(ac.Id,ac);  
            }
            system.debug('Old180DaysDateTime: '+ Old180DaysDateTime);

            List<aggregateResult> resultsCurrent = [select order.accountid,order.status, COUNT(Order.Account.name),Order.Opportunity.OpCo__c from Order 
                                                    where Order.Accountid IN :setAccountId
                                                    AND Display_LastModifiedDate__c >=: Old180DaysDateTime
                                                    AND Order.Opportunity.OpCo__c IN : setCompanyName
                                                    // Group by Opco
                                                    group by order.accountid,Order.Opportunity.OpCo__c,order.status
                                                    having COUNT(Order.Accountid) > 0]; 

            List<aggregateResult> resultsFormer = [select order.accountid,order.status,COUNT(Order.Account.name),Order.Opportunity.OpCo__c from Order 
                                                    where Order.Accountid IN :setAccountId
                                                    AND Display_LastModifiedDate__c <=: Old180DaysDateTime
                                                    AND Order.Opportunity.OpCo__c IN : setCompanyName
                                                    // Group by Opco
                                                    group by order.accountid,Order.Opportunity.OpCo__c,order.status
                                                    having COUNT(Order.Accountid) > 0]; 

            system.debug('former: '+ resultsFormer);
            system.debug('current: ' +   resultsCurrent );


            Map<Id, Account> addedAccount = new Map<Id, Account>();
            List<New_Company_Names__mdt> lstNewCompanyNames = [SELECT MasterLabel,Current_Field_API_Name__c,Former_Field_API_Name__c from New_Company_Names__mdt];

            for (AggregateResult ar : resultsCurrent)
            { 
                ID accId = (ID)ar.get('accountid'); 
                Account acc = new Account();

                acc = nonUpdatedAccountMap.get(accId);
                System.debug('ACCOUNT FIRST: Current'+ acc);
                
                //Clear Previous Totals 
                if(!addedAccount.ContainsKey(acc.Id))
                {
                    for(New_Company_Names__mdt key : lstNewCompanyNames)
                    { 
                        acc.put(key.Current_Field_API_Name__c,null);
                        acc.put(key.Former_Field_API_Name__c,null);
                    }
                }

                string opco = string.valueOf(ar.get('OpCo__c')); 
                integer current = 0;
                string status = string.valueOf(ar.get('Status'));
                if(status == 'Started')
                {
                    current = (Integer)ar.get('expr0');
                    acc.put(mapNewCompanyNames.get(opco).Current_Field_API_Name__c,current);
                }

                if(acc != null && acc.Id != null && !addedAccount.ContainsKey(acc.Id))
                {
                    addedAccount.put(acc.Id,acc);
                }
            }
            system.debug('initial Current'  +  addedAccount.values());



            List<New_Company_Names__mdt> lstNewCompanyNames1 = [SELECT MasterLabel,Current_Field_API_Name__c,Former_Field_API_Name__c from New_Company_Names__mdt];

            for (AggregateResult ar : resultsFormer)
            { 
                ID accId = (ID)ar.get('accountid'); 
                Account acc1 = new Account();
                acc1 = nonUpdatedAccountMap.get(accId);

                System.debug('ACCOUNT FIRST: Former'+ acc1);
                
                //Clear Previous Totals 
                if(!addedAccount.ContainsKey(acc1.Id))
                {
                    for(New_Company_Names__mdt key : lstNewCompanyNames1)
                    { 
                        acc1.put(key.Former_Field_API_Name__c,null);
                        acc1.put(key.Current_Field_API_Name__c,null);
                    }
                }

                string opco = string.valueOf(ar.get('OpCo__c'));
                integer former = 0;
                string status = string.valueOf(ar.get('Status'));
                if(status == 'Started')
                {
                    former = (Integer)ar.get('expr0');
                    acc1.put(mapNewCompanyNames.get(opco).Former_Field_API_Name__c,former);
                }

                if(acc1 != null && acc1.Id != null && !addedAccount.ContainsKey(acc1.Id))
                {
                    addedAccount.put(acc1.Id,acc1);
                }

            } 
            system.debug('initial former'  +  addedAccount.values());


            system.debug('final'  +  addedAccount.values());
            Integer recordid = 0;
            for(database.SaveResult result : database.update(addedAccount.values(),false))
            {
                // Insert the log record accordingly
                if(!result.isSuccess())
                {
                    errorLogs.add(Core_Log.logException(result.getErrors()[0]));

                    errormsgs += 'Account Record:' + addedAccount.values()[recordid].id + ', ' + result.getErrors()[0].getMessage() + '<br/>';

                    isUpdateCompanyJobException = True;
                }
                recordid++;
            }

            if(errorLogs.size() > 0)
            {
                database.insert(errorLogs,false); 
            }
        }
        catch(Exception e)
        {
            // Process exception here and dump to Log__c object
            errorLogs.add(Core_Log.logException(e));
            database.insert(errorLogs,false);
            errormsgs =  e.getMessage();
        }       
        return errorlogs;
    }
  
}