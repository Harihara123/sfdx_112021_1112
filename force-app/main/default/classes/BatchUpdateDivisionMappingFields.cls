global class BatchUpdateDivisionMappingFields implements Database.Batchable<sObject>,Database.Stateful {

	global String query;

	global BatchUpdateDivisionMappingFields() {

		if(Test.isRunningTest()) {

			query = 'SELECT OPCO__c,Division__c,Office__c,Business_Unit__c,Business_Unit_Region__c,IsActive FROM User ' 
				+'WHERE IsActive = true AND OPCO__c=\'ONS\'' 
				+' AND Division__c IN(\'EASi Engineering\',\'Aston Carter\',\'Aerotek Aviation LLC\',\'Aerotek CE\',\'Aerotek Professional Services\',\'Aerotek Commercial Staffing\',\'EASi Sciences\',\'Aerotek Scientific, LLC\') LIMIT 200';
		} else {
			query = 'SELECT OPCO__c,Division__c,Office__c,Business_Unit__c,Business_Unit_Region__c,IsActive FROM User ' 
				+'WHERE IsActive = true AND OPCO__c=\'ONS\'' 
				+' AND Division__c IN(\'EASi Engineering\',\'Aston Carter\',\'Aerotek Aviation LLC\',\'Aerotek CE\',\'Aerotek Professional Services\',\'Aerotek Commercial Staffing\',\'EASi Sciences\',\'Aerotek Scientific, LLC\')';
		}
	}

	//start method
    global Database.queryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	 //execute method
    global void execute(Database.BatchableContext BC, List<User> scope) {
		try {
			UserTriggerHandler.updateDivisionMappingFields(scope,true);
		} catch(Exception e) {
			System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
			System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
			System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
			System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
			System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
		}
	}

	//finish method
    global void finish(Database.BatchableContext BC) {

		AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email   FROM AsyncApexJob where Id =:BC.getJobId()];
      
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Division Mapping user fields ' + a.Status);
        mail.setPlainTextBody('records processed ' + a.TotalJobItems +   'with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

	}
}