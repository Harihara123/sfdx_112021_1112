//***************************************************************************************************************************************/
//* Name        - Batch_CopyLegacyReqToEnterpriseReq 
//* Description - Batchable Class used to Copy Legacy to enterprise req
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham Uppu               06/07/2017               Created
//*****************************************************************************************************************************************/
global class Batch_CopyLegacyReqToEnterpriseReq implements Database.Batchable<sObject>, Database.stateful
{
//Variables
global DateTime dtLastBatchRunTime;
global String QueryReq; 
//Current DateTime stored in a variable
public DateTime newBatchRunTime;
public Boolean isReqJobException = False;
global String strErrorMessage = '';
Global Set<Id> setReqIds;
Global Set<Id> setParentReqIds = new Set<Id>();
Global Set<Id> errorParentReqIds = new Set<Id>();
Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
Global List<String> exceptionList = new List<String>();


//Constructor
global Batch_CopyLegacyReqToEnterpriseReq()
{ 
           Datetime latestBatchRunTime = System.now();
           String strJobName = 'BatchLegacyReqLastRunTime';
           //Get the ProcessLegacyReqs Custom Setting
           ProcessLegacyReqs__c p = ProcessLegacyReqs__c.getValues(strJobName);
             
           //Store the Last time the Batch was Run in a variable
           dtLastBatchRunTime = p.LastExecutedBatchTime__c;
           
           newBatchRunTime = latestBatchRunTime;
           String strCustomSettingName =  'ProcessLegacyReqs';
           //Get the ProcessOpportunityRecords Custom Setting
             // ApexCommonSettings__c s = ApexCommonSettings__c.getValues(strCustomSettingName);
           QueryReq = string.valueof(label.ProcessLegacyReqs); //s.SoqlQuery__c; 
      
}

global database.QueryLocator start(Database.BatchableContext BC)  
 {  
    //Create DataSet of Reqs to Batch
     return Database.getQueryLocator(QueryReq);
 } 
 
global void execute(Database.BatchableContext BC, List<sObject> scope)
 {
        List<Log__c> errors = new List<Log__c>(); 
        setReqIds = new Set<Id>();
        List<Reqs__c> lstQueriedReqs = (List<Reqs__c>)scope;
         try{
              for(Reqs__c r: lstQueriedReqs){
                   if (r.Id != null && r.lastmodifiedby.usertype == 'Standard'){
                     setReqIds.add(r.Id);
                   } 
                  }
                System.Debug('Legacy Reqs'+setReqIds);  
               if(setReqIds.size() > 0){
                  LegacyToEnterpriseReqUtil reqUtil = new LegacyToEnterpriseReqUtil(setReqIds);
                    syncErrors = reqUtil.copyLegacyReqToEnterpriseReq();
               }   
            }Catch(Exception e){
             // Process exception here and dump to Log__c object
             System.debug('ERROR'+e.getMessage());
             //errors.add(Core_Log.logException(e));
             //database.insert(errors,false);
             
             exceptionList.add(e.getMessage());
             if(exceptionList.size()>0)
                 Core_Data.logInsertBatchRecords('Legacy -Ent Runtime',exceptionList);
             syncErrors.errorList.addall(exceptionList);
             isReqJobException = True;
             strErrorMessage = e.getMessage();
        }

  }
  
global void finish(Database.BatchableContext BC)
 {
        System.debug('TEST'+syncErrors);
        // Send an Email on Exception
         // Query the AsyncApexJob object to retrieve the current job's information.
        if(syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0 || Test.isRunningTest()){
           String strOppId = ' \r\n';
           String strOppMessage ='';
           String strEmailBody = '';
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Copy Legacy to EntReq');
           
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with following failures.';
         
             for(Id reqId: syncErrors.errorMap.keyset()){
                if(reqId != null){
                   strOppId += 'Req Id: '+ string.valueof(reqId) +' ' + 'Error Reason: ' + syncErrors.errorMap.get(reqId) + ' \r\n';
                }
             }
                 
           strEmailBody += strOppId;
           
           for(String err: syncErrors.errorList){
                if(!String.IsBlank(err)){
                   strOppMessage += ' \r\n' +  err + ' \r\n' ;
                }
             }
           
           strEmailBody += strOppMessage;
           
           errorText += strEmailBody;                           
           mail.setPlainTextBody(errorText);
           
           //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
         
          DataMigHrXMLBatch batch1 = new DataMigHrXMLBatch(dtLastBatchRunTime);
           Database.executeBatch(batch1, 50);
         
         
          //Daisy Job Chaining -- Call Batch to Create Opportunity Team Members
       
           Batch_CopyReqTeamToOppTeam batch = new Batch_CopyReqTeamToOppTeam();
           Database.executeBatch(batch, 50);
         
          //Update the Custom Setting's LastExecutedTime__c with the New Time Stamp for the last time the job ran
                ProcessLegacyReqs__c reqSetting = ProcessLegacyReqs__c.getValues('BatchLegacyReqLastRunTime');                  
                reqSetting.LastExecutedBatchTime__c = newBatchRunTime ;
                system.debug('newBatchRunTime update settings:::' + newBatchRunTime );
                system.debug('System now update settings::: ' + system.now());
                if(reqSetting != null) 
                    update reqSetting;
                
            //If Exception, Logs are created
            //Reset the Boolean variable and the Error Message string variable
             isReqJobException = False;
             strErrorMessage = '';  
 }

}