//***************************************************************************************************************************************/
//* Name        - Batch_CopyReqTeamToOppTeam 
//* Description - Batchable Class used to Copy Req team members to opportunity team members
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham Uppu               06/07/2017               Created
//*****************************************************************************************************************************************/
global class Batch_CopyReqTeamToOppTeam implements Database.Batchable<sObject>, Database.stateful
{
//Variables
global DateTime dtLastBatchRunTime;
global String QueryReq; 
//Current DateTime stored in a variable
Global DateTime newBatchRunTime;
Global Boolean isReqJobException = False;
global String strErrorMessage = '';
Global Set<Id> reqIds;
Global Set<Id> childTeamMemberIdSet = new set<Id>(); 
Global Set<Id> errorTeamMemberSet  = new Set<Id>();
Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
Global List<String> bugList = new List<String>();



//Constructor
global Batch_CopyReqTeamToOppTeam()
{ 
     String strJobName = 'BatchLegacyReqLastRunTime';
       //Get the ProcessLegacyReqs Custom Setting
       ProcessLegacyReqs__c p = ProcessLegacyReqs__c.getValues(strJobName);
       //Store the Last time the Batch was Run in a variable
       dtLastBatchRunTime = p.LastExecutedBatchTime__c;
       
       String strCustomSettingName = 'ProcessReqEnts';
       //Get the ProcessOpportunityRecords Custom Setting
       ApexCommonSettings__c s = ApexCommonSettings__c.getValues(strCustomSettingName);
       
       QueryReq = 'select Id, Contact__c, Requisition__c, Requisition__r.Origination_System_Id__c,Req_Promoted_Date__c,Requisition_Team_Role__c ,User__c, Send_Req_Update_Notification__c, Siebel_Id__c, Team_Member_Name__c, Title__c,Type__c, Name from Req_Team_Member__c where Id != null and SystemModstamp > :dtLastBatchRunTime and lastmodifiedby.name != \'Batch Integration\'';
}

global database.QueryLocator start(Database.BatchableContext BC)  
{  
//Create DataSet of Reqs to Batch
 return Database.getQueryLocator(QueryReq);
} 
 
global void execute(Database.BatchableContext BC, List<sObject> scope)
{
    List<Log__c> errors = new List<Log__c>(); 
    Set<Id> setoppIds = new Set<Id>();
    List<Req_Team_Member__c> lstQueriedReqTeamMembers = (List<Req_Team_Member__c>)scope;
   
      try
      {
           if(lstQueriedReqTeamMembers.size() > 0){
               LegacyToEnterpriseTeamMembersUtil reqTeamMembersUtil = new LegacyToEnterpriseTeamMembersUtil(lstQueriedReqTeamMembers);
               syncErrors = reqTeamMembersUtil.CopyLegacyToEnterpriseMembers();  
           } 
        }
        Catch(Exception e)
        {
         //Process exception here and dump to Log__c object
         //errors.add(Core_Log.logException(e));
         //database.insert(errors,false);
         bugList.add(e.getMessage());
         if(bugList.size() > 0)
             Core_Data.logInsertBatchRecords('LegacyTeam-EntTeam Runtime', bugList);
         syncErrors.errorList.addall(bugList);
         isReqJobException = True;
         strErrorMessage = e.getMessage();
        }

}
  
global void finish(Database.BatchableContext BC)
{
         // Send an Email on Exception
         // Query the AsyncApexJob object to retrieve the current job's information.
        if(syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0){
           String strOppId = ' \r\n';
           String strOppMessage ='';
           String strEmailBody = '';
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Copy Req-Opp Team');
           
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with following failures.';
         
             for(Id oppId : syncErrors.errorMap.keyset()){
                if(oppId != null){
                   strOppId += 'Opp Id: '+ string.valueof(oppId) +' ' + 'Error Reason: ' + syncErrors.errorMap.get(oppId) + ' \r\n';
                }
             }
                 
           strEmailBody += strOppId;
           
           for(String err: syncErrors.errorList){
                if(!String.IsBlank(err)){
                   strOppMessage += ' \r\n' +  err + ' \r\n' ;
                }
             }
           
           strEmailBody += strOppMessage;
           
           errorText += strEmailBody;                           
           mail.setPlainTextBody(errorText);
           
          // Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
         
    //Reset the Boolean variable and the Error Message string variable
    isReqJobException = False;
    strErrorMessage = '';
    //Start Copy Ent To Legacy Batch after 30 minutes
    // Start again in half an hour
     if(System.label.StopLegacyToEntBatchFromChildBatch != '' && system.label.StopLegacyToEntBatchFromChildBatch.tolowercase() == 'true')
       {
	     List<AsyncApexJob> flexQueueList = [SELECT Id FROM AsyncApexJob WHERE Status = 'Holding' FOR UPDATE];
         if(flexQueueList.size() >= Integer.valueOf(Label.Scheduled_Job_Limit)){
		   System.debug('++ Email Sent ++');
		    // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Important: Scheduled Req Sync job is about to reach Org limit.');
           String errorText = 'Req sync job is about to get aborted please go to Apex jobs in Salesforce and check whether  L>E and E>L jobs are running, if they are not running please restart the jobs.';                   
           mail.setPlainTextBody(errorText);
           //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
		 }
         if(!System.Test.isRunningTest()){ 
            system.scheduleBatch(new Batch_CopyLegacyReqToEnterpriseReq(),'Batch Legacy-Enterprise',30);
         }
      }
    
         
}

}