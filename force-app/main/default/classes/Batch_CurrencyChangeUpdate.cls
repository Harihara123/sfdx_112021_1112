global class Batch_CurrencyChangeUpdate implements Database.Batchable<sObject>, Database.stateful 
{
    global String query{get;set;}
    global Boolean isUpdateOpenOpportunitiesJobException = False;
    global List<log__c> lstLogs = new List<log__c>();
    global String errormsgs{get;set;}

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        set<String> setStageName = new set<string>();
        setStageName.add('Draft');
        setStageName.add('Qualified');
        setStageName.add('Presenting');
        setStageName.add('Interviewing');        
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<Opportunity> scope)
    {
        Id reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Id proactiveRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Proactive').getRecordTypeId();
        Map<Id,String> mapOpenOpportunitiesWithCurrency = new Map<Id,String>();
        Map<Id,Decimal> mapOpportunityWithConversionRate = new Map<Id,Decimal>();
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        
        try
        {
            for(Opportunity op : scope)
            {
                if((op.RecordTypeId == reqRecordTypeId || op.RecordTypeId == proactiveRecordTypeId))
                {
                     mapOpenOpportunitiesWithCurrency.put(op.Id,op.Currency__c);
                     lstOpportunity.add(op);
                }
            }
            
            if(mapOpenOpportunitiesWithCurrency != null && mapOpenOpportunitiesWithCurrency.size() > 0)
            {
                mapOpportunityWithConversionRate = ConvertCurrencyISOCode.getMapCurrencyTypeCustom(mapOpenOpportunitiesWithCurrency);
            }
            
             for(Opportunity op : lstOpportunity)
            {
                decimal conversionRate = 1.0;
                
                if(mapOpportunityWithConversionRate != null && mapOpportunityWithConversionRate.size() > 0)
                {
                    if(mapOpportunityWithConversionRate.get(op.Id) != null)
                    {
                        conversionRate = mapOpportunityWithConversionRate.get(op.Id);
                    }                   
                }
                
                decimal opAmount = op.Amount == null? 0 :op.Amount;
                decimal opTotalSpread = op.Req_Total_Spread__c == null? 0 :op.Req_Total_Spread__c;
                decimal opRetainerFee = op.Retainer_Fee__c == null? 0 :op.Retainer_Fee__c;
                decimal opReqSalaryMin = op.Req_Salary_Min__c == null? 0 :op.Req_Salary_Min__c;
                decimal opReqSalaryMax = op.Req_Salary_Max__c == null? 0 :op.Req_Salary_Max__c;
                decimal opReqFlatFee = op.Req_Flat_Fee__c == null? 0 :op.Req_Flat_Fee__c;
                decimal opReqAddYearComp = op.Req_Additional_1st_Year_Compensation__c == null? 0 :op.Req_Additional_1st_Year_Compensation__c;
                decimal opReqBillRateMin = op.Req_Bill_Rate_Min__c == null? 0 :op.Req_Bill_Rate_Min__c;
                decimal opReqBillRateMax = op.Req_Bill_Rate_Max__c == null? 0 :op.Req_Bill_Rate_Max__c;
                decimal opReqPayRateMin = op.Req_Pay_Rate_Min__c == null? 0 :op.Req_Pay_Rate_Min__c;
                decimal opReqPayRateMax = op.Req_Pay_Rate_Max__c == null? 0 :op.Req_Pay_Rate_Max__c;
                decimal opTotalCompMax = op.Total_Compensation_Max__c == null? 0 :op.Total_Compensation_Max__c;
                decimal opTotalCompMin = op.Total_Compensation_Min__c == null? 0 :op.Total_Compensation_Min__c;
                
                op.Amount_Multi_Currency__c = conversionRate * opAmount;
                op.Total_Spread_Multi_Currency__c = conversionRate * opTotalSpread ;
                op.Project_Retainer_Fee_Multi_Currency__c =  conversionRate * opRetainerFee;
                op.Salary_Min_Multi_Currency__c =  conversionRate * opReqSalaryMin;
                op.Salary_Max_Multi_Currency__c =  conversionRate * opReqSalaryMax;
                op.Flat_Fee_Multi_Currency__c = conversionRate * opReqFlatFee;
                op.Additional_1st_Year_Comp_Converted__c = conversionRate * opReqAddYearComp;
                op.Bill_Rate_Min_Multi_Currency__c = conversionRate * opReqBillRateMin;
                op.Bill_Rate_Max_Multi_Currency__c = conversionRate * opReqBillRateMax;
                op.Pay_Rate_Min_Multi_Currency__c = conversionRate * opReqPayRateMin;
                op.Pay_Rate_Max_Multi_Currency__c = conversionRate * opReqPayRateMax;
                op.Total_Compensation_Max_Converted__c = conversionRate * opTotalCompMax;
                op.Total_Compensation_Min_Converted__c = conversionRate * opTotalCompMin;           
                
            }
            Integer recordid = 0;
            Database.saveResult[] drList = Database.update(lstOpportunity, false);
            
            for(Database.saveResult dr : drList) 
            {
                if(!dr.isSuccess())
                {
                    // Operation failed, so get all errors 
                   // lstLogs.add(Core_Log.logException(dr.getErrors()[0]));
                    errormsgs += 'Opportunity Record:' + lstOpportunity[recordid] + ', ' + dr.getErrors()[0].getMessage() + '<br/>';
                    isUpdateOpenOpportunitiesJobException = true;
                }

                recordid++;
            } 
        }
        catch(Exception e)
        {
             errormsgs =  e.getMessage();
            isUpdateOpenOpportunitiesJobException = true;
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        /*
        system.debug('!!!!!!!!!!!!!!!!!!!!' +lstLogs);
        if(lstLogs.size() > 0){
            insert lstLogs;
        }
        */
        //send email after batch is executed        
        if(isUpdateOpenOpportunitiesJobException == true)
        {
            string strEmailBody =  'The scheduled Update Open Opportunities on Currency Change Job failed to process. There was an exception during execution.\n';
            strEmailBody += errormsgs;
            sendEmails('EXCEPTION - Update Open Opportunities on Currency Change Job',strEmailBody);            
        }
    }
    
    public void sendEmails(String subjectText,String messageText)
    {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};
            message.setToAddresses(toAddresses); 
        message.setSubject(subjectText);
        message.setPlainTextBody(messageText);
        
        //Send the Email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message }); 
    }
}