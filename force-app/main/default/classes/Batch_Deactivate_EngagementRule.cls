/***************************************************************************************************************************************
* Name        - Batch_Deactivate_EngagementRule 
* Description - This class that deactivates Engagement rule if record type is do_Not_Recruit and Expiration date< Today and active = True                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Nidish                    11/09/2013             Created
* Inshan                    11/08/2017             Updated to call HRXML batch in finish method
*****************************************************************************************************************************************/

global class Batch_Deactivate_EngagementRule implements Database.Batchable<sObject>, Database.stateful{
       //Declare Variables
    global String QueryObject;
    global Set<ID> ClientAccountIdSet = new Set<ID>();
        
    global Batch_Deactivate_EngagementRule(){
       //Get the query Custom Setting ApexCommonSettings__c
       String strCustomSettingName =  'Batch_Deactivate_EngagementRule';       
       ApexCommonSettings__c s = ApexCommonSettings__c.getValues(strCustomSettingName);
       QueryObject = s.SoqlQuery__c;
    }
        
        global Database.querylocator start(Database.BatchableContext BC){ 
         return Database.getQueryLocator(QueryObject);  
         // return Database.getQueryLocator([SELECT Account__c,Expiration_Date__c, Active__c, Id, RecordType.DeveloperName ,Start_Date__c FROM Engagement_Rule__c where RecordType.DeveloperName = 'Do_Not_Recruit' and ((Expiration_Date__c < TODAY and Active__c = TRUE)OR (Start_Date__c = TODAY))]);
        } 
        
        
   global void execute(Database.BatchableContext BC, List<Engagement_Rule__c> scope){ 
            System.debug('Scope: '+scope);              
            List<Engagement_Rule__c> EngagementRules = new List<Engagement_Rule__c>();
            for(Engagement_Rule__c a : scope){ 
                   if(a.Start_Date__c == system.TODAY()){
                    a.Active__c = TRUE;
                   }
                   if(a.Expiration_Date__c < system.TODAY()){
                    a.Active__c = FALSE;
                   }               
                   EngagementRules.add(a);
                   ClientAccountIdSet.add(a.Account__c);

                }               

               Database.SaveResult[] updatedResults = Database.Update(EngagementRules,false);
                           if (updatedResults != null){
                                for(Integer i=0;i<updatedResults.size();i++){
                                           if (!updatedResults.get(i).isSuccess()){
                                            // DML operation failed
                                            Database.Error error = updatedResults.get(i).getErrors().get(0);
                                            System.debug('Errors' + error); 
                                          //  strErrorMessage += error.getMessage();
                                          //  failedOppIds.add(dmlUpdateMap.values().get(i).Id);
                                            
                                       }
                                 }
                              }
    }
    
    global void finish(Database.BatchableContext BC){

       String query = 'SELECT Id FROM Account WHERE Current_Employer__c IN';
       
       if(ClientAccountIdSet != null && ClientAccountIdSet.size() > 0){
            HRXMLBatch hBatch = new HRXMLBatch(ClientAccountIdSet, query);
            Database.executeBatch(hBatch, 50);
       }

    }
    
 }