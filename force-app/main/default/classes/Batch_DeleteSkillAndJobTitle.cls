//***************************************************************************************************************************************/
//* Name        - Batch_DeleteSkillAndJobTitle
//* Description - Batchable Class used to delete data of Skills and job title from Global LOV object
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni               01/11/2018               Created


/*anonymous code for all global lov
Batch_DeleteSkillAndJobTitle objBatch_DeleteSkillAndJobTitle = new Batch_DeleteSkillAndJobTitle();
objBatch_DeleteSkillAndJobTitle.QueryReq = 'select id from Global_LOV__c where LOV_Name__c in :lovNames';
Database.executeBatch(objBatch_DeleteSkillAndJobTitle);
*/

/*anonymous code for single lov
Batch_DeleteSkillAndJobTitle objBatch_DeleteSkillAndJobTitle = new Batch_DeleteSkillAndJobTitle();
objBatch_DeleteSkillAndJobTitle.QueryReq = 'select id from Global_LOV__c where LOV_Name__c in :lovNames and id =\'0069E000006TCTP\'';
Database.executeBatch(objBatch_DeleteSkillAndJobTitle);
*/
/*****************************************************************************************************************************************/
global class Batch_DeleteSkillAndJobTitle implements Database.Batchable<sObject>, Database.stateful
{
    //Variables
    global String QueryReq; 
    public Boolean isGlobalLOVJobException = False;
    global String strErrorMessage = '';
    public list<string> lovNames = new list<string>{'MatchSkillList','JobTitleList'};

    //Constructor
    global Batch_DeleteSkillAndJobTitle()
    { 
        QueryReq = '';
    }

    global database.QueryLocator start(Database.BatchableContext BC)  
    {  
        //Create DataSet of global lov to Batch
        return Database.getQueryLocator(QueryReq);
    } 
     
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Log__c> errors = new List<Log__c>(); 
        List<Global_LOV__c> lstQueriedGlobalLOV = (List<Global_LOV__c>)scope;
        try
        {
            delete lstQueriedGlobalLOV;
        }
        Catch(Exception e){
        // Process exception here and dump to Log__c object
        System.debug('ERROR'+e.getMessage());
        errors.add(Core_Log.logException(e));
        database.insert(errors,false);
        isGlobalLOVJobException = True;
        strErrorMessage = e.getMessage();
        }

    }
    
      
    global void finish(Database.BatchableContext BC)
    {
        List<Log__c> errorsLog = new List<Log__c>(); 
        if(isGlobalLOVJobException)
        {

            System.debug('EXCEPTION');
            //If Exception, Logs are created
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
            String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
            mail.setToAddresses(toAddresses); 
            String strEmailBody = '';
                       
            mail.setSubject('EXCEPTION - Delete Skills and Job Title Global LOV batch job failed'); 

            //Prepare the body of the email
            strEmailBody = 'The scheduled Apex Global LOV Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                

            mail.setPlainTextBody(strEmailBody);   

            //Send the Email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            //Reset the Boolean variable and the Error Message string variable
            isGlobalLOVJobException = False;
            strErrorMessage = '';
        }
        else
        {
            Global_LOV__c obj = new Global_LOV__c();
            obj.LOV_Name__c = 'NotifySOAOnDeletionOfSkillsAndJobTitle';
            obj.Source_System_Id__c = 'NotifySOAOnDeletionOfSkillsAndJobTitle';
            insert obj;
        }
    }

}