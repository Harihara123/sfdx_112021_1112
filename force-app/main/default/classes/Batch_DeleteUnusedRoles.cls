global class Batch_DeleteUnusedRoles implements Database.Batchable<sobject>, Database.stateful
{ 
    UserRole role;
    global String query{get;set;}
    global set<Id> setUserRoleToDelete {get;set;}
    // global List<Log__c> lstLogs {get;set;}
    global String errormsgs{get;set;}
    global Boolean isDeleteUnusedRolesJobException = False;
    global boolean isUserRoleAlreadyProcessed = false;
    global List<log__c> lstLogs = new List<log__c>();
    
    //constructor
    global Batch_DeleteUnusedRoles()
    {
        setUserRoleToDelete = new Set<Id>();
        role=[Select Id from UserRole where developername='Inactive_User' Limit 1];
    }
    
    //start method
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(role==null)
        {
            sendEmails('No role : Inactive_User','Role Missing');
            return null;
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<sobject> scope)
    {
        List <User> userslst = new list<User>();
        try
        {
            for(Sobject s : scope)
            {
                User a = (User)s;
                if(a.UserRoleId <>role.id){
                    a.UserRoleId=role.Id;
                    userslst.add(a);
                }
            }  
            //update all inactive users with Inactive_User role.
            Database.update(userslst);
            
            Map<Id,Id> mapParentRoleWithId = new Map<Id,Id>();
            setUserRoleToDelete = new Set<Id>();
            
            //get all UserRoles that are not used in any User and has a parent.
            Map<Id,UserRole> mapAllUnusedUserRoles=  new Map<Id,UserRole>([select Id, Name, ParentRoleId from UserRole where ParentRoleId != null]);
            
            System.debug('mapAllUnusedUserRoles.Values().size()::::: ' + mapAllUnusedUserRoles.Values().size());
            //Looping through list to put records in a map of parent role id and role id
            for(UserRole ur: mapAllUnusedUserRoles.Values())
            {
                mapParentRoleWithId.put(ur.ParentRoleId,ur.Id);                 
            }
            
            for(UserRole userRole: mapAllUnusedUserRoles.Values())
            {
                //checking condition if a roleid is not parent of any other role ie the role is the last hierarichal child which doesnot have any other child role. 
                //So this role can be deleted.
                if(mapParentRoleWithId.get(userRole.Id) == null)
                {
                    setUserRoleToDelete.add(userRole.Id);
                }                   
            }
            
            List<UserRole> lstUserRoleToDelete = [select id from UserRole where Id not IN (Select UserroleId from User) and id in: setUserRoleToDelete];
            
            Integer recordid = 0;
            
            if(lstUserRoleToDelete != null && lstUserRoleToDelete.size() > 0)
            {
                Database.DeleteResult[] drList = Database.delete(lstUserRoleToDelete, false);
                isUserRoleAlreadyProcessed = true;
                // Iterate through each returned result
                for(Database.DeleteResult dr : drList) 
                {
                    if(dr.isSuccess()) 
                    {
                        // Operation was successful, so get the ID of the record that was processed
                        lstLogs.add(Core_Log.logException('Batch_DeleteUnusedRoles',mapAllUnusedUserRoles.get(dr.getId()).Name,'Batch_DeleteUnusedRoles',string.valueOf(dr.getId()),'UserRole','Cleanup Roles'));
                        
                        System.debug('11111111111111111111111' +lstLogs);
                    }
                    if(!dr.isSuccess())
                    {
                        // Operation failed, so get all errors 
                        lstLogs.add(Core_Log.logException(dr.getErrors()[0]));
                        errormsgs += 'Role Record:' + lstUserRoleToDelete[recordid] + ', ' + dr.getErrors()[0].getMessage() + '<br/>';
                        isDeleteUnusedRolesJobException = true;
                    }
                    
                    recordid++;
                } 
            }
            else
            {
                if(isUserRoleAlreadyProcessed != true)
                {
                    errormsgs =  'No unused roles to delete';
                    isDeleteUnusedRolesJobException = true;
                }
            }
        }
        catch(Exception e)
        {
            // Process exception here and dump to Log__c object
            //  lstLogs.add(Core_Log.logException(e));
            //  database.insert(lstLogs,false);
            errormsgs =  e.getMessage();
            isDeleteUnusedRolesJobException = true;
        }
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
        system.debug('!!!!!!!!!!!!!!!!!!!!' +lstLogs);
        if(lstLogs.size() > 0){
            insert lstLogs;
        }
        //send email after batch is executed        
        if(isDeleteUnusedRolesJobException == true)
        {
            string strEmailBody =  'The scheduled Delete Unused Roles Job failed to process. There was an exception during execution.\n';
            strEmailBody += errormsgs;
            sendEmails('EXCEPTION - Deleting Unused Roles Batch Job',strEmailBody);            
        }
    }
    
    public void sendEmails(String subjectText,String messageText)
    {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};
            message.setToAddresses(toAddresses); 
        message.setSubject(subjectText);
        message.setPlainTextBody(messageText);
        
        //Send the Email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message }); 
    }
    
}