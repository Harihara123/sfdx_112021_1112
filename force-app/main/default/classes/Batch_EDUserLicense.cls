global class Batch_EDUserLicense implements Database.Batchable<sObject>,Database.stateful {
    
    //Variables
    global Id grpId;
    global Id psetId;
    Global List<String> errorList = new List<String>();
    Global String Description;
    Global string query;
        
    //constructor
    global Batch_EDUserLicense(string q,Id grId,Id psId)
    {
     query=q;
     grpId=grId;
     psetId=psId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext bc,List<GroupMember> scope){
        
        Map<Id,PermissionSetAssignment> permMap=new Map<Id,PermissionSetAssignment>();
        List<PermissionSetAssignment> permNewList = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> permRemList = new List<PermissionSetAssignment>();
        
        Map<Id,User> uMap =new Map<Id,User>([Select id,name,isActive from user where id in 
                                            (select userOrGroupId from GroupMember where groupid=:grpId) and 
                                             userType='Standard' and isActive=True]);
        
           for(PermissionSetAssignment psa : [SELECT AssigneeId,Assignee.isActive,PermissionSetId 
                                              FROM PermissionSetAssignment WHERE PermissionSetId = :psetId]){
                                                  
                permMap.put(psa.AssigneeId,psa);       
                                                  
              }

        
        //Adding the Permission Set for a user in the Public Group
            for(GroupMember grp : scope) {
                
                if(uMap.containsKey(grp.userOrGroupId)) {
                    
                   User u = uMap.get(grp.userOrGroupId);
                   Boolean psetVal = permMap.containsKey(grp.userOrGroupId);
                       if( psetVal==false && u.isActive==true) {
                          PermissionSetAssignment pa = new PermissionSetAssignment();
                          pa.AssigneeId=grp.userOrGroupId;
                          pa.PermissionSetId=psetId;
                          permNewList.add(pa);               
                       }
                
                }
      }
        //Inserting the Einstein_Discovery_User Permission Set
        Database.SaveResult[] srList = Database.insert(permNewList, false);
        // Iterate through each returned result
             for (Database.SaveResult sr : srList) {
                 if (!sr.isSuccess()){
                  
                // Operation failed, so get all errors                
                   for(Database.Error err : sr.getErrors()) {  
                      errorList.add(err.getMessage());
                      Description = Description+err.getMessage();
                    }
               }
       
             
     }
        
        
        //Removing the Permission set for User who is not in the Public Group
        if(permMap.size()>0) {
           for(Id uId : permMap.keySet()) {
               Boolean valUser=uMap.containsKey(uId);
               if(valUser==false){       
                 PermissionSetAssignment pa1 = permMap.get(uId);
                 permRemList.add(pa1);                
            }
          }  
       }
        
        // Delete the Einstein_Discovery_User Permission set from User
           Database.DeleteResult[] drList = Database.delete(permRemList, false);

       // Iterate through each returned result
       for(Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                
                 // Operation failed, so get all errors                
                 for(Database.Error err : dr.getErrors()) {
                       errorList.add(err.getMessage());
                       Description = Description+err.getMessage();
                    }
                
           }
        }
                       
    }
    
    global void finish(Database.BatchableContext bc){
    
    // Get the ID of the AsyncApexJob representing this batch job
   // from Database.BatchableContext.
   // Query the AsyncApexJob object to retrieve the current job's information.
    if(errorList.size()>0) {  
        Core_Data.logInsertBatchRecords('Einstein Discovery User License',errorList);
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id =:bc.getJobId()];
        
        /*// Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'vvamsi@allegisgroup.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Batch for Einstein Discovery License ' + a.Status);
        mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
         ' batches with '+ a.NumberOfErrors + ' failures.' + Description);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
     }
  }

}