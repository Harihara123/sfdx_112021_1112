/************************************************************
Name- Batch_EmailNotiOppOwnerwithintwodays

Description-Issue email notification daily sent to opportunity owner when
Close Date is within two days one day or on same day and stage equals Interest, Qualifying, Pursue, Bid or Negotiation

************************************************************/

global class Batch_EmailNotiOppOwnerwithintwodays implements Database.Batchable<sObject>{

     
     
    // Query for all the opportunity records where close date is in past and not closed yet.
    global Database.querylocator start(Database.BatchableContext BC){
         
        return Database.getQueryLocator([select Id,Name,CloseDate,StageName,RecordType.Name,Owner.email,ownerID,owner.name from opportunity where (CloseDate = NEXT_N_DAYS: 2  or CloseDate = NEXT_N_DAYS: 1 or CloseDate = TODAY) and (RecordType.Name='TEKsystems Global Services') and StageName IN ('Interest','Qualifying','Solutioning','Proposing','Negotiating')]);
    }
    
    // send email notification to the opportunity owners
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope1){
    
        List<Messaging.SingleEmailMessage> listmail = new List<Messaging.SingleEmailMessage>();
           
           for(Opportunity p : Scope1){  
       
          Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
          
         // string [] toaddress;  as setToAddress will hit the governor limit of 1000 emails per day, setTargetObjectId is used.
         // string toadd=p.owner.email;
         // toaddress= new String [] {toadd};
         // mail.setToAddresses(toAddress);
          mail.setSenderDisplayName(p.owner.name);
          mail.setTargetObjectId(p.ownerID);
          mail.setReplyTo('donotreply@allegisgroup.com');            
          mail.setSaveAsActivity(false);
          mail.setSubject('Opportunity Alert: Close Date is Approaching');
          mail.setHtmlBody('Hi ' + p.owner.name+ ',' + '<br/>' + '<br/>'+ 'Opportunity ' + p.name +' is set to close on ' +p.CloseDate.Month()+'-'+p.CloseDate.Day() +'-' + p.CloseDate.Year()+ '. If this opportuniity will NOT be closing by this date, take action to update the Close Date and any other corresponding opportunity record details. You will continue receiving emails until an update is completed.' + '<br/>' +'<br/>'+ 'To view this Opportunity in Salesforce: '+Label.Instance_URL+p.ID + '<br/>' +  '<br/>' + 'Thanks.');

               listmail.add(mail);    
    }
  
       Messaging.SendEmail(listmail);
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
    }