/************************************************************
Name- Batch_EmailNotificationtoOppOwner

Description-This class is used to issue email notification daily sent to opportunity
owner when Close Date is within two days and stage equals Interest, Qualifying, Pursue, Bid or Negotiation

************************************************************/

global class Batch_EmailNotificationtoOppOwner implements Database.Batchable<sObject>{

     string query;
     
      // Query for all the opportunity records where close date is in past and not closed yet.
    global Database.querylocator start(Database.BatchableContext BC){
    
        return Database.getQueryLocator([select Id,Name,CloseDate,StageName,RecordType.Name,Owner.email,ownerID, owner.name from opportunity where  (CloseDate<Today and RecordType.Name='TEKsystems Global Services') and StageName IN ('Interest','Qualifying','Solutioning','Proposing','Negotiating') ]);
    }
    
    // send email notification to the opportunity owners
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope1){
    
        List<Messaging.SingleEmailMessage> listmail = new List<Messaging.SingleEmailMessage>();
             
           for(Opportunity p : Scope1){  
       
          Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
          
         // string [] toaddress;// as setToAddress will hit the governor limit of 1000 emails per day, setTargetObjectId is used.
         // string toadd=p.owner.email;
         // toaddress= new String [] {toadd};
         // mail.setToAddresses(toAddress);
          mail.setSenderDisplayName(p.owner.name);
          mail.setTargetObjectId(p.ownerID);
          mail.setReplyTo('donotreply@allegisgroup.com');
          mail.setSaveAsActivity(false);
          mail.setSubject('Immediate Action Required: Update Opportunity Close Date ');
          mail.setHtmlBody('Hi ' + p.owner.name+ ',' + '<br/>' + '<br/>'+ 'Opportunity ' + p.name +' did not close on ' +p.CloseDate.Month()+'-'+p.CloseDate.Day() +'-' + p.CloseDate.Year()+ '. Your action is required to update the Close Date or close the opportunity. You will continue to receive this notification until the opportunity has been updated.' + '<br/>' +'<br/>'+ 'To view this Opportunity in Salesforce: '+Label.Instance_URL+p.ID + '<br/>' +  '<br/>' + 'Thanks.');
               listmail.add(mail);    
    }
  
       Messaging.SendEmail(listmail);
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
    }