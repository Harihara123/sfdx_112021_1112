/***************************************************************************************************************************************
* Name        - Batch_HRXML_CanLists 
* Description - This batch job will trigger an updated HR-XML file for Search with Candidate Lists attached 
				flag set to false 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Karthik                    12/08/2017              Created
*****************************************************************************************************************************************/
global class Batch_HRXML_CanLists implements Database.Batchable<sObject>, Database.stateful{
    global String QueryObject;
    global DateTime batchLastRun;
    global DateTime batchNextRun;
    global Boolean jobExceptionFlag = false;

    public class CanListBatchException extends Exception {}
    
    global Batch_HRXML_CanLists(){
        batchNextRun = DateTime.now();
        HRXML_Candidate_Lists_Update__c cs = HRXML_Candidate_Lists_Update__c.getvalues('HRXMLBatchLastRun');
        batchLastRun = cs.Last_Executed_Date_Time__c;
    }
    
    global Database.querylocator start(Database.BatchableContext bc){ 
        return Database.getQueryLocator(QueryObject);  
    } 
        
   	global void execute(Database.BatchableContext bc, List<Contact_Tag__c> scope){
        System.debug('Scope: '+scope);
        if(scope.size()>0)
        {
            try{
                Set<ID> contIds = new Set<Id>();

                for(Contact_Tag__c t : scope){
                    contIds.add(t.Contact__c);
                }

                List<Contact> contactList = [SELECT Id, Search_Ingestion_Trigger_Timestamp__c from Contact where Id in :contIds];
                for (Contact c: contactList) {
                    c.Search_Ingestion_Trigger_Timestamp__c = Datetime.now();
                }

                ConnectedLog.LogSack sack = new ConnectedLog.LogSack();
                Database.SaveResult[] srList = Database.update(contactList, false);
                for (Integer i=0; i<srList.size(); i++) {
                    if (!srList[i].isSuccess()) {
                        Database.Error err = srList[i].getErrors()[0];
                        sack.AddException('Batch_HRXML_CanLists', 'execute', contactList[i].Id, new CanListBatchException(err.getMessage()));
                    }
                }

                sack.Write();
            }
            catch(Exception e){
                jobExceptionFlag = True;
                ConnectedLog.LogException('Batch_HRXML_CanLists', 'execute', e);
            }
        }
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CompletedDate
                                          FROM AsyncApexJob WHERE Id = :bc.getJobId()];
        Map<String, String> mss = new Map<String, String>{
            'JobId' => a.Id,
            'Status' => a.Status,
            'NumberOfErrors' => String.valueOf(a.NumberOfErrors),
            'JobItemsProcessed' => String.valueOf(a.JobItemsProcessed),
            'TotalJobItems' => String.valueOf(a.TotalJobItems),
            'CompletedDate' => String.valueOf(a.CompletedDate)
        };
        ConnectedLog.LogInformation('Batch_HRXML_CanLists', 'execute', 'Candidate Lists batch complete', mss);
        
        //Update custom setting with Batch completed time
        if(!jobExceptionFlag){
            HRXML_Candidate_Lists_Update__c custSett = HRXML_Candidate_Lists_Update__c.getvalues('HRXMLBatchLastRun');
            custSett.Last_Executed_Date_Time__c = batchNextRun;
            update custSett;
        }
        
    }
}