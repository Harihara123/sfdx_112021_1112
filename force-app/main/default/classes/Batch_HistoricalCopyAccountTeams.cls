// Execute Anonymous code
// Make sure to run as Batch Integration user
// Batch_HistoricalCopyAccountTeams bat = new Batch_HistoricalCopyAccountTeams();
// bat.query = 'SELECT Id, Talent_Account_Manager__c, Talent_Recruiter__c, Talent_Biller__c FROM Account WHERE RecordType.Name=\'Talent\''; 
// Id batchJobId = Database.executeBatch(bat);
// 
global class Batch_HistoricalCopyAccountTeams implements Database.Batchable<sObject>, Database.Stateful{
    
   global String query;
   global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
   global Map<Id,String> insertErrorsMap = new Map<Id,String>();
    
   global database.QueryLocator start(Database.BatchableContext BC){  
       return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC, List<Account> accList){
       
       List<AccountTeamMember> accTeamMembers = new List<AccountTeamMember>();
       
       for(Account a: accList){
           if(a.Talent_Account_Manager__c != null && a.Talent_Account_Manager__r.usertype =='Standard' && a.Talent_Account_Manager__r.isActive == true){
                AccountTeamMember AM = new AccountTeamMember();
                AM.AccountId = a.Id;
                AM.TeamMemberRole = 'Account Manager';
                AM.UserId = a.Talent_Account_Manager__c;
                accTeamMembers.add(AM);
           }
           
           if(a.Talent_Recruiter__c != null && a.Talent_Recruiter__r.usertype =='Standard' && a.Talent_Recruiter__r.isActive == true){
                AccountTeamMember Rec = new AccountTeamMember();
                Rec.AccountId = a.Id;
                Rec.TeamMemberRole = 'Recruiter';
                Rec.UserId = a.Talent_Recruiter__c;
                accTeamMembers.add(Rec);
           }
           
           if(a.Talent_Biller__c != null && a.Talent_Biller__r.usertype =='Standard' && a.Talent_Biller__r.isActive == true){
                AccountTeamMember Bill = new AccountTeamMember();
                Bill.AccountId = a.Id;
                Bill.TeamMemberRole = 'Biller';
                Bill.UserId = a.Talent_Biller__c;
                accTeamMembers.add(Bill); 
           }
       }
           List<Database.SaveResult> acSaveResults = Database.insert(accTeamMembers,false);
       
       for(Integer i=0; i<accTeamMembers.size(); i++){
           String errorRecord = 'Failed - Account Team Log: ';
           String exceptionMsg = '';
           if(!acSaveResults[i].isSuccess()){
           	   if(acSaveResults[i].getErrors()[0].getStatusCode()!=null && acSaveResults[i].getErrors()[0].getFields()!=null){
               		exceptionMsg += 'Error Reason: '+ String.ValueOf(acSaveResults[i].getErrors()[0].getStatusCode())+ ' '+ ' Error Fields ' +  acSaveResults[i].getErrors()[0].getFields(); 
           	   }
           	   errorRecord +=exceptionMsg;
           	   insertErrorsMap.put(accTeamMembers[i].AccountId,errorRecord);
           }
       }
           
           if(insertErrorsMap.size() > 0)
                  Core_Data.logBatchRecord('Account Teams Logs', insertErrorsMap);
                                                                                                                                                                 
   }
    
   global void finish(Database.BatchableContext BC){
   
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
           FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'skaviti@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Historical Copy Account Teams');
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with failures. Please Check Log records';        
           mail.setPlainTextBody(errorText);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });           
   }
}