@isTest
public class Batch_HistoricalCopyAccountTeamsTest {

    static testmethod void copyHistoricalDataTeams(){
        
        Profile singleDeskProfile = [SELECT Id, Name FROM Profile WHERE Name LIKE '%Single Desk%' LIMIT 1];
        
        User users = [SELECT Id, Name,usertype, isActive FROM User WHERE ProfileId=:singleDeskProfile.Id AND isActive=true AND usertype='Standard' LIMIT 1];
        
           Account talent = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'testAccount batchtest' //,
           // Desired_Rate_Frequency__c = 'hourly',
           // Desired_Rate__c =200
        );
        insert talent;
        
    //    List<User> teamUsers = TestDataHelper.createUsers(3, singleDeskProfile.Name);
        
        talent.Talent_Account_Manager__c = users.Id;
        talent.Talent_Biller__c = users.Id;
        talent.Talent_Recruiter__c = users.Id;
        update talent;
        
        Batch_HistoricalCopyAccountTeams bat = new Batch_HistoricalCopyAccountTeams();
		bat.query = 'SELECT Id, Talent_Account_Manager__c, Talent_Account_Manager__r.usertype, Talent_Recruiter__c, Talent_Recruiter__r.usertype,  Talent_Biller__c, Talent_Biller__r.usertype, Talent_Account_Manager__r.isActive, Talent_Recruiter__r.isActive, Talent_Biller__r.isActive FROM Account WHERE Id=\''+talent.Id+'\'';
		Id batchJobId = Database.executeBatch(bat);
        
        Batch_HistoricalDeleteAccountTeams atm = new Batch_HistoricalDeleteAccountTeams();
		DataBase.executeBatch(atm);
    }
}