@isTest
public class Batch_HistoricalCopyEntReqToLegacyTest {

 
 static testMethod Void testBatch_HistoricalCopyEntReqToLegacy() {
    TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
         List<opportunity> lstOpportunity = new List<opportunity>();
        // variable declaration
        List<Reqs__c> reqs = new List<Reqs__c>();
        Reqs__c cloneReq = new Reqs__c();
        
        TestData TestCont = new TestData();
        Contact TdContObj = TestCont.createcontactwithoutaccount(lstNewAccounts[0].Id); 
        TdContObj.Recruiter_Peoplesoft_Id__c = '755656';
        TdContObj.recordtypeid = '012U0000000DWoz';
        update TdContObj;
        
        //Product test data
        Product2 testProduct=TdAcc.createTestProduct();
        testProduct.OpCo__c='Allegis Global Solutions, Inc.';
        insert testProduct; 
        
        Reqs__c req = new Reqs__c (Product__c=testProduct.ID,Account__c =  lstNewAccounts[0].Id, stage__c = 'Qualified', Status__c = 'Open', Address__c = '123 Street', City__c = 'Herndonsfchdvchdcdfbdfdfudafadcrardcadcbcgfgefafcfsgasgchdghcfdfctdftadgdcdafctdaf', State__c = 'Virginia', Zip__c = '12345', Placement_Type__c = 'Direct Placement',Exclusive__c=true,Positions__c = 20065, Draft_Reason__c = 'Test Req',
        Start_Date__c = system.today(),Close_Date__c= system.today().adddays(10),primary_Contact__c = TdContObj.id,Bill_Rate_Max__c = 50,BACKGROUND_CHECK__C='Yes',
        Bill_Rate_Min__c = 25,Standard_Burden__c = 2,Duration__c = 50862,Duration_Unit__c = 'Days',
        Job_category__c = 'Admin',Req_priority__c = 'green',Pay_Rate_Max__c = 10,Pay_Rate_Min__c = 5,
        Job_description__c = 'test code',Organizational_role__c = 'Centre Manager',
        Client_Job_Title__c ='Dev2Managerdqfdfgfdvdtwdtqdtfdfdhagcgdyacdcdacacdtyeqcrwsswrsccacatdcgdcgdtcegaccgfwfrwffwrfr',
        Top_3_Skills__c ='test1,dev2,deploy1',Req_Approval_Process__c='AP',Merged_Req_ID__c='MRI',DBO_Loss_Wash_Reason__c='Candidate was a backout',
        Merged_Req_System__c='MRS',Decline_Reason__c='Remote location',System_SourceReq__c='SSR',Additional_Information__c='AI',
        Client_working_on_Req__c='Yes',Performance_Expectations__c='PE',Work_Environment__c='WE',Interview_Information__c='II',
        End_Client__c=lstNewAccounts[0].id,Project_Stage_Lifecycle_Info__c='PSLI',Nice_to_Have_Skills__c='NHC',External_Job_Description__c='EJD',
        Direct_Placement_Fee__c=20,EVP__c='EVP',Qualified_Date__c=System.today(),Interview_Date__c=System.Today(),
        Compliance__c='gcdafcdfafdfdfwfghgfhghghdgchdhgcgwhchhcvvhdfgydafdvhdhcDJKDGfugdghdGHDAHDACDAHDHCDCDVHJCDCDHJVADJCDCVHCDVHJDCHDHJVHVCHVCDYDAHVDCHVCDVCAVGCVlcakjdcGDGCGUDFCUDVCVDSCCDCVCDFCGDJCDHVCDFDAHDHCDCFCUDVHCVCDVCDVHJCSVDCUVDHHCDCDVHDCVHDVCDHJVDJHVDHDCDCYDCCDVJCDVCDVCDCDCD',Max_Salary__c=200,Min_Salary__c=100,Siebel_ID__c='445745465564535');
        
      reqs.add(req); 
      
      Test.StartTest();
      
          insert reqs;
            for(Account Acc : lstNewAccounts) {                     
                    Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                        Formal_Decision_Criteria__c = 'Defined', 
                        Response_Type__c = 'RFI' , stagename = 'Interest', 
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Impacted_Regions__c='Option1',Req_Legacy_Req_Id__c=reqs[0].id,
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',Source_System_Id__c = '445745465564535',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1);  
                        
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                }  
                // insert opportunity
                insert lstOpportunity;
           
          ProcessLegacyReqs__c p = new ProcessLegacyReqs__c();
          p.Name ='BatchLegacyReqLastRunTime';
          p.LastExecutedBatchTime__c = Datetime.newInstance(2017, 06, 22);
          insert p;
          
          ApexCommonSettings__c sts = new ApexCommonSettings__c();
          sts.Name ='ProcessReqEnts';
          sts.SoqlQuery__c = 'Select Id from Reqs__c where Id != null and SystemModstamp > :dtLastBatchRunTime';
          
          insert sts;
          
          OpportunityTeamMember otm = new OpportunityTeamMember();
          otm.OpportunityId = lstOpportunity[0].id;
          otm.teammemberrole  = 'Recruiter';
          otm.userid = Userinfo.getuserid();
          //rtm.Siebel_Id__c = '0989779';
          //rtm.Contact__c=TdContObj.id;
          //insert otm;
          
          Batch_HistoricalCopyEntToLegacyTeams b = new Batch_HistoricalCopyEntToLegacyTeams(); 
          b.QueryReq = 'Select Id,OpportunityId,Opportunity.Req_Legacy_Req_Id__c,userid, teammemberrole , Opportunity.Req_origination_System_Id__c from OpportunityTeammember where Id != null and lastmodifiedby.name != \'Batch Integration\'';
          Id batchJobId = Database.executeBatch(b);
      
      Test.StopTest();
    }
    
}