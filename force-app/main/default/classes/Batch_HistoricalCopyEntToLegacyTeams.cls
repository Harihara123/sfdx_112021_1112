//***************************************************************************************************************************************/
//* Name        - Batch_HistoricalCopyEntToLegacyTeams
//* Description - Batchable Class used to Copy  enterprise req -- Legacy
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham Uppu               02/19/2018               Created
//*****************************************************************************************************************************************/
// Execute Anonynous code
// Make sure to run as Batch Integration user
/*
Batch_HistoricalCopyEntReqToLegacy b = new Batch_HistoricalCopyEntReqToLegacy(); 
b.QueryReq = 'Select Id,OpportunityId,Opportunity.Req_Legacy_Req_Id__c,userid, teammemberrole , Opportunity.Req_origination_System_Id__c from OpportunityTeammember where Id != null and SystemModstamp > :dtLastBatchRunTime and lastmodifiedby.name != \'Batch Integration\'';
Id batchJobId = Database.executeBatch(b);
*/
global class Batch_HistoricalCopyEntToLegacyTeams implements Database.Batchable<sObject>, Database.stateful
{

   global String QueryReq;
   public Boolean isReqJobException = False;
   global String strErrorMessage = '';
   Global Set<Id> childTeamMemberIdSet  = new Set<Id>();
   Global Set<Id> errorTeamMemberSet  = new Set<Id>();
   Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
   Global List<String> bugList = new List<String>();
   
   global Batch_HistoricalCopyEntToLegacyTeams()
   {
      
   }
   
   global database.QueryLocator start(Database.BatchableContext BC)  
    {  
       //Create DataSet of Reqs to Batch
       return Database.getQueryLocator(QueryReq);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Log__c> errors = new List<Log__c>(); 
        Set<Id> setoppIds = new Set<Id>();
        List<opportunityTeamMember> lstQueriedoppTeamMembers = (List<opportunityTeamMember>)scope;
         try{
               if(lstQueriedoppTeamMembers.size() > 0){
                   EnterprisetolegacyTeamMembersUtil reqTeamMembersUtil = new EnterprisetolegacyTeamMembersUtil(lstQueriedoppTeamMembers);
                   syncErrors = reqTeamMembersUtil.CopyEnterprisetoLegacyMembers();
                }   
            }Catch(Exception e){
             //Process exception here and dump to Log__c object
              bugList.add(e.getMessage());
             if(bugList.size() > 0)
                    Core_Data.logInsertBatchRecords('E-L historicalMig Runtime', bugList);
             syncErrors.errorList.addall(bugList);
             isReqJobException = True;
             strErrorMessage = e.getMessage();
        }
    }
     
    global void finish(Database.BatchableContext BC)
     {
        if(syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0){
           
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Historical Ent to Legacy Teams');
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with failures. Please Check Log records';        
           mail.setPlainTextBody(errorText);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
         
     }

}