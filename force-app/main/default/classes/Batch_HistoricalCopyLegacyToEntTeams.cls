//***************************************************************************************************************************************/
//* Name        - Batch_HistoricalCopyLegacyToEntTeams
//* Description - Batchable Class used to Copy  enterprise Legacy -- Req
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham Uppu               02/19/2018               Created
//*****************************************************************************************************************************************/
// Execute Anonynous code
// Make sure to run as Batch Integration user
/*
Batch_HistoricalCopyLegacyToEntTeams b = new Batch_HistoricalCopyLegacyToEntTeams(); 
b.QueryReq = 'select Id, Contact__c, Requisition__c, Requisition__r.Origination_System_Id__c,Req_Promoted_Date__c,Requisition_Team_Role__c ,User__c, Send_Req_Update_Notification__c, Siebel_Id__c, Team_Member_Name__c, Title__c,Type__c, Name from Req_Team_Member__c where Id != null and SystemModstamp > :dtLastBatchRunTime and lastmodifiedby.name != \'Batch Integration\'';
Id batchJobId = Database.executeBatch(b);
*/
global class Batch_HistoricalCopyLegacyToEntTeams implements Database.Batchable<sObject>, Database.stateful
{

   global String QueryReq;
   public Boolean isReqJobException = False;
   global String strErrorMessage = '';
  Global Set<Id> childTeamMemberIdSet  = new Set<Id>();
  Global Set<Id> errorTeamMemberSet  = new Set<Id>();
  Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
  Global List<String> bugList = new List<String>();

   
   global Batch_HistoricalCopyLegacyToEntTeams()
   {
      
   }
   
   global database.QueryLocator start(Database.BatchableContext BC)  
    {  
       //Create DataSet of Reqs to Batch
       return Database.getQueryLocator(QueryReq);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Log__c> errors = new List<Log__c>(); 
        Set<Id> setoppIds = new Set<Id>();
        List<Req_Team_Member__c> lstQueriedReqTeamMembers = (List<Req_Team_Member__c>)scope;
         try{
               if(lstQueriedReqTeamMembers.size() > 0){
                   LegacyToEnterpriseTeamMembersUtil reqTeamMembersUtil = new LegacyToEnterpriseTeamMembersUtil(lstQueriedReqTeamMembers);
                   syncErrors = reqTeamMembersUtil.CopyLegacytoEnterpriseMembers();
                }   
            }Catch(Exception e){
             //Process exception here and dump to Log__c object
            // errors.add(Core_Log.logException(e));
            // database.insert(errors,false);
             bugList.add(e.getMessage());
             if(bugList.size() > 0)
                    Core_Data.logInsertBatchRecords('L-E HistoricalMigration Child Runtime', bugList);
             syncErrors.errorList.addall(bugList);
             isReqJobException = True;
             strErrorMessage = e.getMessage();
        }
    }
     
    global void finish(Database.BatchableContext BC)
     {
        if(syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0){
           
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Historical Legacy to ENT Teams');
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with failures. Please Check Log records';        
           mail.setPlainTextBody(errorText);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
         
     }

}