// Execute Anonymous code
// Make sure to run as Batch Integration user
//Batch_HistoricalDeleteAccountTeams atm = new Batch_HistoricalDeleteAccountTeams();
//DataBase.executeBatch(atm);
// 
global class Batch_HistoricalDeleteAccountTeams implements Database.Batchable<sObject>, Database.Stateful{
    
    
   global database.QueryLocator start(Database.BatchableContext BC){  
       
       String query = 'SELECT Id FROM AccountTeamMember WHERE TeamMemberRole IN (\'Account Manager\',\'Biller\',\'Recruiter\')';
       return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC, List<AccountTeamMember> scope){
       
       delete scope;
      
           
   }
    
   global void finish(Database.BatchableContext BC){
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
           FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'skaviti@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Historical Delete Account Teams');
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with failures. Please Check Log records';        
           mail.setPlainTextBody(errorText);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });           
   }
}