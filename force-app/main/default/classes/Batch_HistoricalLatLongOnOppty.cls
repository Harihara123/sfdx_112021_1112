/*
Batch_HistoricalLatLongOnOppty b = new Batch_HistoricalLatLongOnOppty (); 
b.QueryReq = 'Select Id,Req_Worksite_Country__c,Req_Worksite_Postal_Code__c,Req_GeoLocation__Latitude__s,Req_GeoLocation__Longitude__s,Account.Account_Country__c,Account.Account_Zip__c,Account.Account_Latitude__c,Account.Account_Longitude__c from Opportunity where lastmodifiedby.name!=\'Batch Integration\'';
Id batchJobId = Database.executeBatch(b);
*/
global class Batch_HistoricalLatLongOnOppty implements Database.Batchable<sObject>, Database.stateful
{

    global String QueryReq;


    global Batch_HistoricalLatLongOnOppty ()
    {

    }

    global database.QueryLocator start(Database.BatchableContext BC)  
    {  
        //Create DataSet of Reqs to Batch
        return Database.getQueryLocator(QueryReq);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Opportunity> lstOpportunityToUpdate = new List<Opportunity>();
        List<Opportunity> lstQueriedOpportunity = (List<Opportunity>)scope;
        try
        {
            for(Opportunity opp: lstQueriedOpportunity)
            {
                if(opp.Req_Worksite_Country__c == opp.Account.Account_Country__c && opp.Req_Worksite_Postal_Code__c == opp.Account.Account_Zip__c.substring(0,5))
                {
                    opp.Req_GeoLocation__Latitude__s  = opp.Account.Account_Latitude__c;
                    opp.Req_GeoLocation__Longitude__s  = opp.Account.Account_Longitude__c;
                    lstOpportunityToUpdate.add(opp);
                }
            } 
            if(!lstOpportunityToUpdate.isEmpty())
            {
                update lstOpportunityToUpdate;
            }

        }
        Catch(Exception e)
        {
            system.debug('Exception::' + e.getMessage());
        }

    }

    global void finish(Database.BatchableContext BC)
    {




    }

}