@istest
public class Batch_HistoricalLatLongOnOppty_Test {
    
    static testMethod void coverBatch(){
        
        
        Account Acc =  new Account( Name = 'TESTACCT',
                                    Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
                                    ShippingPostalCode = '21043',ShippingState = 'Testshipstate',
                                    ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
                                    BillingCountry ='USA',BillingPostalCode ='21043',
                                    BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
                                    recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),MLA_Former_Starts__c = 10,MLA_Current_Starts__c = 10);
    	INSERT Acc;
    
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id, Req_Worksite_Country__c = 'USA',
                RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                Customers_Application_or_Project_Scope__c = 'Precisely Defined', Req_Worksite_Postal_Code__c = '21043',
                Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly');  
        
        INSERT NewOpportunity;
        
        Test.startTest();
        Batch_HistoricalLatLongOnOppty b = new Batch_HistoricalLatLongOnOppty (); 
        b.QueryReq = 'Select Id,Req_Worksite_Country__c,Req_Worksite_Postal_Code__c,Req_GeoLocation__Latitude__s,Req_GeoLocation__Longitude__s,Account.Account_Country__c,Account.Account_Zip__c,Account.Account_Latitude__c,Account.Account_Longitude__c from Opportunity';
        Id batchJobId = Database.executeBatch(b);
		Test.stopTest();
        
    }
    
}