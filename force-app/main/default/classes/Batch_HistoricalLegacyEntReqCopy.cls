//***************************************************************************************************************************************/
//* Name        - Batch_HistoricalCopyEnterpriseReqToLegacy
//* Description - Batchable Class used to Copy  enterprise req -- Legacy
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham Uppu               02/19/2018               Created
//*****************************************************************************************************************************************/
// Execute Anonynous code
// Make sure to run as Batch Integration user
/*
Batch_HistoricalLegacyEntReqCopy b = new Batch_HistoricalLegacyEntReqCopy(); 
b.QueryReq = 'Select Id from Reqs__c where Id != null and SystemModstamp > :dtLastBatchRunTime and lastmodifiedby.name!=\'Batch Integration\'';
Id batchJobId = Database.executeBatch(b);
*/
global class Batch_HistoricalLegacyEntReqCopy implements Database.Batchable<sObject>, Database.stateful
{

   global String QueryReq;
   Global Set<Id> reqIds;
   public Boolean isReqJobException = False;
   global String strErrorMessage = '';
   Global Set<Id> setreqIds;
   Global Set<Id> setParentReqIds = new Set<Id>();
    Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
   Global List<String> exceptionList = new List<String>();

   
   global Batch_HistoricalLegacyEntReqCopy ()
   {
      
   }
   
   global database.QueryLocator start(Database.BatchableContext BC)  
    {  
       //Create DataSet of Reqs to Batch
       return Database.getQueryLocator(QueryReq);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Log__c> errors = new List<Log__c>(); 
        setreqIds = new Set<Id>();
        
        List<Reqs__c> lstQueriedReq = (List<Reqs__c>)scope;
         try{
              for(Reqs__c req: lstQueriedReq){
                   if (req.Id != null){
                     setreqIds.add(req.Id);
                   } 
                  }
                System.Debug('Reqs'+setreqIds);  
                if(setreqIds.size() > 0){
                   LegacyToEnterpriseReqUtil reqUtil = new LegacyToEnterpriseReqUtil(setreqIds);
                   syncErrors = reqUtil.copyLegacyReqToEnterpriseReq();
                }   
            }Catch(Exception e){
             //Process exception here and dump to Log__c object
             //errors.add(Core_Log.logException(e));
             //database.insert(errors,false);
              exceptionList.add(e.getMessage());
              if(exceptionList.size() > 0)
                    Core_Data.logInsertBatchRecords('Legacy-Ent ParentHistorical Runtime', exceptionList);
              syncErrors.errorList.addall(exceptionList);
             isReqJobException = True;
             strErrorMessage = e.getMessage();
        }

    }
     
    global void finish(Database.BatchableContext BC)
     {
        System.debug('SYNC LOG' +syncErrors);
        if (syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0 || Test.isRunningTest()){
           
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com','hachanta@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Historical Legacy to EntReq');
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with failures. Please Check Log records';        
           mail.setPlainTextBody(errorText);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
         
     }

}