//***************************************************************************************************************************************/
//* Name        - Batch_IdentifyOutOfSyncAccounts 
//* Description - Batchable Class used to Identify Out Of Sync Accounts and create corresponding Retry Records
//                both contexts - On-demand and Automatic
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Pratz                       01/29/2013            Created
//* Dhruv                       02/05/2013            Modified to Fix Defect # 27613
//* Pratz                       02/05/2013            Modified to Fix Defect # 27671 
//*****************************************************************************************************************************************/


global class Batch_IdentifyOutOfSyncAccounts implements Database.Batchable<sObject>, Database.stateful
{
    //Declare Variables
    global String QueryAccount;    
    global  Integer OutOfSyncAccounts = 0;
    global Integer NumberOfAccountBatches = 0;//# 27671
    global Integer NumberOfAccountErrors = 0;//# 27671
    String strJobName = 'IdentifyOutOfSyncLastRunTime';
    global Boolean isAccountJobException = False;
    global String strErrorMessage = '';
    global string rcdtypeid = string.valueof(label.Account_Client_RecordTypeID);
    
    //Current DateTime stored in a variable
    DateTime newBatchRunTime; 

  
    //Get the ProcessIntegrationResponseRecords Custom Setting Record
    ProcessIntegrationResponseRecords__c mc = ProcessIntegrationResponseRecords__c.getValues(strJobName);
         
    //Store the Last time the Batch was Run in a variable
    DateTime dtLastBatchRunTime = mc.Start_Time__c;

    //Constructor
    global Batch_IdentifyOutOfSyncAccounts(String s, Datetime latestBatchRunTime)
    {
        System.debug('*****'+dtLastBatchRunTime);      
                
        //Update the Batch Run TIme
        newBatchRunTime = latestBatchRunTime;
        
        //Construct the Query to get the Data records for the Batch Processing    
        //02/17/2016-(Developer:Hareesh Achanta)-The below string query has been modified for org merge changes. we are limiting our accounts to only with record type as client    
        s = s+' WHERE AccountServicesChange__c > :dtLastBatchRunTime AND recordtypeid=:rcdtypeid ORDER BY AccountServicesChange__c DESC';
        system.debug('************'+s);
        
        //Assign the Query to a string variable
        QueryAccount = s;        
    }

    global Iterable<sObject> start(Database.BatchableContext BC)  
    {  
        //Create DataSet of Accounts to Batch
        return Database.getQueryLocator(QueryAccount);
    }  


 
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        //Declare variables
        String QueryIntegrationResponse;
        String QueryAccounts;
        Map<Id, Integration_Response__c> IntegrationResponseMap = new Map<Id, Integration_Response__c>();        
        Map<Id, Account> AccountsCreatedUpdatedSinceLastJobRunTimeMap = new Map<Id, Account>();                               
        Map<Id,Integration_Response__c> mapObjIdObjLM1 = new Map<Id,Integration_Response__c>();
        Map<Id,Integration_Response__c> mapLMIntResp = new Map<Id,Integration_Response__c>();        
        List<Retry__c> LSRecordsToBeAddedToRetryObject = new List<Retry__c>();
        List<Log__c> errors = new List<Log__c>();
        String strJobName;
       
        //List of Accounts in scope in this batch (20 Accounts) 
        List<Account> lstQueriedAccounts = (List<Account>)scope;
        System.debug('%%%%%%%'+lstQueriedAccounts);
                                   
        try
        {    
            //Populate the Accounts in this batch in a map
            for(Account Account : lstQueriedAccounts) {
                AccountsCreatedUpdatedSinceLastJobRunTimeMap.put(Account.id, Account);
            }
            
            system.debug('AccountsCreatedUpdatedSinceLastJobRunTimeMap: '+AccountsCreatedUpdatedSinceLastJobRunTimeMap);   
            
                                            
            //Create Sets of AccountIds            
            Set<Id> AccountIds = AccountsCreatedUpdatedSinceLastJobRunTimeMap.keyset();
            system.debug('AccountIds:' + AccountIds);
            
            system.debug('Number of Accounts in this Batch'+  AccountsCreatedUpdatedSinceLastJobRunTimeMap.size());
            
            //Create a List of Integration_Response__c records which have been created since the last time job ran
            List<Integration_Response__c> lstTempIntResRecords = [Select id,Object_Id__c,Object__c,Status__c,Object_Last_Modified__c FROM Integration_Response__c WHERE Object_Last_Modified__c >:dtLastBatchRunTime AND Object_Id__c IN :AccountIds];
            for(Integration_Response__c intRespRecord : lstTempIntResRecords)
            mapLMIntResp.put(intRespRecord.id , intRespRecord);
            
            system.debug('Number of Integration Response Records  for this Batch'+  mapLMIntResp.size());
                        
            for(AggregateResult aggRes : [Select MAX(Object_Last_Modified__c) maxVal,Object_Id__c From Integration_Response__c WHERE Object_Last_Modified__c >:dtLastBatchRunTime AND Object_Id__c IN :AccountIds GROUP BY Object_Id__c ]) {
            
            system.debug('!!!!!!!!!!!!!aggRes'+aggRes);
              
              
               /**********************FIX 3/12/2013***********************************/
               String strObjectId = (String)aggRes.get('Object_Id__c');
               system.debug('strObjectId: '+strObjectId);
               Datetime dtObjectLastModified = (DateTime)aggRes.get('maxVal');
               system.debug('dtObjectLastModified: '+dtObjectLastModified);
               Integration_Response__c IR = [SELECT Id, Object_Id__c, Object_Last_Modified__c, Status__c FROM Integration_Response__c WHERE Object_Last_Modified__c >:dtLastBatchRunTime AND (Object_Id__c = :strObjectId AND Object_Last_Modified__c =: dtObjectLastModified) LIMIT 1]; 
               system.debug(IR);
               //Make a map by having Object_Id__c as a key and the Max Last Modified for this Object_Id__c as a value
               mapObjIdObjLM1.put((Id)aggRes.get('Object_Id__c'),IR);
               /**********************FIX 3/12/2013***********************************/
              
              
              
              //mapObjIdObjLM1.put((Id)aggRes.get('Object_Id__c'), mapLMIntResp.get((Id)aggRes.get('maxId')));             
            } 
            system.debug('************mapObjIdObjLM1'+mapObjIdObjLM1);
             
            system.debug('Number of Aggregate Integration Response Records  for this Batch'+  mapObjIdObjLM1.size());              
            /******************************************************************************************/ 
            // Creating Retries for Accounts 
            /******************************************************************************************/
           
            for(Id AccountId : AccountsCreatedUpdatedSinceLastJobRunTimeMap.keyset())
            {
                   if(mapObjIdObjLM1.containsKey(AccountId))    // check we have an integ response for this one
                       {
                          if( (AccountsCreatedUpdatedSinceLastJobRunTimeMap.get(AccountId).AccountServicesChange__c == mapObjIdObjLM1.get(AccountId).Object_Last_Modified__c) && (mapObjIdObjLM1.get(AccountId).Status__c == 'Success') )
                              {
                                  // Good Accounts with valid(success) IntegrationResponse combination matching datetime; so Do Nothing
                                  
                                  system.debug('@@@@@@@@@@@@@@'+AccountsCreatedUpdatedSinceLastJobRunTimeMap.get(AccountId).AccountServicesChange__c);
                                    system.debug('^^^^^^^^^^^^^^^^^^'+mapObjIdObjLM1.get(AccountId).Object_Last_Modified__c);
                                    system.debug('%%%%%%%%%%%%%%%%%%%'+mapObjIdObjLM1.get(AccountId).Status__c);
                                  
                                  
                                   system.debug('Not Out of Sync');
                               }
                          else
                              {
                                    // Retry entries will be created if Last ACK is missing or is the last ACK status is Failed
                                    Retry__c retry_ir = new Retry__c(Id__c = AccountId, Object__c = 'Account', Status__c = 'Open'); 
                                    LSRecordsToBeAddedToRetryObject.Add(retry_ir);
                                    OutOfSyncAccounts++;
                                    system.debug('Failed ACK');
                                    system.debug('AccountId'+AccountId);
                                    system.debug('@@@@@@@@@@@@@@'+AccountsCreatedUpdatedSinceLastJobRunTimeMap.get(AccountId).AccountServicesChange__c);
                                    system.debug('^^^^^^^^^^^^^^^^^^'+mapObjIdObjLM1.get(AccountId).Object_Last_Modified__c);
                                    system.debug('%%%%%%%%%%%%%%%%%%%'+mapObjIdObjLM1.get(AccountId).Status__c);
                              }                                                                                                              
                       }
                    else
                       {
                           // Retry entries will be created if we havent receievd an ACK for Account change
                           Retry__c retry_ir = new Retry__c(Id__c = AccountId, Object__c = 'Account', Status__c = 'Open'); 
                           LSRecordsToBeAddedToRetryObject.Add(retry_ir);
                           OutOfSyncAccounts++; 
                           system.debug('Missing Ack');                                                                  
                       }
            }
            
            //****************************************************************************************************************    
            system.debug('Out of Sync Accounts: ' +OutOfSyncAccounts);
            System.debug('*****'+LSRecordsToBeAddedToRetryObject);
            
            //If the List of Records to be added to the Retry object is not null
            if(LSRecordsToBeAddedToRetryObject.size()>0) //Added a null check Dhruv[01/02/2102]
            {
              //Insert the records in Retry__c object 
              insert LSRecordsToBeAddedToRetryObject;
              
            } 
        }
        catch (Exception e)
        {
            // Process exception here and dump to Log__c object
            errors.add(Core_Log.logException(e));
            database.insert(errors,false);
            
            //Set Exception Boolean Variable to True
            isAccountJobException = True;
            
            //String variable to store the Error Message
            strErrorMessage = e.getMessage();
        } 
    }        
    global void finish(Database.BatchableContext BC)
    {
        
         //Execute the Req job only if there was no Exception
         if(!isAccountJobException)
         {
              //Create an object of AsyncApexJob class 
              AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems from AsyncApexJob where Id = :BC.getJobId()]; //# 27671
         
              //Total nnumber of Account Batches
              NumberOfAccountBatches = a.TotalJobItems; //# 27671
         
              //Total number of errors processing account batches
              NumberOfAccountErrors = a.NumberOfErrors; //# 27671
              System.debug('########'+OutOfSyncAccounts); 
         
              //Initialise an object of class Batch_IdentifyOutOfSyncReqs for process Req Out of Sync Records
              Batch_IdentifyOutOfSyncReqs IdentifyOutOfSyncReqsBatch = new Batch_IdentifyOutOfSyncReqs('Select Id, SystemModstamp, LastModifiedDate From Reqs__c',newBatchRunTime, OutOfSyncAccounts, NumberOfAccountBatches, NumberOfAccountErrors); //# 27671
         
              //Execute the BatchReqIntegrationResponse Batch, with 20 records per batch
              Database.executeBatch(IdentifyOutOfSyncReqsBatch,20); 
         }
         else
         {
              //If there is an Exception, send an Email to the Admin team member
              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
              String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
              mail.setToAddresses(toAddresses); 
              String strEmailBody;               
              mail.setSubject('EXCEPTION - Identify Out Of Sync Accounts Batch Job'); 
              
              //Prepare the body of the email
              strEmailBody = 'The scheduled Apex Identify Out Of Sync Accounts Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                
              mail.setPlainTextBody(strEmailBody);   
              
              //Send the Email
              Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
              
              //Reset the Boolean variable and the Error Message string variable
              isAccountJobException = False;
              strErrorMessage = '';                       
         }    
                
    }
}