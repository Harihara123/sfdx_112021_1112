//***************************************************************************************************************************************/
//* Name        - Batch_IdentifyOutOfSyncReqs 
//* Description - Batchable Class used to Identify Out Of Sync Reqs and create corresponding Retry Records
//                both contexts - On-demand and Automatic
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Pratz                       01/29/2013            Created
//* Pratz                       02/05/2013            Modified to Fix Defect # 27671
//*****************************************************************************************************************************************/


global class Batch_IdentifyOutOfSyncReqs implements Database.Batchable<sObject>, Database.stateful
{
    //Declare Variables
    global String QueryReq; 
    global Integer OutOfSyncReqs = 0;
    global Integer local_OutOfSyncAccounts;
    global Integer local_NumberOfAccountBatches;//# 27671
    global Integer local_NumberOfAccountErrors;//# 27671
    String strJobName = 'IdentifyOutOfSyncLastRunTime';
    global Boolean isReqJobException = False;
    global String strErrorMessage = '';
    
    //Current DateTime stored in a variable
    DateTime newBatchRunTime; 
    
    //Get the ProcessIntegrationResponseRecords Custom Setting Record
    ProcessIntegrationResponseRecords__c mc = ProcessIntegrationResponseRecords__c.getValues(strJobName);
         
    //Store the Last time the Batch was Run in a variable
    DateTime dtLastBatchRunTime = mc.Start_Time__c;
    
    //Constructor
    global Batch_IdentifyOutOfSyncReqs(String s, Datetime latestBatchRunTime, Integer OutOfSyncAccounts, Integer NumberOfAccountBatches, Integer NumberOfAccountErrors)
    {
        system.debug('@@@@@@@@@@'+OutOfSyncAccounts);
        System.debug('*****'+dtLastBatchRunTime);      
                
        //Update the Batch Run TIme
        newBatchRunTime = latestBatchRunTime;
        
        //Construct the Query to get the Data records for the Batch Processing        
        s = s+' WHERE LastModifiedDate > :dtLastBatchRunTime ORDER BY LastModifiedDate DESC';
        system.debug('************'+s);
        
        //Assign the Query to a string variable
        QueryReq = s;
        
        //Assign the Number of Out Of Sync Accounts Integer value to a loval integer variable
        this.local_OutOfSyncAccounts = OutOfSyncAccounts; 
        this.local_NumberOfAccountBatches = NumberOfAccountBatches;//# 27671
        this.local_NumberOfAccountErrors = NumberOfAccountErrors;//# 27671
        System.debug('&&&&&&&&'+local_OutOfSyncAccounts); 
        System.debug('NumberOfAccountBatches '+local_NumberOfAccountBatches);//# 27671  
        System.debug('Number of Account Errors' + local_NumberOfAccountErrors); //# 27671   
    }

    global Iterable<sObject> start(Database.BatchableContext BC)  
    {  
        return Database.getQueryLocator(QueryReq);
    }  


 
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        //Declare variables
        String QueryIntegrationResponse;
        String QueryReqs;
        Map<Id, Integration_Response__c> IntegrationResponseMap = new Map<Id, Integration_Response__c>();        
        Map<Id, Reqs__c> ReqsCreatedUpdatedSinceLastJobRunTimeMap = new Map<Id, Reqs__c>();                        
        Map<Id,Integration_Response__c> mapObjIdObjLM = new Map<Id,Integration_Response__c>();
        Map<Id,Integration_Response__c> mapIdIntRes = new Map<Id,Integration_Response__c>();
        List<Retry__c> LSRecordsToBeAddedToRetryObject = new List<Retry__c>();
        List<Log__c> errors = new List<Log__c>();       
        String strJobName;
       
        //List of Reqs in scope in this batch (200 Reqs) 
        List<Reqs__c> lstQueriedReqs = (List<Reqs__c>)scope;
        System.debug('%%%%%%%'+lstQueriedReqs);
                                   
        try
        {    
            //Populate the Reqs in this batch in a map
            for(Reqs__c req : lstQueriedReqs) 
                ReqsCreatedUpdatedSinceLastJobRunTimeMap.put(req.id, req);                                  
            //Create Set of Reqs            
            Set<Id> reqIds = ReqsCreatedUpdatedSinceLastJobRunTimeMap.keyset();
            
            
            //Create a List of Integration_Response__c records which have been created since the last time job ran
            List<Integration_Response__c> lstTempIntResRecords = [Select id,Object_Id__c,Object__c,Status__c,Object_Last_Modified__c FROM Integration_Response__c WHERE Object_Last_Modified__c >:dtLastBatchRunTime AND Object_Id__c IN :reqIds]; 
            for(Integration_Response__c intResp : lstTempIntResRecords)
            mapIdIntRes.put(intResp.id, intResp);
            
                                 
            for(AggregateResult aggRes : [Select MAX(Object_Last_Modified__c) maxVal, Object_Id__c From Integration_Response__c WHERE Object_Last_Modified__c >:dtLastBatchRunTime AND Object_Id__c IN :reqIds GROUP BY Object_Id__c]) {
            
               /**********************FIX 3/12/2013***********************************/
               String strObjectId = (String)aggRes.get('Object_Id__c');
               system.debug('strObjectId: '+strObjectId);
               Datetime dtObjectLastModified = (DateTime)aggRes.get('maxVal');
               system.debug('dtObjectLastModified: '+dtObjectLastModified);
               Integration_Response__c IR = [SELECT Id, Object_Id__c, Object_Last_Modified__c, Status__c FROM Integration_Response__c WHERE Object_Last_Modified__c >:dtLastBatchRunTime AND (Object_Id__c = :strObjectId AND Object_Last_Modified__c =: dtObjectLastModified) LIMIT 1]; 
               system.debug(IR);
               //Make a map by having Object_Id__c as a key and the Max Last Modified for this Object_Id__c as a value
               mapObjIdObjLM.put((Id)aggRes.get('Object_Id__c'),IR);
               /**********************FIX 3/12/2013***********************************/
               
               //mapObjIdObjLM.put((Id)aggRes.get('Object_Id__c'), mapIdIntRes.get((Id)aggRes.get('maxId')));
                              
            } 
            
            system.debug('mapObjIdObjLM: '+mapObjIdObjLM);   
          
                                    
             /******************************************************************************************/             
             // Creating Retries for Reqs
             /******************************************************************************************/
            for(Id ReqId : ReqsCreatedUpdatedSinceLastJobRunTimeMap.keyset())
            {
                if(mapObjIdObjLM.containsKey(ReqId))  // check we have an integ response for this one
                {
                    if( (ReqsCreatedUpdatedSinceLastJobRunTimeMap.get(ReqId).LastModifiedDate == mapObjIdObjLM.get(ReqId).Object_Last_Modified__c) && (mapObjIdObjLM.get(ReqId).Status__c == 'Success') )
                    {
                          // Good Reqs with valid(success) IntegrationResponse combination matching datetime; so Do Nothing                    
                     }
                    else
                    {
                        // Retry entries will be created if Last ACK is missing or is the last ACK status is Failed
                        Retry__c retry_ir = new Retry__c(Id__c = ReqId, Object__c = 'Req', Status__c = 'Open'); 
                        LSRecordsToBeAddedToRetryObject.Add(retry_ir);
                        OutOfSyncReqs++;
                        system.debug('Failed ACK :'+ ReqId);
                    }                                                                                                              
                }
                else
                {
                    // Retry entries will be created if we havent receievd an ACK for Req change
                    Retry__c retry_ir = new Retry__c(Id__c = ReqId, Object__c = 'Req', Status__c = 'Open'); 
                    LSRecordsToBeAddedToRetryObject.Add(retry_ir);
                    OutOfSyncReqs++;  
                    system.debug('Missing ACK :'+ ReqId);                                                                 
                }
            }  
         
            system.debug('Out of Sync Reqs: ' +OutOfSyncReqs);
            System.debug('*****'+LSRecordsToBeAddedToRetryObject);
            
            //If the List of Records to be added to the Retry object is not null
            if(LSRecordsToBeAddedToRetryObject.size()>0) //Added a null check Dhruv[01/02/2102]
            {
              //Insert the records in Retry__c object 
              insert LSRecordsToBeAddedToRetryObject;
              
            } 
        }
        catch (Exception e)
        {
            // Process exception here and dump to Log__c object
            errors.add(Core_Log.logException(e));
            database.insert(errors,false);
            
            //Set Exception Boolean Variable to True
            isReqJobException = True;
            
            //String variable to store the Error Message
            strErrorMessage = e.getMessage();
        } 
        
       
      
    }        
    global void finish(Database.BatchableContext BC)
    {
         //Execute the Req job only if there was no Exception
         if(!isReqJobException)
         {   
                //Update the Custom Setting's Start_Time__c with the New Time Stamp for the last time the job ran
                ProcessIntegrationResponseRecords__c IntResCustomSetting1 = ProcessIntegrationResponseRecords__c.getValues('IdentifyOutOfSyncLastRunTime');                  
                IntResCustomSetting1.Start_Time__c = newBatchRunTime ;
                System.debug('&&&&&&&&&'+newBatchRunTime);
                if(IntResCustomSetting1 != null)   //Added a null check 
                    update IntResCustomSetting1;
            
            
                //Send Email with the Batch Job details                
                AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()]; 
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  
                mail.setToAddresses(toAddresses);  
                mail.setSubject('Identify Out Of Sync Accounts/Reqs Batch Job Status: ' + a.Status);                  
                mail.setPlainTextBody('The batch Apex job processed  '+ local_NumberOfAccountBatches +' Account batches with ' +local_NumberOfAccountErrors+ ' failures and ' + a.TotalJobItems +' Req batches with '+ a.NumberOfErrors + ' failures. \nError Description: ' + a.ExtendedStatus +' \nNumber of Accounts Out Of Sync: '+local_OutOfSyncAccounts  +'\nNumber of Reqs Out Of Sync: ' + OutOfSyncReqs);//# 27671  
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         }
         else
         {
                //If there is an Exception, send an Email to the Admin team member
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
                mail.setToAddresses(toAddresses); 
                String strEmailBody;               
                mail.setSubject('EXCEPTION - Identify Out Of Sync Reqs Batch Job'); 
              
                //Prepare the body of the email
                strEmailBody = 'The scheduled Apex Req Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
               
                //Reset the Boolean variable and the Error Message string variable
                isReqJobException = False;
                strErrorMessage = ''; 
                
         }       
    }
}