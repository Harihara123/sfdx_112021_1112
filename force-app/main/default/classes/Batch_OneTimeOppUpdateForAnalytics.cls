//***************************************************************************************************************************************/
//* Name        - Batch_OneTimeOppUpdateForAnalytics
//* Description - Batchable Class used to Update opp for Interview and Sub dates
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham Uppu               04/11/2019               Created
//*****************************************************************************************************************************************/
// Execute Anonynous code
// Make sure to run as Batch Integration user
/*
Batch_OneTimeOppUpdateForAnalytics b = new Batch_OneTimeOppUpdateForAnalytics();
b.QueryReq = 'Select Id,First_Submittal_Date__c,First_Interview_Date__c from Opportunity where Status__c = \'Open\' and RecordType.Name =\'Req\'';
Id batchJobId = Database.executeBatch(b);
*/
global class Batch_OneTimeOppUpdateForAnalytics implements Database.Batchable<sObject>, Database.stateful
{

   global String QueryReq;
   public Boolean isReqJobException = False;
   global Boolean isOpportunityJobException = False;
   global String strErrorMessage = '';
   Global Set<Id> setoppIds;
   Global Set<Id> setParentOppIds = new Set<Id>();
   Global List<String> exceptionList = new List<String>();
   Global List<ID> failedOppIds = new List<ID>();
   Global List<String> dataErrorList = new List<String>();
   
   global Batch_OneTimeOppUpdateForAnalytics()
   {
      
   }
   
   global database.QueryLocator start(Database.BatchableContext BC)  
    {  
       //Create DataSet of Reqs to Batch
       return Database.getQueryLocator(QueryReq);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Log__c> errors = new List<Log__c>(); 
        setoppids = new Set<Id>();
        Map<Id,Order> orderMap;
        Map<Id,Opportunity> tobeUpdatedOppMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> finalOppMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> subOppMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> intOppMap = new Map<Id,Opportunity>();
        List<opportunity> lstQueriedopp = (List<opportunity>)scope;

         try{
              for(opportunity opp: lstQueriedopp){
                   if (opp.Id != null){
                     setoppids.add(opp.Id);
                     tobeUpdatedOppMap.put(opp.Id,opp);
                   } 
                  }
                  
                System.Debug('opportunities'+setoppIds);  
                if(setoppIds.size() > 0){
                   
                   orderMap = new Map<Id,Order>([select Id, status, OpportunityId, CreatedDate, Opportunity.First_Submittal_Date__c, Opportunity.First_Interview_Date__c  from Order 
                                                  Where OpportunityId in :setoppIds and status in ('Submitted','Interviewing','Not Proceeding - Submitted','Not Proceeding - Interviewing')]);
                   
                   List<Event> evts = [select WhatId, type, ActivityDate from Event
                                       where WhatId in (select Id from Order where OpportunityId in : setoppIds) and type in ('Submitted','Interviewing','Not Proceeding - Submitted','Not Proceeding - Interviewing', 'Offer Accepted', 'Started', 'Not Proceeding - Started', 'Not Proceeding - Offer Accepted','Linked')
                                       order by ActivityDate, type asc All Rows];
                   
                   
					if(evts.size() > 0){
                        for(Event evt: evts){
                            if(orderMap.ContainsKey(evt.WhatId)){
                                Opportunity opty = new Opportunity();
                                opty.Id = orderMap.get(evt.WhatId).OpportunityId;
                                if(evt.type == 'Submitted' && orderMap.ContainsKey(evt.WhatId) && tobeUpdatedOppMap.get(opty.Id).First_Submittal_Date__c == null){
                                    tobeUpdatedOppMap.get(opty.Id).First_Submittal_Date__c = evt.ActivityDate;
                                }
								if(evt.type == 'Interviewing' && orderMap.ContainsKey(evt.WhatId) && tobeUpdatedOppMap.get(opty.Id).First_Interview_Date__c == null){
                                    tobeUpdatedOppMap.get(opty.Id).First_Interview_Date__c = evt.ActivityDate;
                                }
                            }
                        }
                    }
                   if(tobeUpdatedOppMap.size() > 0){
                      Database.SaveResult[] updatedResults = Database.Update(tobeUpdatedOppMap.values(),false);
                           if (updatedResults != null){
                                for(Integer i=0;i<updatedResults.size();i++){
                                          if(!updatedResults.get(i).isSuccess()){
                                            // DML operation failed
                                            Database.Error error = updatedResults.get(i).getErrors().get(0);
                                            dataErrorList.add(error.getMessage());
                                            failedOppIds.add(tobeUpdatedOppMap.values().get(i).Id);
                                            isOpportunityJobException = True;
                                       }
                                 }
                                 if(dataErrorList.size() > 0)
                                   Core_Data.logInsertBatchRecords('EinsteinAnalyticsUpdate', dataErrorList); 
                           	if(Test.isRunningTest()){ 
                                isOpportunityJobException = True;
                            }   
                           }
                      }  
                  }
                }Catch(Exception e){
                   //Process exception here and dump to Log__c object
                   exceptionList.add(e.getMessage());
                   if(exceptionList.size() > 0)
                      Core_Data.logInsertBatchRecords('EinsteinAnalyticsOppUpdate', exceptionList);
                   isReqJobException = True;
            
               }
      }
     
    global void finish(Database.BatchableContext BC)
     {
       if(isOpportunityJobException || isReqJobException){
           
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch_OneTimeOppUpdateForAnalytics');
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with failures. Please Check Log records';        
           mail.setPlainTextBody(errorText);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
        
     }

}