@istest
public class Batch_OneTimeOppUpdateForAnalytics_Test {
    
    static testMethod void coverBatch(){
        
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;    
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;    
        Account Acc =  new Account( Name = 'TESTACCT',
                                    Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
                                    ShippingPostalCode = '21043',ShippingState = 'Testshipstate',
                                    ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
                                    BillingCountry ='USA',BillingPostalCode ='21043',
                                    BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
                                    recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),MLA_Former_Starts__c = 10,MLA_Current_Starts__c = 10);
        INSERT Acc;
        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'Aerotek CE',
                                    Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
                                    Jobcode_Id__c  ='700009',  OpCo_Id__c='ONS', Segment_Id__c  ='A_AND_E',  Segment__c = 'Architecture and Engineering',
                                    OpCo_Status__c = 'A', Skill_Id__c  ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id, Req_Worksite_Country__c = 'USA',
                RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='TEKsystems, Inc.',BusinessUnit__c ='EASI',
                Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                Customers_Application_or_Project_Scope__c = 'Precisely Defined', Req_Worksite_Postal_Code__c = '21043', Status__c = 'Open',
                Impacted_Regions__c='TEK APAC',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',
                Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,
                Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly',Legacy_Product__c = prd.id);  
        
        INSERT NewOpportunity;
        
        Contact newCon = BaseController_Test.createTalentContact(Acc.Id);
        
        string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
        Order order = new Order(Name = 'New Order', EffectiveDate = system.today()+1,
            Status = 'Linked',AccountId = NewOpportunity.AccountId, OpportunityId = NewOpportunity.Id, shiptocontactid=newCon.id, recordtypeid = OrderRecordType);
        insert order;   
        
        
        Event evt = new Event (whatId = order.Id, Description = 'Test description', DurationInMinutes = 5, 
                               ActivityDateTime = system.now(), Type='Interviewing',Interview_Status__c='Open',
                              Interview_Type__c='Phone',Submittal_Interviewers__c='test');
        Insert evt;
        
        Test.startTest();
        Batch_OneTimeOppUpdateForAnalytics b = new Batch_OneTimeOppUpdateForAnalytics();
        b.QueryReq = 'Select Id,First_Submittal_Date__c,First_Interview_Date__c from Opportunity where Status__c = \'Open\' and RecordType.Name =\'Req\'';
        Id batchJobId = Database.executeBatch(b);
        Test.stopTest();
        
    }
    
}