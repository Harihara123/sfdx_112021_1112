/***************************************************************************************************************************************
* Name        - Batch_Ownership_AutoExpiration 
* Description - This candidate ownership auto-expiration job will trigger an updated HR-XML file for Search with candidate ownership 
				flag set to false 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Karthik                    11/21/2017             Created
*****************************************************************************************************************************************/
global class Batch_Ownership_AutoExpiration implements Database.Batchable<sObject>, Database.stateful{
    global String QueryObject;
    global Set<ID> talentAccts = new Set<ID>();
        
    global Batch_Ownership_AutoExpiration(){
       //SOQL query from Custom Settings
       String qryFromCustomSetting = 'Batch_Ownership_AutoExpiration';       
       ApexCommonSettings__c s = ApexCommonSettings__c.getValues(qryFromCustomSetting);
       QueryObject = s.SoqlQuery__c;
    }
        
    global Database.querylocator start(Database.BatchableContext BC){ 
        return Database.getQueryLocator(QueryObject);  
    } 
        
   	global void execute(Database.BatchableContext BC, List<Target_Account__c> scope){
        System.debug('Scope: '+scope);
        //List<Target_Account__c> tAccts = new List<Target_Account__c>();
        for(Target_Account__c t : scope){
            talentAccts.add(t.Account__c);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        String query = 'SELECT Id FROM Account WHERE ID IN';
        
        if(talentAccts != null && talentAccts.size() > 0){
            HRXMLBatch hBatch = new HRXMLBatch(talentAccts, query);
            Database.executeBatch(hBatch, 50);
        }
	}
}