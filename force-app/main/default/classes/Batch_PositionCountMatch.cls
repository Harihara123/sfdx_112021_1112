//***************************************************************************************************************************************/
//* Name        - Batch_PositionCountMatch
//* Description - Batchable Class used to reconcile positions summary for fill, loss, wash positions from the position
//* card also, applicable to historical opportunities when they are uploaded so that position log is created.
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Showket Bhat               09/13/2019                Created

/*Anonymous code to run this batch
 Batch_PositionCountMatch objBatch = new Batch_PositionCountMatch();
 objBatch.strQuery = 'SELECT Id, Req_Open_Positions__c, Req_Total_Filled__c, Req_Total_Lost__c, Req_Total_Washed__c, 
                     Req_Loss_Wash_Reason__c, isClosed, CreatedDate, LastModifiedDate FROM opportunity WHERE Id = \'006P0000009II3kIAG\'';
 Database.executeBatch(objBatch);
*/

global class Batch_PositionCountMatch implements Database.Batchable<Sobject>{
    
    global String strQuery;
    
    global Batch_PositionCountMatch(){ 
        strQuery = '';
    }
    
    global database.Querylocator start(Database.BatchableContext context){
        return Database.getQueryLocator(strQuery);          
    }
    
    global void execute(Database.BatchableContext context , Opportunity[] scope){
        
        List<Log__c> errors = new List<Log__c>();
       
        try{
            /*D-12375 - Ensure Position Card Records are in sync with Position Summary Counts */ 
            CRM_PositionCountMatchUtility.syncPositionCount(new Map<Id, Opportunity>(scope));
            if(Test.isRunningTest()){
                integer a = 0/10 ;
            }
        }
        catch (Exception e) 
        {
            errors.add(Core_Log.logException(e));
            database.insert(errors,false);
        }  
        
    }
    
    global void finish(Database.BatchableContext context){
    }
    
}