@isTest
public class Batch_PositionCountMatch_Test {
    
    testmethod static void coverBatch() {
        
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings; 
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;    
        Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);
        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'Aerotek CE',
                                    Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
                                    Jobcode_Id__c  ='700009',  OpCo_Id__c='ONS', Segment_Id__c  ='A_AND_E',  Segment__c = 'Architecture and Engineering',
                                    OpCo_Status__c = 'A', Skill_Id__c  ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity' , Accountid = newClientAcc.id, Req_Total_Positions__c = 12,
                RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='TEKsystems, Inc.',BusinessUnit__c ='EASI',
                stagename = 'Draft',Impacted_Regions__c='TEK APAC', Access_To_Funds__c = 'abc', CloseDate = system.today()+1,
                Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,
                Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,
                Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly', Req_Total_Filled__c = 3, 
                Req_Total_Lost__c = 2 , Req_Total_Washed__c = 2,Legacy_Product__c = prd.id );
        Database.insert(NewOpportunity);
        
        List<Position__c> deletePositions = [Select id from position__c where opportunity__c =: NewOpportunity.Id ];
        delete deletePositions;
        
        Position__c position1 = new position__c( PositionStatus__c = 'Open', opportunity__c = NewOpportunity.id );
        Position__c position2 = new position__c( PositionStatus__c = 'Fill', opportunity__c = NewOpportunity.id );
        Position__c position3 = new position__c( PositionStatus__c = 'Loss', opportunity__c = NewOpportunity.id );
        Position__c position4 = new position__c( PositionStatus__c = 'Wash', opportunity__c = NewOpportunity.id );
        
        List<Position__c> lstNewPositions = new list<Position__c>{position1, position2, position3, position4};
        Database.insert(lstNewPositions);
        
        
        
        Batch_PositionCountMatch objBatch = new Batch_PositionCountMatch();
        objBatch.strQuery = 'SELECT Id, isClosed, lastModifiedDate, Req_Open_Positions__c, Req_Total_Filled__c, Req_Total_Lost__c, Req_Total_Washed__c, Req_Loss_Wash_Reason__c FROM opportunity';
        Database.executeBatch(objBatch);
 
    }
}