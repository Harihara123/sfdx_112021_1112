//***************************************************************************************************************************************/
//* Name        - Batch_ProcessEnterpriseReqHRXML
//* Description - Batchable Class used to Identify Opportunities that did not generate HRXML 
//                and update them
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham                   01/31/2017                Created
//*****************************************************************************************************************************************/
global class Batch_ProcessEnterpriseReqHRXML implements Database.Batchable<sObject>, Database.stateful,Database.AllowsCallouts
{
   global Boolean isHRXMLJobException = False;
   global DateTime dtLastBatchRunTime;
   global String strErrorMessage = '';
   global String QueryAccount;   
   //Current DateTime stored in a variable
    DateTime newBatchRunTime;
    
    //Constructor
    global Batch_ProcessEnterpriseReqHRXML()
    {
       Datetime latestBatchRunTime = System.now();
       
       String strJobName = 'HRXMLLastRunTime';
       //Get the ProcessOpportunityRecords Custom Setting
       ProcessReqHRXML__c p = ProcessReqHRXML__c.getValues(strJobName);
         
       //Store the Last time the Batch was Run in a variable
       dtLastBatchRunTime = p.LastExecutedTime__c;
       
       newBatchRunTime = latestBatchRunTime;
       
       if(Test.isRunningTest()){
                    QueryAccount = 'select Probability, (select Skill__c, Level__c, Years_of_Experience__c, Skill_Usage_Description__c from skills__r),' +
                               'Recordtype.Name,Account.Name, Account.Id,Account.Siebel_ID__c, Closedate,Start_Date__c,' + 
                               'Req_Product__c,Req_Client_Job_Title__r.Name,Req_Job_Description__c,Req_Onet_SOC_Code__c,Req_Qualification__c, ' +
                               'Req_Bill_Rate_Min__c,Req_Bill_Rate_Max__c,Req_Pay_Rate_Min__c,Req_Pay_Rate_Max__c,Req_Salary_Min__c,' +
                               'Req_Salary_Max__c, Req_Bonus__c, Req_Other_Compensations__c,Req_Total_Comp_Formula__c,Req_Standard_Burden__c,' +
                               'Req_Max_Spread__c,Req_Min_Spread__c,Amount, Req_Open_Positions__c, Req_Total_Positions__c, Req_Worksite_Street__c,' +
                               'Req_Worksite_City__c, Req_Worksite_State__c,Req_Worksite_Country__c,Req_Worksite_Postal_Code__c,' +
                               'Req_Duration__c,Req_Duration_Unit__c,Req_OFCCP_Required__c,Req_Export_Controls_Questionnaire_Reqd__c,' +
                               'Req_Drug_Test_Required__c, Req_Background_Check_Required__c,Req_Compliance__c, Req_Can_Use_Approved_Sub_Vendor__c,' +
                               'Req_Loss_Wash_Reason__c,Req_Red_Zone__c, Req_Government__c, Req_Exclusive__c, Source_System_Id__c,' +
                               'Opportunity_Num__c, Req_Travel__c, Organization_Office__r.Name,Req_Payment_terms__c,Req_Terms_of_engagement__c,' +
                               'Req_Flat_Fee__c,Req_Fee_Percent__c, Req_Min_Book_of_Business__c, Req_Work_Remote__c,' +
                               'Req_Contact_like_in_hires__c,OpCo__c, BusinessUnit__c, Id, Req_Additional_1st_Year_Compensation__c,' +
                               '(select contact.firstname,contact.lastname,contact.Id,Role, IsPrimary from opportunitycontactroles),'+
                               'Req_Total_Compensation__c from opportunity where Id!= null and ' +
                               'SystemModstamp > :dtLastBatchRunTime';
  
         }else {
       
                   QueryAccount = 'select Probability, (select Skill__c, Level__c, Years_of_Experience__c, Skill_Usage_Description__c from skills__r),' +
                                   'Recordtype.Name,Account.Name, Account.Id,Account.Siebel_ID__c, Closedate,Start_Date__c,' + 
                                   'Req_Product__c,Req_Client_Job_Title__r.Name,Req_Job_Description__c,Req_Onet_SOC_Code__c,Req_Qualification__c, ' +
                                   'Req_Bill_Rate_Min__c,Req_Bill_Rate_Max__c,Req_Pay_Rate_Min__c,Req_Pay_Rate_Max__c,Req_Salary_Min__c,' +
                                   'Req_Salary_Max__c, Req_Bonus__c, Req_Other_Compensations__c,Req_Total_Comp_Formula__c,Req_Standard_Burden__c,' +
                                   'Req_Max_Spread__c,Req_Min_Spread__c,Amount, Req_Open_Positions__c, Req_Total_Positions__c, Req_Worksite_Street__c,' +
                                   'Req_Worksite_City__c, Req_Worksite_State__c,Req_Worksite_Country__c,Req_Worksite_Postal_Code__c,' +
                                   'Req_Duration__c,Req_Duration_Unit__c,Req_OFCCP_Required__c,Req_Export_Controls_Questionnaire_Reqd__c,' +
                                   'Req_Drug_Test_Required__c, Req_Background_Check_Required__c,Req_Compliance__c, Req_Can_Use_Approved_Sub_Vendor__c,' +
                                   'Req_Loss_Wash_Reason__c,Req_Red_Zone__c, Req_Government__c, Req_Exclusive__c, Source_System_Id__c,' +
                                   'Opportunity_Num__c, Req_Travel__c, Organization_Office__r.Name,Req_Payment_terms__c,Req_Terms_of_engagement__c,' +
                                   'Req_Flat_Fee__c,Req_Fee_Percent__c, Req_Min_Book_of_Business__c, Req_Work_Remote__c,' +
                                   'Req_Contact_like_in_hires__c,OpCo__c, BusinessUnit__c, Id, Req_Additional_1st_Year_Compensation__c,' +
                                   '(select contact.firstname,contact.lastname,contact.Id,Role, IsPrimary from opportunitycontactroles),'+
                                   'Req_Total_Compensation__c,(select id from Hrxmls__r where Req_HRXML_Field_Updated__c = true) from opportunity where Id!= null and ' +
                                   ' SystemModstamp > :dtLastBatchRunTime';
       
        }
       
     
    }


     global Database.QueryLocator start(Database.BatchableContext BC)  
     {  
       //Create DataSet of Opportunies to Batch
        return Database.getQueryLocator(QueryAccount);
     } 
 
     global void execute(Database.BatchableContext BC, List<sObject> scope)
     {
        List<Log__c> errors = new List<Log__c>(); 
        Set<Id> setOpportunityids = new Set<Id>();
        List<Opportunity> UpdatedOpportunities = new List<Opportunity>();
        Map<string,string> mapStatuses =  new Map<string,string>();
        
        Map<Id,Opportunity> dmlUpdateMap = new Map<Id,Opportunity>();
        List<Opportunity> lstQueriedOpps = (List<Opportunity>)scope;
         List<Req_Hrxml__c> lstHRXML = new List<Req_Hrxml__c>();
     
        try{
             for(Opportunity o: lstQueriedOpps){
                   if (o.Id != null){
                     setOpportunityids.add(o.Id);
                   } 
                    
                 }
                 
              
                  
             if(setOpportunityids.size() > 0)
             {
                 /*
                   HRXMLUtil hr = new HRXMLUtil(setOpportunityids);
                   hr.updateOpportunitiesWithHRXML();
                   */
                   HRXMLUtil hr = new HRXMLUtil(setOpportunityids);
                  hr.getGeoLocationFromAPI();
                  hr.updateOpportunitiesWithHRXML();
             }
     
           }Catch(Exception e){
             //Process exception here and dump to Log__c object
             errors.add(Core_Log.logException(e));
             //database.insert(errors,false);
             isHRXMLJobException = True;
             strErrorMessage = e.getMessage();
        }
     
     }


    global void finish(Database.BatchableContext BC)
     {
          if(!isHRXMLJobException){
              //Update the Custom Setting's LastExecutedTime__c with the New Time Stamp for the last time the job ran
                String strJobName = 'HRXMLLastRunTime';
               //Get the ProcessReqHRXML Custom Setting
                ProcessReqHRXML__c p = ProcessReqHRXML__c.getValues(strJobName);            
                p.LastExecutedTime__c = newBatchRunTime ;
                if(p != null) 
                    update p;
        
         }else{
               //If there is an Exception, send an Email to the Admin
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
                mail.setToAddresses(toAddresses); 
                String strEmailBody;
                String strOppId;               
                mail.setSubject('EXCEPTION - Identify Non Updated Reqs HRXML Batch Job'); 
              
                //Prepare the body of the email
                strEmailBody = 'The scheduled Apex HRXML Batch Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                
                if(HRXMLUtil.opptyIdList.size() > 0)
                 {
                     for(Id oppId : HRXMLUtil.opptyIdList){
                       strOppId += string.valueof(oppId) + '\n';
                     }
                 }
                strEmailBody += strOppId;
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
               
                //Reset the Boolean variable and the Error Message string variable
                isHRXMLJobException= False;
                strErrorMessage = '';
        
        }
     
     
     
     
     }













}