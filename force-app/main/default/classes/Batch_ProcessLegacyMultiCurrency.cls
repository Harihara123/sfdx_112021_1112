//***************************************************************************************************************************************/
//* Name        - Batch_ProcessLegacyMultiCurrency 
//* Description - Batchable Class used to Identify Out Of Sync Currency Opportunities and update the Currency Values 
//                  based on corresponding Order/Submittal Status.
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham                   05/03/2019                Created
//*****************************************************************************************************************************************/
global class Batch_ProcessLegacyMultiCurrency implements Database.Batchable<sObject>, Database.stateful
{
   //Declare Variables
    global String QueryAccount;    
    global Integer OutOfSyncOpportunities = 0;
    global Integer NumberOfOpportunityBatches = 0;
    global Integer NumberOfOpportunityErrors = 0;
   
    global Boolean isOpportunityJobException = False;
    global String strErrorMessage = '';
    global DateTime dtLastBatchRunTime;
    List<ID> failedOppIds = new List<ID>();
    Map<Id,String> mapOpenOpportunitiesWithCurrency = new Map<Id,String>();
    Map<Id,map<string,date>> mapClosedOpportunitiesWithCurrencyAndDate = new Map<Id,map<string,date>>();
    Map<Id,Decimal> mapOpportunityWithConversionRate = new Map<Id,Decimal>();
    List<Opportunity> oppList = new List<Opportunity>();

    //Current DateTime stored in a variable
      DateTime newBatchRunTime;
    
    //Constructor
    global Batch_ProcessLegacyMultiCurrency(string currencyQuery)
    {
       QueryAccount = currencyQuery;
    }
    
   
     global database.QueryLocator start(Database.BatchableContext BC)  
     {  
        //Create DataSet of Accounts to Batch
        return Database.getQueryLocator(QueryAccount);
     } 

     global void execute(Database.BatchableContext BC, List<sObject> scope)
     {
        List<Log__c> errors = new List<Log__c>(); 
        Set<Id> setOpportunityids = new Set<Id>();
        List<Opportunity> UpdatedOpportunities = new List<Opportunity>();
        Map<string,string> mapStatuses =  new Map<string,string>();
        Map<Id,Opportunity> nonUpdatedOpportunityMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> dmlUpdateMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> stageUpdateOppMap = new Map<Id,Opportunity>();
        List<Opportunity> lstoppties = (List<Opportunity>)scope;
        
        try{
                 if(lstoppties.size() > 0){
      for(Opportunity op : lstoppties){
               if(op.stageName == 'Draft' || op.stageName == 'Qualified' || op.stageName == 'Presenting' || op.stageName == 'Interviewing')
                        {
                            mapOpenOpportunitiesWithCurrency.put(op.Id,op.Currency__c);
                        }
                        else if(op.stageName == 'Closed Won' || op.stageName == 'Closed Won (Partial)')
                        {
                            map<string,date> mapCurrencyDate = new map<string,date>();
                            date dt = system.today();
                            op.CloseDate = op.CloseDate > dt ?dt: op.CloseDate;
                            mapCurrencyDate.put(op.Currency__c,op.CloseDate);
                            mapClosedOpportunitiesWithCurrencyAndDate.put(op.Id,mapCurrencyDate);
                        } 
          }
            
            if(mapOpenOpportunitiesWithCurrency != null && mapOpenOpportunitiesWithCurrency.size() > 0)
            {
                mapOpportunityWithConversionRate = ConvertCurrencyISOCode.getMapCurrencyTypeCustom(mapOpenOpportunitiesWithCurrency);
            }
            if(mapClosedOpportunitiesWithCurrencyAndDate != null && mapClosedOpportunitiesWithCurrencyAndDate.size() > 0)
            {
                mapOpportunityWithConversionRate = ConvertCurrencyISOCode.getMapDatedCurrencyTypeCustom(mapClosedOpportunitiesWithCurrencyAndDate);
            }
            
            for(Opportunity op : lstoppties)
            {
                decimal conversionRate = 1.0;
                if(mapOpportunityWithConversionRate != null && mapOpportunityWithConversionRate.size() > 0)
                {
                    if(mapOpportunityWithConversionRate.get(op.Id) != null)
                    {
                        conversionRate = mapOpportunityWithConversionRate.get(op.Id);
                    }                   
                }
         integer durationHours = 0;
         String durationUnit = op.Req_Rate_Frequency__c;
        if(durationUnit == '' || durationUnit == null || durationUnit == '--None--')
        {
            durationHours = 0;
        }
        else if(durationUnit == 'Hour(s)' || durationUnit == 'Hourly')
        { 
            durationHours = 1;
        }
        else if(durationUnit == 'Day(s)' || durationUnit == 'Daily')
        {
            durationHours = 8;
        }
        else if(durationUnit == 'Week(s)' || durationUnit == 'Weekly')
        {
            durationHours = 40;
        }
        else if(durationUnit == 'Month(s)' || durationUnit == 'Monthly')
        {
            durationHours = 160;
        }
        else if(durationUnit == 'Year(s)')
        {
            durationHours = 1920;
        }
        
           integer payRateFrequency = durationHours;
                if (payRateFrequency == 0){
                      payRateFrequency = 1;
                }
                decimal opAmount = op.Amount == null? 0 :op.Amount;
                decimal opTotalSpread = op.Req_Total_Spread__c == null? 0 :op.Req_Total_Spread__c;
                decimal opRetainerFee = op.Retainer_Fee__c == null? 0 :op.Retainer_Fee__c;
                decimal opReqSalaryMin = op.Req_Salary_Min__c == null? 0 :op.Req_Salary_Min__c;
                decimal opReqSalaryMax = op.Req_Salary_Max__c == null? 0 :op.Req_Salary_Max__c;
                decimal opReqFlatFee = op.Req_Flat_Fee__c == null? 0 :op.Req_Flat_Fee__c;
                decimal opReqAddYearComp = op.Req_Additional_1st_Year_Compensation__c == null? 0 :op.Req_Additional_1st_Year_Compensation__c;
                decimal opReqBillRateMin = op.Req_Bill_Rate_Min__c == null? 0 :op.Req_Bill_Rate_Min__c;
                decimal opReqBillRateMax = op.Req_Bill_Rate_Max__c == null? 0 :op.Req_Bill_Rate_Max__c;
                decimal opReqPayRateMin = op.Req_Pay_Rate_Min__c == null? 0 :op.Req_Pay_Rate_Min__c;
                decimal opReqPayRateMax = op.Req_Pay_Rate_Max__c == null? 0 :op.Req_Pay_Rate_Max__c;
                decimal opTotalCompMax = op.Total_Compensation_Max__c == null? 0 :op.Total_Compensation_Max__c;
                decimal opTotalCompMin = op.Total_Compensation_Min__c == null? 0 :op.Total_Compensation_Min__c;
                
                 op.Amount_Multi_Currency__c =  conversionRate * opAmount;
                 op.Total_Spread_Multi_Currency__c =  conversionRate * opTotalSpread ;
                 op.Project_Retainer_Fee_Multi_Currency__c = conversionRate * opRetainerFee ;
                 op.Salary_Min_Multi_Currency__c =  conversionRate * opReqSalaryMin ;
                 op.Salary_Max_Multi_Currency__c = conversionRate * opReqSalaryMax ;
                 op.Flat_Fee_Multi_Currency__c = conversionRate * opReqFlatFee ;
                 op.Additional_1st_Year_Comp_Converted__c =  conversionRate * opReqAddYearComp ;
                 op.Bill_Rate_Min_Multi_Currency__c = conversionRate * opReqBillRateMin;
                 op.Bill_Rate_Max_Multi_Currency__c =  conversionRate * opReqBillRateMax ;
                 op.Pay_Rate_Min_Multi_Currency__c = conversionRate * opReqPayRateMin ;
                 op.Pay_Rate_Max_Multi_Currency__c =  conversionRate * opReqPayRateMax ;
                 op.Total_Compensation_Max_Converted__c =  conversionRate * opTotalCompMax;
                 op.Total_Compensation_Min_Converted__c =  conversionRate * opTotalCompMin;
                 
          oppList.add(op);               
        }                
     }
               Database.SaveResult[] updatedResults = Database.Update(oppList,false);
                           if (updatedResults != null){
                                for(Integer i=0;i<updatedResults.size();i++){
                                           if (!updatedResults.get(i).isSuccess()){
                                            // DML operation failed
                                            Database.Error error = updatedResults.get(i).getErrors().get(0);
                                            strErrorMessage += error.getMessage();
                                            failedOppIds.add(dmlUpdateMap.values().get(i).Id);
                                            isOpportunityJobException = True;
                                       }
                                 }
                              }
             
            } // End of Try
            Catch(Exception e){
             // Process exception here and dump to Log__c object
             errors.add(Core_Log.logException(e));
             database.insert(errors,false);
             isOpportunityJobException = True;
             strErrorMessage = e.getMessage();
             System.debug('ERROR MSG '+e.getMessage());
        }
     }
     
     global void finish(Database.BatchableContext BC)
     {
        
               //If there is an Exception, send an Email to the Admin
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
                mail.setToAddresses(toAddresses); 
                String strEmailBody;
                String strOppId;               
                mail.setSubject('EXCEPTION - Identify Non Updated Currency Batch Job'); 
              
                //Prepare the body of the email
                strEmailBody = 'The scheduled Apex Opportunity Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                
                if(failedOppIds.size() > 0)
                 {
                     for(Id oppId : failedOppIds){
                       strOppId += string.valueof(oppId) + '\n';
                     }
                 }
                strEmailBody += strOppId;
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
               
                //Reset the Boolean variable and the Error Message string variable
                isOpportunityJobException = False;
                strErrorMessage = '';
     }

}