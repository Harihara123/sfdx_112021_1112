//***************************************************************************************************************************************/
//* Name        - Batch_ProcessOpportunityRecords 
//* Description - Batchable Class used to Identify Out Of Sync Opportunities and update the Stage 
//                  based on corresponding Order/Submittal Status.
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham                   11/09/2016                Created
//*****************************************************************************************************************************************/
global class Batch_ProcessOpportunityRecords implements Database.Batchable<sObject>, Database.stateful
{
   //Declare Variables
    global String QueryAccount;    
    global Integer OutOfSyncOpportunities = 0;
    global Integer NumberOfOpportunityBatches = 0;
    global Integer NumberOfOpportunityErrors = 0;
   
    global Boolean isOpportunityJobException = False;
    global String strErrorMessage = '';
    global DateTime dtLastBatchRunTime;
    List<ID> failedOppIds = new List<ID>();

    //Current DateTime stored in a variable
      DateTime newBatchRunTime;
    
    //Constructor
    global Batch_ProcessOpportunityRecords()
    {
       Datetime latestBatchRunTime = System.now();
       String strJobName = 'SyncOpportunityLastRunTime';
       //Get the ProcessOpportunityRecords Custom Setting
       ProcessOpportunityRecords__c p = ProcessOpportunityRecords__c.getValues(strJobName);
         
       //Store the Last time the Batch was Run in a variable
       dtLastBatchRunTime = p.LastExecutedTime__c;
       
       newBatchRunTime = latestBatchRunTime;
       String strCustomSettingName =  'ProcessOppRecordsQuery';
       //Get the ProcessOpportunityRecords Custom Setting
       ApexCommonSettings__c s = ApexCommonSettings__c.getValues(strCustomSettingName);
       QueryAccount = s.SoqlQuery__c;
    }
    
   

     global database.QueryLocator start(Database.BatchableContext BC)  
     {  
        //Create DataSet of Accounts to Batch
        return Database.getQueryLocator(QueryAccount);
     } 

     global void execute(Database.BatchableContext BC, List<sObject> scope)
     {
        List<Log__c> errors = new List<Log__c>(); 
        Set<Id> setOpportunityids = new Set<Id>();
        List<Opportunity> UpdatedOpportunities = new List<Opportunity>();
        Map<string,string> mapStatuses =  new Map<string,string>();
        Map<Id,Opportunity> nonUpdatedOpportunityMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> dmlUpdateMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> stageUpdateOppMap = new Map<Id,Opportunity>();
        List<Order> lstQueriedOrders = (List<Order>)scope;
        
        try{
             for(Order o: lstQueriedOrders){
                   if (o.OpportunityId != null){
                     setOpportunityids.add(o.opportunityId);
                   } 
                  }
                  
             if(setOpportunityids.size() > 0){  
              Map<Id,Order_Status__mdt> orderStatuses = new Map<Id,Order_Status__mdt>([SELECT OrderStatusKey__c, OpportunityTotalStatus__c from Order_Status__mdt]); 
              List<Order_Status__mdt> listOrderStatuses = orderStatuses.values();
              //Construct a Dynamic Soql from Custom Metadata
              String Soql = '';
              for(Order_Status__mdt key : listOrderStatuses)
               {
                  mapStatuses.put(String.valueof(key.OrderStatusKey__c), String.valueof(key.OpportunityTotalStatus__c));
                   if(string.valueOf(key.OpportunityTotalStatus__c) != 'Req_Sub_Not_Proceeding__c'){
                        Soql += ',' + String.valueof(key.OpportunityTotalStatus__c);
                   }
               }
               
               String Query = 'Select Id,StageName,OpCo__c,Req_origination_System_Id__c,Req_Division__c,Req_Prev_Stage_Name_c__c,Legacy_Record_Type__c,Req_Total_Positions__c,Req_Total_Washed__c,Req_Total_Filled__c,Req_Total_Lost__c,Req_Sub_Not_Proceeding__c' + Soql + ' from Opportunity where Id in ';
               
               List<Opportunity> opportunityList = (List<Opportunity>)Database.query(Query + ':setOpportunityids');
               
               for(opportunity o: opportunityList)
               {
                 nonUpdatedOpportunityMap.put(o.Id, o);
               }
               
                List<aggregateResult> results = [select Status, Order.OpportunityId, COUNT(Order.Opportunity.Name) from Order 
                                                  where Order.OpportunityId IN :setOpportunityids
                                                  AND Recordtype.DeveloperName='OpportunitySubmission'
                                                  group by Order.OpportunityId, Status
                                                  having COUNT(Order.Opportunity.Name) > 0];
                                                  
               if(results.size() > 0){
                Decimal npval;                   
                Map<Id, Opportunity> addedOpportunity = new Map<Id, Opportunity>();
                for (AggregateResult ar : results)
                {  
                  Opportunity op; 
                  ID oppId = (ID)ar.get('OpportunityId');
                  op = nonUpdatedOpportunityMap.get(oppId);
                  //Clear Previous Totals 
                    if(!addedOpportunity.ContainsKey(op.Id)){
                     for(Order_Status__mdt key : listOrderStatuses)
                       { 
                         op.put(key.OpportunityTotalStatus__c,null);
                         npval = 0;
                       }
                   }
                   
                   if(mapStatuses.ContainsKey(string.valueof(ar.get('Status'))))
                     {
                        string oppurtunityKey = string.valueof(mapStatuses.get(string.valueof(ar.get('Status'))));
                       
                         if(oppurtunityKey =='Req_Sub_Not_Proceeding__c'){
                             npval = npval + (Decimal)ar.get('expr0');
                             op.put('Req_Sub_Not_Proceeding__c',npval);
                         }
                        if(oppurtunityKey !='Req_Sub_Not_Proceeding__c'){                         
                            op.put(oppurtunityKey,(Decimal)ar.get('expr0'));
                        }
                        //Auto Req Progression Logic
                        op.put('Req_Total_Filled__c',(  ((op.Req_Sub_Offer_Accepted__c==null) ? 0 : op.Req_Sub_Offer_Accepted__c) 
                                                      + ((op.Req_Sub_Started__c==null) ? 0 : op.Req_Sub_Started__c)) 
                                                    );

                         if(op != null && (op.OpCo__c =='TEKsystems, Inc.' || op.OpCo__c =='Aerotek, Inc') && 
                                 (op.StageName =='Closed Won' || op.StageName == 'Closed Won (Partial)') &&
                                 (!String.isBlank(op.Req_origination_System_Id__c) && op.Req_origination_System_Id__c.length() > 2 &&
                                  !op.Req_origination_System_Id__c.startsWith('006') )){
                                       
                               op.put('StageName',op.StageName);
                                       
                         }else if(op != null && op.StageName <> 'Closed Lost' && op.StageName <> 'Closed Wash' && op.StageName <> 'Closed Won (Partial)' && op.StageName <> 'Closed Won'){
                           if(op.OpCo__c=='TEKsystems, Inc.' && (op.Req_Division__c=='Global Services' || op.Legacy_Record_Type__c=='Global Services Req')){ 
                                if(op != null && op.Req_Total_Filled__c >= op.Req_Total_Positions__c && op.Req_Total_Positions__c > 0){
                                    op.put('StageName','Closed Won');
                                }                                
                               else{
                                   if(op <> null && op.StageName == 'Draft'){
                                  op.put('StageName','Draft');  
                                }
                               }
                            }
                           else{ 
                                if(op != null && op.Req_Total_Filled__c >= op.Req_Total_Positions__c && op.Req_Total_Positions__c > 0){
                                    op.put('StageName','Closed Won');
                                }                               
                               else if(op != null && Op.stagename!='Draft'&& ( (op.Req_Sub_Interviewing__c >= op.Req_Total_Positions__c) || ((op.Req_Sub_Interviewing__c != null ? op.Req_Sub_Interviewing__c : 0)+ 
                                                                            (op.Req_Sub_Offer_Extended__c != null ? op.Req_Sub_Offer_Extended__c : 0)+(op.Req_Sub_Offer_Accepted__c != null ? op.Req_Sub_Offer_Accepted__c : 0)+(op.Req_Sub_Started__c != null ? op.Req_Sub_Started__c : 0)+
                                                                            (op.Req_Sub_LPQ_in_Process__c != null ? op.Req_Sub_LPQ_in_Process__c : 0)+(op.Req_Sub_Referencing__c != null ? op.Req_Sub_Referencing__c : 0)) >= op.Req_Total_Positions__c)){ 
                                    op.put('StageName','Interviewing');                            
                                }
                               else if(op != null && Op.stagename!='Draft'&& ((op.Req_Sub_Submitted__c >= op.Req_Total_Positions__c)||
                                                                ((op.Req_Sub_Submitted__c != null ? op.Req_Sub_Submitted__c : 0)+(op.Req_Sub_Interviewing__c != null ? op.Req_Sub_Interviewing__c : 0)+
                                                                            (op.Req_Sub_Offer_Accepted__c != null ? op.Req_Sub_Offer_Accepted__c : 0)+(op.Req_Sub_Started__c != null ? op.Req_Sub_Started__c : 0)+
                                                                            (op.Req_Sub_Interview_Requested__c != null ? op.Req_Sub_Interview_Requested__c : 0)+(op.Req_Sub_LPQ_in_Process__c != null ? op.Req_Sub_LPQ_in_Process__c : 0)+
                                                                            (op.Req_Sub_Referencing__c != null ? op.Req_Sub_Referencing__c : 0)+(op.Req_Sub_On_Hold__c != null ? op.Req_Sub_On_Hold__c : 0)) >= op.Req_Total_Positions__c) ){
                                    op.put('StageName','Presenting');
                                }
                               else{
                                if(op <> null && op.stagename=='Draft' || (op.Req_Prev_Stage_Name_c__c == 'Draft' && op.StageName != 'Qualified')){
                                  op.put('StageName','Draft');  
                                }
                                else{
                                   op.put('StageName','Qualified');  
                                }
                            }
                         }                             
                        }
                     }

                   if(op != null & op.Id != null){
                             UpdatedOpportunities.add(op);
                             addedOpportunity.put(op.Id,op);
                     } 
              }                     
            System.debug('ERROR OPPS'+UpdatedOpportunities.size());  
             if(UpdatedOpportunities.size() > 0){ 
                    for(Opportunity o: UpdatedOpportunities)
                     {
                         if(!dmlUpdateMap.containsKey(o.Id))
                                dmlUpdateMap.put(o.Id, o);
                     }
                     
                   if(dmlUpdateMap.size() > 0){
                       for(Opportunity opp: dmlUpdateMap.values()){
                        if(opp!= null & opp.Id != null && (opp.OpCo__c == 'Aerotek, Inc' || opp.OpCo__c == 'TEKsystems, Inc.')){
                         Integer totalPosns =  opp.Req_Total_Positions__c==null ? 0 : opp.Req_Total_Positions__c.intValue();
                         Integer totalOA = opp.Req_Sub_Offer_Accepted__c==null ? 0 : opp.Req_Sub_Offer_Accepted__c.intValue();
                         Integer totalStarts = opp.Req_Sub_Started__c==null ? 0 : opp.Req_Sub_Started__c.intValue();
                         Integer totalFilled =  totalOA + totalStarts;
                         Integer totalLoss = opp.Req_Total_Lost__c==null ? 0 : opp.Req_Total_Lost__c.intValue();
                         Integer totalWash = opp.Req_Total_Washed__c ==null ? 0 : opp.Req_Total_Washed__c.intValue(); 
                         Integer totalLostWash = totalLoss + totalWash;
                         System.debug('ERROR OP MAIN'+ totalPosns); 
                         if(totalPosns < totalFilled){
                            Integer totalPns = totalFilled + totalLostWash;
                            opp.put('Req_Total_Positions__c',totalPns);
                         }
                         stageUpdateOppMap.put(opp.Id,opp);
                      }else{
                         stageUpdateOppMap.put(opp.Id,opp);
                      }
                    }
                   }
                        
                     Database.SaveResult[] updatedResults = Database.Update(stageUpdateOppMap.values(),false);
                           if (updatedResults != null){
                                for(Integer i=0;i<updatedResults.size();i++){
                                           if (!updatedResults.get(i).isSuccess()){
                                            // DML operation failed
                                            Database.Error error = updatedResults.get(i).getErrors().get(0);
                                            strErrorMessage += error.getMessage();
                                            failedOppIds.add(dmlUpdateMap.values().get(i).Id);
                                            isOpportunityJobException = True;
                                       }
                                 }
                              }
                   }
                }
              }
            }Catch(Exception e){
             // Process exception here and dump to Log__c object
             errors.add(Core_Log.logException(e));
             database.insert(errors,false);
             isOpportunityJobException = True;
             strErrorMessage = e.getMessage();
             System.debug('ERROR MSG '+e.getMessage());
        }
     }
     
     global void finish(Database.BatchableContext BC)
     {
        if(!isOpportunityJobException){
              //Update the Custom Setting's LastExecutedTime__c with the New Time Stamp for the last time the job ran
                ProcessOpportunityRecords__c orSetting = ProcessOpportunityRecords__c.getValues('SyncOpportunityLastRunTime');                  
                orSetting.LastExecutedTime__c = newBatchRunTime ;
                if(orSetting != null) 
                    update orSetting;
        
         }else{
               //If there is an Exception, send an Email to the Admin
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
                mail.setToAddresses(toAddresses); 
                String strEmailBody;
                String strOppId;               
                mail.setSubject('EXCEPTION - Identify Out Of Sync Opps Batch Job'); 
              
                //Prepare the body of the email
                strEmailBody = 'The scheduled Apex Opportunity Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                
                if(failedOppIds.size() > 0)
                 {
                     for(Id oppId : failedOppIds){
                       strOppId += string.valueof(oppId) + '\n';
                     }
                 }
                strEmailBody += strOppId;
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
               
                //Reset the Boolean variable and the Error Message string variable
                isOpportunityJobException = False;
                strErrorMessage = '';
        
        }

     }

}