/***************************************************************************************************************************************
* Name        - Batch_RWSCandidateMatch
* Description - This class contains the logic to identify the list of Contacts that have been created/updated (matching criteria fields  
*               only and 'update' them to send outbound message for every contact to TIBCO 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       3/16/2013             Created
*****************************************************************************************************************************************/

 global class Batch_RWSCandidateMatch implements Database.Batchable<Sobject>, Database.Stateful{
        
        global String QueryContact;        
        global Boolean isJobException = False;    
        global String strErrorMessage = '';
        // Declare custom exceptions that can be thrown.
        global class customException extends Exception {}
               
        //Declare and initialise a string variable with the job name
        String strJobName = 'RWSCandidateMatchJob';
         
        //Get the ProcessIntegrationResponseRecords Custom Setting Record
        RWSCandidateMatchJobLastRunTime__c mc = RWSCandidateMatchJobLastRunTime__c.getValues(strJobName);
         
        //Store the Last time the Batch was Run in a variable
        DateTime dtLastBatchRunTime = mc.LastJobRunTime__c;       
                                       
        //constructor
        global Batch_RWSCandidateMatch(String s){
                    
            System.debug('*****'+dtLastBatchRunTime);      
                        
            //Construct the Query to get the Data records for the Batch Processing        
            s = s+' WHERE recordtypeid in (\'012U0000000DWoyIAG\',\'012U0000000DWoxIAG\',\'012U0000000DWozIAG\') And (CreatedDate > :dtLastBatchRunTime OR MatchingCriteriaChange__c > :dtLastBatchRunTime)';
            system.debug('************'+s);
        
            //Assign the Query to a string variable
            QueryContact = s;     
        
        }
                    
        //Method to get the data to be proceesed  
        global database.Querylocator start(Database.BatchableContext context){
                    
            return Database.getQueryLocator(QueryContact);          
        }
            
         
        //Method to execute the batch
        global void execute(Database.BatchableContext context , Sobject[] scope){
            
            system.debug(scope);
            List<Log__c> errors = new List<Log__c>();
            try{            
                   List<Contact> ContactsTobeUpdated = new List<Contact>();              
                   for(Sobject s : scope){ 
                       Contact ct = (Contact)s;
                       system.debug(ct);
                       
                       //Check if the criteria is fulfilled - RWS Candidate URL field is blank & LastName is present and (Phone or HomePhone or MobilePhone or OtherPhone is not null) 
                       //OR (Email or Other_Email__c is not null)                       
                       if( (ct.RWS_Candidate_URL__c == Null && ct.LastName != Null) && ((ct.Phone != Null || ct.HomePhone != Null || ct.MobilePhone != Null || ct.OtherPhone != Null) || (ct.Email != Null || ct.Other_Email__c != Null)))
                       {
                             ContactsTobeUpdated.Add(ct); 
                       }                                                            
                   }
                   
                   system.debug('ContactsToBeUpdated' + ContactsToBeUpdated);
                   //Update Contacts and Send Outbound Message for every contact
                   update ContactsTobeUpdated;      
                   
                   
                   //If this Batch Job is executed from a Test, throw an exception to cover the 'Catch' lines of code
                   if(test.isRunningTest()){
                         throw new customException('Test Class Exception!');                      
                   }                               
            
            }
            catch (Exception e) 
            {
                   //Process exception here & dump to Log__c object
                   errors.add(Core_Log.logException(e));
                   database.insert(errors,false);
                   
                   //Set Exception Boolean Variable to True            
                   isJobException = True;                        
                   //String variable to store the Error Message            
                   strErrorMessage = e.getMessage();
            }  
            
        }
         
         //Method to be called after the excute
         global void finish(Database.BatchableContext context){
             
             //Update the custom setting only if there was no Exception         
             if(!isJobException)
             {                                  
                 //Update the Custom Setting's LastJobRunTime with the New Time Stamp for the last time the job ran
                 RWSCandidateMatchJobLastRunTime__c CustomSetting1 = RWSCandidateMatchJobLastRunTime__c.getValues('RWSCandidateMatchJob');                  
                 CustomSetting1.LastJobRunTime__c = system.now();
                 System.debug('New Batch Job Run Time :'+ CustomSetting1.LastJobRunTime__c);
                 if(CustomSetting1 != null)   //Added a null check 
                    update CustomSetting1;
             }
             else
             {
                 //If there is an Exception, send an Email to the Admin team member
                 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                 String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  
                 mail.setToAddresses(toAddresses);                 
                 mail.setSubject('ATTENTION - Salesforce to RWS - Candidate Match Batch Job Failure');                                  
                 mail.setPlainTextBody('The scheduled Apex Job to send recently updated/created Contacts to RWS for Candidate match has failed to process. There was an exception during execution so kindly investigate and fix this issue.\n'+ strErrorMessage);
                 
                 //Send the Email
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                          
             }                   
       }
}