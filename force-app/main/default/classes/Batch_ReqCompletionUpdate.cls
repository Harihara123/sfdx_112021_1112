global class Batch_ReqCompletionUpdate implements Database.Batchable<sObject>, Database.stateful{
    
 
   global String qualifierStr1='' ;
   
    
    
global database.QueryLocator start(Database.BatchableContext BC){  
    
  Map<String, Schema.SObjectField> fieldMap = Opportunity.sObjectType.getDescribe().fields.getMap();  
    
  Set<String> fieldNames = fieldMap.keySet();
 
  List<String> fieldNamesList=new List<String>();
  fieldNamesList.addAll(fieldNames);
    
  String query='select ' + string.join(fieldNamesList, ',') + ' from Opportunity '+qualifierStr1;
    
  System.debug('Final Query is ---->'+query);   
    
  Database.QueryLocator q= Database.getQueryLocator(query);
   
  return q;
} 
    
global void execute(Database.BatchableContext BC, List<sObject> scope){
    
    System.debug('Size of scope list--->'+scope.size());
    
    List<opportunity> lstQueriedopp = (List<opportunity>)scope;
    try{
              Opportunity_UpdateReqCompletionScoreValue(lstQueriedopp);
     }Catch(Exception e){
             System.debug('Stack Trace--->'+e.getStackTraceString());
     }
    
    
}

global void finish(Database.BatchableContext BC){
}    
    

  private void Opportunity_UpdateReqCompletionScoreValue(List<Opportunity> newOpps)
   {
                
                                
                
        for(Opportunity opp : newOpps)
        {
            boolean redFlag=false;
            boolean yellowFlag=false;
            boolean greenFlag=false;
            
            
            String myString =opp.EnterpriseReqSkills__c;
          
            if(
                 !string.isBlank(opp.AccountId) && !string.isBlank(opp.Req_Hiring_Manager__c) && !string.isBlank(opp.StageName) && !string.isBlank(opp.Name)
                                                                && !string.isBlank(opp.Req_Product__c) && opp.Req_Total_Positions__c>0 && !string.isBlank(opp.Currency__c) &&
                                                                !string.isBlank(opp.Req_Job_Title__c) && !string.isBlank(opp.Req_Terms_of_engagement__c)  && !string.isBlank(opp.Req_Worksite_Street__c) 
                                                                  && !string.isBlank(opp.Req_Worksite_City__c) && !string.isBlank(opp.Req_Worksite_Postal_Code__c) && !string.isBlank(opp.Req_Worksite_Country__c)
                                                                  && !string.isBlank(opp.OpCo__c) && !string.isBlank(opp.Req_Division__c) ){
                                                                                opp.ReqCompletition__c = 'Red';
                                                                                redFlag=true;   
               }
         
            
            boolean detFlag=this.determineRateLabor(opp);
           
            
            if(
                redFlag && (!String.isBlank(myString) && myString.length()>2) || (!string.isBlank(opp.Req_Job_Description__c) ) || detFlag  || (!string.isBlank(opp.Req_Qualification__c)) 
              
              )
                {
                    opp.ReqCompletition__c = 'Yellow';
                    yellowFlag=true;
                }
            
            
            
                if ( redFlag && (!String.isBlank(myString) && myString.length()>2) && ( !string.isBlank(opp.Req_Job_Description__c) ) && (!string.isBlank(opp.Req_Qualification__c)) && detFlag && (!String.isBlank(opp.Req_Job_Level__c) || !string.isBlank(opp.Req_Minimum_Education_Required__c) || !string.isBlank(string.valueof(opp.Start_Date__c)))
                                                                  
				)
                {
                    opp.ReqCompletition__c = 'Green';
                }
                
            System.debug('opp.ReqCompletition__c--3--->'+opp.ReqCompletition__c);
            
        }
        
       Database.SaveResult[]  srList= Database.update(newOpps, false);
       
       for (Database.SaveResult sr : srList) {
               if (sr.isSuccess()) {
                  System.debug('Successfully updated opportunity: ' + sr.getId());
               } else {
                  for(Database.Error objErr : sr.getErrors()) {
                     System.debug(objErr.getStatusCode() + ': ' + objErr.getMessage());
                     System.debug('Error while updating opportunity:' 
                        + objErr.getFields());
                  }
               } 
       }      
        
        
    }
    
    
    private boolean determineRateLabor(Opportunity opp){
        
        System.debug('Inside determineRateLabor');
        
        System.debug('opp.OpCo__c------>'+opp.OpCo__c);
        
        if(opp.OpCo__c=='Allegis Partners, LLC' || opp.OpCo__c=='Allegis Global Solutions, Inc.' || opp.OpCo__c=='Major, Lindsey & Africa, LLC'){
            
            System.debug('opp.Req_Bill_Rate_Min__c--->'+!string.isBlank(string.valueof(opp.Req_Bill_Rate_Min__c)));
            
            if ((opp.Req_Product__c == 'Contract' && !string.isBlank(string.valueof(opp.Req_Duration__c)) && !string.isBlank(opp.Req_Duration_Unit__c) && !string.isBlank(opp.Req_Rate_Frequency__c) && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Min__c)) && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Pay_Rate_Min__c)) && !string.isBlank(string.valueof(opp.Req_Pay_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Standard_Burden__c))) 
                || 
                (opp.Req_Product__c == 'Permanent' && !string.isBlank(string.valueof(opp.Req_Salary_Min__c)) && !string.isBlank(string.valueof(opp.Req_Salary_Max__c)) && (!string.isBlank(string.valueof(opp.Req_Flat_Fee__c)) || !string.isBlank(string.valueof(opp.Req_Fee_Percent__c)))) 
                ||
                   (opp.Req_Product__c == 'Contract to Hire' && !string.isBlank(string.valueof(opp.Req_Duration__c)) && !string.isBlank(opp.Req_Duration_Unit__c) && !string.isBlank(opp.Req_Rate_Frequency__c)  && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Min__c)) && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Pay_Rate_Min__c)) && !string.isBlank(string.valueof(opp.Req_Pay_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Standard_Burden__c)) && !string.isBlank(string.valueof(opp.Req_Salary_Min__c)) && !string.isBlank(string.valueof(opp.Req_Salary_Max__c)) && (!string.isBlank(string.valueof(opp.Req_Flat_Fee__c)) || !string.isBlank(string.valueof(opp.Req_Fee_Percent__c))))) {
                       return true;
               }
            
            
        }else if(opp.OpCo__c=='Aerotek, Inc' || opp.OpCo__c=='TEKsystems, Inc.' || opp.OpCo__c=='AG_EMEA'){
            
               if ((opp.Req_Product__c == 'Contract' && !string.isBlank(string.valueof(opp.Req_Duration__c)) && !string.isBlank(opp.Req_Duration_Unit__c) && !string.isBlank(opp.Req_Rate_Frequency__c) 
                    && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Min__c)) && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Pay_Rate_Min__c)) && 
                    !string.isBlank(string.valueof(opp.Req_Pay_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Standard_Burden__c))) 
                || 
                (opp.Req_Product__c == 'Permanent' && !string.isBlank(string.valueof(opp.Req_Salary_Min__c)) && !string.isBlank(string.valueof(opp.Req_Salary_Max__c)) && 
                 (!string.isBlank(string.valueof(opp.Req_Flat_Fee__c)) || !string.isBlank(string.valueof(opp.Req_Fee_Percent__c)))) 
                ||
                   (opp.Req_Product__c == 'Contract to Hire' && !string.isBlank(string.valueof(opp.Req_Duration__c)) && !string.isBlank(opp.Req_Duration_Unit__c) && !string.isBlank(opp.Req_Rate_Frequency__c) 
                    && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Min__c)) && !string.isBlank(string.valueof(opp.Req_Bill_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Pay_Rate_Min__c)) && 
                    !string.isBlank(string.valueof(opp.Req_Pay_Rate_Max__c)) && !string.isBlank(string.valueof(opp.Req_Standard_Burden__c)))) {
                       return true;
               }
        }else{
            return false;
        }
        
       return false;
    }    
    
    

}