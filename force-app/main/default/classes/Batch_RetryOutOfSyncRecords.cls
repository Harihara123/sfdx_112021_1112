/***************************************************************************************************************************************
* Name        - Batch_RetryOutOfSyncRecords  
* Description - This class holds the logic for Retrying out of sync records 
                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       11/15/2012             Created
* Pratz                       1/30/2012              Updated to Fix Defect #27657  
* Pratz                       1/30/2013              Converted to Batch Apex
* Pratz                        1/31/2013             Modified to display the Account/Req/Retry record update errors in the  email notification                                                                                               
*****************************************************************************************************************************************/

global class Batch_RetryOutOfSyncRecords implements Database.Batchable<sObject>, Database.stateful
{

       //Declare Variables
        String QueryRetry;
        String strRetryStatus = 'Open';
        global Integer retrySuccessRecordCount= 0;
        global Integer retryFailedRecordCount = 0;
        global Integer retryUpdateSucessfully = 0;
        global Integer retryUpdateFailed = 0; 
        /***********************************************************************/
        global Set<String> setAccountUpdateErrorMessage = new Set<String>();
        global Set<String> setReqUpdateErrorMessage = new Set<String>();
        global Set<String>  setRetryUpdateErrorMessage = new Set<String>();
        /***********************************************************************/

    
        //Constructor
        global Batch_RetryOutOfSyncRecords()
        {
            //Query to find all Retry__c object records which have Status__c = 'Open'
            QueryRetry = 'SELECT Id, Id__c, Status__c, Object__c, Retry_Timestamp__c  FROM Retry__c WHERE Status__c =: strRetryStatus';
        }
        
        global Iterable<sObject> start(Database.BatchableContext BC)  
        {  
            //Create DataSet of Retry__c to Batch
            return Database.getQueryLocator(QueryRetry);
        } 
        
        
         global void execute(Database.BatchableContext BC, List<sObject> scope)
        {
             /*********************Method for Retry Logic ************************************************/
             //This method holds the logic for retries to be sent for Account Services and Reqs
             /********************************************************************************************/    
             
             Map<Id,Retry__c> RetryMap = new Map<Id, Retry__c>(); 
             List<Account> LSAccountRecordsToBeRetried = new List<Account>();        
             Set<Id> setTempAccId = new Set<Id>();
             Set<Id> setTempReqId = new Set<Id>();
             Set<Id> setSuccessfulUpdateIds = new Set<Id>();
             Set<Id> setFailedUpdateIds = new Set<Id>();
             Set<Id> setToBeUpdatedIds = new Set<Id>();
                                 
             List<Reqs__c> LSReqRecordsToBeRetried = new List<Reqs__c>();
             List<Retry__c> LSRetryRecordsToBeUpdated = new List<Retry__c>();
             List<Log__c> errors = new List<Log__c>();
        
        try{
                    
             //Store the Retry__c records in a map 
             RetryMap = new Map<Id, Retry__c>(((List<Retry__c>)scope));
             system.debug('RetryMap:' + RetryMap);
  
             //Loop through the Retry__c records in the RetryMap
             for (Id id : RetryMap.keySet()) {
                
                //If the Retry__c record is for 'Account' 
                if(RetryMap.get(id).Object__c == 'Account')
                {   
                    //Check if the Object Id has been already added to the the set, 
                    //so that if it is already added, we don't add that Object Id in the LSAccountsToBeRetried List again  
                    if(!setTempAccId.contains(RetryMap.get(id).Id__c))     //01/14/2013[Dhruv] 
                    {
                        //Create an instance of account using the Id__c field of the Retry__c object record
                        Account acc = new Account(Id = RetryMap.get(id).Id__c);
                        
                        //Add the account instance to the list of Account records to be updated
                        LSAccountRecordsToBeRetried.Add(acc);
                        
                        //Add the 'Id' of the Account that will be updated to a Set
                        setToBeUpdatedIds.Add(RetryMap.get(id).Id__c);
                                                
                    }
                                       
                    //Add the Id to the Set, so that it is not added to the List of Account records to be retried again
                    setTempAccId.add(RetryMap.get(id).Id__c);  
                }

                if(RetryMap.get(id).Object__c== 'Req')
                {   
                     //Check if the Object Id has been already added to the the set, 
                    //so that if it is already added, we don't add that Object Id in the LSReqsToBeRetried List again 
                    if(!setTempReqId.contains(RetryMap.get(id).Id__c))
                    {
                        //Create an instance of Req using the Id__c field of the Retry__c object record
                        Reqs__c req = new Reqs__c(Id = RetryMap.get(id).Id__c);
                        
                        //Add the account instance to the list of Req records to be updated
                        LSReqRecordsToBeRetried.Add(req);
                        
                        //Add the 'Id' of the Req that will be updated to a Set
                        setToBeUpdatedIds.Add(RetryMap.get(id).Id__c);                        
                                          
                    }
                                                       
                    //Add the Id to the Set, so that it is not added to the List of req Records to be retried again
                    setTempReqId.add(RetryMap.get(id).Id__c);
                                                                  
                } 
                                
            }
            
            System.debug('******'+LSAccountRecordsToBeRetried);
            System.debug('setToBeUpdatedIds '+setToBeUpdatedIds);           
            
            // Update Account Records 
            List<Database.SaveResult> LSAccountSaveResult = Database.update(LSAccountRecordsToBeRetried, false); 
            System.debug('LSAccountSaveResult: '+LSAccountSaveResult);
                       
            // Iterate through the Save Results 
            for(Database.SaveResult sr: LSAccountSaveResult)
            {
               // fetch all success DML id's in this set
               if(sr.isSuccess())
                  {
                      retrySuccessRecordCount++;
                      setSuccessfulUpdateIds.add(sr.getId());
                  }
               else
                  {
                      /************************************************************************/
                       //Add the Error Message to the String array of Error Messages for failed Account updates
                        setAccountUpdateErrorMessage.add(sr.getErrors()[0].getMessage());
                      /************************************************************************/
 
                      retryFailedRecordCount++;
                  }
            }
            
            System.debug('******'+LSReqRecordsToBeRetried);
            
            // Update Req records
            List<Database.SaveResult> LSReqSaveResult = Database.update(LSReqRecordsToBeRetried, false);
            System.debug('LSReqSaveResult: '+LSReqSaveResult);
            
            
            // Iterate through the Save Results 
            for(Database.SaveResult sr: LSReqSaveResult)
            {
               // fetch all success DML id's in this set
               if(sr.isSuccess())
                  {
                      retrySuccessRecordCount++;
                      setSuccessfulUpdateIds.add(sr.getId());
                  }
               else
                  {
                      /************************************************************************/
                       //Add the Error Message to the String array of Error Messages for failed Req updates
                        setReqUpdateErrorMessage.add(sr.getErrors()[0].getMessage());
                      /************************************************************************/

                      retryFailedRecordCount++;
                                            
                  }
            }
            
            // One set has all id's, other has id's that were successfully updated. 
            // So compare them to get the id's that failed to update
            for(Id i : setToBeUpdatedIds)
            {  
                 //If the record(Account/Req) update failed, then add the 'id' of that record to a set
                 if(!setSuccessfulUpdateIds.contains(i))
                 {
                    setFailedUpdateIds.add(i);                    
                 }  
            }
            
            system.debug('Successfully Updated Records :'+ setSuccessfulUpdateIds);
            system.debug('Failed Update :'+setFailedUpdateIds);
            
            
            //Loop through the Retry Records in the Retry Map and Keep the Status = 'Open' for Accounts/Reqs that failed to update
            for (Id id : RetryMap.keySet()) {
                
                //If the Retry record'd Id__c field in the Map does not mathch with any of the Ids in the set of Ids that failed to update   
                if(!setFailedUpdateIds.contains(RetryMap.get(id).Id__c))
                { 
                
                    if(RetryMap.get(id).Object__c== 'Account')
                    {   
                           //Update the Retry record in the RetryMap
                           Retry__c retry = new Retry__c(Id = RetryMap.get(id).Id, Id__c = RetryMap.get(id).Id__c, Status__c = 'Retry Sent', Object__c = 'Account', Retry_Timestamp__c = System.Now().Format());
                           RetryMap.put(id, retry);
                           
                           //Add the Retry Record to be updated to a list of Retry__c SObjects
                           LSRetryRecordsToBeUpdated.Add(RetryMap.get(id));
                   
                    }
                     if(RetryMap.get(id).Object__c== 'Req')
                    {   
                           //Update the Retry record in the RetryMap
                           Retry__c retry = new Retry__c(Id = RetryMap.get(id).Id, Id__c = RetryMap.get(id).Id__c, Status__c = 'Retry Sent', Object__c = 'Req', Retry_Timestamp__c = System.Now().Format());
                           RetryMap.put(id, retry);
                           
                           //Add the Retry Record to be updated to a list of Retry__c SObjects
                           LSRetryRecordsToBeUpdated.Add(RetryMap.get(id));
                    }
                
                }
                        
            }
            
            System.debug('*******'+LSRetryRecordsToBeUpdated);
            
            // Update Retry Records
            List<Database.SaveResult> LSRetrySaveResult = Database.update(LSRetryRecordsToBeUpdated,false);
            
            // Iterate through the Save Results 
            for(database.saveResult sr : LSRetrySaveResult)     //database update of Reqs
            {          
                if(sr.isSuccess())
                {
                   
                   retryUpdateSucessfully++;
                }
                else
                {  
                   /************************************************************************/
                    //Add the Error Message to the String array of Error Messages for failed Retry updates
                        setRetryUpdateErrorMessage.add(sr.getErrors()[0].getMessage());
                   /************************************************************************/

                   retryUpdateFailed++;
                }
            }
                                           
            system.debug('retrySuccessRecordCount:' + retrySuccessRecordCount );
            system.debug('retryFailedRecordCount:' + retryFailedRecordCount);
            system.debug('retryUpdateSucessfully:' + retryUpdateSucessfully);
            system.debug('retryUpdateFailed:' + retryUpdateFailed);
            
            /************************************************************************/
            system.debug('strAccountUpdateErrorMessage :' + setAccountUpdateErrorMessage);
            system.debug('strReqUpdateErrorMessage :' + setReqUpdateErrorMessage);
            system.debug('strRetryUpdateErrorMessage :' + setRetryUpdateErrorMessage);
            /************************************************************************/

      }

      catch (Exception e)
      {
            // Process exception here
            // Dump to Log__c object
            errors.add(Core_Log.logException(e));
            //insert the error records.              
            database.insert(errors,false);
            
      } 
    }
    
    global void finish(Database.BatchableContext BC)
    {       
        //Send Email with the Batch Job details                
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()]; 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};   
        //String[] toAddresses = new String[] {a.CreatedBy.Email};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Retry of Accounts/Reqs Batch Job Status: ' + a.Status);  
        mail.setPlainTextBody('The batch Apex job processed  ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. \nError Description: ' + a.ExtendedStatus +' \nSuccessfully Retried: '+ retrySuccessRecordCount + '\nRetry Failed: ' + retryFailedRecordCount + '\nRetry Updates: ' + retryUpdateSucessfully + '\nAccount Retry Failure Log : ' + setAccountUpdateErrorMessage+ '\nReq Retry Failure Log : ' + setReqUpdateErrorMessage + '\nRetry Record Update Failure Log :' + setRetryUpdateErrorMessage);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                         
    }
    
 }