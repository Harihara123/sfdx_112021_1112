global class Batch_Retry_Talent_Hrxml implements Database.Batchable<sObject>, Database.stateful {

//Variables

global String QueryTalentHrxmlRetry; 


public Boolean isRetryJobException = False;
global String strErrorMessage = '';
Global List<Id> setTalentIds;
Global List<String> exceptionList = new List<String>();
Global Boolean  isBatchRetryIncludedFailures = false; 
Global List<Talent_Retry__c> lstQueriedTalents = new List<Talent_Retry__c>();
Global List<ID> successTalentIds = new List<ID>();


    global Batch_Retry_Talent_Hrxml() { 
        QueryTalentHrxmlRetry = string.valueof(label.ATS_ProcessRetryTalentHrxml); //s.SoqlQuery__c;
    }


    global database.QueryLocator start(Database.BatchableContext BC) {  
        //Create DataSet of Talent Retry object to Batch
         return Database.getQueryLocator(QueryTalentHrxmlRetry);
    } 

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
            setTalentIds = new List<Id>();
            lstQueriedTalents = (List<Talent_Retry__c>)scope;

             try {
                      for(Talent_Retry__c tr: lstQueriedTalents) {
                           setTalentIds.add(tr.Action_Record_Id__c);
                      }
           
                      if(setTalentIds.size() > 0) {               
                           successTalentIds = HRXMLService.updateHRXML(setTalentIds);
                      }   

                } Catch(Exception e) {
                     System.debug('ERROR'+e.getMessage());
                     exceptionList.add(e.getMessage());
                     if(exceptionList.size()>0)
                         Core_Data.logInsertBatchRecords('Legacy -Ent Runtime',exceptionList);

                     isRetryJobException = True;
                     strErrorMessage = e.getMessage();
               }
      } 


    global void finish(Database.BatchableContext BC) {
   
            try {
                  if(lstQueriedTalents != null && lstQueriedTalents.size() > 0) {

                         Map<String, List<Id>> talentRetryProcessMap = getProccessRetryTalentIds(lstQueriedTalents, successTalentIds);

                         TalentRetryLog.logRetryRun(TalentRetryLog.STATUS_SUCCESS, 
                                                                      talentRetryProcessMap.get(TalentRetryLog.STATUS_SUCCESS));

                         TalentRetryLog.logRetryRun(TalentRetryLog.STATUS_FAILED, 
                                                                      talentRetryProcessMap.get(TalentRetryLog.STATUS_FAILED));  

                         if (talentRetryProcessMap.get(TalentRetryLog.STATUS_FAILED).size() == lstQueriedTalents.size()) {
                               isBatchRetryIncludedFailures = true; 
                         } 

                  }

            } catch (Exception e) {
                System.debug(logginglevel.DEBUG, ' Failed to log Talent Retry Object : ' + e.getStackTraceString());
                isBatchRetryIncludedFailures = true; 

            }


            try {
                    // Send an Email on Exception
                    // Query the AsyncApexJob object to retrieve the current job's information.
                    if (exceptionList.size() > 0 || isBatchRetryIncludedFailures) {
                    
                           String strOppId = ' \r\n';
                           String strOppMessage ='';
                           String strEmailBody = '';
                           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                                           FROM AsyncApexJob WHERE Id = :BC.getJobId()];
                           //Send an email to the Apex job's submitter notifying of job completion.
                           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                           String[] toAddresses = new String[] {'froghani@allegisgroup.com'};
                           mail.setToAddresses(toAddresses);
                           mail.setSubject('Exception : Batch Retry Talent Hrxml');
                           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches that also included failures.';
                           strEmailBody += strOppId;
                           strEmailBody += strOppMessage;              
                           errorText += strEmailBody;                           
                           mail.setPlainTextBody(errorText);
                           
                           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                     
                     } 

             } catch (Exception e) {
               System.debug(logginglevel.DEBUG, ' Failed to Send Email '  + e.getMessage());
             }

             isRetryJobException = False;
             isBatchRetryIncludedFailures = false; 
             strErrorMessage = '';  


             if (exceptionList != null) {
                   exceptionList.clear();
             } else {
                   exceptionList = new List<String>();
             }

             if (lstQueriedTalents != null) {
                 lstQueriedTalents.clear();
             } else {
                 lstQueriedTalents = new List<Talent_Retry__c>();
             }

             if (successTalentIds != null) {
                  successTalentIds.clear();
             } else {
                  successTalentIds = new List<ID>();
             }

             if (setTalentIds != null) {
                  setTalentIds.clear();
             } else {
                  setTalentIds = new List<ID>();
             }
       
     }


    private  Map<String, List<Id>> getProccessRetryTalentIds(List<Talent_Retry__c> queriedTalents, List<ID> successTalents) {

            Map<String, List<Id>> talentRetryProcessMap = new Map<String, List<Id>>();
            list<Id> talentRetryFailed = new List<Id>();
            list<Id> talentRetrySuccess = new List<Id>();

            Set<Id> sucessTalentIdsSet = new Set<Id>();

            if (successTalents != null) {
               sucessTalentIdsSet.addAll(successTalents);
            }

            if (queriedTalents != null) {

                for (Talent_Retry__c tr : queriedTalents) {

                     if (sucessTalentIdsSet.contains(Id.valueOf(tr.Action_Record_Id__c))) { 
                               talentRetrySuccess.add(tr.Id);    
                     } else {
                               talentRetryFailed.add(tr.Id); 
                     }
                }
            }

           talentRetryProcessMap.put(TalentRetryLog.STATUS_SUCCESS, talentRetrySuccess);
           talentRetryProcessMap.put(TalentRetryLog.STATUS_FAILED, talentRetryFailed);

           return talentRetryProcessMap; 
     }


    
}