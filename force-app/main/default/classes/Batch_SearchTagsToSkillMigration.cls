//***************************************************************************************************************************************/
//* Name        - Batch_SearchTagsToSkillMigration
//* Description - Batchable Class used to convert Search tags to Skills
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Preetham Uppu               04/12/2018               Created
//*****************************************************************************************************************************************/
// Execute Anonynous code
// Make sure to run as Batch Integration user
/*
Batch_SearchTagsToSkillMigration b = new Batch_SearchTagsToSkillMigration(); 
b.QueryReq = 'select Id, Name, Req_Search_Tags_Keywords__c, EnterpriseReqSkills__c  from Opportunity where opco__c ='AG_EMEA' and Req_Search_Tags_Keywords__c != null';
Id batchJobId = Database.executeBatch(b);
*/
global class Batch_SearchTagsToSkillMigration implements Database.Batchable<sObject>, Database.stateful
{

   global String QueryReq;
   Global Set<Id> reqIds;
   public Boolean isSkillJobException = False;
   global String strErrorMessage = '';
   Global Set<Id> setOppIds;
   Global Set<Id> setParentReqIds = new Set<Id>();
   Global ReqSyncErrorLogHelper syncErrors = new ReqSyncErrorLogHelper();
   Global List<String> exceptionList = new List<String>();

   
   global Batch_SearchTagsToSkillMigration()
   {
     QueryReq = label.ProcessSearchTagsToSkills;
   }
   
   global database.QueryLocator start(Database.BatchableContext BC)  
    {  
       //Create DataSet of Reqs to Batch
       return Database.getQueryLocator(QueryReq);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Log__c> errors = new List<Log__c>(); 
        setOppIds = new Set<Id>();
        
        List<Opportunity> lstQueriedOpps = (List<Opportunity>)scope;
         try{
              for(Opportunity opp: lstQueriedOpps){
                   if (opp.Id != null){
                     setOppIds.add(opp.Id);
                   } 
                  }
                
                if(setOppIds.size() > 0){
                    SearchTagsToSkillsUtil skUtil = new SearchTagsToSkillsUtil(setOppIds);
                      syncErrors = skUtil.searchTagsToSkills();
                }   
            }Catch(Exception e){
             //Process exception here and dump to Log__c object
              exceptionList.add(e.getMessage());
              if(exceptionList.size() > 0)
                    Core_Data.logInsertBatchRecords('Searchtags-Skills Migration', exceptionList);
              syncErrors.errorList.addall(exceptionList);
             isSkillJobException  = True;
             strErrorMessage = e.getMessage();
        }

    }
     
    global void finish(Database.BatchableContext BC)
     {
        System.debug('SYNC LOG' +syncErrors);
        if (syncErrors.errorList.size() > 0 || syncErrors.errorMap.size() > 0){
           
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                              FROM AsyncApexJob WHERE Id = :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'puppu@allegisgroup.com','Allegis_FO_Support_Admins@allegisgroup.com','hachanta@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Exception : Batch Search To Skills');
           String errorText = 'The batch Apex job processed ' + a.TotalJobItems +' batches with failures. Please Check Log records';        
           mail.setPlainTextBody(errorText);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
         
         }
         
     }

}