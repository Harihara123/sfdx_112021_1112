global class Batch_Set_Account_LatLong implements Database.Batchable<sObject>, Database.stateful
{

       //Declare Variables
        String QueryAccount;

        //Constructor
        global Batch_Set_Account_LatLong()
        {
            //Query to find all Accounts      
            QueryAccount = 'SELECT Id, Name, DandbCompanyId, ShippingLatitude, ShippingCity, BillingCity, ShippingLongitude, DunsNumber FROM Account where DandbCompanyId <> null';

        }
        
        global Iterable<sObject> start(Database.BatchableContext BC)  
        {  
            //Create DataSet of Retry__c to Batch
            return Database.getQueryLocator(QueryAccount);
        } 
        
        
        global void execute(Database.BatchableContext BC, List<sObject> scope)
        {
             /*********************Method for Retry Logic ************************************************/
             //This method holds the logic for retries to be sent for Account Services and Reqs
             /********************************************************************************************/    
             
            set<string> duns = new set<string>(); 
       
            for(sObject s : scope){
            Account a = (Account)s;
               if(a.BillingCity == a.ShippingCity){
               duns.add(a.DunsNumber);
               }
            }
            List<Log__c> errors = new List<Log__c>();// Collection to maintain log record information  
            List<Account> FinalAccountList =new List<Account>();
            List<DandbCompany> lstScopeduns =new List<DandbCompany>(); 
            lstScopeduns = [SELECT Id, Latitude, Longitude, Name, DunsNumber FROM DandBCompany where DunsNumber IN: duns];
            Map<String, DandbCompany > dunsMap = new Map<string , DandbCompany>();
            for(DandbCompany s : lstScopeduns){               
               dunsMap.put(s.DunsNumber, s);
            } 
            
            
            for(sObject s : scope){
               Account a = (Account)s;
           if(a.BillingCity == a.ShippingCity){    
               if(dunsMap.size() > 0){
                if(dunsMap.containsKey(a.DunsNumber)){
                  if(dunsMap.get(string.valueof(a.DunsNumber)).Latitude <> null && dunsMap.get(string.valueof(a.DunsNumber)).Latitude <>'' && dunsMap.get(string.valueof(a.DunsNumber)).Longitude <> null && dunsMap.get(string.valueof(a.DunsNumber)).Longitude <>''){
                     if((a.ShippingLatitude <> decimal.valueof(dunsMap.get(string.valueof(a.DunsNumber)).Latitude))&&(a.ShippingLongitude <> decimal.valueof(dunsMap.get(string.valueof(a.DunsNumber)).Longitude))){  
                       a.ShippingLatitude = decimal.valueof(dunsMap.get(string.valueof(a.DunsNumber)).Latitude);               
                       a.ShippingLongitude = decimal.valueof(dunsMap.get(string.valueof(a.DunsNumber)).Longitude);
                       FinalAccountList.add(a);
                       }
                  }    
                }
               }
             }  
               
            }  
       if(FinalAccountList.size()>0){
       try{
           Upsert(FinalAccountList);
           }catch(Exception e){
                   //Process exception here & dump to Log__c object
                   errors.add(Core_Log.logException(e));
                   database.insert(errors,false);
           }   
       }   
    }
    
    global void finish(Database.BatchableContext BC)
    {       
        //Send Email with the Batch Job details                
/*        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()]; 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};   
        //String[] toAddresses = new String[] {a.CreatedBy.Email};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Batch Update: ' + a.Status);  
        mail.setPlainTextBody('The batch Apex job processed  ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. \nError Description: ' + a.ExtendedStatus +' \nSuccessfully Retried: '+ retrySuccessRecordCount + '\nRetry Failed: ' + retryFailedRecordCount + '\nRetry Updates: ' + retryUpdateSucessfully + '\nAccount Retry Failure Log : ' + setAccountUpdateErrorMessage+ '\nReq Retry Failure Log : ' + setReqUpdateErrorMessage + '\nRetry Record Update Failure Log :' + setRetryUpdateErrorMessage);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                         
*/   }
    
 }