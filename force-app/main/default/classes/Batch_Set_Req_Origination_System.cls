/***************************************************************************************************************************************
* Name        - Batch_Set_Req_Origination_System 
* Description - This class that sets Req_Origination_Syste to Siebel or Salesforce                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Sudheer                    06/04/2013             Created
*****************************************************************************************************************************************/

global class Batch_Set_Req_Origination_System implements Database.Batchable<sObject>, Database.stateful
{

       //Declare Variables
        String QueryReq;

        //Constructor
        global Batch_Set_Req_Origination_System()
        {
            //Query to find all Reqs      
//          QueryReq = 'Select Id, Name, Req_Origination_System__c, Origination_System__c From Reqs__c ALL ROWS';
            QueryReq = 'Select Id, Name, Req_Origination_System__c, Origination_System__c From Reqs__c';

        }
        
        global Iterable<sObject> start(Database.BatchableContext BC)  
        {  
            //Create DataSet of Retry__c to Batch
            return Database.getQueryLocator(QueryReq);
        } 
        
        
        global void execute(Database.BatchableContext BC, List<sObject> scope)
        {
             /*********************Method for Retry Logic ************************************************/
             //This method holds the logic for retries to be sent for Account Services and Reqs
             /********************************************************************************************/    
             
            List<Reqs__c> AllReqMap = new List<Reqs__c>();

            for(sObject s : scope){
                Reqs__c a = (Reqs__c)s;
                if(a.Origination_System__c =='SIEBEL'){
                    a.Req_Origination_System__c='Siebel';
                    AllReqMap.add(a);
                }   
                else if (a.Origination_System__c =='SALESFORCE'){
                    a.Req_Origination_System__c='Salesforce';
                    AllReqMap.add(a);   
                }               
        	}
        	update AllReqMap;
    }
    
    global void finish(Database.BatchableContext BC)
    {       
        //Send Email with the Batch Job details                
/*        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()]; 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};   
        //String[] toAddresses = new String[] {a.CreatedBy.Email};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Batch Update: ' + a.Status);  
        mail.setPlainTextBody('The batch Apex job processed  ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. \nError Description: ' + a.ExtendedStatus +' \nSuccessfully Retried: '+ retrySuccessRecordCount + '\nRetry Failed: ' + retryFailedRecordCount + '\nRetry Updates: ' + retryUpdateSucessfully + '\nAccount Retry Failure Log : ' + setAccountUpdateErrorMessage+ '\nReq Retry Failure Log : ' + setReqUpdateErrorMessage + '\nRetry Record Update Failure Log :' + setRetryUpdateErrorMessage);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                         
*/   }
    
 }