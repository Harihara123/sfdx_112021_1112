@isTest
public class Batch_TalentMergeTest {
    static testmethod void test1(){
        Account ac1 = new Account();
        ac1.Name ='Test Account';
        insert ac1;
        
        Contact con1 = new Contact();
        con1.lastName='Test Contact';
        con1.AccountID =ac1.id;
        insert Con1;
        
        Contact con2 = new Contact();
        con2.lastName='Test Contact';
        con2.AccountID =ac1.id;
        insert Con2;
        
        Talent_Merge__c tm = new Talent_Merge__c();
        tm.status__c ='Not Processed';
        tm.Batch_Number__c =1;
        tm.Master_Id__c =con1.id;
        tm.Duplicate_Id__c =con2.id;
        insert tm;
        
       Database.executeBatch( new Batch_TalentMerge(), 1 );
    }
    static testmethod void test2(){
        Account ac1 = new Account();
        ac1.Name ='Test Account';
        insert ac1;
        
        Contact con1 = new Contact();
        con1.lastName='Test Contact';
        con1.AccountID =ac1.id;
        insert Con1;
        
        Contact con2 = new Contact();
        con2.lastName='Test Contact';
        con2.AccountID =ac1.id;
        insert Con2;
        Talent_Merge__c tm = new Talent_Merge__c();
        tm.status__c ='Not Processed';
        tm.Batch_Number__c =2;
        tm.Master_Id__c =con1.id;
        tm.Duplicate_Id__c =con2.id;
        insert tm;
		         
        Database.executeBatch( new Batch_TalentMerge_02(), 1 );
    }
    static testmethod void test3(){
        Account ac1 = new Account();
        ac1.Name ='Test Account';
        insert ac1;
        
        Contact con1 = new Contact();
        con1.lastName='Test Contact';
        con1.AccountID =ac1.id;
        insert Con1;
        
        Contact con2 = new Contact();
        con2.lastName='Test Contact';
        con2.AccountID =ac1.id;
        insert Con2;
        
        Talent_Merge__c tm = new Talent_Merge__c();
        tm.status__c ='Not Processed';
        tm.Batch_Number__c =3;
        tm.Master_Id__c =con1.id;
        tm.Duplicate_Id__c =con2.id;
        insert tm;
		         
        Database.executeBatch( new Batch_TalentMerge_03(), 1 );
    }
}