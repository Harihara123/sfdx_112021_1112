global class Batch_TalentMerge_03 implements Database.Batchable<sObject>{
    
    global database.Querylocator start(Database.BatchableContext context){
        String strQuery = 'select Id, master_id__c, duplicate_id__c FROM Talent_Merge__C where status__c != \'Success\' AND Batch_Number__c = 3';
        return Database.getQueryLocator(strQuery);          
    }
    
    //Method to execute the batch
    global void execute(Database.BatchableContext context , Sobject[] scope){
        
        try{            
            for(Sobject obj : scope){ 
                
                Talent_Merge__C talent = (Talent_Merge__C)obj;
                
                /* Get the talentId from contact to process through existing merge fucntionality */
                Contact masContact 	= [select Talent_Id__c from Contact where id =: talent.Master_Id__c];
                Contact dupContact 	= [select AccountId, Talent_Id__c from Contact where id =: talent.Duplicate_Id__c];
                
                /* Call existing merge functionality */
                String str = TalentMergeController.talentMerge( masContact.Talent_Id__c, dupContact.Talent_Id__c );
                
                if( str == 'Merge is Successful.'){
                    String log 		 = 'Duplicate Talent : ' + dupContact.Talent_Id__c + ' Merged Successfully Into Master Talent : ' + masContact.Talent_Id__c;
                    talent.log__c 	 = log;
                    talent.status__c = 'Success'; 
                    
                }else{
                    talent.log__c 		= str;
                    talent.status__c 	= 'Error';
                }
              
                update talent;
                
            }
            
        }
        catch (Exception e) {
            
            //errors.add(Core_Log.logException(e));
            //database.insert(errors,false);
            
            String strErrorMessage = e.getMessage();
            //system.debug('############===--->' + strErrorMessage );
        }  
        
    }
    
    //Method to be called after the excute
    global void finish(Database.BatchableContext context){
        
    }
}