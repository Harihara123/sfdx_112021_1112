//***************************************************************************************************************************************/
//* Name        - Batch_UpdateReqEnterpriseSkills 
//* Description - Batchable Class used to Copy data of Skills from related object to EnterpriseReqSkills field
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                    Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni               01/02/2018               Created


/*anonymous code for all opportunities
Batch_UpdateReqEnterpriseSkills objBatch_UpdateReqEnterpriseSkills = new Batch_UpdateReqEnterpriseSkills();
objBatch_UpdateReqEnterpriseSkills.QueryReq = 'select id,Req_Skill_Details__c,EnterpriseReqSkills__c,(select id,Skill__c,Level__c,Competency__c,Years_of_Experience__c,Skill_Usage_Description__c from Skills__r) from opportunity where id in (select Req__c from Req_Skill__c) and recordtype.name=\'Req\'';
Database.executeBatch(objBatch_UpdateReqEnterpriseSkills);
*/

/*anonymous code for single opportunity
Batch_UpdateReqEnterpriseSkills objBatch_UpdateReqEnterpriseSkills = new Batch_UpdateReqEnterpriseSkills();
objBatch_UpdateReqEnterpriseSkills.QueryReq = 'select id,Req_Skill_Details__c,EnterpriseReqSkills__c,(select id,Skill__c,Level__c,Competency__c,Years_of_Experience__c,Skill_Usage_Description__c from Skills__r) from opportunity where id =\'0069E000006TCTP\'';
Database.executeBatch(objBatch_UpdateReqEnterpriseSkills);
*/
//*****************************************************************************************************************************************/
global class Batch_UpdateReqEnterpriseSkills implements Database.Batchable<sObject>, Database.stateful
{
    //Variables
    global String QueryReq; 
    public Boolean isReqJobException = False;
    global String strErrorMessage = '';

    //Constructor
    global Batch_UpdateReqEnterpriseSkills()
    { 
        QueryReq = '';
    }

    global database.QueryLocator start(Database.BatchableContext BC)  
    {  
        //Create DataSet of Reqs to Batch
        return Database.getQueryLocator(QueryReq);
    } 
     
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Log__c> errors = new List<Log__c>(); 
        Set<Id> setReqIds = new Set<Id>();
        List<Opportunity> lstQueriedReqs = (List<Opportunity>)scope;
        List<opportunity> lstOpportunityToUpdate = new List<Opportunity>();
        try
        {
            for(Opportunity r: lstQueriedReqs)
            {
                List<InnerClassSkills> lstInnerClassSkills = new List<InnerClassSkills>();
                string reqSkillsDetails = '';
                if (r.Skills__r != null)
                {
                    for(Req_Skill__c rs : r.Skills__r)
                    {
                        if(reqSkillsDetails == '')
                        {
                            reqSkillsDetails = 'Skill:' +rs.Skill__c + '; Level:' + rs.Level__c  + '; Competency:' + rs.Competency__c  + '; Years_Of_Experience:' + rs.Years_of_Experience__c  + '; How_the_skill_will_be_used:' + rs.Skill_Usage_Description__c;
                        }
                        else
                        {
                            reqSkillsDetails += '\n\n Skill:' +rs.Skill__c + '; Level:' + rs.Level__c  + '; Competency:' + rs.Competency__c  + '; Years_Of_Experience:' + rs.Years_of_Experience__c  + '; How_the_skill_will_be_used:' + rs.Skill_Usage_Description__c;
                        }
                        InnerClassSkills ics = new InnerClassSkills();
                        ics.Name = rs.Skill__c;
                        if(rs.Level__c == 'Optional')
                        {
                            ics.favorite = false;
                        }
                        else
                        {
                            ics.favorite = true;
                        }
                        lstInnerClassSkills.add(ics);
                    }
                } 
                if(r.EnterpriseReqSkills__c != null && r.EnterpriseReqSkills__c != '')
                {
                    List<ReqSkills.Skills> lstCompetencySkills = ReqSkills.getAllParsedSkills(r.EnterpriseReqSkills__c);
                    for(ReqSkills.Skills s : lstCompetencySkills)
                    {
                        InnerClassSkills ics = new InnerClassSkills();
                        ics.Name = s.name;
                        ics.favorite = s.favorite;
                        lstInnerClassSkills.add(ics);
                    }
                }
                if(r.Req_Skill_Details__c != null && r.Req_Skill_Details__c != '')
                {
                    r.Req_Skill_Details__c = r.Req_Skill_Details__c + '\n\n' + reqSkillsDetails;
                }   
                else
                {
                    r.Req_Skill_Details__c = reqSkillsDetails;
                }               
                r.EnterpriseReqSkills__c = JSON.serialize(lstInnerClassSkills);
                r.Apex_Context__c=true;
                lstOpportunityToUpdate.add(r);
            }
            
            if(lstOpportunityToUpdate.size() > 0)
            {
                update lstOpportunityToUpdate;
            }  
        }
        Catch(Exception e){
        // Process exception here and dump to Log__c object
        System.debug('ERROR'+e.getMessage());
        errors.add(Core_Log.logException(e));
        database.insert(errors,false);
        isReqJobException = True;
        strErrorMessage = e.getMessage();
        }

    }
    
    public class InnerClassSkills
    {
        public string name;
        public boolean favorite;
        
        public InnerClassSkills()
        {
        }
        
        public InnerClassSkills(string n, boolean f)
        {
            name = n;
            favorite = f;
        }
        
    }
      
    global void finish(Database.BatchableContext BC)
    {
        List<Log__c> errorsLog = new List<Log__c>(); 
        if(isReqJobException)
        {

            System.debug('EXCEPTION');
            //If Exception, Logs are created
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
            String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
            mail.setToAddresses(toAddresses); 
            String strEmailBody = '';
                       
            mail.setSubject('EXCEPTION - Copy Skills data to EnterpriseReqSkills field'); 

            //Prepare the body of the email
            strEmailBody = 'The scheduled Apex Opportunity Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                

            mail.setPlainTextBody(strEmailBody);   

            //Send the Email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            //Reset the Boolean variable and the Error Message string variable
            isReqJobException = False;
            strErrorMessage = '';
        }
    }

}