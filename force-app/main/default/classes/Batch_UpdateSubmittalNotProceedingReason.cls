/*
Batch_UpdateSubmittalNotProceedingReason b = new Batch_UpdateSubmittalNotProceedingReason(); 
b.QueryReq =  'select Id, Submittal_Not_Proceeding_Reason__c ,Source_System_LastModifiedBy__c, 
               Source_System_LastModifiedDate__c, LastModifiedDate, LastModifiedById
               from Order where status != 'Not Proceeding' and Submittal_Not_Proceeding_Reason__c != null';
Id batchJobId = Database.executeBatch(b);
*/

global class Batch_UpdateSubmittalNotProceedingReason implements Database.Batchable<sObject>{

    global String QueryReq;
    global List<Order> updatedOrders;

    global Batch_UpdateSubmittalNotProceedingReason()
    {
          updatedOrders = new List<Order>();
    }

    global database.QueryLocator start(Database.BatchableContext BC)  
    {  
        //Create DataSet of Reqs to Batch
        return Database.getQueryLocator(QueryReq);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Order> lstOrderToUpdate = new List<Order>();
        lstOrderToUpdate = (List<Order>)scope;
        try{
          if(lstOrderToUpdate.size() > 0){
            for(Order od: lstOrderToUpdate){
                    od.Submittal_Not_Proceeding_Reason__c = '';
                    od.Source_System_LastModifiedDate__c = od.LastModifiedDate;
                    od.Source_System_LastModifiedBy__c = od.LastModifiedById;
                    updatedOrders.add(od);
              } 
            if(!updatedOrders.isEmpty()){
                update updatedOrders;
            }
          }  
        }
        Catch(Exception e)
        {
            system.debug('Exception::' + e.getMessage());
        }

    }

    global void finish(Database.BatchableContext BC)
    {
    }

}