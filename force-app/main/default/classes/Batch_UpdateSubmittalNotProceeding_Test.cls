@isTest
public class Batch_UpdateSubmittalNotProceeding_Test {
    testmethod static void batchUpdate() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        List<Account> accList = new List<Account>();
        
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;

        Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
        ct.Other_Email__c = 'other0@testemail.com';
        ct.Title = 'test title';
        ct.MailingState = 'test text';

        insert ct;

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = accList[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp1;

        Opportunity opp2 = new Opportunity();
        opp2.Name = 'TESTOPP2';
        opp2.StageName = 'Offer Accepted';
        opp2.AccountId = accList[0].id;
        opp2.Req_Product__c = 'Permanent';
        opp2.CloseDate = System.today();
        opp2.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp2;
        
        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;
        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = ct.Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Linked';
        o.EffectiveDate = System.today();
        o.RecordTypeId = recTypeId;
        o.Submittal_Not_Proceeding_Reason__c = 'Filled by Client';
        insert o;

        Order o2 = new Order();
        o2.Name = 'TestOrder';
        o2.AccountId = accList[0].id;
        o2.ShipToContactId = ct.Id;
        o2.OpportunityId = opp2.Id;
        o2.Status = 'Linked';
        o2.EffectiveDate = System.today();
        o2.RecordTypeId = recTypeId;
        o2.Submittal_Not_Proceeding_Reason__c = 'Filled by Client';
        insert o2;
        Batch_UpdateSubmittalNotProceedingReason b = new Batch_UpdateSubmittalNotProceedingReason(); 
        b.QueryReq = 'select Id, Submittal_Not_Proceeding_Reason__c ,Source_System_LastModifiedBy__c, Source_System_LastModifiedDate__c, LastModifiedDate, LastModifiedById from Order where status != \'Not Proceeding\' and Submittal_Not_Proceeding_Reason__c != null';
        Id batchJobId = Database.executeBatch(b);
    }
}