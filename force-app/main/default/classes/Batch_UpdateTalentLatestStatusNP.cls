//anonymous code for single account
/*
Batch_UpdateTalentLatestStatusNP batch = new Batch_UpdateTalentLatestStatusNP ();
batch.QueryAccount = 'SELECT Id, Name, Talent_Latest_Submittal_Status__c FROM Account where recordtype.name = \'Talent\' and id =\'0069E000006TCTP\'';
database.executeBatch(batch);
*/

//anonymous code for all accounts
/*
Batch_UpdateTalentLatestStatusNP batch = new Batch_UpdateTalentLatestStatusNP ();
batch.QueryAccount = 'SELECT Id, Name, Talent_Latest_Submittal_Status__c FROM Account where recordtype.name =\'Talent\'';
database.executeBatch(batch);
*/

global class Batch_UpdateTalentLatestStatusNP implements Database.Batchable<sObject>, Database.stateful
{

       //Declare Variables
    global String QueryAccount ; 

        //Constructor
        global Batch_UpdateTalentLatestStatusNP ()
        {
                    String QueryAccount = '';

        }
        
        global Iterable<sObject> start(Database.BatchableContext BC)  
        {  
            //Create DataSet of Account to Batch
            return Database.getQueryLocator(QueryAccount);
        } 
        
        
        global void execute(Database.BatchableContext BC, List<sObject> scope)
        {
             
            set<Id> setAccountId = new set<Id>(); 
            List<Account> lstAccount = new List<Account>();
            List<Log__c> errors = new List<Log__c>();// Collection to maintain log record information 
            
            for(sObject s : scope)
            {
                Account a = (Account)s;
                setAccountId.add(a.Id);
            }
            
            if(!setAccountId.isEmpty())
            {
                map<id,Order> mapAccountIdWithOrder = new map<id,Order>();
                
                //Query on orders for given account Id's
                List<Order> lstOrder = [select id,status,Display_LastModifiedDate__c,ShipToContact.AccountId from Order where ShipToContact.AccountId in:setAccountId  order by Display_LastModifiedDate__c desc];
                
                for(Order o : lstOrder)
                {
                    //Get orders whose Display_LastModifiedDate__c is latest
                    if(!mapAccountIdWithOrder.containskey(o.ShipToContact.AccountId))
                    {
                        mapAccountIdWithOrder.put(o.ShipToContact.AccountId,o);
                    }
                }
                
                
                if(mapAccountIdWithOrder != null && mapAccountIdWithOrder.values() != null)
                {
                    for(Order o: mapAccountIdWithOrder.values())
                    {
                        Account a = new Account();
                        //Updating Talent_Latest_Submittal_Status__c of Talent Account with the status of submittal 
                        a.id = o.ShipToContact.AccountId;
                        a.Talent_Latest_Submittal_Status__c = o.status;
                        lstAccount.add(a);
                                            
                    }
                    
                    if(!lstAccount.isEmpty())
                    {
                        try
                        {
                            //Updating accounts whose Talent_Latest_Submittal_Status__c are updated
                            update lstAccount;
                        }
                        catch(Exception e)
                        {
                            //Process exception here & dump to Log__c object
                            errors.add(Core_Log.logException(e));
                            database.insert(errors,false);
                        } 
                        
                    }
                //}
            } 
       }   
        }
    
    global void finish(Database.BatchableContext BC)
    {       
        //Send Email with the Batch Job details                
/*        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()]; 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};   
        //String[] toAddresses = new String[] {a.CreatedBy.Email};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Batch Update: ' + a.Status);  
        mail.setPlainTextBody('The batch Apex job processed  ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. \nError Description: ' + a.ExtendedStatus +' \nSuccessfully Retried: '+ retrySuccessRecordCount + '\nRetry Failed: ' + retryFailedRecordCount + '\nRetry Updates: ' + retryUpdateSucessfully + '\nAccount Retry Failure Log : ' + setAccountUpdateErrorMessage+ '\nReq Retry Failure Log : ' + setReqUpdateErrorMessage + '\nRetry Record Update Failure Log :' + setRetryUpdateErrorMessage);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                         
*/   }
    
 }