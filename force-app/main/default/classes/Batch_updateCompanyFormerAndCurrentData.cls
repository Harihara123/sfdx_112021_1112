//***************************************************************************************************************************************/
//* Name        - updateCompanyFormerAndCurrentDataBatch
//* Description - Batch Utility class to update former and current fields of MLA and AP on Account
//                        based on corresponding opCo and Status.
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni          03/24/2017                Created By
//*****************************************************************************************************************************************/
global class Batch_updateCompanyFormerAndCurrentData implements Database.Batchable<sObject>,Database.Stateful
{
    global Map<String,New_Company_Names__mdt> mapNewCompanyNames {get; set;}
    global list<string> fieldnames {get; set;}
    global String query {get;set;}
    global DateTime dtLastBatchRunTime;
     global DateTime dtDiffOfTodayDate175;
     global DateTime dtDiffOfTodayDate185;
    global DateTime newBatchRunTime;
     global String strErrorMessage = '';
    global SET<string> setCompanyName;
    global Boolean isUpdateCompanyJobException = False;
    global datetime Old180DaysDateTime;
    global Map<Id,Account> nonUpdatedAccountMap;
    
    global Batch_updateCompanyFormerAndCurrentData()
    {
        nonUpdatedAccountMap = new Map<Id,Account>();
        newBatchRunTime = System.now();
        Old180DaysDateTime = System.now().addDays(-181);
       
        String strJobName = 'CompanyFormerCurrentLastRunTime';
       //Get the UpdateCompanyFormerAndCurrentDataBatch Custom Setting
       UpdateCompanyFormerAndCurrentDataBatch__c p = UpdateCompanyFormerAndCurrentDataBatch__c.getValues(strJobName);
         
       //Store the Last time the Batch was Run in a variable
       dtLastBatchRunTime = p.LastExecutedTime__c;
       dtDiffOfTodayDate175 = System.Today() - 175;
       dtDiffOfTodayDate185 = System.Today() - 185;
       
       //string query;
        fieldnames = new list<string>();
        mapNewCompanyNames = new Map<String,New_Company_Names__mdt>();
        //query for getting custom metadata type data that has company name and its related former and current field api name
        List<New_Company_Names__mdt> lstNewCompanyNames = [SELECT MasterLabel,Current_Field_API_Name__c,Former_Field_API_Name__c from New_Company_Names__mdt];
        for(New_Company_Names__mdt company : lstNewCompanyNames)
        {
            fieldnames.add(company.Current_Field_API_Name__c);
            fieldnames.add(company.Former_Field_API_Name__c);
            mapNewCompanyNames.put(company.MasterLabel,company);
        }
        setCompanyName = mapNewCompanyNames.keyset();
    }
    
    //start method
    global Database.queryLocator start(Database.BatchableContext BC)
    {
        
        
        //query order records
       // query = 'Select Id,Display_LastModifiedDate__c,AccountId,Opportunity.OpCo__c from Order where Opportunity.OpCo__c in: setCompanyName and status = \'placed\'';
        
              
         return Database.getQueryLocator(query);
    }
    
    //execute method
    global void execute(Database.BatchableContext BC, List<Order> scope)
    {
        set<string> setAccountId = new set<string>();
        List<Log__c> errorLogs = new List<Log__c>();
        if(scope.size() > 0)
        {
            try
            {
                for(Order o : scope)
                {
                    setAccountId.add(o.AccountId);             
                }
             errorLogs = BatchUpdateCompanyDataHelperclass.processaccnts(setAccountId,fieldnames,Old180DaysDateTime,setCompanyName,isUpdateCompanyJobException,nonUpdatedAccountMap,mapNewCompanyNames);
            
             if(errorLogs.size() >0){
             isUpdateCompanyJobException = True;
            }
}
            Catch(Exception e)
            {
                 // Process exception here and dump to Log__c object
                 errorLogs.add(Core_Log.logException(e));
                 database.insert(errorLogs,false);
                 isUpdateCompanyJobException = True;
                 strErrorMessage =  e.getMessage();
            }
        }
    }
    
    //finish method
    global void finish(Database.BatchableContext BC)
    {
        if(!isUpdateCompanyJobException){
              //Update the Custom Setting's LastExecutedTime__c with the New Time Stamp for the last time the job ran
                UpdateCompanyFormerAndCurrentDataBatch__c orSetting = UpdateCompanyFormerAndCurrentDataBatch__c.getValues('CompanyFormerCurrentLastRunTime');                  
                orSetting.LastExecutedTime__c = newBatchRunTime;
                if(orSetting != null) 
                    update orSetting;
        
         }else{
               //If there is an Exception, send an Email to the Admin
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
                mail.setToAddresses(toAddresses); 
                String strEmailBody;
                              
                mail.setSubject('EXCEPTION - Identify Out Of Sync accs Batch Job'); 
              
                //Prepare the body of the email
                strEmailBody = 'The scheduled Apex Account Job failed to process. There was an exception during execution.\n';//+ strErrorMessage;  
                
                if(BatchUpdateCompanyDataHelperclass.errormsgs != '')
                {
                     strEmailBody += BatchUpdateCompanyDataHelperclass.errormsgs;
                }              
               
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
               
                //Reset the Boolean variable and the Error Message string variable
                isUpdateCompanyJobException = False;
                strErrorMessage = '';
        
        }
    }
}