//***************************************************************************************************************************************/
//* Name        - updateAccountFormerAndCurrentDataBatch 
//* Description - Batch Utility class to update former and current fields of MLA and AP on Account
//                        based on corresponding opCo and Status.
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni         03/24/2017                Created

// *********************************************************************
// Execute Anonynous code
// Make sure to run as Account Integration user
/*
Batch_updateHistoricalData  b = new Batch_updateHistoricalData();
b.query = 'select id, ' + String.join(b.fieldnames, ', ') + ' from account where recordtype.name=\'client\'';
Id batchJobId = Database.executeBatch(b);
*/
//*****************************************************************************************************************************************/
global class Batch_updateHistoricalData implements Database.Batchable<sObject>,Database.Stateful
{
    global Map<String,New_Company_Names__mdt> mapNewCompanyNames {get; set;}
    global list<string> fieldnames {get; set;}
    global SET<string> setCompanyName;
    global Boolean isUpdateCompanyJobException = False;
     global String strErrorMessage = '';
    global datetime Old180DaysDateTime;
    global DateTime newBatchRunTime;
    global Map<Id,Account> nonUpdatedAccountMap;
    global string query;

    
    global Batch_updateHistoricalData ()
    {
        
        //queryAcc=query;
        nonUpdatedAccountMap = new Map<Id,Account>();
        newBatchRunTime = System.now();
        Old180DaysDateTime = System.now().addDays(-180);
        
       //string query;
        fieldnames = new list<string>();
        mapNewCompanyNames = new Map<String,New_Company_Names__mdt>();
        
        //query for getting custom metadata type data that has company name and its related former and current field api name
        List<New_Company_Names__mdt> lstNewCompanyNames = [SELECT MasterLabel,Current_Field_API_Name__c,Former_Field_API_Name__c from New_Company_Names__mdt];
        for(New_Company_Names__mdt company : lstNewCompanyNames)
        {
            fieldnames.add(company.Current_Field_API_Name__c);
            fieldnames.add(company.Former_Field_API_Name__c);
            mapNewCompanyNames.put(company.MasterLabel,company);
        }
        setCompanyName = mapNewCompanyNames.keyset();
    }
    
    //start method
    global Database.queryLocator start(Database.BatchableContext BC)
    {
        //query order records
        //string query = 'select id, ' + String.join(fieldnames, ', ') + ' from account where recordtype.name=\'client\'';
           
         return Database.getQueryLocator(query);
    }
    
    //execute method
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        set<string> setAccountId = new set<string>();
        List<Log__c> errorLogs = new List<Log__c>();
        List<Account>  accList = scope;
        if(accList.size() > 0)
        {
            try
            {
                for(Account acc : accList)
                {
                    setAccountId.add(acc.Id);   
                   // nonUpdatedAccountMap.put(acc.Id,acc);  
                    
                }
             errorLogs = BatchUpdateCompanyDataHelperclass.processaccnts(setAccountId,fieldnames,Old180DaysDateTime,setCompanyName,isUpdateCompanyJobException,nonUpdatedAccountMap,mapNewCompanyNames);
            
             if(errorLogs.size() >0)
             {
                 isUpdateCompanyJobException = True;
             }
            } 
            Catch(Exception e)
            {
                 // Process exception here and dump to Log__c object
                 errorLogs.add(Core_Log.logException(e));
                 database.insert(errorLogs,false);
                 isUpdateCompanyJobException = True;
            }
        }
    }
    
    //finish method
    global void finish(Database.BatchableContext BC)
    {
        
        if(isUpdateCompanyJobException)
        {
             //If there is an Exception, send an Email to the Admin
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                String[] toAddresses = new String[] {Label.Error_Notification_Email_Address};  //
                mail.setToAddresses(toAddresses); 
                String strEmailBody;
                              
                mail.setSubject('EXCEPTION - Identify Out Of Sync accs Batch Job'); 
              
                //Prepare the body of the email
                strEmailBody = 'The scheduled Apex Account Job failed to process. There was an exception during execution.\n'+ strErrorMessage;                
               
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
               
                //Reset the Boolean variable and the Error Message string variable
                isUpdateCompanyJobException = False;
                strErrorMessage = '';
        }
    }
}