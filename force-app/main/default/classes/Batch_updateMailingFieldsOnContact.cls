global class Batch_updateMailingFieldsOnContact implements Database.Batchable<sObject>, Database.Stateful {
	
	global String query;
	@TestVisible private Map<String, String> stateConversionMap;
	@TestVisible private Map<String, String> countryConversionMap;

	private static String TEXTVALUE = 'Text_Value__c';
	private static String TEXTVALUE2 = 'Text_Value_2__c';
	private static String TEXTVALUE3 = 'Text_Value_3__c';

	global Batch_updateMailingFieldsOnContact() {
		//provide default query
		query = 'SELECT Id ' +
					', MailingState' +
					', MailingCountry' + 
				' FROM Contact' +
				' WHERE Account.Is_ATS_Talent__c = true';

		stateConversionMap = GLobalLOVDataAccessor.getStateMap(TEXTVALUE2, TEXTVALUE3);
		countryConversionMap = GLobalLOVDataAccessor.getCountryMap(TEXTVALUE, TEXTVALUE2);
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Contact> contacts = setMailingFieldsOnContacts((List<Contact>)scope);
		updateContacts(contacts);
	}

	global void finish(Database.BatchableContext BC) {
		AsyncApexJob apexJob = [SELECT Id
									, Status 
									, NumberOfErrors
									, JobItemsProcessed
									, TotalJobItems
									, CreatedBy.Email
								FROM AsyncApexJob
								WHERE Id =: BC.getJobId()];
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setToAddresses(new List<String>{apexJob.CreatedBy.Email});
		email.setSubject('One time MailingState and MailingCountrying cleanup batch results');
		email.setPlainTextBody('The batch apex job processed ' + 
								apexJob.TotalJobItems + 
								' batches with ' + 
								apexJob.NumberOfErrors + 
								' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
	}

	@TestVisible
	private List<Contact> setMailingFieldsOnContacts(List<Contact> scope) {
		List<Contact> contacts = new List<Contact>();
		for(Contact candidate : scope) {
			String matchedMailingState = stateConversionMap.get(candidate.MailingState);
			String matchedMailingCountry = countryConversionMap.get(candidate.MailingCountry);
			//preserve system fields
			Boolean modified = false;
			if(String.isNotBlank(matchedMailingState)) {
				candidate.MailingState = matchedMailingState;
				modified = true;
			}
			if(String.isNotBlank(matchedMailingCountry)) {
				candidate.MailingCountry = matchedMailingCountry;
				modified = true;
			}
			if(modified) {
				contacts.add(candidate);
			}
		}
		return contacts;
	}

	@TestVisible
	private void updateContacts(List<Contact> contacts) {
		//allow partial complete
		List<Database.SaveResult> results = Database.update(contacts, false);
		for(Database.SaveResult result : results) {
			if(!result.isSuccess()) {
				for(Database.Error err : result.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(LoggingLevel.WARN, err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Fields impacted: ' + err.getFields());
				}
			}
		}
	}
}