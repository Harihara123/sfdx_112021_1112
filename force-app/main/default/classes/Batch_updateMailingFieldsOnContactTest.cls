@isTest
public class Batch_updateMailingFieldsOnContactTest {
	
	static final String OLDSTATE_CODE = 'TST.88';
	static final String NEWSTATE_FULLNAME = 'Test State';
	static final String OLDCOUNTRY_2CODE = 'TS';
	static final String NEWCOUNTRY_3CODE = 'TST';

	@isTest 
	static void test_constructor() {
		dataSetupGlobalLOVs();
		Test.startTest();
		Batch_updateMailingFieldsOnContact b = new Batch_updateMailingFieldsOnContact();
		Test.stopTest();
		System.assertEquals(b.stateConversionMap.size() > 0, true, 'Did not find test state');
		System.assertEquals(b.countryConversionMap.size() > 0, true, 'Did not find test country');
	}

	@isTest
	static void test_normalFlow() {
		dataSetupGlobalLOVs();
		List<Contact> scope = dataSetupAccountContacts();
		
		System.runAs(getSiebelIntegrationUser()) {
			Test.startTest();
			Batch_updateMailingFieldsOnContact b = new Batch_updateMailingFieldsOnContact();
			Database.executeBatch(b);
			Test.stopTest();
		}

		for(Contact updatedContact : [SELECT Id, MailingState, MailingCountry FROM Contact]) {
			System.assertEquals(updatedContact.MailingState == NEWSTATE_FULLNAME, true, 'MailingState value should be fullname');
			System.assertEquals(updatedContact.MailingCountry == NEWCOUNTRY_3CODE, true, 'MailingCountry value should be 3 digit code');
		}
	}

	@isTest
	static void test_setMailingFieldsOnContacts() {
		dataSetupGlobalLOVs();
		List<Contact> scope = dataSetupAccountContacts();
		Test.startTest();
		Batch_updateMailingFieldsOnContact b = new Batch_updateMailingFieldsOnContact();
		scope = b.setMailingFieldsOnContacts(scope);
		Test.stopTest();
		System.assertEquals(scope[0].MailingState == NEWSTATE_FULLNAME, true, 'Failed to set new state value');
		System.assertEquals(scope[0].MailingCountry == NEWCOUNTRY_3CODE, true, 'Failed to set new country value');
	}

	public static void dataSetupGlobalLOVs() {
		//create some state/country Global_LOV__c
		List<Global_LOV__c> globalLOVs = new List<Global_LOV__c>();
		globalLOVs.add(new Global_LOV__c(LOV_Name__c = 'StateList'
										, Source_System_Id__c = 'StateList.1230'
										, Text_Value_2__c = OLDSTATE_CODE
										, Text_Value_3__c = NEWSTATE_FULLNAME));
		globalLOVs.add(new Global_LOV__c(LOV_Name__c = 'CountryList'
										, Source_System_Id__c = 'CountryList.1230'
		 								, Text_Value__c = OLDCOUNTRY_2CODE
		 								, Text_Value_2__c = NEWCOUNTRY_3CODE));
		insert globalLOVs;
	}

	public static List<Contact> dataSetupAccountContacts() {
		List<Account> accounts = new List<Account>();
		List<Contact> contacts = new List<Contact>();
		System.runAs(getSiebelIntegrationUser()) {
			Account rpoTalentAccount = createAccount('rpoTalentAccount', 'RPO');
			accounts.add(rpoTalentAccount);
			Account apTalentAccount = createAccount('apTalentAccount', 'AP');
			accounts.add(apTalentAccount);
			Account mlaTalentAccount = createAccount('mlaTalentAccount', 'MLA');
			accounts.add(mlaTalentAccount);
			Account agEmeaTalentAccount = createAccount('agEmeaTalentAccount', 'AG_EMEA');
			accounts.add(agEmeaTalentAccount);
			insert accounts;

			contacts.add(createContact('rpo', 'rpoTalentAccount', rpoTalentAccount.Id));
			contacts.add(createContact('ap', 'apTalentAccount', apTalentAccount.Id));
			contacts.add(createContact('mla', 'mlaTalentAccount', mlaTalentAccount.Id));
			contacts.add(createContact('agEmea', 'agEmeaTalentAcount', agEMeaTalentAccount.Id));
			insert contacts;
		}
		return contacts;
	}

	private static String appendRandom(String value) {
		return value + String.valueOf(Math.random());
	}

	private static Account createAccount(String fullName, String ownerShip) {
		return new Account(RecordTypeId = System.Label.CRM_TALENTACCOUNTRECTYPE
							, Name = appendRandom(fullName)
							, Talent_Ownership__c = ownerShip);
	}

	private static Contact createContact(String fName, String lName, Id accountId) {
		return new Contact(RecordTypeId = System.Label.CRM_TALENTCONTACTRECTYPE
									, FirstName = appendRandom(fName)
									, LastName = appendRandom(lName)
									, Email = fName + lName + '@test.com'
									, MailingState = OLDSTATE_CODE
									, MailingCountry = OLDCOUNTRY_2CODE							
									, AccountId = accountId);
	}

	private static User getSiebelIntegrationUser() {
		return [SELECT Id FROM User WHERE CommunityNickname = 'siebel.integration' AND Profile.Name = 'System Integration' AND isActive = true LIMIT 1];
	}
}