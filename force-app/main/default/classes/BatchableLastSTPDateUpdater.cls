global class BatchableLastSTPDateUpdater implements Database.Batchable<SObject> {
	Integer backHours;
	
	global BatchableLastSTPDateUpdater(Integer backHours) {
		this.backHours = backHours;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
		String t = (Datetime.now().addHours(-this.backHours) + '').replace(' ', 'T') + 'Z';
		return Database.getQueryLocator('SELECT Id, WhoId, WhatId from Task where Type = \'Service Touchpoint\' and LastModifiedDate > ' + t + ' and Source_System_Id__c like \'R.%\'');
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Task> scope) {
		Set<Id> contactIdSet = new Set<Id>();
		try {
			for (Task t : scope) {
				contactIdSet.add(t.WhoId);
			}
			Map<Id, Contact> cmap = new Map<Id, Contact>([SELECT Id, Last_Service_Date__c FROM Contact where Id in :contactIdSet]);
			
			//System.debug(cmap);
			ActivityHelper.updateLastServiceDateOnContact(scope, cmap);

			//System.debug('FINAL closeout  ---- ');
			//System.debug(cmap);
			updateContacts(cmap);
		} catch (DMLException de) {
			Integer numErrors = de.getNumDml();
			System.debug(logginglevel.WARN,'getNumDml=' + numErrors);
			for(Integer i=0;i<numErrors;i++) {
				System.debug(logginglevel.WARN,'getDmlFieldNames=' + de.getDmlFieldNames(i));
				System.debug(logginglevel.WARN,'getDmlMessage=' + de.getDmlMessage(i));
			}
		} catch (Exception e) {
			System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
			System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
			System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
			System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
			System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
		}
	}

	private static void updateContacts(Map<Id, Contact> contactSTPs) {
        List<Log__c> errorLogs = new List<Log__c>(); 

        if (contactSTPs.size() > 0) {
            for (database.saveResult result : database.update(contactSTPs.values(), false)) {
                // insert the log record accordingly
                if (!result.isSuccess()) {
                    errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        } 

        if (errorLogs.size() >0) {        
            database.insert(errorLogs, false);
        }
    }
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedById
											FROM AsyncApexJob WHERE Id = :context.getJobId()];
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setTargetObjectId(a.CreatedById);
		String subject = 'Last Service Touchpoint date update';
		mail.setSubject(subject + a.Status);
		mail.setPlainTextBody
			('The batch Apex job processed ' + a.TotalJobItems +
			' batches with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}