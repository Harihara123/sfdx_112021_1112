@isTest
public class BatchableLastSTPDateUpdater_Test {
    
    @isTest
    private static void test1()
    {
        Account a = new Account(name='Testtask');
        insert a;
        List<Task> tasks = new List<Task>{};
            for(Integer i = 0; i < 200; i++) 
        {
            Task t = new Task(Subject='Donni'+i,Status='New',Priority='Normal',CallType='Outbound',Type='Service Touchpoint',Source_System_Id__c='R.1234'+1);
            tasks.add(t);
        }
        
        Test.startTest();
			insert tasks;
            BatchableLastSTPDateUpdater obj = new BatchableLastSTPDateUpdater(200);
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }

}