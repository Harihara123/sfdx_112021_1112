Public class BypassLogic{
 public static final string USER_PROV_ACCOUNT_CONTACT_CREATE = 'User provisioning account and contact creation';
 
    Public Static Map<String,Boolean> BypassVals;    
    Public BypassLogic (){
     BypassVals =new Map<String,Boolean>();
     for (BypassMethods__c each : BypassMethods__c.getAll().values()){
          BypassVals.put(each.Key__c,each.Disable__c);
     }     
    }
    public static Boolean shouldNotBypass(String key) {
    Boolean result = true;
    if (BypassVals.containsKey(key) && BypassLogic.BypassVals.get(key) == true) {
    result = false;
    }
    return result;
    }

}