public class CDCDataExportUtility {
   
    // Sending list of Sobject with all fields
    @future(callout=true)
	public static void getRecords(Set<String> idSet, String sobjectName, List<String> ignoreFields){
        System.debug('SObject Name :::' + sobjectName);
        List<Sobject> recordList = new List<Sobject>();
        ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();
        try{
            if(idSet.size()>0){
                String queryString = 'SELECT ' + fetchFieldNames(sobjectName,ignoreFields) 
                                                      + ' FROM ' + sobjectName + ' WHERE Id IN :idSet ALL ROWS';
                
            	recordList = Database.query(queryString);
                logSack.AddInformation('GCPSync/CDCExportQueryData', 'CDCDataExportUtility', 'getRecords', 
                                'Queried ' + idSet.size() + ' ' + sobjectName + ' records ' + joinIds(idSet));
                GooglePubSubHelper.RaiseQueueEventsWithTopicName(recordList,'');
            }
        	    
        }Catch(Exception e){
         	system.debug('Exception::'+e.getStackTraceString());
            logSack.AddException('GCPSync/CDCExportQueryData/Exception', 'CDCDataExportUtility', 'getRecords', e);
            //Incase of any exception we dump this records in Data_Extraction_EAP__c object
            PubSubBatchHandler.insertDataExteractionRecord(recordList, sobjectName);
        } finally {
            logSack.Write();
        }
            
    }

    public static String joinIds(Set<String> idSet) {
        String joined = '';
        for (String i: idSet) {
            joined += i + ', ';
        }
        return joined;
    }

    public static String joinIds(List<Id> idList) {
        String joined = '';
        for (Id i: idList) {
            joined += i + ', ';
        }
        return joined;
    }

    public static String joinObjIds(List<SObject> objSet) {
        String ids = '';
        for (SObject o: objSet) {
            ids += o.Id + ',';
        }
        return ids;
    }

    // Sending list of Sobject with all fields
    @future(callout=true)
    public static void sendJsonsToPubSub(List<String> recordJsons) {
        ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();
        try {
            if (recordJsons.size() > 0) {
                GooglePubSubHelper.RaiseJsonQueueEventsWithTopicName(recordJsons, '');
                logSack.AddInformation('GCPSync/GCPSyncRequestEvent', 'CDCDataExportUtility', 'sendJsonsToPubSub', 
                                'Success raised ' + recordJsons.size() + 'queue events');
            }
        } catch (Exception e) {
            System.debug('Exception::' + e.getStackTraceString());
            logSack.AddException('GCPSync/GCPSyncRequestEvent/Exception', 'CDCDataExportUtility', 'sendJsonsToPubSub', e);
            //Incase of any exception we dump this records in Data_Extraction_EAP__c object
            // PubSubBatchHandler.insertDataExteractionRecord(recordList, SobjectName);
        } finally {
            logSack.Write();
        }
    }

	// to fetch object field names
	public static String fetchFieldNames(String sObjectName, List<String> ignoreFields){
        List<String> lcaseIF = new List<String>();
        for(String s : ignoreFields){
            lcaseIF.add(s.toLowerCase());
        }
        ignoreFields = null; 
        
        String SobjectApiName = sObjectName;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(!lcaseIF.contains(fieldName)){
                if(commaSepratedFields == null || commaSepratedFields == ''){
                    commaSepratedFields = fieldName;
                }else{
                    commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                }
            }
        }
        return commaSepratedFields;
    }
    
    public static void addRecordToCatcher(Set<String> idSet){
        try{
            List<PubSubCatcher__c> pubsubList = new List<PubSubCatcher__c>();
            for(String s:idSet){
            	PubSubCatcher__c pobj = new PubSubCatcher__c();
                pobj.RecordID__c = s;
                pubsubList.add(pobj);
            }
            insert pubsubList;
        }Catch(Exception e){
            System.debug('exception e'+e.getStackTraceString());
        }
    }

}