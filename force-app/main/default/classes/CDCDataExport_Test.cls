@isTest
public class CDCDataExport_Test {
    public static testMethod void method1(){
        List<CDC_Data_Export_Flow__c>  serviceSettingsList = new List<CDC_Data_Export_Flow__c>();
        CDC_Data_Export_Flow__c serviceSettings = new CDC_Data_Export_Flow__c(name ='DataExport',
                                                                            Change_Event_Topic_Name__c='ingest-digestx',
                                                                            Data_Export_All_Fields__c=true,
                                                                            Sobject_Topic_Name__c ='topic x'
                                                                         );
        serviceSettingsList.add(serviceSettings);
        insert serviceSettingsList;
		Connected_Data_Export_Switch__c serviceSetting2 = new Connected_Data_Export_Switch__c(Name='DataExport',
                                                                                                   PubSub_URL__c='Https://www.google.com');
        insert serviceSetting2;
        

        Test.startTest(); 
        Careersite_Application_Details__c detailObject = new Careersite_Application_Details__c();

        detailObject.OPCO__c = 'Easi';
        detailObject.Application_Source__c ='Phenom';
        detailObject.Application_Date_Time__c = System.now();  
        detailObject.Adobe_Ecvid__c = '434';
        detailObject.Posting_Id__c = '6959608';
        detailObject.Vendor_Application_Id__c = 'JA2H1YB7187QBTGGXFN3';
        detailObject.Retry_Status__c ='PENDING';
        detailObject.Application_Status__c ='PENDING';
        insert detailObject;
        
        Set<String> idSet = new Set<String>();
        idSet.add(detailObject.Id);
        
          
        Test.setMock(HttpCalloutMock.class, new TestCDCExportMock());
         CDCDataExportUtility.getRecords(idSet,'Careersite_Application_Details__c',new List<String>());
       	  
         //CDCDataExportUtility.getRecords(idSet,'Careersite_Application_Details__c',new List<String>());

        Test.stopTest();
    }

}