public with sharing class CRMBaseController {
    @AuraEnabled
    public static Object performServerCall(String subClassName, String methodName, Map<String, Object> parameters){
        Object result = null;

        //Call the relevant performServerCall method in the specified subClass       
        if(subClassName == 'CRMEnterpriseReqFunctions'){
             result = CRMEnterpriseReqFunctions.performSFServerCall(methodName,parameters);
        }
		if(subClassName == 'TalentFirstOpportunity'){			
            result = TalentFirstOpportunity.performSFServerCall(methodName,parameters);
        }
        return result;
    }
}