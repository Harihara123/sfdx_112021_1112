public with sharing class CRMEntReqPicklistModel{

@AuraEnabled
public Map<String, String> stageMappings {get;set;}
@AuraEnabled
public Map<String, String> productMappings {get;set;}
@AuraEnabled
public Map<String, String> currencyMappings {get;set;}
@AuraEnabled
public Map<String, String> durationUnitMappings {get;set;}
@AuraEnabled
public Map<String, String> workRemoteMappings {get;set;}
@AuraEnabled
public Map<String, String> minimumEducationMappings {get;set;}
@AuraEnabled
public Map<String, String> governmentWorkMappings {get;set;}
@AuraEnabled
public Map<String, String> mspMappings {get;set;}
@AuraEnabled
public Map<String, String> lossWashReasonMappings {get;set;}
@AuraEnabled
public Map<String, String> practiceAreaMappings {get;set;}
@AuraEnabled
public Map<String, String> experienceLevelMappings {get;set;}
@AuraEnabled
public Map<String, String> paymentTermsMappings {get;set;}
@AuraEnabled
public Map<String, String> termsOfEngagementMappings {get;set;}
@AuraEnabled
public Map<String, String> opcoMappings {get;set;}
@AuraEnabled
public Map<String, String> businessUnitMappings {get;set;}
@AuraEnabled
public Map<String,String> rateFrequencyMappings {get;set;}
@AuraEnabled
public Map<String,List<String>> opcoDivsionDepdentMappings{get;set;}
@AuraEnabled
public Map<String,String> whyPositionMappings{get;set;}
@AuraEnabled
public Map<String,List<String>> skillSpecialtyDivisionDepdentMappings{get;set;}
@AuraEnabled
public Map<String,List<String>> PrimaryBuyerMapping{get;set;}
@AuraEnabled
public Map<String, String> productPicklistMappings {get;set;}    
@AuraEnabled
public Map<String, String> skillPicklistMappings {get;set;}
@AuraEnabled
public Map<String, String> dispositionMappings {get;set;}
@AuraEnabled
public Map<String,String> SegmentMappings{get;set;}
@AuraEnabled
public Map<String,String> JobCodeMappings{get;set;}
@AuraEnabled
public Map<String,String> CategoryMappings{get;set;}
@AuraEnabled
public Map<String,String> MainSkillMappings{get;set;}
@AuraEnabled
public Map<String,String> ClientReqWorkingMappings{get;set;}

@AuraEnabled
public Map<String,String> draftReasonMappings{get;set;}

@AuraEnabled
public Map<String,String> clientWorkingOnReqMappings{get;set;}

@AuraEnabled
public Map<String,String> serviceProductOfferingMappings{get;set;}
    
@AuraEnabled
public Map<String,String> alternateDeliveryTypeMappings{get;set;}

@AuraEnabled
public Map<String,String> otmultiplierMappings{get;set;}
    
@AuraEnabled
public Map<String,String> alternativeDeliveryOfficeMappings{get;set;}    


    
@AuraEnabled
public Map<String,String> oppLOBEITMapping{get;set;}
    


    
@AuraEnabled
public User_Organization__c office {get;set;}

@AuraEnabled
public String opco{get;set;}

@AuraEnabled
public String cloneOpco{get;set;}

@AuraEnabled
public String ownershipOpco {get;set;}
@AuraEnabled
public Account act {get;set;}
@AuraEnabled
public Contact cnt {get;set;}
@AuraEnabled
public Account endClient {get;set;}
@AuraEnabled
public Opportunity opp {get;set;}

@AuraEnabled
public Order proactiveSubmittal {get;set;} //Proactive Submittal - S-143480

@AuraEnabled
public Id objRecId {get;set;}

@AuraEnabled
public List<Object> skillList{get;set;}
@AuraEnabled
public String skillset{get;set;}
@AuraEnabled
public String sobjType{get;set;}
@AuraEnabled
public Boolean ofccpReqd {get;set;}
@AuraEnabled
public Boolean isTgsReq {get;set;}
@AuraEnabled
public String tgsOpptyId {get;set;}
@AuraEnabled
public Boolean exportControlsReqd {get;set;}
@AuraEnabled
public Map<String, String> TGSReqListMappings {get;set;}
@AuraEnabled
public Map<String, String> PracEngListMappings {get;set;}
@AuraEnabled
public Map<String, String> IsIntTravelListMappings {get;set;}
@AuraEnabled
public Map<String, String> InternalHireMappings {get;set;}
@AuraEnabled
public Map<String, String> EmploymentAlignmentMappings {get;set;}
@AuraEnabled
public Map<String, String> FinalDecisionMakerMappings {get;set;}
@AuraEnabled
public Map<String, String> GlobalServiceLocationMappings {get;set;}
@AuraEnabled
public Map<String, String> funcPositionMappings {get;set;}
@AuraEnabled
public Boolean isSkillVisible{get;set;}
@AuraEnabled
public Map<String, String> ReqOptMappings {get;set;}
@AuraEnabled
public Map<String, String> skillSetMappings {get; set;}
@AuraEnabled
public Boolean isPGMember {get; set;}
}