public with sharing class CRMProductHierarchy {

@AuraEnabled
public Map<String,String> SegmentMappings{get;set;}
@AuraEnabled
public Map<String,String> JobCodeMappings{get;set;}
@AuraEnabled
public Map<String,String> CategoryMappings{get;set;}
@AuraEnabled
public Map<String,String> MainSkillMappings{get;set;}
@AuraEnabled
public Map<String,List<String>> opcoDivsionDepdentMappings{get;set;}
@AuraEnabled
public Map<String, String> opcoMappings {get;set;}
@AuraEnabled
public String productName{get;set;}
@AuraEnabled
public Map<String, String> productPicklistMappings {get;set;}
@AuraEnabled
public String opco{get;set;}
@AuraEnabled
public String division{get;set;}
@AuraEnabled
public String segment{get;set;}
@AuraEnabled
public Product2 prod{get;set;}
@AuraEnabled
public Id objRecId {get;set;}
@AuraEnabled
public String flag{get;set;}
//@AuraEnabled
//public Schema.SObjectType objType {get;set;} 
@AuraEnabled
public Map<String, String> draftReasonMappings {get;set;}
@AuraEnabled
public String draftReason{get;set;}
@AuraEnabled
public Map<String, String> oppLOBEITMapping {get;set;}
@AuraEnabled
public String LOB{get;set;}
@AuraEnabled
public Map<String, String> TGSReqListMappings {get;set;}
@AuraEnabled
public String TGSReq{get;set;}
@AuraEnabled
public Map<String, String> ClientReqWorkingMappings {get;set;}
@AuraEnabled
public String clientWorkingReq{get;set;}    
}