public with sharing class CRMProductHierarchyController{
     


  
  @AuraEnabled
  public static String saveEnterpriseReq(String oppStr,String prodStr){
     System.debug('RESULT'+oppStr);
     
     List<Account> accList;
     List<Contact> ctList;
     String newSkills='';
     try{
     
     Opportunity opp = (Opportunity)JSON.deserialize(oppStr, Opportunity.class);
     Product2 prod=(Product2)JSON.deserialize(prodStr,Product2.class);
     opp.Apex_Context__c=true;
    
     System.debug('Division_Name__c--->'+opp.Req_Division__c); 
     
     System.debug('Job_Code__c--->'+prod.Job_Code__c);
         
    System.debug('OpCo__c--->'+opp.OpCo__c );
     
         
     System.debug('Segment__c--->'+prod.Segment__c);
    
     System.debug('Category__c--->'+prod.Category__c);
     
    System.debug('Skill__c--->'+prod.Skill__c);
     
     System.debug('Product-------select id from Product2 where Division_Name__c =:prod.Division_Name__c and Job_Code__c=:prod.Job_Code__c and OpCo__c=:prod.OpCo__c and Segment__c=:prod.Segment__c and Category__c=:prod.Category__c and Skill__c=:prod.Skill__c');
     
         
      if(opp.OpCo__c=='Aerotek, Inc' || opp.OpCo__c=='TEKsystems, Inc.') {
         
     List<Product2> prodObjList=[select id from Product2 where Division_Name__c =:opp.Req_Division__c and Job_Code__c=:prod.Job_Code__c and OpCo__c=:opp.OpCo__c and Segment__c=:prod.Segment__c 
                       	and Category__c=:prod.Category__c and Skill__c=:prod.Skill__c and IsActive=true];
      
      if(prodObjList!=null && prodObjList.size()>0){                 	
     	 System.debug('Product id is --->'+prodObjList.get(0).id);                 	
      
    	 opp.Legacy_Product__c=prodObjList.get(0).id;
      }
      }else{
          opp.Legacy_Product__c=null;
      }
     //opp.RecordTypeId = '01224000000kMQ4AAM';
     String reqRecordTypeId;
      reqRecordTypeId=getOppReqRecordTypeId();
      opp.RecordTypeId = reqRecordTypeId;
      //opp.RecordTypeId = '01224000000kMQ4AAM';
         
         
     System.debug('NEW SKILLS'+opp.EnterpriseReqSkills__c);
     
     update opp;
     return String.valueOf(opp.Id);
     }Catch(Exception e){
           throw new AuraHandledException('Were sorry, Please Contact System Administrator: ' + e.getMessage() + ''+ e.getCause());

     }
     
  }
   
    //Adding w.r.t Product hierarchy for Tek systems new form
    @AuraEnabled
  public static Opportunity getReqProductdata(String oppStr,String prodStr){
     System.debug('RESULT'+oppStr);
     
     List<Account> accList;
     List<Contact> ctList;
     String newSkills='';
     try{
     
     Opportunity opp = (Opportunity)JSON.deserialize(oppStr, Opportunity.class);
     Product2 prod=(Product2)JSON.deserialize(prodStr,Product2.class);
     opp.Apex_Context__c=true;     
         
      if(opp.OpCo__c=='Aerotek, Inc' || opp.OpCo__c=='TEKsystems, Inc.') {
         
     List<Product2> prodObjList=[select id from Product2 where Division_Name__c =:opp.Req_Division__c and Job_Code__c=:prod.Job_Code__c and OpCo__c=:opp.OpCo__c and Segment__c=:prod.Segment__c 
                       	and Category__c=:prod.Category__c and Skill__c=:prod.Skill__c and IsActive=true];
      
      if(prodObjList!=null && prodObjList.size()>0){        	
      
    	 opp.Legacy_Product__c=prodObjList.get(0).id;
      }
      }else{
          opp.Legacy_Product__c=null;
      }
         
      String reqRecordTypeId;
      reqRecordTypeId=getOppReqRecordTypeId();
      opp.RecordTypeId = reqRecordTypeId;
      //opp.RecordTypeId = '01224000000kMQ4AAM';
     
     System.debug('NEW SKILLS'+opp.EnterpriseReqSkills__c);
     
     //update opp;
     return opp;
     }Catch(Exception e){
           throw new AuraHandledException('Were sorry, Please Contact System Administrator: ' + e.getMessage() + ''+ e.getCause());

     }
     
  }
  
    @AuraEnabled
    public static String getOppReqRecordTypeId(){
        List<RecordType> reqOppList=[select id from RecordType where DeveloperName ='Req' and SobjectType='Opportunity' and IsActive=true];
        return String.valueOf(reqOppList[0].id);
    }
  
  @AuraEnabled
  public static CRMProductHierarchy getEnterpriseReqPicklists(String recId){
      try{
          
       CRMProductHierarchy model = new CRMProductHierarchy();
             
       Id objRecId = Id.Valueof(recId);
       Schema.SObjectType objType = objRecId.getSobjectType();
     
        model.objRecId=objRecId;  
          
      if(objType == Opportunity.sObjectType){
        system.debug('Opprtunity id is--->'+recId);
        Opportunity opp = [select accountid,OpCo__c,Req_Division__c,Product__c,Legacy_Product__c,Req_Draft_Reason__c,REQ_Opportunity_driven_mostly_by_LOB__c,Req_TGS_Requirement__c,Req_Client_working_on_Req__c from Opportunity where id=:recId];
        List<Product2> prodList=[SELECT Id, Division_Name__c, Job_Code__c, OpCo__c, Segment__c, Skill__c ,Category__c FROM Product2 where id=:opp.Legacy_Product__c];// and IsActive=true];
        model.opco=opp.OpCo__c; 
        model.division=opp.Req_Division__c;
        model.segment= prodList[0].Segment__c;
        model.productName=opp.Product__c;
        
        model.draftReason=opp.Req_Draft_Reason__c;
        model.LOB=opp.REQ_Opportunity_driven_mostly_by_LOB__c;
        model.TGSReq=opp.Req_TGS_Requirement__c;
        model.clientWorkingReq=opp.Req_Client_working_on_Req__c;
          
        if(prodList!=null && prodList.size()>0){
        model.prod=prodList.get(0);
        }else{
            model.flag='true';
        }
        
       // Account acc = [select Id, Name, Account_Street__c, Account_City__c,Account_State__c, Account_Zip__c,Account_Country__c from Account where Id =:opp.accountid];
     //   system.debug('Account id is--->'+acc.id);  
       // model.act = acc;
      }
      
       
      Map<string,string> opcoMap = new Map<string,string>();
      Schema.DescribeFieldResult field12 = Opportunity.OpCo__c.getDescribe();
      List<Schema.PicklistEntry> opcoList = field12.getPicklistValues();
      for (Schema.PicklistEntry op: opcoList) {
         opcoMap.put(op.getValue(), op.getLabel());
      }
      model.opcoMappings = opcoMap;
       
      Schema.sObjectField opcoControlField=Opportunity.OpCo__c.getDescribe().getSObjectField();
      Schema.sObjectField depedentField=Opportunity.Req_Division__c.getDescribe().getSObjectField();
      Map<String, List<String>> resultmap=CRM_FieldDescribeUtil.getDependentOptionsImpl(depedentField,opcoControlField);   
      model.opcoDivsionDepdentMappings=resultmap;
      
      Map<string,string> productPicklistMap = new Map<string,string>();
        Schema.DescribeFieldResult field38 = Opportunity.Product__c.getDescribe();
        List<Schema.PicklistEntry> productPicklistList = field38.getPicklistValues();
        for (Schema.PicklistEntry product: productPicklistList) {
        if(product.getValue() == 'Core Applications' || product.getValue() == 'Data Analytics and Insights' || product.getValue() == 'Digital and Creative' || product.getValue() == 'Dynamic Workplace Services' ||
                   product.getValue() == 'Enterprise Applications'  || product.getValue() == 'Infrastructure Optimization' || product.getValue() == 'Risk and Security' ||
           product.getValue() == 'Telecom'){
                       productPicklistMap.put(product.getValue(), product.getLabel());}
        }
        model.productPicklistMappings = productPicklistMap;
      
      Map<string,string> reqDraftMapping = new Map<string,string>();
        Schema.DescribeFieldResult field20 = Opportunity.Req_Draft_Reason__c.getDescribe();
        List<Schema.PicklistEntry> reqDraftList = field20.getPicklistValues();
        for (Schema.PicklistEntry posn: reqDraftList) {
        	reqDraftMapping.put(posn.getValue(), posn.getLabel());
        }
        model.draftReasonMappings = reqDraftMapping;
          
      Map<string,string> oppLOBEITMapping = new Map<string,string>();
        Schema.DescribeFieldResult field22 = Opportunity.REQ_Opportunity_driven_mostly_by_LOB__c.getDescribe();
        List<Schema.PicklistEntry> oppLOBEITListing = field22.getPicklistValues();
        for (Schema.PicklistEntry posn: oppLOBEITListing) {
        	oppLOBEITMapping.put(posn.getValue(), posn.getLabel());
        }
        model.oppLOBEITMapping = oppLOBEITMapping;
          
          Map<string,string> isTGSReqMap = new Map<string,string>();
            Schema.DescribeFieldResult field24 = Opportunity.Req_TGS_Requirement__c.getDescribe();
            List<Schema.PicklistEntry> TGSReqList = field24.getPicklistValues();
            for (Schema.PicklistEntry disps: TGSReqList) {
                isTGSReqMap.put(disps.getValue(), disps.getLabel());
            } 
            model.TGSReqListMappings = isTGSReqMap;
         
          Map<string,string> clientReqWorkingMap = new Map<string,string>();
            Schema.DescribeFieldResult fieldReqClientWork = Opportunity.Req_Client_working_on_Req__c.getDescribe();
            List<Schema.PicklistEntry> clientReqWorkingList = fieldReqClientWork.getPicklistValues();
            for (Schema.PicklistEntry wrk: clientReqWorkingList) {
                clientReqWorkingMap.put(wrk.getValue(), wrk.getLabel());
            }
            model.ClientReqWorkingMappings = clientReqWorkingMap;
          
          
     return model;
     
     }Catch(Exception e){
           throw new AuraHandledException('Were sorry, Please Contact System Administrator: ' + e.getMessage() + ''+ e.getCause());
   
     }
}
    
  @AuraEnabled
  public static CRMEntReqPicklistModel getsegmentReqPicklists(String Opco,string Division){ 
       CRMEntReqPicklistModel model = new CRMEntReqPicklistModel();
      
      Map<string,string> Segmentmap=new Map<string,string>();
      List<Product2> productsegment=new List<product2>();
      //Productsegment=[select id, Segment__c where OpCo__c =: Opco and Division_Name__c=:Division];
      for(Product2 prd : [select id, Segment__c from Product2 where OpCo__c =:Opco and Division_Name__c=:Division and IsActive=true order by Segment__c]){
          segmentmap.put(prd.Segment__c,prd.Segment__c);
      }
      model.SegmentMappings=segmentmap;
      
      return model;
      
      
  }
    
    
    @AuraEnabled
  public static CRMEntReqPicklistModel getJobcodeReqPicklists(String Opco,string Division,string segment){ 
       CRMEntReqPicklistModel model = new CRMEntReqPicklistModel();
      system.debug('Harry'+Opco+Division+segment);
      Map<string,string> JobCodeMap=new Map<string,string>();
      //List<Product2> productsegment=new List<product2>();
      //Productsegment=[select id, Job_Code__c where OpCo__c =: Opco and Division_Name__c=:Division and Segment__c=:segment];
      for(Product2 prd : [select id, Job_Code__c from Product2 where OpCo__c =:Opco and Division_Name__c=:Division and  Segment__c=:segment and IsActive=true order by Job_Code__c]){
          JobCodeMap.put(prd.Job_Code__c,prd.Job_Code__c);
      }
      system.debug('mapharry'+JobCodeMap);
      if(JobCodeMap.size()>0){
      model.JobCodeMappings=JobCodeMap;
      }
      return model;
          
      
      
  }
    
     @AuraEnabled
  public static CRMEntReqPicklistModel getCategoryReqPicklists(String Opco,string Division,string segment,string Jobcode){ 
       CRMEntReqPicklistModel model = new CRMEntReqPicklistModel();
      
      Map<string,string> CategoryMap=new Map<string,string>();
      //List<Product2> productsegment=new List<product2>();
      //Productsegment=[select id, Job_Code__c where OpCo__c =: Opco and Division_Name__c=:Division and Segment__c=:segment];
      for(Product2 prd : [select id, Category__c from Product2 where OpCo__c =:Opco and Division_Name__c=:Division and  Segment__c=:segment and Job_Code__c=:Jobcode and IsActive=true order by Category__c]){
          CategoryMap.put(prd.Category__c,prd.Category__c);
      }
      model.CategoryMappings=CategoryMap;
      System.debug('CategoryMappings-----130---->'+CategoryMap);
      return model;
      
      
  }
    
     @AuraEnabled
  public static CRMEntReqPicklistModel getMainskillReqPicklists(String Opco,string Division,string segment,string Jobcode, string Category){ 
       CRMEntReqPicklistModel model = new CRMEntReqPicklistModel();
      
      Map<string,string> MainSkillMap=new Map<string,string>();
      //List<Product2> productsegment=new List<product2>();
      //Productsegment=[select id, Job_Code__c where OpCo__c =: Opco and Division_Name__c=:Division and Segment__c=:segment];
      for(Product2 prd : [select id, Skill__c from Product2 where OpCo__c =:Opco and Division_Name__c=:Division and  Segment__c=:segment and Job_Code__c=:Jobcode and Category__c=:Category and IsActive=true order by Skill__c]){
          MainSkillMap.put(prd.Skill__c,prd.Skill__c);
      }
      model.MainSkillMappings=MainSkillMap;
      
      return model;
      
      
  }
    
     @AuraEnabled
  public static string getMainskillid(String Opco,string Division,string segment,string Jobcode, string Category,string MainSkill){ 
       //CRMEntReqPicklistModel model = new CRMEntReqPicklistModel();
      string productid;
      //Map<string,string> MainSkillMap=new Map<string,string>();
      //List<Product2> productsegment=new List<product2>();
      //Productsegment=[select id, Job_Code__c where OpCo__c =: Opco and Division_Name__c=:Division and Segment__c=:segment];
      for(Product2 prd : [select id, Skill__c from Product2 where OpCo__c =:Opco and Division_Name__c=:Division and  Segment__c=:segment and Category__c=:Category and Job_Code__c=:Jobcode and Skill__c=:MainSkill and IsActive=true LIMIT 1]){
        productid=prd.id;
      }
      return productid ;
      
  }
    
    

}