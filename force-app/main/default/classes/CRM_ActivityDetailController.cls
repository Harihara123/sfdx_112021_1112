public class CRM_ActivityDetailController {

    @AuraEnabled
    public static Event getActivityDetail(Id activityId) {
        
        Event e = [SELECT Id, Subject, Type, OwnerId FROM Event WHERE Id=:activityId];
        return e;
    }
    
    @AuraEnabled
    public static String saveTask(String newTask){
        Task t = (Task)JSON.deserialize(newTask, Task.class);
        system.debug('@@@@Task is' +t);
        try{
            Database.insert(t);
            return 'Inserted';
        } 
        catch(Exception e){
            system.debug('Exception:'+e.getMessage());
            return 'Failed'+e.getMessage();
        }
    }
}