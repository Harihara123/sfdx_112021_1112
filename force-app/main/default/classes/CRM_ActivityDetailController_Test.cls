@isTest
public class CRM_ActivityDetailController_Test {
    
    testmethod static void coverSaveTask() {
        
        Contact c = new Contact(CS_Desired_Salary_Rate__c='10040',CS_Geo_Pref_Comments__c='Elkridge, MD',CS_Highest_Education__c='',CS_Salary__c=100000,LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        Event e   = new Event(subject='Meeting - ',DurationInMinutes = 1,ActivityDateTime = system.now()+1, WhoId=c.Id);
		insert c;
        
        Test.startTest();
        CRM_ActivityDetailController.saveTask(JSON.serialize( e ));
        insert e;
                
        CRM_ActivityDetailController obj = new CRM_ActivityDetailController();
        CRM_ActivityDetailController.saveTask(JSON.serialize( e ));
        CRM_ActivityDetailController.getActivityDetail(e.id);
            
        Test.stopTest();
    }
}