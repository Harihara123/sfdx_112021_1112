public class CRM_AllegisInsightsLocationDonutCntrl{
    //0010D00000DVm17QAD
    //0010D00000DUzgRQAT
    @AuraEnabled
    public static string CRM_AllegisInsightsDonut(string accId, boolean OpenOrClosed,string whereId){
        
        finalwrapper finalData = new finalwrapper();
        list<DataWrapper> OutterfinalList = new list<DataWrapper>();
        //list<DataWrapper> InnerfinalList = new list<DataWrapper>();
        Map<string, DataWrapper> wrapperMap = new Map<string, DataWrapper>();
        Map<string, DataWrapper> InnerwrapperMap = new Map<string, DataWrapper>();
        Map<string, List<DataWrapper>> FinalInnerwrapperMap = new Map<string, List<DataWrapper>>();
        Map<string, List<DataWrapper>> innerMap = new Map<string, List<DataWrapper>>();
        list<string> OpCo = new list<string>();
        for(AggregateResult a : [select count(id), OpCo__c  
                                 from opportunity 
                                 where AccountId  =: accId
                                 and isclosed =: OpenOrClosed 
                                 and CreatedDate = LAST_N_DAYS:90 
                                 and recordtype.name = 'Req'
                                 group by OpCo__c]){
                                     
            DataWrapper dw = new DataWrapper();
            dw.Name = getselectOptions('Opportunity', 'OpCo__c', string.valueof(a.get('OpCo__c')));
            dw.y = integer.valueof(a.get('expr0'));
            dw.drilldown = string.valueof('Yes');
            dw.showInLegend = true;
            wrapperMap.put(getselectOptions('Opportunity', 'OpCo__c', string.valueof(a.get('OpCo__c'))),dw);
            OpCo.add(string.valueof(a.get('OpCo__c')));
            //OutterfinalList.add(dw);            
        }
        if(!wrapperMap.isEmpty()){
            for(DataWrapper d: wrapperMap.values()){
                OutterfinalList.add(d);
            }
        }
        //system.debug('--OutterfinalList--'+OutterfinalList);
        finalData.OutterData = OutterfinalList;
        if(!OutterfinalList.isEmpty()){
            for(AggregateResult b : [select count(id), Req_Division__c, OpCo__c
                                     from opportunity 
                                     where OpCo__c in: OpCo
                                     and AccountId  =: accId
                                     and isclosed =: OpenOrClosed 
                                     and CreatedDate = LAST_N_DAYS:90 
                                     and recordtype.name = 'Req'
                                     group by Req_Division__c,OpCo__c]){
            
                DataWrapper dw = new DataWrapper(); 
                dw.Name = getselectOptions('Opportunity', 'OpCo__c', string.valueof(b.get('OpCo__c')));
                dw.y = integer.valueof(b.get('expr0'));
                dw.drilldown = string.valueof(b.get('Req_Division__c')); 
                dw.showInLegend = true;
                InnerwrapperMap.put(string.valueOf(b.get('Req_Division__c')),dw);                
            }
            //system.debug('--InnerwrapperMap--'+InnerwrapperMap);
            for(DataWrapper d:InnerwrapperMap.values()){
                if (FinalInnerwrapperMap.containsKey(d.Name)){
                     string KeyVals = d.Name;                 
                     list<DataWrapper> existingList = FinalInnerwrapperMap.get(d.Name);
                     existingList.add(d);
                     FinalInnerwrapperMap.put(KeyVals, existingList);
                 } else {
                     list<DataWrapper> existingList = new List<DataWrapper>();
                     string KeyVal = d.Name;
                     existingList.add(d);
                     FinalInnerwrapperMap.put(KeyVal, existingList);
                 }   
            }
            //system.debug('--FinalInnerwrapperMap--'+FinalInnerwrapperMap);
            for(AggregateResult c : [select count(id), Stagename,Req_Division__c,OpCo__c 
                                     from opportunity 
                                     where OpCo__c in: OpCo
                                     and AccountId  =: accId
                                     and isclosed =: OpenOrClosed 
                                     and CreatedDate = LAST_N_DAYS:90 
                                     and recordtype.name = 'Req'
                                     group by Stagename,Req_Division__c,OpCo__c ]){
                                         
                DataWrapper dw = new DataWrapper(); 
                dw.Name = string.valueof(c.get('Req_Division__c'));
                dw.y = integer.valueof(c.get('expr0'));
                dw.drilldown = string.valueof(c.get('StageName')); 
                dw.showInLegend = true;
                
                //InnerfinalList.add(dw); 
                if (innerMap.containsKey(string.valueOf(c.get('Req_Division__c'))))  {
                     list<DataWrapper> existingList = innerMap.get(string.valueOf(c.get('Req_Division__c')));
                     existingList.add(dw);
                     innerMap.put(string.valueOf(c.get('Req_Division__c')), existingList);
                 } else {
                     list<DataWrapper> existingList = new List<DataWrapper>();
                     existingList.add(dw);
                     innerMap.put(string.valueOf(c.get('Req_Division__c')), existingList);
                 }                                  
                 //system.debug('--innerMap--'+innerMap);                                        
            }
        }
        finalData.InnerData = innerMap;
        finalData.InnerMapData = FinalInnerwrapperMap;
        //system.debug(finalData);
        return System.json.serialize(finalData);
    }
    @AuraEnabled
    public static string CRM_AllegisInsightsLocationFinancialSpreadDonut(string accId, boolean OpenOrClosed,string whereId, string whatIsIt, string timeFrame){
        //system.debug('--whatIsIt--'+whatIsIt);
        //system.debug('--timeFrame--'+timeFrame);
        finalwrapper finalData = new finalwrapper();
        list<DataWrapper> OutterfinalList = new list<DataWrapper>();
        //list<DataWrapper> InnerfinalList = new list<DataWrapper>();
        Map<string, DataWrapper> wrapperMap = new Map<string, DataWrapper>();
        Map<string, DataWrapper> InnerwrapperMap = new Map<string, DataWrapper>();
        Map<string, List<DataWrapper>> FinalInnerwrapperMap = new Map<string, List<DataWrapper>>();
        Map<string, List<DataWrapper>> FinalOutterWrapperMap = new Map<string, List<DataWrapper>>();
        Map<string, List<DataWrapper>> innerMap = new Map<string, List<DataWrapper>>();
        if(whatIsIt == 'spread'){
			if(timeFrame == 'Weekly'){
				for(AggregateResult x: [Select sum(Spread_By_Division_Weekly__c), opco__c 
										from Data360_Accounts__c 
										where Account__c =:accId and time_Frame__c = 'Weekly' 
										group by opco__c]){
					DataWrapper dw = new DataWrapper();
					dw.Name = string.valueof(x.get('OpCo__c'));
					dw.y = integer.valueof(x.get('expr0'));
					dw.drilldown = string.valueof('Yes');
					dw.showInLegend = true;
					wrapperMap.put(string.valueof(x.get('OpCo__c')),dw);
				}
				//system.debug('--wrapperMap--'+wrapperMap);
				if(!wrapperMap.isEmpty()){
					for(DataWrapper d: wrapperMap.values()){
						OutterfinalList.add(d);
					}
				}
				//system.debug('--OutterfinalList--'+OutterfinalList);
				finalData.OutterData = OutterfinalList;
				if(!OutterfinalList.isEmpty()){
					for(AggregateResult b : [Select sum(Spread_By_Division_Weekly__c), opco__c, division__c,time_Frame__c 
											 from Data360_Accounts__c 
											 where Account__c =:accId and time_Frame__c = 'Weekly' 
											 group by time_Frame__c, opco__c, division__c]){
					
						DataWrapper dw = new DataWrapper();
						dw.Name = string.valueof(b.get('OpCo__c'));
						dw.y = integer.valueof(b.get('expr0'));
						dw.drilldown = string.valueof(b.get('division__c'));
						dw.showInLegend = true;
						InnerwrapperMap.put(string.valueOf(b.get('division__c')),dw);                
					}
					//system.debug('--InnerwrapperMap--'+InnerwrapperMap);
					for(DataWrapper d:InnerwrapperMap.values()){
						if (FinalInnerwrapperMap.containsKey(d.Name)){
							 string KeyVals = d.Name;                 
							 list<DataWrapper> existingList = FinalInnerwrapperMap.get(d.Name);
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVals, existingList);
						 } else {
							 list<DataWrapper> existingList = new List<DataWrapper>();
							 string KeyVal = d.Name;
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVal, existingList);
						 }   
					}
					//system.debug('--FinalInnerwrapperMap--'+FinalInnerwrapperMap);
				}
			}else if(timeFrame == 'Yearly'){
				for(AggregateResult x: [Select sum(Spread_By_Division_yearly__c), opco__c,time_Frame__c   
										from Data360_Accounts__c 
										where Account__c =:accId and time_Frame__c != 'Weekly' 
										group by time_Frame__c ,opco__c]){
					DataWrapper dw = new DataWrapper();
					dw.Name = string.valueof(x.get('OpCo__c'));
					dw.y = integer.valueof(x.get('expr0'));
					dw.drilldown = string.valueof(x.get('OpCo__c'))+'-'+string.valueof(x.get('time_Frame__c'));
					dw.showInLegend = true;
					wrapperMap.put(string.valueof(x.get('OpCo__c'))+'-'+string.valueof(x.get('time_Frame__c')),dw);
					//wrapperMap.put(string.valueof(x.get('OpCo__c')),dw);//FinalOutterWrapperMap
				}
				//system.debug('--wrapperMap--'+wrapperMap);
				if(!wrapperMap.isEmpty()){
					for(DataWrapper d: wrapperMap.values()){
						OutterfinalList.add(d);
					}
				}
                for(DataWrapper d:wrapperMap.values()){
                    if (FinalOutterWrapperMap.containsKey(d.drilldown.SubStringBefore('-'))){
                        string KeyVals = d.drilldown.SubStringBefore('-');   
                        d.name = d.drilldown.SubStringAfter('-');
                        list<DataWrapper> existingList = FinalOutterWrapperMap.get(d.drilldown.SubStringBefore('-'));                        
                        existingList.add(d);
                        FinalOutterWrapperMap.put(KeyVals, existingList);
                    } else {
                        list<DataWrapper> existingList = new List<DataWrapper>();
                        string KeyVal = d.drilldown.SubStringBefore('-');
                        d.name = d.drilldown.SubStringAfter('-'); 
                        existingList.add(d);
                        FinalOutterWrapperMap.put(KeyVal, existingList);
                    }   
                }
                //system.debug('--FinalOutterWrapperMap--'+FinalOutterWrapperMap);
				//system.debug('--OutterfinalList--'+OutterfinalList);
                finaldata.OutterDataMap = FinalOutterWrapperMap;
				finalData.OutterData = OutterfinalList;
				if(!OutterfinalList.isEmpty()){
                    OutterfinalList = new list<DataWrapper>();
					for(AggregateResult b : [Select sum(Spread_By_Division_yearly__c), opco__c, division__c,time_Frame__c 
										from Data360_Accounts__c 
										where Account__c =:accId and time_Frame__c != 'Weekly' 
										group by time_Frame__c, opco__c, division__c]){
					
						DataWrapper dw = new DataWrapper();
						dw.Name = string.valueof(b.get('division__c'));
						dw.y = integer.valueof(b.get('expr0'));
						dw.drilldown = string.valueof(b.get('OpCo__c'))+'-'+string.valueof(b.get('time_Frame__c'));
						dw.showInLegend = true;
                        OutterfinalList.add(dw);
						InnerwrapperMap.put(string.valueof(b.get('OpCo__c'))+'-'+string.valueof(b.get('time_Frame__c')),dw);                
					}
					//system.debug('--OutterfinalList--'+OutterfinalList);
					for(DataWrapper d:OutterfinalList){
						if (FinalInnerwrapperMap.containsKey(d.drilldown)){
							 string KeyVals = d.drilldown;                 
							 list<DataWrapper> existingList = FinalInnerwrapperMap.get(d.drilldown);
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVals, existingList);
						 } else {
							 list<DataWrapper> existingList = new List<DataWrapper>();
							 string KeyVal = d.drilldown;
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVal, existingList);
						 }   
					}
					//system.debug('--FinalInnerwrapperMap--'+FinalInnerwrapperMap);
				}
			}
        }else if(whatIsIt == 'revenue'){
			if(timeFrame == 'Weekly'){
				for(AggregateResult x: [Select sum(Revenue_By_Division_Weekly__c), opco__c 
										from Data360_Accounts__c 
										where Account__c =:accId and time_Frame__c = 'Weekly' 
										group by opco__c]){
					DataWrapper dw = new DataWrapper();
					dw.Name = string.valueof(x.get('OpCo__c'));
					dw.y = integer.valueof(x.get('expr0'));
					dw.drilldown = string.valueof('Yes');
					dw.showInLegend = true;
					wrapperMap.put(string.valueof(x.get('OpCo__c')),dw);
				}
				//system.debug('--wrapperMap--'+wrapperMap);
				if(!wrapperMap.isEmpty()){
					for(DataWrapper d: wrapperMap.values()){
						OutterfinalList.add(d);
					}
				}
				//system.debug('--OutterfinalList--'+OutterfinalList);
				finalData.OutterData = OutterfinalList;
				if(!OutterfinalList.isEmpty()){
					for(AggregateResult b : [Select sum(Revenue_By_Division_Weekly__c), opco__c, division__c 
											 from Data360_Accounts__c 
											 where Account__c =:accId and time_Frame__c = 'Weekly' 
											 group by division__c,OpCo__c]){
					
						DataWrapper dw = new DataWrapper();
						dw.Name = string.valueof(b.get('OpCo__c'));
						dw.y = integer.valueof(b.get('expr0'));
						dw.drilldown = string.valueof(b.get('division__c'));
						dw.showInLegend = true;
						InnerwrapperMap.put(string.valueOf(b.get('division__c')),dw);                
					}
					//system.debug('--InnerwrapperMap--'+InnerwrapperMap);
					for(DataWrapper d:InnerwrapperMap.values()){
						if (FinalInnerwrapperMap.containsKey(d.Name)){
							 string KeyVals = d.Name;                 
							 list<DataWrapper> existingList = FinalInnerwrapperMap.get(d.Name);
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVals, existingList);
						 } else {
							 list<DataWrapper> existingList = new List<DataWrapper>();
							 string KeyVal = d.Name;
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVal, existingList);
						 }   
					}
					//system.debug('--FinalInnerwrapperMap--'+FinalInnerwrapperMap);
				}
			}else if(timeFrame == 'Yearly'){
				for(AggregateResult x: [Select sum(Revenue_By_Division_Yearly__c), opco__c,time_Frame__c   
										from Data360_Accounts__c 
										where Account__c =:accId and time_Frame__c != 'Weekly' 
										group by time_Frame__c ,opco__c]){
					DataWrapper dw = new DataWrapper();
					dw.Name = string.valueof(x.get('OpCo__c'));
					dw.y = integer.valueof(x.get('expr0'));
					dw.drilldown = string.valueof(x.get('OpCo__c'))+'-'+string.valueof(x.get('time_Frame__c'));
					dw.showInLegend = true;
					wrapperMap.put(string.valueof(x.get('OpCo__c'))+'-'+string.valueof(x.get('time_Frame__c')),dw);
					//wrapperMap.put(string.valueof(x.get('OpCo__c')),dw);//FinalOutterWrapperMap
				}
				if(!wrapperMap.isEmpty()){
					for(DataWrapper d: wrapperMap.values()){
						OutterfinalList.add(d);
					}
				}
                for(DataWrapper d:wrapperMap.values()){
                    if (FinalOutterWrapperMap.containsKey(d.drilldown.SubStringBefore('-'))){
                        string KeyVals = d.drilldown.SubStringBefore('-');   
                        d.name = d.drilldown.SubStringAfter('-');
                        list<DataWrapper> existingList = FinalOutterWrapperMap.get(d.drilldown.SubStringBefore('-'));                        
                        existingList.add(d);
                        FinalOutterWrapperMap.put(KeyVals, existingList);
                    } else {
                        list<DataWrapper> existingList = new List<DataWrapper>();
                        string KeyVal = d.drilldown.SubStringBefore('-');
                        d.name = d.drilldown.SubStringAfter('-'); 
                        existingList.add(d);
                        FinalOutterWrapperMap.put(KeyVal, existingList);
                    }   
                }
                //system.debug('--FinalOutterWrapperMap--'+FinalOutterWrapperMap);
				//system.debug('--OutterfinalList--'+OutterfinalList);
                finaldata.OutterDataMap = FinalOutterWrapperMap;
				finalData.OutterData = OutterfinalList;
				if(!OutterfinalList.isEmpty()){
                    OutterfinalList = new list<DataWrapper>();
					for(AggregateResult b : [Select sum(Revenue_By_Division_Yearly__c), opco__c, division__c,time_Frame__c 
										from Data360_Accounts__c 
										where Account__c =:accId and time_Frame__c != 'Weekly' 
										group by time_Frame__c, opco__c, division__c]){
					
						DataWrapper dw = new DataWrapper();
						dw.Name = string.valueof(b.get('division__c'));
						dw.y = integer.valueof(b.get('expr0'));
						dw.drilldown = string.valueof(b.get('OpCo__c'))+'-'+string.valueof(b.get('time_Frame__c'));
						dw.showInLegend = true;
                        OutterfinalList.add(dw);
						InnerwrapperMap.put(string.valueof(b.get('OpCo__c'))+'-'+string.valueof(b.get('time_Frame__c')),dw);                
					}
					//system.debug('--OutterfinalList--'+OutterfinalList);
					for(DataWrapper d:OutterfinalList){
						if (FinalInnerwrapperMap.containsKey(d.drilldown)){
							 string KeyVals = d.drilldown;                 
							 list<DataWrapper> existingList = FinalInnerwrapperMap.get(d.drilldown);
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVals, existingList);
						 } else {
							 list<DataWrapper> existingList = new List<DataWrapper>();
							 string KeyVal = d.drilldown;
							 existingList.add(d);
							 FinalInnerwrapperMap.put(KeyVal, existingList);
						 }   
					}
					//system.debug('--FinalInnerwrapperMap--'+FinalInnerwrapperMap);
				}
			}
        }
        finalData.InnerData = innerMap;
        finalData.InnerMapData = FinalInnerwrapperMap;
        //system.debug(finalData);
        //System.debug('FinalData:'+System.json.serialize(finalData));
        return System.json.serialize(finalData);
    }
    public class DataWrapper{
        @AuraEnabled
        public String name;
        @AuraEnabled
        public integer y;
        @AuraEnabled
        public String drilldown;
        //@AuraEnabled
        //public String StageName;
        @AuraEnabled
        public Boolean showInLegend;
    }
    public class finalwrapper{
        @AuraEnabled
        public Map<string, List<DataWrapper>> OutterDataMap;
        @AuraEnabled
        public list<DataWrapper> OutterData;       
        @AuraEnabled
        public Map<string, List<DataWrapper>> InnerMapData;
        @AuraEnabled
        public Map<string, List<DataWrapper>> InnerData;
    }
    public static String getselectOptions(string obj, string fld, string picklistAPI) {
        sObject objObject = schema.getGlobalDescribe().get(obj).newSObject() ;
        string picklistLabel;
        Map <String,String> allOpts = new Map <String,String>();
        Schema.sObjectType objType = objObject.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list <Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            allOpts.put(a.getValue(),a.getlabel());
        }
        picklistLabel = allOpts.get(picklistAPI);
        return picklistLabel;
    }


    @AuraEnabled
    public static String getAccountFinanceData(string accId)
    {
        //system.debug('--accId--'+accId);
        finalwrapper finalData = new finalwrapper();
        list<DataWrapper> OutterfinalList = new list<DataWrapper>();
        //list<DataWrapper> InnerfinalList = new list<DataWrapper>();
        Map<string, DataWrapper> wrapperMap = new Map<string, DataWrapper>();
        Map<string, DataWrapper> InnerwrapperMap = new Map<string, DataWrapper>();
        Map<string, List<DataWrapper>> FinalInnerwrapperMap = new Map<string, List<DataWrapper>>();
        Map<string, List<DataWrapper>> FinalOutterWrapperMap = new Map<string, List<DataWrapper>>();
        Map<string, List<DataWrapper>> innerMap = new Map<string, List<DataWrapper>>();
        if(accId != NULL)
        {
            Account accRecord = [SELECT Id,Current_ESF__c,Former_ESF__c,Total_ESF__c,
                                 EMEA_Current_SIF__c,EMEA_Former_SIF__c,EMEA_Total_SIF__c,
                                 MLA_Current_Starts__c,MLA_Former_Starts__c,Total_MLA__c,
                                 AP_Current_Starts__c, AP_Former_Starts__c,Total_AP__c 
                                 FROM Account 
                                 WHERE Id =: accId];
                                 //WHERE Id = '0011k00000Wx1se'];
            if(accRecord.Total_ESF__c > 0)
            { 
                DataWrapper totalESFWrapper = new DataWrapper();
                totalESFWrapper.Name = String.valueof('TEKandAerotek');
                totalESFWrapper.y = Integer.valueOf(accRecord.Total_ESF__c);
                totalESFWrapper.drilldown = string.valueof('Yes');
                totalESFWrapper.showInLegend = true;
                wrapperMap.put('Total_ESF__c',totalESFWrapper);
            }
            
            if(accRecord.EMEA_Total_SIF__c > 0)
            { 
                DataWrapper totalSIFWrapper = new DataWrapper();
                totalSIFWrapper.Name = String.valueof('EMEA');
                totalSIFWrapper.y = Integer.valueOf(accRecord.EMEA_Total_SIF__c);
                totalSIFWrapper.drilldown = string.valueof('Yes');
                totalSIFWrapper.showInLegend = true;
                wrapperMap.put('Total_SIF__c',totalSIFWrapper);     
            }
            
            if(accRecord.Total_AP__c > 0)
            { 
                DataWrapper totalAPWrapper = new DataWrapper();
                totalAPWrapper.Name = String.valueof('AP');
                totalAPWrapper.y = Integer.valueOf(accRecord.Total_AP__c);
                totalAPWrapper.drilldown = string.valueof('Yes');
                totalAPWrapper.showInLegend = true;
                wrapperMap.put('Total_AP__c',totalAPWrapper);
            }
            
            if(accRecord.Total_MLA__c > 0)
            { 
                DataWrapper totalMLAWrapper = new DataWrapper();
                totalMLAWrapper.Name = String.valueof('MLA');
                totalMLAWrapper.y = Integer.valueOf(accRecord.Total_MLA__c);
                totalMLAWrapper.drilldown = string.valueof('Yes');
                totalMLAWrapper.showInLegend = true;
                wrapperMap.put('Total_MLA__c',totalMLAWrapper);
            }

            //system.debug('--wrapperMap--'+wrapperMap);
            if(!wrapperMap.isEmpty())
            {
                for(DataWrapper d: wrapperMap.values())
                {
                    OutterfinalList.add(d);
                }
            }
            //system.debug('--OutterfinalList--'+OutterfinalList);
            if(!OutterfinalList.isEmpty())
            {  
                List<DataWrapper> innerMapList = new List<DataWrapper>();  
                List<DataWrapper> innerMapList1 = new List<DataWrapper>();  
                List<DataWrapper> innerMapList2 = new List<DataWrapper>();
                List<DataWrapper> innerMapList3 = new List<DataWrapper>(); 
                if(accRecord.Current_ESF__c > 0)
                {             
                    DataWrapper currentESFWrapper = new DataWrapper();
                    currentESFWrapper.Name = String.valueof('TEKandAerotek');
                    currentESFWrapper.y = Integer.valueOf(accRecord.Current_ESF__c);
                    currentESFWrapper.drilldown = string.valueof('TEKandAerotekCurrents');
                    currentESFWrapper.showInLegend = true;
                    innerMapList.add(currentESFWrapper);
                }
                
                if(accRecord.Former_ESF__c > 0)
                {
                    DataWrapper formerESFWrapper = new DataWrapper();
                    formerESFWrapper.Name = String.valueof('TEKandAerotek');
                    formerESFWrapper.y = Integer.valueOf(accRecord.Former_ESF__c);
                    formerESFWrapper.drilldown = string.valueof('TEKandAerotekFormers');
                    formerESFWrapper.showInLegend = true;
                    innerMapList.add(formerESFWrapper);
                }
                
                if(accRecord.EMEA_Current_SIF__c > 0)
                {              
                    DataWrapper currentSIFWrapper = new DataWrapper();
                    currentSIFWrapper.Name = String.valueof('EMEA');
                    currentSIFWrapper.y = Integer.valueOf(accRecord.EMEA_Current_SIF__c);
                    currentSIFWrapper.drilldown = string.valueof('EMEACurrents');
                    currentSIFWrapper.showInLegend = true;
                    innerMapList1.add(currentSIFWrapper);
                }
                
                if(accRecord.EMEA_Former_SIF__c > 0)
                {
                    DataWrapper formerSIFWrapper = new DataWrapper();
                    formerSIFWrapper.Name = String.valueof('EMEA');
                    formerSIFWrapper.y = Integer.valueOf(accRecord.EMEA_Former_SIF__c);
                    formerSIFWrapper.drilldown = string.valueof('EMEAFormers');
                    formerSIFWrapper.showInLegend = true;
                    innerMapList1.add(formerSIFWrapper);
                }
                
                if(accRecord.MLA_Current_Starts__c > 0)
                {                
                    DataWrapper currentMLAWrapper = new DataWrapper();
                    currentMLAWrapper.Name = String.valueof('MLA');
                    currentMLAWrapper.y = Integer.valueOf(accRecord.MLA_Current_Starts__c);
                    currentMLAWrapper.drilldown = string.valueof('MLACurrents');
                    currentMLAWrapper.showInLegend = true;
                    innerMapList2.add(currentMLAWrapper);
                }
                
                if(accRecord.MLA_Former_Starts__c > 0)
                {
                    DataWrapper formerMLAWrapper = new DataWrapper();
                    formerMLAWrapper.Name = String.valueof('MLA');
                    formerMLAWrapper.y = Integer.valueOf(accRecord.MLA_Former_Starts__c);
                    formerMLAWrapper.drilldown = string.valueof('MLAFormers');
                    formerMLAWrapper.showInLegend = true;
                    innerMapList2.add(formerMLAWrapper);
                }
                
                if(accRecord.AP_Current_Starts__c > 0)
                {               
                    DataWrapper currentAPWrapper = new DataWrapper();
                    currentAPWrapper.Name = String.valueof('AP');
                    currentAPWrapper.y = Integer.valueOf(accRecord.AP_Current_Starts__c);
                    currentAPWrapper.drilldown = string.valueof('APCurrents');
                    currentAPWrapper.showInLegend = true;
                    innerMapList3.add(currentAPWrapper);
                }
                
                if(accRecord.AP_Former_Starts__c > 0)
                {
                    DataWrapper formerAPWrapper = new DataWrapper();
                    formerAPWrapper.Name = String.valueof('AP');
                    formerAPWrapper.y = Integer.valueOf(accRecord.AP_Former_Starts__c);
                    formerAPWrapper.drilldown = string.valueof('APFormers');
                    formerAPWrapper.showInLegend = true;
                    innerMapList3.add(formerAPWrapper);
                }
                
                if(!innerMapList.isEmpty())
                    FinalInnerwrapperMap.put('TEKandAerotek',innerMapList);
                if(!innerMapList1.isEmpty())
                    FinalInnerwrapperMap.put('EMEA',innerMapList1);
                if(!innerMapList2.isEmpty())
                    FinalInnerwrapperMap.put('MLA',innerMapList2);
                if(!innerMapList3.isEmpty())
                    FinalInnerwrapperMap.put('AP',innerMapList3);
            }
            finalData.OutterData = OutterfinalList;
            finalData.InnerData = innerMap;
            finalData.InnerMapData = FinalInnerwrapperMap;
           // system.debug(finalData);
            //System.debug('FinalData:'+System.json.serialize(finalData));
        }
        return System.json.serialize(finalData);
    }
}