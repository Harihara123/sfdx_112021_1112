@isTest(seealldata = false)
public class CRM_AllegisInsightsLocationDonut_Test {
    static testMethod Void Test_CRM_AllegisInsightsDonut(){     
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        lstNewAccounts[0].Current_ESF__c = 1;
        lstNewAccounts[0].Former_ESF__c = 1;
        lstNewAccounts[0].EMEA_Current_SIF__c = 1;
        lstNewAccounts[0].EMEA_Former_SIF__c = 1;
        lstNewAccounts[0].MLA_Current_Starts__c = 1;
        lstNewAccounts[0].MLA_Former_Starts__c = 1;
        lstNewAccounts[0].AP_Current_Starts__c = 1;
        lstNewAccounts[0].AP_Former_Starts__c = 1;
        update lstNewAccounts;
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;        
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'Aerotek CE',
                                    Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
                                    Jobcode_Id__c  ='700009',  OpCo_Id__c='ONS', Segment_Id__c  ='A_AND_E',  Segment__c = 'Architecture and Engineering',
                                    OpCo_Status__c = 'A', Skill_Id__c  ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        for(Account Acc : lstNewAccounts) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Qualifying';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc';
                newOpp.Req_Division__c = 'Aerotek Professional Services';
                newOpp.Stagename = 'Open';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                newOpp.Legacy_Product__c = prd.id;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        /*testWrap.Name = 'Aerotek, Inc.';
        testWrap.y = 12;
        testWrap.drilldown = 'true';
        testWrap.showInLegend = true; */
        Test.StartTest();
        String getselectOptions = CRM_AllegisInsightsLocationDonutCntrl.getselectOptions('Opportunity', 'OpCo__c', 'Aerotek, Inc');
        CRM_AllegisInsightsLocationDonutCntrl.DataWrapper testWrap = new CRM_AllegisInsightsLocationDonutCntrl.DataWrapper(); 
        CRM_AllegisInsightsLocationDonutCntrl.finalwrapper finalwrapper = new CRM_AllegisInsightsLocationDonutCntrl.finalwrapper(); 
        string CRM_AllegisInsightsDonut = CRM_AllegisInsightsLocationDonutCntrl.CRM_AllegisInsightsDonut(string.valueof(lstNewAccounts[0].Id),false,''); 
        string getAccountFinanceData = CRM_AllegisInsightsLocationDonutCntrl.getAccountFinanceData(string.valueof(lstNewAccounts[0].Id));
        Test.StopTest(); 
    }
    
    static testMethod Void Test_CRM_AllegisInsightsLocationFinancialSpreadDonut(){     
        List<Data360_Accounts__c> lstOpportunitys = new List<Data360_Accounts__c>();
        TestData TdAccts = new TestData(1);
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        list<String> timeframe = new list<string>{'Weekly','2018'};
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        for(Account Acc : lstNewAccounts) {       
            for(Integer i=0;i < 1; i++){
                Data360_Accounts__c newOpp = new Data360_Accounts__c();
                newOpp.Account__c = Acc.Id;
                newOpp.Spread_By_Division_Weekly__c = 1245;
                newOpp.Spread_By_Division_Yearly__c = 1245;
                newOpp.Revenue_By_Division_Weekly__c = 1245;
                newOpp.Revenue_By_Division_Yearly__c = 1245;
                newOpp.opco__c = 'Aerotek, Inc';
                newOpp.time_Frame__c = timeframe[i];
                newOpp.division__c = 'Aerotek Professional Services';
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys;         
        Test.StartTest();
        CRM_AllegisInsightsLocationDonutCntrl.DataWrapper testWrap = new CRM_AllegisInsightsLocationDonutCntrl.DataWrapper(); 
        CRM_AllegisInsightsLocationDonutCntrl.finalwrapper finalwrapper = new CRM_AllegisInsightsLocationDonutCntrl.finalwrapper(); 
        string SpreadWeekly = CRM_AllegisInsightsLocationDonutCntrl.CRM_AllegisInsightsLocationFinancialSpreadDonut(string.valueof(lstNewAccounts[0].id), false,' ','spread','Weekly'); 
        string revenueWeekly = CRM_AllegisInsightsLocationDonutCntrl.CRM_AllegisInsightsLocationFinancialSpreadDonut(string.valueof(lstNewAccounts[0].id), false,' ','revenue','Weekly');
        lstOpportunitys[0].time_Frame__c = timeframe[1];
        update lstOpportunitys;
        string SpreadYearly = CRM_AllegisInsightsLocationDonutCntrl.CRM_AllegisInsightsLocationFinancialSpreadDonut(string.valueof(lstNewAccounts[0].id), false,' ','spread','Yearly'); 
        string revenueYearly = CRM_AllegisInsightsLocationDonutCntrl.CRM_AllegisInsightsLocationFinancialSpreadDonut(string.valueof(lstNewAccounts[0].id), false,' ','revenue','Yearly'); 
        
        Test.StopTest(); 
    }
}