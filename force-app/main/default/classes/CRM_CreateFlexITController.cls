public class CRM_CreateFlexITController {
 @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        
        Schema.sObjectType objType = objDetail.getSObjectType();
        if (objType==null){
            return objResults;
        }
        
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objResults;     
        }
        
        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        return objResults;
    }
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }        
        return validForBits;
    }
    
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }
    
    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }        
    }
    
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        Schema.sObjectType objType = objObject.getSObjectType();                
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();        
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values = fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    
	@AuraEnabled
    public static list<string> getInitialData(string recId, string tgsOppId){
        system.debug('--recId--'+recId+'--tgsOppId--'+tgsOppId);
        list<string> InitialData = new list<string>();
        string UserRecId = UserInfo.getUserId();
        for(Account y:[Select name from Account where id=:recId]){
            InitialData.add(y.name);
        }
        for(User x:[select id,OPCO__c,Office__c,Companyname from user where id=:UserRecId]){
            InitialData.add(x.Companyname);
            //InitialData.add(x.Office__c);
        }        
        system.debug(InitialData);
        return InitialData;
    }
    
    @AuraEnabled
    public static string SaveFlexITOpp(String FlexOpportunity, String accName){ 
        system.debug(FlexOpportunity);
        Opportunity opp = (Opportunity)JSON.deserialize(FlexOpportunity, Opportunity.class);
        Account act = (Account)JSON.deserialize(accName, Account.class);
        system.debug(opp);
        try{
            if(opp !=null){            
                opp.AccountId = act.Id;
                opp.RecordTypeId = '01224000000gLMyAAM';
                opp.Name = opp.Name;
                opp.StageName = opp.StageName;
                opp.Probability = opp.Probability;
                opp.Response_Type__c = opp.Response_Type__c;
                opp.OpCo__c = opp.OpCo__c;
                opp.BusinessUnit__c = opp.BusinessUnit__c;
                opp.Product_Team__c = opp.Product_Team__c;
                opp.Division__c = opp.Division__c;
                opp.Impacted_Regions__c = opp.Impacted_Regions__c;
                opp.Number_of_Resources__c = opp.Number_of_Resources__c;
                opp.DeliveryModel__c = opp.DeliveryModel__c;
                opp.Amount = opp.Amount;
                opp.Description = opp.Description;  
                opp.CloseDate = opp.CloseDate;
            }
            insert opp;
            system.debug('--opp---'+opp+'--opp id--'+String.valueOf(opp.Id));
            return String.valueOf(opp.Id);
        }catch(exception ex){
            system.debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage()); 
            throw new AuraHandledException(ex.getMessage());
        }        
    }
}