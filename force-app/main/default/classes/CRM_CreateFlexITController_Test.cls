@isTest(seealldata = false)
public class CRM_CreateFlexITController_Test{
    static testMethod Void Test_getDependentMap(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;

        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys;
        Opportunity objDetail; 
        string contrfieldApiName = 'OpCo__c';
        string depfieldApiName = 'BusinessUnit__c';
        string fld = 'DeliveryModel__c';
        String Acc = '{"sobjectType":"Account","Id":"'+id.valueof(lstNewAccounts[0].id)+'"}';
        string FlexOpp = '{"sobjectType":"Opportunity","AccountID":"+string.valueof(lstNewAccounts[0].id)+","Name":"Test Opp Data","StageName":"Interest","Response_Type__c":"Client SOW","OpCo__c":"AG EMEA","BusinessUnit__c":"AG EMEA","Product_Team__c":"Sales","Division__c":"Aerotek","DeliveryModel__c":"Flexible Capacity","Number_of_Resources__c":12,"Amount":65,"Description":"Opp Test Class Data","CloseDate":"2019-01-24"}';
        Test.StartTest();
        system.debug('-objDetail-'+objDetail+'-contrfieldApiName-'+contrfieldApiName+'-depfieldApiName-'+depfieldApiName);
        Map<String, List<String>> DependentMap = CRM_CreateFlexITController.getDependentMap(lstOpportunitys[0],contrfieldApiName,depfieldApiName);  
        List < String > Options = CRM_CreateFlexITController.getselectOptions(lstOpportunitys[0], fld);
        list<string> InitialData = CRM_CreateFlexITController.getInitialData(string.valueof(lstNewAccounts[0]), 'FlexIT');
        string SaveFlexIT = CRM_CreateFlexITController.SaveFlexITOpp(FlexOpp,Acc);
        Test.StopTest();
    }
}