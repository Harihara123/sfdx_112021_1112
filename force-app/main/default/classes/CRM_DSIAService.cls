public class CRM_DSIAService {
    public static String DSIAServiceEndPoint;
	public static String get_access_token(){
        String accessToken=null;
        String Env=Utilities.getSFEnv().toUpperCase();
        Env = (Env=='TEST')?'DEV':Env;        
        List<DSIAService__mdt> DSIAServiceData = [SELECT client_email__c, client_id__c, private_key__c, private_key_id__c, Endpoint__c, target_audience__c, token_uri__c FROM DSIAService__mdt where label = :Env];
                
        HTTP httpInstance=new HTTP();
        HttpRequest req=new HttpRequest();
        HttpResponse res=new HttpResponse();
        //DSIAServiceEndPoint: This end point is for response body
        DSIAServiceEndPoint=DSIAServiceData[0].Endpoint__c;
		//Below endpoint is to get Access token
        //req.setEndpoint('https://oauth2.googleapis.com/token');
        req.setEndpoint(DSIAServiceData[0].token_uri__c);
        req.setMethod('POST');
        req.setHeader('ContentType','application/x-www-form-urlencoded');
        String header = '{"typ":"JWT","alg":"RS256","kid":"'+DSIAServiceData[0].private_key_id__c+'"}';
        String header_encoded = EncodingUtil.base64Encode(blob.valueof(header));           
        String claim_set = '{"aud":"'+DSIAServiceData[0].token_uri__c+'"';
        claim_set += ',"exp":' + datetime.now().addHours(1).getTime()/1000;
    	claim_set += ',"iat":' + datetime.now().getTime()/1000;        
        claim_set += ',"iss":"'+DSIAServiceData[0].client_email__c+'"';
        claim_set += ',"target_audience":"'+DSIAServiceData[0].target_audience__c+'"}';
		
		String claim_set_encoded = EncodingUtil.base64Encode(blob.valueof(claim_set));
        String signature_encoded = header_encoded + '.' + claim_set_encoded;
        String key = DSIAServiceData[0].private_key__c.unescapeJava().remove('-----BEGIN PRIVATE KEY-----\n').remove('\n-----END PRIVATE KEY-----\n');
		blob private_key = EncodingUtil.base64Decode(key);        
        
        signature_encoded = signature_encoded.replaceAll('=','');
        String signature_encoded_url = EncodingUtil.urlEncode(signature_encoded,'UTF-8');
        blob signature_blob =   blob.valueof(signature_encoded_url);           
        String signature_blob_string = EncodingUtil.base64Encode(Crypto.sign('RSA-SHA256', signature_blob, private_key));           
        String JWT = signature_encoded + '.' + signature_blob_string;
        JWT = JWT.replaceAll('=','');  
        String grant_string= 'urn:ietf:params:oauth:grant-type:jwt-bearer';        
        req.setBody('grant_type=' + EncodingUtil.urlEncode(grant_string, 'UTF-8') + '&assertion=' + EncodingUtil.urlEncode(JWT, 'UTF-8'));
        
        	res = httpInstance.send(req);
            String response_debug = res.getBody() +' '+ res.getStatusCode();        
            if(res.getStatusCode() == 200) {
                JSONParser parser = JSON.createParser(res.getBody());
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'id_token')) {                        
                        // Move to the value.
                        parser.nextToken();                      
                        //return parser.getText();
                        accessToken=parser.getText();
                    }
                }                
            }else{
                accessToken='error';                
            }
        return accessToken;
    }
    public static CRM_DSIAServiceRespWrapper callDSIAService(String payload) {
        String access_token = get_access_token();        
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setTimeout(10000); 
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');               
        request.setHeader('Authorization', 'Bearer ' + access_token);
        request.setEndPoint(DSIAServiceEndPoint);
        request.setBody(payload);          
        
        String responseBody;
        CRM_DSIAServiceRespWrapper DSIAWrapper;
        try {
            HttpResponse response = new HTTP().send(request);            
            if (response.getStatusCode() == 200) {
                responseBody = response.getBody();
                System.debug('******responseBody*****'+responseBody);
                DSIAWrapper=new CRM_DSIAServiceRespWrapper();
                DSIAWrapper=(CRM_DSIAServiceRespWrapper)System.JSON.deserialize(responseBody, CRM_DSIAServiceRespWrapper.class);         
                String maxSkillsetClassifier=mapSkillsetClassifier(responseBody);
                DSIAWrapper.skillset.skillset_classifier.maxSkillsetClassifier=maxSkillsetClassifier;
            } else {
                responseBody = 'CALLOUT_EXCEPTION_RESPONSE '+ response.getStatus();
                Exception ex;
                //throw ex;
            }            
        } catch (Exception ex) {
            // Callout to apigee failed
            System.debug('Exception ' + ex.getMessage());
            throw ex;
        }
		System.debug('******skillset*****'+DSIAWrapper.skillset.skillset_classifier.maxSkillsetClassifier);        
        return DSIAWrapper;
    }
    public static String mapSkillsetClassifier(String responseBody) {
		String maxSkillsetClassifier='';
		Double maxValue=null;        
        Map<String,Object> skillsetObj=(Map<String,Object>)JSON.deserializeUntyped(responseBody);
        Map<String,Object> skillsetClassifierObj=(Map<String,Object>)skillsetObj.get('skillset');
        Map<String,Object> mapSkillsetClassifier=(Map<String,Object>)skillsetClassifierObj.get('skillset_classifier');
        for(String str:mapSkillsetClassifier.keySet()){
            if(maxSkillsetClassifier == '' && maxValue == null){
                maxSkillsetClassifier=str;
                maxValue=(Double)mapSkillsetClassifier.get(str);                
            }else if(maxValue <= (Double)mapSkillsetClassifier.get(str)){
                maxSkillsetClassifier=str;
                maxValue=(Double)mapSkillsetClassifier.get(str);                
            }
        }
        System.debug('***********maxSkillsetClassifier****' + maxSkillsetClassifier);        
        return maxSkillsetClassifier;
    }
    /*public static String callService(Map<String, String> payload) {
        String str=null;
        String resp;
        if(payload.get('req_job_title__c') != '' && payload.get('req_job_description__c') != '' && payload.get('enterprisereqskills__c') != ''){
            str=JSON.serialize(payload);
            System.debug('**********sssss*****'+str);
            resp = CRM_DSIAService.callDSIAService(str);
        }
        return resp;
    }*/
}