public class CRM_DSIAServiceRespWrapper {
    @AuraEnabled
	public cls_RVT RVT;
    @AuraEnabled
    public cls_functional functional;
    @AuraEnabled
	public String id;
    @AuraEnabled
	public String message;
    @AuraEnabled
	public cls_onet_job_code onet_job_code;
    @AuraEnabled
	public cls_skill_specialty skill_specialty;
    @AuraEnabled
	public cls_skillset skillset;    
	public class cls_RVT {
        @AuraEnabled
		public String Experience_level;
        @AuraEnabled
		public String Experience_level_confidence_value;
        @AuraEnabled
		public String Occupation;
        @AuraEnabled
        public String Occupation_Code;
        @AuraEnabled
        public String Occupation_Description;
        @AuraEnabled
		public String Occupation_Group;
        @AuraEnabled
		public String Occupation_confidence_value;
        @AuraEnabled
		public cls_RVT_detail RVT_detail;
        @AuraEnabled
		public String message;
        @AuraEnabled
		public String status;
	}    
    public class cls_functional {
        @AuraEnabled
        public String functional;
        @AuraEnabled
        public String message;
        @AuraEnabled
		public String status;
    }    
    public class cls_RVT_detail {
        @AuraEnabled
		public String active_count;
        @AuraEnabled
		public String active_count_without_outlier;
        @AuraEnabled
		public String ags_exclude_median;        
        @AuraEnabled
		public String appx_median_pay_rate;
        @AuraEnabled
		public String average_bill;
        @AuraEnabled
		public String average_bill_wo_outlier;
        @AuraEnabled
		public String bin_size;
        @AuraEnabled
		public String country_code;
        @AuraEnabled
		public String market_grade;
        @AuraEnabled
		public String max_pay_rate;
        @AuraEnabled
		public String maximum_bill_rate;
        @AuraEnabled
		public String maximum_bill_rate_wo_outlier;
        @AuraEnabled
		public String median;
        @AuraEnabled
		public String median_wo_outlier;
        @AuraEnabled
		public String metro_area;
        @AuraEnabled
		public String min_pay_rate;
        @AuraEnabled
		public String minimum_bill_rate;
        @AuraEnabled
		public String open_positions;
        @AuraEnabled
		public String percentage_markup_list;
        @AuraEnabled
		public String premium_fields;
        @AuraEnabled
		public String premium_rate_list;
        @AuraEnabled
		public String premium_skills_list;
        @AuraEnabled
		public String req_count;
        @AuraEnabled
		public String rvt_histogram;
        @AuraEnabled
		public String stdev_bill;
        @AuraEnabled
		public String stdev_bill_wo_outlier;
        @AuraEnabled
		public String total_placements;
        @AuraEnabled
		public String total_placements_wo_outlier;
	}
	public class cls_onet_job_code {
        @AuraEnabled
		public Double confidence;
        @AuraEnabled
		public String message;
        @AuraEnabled
		public String onet_soc_code;
        @AuraEnabled
		public String onet_soc_description;
        @AuraEnabled
		public String status;
	}    
	public class cls_skill_specialty {
        @AuraEnabled
		public String label;
        @AuraEnabled
		public String message;
        @AuraEnabled
		public String score;
        @AuraEnabled
		public String status;
	}    
	public class cls_skillset {
        @AuraEnabled
		public String message;
        @AuraEnabled      
		public cls_skillset_classifier skillset_classifier;
        
        //public Object obj=System.JSON.deserialize(skillset_classifier,Object);
        @AuraEnabled
		public String status;
	}    
	public class cls_skillset_classifier {
        @AuraEnabled
		public String maxSkillsetClassifier;
        /*@AuraEnabled
		public Double app_development;
        @AuraEnabled
		public Integer java_development;
        @AuraEnabled
		public Double search_engine_software;
        @AuraEnabled
		public Double servers_and_middleware;
        @AuraEnabled
		public Double version_control_systems;*/
	}
}