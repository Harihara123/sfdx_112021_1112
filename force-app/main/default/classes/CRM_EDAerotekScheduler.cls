global class CRM_EDAerotekScheduler implements Schedulable {

    global void execute(SchedulableContext sc) 
    {
        
        CRM_EDScoreUpdateBatch b = new CRM_EDScoreUpdateBatch(ed_insights__SDDPredictionConfig__c.getValues('ED Write Back Aerotek CE').ed_insights__Batch_Update_Query__c); 
        database.executebatch(b,100);
    }
}