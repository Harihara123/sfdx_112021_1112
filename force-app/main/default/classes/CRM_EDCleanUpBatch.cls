public class CRM_EDCleanUpBatch implements Database.Batchable <sObject>{
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([select id, ED_Prediction__c,  ED_Prescription__c, ED_Outcome__c 
                                         from opportunity 
                                         where recordtype.name = 'Req' and OpCo__c = 'Aerotek, Inc' and Req_Division__c = 'Aerotek CE' and  ED_Outcome__c != null]);
    }
    
    public void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        List<Opportunity> OppUpdate = new List<Opportunity>();
        try{
            for(Opportunity O : scope){
                O.ED_Prediction__c = '';
                O.ED_Prescription__c = '';
                O.ED_Outcome__c = null;
                OppUpdate.add(O);
            }
            system.debug('--OppUpdate size--'+OppUpdate.size()+'---OppUpdate--'+OppUpdate);
            if (!OppUpdate.isEmpty())
            Database.Update(OppUpdate,false);
        }catch(Exception ex){
            system.debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());
        }      
    }
    
    public void finish(Database.BatchableContext BC) {        
    }
}