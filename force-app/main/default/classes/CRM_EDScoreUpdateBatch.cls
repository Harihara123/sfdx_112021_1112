global class CRM_EDScoreUpdateBatch implements Database.Batchable<Sobject>{
     final String strQuery;
    
     public CRM_EDScoreUpdateBatch(string queryString){
        
        strQuery=queryString;
    }
    
    global database.Querylocator start(Database.BatchableContext context){
       // strQuery = 'Select Id,ED_Time_from_First_Submittal_to_Close__c , Req_Days_Open__c,ED_RefreshCount__c from Opportunity where Id = \'0061E00001E5PsHQAV\'';
        return Database.getQueryLocator(strQuery);          
    }
    global void execute(Database.BatchableContext context , Opportunity[] scope){
        list<Opportunity> OppUpdate = new list<Opportunity>();
        for(Opportunity O:scope){
            if(O.ED_RefreshCount__c == null){
                O.ED_RefreshCount__c=1;
            }else{
                O.ED_RefreshCount__c= O.ED_RefreshCount__c + 1;
               
            }
           
            OppUpdate.add(O);
        }
        if(!OppUpdate.isEmpty())
        update OppUpdate;
    }
    global void finish(Database.BatchableContext context){
    
    }
}