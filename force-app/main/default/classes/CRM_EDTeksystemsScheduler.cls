global class CRM_EDTeksystemsScheduler implements Schedulable{

     global void execute(SchedulableContext sc) 
    {
        
        CRM_EDScoreUpdateBatch b = new CRM_EDScoreUpdateBatch(ed_insights__SDDPredictionConfig__c.getValues('ED Write Back Teksystems Application').ed_insights__Batch_Update_Query__c); 
        database.executebatch(b,100);
    }
}