public class CRM_FilterPicklistValuebyRecType {
    
    public static Map<String, String> getFilterPicklistValues(String objectType, String recordTypeId, String fieldName) {
        //Endpoint
        try{
            String endpoint = 'callout:CRM_Picklist_Recttype_filter'; //URL.getSalesforceBaseUrl().toExternalForm();
            endpoint += '/services/data/v41.0';
            endpoint += '/ui-api/object-info/{0}/picklist-values/{1}/{2}';
            endpoint = String.format(endpoint, new String[]{ objectType, recordTypeId, fieldName });
            EncodingUtil.urlEncode(endpoint,'UTF-8');
            
            //HTTP Request send
            HttpRequest req = new HttpRequest();
            req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId()); 
            req.setEndpoint(endpoint);
            req.setMethod('GET');
            Http http = new Http();
            HTTPResponse res = http.send(req);
            
            //Parse response
            Map<String,String> result = new Map<String,String>();
            Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            if(!root.containsKey('values')){ 
                return result; 
            }
            List<Object> pValues = (List<Object>)root.get('values');
            for(Object pValue : pValues){
                Map<String,Object> pValueMap = (Map<String,Object>)pValue;
                result.put((String)pValueMap.get('value'), (String)pValueMap.get('label'));            
            }
            System.debug(result);
            return result;            
        }catch(Exception ex){
            Map<string,string> PracticeEngagementMap = new Map<string,string>();
            Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectType);
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
            map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
            list<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();
            for (Schema.PicklistEntry disps: values) {
                PracticeEngagementMap.put(disps.getValue(), disps.getLabel());
            } 
            system.debug('--error line No--' + ex.getLineNumber() + '-error Msg-' + ex.getMessage());
            //map<string, string> ErrorResult = new map<string, string>{''=>''};            
            return PracticeEngagementMap;
        } 
        
    }
}