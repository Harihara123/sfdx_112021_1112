@isTest
public class CRM_FilterPicklistValuebyRecType_Test {    
    public static testMethod void testFilterPicklistValuebyRecType(){
        /*String endpoint = 'https://allegisgroup--aprdev19.cs84.my.salesforce.com/services/data/v41.0/ui-api/object-info/Opportunity/picklist-values/012U0000000DWp4IAG/Practice_Engagement__c';
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId()); 
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);*/
        Test.startTest();        
        CRM_FilterPicklistValuebyRecType.getFilterPicklistValues('Opportunity', '012U0000000DWp4IAG','Practice_Engagement__c');
        Test.stopTest();
    }
    @isTest static void testCallout() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());         
        HttpResponse res = new HttpResponse();
        CRM_FilterPicklistValuebyRecType.getFilterPicklistValues('Opportunity', '012U0000000DWp4IAG','Practice_Engagement__c');
        String contentType = res.getHeader('Content-Type');
        String actualValue = res.getBody();
        //{Cisco Program (CP)=Cisco Program (CP)}
        String expectedValue = '{"example":"test"}';
    }
}