public class CRM_HomePageLinkController  {
    
    /*@AuraEnabled public Boolean hideDisplayRVTLink{get;set;}
    @AuraEnabled public String rvtLink{get;set;}
    
    @AuraEnabled
    public static CRM_HomePageLinkController getRVTInfo() {
        CRM_HomePageLinkController hpLinkCtrl = new CRM_HomePageLinkController();
        User usr = [select CompanyName  from User where id =: UserInfo.getUserId()];
        hpLinkCtrl.hideDisplayRVTLink = false;        
        if(usr.CompanyName.contains('Aerotek') || usr.CompanyName.contains('TEK')) {            
            String prodURL = Label.RVT_PROD_URL;
            String sandboxURL = Label.RVT_Sandbox_URL;            
            if(Utilities.isEnvSandbox()) {
                hpLinkCtrl.rvtLink = sandboxURL;
            }
            else {
                hpLinkCtrl.rvtLink = prodURL;
            }            
            hpLinkCtrl.hideDisplayRVTLink = true;
        }		        
        return hpLinkCtrl;
    }
    */
    @AuraEnabled
    public static list<Home_Page_Link__c> getHomePageLinks(String OpCo, String OfficeCode, String ProfileName){
        list<Home_Page_Link__c> UserHomePageLinks = new list<Home_Page_Link__c>();
        for(Home_Page_Link__c x:[Select Id,Company_Name__c,Link__c,Link_Name__c,Office_Code__c,Profile_Name__c,SNo__c 
                                 from Home_Page_Link__c 
                                 where IsActive__c = true and Company_Name__c =:OpCo Order by SNo__c ASC]){
            if(OpCo !=null && x.Company_Name__c == OpCo && x.Office_Code__c == null && x.Profile_Name__c == null){
                UserHomePageLinks.add(x);
            }
            if(OfficeCode !=null && x.Office_Code__c == OfficeCode && x.Profile_Name__c == null){
                UserHomePageLinks.add(x);
            }
            if(ProfileName !=null && x.Profile_Name__c == ProfileName && x.Office_Code__c == null){
                UserHomePageLinks.add(x);
            }
            if(ProfileName !=null && x.Profile_Name__c == ProfileName && OfficeCode !=null && x.Office_Code__c == OfficeCode){
            	UserHomePageLinks.add(x);
            }
        }
        system.debug('--UserHomePageLinks--'+UserHomePageLinks);        
        return UserHomePageLinks;
    }
}