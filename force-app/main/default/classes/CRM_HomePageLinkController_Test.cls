@isTest
public class CRM_HomePageLinkController_Test{
    /*@IsTest
    static void getRVTInfo() {
        Test.startTest();
        User usr = TestDataHelper.createUser('Single Desk 1');
        System.runAs(usr) {
        CRM_HomePageLinkController.getRVTInfo();
        }
        Test.stopTest();
	}*/
    static testMethod Void Test_HomePageLink(){  
        list<Home_Page_Link__c> HPlinks = new list<Home_Page_Link__c>();
        list<string> OpCo = new list<string>{'Aerotek, Inc','TEKsystems, Inc.','AG_EMEA'};
        list<string> Links = new list<string>{'http://gmail.com','http://facebook.com','http://yahoo.com'};
        list<string> LinkName = new list<string>{'Gmail','Facebook','Yahoo'};
        	for(Integer i=0;i < 3; i++){
            	Home_Page_Link__c x = new Home_Page_Link__c();
                x.Company_Name__c = OpCo[i];
                x.IsActive__c = true;
                x.Link__c = Links[i];
                x.Link_Name__c = LinkName[i];
                x.SNo__c = i+1;
                HPlinks.add(x);
            }
        HPlinks[0].Office_Name__c = 'Aerotek';
        HPlinks[2].Profile_Name__c = 'Admin';
        
        insert HPlinks;
        
        Test.startTest();
        User usr = TestDataHelper.createUser('Single Desk 1');
        System.runAs(usr) {
            CRM_HomePageLinkController.getHomePageLinks('TEKsystems, Inc.','','');
            CRM_HomePageLinkController.getHomePageLinks('Aerotek, Inc','Aerotek', null );
            CRM_HomePageLinkController.getHomePageLinks('AG_EMEA','AG_EMEA', 'Admin' );
        }
        Test.stopTest();
    }
}