public class CRM_NumOfSeatsByLocationTriggerHelper{
    
    public void updateOppNumOfResources(Map<Id, Num_of_Seats_by_Location__c>OppMap){
        Integer NumOfResources = 0;
        string OppID;
        Map<id,integer> OppNumResourceMap = new Map<id,integer>();
        list<Opportunity> OppUpdate = new list<Opportunity>();
        if(!OppMap.isEmpty()){
            for(Num_of_Seats_by_Location__c y: OppMap.values()){
                OppID = y.Opportunity__c;
            }            
        }
        if(OppID != null){
            for(Num_of_Seats_by_Location__c x: [Select id,Num_of_Seats__c from Num_of_Seats_by_Location__c where Opportunity__c =: OppID]){
                if(x.Num_of_Seats__c !=null){
                	NumOfResources += integer.valueof(x.Num_of_Seats__c);                     
                }                
            }
            OppNumResourceMap.put(OppID,NumOfResources);
        }
        system.debug('--NumOfResources--'+NumOfResources);
        if(!OppNumResourceMap.isEmpty()){
            for(Opportunity opp:[Select id,Num_of_Resources__c,Apex_Context__c from Opportunity where id =: OppNumResourceMap.keyset()]){
				if(opp.Apex_Context__c == true)
                    opp.Apex_Context__c = false;
                opp.Num_of_Resources__c = OppNumResourceMap.get(opp.Id);
                OppUpdate.add(opp);
            }
        }
        system.debug('--OppUpdate--'+OppUpdate);
        if(!OppUpdate.isEmpty())
            Database.Update(OppUpdate, false);
    }
}