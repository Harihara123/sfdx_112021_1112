public without sharing class CRM_NumSeatsByLocationCntrl{
    @AuraEnabled
    public static list<Num_of_Seats_by_Location__c> getNumSeatsByLocations(string recordId){
        list<Num_of_Seats_by_Location__c> nsl= [select id, Name, Num_of_Seats__c, Location__c, Opportunity__r.Name, Opportunity__r.StageName 
                                                 from Num_of_Seats_by_Location__c 
                                                 where Opportunity__c = :recordId 
                                                 order by lastmodifieddate desc];
        return nsl;
    }
    
    @AuraEnabled
    public static void saveUpdatedSeatsbyLocations(list<Num_of_Seats_by_Location__c> NumSL){
        try{
            system.debug('---NumSL---'+NumSL);
            list<Num_of_Seats_by_Location__c> fixedMappingFields = new list<Num_of_Seats_by_Location__c>(NumSL);        
            list<database.saveResult> saveFieldResults = database.update(fixedMappingFields,false);
            system.debug('--saveFieldResults--'+saveFieldResults);    
        }catch(exception ex){
            system.debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage()); 
            throw new AuraHandledException(ex.getMessage());
        }
    }
	
    @AuraEnabled
    public static list<Competitors_Strongest_to_Weakest__c> getNumCompetitors(string recordId){
        list<Competitors_Strongest_to_Weakest__c> CSW = [select id, Name, Competitor__c, Strongest_to_Weakest__c
                                                 from Competitors_Strongest_to_Weakest__c 
                                                 where Opportunity__c = :recordId 
                                                 order by lastmodifieddate desc];
        return CSW;
    }
	
    @AuraEnabled
    public static void saveCompetitorObj(list<Competitors_Strongest_to_Weakest__c> CSW, list<Decision_Criteria_Strongest_to_Weakest__c> DSO, list<TGS_Differentiation_Strongest_Weakest__c>TDO, string recordId){
        list<Competitors_Strongest_to_Weakest__c> cswToUpdate = new list<Competitors_Strongest_to_Weakest__c>();
        list<Decision_Criteria_Strongest_to_Weakest__c> dsoToUpdate = new list<Decision_Criteria_Strongest_to_Weakest__c>();
        list<TGS_Differentiation_Strongest_Weakest__c> tdoToUpdate = new list<TGS_Differentiation_Strongest_Weakest__c>();
        if(!CSW.isEmpty()){
            for(Competitors_Strongest_to_Weakest__c x: CSW){
                for(integer i=0; i < CSW.size(); i++){
                    if(CSW[i].Competitor__c !=null && CSW[i].Competitor__c !=''){ 
                    	CSW[i].Strongest_to_Weakest__c = string.valueof(i+1);                        
                	}                    
                }
                cswToUpdate.add(x);
            }
        }        
        if(!DSO.isEmpty()){
            for(Decision_Criteria_Strongest_to_Weakest__c s: DSO){
                if(s.Decision_Criteria__c !=null && s.Decision_Criteria__c !=''){
                    Decision_Criteria_Strongest_to_Weakest__c dcsw = new Decision_Criteria_Strongest_to_Weakest__c();
                    dcsw.Decision_Criteria__c = s.Decision_Criteria__c;
                    dcsw.Strongest_to_Weakest__c = string.valueof(s.Strongest_to_Weakest__c);
                    dcsw.Opportunity__c = recordId;
                    dsoToUpdate.add(dcsw);
                }
            }
        }
        if(!TDO.isEmpty()){
            for(TGS_Differentiation_Strongest_Weakest__c s: TDO){
                if(s.TGS_Differentiation__c !=null && s.TGS_Differentiation__c !=''){
                    TGS_Differentiation_Strongest_Weakest__c tdsw = new TGS_Differentiation_Strongest_Weakest__c();
                    tdsw.TGS_Differentiation__c = s.TGS_Differentiation__c;
                    tdsw.Strongest_to_Weakest__c = string.valueof(s.Strongest_to_Weakest__c);
                    tdsw.Opportunity__c = recordId;
                    tdoToUpdate.add(tdsw);
                }
            }
        }
        system.debug('-cswToUpdate size-'+cswToUpdate.size()+'-cswToUpdate-'+cswToUpdate);
        system.debug('-dsoToUpdate size-'+dsoToUpdate.size()+'-dsoToUpdate-'+dsoToUpdate);
        system.debug('-tdoToUpdate size-'+tdoToUpdate.size()+'-tdoToUpdate-'+tdoToUpdate);
        if(!cswToUpdate.isEmpty()){
            update cswToUpdate;
        }
        if(!dsoToUpdate.isEmpty()){
            insert dsoToUpdate;
        }
        if(!tdoToUpdate.isEmpty()){
            insert tdoToUpdate;
        }        
    }
    /*@AuraEnabled
    public static boolean getLocationFromOpportunity(string recordId){
        system.debug('--recordId--'+recordId);
        boolean isLocationPresent = false;
        string Opportunity = [select id, Location__c from Opportunity where id = :recordId].Location__c;
        if(Opportunity !=null)
        isLocationPresent = true;
        system.debug('--isLocationPresent--'+isLocationPresent);
        return isLocationPresent ;
    }*/
}