@isTest(seealldata = false)
public class CRM_NumSeatsByLocationCntrl_Test {
	static testMethod Void Test_NumSeatsByLocation() {     
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        Num_of_Seats_by_Location__c NSL = new Num_of_Seats_by_Location__c(Location__c = 'Local Field Office', Num_of_Seats__c = 10, Opportunity__c = lstOpportunitys[0].id);
        insert NSL;
        
        list<Num_of_Seats_by_Location__c> listNSL = new list<Num_of_Seats_by_Location__c>();
        listNSL.add(NSL);
        
        lstOpportunitys[0].Location__c = 'Dallas Center';
        update lstOpportunitys[0];
        
        Competitors_Strongest_to_Weakest__c CSW = new Competitors_Strongest_to_Weakest__c(Competitor__c = 'Amazon', Strongest_to_Weakest__c = string.valueof('1'), Opportunity__c = lstOpportunitys[0].id);
        insert CSW;
        list<Competitors_Strongest_to_Weakest__c> listCSW = new list<Competitors_Strongest_to_Weakest__c>();
        listCSW.add(CSW);
        Decision_Criteria_Strongest_to_Weakest__c DCW = new Decision_Criteria_Strongest_to_Weakest__c(Decision_Criteria__c = 'Decision 1', Strongest_to_Weakest__c = string.valueof('1'), Opportunity__c = lstOpportunitys[0].id);
        insert DCW;
        list<Decision_Criteria_Strongest_to_Weakest__c> listDCW = new list<Decision_Criteria_Strongest_to_Weakest__c>();
        listDCW.add(DCW);
        TGS_Differentiation_Strongest_Weakest__c DSW = new TGS_Differentiation_Strongest_Weakest__c(TGS_Differentiation__c = 'Differentiation 1', Strongest_to_Weakest__c = string.valueof('1'), Opportunity__c = lstOpportunitys[0].id);
        insert DSW;
        list<TGS_Differentiation_Strongest_Weakest__c> listDSW = new list<TGS_Differentiation_Strongest_Weakest__c>();
        listDSW.add(DSW);
        
        Test.StartTest();
        list<Num_of_Seats_by_Location__c> NumSeatsByLocations = CRM_NumSeatsByLocationCntrl.getNumSeatsByLocations(string.valueof(lstOpportunitys[0].id));
        CRM_NumSeatsByLocationCntrl.saveUpdatedSeatsbyLocations(listNSL);   
        list<Competitors_Strongest_to_Weakest__c> NumCompetitors = CRM_NumSeatsByLocationCntrl.getNumCompetitors(string.valueof(lstOpportunitys[0].id));
        //CRM_NumSeatsByLocationCntrl.saveCompetitorObj(listCSW,listDCW,listDSW,string.valueof(lstOpportunitys[0].id));
        Test.StopTest(); 
    }
    
    static testMethod Void Test_NumSeatsByLocation1() {     
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        Num_of_Seats_by_Location__c NSL = new Num_of_Seats_by_Location__c(Location__c = 'Local Field Office', Num_of_Seats__c = 10, Opportunity__c = lstOpportunitys[0].id);
        insert NSL;
        
        list<Num_of_Seats_by_Location__c> listNSL = new list<Num_of_Seats_by_Location__c>();
        listNSL.add(NSL);
        
        lstOpportunitys[0].Location__c = 'Dallas Center';
        update lstOpportunitys[0];
        
        Competitors_Strongest_to_Weakest__c CSW = new Competitors_Strongest_to_Weakest__c(Competitor__c = 'Amazon', Strongest_to_Weakest__c = string.valueof('1'), Opportunity__c = lstOpportunitys[0].id);
        insert CSW;
        list<Competitors_Strongest_to_Weakest__c> listCSW = new list<Competitors_Strongest_to_Weakest__c>();
        listCSW.add(CSW);
        Decision_Criteria_Strongest_to_Weakest__c DCW = new Decision_Criteria_Strongest_to_Weakest__c(Decision_Criteria__c = 'Decision 1', Strongest_to_Weakest__c = string.valueof('1'), Opportunity__c = lstOpportunitys[0].id);
        //insert DCW;
        list<Decision_Criteria_Strongest_to_Weakest__c> listDCW = new list<Decision_Criteria_Strongest_to_Weakest__c>();
        listDCW.add(DCW);
        TGS_Differentiation_Strongest_Weakest__c DSW = new TGS_Differentiation_Strongest_Weakest__c(TGS_Differentiation__c = 'Differentiation 1', Strongest_to_Weakest__c = string.valueof('1'), Opportunity__c = lstOpportunitys[0].id);
        //insert DSW;
        list<TGS_Differentiation_Strongest_Weakest__c> listDSW = new list<TGS_Differentiation_Strongest_Weakest__c>();
        listDSW.add(DSW);
        
        Test.StartTest();
        //list<Num_of_Seats_by_Location__c> NumSeatsByLocations = CRM_NumSeatsByLocationCntrl.getNumSeatsByLocations(string.valueof(lstOpportunitys[0].id));
        //CRM_NumSeatsByLocationCntrl.saveUpdatedSeatsbyLocations(listNSL);   
        //list<Competitors_Strongest_to_Weakest__c> NumCompetitors = CRM_NumSeatsByLocationCntrl.getNumCompetitors(string.valueof(lstOpportunitys[0].id));
        CRM_NumSeatsByLocationCntrl.saveCompetitorObj(listCSW,listDCW,listDSW,string.valueof(lstOpportunitys[0].id));
        Test.StopTest(); 
    }
}