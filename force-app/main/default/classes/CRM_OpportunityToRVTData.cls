global class CRM_OpportunityToRVTData{  
    @InvocableMethod(label='Fetch RVT data record' 
                     description='This method used to get Opportunity related RVT data record.')
    global static List<Results> fetchRVTData(List<DataContainer> inputData){
        List<RVTData__C> outputs = new List<RVTData__C>();
        List<Results> resultsList = new List<results>();
        Datetime startDateMilestone = Datetime.newInstance(2019, 1, 1);
        Results curResult = new Results();
        	
        if(inputData[0].startDate >= startDateMilestone){
        System.debug(inputData[0].startDate);
        outputs = [SELECT Id,job_level__c,Market_Grade__c,metro__c,Name,occupation_code__c,RVT_Average_Bill_Rate__c,RVT_Median_Bill_Rate__c,
					Worksite_Country__c FROM RVTdata__c WHERE occupation_code__c =:inputData[0].reqRvtOccupationCode  AND job_level__c =:inputData[0].reqJobLevel
					AND metro__c =:inputData[0].metro];
        	if(outputs.Size() > 0){
                curResult.rvtBillRate = outputs[0].RVT_Median_Bill_Rate__c;
                curResult.rvtSuggestion = 'Based on the data in the system, the median bill rate for similar occupation / experience level / metro is $'+outputs[0].RVT_Median_Bill_Rate__c.setScale(2, RoundingMode.HALF_UP)+'.\n '+'Consider updating the Req Max Bill Rate.'+'\n'+ 'Would you like to view options for updating the Max Bill Rate?';
           		curResult.isTeamMember = validateTeamMember(inputData[0].recordId, inputData[0].OwnerId);
        		if(!curResult.isTeamMember)
                  curResult.rvtSuggestion = 'Based on the data in the system, the median bill rate for similar occupation/experience level/metro is $'+outputs[0].RVT_Median_Bill_Rate__c.setScale(2, RoundingMode.HALF_UP)+'. \n\n Is this Information Useful?';
			}else{
                 curResult.isTeamMember = false;
                 curResult.rvtBillRate = 0;
                 curResult.rvtSuggestion = '';
            }
        }else{
                 curResult.isTeamMember = false;
                 curResult.rvtBillRate = 0;
                 curResult.rvtSuggestion = '';
        }	            
      		resultsList.add(curResult);
            return resultsList;
    }

global class DataContainer {
	@InvocableVariable
	public string reqJobLevel;
	@InvocableVariable
	public string metro;
	@InvocableVariable
	public string reqRvtOccupationCode;
	@InvocableVariable
	public string recordId;
    @InvocableVariable
    global DateTime startDate;
    @InvocableVariable
    global string ownerId;
}

global class Results {
      @InvocableVariable
      global Decimal rvtBillRate;
      @InvocableVariable
      global String rvtSuggestion;
      @InvocableVariable
      global Boolean isTeamMember;
      
    }
    
    public static boolean validateTeamMember(String recordId,String ownerId){
    List<OpportunityTeamMember> oppTeamMembers = new List<OpportunityTeamMember>(); 
        System.debug(ownerId+'== '+Userinfo.getUserId());
        System.debug(ownerId == Userinfo.getUserId());
    if(ownerId == Userinfo.getUserId().substring(0,15)) 
        return true;
    oppTeamMembers = [Select Id FROM OpportunityTeamMember WHERE OpportunityId =:recordId AND UserId =:Userinfo.getUserId() ];
    
     return (oppTeamMembers.size() > 0 ? true : false);
   }
}