public class CRM_PositionCardCtrl {
    
    /* Check if loggedin user is Owner / Member of the opportunity */
    @AuraEnabled											
    public static boolean allowUnlossUnwash(ID opportunityId) {
        
        Opportunity opp = [Select Id, StageName, OwnerId from Opportunity where Id =: opportunityId Limit 1];
        
        if(opp.StageName == 'Staging'){
            return false;
        }
        
        if( opp.ownerId == UserInfo.getUserId() ){
            return true;
        }else{
            List<OpportunityTeamMember> teamMembers = [select Id, UserId, OpportunityId from OpportunityTeamMember 
                                                                   Where OpportunityId =: opportunityId];
            for(OpportunityTeamMember tm : teamMembers){
                if(tm.UserId == UserInfo.getUserId() ){
            		return true;        
                }
            }
            
        }
        
        return false;
    }
    
    @AuraEnabled											
    public static List<Position__c> getOpportunityPositionData(ID opportunityId) {
        List<Position__c> lstOppPositions = [Select id, Name, PositionStage__c, PositionStatus__c, ReasonforChange__c, Status_Date__c,
                                            	CreatedDate, LastModifiedDate, Opportunity__r.IsClosed, Opportunity__r.Req_Prev_Stage_Name_c__c 
                                            FROM Position__c WHERE Opportunity__c =: opportunityId
                                            ORDER BY PositionStatus__c];
        if(!lstOppPositions.isEmpty()){
            return lstOppPositions;
        } else {
            return null;
        }
    }

    @AuraEnabled											
    public static List<String> getReasonPicklist() {
        List<String> optionlist = new List<String>();
        
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get('Position__c').getDescribe().fields.getMap(); 
        List<Schema.PicklistEntry> picklistValues = field_map.get('ReasonforChange__c').getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }

        return optionlist;
    }
    
    
    @AuraEnabled											
    public static list<string> editPositionStatuses(String opportunityId, 
                                               String fromStatus, 
                                               String toStatus, 
                                               Integer numberOfPositions, 
                                               String reason,
											   String radioLabel) {
        
        List<Position__c> lstPositions = new List<Position__c>();
		list<string> responseList = new list<string>();
		list<string> orderId = new list<string>();
        list<Order> OrdeList = new list<Order>([SELECT Id, Status, Submittal_Not_Proceeding_Reason__c, MovetoNP_Flag__c FROM Order WHERE OpportunityId =:opportunityId]);
        for ( Position__c pos : [ Select id, PositionStage__c, PositionStatus__c, ReasonforChange__c, Status_Date__c
                                            	FROM Position__c WHERE Opportunity__c =: opportunityId
                                             	AND PositionStatus__c =: fromStatus
                                             	ORDER BY createddate
                                             	Limit: numberOfPositions ]
        ) {
            pos.PositionStatus__c = toStatus;
            pos.ReasonforChange__c = reason;

            if (toStatus == 'Wash' || toStatus == 'Loss' || 
                (toStatus == 'Open' && (fromStatus == 'Wash' || fromStatus == 'Loss'))) {
                pos.Status_Date__c = System.today();
            }

            lstPositions.add( pos );
        }    

        if( !lstPositions.isEmpty() ) { 
            savePoint sp  = Database.setSavepoint();
            try{
                if(radioLabel == 'updateSubStages'){
                    MoveSubmittalstoNP.processRecordsfromPostionObj(opportunityId);
                    if(!OrdeList.isEmpty() && OrdeList.size() > 100)
                    responseList.add('batchUpdate');
                }
                UPDATE lstPositions;
                
                //Adding Total Loss and Total Wash fields - Srini (8/30/2019) 
                Integer totalLosses = 0;
                Integer totalWashes = 0;
                if(numberOfPositions!=null){
                    List<AggregateResult> arList = new List<AggregateResult>([SELECT PositionStatus__c, Count(Id)pos FROM Position__c WHERE Opportunity__c=:opportunityId GROUP BY PositionStatus__c]);
                    for(AggregateResult ar: arList){
                        if(ar.get('PositionStatus__c')=='Loss'){
                        	totalLosses = (Integer)ar.get('pos');
                        }
                        if(ar.get('PositionStatus__c')=='Wash'){
                        	totalWashes = (Integer)ar.get('pos');
                        }
                    }
                }                
                System.Debug('Total Loss - '+totalLosses);
                System.Debug('Total Washes - '+totalWashes);
                    
                Opportunity opp = [Select Id, OwnerId, Name, Opportunity_Num__c, Req_Loss_Wash_Reason__c, Req_Total_Lost__c, Req_Total_Washed__c, Req_Prev_Stage_Name_c__c, StageName, LastModifiedBy.Name from Opportunity where Id =: opportunityId Limit 1];
                opp.Req_Total_Lost__c = totalLosses;
                opp.Req_Total_Washed__c = totalWashes;
                if(reason!=null){
                    opp.Req_Loss_Wash_Reason__c = reason;
                }                
                String stage = opp.StageName;        
                if(toStatus=='open' && stage.contains('Closed')){
                    if(opp.Req_Prev_Stage_Name_c__c=='Draft') {
                       opp.StageName = 'Draft';
                    } else {
                        opp.StageName ='Qualified';
                    }
                }
                opp.Apex_Context__c = true;
                
                update opp;
                //Publishing Order Events
                if(radioLabel == 'reqOpenDisclaimerBox'){
                    orderId.add(opportunityId);
                    DRZReOpenOpportunityOrderEvent.ReOpenOpportunitiyOrder(orderId);
                }
                //End
                List<id> memberUsers = new List<id>();
                if( fromStatus == 'wash' || fromStatus == 'loss' ){
                    if( fromStatus == 'wash' ){
                        fromStatus = 'washed';
                    }
                    if( fromStatus == 'loss' ){
                        fromStatus = 'lost';
                    }
          
                    //Send Email To All Members and Owner | where UserId =: UserInfo.getUserId() and 
                    for(OpportunityTeamMember tm : [select Id, userId from OpportunityTeamMember 
                                                    Where OpportunityId =: opportunityId]){
                         memberUsers.add(tm.userId);
                    }
                    
                    memberUsers.add( opp.OwnerId );
                    
                    EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where developername='Position_Email' limit 1];
            		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            		
                    for(Id recipient  : memberUsers){  
                        
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        String messageBody = emailTemplate.Body;    
                        messageBody = messageBody.replace('{count}',  String.valueOf( numberOfPositions) );
                        messageBody = messageBody.replace('{status}', fromStatus );
                        messageBody = messageBody.replace('{opp_name}', opp.Name);
                        messageBody = messageBody.replace('{opp_num}', opp.Opportunity_Num__c);
                        messageBody = messageBody.replace('{opp_last_modified_by}', opp.LastModifiedBy.Name );
                        messageBody = messageBody.replace('{link}', URL.getSalesforceBaseUrl().toExternalForm()+'/'+ opportunityId );
                        
                        mail.setSubject( 'A Req you are a team member of has positions re-opened' );
                        mail.setPlainTextBody(messageBody);
                        mail.setSenderDisplayName('System Admin');
                        mail.setTemplateId(emailTemplate.Id);
                        mail.setSaveAsActivity(false);
                        mail.setTargetObjectId(recipient);
                        mails.add(mail);
                    }
                    
                    if( mails.size() > 0 && !Test.isRunningTest() ){
                    	Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(mails);    
                    }
              		
                    
                }
                responseList.add('true');
                return responseList; 
                
            }catch(Exception e){
                Database.rollback(sp);
  				throw new AuraHandledException('There was an error, Please Contact System Administrator: ' + e.getMessage() );
            }          
        }
                                                   
        return responseList;                                                   
                                                   
    }
    

}