@isTest 
public class CRM_PositionCardCtrl_Test {
    
    
    public static testmethod void Test_getOpportunityPositionData(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account Acc1 = CreateTalentTestData.createTalent();
        Contact talCont = CreateTalentTestData.createTalentContact(Acc1);
        Acc1.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
   		Update Acc1;

        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
            Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
            Jobcode_Id__c	='700009',	OpCo_Id__c='ONS', Segment_Id__c	='A_AND_E',	Segment__c = 'Architecture and Engineering',
            OpCo_Status__c = 'A', Skill_Id__c	='TIVOLI', Skill__c = 'Tivoli' );
		insert prd;
		
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        
        Opportunity oppty = new Opportunity( Name =  'accName', LDS_Account_Name__c = Acc1.id, Accountid = Acc1.id,                
                    RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc',
					Division__c = 'EASi Engineering', Legacy_Product__c = prd.Id, BusinessUnit__c ='EASI', Response_Type__c = 'Formal RFx',
                    stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required',
                    Customers_Application_or_Project_Scope__c = 'Precisely Defined', Impacted_Regions__c='Option1',
                    Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required',
                    Services_Capabilities_Alignment__c = 'TEK frequently provides', Sales_Resource_Requirements__c = 'Less than',
                    CloseDate = system.today()+1,Req_Job_Description__c = 'Test', Req_Qualification__c = 'Test',
                    Req_Product__c = 'Contract', Req_Bill_Rate_Max__c=10, Req_Bill_Rate_Min__c=1, Req_Duration_Unit__c = 'Day(s)',
                    Req_Duration__c=10, Req_Pay_Rate_Max__c = 10, Req_Pay_Rate_Min__c = 1, Req_Standard_Burden__c = 10, Req_Rate_Frequency__c='Hourly');
        insert oppty;
        
        CRM_PositionCardCtrl.allowUnlossUnwash(oppty.id);
        oppty.StageName='Staging';
        update oppty;
        CRM_PositionCardCtrl.allowUnlossUnwash(oppty.id);
        
        CRM_PositionCardCtrl.getReasonPicklist();
        
        CRM_PositionCardCtrl obj = new CRM_PositionCardCtrl();
        Opportunity opp = [SELECT ID FROM Opportunity LIMIT 1];
        List<Position__c> lst = CRM_PositionCardCtrl.getOpportunityPositionData(opp.ID);
    }
    
    public static testmethod void Test_getReasonPicklist(){
        CRM_PositionCardCtrl.getReasonPicklist();
    }
    
    public static testmethod void Test_editPositionStatuses(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account Acc1 = CreateTalentTestData.createTalent();
        Contact talCont = CreateTalentTestData.createTalentContact(Acc1);
        Acc1.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
   		Update Acc1;

        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
            Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
            Jobcode_Id__c	='700009',	OpCo_Id__c='ONS', Segment_Id__c	='A_AND_E',	Segment__c = 'Architecture and Engineering',
            OpCo_Status__c = 'A', Skill_Id__c	='TIVOLI', Skill__c = 'Tivoli' );
		insert prd;
		
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        
        Opportunity oppty = new Opportunity( Name =  'accName', LDS_Account_Name__c = Acc1.id, Accountid = Acc1.id,                
                    RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc',
					Division__c = 'EASi Engineering', Legacy_Product__c = prd.Id, BusinessUnit__c ='EASI', Response_Type__c = 'Formal RFx',
                    stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required',
                    Customers_Application_or_Project_Scope__c = 'Precisely Defined', Impacted_Regions__c='Option1',
                    Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required',
                    Services_Capabilities_Alignment__c = 'TEK frequently provides', Sales_Resource_Requirements__c = 'Less than',
                    CloseDate = system.today()+1,Req_Job_Description__c = 'Test', Req_Qualification__c = 'Test',
                    Req_Product__c = 'Contract', Req_Bill_Rate_Max__c=10, Req_Bill_Rate_Min__c=1, Req_Duration_Unit__c = 'Day(s)',
                    Req_Duration__c=10, Req_Pay_Rate_Max__c = 10, Req_Pay_Rate_Min__c = 1, Req_Standard_Burden__c = 10, Req_Rate_Frequency__c='Hourly');
        insert oppty;
        
        Position__c p = new Position__c( PositionStatus__c = 'open', Opportunity__c = oppty.id);
        insert p;
        Position__c p1 = new Position__c( PositionStatus__c = 'wash', Opportunity__c = oppty.id);
        insert p1;
        
      	test.startTest();                     
        CRM_PositionCardCtrl.editPositionStatuses( oppty.ID, 'wash', 'loss', 1, 'Other','');
        //CRM_PositionCardCtrl.editPositionStatuses( oppty.ID, 'wash', 'open', 1, 'Other');
        test.stopTest();
    }
    
}