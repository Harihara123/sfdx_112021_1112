global class CRM_PositionCardToOrderBatchUpdate implements Database.Batchable <sObject>{
    private Set<Id> OpptyIdSet {get; set;}	
    private String UserId {get; set;}
    private String Context {get; set;}
    global String strQuery;
    public CRM_PositionCardToOrderBatchUpdate(Set<Id> OpptyIdSet, string UserId, string Context){
        this.OpptyIdSet = OpptyIdSet;
        this.UserId = UserId;
        this.Context = Context;
        if(Context == 'ApexCntrl' || Context == 'AutoCloseReq'){
            //strQuery = 'SELECT Id, Status, Submittal_Not_Proceeding_Reason__c, MovetoNP_Flag__c FROM Order WHERE OpportunityId = \''+OpptyId+'\'';
			strQuery = 'SELECT Id, Status, Submittal_Not_Proceeding_Reason__c, MovetoNP_Flag__c FROM Order WHERE OpportunityId IN :OpptyIdSet';
        }else if(Context == 'Anonymous'){
            strQuery = System.Label.CRM_Position_Update_On_Demand_Query;
        }
    }
    public Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('--strQuery--'+strQuery);
        return Database.getQueryLocator(strQuery);
    }
    public void execute(Database.BatchableContext BC, List<Order> scope) {
        List<Order> updateSubmittals = new List<Order> ();
        if(!scope.isEmpty()){
            for (Order od : scope){
                if(od.Status != 'Offer Accepted' && od.Status != 'Started' && od.Status != 'Not Proceeding' && od.Status.left(14) != 'Not Proceeding' && Context == 'AutoCloseReq'){
                    od.Status = 'Not Proceeding - ' + od.Status;
                    od.Submittal_Not_Proceeding_Reason__c = 'Other';
                    updateSubmittals.add(od);
                }
                else if (od.Status != 'Offer Accepted' && od.Status != 'Started' && od.Status != 'Not Proceeding' && od.Status != 'Linked' && od.Status != 'Applicant' && od.Status.left(14) != 'Not Proceeding') {
                    od.Status = 'Not Proceeding - ' + od.Status;
                    od.Submittal_Not_Proceeding_Reason__c = 'Auto-update due to successful Placement';
                    updateSubmittals.add(od);
                }                
            }
            system.debug('--updateSubmittals--'+updateSubmittals);
            if(!updateSubmittals.isEmpty()){
                Database.update(updateSubmittals,false);
            }
        }
    }
    public void finish(Database.BatchableContext BC) {        
    }
}