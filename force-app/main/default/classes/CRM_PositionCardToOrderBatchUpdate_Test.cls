@isTest
public class CRM_PositionCardToOrderBatchUpdate_Test {
    testmethod static void coverBatch() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);
        Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity' , Accountid = newClientAcc.id, Req_Total_Positions__c = 12,
                                                     RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                                                     stagename = 'Draft', Impacted_Regions__c='Option1', Access_To_Funds__c = 'abc', CloseDate = system.today()+1,
                                                     Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,
                                                     Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,
                                                     Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly', Req_Total_Filled__c = 3, 
                                                     Req_Total_Lost__c = 2 , Req_Total_Washed__c = 2 );
        Database.insert(NewOpportunity);
        TestData tdConts = new TestData();
        string rectype='Talent';
        List<Contact> TalentcontLst = tdConts.createContacts(rectype);
        string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
        Order newOrder = new Order(Name = 'New Order', EffectiveDate = system.today()+1,
                                   Status ='Submitted',AccountId = NewOpportunity.AccountId, OpportunityId = NewOpportunity.Id,shiptocontactid=TalentcontLst[0].id, recordtypeid = OrderRecordType);
        Insert newOrder;  
        Test.startTest();
		Set<Id>OpptyIdSet=new Set<Id>();
		OpptyIdSet.add(NewOpportunity.Id);
        CRM_PositionCardToOrderBatchUpdate objBatch = new CRM_PositionCardToOrderBatchUpdate(OpptyIdSet,'','ApexCntrl');
        Database.executeBatch(objBatch);
        Test.stopTest();
    }
}