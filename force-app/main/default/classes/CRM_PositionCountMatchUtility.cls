public class CRM_PositionCountMatchUtility{
    static List<Position__c> positionsToAdd = new List<Position__c>();
    static List<Position__c> positionsToRemove = new List<Position__c>();
    static List<Position__c> positionsToUpdate = new List<Position__c>();
    
    public static void syncPositionCount(Map<Id, Opportunity> oppMap){
        if(UserInfo.getProfileId().substring(0,15) != Label.CRM_Batch_Integration_User){
              positionsToAdd = new List<Position__c>();
              positionsToRemove = new List<Position__c>();  
              positionsToUpdate = new List<Position__c>();                
          List<Log__c> errors = new List<Log__c>();
           try{
            String reqRecordTypeId = Utility.getRecordTypeId('Opportunity', 'Req');
            //Map<Id,Map<String,List<Position__c>> mapOppPositions = new Map<Id,Map<String,List<Position__c>>();
            List<Opportunity> allOp = [SELECT Id,Req_Open_Positions__c,Req_Total_Filled__c,Req_Total_Lost__c,Req_Total_Washed__c,isClosed, lastModifiedDate,CloseDate,Req_Loss_Wash_Reason__c ,(SELECT Id, Opportunity__c, PositionStatus__c FROM Positions__r Order By lastModifiedDate ) FROM Opportunity 
                                    WHERE RecordTypeId =: reqRecordTypeId AND Id IN:oppMap.keySet()];
            /*for(Opportunity opp : [SELECT Id,Req_Open_Positions__c,Req_Total_Filled__c,Req_Total_Lost__c,Req_Total_Washed__c,isClosed, lastModifiedDate,CloseDate,Req_Loss_Wash_Reason__c ,(SELECT Id, Opportunity__c, PositionStatus__c FROM Positions__r Order By lastModifiedDate ) FROM Opportunity 
                                    WHERE RecordTypeId =: reqRecordTypeId AND Id IN:oppMap.keySet()]){
                           if(!opp.Positions__r.IsEmpty()){*/
               for(Opportunity opp:allOp){
                Integer c=0;
                for(Position__c p:opp.Positions__r) 
                    c++;
                if(c>0){
                System.debug('test Req_Total_Filled__c '+opp.Req_Total_Filled__c );
                    Map<String,List<Position__c>> mapPositionStatus = new Map<String,List<Position__c>>{'Open' => new List<Position__c>(), 'Fill' => new List<Position__c>(), 'Loss' => new List<Position__c>(), 'Wash' => new List<Position__c>()};
                    Map<String,Decimal> mapPositionStatusCount = new Map<String,Decimal>{'Open' => opp.Req_Open_Positions__c, 'Fill' => opp.Req_Total_Filled__c, 'Loss' => opp.Req_Total_Lost__c, 'Wash' => opp.Req_Total_Washed__c};
                    for(Position__c ps : opp.Positions__r){
                        List<Position__c> tempPSList = new List<Position__c>();
                        if(mapPositionStatus.containsKey(ps.PositionStatus__c)){
                            tempPSList = mapPositionStatus.get(ps.PositionStatus__c);
                            if(mapPositionStatusCount.containsKey(ps.PositionStatus__c) && tempPSList.size() < mapPositionStatusCount.get(ps.PositionStatus__c))
                                tempPSList.add(ps);
                            else
                                positionsToRemove.add(ps);
                        }else{
                            tempPSList.add(ps);
                        }
                        mapPositionStatus.put(ps.PositionStatus__c, tempPSList);
                    }
                    //mapOppPositions(opp.Id,mapPositionStatus);
                    addOrRemovePositions(opp,mapPositionStatus);
                }else if(opp.Req_Open_Positions__c  > 0 ){
                    addPositions(opp,'Open',opp.Req_Open_Positions__c);
                }
            }
        
        
    
        if(!positionsToAdd.isEmpty())
            insert positionsToAdd;
        if(!positionsToRemove.isEmpty())
            delete positionsToRemove;
        if(!positionsToUpdate.isEmpty())
            update positionsToUpdate;
       }catch (Exception e){
            errors.add(Core_Log.logException(e));
            database.insert(errors,false);
        }
       }     
    }
    
    
    public static void addOrRemovePositions(Opportunity opp, Map<String,List<Position__c>> mapPositionStatus){
        for(String positionType : mapPositionStatus.keySet()){
            List<Position__c> tempPositionList = mapPositionStatus.get(positionType);
            Integer psMapSize = tempPositionList.Size();
            if(positionType == 'Open'){
                if( opp.Req_Open_Positions__c >  psMapSize){
                    addPositions(opp,positionType,(opp.Req_Open_Positions__c - psMapSize));
                }else if( opp.Req_Open_Positions__c < psMapSize ){
                    for (Integer i = 0 ; i < psMapSize - opp.Req_Open_Positions__c; i++){
                        positionsToRemove.add(tempPositionList[i]);
                    }
                }
            }else if(positionType == 'Fill'){
                if( opp.Req_Total_Filled__c >  psMapSize){
                    addPositions(opp,positionType,(opp.Req_Total_Filled__c - psMapSize));
                }else if( opp.Req_Total_Filled__c < psMapSize ){
                    for (Integer i = 0 ; i < psMapSize - opp.Req_Total_Filled__c; i++){
                        positionsToRemove.add(tempPositionList[i]);
                    }
                }
            }else if(positionType == 'Loss'){
                if( opp.Req_Total_Lost__c >  psMapSize){
                    addPositions(opp,positionType,(opp.Req_Total_Lost__c - psMapSize));
                }else if( opp.Req_Total_Lost__c < psMapSize ){
                    for (Integer i = 0 ; i < psMapSize - opp.Req_Total_Lost__c; i++){
                        positionsToRemove.add(tempPositionList[i]);
                    }
                }
            }else if(positionType == 'Wash'){
                if( opp.Req_Total_Washed__c >  psMapSize){
                    addPositions(opp,positionType,(opp.Req_Total_Washed__c - psMapSize));
                }else if( opp.Req_Total_Washed__c < psMapSize ){
                    for (Integer i = 0 ; i < psMapSize - opp.Req_Total_Washed__c; i++){
                        positionsToRemove.add(tempPositionList[i]);
                    }
                }
            }
        }
    }

    public static void addPositions(Opportunity opp, String positionType, Decimal newPositionCount){
           Datetime createdDate;
           Datetime lastModifiedDate;
           Decimal positionCount = 0;
           
            if(opp.isClosed) {
                createdDate = opp.CloseDate;
                lastModifiedDate = opp.CloseDate;
            } else {
                createdDate = opp.lastModifiedDate;
                lastModifiedDate = opp.lastModifiedDate;
            }

            if (newPositionCount <= positionsToRemove.size()) {
                positionCount = newPositionCount;
            } else {
                positionCount = newPositionCount - positionsToRemove.size();
            }

            while (positionsToRemove.size() > 0 && newPositionCount != 0) {            
                Position__c newPosition = positionsToRemove[0];                
                newPosition.PositionStatus__c = positionType;

                if (positionType == 'Wash' || positionType == 'Loss') {
                    newPosition.ReasonforChange__c = opp.Req_Loss_Wash_Reason__c;    
                }                

                positionsToUpdate.add(newPosition);  
                positionsToRemove.remove(0);
                newPositionCount = newPositionCount - 1;              
            }               
           for (Integer i = 0 ; i < newPositionCount; i++) {
                
                Position__c newPosition = new Position__c();
                newPosition.PositionStatus__c = positionType; 
                newPosition.opportunity__c = opp.id;
                newPosition.createdDate = createdDate;
                newPosition.lastModifiedDate = lastModifiedDate;

                if (positionType == 'Fill') { 
                    newPosition.Status_Date__c = System.today(); 
                } else if (positionType == 'Wash' || positionType == 'Loss') {
                    newPosition.ReasonforChange__c = opp.Req_Loss_Wash_Reason__c;
                }
                                                          
              positionsToAdd.add(newPosition);
            }
        
              
    }

     public static void washPositions(Map<Id, Opportunity> oppMap){  
    
        String reqRecordTypeId = Utility.getRecordTypeId('Opportunity', 'Req');
        List<Opportunity> allOp = [SELECT Id,IsAutoClose__c, Req_Open_Positions__c,Req_Total_Washed__c,isClosed,Req_Loss_Wash_Reason__c ,Req_Total_Lost__c,Req_Total_Filled__c,(SELECT Id, Opportunity__c, PositionStatus__c,ReasonforChange__c  FROM Positions__r Order By lastModifiedDate ) FROM Opportunity 
                                    WHERE RecordTypeId =: reqRecordTypeId AND Id IN:oppMap.keySet()];     
               
        List<Position__c> updatePosList = new List<Position__c> ();
        Map<Id,Opportunity> oppUpdate = new Map<Id,Opportunity>(); 
        for(Opportunity oppty:allOp){
           List<Position__c> updatePos = new List<Position__c> ();
            for(Position__c p:oppty.Positions__r){
                if(p.PositionStatus__c == 'Open'){
                	p.ReasonforChange__c = 'Time-out';
                	p.PositionStatus__c = 'Wash';
                	updatePos.add(p);  
                	}
                else if(p.PositionStatus__c == 'Wash'){
                    updatePos.add(p);                    
                    }
                }
                updatePosList.addAll(updatePos);
                oppty.Req_Total_Washed__c = updatePos.size();                
                oppty.Req_Loss_Wash_Reason__c = 'Time-out';
                oppUpdate.put(oppty.id, oppty); 
            }
             
         
         if(updatePosList.size() > 0)
            	 {
            		update updatePosList; 
            	 }
                     
             if(!updatePosList.isEmpty() && !oppUpdate.isEmpty() ){
                    //Database.update(oppUpdate,false);
                    update oppUpdate.values();
                } 
        }



}