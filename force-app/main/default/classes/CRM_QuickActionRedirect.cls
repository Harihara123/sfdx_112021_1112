public without sharing class CRM_QuickActionRedirect{
      public CRM_QuickActionRedirect(ApexPages.StandardController controller) 
    {
    }
    public PageReference doRedirect(){
          String accountId = ApexPages.currentPage().getParameters().get('relatedId');
          String office = ApexPages.currentPage().getParameters().get('office');
          String opco = ApexPages.currentPage().getParameters().get('opco');
          String Oppty = ApexPages.currentPage().getParameters().get('oppty');
          // This query is used for calling quickAction from new button CreditApp__c
          User currentUser = [Select id, OpCo__c, Companyname from User where Id =: userinfo.getuserId() LIMIT 1];
          
          if(opco == null){
              opco = currentUser.Companyname;
          }
          
          String routeURL = '';
          if(opco == 'Aerotek, Inc' || opco == 'MLA' || currentUser.OpCo__c == 'MLA' || opco == 'Inside Edge' || opco == 'MarketSource'){
              routeURL = '/apex/CRM_QuickActionsEverywhere?action=Create_Credit_App_Aerotek&relatedId='+accountId+'&relatedField=Account__c&office='+office+'&opco='+opco+'&oppty='+Oppty;
          }
          else{
              routeURL = '/apex/CRM_QuickActionsEverywhere?action=Create_Credit_App_TEKsystems&relatedId='+accountId+'&relatedField=Account__c&office='+office+'&opco='+opco+'&oppty='+Oppty;
          }
          PageReference pageRef = new PageReference(routeURL);
          pageRef.setRedirect(true);
          return pageRef;
    }
}