/* @author Jeremiah Dohn
 * @description Used for quick actions everywhere.
 * @version 1.0
 * @license BSD 3-Clause License
 *      Copyright (c) 2018, Jeremiah Dohn
 *      All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, this
 *      list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *    * Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
public with sharing class CRM_QuickActionsEverywhereController {
    
    public static final String OBJECT_QUERY_ERROR = 'You must include an object with your query.';
    public static final String OBJECT_SELECT_ERROR = 'Your select clause must include at least one field.';
    
    private static final string apiVersion = 'v45.0';
    
    // Coded for returning user preferences method due to errors with inserting user preferences in test classes
    private final static List<UserPreference> upref1 = new List<UserPreference>{new UserPreference(Preference = '57',UserId = UserInfo.getUserId(),Value = '15')};
    private final static List<UserPreference> upref2 = new List<UserPreference>{new UserPreference(Preference = '58',UserId = UserInfo.getUserId(),Value = '120')};
    
    /*
     * @description Retrieves the quick action information based on API Name.  
     *              Global actions are formatted as only the API Name, such as "New_Task" and 
     *              object specific actions are formatted as the SobjectType, a period and then the developer name, such as "Contact.Create_Contact"
     */
    @AuraEnabled
    public static string describeAvailableQuickAction(String quickActionApiName){
        //return (CRM_UtilityClass.describeAvailableQuickAction(quickActionApiName));
        String quickActionName = '';
        try{
            quickActionName = CRM_UtilityClass.describeAvailableQuickAction(quickActionApiName);
            if(test.isRunningTest()){
                integer i = 0/10 ;
            }
        }catch(Exception e){
            quickActionName = e.getMessage();
            throw new AuraHandledException(e.getMessage());
        }
        return quickActionName;
    }
    
    /*
     * @description Retrieves the task and event preferences for a user.
     *                          Values are outlined here - https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_userpreference.htm?search_text=userpreference
     */
    @AuraEnabled
    public static string retrieveUserTaskPreferences(string preferenceType){
        //return CRM_UtilityClass.retrieveUserTaskPreferences(preferenceType);
        String preference = '';
        try{
            preference = CRM_UtilityClass.retrieveUserTaskPreferences(preferenceType);
            if(test.isRunningTest()){
                integer i = 0/10 ;
            }
        }catch(Exception e){
            preference = e.getMessage();
            throw new AuraHandledException(e.getMessage());
        }
        return preference;
    }
    
    /*
     * @description Sets the labels for initialization of a lookup field
     */
    @AuraEnabled
    public static string retrieveThisRecordValues(string obj, string searchValue, string fieldList){
        //return CRM_UtilityClass.retrieveThisRecordValues(obj,searchValue,fieldList);
        String recordValue = '';
        try{
            recordValue = CRM_UtilityClass.retrieveThisRecordValues(obj,searchValue,fieldList);
            if(test.isRunningTest()){
                integer i = 0/10 ;
            }
        }catch(Exception e){
            recordValue = e.getMessage();
            throw new AuraHandledException(e.getMessage());
        }
        return recordValue;
    }
    
    /*
     * @description Retrieves related records to parent for update mass actions.
     */
    @AuraEnabled
    public static string retrieveRelatedRecords(string searchValue, string obj, string relatedField, string fieldList){
        //return CRM_UtilityClass.retrieveRelatedRecords(searchValue, obj, relatedField, fieldList);
        String recordValue = '';
        try{
            recordValue = CRM_UtilityClass.retrieveRelatedRecords(searchValue, obj, relatedField, fieldList);
            if(test.isRunningTest()){
                integer i = 0/10 ;
            }
        }catch(Exception e){
            recordValue = e.getMessage();
            throw new AuraHandledException(e.getMessage());
        }
        return recordValue;
    }
    
  
    /*
     * @description Used for determining if the quick action is available for the user.
     */
    @AuraEnabled
    public static boolean isRecordTypeAvailable(String recordTypeId, string obj){
       // return CRM_UtilityClass.isRecordTypeAvailable(recordTypeId, obj);
       Boolean isRecordType;
        try{
            isRecordType = CRM_UtilityClass.isRecordTypeAvailable(recordTypeId, obj);
            if(test.isRunningTest()){
                integer i = 0/10 ;
            }
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        return isRecordType;
        
    }
    
    /*
     * @description Retrieves the search layout for the lookup field and caches the result.
     */
    @AuraEnabled
    public static string retrieveSearchLayout(string sobjectType){
        // return CRM_UtilityClass.retrieveSearchLayout(sobjectType);
        String searchLayout = '';
        system.debug('--sobjectType--'+sobjectType);
        try{
            searchLayout = CRM_UtilityClass.retrieveSearchLayout(sobjectType);
            if(test.isRunningTest()){
                integer i = 0/10 ;
            }
        }catch(Exception e){
            searchLayout = e.getMessage();
            throw new AuraHandledException(e.getMessage());
        }
        return searchLayout;
    }
    
    /*
     * @description Performs a SOSL search.
     */
    @AuraEnabled
    public static string performSearch(string searchString, string sobjectType, string returningFields, string whereClause, integer limitClause){
        if(sobjectType == 'Contact'){
            String recType = 'CSA';
            whereClause = ' Contact_Active__c = true and recordtype.Name = \'' + recType + '\'';
        }
        if(sobjectType == 'Account'){
            String recType = 'Client';
            whereClause = ' recordtype.Name = \'' + recType + '\'';
        }
        //return CRM_UtilityClass.performSearch(searchString,sobjectType,returningFields,whereClause,limitClause);
        String searchResult = '';
        try{
            searchResult = CRM_UtilityClass.performSearch(searchString,sobjectType,returningFields,whereClause,limitClause);
            if(test.isRunningTest()){
                integer i = 0/10 ;
            }
        }catch(Exception e){
            searchResult = e.getMessage();
            throw new AuraHandledException(e.getMessage());
        }
        return searchResult;
    }
    
    // Shout out to SFDCFox - using super fast schema describe
    // https://salesforce.stackexchange.com/questions/218982/why-is-schema-describesobjectstypes-slower-than-schema-getglobaldescribe/219010#219010
    @AuraEnabled
    public static string describeSobjects(List<String> objList, boolean includeFields){
        Map<String, objectDescribeResult> dsr = new Map<String, objectDescribeResult>();
        for (String obj : objList) {
            if(obj != 'Calendar'){ // Assigned To Id on events are related to calendars which is not a valid sobject type
                SObjectType r = ((SObject)(Type.forName('Schema.'+obj).newInstance())).getSObjectType();
                DescribeSObjectResult d = r.getDescribe();
                objectDescribeResult odr = new objectDescribeResult(d, includeFields);
                dsr.put(odr.name, odr);
            }
        }
        return JSON.serialize(dsr);
    }
    
    /*
     * @description Used to get Icon information for the application.
     */
    @AuraEnabled
    public static string describeIcons(){
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.qaeRestCache');
        String cachedTabs = (String)orgPart.get('tabs');
        
        if(cachedTabs != null){
            return cachedTabs;
        } else{
            Http h = new Http();
            HttpRequest req = new HttpRequest();  
            req.setEndpoint('callout:qae_REST_API/' + apiVersion + '/theme');
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('GET');
            HttpResponse res = h.send(req);
            if(res.getStatus() == 'OK'){
                orgPart.put('tabs', (string)res.getBody());
            }
            
            return res.getBody();
        }
    }
    
    /*
     * @description Describe default values for the quick action.  Is called every time there are default values to be set on the action.
     */
    @AuraEnabled
    public static string describeDefaultValues(String obj, string action, boolean isGlobal){
        string calloutUrl = '';
        if(isGlobal){
            calloutUrl = '/quickActions/' + action + '/defaultValues';
        } else{
            calloutUrl = '/sobjects/' + obj  + '/quickActions/' + action + '/defaultValues';
        }
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();  
        req.setEndpoint('callout:qae_REST_API/' + apiVersion + calloutUrl);
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('GET');
        HttpResponse res = h.send(req);
        
        return res.getBody();
    }
    
    /*
     * @description Sets the labels for initialization of a lookup field
     */
     @RemoteAction 
    public static saveResultList saveThisRecord(string obj, String sobj, string redirectValue, string quickActionType){
        saveResultList sr = new saveResultList();
        if(redirectValue == null || sobj == null){
            sr.errorMsg = 'Values not provided to save.';
            return sr;
        } else {
            string schemaDescribe = describeSobjects(new List<String>{obj}, true);
            
            Map<string, objectDescribeResult> odr =  (Map<string, objectDescribeResult>) JSON.deserialize(schemaDescribe, Map<string, objectDescribeResult>.class);
            Sobject sobjSerialized =  (Sobject) JSON.deserialize(sobj, Sobject.class);
            Map<String, Object> fieldsToValue = sobjSerialized.getPopulatedFieldsAsMap();
            SObjectType dsr = ((SObject)(Type.forName('Schema.'+obj).newInstance())).getSObjectType();
            sObject sObjToCommit = dsr.newSobject();
            List<Sobject> sobjList = new List<Sobject>();
            for(string s : fieldsToValue.keySet()){
                if(odr.get(obj).fields.containsKey(s) && odr.get(obj).fields.get(s).updateable && quickActionType == 'Update'){
                                        sObjToCommit.put(s, fieldsToValue.get(s));
                } else if(s == 'Id'){
                    sObjToCommit.put('Id', fieldsToValue.get('Id'));
                } else if(odr.get(obj).fields.containsKey(s) && odr.get(obj).fields.get(s).createable  && quickActionType != 'Update'){
                                        sObjToCommit.put(s, fieldsToValue.get(s));
                }
            }
            
            sObjList.add(sobjToCommit);
            
            if(quickActionType == 'Update'){
                try{
                    Database.SaveResult[] srRes = database.update(sObjList);
                    sr.saveResult = srRes;
                } catch(exception e){
                    sr.errorMsg = e.getMessage();
                }
                return sr;
            } else {
                try{
                    Database.SaveResult[] srRes = database.insert(sObjList);
                    sr.saveResult = srRes;
                } catch(exception e){
                    sr.errorMsg = e.getMessage();
                }
                return sr;
            }
        }
    }
  
    /*
     * @description Saves record and returns in a JSON friendly format.
     */
    @AuraEnabled
    public static string saveThisRecordLightning(string obj, String sobj, string redirectValue, string quickActionType){
        return JSON.serialize(saveThisRecord(obj, sobj, redirectValue, quickActionType));
    }
    
    /*
     * @description Gets base URL for links
     */
    @AuraEnabled
    public static string getBaseURL(){
        return System.URL.getSalesforceBaseUrl().toExternalForm();
    }
    
    /*
     * @description Redirects to the value provided to the method or the homepage if the record is missing.
     */
    public static PageReference redirect(){
        string value = Apexpages.currentPage().getParameters().get('value');
        
        if(value != 'null'){
            PageReference pageRef = new PageReference('/' + value);
            pageRef.setRedirect(true);
            return pageRef;
        } else{
            string defaultUrl = (UserInfo.getUiThemeDisplayed() == 'Theme3' ? '/home/home.jsp' : '/lightning/page/home');
            PageReference pageRef = new PageReference(defaultUrl);
            pageRef.setRedirect(true);
            return pageRef;
        }
    }
    
    @AuraEnabled
    public static List<String> setUserData(){
        List<String> lstUserVal = new List<String>();
        User currentUser = [select id, name, opco__c, Office__c, Division__c, companyname,Office_Code__c from User where id =: userinfo.getuserid() Limit 1];
        List<User_Organization__c> lstOrg = [select id, Name, OpCo_Code__c, OpCo_Name__c,Office_Code__c from User_Organization__c where Active__c = true and recordtype.name = 'Office' and Office_Code__c =: currentUser.Office_Code__c and OpCo_Code__c =: currentUser.opco__c];
        OPCO_Picklist_Mapping__c opco = OPCO_Picklist_Mapping__c.getValues(currentUser.opco__c);
        //lstUserVal.add(opco.Picklist_Value__c);
        String opcoValue = '';
        Schema.DescribeFieldResult fieldResultOpco = Credit_App__c.opco__c.getDescribe();
        List<Schema.PicklistEntry> ple2 = fieldResultOpco.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple2){
            if(currentUser.companyname == pickListVal.getLabel())
                opcoValue = pickListVal.getLabel();
            if(currentUser.opco__c == 'MLA')
                opcoValue = 'MLA';    
            if(currentUser.companyname == 'Aerotek, Inc')
                opcoValue = 'Aerotek';    
            if(currentUser.companyname == 'TEKsystems, Inc.')
                opcoValue = 'TEKsystems';        
        }
        lstUserVal.add(opcoValue);
        
        if(lstOrg.size() > 0){
            lstUserVal.add(lstOrg[0].Id);
        }else{
            lstUserVal.add('No Office Found');
        }    
        String division = '';
        Schema.DescribeFieldResult fieldResult = Credit_App__c.division__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            if(currentUser.Division__c == pickListVal.getLabel())
                division = pickListVal.getLabel();
        }
		string Owner = string.valueof(currentUser.id);
        lstUserVal.add(division);
		lstUserVal.add(Owner);
        return lstUserVal;
    }
    // Have to make a wrapper for describes or a javalang error will occur:
    // System.JSONException: (was java.lang.NullPointerException) (through reference chain: common.api.soap.wsdl.DescribeSObjectResult["listviewable"])
    public class objectDescribeResult{
        public string name {get;set;}
        public string keyPrefix {get;set;}
        public string label {get;set;}
        public Map<String, field> fields {get;set;}
        
 
        public objectDescribeResult(DescribeSObjectResult d, boolean includeFields) {
            name = d.getName();
            keyPrefix = d.getKeyPrefix();
            label = d.getLabel();
            
            if(includeFields){
                // Describe fields
                fields = new Map<String, field>();
                Map<String, Schema.SobjectField> dfrMap = d.fields.getMap();
                for(String fr : dfrMap.keyset()){
                    field f = new field(dfrMap.get(fr).getDescribe());
                    fields.put(f.name, f);
                }
            }
        }
    }

    public class field{
        public string name {get;set;}
        public string label {get;set;}
        public boolean accessible{get;set;}
        public boolean createable{get;set;}
        public boolean updateable{get;set;}
        public string fieldType{get;set;}
        public List<String> referenceTo{get;set;}

        public field(Schema.DescribeFieldResult field){
            this.name = field.getName();
            this.label = field.getLabel();
            this.fieldType = String.valueOf(field.getType()); 
            this.accessible = field.isAccessible();
            this.createable = field.isCreateable();
            this.updateable = field.isUpdateable();
            this.referenceTo = new List<String>();
            for(Schema.SobjectType s : field.getReferenceTo()){
                referenceTo.add(s.getDescribe().getName());
            }
        }
    } 
    
    public class queryResult{
        public String error {get;set;}
        public List<Sobject> result {get;set;}
    }
                         
    public class searchResult{
        public string errorMsg {get;set;}
        public string label {get;set;}
        public string limitRows {get;set;}
        public string objectType {get;set;}
        public List<searchColumns> searchColumns {get;set;}
   }
                 
   public class saveResultList{
       public string errorMsg {get;set;}
       public List<Database.SaveResult> saveResult {get;set;}
   }
    
   public class searchColumns{
       public string field {get;set;}
       public string format {get;set;}
       public string label {get;set;}
       public string name {get;set;}
    }     
    
}