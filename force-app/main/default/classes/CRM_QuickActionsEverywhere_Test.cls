/* @author Jeremiah Dohn
* @description Used for quick actions everywhere.
* @version 1.0
* @license BSD 3-Clause License
*      Copyright (c) 2018, Jeremiah Dohn
*      All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are met:
*
*    * Redistributions of source code must retain the above copyright notice, this
*      list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*
*    * Neither the name of the copyright holder nor the names of its
*      contributors may be used to endorse or promote products derived from
*      this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
@isTest(seeAllData=false)
private class CRM_QuickActionsEverywhere_Test {
    
    // Note: While typically bulk data testing should be conducted, since the nature of this application is transactional and is one record save at a time, bulk testing is not conducted.
    //       If and when this application supports mass data inserts / updates, such a test I would so make.
    
    private final static string LOG_A_CALL = 'LogACall';
    private final static string ACC_NAME = 'My Awesome Account';
    private final static string ACC_BILLINGSTREET = '1234 Greenbury Lane';
    private final static string ACC_BILLINGSTATE = 'Nebraska';
    private final static string ACC_BILLINGPOSTALCODE = '68140';
    private final static string ACC_BILLINGCOUNTRY = 'United States';
    private final static string OPP_NAME = 'Arkaham Widgets';
    
    @isTest
    static void describeAvailableQuickAction(){
        
        Test.startTest();
        
        String daqa = CRM_QuickActionsEverywhereController.describeAvailableQuickAction(LOG_A_CALL);
        system.assertNotEquals(null, daqa);
        List<Object> daqaDeserialized = (List<Object>) json.deserializeUntyped(daqa);
        Map<String,Object> daqaMap = (Map<String,Object>) daqaDeserialized[0];
        system.assertEquals(LOG_A_CALL, daqaMap.get('name'), 'log created');
        
        Test.stopTest();
    }
    
    @isTest
    static void retrieveUserTaskPreferences(){
        
        // System will not allow insert of user preferences even with system.runAs current testing user. As a result, the variables are coded to return properly in Test Context.
        
        Test.startTest();
        
        String upref1Json = CRM_QuickActionsEverywhereController.retrieveUserTaskPreferences('Task');
        String upref2Json = CRM_QuickActionsEverywhereController.retrieveUserTaskPreferences('Event');
        List<UserPreference> upref1List = (List<UserPreference>) json.deserialize(upref1Json, List<UserPreference>.class);
        List<UserPreference> upref2List = (List<UserPreference>) json.deserialize(upref2Json, List<UserPreference>.class);
        system.assertEquals('15', upref1List[0].Value, 'log created');
        system.assertEquals('120', upref2List[0].Value, 'log created');
        
        Test.stopTest();
    }
    
    @isTest
    static void retrieveThisRecordValues(){
        //retrieveThisRecordValues(string obj, string searchValue, string fieldList)
        
        Account a = new Account(Name=ACC_NAME, BillingStreet=ACC_BILLINGSTREET,BillingState=ACC_BILLINGSTATE,BillingPostalCode=ACC_BILLINGPOSTALCODE,BillingCountry=ACC_BILLINGCOUNTRY);
        insert a;
        
        Test.startTest();
        String retrieveResult1 = CRM_QuickActionsEverywhereController.retrieveThisRecordValues('Account', a.Id, 'Name,BillingStreet,BillingState,BillingPostalCode,BillingCountry');
        CRM_QuickActionsEverywhereController.QueryResult qr1 = (CRM_QuickActionsEverywhereController.QueryResult) JSON.deserialize(retrieveResult1, CRM_QuickActionsEverywhereController.QueryResult.class);
        system.assertEquals(null, qr1.error, 'log created');
        system.assertEquals(1, qr1.result.size(), 'log created');
        system.assertEquals(ACC_NAME, qr1.result[0].get('Name'));
        system.assertEquals(ACC_BILLINGSTREET, qr1.result[0].get('BillingStreet'), 'BillingStreet');
        system.assertEquals(ACC_BILLINGSTATE, qr1.result[0].get('BillingState'), 'State');
        system.assertEquals(ACC_BILLINGPOSTALCODE, qr1.result[0].get('BillingPostalCode'),'Code');
        //system.assertEquals(ACC_BILLINGCOUNTRY, qr1.result[0].get('BillingCountry'));
        
        String retrieveResult2 = CRM_QuickActionsEverywhereController.retrieveThisRecordValues('Account', a.Id, null);
        CRM_QuickActionsEverywhereController.QueryResult qr2 = (CRM_QuickActionsEverywhereController.QueryResult) JSON.deserialize(retrieveResult2, CRM_QuickActionsEverywhereController.QueryResult.class);
        system.assertEquals(null, qr2.error);
        system.assertEquals(1, qr2.result.size(), 'size');
        system.assertEquals(ACC_NAME, qr2.result[0].get('Name'), 'account name');
        system.assertEquals(null, qr2.result[0].get('BillingStreet'), 'street');
        system.assertEquals(null, qr2.result[0].get('BillingState'), 'state');
        system.assertEquals(null, qr2.result[0].get('BillingPostalCode'), 'code');
        system.assertEquals(null, qr2.result[0].get('BillingCountry'), 'country');
        
        Test.stopTest();
    }
    
    @isTest
    static void retrieveRelatedRecords(){
        //retrieveRelatedRecords(string searchValue, string obj, string relatedField, string fieldList)
        
        Account a = new Account(Name=ACC_NAME, BillingStreet=ACC_BILLINGSTREET,BillingState=ACC_BILLINGSTATE,BillingPostalCode=ACC_BILLINGPOSTALCODE,BillingCountry=ACC_BILLINGCOUNTRY);
        insert a;
        
        Opportunity o = new Opportunity(Name=OPP_NAME,AccountId=a.Id, CloseDate=System.today() + 20, StageName='1. Identifying Decisionmaker');
        insert o;
        
        Test.startTest();
        String retrieveResult1 = CRM_QuickActionsEverywhereController.retrieveRelatedRecords(a.Id,'Opportunity','AccountId', 'Name,BillingStreet,BillingState,BillingPostalCode,BillingCountry');
        CRM_QuickActionsEverywhereController.QueryResult qr1 = (CRM_QuickActionsEverywhereController.QueryResult) JSON.deserialize(retrieveResult1, CRM_QuickActionsEverywhereController.QueryResult.class);
        system.assertEquals(null, qr1.error);
        system.assertEquals(1, qr1.result.size());
        system.assertEquals(OPP_NAME, qr1.result[0].get('Name'));
        
        String retrieveResult2 = CRM_QuickActionsEverywhereController.retrieveRelatedRecords(a.Id,'Opportunity','AccountId', null);
        CRM_QuickActionsEverywhereController.QueryResult qr2 = (CRM_QuickActionsEverywhereController.QueryResult) JSON.deserialize(retrieveResult2, CRM_QuickActionsEverywhereController.QueryResult.class);
        system.assertEquals(null, qr2.error, 'error');
        system.assertEquals(1, qr2.result.size(), 'size');
        system.assertEquals(OPP_NAME, qr2.result[0].get('Name'), 'account name');
        
        Test.stopTest();
    }
    
    
  
    
    @isTest
    static void isRecordTypeAvailable(){
        //isRecordTypeAvailable(String recordTypeId, string obj)
        
        Test.startTest();
        
        boolean recType = CRM_QuickActionsEverywhereController.isRecordTypeAvailable(null, 'Account');
        system.assertEquals(false, recType, 'rectype');
        
        SObjectType r = ((SObject)(Type.forName('Schema.Account').newInstance())).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        if(rtMapById != null){
            String recordTypeId = (String) new list<Id>(rtMapById.keyset())[0];
            boolean expected = rtMapById.get(recordTypeId).isAvailable();
            boolean recTypeIfAvail = CRM_QuickActionsEverywhereController.isRecordTypeAvailable(recordTypeId, 'Account');
            system.assertEquals(expected, recTypeIfAvail, 'access');
        }
        
        Test.stopTest();
    }
    
    @isTest
    static void performSearch(){
        //performSearch(string searchString, string sobjectType, string returningFields, string whereClause, integer limitClause)
        Account a = new Account(Name=ACC_NAME, BillingStreet=ACC_BILLINGSTREET,BillingState=ACC_BILLINGSTATE,BillingPostalCode=ACC_BILLINGPOSTALCODE,BillingCountry=ACC_BILLINGCOUNTRY);
        insert a;
        
        Opportunity o = new Opportunity(Name=OPP_NAME,AccountId=a.Id, CloseDate=System.today() + 20, StageName='1. Identifying Decisionmaker');
        insert o;
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = o.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        
        Test.startTest();
        
        string result = CRM_QuickActionsEverywhereController.performSearch(ACC_NAME, 'Opportunity', 'Id,AccountId,Account.Name,Account.Parent.Name,Name', null, null);
        string result1 = CRM_QuickActionsEverywhereController.performSearch(ACC_NAME, 'Account', 'Id,Name', null, null);
        string result2 = CRM_QuickActionsEverywhereController.performSearch(ACC_NAME, 'Contact', 'Id,Name', null, null);
        List<Sobject> sobj = (List<Sobject>) json.deserialize(result, List<Sobject>.class);
        system.assertEquals(1, sobj.size(), 'siz');
        system.assertEquals(o.Id, sobj[0].get('Id'), 'ID');
        system.assertEquals(a.Id, sobj[0].get('AccountId'), 'accountid');

        Test.stopTest();
    }
   
   
   
    @isTest
    static void saveThisRecordLightning(){
        //saveThisRecordLightning(string obj, String sobj, string redirectValue, string quickActionType)
        Test.startTest();
        string sr1String = CRM_QuickActionsEverywhereController.saveThisRecordLightning('Account', JSON.serialize(new Account(Name=ACC_NAME)), 'parent', 'Create');
        CRM_QuickActionsEverywhereController.saveResultList sr1 = (CRM_QuickActionsEverywhereController.saveResultList) JSON.deserialize(sr1String, CRM_QuickActionsEverywhereController.saveResultList.class);
        system.assertEquals(null, sr1.errorMsg, 'error');
        
        Account acc = [SELECT Id, Name FROM Account Limit 1][0];
        system.assertEquals(acc.Name, ACC_NAME, 'ac name');
        acc.Name = 'My new acc name';
        
        string sr2String = CRM_QuickActionsEverywhereController.saveThisRecordLightning('Account', JSON.serialize(acc), 'parent', 'Update');
        CRM_QuickActionsEverywhereController.saveResultList sr2 = (CRM_QuickActionsEverywhereController.saveResultList) JSON.deserialize(sr2String, CRM_QuickActionsEverywhereController.saveResultList.class);
        system.assertEquals(null, sr2.errorMsg, 'error');
        
        acc = [SELECT Id, Name FROM Account  Limit 1][0];
        system.assertEquals('My new acc name', acc.Name, 'account name');
        
        Test.stopTest();
    }
    
    @isTest
    static void saveThisRecordLightning_NullVals(){
        Test.startTest();
        
        string sr1String = CRM_QuickActionsEverywhereController.saveThisRecordLightning('Task', null, 'parent', 'Create');
        CRM_QuickActionsEverywhereController.saveResultList sr1 = (CRM_QuickActionsEverywhereController.saveResultList) JSON.deserialize(sr1String, CRM_QuickActionsEverywhereController.saveResultList.class);
        system.assertEquals('Values not provided to save.', sr1.errorMsg, 'error');
        
        string sr2String = CRM_QuickActionsEverywhereController.saveThisRecordLightning('Task', JSON.serialize(new Account(Name=ACC_NAME)), null, 'Create');
        CRM_QuickActionsEverywhereController.saveResultList sr2 = (CRM_QuickActionsEverywhereController.saveResultList) JSON.deserialize(sr2String, CRM_QuickActionsEverywhereController.saveResultList.class);
        system.assertEquals('Values not provided to save.', sr2.errorMsg, 'errorr');
        
        Test.stopTest();
    }
    
    @isTest
    static void getBaseURL(){
        //getBaseURL()
        system.assertEquals(System.URL.getSalesforceBaseUrl().toExternalForm(), CRM_QuickActionsEverywhereController.getBaseURL(), 'the url');
    }
    
    @isTest
    static void redirect(){
        //redirect() 
        Account a = new Account(Name=ACC_NAME, BillingStreet=ACC_BILLINGSTREET,BillingState=ACC_BILLINGSTATE,BillingPostalCode=ACC_BILLINGPOSTALCODE,BillingCountry=ACC_BILLINGCOUNTRY);
        insert a;
        
        Test.startTest();
        
        PageReference pageRef = Page.CRM_quickActionsEverywhere;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('value',a.id);
        PageReference redirect = CRM_QuickActionsEverywhereController.redirect();
        system.assertNotEquals(redirect, pageRef, 'thepage');
        
        string defaultUrl = (UserInfo.getUiThemeDisplayed() == 'Theme3' ? '/home/home.jsp' : '/lightning/page/home');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('value',null);
        redirect =  CRM_QuickActionsEverywhereController.redirect();
        //system.assert(redirect.getUrl().contains(defaultUrl), 'redirect');
        
        Test.stopTest();
    }
    
    @isTest
    static void CRM_QuickRedirect(){
        //redirect() 
        Account a = new Account(Name=ACC_NAME, BillingStreet=ACC_BILLINGSTREET,BillingState=ACC_BILLINGSTATE,BillingPostalCode=ACC_BILLINGPOSTALCODE,BillingCountry=ACC_BILLINGCOUNTRY);
        insert a;
        
        Test.startTest();
        CRM_QuickActionRedirect dir = new CRM_QuickActionRedirect(new ApexPages.StandardController(a));
        PageReference pageRef = Page.CRM_QuickActionRedirector;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('value',a.id);
        PageReference redirect = dir.doRedirect();
        dir.doRedirect();
        Test.stopTest();
    }
    
    @isTest
    static void setUserData_Test(){
        User usr = TestDataHelper.createUser('Single Desk 1');
        usr.opco__c = 'MLA';
        Test.startTest();
        List<String> lstUser1 = CRM_QuickActionsEverywhereController.setUserData();
        Test.stopTest();
    }
    
    //@isTest
    static testMethod void describeDefaultValues_Test(){
        String responseJSON = '{ "statusCode": "400 - Success", "statusMessage": "Request Successfully Processed"}';
        String contentType = 'application/json';
        Integer statusCode = 400;
        
        calloutMockResponse mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        Test.setMock(HttpCalloutMock.class, mkResponse);
        
        mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        Test.startTest();
        string val = CRM_QuickActionsEverywhereController.describeDefaultValues('Opportunity','Update',True);
        Test.stopTest();
    } 
    
    //@isTest
    static testMethod void retrieveSearchLayout_Test(){
        String responseJSON = '{ "statusCode": "400 - Success", "statusMessage": "Request Successfully Processed"}';
        String contentType = 'application/json';
        Integer statusCode = 400;
        
        calloutMockResponse mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        Test.setMock(HttpCalloutMock.class, mkResponse);
        
        mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        Test.startTest();
        string val = CRM_QuickActionsEverywhereController.retrieveSearchLayout('Opportunity');
        Test.stopTest();
    }
    
    // @isTest
    static testMethod void describeIcons_Test(){
        String responseJSON = '{ "statusCode": "400 - Success", "statusMessage": "Request Successfully Processed"}';
        String contentType = 'application/json';
        Integer statusCode = 400;
        
        calloutMockResponse mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        Test.setMock(HttpCalloutMock.class, mkResponse);
        
        mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        Test.startTest();
        string val1 = CRM_QuickActionsEverywhereController.describeIcons();
        Test.stopTest();
    }
}