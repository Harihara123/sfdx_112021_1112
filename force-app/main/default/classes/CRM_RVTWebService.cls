public class CRM_RVTWebService {

    @future(callout = true)
    public static void SOACallout(Id oppId, String jobTitle, String jobDescription){
        
		JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('OpportunityID',oppId);
        gen.writeStringField('SessionID',UserInfo.getSessionId());
        gen.writeStringField('ServerURL','www.salesforce.com');
        gen.writeStringField('JobTitle',jobTitle);
        gen.writeStringField('Description', jobDescription);
        gen.writeEndObject();
        String reqBody = gen.getAsString();
            
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new http();        
        req.setEndpoint('http://allegistest.com/');
        req.setMethod('GET');
        req.setBody(reqBody);
        req.setCompressed(true);

        System.Debug('Callout Message: '+reqBody);        
        try{
            res = http.send(req);

        }
        catch(System.CalloutException e){
            System.Debug('SOA Callout Error: '+e);
        }
        
    }

//    public static testMethod void OutboundTest() {
//        CRM_RVTWebService.SOACallout('O-543535', 'Test Engineer', 'This is a test');
//    }
    
}