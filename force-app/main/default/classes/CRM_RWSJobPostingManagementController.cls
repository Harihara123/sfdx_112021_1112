public class CRM_RWSJobPostingManagementController {
    
    @AuraEnabled
    public static  RWSJobPostingResponse massrefreshJobPostings(List<String> jobPostingIdList){
        
        String postings = String.join(jobPostingIdList,';');
        postings = postings.replaceAll('\\s','');
        postings = postings.replace('[', '');
        postings = postings.replace(']','');
        System.Debug('Postings '+postings);
        List<String> newPostings = postings.split(';');
        
        List<String> refreshList=new List<String>();
        RWSJobPostingResponse response=new RWSJobPostingResponse();
        Job_Posting__c jpc=null; 
        System.debug('RefreshJobPostings----->'+jobPostingIdList);
   
        if(jobPostingIdList!=null && jobPostingIdList.size()>0){
            System.Debug('Inside the loop');
            User usr=[select id,Peoplesoft_Id__c  from User where id =:Userinfo.getUserId()];
            List<Job_Posting__c> jobPostingList=[select id,Source_System_id__c from Job_Posting__c where id in:newPostings];
            RWSJobPostingRequest request=new RWSJobPostingRequest();
        	request.hrEmplId=usr.Peoplesoft_Id__c;
            request.postingIdList= new List<String>();
            
            List<String> jpIdList=new List<String>();
                   for(Job_Posting__c jp:jobPostingList){
                        String jobPostingId=jp.Source_System_id__c;
                        String rwsJobPostingId='';
                        if(String.isNotBlank(jobPostingId)){
                            rwsJobPostingId=jobPostingId.replaceAll('R.','');
                        }
                        jpIdList.add(rwsJobPostingId);   
                }
            request.postingIdList.addAll(jpIdList);
            response=RWSJobPostingRestCallout.refreshJobPosting(request);
            if(response!=null ){
            	RWSJobPostingRestCallout.updateProgress(response,jpc);
            }
        }
        System.debug('Mass Refresh Response from RWS--->'+response);
        
        return response;
        
    }
/*    
    @AuraEnabled    
    public static RWSJobPostingResponse massinActivateJobPostings(List<String> jobPostingIdList){

	    String postings = String.join(jobPostingIdList,';');
        postings = postings.replaceAll('\\s','');        
        postings = postings.replace('[', '');
        postings = postings.replace(']','');
        System.Debug('Postings '+postings);
        List<String> newPostings = postings.split(';'); 
        
        List<String> inActivateList=new List<String>();
        RWSJobPostingResponse response=new RWSJobPostingResponse();
        Job_Posting__c jpc=null; 
        System.debug('InactivateJobPostings----->'+newPostings);
        
        if(jobPostingIdList!=null && jobPostingIdList.size()>0){
            User usr=[select id,Peoplesoft_Id__c  from User where id =:Userinfo.getUserId()];
            List<Job_Posting__c> jobPostingList=[select id,Source_System_id__c, Name, eQuest_Job_Id__c, Status_Id__c,Posting_Status__c, opco__c, OwnerId from Job_Posting__c where id in:newPostings];
            RWSJobPostingRequest request=new RWSJobPostingRequest();
        	request.hrEmplId=usr.Peoplesoft_Id__c;
            request.postingIdList= new List<String>();
            
            List<String> jpSysIdList=new List<String>();
			Map<ID, Job_Posting__c> jpMap = new Map<ID, Job_Posting__c>();
            for(Job_Posting__c jp:jobPostingList){
				String jobPostingId=jp.Source_System_id__c;
				String rwsJobPostingId='';
 
				if(String.isNotBlank(jobPostingId)){
					rwsJobPostingId=jobPostingId.replaceAll('R.','');
					jpSysIdList.add(rwsJobPostingId);   
				}
				else {
					jpMap.put(jp.Id, jp);
				}
				
            }           
            
            if(jpSysIdList.size() > 0) {//inactive job posting created from RWS
				 request.postingIdList.addAll(jpSysIdList);
				response=RWSJobPostingRestCallout.inactivateJobPosting(request);
           		if(response!=null ){
            		RWSJobPostingRestCallout.updateProgress(response, jpc);
				}
			}
			if(jpMap.size() > 0) {//inactive job posting created from salesforce
				Map<Id, String> recStatusMap = JobPostingFormController.massInactivateJobPosting(jpMap);		
				RWSJobPostingResponse rep;
				if(response!=null ){
					rep = inactiveResponse(response, jpMap.values(), recStatusMap);					
				}		
				else {
					rep = inactiveResponse(new RWSJobPostingResponse(), jpMap.values(), recStatusMap);
				}
				response = rep;
			}
           
        }
        
        return response;
    }
	*/   
	//@author : Neel Kamal
	//@Date : 09/22/20
	//preparing reponse Model for Salesforce related Job Posting record status
	private static RWSJobPostingResponse inactiveResponse(RWSJobPostingResponse rep, List<Job_Posting__c> jpIdList, Map<Id, String> recStatusMap){
		RWSJobPostingResponse jpRep = new RWSJobPostingResponse();
		List<RWSJobPostingResponse.PostingIdList> repPostingList = new List<RWSJobPostingResponse.PostingIdList>();
		RWSJobPostingResponse.PostingIdList repPosting = new RWSJobPostingResponse.PostingIdList();

		for(Job_Posting__c jp : jpIdList){
			repPosting = new RWSJobPostingResponse.PostingIdList();
			repPosting.postingId = jp.Name;
			
			String status = recStatusMap.get(jp.Id);
			if(status == 'DIFF_OWNER') {
				repPosting.message = 'DIFF_OWNER';
			}
			else if(status == 'ALREADY_INACTIVE'){
				repPosting.message = 'ALREADY_INACTIVE';
			}
			else if(status == 'SUCCESS'){
				repPosting.message = 'SUCCESS';
			}
			
			repPostingList.add(repPosting);
		}
		jpRep.postingIdList = repPostingList;
		jpRep.sfdcMsg = 'SUCCESS';

		if(rep != null && rep.postingIdList != null) {
			rep.postingIdList.addAll(jpRep.postingIdList);
			rep.sfdcMsg = 'SUCCESS';
		}
		else {
			rep = jpRep;
		}
		return rep;
	}

    @AuraEnabled
    public static  RWSJobPostingResponse refreshJobPostings(List<String> jobPostingIdList){
        
        List<String> refreshList=new List<String>();
        RWSJobPostingResponse response=new RWSJobPostingResponse();
        Job_Posting__c jpc=null; 
        System.debug('RefreshJobPostings----->'+jobPostingIdList);
   
        if(jobPostingIdList!=null && jobPostingIdList.size()>0){
            User usr=[select id,Peoplesoft_Id__c  from User where id =:Userinfo.getUserId()];
            Map<Id,Job_Posting__c> jobPostingMap=new Map<Id,Job_Posting__c>((List<Job_Posting__c>)Database.query('select id,Source_System_id__c from Job_Posting__c  where id in :jobPostingIdList '));
            RWSJobPostingRequest request=new RWSJobPostingRequest();
        	request.hrEmplId=usr.Peoplesoft_Id__c;
            request.postingIdList= new List<String>();
            
            List<String> jpIdList=new List<String>();
            if(jobPostingMap!=null && jobPostingMap.size()>0 && jobPostingMap.containsKey(jobPostingIdList.get(0))){
                String jobPostingId=jobPostingMap.get(jobPostingIdList.get(0)).Source_System_id__c;
                jpc=jobPostingMap.get(jobPostingIdList.get(0));
                String rwsJobPostingId='';
                if(String.isNotBlank(jobPostingId)){
                    rwsJobPostingId=jobPostingId.replaceAll('R.','');
                }
            	jpIdList.add(rwsJobPostingId);   
            }
            
            request.postingIdList.addAll(jpIdList);
            response=RWSJobPostingRestCallout.refreshJobPosting(request);
            if(jpc!=null && response!=null ){
            	RWSJobPostingRestCallout.updateProgress(response,jpc);
            }
        }
        System.debug('Response from RWS--->'+response);
        
        return response;
        
    }

    @AuraEnabled    
    public static RWSJobPostingResponse inActivateJobPostings(List<String> jobPostingIdList){
        
        List<String> inActivateList=new List<String>();
        RWSJobPostingResponse response=null;
        Job_Posting__c jpc=null; 
        if(jobPostingIdList!=null && jobPostingIdList.size()>0){
            User usr=[select id,Peoplesoft_Id__c  from User where id =:Userinfo.getUserId()];
            Map<Id,Job_Posting__c> jobPostingMap=new Map<Id,Job_Posting__c>((List<Job_Posting__c>)Database.query('select id,Source_System_id__c from Job_Posting__c  where id in :jobPostingIdList '));
            RWSJobPostingRequest request=new RWSJobPostingRequest();
        	request.hrEmplId=usr.Peoplesoft_Id__c;
            request.postingIdList= new List<String>();
            
            
             List<String> jpIdList=new List<String>();
            if(jobPostingMap!=null && jobPostingMap.size()>0 && jobPostingMap.containsKey(jobPostingIdList.get(0))){
                String jobPostingId=jobPostingMap.get(jobPostingIdList.get(0)).Source_System_id__c;
                String rwsJobPostingId='';
                jpc=jobPostingMap.get(jobPostingIdList.get(0));
                if(String.isNotBlank(jobPostingId)){
                    rwsJobPostingId=jobPostingId.replaceAll('R.','');
                }
            	jpIdList.add(rwsJobPostingId);   
            }
            
            
            
            request.postingIdList.addAll(jpIdList);
            response=RWSJobPostingRestCallout.inactivateJobPosting(request);
           	if(jpc!=null && response!=null ){
            	RWSJobPostingRestCallout.updateProgress(response, jpc);
            }
        }
        
        return response;
    }
    
    @AuraEnabled    
    public static RWSJobPostingResponse transferJobPostings(String ownerId,List<String> jobPostingIdList){
        
        List<String> inActivateList=new List<String>();
        RWSJobPostingResponse response=null;
         Job_Posting__c jpc=null; 
        if(jobPostingIdList!=null && jobPostingIdList.size()>0){
            List<String> userIdList=new List<String>();
            userIdList.add(ownerId);
            userIdList.add(UserInfo.getUserId());
            Map<Id,User> usrMap=new Map<Id,User>([select id,Peoplesoft_Id__c  from User where id =:userIdList]);
            Map<Id,Job_Posting__c> jobPostingMap=new Map<Id,Job_Posting__c>((List<Job_Posting__c>)Database.query('select id,Source_System_id__c from Job_Posting__c  where id in :jobPostingIdList '));
            RWSJobPostingRequest request=new RWSJobPostingRequest();
            if(usrMap.containsKey(userInfo.getUserId())){
            	request.fromHrEmplId=usrMap.get(userInfo.getUserId()).Peoplesoft_Id__c;
            }
            if(usrMap.containsKey(ownerId)){
        		request.toHrEmplId=usrMap.get(ownerId).Peoplesoft_Id__c;
            }
            request.postingIdList= new List<String>();
            
             List<String> jpIdList=new List<String>();
            if(jobPostingMap!=null && jobPostingMap.size()>0 && jobPostingMap.containsKey(jobPostingIdList.get(0))){
                String jobPostingId=jobPostingMap.get(jobPostingIdList.get(0)).Source_System_id__c;
                String rwsJobPostingId='';
                jpc=jobPostingMap.get(jobPostingIdList.get(0));
                if(String.isNotBlank(jobPostingId)){
                    rwsJobPostingId=jobPostingId.replaceAll('R.','');
                }
            	jpIdList.add(rwsJobPostingId);   
            }
            
            
            
            request.postingIdList.addAll(jpIdList);
            response=RWSJobPostingRestCallout.transferJobPosting(request);
           	if(jpc!=null && response!=null ){
                RWSJobPostingRestCallout.updateProgress(response, jpc);    
            }
            
        }
        
        return response;
    }
    @AuraEnabled
    public static String getListViewId(){
        String listViewId='';
        List<ListView> lstView=[SELECT Id, Name, DeveloperName, SobjectType FROM ListView where SobjectType='Job_Posting__c' and DeveloperName='My_Active_Job_Postings'];
        if(lstView!=null && lstView.size()>0){
             listViewId=lstView[0].id;
        }
        return listViewId;
    }   
	
	@AuraEnabled
	public static Map<String, String> jobPostingSwitch(String jpId){
		Map<String, String> switchMap = JobPostingFormController.switchToSFDC();//check whether RWS JP or SFDC JP
		switchMap.putAll(JobPostingFormController.postingStatus(jpId)); //get inactiv information of Job Posting

		return switchMap;
	}

	@AuraEnabled
	public static Map<String, String> switchSFDC(String jpId) {
		Boolean status = JobPostingFormController.getStatus(jpId);
		Map<String, String> switchMap = JobPostingFormController.switchToSFDC();
		//switchMap.put('switchSFDC', sw);
		switchMap.put('status', String.valueOf(status));

		//String swJSON = JSON.serialize(switchMap);
		
		return switchMap;
	} 

	@AuraEnabled
	public static String inactivateJobPosting(String jpId) {
		String msg = '';
		try{
			msg = JobPostingFormController.inactivateJobPosting(jpId);
		}catch(Exception exp) {
			ConnectedLog.LogException('JPF/JobPostingForm', 'JobPostingFormController', 'inactivateJobPosting', jpId, exp);
			throw new AuraHandledException(Label.Error);
		}

		return msg;
	}
}