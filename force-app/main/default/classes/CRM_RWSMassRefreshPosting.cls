public class CRM_RWSMassRefreshPosting {
    
    private List<Job_Posting__c> postingIdList=new List<Job_Posting__c>();
    
    private Job_Posting__c record;
    
    public String requestLabel;
    
    public String webServiceName;

    
    public CRM_RWSMassRefreshPosting(ApexPages.StandardSetController stdSetController) {
            postingIdList=stdSetController.getSelected();
            System.debug('Selected records on list view in standard controller--->'+postingIdList);
    }
    
    public CRM_RWSMassRefreshPosting(ApexPages.StandardController stdController) {
            record=(Job_Posting__c)stdController.getRecord();
            System.debug('Selected records on list view in standard controller--->'+record.id);
            postingIdList.add(record);
    }
    
    
    public Object massInActivate(){
        
           RWSJobPostingRequest request=new RWSJobPostingRequest();
           webServiceName='Inactivate';
           if(postingIdList!=null && postingIdList.size()>0){
          
                User usr=[select id,Peoplesoft_Id__c  from User where id =:Userinfo.getUserId()];
                List<Job_Posting__c> jobPostingList=[select id,Source_System_id__c,Opportunity__c from Job_Posting__c where id in :postingIdList];
               
                request.hrEmplId=usr.Peoplesoft_Id__c;
                request.postingIdList= new List<String>();
                List<String> jpIdList=new List<String>();
                for(Job_Posting__c jpc:jobPostingList){
                        String jobPostingId=jpc.Source_System_id__c;
                        String rwsJobPostingId='';
                        if(String.isNotBlank(jobPostingId)){
                            rwsJobPostingId=jobPostingId.replaceAll('R.','');
                        }
                        jpIdList.add(rwsJobPostingId);   
                }
                request.postingIdList.addAll(jpIdList);
               
           
        
          Continuation con = new Continuation(15);
          // Set callback method
          con.continuationMethod='processResponse';
     
          // Create callout request
          HttpRequest req = new HttpRequest();
          String restUrl=System.Label.RWS_Job_Posting_URL;
          req.setEndpoint(restUrl+'/RWS/rest/posting/inactivate');
          req.setMethod('POST');
          req.setHeader('Content-Type', 'application/json;charset=UTF-8');
          req.setBody(JSON.serialize(request));
          // Add callout request to continuation
          this.requestLabel = con.addHttpRequest(req);
      
         // Return the continuation
         return con;  
       } else {
           
               List<RWS_Posting__e> postingEventList=new List<RWS_Posting__e>();
               RWS_Posting__e ePosting=new RWS_Posting__e();
               ePosting.Code__c=webServiceName;
               ePosting.SUBCODE__c='ERROR';
               ePosting.RWSResponse__c='Please select atleast one record to inactivate';
               ePosting.UserId__c=UserInfo.getUserId();
               postingEventList.add(ePosting);
               
               List<Database.SaveResult> results = EventBus.publish(postingEventList);
               
               String listViewId='';
               List<ListView> lstView=[SELECT Id, Name, DeveloperName, SobjectType FROM ListView where SobjectType='Job_Posting__c' and DeveloperName='My_Active_Job_Postings'];
               if(lstView!=null && lstView.size()>0){
                    listViewId=lstView[0].id;
               }
              
               PageReference pageRef = new PageReference('/lightning/o/Job_Posting__c/list?filterName='+listViewId);
               pageRef.setRedirect(true);
               return pageRef;  
         }
        
    }
    
    public Object  massRefresh(){
           RWSJobPostingRequest request=new RWSJobPostingRequest();
           webServiceName='Refresh';
           if(postingIdList!=null && postingIdList.size()>0){
               
               System.debug('Inside MassRefresh--->'+postingIdList);
          
                User usr=[select id,Peoplesoft_Id__c  from User where id =:Userinfo.getUserId()];
                List<Job_Posting__c> jobPostingList=[select id,Source_System_id__c,Opportunity__c from Job_Posting__c where id in :postingIdList];
               
                request.hrEmplId=usr.Peoplesoft_Id__c;
                request.postingIdList= new List<String>();
                List<String> jpIdList=new List<String>();
                for(Job_Posting__c jpc:jobPostingList){
                        String jobPostingId=jpc.Source_System_id__c;
                        String rwsJobPostingId='';
                        if(String.isNotBlank(jobPostingId)){
                            rwsJobPostingId=jobPostingId.replaceAll('R.','');
                        }
                        jpIdList.add(rwsJobPostingId);   
                }
                request.postingIdList.addAll(jpIdList);
               
           
        
          Continuation con = new Continuation(15);
          // Set callback method
          con.continuationMethod='processResponse';
     
          // Create callout request
          HttpRequest req = new HttpRequest();
          String restUrl=System.Label.RWS_Job_Posting_URL;
          req.setEndpoint(restUrl+'/RWS/rest/posting/refresh');
          req.setMethod('POST');
          req.setHeader('Content-Type', 'application/json;charset=UTF-8');
          req.setBody(JSON.serialize(request));
          // Add callout request to continuation
          this.requestLabel = con.addHttpRequest(req);
      
         // Return the continuation
         return con;  
       } else {
           
               List<RWS_Posting__e> postingEventList=new List<RWS_Posting__e>();
               RWS_Posting__e ePosting=new RWS_Posting__e();
               ePosting.Code__c=webServiceName;
               ePosting.SUBCODE__c='ERROR';
               ePosting.RWSResponse__c='Please select atleast one record to refresh';
               ePosting.UserId__c=UserInfo.getUserId();
               postingEventList.add(ePosting);
               
               List<Database.SaveResult> results = EventBus.publish(postingEventList);
               
               String listViewId='';
               List<ListView> lstView=[SELECT Id, Name, DeveloperName, SobjectType FROM ListView where SobjectType='Job_Posting__c' and DeveloperName='My_Active_Job_Postings'];
               if(lstView!=null && lstView.size()>0){
                    listViewId=lstView[0].id;
               }
              
               PageReference pageRef = new PageReference('/lightning/o/Job_Posting__c/list?filterName='+listViewId);
               pageRef.setRedirect(true);
               return pageRef;  
         }
    }
    
    // Callback method 
    public Object processResponse() {   
      
      System.debug('Inside call back method -processResponse--->');
        
      // Get the response by using the unique label
      HttpResponse res = Continuation.getResponse(this.requestLabel);
      // Set the result variable that is displayed on the Visualforce page
     
        if(res==null || res.getStatusCode()>= 2000){
            
            System.debug('Continuation error: '+res.getStatus());
            
            List<RWS_Posting__e> postingEventList=new List<RWS_Posting__e >();
            RWS_Posting__e sPosting=new RWS_Posting__e();
            sPosting.Code__c=webServiceName;
            sPosting.SUBCODE__c='ERROR';
            sPosting.RWSResponse__c='Error occured during communication with RWS.Please try again.';
            sPosting.UserId__c=UserInfo.getUserId();
            postingEventList.add(sPosting);
            List<Database.SaveResult> results = EventBus.publish(postingEventList);
            String listViewId='';
            List<ListView> lstView=[SELECT Id, Name, DeveloperName, SobjectType FROM ListView where SobjectType='Job_Posting__c' and DeveloperName='My_Active_Job_Postings'];
            if(lstView!=null && lstView.size()>0){
                listViewId=lstView[0].id;
            }
            PageReference pageRef =null;    
            pageRef = new PageReference('/lightning/o/Job_Posting__c/list?filterName='+listViewId);
            pageRef.setRedirect(true);
               
            return pageRef;
              
        }else{   
     
      RWSJobPostingResponse response= (RWSJobPostingResponse)JSON.deserialize(res.getBody(), RWSJobPostingResponse.class);
      // Return null to re-render the original Visualforce page
      System.debug('Received Response RWSJobPostingResponse----->'+response);
      System.debug('Inside Continuous Call back--->'+webServiceName);
        
     List<String> sourceSystemIdList=new List<String>();
     if(response!=null && response.postingIdList!=null){
            for(RWSJobPostingResponse.PostingIdList posting:response.postingIdList){
                if((response.responseMsg=='SUCCESS'|| response.responseMsg=='PARTIAL') && posting!=null && String.isNotBlank(posting.postingId)){
                     if(posting.code=='SUCCESS'){
                            sourceSystemIdList.add('R.'+posting.postingId.trim());
                     }     
                }   
            }
        
        
        
     Map<Id,Job_Posting__c> jobPostingMap=new Map<Id,Job_Posting__c>((List<Job_Posting__c>)Database.query('select id,Source_System_id__c from Job_Posting__c  where Source_System_id__c in :sourceSystemIdList '));
     List<Job_Posting__c> jpcList=new List<Job_Posting__c>();
     for(Job_Posting__c jpc:jobPostingMap.values()){
            jpc.SFPosting_Status__c='In Progress';
            jpcList.add(jpc);
     }   
     
       
    try{
        update jpcList;   
    }catch(Exception e){
        system.debug('Exception occurred during update--->'+e);   
    }    
    
   }      
        
       List<RWS_Posting__e> postingEventList=new List<RWS_Posting__e >();
            if(response!=null){
                
                if(response.responseMsg=='SUCCESS'){
                    RWS_Posting__e sPosting=new RWS_Posting__e();
                    if(webServiceName=='Refresh')
                    sPosting.RWSResponse__c='Refresh has been initiated for the posting/s. Please check the Posting Progress icon for current status.';
                    else if(webServiceName=='Inactivate')
                    sPosting.RWSResponse__c='Inactivaiton has been initiated for the posting/s.Please check the Posting Progress icon for current status.';
                    sPosting.Code__c=webServiceName;
                    sPosting.SUBCODE__c='SUCCESS';
                    sPosting.UserId__c=UserInfo.getUserId();
                    postingEventList.add(sPosting);
                }else if(response.responseMsg=='ERROR'){
                    RWS_Posting__e sPosting=new RWS_Posting__e();
                    sPosting.Code__c=webServiceName;
                    sPosting.SUBCODE__c='ERROR';
                    sPosting.RWSResponse__c=response.responseDetail;
                    sPosting.UserId__c=UserInfo.getUserId();
                    postingEventList.add(sPosting);
                    
                }else if(response.responseMsg=='PARTIAL'){
                    
                  List<RWSJobPostingResponse.PostingIdList>  pIdList=response.postingIdList;
                   
                   String successStr='';
                   String successError='';
                    for(RWSJobPostingResponse.PostingIdList jPost:pIdList){
                        if(jPost.code=='SUCCESS'){
                            successStr+=jPost.postingId+':-'+jPost.message+'\r';
                        }else if(jPost.code=='ERROR'){
                            successError+=jPost.postingId+':-'+jPost.message+'<br/>';
                        }
                    }
                    
                    if(String.isNotBlank(successStr)){
                        RWS_Posting__e sPosting=new RWS_Posting__e();
                        sPosting.Code__c=webServiceName;
                        sPosting.SUBCODE__c='SUCCESS';
                        sPosting.RWSResponse__c=successStr;
                        sPosting.UserId__c=UserInfo.getUserId();
                        postingEventList.add(sPosting);
                    }
                    
                    
                    if(String.isNotBlank(successError)){
                        RWS_Posting__e ePosting=new RWS_Posting__e();
                        ePosting.Code__c=webServiceName;
                        ePosting.SUBCODE__c='ERROR';
                        ePosting.RWSResponse__c=successError;
                        ePosting.UserId__c=UserInfo.getUserId();
                        postingEventList.add(ePosting);
                    }
                    
                }
                
                  System.debug('List of postingEventList to be published'+postingEventList);
                
                  List<Database.SaveResult> results = EventBus.publish(postingEventList);

            }
        
            String listViewId='';
            List<ListView> lstView=[SELECT Id, Name, DeveloperName, SobjectType FROM ListView where SobjectType='Job_Posting__c' and DeveloperName='My_Active_Job_Postings'];
            if(lstView!=null && lstView.size()>0){
                listViewId=lstView[0].id;
            }
        PageReference pageRef =null;    
        if(record!=null){
             pageRef = new PageReference('/lightning/r/Job_Posting__c/'+record.Id+'/view');
        }else{
            pageRef = new PageReference('/lightning/o/Job_Posting__c/list?filterName='+listViewId);
        }
            pageRef.setRedirect(true);
           
      return pageRef;
      
    }
   }
}