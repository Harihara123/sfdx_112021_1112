public class CRM_RecruitersCountBatch implements Database.Batchable<sObject>{
    String query;
    public CRM_RecruitersCountBatch (String query) {
        this.query = query;        
    }
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Opportunity> scope){
        for(Opportunity opp : scope){
            opp.Number_Of_Allocated_Recruiters__c = opp.OpportunityTeamMembers.size();
        }
        
        Database.SaveResult[] srList = Database.update(scope, false);
 		List<Log__c> logs = new List<Log__c>();
        for (Integer i = 0; i < scope.size(); i++) {
            Database.SaveResult s = srList[i];
            Opportunity origRecord = scope[i];
            if (!s.isSuccess()) {
                System.debug('Record Id - '+origRecord.Id);  
                for(Database.Error err : s.getErrors()) {
                    System.debug(' The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                    String errorMessage = 'The following error has occurred. Error Code: '+err.getStatusCode()+' Error Message: '+err.getMessage()+' Fields: '+err.getFields();
                    Log__c log = new Log__c (
                    	Description__c = errorMessage,
                        Related_Id__c = origRecord.Id,
                        Class__c = 'CRM_RecruitersCountBatch',
                        Log_Date__c = System.today(),
                        Subject__c = 'Count'
                    );
                    logs.add(log);
                }
            } 
        }
        
        if(!logs.isEmpty()) {
            insert logs;
        }
    }

    public void finish(Database.BatchableContext BC){

    }
}