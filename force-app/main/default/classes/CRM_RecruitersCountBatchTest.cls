@isTest
public class CRM_RecruitersCountBatchTest {
	
    static testMethod void batchTest () {
        String query = 'Select Id, (Select Id from OpportunityTeamMembers where TeamMemberRole = \'Allocated Recruiter\') from Opportunity where CreatedDate >= 2019-01-01T00:00:00.000Z AND Opco__c = \'TEKsystems, Inc.\'';
        CRM_RecruitersCountBatch newBatch = new CRM_RecruitersCountBatch(query);
        ID batchprocessid = Database.executeBatch(newBatch, 200);
    }
}