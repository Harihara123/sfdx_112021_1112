public without sharing class CRM_ReqMetrics {   
    @AuraEnabled
    public static void saveReqMetrics(String reqMetricsData){
        try{
            //{"sobjectType":"Req_metrics__c","Skill_Speciality_From_Model__c":"Development","F_NF_From_Model__c":"Functional","SOCONET__c":"15-1299.09","Job_Title__c":"test analyst","Job_Description__c":"technical lead and analyst with lead manager","OpptyId__c":"0068A000009bVGdQAM"}
        	Map<String,Object> metObject = (Map<String,Object>) JSON.deserializeUntyped(reqMetricsData);
        	List<Req_metrics__c> saveMetricList=new list<Req_metrics__c>();
            saveMetricList=mapReqMetrics(metObject);
			if(saveMetricList != null && saveMetricList.size()>0){
                insert saveMetricList;
            }
        }catch(Exception excp){
			System.debug('***********excp**'+excp);
        }       
    }
    
    @AuraEnabled
    public static List<Req_metrics__c> mapReqMetrics(Map<String,Object> metObject){
        try{
            String ReferenceType='Skill Speciality';
            List<Req_metrics__c> saveMetricList=new List<Req_metrics__c>();
        	if(metObject != null){
            	Req_metrics__c saveMetricData=new Req_metrics__c(); 
            	if(metObject.size()>0){
                    saveMetricData.Skill_Speciality_From_Model__c=(String)metObject.get('Skill_Speciality_From_Model__c');
                    saveMetricData.F_NF_From_Model__c=(String)metObject.get('F_NF_From_Model__c');
                    saveMetricData.SOCONET__c=(String)metObject.get('SOCONET__c');
                    saveMetricData.Job_Title__c=(String)metObject.get('Job_Title__c');
                    saveMetricData.Job_Description__c=(String)metObject.get('Job_Description__c');
                    saveMetricData.Origination_Page__c=(String)metObject.get('Origination_Page__c');
                    saveMetricData.OpptyId__c=(Id)metObject.get('OpptyId__c');
                    saveMetricData.Skillset_From_Model__c=(String)metObject.get('Skillset_From_Model__c');
                    saveMetricData.Experience_Level_From_Model__c=(String)metObject.get('Experience_Level_From_Model__c');
                    saveMetricData.Type__c=ReferenceType;
            	}                
				saveMetricList.add(saveMetricData);
            	return saveMetricList;
        	}else{
                return null;
        	}
        }catch(Exception excp){
            return null;
        }    	
    }
}