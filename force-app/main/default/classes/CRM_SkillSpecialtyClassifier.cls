public with sharing class CRM_SkillSpecialtyClassifier  {
	//private static String SSC_URL = 'https://automl.googleapis.com/v1beta1/projects/connected-ingest-prod/locations/us-central1/models/TBL5089034593579827200:predict';
    private static String SSC_URL = System.Label.CRM_SSCClassifier;
    //private static String SOC_URL = 'https://automl.googleapis.com/v1beta1/projects/connected-ingest-prod/locations/us-central1/models/TBL1132017589539569664:predict';
    private static String SOC_URL = System.Label.CRM_SOCClassifier;
    private static String INGEST10X = 'ingest10x';

    public static String get_access_token(){       
        if (Cache.Org.contains('local.Ingest10x.GoogleIngestPubSubToken')) {
            return (String)Cache.Org.get('local.Ingest10x.GoogleIngestPubSubToken');
        } else {           
            String resToken = [SELECT token__c FROM IngestPubSub__mdt where label = 'Prod'].token__c;           
            JSONParser tokenParser = JSON.createParser(resToken);
            Map<String,String> tokenMap = new Map<String,String>();                     
            while (tokenParser.nextToken() != null) {
                if (tokenParser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String key = tokenParser.getText();
                   
                    tokenParser.nextToken();
                    tokenMap.put(key,tokenParser.getText());
                }                
            }           
            resToken = '';
            tokenParser = null;           
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            req.setEndpoint('https://accounts.google.com/o/oauth2/token');
            req.setMethod('POST');           
            req.setHeader('ContentType','application/x-www-form-urlencoded');           
            String header = '{"alg":"RS256","typ":"JWT"}';
            String header_encoded = EncodingUtil.base64Encode(blob.valueof(header));           
            String claim_set = '{"iss":"' +tokenMap.get('client_email') +'"';
            //claim_set += ',"scope":"https://www.googleapis.com/auth/pubsub"';
            claim_set += ',"scope":"https://www.googleapis.com/auth/cloud-platform"';
            claim_set += ',"aud":"https://accounts.google.com/o/oauth2/token"';
            claim_set += ',"exp":"' + datetime.now().addHours(1).getTime()/1000;
            claim_set += '","iat":"' + datetime.now().getTime()/1000 + '"}';
           
            String claim_set_encoded = EncodingUtil.base64Encode(blob.valueof(claim_set));
            String signature_encoded = header_encoded + '.' + claim_set_encoded;
            String key = tokenMap.get('private_key').remove('-----BEGIN PRIVATE KEY-----\n').remove('\n-----END PRIVATE KEY-----\n');
           
            blob private_key = EncodingUtil.base64Decode(key);
            signature_encoded = signature_encoded.replaceAll('=','');
            String signature_encoded_url = EncodingUtil.urlEncode(signature_encoded,'UTF-8');
            blob signature_blob =   blob.valueof(signature_encoded_url);           
            String signature_blob_string = EncodingUtil.base64Encode(Crypto.sign('RSA-SHA256', signature_blob, private_key));           
            String JWT = signature_encoded + '.' + signature_blob_string;           
            JWT = JWT.replaceAll('=','');           
            String grant_string= 'urn:ietf:params:oauth:grant-type:jwt-bearer';
            req.setBody('grant_type=' + EncodingUtil.urlEncode(grant_string, 'UTF-8') + '&assertion=' + EncodingUtil.urlEncode(JWT, 'UTF-8'));
            res = h.send(req);
            String response_debug = res.getBody() +' '+ res.getStatusCode();
            if(res.getStatusCode() == 200) {
                JSONParser parser = JSON.createParser(res.getBody());
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                        // Move to the value.
                        parser.nextToken();                       
                        if (!Test.isRunningTest()) {
                            Cache.Org.put('local.Ingest10x.GoogleIngestPubSubToken', parser.getText(), 1800); //ttl  1/2 hour (can be an hour - but this give good margin for error)
                        }
                        return parser.getText();
                    }
                }
            }           
        }
        return 'error';
    }

	@AuraEnabled
    public static String makeSSCClassifierCallout(String payload) {
        try{
			String responseBody=CRM_SkillSpecialtyClassifier.makeCallout(payload, SSC_URL);
        	return responseBody;            
        }catch (Exception ex) {
            System.debug('Exception ' + ex.getMessage());
            throw ex;            
        }        
    }
    
    @AuraEnabled
    public static String makeSOCClassifierCallout(String payload) {
        try{
			String responseBody=CRM_SkillSpecialtyClassifier.makeCallout(payload, SOC_URL);
        	return responseBody;            
        }catch (Exception ex) {
            System.debug('Exception ' + ex.getMessage());
            throw ex;            
        }       
    }

	private static String makeCallout(String payload, String endpoint_raw) {
        String access_token = get_access_token();       
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setTimeout(10000); 
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');               
        request.setHeader('Authorization', 'Bearer ' + access_token);       
        request.setEndPoint(endpoint_raw);
        request.setBody(payload);  
        
        if (!Test.isRunningTest()) {
            HttpResponse response = new HTTP().send(request);
        }
        String responseBody;
        try {
            HttpResponse response = new HTTP().send(request);
            if (response.getStatusCode() == 200) {
                responseBody = response.getBody();
            } else {
                responseBody = 'CALLOUT_EXCEPTION_RESPONSE '+ response.getStatus();
                Exception ex;
                throw ex;
            }            
        } catch (Exception ex) {
            // Callout to apigee failed
            System.debug('Exception ' + ex.getMessage());
            throw ex;
        }        
        return responseBody;
    }
}