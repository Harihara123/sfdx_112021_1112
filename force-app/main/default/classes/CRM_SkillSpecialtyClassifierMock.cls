@isTest
global class CRM_SkillSpecialtyClassifierMock implements HttpCalloutMock {
    global static HttpResponse respond(HttpRequest request){
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        String jsonBody = '{'+
		'"type": "service_account",'+
		'"project_id": "connected-ingest-test",'+
		'"private_key_id": "9b5fd57f6df833a877aa108c74a88e2a932607dc",'+
		'"private_key": "-----BEGIN PRIVATE KEY-----\\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDCPKOVNbRxPw8P\\nT3V9I2du3CFDPeEXva1ktdwe3izjLRIdNksPQ7H4A5fe7VNsJk99rxyi6K9VtORI\\nFkhflmMJFaCkj+lIP9CDERhlrZYs1mlikGIx+0jjn0gZCaZ2SZoI7M6ibhu9MrUr\\nOFPami0Tj1G82OLGWoBC7tiK5OAOJIw+rNQQ9E3fsLtsSsPHGXmk9ZMwEF+hBSZd\\n/vZ6H8Us1Tw8t/yCIePpC1+d8tBJFR+TzBQgabYcYjIYDUr5SVc8sAxKdNhLRjPi\\nn6CzAzYm+A/GZ+9gm8o0Zjk+AI3/XeZmm3IbylZ896InN4xLD26a6CB9UKsGZndW\\ns6UOAQ7vAgMBAAECggEAC72xn6/W3++RX/oMHovY0T0yL4faht5a0Fcj3qPMb9EG\\nIsjo4ZvUcqr476tF2yg3rt+6aeiJzLL6B5d9OXX0N5TcmIXwcoMU/g82+9Jx3PT6\\n+s/Cd8X6QnQa3hSUwNqzGm4CEZR5Tcb2Z43ol7KxHdkQdktf7eCDl0grw72RA5m7\\n2JghChrHjdx/yKJroLUsvcUHBAPVAFFrYM2W2OXLNI7VZuYGICI+BsYFe4eHg08/\\nExgykVs2BgEUoE9UjkXDDNN8kYh5wljAmiLsPU+CjXsGp5Grwoz7VLKw3B5v+9Ml\\nkeepIox4fEBeb4mzdjxUr5L7mNC+2y66oWBUMK65YQKBgQD4gMiQ28L9w5CyPYgG\\n5qWuFASdwCFlHTQaPgVZFnN1GvizKSNt9/0iRpxK/e1JsY6ndvwNk1o5LHHlJXJJ\\nXvTo42sRWXzMJvDns/BSO0zInjuYWNLPnFR90zuFPRJ4PKDG11KOG24MVJGYK3KA\\nLHugZMe9QEtuRMLnRMiOny2yzwKBgQDIGMB0DAH/1xr8aUNBBTwosKc9Kjdaqahr\\nddPVYpkENpByFp9QltmwBiR9gNc4qw6Bhnv5ZZ+BgvJst2og340+x/8x/3lFw4+f\\nSx3qQZNY0sm0jeegBuujxOjROZ3ZpTK1tJ8PvKmCwJfA5uuYKM0Qz37zbE03EwZZ\\nfWtsL4Bp4QKBgQDfkXVgJeaSyWwXf5YupzktmOLiTaV9w71JUr0a/kDXfXF8Nimi\\n2PYK7T3VlgfyQeUWssxnJkeIonzBbGQasGxkTZ3Y2QqW3P3lZxRx1tnor7rsABWB\\nJKM7JFwrLdu9Asaevb+PDAyA2WC3Z3c260456hFtKTfzt7clu9M3xOunLwKBgQC1\\ny7828B306uNqxtxCu9Xfhjg72xo1tgT9tSzEJ3Osn0J3VVRjrNqVEc+fcyqXItro\\nFV6XxZu42FcXLUEwIsu6aYzTSTBOkRHxARdp2TBdOA3ojDEQdJkszt9UEEv5DR1Y\\nd4U4kAlWcVKvAV61fQRrLIpD8Xbp/c/8CHZL8ZBzIQKBgQCacWJ5lwO2EAXzknck\\n81r0hmr1Db0bSWAc8kcctavF/x9EeMsYz7OdQQAR0JMC81RZ8qy7v6Mojo7YrsUf\\nGob6l6uNU/hw8GAuGL5U710ALZSMaJJ9V06xf6hnny0ZyVBVcayd0rNE8Hvgqj8J\\n3FNDoOc2KV1sN9Zq3K7TB+R5+w==\\n-----END PRIVATE KEY-----\\n",'+			
        '"client_email": "ingestpubsub@connected-ingest-test.iam.gserviceaccount.com",'+
		'"client_id": "110244580768699561672",'+
		'"auth_uri": "https://accounts.google.com/o/oauth2/auth",'+
		'"token_uri": "https://oauth2.googleapis.com/token",'+
		'"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",'+
		'"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/ingestpubsub%40connected-ingest-test.iam.gserviceaccount.com"'+
		'}';
        response.setBody(jsonbody);
        response.setStatusCode(200);
        return response;
    }

}