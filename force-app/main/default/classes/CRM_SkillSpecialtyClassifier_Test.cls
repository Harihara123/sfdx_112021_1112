@isTest
public class CRM_SkillSpecialtyClassifier_Test {
	@isTest
    public static void getAccessTokenTest(){
   		//String respToken = [SELECT label,token__c FROM IngestPubSub__mdt where label = 'Prod'].token__c;
        IngestPubSub__mdt respToken=new IngestPubSub__mdt();
        respToken.Label = 'Prod';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_SkillSpecialtyClassifierMock());
        CRM_SkillSpecialtyClassifier.get_access_token();
        Test.stopTest();        
    }
    @isTest
    public static void makeSSCClassifierCalloutTest(){
        //String payload = 'Java Developer';
        //Integer endpoint_raw = getAccessTokenTest();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_SkillSpecialtyClassifierMock());
        CRM_SkillSpecialtyClassifier.makeSSCClassifierCallout('Java Developer');
        Test.stopTest();
    }
    
    @isTest
    public static void makeSOCClassifierCalloutTest(){
        //String payload = 'Java Developer';
        //Integer endpoint_raw = getAccessTokenTest();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_SkillSpecialtyClassifierMock());
        CRM_SkillSpecialtyClassifier.makeSOCClassifierCallout('Java Developer');
        Test.stopTest();
    }
}