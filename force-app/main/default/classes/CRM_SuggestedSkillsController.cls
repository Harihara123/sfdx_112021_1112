public class CRM_SuggestedSkillsController {
    
    @AuraEnabled
    public static String Skills(String httpRequestType,String requestBody) {
        system.debug('==============' + httpRequestType);
        String queryString='';
        String location='';
        //String requestBody ='{"query":{"term": {"titlevector": "qa tester"}},"aggs":{"skill_counts":{"terms":{"script":{"inline": "doc['skillsvector'].values"},"size": 100}}}}';
        
        ApigeeOAuthSettings__c serviceSettings = null;
        if (httpRequestType == 'GET' ) {
            serviceSettings = ApigeeOAuthSettings__c.getValues('SkillSearch_CRM');
           // if(String.isBlank(serviceSettings.OAuth_Token__c)){
                getAccessToken();
           // }
        }
        return match(serviceSettings, queryString, httpRequestType, requestBody, location);
    }
    
    private static String match(ApigeeOAuthSettings__c connSettings, String queryString, String httpRequestType, String requestBody, String location) {
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
        System.debug('serviceSettings 2'+connSettings.Service_URL__c);
        System.debug('queryString >>>>>>>> ' + connSettings.Service_URL__c + queryString);
        //queryString = addTrackingIds(queryString);
        System.debug('requestBody >>>>>>>> ' + requestBody);
        String paramURL = String.valueOf(connSettings.Service_URL__c)+requestBody;
        req.setEndpoint(paramURL);
        req.setMethod(connSettings.Service_Http_Method__c);
        String timeOutvalue = Label.ATS_SEARCH_TIMEOUT;
        req.setTimeout(Integer.valueOf(timeOutvalue));
        req.setHeader('Authorization', 'Bearer ' + connSettings.OAuth_Token__c);
        if (httpRequestType == 'GET' ) {
         //   req.setBody(requestBody);
            req.setHeader('Content-Type','application/json') ;
        }
        String responseBody;
        try {
            HttpResponse response = connection.send(req);
            responseBody = response.getBody();
            System.debug('responseBody >>>>>>>> ' + responseBody);
        } catch (CalloutException ex) {
            // Callout to apigee failed
            System.debug(LoggingLevel.ERROR, 'Apigee call to Match Service failed! ' + ex.getMessage());
            // responseBody = CALLOUT_EXCEPTION_RESPONSE.replace('&&&&', ex.getMessage());
        } catch (Exception ex) {
            // Callout to apigee failed
            System.debug(LoggingLevel.ERROR, 'Apigee call to Match Service failed! ' + ex.getMessage());
            throw ex;
        }
        
        if (String.isBlank(responseBody)) {
            throw new SearchController.InvocableApiException('Apigee match service call returned empty response body!');
        }
        return responseBody;
    }
    
    
     private static void getAccessToken()
    {        
        ApigeeOAuthSettings__c settings= new ApigeeOAuthSettings__c();
        settings = ApigeeOAuthSettings__c.getValues('SkillSearch_CRM');
        oAuth_Connector oauth = new oAuth_Connector();
        oauth.token_url = settings.Token_URL__c;
        oauth.client_Id = settings.Client_Id__c;
        oauth.client_Secret = settings.Client_Secret__c;
        system.debug('****TOKEN B4**** ' + oauth);
        oauth = oauth.getAccessToken(oauth);
        settings.OAuth_Token__c = oauth.access_Token;
        settings.Token_Expiration__c = oauth.expires_In;
        system.debug('****settings**** ' + settings);
       
        
    }

	@AuraEnabled
    public static List<OpportunityOpcoMapping__mdt> fetchOpcoMappings() {
		List<OpportunityOpcoMapping__mdt> opcoMap = new List<OpportunityOpcoMapping__mdt>();
		try {
			opcoMap = [SELECT Id, MasterLabel,Translated_Value__c FROM OpportunityOpcoMapping__mdt];
		
			return opcoMap;
		} catch(Exception e){
			System.debug('Exception Opco Mapping details:'+e.getMessage());
			return opcoMap;
		}

	}

    
}