@isTest
global class CRM_SuggestedSkills_Test implements HttpCalloutMock{
    
    testmethod static void coverCRM_SuggestedSkills(){
        
        ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c();
        serviceSettings.Client_Id__c = '121111';
        serviceSettings.Name = 'SkillSearch_CRM';
        serviceSettings.Service_Http_Method__c = 'GET';
        serviceSettings.Service_URL__c = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';
        serviceSettings.Token_URL__c = '--';
        insert serviceSettings;
        Test.setMock(HttpCalloutMock.class,new CRM_SuggestedSkills_Test() );
		
        Test.startTest();
        String requestBody ='{"query":{"term": {"titlevector": "qa tester"}},"aggs":{"skill_counts":{"terms":{"script":{"inline": "doc[\'skillsvector\'].values"},"size": 100}}}}';
    	CRM_SuggestedSkillsController.Skills('GET', requestBody);
        Test.stopTest();
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        
       // System.assertEquals('POST', req.getMethod());
		
        HttpResponse resp = new HTTpResponse();
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
		
        return res;
    }
    
    testmethod static void coverCRMSkillsModel(){
        CRMSkillsModel obj = new CRMSkillsModel();
        obj.skillList = new List<Object>();
        obj.Opco = 'Aerotek';
            
    }
    
    testmethod static void testFetchOpcoMappings() {
		CRM_SuggestedSkillsController.fetchOpcoMappings();    
    } 
}