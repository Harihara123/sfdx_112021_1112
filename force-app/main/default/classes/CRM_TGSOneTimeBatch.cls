global class CRM_TGSOneTimeBatch implements Database.Batchable<Sobject>{
	//Stage = Closed Wash
	public Map<String, String> ClosedWashWinLossWashReason = new Map<String, String>{'Organizational Change'=>'Change in Leadership','Client Cancelled'=>'Doing In-house','Budgeting'=>'Lost Funding','Other'=>'Other','On Hold'=>'Priority Shifted'};
	//Stage = Close Lost
	public Map<String,String> ClosedLostWinLossWashReason = new Map<String, String>{'Other'=>'Other','Relationship'=>'Pre-existing Relationship','Services Offered'=>'Service/Capability Alignment','Terms and Conditions'=>'Terms and Conditions Alignment','Price'=>'Unable to Convey Value','Too Late in Process'=>'Other','RFP Compliance'=>'Other','Past Performance'=>'Other','Geographic Coverage'=>'Other'};
	//Stage = Services Offered & Closed No Bid
	public Map<String,String> ServicesOfferedAndClosedBidMap = new Map<String, String>{'Geographic Coverage'=>'Other','Past Performance'=>'Other','Price'=>'Other','Relationship'=>'Other','RFP Compliance'=>'Other','Services Offered'=>'Other','Terms and Conditions'=>'Other','Too Late in Process'=>'Other','Other'=>'Other'};
    // Stage = Closed Won
    public Map<String,String> CloseWonWinLossWashReason = new Map<String, String>{'Services Offered'=>'Blend of Services / Flexibility','Price'=>'Competitive Pricing','Geographic Coverage'=>'Offshore Capability','Relationship'=>'Relationship w/ Buyer','Other'=>'Other'};
    
	public Map<String,String> DoesTeamRequireDrugAndBackgroundCheckMap = new Map<String, String>{'YES'=>'Drug Test or Background Check is Required','NO'=>'N/A'};
    
    public Map<String,String> SegmentToVerticalMap = new Map<String,String>{'Communications'=>'Communications','Consumer Products/Services'=>'Consumer Products/Services','Education'=>'Education','Energy'=>'Energy','Entertainment'=>'Media & Entertainment','Healthcare'=>'Healthcare','Hospitality'=>'Hospitality','Legal'=>'Other','Manufacturing'=>'Manufacturing','Non-Profit'=>'Non-Profit','Professional Services'=>'Professional Services','Retail'=>'Retail','Technology'=>'Technology Enablement','Transportation'=>'Transportation','Other'=>'Other','Financial Services'=>'Financial Services'};
    
    public Map<String,String> PricingBillingMap = new Map<String, String>{'Time & Material - not to exceed'=>'Time & Material Not to Exceed','Fix-Fee plus time and material'=>'Fixed Fee and Time & Material','Performance Based/Shared Risk'=>'Milestone Based','Fixed Price'=>'Fixed Fee'};
        
	public Map<String,String> MSAAgreementMap = new Map<String, String>{'Services Agreement in place that allows us to provide this opportunity'=>'TEKsystems, Inc. Agreement','Services Agreement in place for providing other services'=>'TEKsystems Global Services, LLC Agreement','No Services Agreement in place'=>'None'};

	public Map<String,String> DoesTeamRequireSecurityClearanceMap = new Map<String, String>{'YES'=>'Clearance Required','NO'=>'N/A'};	
	
    public Map<String,String> ResponseTypeMap = new Map<String,String>{'Material Change PCR'=>'PCR'};
    
    public Map<String,String> CloseReqStageMap = new Map<String,String>{'Closed No Bid'=>'Opting Out of Bid','Closed No Pursue'=>'Elect to Not Bid','Closed Lost'=>'Lost','Closed Wash'=>'Washed','Closed Won'=>'Won','Interest'=>'Interest','Qualifying'=>'Qualifying','Pursue'=>'Solutioning','Bid'=>'Proposing','Negotiation'=>'Negotiating'};
        
	public Map<String,String> OpenReqStageMap = new Map<String,String>{'Interest'=>'Interest','Qualifying'=>'Interest','Pursue'=>'Interest','Bid'=>'Proposing','Negotiation'=>'Proposing'};
    global String strQuery;
    
    global CRM_TGSOneTimeBatch(){ 
        strQuery = '';
    }
    
    global database.Querylocator start(Database.BatchableContext context){
        return Database.getQueryLocator(strQuery);          
    }
    
    global void execute(Database.BatchableContext context , list<Opportunity> scope){
        list<Opportunity> UpdatedHistoricalTGSOpp = new list<Opportunity>();
        try{
            for(Opportunity x:scope){
                //Closed Reason Category
                if(x.StageName == 'Closed Wash' && ClosedWashWinLossWashReason.containsKey(x.Win_Loss_Wash_reason__c)){
                    x.Closed_Reason_Category__c = ClosedWashWinLossWashReason.get(x.Win_Loss_Wash_reason__c);
                    x.Closed_Reason__c = 'Washed';
                }
                if(x.StageName == 'Closed Lost' && ClosedLostWinLossWashReason.containsKey(x.Win_Loss_Wash_reason__c)){
                    x.Closed_Reason_Category__c = ClosedLostWinLossWashReason.get(x.Win_Loss_Wash_reason__c);
                    x.Closed_Reason__c = 'Lost';
                }
                if(x.StageName == 'Closed No Pursue' && ServicesOfferedAndClosedBidMap.containsKey(x.Win_Loss_Wash_reason__c)){
                    x.Closed_Reason_Category__c = ServicesOfferedAndClosedBidMap.get(x.Win_Loss_Wash_reason__c);
                    x.Closed_Reason__c = 'Elect to Not Bid';
                }
                if(x.StageName == 'Closed No Bid' && ServicesOfferedAndClosedBidMap.containsKey(x.Win_Loss_Wash_reason__c)){
                    x.Closed_Reason_Category__c = ServicesOfferedAndClosedBidMap.get(x.Win_Loss_Wash_reason__c);
                    x.Closed_Reason__c = 'Opting Out of Bid';
                }
                if(x.StageName == 'Closed Won'){
                    if(x.Global_Services_Opportunity_Type__c !=null && x.Global_Services_Opportunity_Type__c == 'New Business' && x.Win_Loss_Wash_reason__c == 'Past Performance'){
                        x.Closed_Reason_Category__c = 'Past Performance w/ Similar - Like Work';
                    }else if(x.Win_Loss_Wash_reason__c == 'Past Performance' && x.Global_Services_Opportunity_Type__c !=null && (x.Global_Services_Opportunity_Type__c == 'Renewal/Extension' || x.Global_Services_Opportunity_Type__c == 'Net New Expansion')){
                        x.Closed_Reason_Category__c = 'Performance on Initial Scope / Project';
                    }else if(CloseWonWinLossWashReason.containsKey(x.Win_Loss_Wash_reason__c)){
                        x.Closed_Reason_Category__c = CloseWonWinLossWashReason.get(x.Win_Loss_Wash_reason__c);
                    }
                    x.Closed_Reason__c = 'Won';
					x.Probability_TGS__c = '100%';
                }else{
					x.Probability_TGS__c = '0%';
				}
                //Segment
                if(x.Segment__c !=null){
                    List<String> Segment = x.Segment__c.split(';');
                    if(!Segment.isEmpty() && Segment.size() < 2 && SegmentToVerticalMap.containsKey(string.valueof(Segment[0]))){
                        x.Vertical__c = SegmentToVerticalMap.get(string.valueof(Segment[0]));
                    }
                }
                //Contract Type
                if(x.MSA_Agreement__c !=null && MSAAgreementMap.containsKey(x.MSA_Agreement__c)){
                    x.ContractType__c = MSAAgreementMap.get(x.MSA_Agreement__c);
                }else if(x.MSA_Agreement__c == null){                    
                    x.ContractType__c = 'Other';
                }
                //Clearance Explain            
                if(x.TGS_Does_team_require_security_clearance__c !=null && DoesTeamRequireSecurityClearanceMap.containsKey(x.TGS_Does_team_require_security_clearance__c)){
                    x.Clearance_Explain__c = DoesTeamRequireSecurityClearanceMap.get(x.TGS_Does_team_require_security_clearance__c);
                }
                //Drug & Background Explain
                if(x.TGS_Require_Drug_Background_Check__c !=null && DoesTeamRequireDrugAndBackgroundCheckMap.containsKey(x.TGS_Require_Drug_Background_Check__c)){
                    x.Drug_Test_Explain__c = DoesTeamRequireDrugAndBackgroundCheckMap.get(x.TGS_Require_Drug_Background_Check__c);
                    x.Background_Check_Explain__c = DoesTeamRequireDrugAndBackgroundCheckMap.get(x.TGS_Require_Drug_Background_Check__c);
                }
                //Response Type
                if(x.Response_Type__c !=null && ResponseTypeMap.containsKey(x.Response_Type__c)){
                    x.Response_Type__c = ResponseTypeMap.get(x.Response_Type__c);
                }
                //Pricing Billing Type
                if(x.Pricing_Billing_Type__c !=null && PricingBillingMap.containsKey(x.Pricing_Billing_Type__c)){
                    x.Pricing_Billing_Type__c = PricingBillingMap.get(x.Pricing_Billing_Type__c);
                }
                //Stage Name
                if(x.IsClosed == true && CloseReqStageMap.containsKey(x.StageName)){
                    x.StageName = CloseReqStageMap.get(x.StageName);					
                }else if(x.IsClosed == false && OpenReqStageMap.containsKey(x.StageName)){
                    x.StageName = OpenReqStageMap.get(x.StageName);
                }
                //Client as Reference
                if(x.Client_willing_to_be_reference__c == True){
                    x.Will_client_serve_as_a_reference__c = 'Yes';
                }else{
                    x.Will_client_serve_as_a_reference__c = 'No';
                }
                //Is Credit Approved
                /*if(x.Pricing_Approved_By_SSG__c == True){
                    x.Is_credit_approved__c = 'Yes';
                }else{
                    x.Is_credit_approved__c = 'No';
                }*/                   
                //Total Revenue
                x.Validate_Total_Revenue_Life_of_Deal__c = x.Amount;
                //OSG Approved
                x.OSG_Approved__c = x.Pricing_Approved_By_SSG__c;
                //Solution GP%
                x.Validate_Solutioned_GP__c = x.Services_GP__c;
                //Alligned SE
                x.Aligned_SE__c = x.Solution_Owner__c;
                //Deal Owner
                x.Deal_Owner__c = x.Delivery_Owner__c;
                x.Closed_Other_Details__c = 'N/A';				
                UpdatedHistoricalTGSOpp.add(x);
            }
            system.debug('--UpdatedHistoricalTGSOpp size--'+UpdatedHistoricalTGSOpp.size()+'---UpdatedHistoricalTGSOpp--'+UpdatedHistoricalTGSOpp);
            Database.SaveResult[] updatedResults = database.update(UpdatedHistoricalTGSOpp,false);
            for(Integer i=0;i<updatedResults.size();i++){
                if(!updatedResults.get(i).isSuccess()){
                    Database.Error error = updatedResults.get(i).getErrors().get(0);
                    system.debug('---error--' + error);
                    system.debug('---error--' + string.valueof(error.getMessage()));
                }
            }
            //if (!UpdatedHistoricalTGSOpp.isEmpty())
                //Database.Update(UpdatedHistoricalTGSOpp,false);
        }catch(Exception ex){
            system.debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext context){
    }
}