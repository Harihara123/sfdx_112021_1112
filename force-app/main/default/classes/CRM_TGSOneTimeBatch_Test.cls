@isTest(seealldata = false)
public class CRM_TGSOneTimeBatch_Test {
    static testMethod Void Test_TGSOneTimeBatch(){
    	List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();

        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Closed Won';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Global_Services_Opportunity_Type__c = 'New Business';
                newOpp.Win_Loss_Wash_reason__c = 'Past Performance';
                newOpp.Segment__c = 'Communications';
                newOpp.MSA_Agreement__c = 'No Services Agreement in place';
                newOpp.TGS_Does_team_require_security_clearance__c = 'No Services Agreement in place';
                newOpp.TGS_Require_Drug_Background_Check__c = 'YES';
                newOpp.Response_Type__c = 'Material Change PCR';
                newOpp.Pricing_Billing_Type__c = 'Time & Material - not to exceed';
                newOpp.Client_willing_to_be_reference__c = true;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys;
        Test.StartTest();
        CRM_TGSOneTimeBatch legacyOrderBatch = new CRM_TGSOneTimeBatch();
        legacyOrderBatch.strQuery = 'Select id, isclosed, StageName, Amount, Response_Type__c, Probability, Pricing_Billing_Type__c, Win_Loss_Wash_reason__c, Segment__c, MSA_Agreement__c, Pricing_Approved_By_SSG__c, Services_GP__c, TGS_Does_team_require_security_clearance__c, TGS_Require_Drug_Background_Check__c, Client_willing_to_be_reference__c, Solution_Owner__c, Delivery_Owner__c, Validate_Total_Revenue_Life_of_Deal__c, Closed_Reason_Category__c, Vertical__c, ContractType__c, OSG_Approved__c, Validate_Solutioned_GP__c, Clearance_Explain__c, Drug_Test_Explain__c, Background_Check_Explain__c, Will_client_serve_as_a_reference__c, Aligned_SE__c, Deal_Owner__c, Global_Services_Opportunity_Type__c, Closed_Other_Details__c from Opportunity where recordtype.name = \'TEKsystems Global Services\'';
        ID batchprocessid =  Database.executeBatch(legacyOrderBatch);        
        Test.StopTest();
    }
}