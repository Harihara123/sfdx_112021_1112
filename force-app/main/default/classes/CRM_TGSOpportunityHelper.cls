public without sharing class CRM_TGSOpportunityHelper{
    
    @AuraEnabled
    public static string stageValidationMessages(list<Opportunity> opptyNewList){
        Map<Id, Opportunity> opptyNewMap = new Map<Id, Opportunity>();
        string updatedStageName;
        for(Opportunity y : [Select id, StageName from Opportunity where id=: opptyNewList[0].id]){
            updatedStageName = y.StageName;
        }        
        for(Opportunity x: opptyNewList){
            if(x.StageName != updatedStageName){
                x.StageName = updatedStageName;
            }
            opptyNewMap.put(x.id,x);
        }
        Boolean ShouldShowValidation = false;
        String validationField = '';
        Map<String, Opportunity_Validation__mdt> stageAgainstValidationMap = new Map<String, Opportunity_Validation__mdt>();
        Map<String, String> fieldAgainstMessageMap = new Map<String, String>();
        for(Opportunity_Validation__mdt validate : [Select id, MasterLabel, isRequired__c, Message__c, Required_Opportunity_fields__c, Stage__c, Not_Required_Fields_For_Renewal_Opp__c from Opportunity_Validation__mdt where isRequired__c = true]){
            stageAgainstValidationMap.put(validate.stage__c, validate);
        }
        for(validation_Message__c val : validation_Message__c.getall().values()){
            if(val.Message__c != null)
                fieldAgainstMessageMap.put(val.FieldName__c, val.Message__c);
        }
        if(stageAgainstValidationMap.size() > 0){
            Boolean isInterest = false;
            Boolean isQualifying = false;
            Boolean isPursue = false;
            Boolean isPrebid = false;
            Boolean isPostbid = false;
            Integer indexCounter = 1;
            Integer indexInterest = 1;
            Integer indexQualifying = 1;
            Integer indexPursue = 1;
            Integer indexPrebid = 1;
            Integer indexPostbid = 1;
            
            for(Opportunity oppty : opptyNewMap.values()){
                Opportunity_Validation__mdt validate = stageAgainstValidationMap.get(oppty.StageName);
                List<String> lstRequiredField = String.valueOf(validate.Required_Opportunity_fields__c).split(',');
                Map<String, Object> dynamicOpptyMap = oppty.getPopulatedFieldsAsMap();
                try{
                    Boolean isRenewal = oppty.Global_Services_Opportunity_Type__c == 'Renewal' && 
                                            oppty.Response_Type__c == 'PCR(Renewal)' ? true : false;

                    if(isRenewal && String.isNotBlank(validate.Not_Required_Fields_For_Renewal_Opp__c)) {
                        List<String> lstNotRequiredFields = String.valueOf(validate.Not_Required_Fields_For_Renewal_Opp__c).split(',');                        
                        for(String fieldName: lstNotRequiredFields) {
                            if(lstRequiredField.indexOf(fieldName) != -1) {
                                lstRequiredField.remove(lstRequiredField.indexOf(fieldName));
                            }
                        }
                    }                    
                    for(String fieldName : lstRequiredField){                        
                        //system.debug('@#@#@#@'+fieldName);
                        if(!isInterest && fieldName == 'Interest_Stage'){
                            indexCounter = 1;
                            isInterest = true;
                            validationField = validationField + '\n Interest Stage' + ':' +  '\n';
                        }
                        
                        if(!isQualifying && fieldName == 'Qualifying_Stage'){
                            // this if statement is added to remove given string in case no validation error
                            if (indexCounter == 1) {
                               // validationField = validationField.remove('Interest Stage:'+'\n');
                               // validationField = validationField.remove('Qualifying Stage:'+'\n');
                            }
                            indexCounter = 1;
                            isQualifying = true;
                            validationField = validationField + '\n Qualifying Stage' + ':' +  '\n';
                        }if(!isPursue && fieldName == 'Pursue_Stage'){
                            // this if statement is added to remove given string in case no validation error
                            if (indexCounter == 1) {
                               // validationField = validationField.remove('Qualifying Stage:'+'\n');
                            }
                            indexCounter = 1;
                            isPursue = true;
                            validationField = validationField + '\n Solutioning Stage' + ':' +  '\n';
                        }
                        if(!isPrebid && fieldName == 'Prebid_Stage'){
                            // this if statement is added to remove given string in case no validation error
                            if (indexCounter == 1) {
                               // validationField = validationField.remove('Qualifying Stage:'+'\n');
                               // validationField = validationField.remove('Pursue Stage:'+'\n');
                                //validationField = validationField.remove('\n');
                            }
                            indexCounter = 1;
                            isPrebid = true;
                            validationField = validationField + '\n Proposing Stage' + ':' +  '\n';
                        }
                        if(!isPostbid && fieldName == 'Postbid_Stage'){
                            // this if statement is added to remove given string in case no validation error
                            if (indexCounter == 1) {
                               // validationField = validationField.remove('Qualifying Stage:'+'\n');
                               // validationField = validationField.remove('Pursue Stage:'+'\n');
                                //validationField = validationField.remove('Pre-Bid Stage:'+'\n');
                               // validationField = validationField.remove('\n');
                            }
                            indexCounter = 1;
                            isPostbid = true;
                            validationField = validationField + '\n Negotiating Stage' + ':' +  '\n';
                        }
                        if((dynamicOpptyMap.get(fieldName) == null || dynamicOpptyMap.get(fieldName) == '') && fieldAgainstMessageMap.get(fieldName) != null){
                            String fieldLabel = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel();//(fieldName.replace('__c','')).replace('_', '  ');
                            validationField = validationField + indexCounter + '.' + fieldLabel + ': ' + fieldAgainstMessageMap.get(fieldName) +  '\n' ;
                            ShouldShowValidation = true;
                            indexCounter++;
                        }
                    }                                      
                    if(!isRenewal && (oppty.Number_of_Competitors__c != NULL && oppty.Number_of_Competitors__c != '') && (oppty.Competitors_Strong_to_Weak__c == null || oppty.Competitors_Strong_to_Weak__c == '')){
                        validationField = validationField + indexCounter + '.' + 'Competitors (Strong to Weak)' + ': ' + 'Add at least one competitor.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(!isRenewal && (oppty.Number_of_Competitors__c != NULL && oppty.Number_of_Competitors__c != '') && (oppty.Decision_Criteria_Strong_to_Weak__c == null || oppty.Decision_Criteria_Strong_to_Weak__c== '')){
                        validationField = validationField + indexCounter + '.' + 'Decision Criteria (Strong to Weak)' + ': ' + 'Add at least one decision criteria.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(!isRenewal && (oppty.Number_of_Competitors__c != NULL && oppty.Number_of_Competitors__c != '') && (oppty.TGS_Differentiation_Strong_to_Weak__c == null || oppty.TGS_Differentiation_Strong_to_Weak__c == '')){
                        validationField = validationField + indexCounter + '.' + 'TGS Differentiation (Strong to Weak)' + ': ' + 'Add at least one TGS differentiation.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if((oppty.Global_Services_Opportunity_Type__c == 'Renewal' || oppty.Global_Services_Opportunity_Type__c == 'Expansion') && (oppty.Originating_Opportunity__c == null)){
                        validationField = validationField + indexCounter + '.' + 'Originating Opportunity' + ': ' + 'Please select a value.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(!isRenewal && (oppty.Economic_Buyer_Name__c == null && oppty.IT_Sponsor_Name__c == null && oppty.Business_Sponsor_Name__c == null && oppty.Procurement_Name__c == null) && oppty.StageName == 'Qualifying'){
                        validationField = validationField + indexCounter + '.' + 'Please complete one of the following fields (Economic Buyer Name, IT Sponsor Name, Business Sponsor Name, Procurement Name)' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if((oppty.Is_credit_approved__c == 'No' || oppty.Is_credit_approved__c == null) && oppty.StageName == 'Qualifying'){
                        validationField = validationField + indexCounter + '.' +  'Is credit approved?' + ': ' + 'Only Pending & Yes can be selected when going from Qualifying to Solutioning' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(oppty.Met_with_Economic_Buyer__c == 'Yes' && oppty.StageName != 'Interest' && oppty.Date_met_with_Economic_Buyer__c != null && oppty.Date_met_with_Economic_Buyer__c > system.today()){
                        validationField = validationField + indexCounter + '.' + 'Date met with Economic Buyer' + ': ' + 'If Met with Economic Buyer is Yes, then date is required.  Date can only be in the past or today.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(oppty.Met_with_IT_Sponsor__c == 'Yes' && oppty.StageName != 'Interest' && oppty.Date_met_with_IT_Sponsor__c != null && oppty.Date_met_with_IT_Sponsor__c > system.today()){
                        validationField = validationField + indexCounter + '.' + 'Date met with IT Sponsor' + ': ' + 'If Met with IT Sponsor is Yes, then date is required.  Date can only be in the past or today.' +  '\n' ;
                        indexCounter++;
                    }
                    if(oppty.Met_with_Business_Sponsor__c == 'Yes' && oppty.StageName != 'Interest' && oppty.Date_met_with_Business_Sponsor__c != null && oppty.Date_met_with_Business_Sponsor__c > system.today()){
                        validationField = validationField + indexCounter + '.' + 'Date met with Business Sponsor' + ': ' + 'If Met with Business Sponsor is Yes, then date is required.  Date can only be in the past or today.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(oppty.Met_with_Procurement__c == 'Yes' && oppty.StageName != 'Interest' && oppty.Date_met_with_Procurement__c != null && oppty.Date_met_with_Procurement__c > system.today()){
                        validationField = validationField + indexCounter + '.' + 'Date met with Procurement' + ': ' + 'If Met with Procurement is Yes, then date is required.  Date can only be in the past or today.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if((oppty.Is_credit_approved__c == 'No' || oppty.Is_credit_approved__c == 'Pending' || oppty.Is_credit_approved__c == null) && (oppty.StageName == 'Solutioning' || oppty.StageName == 'Proposing' || oppty.StageName == 'Negotiating')){
                        validationField = validationField + indexCounter + '.' + 'Is credit approved?' + ': ' + 'Only Yes can be selected beyond Solutioning.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }          
                    if(oppty.Billable_Expenses__c == 'Yes' && oppty.Billing_Expense_Instructions_Explain__c == null){
                        validationField = validationField + indexCounter + '.' + 'Billing Expense Instructions, Explain' + ': ' + 'If Billable Expenses is Yes, then this is required.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(oppty.OSG_Approved__c == false && oppty.StageName == 'Proposing'){
                        validationField = validationField + indexCounter + '.' + 'OSG Approved' + ': ' + 'OSG must approve.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if((oppty.Scope_Assumption_Changes_Explain__c == 'Yes' || oppty.Pricing_Changes_Explain__c == 'Yes' || oppty.Location_Changes_Explain__c == 'Yes' 
                       || oppty.Capacity_Changes_Explain__c == 'Yes' || oppty.Stakeholder_Changes_Explain__c == 'Yes' || oppty.Date_Changes_Explain__c == 'Yes' 
                       || oppty.Competitive_Landscape_Changes_Explain__c == 'Yes' || oppty.Practice_Delivery_Resources_Needed_List__c == 'Yes' 
                       || oppty.Concessions_required_by_Client_Explain__c == 'Yes'|| oppty.Concessions_required_by_TGS_Explain__c == 'Yes' 
                       || oppty.Terms_and_Conditions_Explain__c == 'Yes') && oppty.Summary_of_Negotiated_Changes__c == null && !isRenewal){
                           validationField = validationField + indexCounter + '.' + 'Summary of Negotiated Changes' + ': ' + 'Complete this field.' +  '\n' ;
                           indexCounter++;
                           ShouldShowValidation = true; 
                    }
                    if(oppty.Will_client_serve_as_a_reference__c == 'No' && (oppty.Client_Reference_Limitations_Explain__c == null || oppty.Client_Reference_Limitations_Explain__c == '')){
                        validationField = validationField + indexCounter + '.' + 'Client Reference Limitations, Explain' + ': ' + 'Complete this field.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(oppty.Can_we_use_their_name_in_marketing__c == 'No' && (oppty.Client_Testimonial_Exceptions_Explain__c == null || oppty.Client_Testimonial_Exceptions_Explain__c == '')){
                        validationField = validationField + indexCounter + '.' + 'Client Testimonial Exceptions, Explain' + ': ' + 'Complete this field.' +  '\n' ;
                        indexCounter++;
                        ShouldShowValidation = true;
                    }
                    if(!ShouldShowValidation)
                        validationField = ''; 
                        
                }
                Catch(Exception ex){
                    validationField = ex.getMessage();
                }   
            }
        }
       
        return validationField;
    }
    
    @AuraEnabled
    public static void UpdateTGSOppStage(string TGSOppId, string probValue){
       
        Map<String, String> stageProbabilityMap = new Map<String, String>{
                                                     'Interest'=> 'Qualifying,0%',
                                                     'Qualifying'=> 'Solutioning,0%',
                                                     'Solutioning'=> 'Proposing,50%'    
                                                  };                           
        list<Opportunity> UpdateTGSOpp = new list<Opportunity>();
        if(TGSOppId !=null && TGSOppId !=''){
            for(Opportunity x : [select id, StageName,Probability,Probability_TGS__c,Apex_Context__c from Opportunity where id =:TGSOppId]){
                if(stageProbabilityMap.get(x.StageName) != null && stageProbabilityMap.get(x.StageName) != ''){
                    List<String> lstTempString = (stageProbabilityMap.get(x.StageName)).split(',',2);
                    x.StageName = lstTempString[0];
                    x.Probability_TGS__c = string.valueOf(probValue);
					x.Apex_Context__c = true;
                }
                UpdateTGSOpp.add(x);
            }
        }
        
        if(!UpdateTGSOpp.isEmpty())
            update UpdateTGSOpp;
    }

    @AuraEnabled
    public static void ApproveOrRejectTGSOpportunity(string oppId, string approvalAction, string oppProbabilityValue){
       
        list<Opportunity> UpdateTGSOppProbability = new list<Opportunity>();
        List<ProcessInstanceWorkitem> approvalItems = [SELECT Id, ProcessInstanceId, ProcessInstance.Status 
                                                   FROM ProcessInstanceWorkitem 
                                                   WHERE ProcessInstance.TargetObjectId = :oppId and ProcessInstance.Status = 'Pending' limit 1];
       
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        if(!approvalItems.isEmpty()){
            for(ProcessInstanceWorkitem workItem : approvalItems){
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setWorkitemId(workItem.Id);
                req.setAction(string.valueof(approvalAction));
                requests.add(req);            
            }
        }
        if(!requests.isEmpty())
        Approval.ProcessResult[] processResults = Approval.process(requests);        
        /*Map<String, String> stageMap = new Map<String, String>{
                                                     'Pursue'=> 'Pre-Bid',
                                                     'Pre-Bid'=> 'Post-Bid Negotiation'
                                                  };*/
        Map<String, String> stageMap = new Map<String, String>{
                                                     'Proposing'=> 'Negotiating'
                                                  };                                              
        if(oppProbabilityValue !=null && oppProbabilityValue != ''){
            for(Opportunity x: [select id, Probability_TGS__c, StageName, Probability, Apex_Context__c from Opportunity where id=:oppId]){
                x.Probability_TGS__c = string.valueof(oppProbabilityValue);
                if(stageMap.get(x.StageName) != null && stageMap.get(x.StageName) != '' && requests.isEmpty()){
                    x.StageName = stageMap.get(x.StageName);
					x.Apex_Context__c = true;
                }
                UpdateTGSOppProbability.add(x);
            }
        }
        system.debug('--UpdateTGSOppProbability--'+UpdateTGSOppProbability);
        if(!UpdateTGSOppProbability.isEmpty())
            update UpdateTGSOppProbability;        
    }
    
    @AuraEnabled
    public static string getTGSOppProcessInstance(string oppId){
        string ProcessStatus = '';
        List<ProcessInstanceWorkitem> approvalItems = [SELECT Id, ProcessInstanceId, ProcessInstance.Status 
                                                   FROM ProcessInstanceWorkitem 
                                                   WHERE ProcessInstance.TargetObjectId = :oppId];
        if(!approvalItems.isEmpty()){
            for(ProcessInstanceWorkitem x: approvalItems){
                ProcessStatus = x.ProcessInstance.Status;
            }
        }
        return ProcessStatus;
    }
    
    @AuraEnabled
    public static void updateOppBidNotifiction(string oppId){
        list<Opportunity> UpdateTGSOpp = new list<Opportunity>(); 
        for(Opportunity opp:[select id,TGS_Bid_Notification_Date__c,TGS_Bid_Notification_User__c from Opportunity where id =:oppId]){
            if(opp.TGS_Bid_Notification_Date__c == null && opp.TGS_Bid_Notification_User__c == null){
                opp.TGS_Bid_Notification_Date__c = System.Now();
                opp.TGS_Bid_Notification_User__c = UserInfo.getUserId();
            }
            UpdateTGSOpp.add(opp);
        }
        if(!UpdateTGSOpp.isEmpty())
            update UpdateTGSOpp;
    }
    
    @AuraEnabled
    public static String getStageInstruction(String stageValue, string oppId){
        Boolean isApproved = false;
        string ProcessInstance = getTGSOppProcessInstance(oppId);
        if(ProcessInstance == 'Pending')
            isApproved = true;
        String instructions = '';
        User currentUser = [Select id, IsRegionalDirector__c, Profile.Name from User where id =: userinfo.getUserId()];
        for(Opportunity_Validation__mdt validate : [Select id, MasterLabel, RD_View__c, Non_RD_View__c, isRequired__c, Message__c, Required_Opportunity_fields__c, Stage__c, Closed_Reason__c, Closed_Reason_Category__c, NonRD_View_PendingApproval__c from Opportunity_Validation__mdt where Stage__c =: stageValue]){
            if(stageValue == 'Interest'){
                instructions = validate.Non_RD_View__c;
            }
            else if(stageValue == 'Qualifying'){
                if(currentUser.Profile.Name == label.TGS_Executive_Profile && isApproved == true){
                    instructions = validate.RD_View__c;
                }else if(currentUser.Profile.Name != label.TGS_Executive_Profile && isApproved == true){
                    instructions = validate.NonRD_View_PendingApproval__c;
                }else{
                    instructions = validate.Non_RD_View__c;
                }
            }
            else if(stageValue == 'Solutioning'){
                if(isApproved == true){
                    if(currentUser.Profile.Name == label.TGS_Executive_Profile){
                        instructions = validate.RD_View__c;
                    }else if(currentUser.Profile.Name != label.TGS_Executive_Profile){
                        instructions = validate.Message__c;
                    }    
                }else{
                    if(currentUser.Profile.Name == label.TGS_Executive_Profile){
                        instructions = validate.Non_RD_View__c;
                    }else if(currentUser.Profile.Name != label.TGS_Executive_Profile){
                        instructions = validate.NonRD_View_PendingApproval__c;
                    } 
                }
            }else if(stageValue == 'Proposing'){
                if(currentUser.Profile.Name == label.TGS_Executive_Profile){
                    instructions = validate.RD_View__c;
                }else if(currentUser.Profile.Name != label.TGS_Executive_Profile){
                    instructions = validate.Non_RD_View__c;
                }                
            }else if(stageValue == 'Negotiating'){
                instructions = validate.Non_RD_View__c;
            }
        }       
        return instructions;
    }    
    
	@AuraEnabled
    public static void emailNegotiatingNotification(string oppId){
        List<Messaging.SingleEmailMessage> listmail = new List<Messaging.SingleEmailMessage>();
        EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where developername='TGS_Opportunity_Negotiating_Review' limit 1];
        List<String> sendTo = new List<String>();
        for(Opportunity x: [select id, name, OwnerId, Owner.Name, Regional_Director__c from Opportunity where id=:oppId limit 1]){
            string linkURL = URL.getSalesforceBaseUrl().toExternalForm()+ '/' + x.Id;
            sendTo.add(x.Regional_Director__c);
			sendTo.add(x.OwnerId);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(sendTo);
            mail.setSenderDisplayName(x.owner.name);
            mail.setTargetObjectId(x.ownerID);
            mail.setSaveAsActivity(false);
            mail.setSubject('Opportunity '+ x.Name+' Ready to Advance to Negotiating');
            mail.setPlainTextBody('Opportunity '+ x.Name+' ready to advance to Negotiating.\n \n'+'Regional Director, click here to approve: '+linkURL);
            listmail.add(mail);
        }
        if(!listmail.isEmpty())
        Messaging.SendEmail(listmail);
    }
    /*@AuraEnabled
    public static String closeOppty(Opportunity oppty,String closedReason,String closedCategory, String otherDetails){
        String message = 'Success';
        system.debug('@@@@Closed Reason:'+closedReason);
        system.debug('@@@@Closed Reason:'+closedCategory);
        try{
            oppty.closed_Reason_Category__c = closedCategory;
            oppty.Closed_Reason__c = closedReason;
            oppty.Closed_Other_Details__c = otherDetails;
            //oppty.StageName = 'Closed';
            if(oppty.StageName == 'Qualifying' || oppty.StageName == 'Pursue' || oppty.StageName == 'Pre-Bid' || oppty.StageName == 'Post-Bid Negotiation')
            {
                oppty.StageName = closedReason; 
                if(closedReason == 'Won')
                {
                    oppty.Probability = 100;
                }
                else
                {
                    oppty.Probability = oppty.Probability;
                }
            }
            update oppty;
        }
        catch(Exception ex){
            message = ex.getMessage();
        }
        return message;
    }
    
    // Code below is for dependent picklist
    @AuraEnabled
    public static List<Map<String, List<String>>> createDependentMap(sObject objDetail){
        List<Map<String, List<String>>> objResults = new List<Map<String, List<String>>>();
        objResults.add(CRM_TGSOpportunityHelper.getDependentMap(objDetail, 'StageName', 'Closed_Reason__c'));
        objResults.add(CRM_TGSOpportunityHelper.getDependentMap(objDetail, 'Closed_Reason__c', 'closed_Reason_Category__c'));
        
        return objResults;
    }
    public static Map<String, List<String>> getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        
        Schema.sObjectType objType = objDetail.getSObjectType();
        if (objType==null){
            return objResults;
        }
        
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objResults;     
        }
        
        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        
        return objResults;
    }
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';
        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        
        return validForBits;
    }
    
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }
    
    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }
        
    }*/
     @AuraEnabled
    public static Boolean isTGSEMEADirectorGroupMember(){
    
         List<groupmember> groupMembers = [SELECT userorgroupid FROM groupmember 
                                            WHERE group.DeveloperName  = 'TGS_EMEA_Directors'
                                            AND userorgroupid =: UserInfo.getUserId()];
    
            boolean isTGSEMEADirectorGroupMember = (groupMembers.size() == 0)?false:true;
            
            return isTGSEMEADirectorGroupMember;
        
    } 
}