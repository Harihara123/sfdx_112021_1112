@isTest(seealldata = false)
public class CRM_TGSOpportunityHelper_Test{
    static testMethod Void Test_TGSOpportunityHelper(){     
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;        
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Qualifying';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                newOpp.Number_of_Competitors__c = '1';
                newOpp.Global_Services_Opportunity_Type__c = 'Renewal';
				newOpp.Is_credit_approved__c = 'No';
                newOpp.Will_client_serve_as_a_reference__c = 'No';
				newOpp.Can_we_use_their_name_in_marketing__c = 'No';
                newOpp.Billable_Expenses__c = 'Yes';
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
         Test.StartTest();
        CRM_TGSOpportunityHelper.stageValidationMessages(lstOpportunitys);
        lstOpportunitys[0].StageName = 'Solutioning';
        update lstOpportunitys;
        
        Opportunity_Validation__mdt OppMDT = new Opportunity_Validation__mdt(MasterLabel = 'Solutioning', RD_View__c = '', Non_RD_View__c = '', isRequired__c = true, Message__c = 'This is test class validation', Required_Opportunity_fields__c = 'Regional_Director__c', Stage__c = 'Solutioning', NonRD_View_PendingApproval__c = 'Test', Closed_Reason__c = '', Closed_Reason_Category__c = '');
        Validation_Message__c ValMsg = new Validation_Message__c(Name = '1',FieldName__c = 'Regional_Director__c', Message__c ='Complete this field.');
        insert ValMsg;
       
        CRM_TGSOpportunityHelper.stageValidationMessages(lstOpportunitys);
        CRM_TGSOpportunityHelper.UpdateTGSOppStage(string.valueof(lstOpportunitys[0].id),'0%');
        CRM_TGSOpportunityHelper.ApproveOrRejectTGSOpportunity(string.valueof(lstOpportunitys[0].id), 'Approve', '50');
        string TGSOppProcessInstance = CRM_TGSOpportunityHelper.getTGSOppProcessInstance(lstOpportunitys[0].id);
        string StageInstruction = CRM_TGSOpportunityHelper.getStageInstruction('Pursue',string.valueof(lstOpportunitys[0].id));
		CRM_TGSOpportunityHelper.updateOppBidNotifiction(string.valueof(lstOpportunitys[0].id));
        Test.StopTest(); 
    }
    
    static testMethod Void Test_TGSOpportunityHelper_Qualifying(){   
        Profile p = [SELECT id, Name FROM Profile WHERE Name ='System Administrator' LIMIT 1 ];
        User u = new User(CompanyName = 'TestComp',FirstName = 'first',LastName = 'last',LocaleSidKey = 'en_CA',UserName = 'testfirst.last@allegisgroup.com',
            TimeZoneSidKey = 'America/Indiana/Indianapolis', Email = 'testfrt.last@allegisgroup.com',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US',
            Alias = 't12345',Team__c = 'Birmingham AC Perm',OPCO__c = 'TEM',Region__c='POPULUS',Region_Code__c='POPUL',ProfileId = p.Id,IsRegionalDirector__c = true
        );
        System.runAs(u){
            List<Opportunity> lstOpportunitys = new List<Opportunity>();
            TestData TdAccts = new TestData(1);
            List<Account> lstNewAccounts = TdAccts.createAccounts();
            TestData TestCont = new TestData();
            testdata tdConts = new TestData();
            string recTypes = 'Recruiter';
            list<Contact> TdContObj = TestCont.createContacts(recTypes); 
            string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
            Job_Title__c jt = new Job_Title__c(Name ='Admin');
            insert jt;
            List<User_Organization__c> orgList = new List<User_Organization__c>();
            User_Organization__c org1 = new User_Organization__c();
            Set<String> officeList = new Set<String>();
            org1.Active__c = true;
            org1.Office_Code__c = 'Office1';
            org1.OpCo_Code__c = 'Opco1';
            officeList.add(org1.Office_Code__c);
            orgList.add(org1);
            insert orgList;
            List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
            for(Account Acc : TdAccts.createAccounts()) {       
                for(Integer i=0;i < 1; i++){
                    Opportunity newOpp = new Opportunity();
                    newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                    newOpp.Accountid = Acc.Id;
                    newOpp.RecordTypeId = ReqRecordType;
                    Date closingdate = system.today();
                    newOpp.CloseDate = closingdate.addDays(25);
                    newOpp.StageName = 'Interest';
                    newOpp.Req_Total_Positions__c = 2;
                    newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                    newOpp.Req_Job_Description__c = 'Testing';
                    newOpp.Req_HRXML_Field_Updated__c = true;
                    newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                    newOpp.Opco__c = 'Aerotek, Inc.';
                    newOpp.BusinessUnit__c = 'Board Practice';
                    newOpp.Req_Product__c = 'Permanent';
                    newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                    newOpp.Req_Worksite_Street__c = '987 Hidden St';
                    newOpp.Req_Worksite_City__c = 'Baltimore';
                    newOpp.Req_Worksite_Postal_Code__c = '21228';
                    newOpp.Req_Worksite_Country__c = 'United States';
                    newOpp.Req_Duration_Unit__c = 'Day(s)';
                    newOpp.Status__c = 'Open';
                    newOpp.Location__c = 'Local Field Office';
                    newOpp.Organization_Office__c = orgList[0].id;
                    newOpp.Regional_Director__c = U.id;
                    lstOpportunitys.add(newOpp);
                }     
            }
            insert lstOpportunitys; 
            lstOpportunitys[0].StageName = 'Qualifying';
            update lstOpportunitys;
            
            Opportunity_Validation__mdt OppMDT = new Opportunity_Validation__mdt(MasterLabel = 'Qualifying', NonRD_View_PendingApproval__c = 'Test', RD_View__c = '', Non_RD_View__c = '', isRequired__c = true, Message__c = 'This is test class validation', Required_Opportunity_fields__c = 'Regional_Director__c', Stage__c = 'Qualifying', Closed_Reason__c = '', Closed_Reason_Category__c = '');
            Validation_Message__c ValMsg = new Validation_Message__c(Name = '1',FieldName__c = 'Regional_Director__c', Message__c ='Complete this field.');
            insert ValMsg;        
            
            Test.StartTest();
            string StageInstruction = CRM_TGSOpportunityHelper.getStageInstruction('Qualifying',string.valueof(lstOpportunitys[0].id));
            CRM_TGSOpportunityHelper.emailNegotiatingNotification(string.valueof(lstOpportunitys[0].id));
            Test.StopTest(); 
        }
    }
    static testMethod Void Test_TGSOpportunityHelper_Solutioning(){           
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Qualifying';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        lstOpportunitys[0].StageName = 'Solutioning';
        update lstOpportunitys;
        
        Opportunity_Validation__mdt OppMDT = new Opportunity_Validation__mdt(MasterLabel = 'Solutioning', NonRD_View_PendingApproval__c = 'Test', RD_View__c = '', Non_RD_View__c = '', isRequired__c = true, Message__c = 'This is test class validation', Required_Opportunity_fields__c = 'Regional_Director__c', Stage__c = 'Solutioning', Closed_Reason__c = '', Closed_Reason_Category__c = '');
        Validation_Message__c ValMsg = new Validation_Message__c(Name = '1',FieldName__c = 'Regional_Director__c', Message__c ='Complete this field.');
        insert ValMsg;                    
        Test.StartTest();
        string StageInstruction = CRM_TGSOpportunityHelper.getStageInstruction('Solutioning',string.valueof(lstOpportunitys[0].id));
        Test.StopTest();        
    }
    static testMethod Void Test_TGSOpportunityHelper_Negotiating(){           
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Proposing';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        lstOpportunitys[0].StageName = 'Negotiating';
        update lstOpportunitys;
        
        Opportunity_Validation__mdt OppMDT = new Opportunity_Validation__mdt(MasterLabel = 'Negotiating', NonRD_View_PendingApproval__c = 'Test', RD_View__c = '', Non_RD_View__c = '', isRequired__c = true, Message__c = 'This is test class validation', Required_Opportunity_fields__c = 'Regional_Director__c', Stage__c = 'Negotiating', Closed_Reason__c = '', Closed_Reason_Category__c = '');
        Validation_Message__c ValMsg = new Validation_Message__c(Name = '1',FieldName__c = 'Regional_Director__c', Message__c ='Complete this field.');
        insert ValMsg;                    
        Test.StartTest();
        string StageInstruction = CRM_TGSOpportunityHelper.getStageInstruction('Negotiating',string.valueof(lstOpportunitys[0].id));
        Test.StopTest();        
    }
    static testMethod Void Test_TGSOpportunityHelper_Proposing(){           
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Solutioning';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        lstOpportunitys[0].StageName = 'Proposing';
        update lstOpportunitys;
        
        Opportunity_Validation__mdt OppMDT = new Opportunity_Validation__mdt(MasterLabel = 'Proposing', NonRD_View_PendingApproval__c = 'Test', RD_View__c = '', Non_RD_View__c = '', isRequired__c = true, Message__c = 'This is test class validation', Required_Opportunity_fields__c = 'Regional_Director__c', Stage__c = 'Proposing', Closed_Reason__c = '', Closed_Reason_Category__c = '');
        Validation_Message__c ValMsg = new Validation_Message__c(Name = '1',FieldName__c = 'Regional_Director__c', Message__c ='Complete this field.');
        insert ValMsg;                    
        Test.StartTest();
        string StageInstruction = CRM_TGSOpportunityHelper.getStageInstruction('Proposing',string.valueof(lstOpportunitys[0].id));
        Test.StopTest();        
    }
    @isTest(seealldata = true)
    static void Test_isTGSEMEADirectorGroupMember()
    {
        Test.startTest();
        Group gp = [select id from group where developername = 'TGS_EMEA_Directors' limit 1];
        Groupmember gm = new Groupmember();
        gm.GroupId = gp.Id;
        gm.UserOrGroupId = Userinfo.getUserId();
        insert gm;
       boolean isTGSEMEADirectorGroupMember = CRM_TGSOpportunityHelper.isTGSEMEADirectorGroupMember(); 
        Test.stopTest();
            
    }
}