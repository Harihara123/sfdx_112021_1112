public class CRM_TGSRelatedTriggerHelper 
{
    public static void updateOpportunity(List<String> oppList,Map<Id,TGS_Differentiation_Strongest_Weakest__c> newTGSMap,
                                        Map<Id,Decision_Criteria_Strongest_to_Weakest__c> newDCSMap,Map<Id,Competitors_Strongest_to_Weakest__c> newCSWMap)
    {
        List<Opportunity> updateList = new List<Opportunity>();
        
        Map<String,Set<String>> TGSDOrderMap = new Map<String,Set<String>>();
        Set<String> newTGSIdSet = new Set<String>();
        if(newTGSMap != NULL)
        {
            for(TGS_Differentiation_Strongest_Weakest__c TGSVar:newTGSMap.values())
            {
                newTGSIdSet.add(TGSVar.Id);
            }
        }
        
        Map<String,Set<String>> DCSOrderMap = new Map<String,Set<String>>();
        Set<String> newDCSIdSet = new Set<String>();
        if(newDCSMap != NULL)
        {
            for(Decision_Criteria_Strongest_to_Weakest__c DCSVar:newDCSMap.values())
            {
                newDCSIdSet.add(DCSVar.Id);
            }
        }
        
        Map<String,Set<String>> CSWOrderMap = new Map<String,Set<String>>();
        Set<String> newCSWIdSet = new Set<String>();
        if(newCSWMap != NULL)
        {
            for(Competitors_Strongest_to_Weakest__c CSWVar:newCSWMap.values())
            {
                newCSWIdSet.add(CSWVar.Id);
            }
        }
        
        Map<Id,Opportunity> opptyMap = new Map<Id,Opportunity>([SELECT id, TGS_Differentiation_Strong_to_Weak__c,Decision_Criteria_Strong_to_Weak__c,
                                                                Competitors_Strong_to_Weak__c,
                                                                (SELECT Strongest_to_Weakest__c,TGS_Differentiation__c,Opportunity__c  
                                                                 FROM TGS_Differentiation_Strongest_Weakest__r ORDER BY Strongest_to_Weakest__c),
                                                                (SELECT Strongest_to_Weakest__c,Decision_Criteria__c,Opportunity__c 
                                                                 FROM Decision_Criteria_Strongest_to_Weakest__r ORDER BY Strongest_to_Weakest__c),
                                                                (SELECT Strongest_to_Weakest__c,Competitor__c,Opportunity__c 
                                                                 FROM Competitors_Strongest_to_Weakest__r ORDER BY Strongest_to_Weakest__c)
                                                                FROM Opportunity 
                                                                WHERE id IN: oppList]);
        
        List<TGS_Differentiation_Strongest_Weakest__c> TGSDList = new List<TGS_Differentiation_Strongest_Weakest__c>();
        List<Decision_Criteria_Strongest_to_Weakest__c> DecCriteriaList = new List<Decision_Criteria_Strongest_to_Weakest__c>();
        List<Competitors_Strongest_to_Weakest__c> competitorList = new List<Competitors_Strongest_to_Weakest__c>();
        
        for(Opportunity opp : opptyMap.values())
        {
            TGSDList.addAll(opp.TGS_Differentiation_Strongest_Weakest__r);
            DecCriteriaList.addAll(opp.Decision_Criteria_Strongest_to_Weakest__r);
            competitorList.addAll(opp.Competitors_Strongest_to_Weakest__r);
        }
        System.debug('TGSDList:'+TGSDList.size());
        Map<String,String> tempDifferentiationStringMap = new Map<String,String>();
        Map<String,String> tempDecisionCriteriaStringMap = new Map<String,String>();
        Map<String,String> tempCompetitorStringMap = new Map<String,String>();
        
        for(TGS_Differentiation_Strongest_Weakest__c TGSVar:TGSDList)
        {
            if(!TGSDOrderMap.containsKey(TGSVar.Opportunity__c) && !newTGSIdSet.contains(TGSVar.Id))
            {
                TGSDOrderMap.put(TGSVar.Opportunity__c,new Set<String>{TGSVar.Strongest_to_Weakest__c});
            }
            else if(!newTGSIdSet.contains(TGSVar.Id))
            {
                TGSDOrderMap.get(TGSVar.Opportunity__c).add(TGSVar.Strongest_to_Weakest__c);
            }
            
            if(!tempDifferentiationStringMap.containsKey(TGSVar.Opportunity__c))
            {
                String tempDifferentiationStringValue = (TGSVar.Strongest_to_Weakest__c == null ? '' : TGSVar.Strongest_to_Weakest__c) + '. ' + (TGSVar.TGS_Differentiation__c == null ? '' :TGSVar.TGS_Differentiation__c) + '\n';
                tempDifferentiationStringMap.put(TGSVar.Opportunity__c,tempDifferentiationStringValue);
            }
            else
            {
                String tempDifferentiationStringValueTemp = (TGSVar.Strongest_to_Weakest__c == null ? '' : TGSVar.Strongest_to_Weakest__c) + '. ' + (TGSVar.TGS_Differentiation__c == null ? '' :TGSVar.TGS_Differentiation__c) + '\n';
                String existingValue = tempDifferentiationStringMap.get(TGSVar.Opportunity__c);  
                tempDifferentiationStringMap.put(TGSVar.Opportunity__c,existingValue + tempDifferentiationStringValueTemp);
            }
        }
        
        for(Decision_Criteria_Strongest_to_Weakest__c obj:DecCriteriaList)
        {
            if(!DCSOrderMap.containsKey(obj.Opportunity__c) && !newDCSIdSet.contains(obj.Id))
            {
                DCSOrderMap.put(obj.Opportunity__c,new Set<String>{obj.Strongest_to_Weakest__c});
            }
            else if(!newDCSIdSet.contains(obj.Id))
            {
                DCSOrderMap.get(obj.Opportunity__c).add(obj.Strongest_to_Weakest__c);
            }
            
            if(!tempDecisionCriteriaStringMap.containsKey(obj.Opportunity__c))
            {
                String tempDifferentiationStringValue = (obj.Strongest_to_Weakest__c == null ? '' : obj.Strongest_to_Weakest__c) + '. ' + (obj.Decision_Criteria__c == null ? '' :obj.Decision_Criteria__c) + '\n';
                tempDecisionCriteriaStringMap.put(obj.Opportunity__c,tempDifferentiationStringValue);
            }
            else
            {
                String tempDifferentiationStringValueTemp = (obj.Strongest_to_Weakest__c == null ? '' : obj.Strongest_to_Weakest__c) + '. ' + (obj.Decision_Criteria__c == null ? '' :obj.Decision_Criteria__c) + '\n';
                String existingValue = tempDecisionCriteriaStringMap.get(obj.Opportunity__c);  
                tempDecisionCriteriaStringMap.put(obj.Opportunity__c,existingValue + tempDifferentiationStringValueTemp);
            }
        }
        
        for(Competitors_Strongest_to_Weakest__c obj:competitorList)
        {
            if(!CSWOrderMap.containsKey(obj.Opportunity__c) && !newCSWIdSet.contains(obj.Id))
            {
                CSWOrderMap.put(obj.Opportunity__c,new Set<String>{obj.Strongest_to_Weakest__c});
            }
            else if(!newCSWIdSet.contains(obj.Id))
            {
                CSWOrderMap.get(obj.Opportunity__c).add(obj.Strongest_to_Weakest__c);
            }
            
            if(!tempCompetitorStringMap.containsKey(obj.Opportunity__c))
            {
                String tempDifferentiationStringValue = (obj.Strongest_to_Weakest__c == null && obj.Competitor__c == null ? '' : obj.Strongest_to_Weakest__c +'. '+obj.Competitor__c)+ '\n';
                tempCompetitorStringMap.put(obj.Opportunity__c,tempDifferentiationStringValue);
            }
            else
            {
                String tempDifferentiationStringValueTemp = (obj.Strongest_to_Weakest__c == null && obj.Competitor__c == null ? '' : obj.Strongest_to_Weakest__c +'. '+obj.Competitor__c)+ '\n';
                String existingValue = tempCompetitorStringMap.get(obj.Opportunity__c);  
                tempCompetitorStringMap.put(obj.Opportunity__c,existingValue + tempDifferentiationStringValueTemp);
            }
        }            
        
        System.debug('DCSOrderMap:'+DCSOrderMap);
        System.debug('tempDifferentiationStringMap:'+tempDifferentiationStringMap);
        List<Opportunity> oppVarToUpdateList = new List<Opportunity>();
        Map<Id,Opportunity> oppMapToUpdate = new Map<Id,Opportunity>();
        for(String oppVar:oppList)
        {
            Opportunity oppVarToUpdate;
            if(oppMapToUpdate.containsKey(oppVar))
            {
                oppVarToUpdate = oppMapToUpdate.get(oppVar);
            }
            else
            {
                oppVarToUpdate = new Opportunity();
                oppVarToUpdate.Id = oppVar;
            }
			if(tempDifferentiationStringMap.containsKey(oppVar))
            {
                oppVarToUpdate.TGS_Differentiation_Strong_to_Weak__c = tempDifferentiationStringMap.get(oppVar);
            }
            if(tempDecisionCriteriaStringMap.containsKey(oppVar))
            {
                oppVarToUpdate.Decision_Criteria_Strong_to_Weak__c = tempDecisionCriteriaStringMap.get(oppVar);
            }
            if(tempCompetitorStringMap.containsKey(oppVar))
            {
                oppVarToUpdate.Competitors_Strong_to_Weak__c = tempCompetitorStringMap.get(oppVar);
            }
            //oppVarToUpdateList.add(oppVarToUpdate);
            oppMapToUpdate.put(oppVar,oppVarToUpdate);
        }
        
        
        if(newTGSMap != NULL)
        {
            for(TGS_Differentiation_Strongest_Weakest__c TGSVar:newTGSMap.values())
            {
                if(TGSDOrderMap.containsKey(TGSVar.Opportunity__c) && TGSDOrderMap.get(TGSVar.Opportunity__c).contains(TGSVar.Strongest_to_Weakest__c))
                {
                    TGSVar.addError('Please rank correctly.');
                }
            }
        }
        if(newDCSMap != NULL)
        {
            for(Decision_Criteria_Strongest_to_Weakest__c DCSVar:newDCSMap.values())
            {
                if(DCSOrderMap.containsKey(DCSVar.Opportunity__c) && DCSOrderMap.get(DCSVar.Opportunity__c).contains(DCSVar.Strongest_to_Weakest__c))
                {
                    DCSVar.addError('Please rank correctly 11.');
                }
            }
        }
        if(newCSWMap != NULL)
        {
            for(Competitors_Strongest_to_Weakest__c CSWVar:newCSWMap.values())
            {
                if(CSWOrderMap.containsKey(CSWVar.Opportunity__c) && CSWOrderMap.get(CSWVar.Opportunity__c).contains(CSWVar.Strongest_to_Weakest__c))
                {
                    CSWVar.addError('Please rank correctly.');
                }
            }
        }
        system.debug('--oppVarToUpdateList--'+oppMapToUpdate.values());
        if(oppMapToUpdate.size() > 0)
        {
            update oppMapToUpdate.values();
        }
        
    }
    
    
    
    
    
    
    
    
    
    public static void updateOpportunityBackup(List<String> oppList,Map<Id,TGS_Differentiation_Strongest_Weakest__c> newTGSMap,
                                        Map<Id,Decision_Criteria_Strongest_to_Weakest__c> newDCSMap,Map<Id,Competitors_Strongest_to_Weakest__c> newCSWMap)
    {
        List<Opportunity> updateList = new List<Opportunity>();
        
        Map<String,Set<String>> TGSDOrderMap = new Map<String,Set<String>>();
        Set<String> newTGSIdSet = new Set<String>();
        if(newTGSMap != NULL)
        {
            for(TGS_Differentiation_Strongest_Weakest__c TGSVar:newTGSMap.values())
            {
                newTGSIdSet.add(TGSVar.Id);
            }
        }
        
        Map<String,Set<String>> DCSOrderMap = new Map<String,Set<String>>();
        Set<String> newDCSIdSet = new Set<String>();
        if(newDCSMap != NULL)
        {
            for(Decision_Criteria_Strongest_to_Weakest__c DCSVar:newDCSMap.values())
            {
                newDCSIdSet.add(DCSVar.Id);
            }
        }
        
        Map<String,Set<String>> CSWOrderMap = new Map<String,Set<String>>();
        Set<String> newCSWIdSet = new Set<String>();
        if(newCSWMap != NULL)
        {
            for(Competitors_Strongest_to_Weakest__c CSWVar:newCSWMap.values())
            {
                newCSWIdSet.add(CSWVar.Id);
            }
        }
        
        for(Opportunity opp : [SELECT id, TGS_Differentiation_Strong_to_Weak__c,Decision_Criteria_Strong_to_Weak__c,
                               Competitors_Strong_to_Weak__c,
                               (SELECT Strongest_to_Weakest__c,TGS_Differentiation__c,Opportunity__c  
                                FROM TGS_Differentiation_Strongest_Weakest__r ORDER BY Strongest_to_Weakest__c),
                               (SELECT Strongest_to_Weakest__c,Decision_Criteria__c,Opportunity__c 
                                FROM Decision_Criteria_Strongest_to_Weakest__r ORDER BY Strongest_to_Weakest__c),
                               (SELECT Strongest_to_Weakest__c,Competitor__c,Opportunity__c 
                                FROM Competitors_Strongest_to_Weakest__r ORDER BY Strongest_to_Weakest__c)
                               FROM Opportunity 
                               WHERE id IN: oppList])
        {
            String tempDifferentiationString = '';
            String tempDecisionString = '';
            String tempCompetitorsString = '';
            for(TGS_Differentiation_Strongest_Weakest__c TGSVar:opp.TGS_Differentiation_Strongest_Weakest__r)
            {
                Set<String> existingRankings = TGSDOrderMap.get(opp.Id);
                if(!TGSDOrderMap.containsKey(TGSVar.Opportunity__c) && !newTGSIdSet.contains(TGSVar.Id))
                {
                    TGSDOrderMap.put(TGSVar.Opportunity__c,new Set<String>{TGSVar.Strongest_to_Weakest__c});
                }
                else if(!newTGSIdSet.contains(TGSVar.Id))
                {
                    TGSDOrderMap.get(TGSVar.Opportunity__c).add(TGSVar.Strongest_to_Weakest__c);
                }
                
                if(TGSVar.Strongest_to_Weakest__c != null || TGSVar.TGS_Differentiation__c != null)
                {
                    tempDifferentiationString = tempDifferentiationString + (TGSVar.Strongest_to_Weakest__c == null ? '' : TGSVar.Strongest_to_Weakest__c) + '. ' + (TGSVar.TGS_Differentiation__c == null ? '' :TGSVar.TGS_Differentiation__c) + '\n';
                }
            } 
            for(Decision_Criteria_Strongest_to_Weakest__c obj:opp.Decision_Criteria_Strongest_to_Weakest__r)
            {
                Set<String> existingRankingsDCS = DCSOrderMap.get(opp.Id);
                if(!DCSOrderMap.containsKey(obj.Opportunity__c) && !newDCSIdSet.contains(obj.Id))
                {
                    DCSOrderMap.put(obj.Opportunity__c,new Set<String>{obj.Strongest_to_Weakest__c});
                }
                else if(!newDCSIdSet.contains(obj.Id))
                {
                    DCSOrderMap.get(obj.Opportunity__c).add(obj.Strongest_to_Weakest__c);
                }
                
                if(obj.Strongest_to_Weakest__c != null || obj.Decision_Criteria__c != null)
                {
                    tempDecisionString = tempDecisionString + (obj.Strongest_to_Weakest__c == null ? '' : obj.Strongest_to_Weakest__c) + '. ' + (obj.Decision_Criteria__c == null ? '' :obj.Decision_Criteria__c) + '\n';
                }
            }
            for(Competitors_Strongest_to_Weakest__c obj : opp.Competitors_Strongest_to_Weakest__r)
            {
                Set<String> existingRankingsDCS = CSWOrderMap.get(opp.Id);
                if(!CSWOrderMap.containsKey(obj.Opportunity__c) && !newCSWIdSet.contains(obj.Id))
                {
                    CSWOrderMap.put(obj.Opportunity__c,new Set<String>{obj.Strongest_to_Weakest__c});
                }
                else if(!newCSWIdSet.contains(obj.Id))
                {
                    CSWOrderMap.get(obj.Opportunity__c).add(obj.Strongest_to_Weakest__c);
                }
                
                if(obj.Strongest_to_Weakest__c != null || obj.Competitor__c != null)
                {
                    tempCompetitorsString = tempCompetitorsString + (obj.Strongest_to_Weakest__c == null ? '' : obj.Strongest_to_Weakest__c) + '. ' + (obj.Competitor__c == null ? '' :obj.Competitor__c) + '\n';
                }
            }            
            opp.Competitors_Strong_to_Weak__c = tempCompetitorsString;
            opp.Decision_Criteria_Strong_to_Weak__c = tempDecisionString;
            opp.TGS_Differentiation_Strong_to_Weak__c = tempDifferentiationString;
            updateList.add(opp);
        }
        
        if(newTGSMap != NULL)
        {
            for(TGS_Differentiation_Strongest_Weakest__c TGSVar:newTGSMap.values())
            {
                if(TGSDOrderMap.containsKey(TGSVar.Opportunity__c) && TGSDOrderMap.get(TGSVar.Opportunity__c).contains(TGSVar.Strongest_to_Weakest__c))
                {
                    TGSVar.addError('Please rank correctly.');
                }
            }
        }
        if(newDCSMap != NULL)
        {
            for(Decision_Criteria_Strongest_to_Weakest__c DCSVar:newDCSMap.values())
            {
                if(DCSOrderMap.containsKey(DCSVar.Opportunity__c) && DCSOrderMap.get(DCSVar.Opportunity__c).contains(DCSVar.Strongest_to_Weakest__c))
                {
                    DCSVar.addError('Please rank correctly.');
                }
            }
        }
        if(newCSWMap != NULL)
        {
            for(Competitors_Strongest_to_Weakest__c CSWVar:newCSWMap.values())
            {
                if(CSWOrderMap.containsKey(CSWVar.Opportunity__c) && CSWOrderMap.get(CSWVar.Opportunity__c).contains(CSWVar.Strongest_to_Weakest__c))
                {
                    CSWVar.addError('Please rank correctly.');
                }
            }
        }
        
        if(updateList.size() > 0)
        {
            update updateList;
        }
    }
}