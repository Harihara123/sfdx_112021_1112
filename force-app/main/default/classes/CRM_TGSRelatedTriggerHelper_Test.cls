@isTest
public class CRM_TGSRelatedTriggerHelper_Test {
    
    static testMethod void updateOpportunityTest(){
        Account acnt=TestData.newAccount(1);
        insert acnt;
		Opportunity oppr=TestData.newOpportunity(acnt.Id,1);
        insert oppr;
        
        List<String> opList = new List<String>();
        opList.add(oppr.id);
        
        Test.startTest();
        TGS_Differentiation_Strongest_Weakest__c T1 = new TGS_Differentiation_Strongest_Weakest__c( Opportunity__c = oppr.id );
        insert T1 ;
        Decision_Criteria_Strongest_to_Weakest__c D1 = new Decision_Criteria_Strongest_to_Weakest__c(Opportunity__c = oppr.id);
        insert D1;
        Competitors_Strongest_to_Weakest__c C1 = new Competitors_Strongest_to_Weakest__c(Opportunity__c = oppr.id);
        insert C1;
       
        
        Map<Id,TGS_Differentiation_Strongest_Weakest__c> TGSMap = new Map<Id,TGS_Differentiation_Strongest_Weakest__c>();
        TGSMap.put(oppr.id, T1 );
        
        Map<Id,Decision_Criteria_Strongest_to_Weakest__c> DCSMap = new Map<Id,Decision_Criteria_Strongest_to_Weakest__c>();
        DCSMap.put(oppr.id,D1);
        
        Map<Id,Competitors_Strongest_to_Weakest__c> CSWMap = new Map<Id,Competitors_Strongest_to_Weakest__c>();
        CSWMap.put(oppr.id,C1);
        
        CRM_TGSRelatedTriggerHelper.updateOpportunity(opList,TGSMap, DCSMap, CSWMap);
        CRM_TGSRelatedTriggerHelper.updateOpportunityBackup(opList,TGSMap, DCSMap, CSWMap);
        system.assert(oppr!=NULL, 'Opportunity cant be null');
        Test.stopTest();
        
    }

}