/*
    This utility class includes method for - 
    1) describeAvailableQuickAction : Retrieves the quick action information based on API Name.  
               

*/
public without sharing class CRM_UtilityClass{

  public static final String OBJECT_QUERY_ERROR = 'You must include an object with your query.';
    public static final String OBJECT_SELECT_ERROR = 'Your select clause must include at least one field.';
    
    private static final string apiVersion = 'v45.0';
    
    // Coded for returning user preferences method due to errors with inserting user preferences in test classes
    private final static List<UserPreference> upref1 = new List<UserPreference>{new UserPreference(Preference = '57',UserId = UserInfo.getUserId(),Value = '15')};
    private final static List<UserPreference> upref2 = new List<UserPreference>{new UserPreference(Preference = '58',UserId = UserInfo.getUserId(),Value = '120')};
    
    /*
     * @description Retrieves the quick action information based on API Name.  
     *              Global actions are formatted as only the API Name, such as "New_Task" and 
     *              object specific actions are formatted as the SobjectType, a period and then the developer name, such as "Contact.Create_Contact"
     */
    public static string describeAvailableQuickAction(String quickActionApiName){
        List<QuickAction.DescribeQuickActionResult> daqa = QuickAction.describeQuickActions(new List<String>{quickActionApiName});
        return JSON.serialize(daqa);
    }
    
    
    /*
     * @description Retrieves the task and event preferences for a user.
     *    Values are outlined here - https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_userpreference.htm?search_text=userpreference
     */
    public static string retrieveUserTaskPreferences(string preferenceType){
        string obj = 'UserPreference';
        string schemaDescribe = describeSobjects(new List<String>{obj}, true);
        
        Map<string, objectDescribeResult> odr =  (Map<string, objectDescribeResult>) JSON.deserialize(schemaDescribe, Map<string, objectDescribeResult>.class);
        
        Set<String> retrieveSet = new Set<String>{'Id', 'Preference', 'UserId', 'Value'};
        List<String> retrieveList = new List<String>();
        for(string s : retrieveSet){
            if(odr.get(obj).fields.containsKey(s) && odr.get(obj).fields.get(s).accessible){
                retrieveList.add(s);
            }
        }
        string whereClause = 'UserId = \'' + UserInfo.getUserId() + '\'';
        if(preferenceType == 'Task'){
            whereClause += ' AND Preference = \'' + 58 + '\'';
        } else {
            whereClause += ' AND Preference = \'' + 57 + '\'';
        }
        QueryResult qr = queryDatabase(retrieveList, obj, whereClause, null, null, null);
        
        if(Test.isRunningTest()){
            return (preferenceType == 'Task' ? JSON.serialize(upref1) : JSON.serialize(upref2));
        } else{
          return JSON.serialize(qr);  
        }
        
    }
    
    /*
     * @description Sets the labels for initialization of a lookup field
     */
    public static string retrieveThisRecordValues(string obj, string searchValue, string fieldList){
        string schemaDescribe = describeSobjects(new List<String>{obj}, true);
        
        Map<string, objectDescribeResult> odr =  (Map<string, objectDescribeResult>) JSON.deserialize(schemaDescribe, Map<string, objectDescribeResult>.class);
        
        Set<String> retrieveSet = new Set<string>();
        if(fieldList == null){
            retrieveSet = new Set<String>{'Id', 'Name'};
        } else{
            retrieveSet = new Set<String>(fieldList.split(','));
        }
        
        List<String> retrieveList = new List<String>();
        for(string s : retrieveSet){
            if(odr.get(obj).fields.containsKey(s) && odr.get(obj).fields.get(s).accessible){
                retrieveList.add(s);
            }
        }
        
        // Technically the user isn't inputting the search value so SOQL injection chances are minimal.
        // You can never be too safe!
        string whereClause = 'Id = \'' + string.escapeSingleQuotes(searchValue)+ '\'';
        QueryResult qr = queryDatabase(retrieveList, obj, whereClause, null, null, null);
        
        return JSON.serialize(qr);
    }
    
    /*
     * @description Retrieves related records to parent for update mass actions.
     */
    public static string retrieveRelatedRecords(string searchValue, string obj, string relatedField, string fieldList){
        string schemaDescribe = describeSobjects(new List<String>{obj}, true);
        
        Map<string, objectDescribeResult> odr =  (Map<string, objectDescribeResult>) JSON.deserialize(schemaDescribe, Map<string, objectDescribeResult>.class);
        
        Set<String> retrieveSet = new Set<string>();
        if(fieldList == null){
            retrieveSet = new Set<String>{'Id', 'Name'};
        } else{
            retrieveSet = new Set<String>(fieldList.split(','));
        }
        
        List<String> retrieveList = new List<String>();
        for(string s : retrieveSet){
            if(odr.get(obj).fields.containsKey(s) && odr.get(obj).fields.get(s).accessible){
                retrieveList.add(s);
            }
        }
        
        string whereClause = string.escapeSingleQuotes(relatedField) +  ' = \'' + string.escapeSingleQuotes(searchValue)+ '\'';
        QueryResult qr = queryDatabase(retrieveList, obj, whereClause, null, null, null);
        
        return JSON.serialize(qr);
    }
    
     /*
     * @description get the recordtypeId
     */
    
    public static boolean isRecordTypeAvailable(String recordTypeId, string obj){
        Id cleanRecordTypeId = recordTypeId; // Quick Action only uses 15 char instead of 18.  This is to convert to 18.
        
        SObjectType r = ((SObject)(Type.forName('Schema.'+obj).newInstance())).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        if(recordTypeId != null){
            return rtMapById.get(cleanRecordTypeId).isAvailable();  
        } else { // This should never happen - method only called if recordTypeId is specified
            return false;
        }
        
    }
    
    /*
     * @description Retrieves the search layout for the lookup field and caches the result.
     */
    public static string retrieveSearchLayout(string sobjectType){
        
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.qaeRestCache');
        
        if(orgPart.contains('searchLayout' + sobjectType)){
            String cachedSearchLayout = (String) orgPart.get('searchLayout' + sobjectType);
            return cachedSearchLayout;
        } else{
            Http h = new Http();
            HttpRequest req = new HttpRequest();  
            req.setEndpoint('callout:qae_REST_API/' + apiVersion + '/search/layout/?q=' + sobjectType);
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('GET');
            HttpResponse res = h.send(req);
            if(res.getStatus() == 'OK'){
                orgPart.put('searchLayout' + sobjectType, (string)res.getBody());
            }
            return res.getBody();
        }
    }
    
    /*
     * @description Performs a SOSL search.
     */
    
    public static string performSearch(string searchString, string sobjectType, string returningFields, string whereClause, integer limitClause){
        if(limitClause == null){
            limitClause = 2000; // default search length
        }
        
        string schemaDescribe = describeSobjects(new List<String>{sobjectType}, true);   
        Map<string, objectDescribeResult> odr =  (Map<string, objectDescribeResult>) JSON.deserialize(schemaDescribe, Map<string, objectDescribeResult>.class);

        List<String> retrieveList = new List<String>();
       
        for(string s : returningFields.split(',')){
            if(odr.get(sobjectType).fields.containsKey(s) && odr.get(sobjectType).fields.get(s).accessible){
                retrieveList.add(s);
            } else if(s.contains('.') || s.contains('__r')){
                List<string> fields = s.split('\\.');
                string currentField = (fields[0].contains('__r') ? fields[0].replace('__r', '__c') : fields[0] + 'Id');
                fields.remove(0);
                if(odr.get(sobjectType).fields.containsKey(currentField) && odr.get(sobjectType).fields.get(currentField).accessible){
                    
                    boolean hasAccess = getRecursiveFieldAccess(fields, odr.get(sobjectType).fields.get(currentField).referenceTo);
                    if(hasAccess){
                        retrieveList.add(s);
                    }
                }
                
            }
        }
        
        
        List<Sobject> searchList = new List<Sobject>();
        if(whereClause != null){
            if(sobjectType == 'Contact' || sobjectType == 'Account'){
                searchList = search.query('FIND {' + string.escapeSingleQuotes(searchString) + '} RETURNING ' + sobjectType + ' (' + string.join(retrieveList, ',') + ' WHERE ' + whereClause + ') LIMIT ' + limitClause)[0];
            }
            else{
                searchList = search.query('FIND {' + string.escapeSingleQuotes(searchString) + '} RETURNING ' + sobjectType + ' (' + string.join(retrieveList, ',') + ' WHERE ' + string.escapeSingleQuotes(whereClause) + ') LIMIT ' + limitClause)[0];
            }    
        } else{
            searchList = search.query('FIND {' + string.escapeSingleQuotes(searchString) + '} RETURNING ' + sobjectType + ' (' + string.join(retrieveList, ',') + ') LIMIT ' + limitClause)[0];
        }
        
        return JSON.serialize(searchList);
    }
      /*
     * @description Recursively gets access for parent fields
     */
    public static boolean getRecursiveFieldAccess(List<string> s, List<String> referenceTo){
        
        string field = (s[0].contains('__r') ? s[0].replace('__r', '__c') : s[0]);
        SObjectType r = ((SObject)(Type.forName('Schema.'+ referenceTo[0])).newInstance()).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        objectDescribeResult odr = new objectDescribeResult(d, true);
        if(odr.fields.containsKey(field) && odr.fields.get(field).accessible){
            return true;
        } else if(s.size() > 1){
            s.remove(0);
            boolean hasAccess = getRecursiveFieldAccess(s, referenceTo);
            if(hasAccess){
                return true;
            } else{
                return false;
            }
        } else{
            return false;
        }
    }
    public static saveResultList saveThisRecord(string obj, String sobj, string redirectValue, string quickActionType){
        saveResultList sr = new saveResultList();
        if(redirectValue == null || sobj == null){
            sr.errorMsg = 'Values not provided to save.';
            return sr;
        } else {
            string schemaDescribe = describeSobjects(new List<String>{obj}, true);
            
            Map<string, objectDescribeResult> odr =  (Map<string, objectDescribeResult>) JSON.deserialize(schemaDescribe, Map<string, objectDescribeResult>.class);
            Sobject sobjSerialized =  (Sobject) JSON.deserialize(sobj, Sobject.class);
            Map<String, Object> fieldsToValue = sobjSerialized.getPopulatedFieldsAsMap();
            SObjectType dsr = ((SObject)(Type.forName('Schema.'+obj).newInstance())).getSObjectType();
            sObject sObjToCommit = dsr.newSobject();
            List<Sobject> sobjList = new List<Sobject>();
            for(string s : fieldsToValue.keySet()){
                if(odr.get(obj).fields.containsKey(s) && odr.get(obj).fields.get(s).updateable && quickActionType == 'Update'){
                                        sObjToCommit.put(s, fieldsToValue.get(s));
                } else if(s == 'Id'){
                    sObjToCommit.put('Id', fieldsToValue.get('Id'));
                } else if(odr.get(obj).fields.containsKey(s) && odr.get(obj).fields.get(s).createable  && quickActionType != 'Update'){
                                        sObjToCommit.put(s, fieldsToValue.get(s));
                }
            }
            
            sObjList.add(sobjToCommit);
            
            if(quickActionType == 'Update'){
                try{
                    Database.SaveResult[] srRes = database.update(sObjList);
                    sr.saveResult = srRes;
                } catch(exception e){
                    sr.errorMsg = e.getMessage();
                }
                return sr;
            } else {
                try{
                    Database.SaveResult[] srRes = database.insert(sObjList);
                    sr.saveResult = srRes;
                } catch(exception e){
                    sr.errorMsg = e.getMessage();
                }
                return sr;
            }
        }
    }
    // Used in multiple method
      // Shout out to SFDCFox - using super fast schema describe
    // https://salesforce.stackexchange.com/questions/218982/why-is-schema-describesobjectstypes-slower-than-schema-getglobaldescribe/219010#219010
    public static string describeSobjects(List<String> objList, boolean includeFields){
        Map<String, objectDescribeResult> dsr = new Map<String, objectDescribeResult>();
        for (String obj : objList) {
            if(obj != 'Calendar'){ // Assigned To Id on events are related to calendars which is not a valid sobject type
                SObjectType r = ((SObject)(Type.forName('Schema.'+obj).newInstance())).getSObjectType();
                DescribeSObjectResult d = r.getDescribe();
                objectDescribeResult odr = new objectDescribeResult(d, includeFields);
                dsr.put(odr.name, odr);
            }
        }
        return JSON.serialize(dsr);
    }
      /*
     * @description Queries the database for a record and returns a QueryResult.  This includes errors or successes.
     */
    public static queryResult queryDatabase(List<String> selectClause, String obj, string whereClause, string groupBy, string orderBy, integer limitClause){
        queryResult qr = new queryResult();
        if(selectClause != null && selectClause.size() > 0){
            if(obj != null){
               
                string selectClauseClean = string.join(selectClause,',');
                string query = 'SELECT ' + string.escapeSingleQuotes(selectClauseClean) + ' FROM ' + obj;
                if(whereClause != null){
                    query += ' WHERE ' + whereClause;
                }
                if(groupBy != null){
                    query += ' GROUP BY ' +  string.escapeSingleQuotes(groupBy);
                }
                if(orderBy != null){
                    query += ' ORDER BY ' + string.escapeSingleQuotes(orderBy);
                }
                if(limitClause != null){
                    query += ' LIMIT ' + limitClause;
                }
                
                try{
                    List<sObject> result = database.query(query);
                    qr.result = result;
                } catch(exception e){
                    qr.error = e.getMessage();
                }

            } else{
                qr.error = OBJECT_QUERY_ERROR;
            }
        } else{
            qr.error = OBJECT_SELECT_ERROR;
        }
        return qr;
    }
    
    //Wrapper classes
     // Have to make a wrapper for describes or a javalang error will occur:
    // System.JSONException: (was java.lang.NullPointerException) (through reference chain: common.api.soap.wsdl.DescribeSObjectResult["listviewable"])
    public class objectDescribeResult{
        public string name {get;set;}
        public string keyPrefix {get;set;}
        public string label {get;set;}
        public Map<String, field> fields {get;set;}
        
 
        public objectDescribeResult(DescribeSObjectResult d, boolean includeFields) {
            name = d.getName();
            keyPrefix = d.getKeyPrefix();
            label = d.getLabel();
            
            if(includeFields){
                // Describe fields
                fields = new Map<String, field>();
                Map<String, Schema.SobjectField> dfrMap = d.fields.getMap();
                for(String fr : dfrMap.keyset()){
                    field f = new field(dfrMap.get(fr).getDescribe());
                    fields.put(f.name, f);
                }
            }
        }
    }
     public class field{
        public string name {get;set;}
        public string label {get;set;}
        public boolean accessible{get;set;}
        public boolean createable{get;set;}
        public boolean updateable{get;set;}
        public string fieldType{get;set;}
        public List<String> referenceTo{get;set;}

        public field(Schema.DescribeFieldResult field){
            this.name = field.getName();
            this.label = field.getLabel();
            this.fieldType = String.valueOf(field.getType()); 
            this.accessible = field.isAccessible();
            this.createable = field.isCreateable();
            this.updateable = field.isUpdateable();
            this.referenceTo = new List<String>();
            for(Schema.SobjectType s : field.getReferenceTo()){
                referenceTo.add(s.getDescribe().getName());
            }
        }
    } 
    
    public class queryResult{
        public String error {get;set;}
        public List<Sobject> result {get;set;}
    }
                         
    public class searchResult{
        public string errorMsg {get;set;}
        public string label {get;set;}
        public string limitRows {get;set;}
        public string objectType {get;set;}
        public List<searchColumns> searchColumns {get;set;}
   }
                 
   public class saveResultList{
       public string errorMsg {get;set;}
       public List<Database.SaveResult> saveResult {get;set;}
   }
    
   public class searchColumns{
       public string field {get;set;}
       public string format {get;set;}
       public string label {get;set;}
       public string name {get;set;}
    }    
}