/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* CSC_AdjustmentsController class is used for payroll form.
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ismail Shaik
* @modifiedBy     
* @maintainedBy   
* @version        1.0
* @created        
* @modified       

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class CSC_AdjustmentsController {
    @AuraEnabled
    public static boolean isAdjustmentCheck(String caseRecId){
        list<String> adjTypes = System.label.CSC_Adjustment_Types.split(',');
        boolean isAdjustment = false;
        list<Case> caseList = [select 
                               id,
                               Type,
                               Sub_type__c 
                               from case 
                               where id =: caseRecId
                               AND Sub_type__c IN: adjTypes
                              ];
        if(!caseList.isEmpty()){
            isAdjustment = true;
        }
        return isAdjustment;
    }
    
    @AuraEnabled
    public static boolean isCaseClosed(String caseRecId){
        boolean isClosed = false;
        list<Case> caseList = [select 
                               id,
                               Status 
                               from case 
                               where id =: caseRecId
                               AND Status = 'Closed'
                              ];
        if(!caseList.isEmpty()){
            isClosed = true;
        }
        return isClosed;
    }
    
    @AuraEnabled
    public static boolean isCSCTalentOffice(String caseRecId){
        list<String> cscOffices = System.label.CSC_Payroll_Talent_Office.split(',');
        boolean isCSCTalentOffice = false;
        list<Case> caseList = [select 
                               id,
                               Status,
                               Talent_Office__c,
                               Talent_Office__r.Talent_s_Office_Code__c
                               from case 
                               where id =: caseRecId
                               AND Talent_Office__c != NULL
                               AND Talent_Office__r.Talent_s_Office_Code__c IN: cscOffices
                              ];
        if(!caseList.isEmpty()){
            isCSCTalentOffice = true;
        }
        return isCSCTalentOffice;
    }
    
    @AuraEnabled
    public static boolean isParentORChildCase(String caseRecId){
        boolean isParentORChild = false;
        list<Case> caseList = [select 
                               id,
                               parentId,
                               Client_Account_Name__c,
                               Status 
                               from case 
                               where id =: caseRecId
                               AND (parentId != null
                               OR Client_Account_Name__c != null)
                              ];
        if(!caseList.isEmpty()){
            isParentORChild = true;
        }
        return isParentORChild;
    }
    
    @AuraEnabled
    public static Case isPayrollORFS(String caseRecId){
        list<Case> caseList = [select 
                               id,
                               Type,
                               Sub_type__c,
                               OPCO_Talent__c,
                               Talent_Office__r.Talent_s_OPCO__c,
                               Talent_Office__r.Talent_s_Office_Code__c
                               from case 
                               where id =: caseRecId
                              ];
        return caseList[0];
    }
    
    @AuraEnabled
    public static list<CSC_Payroll_Adjustments__c> getAdjustments(String caseRecId, String args){
        system.debug('caseRecId======>>>>>>>>>'+caseRecId);
        // Case__c
        if(args == 'hasUTA'){
            return [select id,CSC_Adjustment_Code__c,CSC_Earn_Code__c,CSC_Earnings_Code__c,Correct_Hours__c,Original_Hours__c,Hours_Difference__c,Weekday__c,Weekending__c,Original_Gross__c,Correct_Gross__c,Gross_Difference__c,Travel_Adv_Pd__c,Receipts_Submtd__c from CSC_Payroll_Adjustments__c where Case__c =: caseRecId ORDER BY CreatedDate ASC];
        }
        return [select id,CSC_Adjustment_Code__c,CSC_Earn_Code__c,CSC_Earnings_Code__c,Correct_Bill_Rate__c,Correct_Hours__c,Original_Bill_Rate__c,Original_Hours__c,Original_Pay_Rate__c,Correct_Pay_rate__c,Hours_Difference__c,Weekday__c,Weekending__c,Pay_Rate_Difference__c,Bill_Rate_Difference__c,Original_Gross__c,Correct_Gross__c,Gross_Difference__c,Job_Req_ID__c from CSC_Payroll_Adjustments__c where Case__c =: caseRecId ORDER BY CreatedDate ASC];
    }
    
    @AuraEnabled
    public static list<CSC_Payroll_Adjustments__c> deleteAdjustments(String adjRecId){
        system.debug('adjRecId======>>>>>>>>>'+adjRecId);
        string caseRecId;
        list<CSC_Payroll_Adjustments__c> adjList = [select id,case__c from CSC_Payroll_Adjustments__c where id=: adjRecId];
        caseRecId = adjList[0].case__c;
        delete adjList;
        return [select id,CSC_Adjustment_Code__c,CSC_Earn_Code__c,CSC_Earnings_Code__c,Correct_Bill_Rate__c,Correct_Hours__c,Original_Bill_Rate__c,Original_Hours__c,Original_Pay_Rate__c,Correct_Pay_rate__c,Hours_Difference__c,Weekday__c,Weekending__c,Pay_Rate_Difference__c,Bill_Rate_Difference__c,Original_Gross__c,Correct_Gross__c,Gross_Difference__c,Job_Req_ID__c,Travel_Adv_Pd__c,Receipts_Submtd__c from CSC_Payroll_Adjustments__c where Case__c =: caseRecId ORDER BY CreatedDate ASC];
    }
    
    @AuraEnabled
    public static list<CSC_Payroll_Adjustments__c> copyAdjustments(String adjRecId){
        string caseRecId;
        CSC_Payroll_Adjustments__c adjRec = [select id,case__c,CSC_Adjustment_Code__c,CSC_Earn_Code__c,CSC_Earnings_Code__c,Correct_Bill_Rate__c,Correct_Hours__c,Original_Bill_Rate__c,Original_Hours__c,Original_Pay_Rate__c,Correct_Pay_rate__c,Hours_Difference__c,Weekday__c,Weekending__c,Pay_Rate_Difference__c,Bill_Rate_Difference__c,Original_Gross__c,Correct_Gross__c,Gross_Difference__c,Job_Req_ID__c,Travel_Adv_Pd__c,Receipts_Submtd__c from CSC_Payroll_Adjustments__c where id =: adjRecId];
        caseRecId = adjRec.case__c;
        CSC_Payroll_Adjustments__c adjClone = adjRec.clone(false, false, false, false);
        insert adjClone;
        return [select id,CSC_Adjustment_Code__c,CSC_Earn_Code__c,CSC_Earnings_Code__c,Correct_Bill_Rate__c,Correct_Hours__c,Original_Bill_Rate__c,Original_Hours__c,Original_Pay_Rate__c,Correct_Pay_rate__c,Hours_Difference__c,Weekday__c,Weekending__c,Pay_Rate_Difference__c,Bill_Rate_Difference__c,Original_Gross__c,Correct_Gross__c,Gross_Difference__c,Job_Req_ID__c,Travel_Adv_Pd__c,Receipts_Submtd__c from CSC_Payroll_Adjustments__c where Case__c =: caseRecId ORDER BY CreatedDate ASC];
	}
    
    @AuraEnabled
    public static list<CSC_Payroll_Adjustments__c> addAdjustments(String inputJson){
        system.debug('inputJson=====>>>>>>>>>'+inputJson);
        Id finalCaseId;
        AdjustmentWrapper adjWrap = (AdjustmentWrapper) JSON.deserialize(inputJson, AdjustmentWrapper.class);
        system.debug('adjWrap=======>>>>>>>>'+adjWrap);
        if(adjWrap != null){
            finalCaseId = adjWrap.CaseId;
            list<CSC_Payroll_Adjustments__c> adjList = new list<CSC_Payroll_Adjustments__c>();
            for(Adjustments adj : adjWrap.adjustments){
                CSC_Payroll_Adjustments__c adjObj = new CSC_Payroll_Adjustments__c();
                if(adj.Record_ID != null && adj.Record_ID != ''){
                    adjObj.Id = adj.Record_ID;
                }else{
                    adjObj.Case__c = adjWrap.CaseId;
                }
                if(adj.Adjustment_Code != null && adj.Adjustment_Code != '')
                	adjObj.CSC_Adjustment_Code__c = adj.Adjustment_Code;
                if(adj.Earn_Code != null && adj.Earn_Code != '')
                	adjObj.CSC_Earn_Code__c = adj.Earn_Code;
                if(adj.Earnings_Code != null && adj.Earnings_Code != '')
                	adjObj.CSC_Earnings_Code__c = adj.Earnings_Code;
                if(adj.ORGNL_HRS != null && adj.ORGNL_HRS != '')
                	adjObj.Original_Hours__c = Decimal.valueOf(adj.ORGNL_HRS);
                if(adj.Correct_HRS != null && adj.Correct_HRS != '')
                	adjObj.Correct_Hours__c = Decimal.valueOf(adj.Correct_HRS);
                if(adj.ORGNL_Bill_RT != null && adj.ORGNL_Bill_RT != '')
                	adjObj.Original_Bill_Rate__c = Decimal.valueOf(adj.ORGNL_Bill_RT);
                if(adj.Correct_Pay_RT != null && adj.Correct_Pay_RT != '')
                	adjObj.Correct_Pay_rate__c = Decimal.valueOf(adj.Correct_Pay_RT);
                if(adj.Correct_Bill_RT != null && adj.Correct_Bill_RT != '')
                	adjObj.Correct_Bill_Rate__c = Decimal.valueOf(adj.Correct_Bill_RT);
                if(adj.ORGNL_Pay_RT != null && adj.ORGNL_Pay_RT != '')
                	adjObj.Original_Pay_Rate__c = Decimal.valueOf(adj.ORGNL_Pay_RT);
                if(adj.Job_Req_Id != null && adj.Job_Req_Id != '')
                	adjObj.Job_Req_ID__c = adj.Job_Req_Id;
                if(adj.Travel_Adv_Pd != null && adj.Travel_Adv_Pd != '')
                	adjObj.Travel_Adv_Pd__c = Decimal.valueOf(adj.Travel_Adv_Pd);
                if(adj.Receipts_Submtd != null && adj.Receipts_Submtd != '')
                	adjObj.Receipts_Submtd__c = Decimal.valueOf(adj.Receipts_Submtd);
                if(adj.Weekending != null){
                	adjObj.Weekending__c = Date.valueOf(adj.Weekending);
                }
                if(adj.Weekday != null){
                    adjObj.Weekday__c = Date.valueOf(adj.Weekday);
                }else if(adj.Weekday == null){
                    adjObj.Weekday__c = null;
                }
                
                adjList.add(adjObj);
            }
            if(!adjList.isEmpty()){
                upsert adjList;
            }
        }
        return [select id,CSC_Adjustment_Code__c,CSC_Earn_Code__c,CSC_Earnings_Code__c,Correct_Bill_Rate__c,Correct_Hours__c,Original_Bill_Rate__c,Original_Hours__c,Original_Pay_Rate__c,Correct_Pay_rate__c,Hours_Difference__c,Weekday__c,Weekending__c,Pay_Rate_Difference__c,Bill_Rate_Difference__c,Original_Gross__c,Correct_Gross__c,Gross_Difference__c,Job_Req_ID__c,Travel_Adv_Pd__c,Receipts_Submtd__c from CSC_Payroll_Adjustments__c where Case__c =: finalCaseId ORDER BY CreatedDate ASC];
    }
    
    @AuraEnabled
    public static void saveBackupTypes(String inputBackup ){
        system.debug('backupTypes=====>>>>>>>>>'+inputBackup);
        Case saveCase = new Case();
        BackupWrapper inputWrp = (BackupWrapper) JSON.deserialize(inputBackup, BackupWrapper.class);
        if(inputWrp.caseRecID != ''){
          saveCase.ID = inputWrp.caseRecId;
            if(inputWrp.backupTypes != null){
        		for(Backup bkup : inputWrp.backupTypes){
                    switch on  bkup.backupType{
                        when 'Time_Corrections__c' {
                            saveCase.Time_Corrections__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Late_Timecards__c' {
                            saveCase.Late_Timecards__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Contractor_Did_Not_Work__c' {
                            saveCase.Contractor_Did_Not_Work__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Expenses__c' {
                            saveCase.Expenses__c = boolean.valueof(bkup.backupState);
                        }
                        when 'CSC_Billable_Vacation__c' {
                            saveCase.CSC_Billable_Vacation__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Non_Billable_Vacation__c' {
                            saveCase.Non_Billable_Vacation__c = boolean.valueof(bkup.backupState);
                        }
                        when 'CSC_Billable_Per_Diem__c' {
                            saveCase.CSC_Billable_Per_Diem__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Non_Billable_Per_Diem__c' {
                            saveCase.Non_Billable_Per_Diem__c = boolean.valueof(bkup.backupState);
                        }
                        when 'CSC_Billable_Holiday__c' {
                            saveCase.CSC_Billable_Holiday__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Non_Billable_Holiday__c' {
                            saveCase.Non_Billable_Holiday__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Rate_Subvendor_Adjustments__c' {
                            saveCase.Rate_Subvendor_Adjustments__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Time_Subvendor_Adjustments__c' {
                            saveCase.Time_Subvendor_Adjustments__c = boolean.valueof(bkup.backupState);
                        }
                        when 'On_Premise_Adjustments__c' {
                            saveCase.On_Premise_Adjustments__c = boolean.valueof(bkup.backupState);
                        }
                        when 'CSC_Billable_Drug_Background__c' {
                            saveCase.CSC_Billable_Drug_Background__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Non_Billable_Drug_Background__c' {
                            saveCase.Non_Billable_Drug_Background__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Incorrect_Requisition__c' {
                            saveCase.Incorrect_Requisition__c = boolean.valueof(bkup.backupState);
                        }
                        when 'CSC_Billable_Sick_Pay__c' {
                            saveCase.CSC_Billable_Sick_Pay__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Non_Billable_Sick_Pay__c' {
                            saveCase.Non_Billable_Sick_Pay__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Non_Billable_Meal_Break_Penalty__c' {
                            saveCase.Non_Billable_Meal_Break_Penalty__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Bill_Rate_Adjustments__c' {
                            saveCase.Bill_Rate_Adjustments__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Pay_Rate_Adjustments__c' {
                            saveCase.Pay_Rate_Adjustments__c = boolean.valueof(bkup.backupState);
                        }
                        when 'Compliance_Conducted_Audits__c' {
                            saveCase.Compliance_Conducted_Audits__c = boolean.valueof(bkup.backupState);
                        }
                    }
        		}
                try{
                update saveCase;
                }catch(Exception e){
                    System.debug('e=====>>>>>'+e);
                }
            }
        }
    }
    
    @AuraEnabled
    public static string[] returnBackupTypes(String caseRecId){
        List<String> inputList = new List<String>();
        case obj = [select id,Is_this_a_Rate_or_Time_adjustment__c,Time_Corrections__c,Late_Timecards__c,Contractor_Did_Not_Work__c,Expenses__c,CSC_Billable_Vacation__c,Non_Billable_Vacation__c,CSC_Billable_Per_Diem__c,Non_Billable_Per_Diem__c,CSC_Billable_Holiday__c,Non_Billable_Holiday__c,Rate_Subvendor_Adjustments__c,Time_Subvendor_Adjustments__c,On_Premise_Adjustments__c,CSC_Billable_Drug_Background__c,Non_Billable_Drug_Background__c,Incorrect_Requisition__c,CSC_Billable_Sick_Pay__c,Non_Billable_Sick_Pay__c,Non_Billable_Meal_Break_Penalty__c,Bill_Rate_Adjustments__c,Pay_Rate_Adjustments__c,Compliance_Conducted_Audits__c from case where id =: caseRecId];
        String backupTypes = '';
        backupTypes += '{';
        backupTypes += '"Adjustment_Type":"'+obj.Is_this_a_Rate_or_Time_adjustment__c+'"';
        if(obj.Time_Corrections__c == true)
            backupTypes += ',"Time_Corrections__c":"'+obj.Time_Corrections__c+'"';
        if(obj.Late_Timecards__c == true)
            backupTypes += ',"Late_Timecards__c":"'+obj.Late_Timecards__c+'"';
        if(obj.Contractor_Did_Not_Work__c == true)
            backupTypes += ',"Contractor_Did_Not_Work__c":"'+obj.Contractor_Did_Not_Work__c+'"';
        if(obj.Expenses__c == true)
            backupTypes += ',"Expenses__c":"'+obj.Expenses__c+'"';
        if(obj.CSC_Billable_Vacation__c == true)
            backupTypes += ',"CSC_Billable_Vacation__c":"'+obj.CSC_Billable_Vacation__c+'"';
        if(obj.Non_Billable_Vacation__c == true)
            backupTypes += ',"Non_Billable_Vacation__c":"'+obj.Non_Billable_Vacation__c+'"';
        if(obj.CSC_Billable_Per_Diem__c == true)
            backupTypes += ',"CSC_Billable_Per_Diem__c":"'+obj.CSC_Billable_Per_Diem__c+'"';
        if(obj.Non_Billable_Per_Diem__c == true)
            backupTypes += ',"Non_Billable_Per_Diem__c":"'+obj.Non_Billable_Per_Diem__c+'"';
        if(obj.CSC_Billable_Holiday__c == true)
            backupTypes += ',"CSC_Billable_Holiday__c":"'+obj.CSC_Billable_Holiday__c+'"';
        if(obj.Non_Billable_Holiday__c == true)
            backupTypes += ',"Non_Billable_Holiday__c":"'+obj.Non_Billable_Holiday__c+'"';
        if(obj.Rate_Subvendor_Adjustments__c == true)
            backupTypes += ',"Rate_Subvendor_Adjustments__c":"'+obj.Rate_Subvendor_Adjustments__c+'"';
        if(obj.Time_Subvendor_Adjustments__c == true)
            backupTypes += ',"Time_Subvendor_Adjustments__c":"'+obj.Time_Subvendor_Adjustments__c+'"';
        if(obj.On_Premise_Adjustments__c == true)
            backupTypes += ',"On_Premise_Adjustments__c":"'+obj.On_Premise_Adjustments__c+'"';
        if(obj.CSC_Billable_Drug_Background__c == true)
            backupTypes += ',"CSC_Billable_Drug_Background__c":"'+obj.CSC_Billable_Drug_Background__c+'"';
        if(obj.Non_Billable_Drug_Background__c == true)
            backupTypes += ',"Non_Billable_Drug_Background__c":"'+obj.Non_Billable_Drug_Background__c+'"';
        if(obj.Incorrect_Requisition__c == true)
            backupTypes += ',"Incorrect_Requisition__c":"'+obj.Incorrect_Requisition__c+'"';
        if(obj.CSC_Billable_Sick_Pay__c == true)
            backupTypes += ',"CSC_Billable_Sick_Pay__c":"'+obj.CSC_Billable_Sick_Pay__c+'"';
        if(obj.Non_Billable_Sick_Pay__c == true)
            backupTypes += ',"Non_Billable_Sick_Pay__c":"'+obj.Non_Billable_Sick_Pay__c+'"';
        if(obj.Non_Billable_Meal_Break_Penalty__c == true)
            backupTypes += ',"Non_Billable_Meal_Break_Penalty__c":"'+obj.Non_Billable_Meal_Break_Penalty__c+'"';
        if(obj.Bill_Rate_Adjustments__c == true)
            backupTypes += ',"Bill_Rate_Adjustments__c":"'+obj.Bill_Rate_Adjustments__c+'"';
        if(obj.Pay_Rate_Adjustments__c == true)
            backupTypes += ',"Pay_Rate_Adjustments__c":"'+obj.Pay_Rate_Adjustments__c+'"';
        if(obj.Compliance_Conducted_Audits__c == true)
            backupTypes += ',"Compliance_Conducted_Audits__c":"'+obj.Compliance_Conducted_Audits__c+'"';
        backupTypes += '}';
        //backupTypes = '{"Adjustment_Type": "'+obj.Adjustment_Type__c+'","Time_Corrections__c": "'+obj.Time_Corrections__c+'","Late_Timecards__c": "'+obj.Late_Timecards__c+'","Contractor_Did_Not_Work__c": "'+obj.Contractor_Did_Not_Work__c+'","Expenses__c": "'+obj.Expenses__c+'","Billable_Vacation__c": "'+obj.Billable_Vacation__c+'","Non_Billable_Vacation__c": "'+obj.Non_Billable_Vacation__c+'","Billable_Per_Diem__c": "'+obj.Billable_Per_Diem__c+'","Non_Billable_Per_Diem__c": "'+obj.Non_Billable_Per_Diem__c+'","Billable_Holiday__c": "'+obj.Billable_Holiday__c+'","Non_Billable_Holiday__c": "'+obj.Non_Billable_Holiday__c+'","Subvendor_Adjustments__c": "'+obj.Subvendor_Adjustments__c+'","On_Premise_Adjustments__c": "'+obj.On_Premise_Adjustments__c+'","Billable_Drug_Background__c": "'+obj.Billable_Drug_Background__c+'","Non_Billable_Drug_Background__c": "'+obj.Non_Billable_Drug_Background__c+'","Incorrect_Requisition__c": "'+obj.Incorrect_Requisition__c+'","Billable_Sick_Pay__c": "'+obj.Billable_Sick_Pay__c+'","Non_Billable_Sick_Pay__c": "'+obj.Non_Billable_Sick_Pay__c+'","Bill_Rate_Adjustments__c": "'+obj.Bill_Rate_Adjustments__c+'","Pay_Rate_Adjustments__c": "'+obj.Pay_Rate_Adjustments__c+'","Compliance_Conducted_Audits__c": "'+obj.Compliance_Conducted_Audits__c+'"}';
        system.debug('backupTypes====>>>>>'+backupTypes);
        inputList.add(backupTypes);
        return inputList;
    }
	
    @AuraEnabled
    public static String [] calculateDifference(String userInput){
        System.debug('calculateDifference===>'+ userInput);
        try{
            Decimal hoursDiff = 0;
            Decimal payRateDiff = 0;
            Decimal billRateDiff = 0;
            Decimal correctGross = 0;
            Decimal originalGross = 0;
            Decimal grossDiff = 0;
       		userInputWrapper inputWrap = (userInputWrapper) JSON.deserialize(userInput, userInputWrapper.class);
        	List<String> inputList = new List<String>();
        	for(CSC_Payroll_Adjustments__c usrInput : inputWrap.fieldUpdate){
                
                if(usrInput.Correct_Hours__c!=null){
                    if(usrInput.Original_Hours__c!=null){
                        hoursDiff = usrInput.Correct_Hours__c - usrInput.Original_Hours__c;
                    }else{
                        hoursDiff = usrInput.Correct_Hours__c;
                        usrInput.Original_Hours__c = 0;
                    }
                }
                else if(usrInput.Original_Hours__c!=null){
                    hoursDiff = 0 - usrInput.Original_Hours__c;
                    usrInput.Correct_Hours__c = 0;
                }
                else{
                    hoursDiff=0;
                }
                if(usrInput.Correct_Pay_Rate__c!=null){
                    if(usrInput.Original_Pay_Rate__c!=null){
                        payRateDiff = usrInput.Correct_Pay_Rate__c - usrInput.Original_Pay_Rate__c;
                    }else{
                        payRateDiff = usrInput.Correct_Pay_Rate__c;
                        usrInput.Original_Pay_Rate__c = 0;
                    }
                }
                else if(usrInput.Original_Pay_Rate__c!=null){
                    payRateDiff = 0 - usrInput.Original_Pay_Rate__c;
                    usrInput.Correct_Pay_Rate__c = 0;
                }
                else{
                    payRateDiff=0;
                }
                
                if(usrInput.Correct_Bill_Rate__c!=null){
                    if(usrInput.Original_Bill_Rate__c!=null){
                        billRateDiff = usrInput.Correct_Bill_Rate__c - usrInput.Original_Bill_Rate__c;
                    }else{
                        billRateDiff = usrInput.Correct_Bill_Rate__c;
                        usrInput.Original_Bill_Rate__c = 0;
                    }
                }
                else if(usrInput.Original_Bill_Rate__c!=null){
                    billRateDiff = 0 - usrInput.Original_Bill_Rate__c;
                    usrInput.Correct_Bill_Rate__c = 0;
                }
                else{
                    billRateDiff = 0;
                }
                if(usrInput.Correct_Hours__c!=null && usrInput.Correct_Pay_Rate__c!=null){
                    if(usrInput.Original_Hours__c!=null && usrInput.Original_Pay_Rate__c!=null){
                        grossDiff = (usrInput.Correct_Hours__c * usrInput.Correct_Pay_Rate__c) - (usrInput.Original_Hours__c * usrInput.Original_Pay_Rate__c);
                        correctGross = usrInput.Correct_Hours__c * usrInput.Correct_Pay_Rate__c;
                        originalGross = usrInput.Original_Hours__c * usrInput.Original_Pay_Rate__c;
                    }else{
                        grossDiff = (usrInput.Correct_Hours__c * usrInput.Correct_Pay_Rate__c);
                    	correctGross = usrInput.Correct_Hours__c * usrInput.Correct_Pay_Rate__c;
                    	originalGross = 0;
                    }
                }
                else if(usrInput.Original_Hours__c!=null && usrInput.Original_Pay_Rate__c!=null){
                    grossDiff = usrInput.Original_Hours__c * usrInput.Original_Pay_Rate__c;
                    originalGross = usrInput.Original_Hours__c * usrInput.Original_Pay_Rate__c;
                    correctGross = 0;
                }
                else{
                    grossDiff =0;
                     originalGross=0; 
                      correctGross =0;
                  }
                inputList.add('{"Id":"'+usrInput.Id+'","Correct_Bill_Rate__c":'+usrInput.Correct_Bill_Rate__c+',"Correct_Hours__c": '+usrInput.Correct_Hours__c+',"Original_Bill_Rate__c":'+usrInput.Original_Bill_Rate__c+',"Original_Hours__c":'+usrInput.Original_Hours__c+',"Original_Pay_Rate__c":'+usrInput.Original_Pay_Rate__c+',"Correct_Pay_rate__c":'+usrInput.Correct_Pay_rate__c+',"Hours_Difference__c":'+hoursDiff+',"Pay_Rate_Difference__c":'+payRateDiff+',"Bill_Rate_Difference__c":'+billRateDiff+',"Original_Gross__c":'+originalGross+',"Correct_Gross__c":'+correctGross+',"Gross_Difference__c":'+grossDiff+',"Adjustment_Code__c":"'+usrInput.Adjustment_Code__c+'","Earn_Code__c":"'+usrInput.Earn_Code__c+'"}');
                }
            	System.debug('inputList======>>>>>>>'+inputList);
                return(inputList);
        } catch(Exception error) {
            System.debug('calculateDifference===>' + error.getMessage());
        }
        return(null);
    }
    
    public class AdjustmentWrapper {
        public String CaseId;
        public List<Adjustments> Adjustments;
    }
    public class Adjustments {
        public String Record_ID;
        public String ORGNL_HRS;
        public String Correct_HRS;
        public String ORGNL_Bill_RT;
        public String Correct_Pay_RT;
        public String ORGNL_Pay_RT;
        public String Correct_Bill_RT;
        public String Weekending;
        public String Weekday;
        public String Adjustment_Code;
        public String Earn_Code;
        public String Earnings_Code;
        public String Job_Req_Id;
        public String Travel_Adv_Pd;
        public String Receipts_Submtd;
    }
    public class Backup{
        public String backupType;
        public String backupState;
    }
    public class BackupWrapper{
        public String caseRecID;
        public List<Backup> backupTypes;
    }
    public class userInputWrapper{
        public List<CSC_Payroll_Adjustments__c> fieldUpdate;
    }
}