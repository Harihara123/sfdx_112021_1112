@isTest
public class CSC_AdjustmentsControllerTest {
    static testmethod void testAdjustment() {
        
    	CSC_Holidays__c setting = new CSC_Holidays__c();
        setting.Name = 'hol1';
        setting.Date__c = system.today();
        insert setting;
    	
    	CSC_General_Setup__c setting2 = new CSC_General_Setup__c();
        setting2.Name = 'Lock Status';
        setting2.Lock_Status_over_Weekend_and_Holidays__c = false;
        insert setting2;
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Contact testContactWOUSer = new Contact(Title='Java Applications Developer', LastName='manish2', accountId=acc.id);
            insert testContactWOUSer;
            
            Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact.Id,
                    timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
           
            insert Talentusr;
                
            Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
            insert testContact222;
            
            //Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact222.Id,Region__c = 'TEK Central',OPCO__c = 'ONS',
                    timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
           
            insert Talentusr222;
            
            Test.startTest();
            // Region Alignment Mapping 
            CSC_Region_Alignment_Mapping__c rMap = new CSC_Region_Alignment_Mapping__c();
            rMap.Talent_s_Office_Code__c = '00473';
            rMap.Center_Name__c = 'Jacksonville';
            rMap.Talent_s_OPCO__c = 'ONS';
            rMap.Location_Description__c = 'Austin';
            insert rMap;
        
            // Inserting a new CSC case record
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Billing/Invoices', 
                                     Sub_Type__c='Bonus Bill Rate Discrepancy',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     Contact=testContact,
                                     Adjustment_Type__c = 'Oncycle Adjustment',
                                     CSC_Client_PS_ID__c = '12345', Talent_Office__c = rMap.Id,
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase; 
            list<CSC_Payroll_Adjustments__c> adjList = new list<CSC_Payroll_Adjustments__c>();
            CSC_Payroll_Adjustments__c adjustment = new CSC_Payroll_Adjustments__c(Case__c = testCase.Id, Job_Req_ID__c = '1234',
                                                                                  Correct_Hours__c = 10, Original_Hours__c = 20,
                                                                                  Correct_Pay_rate__c = 100.00, Original_Pay_Rate__c = 200.00,
                                                                                  Correct_Bill_Rate__c = 300.00, Original_Bill_Rate__c = 400.00,
                                                                                  Weekday__c = System.today(), Weekending__c = System.today());
            adjList.add(adjustment);
            CSC_Payroll_Adjustments__c adjustmentDel = new CSC_Payroll_Adjustments__c(Case__c = testCase.Id, Job_Req_ID__c = '1234',
                                                                                  Correct_Hours__c = 10, Original_Hours__c = 20,
                                                                                  Correct_Pay_rate__c = 100.00, Original_Pay_Rate__c = 200.00,
                                                                                  Correct_Bill_Rate__c = 300.00, Original_Bill_Rate__c = 400.00,
                                                                                  Weekday__c = System.today(), Weekending__c = System.today());
            adjList.add(adjustmentDel);
            insert adjList;
            
            CSC_AdjustmentsController.isAdjustmentCheck(testCase.Id);
            CSC_AdjustmentsController.isCaseClosed(testCase.Id);
            CSC_AdjustmentsController.isCSCTalentOffice(testCase.Id);
            CSC_AdjustmentsController.isParentORChildCase(testCase.Id);
            CSC_AdjustmentsController.getAdjustments(testCase.Id,'hasUTA');
            CSC_AdjustmentsController.getAdjustments(testCase.Id,'UTA');
            CSC_AdjustmentsController.isParentORChildCase(testCase.Id);
            CSC_AdjustmentsController.deleteAdjustments(adjList[1].Id);
            CSC_AdjustmentsController.addAdjustments('{"CaseId":"'+testCase.Id+'","adjustments":[{"Record_ID":null,"ORGNL_HRS":"30","Job_Req_Id":"1122","Travel_Adv_Pd":"30","Receipts_Submtd":"30","ORGNL_Pay_RT":"100.00","Correct_HRS":"40","ORGNL_Bill_RT":"200.00","Correct_Pay_RT":"300.00","Correct_Bill_RT":"400.00","Weekending":"2021-07-05T10:54:41.929Z","Weekday":"2021-07-05T10:54:41.929Z","Adjustment_Code":null,"Earn_Code":null}]}');
            CSC_AdjustmentsController.saveBackupTypes('{"caseRecId":"'+testCase.Id+'","backupTypes":[{"backupType":"Time_Corrections__c","backupState":true},{"backupType":"Late_Timecards__c","backupState":true},{"backupType":"Contractor_Did_Not_Work__c","backupState":true},{"backupType":"Expenses__c","backupState":true},{"backupType":"CSC_Billable_Vacation__c","backupState":true},{"backupType":"Non_Billable_Vacation__c","backupState":true},{"backupType":"CSC_Billable_Per_Diem__c","backupState":true},{"backupType":"Non_Billable_Per_Diem__c","backupState":true},{"backupType":"CSC_Billable_Holiday__c","backupState":true},{"backupType":"Non_Billable_Holiday__c","backupState":true},{"backupType":"Rate_Subvendor_Adjustments__c","backupState":true},{"backupType":"Time_Subvendor_Adjustments__c","backupState":true},{"backupType":"On_Premise_Adjustments__c","backupState":true},{"backupType":"CSC_Billable_Drug_Background__c","backupState":true},{"backupType":"Non_Billable_Sick_Pay__c","backupState":true},{"backupType":"Bill_Rate_Adjustments__c","backupState":true},{"backupType":"Pay_Rate_Adjustments__c","backupState":true},{"backupType":"Compliance_Conducted_Audits__c","backupState":true},{"backupType":"CSC_Billable_Sick_Pay__c","backupState":true},{"backupType":"Incorrect_Requisition__c","backupState":true},{"backupType":"Non_Billable_Drug_Background__c","backupState":true}]}');
            CSC_AdjustmentsController.returnBackupTypes(testCase.Id);
            CSC_AdjustmentsController.calculateDifference('{"fieldUpdate":[{"Weekday__c":"2021-06-29","Correct_Bill_Rate__c":500,"Original_Hours__c":10,"Correct_Hours__c":20,"Bill_Rate_Difference__c":20,"Gross_Difference__c":700,"Original_Gross__c":300,"Weekending__c":"2021-06-29","Correct_Pay_rate__c":50,"Job_Req_ID__c":"11111"}]}');
			CSC_AdjustmentsController.calculateDifference('{"fieldUpdate":[{"Weekday__c":"2021-06-29","Correct_Bill_Rate__c":null,"Original_Hours__c":null,"Correct_Hours__c":null,"Bill_Rate_Difference__c":null,"Gross_Difference__c":null,"Original_Gross__c":null,"Weekending__c":"2021-06-29","Correct_Pay_rate__c":null,"Job_Req_ID__c":"11111"}]}');
			Test.stopTest();
            }
    	
        
    }
}