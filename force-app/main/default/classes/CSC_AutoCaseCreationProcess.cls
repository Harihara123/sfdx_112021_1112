/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* CSC_AutoCaseCreationProcess class is used to Create Started Cases.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ismail Jabeebulla Shaik
* @modifiedBy     Ismail Jabeebulla Shaik
* @maintainedBy   Team CSC
* @version        1.0
* @created        2020-09-16
* @modified       2020-10-14

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class CSC_AutoCaseCreationProcess {
    public static void createVSBCases(list<Order> orderList, Map<Id,Opportunity> oppMap){
        system.debug('createVSBCases=======::Started::======>>>>>');
        try{
            list<String> officeCodes = getOfficeCodes();
            list<String> CSCOfficeCodes = new list<String>();
            list<String> divisionList = System.label.CSC_Divisions.split(';');
            string opcoSwitch = System.label.CSC_OPCO_Switch;
            list<Case> newCaseList = new list<Case>();
            set<Id> talentIds = new set<Id>();
            list<String> globalIds = new list<String>();
            Id cscRecordtypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collocated Services Center').getRecordTypeId();
            list<CSC_National_Accounts_Global_Ids__c> nationalAccts = CSC_National_Accounts_Global_Ids__c.getall().values();
            globalIds = nationalAccts[0].Global_Ids__c.split(',');
            for(CSC_Region_Alignment_Mapping__c aliMap : [select id,Talent_s_Office_Code__c from CSC_Region_Alignment_Mapping__c]){
                CSCOfficeCodes.add(aliMap.Talent_s_Office_Code__c);
            }
            for(Order orderObj : orderList){
                if(oppMap.containsKey(orderObj.OpportunityId)){
                    String officeCode = '';
                    String opco = '';
                    Opportunity oppObj = oppMap.get(orderObj.OpportunityId);
                    officeCode = oppObj.Organization_Office__r.Office_Code__c;
                    if(oppObj.OpCo__c == 'Aerotek, Inc'){
                        opco = 'ONS';
                    }else if(oppObj.OpCo__c == 'TEKsystems, Inc.'){
                        opco = 'TEK';
                    }else{
                        opco = oppObj.OpCo__c;
                    }
                    system.debug('officeCode======>>>>>>>'+officeCode);
                    system.debug('opco======>>>>>>>'+opco);
                    if(officeCode != '' && opco != '' && (officeCodes.contains(officeCode) || (System.label.CSC_Activate_Field_Offices == 'YES' && opco == 'ONS' && !CSCOfficeCodes.contains(officeCode))) && opcoSwitch.contains(opco) && divisionList.contains(oppObj.Req_Division__c) && oppObj.Req_TGS_Requirement__c != 'Yes'){
                        system.debug('Case Created=====>>>>');
                        Case caseObj = new Case();
                        caseObj.RecordTypeId = cscRecordtypeId;
                        caseObj.Type = 'Started';
                        //caseObj.Sub_Type__c = 'New Hire';
                        caseObj.Subject = 'New Started Case Created';
                        caseObj.VSB_Start_Date__c = orderObj.Start_Date__c;
                        caseObj.Recruiter__c = Userinfo.getUserId();
                        caseObj.Recruiter_Name__c = Userinfo.getName();
                        caseObj.ESF_ID__c = orderObj.Start_Form_ID__c;
                        caseObj.ContactId = orderObj.ShipToContactId;
                        caseObj.Contractor__c = orderObj.ShipToContactId;
                        caseObj.Contractor_Name__c = orderObj.Contractor_Name__c;
                        if(oppObj.Req_Product__c == 'Permanent')
                            caseObj.Employee_Type__c = 'Direct Placement/Conversion';
                        else
                        	caseObj.Employee_Type__c = oppObj.Req_Product__c;
                        caseObj.VSB_Client_Account_Name__c = oppObj.Account.Name;
                        caseObj.VSB_Division__c = oppObj.Req_Division__c;
                        caseObj.Account_Manager__c = oppObj.OwnerId;
                        caseObj.Account_Manager_Name__c = oppObj.Owner.Name;
                        caseObj.VSB_Office_Code__c = officeCode;
                        if(orderObj.Comments__c != '' && orderObj.Comments__c != NULL)
                            caseObj.Case_Issue_Description__c = orderObj.Comments__c;
                        else
                        	caseObj.Case_Issue_Description__c = 'Started case has been generated';
                        caseObj.VSB_OPCO__c = opco;
                        caseObj.Submittal__c = orderObj.Id;
                        caseObj.Req_TGS_Requirement__c = oppObj.Req_TGS_Requirement__c;
                        caseObj.Practice_Engagement__c = oppObj.Practice_Engagement__c;
                        if(opco == 'ONS' && (globalIds.contains(oppObj.Account.Master_Global_Account_Id__c) || (oppObj.Account.Master_Global_Account_Id__c == '1-PO59-162' && oppObj.Req_Additional_Information__c != '' && oppObj.Req_Additional_Information__c != NULL && oppObj.Req_Additional_Information__c.trim() == 'AllianceRx Walgreens Prime SAP' ))){
                            caseObj.Account_Identifier__c = 'National';
                        }else{
                            caseObj.Account_Identifier__c = 'Retail';
                        }
                        if(oppObj.Req_Division__c == 'EASi Engineering' || oppObj.Req_Division__c == 'EASi Sciences'){
                            caseObj.Non_CSC_started_case__c = True;
                        }
                        if((System.label.CSC_Activate_Field_Offices == 'YES' && opco == 'ONS' && !CSCOfficeCodes.contains(officeCode))){
                            caseObj.Non_CSC_started_case__c = True;
                        }
                        newCaseList.add(caseObj);
                    }
                }
            }
            if(!newCaseList.isEmpty()){
                //database.insert(newCaseList,false);
                insert newCaseList;
            }
        }Catch(Exception ex){
            set<Id> talentRecIds = new set<Id>();
            for(Order orderObj : orderList){
                talentRecIds.add(orderObj.ShipToContactId);
            }
            list<Contact> talentRecs = [select id,Name,Peoplesoft_ID__c from Contact where Id IN: talentRecIds];
            CSC_Scheduler_Error_Log__c err = new CSC_Scheduler_Error_Log__c();
            err.Class_Name__c = 'Started Cases';
            err.Error__c = ex.getMessage();
            if(!talentRecs.isEmpty()){
                err.Talent_Name__c = talentRecs[0].Name;
                err.Talent_People_Soft_Id__c = talentRecs[0].Peoplesoft_ID__c;
            }
            insert err;
            system.debug('ex====>>>>>'+ex);
        }
    }
    
    public static list<String> getOfficeCodes(){
        list<String> all_OfficeCodes = new list<String>();
        list<CSC_Office_Codes__c> officeCodes = CSC_Office_Codes__c.getall().values();
        for(CSC_Office_Codes__c code : officeCodes){
            list<String> zipCodes = code.Office_Codes__c.split(',');
            all_OfficeCodes.addAll(zipCodes);
        }
        return all_OfficeCodes;
    }
    
    // Code to update Not proceeding cases
    public static void updateNotProcCases(list<Order> orderList, Map<Id,Opportunity> oppMap){
        set<Id> submittalIds = new set<Id>();
        Map<Id,Order> orderMap = new Map<Id,Order>();
        list<Case> startedCases = new list<Case>();
        for(Order obj : orderList){
            orderMap.put(obj.Id, obj);
        }
        for(Case caseObj : [Select id,Case_Issue_Description__c,Status,Submittal__c from case where Submittal__c IN: orderMap.keySet()]){
            if(orderMap.containsKey(caseObj.Submittal__c)){
                if(caseObj.Status != 'Closed'){
                	caseObj.Case_Issue_Description__c = caseObj.Case_Issue_Description__c + '<p></p>' + 'TALENT NOT PROCEEDING - ' + orderMap.get(caseObj.Submittal__c).Submittal_Not_Proceeding_Reason__c;
                    startedCases.add(caseObj);
                }else{
                    caseObj.Status = 'Under Review';
                	caseObj.Case_Issue_Description__c = caseObj.Case_Issue_Description__c + '<p></p>' + 'TALENT NOT PROCEEDING - ' + orderMap.get(caseObj.Submittal__c).Submittal_Not_Proceeding_Reason__c;
                    caseObj.Under_Review_Comments__c = 'TALENT NOT PROCEEDING - ' + orderMap.get(caseObj.Submittal__c).Submittal_Not_Proceeding_Reason__c;
                    startedCases.add(caseObj);
                }
            }
        }
        if(!startedCases.isEmpty()){
            try{
            	update startedCases;
            }catch(Exception ex){
                system.debug('ex====>>>>>'+ex);
            }
        }
    }
    
}