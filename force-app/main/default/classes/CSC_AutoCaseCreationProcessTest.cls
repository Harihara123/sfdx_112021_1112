@isTest
public class CSC_AutoCaseCreationProcessTest {
    static testmethod void processRecordsTest(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Test.startTest();
        
        // Region Alignment Mapping 
        CSC_Region_Alignment_Mapping__c rMap = new CSC_Region_Alignment_Mapping__c();
        rMap.Talent_s_Office_Code__c = '00001';
        rMap.Center_Name__c = 'Jacksonville';
        rMap.Talent_s_OPCO__c = 'TEK';
        rMap.Location_Description__c = 'Austin';
        insert rMap;
        
        CSC_National_Accounts_Global_Ids__c globalIds = new CSC_National_Accounts_Global_Ids__c();
        globalIds.Name = 'Master Global Id';
        globalIds.Global_Ids__c = '1-ZU-2006,1-10O-6973,1-10Q-7103,1-MG7JTU,1-100V4LZ,1-10O-7534,1-10O-8507,1-KZQ2-1442,1-5UPG-128,1-27R6-82,1-XWO7MQ,1-102-9320,1-KIGI-57,1-MMN-51,1-106-5997';
        globalIds.Req_Office_Codes__c = '01378,00407';
        insert globalIds;
        
        //Inserting Custom settings Data
        CSC_Office_Codes__c code = new CSC_Office_Codes__c();
        code.Name = 'Jax_Tempe1';
        code.Office_Codes__c = '000592,00001';
        insert code;
        
        TestData td=new TestData();
        td.listSize=5;
        List<Account> acclist=td.createAccounts();
        Account acc = accList.get(0);
        acc.Talent_Ownership__c = 'TEK';
        update acc;
        List<Contact> conList=td.createContacts('Talent');
        
        User_Organization__c org = new User_Organization__c();
        org.Active__c = True;
        org.OpCo_Name__c = 'TEK';
        org.Office_Code__c = '00001';
        insert org;
        
        List<Opportunity> oppList = td.createOpportunities();
        Opportunity opp=oppList.get(0);
        opp.Req_Total_Positions__c=1;
        opp.OpCo__c = 'TEKsystems, Inc.';
        opp.Organization_Office__c = org.Id;
        opp.Req_TGS_Requirement__c = 'No';
        opp.Req_Division__c = 'Applications';    
        update opp;
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>();
        oppMap.put(opp.Id, opp);
        Schema.DescribeSObjectResult d = Schema.SObjectType.Order;
        
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('OpportunitySubmission');
        
        List<Order> odList=new List<Order>();
        Order NewOrder1 = new Order(Name = 'New Order1', EffectiveDate = system.today()+1,Comments__c = 'Test Comments',
                                    Status ='Submitted',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());
        
        /*Order NewOrder2 = new Order(Name = 'New Order2', EffectiveDate = system.today()+1,
                                    Status ='Submitted',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());   */
        odList.add(NewOrder1);
        //    odList.add(NewOrder2);
        insert odList;
        
        NewOrder1.status='Started';
        update NewOrder1;
        
        CSC_AutoCaseCreationProcess.updateNotProcCases(odList, oppMap);
        
        Test.stopTest();
    }
}