global class CSC_CRAdjustmentProvisionBatch implements Database.Batchable<Sobject> ,Database.stateful, Schedulable{
    public integer currentBatch;
    //execute method vor schedular
    public void execute(SchedulableContext sc){
        CSC_CRAdjustmentProvisionBatch batchable = new CSC_CRAdjustmentProvisionBatch(1);
        Database.executeBatch(batchable,50);
    }
    global CSC_CRAdjustmentProvisionBatch(integer batchJobNo){
        currentBatch = batchJobNo;
        system.debug('currentBatch=======>>>>>>>>'+currentBatch);
    }
    global database.Querylocator start(Database.BatchableContext bc){
        system.debug('currentBatch=======>>>>>>>>'+currentBatch);
        String strQuery = '';
        list<String> jobCodeQAList = System.label.CSC_Check_Request_Job_codes.split(',');
        list<String> jobCodeParollList = System.label.CSC_Check_Request_Payroll_Job_codes.split(',');
        if(currentBatch == 1){
            strQuery = 'select Id,ODS_Jobcode__c from User where IsActive = true AND OPCO__c = \'TEK\' AND ODS_Jobcode__c IN :jobCodeQAList AND UserType = \'Standard\'';
        }
        if(currentBatch == 2){
            strQuery = 'select Id,ODS_Jobcode__c from User where IsActive = true AND OPCO__c = \'TEK\' AND ODS_Jobcode__c IN :jobCodeParollList AND UserType = \'Standard\'';
        }
        system.debug('strQuery=====>>>>>>'+strQuery);
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext bc , list<User> scope){
        system.debug('currentBatch=======>>>>>>>>'+currentBatch);
        List<GroupMember> GroupMemberlist = new List<GroupMember>();
        List<PermissionSetAssignment> PermissionSetList = new list<PermissionSetAssignment>();
        list<String> checkRequestQAPGPS = System.label.CSC_CR_QA_PG_PS.split(',');
        list<String> checkRequestPayrollPGPS = System.label.CSC_CR_Payroll_PG_PS.split(',');
        for(User userObj : scope){
            if(currentBatch == 1){
                // Group Mamber
                GroupMember GM = new GroupMember();
                GM.GroupId = id.valueof(checkRequestQAPGPS[0]);
                GM.UserOrGroupId = userObj.Id;
                GroupMemberlist.add(GM);
                
                // Group Mamber For Queue
                GroupMember QGM = new GroupMember();
                QGM.GroupId = id.valueof(checkRequestQAPGPS[2]);
                QGM.UserOrGroupId = userObj.Id;
                GroupMemberlist.add(QGM);
                
                // Permission Set
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.PermissionSetId = id.valueof(checkRequestQAPGPS[1]);
                psa.AssigneeId = userObj.Id;
                PermissionSetList.add(psa);
            }
            if(currentBatch == 2){
                // Group Mamber
                GroupMember GM = new GroupMember();
                GM.GroupId = id.valueof(checkRequestPayrollPGPS[0]);
                GM.UserOrGroupId = userObj.Id;
                GroupMemberlist.add(GM);
                
                // Permission Set
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.PermissionSetId = id.valueof(checkRequestPayrollPGPS[1]);
                psa.AssigneeId = userObj.Id;
                PermissionSetList.add(psa);
            }
        }
        // Adding Users to Group & Permission set
        if(!GroupMemberlist.isEmpty()){
            database.insert(GroupMemberlist,false);
        }
        if(!PermissionSetList.isEmpty()){
            database.insert(PermissionSetList,false);
        }
    }
    global void finish(Database.BatchableContext bc){
        if(currentBatch < 2){
            currentBatch = currentBatch + 1;
            CSC_CRAdjustmentProvisionBatch batchable = new CSC_CRAdjustmentProvisionBatch(currentBatch);
        	Database.executeBatch(batchable,50);
        }
    }
}