public with Sharing class CSC_CaseAgeOfEachStatus {
	public transient list<WrapperClass> lstWrap {get;set;}
    public boolean showTable {get;set;}
	public Case caseObj {get;set;}
	public String opco {get;set;}
	public String region {get;set;}
	public String ownerName {get;set;}
	public list<String> currentStatus {get;set;} 
    public Date startCloseDate{get;set;}
    public Date endCloseDate{get;set;}
	public list<selectOption> lstRegion {get;set;}
    public list<selectOption> lstStatus {get;set;}
    public list<selectOption> lstCenter {get;set;}
	public list<String> selectedRegions {get;set;}
    public String selectedCenter {get;set;}
    
    public CSC_CaseAgeOfEachStatus(){
        showTable = false;
		caseObj = new Case();
		opco = 'TEK';
		populateRegion();
        populateStatus();
        lstCenter = new list<selectOption>();
        lstCenter.add(new SelectOption('','--Select--'));
        lstCenter.add(new SelectOption('Jacksonville','Jacksonville'));
        lstCenter.add(new SelectOption('Tempe','Tempe'));
    }
	
	public void validationCheck(){
		
		
	}
	Public void populateRegion(){
		list<User_Organization__c> lstUo = [select Id, Name, OpCo_Code__c,Office_Code__c,Region_Name__c,Region_Code__c from User_Organization__c where OpCo_Code__c = :opco limit 50000];
		set<String> uniqueRegion = new set<String>();
		for(User_Organization__c eachOrg : lstUo){
			if(eachOrg.Region_Name__c != null && eachOrg.Region_Name__c != ''){
				uniqueRegion.add(eachOrg.Region_Name__c);
            }
		}
		lstRegion = new List<SelectOption>();
		lstRegion.add(new SelectOption('','--Select--'));
		for(String reg : uniqueRegion){
			lstRegion.add(new SelectOption(reg,reg));
		}
		lstWrap = new list<WrapperClass>();
        system.debug('----------lstRegion----------'+lstRegion);
	}
    
    Public void populateStatus(){
        Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        lstStatus = new List<SelectOption>();
        lstStatus.add(new SelectOption('','--Select--'));
        for( Schema.PicklistEntry f : ple){
        	if(f.getValue() != 'Add to backlog'){
            	lstStatus.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }       
        system.debug('----------lstStatus----------'+lstStatus);
	}
        
	public void searchResult(){	
        validationCheck();
		lstWrap = new list<WrapperClass>();
		system.debug('---------caseObj.Future_Date__c---------'+caseObj.Future_Date__c);
		system.debug('----------caseObj.Requested_Date__c------------'+caseObj.Requested_Date__c);
        system.debug('---------caseObj.Start_Close_Date__c---------'+caseObj.Start_Close_Date__c);
		system.debug('----------caseObj.End_Close_Date__c------------'+caseObj.End_Close_Date__c);
		Date fromDate = caseObj.Future_Date__c;
		Date toDate = caseObj.Requested_Date__c;
        Date startCloseDate = caseObj.Start_Close_Date__c;
        Date endCloseDate = caseObj.End_Close_Date__c;
        String talentOffice = caseObj.Talent_Office__c;
        toDate = toDate.addDays(1);
		String baseQuery = 'select id, CaseNumber, Owner.Name,Contact.Name, CreatedDate,ClosedDate,Creator_Office__c,Creator_s_OpCo__c,Region__c ,Status, Type, sub_Type__c,Business_Unit__c,Business_Unit_Region__c,Center_Name__c,Talent_Office__r.name,Case_Resolution_Comment__c,Re_Opened_Count__c,(select status__c, Id, Live_Age__c, On_Hold_Reason__c,Created_on_Weekend_Holiday__c from Case_Status_Histories__r) from Case where ( recordType.DeveloperName = \'FSG\' OR recordType.DeveloperName = \'CSC_Read_Only\') ';
		
        if(!Test.isRunningTest()){
            baseQuery = baseQuery + ' AND CreatedDate >= :fromDate AND Client_Account_Name__c = null AND CreatedDate <= :toDate AND Creator_s_OpCo__c = :opco ';
        }
		/*list<Case> lstCaseWithStatusHistory = [select id, CaseNumber, Owner.Name,Contact.Name, CreatedDate,Creator_Office__c,Creator_s_OpCo__c,Region__c Status, Type, sub_Type__c,(select status__c, Id, Live_Age__c from Case_Status_Histories__r) from Case where recordType.DeveloperName = 'FSG' AND CreatedDate >= caseObj.Future_Date__c AND CreatedDate <= caseObj.Requested_Date__c AND Creator_s_OpCo__c = :opco limit 50000];*/
		if(selectedRegions != null && !selectedRegions.isEmpty() ){
			baseQuery = baseQuery + ' AND Region__c IN :selectedRegions ';
		}
		if(ownerName != null && ownerName != ''){
			baseQuery = baseQuery + ' AND Owner.Name = :ownerName ';
		}
		if(currentStatus != null && !currentStatus.isEmpty()){
			baseQuery = baseQuery + ' AND Status IN :currentStatus ';
		}
        if(currentStatus != null && !currentStatus.isEmpty() && currentStatus.contains('Closed')){
            baseQuery = baseQuery + ' AND ClosedDate >= :startCloseDate AND ClosedDate <= :endCloseDate ';
        }
        if(selectedCenter != null && selectedCenter != ''){
			baseQuery = baseQuery + ' AND Center_Name__c = \''+selectedCenter+'\' ';
		}
        if(talentOffice != null && talentOffice != ''){
            baseQuery = baseQuery + ' AND Talent_Office__c = \''+talentOffice+'\' ';
        }
        
		baseQuery = baseQuery + ' LIMIT 100000';
		
		system.debug('----------baseQuery------------'+baseQuery);
		transient list<Case> lstCaseWithStatusHistory = database.query(baseQuery);
		for(Case eachcase : lstCaseWithStatusHistory){
			WrapperClass wrapObj = new WrapperClass();
			wrapObj.caseId = eachcase.Id;
			wrapObj.caseNumber = eachcase.CaseNumber;
			wrapObj.caseOwnerName = eachcase.Owner.Name;
			wrapObj.contactName = eachcase.Contact.Name;
			wrapObj.CreatedDate = eachcase.CreatedDate.format();
            if(eachcase.ClosedDate != null){
                wrapObj.ClosedDate = eachcase.ClosedDate.format();
            }
            wrapObj.officeId = eachcase.Creator_Office__c;
			wrapObj.caseOPCO = eachcase.Creator_s_OpCo__c;
			wrapObj.caseRegion = eachcase.Region__c;
			wrapObj.Status = eachcase.Status;
			wrapObj.caseType = eachcase.Type;
			wrapObj.caseSubType = eachcase.sub_Type__c;
			wrapObj.caseResolutionComment = eachcase.Case_Resolution_Comment__c;
			wrapObj.reOpenCount = Integer.valueOf(eachcase.Re_Opened_Count__c);
            wrapObj.businessUnit = eachcase.Business_Unit__c;
            wrapObj.businessRegion = eachcase.Business_Unit_Region__c;
            wrapObj.centerName = eachcase.Center_Name__c;
            wrapObj.officeTalent = eachcase.Talent_Office__r.name;
			wrapObj.ageNew = 0.0;
			wrapObj.countNew = 0;
			wrapObj.ageAssigned = 0.0;
			wrapObj.countAssigned = 0;
			wrapObj.ageInProgress = 0.0;
			wrapObj.countInProgress = 0;
			wrapObj.ageOnHold = 0.0;
			wrapObj.countOnHold = 0;
			wrapObj.ageEscalated = 0.0;
			wrapObj.countEscalated = 0;
			wrapObj.ageUnderReview = 0.0;
			wrapObj.countUnderReview = 0;
			
			for(case_Status_History__c csh : eachcase.Case_Status_Histories__r){
                if(csh.Created_on_Weekend_Holiday__c == false){
                    if(csh.status__c == 'New'){
                        wrapObj.ageNew += csh.Live_Age__c;
                        wrapObj.countNew +=1;
                    }
                    if(csh.status__c == 'Assigned'){
                        wrapObj.ageAssigned += csh.Live_Age__c;
                        wrapObj.countAssigned +=1;
                    }
                    if(csh.status__c == 'In Progress'){
                        wrapObj.ageInProgress += csh.Live_Age__c;
                        wrapObj.countInProgress +=1;
                    }
                    
                    if(csh.status__c == 'On Hold' && csh.On_Hold_Reason__c != 'Future Dated Change'){
                        wrapObj.ageOnHold += csh.Live_Age__c;
                        wrapObj.countOnHold +=1;
                    }
                    if(csh.status__c == 'Escalated'){
                        wrapObj.ageEscalated += csh.Live_Age__c;
                        wrapObj.countEscalated +=1;
                    }
                    if(csh.status__c == 'Under Review'){
                        wrapObj.ageUnderReview += csh.Live_Age__c;
                        wrapObj.countUnderReview +=1;
                    }
                }
			}
			wrapObj.caseAge = wrapObj.ageNew + wrapObj.ageAssigned + wrapObj.ageInProgress +wrapObj.ageEscalated + wrapObj.ageUnderReview;
			lstWrap.add(wrapObj);
            
		}
        if(!lstWrap.isEmpty()){
            showTable = true;
        }else{
            showTable = false;
        }
	}

	class WrapperClass{

		public Id caseId {get;set;}
		public String caseNumber {get;set;}
		public String caseOwnerName {get;set;}
		public String contactName {get;set;}
		public String CreatedDate {get;set;}
        public String ClosedDate {get;set;}
		public String officeId {get;set;}
		public String caseOPCO {get;set;}
		public String caseRegion {get;set;}
		public String Status {get;set;}
		public String caseType {get;set;}
		public String caseSubType {get;set;}
		public String caseResolutionComment{get;set;}
		public Integer reOpenCount {get;set;}
        public String businessUnit{get;set;}
        public String businessRegion{get;set;}
        public String centerName{get;set;}
        public String officeTalent{get;set;}
		public Decimal ageNew {get;set;}
		public Integer countNew {get;set;}
		public Decimal ageAssigned {get;set;}
		public Integer countAssigned {get;set;}
		public Decimal ageInProgress {get;set;}
		public Integer countInProgress {get;set;}
		public Decimal ageOnHold {get;set;}
		public Integer countOnHold {get;set;}
		public Decimal ageEscalated {get;set;}
		public Integer countEscalated {get;set;}
		public Decimal ageUnderReview {get;set;}
		public Integer countUnderReview {get;set;}
		public Decimal caseAge {get;set;}
	}
}