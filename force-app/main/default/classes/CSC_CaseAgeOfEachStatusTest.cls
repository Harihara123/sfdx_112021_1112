@isTest
private class CSC_CaseAgeOfEachStatusTest {
static testmethod void test() {
        Test.startTest();
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id,OPCO__c,Name FROM User WHERE Id = :UserInfo.getUserId()];
        
        thisUser.OPCO__c = 'TEK';
        update thisUser;
        String currentUserName = thisUser.Name; 
       // Insert account as current user
        System.runAs (thisUser) {
        // Inserting test users    
        List<User> testUsers = new List<User>();
        
        User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
        User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
        
        testUsers.add(testUsr1);
        testUsers.add(testUsr2);
        insert testUsers; 
        
        User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
        
        Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
        insert acc;
        
        Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
        insert testContact;
        
        Id p = [select id from profile where name='Aerotek Community User'].id;
        User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = testContact.Id,
                timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
       
        insert Talentusr;
            
        Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
        insert testContact222;
        
        //Id p = [select id from profile where name='Aerotek Community User'].id;
        User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = testContact222.Id,Region__c = 'TEK Central',
                timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
       
        insert Talentusr222;
        
        // Inserting a new CSC case record
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
        system.debug('-----'+recTypeId); 
        
        //Future_Date__c = System.today()-2,
        //Requested_Date__c = System.today()+2,
        
        Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                 Type ='Payroll', 
                                 Sub_Type__c='Tax Error',
                                 AccountId = acc.id,
                                 ContactId = testContact.Id,
                                 ownerId = thisUser.Id,
                                 Contact=testContact, 
                                 Case_Issue_Description__c ='test Case Issue Description', 
                                 recordTypeId=recTypeId );       
        insert testCase; 
            
         Case testCase2 = new Case(subject = 'CSC Test222', description = 'CSC_Test_Group', 
                                 Type ='Payroll', 
                                 Sub_Type__c='Tax Error',
                                 AccountId = acc.id,
                                 ContactId = testContact222.Id,
                                 ownerId = thisUser.Id,
                                 //Region_Talent__c = 'TEK Central',
                                 //Contact=testContact222, 
                                 Case_Issue_Description__c ='test Case Issue Description', 
                                 recordTypeId=recTypeId );       
        insert testCase2; 
        testCase2.Center_Name__c = 'Tempe';
            try{
        update testCase2;
            }catch(exception e){
                
            }
            
         Case_Status_History__c csh1 = new Case_Status_History__c();
         csh1.Case__c = testCase.Id;
         csh1.Start_Time__c = system.now();
         csh1.End_Time__c = system.now().addHours(1);
         csh1.Status__c = 'new';
            
         Case_Status_History__c csh2 = new Case_Status_History__c();
         csh2.Case__c = testCase.Id;
         csh2.Start_Time__c = system.now().addHours(1);
         csh2.End_Time__c = system.now().addHours(2);
         csh2.Status__c = 'Assigned';
         
         Case_Status_History__c csh3 = new Case_Status_History__c();
         csh3.Case__c = testCase.Id;
         csh3.Start_Time__c = system.now().addHours(2);
         csh3.End_Time__c = system.now().addHours(3);
         csh3.Status__c = 'In Progress';
            
         Case_Status_History__c csh4 = new Case_Status_History__c();
         csh4.Case__c = testCase.Id;
         csh4.Start_Time__c = system.now().addHours(3);
         csh4.End_Time__c = system.now().addHours(4);
         csh4.Status__c = 'On Hold';
            
         Case_Status_History__c csh5 = new Case_Status_History__c();
         csh5.Case__c = testCase.Id;
         csh5.Start_Time__c = system.now().addHours(4);
         csh5.End_Time__c = system.now().addHours(5);
         csh5.Status__c = 'Escalated';
            
         Case_Status_History__c csh6 = new Case_Status_History__c();
         csh6.Case__c = testCase.Id;
         csh6.Start_Time__c = system.now().addHours(5);
         csh6.End_Time__c = system.now().addHours(6);
         csh6.Status__c = 'Under Review';
            
         Case_Status_History__c csh7 = new Case_Status_History__c();
         csh7.Case__c = testCase2.Id;
         csh7.Start_Time__c = system.now();
         //csh7.End_Time__c = system.now().addHours(1);
         csh7.Status__c = 'new';
            
         list<Case_Status_History__c> listCsh = new list<Case_Status_History__c>();
         listCsh.add(csh1); 
            listCsh.add(csh2);
            listCsh.add(csh3);
            listCsh.add(csh4);
            listCsh.add(csh5);
            listCsh.add(csh6);
            listCsh.add(csh7);
        insert listCsh;
         list<User_Organization__c> lstUo = new list<User_Organization__c>(); 
         User_Organization__c uo = new User_Organization__c();
         uo.Region_Name__c = 'TEK WEST';
         uo.OpCo_Code__c = 'TEK';
         User_Organization__c uo1 = new User_Organization__c();
         uo1.Region_Name__c = 'Aerotek Southeast';
         uo1.OpCo_Code__c = 'ONS';
         lstUo.add(uo);
         lstUo.add(uo1);
         insert lstUo;
         
            CSC_CaseAgeOfEachStatus ageEachStatus = new CSC_CaseAgeOfEachStatus();
          ageEachStatus.populateRegion();
          ageEachStatus.populateStatus();
          ageEachStatus.caseObj = new Case();
          //ageEachStatus.ownerName = currentUserName;
          ageEachStatus.caseObj.Future_Date__c = system.today().addDays(-30);
          ageEachStatus.caseObj.Requested_Date__c = system.today();
          ageEachStatus.searchResult();
            
          	string input = '{"opcoValue":"TEK","centerValue":"JACKSONVILLE","createdDate":"1/2/2021,2/3/2023","statusValues":["New"]}';
            CSC_RTCAReportController.searchResult(input);
            
            // Test class method for adjustment CSC_AdjustmentsController
            CSC_AdjustmentsController.isAdjustmentCheck(testCase.Id);
        }
     
      
        
    
    
      CSC_ExcludeWeekendsAndHolidaysLogoutJax m = new CSC_ExcludeWeekendsAndHolidaysLogoutJax();
        String sch = '0 59 9 ? * MON-FRI';
        String jobID = system.schedule('CSC Logout Job jax test', sch, m);
    
      /*CSC_ExcludeWeekendsAndHolidaysLogoutJax m1 = new CSC_ExcludeWeekendsAndHolidaysLogoutJax();
        String sch1 = '0 59 9 1 1 ? *';
        String jobID1 = system.schedule('CSC Logout Job jax test hol', sch1, m1);*/
        
        CSC_ExcludeWeekendsAndHolidaysLoginJax m2 = new CSC_ExcludeWeekendsAndHolidaysLoginJax();
        String sch2 = '0 0 6 ? * MON-FRI';
        String jobID2 = system.schedule('CSC Login Job jax test', sch2, m2);
    
      /*CSC_ExcludeWeekendsAndHolidaysLoginJax m3 = new CSC_ExcludeWeekendsAndHolidaysLoginJax();
        String sch3 = '0 0 6 1 1 ? *';
        String jobID3 = system.schedule('CSC Login Job jax test hol', sch3, m3);*/
    
      CSC_ExcludeWeekendsAndHolidaysLogoutTemp m3 = new CSC_ExcludeWeekendsAndHolidaysLogoutTemp();
        String sch3 = '23 59 23 ? * MON-FRI';
        String jobID3 = system.schedule('CSC Logout Job tempe test', sch3, m3);
        
        CSC_ExcludeWeekendsAndHolidaysLoginTempe m4 = new CSC_ExcludeWeekendsAndHolidaysLoginTempe();
        String sch4 = '0 0 0 ? * MON-FRI';
        String jobID4 = system.schedule('CSC Login Job Tempe test', sch4, m4);
      
      CSC_Holidays__c cusHol = new CSC_Holidays__c();
      cusHol.Date__c = system.today();
      cusHol.Name = 'testclassholiday';
      insert cusHol;
      
      CSC_ExcludeWeekendsAndHolidaysLogoutJax m5 = new CSC_ExcludeWeekendsAndHolidaysLogoutJax();
        String sch5 = '20 59 23 ? * MON-FRI';
        String jobID5 = system.schedule('CSC Logout Job jax test hol5', sch5, m5);
        database.executeBatch(new CSC_BatchToUpdateAssignedTimestamp(),200);    
        Test.stopTest();
    }
}