public with sharing class CSC_CaseFileUploadHelper {
    public static void updateCaseFlag (list<ContentDocumentLink> lstContentDocumentLink){
        Set<Id> caseIds = new Set<Id>();
        Set<Id> closedCaseIds = new Set<Id>();
        List<Case> listCaseToUpdate = new List<Case>();
        list<String> centerOfficeCodes = new list<String>{'01464','01468'};
        for(ContentDocumentLink cl : lstContentDocumentLink){
            if(String.valueOf(cl.LinkedEntityId).startsWith('500')){
                caseIds.add(cl.LinkedEntityId);
            }
        }
        if(!caseIds.isEmpty()){
            for(Case caseObj : [select id,Status from Case where Id IN: caseIds AND Status = 'Closed' AND RecordType.Name = 'CSC Read Only']){
                closedCaseIds.add(caseObj.Id);
            }
        }
        if(!closedCaseIds.isEmpty()){
            for(ContentDocumentLink linkObj : lstContentDocumentLink){
                if(closedCaseIds.contains(linkObj.LinkedEntityId)){
                    linkObj.addError('Files cannot be attached on a closed CSC case.');
                }
            }
        }else{
            list<User> userList = [select id,Office_Code__c from User where Id =: UserInfo.getUserId() AND Office_Code__c IN: centerOfficeCodes];
            system.debug('userList=====>>>>>>>>'+userList);
            for(Id eachcCaseId : caseIds){
                Case newCase = new Case();
                newCase.Id = eachcCaseId;
                if(userList.isEmpty())
                	newCase.Comments_Updated__c = true;
                listCaseToUpdate.add(newCase);
            }
            if(!listCaseToUpdate.IsEmpty()){
                update listCaseToUpdate;
            }
        }
    }
    // Restricting users to remove the files from closed cases
    public static void beforeDelete(list<ContentDocumentLink> contentDocumentLinkList){
        set<Id> caseIds = new set<Id>();
        set<Id> closedCaseIds = new set<Id>();
        for(ContentDocumentLink linkObj : contentDocumentLinkList){
            string cdlentityId = linkObj.LinkedEntityId;
            if(cdlentityId != '' && cdlentityId.subString(0,3) == '500'){
                caseIds.add(linkObj.LinkedEntityId);
            }
        }
        if(!caseIds.isEmpty()){
            for(Case caseObj : [select id,Status from Case where Id IN: caseIds AND Status = 'Closed' AND RecordType.Name = 'CSC Read Only']){
                closedCaseIds.add(caseObj.Id);
            }
        }
        if(!closedCaseIds.isEmpty()){
            for(ContentDocumentLink linkObj : contentDocumentLinkList){
                if(closedCaseIds.contains(linkObj.LinkedEntityId)){
                    linkObj.addError('Files attached on a closed csc case cannot be removed.');
                }
            }
        }
    }
}