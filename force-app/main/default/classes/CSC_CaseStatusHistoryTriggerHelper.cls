public class CSC_CaseStatusHistoryTriggerHelper {
    public static void updateOnHoldReason(list<Case_Status_History__c> cshList){
        set<Id> caseIds = new set<Id>();
        for(Case_Status_History__c cshObj : cshList){
            if(isWeekendOrHoliday(System.now()) == True){
                cshObj.Created_on_Weekend_Holiday__c = true;
            }
            caseIds.add(cshObj.Case__c);
        }
        Map<Id,Case> caseMap = new Map<Id,Case>([select id,Status,On_Hold_Reason__c from case where Id IN: caseIds]);
        for(Case_Status_History__c cshObj : cshList){
            if(caseMap.containsKey(cshObj.Case__c) && caseMap.get(cshObj.Case__c).Status == 'On Hold')
                cshObj.On_Hold_Reason__c = caseMap.get(cshObj.Case__c).On_Hold_Reason__c;
        }
    }
    
    public static boolean isWeekendOrHoliday(Datetime customDate){
        boolean nonWorkingDay = false;
         if(customDate.format('EEEE') == 'Saturday' || customDate.format('EEEE') == 'Sunday'){
             nonWorkingDay = True;
         }
         List<CSC_Holidays__c> listOfHolidays = CSC_Holidays__c.getall().values();
         for(CSC_Holidays__c hol : listOfHolidays){
              if(hol.Date__c == customDate.date()){
                 nonWorkingDay = True;
                 break;
             }
         }
         
        return nonWorkingDay;
    }
}