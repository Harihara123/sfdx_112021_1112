public class CSC_CaseTriggerHelper {
    public static Boolean isFirstTime = true;
    public static boolean isWorkingDay(Datetime customDate){
        boolean workingDay = true;
         if(customDate.format('EEEE') == 'Saturday' || customDate.format('EEEE') == 'Sunday'){
             workingDay = false;
         }
         List<CSC_Holidays__c> listOfHolidays = CSC_Holidays__c.getall().values();
         for(CSC_Holidays__c hol : listOfHolidays){
              if(hol.Date__c == customDate.date()){
                 workingDay = false;
                 break;
             }
         }
         
        return workingDay;
    }
    public static void lockStatus(List<Case> caseList,Map<Id, Case>oldMap, Map<Id, Case>newMap, Map<Id,Schema.RecordTypeInfo> rtMap){
        CSC_General_Setup__c listOfCSCGeneralSetUp = CSC_General_Setup__c.getValues('Lock Status');
        if(listOfCSCGeneralSetUp != null && listOfCSCGeneralSetUp.Lock_Status_over_Weekend_and_Holidays__c){
                for(Case eachCase : caseList){
                    if(rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'FSG' && !isWorkingDay(system.Now()) && oldMap.get(eachCase.Id).status != newMap.get(eachCase.Id).status){
                        newMap.get(eachCase.id).addError('Case status cannot be changed on weekends or holidays');
                    }
                }
        }
        
    }
    
    public static void futureDateValidation(List<Case> caseList,Map<Id, Case>oldMap, Map<Id, Case>newMap, Map<Id,Schema.RecordTypeInfo> rtMap){
        
        for(Case eachCase : caseList){
            if(rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'FSG' && eachCase.Future_Date__c != null && !isWorkingDay(DateTime.newInstance(eachCase.Future_Date__c, Time.newInstance(0, 0, 0, 0))) ){
                newMap.get(eachCase.id).addError('Future Date must be  a working day');
            }
        }
        
    }
    public static void populateCenterName(List<Case> caseList,Map<Id, Case>oldMap, Map<Id, Case>newMap, Map<Id,Schema.RecordTypeInfo> rtMap){
        Set<Id> setTalentContactIds = new set<Id>();
        boolean isStarted = False;
        boolean noTalent = False;
        boolean isFinalHoursCase = False;
        //Set<Id> setTalentContactIdsWoUser = new set<Id>();
        Set<String> setTalentOfficeCodes = new set<String>();
        Map<Id,Id> accConMap = new Map<Id,Id>();
        Map<Id,Talent_Work_History__c> startedConTWHMap = new Map<Id,Talent_Work_History__c>();
        Map<Id,Talent_Work_History__c> conTWHMap = new Map<Id,Talent_Work_History__c>();
        Map<Id,User> mapTalentContactAndUser = new map<Id,User>();
        map<String, String> mapOfficeCodeOpcoAndCenter = new map<String, String>();
        map<Id, String> mapOppOfficeAndOpco = new map<Id,String>();
        map<Id, String> mapConTalentState = new map<Id,String>();
        Map<String, Id> mapOfficeName = new Map<String, Id>();
        Map<String, Id> mapTalentOffice = new Map<String, Id>();
        Map<Id, string> conESFMap = new Map<Id, string>();
        for(Case eachCase : caseList){
            if(rtMap != Null && rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'FSG' && eachCase.VSB_Office_Code__c != null && eachCase.Type == 'Started'){
                setTalentOfficeCodes.add(eachCase.VSB_Office_Code__c);
                isStarted = True;
            }
            if(rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'FSG' && eachCase.ContactId != null){
                setTalentContactIds.add(eachCase.ContactId);
            }
            if(eachCase.Sub_Type__c == 'Final Hours'){
                isFinalHoursCase = True;
            }
        }
        for(Contact con : [select id,AccountId from Contact where Id IN: setTalentContactIds]){
            accConMap.put(con.AccountId,con.Id);
        }
        for(Talent_Work_History__c twh : [select id,SourceCompany__c,Talent__c,Start_Date__c,End_Date__c,ESFId__c from Talent_Work_History__c where Talent__c IN: accConMap.keyset() ORDER BY End_Date__c DESC]){
            if(!conESFMap.containsKey(accConMap.get(twh.Talent__c)) && twh.ESFId__c != '' && twh.ESFId__c != NULL){
                conESFMap.put(accConMap.get(twh.Talent__c),twh.ESFId__c);
            }
            if(accConMap.Containskey(twh.Talent__c)){
                startedConTWHMap.put(accConMap.get(twh.Talent__c), twh);
                if(twh.Start_Date__c <= System.today() && twh.End_Date__c >= System.today()){
                	conTWHMap.put(accConMap.get(twh.Talent__c), twh);
                }
            }
        }
        system.debug('startedConTWHMap======>>>>>>>'+startedConTWHMap);
        system.debug('conTWHMap======>>>>>>>'+conTWHMap);
        list<User> lstUserForCommunity = [select Id,ContactId, Office_Code__c, OPCO__c,Region__c,Office__c,Division__c,Business_Unit__c,Business_Unit_Region__c from User where ContactId in :setTalentContactIds and IsActive=true];
        for(User eachUser : lstUserForCommunity){
            if(eachUser.Office_Code__c != null){
                setTalentOfficeCodes.add(eachUser.Office_Code__c);
            }
            mapTalentContactAndUser.put(eachUser.ContactId,eachUser);
        }
        system.debug('lstUserForCommunity=====>>>>>>>'+lstUserForCommunity);
        system.debug('setTalentOfficeCodes======>>>>>>'+setTalentOfficeCodes);
        /*for(Id conId : setTalentContactIds){
            if(!mapTalentContactAndUser.containsKey(conId)){
                setTalentContactIdsWoUser.add(conId);
            }
        }*/
        if((isStarted == False && setTalentOfficeCodes.size() != setTalentContactIds.size()) || isFinalHoursCase == True){
            for(Order sub : [select id,Opportunity.Req_Worksite_State__c,Opportunity.OpCo__c,Opportunity.Organization_Office__r.Office_Code__c,ShipToContactId,Start_Form_ID__c from Order where ShipToContactId IN: setTalentContactIds AND Status != 'Applicant' AND Start_Form_ID__c IN: conESFMap.values() ORDER BY CreatedDate DESC]){
                setTalentOfficeCodes.add(sub.Opportunity.Organization_Office__r.Office_Code__c);
                string opco = '';
                if(sub.Opportunity.OpCo__c == 'Aerotek, Inc'){
                    opco = 'ONS';
                }else if(sub.Opportunity.OpCo__c == 'TEKsystems, Inc.'){
                    opco = 'TEK';
                }
                if(!mapOppOfficeAndOpco.containsKey(sub.ShipToContactId)){
                    mapOppOfficeAndOpco.put(sub.ShipToContactId, sub.Opportunity.Organization_Office__r.Office_Code__c+'___'+opco);
                    mapConTalentState.put(sub.ShipToContactId, sub.Opportunity.Req_Worksite_State__c);
                }
            }
        }
        for(CSC_Region_Alignment_Mapping__c mappingrec : [select Id,Name,Talent_s_Office_Code__c, Talent_s_OPCO__c,Center_Name__c from CSC_Region_Alignment_Mapping__c where Talent_s_Office_Code__c in :setTalentOfficeCodes]){
            mapOfficeCodeOpcoAndCenter.put(mappingrec.Talent_s_Office_Code__c+'___'+mappingrec.Talent_s_OPCO__c,mappingrec.Center_Name__c);
            mapOfficeName.put(mappingrec.Talent_s_Office_Code__c,mappingrec.Id);
            mapTalentOffice.put(mappingrec.Talent_s_Office_Code__c+'___'+mappingrec.Talent_s_OPCO__c, mappingrec.Id);
        }
        system.debug('mapOfficeCodeOpcoAndCenter========>>>>>>>>>'+mapOfficeCodeOpcoAndCenter);
        system.debug('mapOfficeName========>>>>>>>>>'+mapOfficeName);
        for(Case eachCaseRec : caseList){
            if(rtMap.get(eachCaseRec.RecordTypeId).getDeveloperName() == 'FSG' && (oldMap == null || (oldMap != null && (oldMap.get(eachCaseRec.Id).ContactId != newMap.get(eachCaseRec.Id).ContactId || oldMap.get(eachCaseRec.Id).VSB_Office_Code__c != newMap.get(eachCaseRec.Id).VSB_Office_Code__c))) && eachCaseRec.Client_Account_Name__c == null ){
                //before Insert and after update Logic
                system.debug('eachCaseRec.VSB_Office_Code__c========>>>>>>>>>'+eachCaseRec.VSB_Office_Code__c);
                system.debug('eachCaseRec.VSB_OPCO__c========>>>>>>>>>'+eachCaseRec.VSB_OPCO__c);
                system.debug('----update------');
                if(mapTalentContactAndUser.containsKey(eachCaseRec.ContactId)){ 
                    User userRec = mapTalentContactAndUser.get(eachCaseRec.ContactId);
                    if(eachCaseRec.Type == 'Started'){
                        if(oldMap != null && eachCaseRec.VSB_OPCO__c == 'ONS' && mapOfficeName.keySet().contains(eachCaseRec.VSB_Office_Code__c)){
                            eachCaseRec.Non_CSC_started_case__c = false;
                        }else if(oldMap != null && eachCaseRec.VSB_OPCO__c == 'ONS' && !mapOfficeName.keySet().contains(eachCaseRec.VSB_Office_Code__c)){
                            eachCaseRec.Non_CSC_started_case__c = true;
                        }
                        if(mapOfficeCodeOpcoAndCenter.containsKey(eachCaseRec.VSB_Office_Code__c+'___'+eachCaseRec.VSB_OPCO__c)){
                            eachCaseRec.Center_Name__c = mapOfficeCodeOpcoAndCenter.get(eachCaseRec.VSB_Office_Code__c+'___'+eachCaseRec.VSB_OPCO__c);
                        }else{
                            // default center Name is Jax when no match found
                            eachCaseRec.Center_Name__c = 'Jacksonville';
                        }
                    }else if(mapOfficeCodeOpcoAndCenter.containsKey(userRec.Office_Code__c+'___'+userRec.OPCO__c)){
                        eachCaseRec.Center_Name__c = mapOfficeCodeOpcoAndCenter.get(userRec.Office_Code__c+'___'+userRec.OPCO__c);
                    }else if(mapOppOfficeAndOpco.containsKey(eachCaseRec.ContactId)){
                        if(mapOfficeCodeOpcoAndCenter.containsKey(mapOppOfficeAndOpco.get(eachCaseRec.ContactId))){
                            eachCaseRec.Center_Name__c = mapOfficeCodeOpcoAndCenter.get(mapOppOfficeAndOpco.get(eachCaseRec.ContactId));
                        }else{
                            eachCaseRec.Center_Name__c = 'Jacksonville';
                        }
                    }else{
                        // default center Name is Jax when no match found
                        eachCaseRec.Center_Name__c = 'Jacksonville';
                    }
                    if(userRec.OPCO__c == 'ONS'){
                        eachCaseRec.Business_Unit__c = userRec.Business_Unit__c;
                        eachCaseRec.Business_Unit_Region__c = userRec.Business_Unit_Region__c;
                    }else{
                        eachCaseRec.Business_Unit__c = Null;
                    	eachCaseRec.Business_Unit_Region__c = null;
                    }
                    eachCaseRec.OPCO_Talent__c = userRec.OPCO__c;
                    eachCaseRec.Office_Talent__c = userRec.Office__c;
                    eachCaseRec.Region_Talent__c = userRec.Region__c;
                    eachCaseRec.Division_Talent__c = userRec.Division__c;
                    system.debug('mapTalentOffice======>>>>>>'+mapTalentOffice);
                    system.debug('eachCaseRec.Office_Talent__c======>>>>>>'+eachCaseRec.Office_Talent__c);
                    if(mapTalentOffice.containskey(userRec.Office_Code__c+'___'+userRec.OPCO__c)){
                        eachCaseRec.Talent_Office__c = mapTalentOffice.get(userRec.Office_Code__c+'___'+userRec.OPCO__c);
                    }
                    
                }else if(eachCaseRec.Type == 'Started'){
                    system.debug('No Talent====But Started Case======>>>>>>>');
                    system.debug('mapOfficeCodeOpcoAndCenter=====>>>>>>>'+mapOfficeCodeOpcoAndCenter);
                     system.debug('eachCaseRec.VSB_Office_Code__c=====>>>>>>>'+eachCaseRec.VSB_Office_Code__c);
                     system.debug('eachCaseRec.VSB_OPCO__c=====>>>>>>>'+eachCaseRec.VSB_OPCO__c);
                    if(oldMap != null && eachCaseRec.VSB_OPCO__c == 'ONS' && mapOfficeName.keySet().contains(eachCaseRec.VSB_Office_Code__c)){
                        eachCaseRec.Non_CSC_started_case__c = false;
                    }else if(oldMap != null && eachCaseRec.VSB_OPCO__c == 'ONS' && !mapOfficeName.keySet().contains(eachCaseRec.VSB_Office_Code__c)){
                        eachCaseRec.Non_CSC_started_case__c = true;
                    }
                    if(mapOfficeCodeOpcoAndCenter.containsKey(eachCaseRec.VSB_Office_Code__c+'___'+eachCaseRec.VSB_OPCO__c)){
                        eachCaseRec.Center_Name__c = mapOfficeCodeOpcoAndCenter.get(eachCaseRec.VSB_Office_Code__c+'___'+eachCaseRec.VSB_OPCO__c);
                    }else{
                        // default center Name is Jax when no match found
                        eachCaseRec.Center_Name__c = 'Jacksonville';
                    }
                }else if(mapOppOfficeAndOpco.containsKey(eachCaseRec.ContactId)){
                    if(mapOfficeCodeOpcoAndCenter.containsKey(mapOppOfficeAndOpco.get(eachCaseRec.ContactId))){
                        eachCaseRec.Center_Name__c = mapOfficeCodeOpcoAndCenter.get(mapOppOfficeAndOpco.get(eachCaseRec.ContactId));
                    }else{
                        // default center Name is Jax when no match found
                        eachCaseRec.Center_Name__c = 'Jacksonville';
                    }
                }else{
                    eachCaseRec.Center_Name__c = 'Jacksonville';
                    eachCaseRec.Business_Unit__c = Null;
                    eachCaseRec.Business_Unit_Region__c = null;
                    eachCaseRec.OPCO_Talent__c = null;
                    eachCaseRec.Office_Talent__c = null;
                    eachCaseRec.Region_Talent__c = null;
                    eachCaseRec.Division_Talent__c = null;
                }
                if(conTWHMap.containsKey(eachCaseRec.ContactId)){
                    eachCaseRec.Client_Name__c = conTWHMap.get(eachCaseRec.ContactId).SourceCompany__c != '' ? conTWHMap.get(eachCaseRec.ContactId).SourceCompany__c : ''; 
                }
                if(startedConTWHMap.containsKey(eachCaseRec.ContactId) && eachCaseRec.Type == 'Started'){
                    eachCaseRec.Sub_Type__c = 'Rehire'; 
                }else if(eachCaseRec.Type == 'Started'){
                    eachCaseRec.Sub_Type__c = 'New Hire';
                }
                if(mapConTalentState.containsKey(eachCaseRec.ContactId)){
                    eachCaseRec.Talent_Req_WorkSite_State__c = mapConTalentState.get(eachCaseRec.ContactId);
                }
            }
            
        }
        /*for(Case eachCase : caseList){
             if(oldMap != null && oldMap.get(eachCase.Id).OwnerId != newMap.get(eachCase.Id).OwnerId && oldMap.get(eachCase.Id).Assigned_Time__c == null && string.valueOf(oldMap.get(eachCase.Id).OwnerId).substring(0,3) == '00G' && string.valueOf(newMap.get(eachCase.Id).OwnerId).substring(0,3) == '005'){
                eachCase.Assigned_Time__c = system.now();
            }
            if(rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'FSG' && oldMap != null && oldMap.get(eachCase.Id).Status != newMap.get(eachCase.Id).Status && newMap.get(eachCase.Id).Status == 'Closed'){
                // Change Record Type to CSC Read Only
                eachCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSC_Read_Only').getRecordTypeId();
                eachCase.Case_Closure_Email__c = UserInfo.getUserEmail();
            }
            if(rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'CSC_Read_Only' && oldMap != null && oldMap.get(eachCase.Id).Status != newMap.get(eachCase.Id).Status && oldMap.get(eachCase.Id).Status == 'Closed' && newMap.get(eachCase.Id).Status != 'Closed'){
                // Change Record Type to FSG
                eachCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            }
        }*/
        
    }
    
    // Method to update record type based on Status Change
    public static void populateFSGRecordType(List<Case> caseList,Map<Id, Case>oldMap, Map<Id, Case>newMap, Map<Id,Schema.RecordTypeInfo> rtMap){
    	for(Case eachCase : caseList){
             if(oldMap != null && oldMap.get(eachCase.Id).OwnerId != newMap.get(eachCase.Id).OwnerId && oldMap.get(eachCase.Id).Assigned_Time__c == null && string.valueOf(oldMap.get(eachCase.Id).OwnerId).substring(0,3) == '00G' && string.valueOf(newMap.get(eachCase.Id).OwnerId).substring(0,3) == '005'){
                eachCase.Assigned_Time__c = system.now();
            }
            if(rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'FSG' && oldMap != null && oldMap.get(eachCase.Id).Status != newMap.get(eachCase.Id).Status && newMap.get(eachCase.Id).Status == 'Closed'){
                // Change Record Type to CSC Read Only
                eachCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSC_Read_Only').getRecordTypeId();
                eachCase.Case_Closure_Email__c = UserInfo.getUserEmail();
                eachCase.Closed_by__c = UserInfo.getFirstName()+' '+UserInfo.getLastName();
                eachCase.Is_Start_Date_Changed__c = false;
            }
            if(rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'CSC_Read_Only' && oldMap != null && oldMap.get(eachCase.Id).Status != newMap.get(eachCase.Id).Status && oldMap.get(eachCase.Id).Status == 'Closed' && newMap.get(eachCase.Id).Status != 'Closed'){
                // Change Record Type to FSG
                eachCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            }
        }
    }
    
    public static void autoPriorityOnCases(list<Case> newCaseList, Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap, Map<Id,Schema.RecordTypeInfo> rtMap){
        list<String> monTueHighPriority = new list<String>{'Banking Information','Cash Pay Card information','Employee Address','Pay Rate Change','Tax Changes','Time Approver Change','Work Site Address','Add Expense Earn Code,including Per Diem','Pre-Paid Expense Receipts','Pre-Paid Travel Back-Up','Submit New Expense','AT&E / VMS Log in Support','PTO/Sick Time Accrual','W2, Paystubs & Taxes','Add Holiday/Vacation/Sick time Code','Per Diem','PPE / Equipment Deduction','Tax Error'};
        list<String> allTimeHighPriority = new list<String>{'Start Date Error','Expense Discrepancy','Cash Advance','Cash Pay Error','Direct Deposit Error','Final Hours','Hours Discrepancy','Missing Paycheck','Pay Rate Error','Assets (TEK Only)','Rate Adjustment','Time Adjustment'};
        DateTime dateForCal; 
        for(Case caseObj : newCaseList){
            if(oldCaseMap == Null)
                dateForCal = System.now();
            if(oldCaseMap != Null)
                dateForCal = caseObj.CreatedDate;
            if(rtMap.get(caseObj.RecordTypeId).getDeveloperName() == 'FSG' && (oldCaseMap == null || (oldCaseMap != null && oldCaseMap.get(caseObj.Id).Sub_Type__c != newCaseMap.get(caseObj.Id).Sub_Type__c))){
                String dayOfWeek=dateForCal.format('EEEE');
                if(((dayOfWeek == 'Monday' || dayOfWeek == 'Tuesday') && monTueHighPriority.contains(caseObj.Sub_Type__c)) || allTimeHighPriority.contains(caseObj.Sub_Type__c)){
                    caseObj.Priority = 'High';
                }else{
                    caseObj.Priority = 'Normal';
                }
            }
        }
    }
    
    // S-176349 Create New Case Status History when onHold reason changed
    Public static void updateCaseStatusHistory(list<Case> newCaseList, Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap, Map<Id,Schema.RecordTypeInfo> rtMap){
        set<Id> caseIds = new set<Id>();
        list<Case_Status_History__c> InsertCSHList = new list<Case_Status_History__c>();
        list<Case_Status_History__c> updateCSFList = new list<Case_Status_History__c>();
        for(Case caseObj : newCaseList){
            if(rtMap.get(caseObj.RecordTypeId).getDeveloperName() == 'FSG' && oldCaseMap != NULL && oldCaseMap.get(caseObj.Id).Status == newCaseMap.get(caseObj.Id).Status && newCaseMap.get(caseObj.Id).Status == 'On Hold' && oldCaseMap.get(caseObj.Id).On_Hold_Reason__c != newCaseMap.get(caseObj.Id).On_Hold_Reason__c){
                caseIds.add(caseObj.ID);
            }
        }
        for(Case_Status_History__c cshObj : [select id,Status__c,Start_Time__c,End_Time__c,Case__c,Total_Live_Age__c from Case_Status_History__c Where Case__c IN: caseIds AND End_Time__c = NULL AND Start_Time__c != NULL AND Status__c = 'On Hold']){
            // Update recent Case Status History with Current Time.
            Case_Status_History__c cshUpdate = 	new Case_Status_History__c();
            cshUpdate.Id = cshObj.Id;
            cshUpdate.End_Time__c = System.now();
            updateCSFList.add(cshUpdate);
            
            // Create new Case History with new On Hold Reason.
            Case_Status_History__c cshInsert = 	new Case_Status_History__c();
            cshInsert.Case__c = cshObj.Case__c;
            cshInsert.Start_Time__c = System.now();
            cshInsert.Status__c = 'On Hold';
            cshInsert.On_Hold_Reason__c = newCaseMap.get(cshObj.Case__c).On_Hold_Reason__c;
            cshInsert.Carry_Forwarded_Age__c = cshObj.Total_Live_Age__c;
            InsertCSHList.add(cshInsert);
        }
        if(!updateCSFList.isEmpty()){
            update updateCSFList;
        }
        if(!InsertCSHList.isEmpty()){
            insert InsertCSHList;
        }
    }
    
    // S-178309 Auto close Parent cases
    public static void autoCloseParentCases(list<Case> newCaseList, Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap, Map<Id,Schema.RecordTypeInfo> rtMap){
        set<Id> parentCaseIds = new set<Id>();
        set<Id> dupParentIds = new set<Id>();
        set<Id> closedParentIds = new set<Id>();
        list<Case> parentCaseUpdate = new list<Case>();
        for(Case caseObj : newCaseList){
            if(rtMap.get(caseObj.RecordTypeId).getDeveloperName() == 'CSC_Read_Only' && caseObj.ParentId != NULL && oldCaseMap != NULL && oldCaseMap.get(caseObj.Id).Status != newCaseMap.get(caseObj.Id).Status && newCaseMap.get(caseObj.Id).Status == 'Closed'){
                parentCaseIds.add(caseObj.ParentId);
            }
        }
        system.debug('parentCaseIds=====>>>>>>'+parentCaseIds);
        if(!parentCaseIds.isEmpty()){
            for(Case caseObj : [select id,ParentID,Status from Case where ParentID IN : parentCaseIds AND Status != 'Closed']){
            	dupParentIds.add(caseObj.ParentId);
            }
            system.debug('dupParentIds=====>>>>>>'+dupParentIds);
            for(Id idObj : parentCaseIds){
                if(!dupParentIds.contains(idObj))
                    closedParentIds.add(idObj);
            }
            system.debug('closedParentIds=====>>>>>'+closedParentIds);
            if(!closedParentIds.isEmpty()){
                for(ID idObj : closedParentIds){
                    Case newCaseObj = new Case();
                    newCaseObj.Id = idObj;newCaseObj.Status = 'Closed';newCaseObj.All_Child_Cases_Closed__c = TRUE;newCaseObj.Case_Resolution_Comment__c = 'Auto closed process triggered, due to all related child cases been closed.';newCaseObj.Closed_Reason__c = 'Case is Resolved';newCaseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSC_Read_Only').getRecordTypeId();parentCaseUpdate.add(newCaseObj);
                }
            }
            if(!parentCaseUpdate.isEmpty())
                update parentCaseUpdate;
        }
    }
    
    //Update parent case status
    public static void updateParentStatus(list<Case> newCaseList, Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap, Map<Id,Schema.RecordTypeInfo> rtMap){
        set<Id> parentCaseIds = new set<Id>();
        list<Case> updateCases = new list<Case>();
        for(Case caseObj : newCaseList){
            if(caseObj.ParentId != NULL && caseObj.Parent_Status__c == 'New' && rtMap.get(caseObj.RecordTypeId).getDeveloperName() == 'FSG' && caseObj.Client_Account_Name__c == NULL && oldCaseMap != NULL && oldCaseMap.get(caseObj.Id).Status != newCaseMap.get(caseObj.Id).Status && newCaseMap.get(caseObj.Id).Status != 'New'){
                parentCaseIds.add(caseObj.ParentId);
            }
        }
        if(!parentCaseIds.isEmpty()){
            for(ID idObj : parentCaseIds){
                Case newCase = new Case();
                newCase.Id = idObj;
                newCase.status = 'In Progress';
                updateCases.add(newCase);
            }
        }
        if(!updateCases.isEmpty()){
            update updateCases;
        }
    }	
    
    public static void processMilestone(list<Case> newCaseList, Map<Id,Case> oldCaseMap){
        // Variables
        Map<Id,String> caseMileStoneMap = new Map<Id,String>();
        Set<String> newCaseMileStones = new Set<String>();
        // Map for Case status and Milestone Name
        Map<String,String> caseStatusMap = new Map<String,String>();
        caseStatusMap.put('New', 'Milestone: New');
        caseStatusMap.put('Assigned', 'Milestone: Assigned');
        caseStatusMap.put('In Progress', 'Milestone: In Progress');
        caseStatusMap.put('On Hold', 'Milestone: On Hold');
        caseStatusMap.put('Escalated', 'Milestone: Escalated');
        caseStatusMap.put('Under Review', 'Milestone: Under Review');
        
        DateTime currentDateTime = System.now();
        for(Case caseObj : newCaseList){
            if(caseObj.Status != 'Closed' && caseObj.Status != oldCaseMap.get(caseObj.Id).Status){
                caseMileStoneMap.put(caseObj.Id, caseStatusMap.get(oldCaseMap.get(caseObj.Id).Status));
                newCaseMileStones.add(caseStatusMap.get(caseObj.status));
            }
        }
        if(!caseMileStoneMap.isEmpty()){
            CSC_MilestoneUtils.completeMileStones(caseMileStoneMap,currentDateTime,newCaseMileStones);
        }
    }
    
    public static void processClosedMilestone(list<Case> newCaseList, Map<Id,Case> oldCaseMap){
        // Variables
        Map<Id,String> caseMileStoneMap = new Map<Id,String>();
        
        // Map for Case status and Milestone Name
        Map<String,String> caseStatusMap = new Map<String,String>();
        caseStatusMap.put('New', 'Milestone: New');
        caseStatusMap.put('Assigned', 'Milestone: Assigned');
        caseStatusMap.put('In Progress', 'Milestone: In Progress');
        caseStatusMap.put('On Hold', 'Milestone: On Hold');
        caseStatusMap.put('Escalated', 'Milestone: Escalated');
        caseStatusMap.put('Under Review', 'Milestone: Under Review');
        
        DateTime currentDateTime = System.now();
        for(Case caseObj : newCaseList){
            if(caseObj.Status == 'Closed' && caseObj.Status != oldCaseMap.get(caseObj.Id).Status){
                caseMileStoneMap.put(caseObj.Id, caseStatusMap.get(oldCaseMap.get(caseObj.Id).Status));
            }
        }
        if(!caseMileStoneMap.isEmpty()){
            CSC_MilestoneUtils.completeClosedMileStones(caseMileStoneMap,currentDateTime);
        }
    }
    public static void changeStartDate(List<Case> newCaseList, Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap,Map<Id,Schema.RecordTypeInfo> rtMap){
        for(Case caseObjs : newCaseList){
            if(UserInfo.getProfileId().substring(0, 15) != system.label.Profile_System_Integration && UserInfo.getUserId().substring(0, 18) != system.label.AutomatedProcessId){
                if(oldCaseMap.get(caseObjs.Id).VSB_Start_Date__c != newCaseMap.get(caseObjs.Id).VSB_Start_Date__c ){               
                    SubmittalStatusController.onStartDateChange(caseObjs.VSB_Start_Date__c, caseObjs.Id,null);
				}
       	    }
     	}   
	}
    //S-199690
    public static void reopenCaseStatus(List<Case> newCaseList, Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap,Map<Id,Schema.RecordTypeInfo> rtMap){
        for(Case caseObjs :newCaseList){
            if(caseObjs.Status == 'Closed'){
                if(oldCaseMap.get(caseObjs.Id).VSB_Start_Date__c != newCaseMap.get(caseObjs.Id).VSB_Start_Date__c || oldCaseMap.get(caseObjs.Id).Case_Issue_Description__c != newCaseMap.get(caseObjs.Id).Case_Issue_Description__c ){
                	caseObjs.Status = 'Under Review';
                    caseObjs.Under_Review_Comments__c = 'Talent start date or case description has been updated';
                }
            }
        }        
    }
    
    //S-242776
    public static void processE2CCases(list<Case> newCaseList, Map<Id,Schema.RecordTypeInfo> rtMap){
        list<String> suppliedEmailList = new list<String>();
        list<String> profileList = new list<String>{'Single Desk 1','System Administrator','IS Admin'};
        list<String> internalEmails = new list<String>();
        for(Case eachCase : newCaseList){
            system.debug('Recordtype======>>>>>>>'+rtMap.get(eachCase.RecordTypeId).getDeveloperName());
            system.debug('SuppliedEmail=====>>>>>>>'+eachCase.SuppliedEmail);
            if(rtMap != null && rtMap.get(eachCase.RecordTypeId).getDeveloperName() == 'CSC_Email_to_Case' && eachCase.SuppliedEmail != ''){
                suppliedEmailList.add(eachCase.SuppliedEmail);
            }
        }
        system.debug('suppliedEmailList======>>>>>>'+suppliedEmailList);
        if(!suppliedEmailList.isEmpty()){
            for(User userObj : [select id,Name,Email from User where Email IN: suppliedEmailList AND (Opco__c = 'TEK' OR Opco__c = 'ONS') AND Profile.Name IN:profileList]){
                internalEmails.add(userObj.Email);
            }
        }
        system.debug('internalEmails======>>>>>>'+internalEmails);
        if(!internalEmails.isEmpty()){
            for(Case eachCase : newCaseList){
                if(internalEmails.contains(eachCase.SuppliedEmail)){
                    eachCase.Is_E2C_from_Internal__c = true;
                }
            }
        }
    }
}