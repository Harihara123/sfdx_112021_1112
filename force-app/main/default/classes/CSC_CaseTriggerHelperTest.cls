@isTest
private class CSC_CaseTriggerHelperTest {
static testmethod void test() {
        
    	CSC_Holidays__c setting = new CSC_Holidays__c();
        setting.Name = 'hol1';
        setting.Date__c = system.today();
        insert setting;
    	
    	CSC_General_Setup__c setting2 = new CSC_General_Setup__c();
        setting2.Name = 'Lock Status';
        setting2.Lock_Status_over_Weekend_and_Holidays__c = false;
        insert setting2;
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Contact testContactWOUSer = new Contact(Title='Java Applications Developer', LastName='manish2', accountId=acc.id);
            insert testContactWOUSer;
            
            Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact.Id,
                    timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
           
            insert Talentusr;
                
            Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
            insert testContact222;
            
            //Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact222.Id,Region__c = 'TEK Central',OPCO__c = 'ONS',
                    timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
           
            insert Talentusr222;
            Test.startTest();
            // Inserting a new CSC case record
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     Contact=testContact, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase; 
                
             Case testCase2 = new Case(subject = 'CSC Test222', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContactWoUser.Id,
                                     ownerId = thisUser.Id,
                                     //Region_Talent__c = 'TEK Central',
                                     //Contact=testContact222, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase2; 
            
            	Map<Id,case> oldMap = new Map<Id,Case>();
                Map<Id,Case> newMap = new Map<Id,Case>();
                list<Case> newCase = new list<Case>();
                testCase.Status = 'Assigned';
            	testCase.OwnerId = thisUser.Id;
                update testCase;
            
                testCase.Status = 'On Hold';
            	testCase.On_Hold_Reason__c = 'Future Dated Change';
            	testCase.Future_Date__c = System.today()+4;
                update testCase;
            	testCase.On_Hold_Reason__c = 'Pending InfoPath Approval';
            	update testCase;
				Test.stopTest();
            }
    	
        
    }
    
    static testmethod void unitTest() {
        Test.startTest();
    	CSC_Holidays__c setting = new CSC_Holidays__c();
        setting.Name = 'hol1';
        setting.Date__c = system.today();
        insert setting;
    	
    	CSC_General_Setup__c setting2 = new CSC_General_Setup__c();
        setting2.Name = 'Lock Status';
        setting2.Lock_Status_over_Weekend_and_Holidays__c = false;
        insert setting2;
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Contact testContactWOUSer = new Contact(Title='Java Applications Developer', LastName='manish2', accountId=acc.id);
            insert testContactWOUSer;
            
            Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact.Id,
                    timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
           
            insert Talentusr;
                
            Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
            insert testContact222;
            
            //Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact222.Id,Region__c = 'TEK Central',OPCO__c = 'ONS',
                    timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
           
            insert Talentusr222;
            
            // Inserting a new CSC case record
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     Contact=testContact, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase; 
                
             Case testCase2 = new Case(subject = 'CSC Test222', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContactWoUser.Id,
                                     ownerId = thisUser.Id,
                                     parentId = testCase.Id,
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase2; 
            
            	Map<Id,case> oldMap = new Map<Id,Case>();
                Map<Id,Case> newMap = new Map<Id,Case>();
                list<Case> newCase = new list<Case>();
                testCase2.Status = 'Assigned';
            	testCase2.OwnerId = thisUser.Id;
                update testCase2;
            	testCase2.Status = 'closed';
            	testCase2.Closed_Reason__c = 'Case is Resolved';
            	testCase2.Case_Resolution_Comment__c = 'Test Case Feature Unit Testing';
                try{
                    update testCase2;
                }catch(exception ex){
                    system.debug('ex=====>>>>>'+ex);
                }
            	
            	system.debug('testCase2=====>>>>>'+testCase2.RecordType.Name);
            	
			}
    	
        Test.stopTest();
    }
    
    static testmethod void unitTest2() {
        Test.startTest();
    	CSC_Holidays__c setting = new CSC_Holidays__c();
        setting.Name = 'hol1';
        setting.Date__c = system.today();
        insert setting;
    	
    	CSC_General_Setup__c setting2 = new CSC_General_Setup__c();
        setting2.Name = 'Lock Status';
        setting2.Lock_Status_over_Weekend_and_Holidays__c = true;
        insert setting2;
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Contact testContactWOUSer = new Contact(Title='Java Applications Developer', LastName='manish2', accountId=acc.id);
            insert testContactWOUSer;
            
            Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact.Id,
                    timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
           
            insert Talentusr;
                
            Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
            insert testContact222;
            
            //Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = testContact222.Id,Region__c = 'TEK Central',OPCO__c = 'ONS',
                    timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
           
            insert Talentusr222;
            
            // Inserting a new CSC case record
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     Contact=testContact, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase; 
                
             Case testCase2 = new Case(subject = 'CSC Test222', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContactWoUser.Id,
                                     ownerId = thisUser.Id,
                                     //Region_Talent__c = 'TEK Central',
                                     //Contact=testContact222, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase2; 
            }
    	
        Test.stopTest();
    }
}