public with Sharing class CSC_CloseCase {

    @AuraEnabled
    public static string closeTheCase(String caseId, String Comments, String Status, String closedReason) {
        String result = 'success';
        Case caseObj = new case();
        caseObj.Id = caseId;
        caseObj.Status = Status;
        caseObj.Closed_Reason__c = closedReason;
        caseObj.Case_Resolution_Comment__c = Comments;
        try{
            system.debug('------caseObj--------'+caseObj);
            update caseObj;
            
        }catch(exception e){
            system.debug('------e.getMessage()--------'+e.getMessage());
            return e.getMessage();
        }
        return result;
    }
    
    @AuraEnabled
    public static List<case> getCaseStatus(String caseId) {
        List<case> lstCase = [select Id, Status,OwnerId,All_Child_Cases_Closed__c,Client_Account_Name__c from case where Id = :caseId];
        return lstCase;
    }
    
    @AuraEnabled //get case Closed_Reason__c Picklist Values
    public static Map<String, String> getClosedReason(){
        Map<String, String> options = new Map<String, String>();
        //get Account Industry Field Describe
        Schema.DescribeFieldResult fieldResult = Case.Closed_Reason__c.getDescribe();
        //get Account Industry Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
}