@isTest
private class CSC_CloseCaseTest {
    
    static testmethod void test() {
        Test.startTest();
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
        // Inserting test users    
        List<User> testUsers = new List<User>();
        
        User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
        User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
        
        testUsers.add(testUsr1);
        testUsers.add(testUsr2);
        insert testUsers; 
        
        User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
        
        Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
        insert acc;
        
        Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
        insert testContact;
        
        Id p = [select id from profile where name='Aerotek Community User'].id;
        User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = testContact.Id,
                timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
       
        insert Talentusr;
        
        // Inserting a new CSC case record
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
        system.debug('-----'+recTypeId); 
        Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                 Type ='Payroll', 
                                 Sub_Type__c='Tax Error',
                                 AccountId = acc.id,
                                 ContactId = testContact.Id,
                                 ownerId = thisUser.Id,
                                 Contact=testContact, 
                                 Case_Issue_Description__c ='test Case Issue Description', 
                                 recordTypeId=recTypeId );       
        insert testCase;    
        
        
        system.debug('-------------------'+TriggerState.isActive('caseTrigger'));
            CSC_CloseCase.closeTheCase(testCase.Id, 'this is a test comments. this is a test comments', 'Closed', 'Case is Resolved');
            CSC_CloseCase.getCaseStatus(testCase.Id);
            CSC_CloseCase.getClosedReason();
            
        }
        Test.stopTest();
    }
}