public class CSC_ContentDocumentTriggerHelper {
    public static void beforeDelete(list<ContentDocument> contentDocList){
        system.debug('contentDocList====>>>>>>'+contentDocList);
        set<Id> conentDocId = new set<Id>();
        set<Id> caseIds = new set<Id>();
        Map<Id,Id> contentDocMap = new Map<Id,Id>();
        Map<Id,Case> caseStatusMap = new Map<Id,Case>();
        for(ContentDocument cdObj : contentDocList){
            conentDocId.add(cdObj.Id);
        }
        for(ContentDocumentLink cdlObj : [select LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN:conentDocId]){
            string cdlentityId = cdlObj.LinkedEntityId;
            if(cdlentityId != '' && cdlentityId.subString(0,3) == '500'){
                caseIds.add(cdlObj.LinkedEntityId);
                contentDocMap.put(cdlObj.ContentDocumentId, cdlObj.LinkedEntityId);
            }
        }
        system.debug('caseIds=====>>>>>>'+caseIds);
        system.debug('contentDocMap=====>>>>>>'+contentDocMap);
        if(!caseIds.isEmpty()){
            for(Case caseObj : [select id,Status from Case where Id IN: caseIds AND Status = 'Closed' AND RecordType.Name = 'CSC Read Only']){
                caseStatusMap.put(caseObj.Id, caseObj);
            }
        }
        system.debug('caseStatusMap=====>>>>>'+caseStatusMap);
        if(!caseStatusMap.isEmpty()){
            for(ContentDocument cdObj : contentDocList){
                Id conDocLinkEntityId = contentDocMap.get(cdObj.Id);
                if(caseStatusMap.containsKey(conDocLinkEntityId)){
                    cdObj.addError('Files attached on a closed csc case cannot be deleted.');
                }
            }
        }
    }
}