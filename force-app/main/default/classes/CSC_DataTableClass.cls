public class CSC_DataTableClass {
	@AuraEnabled
    Public static List<Contact> getTalentList(String accountId){
        //get all contact list
        List<Contact> talentList = new List<Contact>();
        if(accountId !=null){
        	List<talent_work_history__c> talentWorkHistoryList = [select talent__c from talent_work_history__c where client_account_id__c = :accountId AND (Current_assignment_Formula__c = TRUE OR (Current_assignment_Formula__c = FALSE AND End_Date__c = LAST_N_DAYS : 90)) ];
            set<Id> setAccountIds = new set<Id>();
            for(talent_work_history__c twh : talentWorkHistoryList){
                setAccountIds.add(twh.talent__c);
            }
            talentList = [SELECT id,Email,FirstName,LastName,Account.Name,Peoplesoft_ID__c,Candidate_Status__c,Title, Contact_Location__c,Phone FROM Contact where accountId in :setAccountIds limit 2000];
    	}
        return talentList;
    }
}