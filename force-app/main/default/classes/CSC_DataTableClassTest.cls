@isTest
public class CSC_DataTableClassTest {
    private static testMethod void unitTest(){
        Account a = new Account(Name='Allegis');
        insert a;
        
        Contact c = new Contact(LastName = 'Alex', AccountId = a.id);
        insert c;
        
        Talent_Work_History__c twh = new Talent_Work_History__c();
        twh.Start_Date__c = Date.today();
        twh.End_Date__c = Date.today().addDays(60);
        twh.Talent__c = a.Id;
        twh.client_account_id__c = a.Id;
        insert twh;
        
        CSC_DataTableClass.getTalentList(a.Id);
    }
}