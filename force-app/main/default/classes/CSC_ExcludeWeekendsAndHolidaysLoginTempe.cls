/*
// Run this schedular Mon to Friday at 12AM MST
// 
// 
//
// */
global class CSC_ExcludeWeekendsAndHolidaysLoginTempe implements Schedulable{
    
     global void execute(SchedulableContext SC) {
         
         // check today is working day or not? if Yes, then run the below logic
         boolean runLogic = true;
         if(System.now().format('EEEE') == 'Saturday' || System.now().format('EEEE') == 'Sunday'){
             runLogic = false;
         }
         List<CSC_Holidays__c> listOfHolidays = CSC_Holidays__c.getall().values();
         for(CSC_Holidays__c hol : listOfHolidays){
             if(hol.Date__c == system.today()){
                 runLogic = false;
                 break;
             }
         }
         if(test.isRunningTest()){
             runLogic = true;
         }
         if(runLogic){
             try{
                 list<Case> lstcases = [select Id, (select id, case__c, End_Time__c,Start_Time__c, Login__c,Logout__c,Carry_Forwarded_Age__c,Total_Live_Age__c,Status__c,On_Hold_Reason__c from Case_Status_Histories__r where Status__c != 'Closed' AND Logout__c = true order by End_Time__c DESC limit 1) from case where recordType.DeveloperName = 'FSG' AND Center_Name__c = 'Tempe' AND Status != 'Closed'];
                 
                 //list<Case_Status_History__c> lstCsh = [select id, case__c, End_Time__c,Start_Time__c, Login__c,Logout__c,Carry_Forwarded_Age__c,Total_Live_Age__c,Status__c from Case_Status_History__c where case__r.Region_Talent__c != 'TEK Central' AND Status__c != 'Closed' AND Logout__c = true];
                 list<Case_Status_History__c> lstCshToInsert = new list<Case_Status_History__c>();
                 for(Case eachCase : lstcases){
                     for(Case_Status_History__c eachCSH : eachCase.Case_Status_Histories__r){
                         Case_Status_History__c newCsh = new Case_Status_History__c();
                         newCsh.Start_Time__c = system.now();
                         newCsh.Login__c = true;
                         newCsh.Status__c = eachCSH.Status__c;
                         if(eachCSH.Status__c == 'On Hold'){
                             newCsh.On_Hold_Reason__c = eachCSH.On_Hold_Reason__c;
                         }
                         newCsh.Case__c = eachCSH.case__c;
                         newCsh.Carry_Forwarded_Age__c = eachCSH.Total_Live_Age__c;
                         lstCshToInsert.add(newCsh);
                     }
                 }
                 if(!lstCshToInsert.isEmpty()){
                     database.insert(lstCshToInsert,false);
                 }
            }catch(Exception e){
                 CSC_Scheduler_Error_Log__c err = new CSC_Scheduler_Error_Log__c(Class_Name__c = 'Login Tempe',Error__c = e.getMessage());
                 insert err;
             }
         }
    }
    
}