/*
// Run this schedular Mon to Friday at 11:59PM MST
// 
// 
//
// */
global class CSC_ExcludeWeekendsAndHolidaysLogoutTemp implements Schedulable{
    
    global void execute(SchedulableContext SC) {
        // check today is working day or not? if Yes, then run the below logic
        boolean runLogic = true;
        boolean runWeekendLogic = true;
        list<Case> caselist = new list<Case>();
        list<Case_Status_History__c> updateCSH = new list<Case_Status_History__c>();
        list<Case> parentCaseList = new list<Case>();
        if(system.Now().addHours(-6).format('EEEE') == 'Saturday' || System.now().format('EEEE') == 'Sunday'){
            runLogic = false;
        }
        List<CSC_Holidays__c> listOfHolidays = CSC_Holidays__c.getall().values();
        for(CSC_Holidays__c hol : listOfHolidays){
            if(hol.Date__c == system.Now().addHours(-6).date()){
                runLogic = false;
                break;
            }
        }
        
        if(test.isRunningTest()){
            runLogic = true;
        }
        
        try{
            list<Case_Status_History__c> lstCsh = new list<Case_Status_History__c>();
            if(runLogic){
                lstCsh = [select id, case__c, End_Time__c,Logout__c from Case_Status_History__c where case__r.recordType.DeveloperName = 'FSG' AND case__r.Center_Name__c = 'Tempe' AND Status__c != 'Closed' AND End_Time__c = NULL];
            }else{
                lstCsh = [select id, case__c, End_Time__c,Logout__c from Case_Status_History__c where case__r.recordType.DeveloperName = 'FSG' AND case__r.Center_Name__c = 'Tempe' AND Status__c != 'Closed' AND End_Time__c = NULL AND Created_on_Weekend_Holiday__c = TRUE];
            }
            list<Case_Status_History__c> lstCshToUpdate = new list<Case_Status_History__c>();
            for(Case_Status_History__c eachCSH : lstCsh){
                eachCSH.End_Time__c = system.now();
                eachCSH.Logout__c = true;
                lstCshToUpdate.add(eachCSH);
            }
            if(!lstCshToUpdate.isEmpty()){
                database.update(lstCshToUpdate,false);
            }
        }catch(Exception e){
            CSC_Scheduler_Error_Log__c err = new CSC_Scheduler_Error_Log__c(Class_Name__c = 'Logout Tempe',Error__c = e.getMessage());
            insert err;
        }
        
    }
    
}