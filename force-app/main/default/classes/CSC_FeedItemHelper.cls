public class CSC_FeedItemHelper {
    public static void postFeedItemOnChild(list<FeedItem> feedItemsList){
        set<Id> caseIds = new set<Id>();
        Map<ID,FeedItem> parentCaseFeedMap = new Map<ID,FeedItem>();
        list<FeedItem> insertFeedItems = new list<FeedItem>();
        for(FeedItem item : feedItemsList){
            if(item.ParentId != null && String.valueOf(item.ParentId).startsWith('500') && item.type == 'TextPost' && item.Body != ''){
                caseIds.add(item.ParentId);
                parentCaseFeedMap.put(item.ParentId, item);
            }
        }
        if(!caseIds.isEmpty()){
            for(Case caseObj : [select id,ParentId from Case where ParentId IN: parentCaseFeedMap.keySet()]){
                if(parentCaseFeedMap.containsKey(caseObj.parentId)){
                    //insert post
                    FeedItem post = new FeedItem();
                    string newBody = parentCaseFeedMap.get(caseObj.parentId).Body;
                    post.Body = newBody;
                    post.IsRichText = TRUE;
                    post.ParentId = caseObj.Id;
                    insertFeedItems.add(post);
                    system.debug('newBody======>>>>>'+newBody);
                    system.debug('post.Body=======>>>>>>>'+post.Body);
                }
            }
            if(!insertFeedItems.isEmpty())
                insert insertFeedItems;
        }
    }
}