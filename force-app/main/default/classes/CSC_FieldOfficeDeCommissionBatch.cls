global class CSC_FieldOfficeDeCommissionBatch implements Database.Batchable<Sobject> ,Database.stateful, Schedulable{
    global void execute(SchedulableContext SC) {
        CSC_FieldOfficeDeCommissionBatch batchable = new CSC_FieldOfficeDeCommissionBatch();
        Database.executeBatch(batchable,10);
    }
    global database.QueryLocator start(Database.BatchableContext bc){
        string query = 'select id,Talent_s_Office_Code__c from CSC_Region_Alignment_Mapping__c where createdDate = LAST_N_DAYS : 1';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, list<CSC_Region_Alignment_Mapping__c> scope){
        set<String> officeCodes = new set<String>();
        set<Id> userOrGroupIdSet = new set<Id>();
        set<Id> groupIdSet = new set<Id>();
        list<String> FieldOfficePGPS = System.label.CSC_Field_Office_PG_PS.split(',');
        id permissionId = id.valueof(FieldOfficePGPS[1]);
        for(CSC_Region_Alignment_Mapping__c aliMap : scope){
            officeCodes.add(aliMap.Talent_s_Office_Code__c);
        }
        for(User uobj : [select id,Office_Code__c from User where Office_Code__c IN: officeCodes]){
            userOrGroupIdSet.add(uobj.Id);
        }
        for(Group gObj : [SELECT Id, DeveloperName from Group Where DeveloperName='Field_Office_Quick_Close_PG' OR DeveloperName = 'Field_Office_Started_Cases_PG']){
            groupIdSet.add(gObj.Id);
        }
        if(!userOrGroupIdSet.isEmpty()){
            list<GroupMember> listGM = [select id from GroupMember where UserOrGroupID in :userOrGroupIdSet and GroupId  in :groupIdSet ];
            if(!listGM.isEmpty()){
                delete listGM;
            }
            list<PermissionSetAssignment> listPSA = [select id from PermissionSetAssignment where PermissionSetId =: permissionId AND AssigneeId IN: userOrGroupIdSet];
            if(!listPSA.isEmpty()){
                delete listPSA;
            }
        }
    }
    global void finish(Database.BatchableContext bc){
        
    }
}