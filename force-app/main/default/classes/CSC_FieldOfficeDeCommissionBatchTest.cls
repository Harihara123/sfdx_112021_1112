@isTest
public class CSC_FieldOfficeDeCommissionBatchTest {
	private static testMethod void unitTest(){
        Test.startTest();
        // Region Alignment Mapping 
        CSC_Region_Alignment_Mapping__c rMap = new CSC_Region_Alignment_Mapping__c();
        rMap.Talent_s_Office_Code__c = '00002';
        rMap.Center_Name__c = 'Tempe';
        rMap.Talent_s_OPCO__c = 'ONS';
        rMap.Location_Description__c = 'Austin';
        insert rMap;
        
        CSC_FieldOfficeDeCommissionBatch schedulable = new CSC_FieldOfficeDeCommissionBatch();
        String sch2 = '0 0 23 * * ? *';
        String jobID2 = system.schedule('CSC Decommission', sch2, schedulable);
        Test.stopTest();
    }
    private static testMethod void unitTestForAutoClose(){
        
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                     emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                     localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                     timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                     emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                     localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                     timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Contact testContactWOUSer = new Contact(Title='Java Applications Developer', LastName='manish2', accountId=acc.id);
            insert testContactWOUSer;
            
            Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                                      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                      localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                      ContactId = testContact.Id,
                                      timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
            
            insert Talentusr;
            
            Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
            insert testContact222;
            
            //Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                                         emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                         localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                         ContactId = testContact222.Id,Region__c = 'TEK Central',OPCO__c = 'ONS',
                                         timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
            
            insert Talentusr222;
            
            // Inserting a new CSC case record
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     Contact=testContact,
                                     Non_CSC_started_case__c = true,
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase; 
            Datetime yesterday = Datetime.now().addDays(-100);
        	Test.setCreatedDate(testCase.Id, yesterday);  
            Test.startTest();
            CSC_FieldOfficeStartedAutoCloseBatch sch4 = new CSC_FieldOfficeStartedAutoCloseBatch();
            string cron4 =  '0 0 0 1 * ? *';
            System.schedule('CSC Field Office Auto Test Close', cron4, sch4);
            Test.stopTest();
        }
    }
}