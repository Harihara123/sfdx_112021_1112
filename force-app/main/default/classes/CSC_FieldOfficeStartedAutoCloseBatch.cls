global class CSC_FieldOfficeStartedAutoCloseBatch implements Database.Batchable<Sobject> ,Database.stateful, Schedulable{
    global void execute(SchedulableContext SC) {
        CSC_FieldOfficeStartedAutoCloseBatch batchable = new CSC_FieldOfficeStartedAutoCloseBatch();
        Database.executeBatch(batchable,10);
    }
	global database.Querylocator start(Database.BatchableContext bc){
        //CreatedDate < LAST_N_DAYS: 1 AND 
        Integer noOfDays = Integer.valueOF(System.Label.CSC_Field_Office_Auto_Close_Days);
        string query = 'select id,Status,VSB_Quick_Close__c,Case_Resolution_Comment__c,Closed_Reason__c from case where CreatedDate < LAST_N_DAYS: '+noOfDays+' AND Status != \'Closed\' AND RecordType.Name = \'Collocated Services Center\' AND Non_CSC_started_case__c = true';
    	return Database.getQueryLocator(query);  
    }
    global void execute(Database.BatchableContext bc , list<Case> scope){
    	list<Case> updateCase = new list<Case>();
        for(Case caseObj : scope){
            caseObj.Status = 'Closed';
            caseObj.VSB_Quick_Close__c = True;
            caseObj.Case_Resolution_Comment__c = 'Auto Case closure initiated due to time-out';
			caseObj.Closed_Reason__c = 'Lack of Response';
            updateCase.add(caseObj);
        }
        if(!updateCase.isEmpty()){
            update updateCase;
        }
    }
    global void finish(Database.BatchableContext bc){
    
    }
}