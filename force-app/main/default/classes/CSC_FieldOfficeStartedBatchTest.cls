@isTest
public class CSC_FieldOfficeStartedBatchTest {
    private static testMethod void unitTestProvision(){
        try{
            Test.startTest();
            // Region Alignment Mapping 
            CSC_Region_Alignment_Mapping__c rMap = new CSC_Region_Alignment_Mapping__c();
            rMap.Talent_s_Office_Code__c = '00034';
            rMap.Center_Name__c = 'Jacksonville';
            rMap.Talent_s_OPCO__c = 'TEK';
            rMap.Location_Description__c = 'Austin';
            insert rMap;
            
            CSC_Field_Office_Started_Batch sch1 = new CSC_Field_Office_Started_Batch();
            string cron1 = '0 0 4 * * ? *';
            System.schedule('CSC Field Office Provision Test', cron1, sch1);
            
            Test.stopTest();
        }Catch(exception ex){
            
        }
    }
}