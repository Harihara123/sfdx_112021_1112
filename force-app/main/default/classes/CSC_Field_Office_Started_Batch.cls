global class CSC_Field_Office_Started_Batch implements Database.Batchable<Sobject> ,Database.stateful, Schedulable{
    global void execute(SchedulableContext SC) {
        CSC_Field_Office_Started_Batch batchable = new CSC_Field_Office_Started_Batch();
        Database.executeBatch(batchable,10);
    }
    global database.Querylocator start(Database.BatchableContext bc){
        String strQuery = '';
        list<String> CLSOfficeCodes = new list<String>();
        for(CSC_Region_Alignment_Mapping__c alignMap : [select id,Talent_s_Office_Code__c from CSC_Region_Alignment_Mapping__c]){
            CLSOfficeCodes.add(alignMap.Talent_s_Office_Code__c);
        }
        strQuery = '';
        strQuery += 'select Id,ODS_Jobcode__c from User where Office_Code__c != NULL AND OPCO__c = \'ONS\' AND Office_Code__c NOT IN :CLSOfficeCodes AND IsActive = true AND UserType = \'Standard\'';
        if(Test.isRunningTest()){
            strQuery += ' limit 10';
        }
        system.debug('strQuery=====>>>>>>'+strQuery);
        return Database.getQueryLocator(strQuery);   
    }
    global void execute(Database.BatchableContext bc , list<User> scope){
        List<GroupMember> GroupMemberlist = new List<GroupMember>();
        List<PermissionSetAssignment> PermissionSetList = new list<PermissionSetAssignment>();
        list<String> FieldOfficePGPS = System.label.CSC_Field_Office_PG_PS.split(',');
        list<String> FieldOfficeJobCodes = System.label.CSC_Field_Office_Job_Codes.split(',');
        try{
            for(User obj : scope){
                // Group Mamber
                GroupMember GM = new GroupMember();
                GM.GroupId = id.valueof(FieldOfficePGPS[0]);
                GM.UserOrGroupId = obj.Id;
                GroupMemberlist.add(GM);
                
                // Group Member for Quick Close Field Offices
                if(FieldOfficeJobCodes.contains(obj.ODS_Jobcode__c)){
                    GroupMember QGM = new GroupMember();
                    QGM.GroupId = id.valueof(FieldOfficePGPS[2]);
                    QGM.UserOrGroupId = obj.Id;
                    GroupMemberlist.add(QGM);
                }
                
                // Permission Set
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.PermissionSetId = id.valueof(FieldOfficePGPS[1]);
                psa.AssigneeId = obj.Id;
                PermissionSetList.add(psa);
            }
            // Adding Users to Group & Permission set
            if(!GroupMemberlist.isEmpty()){
                database.insert(GroupMemberlist,false);
            }
            if(!PermissionSetList.isEmpty()){
                database.insert(PermissionSetList,false);
            }
        }catch(Exception ex){
            system.debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());
        }
    }
    global void finish(Database.BatchableContext bc){
    
    }
}