public class CSC_MilestoneUtils {
    public static void completeClosedMileStones(Map<Id,String> caseMileStoneMap, DateTime complDate){
        list<CaseMileStone> cmsToUpdate = [select Id, completionDate
                                           from CaseMilestone cm
                                           where caseId in :caseMileStoneMap.keySet() and (cm.MilestoneType.Name IN :caseMileStoneMap.values() OR cm.MilestoneType.Name = 'CSC Milestone Overview')
                                           and completionDate = null];
        if (!cmsToUpdate.isEmpty()){
            for (CaseMilestone cm : cmsToUpdate){
                cm.completionDate = complDate;
            }
            try{
                update cmsToUpdate;
            }Catch(Exception ex){
                
            }
        }
    }
    
    public static void completeMileStones(Map<Id,String> caseMileStoneMap, DateTime complDate, Set<String> newCaseMileStones){
        list<CaseMileStone> cmsToUpdate = [select Id, completionDate, IsCompleted, MilestoneType.Name
                                           from CaseMilestone cm
                                           where caseId in :caseMileStoneMap.keySet() and (cm.MilestoneType.Name IN :caseMileStoneMap.values() OR cm.MilestoneType.Name IN :newCaseMileStones OR cm.MilestoneType.Name = 'CSC Milestone Overview')];
        if (!cmsToUpdate.isEmpty()){
            for (CaseMilestone cm : cmsToUpdate){
                if(cm.completionDate != NULL && cm.IsCompleted == TRUE && (newCaseMileStones.contains(cm.MilestoneType.Name) || cm.MilestoneType.Name == 'CSC Milestone Overview')){
                    cm.completionDate = NULL;
                }else if(!newCaseMileStones.contains(cm.MilestoneType.Name) && cm.MilestoneType.Name != 'CSC Milestone Overview')
                    cm.completionDate = complDate;
            }
            try{
                update cmsToUpdate;
            }Catch(Exception ex){
                
            }
        }
    }
    
    public static list<Case> fetchChildCases(Id CaseId){
        list<Case> parentCases = fetchParentCases(CaseId);
        list<Case> parentClosedCases = fetchParentClosedCases(CaseId);
        list<Case> childCases = new list<Case>();
        list<Case> returnCases = new list<Case>();
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
        childCases = [select id,Subject,ParentID,RecordTypeId,Case_Age__c from case where ParentId =: CaseId];
        for(Case caseObj : childCases){
            if(caseObj.RecordTypeId == recTypeId){
                returnCases.add(caseObj);
            }
        }
        if(!returnCases.isEmpty()){
            Integer caseAge = 0;
            for(Case caseObj : returnCases){
                caseAge += Integer.valueOf(caseObj.Case_Age__c);
            }
            Integer finalCaseAge = caseAge;
        }
        return returnCases;
    }
    
    public static list<Case> fetchParentCases(Id CaseId){
        list<Case> childCases = new list<Case>();
        list<Case> returnCases = new list<Case>();
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
        childCases = [select id,Subject,ParentID,RecordTypeId,Case_Age__c from case where ParentId =: CaseId];
        for(Case caseObj : childCases){
            if(caseObj.RecordTypeId == recTypeId){
                returnCases.add(caseObj);
            }
        }
        if(!returnCases.isEmpty()){
            Integer caseAge = 0;
            for(Case caseObj : returnCases){
                caseAge += Integer.valueOf(caseObj.Case_Age__c);
            }
            Integer finalCaseAge = caseAge;
        }
        return returnCases;
    }
    
    public static list<Case> fetchParentClosedCases(Id CaseId){
        list<Case> childCases = new list<Case>();
        list<Case> returnCases = new list<Case>();
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
        childCases = [select id,Subject,ParentID,RecordTypeId,Case_Age__c from case where ParentId =: CaseId];
        for(Case caseObj : childCases){
            if(caseObj.RecordTypeId == recTypeId){
                returnCases.add(caseObj);
            }
        }
        if(!returnCases.isEmpty()){
            Integer caseAge = 0;
            for(Case caseObj : returnCases){
                caseAge += Integer.valueOf(caseObj.Case_Age__c);
            }
            Integer finalCaseAge = caseAge;
        }
        return returnCases;
    }
    
    /*public static boolean isWeekendOrHoliday(Datetime customDate){
        boolean nonWorkingDay = false;
         if(customDate.format('EEEE') == 'Saturday' || customDate.format('EEEE') == 'Sunday'){
             nonWorkingDay = True;
         }
         List<CSC_Holidays__c> listOfHolidays = CSC_Holidays__c.getall().values();
         for(CSC_Holidays__c hol : listOfHolidays){
              if(hol.Date__c == customDate.date()){
                 nonWorkingDay = True;
                 break;
             }
         }
         
        return nonWorkingDay;
    }*/
}