public  with Sharing class CSC_NewCaseOverrideCtlr {
    @AuraEnabled
    public static RecordType getRecordTypeDeatilsById(String recTypeId) {
        list<RecordType> lstRecordType = new list<RecordType>();
        lstRecordType = [SELECT Id,  Name, DeveloperName, Description FROM RecordType WHERE Id = :recTypeId];
        return lstRecordType.isEmpty() ?null:lstRecordType[0];
    }
    
    @AuraEnabled
    public static boolean getWorksiteAddress(string contactId){
        boolean error = true;
        set<Id> accountIds = new set<Id>();
        Map<Id,String> conESFMap = new Map<Id,String>();
        list<String> workSiteStates = System.label.CSC_WorkSite_States.split(',');
        // List<Contact> listOfContacts = [select Id,AccountId,Account.ShippingState from Contact where Id =: contactId AND Account.ShippingState IN: workSiteStates];
        for(Contact conObj : [select Id,AccountId,Account.ShippingState from Contact where Id =: contactId]){
            accountIds.add(conObj.AccountId);
        }
        if(!accountIds.isEmpty()){
            for(Talent_Work_History__c twh : [select id,SourceCompany__c,Talent__c,Start_Date__c,End_Date__c,ESFId__c from Talent_Work_History__c where Talent__c IN: accountIds ORDER BY End_Date__c DESC]){
                if(!conESFMap.containsKey(twh.Talent__c) && twh.ESFId__c != '' && twh.ESFId__c != NULL){
                    conESFMap.put(twh.Talent__c,twh.ESFId__c);
                }
            }
            if(!conESFMap.isEmpty()){
                for(Order sub : [select id,Opportunity.Req_Worksite_State__c,ShipToContactId,Start_Form_ID__c from Order where ShipToContactId =: contactId AND Status != 'Applicant' AND Start_Form_ID__c IN: conESFMap.values() ORDER BY CreatedDate DESC]){
                    if(workSiteStates.contains(sub.Opportunity.Req_Worksite_State__c)){
                        error = false;
                    }
                }
            }
        }
        system.debug('error=====>>>>>>'+error);
        return error;
    }
    
    @AuraEnabled
    public static boolean getMassCaseWorksiteAddress(list<Id> contactId){
        boolean error = true;
        set<Id> accountIds = new set<Id>();
        Map<Id,String> conESFMap = new Map<Id,String>();
        list<String> workSiteStates = System.label.CSC_WorkSite_States.split(',');
        system.debug('contactId====>>getMassCaseWorksiteAddress>>>>>'+contactId);
        for(Contact conObj : [select Id,AccountId,Account.ShippingState from Contact where Id IN: contactId]){
            accountIds.add(conObj.AccountId);
        }
        if(!accountIds.isEmpty()){
            for(Talent_Work_History__c twh : [select id,SourceCompany__c,Talent__c,Start_Date__c,End_Date__c,ESFId__c from Talent_Work_History__c where Talent__c IN: accountIds ORDER BY End_Date__c DESC]){
                if(!conESFMap.containsKey(twh.Talent__c) && twh.ESFId__c != '' && twh.ESFId__c != NULL){
                    conESFMap.put(twh.Talent__c,twh.ESFId__c);
                }
            }
            if(!conESFMap.isEmpty()){
                for(Order sub : [select id,Opportunity.Req_Worksite_State__c,ShipToContactId,Start_Form_ID__c from Order where ShipToContactId IN: contactId AND Status != 'Applicant' AND Start_Form_ID__c IN: conESFMap.values() ORDER BY CreatedDate DESC]){
                    if(workSiteStates.contains(sub.Opportunity.Req_Worksite_State__c)){
                        error = false;
                    }
                }
            }
        }
        return error;
    }
    
    @AuraEnabled
    public static String getFsgRecordTypeId() {
        String recordTypeId = '';
        list<RecordType> lstRecordType = [SELECT Id,  Name, DeveloperName, Description FROM RecordType WHERE DeveloperName = 'FSG'];
        return lstRecordType.isEmpty() ?null:lstRecordType[0].Id;
    }
    
    @AuraEnabled
    public static String createChildCases(String parentCaseId, String TalentIds) {
        system.debug('------parentCaseId------'+parentCaseId);
         system.debug('------TalentIds------'+TalentIds);
        String status = 'success';
        List<string> listTalentIds = (List<string>) JSON.deserialize(TalentIds, Type.forName('List<string>'));
        system.debug('------listTalentIds------'+listTalentIds);
        if(parentCaseId != null){
            list<Case> parentCaseList = [select Id, Subject, Type, recordTypeId,Status, Sub_Type__c,Case_Issue_Description__c,OneX_Bill_Rate__c,TwoX_Bill_Rate__c,Required_Date__c,Requested_Date__c,OneX_Pay_Rate__c,Finish_Reason__c from case Where Id = :parentCaseId ];
            list<Case> lstCaseToInsert = new list<Case>();
            
            for(String eachTalentId : listTalentIds){
                Case childCaseObj = new Case();
                childCaseObj.ContactId = eachTalentId;
                childCaseObj.ParentId = parentCaseId;
                childCaseObj.recordTypeId = parentCaseList[0].recordTypeId;
                childCaseObj.Status = parentCaseList[0].Status;
                childCaseObj.Subject = parentCaseList[0].Subject;
                childCaseObj.Type = parentCaseList[0].Type;
                childCaseObj.Finish_Reason__c = parentCaseList[0].Finish_Reason__c;
                childCaseObj.Sub_Type__c = parentCaseList[0].Sub_Type__c;
                childCaseObj.Case_Issue_Description__c = parentCaseList[0].Case_Issue_Description__c;
                childCaseObj.OneX_Bill_Rate__c = parentCaseList[0].OneX_Bill_Rate__c;
                childCaseObj.TwoX_Bill_Rate__c = parentCaseList[0].TwoX_Bill_Rate__c;
                childCaseObj.Required_Date__c = parentCaseList[0].Required_Date__c;
                childCaseObj.Requested_Date__c = parentCaseList[0].Requested_Date__c;
                childCaseObj.OneX_Pay_Rate__c = parentCaseList[0].OneX_Pay_Rate__c;
                lstCaseToInsert.add(childCaseObj);
             }
             try{
                if(!lstCaseToInsert.isEmpty()){
                    insert lstCaseToInsert;
                    parentCaseList[0].Number_Of_Child_Case__c = listTalentIds.size(); 
                    update parentCaseList[0];
                } 
            }catch(Exception e){
                system.debug('------e------'+e);
                status = e.getMessage();
            }
        }
        
        return status;
    }
    
    @AuraEnabled
    public static list<case> getCaseList(String caseId) {
        return [SELECT Id,  CaseNumber, Status, Contact.Name,Contact_s_Peoplesoft_ID__c, Center_Name__c,Owner.Name FROM Case WHERE ParentId = :caseId AND Status != 'Closed' ];
    } 
    
    @AuraEnabled
    public static list<case> getCurrentCaseStatus(String caseId) {
        return [SELECT Id,Client_Account_Name__c FROM Case WHERE Id = :caseId];
    }
    
    @AuraEnabled //get case Closed_Reason__c Picklist Values
    public static Map<String, String> getPicklistOptions(String objectApiName, String fieldApiName){
        Map<String, String> options = new Map<String, String>();
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectApiName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: ple) {
            //Put Picklist Value & Label in Map
            if(fieldApiName == 'Status' && p.getValue() != 'Under Review' && p.getValue() != 'Add to backlog'){
                options.put(p.getValue(), p.getLabel());
            }
            if(fieldApiName != 'Status'){
                options.put(p.getValue(), p.getLabel());
            }
            
        }
        return options;
    }
	
	/*@AuraEnabled
    public static void testFileInsert(){
        list<ContentDocumentLink> lsttCd = [select LinkedEntityId ,ContentDocumentId ,shareType  from ContentDocumentLink where LinkedEntityId = '5000n0000077EptAAE'];
        list<case> lstChildCase = [select Id from Case where parentId = '5000n0000077EptAAE'];
        list<ContentDocumentLink> lstCdToInsert = new list<ContentDocumentLink>();
        for(Case eachcase : lstChildCase){
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = eachcase.Id; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
            conDocLink.ContentDocumentId = lsttCd[0].ContentDocumentId;  //ContentDocumentId Id from ContentVersion
            conDocLink.shareType = 'v';
            lstCdToInsert.add(conDocLink);
        }
        if(!lstCdToInsert.isEmpty()){
            insert lstCdToInsert;
        }
    } */   
    
    @AuraEnabled
        public static wrapResult saveChildCases(String parentCaseId, String Status, String closeReason, String closecomment, String onHoldReason, Date futureDate, String childCaseIds){
            List<string> caseIds = (List<string>) JSON.deserialize(childCaseIds, Type.forName('List<string>'));
            wrapResult wrapResObj = new  wrapResult ();        
            list<Case> lstCase = [select Id,CaseNumber,Status,On_Hold_Reason__c,Future_Date__c,Case_Resolution_Comment__c,Closed_Reason__c from Case where Id IN :caseIds];
            list<case> listCaseToUpdate = new list<case>();
            for(Case eachCase : lstCase){
                eachCase.Status = Status;
                eachCase.On_Hold_Reason__c = onHoldReason;
                eachCase.Future_Date__c = futureDate;
                eachCase.Case_Resolution_Comment__c = closecomment;
                eachCase.Closed_Reason__c = closeReason;
                listCaseToUpdate.add(eachCase);
            }
            Database.SaveResult[] srList = Database.update(listCaseToUpdate, false);
            String errorMessage = '';
            Integer successRecCount = 0;
            
            for(Integer i=0; i < srList.size(); i++) {
                if(srList.get(i).isSuccess()) {
                    System.debug('Records are updated Successfully');
                    successRecCount ++;
                } 
                else if(!srList.get(i).isSuccess()) {
                    Database.Error errors =  srList.get(i).getErrors().get(0);
                    //System.debug('errors-----'+srList.get(i).getErrors());
                    //System.debug('Error Occurs While Processing The Record'+errors.getMessage());
                    //System.debug('Failure Record Ids While Updating'+srList.get(i).Id);
                    errorMessage = errorMessage + 'Case Number: '+listCaseToUpdate.get(i).CaseNumber + ', Error: ' +errors.getMessage() + ' || ';
                    
                }
            }
            
            if(errorMessage != ''){
                errorMessage = errorMessage.removeEnd(' || ');
                wrapResObj.isSuccess = false;
                wrapResObj.errorMessage = errorMessage;
                wrapResObj.successCount = successRecCount;
            }else{
                wrapResObj.isSuccess = true;
                wrapResObj.errorMessage = '';
                wrapResObj.successCount = successRecCount;
            }
            return wrapResObj;    
        }
        
        
    
    public class wrapResult{
        @AuraEnabled public boolean isSuccess {get;set;}
        @AuraEnabled public String errorMessage {get;set;}
        @AuraEnabled public integer successCount {get;set;}
    }
    
}