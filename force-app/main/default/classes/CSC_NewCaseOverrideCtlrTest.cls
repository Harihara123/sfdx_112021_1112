@isTest
private class CSC_NewCaseOverrideCtlrTest {
static testmethod void test() {
        Test.startTest();
    	CSC_Holidays__c setting = new CSC_Holidays__c();
        setting.Name = 'hol1';
        setting.Date__c = system.today();
        insert setting;
    	
    	CSC_General_Setup__c setting2 = new CSC_General_Setup__c();
        setting2.Name = 'Lock Status';
        setting2.Lock_Status_over_Weekend_and_Holidays__c = true;
        insert setting2;
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                    emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                    localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                    timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            Id clientRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
			Account clientAacc = new Account(name='test Client', recordTypeId = clientRecTypeId);
			insert clientAacc;
			Case testParentCase = new Case(subject = 'CSC Test parentCase', description = 'CSC_Test_Group', 
										 Type ='Payroll', 
										 Sub_Type__c='Tax Error',
										 //AccountId = acc.id,
										 //ownerId = thisUser.Id,
										 Client_Account_Name__c = clientAacc.Id,
										 //Region_Talent__c = 'TEK Central',
										 //Contact=testContact222, 
										 Case_Issue_Description__c ='test Case Issue Description', 
										 recordTypeId=recTypeId );       
			insert testParentCase; 
            
            Case testChildCase1 = new Case(subject = 'CSC Test222', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     ParentId = testParentCase.Id,
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     //Region_Talent__c = 'TEK Central',
                                     //Contact=testContact222, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testChildCase1; 
			
			CSC_NewCaseOverrideCtlr.getRecordTypeDeatilsById('213421');
			CSC_NewCaseOverrideCtlr.getFsgRecordTypeId();
			list<String> talentIds = new list<String>();
			talentIds.add(String.valueOf(testContact.Id));
			CSC_NewCaseOverrideCtlr.createChildCases(String.valueOf(testParentCase.Id), JSON.serialize(talentIds));
            CSC_NewCaseOverrideCtlr.getCaseList(String.valueOf(testParentCase.Id));
            CSC_NewCaseOverrideCtlr.getPicklistOptions('Case','Status');
            CSC_NewCaseOverrideCtlr.getPicklistOptions('Case','Closed_Reason__c');
            
            list<String> childCaseIds = new list<String>();
			childCaseIds.add(String.valueOf(testChildCase1.Id));
            
            CSC_NewCaseOverrideCtlr.saveChildCases(String.valueOf(testParentCase.Id), 'In Progress', '','', '', null, JSON.serialize(childCaseIds) );
        }
    	
        Test.stopTest();
    }
}