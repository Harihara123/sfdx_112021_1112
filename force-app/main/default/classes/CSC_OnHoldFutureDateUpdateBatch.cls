global class CSC_OnHoldFutureDateUpdateBatch implements Database.Batchable<Sobject>{
    global database.QueryLocator start(Database.BatchableContext bc){
        string query = 'select Id,Future_Date__c,On_Hold_Reason__c,status,CSC_Change_Ownership_Comment__c from Case where recordType.DeveloperName = \'FSG\' and Status = \'On Hold\' and Future_Date__c = TODAY';
        return database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, list<Case> scope){
        system.debug('scope=====>>>>>>'+scope);
        list<case> lstCaseToUpdate = new list<case>();
        for(Case eachCase : scope){
            eachCase.Status = 'In Progress';
            eachCase.On_Hold_Reason__c = null;
            eachCase.Future_Date__c = null;
            /*eachCase.OwnerId = cscQueue[0].Id;
            eachCase.CSC_Change_Ownership_Comment__c = eachCase.CSC_Change_Ownership_Comment__c + '  Updated: '+system.now();
            if(eachCase.CSC_Change_Ownership_Comment__c.length() > 255){
            eachCase.CSC_Change_Ownership_Comment__c = eachCase.CSC_Change_Ownership_Comment__c.subString(0,255);
            }*/
            lstCaseToUpdate.add(eachCase);
        }
        if(!lstCaseToUpdate.isEmpty()){
            update lstCaseToUpdate;
        }
    }
    global void finish(Database.BatchableContext bc){
        
    }
}