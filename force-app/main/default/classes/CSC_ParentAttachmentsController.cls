public class CSC_ParentAttachmentsController {
    @AuraEnabled
    public static list<ContentVersion> getAttachments(Id caseRecId){
        list<ContentVersion> conDocList;
        list<Case> parentCaseIds = new list<Case>();
        list<ContentDocumentLink> linkIds = new list<ContentDocumentLink>();
        parentCaseIds = [select ParentId from Case where Id =: caseRecId AND ParentId != NULL limit 1];
        if(parentCaseIds.isEmpty())
            conDocList = new list<ContentVersion>();
        else{
            linkIds = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: parentCaseIds[0].ParentId];
            if(linkIds.isEmpty())
                conDocList = new list<ContentVersion>();
            else{
                list<Id> docIds = new list<Id>();
                for(ContentDocumentLink link : linkIds){
                    docIds.add(link.ContentDocumentId);
                }
                conDocList = new list<ContentVersion>();
                conDocList = [Select Id ,Title, FileType, ContentSize from ContentVersion Where ContentDocumentId In : docIds];
            }
        }
        system.debug('conDocList=======>>>>>>'+conDocList);
        return conDocList;
    }
}