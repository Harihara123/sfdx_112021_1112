@isTest
public class CSC_ParentAttachmentsControllerTest {
    private static testMethod void unitTest(){
        Test.startTest();
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                     emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                     localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                     timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                     emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                     localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                     timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Contact testContactWOUSer = new Contact(Title='Java Applications Developer', LastName='manish2', accountId=acc.id);
            insert testContactWOUSer;
            
            Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                                      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                      localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                      ContactId = testContact.Id,
                                      timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
            
            insert Talentusr;
            
            Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
            insert testContact222;
            
            //Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                                         emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                         localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                         ContactId = testContact222.Id,Region__c = 'TEK Central',OPCO__c = 'ONS',
                                         timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
            
            insert Talentusr222;
            
            // Inserting a new CSC case record
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     Contact=testContact, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase; 
            
            ContentVersion contentVersion = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
            );
            insert contentVersion;    
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = testCase.id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
            
            Case testCase2 = new Case(subject = 'CSC Test222', description = 'CSC_Test_Group', 
                                      Type ='Payroll', 
                                      Sub_Type__c='Tax Error',
                                      parentId = testCase.Id,
                                      AccountId = acc.id,
                                      ContactId = testContactWoUser.Id,
                                      ownerId = thisUser.Id,
                                      //Region_Talent__c = 'TEK Central',
                                      //Contact=testContact222, 
                                      Case_Issue_Description__c ='test Case Issue Description', 
                                      recordTypeId=recTypeId );       
            insert testCase2; 
            
            CSC_ParentAttachmentsController.getAttachments(testCase2.Id);
            try{
                testCase.status = 'Assigned';
                testCase.Case_Resolution_Comment__c = 'Trying to Delete ContentDocument from Test Class';
                testCase.Closed_Reason__c = 'Case is Resolved';
                testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSC_Read_Only').getRecordTypeId();
                update testCase;
                
                testCase.Status = 'Closed';
                update testCase;
                system.assertEquals('Closed', testCase.Status);
                delete documents;
            }catch(Exception ex){
                system.debug('ex===>>>>>'+testCase);
            }
            Test.stopTest();
        }
    }
    private static testMethod void DocumentLinkTest(){
        Test.startTest();
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                     emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                     localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                     timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            User testUsr2 = new user(alias = 'tstUsr', email='csctest2@teksystems.com',
                                     emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User2', languagelocalekey='en_US',
                                     localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true,
                                     timezonesidkey='Asia/Kolkata', username='csctest2@teksystems.com', OPCO__c ='TEK', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            
            testUsers.add(testUsr1);
            testUsers.add(testUsr2);
            insert testUsers; 
            
            User usr = [SELECT Id FROM User WHERE username='csctest2@teksystems.com'];
            
            Account acc = new Account(name='test manish', Talent_CSA__c=usr.id, merge_state__c ='Post',Talent_Ownership__c='RWS');
            insert acc;
            
            Contact testContact = new Contact(Title='Java Applications Developer', LastName='manish', accountId=acc.id);
            insert testContact;
            
            Contact testContactWOUSer = new Contact(Title='Java Applications Developer', LastName='manish2', accountId=acc.id);
            insert testContactWOUSer;
            
            Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr = new User(alias = 'test0424', email='test5719@noemail.com',
                                      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                      localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                      ContactId = testContact.Id,
                                      timezonesidkey='America/Los_Angeles', username='test5719@noemail.com');
            
            insert Talentusr;
            
            Contact testContact222 = new Contact(Title='Java Applications Developer', LastName='manish222', accountId=acc.id);
            insert testContact222;
            
            //Id p = [select id from profile where name='Aerotek Community User'].id;
            User Talentusr222 = new User(alias = 'test2612', email='test2612@noemail.com',
                                         emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                         localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                         ContactId = testContact222.Id,Region__c = 'TEK Central',OPCO__c = 'ONS',
                                         timezonesidkey='America/Los_Angeles', username='test2612@noemail.com');
            
            insert Talentusr222;
            
            // Inserting a new CSC case record
            Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = testContact.Id,
                                     ownerId = thisUser.Id,
                                     Contact=testContact, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            insert testCase; 
            
            ContentVersion contentVersion = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
            );
            insert contentVersion;    
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = testCase.id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
            
            Case testCase2 = new Case(subject = 'CSC Test222', description = 'CSC_Test_Group', 
                                      Type ='Payroll', 
                                      Sub_Type__c='Tax Error',
                                      parentId = testCase.Id,
                                      AccountId = acc.id,
                                      ContactId = testContactWoUser.Id,
                                      ownerId = thisUser.Id,
                                      //Region_Talent__c = 'TEK Central',
                                      //Contact=testContact222, 
                                      Case_Issue_Description__c ='test Case Issue Description', 
                                      recordTypeId=recTypeId );       
            insert testCase2; 
            
            CSC_ParentAttachmentsController.getAttachments(testCase2.Id);
            CSC_MilestoneUtils.fetchChildCases(testCase2.parentId);
            try{
                testCase.status = 'Assigned';
                testCase.Case_Resolution_Comment__c = 'Trying to Delete ContentDocument from Test Class';
                testCase.Closed_Reason__c = 'Case is Resolved';
                testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSC_Read_Only').getRecordTypeId();
                update testCase;
                
                testCase.Status = 'Closed';
                update testCase;
                system.assertEquals('Closed', testCase.Status);
                delete cdl;
            }catch(Exception ex){
                system.debug('ex===>>>>>'+testCase);
            }
            Test.stopTest();
        }
    }
}