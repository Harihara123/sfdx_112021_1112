global class CSC_PayrollAdjustmentProvisionBatch implements Database.Batchable<Sobject> ,Database.stateful, Schedulable{
    //execute method vor schedular
    public void execute(SchedulableContext sc){
        CSC_PayrollAdjustmentProvisionBatch batchable = new CSC_PayrollAdjustmentProvisionBatch();
		Database.executeBatch(batchable,50);
    }
	global database.Querylocator start(Database.BatchableContext bc){
        String strQuery = '';
        list<String> jobCodeList = System.label.CSC_Payroll_Job_Codes.split(',');
        strQuery = 'select Id,ODS_Jobcode__c from User where IsActive = true AND OPCO__c = \'ONS\' AND ODS_Jobcode__c IN :jobCodeList AND UserType = \'Standard\'';
        system.debug('strQuery=====>>>>>>'+strQuery);
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext bc , list<User> scope){
        List<GroupMember> GroupMemberlist = new List<GroupMember>();
        List<PermissionSetAssignment> PermissionSetList = new list<PermissionSetAssignment>();
        list<String> payrollAdjPGPS = System.label.CSC_Payroll_PG_PS.split(',');
        for(User userObj : scope){
            // Group Mamber
            GroupMember GM = new GroupMember();
            GM.GroupId = id.valueof(payrollAdjPGPS[0]);
            GM.UserOrGroupId = userObj.Id;
            GroupMemberlist.add(GM);
            
            // Group Mamber For Queue
            GroupMember QGM = new GroupMember();
            QGM.GroupId = id.valueof(payrollAdjPGPS[2]);
            QGM.UserOrGroupId = userObj.Id;
            GroupMemberlist.add(QGM);
            
            // Permission Set
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = id.valueof(payrollAdjPGPS[1]);
            psa.AssigneeId = userObj.Id;
            PermissionSetList.add(psa);
        }
        // Adding Users to Group & Permission set
        if(!GroupMemberlist.isEmpty()){
            database.insert(GroupMemberlist,false);
        }
        if(!PermissionSetList.isEmpty()){
            database.insert(PermissionSetList,false);
        }
    }
    global void finish(Database.BatchableContext bc){
    	
    }
}