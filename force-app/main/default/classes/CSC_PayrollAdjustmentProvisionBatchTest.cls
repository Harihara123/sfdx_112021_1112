@isTest
public class CSC_PayrollAdjustmentProvisionBatchTest {
    static testMethod void testProvision(){
        // query profile which will be used while creating a new user
        Profile testProfile = [select id 
                               from Profile 
                               where name='Single Desk 1']; 
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // Insert account as current user
        System.runAs (thisUser) {
            // Inserting test users    
            List<User> testUsers = new List<User>();
            
            User testUsr1 = new user(alias = 'tstUsr1', email='csctest1@teksystems.com',
                                     emailencodingkey='UTF-8', firstName='CSC_Test', lastname='User1', languagelocalekey='en_US',
                                     localesidkey='en_IN', profileid = testProfile.Id, country='India', isActive = true, ODS_Jobcode__c = '1124',
                                     timezonesidkey='Asia/Kolkata', username='csctest1@teksystems.com', OPCO__c ='ONS', Office_Code__c ='31114', Region_Code__c ='TEKGS'); 
            testUsers.add(testUsr1);
            insert testUsers; 
            Test.startTest();
            CSC_PayrollAdjustmentProvisionBatch prov = new CSC_PayrollAdjustmentProvisionBatch();
            String sch3 = '0 30 23 * * ? *';
            String jobID3 = system.schedule('Provision Test', sch3, prov);
            
            CSC_PayrollAdjustmentProvisionBatch batchable = new CSC_PayrollAdjustmentProvisionBatch();
			Database.executeBatch(batchable,50);
            Test.stopTest();
        }
    }
}