public class CSC_PayrollValidationClass {
	@AuraEnabled
    public static Boolean fetchCaseInfo(String processId){
        boolean showToast = false;
        list<ProcessInstanceStep> pisList = [SELECT id,ProcessInstanceId FROM ProcessInstanceStep where id =: processId];
        list<ProcessInstance> piList = [SELECT Id,Status,TargetObjectId FROM ProcessInstance where id =: pisList[0].ProcessInstanceId];
        if(!piList.isEmpty() && String.valueOF(piList[0].TargetObjectId).startsWith('500')){
            Case obj = [select Id, Type, Sub_type__c,Comments_Updated__c,Is_CSC_Rejected_Case__c,Payroll_Rejected_Reason__c from Case Where Id =:piList[0].TargetObjectId];
            system.debug('obj.Is_CSC_Rejected_Case__c=======>>>>>>>'+obj.Is_CSC_Rejected_Case__c);
            system.debug('obj.Payroll_Rejected_Reason__c======>>>>>>>'+obj.Payroll_Rejected_Reason__c);
            if(obj.Is_CSC_Rejected_Case__c == true && (obj.Payroll_Rejected_Reason__c == Null || obj.Payroll_Rejected_Reason__c == '')){
                showToast = true;
            }
        }
        return showToast;
    }
}