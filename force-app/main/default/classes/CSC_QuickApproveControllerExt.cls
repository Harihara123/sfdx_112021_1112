/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* CSC_QuickApproveControllerExt class is used to Approve Pending approval Cases one or more.
* This class is accessed by "CSC access permissison for Center" permission set
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ismail Jabeebulla Shaik
* @modifiedBy     
* @maintainedBy   
* @version        1.0
* @created        2021-03-01
* @modified       

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class CSC_QuickApproveControllerExt {
    public String selectedRecordIds {get;set;} //JSON string of selected Case Ids
    public String listViewId {get;set;}
    public CSC_QuickApproveControllerExt(ApexPages.StandardSetController controller) {
        listViewId = controller.getFilterId();
        //Iterate all selected records and get a JSON string of selected Case Ids
        list<Id> listCaseIds = new list<Id>();
        for(Case eachcase : (list<Case>) controller.getSelected()){
            listCaseIds.add(eachcase.Id);
        }
        selectedRecordIds = JSON.serialize(listCaseIds);
        system.debug('-----selectedRecordIds----------'+selectedRecordIds);
    }
    
    //This method is used to validate whether selected cases are pending for approval or not?
    @AuraEnabled
    public static boolean validateApprovalRec(list<String> listOfselectedCaseIds) {
        boolean error = false;
        List<Case> listOfCases = [select type,status,Pending_for_Approval__c from Case where Id in :listOfselectedCaseIds and Pending_for_Approval__c != True];
        if(listOfCases.size() > 0){
            return true;
        }
        return error;
    }
    
    //This method is used to approve the cases
    @AuraEnabled
    public static void approveCases(list<String> listOfselectedCaseIds) {
        Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :listOfselectedCaseIds])).keySet();
        Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
        List<Approval.ProcessWorkitemRequest> allReq = new List<Approval.ProcessWorkitemRequest>(); 
        for (Id pInstanceWorkitemsId:pInstanceWorkitems){
            system.debug(pInstanceWorkitemsId);
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approved from Quick Approve');
            req2.setAction('Approve'); //to reject use 'Reject'
            req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            
            // Use the ID from the newly created item to specify the item to be worked
            req2.setWorkitemId(pInstanceWorkitemsId);
            
            // Add the request for approval
            allReq.add(req2);
        }
        Approval.ProcessResult[] result2 =  Approval.process(allReq);
        //return null;
    }
}