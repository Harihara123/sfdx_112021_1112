/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* CSC_QuickCloseControllerExt class is used to close VSB cases one or more.
* This class is accessed by "CSC access permissison for Center" permission set
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ismail Jabeebulla Shaik
* @modifiedBy     
* @maintainedBy   
* @version        1.0
* @created        2020-08-25
* @modified       

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class CSC_QuickCloseControllerExt {
	public String selectedRecordIds {get;set;} //JSON string of selected Case Ids
    public String listViewId {get;set;}
    public CSC_QuickCloseControllerExt(ApexPages.StandardSetController controller) {
        listViewId = controller.getFilterId();
        //Iterate all selected records and get a JSON string of selected Case Ids
        list<Id> listCaseIds = new list<Id>();
        for(Case eachcase : (list<Case>) controller.getSelected()){
            listCaseIds.add(eachcase.Id);
        }
        selectedRecordIds = JSON.serialize(listCaseIds);
        system.debug('-----selectedRecordIds----------'+selectedRecordIds);
    }
    
    //This method is used to validate whether logged in user is allowed to perform Bulk Transfer  or not.
    @AuraEnabled
    public static boolean fetchGroupMembers(String userId) {
        system.debug('userId=====from page===>>>>>'+userId);
        system.debug('current User ID=======>>>>>>>>'+UserInfo.getUserId());
        boolean error = false;
        list<string> closePermission = System.Label.CSC_Quick_Close.split(',');
        system.debug('closePermission=====>>>>>>>>'+closePermission);
        List<groupmember> listOfGroupMembers = [select userorgroupid from groupmember where group.DeveloperName IN: closePermission and userorgroupid = :userId];
        system.debug('listOfGroupMembers=====>>>>>>>>'+closePermission);
        system.debug('listOfGroupMembers.size()=======>>>>>>'+listOfGroupMembers.size());
        if(listOfGroupMembers.size() == 0){
            return true;
        }
        return error;
    }
    
    //This method is used to validate whether selected cases are of VSB or not.
    @AuraEnabled
    public static boolean validateVSBRecords(list<String> listOfselectedCaseIds) {
        boolean error = false;
        List<Case> listOfCases = [select type,status from Case where Id in :listOfselectedCaseIds and (type != 'Started' OR Status = 'closed')];
        if(listOfCases.size() > 0){
            return true;
        }
        return error;
    }
    
    //get case Closed_Reason__c Picklist Values
    @AuraEnabled 
    public static Map<String, String> getClosedReason(){
        Map<String, String> options = new Map<String, String>();
        //get Account Industry Field Describe
        Schema.DescribeFieldResult fieldResult = Case.Closed_Reason__c.getDescribe();
        //get Account Industry Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
    //Method to close the started cases
    @AuraEnabled 
    public static string closeTheCase(list<String> listOfselectedCaseIds, String Comments, String Status, String closedReason) {
        String result = 'success';
        list<case> caseList = new list<Case>();
        for(String str : listOfselectedCaseIds){
            Case caseObj = new case();
            caseObj.Id = Id.valueOf(str);
            caseObj.Status = Status;
            caseObj.Closed_Reason__c = closedReason;
            caseObj.Case_Resolution_Comment__c = Comments;
            caseObj.VSB_Quick_Close__c = TRUE;
            caseList.add(caseObj);
        }
        try{
            system.debug('------caseList--------'+caseList);
            update caseList;
            
        }catch(exception e){
            system.debug('------e.getMessage()--------'+e.getMessage());
            return e.getMessage();
        }
        return result;
    }
}