/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* CSC_RTCABatchable: This is a batch class for RTCA report runs everyday @ 11PM 
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ismail Shaik
* @modifiedBy     
* @maintainedBy   
* @version        1.0
* @created        
* @modified       

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class CSC_RTCAOpenBatchable implements Database.Batchable<SObject>, Database.Stateful, Schedulable{
    public list<ResWrapperClass> lstWrap = new list<ResWrapperClass>();
    //execute method vor schedular
    public void execute(SchedulableContext sc){
        CSC_RTCAOpenBatchable batchable = new CSC_RTCAOpenBatchable();
        Database.executeBatch(batchable,10);
    }
    public Database.QueryLocator start(Database.BatchableContext bc){
        string query = '';
        query += 'select id, CaseNumber, Owner.Name,Contact.Name, CreatedDate,ClosedDate,Creator_Office__c,Creator_s_OpCo__c,Region__c ,Status, Type, sub_Type__c,Business_Unit__c,Business_Unit_Region__c,Center_Name__c,Talent_Office__r.name,Case_Resolution_Comment__c,Re_Opened_Count__c from Case where recordType.DeveloperName = \'FSG\' AND Client_Account_Name__c = null AND Type != \'Started\' AND Status != \'Closed\'';
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, list<Case> scope){
        list<case_Status_History__c> cshList = new list<case_Status_History__c>();
        set<Id> caseIds = new set<Id>();
        Map<String,Decimal> cshMap = new Map<String,Decimal>();
        for(Case eachcase : scope){
            //cshList.addAll(eachcase.Case_Status_Histories__r);
            caseIds.add(eachcase.Id);
        }
        for(case_Status_History__c cshObj : [select status__c, Id, Live_Age__c, On_Hold_Reason__c,Created_on_Weekend_Holiday__c,case__c from Case_Status_History__c where case__c IN: caseIds]){
            if(cshObj.Created_on_Weekend_Holiday__c == False && cshObj.On_Hold_Reason__c != 'Future Dated Change'){
                if(cshMap.containsKey(cshObj.Case__c+'___'+cshObj.Status__c)){
                    cshMap.put(cshObj.Case__c+'___'+cshObj.Status__c, cshMap.get(cshObj.Case__c+'___'+cshObj.Status__c) + cshObj.Live_Age__c);
                }else
                    cshMap.put(cshObj.Case__c+'___'+cshObj.Status__c, cshObj.Live_Age__c);
            }
        }
        for(Case eachcase : scope){
            ResWrapperClass wrapObj = new ResWrapperClass();
            wrapObj.caseId = eachcase.Id;
            wrapObj.caseNumber = eachcase.CaseNumber;
            wrapObj.caseOwnerName = eachcase.Owner.Name;
            if(eachcase.Contact.Name != NULL && eachcase.Contact.Name != '')
                wrapObj.contactName = eachcase.Contact.Name.replaceAll('[^a-zA-Z0-9\\s+]', '');
            wrapObj.CreatedDate = eachcase.CreatedDate.format();
            if(eachcase.ClosedDate != null){
                wrapObj.ClosedDate = eachcase.ClosedDate.format();
            }
            wrapObj.officeId = eachcase.Creator_Office__c != null ? eachcase.Creator_Office__c.replace(',','-') : '';
            wrapObj.caseOPCO = eachcase.Creator_s_OpCo__c;
            wrapObj.caseRegion = eachcase.Region__c;
            wrapObj.Status = eachcase.Status;
            wrapObj.caseType = eachcase.Type;
            wrapObj.caseSubType = eachcase.sub_Type__c.replace(',','-');
            wrapObj.caseResolutionComment = eachcase.Case_Resolution_Comment__c;
            wrapObj.reOpenCount = eachcase.Re_Opened_Count__c != null ? Integer.valueOf(eachcase.Re_Opened_Count__c) : 0;
            wrapObj.businessUnit = eachcase.Business_Unit__c != null ? eachcase.Business_Unit__c : '';
            wrapObj.businessRegion = eachcase.Business_Unit_Region__c != null ? eachcase.Business_Unit_Region__c : '';
            wrapObj.centerName = eachcase.Center_Name__c;
            wrapObj.officeTalent = eachcase.Talent_Office__r.name != null ? eachcase.Talent_Office__r.name.replace(',','-') : '';
            wrapObj.ageNew = 0.0;
            wrapObj.countNew = 0;
            wrapObj.ageAssigned = 0.0;
            wrapObj.countAssigned = 0;
            wrapObj.ageInProgress = 0.0;
            wrapObj.countInProgress = 0;
            wrapObj.ageOnHold = 0.0;
            wrapObj.countOnHold = 0;
            wrapObj.ageEscalated = 0.0;
            wrapObj.countEscalated = 0;
            wrapObj.ageUnderReview = 0.0;
            wrapObj.countUnderReview = 0;
            
            if(cshMap.containsKey(eachcase.Id+'___New')){
                wrapObj.ageNew += cshMap.get(eachcase.Id+'___New');
                wrapObj.countNew +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___Assigned')){
                wrapObj.ageAssigned += cshMap.get(eachcase.Id+'___Assigned');
                wrapObj.countAssigned +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___In Progress')){
                wrapObj.ageInProgress += cshMap.get(eachcase.Id+'___In Progress');
                wrapObj.countInProgress +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___On Hold')){
                wrapObj.ageOnHold += cshMap.get(eachcase.Id+'___On Hold');
                wrapObj.countOnHold +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___Escalated')){
                wrapObj.ageEscalated += cshMap.get(eachcase.Id+'___Escalated');
                wrapObj.countEscalated +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___Under Review')){
                wrapObj.ageUnderReview += cshMap.get(eachcase.Id+'___Under Review');
                wrapObj.countUnderReview +=1;
            }
            wrapObj.caseAge = wrapObj.ageNew + wrapObj.ageAssigned + wrapObj.ageInProgress +wrapObj.ageEscalated + wrapObj.ageUnderReview;
            lstWrap.add(wrapObj);
        }
    }
    public void finish(Database.BatchableContext bc){
        // Code to generate Excel
        String finalRTCA = '';
        String header = 'Case Number,Owner,Contact,CreatedDate,Closed Date,Case Creator-Office ID,Region,Status,Type,Sub Type,Business Unit,Business Region,Center Name,Talent Office,New,Assigned,In Progress,OnHold,Escalated,Under Review,Case Age Excluding On Hold,Case Re-opened Count \n';
        finalRTCA = header;
        for(ResWrapperClass obj : lstWrap){
            String rowData = obj.caseNumber+','+obj.caseOwnerName+','+obj.contactName+','+obj.CreatedDate+','+obj.ClosedDate+','+obj.officeId+','+obj.caseRegion+','+obj.Status+','+obj.caseType+','+obj.caseSubType+','+obj.businessUnit+','+obj.businessRegion+','+obj.centerName+','+obj.officeTalent+','+obj.ageNew+','+obj.ageAssigned+','+obj.ageInProgress+','+obj.ageOnHold+','+obj.ageEscalated+','+obj.ageUnderReview+','+obj.caseAge+','+obj.reOpenCount+'\n';
            finalRTCA = finalRTCA+rowData;
        }
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        blob excel = blob.valueOf(finalRTCA);
        attach.setBody(excel);
        attach.setFileName('Open RTCA-Report.csv');
        String subject = 'Open RTCA as of '+System.today();
        String body = 'Open RTCA as of '+System.today();
        //string[] address = new string[]{'sjabeebu@teksystems.com'};
        Messaging.singleEmailMessage Emailwithattch = new Messaging.singleEmailMessage();
        Emailwithattch.setSubject(subject);
        Emailwithattch.setToaddresses(getMailAddresses());
        Emailwithattch.setPlainTextBody(body);
        Emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
        
        // Sends the email
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {Emailwithattch});
        
    }
    
    //Get Email Addresses
    public List<String> getMailAddresses(){
        List<String> idList = new List<String>();
        List<String> mailToAddresses = new List<String>(); 
        for (GroupMember gm : [Select Id, UserOrGroupId From GroupMember where group.name IN ('CSC RTCA - TEK','CSC RTCA - ONS')]) {
            idList.add(gm.UserOrGroupId);
        }
        List<User> userList = [SELECT Email FROM User WHERE Id IN :idList];
        for(User u : userList) {
            mailToAddresses.add(u.email);
        } 
        return mailToAddresses;
    }
    
    public class ResWrapperClass{
        public Id caseId {get;set;}
        public String caseNumber {get;set;}
        public String caseOwnerName {get;set;}
        public String contactName {get;set;}
        public String CreatedDate {get;set;}
        public String ClosedDate {get;set;}
        public String officeId {get;set;}
        public String caseOPCO {get;set;}
        public String caseRegion {get;set;}
        public String Status {get;set;}
        public String caseType {get;set;}
        public String caseSubType {get;set;}
        public String caseResolutionComment{get;set;}
        public Integer reOpenCount {get;set;}
        public String businessUnit{get;set;}
        public String businessRegion{get;set;}
        public String centerName{get;set;}
        public String officeTalent{get;set;}
        public Decimal ageNew {get;set;}
        public Integer countNew {get;set;}
        public Decimal ageAssigned {get;set;}
        public Integer countAssigned {get;set;}
        public Decimal ageInProgress {get;set;}
        public Integer countInProgress {get;set;}
        public Decimal ageOnHold {get;set;}
        public Integer countOnHold {get;set;}
        public Decimal ageEscalated {get;set;}
        public Integer countEscalated {get;set;}
        public Decimal ageUnderReview {get;set;}
        public Integer countUnderReview {get;set;}
        public Decimal caseAge {get;set;}
    }
}