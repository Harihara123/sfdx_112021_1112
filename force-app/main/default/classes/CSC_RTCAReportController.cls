public class CSC_RTCAReportController {
    @AuraEnabled
    public static list<ResWrapperClass> searchResult(String inputData){
        system.debug('inputData======>>>>>>>'+inputData);
        list<case_Status_History__c> cshList = new list<case_Status_History__c>();
        list<ResWrapperClass> lstWrap = new list<ResWrapperClass>();
    	Map<String,Decimal> cshMap = new Map<String,Decimal>();
        ReqWrapper reqWrap = (ReqWrapper) JSON.deserialize(inputData, ReqWrapper.class);
        Date startCreatedDate;
        Date endCreatedDate;
        Date startClosedDate;
        Date endClosedDate;
        list<String> statusValues = new list<String>();
        String talentId;
        if(reqWrap.createdDate != '' && reqWrap.createdDate != null){
            startCreatedDate = Date.parse(reqWrap.createdDate.split(',')[0]);
            startCreatedDate = startCreatedDate.addDays(-1);
        	endCreatedDate = Date.parse(reqWrap.createdDate.split(',')[1]);
            endCreatedDate = endCreatedDate.addDays(-1);
        }
        if(reqWrap.closedDate != '' && reqWrap.closedDate != null){
            startClosedDate = Date.parse(reqWrap.closedDate.split(',')[0]);
            startClosedDate = startClosedDate.addDays(-1);
        	endClosedDate = Date.parse(reqWrap.closedDate.split(',')[1]);
            endClosedDate = endClosedDate.addDays(-1);
        }
        if(reqWrap.statusValues != NULL && !reqWrap.statusValues.isEmpty()){
        	statusValues = reqWrap.statusValues;
        }
        if(reqWrap.talentOfficeId != NULL && !reqWrap.talentOfficeId.isEmpty()){
            talentId = reqWrap.talentOfficeId[0];
        }
        
        String baseQuery = '';
        baseQuery += 'select id, CaseNumber, Owner.Name,Contact.Name, CreatedDate,ClosedDate,Creator_Office__c,Creator_s_OpCo__c,Region__c ,Status, Type, sub_Type__c,Business_Unit__c,Business_Unit_Region__c,Center_Name__c,Talent_Office__r.name,Case_Resolution_Comment__c,Re_Opened_Count__c,(select status__c, Id, Live_Age__c, On_Hold_Reason__c,Created_on_Weekend_Holiday__c,case__c from Case_Status_Histories__r) from Case where ( recordType.DeveloperName = \'FSG\' OR recordType.DeveloperName = \'CSC_Read_Only\') AND Client_Account_Name__c = null ';
		if(startCreatedDate != NULL && endCreatedDate != NULL){
            baseQuery += ' AND CreatedDate >= :startCreatedDate AND CreatedDate <= :endCreatedDate ';
        }
        if(statusValues != null && !statusValues.isEmpty()){
            baseQuery += ' AND Status IN: statusValues ';
        }
        if(reqWrap.opcoValue != null && reqWrap.opcoValue != ''){
			baseQuery = baseQuery + ' AND Creator_s_OpCo__c = \''+reqWrap.opcoValue+'\' ';
		}
        if(reqWrap.talentOfficeId != null && !reqWrap.talentOfficeId.isEmpty()){
            baseQuery += ' AND Talent_Office__c = \''+talentId+'\' ';
        }
        if(reqWrap.centerValue != null && reqWrap.centerValue != ''){
			baseQuery = baseQuery + ' AND Center_Name__c = \''+reqWrap.centerValue+'\' ';
		}
        if(startClosedDate != NULL && endClosedDate != NULL  && statusValues.contains('Closed')){
            baseQuery += ' AND ClosedDate >= :startClosedDate AND ClosedDate <= :endClosedDate ';
        }
        system.debug('baseQuery======>>>>>>>>'+baseQuery);
        transient list<Case> lstCaseWithStatusHistory = database.query(baseQuery);
        for(Case eachcase : lstCaseWithStatusHistory){
            cshList.addAll(eachcase.Case_Status_Histories__r);
		}
        for(case_Status_History__c cshObj : cshList){
            if(cshObj.Created_on_Weekend_Holiday__c == False && cshObj.On_Hold_Reason__c != 'Future Dated Change'){
                if(cshMap.containsKey(cshObj.Case__c+'___'+cshObj.Status__c)){
                    cshMap.put(cshObj.Case__c+'___'+cshObj.Status__c, cshMap.get(cshObj.Case__c+'___'+cshObj.Status__c) + cshObj.Live_Age__c);
                }else
                    cshMap.put(cshObj.Case__c+'___'+cshObj.Status__c, cshObj.Live_Age__c);
            }
        }
        system.debug('cshMap======>>>>>>>'+cshMap);
        for(Case eachcase : lstCaseWithStatusHistory){
            ResWrapperClass wrapObj = new ResWrapperClass();
			wrapObj.caseId = eachcase.Id;
			wrapObj.caseNumber = eachcase.CaseNumber;
			wrapObj.caseOwnerName = eachcase.Owner.Name;
            if(eachcase.Contact.Name != NULL && eachcase.Contact.Name != '')
				wrapObj.contactName = eachcase.Contact.Name.replaceAll('[^a-zA-Z0-9\\s+]', '');
			wrapObj.CreatedDate = eachcase.CreatedDate.format();
            if(eachcase.ClosedDate != null){
                wrapObj.ClosedDate = eachcase.ClosedDate.format();
            }
            wrapObj.officeId = eachcase.Creator_Office__c;
			wrapObj.caseOPCO = eachcase.Creator_s_OpCo__c;
			wrapObj.caseRegion = eachcase.Region__c;
			wrapObj.Status = eachcase.Status;
			wrapObj.caseType = eachcase.Type;
			wrapObj.caseSubType = eachcase.sub_Type__c;
			wrapObj.caseResolutionComment = eachcase.Case_Resolution_Comment__c;
			wrapObj.reOpenCount = Integer.valueOf(eachcase.Re_Opened_Count__c);
            wrapObj.businessUnit = eachcase.Business_Unit__c;
            wrapObj.businessRegion = eachcase.Business_Unit_Region__c;
            wrapObj.centerName = eachcase.Center_Name__c;
            wrapObj.officeTalent = eachcase.Talent_Office__r.name;
			wrapObj.ageNew = 0.0;
			wrapObj.countNew = 0;
			wrapObj.ageAssigned = 0.0;
			wrapObj.countAssigned = 0;
			wrapObj.ageInProgress = 0.0;
			wrapObj.countInProgress = 0;
			wrapObj.ageOnHold = 0.0;
			wrapObj.countOnHold = 0;
			wrapObj.ageEscalated = 0.0;
			wrapObj.countEscalated = 0;
			wrapObj.ageUnderReview = 0.0;
			wrapObj.countUnderReview = 0;
            
            if(cshMap.containsKey(eachcase.Id+'___New')){
                wrapObj.ageNew += cshMap.get(eachcase.Id+'___New');
                wrapObj.countNew +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___Assigned')){
                wrapObj.ageAssigned += cshMap.get(eachcase.Id+'___Assigned');
                wrapObj.countAssigned +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___In Progress')){
                wrapObj.ageInProgress += cshMap.get(eachcase.Id+'___In Progress');
                wrapObj.countInProgress +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___On Hold')){
                wrapObj.ageOnHold += cshMap.get(eachcase.Id+'___On Hold');
                wrapObj.countOnHold +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___Escalated')){
                wrapObj.ageEscalated += cshMap.get(eachcase.Id+'___Escalated');
                wrapObj.countEscalated +=1;
            }
            if(cshMap.containsKey(eachcase.Id+'___Under Review')){
                wrapObj.ageUnderReview += cshMap.get(eachcase.Id+'___Under Review');
                wrapObj.countUnderReview +=1;
            }
            wrapObj.caseAge = wrapObj.ageNew + wrapObj.ageAssigned + wrapObj.ageInProgress +wrapObj.ageEscalated + wrapObj.ageUnderReview;
			lstWrap.add(wrapObj);
		}
        return lstWrap;
    }
    
    public class ReqWrapper{
        public string opcoValue;
        public list<string> talentOfficeId;
        public string centerValue;
        public string createdDate;
        public string closedDate;
        public list<string> statusValues;
    }
    
    public class ResWrapperClass{
		@AuraEnabled public Id caseId {get;set;}
		@AuraEnabled public String caseNumber {get;set;}
		@AuraEnabled public String caseOwnerName {get;set;}
		@AuraEnabled public String contactName {get;set;}
		@AuraEnabled public String CreatedDate {get;set;}
        @AuraEnabled public String ClosedDate {get;set;}
		@AuraEnabled public String officeId {get;set;}
		@AuraEnabled public String caseOPCO {get;set;}
		@AuraEnabled public String caseRegion {get;set;}
		@AuraEnabled public String Status {get;set;}
		@AuraEnabled public String caseType {get;set;}
		@AuraEnabled public String caseSubType {get;set;}
		@AuraEnabled public String caseResolutionComment{get;set;}
		@AuraEnabled public Integer reOpenCount {get;set;}
        @AuraEnabled public String businessUnit{get;set;}
        @AuraEnabled public String businessRegion{get;set;}
        @AuraEnabled public String centerName{get;set;}
        @AuraEnabled public String officeTalent{get;set;}
		@AuraEnabled public Decimal ageNew {get;set;}
		@AuraEnabled public Integer countNew {get;set;}
		@AuraEnabled public Decimal ageAssigned {get;set;}
		@AuraEnabled public Integer countAssigned {get;set;}
		@AuraEnabled public Decimal ageInProgress {get;set;}
		@AuraEnabled public Integer countInProgress {get;set;}
		@AuraEnabled public Decimal ageOnHold {get;set;}
		@AuraEnabled public Integer countOnHold {get;set;}
		@AuraEnabled public Decimal ageEscalated {get;set;}
		@AuraEnabled public Integer countEscalated {get;set;}
		@AuraEnabled public Decimal ageUnderReview {get;set;}
		@AuraEnabled public Integer countUnderReview {get;set;}
		@AuraEnabled public Decimal caseAge {get;set;}
    }
    // Controller for lookup field
    @AuraEnabled(cacheable=true)  
    public static List<sobject> findTargetOffice(String searchKey) {  
        string searchText = '\'' + String.escapeSingleQuotes(searchKey) + '%\'';  
        string query = 'SELECT Talent_Office__c FROM Case WHERE Name LIKE '+searchText+' LIMIT 6';  
        return Database.query(query);  
    }  
}