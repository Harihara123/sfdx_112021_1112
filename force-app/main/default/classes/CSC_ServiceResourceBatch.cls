global class CSC_ServiceResourceBatch implements Database.Batchable<sObject>{
	
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([Select id , Name from User where Office_Code__c IN ('01464','01468') and isActive = True and Profile.Name = 'Single Desk 1']);
    }
    
    global void execute(Database.BatchableContext bc, List<User> userList){
        List<ServiceResource> serviceResourceList = new List<ServiceResource>();
        for(User userRecord:userList){
            ServiceResource serviceResourceRecord = new ServiceResource();
            serviceResourceRecord.Name = userRecord.Name;
            serviceResourceRecord.RelatedRecordId = userRecord.id;
            serviceResourceRecord.ResourceType = 'A';
            serviceResourceRecord.IsActive = True;
            if(userRecord.Office_Code__c == '01464'){
                serviceResourceRecord.Description = 'Tempe';
            }else{
                serviceResourceRecord.Description = 'Jacksonville';
            }
            serviceResourceList.add(serviceResourceRecord);
        }
        system.debug(serviceResourceList);
        insert serviceResourceList;
    }
    
    global void finish(Database.BatchableContext bc){
        CSC_ServiceResourceSkillBatch batchable = new CSC_ServiceResourceSkillBatch();
		Database.executeBatch(batchable);
        /*AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email   FROM AsyncApexJob where Id =:BC.getJobId()];
      
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Division Mapping user fields ' + a.Status);
        mail.setPlainTextBody('records processed ' + a.TotalJobItems +   'with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
    }
}