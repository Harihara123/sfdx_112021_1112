global class CSC_ServiceResourceSkillBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id,Name,ResourceType,Description FROM ServiceResource where createdDate =: System.today()]);
    }
    
    global void execute(Database.BatchableContext bc, List<ServiceResource> serviceResourceList){
        Map<String,Id> skillsMap = new Map<String,Id>();
        list<ServiceResourceSkill> serviceResourceSkillList = new list<ServiceResourceSkill>();
        for(Skill skillRec : [SELECT id,DeveloperName FROM Skill]){
            skillsMap.put(skillRec.DeveloperName,skillRec.Id);
        }
        for(ServiceResource serviceResourceRecord : serviceResourceList){
            ServiceResourceSkill ServiceResourceSkillRec = new ServiceResourceSkill();
            ServiceResourceSkillRec.EffectiveStartDate = System.today();
            ServiceResourceSkillRec.ServiceResourceId = serviceResourceRecord.Id;
            if(skillsMap.containsKey(serviceResourceRecord.Description)){
                ServiceResourceSkillRec.SkillId = skillsMap.get(serviceResourceRecord.Description);
            }
            serviceResourceSkillList.add(ServiceResourceSkillRec);
        }
        if(!serviceResourceSkillList.isEmpty()){
            insert serviceResourceSkillList;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
}