public with Sharing class CSC_TypeSubTypeValidationClass {
	@AuraEnabled
    public static case fetchCaseInfo(String caseId){
        return [select Id, Type, Sub_type__c,Comments_Updated__c,Is_CSC_Rejected_Case__c,Payroll_Rejected_Reason__c from Case Where Id =:CaseId];
    }
    
    @AuraEnabled
    public static string updateCaseFlag(String caseId){
        String res = 'success';
        try{
            case caseRec = new Case();
            caseRec.id = caseId;
            caseRec.Comments_Updated__c = false;
            update caseRec;
        }catch(Exception e){
            res = 'error';
        }
        return res;
    }
}