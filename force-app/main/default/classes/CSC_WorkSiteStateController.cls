public class CSC_WorkSiteStateController {
	@AuraEnabled
    public static list<CSC_Work_Site_State__c> fetchWorkSiteState(Id caseRecId){
        list<CSC_Work_Site_State__c> workSiteList;
        string subType = '';
        string workSiteState = '';
        list<Case> caseList = [select id,Sub_Type__c,Talent_Req_WorkSite_State__c from Case where id =: caseRecId limit 1];
        subType = caseList[0].Sub_Type__c;
        workSiteState = caseList[0].Talent_Req_WorkSite_State__c;
    	if(subType == 'Final Hours'){
            workSiteList = new list<CSC_Work_Site_State__c>();
            workSiteList = [select id,Name,CPC_Refusal_Required__c,Immediate_or_24_Hours__c,InfoPath_Reason__c,InfoPath_to_Complete__c,Other_Required_Documents__c,Term_Code_Dependant__c,Vacation_Payout_Required__c from CSC_Work_Site_State__c where name =: workSiteState limit 1];
        }else{
            workSiteList = new list<CSC_Work_Site_State__c>();
        }
        return workSiteList;
    }
    
}