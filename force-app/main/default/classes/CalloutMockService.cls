@isTest
public class CalloutMockService  implements HttpCalloutMock{
String responseJSON;
String ContentType;
String status;
Integer statusCode;

  public CalloutMockService(String responseJSON,String ContentType,Integer statusCode) {
       this.responseJSON = responseJSON;
	   this.ContentType = ContentType;
	   this.statusCode = statusCode;
    }
  public HTTPResponse respond(HTTPRequest req) {
       HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', ContentType);
        res.setBody(responseJSON);
        res.setStatusCode(statusCode);
        return res;
    }
}