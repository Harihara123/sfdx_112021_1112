@istest
public class CandidateTest {
	static testMethod void validUpdateMethodTest() {
		Contact contact = new Contact();
		contact.firstname = 'Test';
		Contact.LastName = 'testing1';

		insert contact;

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = '';
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '10';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = 'PP';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta@allegisgroup.com';
		emailDetails.workEmail = 'psammeta@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376920';
		atsPhoneDetails.phone = '1223344556';
		atsPhoneDetails.workPhone = '23232';
		atsPhoneDetails.otherPhone = '85858687890';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'false';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;
		atsCandidate.linkedInURL = 'https://www.linkedin.com/in/abc-efg-%E2%98%81-%E2%9A%A1-51a64b53/';


		//CreateTalentTestData.createCustomSettings();

		User user = TestDataHelper.createUser('System Administrator');
		insert user;
        
  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');

		System.runAs(user) {

			Test.startTest();

			// using mock
			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'false';
			atsCandidate.textConsent = 'false';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = '';
			atsCandidate.textConsent = '';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'true';
			atsCandidate.textConsent = 'true';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			Test.stopTest();
		}
	}

	static testMethod void validUpdateMethodTest1() {
		Contact contact = new Contact();
		contact.firstname = 'Test';
		Contact.LastName = 'testing1';

		insert contact;

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = '';
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '10';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = 'PP';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta@allegisgroup.com';
		emailDetails.workEmail = 'psammeta@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376920';
		atsPhoneDetails.phone = '1223344556';
		atsPhoneDetails.workPhone = '23232';
		atsPhoneDetails.otherPhone = '85858687890';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'false';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;
		atsCandidate.linkedInURL = 'https://www.linkedin.com/in/abc-efg-%E2%98%81-%E2%9A%A1-51a64b53/';


		//CreateTalentTestData.createCustomSettings();

		User user = TestDataHelper.createUser('System Administrator');
		insert user;

  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');

		System.runAs(user) {

			Test.startTest();

			// using mock
			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);			

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '8';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '9';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '11';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '17';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);
			Test.stopTest();
		}
	}


	static testMethod void validUpdateMethodTest_equest() {
		Contact contact = new Contact();
		contact.firstname = 'Test';
		Contact.LastName = 'testing1';

		insert contact;

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.jbSourceId = 'JobServe';
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = '';
		atsCandidate.talentUpdate = 'no';
		atsCandidate.vendorId = '10';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = 'equest';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta@allegisgroup.com';
		emailDetails.workEmail = 'psammeta@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376920';
		atsPhoneDetails.phone = '1223344556';
		atsPhoneDetails.workPhone = '23232';
		atsPhoneDetails.otherPhone = '85858687890';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'false';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;
		atsCandidate.linkedInURL = 'https://www.linkedin.com/in/abc-efg-%E2%98%81-%E2%9A%A1-51a64b53/';


		//CreateTalentTestData.createCustomSettings();
		//User user = CreateTalentTestData.createUser('System Integration', 'Aerotek, Inc.', 'Application');
		User user = TestDataHelper.createUser('System Administrator');
		//insert user;

  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');
		
		System.runAs(user) {

			Test.startTest();

			// using mock
			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '19';
			atsCandidate.emailConsent = 'false';
			atsCandidate.textConsent = 'false';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '20';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '21';
			atsCandidate.emailConsent = '';
			atsCandidate.textConsent = '';
			results = Candidate.doPost(atsCandidate);

			Test.stopTest();
		}
	}
	// Test Candiate upsert
	static testMethod void updateCandidateWithSFIdTest() {

		Account talAcc = CreateTalentTestData.createTalent();
		Contact talCon = CreateTalentTestData.createTalentContact(talAcc);

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = talCon.Id;
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '9';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		//atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = 'pp';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta1@allegisgroup.com';
		emailDetails.workEmail = 'psammeta1@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta1@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376921';
		atsPhoneDetails.phone = '1223344551';
		atsPhoneDetails.workPhone = '232321';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'true';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;

		User user = TestDataHelper.createUser('System Administrator');
		insert user;
        
  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');
        
		System.runAs(user) {
			Test.startTest();


			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);
			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);
			Test.stopTest();


		}
	}

	static testMethod void testInvalidSecurityClearance() {


		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_222';
		atsCandidate.sfId = '';
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '8';
		atsCandidate.jobTitle = 'Sr Java Developer';
		//atsCandidate.securityClearance = 'Confidential Or R, invalid';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = 'pp';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta2@allegisgroup.com';
		emailDetails.workEmail = 'psammeta2@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta2@allegisgroup.com';
		atsCandidate.emailDetails = emailDetails;

		atsCandidate.vendorId = '11';
		atsCandidate.linkedInURL = 'https://www.linkedin1.com/in/phanisammeta';
		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '47023769202';
		atsPhoneDetails.phone = '12233445562';
		atsPhoneDetails.workPhone = '232322';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;

		//CreateTalentTestData.createCustomSettings();

		User user = TestDataHelper.createUser('System Administrator');
		//insert user;		
        
  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');
        
		System.runAs(user) {
			Test.startTest();


			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);
			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);
			Test.stopTest();
		}
	}
	static testMethod void testMethod2WithUserId() {
		Contact contact = new Contact();
		contact.firstname = 'Test';
		Contact.LastName = 'testing1';

		insert contact;

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = '';
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '18';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = '';
		atsCandidate.userId = UserInfo.getUserId();
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta@allegisgroup.com';
		emailDetails.workEmail = 'psammeta@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376920';
		atsPhoneDetails.phone = '1223344556';
		atsPhoneDetails.workPhone = '23232';
		atsPhoneDetails.otherPhone = '85858687890';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'false';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;
		atsCandidate.linkedInURL = 'https://www.linkedin.com/in/abc-efg-%E2%98%81-%E2%9A%A1-51a64b53/';


		//CreateTalentTestData.createCustomSettings();

		User user = TestDataHelper.createUser('System Administrator');
		insert user;

  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');

		System.runAs(user) {

			Test.startTest();

			// using mock
			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'false';
			atsCandidate.textConsent = 'false';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = '';
			atsCandidate.textConsent = '';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'true';
			atsCandidate.textConsent = 'true';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '8';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '9';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '11';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '17';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);
			Test.stopTest();
		}
	}

	static testMethod void testMethod2WithoutUserId() {
		Contact contact = new Contact();
		contact.firstname = 'Test';
		Contact.LastName = 'testing1';

		insert contact;

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = '';
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '18';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = '';
		atsCandidate.userId = '';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta@allegisgroup.com';
		emailDetails.workEmail = 'psammeta@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376920';
		atsPhoneDetails.phone = '1223344556';
		atsPhoneDetails.workPhone = '23232';
		atsPhoneDetails.otherPhone = '85858687890';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'false';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;
		atsCandidate.linkedInURL = 'https://www.linkedin.com/in/abc-efg-%E2%98%81-%E2%9A%A1-51a64b53/';


		//CreateTalentTestData.createCustomSettings();

		User user = TestDataHelper.createUser('System Administrator');
		insert user;

  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');

		System.runAs(user) {

			Test.startTest();

			// using mock
			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'false';
			atsCandidate.textConsent = 'false';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = '';
			atsCandidate.textConsent = '';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'true';
			atsCandidate.textConsent = 'true';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '8';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '9';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '11';
			results = Candidate.doPost(atsCandidate);
			atsCandidate.vendorId = '17';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);
			Test.stopTest();
		}
	}
	static testMethod void updateCandidateWithSFIdTest1() {

		Account talAcc = CreateTalentTestData.createTalent();
		Contact talCon = CreateTalentTestData.createTalentContact(talAcc);

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = talCon.Id;
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '17';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		//atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = 'pp';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta1@allegisgroup.com';
		emailDetails.workEmail = 'psammeta1@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta1@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376921';
		atsPhoneDetails.phone = '1223344551';
		atsPhoneDetails.workPhone = '232321';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'true';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;

		User user = TestDataHelper.createUser('System Administrator');
		insert user;
        
  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');
        
		System.runAs(user) {
			Test.startTest();


			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);
			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);
			Test.stopTest();


		}
	}
    
    static testMethod void validUpdateMethodTest2() {
		Contact contact = new Contact();
		contact.firstname = 'Test';
		Contact.LastName = 'testing1';

		insert contact;

		ATSCandidate atsCandidate = new ATSCandidate();
		atsCandidate.sourceId = 'CS_221';
		atsCandidate.sfId = '';
		atsCandidate.talentUpdate = 'yes';
		atsCandidate.vendorId = '8';
		atsCandidate.jobTitle = 'Sr Java Developer';
		// atsCandidate.securityClearance = 'Confidential Or R,';
		atsCandidate.workEligibility = 'I have the right to work for any employer';
		atsCandidate.highestEducation = 'Masters';
		atsCandidate.geoPrefComments = 'Fairfax,VA';
		atsCandidate.desiredRate = '100';
		atsCandidate.desiredSalary = '100000';
		atsCandidate.emailConsent = 'true';
		atsCandidate.textConsent = 'false';
		atsCandidate.transactionsource = 'PhenomCRM';
		ATSCandidate.ATSNameDetails nameDetails = new ATSCandidate.ATSNameDetails();

		nameDetails.firstName = 'SSaammeettaa';
		nameDetails.lastName = 'Pphhaannii';
		nameDetails.suffix = 'Mr';
		atsCandidate.nameDetails = nameDetails;

		ATSCandidate.ATSEmailDetails emailDetails = new ATSCandidate.ATSEmailDetails();
		emailDetails.email = 'psammeta@allegisgroup.com';
		emailDetails.workEmail = 'psammeta@allegisgroup.com';
		emailDetails.otherEmail = 'psammeta@allegisgroup.com';
		emailDetails.Email_Invalid = 'true';
		emailDetails.Other_Email_Invalid = 'true';
		emailDetails.Work_Email_Invalid = 'false';
		atsCandidate.emailDetails = emailDetails;

		ATSCandidate.ATSPhoneDetails atsPhoneDetails = new ATSCandidate.ATSPhoneDetails();
		atsPhoneDetails.mobile = '4702376920';
		atsPhoneDetails.phone = '1223344556';
		atsPhoneDetails.workPhone = '23232';
		atsPhoneDetails.otherPhone = '85858687890';
		atsPhoneDetails.Mobile_Phone_Invalid = 'true';
		atsPhoneDetails.Home_Phone_Invalid = 'false';
		atsPhoneDetails.Other_Phone_Invalid = 'false';
		atsPhoneDetails.Work_Phone_Invalid = 'false';
		atsCandidate.phoneDetails = atsPhoneDetails;

		ATSCandidate.ATSAddressDetails addressDetails = new ATSCandidate.ATSAddressDetails();

		addressDetails.street = 'Fairfax Ln';
		addressDetails.city = 'Fairfax';
		addressDetails.state = 'VA';
		addressDetails.country = 'USA';
		addressDetails.postalCode = '12222';
		atsCandidate.mailingAddress = addressDetails;
		atsCandidate.linkedInURL = 'https://www.linkedin.com/in/abc-efg-%E2%98%81-%E2%9A%A1-51a64b53/';


		//CreateTalentTestData.createCustomSettings();

		User user = TestDataHelper.createUser('System Administrator');
		insert user;

  		//create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
		Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
		OAuth_Token__c='123456788889',
		ClientID__c ='abe1234',
		Client_Secret__c ='hkbkheml',
		Service_Method__c='POST',
		Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
		Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials');

		System.runAs(user) {

			Test.startTest();

			// using mock
			Test.setMock(HttpCalloutMock.class, new ATSApplicationManagementMockCallout());
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			Map<String, String> results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'false';
			atsCandidate.textConsent = 'false';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = '';
			atsCandidate.textConsent = '';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '6';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			atsCandidate.vendorId = '14';
			atsCandidate.emailConsent = 'true';
			atsCandidate.textConsent = 'true';
			results = Candidate.doPost(atsCandidate);

			System.debug(' Canidate Upsert Testing using Mock ' + results);
			System.assert(results != null);

			Test.stopTest();
		}
	}


}