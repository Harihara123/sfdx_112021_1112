public class CareersitApplicationDetailTriggerHandler  {



   public void OnAfterInsert(List<Careersite_Application_Details__c> newRecords) {
       callDataExport(newRecords);

    }


     
    public void OnAfterUpdate (List<Careersite_Application_Details__c> newRecords) {        
       callDataExport(newRecords);

    }




    private static void callDataExport(List<Careersite_Application_Details__c> newRecords) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'CareersitApplicationDetailTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' Careersite_Application_Details__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Careersite_Application_Details__c');
        }

    }

}