public class CaseTriggerHandler {
    private static boolean hasBeenProccessed = false;


    public void CaseTriggerHandler() {

    }

    public void handleCaseBeforeInsert(List<Case> cases) {
        try {
            //Make sure it's a communities case
            Set<Id> contactIdSet = new Set<Id>();
            Map<Id,User> contactIdUserMap = new Map<Id,User>();
            RecordType rt = [SELECT Id, Name, DeveloperName, sObjectType FROM RecordType WHERE DeveloperName = 'Talent' AND SobjectType = 'Case' LIMIT 1];
            for(Case c : cases){
               if(c.ContactId != null){
                    contactIdSet.add(c.ContactId);
                }
            }
            for(User userObj : [SELECT Id,Profile_Name__c,Contact.Id,   
                            ProfileId, 
                            Contact.Account.Candidate_Status__c,
                            Contact.Account.Talent_Recruiter__r.Id,
                            Contact.Account.Talent_Recruiter__r.IsActive,
                            Contact.Account.Talent_Account_Manager__r.Id,
                            Contact.Account.Talent_Account_Manager__r.IsActive,
                            Contact.Account.Talent_Account_Manager__r.Email,
                            Contact.Account.Talent_Community_Manager__r.Id,
                            Contact.Account.Talent_Community_Manager__r.IsActive,
                            Contact.Account.Talent_CSA__r.Id,
                            Contact.Account.Talent_CSA__r.IsActive,
                            Contact.Account.Talent_Biller__r.Id,
                            Contact.Account.Talent_Biller__r.IsActive,
                            Contact.Account.Talent_Program_Manager__r.Id,
                            Contact.Account.Talent_Program_Manager__r.IsActive,
                            Contact.Account.Talent_Delivery_Manager__r.Id,
                            Contact.Account.Talent_Delivery_Manager__r.IsActive,
                            Contact.Account.Talent_Community_Rep__r.Id,
                            Contact.Account.Talent_Community_Rep__r.IsActive,
                            Contact.Account.Talent_Employee_Service_Specialist__r.Id,
                            Contact.Account.Talent_Employee_Service_Specialist__r.IsActive
                         
                                FROM User WHERE Contact.Id IN: contactIdSet]){
                contactIdUserMap.put(userObj.Contact.Id,userObj);
            }
            List<String> contactIds = new List<String>();
            for (Case c : cases) {
                 
                if (c.RecordTypeId == rt.Id) {
                    User u = contactIdUserMap.get(c.ContactId);
                    if (!String.isBlank(u.Id)) {
                        //Assign Ownership
                        if (c.Type == 'Retention' || c.Type == 'Address Change' || c.Type == 'End Date Review' || c.Type == 'Job Inquiry' || c.Type == 'Job search' || c.Type == 'Name Change' || c.Type == 'Recruiter Change' || c.Type == 'Refer a friend' || c.Type == 'Biller Change' || c.Type == 'NPS' || c.Type == 'Ask A Question' || c.Type == 'IR35 Question') {
                            System.debug('TC_CaseAssignmentTrigger: UserId: ' + u.Id);
                            Id newOwner = TC_CaseAssignment.GetCaseOwner(u.Id, c.Type,u);
                            if (!String.isBlank(newOwner)) {
                                System.debug('TC_CaseAssignmentTrigger: New Owner Id: ' + newOwner);
                                c.OwnerId = newOwner;
                            }
                        }
                        if(c.Type == 'NPS' && c.Sub_Type__c !='Pulse Check Middle Assignment' && c.Sub_Type__c !='Pulse Check Onboarding' && c.Sub_Type__c !='NPS Offboarding'){
                            List<VOC__c> voclst = [Select id, Consultant_recruiter__c, Consultant_Account_Manager__c from Voc__c where contact__c =: c.ContactId Order By Createddate DESC];
                            if(!voclst.isEmpty() && voclst.size()>0){
                                String RecruiterId =  voclst[0].Consultant_recruiter__c;
                                String AccountManagerId =  voclst[0].Consultant_Account_Manager__c;
                                if(!String.isBlank(RecruiterId)){
                                    c.OwnerId = id.ValueOf(RecruiterId);    
                                }/*else if(!String.isBlank(AccountManagerId)){
                                    c.OwnerId = id.ValueOf(AccountManagerId);
                                }*/
                                
                            }
                        }
                    }
                    
                    if (!String.isBlank(u.Profile_Name__c) && (String.valueOf(u.profile_Name__c).contains('TekSystem') || String.valueOf(u.profile_Name__c).contains('TEKsystem')) && !String.valueOf(u.profile_Name__c).contains('EMEA') 
                       && ((c.Type == 'NPS' && (c.Sub_Type__c == 'Onboarding' || c.Sub_Type__c == 'Offboarding' || c.Sub_Type__c == '60 Day'))	|| c.Type == 'End Date Review' || c.Type == 'Job Inquiry' 
                           || c.Type == 'Job search' || c.Type == 'Refer a friend')) {
                               if(u.Contact.Account.Candidate_Status__c =='Current' && (c.Type == 'Job Inquiry' || c.Type == 'Job search')){
                           			c.Account_Manager_Email__c = u.Contact.Account.Talent_Account_Manager__r.Email;
                               }else if(c.Type != 'Job Inquiry' && c.Type != 'Job search'){
                               		c.Account_Manager_Email__c = u.Contact.Account.Talent_Account_Manager__r.Email;                                   
                               }
                    }
                    
                    
                    if(!String.isBlank(u.Profile_Name__c) && (String.valueOf(u.profile_Name__c).contains('TekSystem') || String.valueOf(u.profile_Name__c).contains('TEKsystem'))){
                        c.Account_Ownership__c = 'Teksystems Community';
                    }else if(!String.isBlank(u.Profile_Name__c) && String.valueOf(u.profile_Name__c).contains('Aerotek')){
                         c.Account_Ownership__c = 'Aerotek Community';
                    }else if(!String.isBlank(u.Profile_Name__c) && String.valueOf(u.profile_Name__c).contains('Aston')){
                        c.Account_Ownership__c = 'AstonCarter Community';
                    }else if(!String.isBlank(u.Profile_Name__c) && String.valueOf(u.profile_Name__c).contains('EASi')){
                        c.Account_Ownership__c = 'Aerotek Community';
                    }
                }
                //For Office/Region
                if(c.ContactId != null){
                    contactIds.add(c.ContactId);
                }
                //After Communities Loop

            }
            List<User> users = [SELECT Id, ContactId, Office__c, Region__c FROM User WHERE ContactId in :contactIds];
            System.debug('1');
            System.debug(users);
            for (Case c : cases) {
                for (User u : users) {
                    if (c.ContactId == u.ContactId) {
                        System.debug('2');
                        System.debug(u.Office__c);
                        System.debug(u.Region__c);
                        
                        c.Office_Talent__c = u.Office__c;
                        c.Region_Talent__c = u.Region__c;
                    }
                }
            }
        }
        catch (Exception ex) {
            System.debug('TC_CaseAssignmentTrigger Error: ' + ex.getMessage());
        }
    }

    public void handleCaseAfterInsert(List<Case> cases) {
        try {
            //Create List for bulkification
            List<Id> userIds = new List<Id>();
            Map<Id, Id> userCaseIds = new Map<Id, Id>();
            Map<Id,User> contactIdUserMap = new Map<Id,User>();
            Set<Id> contactIdSet = new Set<Id>();
            RecordType rt = [SELECT Id, Name, DeveloperName, sObjectType FROM RecordType WHERE DeveloperName = 'Talent' AND SobjectType = 'Case' LIMIT 1];
            for(Case c : cases){
                if(c.ContactId != null){
                    contactIdSet.add(c.ContactId);
                }

            }
           for(User userObj : [SELECT Id,Profile_Name__c,Contact.Id  FROM User WHERE Contact.Id IN: contactIdSet]){
                contactIdUserMap.put(userObj.Contact.Id,userObj);
            }
            
            for (Case c : cases) {
                //Communities case
                
                if (c.RecordTypeId == rt.Id) {
                    User u = contactIdUserMap.get(c.ContactId);
                    if (!String.isBlank(u.Id)) {
                        userIds.add(u.Id);
                        userCaseIds.put(u.Id, c.Id);
                    }
                }
                //After Communities Loop
            }


            //Update Event_Log_Item__b
            if (userIds.size() > 0) {// &&  !System.isBatch()
                TC_EventHandler.InsertEventLog(userIds, userCaseIds);
            }
        }
        catch (Exception ex) {
            System.debug(System.isBatch()+'TC_CaseAssignmentTrigger Error: ' + ex.getMessage());
        }
    }
    public void  handleCaseAfterUpdate(List<Case> caseList,Map<Id, Case>oldMap, Map<Id, Case>newMap){

    }
    public void handleActivityCreation(List<Case> caseList,Map<Id, Case>oldMap, Map<Id, Case>newMap,Boolean isAfterInsert){
        System.debug('handleCaseAfterUpdate');
        String newCases = json.serialize(newMap.values());
        String oldCases = '';
        if(!isAfterInsert){
            oldCases = json.serialize(oldMap.values());
        }
        ActivityHelper.createTasks(oldCases,newCases,isAfterInsert);
    }

    public void syncToGCP(List<Case> syncList) {
        if (!Test.isRunningTest() && syncList != null && syncList.size() > 0) {
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'CaseTriggerHandler', 'syncToGCP', 
                        'Triggered ' + syncList.size() + ' Case records: ' + CDCDataExportUtility.joinObjIds(syncList));
                }
                PubSubBatchHandler.insertDataExteractionRecord(syncList, 'Case');
                hasBeenProccessed = true; 
            }
        }
    }

}