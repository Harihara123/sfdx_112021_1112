/**
 * An apex page controller that exposes the change password functionality
 */
public class ChangePasswordController {

    public Boolean displayInputputText{get;set;}
    public Boolean chkBx{get;set;}

    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}
    
    public PageReference changePassword() {
        //return Site.changePassword(newPassword, verifyNewPassword,oldPassword);
        Boolean redirectUser = true;
        PageReference pageRef = Site.changePassword(newPassword, verifyNewPassword,oldPassword);
        if(redirectUser){
           pageRef = getCareerBuilderSite();
        }
        return pageRef;
    }
    
    public ChangePasswordController() {
  
    }

    public PageReference getCareerBuilderSite(){
        PageReference pageRef= null;
        try{
            String userName = UserInfo.getUserName();
            //String userOauthDetails = getUserOauthTokens(userName,newPassword);
            //pageRef = new PageReference('https://hidden-dawn-35695.herokuapp.com?userDetails=' + userOauthDetails);
            String cbLoginURL = 'https://aerotek.herokuapp.com';
                
            pageRef = new PageReference(cbLoginURL);
        }catch(Exception e){
            
        }        
        return pageRef;
    }

    public PageReference click(){  
        
         if(chkBx){
             displayInputputText = true;
         }
         else{
             displayInputputText = false;
         }
         return null;
     }



}