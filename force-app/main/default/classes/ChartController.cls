public class ChartController {
    Public Opportunity opp;
    public string Oppid{get;set;}
    Public Boolean proactiv{get;set;}
   
public ChartController(ApexPages.StandardController stdController) {
this.Opp = (Opportunity)stdController.getRecord();
List<Opportunity> opp1=[select id, StageName,Division__c,Impacted_Regions__c,Location_Details__c, Location__c,Req_Created_from_Proactive__c,Originating_Opportunity__c from Opportunity where id =:opp.id];
   PageReference pageRef = ApexPages.currentPage();

   
       system.debug('this'+ opp);
       if(opp1!=null){
       
       if(Opp1[0].Req_Created_from_Proactive__c == 'Created as Won'){
       proactiv=true;
           
           Oppid=opp1[0].Originating_Opportunity__c;
           
           pageRef.getParameters().put('id', opp1[0].Originating_Opportunity__c);
           }
       else{
           Oppid=opp1[0].id;
           pageRef.getParameters().put('id', opp1[0].id);
           proactiv=false;
       }
       
   }  
}
}