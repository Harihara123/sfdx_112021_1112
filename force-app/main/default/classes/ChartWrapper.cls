Public with sharing class ChartWrapper {
        @AuraEnabled
        public Decimal FillRatio{get;set;}
        @AuraEnabled
        public String WentDirectClient{get;set;}
        @AuraEnabled
        public Decimal ProducerSpread{get;set;}
        @AuraEnabled
        public Decimal OpCoSpread{get;set;}
        @AuraEnabled
        public Decimal GlobalAccountSpread{get;set;} 
}