/***************************************************************************************************************************************
* Name        - CheckIfValidId 
* Description - This class checks if the Salesforce/Siebel Account Ids of the Accounts 
*               to be merged are valid and present in Salesforce                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       11/12/2012             Created
*****************************************************************************************************************************************/
global class CheckIfValidId {
    
    //Declare Variables
    public String str1;
    public String str2;
    String QueryAccounts;
    public Account SFWinningAccount = new Account();
    public Account SBWinningAccount = new Account();
    public Account SFLosingAccount = new Account();
    public Account SBLosingAccount = new Account();   
    public Map<Id, Account> AccountIdMap = new Map<Id, Account>();
    
  
    //Constructor
    public CheckIfValidId(String SF_MasterAccountId,String SF_AccountToMergeId,String SB_MasterAccountId,String SB_AccountToMergeId)
    {        
        //Form the Query String               
        string rcdtypeid = string.valueof(label.Account_Client_RecordTypeID);
        QueryAccounts = 'SELECT Id, Siebel_ID__c FROM Account WHERE (Id = :SF_MasterAccountId OR Id = :SF_AccountToMergeId OR Siebel_ID__c = :SB_MasterAccountId OR Siebel_ID__c = :SB_AccountToMergeId) AND recordtypeid=:rcdtypeid '; //Added filter to query CRM accounts only - Nidish
        
        //Populate the query result in a map
        AccountIdMap = new Map<Id, Account>((List<Account>)Database.query(QueryAccounts)); 
        
        // Iterate over the list of records in the map 
        for (Id id : AccountIdMap.keySet()) {
          
          System.debug('=== ' + AccountIdMap.get(id).Id);
          System.debug('=== ' + AccountIdMap.get(id).Siebel_ID__c);
        
          //If the Siebel_ID__c on the Account record is not null  
          if(AccountIdMap.get(id).Siebel_ID__c != Null)
          { 
              //If the Siebel_ID__c is equal to the Siebel Winning account Id string
              if(AccountIdMap.get(id).Siebel_ID__c == SB_MasterAccountId)
              {
                 SBWinningAccount = AccountIdMap.get(id);
                 system.debug('Winning Account SB Id:' + AccountIdMap.get(id));
              }
              //If the Siebel_ID__c is equal to the Siebel Losing account Id string
              if(AccountIdMap.get(id).Siebel_ID__c == SB_AccountToMergeId)
              {
                 SBLosingAccount = AccountIdMap.get(id);
                 system.debug('Losing Account SB Id:' + AccountIdMap.get(id));
              }          
           }
           //If the Siebel_ID__c on the Account record is null
           else
           {
              //If the Id on the Account record is not null
              if(AccountIdMap.get(id).Id != Null)
              {  
                  //If the Id is equal to the Salesforce Winning account Id string
                  if(AccountIdMap.get(id).Id == SF_MasterAccountId)
                  {
                     SFWinningAccount = AccountIdMap.get(id);
                     system.debug('Winning Account SF Id:' + AccountIdMap.get(id));
                  }
                  
                  //If the Id is equal to the Salesforce Losing account Id string
                  if(AccountIdMap.get(id).Id == SF_AccountToMergeId)
                  {
                     SFLosingAccount = AccountIdMap.get(id);
                     system.debug('Losing Account SF Id:' + AccountIdMap.get(id));
                  }
                  
               }    
             
            }
           
        }                                 
        
        //Get the Winning Account Id as a String                   
        str1 = ReturnWinningAccountId(SF_MasterAccountId,SB_MasterAccountId,SF_AccountToMergeId,SB_AccountToMergeId);
        system.debug('str1:' + str1);
        
        //Get the Losing Account Id as a String
        str2 = ReturnLosingAccountId(SF_MasterAccountId,SB_MasterAccountId,SF_AccountToMergeId,SB_AccountToMergeId);
        system.debug('str2:' + str2);      
    }   

   /*
    * Name: CheckIfValid
    * Description: Method to check if the id is a Valid Salesforce Account Id
    * Parameters:None 
    * Return Type: Boolean
    */
   public Boolean CheckIfValid() {
        
        //Declare Boolean variable
        Boolean isValid;               
        system.debug('str1:' + str1);
        system.debug('str2:' + str2);
        //If str1 is not null and str2 is not null then set Boolean equal to true
        If(str1 != '' && str2 != '')
        {
          system.debug('Return True');
          isValid = True;          
          
        }
        //If str1 is null or str2 is null then set Boolean equal to false
        else
        {
          system.debug('Return False');
          isValid = False;          
          
        } 
        
        //Return Boolean  
        return isValid; 
   }
   
   /*
    * Name: isValidSFId
    * Description: Method to check if the id is a Valid Salesforce Account Id
    * Parameters:
    *    String s: String variable that needs to be validated as a Salesforce Account Id
    * Return Type: Boolean
    */
    public Boolean isValidSFId(String s) {
         
         //Declare Boolean variable
         Boolean isValid;
         
         //If length of SF Id string is 15 or 18 charachters and it starts with '001' 
         //then set Boolean return value = True else False       
         If((s.length() == 15 || s.length() == 18) && s.left(3) == '001')
         {              
           isValid = True;
         } 
         else 
         {
           isValid = False;
         }  
         
         //Return Boolean 
         return isValid;
   }
      
   /*
    * Name: ReturnWinningAccountId
    * Description: Method to check if the Salesforce Account Ids and Siebel Account Ids  
    *              are null/not null and return the Winning Account Ids as strings
    * Parameters:
    *    String SF_MasterAccountId: Winning Account Salesforce Id   
    *    String SB_MasterAccountId: Winning Account Siebel Id   
    * Return Type: String
    */   
    
    public string ReturnWinningAccountId(String SF_MasterAccountId,String SB_MasterAccountId, String SF_AccountToMergeId,String SB_AccountToMergeId) {
       //Declare a String
       String strLogMessage;
       //Declare a list of object Log__c
       List<Log__c> errors = new List<Log__c>();
       
       //Declare a string return variable
       String str_MasterAccountId;
       
       //Check if the Siebel Winning Account Id is not null
       if(SB_MasterAccountId != Null)
       {                      
           //Check if Winning Account's Siebel Id was present on the Salesforce record           
           if(string.valueOf(SBWinningAccount.Siebel_ID__c) != Null)
            { 
              //Assign the string value of the Winning Account's Id to the Return string variable            
              str_MasterAccountId = string.valueOf(SBWinningAccount.Id);              
              
            }
           else
            {   
               // Siebel_ID__C field value on Winning Account record is Null 
               //If Siebel Id not found in Salesforce then check if the Winning Account's Salesforce Id is valid and not null and is present in Salesforce               
               if(SF_MasterAccountId != Null && isValidSFId(SF_MasterAccountId) && AccountIdMap.get(SFWinningAccount.Id) != Null)
               {
                     //Assign the value of the Winning Account's Salesforce Id to the Return string variable
                     str_MasterAccountId =   SF_MasterAccountId;                     
               }
               else
               {     
                     //For Winning Account: Siebel Account Id is not present in Salesforce and 
                     //Salesforce Account Id is null/not valid/does not exist in Salesforce
                     //Dump an Error in Log__c object
                     strLogMessage = '\nSiebel Winning Account Id : '+SB_MasterAccountId+'\nSiebel Losing Account Id : '+SB_AccountToMergeId;
                     errors.add(Core_Log.CustomException('Winning Account('+SB_MasterAccountId+') is not present in Salesforce to perform merge. Please ensure Account #'+SB_MasterAccountId+' is present in Salesforce before initiating a manual merge to fix this error. Refer to the details below\n'+ strLogMessage));

                     //check if there are error records. 
                     if(errors.size() > 0) { 
        
                     //insert the error records.              
                     database.insert(errors,false);  
                     
                     } 
                     
                     //Assign Null value to Return String variable                   
                     str_MasterAccountId = '';
               }
               
            }

       }

       else
       {
               //Siebel_ID__c of the Winning Account is Null          
               //If Siebel Id is null then check if the Winning Account's Salesforce Id is valid and not null and is present in Salesforce            
               if(SF_MasterAccountId != Null && isValidSFId(SF_MasterAccountId) && AccountIdMap.get(SFLosingAccount.Id) != Null)
               {
                     //Assign the Salesforce Account Id to the Return string variable
                     str_MasterAccountId = SF_MasterAccountId;
                     
               }
               else
               {     //For Winning Account: Siebel Account Id is null and Salesforce Account Id is not valid/null
                     //Dump an Error in Log__c object
                     strLogMessage = '\nSiebel Winning Account Id : '+SB_MasterAccountId+'\nSiebel Losing Account Id :'+SB_AccountToMergeId;
                     errors.add(Core_Log.CustomException('Winning Account('+SB_MasterAccountId+') is null. Please ensure Winning Account Siebel Id is not null before initiating a manual merge to fix this error. Refer to the details below\n'+ strLogMessage));

                     
                     //check if there are error records. 
                     if(errors.size() > 0) { 
        
                     //insert the error records.              
                     database.insert(errors,false);  
                     
                     }  
                     
                     //Assing Null value to Return String variable              
                     str_MasterAccountId = '';

               }

        }   
        
        //Return string variable
        return str_MasterAccountId;           
   }
   
   /*
    * Name: ReturnLosingAccountId
    * Description: Method to check if the Salesforce Account Ids and Siebel Account Ids  
    *              are null/not null and return the Losing Account Ids as strings
    * Parameters:
    *    String SF_AccountToMergeId: Losing Account Salesforce Id   
    *    String SB_AccountToMergeId: Losing Account Siebel Id 
    * Return Type: String  
    */   
    
    public string ReturnLosingAccountId(String SF_MasterAccountId,String SB_MasterAccountId,String SF_AccountToMergeId,String SB_AccountToMergeId) {
       
       //Declare a list of Log__c object
       List<Log__c> errors = new List<Log__c>();
       
        //Declare a String
       String strLogMessage;
       
       //Declare a string variable
       String str_AccountToMergeId;
       
       //Check if the Siebel Losing Account Id is not null
       if(SB_AccountToMergeId != Null)
       {
            //If Losing Account's Siebel Id is not null 
            //Then assign the Losing Account Id to the string variablefor Losing Account Id
            if(string.valueOf(SBLosingAccount.Siebel_ID__c) != Null)
            {            
              str_AccountToMergeId = string.valueOf(SBLosingAccount.Id);
              
            }
           else
            {
               //If Siebel Id not found in Salesforce then check if the Losing Account's Salesforce Id is valid and not null and is present in Salesforce
               if(SF_AccountToMergeId != Null && isValidSFId(SF_AccountToMergeId) && AccountIdMap.get(SFLosingAccount.Id) != Null)
               {
                     str_AccountToMergeId =  SF_AccountToMergeId;                     
               }
               else
               {
                     //If the Siebel Account Id is not present in Salesforce and 
                     //Salesforce Account Id is null/not valid/not present in Salesforce
                     //Dump an Error in Log__c object
                     strLogMessage = '\nSiebel Winning Account Id : '+SB_MasterAccountId+'\nSiebel Losing Account Id : '+SB_AccountToMergeId;
                     errors.add(Core_Log.CustomException('Losing Account('+SB_AccountToMergeId+') is not present in Salesforce to perform merge. Please ensure Account #'+SB_AccountToMergeId+' is present in Salesforce before initiating a manual merge to fix this error. Refer to the details below\n'+ strLogMessage));
                     
                     //check if there are error records. 
                     if(errors.size() > 0) { 
        
                     //insert the error records.              
                     database.insert(errors,false);  
                     
                     } 
                     
                     //Assign null value to return Id string variable
                     str_AccountToMergeId = '';
               }
               
            }
       }
       else
       {       //Siebel Id of the Account is Null  
          
               //If Siebel Id is null then check if the Losing Account's Salesforce Id is valid and not null
               if(SF_AccountToMergeId != Null && isValidSFId(SF_AccountToMergeId) && AccountIdMap.get(SFLosingAccount.Id) != Null)
               {
                     str_AccountToMergeId =  SF_AccountToMergeId;                     
               }
               else
               {     //For Losing Account: Siebel Account Id is null and Salesforce Account Id 
                     //is not valid/null/not present in Salesforce
                     //Dump an Error in Log__c object
                     
                     strLogMessage = '\nSiebel Winning Account Id : '+SB_MasterAccountId+'\nSiebel Losing Account Id : '+SB_AccountToMergeId;
                     errors.add(Core_Log.CustomException('Losing Account('+SB_AccountToMergeId+') is null. Please ensure Losing Account Siebel Id is not null before initiating a manual merge to fix this error. Refer to the details below\n'+ strLogMessage));
                     
                     //check if there are error records. 
                     if(errors.size() > 0) { 
        
                     //insert the error records.              
                     database.insert(errors,false);  
                     
                     }
                     
                     //Assign Null to return string variable
                     str_AccountToMergeId = '';
               }

       } 
       
       //Return Losing Account String Id
       return str_AccountToMergeId;   
   
   }
   
}