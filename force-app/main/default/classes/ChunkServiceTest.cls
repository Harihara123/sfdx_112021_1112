@isTest
public class ChunkServiceTest {
    static testMethod void test_methodOne() {
        String n='Test';
        ChunkService.base62decode(n);
        System.assert(n=='Test');
    }
    static testMethod void test_methodTwo() {
        Long a=8;
        ChunkService.base62encode(a);
        System.assert(a==8);
    }
    static testMethod void test_methodThree() {
         Long a=0;
    ChunkService.base62encode(a);
          System.assert(a==0);
    }
    static testMethod void test_methodFour() {
        String start='T97608';
        String endi='76965d';
        Integer size=2;
        ChunkService.chunkIdRange(start,endi,size);
        System.assert(start=='T97608');
        System.assert(endi=='76965d');
        System.assert(size==2);
    }
    static testMethod void test_methodFive() {
        Long first=10;
        Long last=2;
        Integer total=1;
        Integer size=2;
        ChunkService.chunkRange(first,last,total,size);
        System.assert(first==10);
        System.assert(last==2);
        System.assert(total==1);
        System.assert(size==2);
    }
    static testMethod void test_methodSix() {
        Long first=10;
        Long last=40;
        Integer total=1;
        Integer size=2;
        ChunkService.chunkRange(first,last,total,size);
        System.assert(first==10);
        System.assert(last==40);
        System.assert(total==1);
        System.assert(size==2);
    }
    
    static testMethod void test_methodSeven() {
        String passed='a3f45qq456';
        ChunkService.drop_leading_zeros(passed);
        System.assert(passed=='a3f45qq456');
    }
    static testMethod void test_methodEight() {
        String passed='0';
        ChunkService.drop_leading_zeros(passed);
        System.assert(passed=='0');
    }
    static testMethod void test_methodNine() {
        String passed='000090';
        ChunkService.drop_leading_zeros(passed);
        System.assert(passed=='000090');
    }
    
    static testMethod void test_methodTen() {
        String input='Test';
        Integer total=10;
        long input1=1;
        ChunkService.prefixLongWithZeros(input1,total);
        ChunkService.prefixStringWithZeros(input,total);
        System.assert(input=='Test');
        System.assert(input1==1);
        System.assert(total==10);             
    }
}