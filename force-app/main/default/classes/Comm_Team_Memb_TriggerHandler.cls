/***************************************************************************
Name        : Comm_Team_Memb_TriggerHandler
Created By  : JFreese (Appirio)
Date        : 19 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : Class that contains all of the functionality called by the
              Comm_Team_Memb_Trigger. All contexts should be in this class.
*****************************************************************************/

public with sharing class Comm_Team_Memb_TriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    Map<Id, Commission_Team_Member__c> oldMap = new Map<Id, Commission_Team_Member__c>();

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Commission_Team_Member__c>newRecs) {
        CommissionTeamMember_ValidateTotalCommission(beforeInsert, oldMap, newRecs);
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, Commission_Team_Member__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Commission_Team_Member__c>oldMap,
                               Map<Id, Commission_Team_Member__c>newMap) {
        List<Commission_Team_Member__c> newRecs = newMap.values();
        CommissionTeamMember_ValidateTotalCommission(beforeUpdate, oldMap, newRecs);
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Commission_Team_Member__c>oldMap,
                               Map<Id, Commission_Team_Member__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Commission_Team_Member__c>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Commission_Team_Member__c>oldMap) {

    }



    /***************************************************************************************************************************************
    * Original Header: trigger CommissionTeamMember_ValidateTotalCommission on Commission_Team_Member__c (before insert,before update) {
    *****************************************************************************************************************************************/
    public void CommissionTeamMember_ValidateTotalCommission (String Context,
                                                              Map<Id, Commission_Team_Member__c>oldMap,
                                                              List<Commission_Team_Member__c>TriggerNew)
    {
        Map<string,List<Commission_Team_Member__c>> opportunityMap = new Map<string,List<Commission_Team_Member__c>>();
        set<string> teamMembersUpdated = new set<string>();

        for(Commission_Team_Member__c member : TriggerNew)
        {
            boolean addToList = true;
            if(!Context.equals(beforeInsert))
            {
                if(member.Commission__c == oldMap.get(member.Id).Commission__c && member.Opportunity_Name__c == OldMap.get(member.Id). Opportunity_Name__c)
                    addToList = false;
                teamMembersUpdated.add(member.Id);
            }
            if(addToList)
            {
                if(opportunityMap.get(member.Opportunity_Name__c) != Null)
                    opportunityMap.get(member.Opportunity_Name__c).add(member);
                else
                {
                    List<Commission_Team_Member__c> teamMembers = new List<Commission_Team_Member__c>();
                    teamMembers.add(member);
                    opportunityMap.put(member.Opportunity_Name__c,teamMembers);
                }
            }
        }
        // query the associated opportunities
        for(Opportunity opp : [select Name,(select Commission__c,Opportunity_Name__c from Commission_Team_Members__r) from Opportunity where id in :opportunityMap.keyset() limit 50000])
        {
            double existingCommission = 0;
            List<Commission_Team_Member__c> membersInContext = new List<Commission_Team_Member__c>();
            // process the existing commission team member records
            for(Commission_Team_Member__c member : opp.Commission_Team_Members__r)
            {
                   // do not add the commission in case of update
                if(teamMembersUpdated.contains(member.Id))
                   continue;
                else
                   existingCommission += member.Commission__c;
            }
            //commission should not be greater than 100%
            if(existingCommission < 100)
            {
                for(Commission_Team_Member__c member : OpportunityMap.get(opp.Id))
                {
                    // add the commission for records in context
                    existingCommission += member.Commission__c;
                    if(existingCommission <= 100)
                        continue;
                    else
                    {
                        membersInContext.add(member);
                        // remove the commision added to validate next record
                        existingCommission -= member.Commission__c;
                    }
                }
                // add the error message accordingly
                if(membersInContext.size() > 0)
                    addErrorMessages(membersInContext);
            }
            else
               addErrorMessages(OpportunityMap.get(opp.Id));
        }
    }
	@TestVisible
    //Method which was contained within the former trigger
    void addErrorMessages(List<Commission_Team_Member__c> members)
    {
        for(Commission_Team_Member__c member : members)
            member.addError(Label.CommissionTeamMember_ErrorMessage);
    }
}