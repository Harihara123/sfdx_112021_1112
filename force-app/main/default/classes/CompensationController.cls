public class CompensationController {
    Public Opportunity opp;
    public Boolean flag{get;set;}
   
public CompensationController(ApexPages.StandardController stdController) {
this.Opp = (Opportunity)stdController.getRecord();
   string Oppid = this.opp.name;
       system.debug('this'+ opp);
       
       if(UserInfo.getUiThemeDisplayed()=='Theme4d'){
           flag=true;
       }
       else{
           flag=false;
       }
     
}
    
    public Flow.Interview.Compensation_Flow myflow { get; set; }    
    
    public String getvar() {
      String stage;
      if (myFlow==null) { return 'Home';}
      else stage = (String) myFlow.getVariableValue('Vstage');
        system.debug('stage'+stage);

      return(stage==null ? 'Home': stage);

   }


Public PageReference getFinishPage(){
    system.debug('stage'+getvar());
    system.debug(ApexPages.CurrentPage().GetParameters());
    map<string,Routing_Controller_Settings__c> RoutingControllerSettings = Routing_Controller_Settings__c.getAll();
    string Oppname = opp.Name;
    string Recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Compensation').getRecordTypeId();
    String RegionsTobeIncluded = RoutingControllerSettings.get('RegionsTobeIncluded').FieldID__c;
    String LocationTobeIncluded = RoutingControllerSettings.get('LocationTobeIncluded').FieldID__c;
    String DivisionTobeIncluded = RoutingControllerSettings.get('DivisionTobeIncluded').FieldID__c;
    String Opportunityid = RoutingControllerSettings.get('Opportunityid').FieldID__c;
    String CompensationURL = RoutingControllerSettings.get('CompensationURL').FieldID__c;
    
    
    string oppid = opp.id;
    string url;
   List<Opportunity> opp1=[select StageName,Division__c,Impacted_Regions__c,Location_Details__c, Location__c from Opportunity where id =:oppid];
   
    
    if(opp1[0].stagename=='Closed Won'&& UserInfo.getUiThemeDisplayed()!='Theme4d'){
        
       
url=CompensationURL+Opportunityid+'='+Oppname+'&RecordType='+Recordtypeid+'&'+RegionsTobeIncluded+'='+opp1[0].Impacted_Regions__c+'&'+LocationTobeIncluded+'='+opp1[0].Location__c+'&'+DivisionTobeIncluded+'='+opp1[0].Division__c;
    }
    else {
        url='/'+oppid;
    }
    
    PageReference p ;   
    p = new PageReference(Url);
p.setRedirect(true);
return p;
}
    
    
}