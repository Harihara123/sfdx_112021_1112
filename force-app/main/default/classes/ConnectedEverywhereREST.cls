/**
* @author Peter Beazer
* @description Primary REST Class to handle Connected Everywhere Chrome Extension interactions
*/
@RestResource(urlMapping='/ConnectedEverywhere/*')
global with sharing class ConnectedEverywhereREST {
    /**
    * @description Get User Details
    * @return HTTP Response containing map of user details
    */ 
    @HttpGet
    global static void doGet(){
        Map<String,String> m = ConnectedEverywhere.GetUserDetails();
        
        RestResponse response = RestContext.response;
        response.statusCode = 200;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(m));
    }
    /**
    * @description Request CE FindPerson or Perform Record Updates (JobTitle, LI Url, Location, etc)
    * @return HTTP Response containing Contact Information (for FindPerson) or Ok response (All others)
    */ 
    @HttpPost
    global static void doPost(){     
        RestResponse response = RestContext.response;
        try{
            String body = RestContext.request.requestBody.toString();
            if(body != null){ 
                body = JSON.serializePretty(JSON.deserializeUntyped(body));
            }
            
            ConnectedEverywhere.Request req = (ConnectedEverywhere.Request)JSON.deserialize(body, ConnectedEverywhere.Request.class);
            
            if(req.requestType == 'FindPerson'){
                response.responseBody = Blob.valueOf(JSON.serialize(ConnectedEverywhere.getPerson(req)));
            }else if(req.requestType == 'GetAutoMatchReqs'){
                response.responseBody = Blob.valueOf(ConnectedEverywhere.getMatchedReqs(req));
            }else if(req.requestType == 'PutCEUpdateJT') {
                if(req.id != null) {
                    if (req.updateValue != null) {
                        List<Contact> cList = getContacts(req.id);
                        if (!cList.isEmpty()) {
                            Contact queriedContact = cList[0];
                            queriedContact.Title = req.updateValue;
                            update queriedContact;
                            response.responseBody = Blob.valueOf('Job Title Updated');
                        } else {
                            response.responseBody = Blob.valueOf('Contact Not Found. Possibly Deleted!');
                        }
                    } else {
                        response.responseBody = Blob.valueOf('No Job Title Specified');
                    }
                } else {
                    response.responseBody = Blob.valueOf('No Contact Id Specified');
                }
            }else if(req.requestType == 'PutCEUpdateLI') {
                if(req.id != null) {
                    if (req.updateValue != null) {
                        List<Contact> cList = getContacts(req.id);
                        if (!cList.isEmpty()) {
                            Contact queriedContact = cList[0];
                            if (queriedContact.LinkedIn_URL__c == null || queriedContact.LinkedIn_URL__c == '') {
                                queriedContact.LinkedIn_URL__c = req.updateValue;
                                update queriedContact;
                                response.responseBody = Blob.valueOf('LIUrl Updated');
                            } else {
                                response.responseBody = Blob.valueOf('LIUrl Already Set');
                            }
                        } else {
                            response.responseBody = Blob.valueOf('Contact Not Found. Possibly Deleted!');
                        }
                    } else {
                        response.responseBody = Blob.valueOf('No LI Url Specified');
                    }
                } else {
                    response.responseBody = Blob.valueOf('No Contact Id Specified');
                }
            }else if(req.requestType == 'PutCEUpdateLocation') {
                if(req.id != null) {
                    if (req.locationValue != null) {
                        
                        List<Contact> cList = getContacts(req.id);
                        if (!cList.isEmpty()) {
                            Contact queriedContact = cList[0];
                            queriedContact.MailingCity = req.locationValue.city;
                            queriedContact.MailingCountry = req.locationValue.country;
                            queriedContact.MailingLatitude = req.locationValue.lat;
                            queriedContact.MailingLongitude = req.locationValue.lng;
                            queriedContact.MailingPostalCode = '';
                            queriedContact.MailingState = req.locationValue.state;
                            queriedContact.MailingStreet = '';
                            queriedContact.Talent_Country_Text__c = req.locationValue.country;
                            
                            update queriedContact;
                            response.responseBody = Blob.valueOf('Location Updated');
                        } else {
                            response.responseBody = Blob.valueOf('Contact Not Found. Possibly Deleted!'); 
                        }
                    } else {
                        response.responseBody = Blob.valueOf('No Location Value Specified');
                    }
                } else {
                    response.responseBody = Blob.valueOf('No Contact Id Specified');
                }
            }else if(req.requestType == 'PutCEUpdateCallSheet') {
                if (req.newCallSheet != null) {
                    Task t = new Task();
                    
                    t.WhoId = req.newCallSheet.WhoId;
                    t.Subject = req.newCallSheet.Subject;
                    t.Status = req.newCallSheet.Status;
                    t.Priority = req.newCallSheet.Priority;
                    t.ActivityDate = req.newCallSheet.ActivityDate;
                    t.Type = req.newCallSheet.Type;
                    t.WhatId = req.newCallSheet.WhatId;
                    t.Description = req.newCallSheet.Description;
                    
                    insert t;
                    response.responseBody = Blob.valueOf('Task Created');
                } else {
                    response.responseBody = Blob.valueOf('No CallSheet Task Specified');
                }
            } else if(req.requestType == 'GetContactPhoneNumbers') {
                String output = '';
                if(req.id != null) {
                    List<Contact> cList = [Select Phone, MobilePhone, HomePhone, OtherPhone, Preferred_Phone__c, Preferred_Phone_Value__c, FirstName, LastName From Contact where id=:req.id LIMIT 1];
                    if (!cList.isEmpty()) {
                        if(cList.size() > 0) {
                            output = JSON.serialize(cList);
                        }
                    }  
                }
                response.responseBody = Blob.valueOf(output);
            }
            response.statusCode = 200;
            response.addHeader('Content-Type', 'application/json');       	
        }catch(Exception e){
            connectedLog.LogException('ConnectedEverywhere', 'ConnectedEverywhereREST', 'doPost', e);
            response.statusCode = 500;
            response.responseBody = Blob.valueOf(e.getMessage()+'\n'+e.getStackTraceString());
        }
    }
    /**
    * @description Get Contact List (handles Account or Contact Id)
    * @return List<Contact>
    */
    static List<Contact> getContacts(String id) {
        if (id.StartsWith('003')) {
            List<Contact> cList = [SELECT Id, LinkedIn_URL__c FROM Contact WHERE id=:id LIMIT 1];
            return cList;
        } else if (id.StartsWith('001')) {
            List<Contact> cList = [SELECT Id, LinkedIn_URL__c FROM Contact WHERE AccountId=:id LIMIT 1];
            return cList;
        } else {
            return new List<Contact>();
        }
    }
}