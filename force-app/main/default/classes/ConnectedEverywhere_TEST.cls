/**
* @author Peter Beazer
* @description Test Cases for Connected Everywhere Classes
*/
@isTest
private class ConnectedEverywhere_TEST {
    /**
    * @description Test REST Get
    * @return Void
    */ 
    @isTest(SeeAllData=true)
    static void testRESTGet() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/ConnectedEverywhere';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response= res;
        
        ConnectedEverywhereREST.doGet();
        System.assertEquals(200, res.statusCode);
        
    }
    /**
    * @description Test REST Post - FindPerson
    * @return Void
    */ 
    @isTest(SeeAllData=true)
    static void testRESTPost() {
        Test.startTest();
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.inputPerson = new ConnectedEverywhere.Person();
        creq.requestType = 'FindPerson';
        
        
        String r = doTestPost(creq);
        System.assertNotEquals(null, r);
        Test.stopTest();
    }
    /**
    * @description Test REST Post - GetAutoMatchReqs
    * @return Void
    */ 
    @isTest(SeeAllData=true)
    static void testRESTPostMatchedReqs() {
        Test.startTest();
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.inputPerson = new ConnectedEverywhere.Person();
        creq.requestType = 'GetAutoMatchReqs';
        
        String r = doTestPost(creq);
        System.assertNotEquals(null, r);
        Test.stopTest();
    }
    /**
    * @description Test ConnectedEverywhere.GetUserDetails()
    * @return Void
    */ 
    @isTest(SeeAllData=true)
    static void testGetUserDetails(){
        User usr = [Select Id, name From User Where Name ='Talent Integration' LIMIT 1];
        usr.OPCO__c = 'AG_EMEA';
        usr.Team__c = 'Birmingham TEK Perm';
        usr.CompanyName = 'TEKsystems';
        usr.Division__c = 'TEK';
        usr.Office_Code__c = '22222';
        usr.MobilePhone = '1234567890';
        usr.Phone = '1234567890';
        update(usr);
        System.runAs(usr){
            Map<String, String> ud = ConnectedEverywhere.GetUserDetails();
            
            System.assertNotEquals(null, ud);
        }
    }
    /**
    * @description Test REST Exceptions
    * @return Void
    */ 
    @isTest(SeeAllData=true)
    static void testRESTExceptions(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/ConnectedEverywhere';
        req.httpMethod = 'POST';
        
        
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.inputPerson = new ConnectedEverywhere.Person();
        creq.requestType = 'FindPerson';
        
        req.requestBody = null;
        
        RestContext.request = req;
        RestContext.response= res;
        
        ConnectedEverywhereREST.doPost();
        
        System.assertEquals(500, res.statusCode);
    }
    /**
    * @description Setup Test Data
    * @param User - test user
    * @return Map<String, String> - Test Contact Details
    */ 
    static Map<String,String> setupTestData(User usr){
        Id rType = [Select Id From RecordType WHERE Name = 'Talent' AND SobjectType='Account' LIMIT 1].Id;
        Id rTypeContact = [Select Id From RecordType WHERE Name = 'Talent' AND SobjectType='Contact' LIMIT 1].Id;
        
        ConnectedEverywhere.CandidateSummaryJSON jsonC = new ConnectedEverywhere.CandidateSummaryJSON();
        jsonC.skills = new List<string>();
        jsonC.skills.add('.NET');
        jsonC.languages = new List<string>();
        jsonC.languages.add('de');
        jsonC.geographicPrefs = new ConnectedEverywhere.GeoPreferences();
        jsonC.geographicPrefs.nationalOpportunities = true;
        jsonC.geographicPrefs.commuteLength = new ConnectedEverywhere.CommuteLength();
        jsonC.geographicPrefs.commuteLength.distance = '100';
        jsonC.geographicPrefs.commuteLength.unit = 'km';
        jsonC.geographicPrefs.desiredLocation = new List<ConnectedEverywhere.DesiredLocation>();
        
        ConnectedEverywhere.DesiredLocation dl = new ConnectedEverywhere.DesiredLocation();
        dl.city = 'Baltimore';
        dl.country = 'USA';
        dl.state = 'MD';
        jsonC.geographicPrefs.desiredLocation.add(dl);
        
        ConnectedEverywhere.Employability ei = new ConnectedEverywhere.Employability();
        ei.backgroundCheck = false;
        ei.drugTest = false;
        ei.eligibleIn = new String[]{'USA'};
        ei.reliableTransportation = true;
        ei.securityClearance = false;
        jsonC.employabilityInformation = ei;
        
        Account a = new Account();
        a.Name = 'Test User';
        a.RecordTypeId = rType;
        a.Skills__c = JSON.serialize(jsonC);
        a.Talent_Preference_Internal__c = JSON.serialize(jsonC);
        a.Talent_Ownership__c = 'TEK';
        Database.insert(a);
        
        Account a2 = new Account();
        a2.Name = 'Test Account';
        Database.insert(a2);
        
        Account a3 = new Account();
        a3.Name = 'Test User Comm';
        a3.RecordTypeId = rType;
        
        Database.insert(a3);
        
        
        Contact c = new Contact();
        c.AccountID = a.Id;
        c.FirstName = 'Test';
        c.LastName = 'User';
        c.Email = 'test@test.com';
        c.linkedIn_URL__c = 'https://www.linkedin.com/in/testUser/';
        c.MobilePhone = '1234567890';
        c.HomePhone='0000';
        c.Phone = '1111';
        c.Source_System_ID__c = 'R.1234';
        c.Website_URL__c = 'www.test.com';
        c.MailingLatitude = 39.2481479;
        c.MailingLongitude = -76.7100271;
        c.RecordTypeId = rTypeContact;
        
        Database.insert(c);
        
        Contact c2 = new Contact();
        c2.AccountID = a3.Id;
        c2.FirstName = 'Test';
        c2.LastName = 'User';
        c2.Email = 'test@test.com';
        c2.linkedIn_URL__c = 'https://www.linkedin.com/in/testUser/';
        c2.MobilePhone = '1234567890';
        c2.HomePhone='0000';
        c2.Phone = '1111';
        c2.Source_System_ID__c = 'COM.1234';
        c2.Website_URL__c = 'www.test.com';
        c2.MailingCity = 'Catonsville';
        c2.Talent_Country_Text__c = 'USA';
        c2.MailingState = 'MD';
        c2.MailingPostalCode = '21228';
        c2.RecordTypeId = rTypeContact;

        Database.insert(c2);
        
        User commUser = new User();
        commUser.FirstName = c2.FirstName;
        commUser.LastName = c2.LastName;
        commUser.ContactId = c2.Id;
        
        commUser.Username = 'testCommUser@test.com';
        commUser.Email= 'testCommUser@test.com';
        commUser.Alias ='commuser';
        commUser.CommunityNickname = '';
        commUser.TimeZoneSidKey='America/New_York';
        commUser.LocaleSidKey = 'en_GB';
        commUser.EmailEncodingKey='UTF-8';
        commUser.LanguageLocaleKey='en_US';
        
        Profile pro = [select Id, Name from Profile where Name like '%Community User%' LIMIT 1];
        commUser.ProfileId =pro.Id;
        
        //Database.insert(commUser);
        
        Talent_Document__c talentDocument = null;                                           
        talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = a.Id;
        talentDocument.Document_Name__c = 'test.doc';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = true;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        talentDocument.HTML_Version_Available__c=True;
        insert(talentDocument);
        
        Attachment attach = new Attachment(Name='UnitTestAttachment.txt',parentId=talentDocument.id);    
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'text/plain';
        Database.insert(attach);
        
        Task t1 = new Task();
        t1.Subject='Test Task';
        t1.Status = 'Completed';
        t1.ActivityDate = date.Today();
        t1.Type='Correspondence';
        t1.OwnerId = UserInfo.getUserId();
        t1.WhoId = c.Id;
        Database.insert(t1);
        
        
        
        Opportunity op = new Opportunity();
        op.OpCo__c = 'AG_EMEA';
        op.BusinessUnit__c = 'TEKsystems';
        op.Name = 'OppTest1';
        op.StageName = 'Draft';
        op.CloseDate = Date.Today();
        op.AccountId = a.Id;
        Database.insert(op);
        
        Order o = new Order();
        o.OpportunityId = op.Id;
        o.AccountId = a.Id;
        o.EffectiveDate = Date.Today();
        o.Status = 'Offer Accepted';
        o.ShipToContactId = c.Id;
        
        List<RecordType> rList = [select Id from RecordType where DeveloperName='OpportunitySubmission' LIMIT 1];
        o.RecordTypeId = rList[0].Id;
        
        Database.insert(o);
        
        Map<String,String> m = new Map<String,String>();
        m.put('ContactId', c.Id);
        m.put('ContactAccountId', c.AccountId);
        m.put('Contact2Id', c2.Id);  
        m.put('Contact2AccountId', c2.AccountId);
        m.put('OrderId', o.Id);
        
        return m;
    }
    /**
    * @description Test ConnectedEverywhere.GetAutoMatchReqs
    * @return Void
    */ 
    @isTest(SeeAllData=true)
    static void testGetAutoMatchReqs(){
        User usr = [Select Id, name From User Where Name ='Talent Integration' LIMIT 1];
        usr.OPCO__c = 'AG_EMEA';
        update(usr);
        System.runAs(usr){
            Map<String,String> m = setupTestData(usr);
            ConnectedEverywhere.Person p = new ConnectedEverywhere.Person();
            p.agWorkHistory = new List<ConnectedEverywhere.WorkHistory>();
            p.recentActivity = new List<ConnectedEverywhere.Activity>();
            p.socialProfiles = new List<ConnectedEverywhere.SocialProfile>();
            p.firstName = 'Test';
            p.lastName = 'User';
            p.emailAddresses = new List<String>();
            p.phoneNumbers = new List<String>();
            p.emailAddresses.add('test@test.com');
            p.phoneNumbers.add('1234567890');
            
            ConnectedEverywhere.Request req = new ConnectedEverywhere.Request();
            req.inputPerson = p;
            req.requestType = 'FindPerson';
            req.requestMessage = '';
            req.url = '';
            req.inputPerson.accountId = m.get('ContactAccountId');
            req.inputPerson.id = m.get('ContactId');
            req.autoMatchIncLocation = true;
            
            ConnectedEverywhere.Response resp = ConnectedEverywhere.getPerson(req);
            String amr = ConnectedEverywhere.getMatchedReqs(req);
            System.assertNotEquals(null, amr);
            
            req.inputPerson.accountId = m.get('Contact2AccountId');
            req.inputPerson.id = m.get('Contact2Id');
			            
            ConnectedEverywhere.Response resp2 = ConnectedEverywhere.getPerson(req);
            String amr2 = ConnectedEverywhere.getMatchedReqs(req);
            System.assertNotEquals(null, amr2);
        }
    }
    /**
    * @description Test ConnectedEverywhere.FindPerson
    * @return Void
    */ 
    @isTest(SeeAllData=true)
    static void testFindPerson(){
        User usr = [Select Id, name From User Where Name ='Talent Integration' LIMIT 1];
        usr.OPCO__c = 'AG_EMEA';
        update(usr);
        System.runAs(usr){
            
            Map<String,String> m = setupTestData(usr);
            
            ConnectedEverywhere.Person p = new ConnectedEverywhere.Person();
            p.agWorkHistory = new List<ConnectedEverywhere.WorkHistory>();
            p.recentActivity = new List<ConnectedEverywhere.Activity>();
            p.socialProfiles = new List<ConnectedEverywhere.SocialProfile>();
            p.firstName = 'Test';
            p.lastName = 'User';
            p.emailAddresses = new List<String>();
            p.phoneNumbers = new List<String>();
            p.emailAddresses.add('test@test.com');
            p.phoneNumbers.add('1234567890');
            
            p.socialProfiles.add(new ConnectedEverywhere.SocialProfile('RWS', '1234'));
            p.socialProfiles.add(new ConnectedEverywhere.SocialProfile('LinkedIn', 'testUser'));
            
            Talent_Experience__c t2 = new Talent_Experience__c();
            t2.Talent__c = m.get('Contact2AccountId');
            t2.Type__c='Work';
            t2.Allegis_Placement__c=true;
            t2.Order__c = m.get('OrderId');
            t2.Current_Assignment__c = true;
            t2.End_Date__c = Date.Today().addDays(-1);
            t2.Start_Date__c = Date.Today().addDays(-14);
            t2.Title__c = 'Test Title 2';
            t2.PersonaIndicator__c = 'Recruiter';
            Database.insert(t2);
            
            Task ta = new Task();
            ta.WhatId = m.get('Contact2AccountId');
            ta.WhoId = m.get('Contact2Id');
            ta.Subject = 'Test Task';
            ta.ActivityDate = Date.Today().addDays(-1);
            ta.Activity_Type__c = 'Call';
            ta.Status = 'Completed';
            ta.Post_Meeting_Notes__c = 'Test';
            
            Database.insert(ta);
            
            Map<String,Object> par = new Map<String,Object>();
            par.put('recordId', m.get('Contact2AccountId'));
            par.put('maxItems', '30');
            par.put('post', 'test_method_updateEvent_Simple');
            List<Object> e1 = (List<Object>)ATSTalentActivityFunctions.performServerCall('getNewAccountEvent', par);
            Event newEvent = (Event)e1[0];
            newEvent.StartDateTime = DateTime.newInstance(2017,1,1,0,0,0);
            newEvent.EndDateTime = DateTime.newInstance(2017,1,2,0,0,0);
            newEvent.Subject = 'Test Event';
            newEvent.Pre_Meeting_Notes__c = 'test';
            newEvent.Post_Meeting_Notes__c = 'test';
            Database.insert(newEvent);
            

            Talent_Experience__c t = new Talent_Experience__c();
            t.Talent__c = m.get('Contact2AccountId');
            t.Type__c='Work';
            t.Allegis_Placement__c=true;
            t.Order__c = m.get('OrderId');
            t.Current_Assignment__c = true;
            t.End_Date__c = Date.Today().addDays(14);
            t.Start_Date__c = Date.Today().addDays(-14);
            t.Title__c = 'Test Title';
            t.PersonaIndicator__c = 'Recruiter';
            Database.insert(t);
            
            Test.startTest();
            ConnectedEverywhere.Request req = new ConnectedEverywhere.Request();
            req.inputPerson = p;
            req.requestType = 'FindPerson';
            req.requestMessage = '';
            req.url = '';
            
            ConnectedEverywhere.Response resp = ConnectedEverywhere.getPerson(req);
                        
            System.assertNotEquals(0, resp.personList.size());
            System.assertEquals(true, resp.personList[0].showSocialProfiles);
            
            Boolean agWorkHistory = false;
            for(ConnectedEverywhere.Person pE : resp.personList){
                if(pE.showagWorkHistory){
                    agWorkHistory = true;
                }
            }
            System.assertEquals(true, agWorkHistory);
            
            req.inputPerson.accountId = m.get('ContactAccountId');
            req.inputPerson.id = m.get('ContactId');
            ConnectedEverywhere.Response resp2 = ConnectedEverywhere.getPerson(req);
            System.assertNotEquals(0, resp2.personList.size());
            
            req.inputPerson.accountId = '';
            req.inputPerson.id = m.get('Contact2Id');
            ConnectedEverywhere.Response resp3 = ConnectedEverywhere.getPerson(req);
            
            System.assertNotEquals(0, resp3.personList.size());
            
            Test.stopTest();
        }   
    }
    /**
    * @description Test REST Post - PutCEUpdateJT
    * @return Void
    */    
    @isTest(SeeAllData=true)
    static void testRESTPostPutCEUpdateJT() {
        Contact c = getTestContact();
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.requestType = 'PutCEUpdateJT';
        creq.id = c.Id;
        creq.updateValue = 'Test Job Title';

        String r1 = doTestPost(creq);
        System.assertEquals('Job Title Updated', r1);
        
        creq.id = 'deleted_id';
        String r2 = doTestPost(creq);
        System.assertEquals('Contact Not Found. Possibly Deleted!', r2);   
        
        creq.id = c.Id;
        creq.updateValue = null;
        String r3 = doTestPost(creq);
        System.assertEquals('No Job Title Specified', r3);   
        
        creq.id = null;
        String r4 = doTestPost(creq);
        System.assertEquals('No Contact Id Specified', r4); 
    }
    /**
    * @description Test REST Post - PutCEUpdateLI
    * @return Void
    */    
    @isTest(SeeAllData=true)
    static void testRESTPostPutCEUpdateLI() {
       	Contact c = getTestContact();
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.requestType = 'PutCEUpdateLI';
        creq.id = c.Id;
        creq.updateValue = 'https://linkedin.com/in/testuser';
        
        String r = doTestPost(creq);
        System.assertEquals('LIUrl Updated', r);
        
        String r1 = doTestPost(creq);
        System.assertEquals('LIUrl Already Set', r1);
        
        creq.id = 'deleted_id';
        String r2 = doTestPost(creq);
        System.assertEquals('Contact Not Found. Possibly Deleted!', r2);   
        
        creq.id = c.AccountId;
        String r5 = doTestPost(creq);
        System.assertEquals('LIUrl Already Set', r5);
        
        creq.id = c.Id;
        creq.updateValue = null;
        String r3 = doTestPost(creq);
        System.assertEquals('No LI Url Specified', r3);   
        
        creq.id = null;
        String r4 = doTestPost(creq);
        System.assertEquals('No Contact Id Specified', r4);   
    }
    /**
    * @description Test REST Post - GetContactPhoneNumbers
    * @return Void
    */     
    @isTest(SeeAllData=true)
    static void testRESTPostGetContactPhoneNumbers() {
       	Contact c = getTestContact();
        c.Phone = '1234567890';
        Update c;
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.requestType = 'GetContactPhoneNumbers';
        creq.id = c.Id;
        
        String r = doTestPost(creq);
        System.assertNotEquals('', r);
    }
    /**
    * @description Test REST Post - PutCEUpdateLocation
    * @return Void
    */     
    @isTest(SeeAllData=true)
    static void testRESTPostPutCEUpdateLocation() {
        Contact c = getTestContact();
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.requestType = 'PutCEUpdateLocation';
        creq.id = c.Id;
        ConnectedEverywhere.LocationUpdate l = new ConnectedEverywhere.LocationUpdate();
        l.city = 'Baltimore';
        l.state = 'MD';
        l.country = 'USA';
        l.lat = 39.284668;
        l.lng = -76.6115208;
                
        creq.locationValue = l;
                
        String r1 = doTestPost(creq);
        System.assertEquals('Location Updated', r1);
        
        creq.id = 'deleted_id';
        String r2 = doTestPost(creq);
        System.assertEquals('Contact Not Found. Possibly Deleted!', r2);   
        
        creq.id = c.Id;
        creq.locationValue = null;
        String r3 = doTestPost(creq);
        System.assertEquals('No Location Value Specified', r3);   
        
        creq.id = null;
        String r4 = doTestPost(creq);
        System.assertEquals('No Contact Id Specified', r4); 
    }
    /**
    * @description Test REST Post - PutCEUpdateCallSheet
    * @return Void
    */           
    @isTest(SeeAllData=true)
    static void testRESTPostPutCEUpdateCallSheet() {
        Contact c = getTestContact();
        ConnectedEverywhere.Request creq = new ConnectedEverywhere.Request();
        creq.requestType = 'PutCEUpdateCallSheet';
        creq.id = c.Id;
        ConnectedEverywhere.CallSheetTask t = new ConnectedEverywhere.CallSheetTask();
        
        t.WhoId = c.Id;
      	t.Subject = 'Added From Connected Everywhere';
        t.Status = 'Not Started';
        t.Priority = 'Normal';
        t.ActivityDate = Date.today();
        t.Type = 'Call';
        t.Description = 'Test Description';
        t.WhatId = c.AccountId;
                
        creq.newCallSheet = t;
                
        String r = doTestPost(creq);
        System.assertEquals('Task Created', r);

        creq.newCallSheet = null;
        String r1 = doTestPost(creq);
        System.assertEquals('No CallSheet Task Specified', r1); 
    } 
    /**
    * @description Get Test Contact
    * @return Contact - Random Talent Contact
    */       
    static Contact getTestContact() {
        Id rTypeContact = [Select Id From RecordType WHERE Name = 'Talent' AND SobjectType='Contact' LIMIT 1].Id;
        Contact c = [Select Id, Title, AccountId From Contact WHERE RecordTypeId =: rTypeContact LIMIT 1];
        return c;
    }
    /**
    * @description Perform Post To ConnectedEverywhere REST Endpoint
    * @param ConnectedEverywhere.Request creq - Request Details
    * @return String - Response Body
    */       
    static String doTestPost(ConnectedEverywhere.Request creq) {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/ConnectedEverywhere';
        req.httpMethod = 'POST';
        
        String rBody = JSON.serialize(creq);
        req.requestBody = Blob.valueOf(rBody);
        
        RestContext.request = req;
        RestContext.response = res;
        
        ConnectedEverywhereREST.doPost();
        return res.responseBody.toString();
    }
}