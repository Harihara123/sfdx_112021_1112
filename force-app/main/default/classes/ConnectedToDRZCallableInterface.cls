/******
 * This should be part of 'CONNECTED Package'
 * This class used as interface between connected and Drz
 * class with callable interface which takes method name and method parameters and orgs and executing existing match jobs method from searchcontroller class
 * **/
public class ConnectedToDRZCallableInterface implements callable{

    public string matchJobs( Map<String, Object> args){
        String queryString = (String)args.get('queryString');
        String httpRequestType = (String)args.get('httpRequestType');
        String requestBody = (String)args.get('requestBody');
        String location = (String)args.get('location');
        return SearchController.matchJobs(queryString,httpRequestType,requestBody,location);
    }
   public Object call(String action, Map<String, Object> args) {
     switch on action {
       when 'matchJobs' {
         return this.matchJobs(args);
       }
       when else {
        throw new ExtensionMalformedCallException('Method not implemented');
       }
     }
   }

   public class ExtensionMalformedCallException extends Exception {}
}