/*
@author : Neel Kamal
@date : 15-July-2021
@description : This class handles methods executed from ContactChangeAsyncTrigger  
*/
public without sharing class ContactChangeAsyncTriggerHandler {
	private static final string LOG_TYPE_ID = 'CCATH/OptoutPE';
	private static final string CL_NAME = 'ContactChangeAsyncTriggerHandler';
	private static final string YES = 'Yes';
	public static List<Consent_Preferences_Fields_Mapping__mdt> cpfmList = new List<Consent_Preferences_Fields_Mapping__mdt >();
	public static void consentPrefCompany() {
		cpfmList = [SELECT Id, CP_API_Field__c, Company_Short_Name__c FROM Consent_Preferences_Fields_Mapping__mdt WHERE Company_Short_Name__c != null];
	}

	//S-233076
	//Neel - Ths method returns map of Optout brnads. Track updted pref center field and put in map.
	public static Map<Id, String> optoutBrand(Id contactId, ContactChangeEvent ev, Map<Id, String> optoutBrandMap) {
		
		if(cpfmList.size() == 0 ) {
			consentPrefCompany();
		}
		String brand;
		for(Consent_Preferences_Fields_Mapping__mdt cpfm : cpfmList){
			System.debug(ev.get(cpfm.CP_API_Field__c));
			if(ev.get(cpfm.CP_API_Field__c) == YES) {
				if(optoutBrandMap.get(contactId) != null) {
						 brand += ';'+cpfm.Company_Short_Name__c;
				}
				else {
					brand = cpfm.Company_Short_Name__c;
				}
				optoutBrandMap.put(contactId, brand);
			}
		}		
		return optoutBrandMap;
	}
	//Neel -S-233076 - This method genrates PE for optut brands. newMap has all contact changed reocrds. 
	//       optoutBrandMap has only pref center updated records map.
	public static void consentPrefEmailOptoutPE(Map<Id, Contact> newMap, Map<Id, String> optoutBrandMap) {
		System.debug('consentPrefEmailOptoutPE::::::'+newMap);
		System.debug('optoutBrandMap::::::'+optoutBrandMap);
		List<Consent_Pref_Email_Optout__e> consentPEList = new List<Consent_Pref_Email_Optout__e> ();

		for(Id conId : optoutBrandMap.keySet()) {
			Contact con = newMap.get(conId);
			List<String> brandList =  optoutBrandMap.get(conId).split(';');
			for(String brand : brandList) {
				Consent_Pref_Email_Optout__e consent = new Consent_Pref_Email_Optout__e();
				consent.Contact_ID__c = con.Id;
				consent.Emails__c = con.Email;
				consent.External_Source_ID__c = con.externalSourceId__c;
				consent.Brand__c = brand;
				consentPEList.add(consent);
			}
		}
		
		
		try {
			List<Database.SaveResult> results;
			// Call method to publish events
			if (consentPEList.size() > 0) {
				results = EventBus.publish(consentPEList);
				System.debug('Platform events created for Contacts::::' + consentPEList);
			}
			//Notes for Dev - Update the logging as per consentPEList. repalce contact list
			Boolean isConsentError = isError(results);
			if (!isConsentError) {
				logEvent(consentPEList);
			}
			if (isConsentError) {
				throw new AllegisException(errorMsg);
			}
		} catch(DMLException dexp) {
			ConnectedLog.LogException(LOG_TYPE_ID, CL_NAME, 'consentPrefEmailOptoutPE', dexp);
		} catch(Exception exp) {
			ConnectedLog.LogException(LOG_TYPE_ID, CL_NAME, 'consentPrefEmailOptoutPE', exp);
		}

	}


	//Loggin Platform event
	public static void logEvent(List<Consent_Pref_Email_Optout__e> consentPEList) {
		List<Consent_Email_Optout_PE_Log__c> cpeLogList = new List<Consent_Email_Optout_PE_Log__c> ();
		Consent_Email_Optout_PE_Log__c cpeLog;
		for (Consent_Pref_Email_Optout__e cp : consentPEList) {
			cpeLog = new Consent_Email_Optout_PE_Log__c();
			cpeLog.Contact_ID__c = cp.Contact_ID__c;
			cpeLog.Brand__c = cp.Brand__c;
			cpeLog.Emails__c = cp.Emails__c;
			cpeLog.External_Source_ID__c = cp.External_Source_ID__c;

			cpeLogList.add(cpeLog);
		}
		try {
			insert cpeLogList;
		} catch(DmlException exp) {
			ConnectedLog.LogException(LOG_TYPE_ID, CL_NAME, 'logEvent', exp);
		}
	}

	static String errorMsg = 'Error while publishing PE: ';
	private static Boolean isError(List<Database.SaveResult> results) {
		Boolean isError = false;
		if (results != null) {
			for (Database.SaveResult sr : results) {
				if (!sr.isSuccess()) {
					isError = true;
					for (Database.Error err : sr.getErrors()) {
						errorMsg += ' - ' + err.getMessage();
					}
				}
				else {
					System.debug('Job Posting Scheduler : PE generated');
				}
			}
		}
		return isError;
	}
}