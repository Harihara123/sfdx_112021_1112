global class ContactDetailsParser {
    
    public List<ContactDetail> ContactDetails;
        
    public class ContactDetail {
        public String EmployeeId{get;set;}
        public String RWSId{get;set;}
        public String MDMID{get;set;}
		public String SalesforceContactID{get;set;}
        
		public String externalId{get;set;}
        public String externalIdType{get;set;}
        
        public String FirstName{get;set;}
        public String LastName{get;set;}
        public String PreferredName{get;set;}
        public List<Email> Emails{get;set;}
        public List<Address> Addresses{get;set;}
        public List<Phone> Phones{get;set;}
        
        public String Title{get;set;}
        
        public String message{get;set;}
    }
    
    public class Email {
        public Email() {
            this.Preferred = false;
        }
        public String Id {get;set;}
        public String Type {get;set;}
        public boolean Preferred {get;set;}
    }
    
    public class Phone {
        public Phone() {
            this.Preferred = false;
        }
        public String Phone_Number {get;set;}
        public String Type {get;set;}
        public boolean Preferred {get;set;}
    }
    
    public class Address {
        public String Street {get;set;}
        public String City {get;set;}
        public String State {get;set;}
        public String Country {get;set;}
        public String PostalCode {get;set;}
        public String Type {get;set;}
        
    }
    
    
    public ContactDetailsParser parse(String json) {
        return (ContactDetailsParser) System.JSON.deserialize(json, ContactDetailsParser.class);
    }
}