/**
 * This class is used to test the configuration of 
 */
@isTest
private class ContactGeoLocationAddressTest {

    @testSetup
    static void setupTestData(){
        List<Contact> mTestContacts = new List<Contact>();
        //create five contacts to test geolocation fields
        List<List<String>> addresses = new List<List<String>>{
            new List<String>{'5255 Research Park Drive', 'Baltimore', 'Maryland','21228', 'USA'},
            new List<String>{'206699 Parkside Circle', 'Sterling', 'Virginia','20165','USA'},
            new List<String>{'7007 W Indian School Road', 'Pheonix', 'Arizona','85033','USA'},
            new List<String>{'7360 Gallagher Drive, Apt# A113', 'Edina', 'Minnesota','55435','USA'},
            new List<String>{'1466 East Crossings Place', 'Westlake', 'Ohio','44145','USA'}
        };
        for(integer i = 0; i < 5; i++){
            Contact mContact = new Contact();
            mContact.firstName = 'Test';
            mContact.LastName = 'Record ' + i;
            mContact.mailingstreet = addresses[i][0];
            mContact.mailingcity = addresses[i][1];
            mContact.mailingstate = addresses[i][2];
            mContact.mailingpostalcode = addresses[i][3];
            mContact.mailingcountry = addresses[i][4];
            mTestContacts.add(mContact);    
        }
        insert mTestContacts;
    }
/*    
    static testMethod void testContactLatitude() {
        test.starttest();
       List<Contact> mContacts = [select firstname, lastname, MailingAddress from contact];
       System.assertEquals(mContacts.size(),5);
       for(Contact c: mContacts){
        Address addr = c.MailingAddress;
        System.debug(addr);
        System.debug(addr.getLatitude());
        //System.debug(c.mailinglatitude);
       }
       test.stoptest();
    }
*/    
    static testMethod void testContactLongitude(){
        List<Contact> mContacts = [select firstname, lastname, mailingstreet, mailingcity, mailingstate, mailingcountry, mailingpostalcode, mailinglatitude from contact];
        System.assertEquals(mContacts.size(),5);    
    }
}