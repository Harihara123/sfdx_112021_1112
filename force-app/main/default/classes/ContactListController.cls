public class ContactListController  {

    @AuraEnabled
    public static ContactListsAndProfileName getContactsForList(String tagId) {
        ContactListsAndProfileName returnObject = new ContactListsAndProfileName();

        Map<String, List<Contact>> returnMap = new Map<String, List<Contact>>();
		Map<String, String> notesMap = new Map<String, String>();

        List<Contact_Tag__c> contactList = new List<Contact_Tag__c>();

        contactList = [SELECT Contact__r.Id, 
                                //(SELECT COUNT(ID) FROM Talent_Document__c WHERE Talent__c = Contact__r.AccountId),
                                Contact__r.Name, 
                                Contact__r.Preferred_Name__c,
                                Contact__r.RecordType.Name, 
                                Contact__r.Preferred_Phone_Value__c, 
                                Contact__r.Preferred_Phone__c,
                                Contact__r.Phone, 
								Contact__r.WorkExtension__c,
                                Contact__r.HomePhone,
                                Contact__r.MobilePhone, 
                                Contact__r.OtherPhone, 
                                Contact__r.Email, 
                                Contact__r.Preferred_Email__c,
                                Contact__r.Preferred_Email_Value__c, 
                                Contact__r.Work_Email__c,
                                Contact__r.Other_Email__c, 
                                Contact__r.Title, 
                                Contact__r.Customer_Status__c, 
                                Contact__r.LastActivityDate, 
                                Contact__r.LastModifiedDate,
                                Contact__r.Account.Name, 
                                Contact__r.Do_Not_Contact__c,
                                Contact__r.Do_Not_Use__c,
                                Contact__r.RWS_DNC__c,
                                Contact__r.AccountId,
                                Contact__r.Talent_Id__c,
                                Contact__r.Last_Req_Created_Date__c,
                                Contact__r.MailingStreet, 
                                Contact__r.MailingCity,
                                Contact__r.Account.Talent_Latest_Submittal_Status__c,
                                Contact__r.Account.Talent_Latest_Submittal_Timestamp__c,
                                Contact__r.Account.Talent_Current_Employer_Formula__c,
                                Contact__r.Account.Talent_Internal_Resume_Count__c,
                                Contact__r.Account.Do_Not_Contact__c,
                                Contact__r.MailingState,
                                Contact__r.MailingPostalCode,
                                Contact__r.MailingCountry,
                                Contact__r.Mailing_Address_Formatted__c,
                                Contact__r.Talent_Country_Text__c,
                                Contact__r.Related_Contact__c,
                                Contact__r.Related_Contact__r.Name,
                                Contact__r.Website_URL__c,
                       /*S-96322 Sandeep: add or remove talent to Pipeline for Lists*/
                       			Contact__r.Account.Target_Account_Stamp_0__c, 
                       			Contact__r.Account.Target_Account_Stamp_1__c,
                       			Contact__r.Account.Target_Account_Stamp_2__c,
                       			Contact__r.Account.Target_Account_Stamp_3__c,
                       			Contact__r.Account.Target_Account_Stamp_4__c,
                       			Contact__r.Account.Target_Account_Stamp_5__c,
								Contact__r.Last_Activity_Date__c,
								/*S-135076 Texting Indicator on My Lists (Aerotek Pilot Users Only)*/
								Contact__r.PrefCentre_Aerotek_OptOut_Mobile__c,
								Notes__c
                        FROM Contact_Tag__c 
                        WHERE Tag_Definition__c =: tagId 
                        ORDER BY Contact__r.Name ASC];


        List<Contact> talentList = new List<Contact>();
        List<Contact> clientList = new List<Contact>();

        for(Contact_Tag__c c : contactList) {
            if(c.Contact__r.RecordType.Name == 'Talent') {
                talentList.add(c.Contact__r);
            } else if(c.Contact__r.RecordType.Name == 'Client') {
                clientList.add(c.Contact__r);
            }
			notesMap.put(c.Contact__r.Id,c.Notes__c);
        }
		
        returnMap.put('Talent', talentList);
        returnMap.put('Client', clientList);

		returnObject.contactNotes = notesMap;
        returnObject.contactMap = returnMap;
      
		returnObject.uoModal =(ATS_UserOwnershipModal)BaseController.performServerCall('', '', 'getCurrentUserWithOwnership', null);
		returnObject.hasUserSMSAccess =  userHasAccessPermission('SMS'); //S-135076 
        return returnObject;
    }

    @AuraEnabled
    public static Boolean removeContactFromList(String tagId, String contactId) {
        Contact_Tag__c contactTag; 
        //Move query and delete to DAO

        try {
            contactTag = [SELECT Id FROM Contact_Tag__c WHERE Tag_Definition__c =: tagId AND Contact__c =: contactId LIMIT 1];
        } catch(QueryException ex) {
            return false;
        }

        //Refactor to throw exception, handle boolean logic in js controller
        if(contactTag != null) {

            try {
                delete contactTag;
                return true;
            } catch (DmlException ex) {
                return false;
            }

        }

        return false;
    }

	@AuraEnabled
    public static Boolean removeSelectedContactsFromList(String tagId, List<String> contactIds) {
        List<Contact_Tag__c> contactTags = new List<Contact_Tag__c>();
        try {
            contactTags = [SELECT Id FROM Contact_Tag__c WHERE Tag_Definition__c =: tagId AND Contact__c IN : contactIds];
			if (contactTags.size() > 0) {
				delete contactTags;
                return true;
			}
        } catch(QueryException ex) {
            return false;
        }        
        return false;
    }

    @AuraEnabled
    public static void addContactsToCallSheet(List<Contact> contacts, Map<String, String> callSheetDetails) {
        List<Task> newCallSheets = new List<Task>();
        Id currentUserId = UserInfo.getUserId();

        for(Contact c : contacts) {
            Task newTask = new Task();
            newTask.OwnerId = currentUserId;
            newTask.CreatedById = currentUserId;
            newTask.ActivityDate = Date.valueOf(callSheetDetails.get('ActivityDate')); 
            newTask.Type = callSheetDetails.get('Type');
            newTask.Status = callSheetDetails.get('Status');
            newTask.Priority = callSheetDetails.get('Priority');
            newTask.Subject = callSheetDetails.get('Subject');
            newTask.WhoId = c.Id;
            newTask.WhatId = c.AccountId;

            newCallSheets.add(newTask);
        }
        //Throw new AuraHandledException
        insert newCallSheets;
    }

    @AuraEnabled
    public static void addContactToCampaign(List<Contact> contacts, Id campaignId) {
        List<CampaignMember> campaignMemberList = new List<CampaignMember>();
        for(Contact c : contacts) {
            CampaignMember cm = new CampaignMember();
            cm.ContactId = c.Id;
            cm.CampaignId = campaignId;

            campaignMemberList.add(cm);
        }
        //Throw new AuraHandledException
        //insert campaignMemberList;
		Database.insert(campaignMemberList, false);
    }
	
	@AuraEnabled
    public static void addAllContactsToCampaign(Id campaignId, string queryString) {
        
        String searchResponse = SearchController.search('Search Service',queryString);
        String userId = UserInfo.getUserId();
        String searchQuery;
        object hitsobj;
        object totalCnt;
        Integer totalCount;
        if(!String.isBlank(queryString)){
            searchQuery = queryString.replaceAll('from=0','from=dynamicinput');
        }
        Map<String,Object>responseMap = new Map<String,Object>();
        responseMap = (Map<String, Object>)JSON.deserializeUntyped(searchResponse);
        if(responseMap!= null && responseMap.containsKey('response')){
            hitsobj = responseMap.get('response');
        }
        Map<String,Object>hitsobj_Map = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(hitsobj));
        if(hitsobj_Map != null && hitsobj_Map.containsKey('hits')){
            totalCnt = hitsobj_Map.get('hits');
        }
        Map<String,Object>totalCnt_Map = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(totalCnt));
        if(totalCnt_Map != null && totalCnt_Map.containsKey('total') && totalCnt_Map.get('total') != '' && totalCnt_Map.get('total') != null){
            totalCount = Integer.valueOf(totalCnt_Map.get('total'));
        }
        String dynamicCount = System.Label.ATS_ADD_ALL_TO_CAMPAIGN;
        if(totalCount != null && totalCount>=9999){
            totalCount = 10000;
        }
        Integer totalcallsRequired;
        if(totalCount != null){
            totalcallsRequired = totalCount/integer.valueOf(dynamicCount);
        }else if(totalCount < integer.valueOf(dynamicCount)){
            totalcallsRequired = 1;
        }
        if(totalCount > 0){
            System.enqueuejob(new  TC_addAllTalentstoCampaign(campaignId , totalCount, totalcallsRequired, userId, searchQuery));
        }
    }
	
    @AuraEnabled
    public static String getTalentDefaultResume(Contact record) {
        String resumeId;

        try {
            resumeId = SearchController.getAttachmentId(record.AccountId); 
        } catch(QueryException ex) {
            System.debug('ERROR GRABBING DOCUMENT');
            throw new AuraHandledException(ex.getMessage()); 
        }

        return resumeId;
    }
	@AuraEnabled
    public Static String addToPipeLine(String methodName, Map<String, Object> parameters){
         return (String)ATSContactListFunctions.performServerCall(methodName,parameters);
    }
    @AuraEnabled
    public Static Boolean removeFromPipeLine(String methodName, Map<String, Object> parameters){
        return (Boolean)ATSContactListFunctions.performServerCall(methodName,parameters);
    }   
    @AuraEnabled
    public static CallSheetModalFields getPicklistValues() {
        Map<string,string> typeMap = ContactListController.extractFieldset(Task.type.getDescribe());
        Map<string,string> statusMap = ContactListController.extractFieldset(Task.status.getDescribe());            
        Map<string,string> priorityMap = ContactListController.extractFieldset(Task.priority.getDescribe());

        return new CallSheetModalFields(typeMap, statusMap, priorityMap);
    }


    private static Map<String, String> extractFieldset(Schema.DescribeFieldResult fields) {
        Map<String, String> fieldMap = new Map<String, String>();

        List<Schema.PicklistEntry> fieldList = fields.getPicklistValues();
        for(Schema.PicklistEntry f : fieldList) {
            fieldMap.put(f.getValue(), f.getLabel());
        }

        return fieldMap;
    } 
 

    public class ContactListsAndProfileName {
        @AuraEnabled
        public Map<String, List<Contact>> contactMap;
        @AuraEnabled
        public ATS_UserOwnershipModal uoModal;
		@AuraEnabled
		public Boolean hasUserSMSAccess;//S-135076 
		@AuraEnabled
        public Map<String, String> contactNotes;
    }

    public class CallSheetModalFields {
        @AuraEnabled
        public Map<String, String> typeMapping;
        @AuraEnabled
        public Map<String, String> statusMapping;
        @AuraEnabled
        public Map<String, String> priorityMapping;

        CallSheetModalFields(Map<String, String> typeMap, Map<String, String> statusMap, Map<String, String> priorityMap) {
            this.typeMapping = typeMap;
            this.statusMapping = statusMap;
            this.priorityMapping = priorityMap;
        }
    }


   @AuraEnabled 
   public static Boolean userHasAccessPermission(String permissionName){
    Boolean hasAccess=false;
    Boolean hasCustomPermission = FeatureManagement.checkPermission(permissionName);
    System.debug('hasCustomPermission'+hasCustomPermission);
    return hasCustomPermission;
   }

    @AuraEnabled 
   public static string setNotesInContactTag(String notes, string tagId, string contactId){
		List<Contact_Tag__c> contactList = [select id, Notes__c from Contact_Tag__c where Tag_Definition__c =:tagId and Contact__c = :contactId limit 1];
		if (contactList.size() > 0) {
			contactList[0].notes__c = notes;
			update contactList[0];
		}
		return 'SUCCESS';
   }
   
}