@isTest
public class ContactListControllerTest  {
    @TestSetup
    static void insertApigeeOAuthSettings() {
        //I shouldn't have to insert this
        insert new ApigeeOAuthSettings__c(name = 'SOALatLong', Client_Id__c='1234567', Service_Http_Method__c='POST', Service_URL__c='http://allegisgrouptest.com',Token_URL__c='http://test.com');
    }

    @isTest
    static void getContactsForList_givenListWithNoContacts_returnsEmptyLists() {
        //Map<String, List<Contact>> resultList = new Map<String, List<Contact>>();//Sandeep
        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list name';

        insert testList;

        Test.startTest();
        //Sandeep: return type changed from the method 
        ContactListController.ContactListsAndProfileName  resultList = ContactListController.getContactsForList(testList.Id);
        Test.stopTest();

        System.assertEquals(2, resultList.contactMap.size(), 'Two lists should have been returned.');
        System.assertEquals(0, resultList.contactMap.get('Client').size(), 'The Client list should be empty.');
        System.assertEquals(0, resultList.contactMap.get('Talent').size(), 'The Talent list should be empty.');
    }

    @isTest
    static void getContactsForList_givenListWithClients_returnsOneEmptyList() {
        //Map<String, List<Contact>> resultList = new Map<String, List<Contact>>();

        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list name';

        insert testList;

        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';

        insert testContact;

        Contact_Tag__c tagList = new Contact_Tag__c();
        tagList.Tag_Definition__c = testList.Id;
        tagList.Contact__c = testContact.Id;

        insert tagList;

        Test.startTest();
        ContactListController.ContactListsAndProfileName   resultList = ContactListController.getContactsForList(testList.Id);
        Test.stopTest();

        System.assertEquals(2, resultList.contactMap.size(), 'Two lists should have been returned.');
        System.assertEquals(1, resultList.contactMap.get('Client').size(), 'The Client list should not be empty.');
        System.assertEquals(0, resultList.contactMap.get('Talent').size(), 'The Talent list should be empty.');
    }

    @isTest
    static void getContactsForList_givenListWithTalents_returnsOneEmptyList() {
        //Map<String, List<Contact>> resultList = new Map<String, List<Contact>>();

        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list name';

        insert testList;

        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;
        talentAcc.G2_Completed__c = true;
        insert talentAcc;

        Contact ct = TestData.newContact(talentAcc.id, 1, 'Talent'); 
        ct.Other_Email__c = 'other@testemail.com';
        ct.Title = 'test title';
        ct.Talent_State_Text__c = 'test text';
        ct.MailingState = 'test text';
        insert ct;

        Contact_Tag__c tagList = new Contact_Tag__c();
        tagList.Tag_Definition__c = testList.Id;
        tagList.Contact__c = ct.Id;

        insert tagList;

        Test.startTest();
        ContactListController.ContactListsAndProfileName  resultList = ContactListController.getContactsForList(testList.Id);
        Test.stopTest();

        System.assertEquals(2, resultList.ContactMap.size(), 'Two lists should have been returned.');
        System.assertEquals(0, resultList.ContactMap.get('Client').size(), 'The Client list should be empty.');
        System.assertEquals(1, resultList.ContactMap.get('Talent').size(), 'The Talent list should not be empty.');
    }

    @isTest
    static void getContactsForList_givenListWithTalentsAndClients_returnsNoEmptyLists() {
        //Map<String, List<Contact>> resultList = new Map<String, List<Contact>>();

        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list name';

        insert testList;

        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';

        insert testContact;

        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;
        talentAcc.G2_Completed__c = true;
        insert talentAcc;

        Contact ct = TestData.newContact(talentAcc.id, 1, 'Talent'); 
        ct.Other_Email__c = 'other@testemail.com';
        ct.Title = 'test title';
        ct.Talent_State_Text__c = 'test text';
        ct.MailingState = 'test text';
        insert ct;

        Contact_Tag__c tagList = new Contact_Tag__c();
        tagList.Tag_Definition__c = testList.Id;
        tagList.Contact__c = testContact.Id;

        insert tagList;

        Contact_Tag__c tagList2 = new Contact_Tag__c();
        tagList2.Tag_Definition__c = testList.Id;
        tagList2.Contact__c = ct.Id;

        insert tagList2;

        Test.startTest();
        ContactListController.ContactListsAndProfileName   resultList = ContactListController.getContactsForList(testList.Id);
        Test.stopTest();

        System.assertEquals(2, resultList.ConTactMap.size(), 'Two lists should have been returned.');
        System.assertEquals(1, resultList.ConTactMap.get('Client').size(), 'The Client list should be empty.');
        System.assertEquals(1, resultList.ConTactMap.get('Talent').size(), 'The Talent list should not be empty.');
    }

    @isTest
    static void removeContactFromList_givenContactToRemove_returnsOneEmptyList() {
        Map<String, List<Contact>> resultList = new Map<String, List<Contact>>();

        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list name';

        insert testList;

        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';

        insert testContact;

        Contact_Tag__c tagList = new Contact_Tag__c();
        tagList.Tag_Definition__c = testList.Id;
        tagList.Contact__c = testContact.Id;

        insert tagList;

        List<Contact_Tag__c> firstResult = new List<Contact_Tag__c>();
        List<Contact_Tag__c> finalResult = new List<Contact_Tag__c>();

        Test.startTest();
            firstResult = [SELECT Id FROM Contact_Tag__c WHERE Id =: tagList.Id];
            ContactListController.removeContactFromList(testList.Id, testContact.Id);
            finalResult = [SELECT Id FROM Contact_Tag__c WHERE Id =: tagList.Id];
        Test.stopTest();

        System.assertEquals(1, firstResult.size(), 'The tag should have been returned.');
        System.assertEquals(0, finalResult.size(), 'The tag should have been deleted.');
    }

    @isTest
    static void removeContactFromList_givenRemovedContact_returnsFalse() {
        Map<String, List<Contact>> resultList = new Map<String, List<Contact>>();

        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list name';

        insert testList;

        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';

        insert testContact;

        Contact_Tag__c tagList = new Contact_Tag__c();
        tagList.Tag_Definition__c = testList.Id;
        tagList.Contact__c = testContact.Id;

        insert tagList;
        delete tagList;

        Boolean result = true;

        Test.startTest();
            result = ContactListController.removeContactFromList(testList.Id, testContact.Id);
        Test.stopTest();

        System.assertEquals(false, result, 'The method should have caught the exception and returned false.');
    }
    @isTest
    static void removeSelectedContactsFromListTest() {
        Map<String, List<Contact>> resultList = new Map<String, List<Contact>>();

        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list name';
        insert testList;

        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';
        insert testContact;

        Contact_Tag__c tagList = new Contact_Tag__c();
        tagList.Tag_Definition__c = testList.Id;
        tagList.Contact__c = testContact.Id;
        insert tagList;
		List<String> contactIds = new List<String>();
        contactIds.add(testContact.Id);
        Boolean result = true;

        Test.startTest();
            result = ContactListController.removeSelectedContactsFromList(testList.Id, contactIds);
        Test.stopTest();

    }
    
    @isTest
    static void addContactsToCallsheetTest() {
        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';
        insert testContact;
	
    	List<Contact> conList = new List<Contact>();
        conList.add(testContact);
        
        Map<String, String> testDetails = new Map<String, String>();
        Date d = System.today();
        
        testDetails.put('ActivityDate', string.valueOf(d));
        testDetails.put('Type', 'test');
        testDetails.put('Status', 'test Status');
        testDetails.put('Priority', 'test Priority');
        testDetails.put('Subject', 'test Subject');
        
        Test.startTest();
        	ContactListController.addContactsToCallSheet(conList, testDetails);
        Test.stopTest();
    }
    
    @isTest
    static void addContactToCampaign() {
        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';
        insert testContact;
	
    	List<Contact> conList = new List<Contact>();
        conList.add(testContact);
        
        Campaign cam = new Campaign();
        
        Test.startTest();
        	ContactListController.addContactToCampaign(conList, cam.Id);
        Test.stopTest();
    }
    
    @isTest
    static void getPicklistValuesTest() {
        Test.startTest();
        	ContactListController.CallSheetModalFields x = ContactListController.getPicklistValues();
        Test.stopTest();
    }
    
    @isTest 
    static void getTalentDefaultResumeTest() {
        Contact testContact = new Contact();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        testContact.RecordTypeId = RecordTypeIdContact;
        testContact.LastName = 'Wright';
        testContact.Email = 'test@allegisgroup.com';
        insert testContact; 
        
        Test.startTest();
        	ContactListController.getTalentDefaultResume(testContact);
        Test.stopTest();
    }
    
    @isTest
    static void userHasAccessPermissionTest() {
        Test.startTest();
        	ContactListController.userHasAccessPermission('permissionName');
        Test.stopTest();
    }

}