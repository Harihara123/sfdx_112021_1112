global class ContactLocationUpdater implements Database.Batchable<sObject> {

    String query;
    global ContactLocationUpdater(String q) {
        if (String.isBlank(q)) {
            this.query = 'SELECT Id, Talent_State_Text__c, Talent_Country_Text__c, MailingState, MailingCountry FROM Contact where RecordType.Name=\'Talent\' AND (MailingState != null OR MailingCountry != null)';
        } else {
            this.query = q;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        Map<String,String> countryMap = new Map<String,String>();
        Map<String,String> stateMap = new Map<String,String>();
        
        List<Contact> records = new List<Contact>();
        
        for (Global_LOV__c country : [SELECT Text_Value__c, Text_Value_3__c FROM Global_LOV__c WHERE LOV_Name__c = 'CountryList']){
             countryMap.put(country.Text_Value__c, country.Text_Value_3__c);
        }
                
        for (Global_LOV__c state : [SELECT Text_Value_2__c, Text_Value_3__c FROM Global_LOV__c WHERE LOV_Name__c = 'StateList']){
             stateMap.put(state.Text_Value_2__c, state.Text_Value_3__c);
        }
        
        for(Contact c: scope) {
            c.Talent_State_Text__c = stateMap.get(c.MailingState);
            c.Talent_Country_Text__c = countryMap.get(c.MailingCountry);
            records.add(c);
        }
        
        try {
            List<Database.SaveResult> results;
            List<ID> cIds = new List<ID>();
            
            if (!records.isEmpty()) {
                System.debug(logginglevel.WARN,'Accounts to update Talent_Preference_Completion_Flags__c - ' + records.size());
                results = Database.update(records, false);
                System.debug(logginglevel.WARN,'results :' + results);
                Integer errCount = 0;
                for (Database.SaveResult result : results) {
                    if (!result.getErrors().isEmpty()) {
                        System.debug(logginglevel.WARN,'Errors :' + result.getErrors());
                        errCount++;
                    }
                    if (result.isSuccess()) {
                        cIds.add(result.getId());
                    }
                }
                System.debug(logginglevel.WARN,'Unsuccessful Updates :' + errCount);
                System.debug(logginglevel.WARN,'Successful Updates :' + cIds.size());
            }
        } catch (DmlException de) {
            Integer numErrors = de.getNumDml();
            System.debug(logginglevel.WARN,'getNumDml=' + numErrors);
        } catch (Exception e) {
            System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC){
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('HRXML Updates ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}