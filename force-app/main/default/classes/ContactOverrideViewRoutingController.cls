public class ContactOverrideViewRoutingController{

   private final ApexPages.StandardController controller;
   private static Map<Id,String> contactRecordTypeNameCache = null;
   public string url {get;set;}
   
   public ContactOverrideViewRoutingController(ApexPages.StandardController controller) 
   {
        this.controller = controller;
        url = '';
   }
   
     public void onLoad() 
    {
        Contact c = [Select id, recordtypeid, Accountid, contact.Account.Talent_Ownership__c From Contact Where Id = :ApexPages.currentPage().getParameters().get('id')];
        if(c.recordtypeid == System.Label.CRM_CONTACTCLIENTRECTTYPE || (c.Account.Talent_Ownership__c != null && c.Account.Talent_Ownership__c.contains('Community')))
        {
              url = '/' + c.id +  '?nooverride=1';
        }
        else if(c.recordtypeid ==  System.Label.CRM_TALENTCONTACTRECTYPE && (c.Account.Talent_Ownership__c != null && !c.Account.Talent_Ownership__c.contains('Community')))
        {
              //if it's not mobile l
              if (UserInfo.getUiThemeDisplayed() != 'Theme4t') {
                url = '/' +  c.Accountid;
              }
        }
        else if(c.recordtypeid == System.Label.CRM_REFERENCECONTACTRECTYPE)
        { 
            List<Talent_Recommendation__c> recs = [select Talent_Contact__r.AccountId from Talent_Recommendation__c where Recommendation_From__c = :c.id LIMIT 1];
            if(recs.size() > 0)
            {
                url = '/' + recs[0].Talent_Contact__r.AccountId;
            }
            else
            {
                url = '';
            }
        }
        else
        {
              url = '';
        }
    }
    



}