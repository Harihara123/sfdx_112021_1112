@isTest public class ContactOverrideViewRoutingControllerTest{
@isTest static void test_RoutingForTalentRecordType(){
    Profile p = [select id from profile where name='System Administrator']; 
    //User as System Admin
    User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
     System.runAs(u){
          Test.startTest(); 
           Account a = new Account(RecordTypeId = System.Label.CRM_TALENTACCOUNTRECTYPE,
                                    Name = 'bwiIadTestContact' + String.Valueof(Math.random()),
                                    Talent_Ownership__c = 'RPO');
           Insert a;
           Contact c =  new Contact(LastName = 'Smith',FirstName = 'Bobby',MobilePhone = '1111111999',Email='bbysmith@test.com',
                                     recordtypeid=System.Label.CRM_TALENTCONTACTRECTYPE);
           c.AccountId = a.Id;
           Insert c; 
         Test.stopTest();  
            
            PageReference pref =  Page.CandidateSummary;
            pref.getParameters().put('id', c.Id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController conCtrl = new ApexPages.StandardController(c);
            ContactOverrideViewRoutingController ctrl = new ContactOverrideViewRoutingController(conCtrl);
            
            ctrl.onLoad();
               }
            }
            
@isTest static void test_RoutingForClientRecordType(){
       Profile p = [select id from profile where name='System Administrator']; 
       //User as System Admin
       User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
      System.runAs(u){
           List<RecordType> recAccTypes = [select Id from recordtype where name ='Client' and SobjectType = 'Account' LIMIT 1];
           List<RecordType> recTypes = [select Id from recordtype where name ='Client' and SobjectType = 'Contact' LIMIT 1];
           Account a =  new Account(RecordTypeId = recAccTypes[0].Id,
                                    Name = 'TestContctAccount' + String.Valueof(Math.random()));
              
          Test.startTest();            
             Insert a;
            
              Contact c =  new Contact(LastName = 'Smithsonian',FirstName = 'Bobb',MobilePhone = '2291111111',Email='bsonsmith@test.com',
                                     recordtypeid = recTypes[0].Id); 
              c.AccountId = a.Id;
              Insert c; 
           Test.stopTest();  
            
            PageReference pref =  Page.CandidateSummary;
            pref.getParameters().put('id', c.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController conCtrl = new ApexPages.StandardController(c);
            ContactOverrideViewRoutingController ctrl = new ContactOverrideViewRoutingController(conCtrl);
            
          ctrl.onLoad();
          }
    } 

@isTest static void test_RoutingForReferenceRecordType(){
       Profile p = [select id from profile where name='System Administrator']; 
       //User as System Admin
       User u = [select Id from User where ProfileId =: p.Id and isactive = true Limit 1];        
      System.runAs(u){
           Account z =  new Account(RecordTypeId = System.Label.CRM_REFERENCEACCOUNTRECTYPE,
                                    Name = 'TestReferenceAccount12' + String.Valueof(Math.random()));
           
           Test.startTest();   
             Insert z;

             Contact i =  new Contact(LastName = 'Flacco',FirstName = 'joe',MobilePhone = '2231111188',Email='bflaccoh@test.com',
                                     recordtypeid = System.Label.CRM_REFERENCECONTACTRECTYPE); 
             i.AccountId = z.Id;
             Insert i;
            
             Talent_Recommendation__c talent = new Talent_Recommendation__c();
             talent.Recommendation_From__c  = i.Id;
             talent.Talent_Contact__c = i.Id;
             Insert talent;
           Test.stopTest();  
            
            PageReference preff =  Page.CandidateSummary;
            preff.getParameters().put('id', i.Id);
            Test.setCurrentPage(preff);
            ApexPages.StandardController cntCtrl = new ApexPages.StandardController(i);
            ContactOverrideViewRoutingController ctrl = new ContactOverrideViewRoutingController(cntCtrl);
            
           ctrl.onLoad();
          }
    } 

}