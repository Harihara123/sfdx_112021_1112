Public with sharing class ContactPageExtension{
 private Contact Con;
    Set<id> duplicateid = new set<id>();
    List<Contact> duplicateCntLst = new List<contact>();
    
    // Initialize a list to hold any duplicate records
    private List<sObject> duplicateRecords;
    Map<string,string> mdtprofiles=new Map<string,string>();
    
    // Define variable that’s true if there are duplicate records
    public boolean hasDuplicateResult{get;set;}
    
    private ApexPages.StandardController stdController;    
    
    public ContactPageExtension(ApexPages.StandardController controller) { 
        String ACCID = ApexPages.CurrentPage().GetParameters().Get('accid');       
        //pre-populate values
        system.debug('@@@@@' +ACCID);
        if(ACCID <> NULL){
        Account ConAccount = [Select id, Phone, BillingCity, BillingStreet, BillingState, BillingPostalCode, BillingCountry from Account where id=: ACCID];
        this.Con = (Contact)controller.getRecord();
        Con.Phone =  ConAccount.Phone;
        Con.MailingStreet= ConAccount.BillingStreet;
        Con.MailingCity= ConAccount.BillingCity;
        Con.MailingState= ConAccount.BillingState;
        Con.MailingPostalCode= ConAccount.BillingPostalCode;
        Con.MailingCountry= ConAccount.BillingCountry;         
  
        }
        stdcontroller=controller;
        system.debug('stdcontroller'+stdcontroller );
        this.duplicateRecords = new List<sObject>();
        this.hasDuplicateResult = false;
    }    
    
    // Return duplicate records to the Visualforce page for display
    public List<sObject> getDuplicateRecords() {
        return this.duplicateRecords;
    }

     public PageReference save1() {
        Id id = stdController.getId();
        String ACCID = ApexPages.CurrentPage().GetParameters().Get('accid');
        String RecordType = ApexPages.CurrentPage().GetParameters().Get('RecordType');
         Contact contact=(Contact) stdcontroller.getRecord();
         system.debug('contact' + contact);
         Database.SaveResult saveResult = Database.insert(contact, false);
         system.debug('saveresult' +saveresult.geterrors());
         
         if (!saveResult.isSuccess()) {
            for (Database.Error error : saveResult.getErrors()) {
                
                // If there are duplicates, an error occurs
                // Process only duplicates and not other errors 
                //   (e.g., validation errors)
                if (error instanceof Database.DuplicateError) {
                    // Handle the duplicate error by first casting it as a 
                    //   DuplicateError class
                    // This lets you use methods of that class 
                    //  (e.g., getDuplicateResult())
                    Database.DuplicateError duplicateError = 
                            (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = 
                            duplicateError.getDuplicateResult();
                    system.debug('duperesult'+duplicateResult);
                    
                    // Display duplicate error message as defined in the duplicate rule
                    ApexPages.Message errorMessage = new ApexPages.Message(
                            ApexPages.Severity.ERROR, 'Duplicate Error: ' + 
                            duplicateResult.getErrorMessage());
                    ApexPages.addMessage(errorMessage);
                    
                    // Get duplicate records
                    this.duplicateRecords = new List<sObject>();

                    // Return only match results of matching rules that 
                    //  find duplicate records
                    Datacloud.MatchResult[] matchResults = 
                            duplicateResult.getMatchResults();

                    // Just grab first match result (which contains the 
                    //   duplicate record found and other match info)
                    Datacloud.MatchResult matchResult = matchResults[0];

                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    // Add matched record to the duplicate records variable
                    for (Datacloud.MatchRecord matchRecord : matchRecords) {
                        System.debug('MatchRecord: ' + matchRecord.getRecord());
                        duplicateid.add(matchrecord.getrecord().id);
                        //this.duplicateRecords.add(matchRecord.getRecord());
                        system.debug('duplicateid' + duplicateid);
                    }
                    
                    duplicateCntLst=[select id,Name, Accountid,Account.name, phone, MobilePhone,Email,ownerid,LastModifiedDate from Contact where id in:duplicateid];
                    system.debug('duplicateCntLst' + duplicateCntLst);
                    
                    if(!duplicateCntLst.isEmpty()){
                        this.duplicateRecords= duplicateCntLst;
                        system.debug('duplicateRecords'+ this.duplicateRecords );
                    }
                   this.hasDuplicateResult = !duplicateCntLst.isEmpty();
                    system.debug('hasdup' + this.hasDuplicateResult );
                }
                else{
                    
               this.hasDuplicateResult=false;
                    ApexPages.Message errorMessage = new ApexPages.Message(
                            ApexPages.Severity.ERROR, error.getmessage());
                    ApexPages.addMessage(errorMessage);
                }
                
                    
            }
            
            //If there’s a duplicate record, stay on the page
            return null;
        }
         
         else{
 
       // stdController.save();
        Boolean saveAndNew = false;
        system.debug('###########'+Apexpages.currentPage().getParameters());
        for(String key : Apexpages.currentPage().getParameters().keySet()){
            if(key.contains('saveAndNew')){
                saveAndNew = true;
                break;
            }
        }
        if(saveAndNew){
            Apexpages.currentPage().setRedirect(true);
            if(ACCID <> NULL){
            return new PageReference('/'+stdController.getRecord().getSObjectType().getDescribe().getKeyPrefix()+'/e?retURL=%2F'+ACCID+'&accid='+ACCID+'&RecordType='+RecordType+'&ent=Contact&save_new=1&sfdc.override=1');
            }else{
            return new PageReference('/'+stdController.getRecord().getSObjectType().getDescribe().getKeyPrefix()+'/e?retURL=%2F003%2Fo&RecordType='+RecordType+'&ent=Contact');
            }
        }
             else{
            //return (new ApexPages.StandardController(contact)).view();
            for(ProfilesforAutoTalent__mdt mdt:[select Profileid__c,DeveloperName from ProfilesforAutoTalent__mdt ]){
           mdtprofiles.put(mdt.Profileid__c, mdt.DeveloperName);
            }
                 
         if((mdtprofiles.get(userinfo.getProfileId())!=null)&& ((contact.MobilePhone != null) || (contact.HomePhone != null) || (contact.Phone != null) || (contact.Email != null) || (contact.Other_Email__c != null))){
             
             //PageReference acctPage =  new PageReference('/one/one.app#/sObject/' + contact.Id + '/view' +'?ClientFlow=true'); // S-75078 - Santosh Chitalkar
             // changes for lightning URL critical updates
             String url = '';
             String urlChangesOn = System.Label.CONNECTED_Summer18URLOn;
             String newUrl = System.Label.CONNECTED_Summer18URL;
             if(urlChangesOn != null && (urlChangesOn.equalsIgnoreCase('true'))){
                 url = newUrl + '/r/Contact/' + contact.Id + '/view' +'?ClientFlow=true';
             }else
             {
                 url = newUrl + '/sObject/' + contact.Id + '/view' +'?ClientFlow=true';
             }
            PageReference acctPage =  new PageReference(url);
             
            //PageReference acctPage =  new PageReference('/one/one.app#/n/Find_or_Add_Candidate?clientcontactid='+contact.Id );
            //PageReference acctPage =  new PageReference('/one/one.app#/n/Add_Candidate_Ltng?clientcontactid='+contact.Id );
            //PageReference acctPage =  new PageReference('/apex/CandidateDuplicateCheck?clientcontactid='+contact.Id );

              acctPage.setRedirect(true);
              return acctPage;
         }
                 else return (new ApexPages.StandardController(contact)).view();
         }
            
            
         
     }
         
    }
}