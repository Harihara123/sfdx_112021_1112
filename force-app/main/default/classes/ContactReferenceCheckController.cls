public with sharing class ContactReferenceCheckController{
   @AuraEnabled
    public static Contact getContactReference(Id contactId){
    try {
           String returnVal ='';
            Contact c = [Select Related_Contact__c, MobilePhone, HomePhone, Phone, Email, Other_Email__c,IsPopupShown__c From Contact Where Id =: contactId ];
            if(c.Related_Contact__c == null && c.IsPopupShown__c == false){
              // && ((c.MobilePhone != null) || (c.HomePhone != null) || (c.Phone != null) || (c.Email != null) || (c.Other_Email__c != null))
                 //returnVal = 'none';
                 return null;
            }else 
            {
                  // returnVal = 'present';
                  return c;
            }  
          //return returnVal;
         }catch(Exception ex){
            return null;
        }
    }    
   @AuraEnabled
    public static String getDuplicateCheckUrl(Id contactId) {
        try {
            String returnUrl='';

            if (contactId != null) {
                /*PageReference newPage =  new PageReference('/apex/CandidateDuplicateCheck'); 
                newPage.getParameters().put('clientcontactid', contactId);
                returnUrl = (newPage != null)? newPage.getUrl() : '';*/
                //returnUrl = '/one/one.app#/n/Add_Candidate_Ltng?clientcontactid=' + contactId;
                //returnUrl = '/one/one.app#/n/Find_or_Add_Candidate?clientcontactid=' + contactId;
                // changes for lightning URL critical updates                 
                 String urlChangesOn = System.Label.CONNECTED_Summer18URLOn;
                 String newUrl = System.Label.CONNECTED_Summer18URL;
                 if((urlChangesOn != null && (urlChangesOn.equalsIgnoreCase('true')))){
	                     returnUrl = newUrl + '/n/Find_or_Add_Candidate?clientcontactid=' + contactId;
                     }else{
    					 returnUrl = newUrl + '/n/Add_Candidate_Ltng?clientcontactid=' + contactId;                     
                     }
            } else {
                returnUrl = 'none';
            } 
            return returnUrl;
        } catch(Exception ex){
            return '';
        }
    }
    
@AuraEnabled
public static String savePopupFlag(Id contactId) {
        try {
            String returnVal ='';
            if (contactId != null) {
                Contact c = [Select IsPopupShown__c From Contact Where Id =: contactId];
                c.IsPopupShown__c = true;
                update c;
               return 'saved'; 
            } else {
                return 'none';
            } 
           // return returnUrl;
        } catch(Exception ex){
           
            System.debug('excep'+ex);
             return '';
        }
    }

    
}