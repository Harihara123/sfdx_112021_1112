@isTest
public class ContactReferenceCheckController_Test {
    @testSetup static void testSetup(){
        Contact cont = new Contact();        
        cont.LastName = 'testcon';
        insert cont;
    }

    @isTest static void test_success_getDuplicateCheckUrl() {
        Contact cont = [Select id From contact LIMIT 1];
        String resp = ContactReferenceCheckController.getDuplicateCheckUrl(cont.Id);
        System.debug('test_getDuplicateCheckUrl '+ resp);
        System.assert(resp != '' && resp != 'none');
    }
     @isTest static void test_success_getContactReference() {
        Contact cont = [Select id From contact LIMIT 1];
        Contact resp = ContactReferenceCheckController.getContactReference(cont.Id);
        System.debug('test_getDuplicateCheckUrl '+ resp);

    }
    @isTest static void test_sucess_savePopupFlag(){
        Contact cont = [Select id From contact LIMIT 1];
        String resp = ContactReferenceCheckController.savePopupFlag(cont.Id);
    }
    @isTest static void test_fail_savePopupFlag(){
        Contact cont =new Contact();
        String resp = ContactReferenceCheckController.savePopupFlag(cont.Id);
    }
    @isTest static void test_fail_getDuplicateCheckUrl() {
          Contact cont =new Contact();
        String resp = ContactReferenceCheckController.getDuplicateCheckUrl(cont.Id);
    }
}