public class ContactRoleUtility {
    
    public static void createContactRolesForActivities (Map<Id,sObject> newMap) {
       	Set<Id> contactIds = new Set<Id> ();
        Set<Id> opportunityIds = new Set<Id> ();
        List<sObject> filteredActivities = new List<sObject> ();
        
        for(sObject activity: newMap.values()){            
            if(activity.get('WhoId') != null && activity.get('WhatId') != null){
                String whoObjType = String.valueOf(((Id)activity.get('WhoId')).getSObjectType());
            	String whatObjType = String.valueOf(((Id)activity.get('WhatId')).getSObjectType());
                
                if(whoObjType == 'Contact' && whatObjType == 'Opportunity') {
                    contactIds.add((Id)activity.get('WhoId'));
                    opportunityIds.add((Id)activity.get('WhatId'));
                    filteredActivities.add(activity);
                }
            }
        }
        
        if(contactIds.isEmpty() && opportunityIds.isEmpty()) return;
        
        Map<Id, Contact> contactsMap = new Map<Id, Contact> ([Select Id from Contact where Id IN: contactIds AND RecordType.DeveloperName = 'Client']);
        Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity> ([Select Id, (Select Id, OpportunityId, ContactId from OpportunityContactRoles) from Opportunity where Id IN: opportunityIds AND RecordType.DeveloperName = 'Talent_First_Opportunity']);
        
        if(contactsMap.isEmpty() && opportunitiesMap.isEmpty()) return;
        
        Map<Id, Set<Id>> opportunityExistingContactRolesMap = new Map<Id, Set<Id>> ();
            
        for(Opportunity opp: opportunitiesMap.values()) {
            if(!opp.OpportunityContactRoles.isEmpty()) {
                for(OpportunityContactRole role: opp.OpportunityContactRoles) {
                    if(opportunityExistingContactRolesMap.get(opp.Id) != null) {
                        Set<Id> existingContactIds = opportunityExistingContactRolesMap.get(opp.Id);
                        existingContactIds.add(role.ContactId);
                        opportunityExistingContactRolesMap.put(role.OpportunityId, existingContactIds);
                    } else {
                        opportunityExistingContactRolesMap.put(role.OpportunityId, new Set<Id> {role.ContactId});
                    }
                }
            }
        }
        
        List<OpportunityContactRole> contactRolesToInsert = new List<OpportunityContactRole> ();
        
        for(sObject activity: filteredActivities) {
            if(
                contactsMap.get((Id)activity.get('WhoId')) != null && 
                opportunitiesMap.get((Id)activity.get('WhatId')) != null
            ) {
                Id contactId = (Id)activity.get('WhoId');
                Id opportunityId = (Id)activity.get('WhatId');
                if(
                    opportunityExistingContactRolesMap.get(opportunityId) == null ||
                    (
                        opportunityExistingContactRolesMap.get(opportunityId) != null &&
                    	!opportunityExistingContactRolesMap.get(opportunityId).contains(contactId)
                    )
                ) {
                    OpportunityContactRole newRole = new OpportunityContactRole(
                       	ContactId = contactId,
                        OpportunityId = opportunityId,
                        Role = 'Hiring Manager'
                    );
                    contactRolesToInsert.add(newRole);
                }
            }
        }
        
        if(!contactRolesToInsert.isEmpty()) insert contactRolesToInsert;
    }
}