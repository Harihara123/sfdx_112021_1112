@isTest
public class ContactRoleUtilityTest {
    @testsetup
    static void setup(){
        Account accnt = TestData.newAccount(1);
			accnt.Name = 'testAccount';
			accnt.Talent_Committed_Flag__c = true;
			accnt.Source_System_Id__c = null;
			insert accnt; 
        Contact cont = TestData.newContact(accnt.Id,1,'Client');
        insert cont;
        Opportunity opp= new Opportunity(
            Name = 'TESTOPP',
            recordTypeId =  Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Talent First Opportunity').getRecordTypeId(),
            CloseDate = system.today(),
            StageName = 'test',
            
            accountId= accnt.Id,
            Req_Job_Description__c = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890124',
            Req_Job_Title__c = 'Sr Java Developer   ',
            Location__c = 'Hanover, MD',
            CreatedDate=system.today()
        );
        insert opp;
        //OpportunityContactRole contactRole = new OpportunityContactRole(ContactId = cont.Id,OpportunityId =opp.Id);
       // insert contactRole;
    }
    static testMethod void createContactRolesForActivities_positiveScenario(){
       Opportunity oppt =[Select Id,Name,accountId from Opportunity limit 1];
       Contact cont =[Select Id, Name from Contact limit 1];
       //OpportunityContactRole role =[Select Id,ContactId,OpportunityId from OpportunityContactRole limit 1];
       Task tsk =  new Task(WhoId =cont.Id,
                            whatId = oppt.Id, 
                            Subject = 'Test G2',
                            Type = 'G2',
                            Status = 'Completed' ,
                            ActivityDate =  system.today() );
        insert tsk;
        Event evt =  new Event(WhoId =cont.Id,
                            whatId = oppt.Id, 
                            Subject = 'Test G2',
                            Type = 'G2',
                            DurationInMinutes = 1,
                            ActivityDateTime = system.today(),
                            ActivityDate =  system.today() );
        insert evt;
    	List<OpportunityContactRole> role =[Select Id from OpportunityContactRole];
        System.debug('role'+role);
        System.assertEquals(1,role.size());
    }
    
}