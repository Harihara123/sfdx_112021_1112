public with sharing class ContactTagTriggerHandler  {


 private static boolean hasBeenProccessed = false;


   public void OnAfterInsert(List<Contact_Tag__c> newRecords) {
       callDataExport(newRecords);

    }


     
    public void OnAfterUpdate (List<Contact_Tag__c> newRecords) {        
       callDataExport(newRecords);

    }

    public void OnAfterDelete (Map<Id, Contact_Tag__c> oldMap) {
       callDataExport(oldMap.values());

    }




    private static void callDataExport(List<Contact_Tag__c> newRecords) {
      Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
           if (config.Enable_Trace_Logging__c) {
               ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ContactTagTriggerHandler', 'callDataExport', 
                  'Triggered ' + newRecords.size() + ' Contact_Tag__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
           }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Contact_Tag__c');  
            hasBeenProccessed = true; 
        }

    }


}