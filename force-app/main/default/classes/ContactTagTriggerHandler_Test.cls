@isTest
private class ContactTagTriggerHandler_Test{
     
  @testSetup
  static void setupTestData(){
    test.startTest();
    Contact_Tag__c contact_tag_Obj = new Contact_Tag__c();
    test.stopTest();
  }
     
 static testMethod void test_OnAfterInsert_UseCase1(){
    List<Contact_Tag__c> contact_tag_Obj  =  [SELECT Contact__c,Tag_Definition__c from Contact_Tag__c];
    System.assertEquals(false,contact_tag_Obj.size()>0);
    ContactTagTriggerHandler obj01 = new ContactTagTriggerHandler();
    obj01.OnAfterInsert(contact_tag_Obj);
  }
  static testMethod void test_OnAfterUpdate_UseCase1(){
    List<Contact_Tag__c> contact_tag_Obj  =  [SELECT Contact__c,Tag_Definition__c from Contact_Tag__c];
    System.assertEquals(false,contact_tag_Obj.size()>0);
    ContactTagTriggerHandler obj01 = new ContactTagTriggerHandler();
    obj01.OnAfterUpdate(contact_tag_Obj);
  }
  static testMethod void test_OnAfterDelete_UseCase1(){
    List<Contact_Tag__c> contact_tag_Obj1  =  new List <Contact_Tag__c>();
    System.assertEquals(false,contact_tag_Obj1.size()>0);
    ContactTagTriggerHandler obj02 = new ContactTagTriggerHandler();
    obj02.OnAfterDelete( new Map<Id,Contact_Tag__c>());
  }
}