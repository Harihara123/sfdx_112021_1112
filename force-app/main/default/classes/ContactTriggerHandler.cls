/***************************************************************************
Name        : ContactTriggerHandler
Created By  : Suraj Rajoria (Appirio)
Date        : 20 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : Class that contains all of the functionality called by the
              ContactTrigger. All contexts should be in this class.
*****************************************************************************/
public with sharing class ContactTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    String yes = 'Yes';
    String No = 'No';
    Map<Id, Contact> oldMap = new Map<Id, Contact>();
    Map<Id, Contact> newMap = new Map<Id, Contact>();

    Static String ATS_DATS_MIGRATION = 'ATS Data Migration';
    Static String TALENT = 'Talent'; 
    Static ID TALENT_RECORD_ID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(TALENT).getRecordTypeId(); 


    private static boolean hasBeenProccessed = false;



    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Contact> newRecords) {
        Contact_Preference_Centre_Update(beforeInsert, oldMap, newMap, newRecords);
        stampTalentProfileOnInsert(newRecords);
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Contact> newRecords, Map<Id, Contact>oldMap, Map<Id, Contact>newMap) {
        ContactFormerlyWithAccount(afterInsert,oldMap,newMap,newRecords);
        // Commended linked in for S-135034 
        CandidateSyncWithLinkedIn(afterInsert,oldMap,newMap,true);
        //ContactTriggeredSends(afterInsert,oldMap,newMap,newRecords);
        
        if (!Test.isRunningTest()){
            if (newRecords != null) {
                for (Contact a : newRecords) {
                    TimemachineActionEvent tae = new TimemachineActionEvent();
                    tae.RaiseObjectChangeEvent('Create', 'Contact', a.Id, a);
                }
            }
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ContactTriggerHandler', 'OnAfterInsert', 
                        'Triggered ' + newRecords.size() + ' Contact records: ' + CDCDataExportUtility.joinObjIds(newRecords));
                }
                PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Contact');
                hasBeenProccessed = true; 
            }
          
        }

    }

    
    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Contact>oldMap, Map<Id, Contact>newMap) {

        List<Contact> newRecords = newMap.values();
        ContactAccountChanged(beforeUpdate,oldmap,newmap);
        Contact_Preference_Centre_Update(beforeUpdate, oldMap, newMap, newRecords);

        stampTalentProfileOnUpdate(oldMap,newMap);
		//syncingSMSPrefCenterField(oldMap,newMap);
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Contact>oldMap, Map<Id, Contact>newMap) {
        List<Contact> newRecords = newMap.values();
        ContactFormerlyWithAccount(afterUpdate,oldMap,newMap,newRecords);
        // Commended linked in for S-135034 
        if(!System.isBatch()){
            CandidateSyncWithLinkedIn(afterUpdate,oldMap,newMap,true);
        }
        ContactMerge_UpdateTMeetingsTMeals(afterUpdate,oldMap,newMap);
        if(!TriggerStopper.ContactIntelHistory){
            TriggerStopper.ContactIntelHistory = true;
        Utility.createHistoryRecord(oldMap,newMap, 'Contact');
        }
       // ContactTriggeredSends(afterUpdate,oldMap,newMap,newRecords);
        
       if (!Test.isRunningTest()){
            if (newMap != null) {
                for (Contact a : newMap.values()) {
                    TimemachineActionEvent tae = new TimemachineActionEvent();
                    tae.RaiseObjectChangeEvent('Update', 'Contact', a.Id, a);
                }
            } 
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ContactTriggerHandler', 'OnAfterUpdate', 
                        'Triggered ' + newRecords.size() + ' Contact records: ' + CDCDataExportUtility.joinObjIds(newRecords));
                }
                PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Contact');
                hasBeenProccessed = true; 
           }
          
        }

    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Contact>oldMap) {
        ContactMerge_UpdateTMeetingsTMeals(beforeDelete,oldMap,oldMap);
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Contact>oldMap,Boolean isSyncTrigger) {
        List<Contact> oldRecords = oldMap.values();
        ContactMerge_UpdateTMeetingsTMeals(afterDelete,oldMap,oldMap);
        // Commended linked in for S-135034         
        if(!Test.isRunningTest()) {
            if(!isSyncTrigger){ // Update for S-183029
                System.debug( ' = ' + isSyncTrigger + ' ' +newMap.size());
                LinkedInCandidateSyncFunctions.deleteCandidateRecord(oldMap);   
            }else if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration){
             	LinkedInCandidateSyncFunctions.deleteCandidateRecord(oldMap);   
            }        	
        }
        
        if (!Test.isRunningTest()){
            if (oldMap != null) {
                for (Contact a : oldMap.values()) {
                    TimemachineActionEvent tae = new TimemachineActionEvent();
                    tae.RaiseObjectChangeEvent('Delete', 'Contact', a.Id, a);
                }
                Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
                if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                    if (config.Enable_Trace_Logging__c) {
                        ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ContactTriggerHandler', 'OnAfterDelete', 
                            'Triggered ' + oldMap.values().size() + ' Contact records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
                    }
                    PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Contact');
                    hasBeenProccessed = true; 
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, Contact>newMap) {
        ContactMerge_UpdateTMeetingsTMeals(afterUnDelete,newMap,newMap);
    }
    
    
    
    Public void  ContactAccountChanged(String Context,Map<Id, Contact>oldMap, Map<Id, Contact>newMap){
//        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) {        // Sudheer for ACE-414
            
            for(Contact ct:newmap.values()){
                if((ct.accountid!=oldmap.get(ct.id).accountid)&&(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
                    ct.AccountChanged__c=True;
                }

                else if(ct.email!=oldmap.get(ct.id).email){    // Sudheer for ACE-414
                    ct.Email_Verified_Date__c=null;
                    ct.Email_Verification_Status_Code__c=null;
                    ct.Email_Verification_Status_Description__c=null;                                   
                }                
            }
        
//    }
    }
    
    

    /*************************************************************************************************************************
    * Original Header: trigger Contact_formerlyWithAccount on Contact (after insert,after update)
    Apex Trigger Name:  Contact_formerlyWithAccount
    Function    :       Trigger to create account contact association records with status as Formerly with
                        when ever the contacts account is updated.
    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------
    * Pavan                     05/14/2012           Created
    * Ganesh                    05/18/2012           Included the custom setting
    * Pratz                     04/10/2013           Add an If clause to check that the event is not fired by Contact Merge
    /***************************************************************************************************************************/
    public void ContactFormerlyWithAccount(String Context, Map<Id, Contact>oldMap, Map<Id, Contact>newMap,List<Contact> newRecords) {
        final string CURRENT = 'Currently With';
        // Skip when the current user profile is 'System Integration'
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) {
        List<Account_Contact_Association__c> contactAssociationRecords = new List<Account_Contact_Association__c>();
        List<Contact> contactsFiltered = new List<Contact>();
        List<Log__c> errorLogs = new List<Log__c>();
        Map<string,Account_Contact_Association__c> existingRecords = new Map<string,Account_Contact_Association__c>();
        set<string> accountIds = new set<string>();
        set<string> contactIds = new set<string>();

          //Execute this trigger logic only if the event is not fired due to Contact merge
          if(!TriggerStopper.isMergeEvent) {
                if(!TriggerStopper.isContactUpdated){
                    // Loop over the contacts
                  for(Contact con : newRecords){
                       boolean addToList = true;
                    if(con.recordtypeid == '012U0000000DWoyIAG'|| con.recordtypeid == '012U0000000DWoxIAG' ||con.recordtypeid == '012U0000000DWozIAG'){ // adding condition to run this trigger just for CRM recordtypes - Nidish
                       if(Context.equals(afterUpdate)){
                             if(con.AccountId == oldMap.get(con.Id).AccountId)
                                 addToList = false;
                             // check if the contact title is updated
                             if(con.Title != OldMap.get(con.Id).Title)
                                 contactIds.add(con.Id);
                       }
                       if(addToList){
                           accountIds.add(con.AccountId);
                           contactsFiltered.add(con);
                       }
                    }
                  }
                }
                //update associated activitities of the contacts
                if(contactIds.size() > 0)
                    ActivityHelper.updateAssociatedActivities(contactIds);
                //Query for associated records and update them accordingly
                if(accountIds.size() > 0){
                   for(Account_Contact_Association__c rec : [select Contact_Moved_date__c,Account__c,Contact__c,Status__c,Is_Primary_Account__c from Account_Contact_Association__c where Account__c in :accountIds and contact__c in :newMap.keyset()])
                     existingRecords.put(rec.Account__c+'-'+rec.Contact__c,rec);
                }
                //Loop over the filtered contacts
                for(Contact con : contactsFiltered){
                   Account_Contact_Association__c associationRecord = new Account_Contact_Association__c();
                   if(existingRecords.get(con.AccountId+'-'+con.Id) != Null)
                       associationRecord = existingRecords.get(con.AccountId+'-'+con.Id).clone(true);
                   else{
                       associationRecord.Account__c = con.accountid;                  //assign the Account Id value to new record
                       associationRecord.contact__c = con.id;
                   }
                   if(con.IsPlacedFromSkuid__c){
                     associationRecord.IsAllegisPlacedClientContact__c = true;
                   }else{
                     associationRecord.IsAllegisPlacedClientContact__c = false;
                   }
                   associationRecord.status__c = CURRENT;                     //assign status to the new record
                   associationRecord.Is_Primary_Account__c = True;              //Check the IsPrimary Checkbox
                   associationRecord.Contact_Moved_date__c = system.now();      //Assign the date moved to present time.
                   associationRecord.Created_Using_Code__c = True;              //Check the hidden field
                   contactAssociationRecords.add(associationRecord);
                }

                //insert the records accordingly
                if(contactAssociationRecords.size() > 0){
                    //set the boolean variable
                    TriggerStopper.triggeredFromContact = true;
                    for(database.upsertResult result : database.upsert(contactAssociationRecords,false)){
                        // insert the log record accordingly
                        if(!result.isSuccess()){
                             errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                        }
                    }
                }
                 // insert error logs
                if(errorLogs.size() > 0)
                    database.insert(errorLogs,false);
              }
          }
    }


    /*public void ContactTriggeredSends(String Context, Map<Id, Contact>oldMap, Map<Id, Contact>newMap,List<Contact> newRecords) {
        if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
            System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
            et4ae5.triggerUtility.automate('Contact');
       } 
    }*/
    
    
    /***************************************************************************************************************************************
    * Orignal Header : trigger trg_ContactMerge_UpdateTMeetingsTMeals on Contact (After Update, Before Delete, After Delete, After Undelete)
    * Name        - trg_ContactMerge_UpdateTMeetings_TMeals
    * Description - This trigger fires on the After Update and After Undelete Events on Contact Record during Contact Merge and re-calculates
    *               the Total_Meetings__c Total_Meals__c and Total_Filled__c on the Winning Contact Record
    *
    * Modification Log :
    * ---------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ---------------------------------------------------------------------------
    * Pratz                       3/26/2013             Created
    *****************************************************************************************************************************************/

    public void ContactMerge_UpdateTMeetingsTMeals(String Context, Map<Id, Contact>oldMap, Map<Id, Contact>newMap) {
         //After the Losing Contact has merged with the Winning Contact, the Losing Account's associated Events
         //are Reparented to the Winning Contact and the After Update event on the Winning contact fires
         //OR
         //If the deleted contacts are undeleted by the users from the recycle bin, then the After Undelete event fires this trigger

         //If the event firing this trigger is an Update or Undelete
         set<id> contset=new set<id>();
         system.debug('Merge Event Check: '+ TriggerStopper.isMergeEvent);
         if((Context.equals(afterUpdate) && TriggerStopper.isMergeEvent) || Context.equals(afterUndelete)){
               List<Contact> TriggerNew = newMap.Values();
              //Check the TriggerStopper variable to confirm if this trigger is being fired directly in response to Contact Merge
              if(!TriggerStopper.updateContactstopper){
                 Set<Id> sWinningContactIds = new Set<Id>();
                 //Loop through the Winning contact OR Loop through the Undeleted Contacts
                 for(Contact ct : TriggerNew){
                    //Loop through the Contacts in Trigger.new and add the Contact Ids to the Set
                    if(ct.recordtypeid == '012U0000000DWoyIAG'|| ct.recordtypeid == '012U0000000DWoxIAG' ||ct.recordtypeid == '012U0000000DWozIAG'){ // adding condition to run this just for CRM record types - Nidish
                      sWinningContactIds.add(ct.Id);
                    }
                  }  
                 //Call the method "Recalculate_TMeetings_TMeals_TNumFilledPos()" of clsRecalculation Class
                 //To Recalculate the Total Meetings, Total Meals and Total Filled Position fields
                 clsRecalculation.Recalculate_TMeetings_TMeals_TNumFilledPos(sWinningContactIds);
              }
         }
         //if the event firing this trigger is after delete
         if(Context.equals(afterDelete)){
            List<Contact> TriggerOld = oldMap.Values();
            //Loop through the losing contacts to check if the MasterRecordId is not null
            for(Contact ct: TriggerOld){
                 //system.debug(ct.MasterRecordId);
                 //If the MasterRecordId is not null, then set the global variable isMergeEvent to True
                 //Note that the MasterRecordId being populated for the Losing Contact indicates that this is a merge event
                 if(ct.MasterRecordId != null){
                     TriggerStopper.isMergeEvent = True;
                 }
                 system.debug('Merge Event Set: '+ TriggerStopper.isMergeEvent);
            }
         }
         //If the event firing this trigger is Before Delete
         if(Context.equals(beforeDelete)){
               List<Contact> TriggerOld = oldMap.Values();
              //process Req Push on Contact Merge for Losing Contacts
              //This code will blanket update all Reqs for which Contact relationship was updated from losing
              //Contact to the Winning Contact. Trigger was neccesary since WF was not firing Req after Contact merge
              Set<Id> conIdSet = new Set<Id>();
              for (Contact con : TriggerOld){
              
                   
                  if(con.recordtypeid == '012U0000000DWoyIAG'|| con.recordtypeid == '012U0000000DWoxIAG' ||con.recordtypeid == '012U0000000DWozIAG'){ // Adding Condition to run this just for CRM recordtype
                    conIdSet.add(con.Id);
                    
                    if(con.related_contact__c!=null){
              contset.add(con.related_contact__c);
              }
                  }
              
              }
              
              List<contact> RelatedCon=new list<contact>();
              for(contact rcn:[select id,ispopupshown__c from contact where id in:contset]){
              rcn.ispopupshown__c= false;
              RelatedCon.add(rcn);
              }
              
              System.debug('!!!!!!!!!!!!!!!!!!!!!1' + conIdSet);
              List<Id> ListOfModifiedReqs = new List<Id>();
              //  Reqs__c[] allModifiedReqs  = [select Id from reqs__c where Primary_Contact__r.Id IN :conIdSet]; // Commented to avoid sub query to contacts which leads to "System.QueryException: Non-selective query against large object type" issue
              Reqs__c[] allModifiedReqs  = [select Id from reqs__c where Primary_Contact__c IN :conIdSet];
              Account_Contact_Association__c[] ACA = [select id,name,Contact__c, Merging_contact__c, Account__c,Is_Primary_Account__c from Account_Contact_Association__c where Contact__c IN :conIdSet];
              for (Reqs__c req : allModifiedReqs) {
                ListOfModifiedReqs.add(req.Id);
              }

              for (Account_Contact_Association__c ACALOOP : ACA) {
                ACALOOP.Merging_contact__c= TRUE;
              }
              update(ACA);
              Delete(ACA);
              database.update(RelatedCon,false);

             if (ListOfModifiedReqs.size() > 0) { 
                AsyncREQPushToIRClass.WS_AsyncREQPushToIR(ListOfModifiedReqs);
             }
         }
    }

  /*************************************************************************************************************************
    * Original Header:  Trigger Contact_Preference_Centre_Update on Contact (before insert,before update)
    Apex Trigger Name:  Contact_Preference_Centre_Update
    Function    :       Trigger to update the preference centre fields for ACE based on specific inputs.
    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                 Date                 Description
    * ----------------------------------------------------------------------------
    * Peter Beazer              01/23/2016           Created
    /***************************************************************************************************************************/
    public void Contact_Preference_Centre_Update(String Context, Map<Id, Contact>oldMap, Map<Id, Contact>newMap,List<Contact> newRecords) {
          //Execute this trigger logic only if the event is not fired due to Contact merge
          if(!TriggerStopper.isMergeEvent) {
                    // Loop over the contacts
                    for(Contact c : newRecords){
                    //Updated Allegis Placement
                    if(Context.equals(beforeInsert)){
                           if(c.IsPlacedFromSkuid__c && !TriggerStopper.allegisPlacedStopped){
                               c.IsAllegisPlacedContact__c = true;
                           }
                    }else if(Context.equals(beforeUpdate)){
                         if(!TriggerStopper.allegisPlacedStopped){
                              System.debug('updated'+c.IsPlacedFromSkuid__c);
                               c.IsAllegisPlacedContact__c = c.IsPlacedFromSkuid__c; 
                           }
                    }
                    TriggerStopper.allegisPlacedStopped = true;
                    //Only continue if the CapturedDate for the preferences has been set
                    if(c.PrefCentre_CapturedDate__c != null){
                        Boolean updatePrefCentre = true;
                        
                        if(oldMap != null){
                            if(oldMap.get(c.id) != null){
                                Contact oldC = oldMap.get(c.id);
                                //Compare the previous capture date (only update if changed)
                                if(oldC.PrefCentre_CapturedDate__c != null){
                                    DateTime oldPrefDate = oldC.PrefCentre_CapturedDate__c;
                                    if(c.PrefCentre_CapturedDate__c == oldPrefDate){
                                        updatePrefCentre = false;
                                    }
                                }
                            }
                        }
                        if(updatePrefCentre){
                            //Update Preference Centre Options on the Contact
                            c = PreferenceCentreClass.UpdatePreferenceCentre(c);
                        }
                    }
                }
          }
    }
    
	public static void CandidateSyncWithLinkedIn(String Context, Map<Id, Contact>oldMap, Map<Id, Contact>newMap,Boolean isSyncTrigger) {        
        System.debug(Context + ' ' + isSyncTrigger + ' ' +newMap.size());
		if(!Test.isRunningTest()) {
            if(!isSyncTrigger){ // Update for S-183029
                System.debug(Context + ' = ' + isSyncTrigger + ' ' +newMap.size());
                LinkedInCandidateSyncFunctions.createCandidateRecord(oldMap,newMap);
            }
            else if((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
                LinkedInCandidateSyncFunctions.createCandidateRecord(oldMap,newMap);
            }
		}
	}

 

    /** Farid 10/11/2018
    **  This method handles populating Contact Fields Profile_Last_Modified_By__c and Profile_Last_Modified_Date__c
    **  when there is change to the profile information by the users
    **  these two fields are used by SOA to determine whether they should overwrite the Profile section of a talent 
    **/ 
    private static void stampTalentProfileOnUpdate(Map<Id, Contact>oldMap, Map<Id, Contact>newMap) {

       if((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) || (UserInfo.getName() == ATS_DATS_MIGRATION)) {

            for(Contact contNewVals :newmap.values()) {

                  if(contNewVals.recordtypeid == TALENT_RECORD_ID && UserInfo.getUserType() == 'Standard') {

                         boolean isTalentProfiledChanged = false;

                         if(contNewVals.Phone != oldmap.get(contNewVals.id).Phone) {isTalentProfiledChanged = true;}
                         else if(contNewVals.MobilePhone != oldmap.get(contNewVals.id).MobilePhone) {isTalentProfiledChanged = true;}
                         else if(contNewVals.OtherPhone != oldmap.get(contNewVals.id).OtherPhone) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.HomePhone != oldmap.get(contNewVals.id).HomePhone) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.Preferred_Phone__c != oldmap.get(contNewVals.id).Preferred_Phone__c) {isTalentProfiledChanged = true;} 

                         else if(contNewVals.FirstName != oldmap.get(contNewVals.id).FirstName) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.LastName != oldmap.get(contNewVals.id).LastName) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.MiddleName != oldmap.get(contNewVals.id).MiddleName) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.Suffix != oldmap.get(contNewVals.id).Suffix) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.Preferred_Name__c != oldmap.get(contNewVals.id).Preferred_Name__c) {isTalentProfiledChanged = true;} 

                         else if(contNewVals.Email != oldmap.get(contNewVals.id).Email) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.Other_Email__c != oldmap.get(contNewVals.id).Other_Email__c) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.Work_Email__c != oldmap.get(contNewVals.id).Work_Email__c) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.Preferred_Email__c != oldmap.get(contNewVals.id).Preferred_Email__c) {isTalentProfiledChanged = true;} 

                         else if(contNewVals.MailingCity != oldmap.get(contNewVals.id).MailingCity) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.MailingStreet != oldmap.get(contNewVals.id).MailingStreet) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.MailingState != oldmap.get(contNewVals.id).MailingState) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.MailingCountry != oldmap.get(contNewVals.id).MailingCountry) {isTalentProfiledChanged = true;} 
                         else if(contNewVals.MailingPostalCode != oldmap.get(contNewVals.id).MailingPostalCode) {isTalentProfiledChanged = true;}

                         //Job Title is marked from contact PB updating account G2 LMD LMB
                     
                         if (isTalentProfiledChanged){
                                  contNewVals.Profile_Last_Modified_By__c = UserInfo.getUserId();
                                  contNewVals.Profile_Last_Modified_Date__c = System.now();
                          }

                 }
            } 
       }

    }


    /** Farid 10/11/2018
    **  This method handles populating Contact Fields Profile_Last_Modified_By__c and Profile_Last_Modified_Date__c
    **  when there is a new talent created 
    **  these two fields are used by SOA to determine whether they should overwrite the Profile section of a talent 
    **/ 
    private static void stampTalentProfileOnInsert(List<Contact> newRecords) {

       if((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) || (UserInfo.getName() == ATS_DATS_MIGRATION)) {

            for(Contact contactNewRecord : newRecords) {

                 if(contactNewRecord.recordtypeid == TALENT_RECORD_ID && UserInfo.getUserType() == 'Standard') {

                            contactNewRecord.Profile_Last_Modified_By__c = UserInfo.getUserId();
                            contactNewRecord.Profile_Last_Modified_Date__c = System.now();
                 }
            } 
       }

   }
   
   /***
   * @author : Neel Kamal
   * @date : 05/14/2019
   * SMS 360 application is using field tdc_tsw__SMS_Opt_out__c for opt-in/opt-out. 
   * Consent & preferences using field PrefCentre_Aerotek_OptOut_Mobile__c for texting consent(opt-in/opt/out).
   * So both field need to synchronize. 
    */

   /*private static void syncingSMSPrefCenterField(Map<Id, Contact>oldMap, Map<Id, Contact>newMap) {
        
        if(!TriggerStopper.sms360PrefcenterAerotekStopper && 
          (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) || (UserInfo.getName() == ATS_DATS_MIGRATION)) 
        {
            for(Contact contNewVals :newmap.values()) {
                Contact oldContact = oldMap.get(contNewVals.id);
                if(contNewVals.tdc_tsw__SMS_Opt_out__c != oldContact.tdc_tsw__SMS_Opt_out__c) {
                    TriggerStopper.sms360PrefcenterAerotekStopper = true;
                    contNewVals.PrefCentre_Aerotek_OptOut_Mobile__c = contNewVals.tdc_tsw__SMS_Opt_out__c ? 'Yes' : 'No';
                }
                else if(!contNewVals.PrefCentre_Aerotek_OptOut_Mobile__c.equalsIgnoreCase(oldContact.PrefCentre_Aerotek_OptOut_Mobile__c)) {
                    TriggerStopper.sms360PrefcenterAerotekStopper = true;
                    contNewVals.tdc_tsw__SMS_Opt_out__c = contNewVals.PrefCentre_Aerotek_OptOut_Mobile__c.equalsIgnoreCase('Yes') ? True : False;
                }
            }
        }
   }*/
  
}