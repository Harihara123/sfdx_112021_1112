/**
* test method is just for mehotd syncingSMSPrefCenterField. Others are just covered while creating contact records.
*
**/
@isTest	
public class ContactTriggerHandler_Test  {
	
	static testmethod void testSyncingSMSPrefCenterField() {
		Account acc = TestData.newAccount(1, 'Talent');
		insert acc; 
		Contact ct = TestData.newContact(acc.id, 1, 'Talent');
		insert ct;

		Contact ct1 = [select id from contact where id =: ct.id];
		//ct1.tdc_tsw__SMS_Opt_out__c = true;
		update ct1;

		Contact con = [select id, PrefCentre_Aerotek_OptOut_Mobile__c from contact where id =: ct.id];

		//System.assertEquals(con.PrefCentre_Aerotek_OptOut_Mobile__c, 'Yes');
	}

	static testmethod void testSyncingSMSPrefCenterField1() {
		Account acc = TestData.newAccount(1, 'Talent');
		insert acc; 
		Contact ct = TestData.newContact(acc.id, 1, 'Talent');
		insert ct;

		Contact ct1 = [select id from contact where id =: ct.id];
		ct1.PrefCentre_Aerotek_OptOut_Mobile__c = 'Yes';
		update ct1;

		//Contact con = [select id, tdc_tsw__SMS_Opt_out__c from contact where id =: ct.id];

		//System.assertEquals(con.tdc_tsw__SMS_Opt_out__c, True);
	}
    
    static testmethod void testDeleteContact() {
        Account acc2 = TestData.newAccount(1, 'Talent');
		insert acc2; 
		Contact ct2 = TestData.newContact(acc2.id, 1, 'Talent');
		insert ct2;
               
        delete ct2;
    }
    
    static testmethod void testContactAccountUpdate() {
        Test.startTest();
        Account acc3 = TestData.newAccount(1, 'Talent');
		insert acc3; 
		Contact ct3 = TestData.newContact(acc3.id, 1, 'Talent');
        ct3.email = 'test1@gmai.com';
		insert ct3;
        
        Account acc4 = TestData.newAccount(1, 'Talent');
		insert acc4; 
        
        ct3.AccountId = acc4.Id;
        ct3.email = 'test2@gmai.com';        
        ct3.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        ct3.PrefCentre_CapturedDate__c = System.today();
        update ct3;
        Test.stopTest();
    }
	static testmethod void testContactAccountUpdateEmail() {
        Test.startTest();
        Account acc3 = TestData.newAccount(1, 'Talent');
		insert acc3; 
		Contact ct3 = TestData.newContact(acc3.id, 1, 'Talent');
        ct3.email = 'test1@gmai.com';
		insert ct3;
        ct3.email = 'test2@gmai.com';        
        ct3.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        ct3.PrefCentre_CapturedDate__c = System.today();
        update ct3;
        Test.stopTest();
    }	
    static testmethod void testUnDeleteContact() {
        Test.startTest();
        Account acc2 = TestData.newAccount(1, 'Talent');
		insert acc2; 
		Contact ct2 = TestData.newContact(acc2.id, 1, 'Talent');
		insert ct2;               
        delete ct2;        
        undelete ct2;
        Test.stopTest();
    }
}