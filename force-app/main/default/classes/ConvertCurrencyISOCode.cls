public class ConvertCurrencyISOCode
{   
    public static Map<Id,Decimal> getMapCurrencyTypeCustom(Map<Id,String> mapOpenOpportunitiesWithCurrency)
    {
        Map<Id,Decimal> returnData = new Map<Id,Decimal>();
        Set<String> setCurrency = new Set<String>();
        Map<String,Decimal> mapISOConversionRate = new Map<String,Decimal>();
        
        for(String strCurrency : mapOpenOpportunitiesWithCurrency.Values())
        {
            system.debug('Currency::::' + strCurrency);
            system.debug('CurrencyISOCodeMappings__c.getInstance(strCurrency)::::: ' + CurrencyISOCodeMappings__c.getInstance(strCurrency));
            if(CurrencyISOCodeMappings__c.getInstance(strCurrency) != null){
                setCurrency.add(CurrencyISOCodeMappings__c.getInstance(strCurrency).Actual_ISO_Code__c);
            }
           
        }
        List<CurrencyType__c> lstCT = [select id,ConversionRate__c,IsoCode__c from CurrencyType__c where IsoCode__c in: setCurrency];
        
        for(CurrencyType__c ct : lstCT)
        {
            mapISOConversionRate.put(ct.IsoCode__c, ct.ConversionRate__c);
        }
        
        for(Id opptyId: mapOpenOpportunitiesWithCurrency.keyset())
        {
            for(String iso : mapISOConversionRate.keyset())
            {
                if(CurrencyISOCodeMappings__c.getInstance(mapOpenOpportunitiesWithCurrency.get(opptyId)).Actual_ISO_Code__c == iso)
                {
                    returnData.put(opptyId, mapISOConversionRate.get(iso));
                }
            }
        }
        return returnData;       
    }
    
    public static Map<Id,Decimal> getMapDatedCurrencyTypeCustom(Map<Id,map<string,date>> mapClosedOpportunitiesWithCurrencyAndDate)
    {
        Map<Id,Decimal> returnData = new Map<Id,Decimal>();
        Set<String> setCurrency = new Set<String>();
        Map<String,CurrencyDateData> mapAfterQueryCurrencyDateData = new Map<String,CurrencyDateData>();
        
        for(map<string,date> mapCurrencyDate : mapClosedOpportunitiesWithCurrencyAndDate.Values())
        {
            for(String strCurrency : mapCurrencyDate.keyset())
            {
                 if(CurrencyISOCodeMappings__c.getInstance(strCurrency) != null){
                setCurrency.add(CurrencyISOCodeMappings__c.getInstance(strCurrency).Actual_ISO_Code__c);
            }           
            }
            
        }
        
        System.debug('mapClosedOpportunitiesWithCurrencyAndDate.keyset()::: ' + mapClosedOpportunitiesWithCurrencyAndDate.keyset());
        System.debug('mapClosedOpportunitiesWithCurrencyAndDate.Values()::: ' + mapClosedOpportunitiesWithCurrencyAndDate.Values());
        
        List<DatedConversionRate__c> lstDCT = [select id,ConversionRate__c,StartDate__c,NextStartDate__c,IsoCode__c from DatedConversionRate__c where IsoCode__c in: setCurrency order by StartDate__c desc limit :Limits.getLimitQueryRows()];
        
        for(DatedConversionRate__c dcr: lstDCT)
        {
            if(dcr.NextStartDate__c != null && dcr.StartDate__c != null)
            {
                CurrencyDateData cdd = new CurrencyDateData();
                cdd.isoCode = dcr.IsoCode__c;
                cdd.startDate = dcr.StartDate__c;            
                cdd.nextStartDate = dcr.NextStartDate__c;
                cdd.conversionRate = dcr.ConversionRate__c;
                mapAfterQueryCurrencyDateData.put(dcr.IsoCode__c,cdd);
            }
        }
        
        for(Id opptyId: mapClosedOpportunitiesWithCurrencyAndDate.keyset())
        {
            Map<String,Date> mapCurrencyDate = mapClosedOpportunitiesWithCurrencyAndDate.get(opptyId);
            //for(Map<String,Date> mapCurrencyDate : mapClosedOpportunitiesWithCurrencyAndDate.get(opptyId))
            //{
                for(String strCurrency : mapCurrencyDate.keyset())
                {
                    System.debug('strCurrency::::: ' + strCurrency);
                    for(string iso : mapAfterQueryCurrencyDateData.keyset())
                    { 
                        if(CurrencyISOCodeMappings__c.getInstance(strCurrency).Actual_ISO_Code__c == iso)
                        {
                            CurrencyDateData cdd = new CurrencyDateData();
                            cdd = mapAfterQueryCurrencyDateData.get(iso);
                            System.debug('mapCurrencyDate.get(strCurrency)::::: ' + mapCurrencyDate.get(strCurrency));
                            if(mapCurrencyDate.get(strCurrency) > cdd.startDate && mapCurrencyDate.get(strCurrency) < cdd.nextStartDate)
                            {
                                returnData.put(opptyId,cdd.conversionRate);
                                System.debug('OpportunityId:::: ' + opptyId);
                                System.debug('cdd.conversionRate:::: ' + cdd.conversionRate);
                            }
                        }
                    }
                    
                }
            //}
        }
        
        System.debug('returnData::: ' + returnData);
        return returnData;       
    }
    
    public class CurrencyDateData
    {
        public string isoCode {get;set;}
        public date startDate {get;set;}
        public date nextStartDate {get;set;}
        public decimal conversionRate {get;set;}
        
        public CurrencyDateData()
        {
            
        }
    }
    
}