public  class ConvertReferenceToClientOrTalent {
    
    @AuraEnabled
    public static Contact getReferenceData(Id contactId){
        List<Contact> lstContact = [Select Id, FirstName, LastName, Title, AccountId, HomePhone, MobilePhone, Phone, WorkExtension__c, Email, MailingStreet, MailingCity, MailingState, MailingPostalCode FROM Contact WHERE id =: contactId];
        if( !lstContact.isEmpty() ){
            return lstContact[0];   
        }else{
            return null;
        }
    }
    @AuraEnabled
    public static Map<String,String> getCountryMappings(){
        Map<String,String> countriesMap = new Map<String,String>();
        for(CountryMapping__mdt country:[SELECT CountryCode__c,CountryName__c,Id,Label FROM CountryMapping__mdt]) {
            countriesMap.put(country.Label,country.CountryCode__c);
        }
        return countriesMap;
    }
    @AuraEnabled
    public static Boolean convertReferenceToTalent(Contact contact_obj, Id referenceId){
        
        boolean isSuccess = false;
        List<RecordType> contactRecTypes = [select Id from recordtype where name ='Talent' and SobjectType = 'Contact' LIMIT 1];
        List<RecordType> accountRecTypes = [select Id from recordtype where name ='Talent' and SobjectType = 'Account' LIMIT 1];
        List<Account> lstAccount = [select id, recordtypeId from Account where id =: contact_obj.AccountId ];
        ATS_UserOwnershipModal userOwnershipModal = getCurrentUserWithOwnership();
        if( !contactRecTypes.isEmpty() && !accountRecTypes.isEmpty() ){
            if( contact_obj.id != null && !lstAccount.isEmpty() ){
                contact_obj.recordTypeId    =  contactRecTypes[0].Id;
                contact_obj.Reference_History_Tracking__c ='Reference - Talent';
                lstAccount[0].recordTypeId  =  accountRecTypes[0].Id;
                lstAccount[0].talent_Ownership__c =  userOwnershipModal.userOwnership;
                Savepoint sp = Database.setSavepoint();
                try{
                    update contact_obj;
                    update lstAccount;
                    isSuccess = true;
                }catch(Exception Ex){
                    String message = ex.getMessage();
                    system.debug(' convertReferenceToTalent Error1 : ' + message );
                    Database.rollback(sp);
                }    
            }else{
                // RWS Migrated Record wont have account and contact hence create new Account and Contact
                Talent_Recommendation__c recommendation = [Select Id,Recommendation_From__c from Talent_Recommendation__c where Id =: referenceId];
                Savepoint sp = Database.setSavepoint();
                
                try{
                    Account accountObject = new Account();
                    accountObject.RecordTypeId = accountRecTypes[0].Id;
                    accountObject.Name= contact_obj.FirstName +' '+ contact_obj.LastName;
                    accountObject.Talent_Ownership__c = contact_obj.talent_Ownership__c;
                    insert accountObject;    
                    
                    contact_obj.AccountId = accountObject.Id;
                    contact_obj.RecordTypeId = contactRecTypes[0].Id;
                    contact_obj.Reference_History_Tracking__c ='RWS - Talent';
                    insert contact_obj;
                    
                    recommendation.Recommendation_From__c = contact_obj.Id;
                    update recommendation;
                    isSuccess = true;        
                }catch(Exception Ex){
                    String message = ex.getMessage();
                    system.debug(' convertReferenceToTalent Error2 : ' + message );
                    Database.rollback(sp);
                    
                }        
            }
            
        }  
        
        
        return isSuccess;
    }
    
    @AuraEnabled
    public static Boolean convertReferenceToClient(Contact contact_obj,  Id referenceId, Account account_obj){
        
        boolean isSuccess = false;
        List<RecordType> contactRecTypes = [select Id from recordtype where name ='Client' and SobjectType = 'Contact' LIMIT 1];
        List<RecordType> accountRecTypes = [select Id from recordtype where name ='Client' and SobjectType = 'Account' LIMIT 1];
        
        if( !contactRecTypes.isEmpty() && !accountRecTypes.isEmpty()  ){
            
            if( contact_obj.id != null && account_obj.id != null){
                /* Update */
                // store reference account in variable to delete after convertion
                String referenceAccountId = contact_obj.AccountId;
                contact_obj.recordTypeId    =  contactRecTypes[0].Id;
                contact_obj.AccountId = account_obj.Id; // link existing Refernce Contact to CLient Account.
                contact_obj.Reference_History_Tracking__c ='Reference - Client';
                
                Savepoint sp = Database.setSavepoint();
                try{
                    update contact_obj;
               		
                    // retrieve refernce account where we set delete flag
                    Account refAccount=[select id, name, Error__c from Account where id=:referenceAccountId];
					refAccount.Error__c='Delete'; // setting delete in error text field as thi is orphan reference account that needs to be deleted                    
                    update refAccount;
                    
                    isSuccess = true;
                    
                }catch(Exception Ex){
                    String message = ex.getMessage();
                    system.debug(' convertReferenceToClient Error1 : ' + message );
                    Database.rollback(sp);
                }
            }else if(contact_obj.id == null && account_obj.id != null){ 
                /* Insert :    
                RWS Migrated Record wont have account and contact hence create new Account and Contact */
                Talent_Recommendation__c recommendation = [Select Id,Recommendation_From__c from Talent_Recommendation__c where Id =: referenceId];
                
                Savepoint sp = Database.setSavepoint();
                try{
                    contact_obj.AccountId = account_obj.Id;
                    contact_obj.RecordTypeId = contactRecTypes[0].Id;
                    contact_obj.Reference_History_Tracking__c ='RWS - Client';
                    insert contact_obj;
                    
                    recommendation.Recommendation_From__c = contact_obj.Id;
                    update recommendation;
                    isSuccess = true;
                    
                }catch(Exception Ex){
                    String message = ex.getMessage();
                    system.debug(' convertReferenceToClient Error2 : ' + message );
                    Database.rollback(sp);
                }
            }   
        }
        
        return isSuccess;    
    }
    
    
    // for Rws Migrated Recommendation Record to be converted to Reference Contact Object
    @AuraEnabled 
    public static Boolean convertReferenceToReference(Contact contact_obj , Id referenceId){
        
        boolean isSuccess = false;
        List<Account> lstAccount = [select id, recordtypeId from Account where id =: contact_obj.AccountId ];
        List<RecordType> contactRecTypes = [select Id from recordtype where name ='Reference' and SobjectType = 'Contact' LIMIT 1];
        List<RecordType> accountRecTypes = [select Id from recordtype where name ='Reference' and SobjectType = 'Account' LIMIT 1];
        
        if( lstAccount.isEmpty() ){
            
            // RWS Migrated Record wont have account and contact hence create new Account and Contact
            Talent_Recommendation__c recommendation = [Select Id,Recommendation_From__c from Talent_Recommendation__c where Id =: referenceId];
            
            Savepoint sp = Database.setSavepoint();
            try{
                Account accountObject = new Account();
                accountObject.Name= contact_obj.FirstName +' '+ contact_obj.LastName;
                accountObject.RecordTypeId = accountRecTypes[0].Id;
                insert accountObject;    
                
                contact_obj.AccountId = accountObject.Id;
                contact_obj.RecordTypeId = contactRecTypes[0].Id;
                contact_obj.Reference_History_Tracking__c ='RWS - Reference';
                insert contact_obj;
                
                recommendation.Recommendation_From__c = contact_obj.Id;
                update recommendation;
                isSuccess = true;
            }catch(Exception Ex){
                String message = ex.getMessage();
                system.debug(' convertReferenceToReference Error1 : ' + message );
                Database.rollback(sp);
            }
        }
        
        return isSuccess;    
    } 
    @AuraEnabled
    public static Contact getReferenceContactDetails(Id referenceId){
        // for RWS migrated record logic
        
        Contact contactObj = new Contact();
        Account accountObj = new Account();
        Talent_Recommendation__c recommendation = [Select Id,Relationship__c,Relationship_Duration__c, Client_Account__r.Id, Client_Account__c, End_Date__c, Start_Date__c, 
                                                   Interaction_Frequency__c,Talent_Current_Job__c,Talent_Job_Duration__c,Talent_Job_Title__c, Talent_Job_Duties__c, Opportunity__c, Client_Account__r.Name,
                                                   Rehire_Status__c,Quality__c,Workload__c,Quality_Description__c, Workload_Description__c,Competence__c,Recommendation_From__c, Opportunity__r.Id,
                                                   Competence_Description__c,Leadership__c,Leadership_Description__c,Collaboration__c,Collaboration_Description__c, Opportunity__r.Name,
                                                   Trustworthy__c,Trustworthy_Description__c,Likeable__c,Likeable_Description__c,Outlook__c, Outlook_Description__c, Reference_Check_Completed__c, Organization_Name__c,
                                                   Team_Player__c,Team_Player_Description__c,Professionalism__c, Professionalism_Description__c,Recommendation__c, 
                                                   Growth_Opportunities__c,Comments__c, Completion_Date__c, Project_Description__c, Non_technical_skills__c, Cultural_Environment__c, Additional_Reference_Notes__c, Reference_Type__c,
                                                   First_Name__c,Last_Name__c,Source_System_Id__c,Email__c,Home_Phone__c,Mobile_Phone__c,Other_Phone__c,Work_Phone__c,Reference_Job_Title__c       
                                                   from Talent_Recommendation__c where Id =: referenceId
                                                  ];
        
        
        
        if(recommendation.Recommendation_From__c != null){
            contactObj = [ Select id, AccountId, FirstName, MiddleName, LastName,Email, Title, Salutation, WorkExtension__c,
                          HomePhone,Phone,MobilePhone, Suffix, recordtype.Name 
                          from Contact where id =: recommendation.Recommendation_From__c];        
            accountObj = [Select Id, Name from Account where Id =: contactObj.AccountId];
            
            
        }else{
            contactObj.firstname = recommendation.First_Name__c;
            contactObj.Lastname = recommendation.Last_Name__c;
            contactObj.Email = recommendation.Email__c;
            contactObj.MobilePhone = recommendation.Mobile_Phone__c;
            contactObj.phone = recommendation.Work_Phone__c;
            contactObj.HomePhone = recommendation.Home_Phone__c;
            contactObj.Title = recommendation.Reference_Job_Title__c;
        }
        return contactObj;    
    }
    
    @AuraEnabled
    public static Account getClientAccountDetails (Id referenceId){
        Talent_Recommendation__c recommendationObj = [select id,name, Client_Account__r.Id, Client_Account__c from Talent_Recommendation__c where id=: referenceId ];
         Account accObject = new Account();
        if(recommendationObj.Client_Account__c != null){
           accObject =[select Id ,Name, shippingStreet,shippingCity,ShippingCountry,BillingCity,BillingCountry from Account where id=: recommendationObj.Client_Account__c];
        }
  
        return accObject;
    }
    
    @AuraEnabled
    public static ATS_UserOwnershipModal getCurrentUserWithOwnership() {
        ATS_UserOwnershipModal uoModal = new ATS_UserOwnershipModal();
        User user = [select id, Name, CompanyName, OPCO__c, Profile.Name, Profile.PermissionsModifyAllData from User where id = :Userinfo.getUserId()];
        uoModal.usr = user;
        
        if(!String.isBlank(user.OPCO__c) ) {
            Talent_Role_to_Ownership_Mapping__mdt[] Ownership = [SELECT Opco_Code__c,Talent_Ownership__c,LinkedIn_Sync__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: user.OPCO__c];      
            if(Ownership!=null && !Ownership.isEmpty()) {
                uoModal.userOwnership = Ownership[0].Talent_Ownership__c;
                uoModal.haslinkedInSync = Ownership[0].LinkedIn_Sync__c;            
            }
        }
        return uoModal;
    }

    @AuraEnabled
    public static String callDedupeService(String fullName, List<String>email, List<String>phones, String recordType){
        
        String returnCount          = 'MANY';
        List<String> recordTypes    = new List<String>{recordType};
        boolean IsAtsTalent         = true;
        boolean ignoreIs_ATS_Talent = true;
        
        
        try{
            List<List<SObject>> duplicateTalents = TalentDedupeSearch_v2.retriveDuplicateTalents( fullName, email, phones, '', '', recordTypes , returnCount, IsAtsTalent, ignoreIs_ATS_Talent );
            //return JSON.serialize( duplicateTalents;
            if( duplicateTalents.isEmpty() || duplicateTalents[0].isEmpty() ){
                return null;
            }else{
                return JSON.serialize( duplicateTalents[0] );
            }    
        }catch(Exception ex){
            throw new AuraHandledException('There was an error, please contact Administrator: ' + ex.getMessage());
        }
        
    }
    
    @AuraEnabled
    public static void useSelectedContact(String selectedContactId, Contact existingContact_obj, Id referenceId ){
        
        List<Talent_Recommendation__c> talentRecommendationList = new List<Talent_Recommendation__c>();
        Savepoint sp = Database.setSavepoint();
        try{
            if( existingContact_obj.Id != null ){
                List<Talent_Recommendation__c> recommendationList =[select id, name, Recommendation_from__c from Talent_Recommendation__c
                                                                    where Recommendation_from__c =: existingContact_obj.id];
                
                if(recommendationList.size() > 0){
                    for(Talent_Recommendation__c rec : recommendationList){
                        rec.Recommendation_from__c = selectedContactId;
                        talentRecommendationList.add(rec);
                    }
                    update talentRecommendationList;
                    Account refAccount=[select id, name, Error__c from Account where id=:existingContact_obj.AccountId];
                    refAccount.Error__c='Delete'; // setting delete in error text field as thi is orphan reference account that needs to be deleted                    
                    update refAccount;
                }
                
                
            }else{ // for rws migrated record where contact will be null
                talentRecommendationList = [select id, name, Recommendation_from__c from Talent_Recommendation__c
                                            where id =: referenceId];
                
                if(talentRecommendationList.size() > 0){
                    talentRecommendationList[0].Recommendation_from__c = selectedContactId;
                    update talentRecommendationList;
                }
            }    
            
        }catch(Exception ex){
            database.rollback(sp);
            throw new AuraHandledException('There was an error, please contact Administrator: ' + ex.getMessage());
        }
            
    } 

}