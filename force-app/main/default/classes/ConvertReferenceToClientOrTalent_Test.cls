@isTest 
private class ConvertReferenceToClientOrTalent_Test {

    @isTest
    private static void ConvertRefTest1() {
        Account newAcc = BaseController_Test.createTalentAccount('');
        newAcc.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        update newAcc;
        Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
        Contact newCon2 = BaseController_Test.createClientContact(newAcc.Id);       
        
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Contact'];
        Contact con = new contact();
        con.lastname = 'testlast';
        con.email = 'test9@email.com';
        con.phone='5000005';
        con.AccountId=newAcc.Id;
        con.RecordTypeId = types[0].Id;

        List<Talent_Recommendation__c> ListRef = new List<Talent_Recommendation__c>();
        Talent_Recommendation__c ref= new Talent_Recommendation__c(
        Client_Account__c = newAcc.Id,
        Recommendation_From__c = newCon.Id,
        Talent_Contact__c = newCon.Id,
        RecordTypeId = Schema.SObjectType.Talent_Recommendation__c.getRecordTypeInfosByName().get('Reference').getRecordTypeId()
        );
        ListRef.add(ref);

        Talent_Recommendation__c refWithoutForm= new Talent_Recommendation__c(
        Client_Account__c = newAcc.Id,
        //Recommendation_From__c = newCon.Id,
        Talent_Contact__c = newCon.Id,
        RecordTypeId = Schema.SObjectType.Talent_Recommendation__c.getRecordTypeInfosByName().get('Reference').getRecordTypeId()
        );
        ListRef.add(refWithoutForm);

        insert ListRef;
        

        Test.startTest();
        Contact TestCon = ConvertReferenceToClientOrTalent.getReferenceData(newCon.Id);
        Map<String,String> countriesMap = ConvertReferenceToClientOrTalent.getCountryMappings();
        Boolean flag1 = ConvertReferenceToClientOrTalent.convertReferenceToTalent(newCon,ref.Id);
        Boolean flag2 = ConvertReferenceToClientOrTalent.convertReferenceToTalent(con,ref.Id);
        Boolean flag3 = ConvertReferenceToClientOrTalent.convertReferenceToClient(newCon,ref.Id,newAcc);        
        Boolean flag4 = ConvertReferenceToClientOrTalent.convertReferenceToReference(newCon, ref.Id);        
        Contact con2 = ConvertReferenceToClientOrTalent.getReferenceContactDetails(ref.Id);
        Contact con3 = ConvertReferenceToClientOrTalent.getReferenceContactDetails(refWithoutForm.Id);
        Account Acnt2 = ConvertReferenceToClientOrTalent.getClientAccountDetails (ref.Id);

        List<String> email = new List<String>();
        email.add('test1@email.com');
        List<String> phones = new List<String>();
        phones.add('001122');        
        String str1 = ConvertReferenceToClientOrTalent.callDedupeService('testdata',email,phones,'Client');
        Test.stopTest();
    }
    @isTest
    private static void ConvertRefTest2() {
        Account newAcc = BaseController_Test.createTalentAccount('');
        newAcc.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        update newAcc;
        Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
        Contact newCon2 = BaseController_Test.createClientContact(newAcc.Id);       
        Contact con = new contact();
        con.lastname = 'testlast';
        con.email = 'test9@email.com';
        con.phone='5000005';
        
        List<Talent_Recommendation__c> ListRef = new List<Talent_Recommendation__c>();
        Talent_Recommendation__c ref= new Talent_Recommendation__c(
        Client_Account__c = newAcc.Id,
        Recommendation_From__c = newCon.Id,
        Talent_Contact__c = newCon.Id,
        RecordTypeId = Schema.SObjectType.Talent_Recommendation__c.getRecordTypeInfosByName().get('Reference').getRecordTypeId()
        );
        ListRef.add(ref);

        Talent_Recommendation__c refWithoutForm= new Talent_Recommendation__c(
        Client_Account__c = newAcc.Id,
        Talent_Contact__c = newCon.Id,
        RecordTypeId = Schema.SObjectType.Talent_Recommendation__c.getRecordTypeInfosByName().get('Reference').getRecordTypeId()
        );
        ListRef.add(refWithoutForm);
        insert ListRef;
        
        Test.startTest();       
        ConvertReferenceToClientOrTalent.useSelectedContact((String)newCon.id, con, ref.Id);
        Boolean flag1 = ConvertReferenceToClientOrTalent.convertReferenceToReference(con, ref.Id);        
        ConvertReferenceToClientOrTalent.useSelectedContact((String)newCon2.id, newCon, ref.Id);
        Test.stopTest();        
    }
    
    @isTest
    private static void ConvertRefTest3() {
        Account newAcc = BaseController_Test.createTalentAccount('');
        newAcc.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        update newAcc;
        Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
        Contact newCon2 = BaseController_Test.createClientContact(newAcc.Id);       
        Contact con = new contact();
        con.lastname = 'testlast';
        con.email = 'test9@email.com';
        con.phone='5000005';
        
        List<Talent_Recommendation__c> ListRef = new List<Talent_Recommendation__c>();
        Talent_Recommendation__c ref= new Talent_Recommendation__c(
        Client_Account__c = newAcc.Id,
        Recommendation_From__c = newCon.Id,
        Talent_Contact__c = newCon.Id,
        RecordTypeId = Schema.SObjectType.Talent_Recommendation__c.getRecordTypeInfosByName().get('Reference').getRecordTypeId()
        );
        ListRef.add(ref);

        Talent_Recommendation__c refWithoutForm= new Talent_Recommendation__c(
        Client_Account__c = newAcc.Id,
        Talent_Contact__c = newCon.Id,
        RecordTypeId = Schema.SObjectType.Talent_Recommendation__c.getRecordTypeInfosByName().get('Reference').getRecordTypeId()
        );
        ListRef.add(refWithoutForm);
        insert ListRef;
        
        Test.startTest();   
        ConvertReferenceToClientOrTalent.useSelectedContact((String)newCon2.id, newCon, ref.Id);
        Boolean flag2 = ConvertReferenceToClientOrTalent.convertReferenceToClient(con,ref.Id,newAcc);
        Test.stopTest();        
    }
    
}