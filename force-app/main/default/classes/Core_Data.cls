/******************************************************************************************************************************
* Name        - Core_Data
* Description - Utility class that has method to retrieve Local Date Format. 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Dhruv                       04/10/2012             Created
********************************************************************************************************************************/

public with sharing class Core_Data {
    public static Map<String, String> mapRecordTypeIdByName = new Map<String,String>();
    public static Map<String, Schema.SObjectType> objects = new Map<String, Schema.SObjectType>();
    public static final string sourceSystem = 'ODS';
    public static final string integrationSystem = 'TIBCO';
    public static String remainingString;
    public static Boolean triggerStatus = false;
    
    public static Integer j = 0;
    
    public static Boolean booleanSetter()
    {
        triggerStatus = true;
        return triggerStatus; 
    }
    
    
    
    
    //============================Method Definition=============================================================
    //Method Name:          retrieveLocaleDateFormat
    //Description:          This method is used to fetch the local Date Format based on the logged in user.
    //Input Parameters:     None 
    //Output Parameters:    String(localeFormat) 
    //=========================================================================================================== 
    public static string retrieveLocaleDateFormat()
    {
       String localeFormat;
       Map<String,Locale_Setting__c> localeMap = Locale_Setting__c.getAll();   //fetch the values from the custom Setting.
       if(localeMap != Null && localeMap.get(userInfo.getLocale()) != Null)    //check if there are any values.Based on current user fetch the value.
         {
           localeFormat = localeMap.get(userInfo.getLocale()).Date_Format__c;   //assign the appropriate date format to a variable.
           system.debug('====>localeFormat'+localeFormat);
         } 
       else
         {  
           localeFormat = Label.Date_Format_for_Submittals;                    
           system.debug('====>localeFormat_Label'+localeFormat);
         }  
       return localeFormat;                        //return the format.
    }
    
    //============================Method Definition=============================================================
    //Method Name:          multipleLogRecord
    //Description:          This method is used display the information in Log record properly.
    //Input Parameters:     String loadName, String tempString, Integer totalRecords, Integer numOfUpserts
    //Output Parameters:    None
    //=========================================================================================================== 
    
     public static void multipleLogRecord(String loadName, String tempString, Integer totalRecords, Integer numOfUpserts)
    {
        if(tempString!=null)
        {
            if(tempString.length()> 30000)
            {
                Integer descLength = 0;
                String descString = '';
                List<String> descriptionStrings = splitDescription(tempString);
                
                system.debug('====>'+descriptionStrings);
                
                
                for(Integer i = 0; i< descriptionStrings.size() ; i++)
                {   
                    descriptionStrings[i]= descriptionStrings[i]+';';
                    descString = descString+descriptionStrings[i];
                    
                    if(descString.length()>32000)
                    {
                        
                   // descLength = descLength + descriptionStrings[i].length();
                        descString.substring(0,descString.lastIndexOf(';'));
                        System.debug('===>descString'+descString);
                        descString.substring(0,descString.lastIndexOf(';'));
                        System.debug('===>descString'+descString);
                        Core_Data.logRecord(totalRecords,numOfUpserts,'upsertUserRole',descString);
                        descString = null;
                        i--;
                    }    
                    
                    if(i == descriptionStrings.size()-1)
                    {
                     Core_Data.logRecord(totalRecords,numOfUpserts,loadName,descString);  
                     System.debug('I am here first time');
                     
                     }   
                }
            }
                        else     
                            Core_Data.logRecord(totalRecords,numOfUpserts,loadName,tempString);
        }
    }
    
    public static List<String> splitDescription(String description)
    {   
        List<String> descriptionStrings = new List<String>();
        if(description.contains('<br>'))
        {
            descriptionStrings = description.split('<br>');   
            
        }
        else if(description.contains(';'))
        {
            descriptionStrings = description.split(';');   
        }   
          return descriptionStrings;
    }   
    
    
    
    
    @future 
    public static void logRecord(Integer totalRecords, Integer numOfUpserts,String Subject,String description)
    {
         System.debug('I am here in future');
        log__c usrRoleLoadLog = new log__c();
        usrRoleLoadLog.Subject__c = Subject;
        usrRoleLoadLog.Log_Date__c = system.now();
        usrRoleLoadLog.Source_System__c = sourceSystem;
        description = 'Returned'+totalRecords+'records from parameters.\n'+'Upserting'+numOfUpserts+'records:\n'+description;
        usrRoleLoadLog.Integration_System__c = integrationSystem;
        usrRoleLoadLog.Description__c = description;
        insert usrRoleLoadLog;
    }
    
    
    public static void logBatchRecord(String Subject, Map<Id,String> errorMap)
    {
        System.debug('Req Sync Batch Errors');
        List<log__c> usrRoleLoadLogLst = new List<log__c>();
        
        if(errorMap.size() > 0){
          for(Id errId : errorMap.keyset()){
            log__c usrRoleLoadLog = new log__c();
            usrRoleLoadLog.Subject__c =  Subject;
            usrRoleLoadLog.Log_Date__c = system.now();
            usrRoleLoadLog.Description__c = errorMap.ContainsKey(errId) ? errorMap.get(errId) : 'Blank Error Message';
            usrRoleLoadLog.Related_Id__c = errId != null ? String.valueOf(errId): '';
            usrRoleLoadLogLst.add(usrRoleLoadLog);
          }
        }
        
        insert usrRoleLoadLogLst;
    }
    
    public static void logInsertBatchRecords(String Subject, List<String> errorList)
    {
        System.debug('Req Team Members Batch Errors');
        List<log__c> usrRoleLoadLogLst = new List<log__c>();
        
        if(errorList.size() > 0){
          for(String str : errorList){
            log__c usrRoleLoadLog = new log__c();
            usrRoleLoadLog.Subject__c = Subject;
            usrRoleLoadLog.Log_Date__c = system.now();
            usrRoleLoadLog.Description__c = !String.IsBlank(str) ? str : 'Error' ;
            usrRoleLoadLogLst.add(usrRoleLoadLog);
          }
        }
        
        insert usrRoleLoadLogLst;
    }
    
    
}