/******************************************************************************************************************************
* Name        - Core_Log
* Description - Helper class to create error log information
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pavan                       03/16/2012             Created
* Pratz                       01/14/2013             Edited CustomException method
********************************************************************************************************************************/

Public class Core_Log{
    /**********************************************************
Method to return the error log object
***********************************************************/
    public static Log__c logException(CalloutException ex)
    {
        Log__c excep = new Log__c();
        excep.Description__c = ex.getMessage();
        excep.Subject__c = Label.CalloutException;
        return excep; 
    }
    public static Log__c logException(Exception ex)
    {
        Log__c excep = new  Log__c();
        excep.Description__c = ex.getMessage();
        excep.Subject__c = Label.Exception;
        return excep; 
    }
    public static Log__c logException(database.Error ex)
    {
        Log__c excep = new  Log__c();
        excep.Description__c = ex.getMessage();
        excep.Subject__c = Label.Exception;
        return excep; 
    }
    public static Log__c CustomException(String Message)
    {
        DateTime TodayDateTime;
        Log__c excep = new  Log__c();
        excep.Description__c = Message;
        excep.Subject__c = 'Account Merge Exception';
        TodayDateTime = datetime.parse(system.now().format());         
        excep.Log_Date__c = TodayDateTime;
        excep.Integration_System__c = 'TIBCO';
        excep.Related_Object__c = 'Account';
        excep.Source_System__c = 'Siebel';
        return excep; 
    }
    
    public static Log__c CustomException(String message, String logLevel, String subject, String srcSystem)
    {
        DateTime TodayDateTime;
        Log__c excep = new  Log__c();
        excep.Description__c = message;
        excep.Debug_Level__c = logLevel;
        excep.Subject__c = subject;
        TodayDateTime = datetime.parse(system.now().format());         
        excep.Log_Date__c = TodayDateTime;
        excep.Integration_System__c = 'TIBCO';
        excep.Source_System__c = srcSystem;
        return excep; 
    }
    
    public static Log__c CustomException(String message, String logLevel, String subject, String srcSystem, String clazz, String method)
    {
        DateTime TodayDateTime;
        Log__c excep = new  Log__c();
        excep.Description__c = message;
        excep.Debug_Level__c = logLevel;
        excep.Subject__c = subject;
        excep.Class__c = clazz;
        excep.Method__c = method;
        TodayDateTime = datetime.parse(system.now().format());         
        excep.Log_Date__c = TodayDateTime;
        excep.Integration_System__c = 'TIBCO';
        excep.Source_System__c = srcSystem;
        return excep; 
    }
    
    public static Log__c CustomException(String message, String logLevel, String subject, String srcSystem, String clazz, String method, String transactionId)
    {
        DateTime TodayDateTime;
        Log__c excep = new  Log__c();
        excep.Description__c = message;
        excep.Debug_Level__c = logLevel;
        excep.Subject__c = subject;
        excep.Class__c = clazz;
        excep.Method__c = method;
        TodayDateTime = datetime.parse(system.now().format());         
        excep.Log_Date__c = TodayDateTime;
        excep.Integration_System__c = 'TIBCO';
        excep.Source_System__c = srcSystem;
        excep.TransactionId__c = transactionId;
        return excep; 
    }
    
    public static Log__c logException(String strClass, String description, String method, String relatedId, String relatedObject, String subject)
    {
        DateTime TodayDateTime;
        TodayDateTime = datetime.parse(system.now().format()); 
        Log__c excep = new  Log__c();
        excep.Class__c = strClass;
        excep.Log_Date__c = TodayDateTime;
        excep.Description__c = description;
        excep.Method__c = method;                
        excep.Related_Id__c = relatedId;
        excep.Related_Object__c = relatedObject;
        excep.Subject__c = subject;
        return excep; 
    }
    
}