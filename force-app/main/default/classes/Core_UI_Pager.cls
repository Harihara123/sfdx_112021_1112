public with sharing class Core_UI_Pager {
   // public static final Integer DEFAULT_PAGE_SIZE = 100; // Commented to make page size dynamic
    public class Pager {
        public Integer DEFAULT_PAGE_SIZE; // Added to make default page size dynamic
        private Integer totalPageNumber = 0;
       // private Integer pageSize = DEFAULT_PAGE_SIZE; // Commented to make page size dynamic
        private Integer currentPage = 1;
        
       
        //============================Method Definition=============================================================
        //Method Name:          Pager
        //Method Type :         Constructor 
        //Description:          This method is used to calculate the total pages required to display the information.
        //Input Parameters:     Integer(recordCount),Integer(pageSize)
        //Output Parameters:    None
        //=========================================================================================================== 
        public Pager(Integer recordCount, Integer pageSize) 
        {

            if (totalPageNumber == 0 && recordCount > 0) 
            {
                  totalPageNumber = recordCount / pageSize;
                  Integer mod = recordCount - (totalPageNumber * pageSize);
                  if (mod > 0)
                    totalPageNumber++;
            }
           DEFAULT_PAGE_SIZE =  pageSize; // Added to make page size dynamic
        }
        //==========================================================================================================
                                            /*Pagination Functions*/
        //==========================================================================================================
        
        //returns the total number of pages required to display info.
        public Integer getTotalPageNumber() {
            return totalPageNumber;
        }
        //Returns the index for current page 
        public Integer getCurrentPage() {
            return currentPage;
        }
        //increments the page index by one page.
        public void moveNextPage() {
            currentPage++;
        }
        //decrements the page index by one page.
        public void movePrevPage() {
            currentPage--;
        }
        // changes the current page index to supplied value.
        public void moveToPage(Integer pageNumber) {
            currentPage = pageNumber;
        }
        //Enable or disable the Previous Button.
        public Boolean getPreviousButtonStatus() {
           if(currentPage > 1)
            return true;
           else 
            return false; 
        }
        //Enable or disable the Next Button.
        public Boolean getNextButtonStatus(Integer listSize) {
            boolean enableButton = false;
            if(listSize > 0)
                enableButton = ((getCurrentPage() * DEFAULT_PAGE_SIZE) < listSize); 
            return enableButton;
        }
        //============================Method Definition=============================================================
        //Method Name:          getMinMaxValues           
        //Description:          This method is used to calculate min and max values that are required to display info 
        //                      on page in a corrdct format.
        //Input Parameters:     Integer(PageIndex ),Integer(listSize)
        //Output Parameters:    Map<String,Integer>
        //=========================================================================================================== 
        
        public Map<String,Integer> getMinMaxValues(Integer PageIndex , Integer listSize) {
            Map<String,Integer> minMaxMap = new Map<String,Integer>(); 
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;

            if (PageIndex == getTotalPageNumber()) {
              max = listSize;
              min = (PageIndex -1) * DEFAULT_PAGE_SIZE;
              minMaxMap.put('Max',max);
              minMaxMap.put('Min',min); 
            }
            else if (PageIndex > getCurrentPage()) {
              min = getCurrentPage() * DEFAULT_PAGE_SIZE;
              max = PageIndex * DEFAULT_PAGE_SIZE;
              minMaxMap.put('Max',max);
              minMaxMap.put('Min',min);
            }
            else {
              max = PageIndex * DEFAULT_PAGE_SIZE;
              min = max - DEFAULT_PAGE_SIZE;
              minMaxMap.put('Max',max);
              minMaxMap.put('Min',min);
            }
            return minMaxMap;
        }
        //============================Method Definition=============================================================
        //Method Name:          getOriginalkey
        //Description:          This method is used to convert the data type of date fields back from Date to String.
        //                      so that they point to the original key of the map. 
        //Input Parameters:     List<Date> (sortedKeysDate)
        //Output Parameters:    List<String> (sortedKeys)
        //=========================================================================================================== 
        public List<String> getOriginalkey(List<Date> sortedKeysDate){
            List<String> sortedKeys = new List<String>();
            for(Date keyDate : sortedKeysDate)
            {   //Iterate over the populated list
               
                    String keyDateString =  String.valueof(KeyDate);
                    System.debug('&&&'+keyDateString);                    
                    Datetime DtSubmitted = Datetime.valueOf((keyDateString).substring(0,10) +' '+ Label.TimeStamp_for_Submittals);
                    String originalKey = DtSubmitted.format(Core_Data.retrieveLocaleDateFormat());
                    /* String yyyy = keyDateString.Substring(0,4);
                    String mm = keyDateString.Substring(5,7);
                    String dd = keyDateString.Substring(8,10);
                    String originalKey = mm+'/'+dd+'/'+yyyy; */
                    sortedKeys.add(originalKey);                                                    //Add the keys to a list 
                    system.debug('====>sortedKeys'+sortedKeys);                                    
            }
            return(sortedKeys);
        }
    }
    
    public interface PagerInterface {

        void firstBtnClick();
        void previousBtnClick();
        void nextBtnClick();
        void lastBtnClick();
        Boolean getPreviousButtonEnabled();
        Boolean getNextButtonEnabled();
    }
    
}