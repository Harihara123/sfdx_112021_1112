public class CreateDocumentController  {

    @AuraEnabled
    public static String saveCopiedPasteContent(String talentId
                                                , String fileName
                                                , String encodedURIText
                                                , String replaceResumeId
												, String source) {

        Map<String, Object> parameters = new Map<String, Object>{ 'recordId' => talentId
                                                                    , 'fileName' => fileName + '.pdf'
                                                                    , 'documentType' => 'Resume'
                                                                    , 'replaceResume' => ''
                                                                    , 'defaultFlag' => true };

        Id talentDocumentId = (Id)ATSTalentDocumentFunctions.performServerCall('createTalentDocument', parameters);
       
        if(!source.equalsIgnoreCase('Find_Add_Talent')) { //For 'Find/Add Talent fucntionality no need to commit the Document true.
            Talent_Document__c talentDocument = [SELECT Id, Committed_Document__c FROM Talent_Document__c WHERE Id =: talentDocumentId];
            talentDocument.Committed_Document__c = true;
            update talentDocument;
            deleteReplaceResume(replaceResumeId);
        }
		Attachment resume = createAttachment(encodedURIText, fileName, talentDocumentId);
        insert resume;
        return talentDocumentId;
    }

    /*
      @method : parseContentsAndCreateTalent
      @Date : 02/07/2019
      @Description : Method will be called by 'create document' fucntionality from 'Find/Add Talent'.
                     This method take talentdocument id as parameter and send the contents for parsing and return the contacts info for creating talents after parsing the contents.
                        
    */
    @AuraEnabled
    public static String parseContentsAndCreateTalent(String talentDocumentId) { 
        Map<String, Object> parameters = new Map<String, Object>{ 'talentDocumentId' => talentDocumentId};
        return (String)ATSTalentDocumentFunctions.performServerCall('parseResumeAndCreateTalent', parameters);
    }

    @AuraEnabled
    public static List<Talent_Document__c> getResumeListToReplace(String talentAccountId) {
        return (List<Talent_Document__c>)ATSTalentDocumentFunctions.performServerCall('getResumeListToReplace', new Map<String, Object>{ 'recordId' => talentAccountId });
    }

    private static Attachment createAttachment(String encodedURIText, String fileName, Id talentDocumentId) {
        Attachment attach = new Attachment();
        String convertedBlob = EncodingUtil.urlDecode(encodedURIText, 'UTF-8');
        attach.Body = Blob.toPdf(convertedBlob);
        attach.Name = trySanitizeName(fileName) + '.pdf';
        attach.ContentType = 'application/pdf';
        attach.ParentId = talentDocumentId;
        return attach;
    }

    private static String trySanitizeName(String fileName) {
        try {
            fileName  = fileName.replaceAll('[^a-zA-Z0-9\\s-_.]', '');
        }
        catch(Exception e){
            system.debug('Fail to remvoe Special Chars from Resume name: ' + e.getMessage());
        }
        return fileName;     
    }

    private static void deleteReplaceResume(String replaceResumeId) {
        if (!String.isEmpty(replaceResumeId)) {
            String[] parts = replaceResumeId.split('-');
            String oldTalentDocumentId = parts[0];
            String attachmentId = parts[1];
            Attachment attachment = new Attachment(Id = attachmentId); 
            delete attachment;
            
            Talent_Document__c oldTalentDocument = [SELECT Id FROM Talent_Document__c where Id =:oldTalentDocumentId];
            oldTalentDocument.Mark_For_Deletion__c = true;
            update (oldTalentDocument);
        }
    }
   
    @AuraEnabled
    public static String callSearchDedupeService(String talentDocID, String Source, String contactDetails, String transactionId){
       // talentDocID  will be talent document id
       // Source from which this process initiated eg: Find_Add_Talent
       // ContactDetails will be ParsedResume json contact Json string, this will only have value for Find_Add_Talent flow.
        system.debug('callSearchDedupeService--talentDocID'+talentDocID);
        system.debug('callSearchDedupeService--Source'+Source);
        system.debug('callSearchDedupeService--contactDetails'+contactDetails);
		system.debug('callSearchDedupeService--transactionId'+transactionId);
        
        User CUser = [Select Id,OPCO__c from User where Id =:UserInfo.getUserId()];
     	List<Data_Shielding_Search_Query_Filter__mdt> searchFilter = new List<Data_Shielding_Search_Query_Filter__mdt>([SELECT OPCO_Name__c,Filter_String__c FROM Data_Shielding_Search_Query_Filter__mdt Where OPCO_Name__c =: CUser.OPCO__c]);
        
        String AccountID ='';
        String responseBody ='';  
        String jsonBody='';
        String UnparssedBase64String='';
        String ServiceUrl = '';
        String firstName='';
        String lastName ='';
        String sourceSystemId='';
        String city='';
        String country='';
        String state ='';

        List<String> phoneList = new List<String>();
        List<String> emailList = new List<String>();
       
        String jsonEmailString ='[]';
        String jsonPhoneString='[]';
        
        List<Talent_Document__c> td = [SELECT Id, Name, Talent__c FROM Talent_Document__c WHERE id=:talentDocID];
        
        Attachment[] atts = [SELECT Id, Name, Body from Attachment WHERE ParentId = :talentDocID];
        UnparssedBase64String = EncodingUtil.base64Encode(atts[0].Body);
        
        ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues('Data Quality Service');
        ServiceUrl = serviceSettings.Service_URL__c;
        
        if(Source=='Find_Add_Talent' && contactDetails != '' && contactDetails != null && contactDetails !='"null"'){
                String parsedHRXML ='';
            	Map<String, Object> objEntry = (Map<String, Object>) JSON.deserializeUntyped(contactDetails);
                if(objEntry.containsKey('firstname')){
                    firstName = String.valueOf(objEntry.get('firstname'));
                }
                if(objEntry.containsKey('lastname')){
                    lastName = String.valueOf(objEntry.get('lastname')); 
                }
                if(objEntry.containsKey('parsedHRXML')){
                    parsedHRXML = String.valueOf(objEntry.get('parsedHRXML')); 
                }
               if(objEntry.containsKey('email')){
                    if(objEntry.get('email') != null && objEntry.get('email') != ''){
                        emailList.add(String.valueOf(objEntry.get('email')));
                    }
            	}
            	if(objEntry.containsKey('otheremail')){
                    if(objEntry.get('otheremail') != null && objEntry.get('otheremail') != ''){
                        emailList.add(String.valueOf(objEntry.get('otheremail')));
                    }
            	}
            	if(objEntry.containsKey('workemail')){
                    if(objEntry.get('workemail') != null && objEntry.get('workemail') != ''){
                        emailList.add(String.valueOf(objEntry.get('workemail')));
                    }
            	}
            	if(objEntry.containsKey('mobilephone')){
                    if(objEntry.get('mobilephone') != null && objEntry.get('mobilephone') != ''){
                        phoneList.add(String.valueOf(objEntry.get('mobilephone')));
                    }
            	}
            	if(objEntry.containsKey('workphone')){
                    if(objEntry.get('workphone') != null && objEntry.get('workphone') != ''){
                        phoneList.add(String.valueOf(objEntry.get('workphone')));
                    }
            	}
            	if(objEntry.containsKey('homephone')){
                    if(objEntry.get('homephone') != null && objEntry.get('homephone') != ''){
                        phoneList.add(String.valueOf(objEntry.get('homephone')));
                    }
            	}
            
            jsonEmailString = json.serialize(emailList);
            jsonPhoneString = json.serialize(phoneList);
                
            jsonBody ='{'+
            '"sourceSystemId":"'+sourceSystemId+'",'+
            '"firstName":  "'+firstName+'",'+
            '"lastName":  "'+lastName+'",'+ 
            '"email":  '+jsonEmailString+','+
            '"phone":  '+jsonPhoneString+','+
            '"city":  "'+city+'",'+                     
            '"state":  "'+state+'",'+                       
            '"country":  "'+country+'",'+                       
			'"unparsedResume" :  "'+UnparssedBase64String+'"'+
            '}';        
            
        }else if(Source !='Find_Add_Talent'){
            List<Contact> conObject =[select Id, firstName, AccountID,lastName,
                                                Phone,MobilePhone,HomePhone,OtherPhone,
                                                Email, Work_Email__c, Other_Email__c, Account.Source_System_Id__c,
                                                MailingState,MailingCity,Talent_Country_Text__c from Contact where AccountID=: td[0].Talent__c ];
            
            
            
            if(conObject.size()>0){
                AccountID = conObject[0].AccountID;
                firstName = conObject[0].firstName;
                lastName = conObject[0].lastName; 
                sourceSystemId = conObject[0].Account.Source_System_Id__c;
                city =conObject[0].MailingCity;
                state =conObject[0].MailingState;
                country =conObject[0].Talent_Country_Text__c;
                if(conObject[0].HomePhone != null ){
                    phoneList.add(conObject[0].HomePhone);
                }
                if(conObject[0].MobilePhone != null){
                    phoneList.add(conObject[0].MobilePhone);
                }
                if(conObject[0].OtherPhone != null){
                    phoneList.add(conObject[0].OtherPhone);
                }
                if(conObject[0].Email != null ){
                    emailList.add(conObject[0].Email);
                }
                if(conObject[0].Other_Email__c != null){
                    emailList.add(conObject[0].Other_Email__c);
                }
                if(conObject[0].Work_Email__c != null){
                    emailList.add(conObject[0].Work_Email__c);
                }
                jsonEmailString = json.serialize(emailList);
                jsonPhoneString = json.serialize(phoneList);
            }
            // Adding candidateId new parameter to request when service being called from Talent Landing Page
            jsonBody ='{'+
            '"candidateId":  "'+AccountID+'",'+
            '"sourceSystemId":"'+sourceSystemId+'",'+
            '"firstName":  "'+firstName+'",'+
            '"lastName":  "'+lastName+'",'+
            '"email":  '+jsonEmailString+','+
            '"phone":  '+jsonPhoneString+','+
            '"city":  "'+city+'",'+                     
            '"state":  "'+state+'",'+                       
            '"country":  "'+country+'",'+                       
            '"unparsedResume" :  "'+UnparssedBase64String+'"'+
            '}';
        }
        
        
       // system.debug('Email List:'+emailList.size());
       //	system.debug('Phone List:'+phoneList.size());
        system.debug('JsonBody-->'+jsonBody);
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        try{
            
            Http http = new Http();
            request.setEndpoint(ServiceUrl+searchFilter[0].Filter_String__c);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setHeader('Authorization', 'Bearer '+ serviceSettings.OAuth_Token__c);
            request.setBody(jsonBody);
            request.setTimeout(120000);
            response = http.send(request);
            System.debug('Response-->'+ response.getStatusCode());
            if (response.getStatusCode() == 200) {
                
                //system.debug('Response Body-->'+response.getBody());
             
              DataQualityServiceResponseWrapper  wrap =  DataQualityServiceResponseWrapper.parse(String.valueof(response.getBody()));
              system.debug('Wrap--'+wrap.response.duplicates);
                           
                if(wrap.response.duplicates.status=='FOUND'){
                    responseBody = JSON.serialize(wrap.response.duplicates.records);
                }else{
                 	responseBody = 'No_Result_Found';   
                }
            } else {
               // callInternalDedupe
               //createLogForFailedCallout( 'callSearchDedupeService', responseBody);
                
               List<List<SObject>> internalDedupeResult  = new List<List<SObject>>();
                
                if(phoneList.size()==0 && emailList.size()==0){
                    responseBody = 'No_Result_Found'; 
                }else{
                    internalDedupeResult = TalentDedupeSearch.postDedupTalents(firstName+' '+lastName, emailList, phoneList, '', '', new List<String>{'Talent'}, 'Many', true );
                    system.debug('internalDedupeResult-->'+internalDedupeResult);
                    if(internalDedupeResult.size() >0){
                        List <DataQualityServiceResponseWrapper.Records> wrapRecordList = new List <DataQualityServiceResponseWrapper.Records>();
                        for(integer i=0; i<internalDedupeResult[0].size(); i++){
                            DataQualityServiceResponseWrapper.Records wrapRecord = new DataQualityServiceResponseWrapper.Records();
                            DataQualityServiceResponseWrapper.DuplicateTalent dupeRecord = new DataQualityServiceResponseWrapper.DuplicateTalent();
                            dupeRecord.candidateId= String.valueof(internalDedupeResult[0][i].get('AccountId'));
                            dupeRecord.contactId = String.valueof(internalDedupeResult[0][i].get('Id'));
                            dupeRecord.firstName = String.valueof(internalDedupeResult[0][i].get('FirstName'));
                            dupeRecord.lastName = String.valueof(internalDedupeResult[0][i].get('LastName'));
                            dupeRecord.city = String.valueof(internalDedupeResult[0][i].get('MailingCity'));
                            dupeRecord.state = String.valueof(internalDedupeResult[0][i].get('MailingState'));
                            dupeRecord.country = String.valueof(internalDedupeResult[0][i].get('Talent_Country_Text__c'));
                            dupeRecord.resumeModifiedDate = String.valueOf(DateTime.valueOf(internalDedupeResult[0][i].get('LastModifiedDate')).date());
                            if(internalDedupeResult[0][i].get('LastActivityDate') != null){
                            	dupeRecord.qualificationsLastActivityDate = String.valueOf(Date.valueOf(internalDedupeResult[0][i].get('LastActivityDate')));    
                            }
                            dupeRecord.candidateStatus = String.valueof(internalDedupeResult[0][i].get('Candidate_Status__c'));
                            dupeRecord.talentId = String.valueof(internalDedupeResult[0][i].get('Talent_Id__c'));
                            dupeRecord.otherPhone = String.valueOf(internalDedupeResult[0][i].get('OtherPhone'));
                            dupeRecord.mobilePhone = String.valueOf(internalDedupeResult[0][i].get('MobilePhone'));
                            dupeRecord.workPhone = String.valueOf(internalDedupeResult[0][i].get('phone'));
                            dupeRecord.homePhone = String.valueof(internalDedupeResult[0][i].get('HomePhone'));
                            dupeRecord.otherEmail = String.valueof(internalDedupeResult[0][i].get('Other_Email__c'));
                            dupeRecord.personalEmail = String.valueof(internalDedupeResult[0][i].get('email'));
                            dupeRecord.workEmail = String.valueof(internalDedupeResult[0][i].get('Work_Email__c'));
                            dupeRecord.employmentPositionTitle = String.valueof(internalDedupeResult[0][i].get('title'));
                            wrapRecord.duplicateTalent = dupeRecord;
                            wrapRecord.similarity = 0.0;
                            wrapRecord.duplicateMatchFlags = new List<String>{'Internal_Dedupe_Match'};
                            wrapRecordList.add(wrapRecord);                            
                        }
                       // system.debug('wrapRecordList-->'+wrapRecordList);
                        if(wrapRecordList.size()>0){
                           responseBody = JSON.serialize(wrapRecordList);
                		}else{
                 			responseBody = 'No_Result_Found';   
                		}
                        
                    }
                }
            }
            
			if(transactionId != '' && transactionId !=null){
				if(responseBody !='No_Result_Found'){
					logInformation(jsonBody,'Result Found',transactionId);
				}else{
					logInformation(jsonBody,responseBody,transactionId);
				}
				
			}

          return responseBody;
        }
        catch(System.StringException e){
            system.debug('Exception -->'+e.getMessage());
            //createLogForFailedCallout('callSearchDedupeService', e.getStackTraceString());
			ConnectedLog.LogException('CreateDocumentController','callSearchDedupeService',e);
            return null;
        }
      
    }
   /* 
    public static void createLogForFailedCallout(String methodName, String exceptionString){
        Log__c callOutLog = new Log__c();
        callOutLog.Class__c = 'CreateDocumentController';
        callOutLog.Method__c = methodName;
        callOutLog.Description__c = exceptionString;           
        DateTime TodayDateTime = datetime.parse(system.now().format());         
        callOutLog.Log_Date__c = TodayDateTime;
        
        callOutLog.Subject__c = 'Copy Paste flow';
        database.insert(callOutLog);
        
    }
	*/
	@AuraEnabled
		public static void logInformation(String request, String result, String transactionId){
		
			System.debug('LogInformation_request'+request);
			System.debug('LogInformation_result'+result);
			System.debug('LogInformation_transactionId'+transactionId);
			
			Map<String,String> infoMap = new Map<String,String>();
			//infoMap.put('TransactionId',transactionId);
			infoMap.put('Type','Resume Search');
			infoMap.put('Request',request);
			infoMap.put('Result',result);

			ConnectedLog.LogInformation('Find/Add Page','CreateDocumentController','callSearchDedupeService',transactionId,'','logInformation',infoMap);
		}
    
}