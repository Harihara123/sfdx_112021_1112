@isTest 

public class CreateDocumentControllerTest  {
    @IsTest
    static void test_saveCopiedPasteContent() {
        Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='pdf',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
        insert glob;
        
        Account talentAccount = CreateTalentTestData.createTalent();
        Test.startTest();
        Id talentDocumentId = CreateDocumentController.saveCopiedPasteContent(talentAccount.Id
                                                                              , 'fileName'
                                                                              , EncodingUtil.urlEncode('someText for a resume', 'UTF-8')
                                                                              , null 
                                                                              , 'OTHER');
        Test.stopTest();
        Talent_Document__c talentDocument = [SELECT Id, Document_Name__c, Document_Type__c, Committed_Document__c, HTML_Version_Available__c FROM Talent_Document__c WHERE Id =: talentdocumentId];
        System.assertNotEquals(talentDocument, null, 'should not be null');
    }
    
    @IsTest
    static void test_saveCopiedPasteContent_FindOrAddTalent() {
        Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='pdf',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
        insert glob;
        
        Account talentAccount = CreateTalentTestData.createTalent();
        Test.startTest();
        Id talentDocumentId = CreateDocumentController.saveCopiedPasteContent(talentAccount.Id
                                                                              , 'fileName'
                                                                              , EncodingUtil.urlEncode('someText for a resume', 'UTF-8')
                                                                              , null 
                                                                              , 'OTHER');
        Test.stopTest();
        Talent_Document__c talentDocument = [SELECT Id, Document_Name__c, Document_Type__c, Committed_Document__c, HTML_Version_Available__c FROM Talent_Document__c WHERE Id =: talentdocumentId];
        System.assertNotEquals(talentDocument, null, 'should not be null');
    }
    
    @isTest
    static void test_getResumeListToReplace() { 
        Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='pdf',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
        insert glob;
        Account talentAccount = CreateTalentTestData.createTalent();  
        Id talentDocumentId = CreateDocumentController.saveCopiedPasteContent(talentAccount.Id
                                                                              , 'fileName'
                                                                              , EncodingUtil.urlEncode('someText for a resume', 'UTF-8')
                                                                              , null
                                                                              , 'OTHER' );           
        Test.startTest();
        List<Talent_Document__c> results = CreateDocumentController.getResumeListToReplace(talentAccount.Id);
        Test.stopTest();   
        System.assertNotEquals(results, null, 'should have 1');
    }
    
    @isTest
    static void test_replaceResumeId() {
        Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='pdf',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
        insert glob;
        Account talentAccount = CreateTalentTestData.createTalent();  
        Id talentDocumentId = CreateDocumentController.saveCopiedPasteContent(talentAccount.Id
                                                                              , 'fileName'
                                                                              , EncodingUtil.urlEncode('someText for a resume', 'UTF-8')
                                                                              , null 
                                                                              , 'OTHER');  
        Attachment talentDocumentAttachment = [SELECT Id FROM Attachment WHERE ParentId =: talentDocumentId];
        Test.startTest();
        Id replaceTalentDocumentId = CreateDocumentController.saveCopiedPasteContent(talentAccount.Id
                                                                                     , 'fileName2'
                                                                                     , EncodingUtil.urlEncode('someText for a resume2', 'UTF-8')
                                                                                     , talentDocumentId + '-' + talentDocumentAttachment.Id
                                                                                     , 'OTHER');  
        Test.stopTest();
        
        Talent_Document__c result = [SELECT Id, Mark_For_Deletion__c FROM Talent_Document__c WHERE Id =: talentDocumentId];
        System.assertEquals(result.Mark_For_Deletion__c, true, 'should be marked for deletion');
    }
    @isTest
    static void test_callSearchDedupeService(){
        CreateTalentTestData.createAcceptedFiletypeLOVs();
        Account account = CreateTalentTestData.createTalentAccount(); 
        Account accountnew = CreateTalentTestData.createTalentAccount(); 
        Contact conta = CreateTalentTestData.createTalentContact(account);
        conta.Other_Email__c ='teeeeest@test.com';
        conta.Work_Email__c ='teeeeest@test.com';
        conta.MobilePhone ='9090909056';
        conta.Phone ='9090909090';
        conta.HomePhone ='9090909090';
        update conta;
        Contact contanew = CreateTalentTestData.createTalentContact(accountnew);
        contanew.Other_Email__c ='test@test.com';
        contanew.Work_Email__c ='test@test.com';
        contanew.MobilePhone ='9090909090';
        contanew.Phone ='9090909090';
        contanew.HomePhone ='9090909090';
        update contanew;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(contanew.Id);
        Attachment attach=new Attachment();     
        attach.Name='Somefile.docx';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=account.id;
        insert attach;      
        Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.docx', 'Resume'); 
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('firstname',conta.FirstName);
        gen.writeStringField('lastname', conta.LastName);
        gen.writeStringField('parsedHRXML', 'test');
        gen.writeStringField('email', conta.Email);
        gen.writeStringField('otheremail', conta.Other_Email__c);
        gen.writeStringField('workemail', conta.Work_Email__c);
        gen.writeStringField('mobilephone', conta.MobilePhone);
        gen.writeStringField('workphone', conta.Phone);
        gen.writeStringField('homephone', conta.HomePhone);      
        gen.writeEndObject();
        system.debug('***'+gen.getAsString());
        String contactDetails = gen.getAsString();
        
        ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c();
        serviceSettings.Client_Id__c = '121111';
        serviceSettings.Name = 'Data Quality Service';
        serviceSettings.Service_Http_Method__c = 'POST';
        serviceSettings.Service_URL__c = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';
        serviceSettings.Token_URL__c = '--';
        insert serviceSettings;
        System.assertEquals('Data Quality Service', serviceSettings.Name);
        Test.setMock(HttpCalloutMock.class,new CreateDocumentServiceCalloutMock());
        Test.startTest();      
        CreateDocumentController.callSearchDedupeService( validAttachmentDocx.Id, 'Find_Add_Talent', contactDetails,'12345');
        Test.stopTest();  
        /* Test.startTest();      
CreateDocumentController.callSearchDedupeService( validAttachmentDocx.Id, 'Find_Add_Talent1', contactDetails,'12345');
Test.stopTest(); */
        
        
    }
    @isTest static void test_callSearchDedupeServiceOther(){    
        
        CreateTalentTestData.createAcceptedFiletypeLOVs();
        Account account = CreateTalentTestData.createTalentAccount(); 
        Account accountnew = CreateTalentTestData.createTalentAccount(); 
        Contact conta = CreateTalentTestData.createTalentContact(account);
        conta.Other_Email__c ='teeeeest@test.com';
        conta.Work_Email__c ='teeeeest@test.com';
        conta.MobilePhone ='9090909056';
        conta.Phone ='9090909090';
        conta.HomePhone ='9090909090';
        update conta;
        Contact contanew = CreateTalentTestData.createTalentContact(accountnew);
        contanew.Other_Email__c ='teeeeest@test.com';
        contanew.Work_Email__c ='test@test.com';
        contanew.MobilePhone ='9090909056';
        contanew.Phone ='9090909090';
        contanew.HomePhone ='9090909090';
        update contanew;
        Attachment attach=new Attachment();     
        attach.Name='Somefile.docx';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=account.id;
        insert attach; 
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(contanew.Id);
        
        Test.setFixedSearchResults(fixedSearchResults);
        Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.docx', 'Resume'); 
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('firstname',conta.FirstName);
        gen.writeStringField('lastname', conta.LastName);
        gen.writeStringField('parsedHRXML', 'test');
        gen.writeStringField('email', conta.Email);
        gen.writeStringField('otheremail', conta.Other_Email__c);
        gen.writeStringField('workemail', conta.Work_Email__c);
        gen.writeStringField('mobilephone', conta.MobilePhone);
        gen.writeStringField('workphone', conta.Phone);
        gen.writeStringField('homephone', conta.HomePhone);      
        gen.writeEndObject();
        system.debug('***'+gen.getAsString());
        String contactDetails = gen.getAsString();
        
        ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c();
        serviceSettings.Client_Id__c = '121111';
        serviceSettings.Name = 'Data Quality Service';
        serviceSettings.Service_Http_Method__c = 'POST';
        serviceSettings.Service_URL__c = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';
        serviceSettings.Token_URL__c = '--';
        insert serviceSettings;
        System.assertEquals('Data Quality Service', serviceSettings.Name);
        Test.setMock(HttpCalloutMock.class,new CreateDocumentServiceCalloutMock());
        Test.startTest();
        CreateDocumentController.callSearchDedupeService( validAttachmentDocx.Id, 'Code', contactDetails,'123456');
        Test.stopTest();
    }
    
    
    
    
}