/*************************************************************************************************
Apex Class Name :  CreateMultipleReqsController
Version         : 1.0 
Created Date    : 29 JAN 2012
Function        : Class used to create a mulitple Req.  
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------                 
* *                         01/27/2012                  Original Version
* Rajkumar                  07/02/2013              Change logic to set the System as Siebel or FieldGlass or Salesforce
* Rajkumar                  07/04/2013              Set Source system as Salesforce. when req created.. 
* Sudheer                   07/08/2013              Set VMS section's content to NULL. when req created.. 
* Aditya V(VA)              02/10/2015              Change logic to set the System as SIEBEL or Salesforce or VMS Reqs
                                                    Commented few lines
* Aditya V(VA)              03/26/2015              Making few fields to Null, when req created.
                                                (RRC_Positions__c,RRC_Open_Date__c,Decline_Reason__c,Merged_Req_ID__c,Merged_Req_System__c,Proactive_Req_Link__c)                                                    
* Aditya V(VA)              08/31/2015              Added additional logic to assign value '1004' for Origination Partner System Id 
                                                    whenever Req Origination System is 'Salesforce'                                                
**************************************************************************************************/ 
public class CreateMultipleReqsController{

    final static string SYSTEM_SOURCEREQ = 'SIEBEL';
//    final static string FIELDGLASS_SOURCEREQ = 'Fieldglass'; (VA)
    final static string SFDC_SOURCEREQ = 'SALESFORCE';
    public List<ReqStructure> reqsToCreate{get;set;}
    string reqId;
    Reqs__c sourceReq;
         
    ReqsExtension_AddressInformationHelper addressHelper = new ReqsExtension_AddressInformationHelper();
    set<string> reqFields = MultipleReqsHelper__c.getAll().keyset();
    Map<String, Schema.SObjectField> reqDescribe = Schema.SObjectType.Reqs__c.fields.getMap();
    Map<string,Account> associatedAccountMap = new Map<string,Account>();
    //string query = 'select Name,Origination_System__c,Origination_System_Id__c ';
    string query = 'select Name,Origination_System_Id__c,Req_Origination_System__c ';
    list<Log__c> errorLogs = new list<Log__c>();
    public Boolean Startdatevalidation {get;set;} 
   //wrapper class
   public class ReqStructure
   {
       public integer rowNum{get;set;}
       public Reqs__c requesition{get;set;}
       //constructor
       public ReqStructure(integer rowNum,Reqs__c requesition)
       {
          this.rowNum = rowNum;
          this.requesition = requesition;
       }
   }    
    //constructor
    public CreateMultipleReqsController()
    {
          reqsToCreate = new List<ReqStructure>();
          reqId = ApexPages.currentPage().getParameters().get('id');
          Startdatevalidation = False;
          // build the query string
          for(string fld : reqDescribe.keyset())
          {
              if(!reqFields.contains(fld))
              {
                  Schema.DescribeFieldResult allField = reqDescribe.get(fld).getDescribe();  
                  if(allField.isUpdateable() && allField.isCreateable() && !allField.isUnique())
                  {
                    //This field should not add into Query list as we have already added into initial Query string
                    if(fld !='Req_Origination_System__c')
                     query = query + ',' + fld;
                  }
              }
          }//end of loop
          string reqstr = String.escapeSingleQuotes(reqId);
           query += ' from Reqs__c where id = \'' + reqId + '\'';
         // query += ' from Reqs__c where id = reqstr';//Changed for Sec review
          sourceReq = database.query(query); 
          if(sourceReq.Stage__c == system.label.Req_Stage)
              Startdatevalidation = True;
          addNewReq();
    }
    public void addNewReq()
    {
          Reqs__c req = sourceReq.clone(false);          
          // Assign if SIEBEL Id has value         
         if(sourceReq.Req_Origination_System__c == SYSTEM_SOURCEREQ) 
          {
              req.System_SourceReq__c = SYSTEM_SOURCEREQ;
              req.SourceReq__c = sourceReq.Origination_System_Id__c;
                        
          }
          //added for Fieldglass req
          // Commented this section to change additional logic (VA)
//          else if(sourceReq.Req_Origination_System__c == FIELDGLASS_SOURCEREQ) 
//          {
//              req.System_SourceReq__c = FIELDGLASS_SOURCEREQ;
//              req.SourceReq__c = sourceReq.Origination_System_Id__c;              
//          }
          //added for Salesforce req (VA)
          else if(sourceReq.Req_Origination_System__c == SFDC_SOURCEREQ)
          {
              req.System_SourceReq__c = SFDC_SOURCEREQ;
              req.SourceReq__c = SourceReq.Name;
           
          }  
          else 
          {
              //req.System_SourceReq__c = SFDC_SOURCEREQ; (VA)
              //req.SourceReq__c = sourceReq.Name; (VA)
              //To allow all VMS reqs (VA)
              req.System_SourceReq__c = sourceReq.Req_Origination_System__c;
              req.SourceReq__c = sourceReq.Origination_System_Id__c;
              
          } 
          //Set source system as Salesforce as Req is created from Salesforce.com UI - #28101
          req.Req_Origination_System__c = 'Salesforce';
          req.VMS_Requirement_Status__c = Null;
          req.VMS_Requirement_Closed_Reason__c = Null; 
          req.Max_Submissions_Per_Supplier__c = Null; 
          req.Template__c=False;
          
          // ** 08/31/2015 Aditya added additional logic to assign default value '1004' to Origination Partner System Id, if Req Origination System is 'Salesforce'
          req.Orig_Partner_System_Id__c = '1004';
          
          //Making few fields to Null, when req is created from Salesforce UI (VA)
          //RRC_Positions__c,RRC_Open_Date__c,Decline_Reason__c,Merged_Req_ID__c,Merged_Req_System__c,Proactive_Req_Link__c
          req.RRC_Positions__c = Null;
          req.RRC_Open_Date__c = Null;
          req.Decline_Reason__c = Null;
          req.Merged_Req_ID__c = Null;
          req.Merged_Req_System__c = Null;
          req.Proactive_Req_Link__c = Null;
          req.EnterpriseReqId__c=Null;
          
          
          //Making RRC Open date to Null for All Reqs For Defect #-8089
          //req.RRC_Open_Date__c =null;         
          //assign the office accordingly
          User_Organization__c ofc = OfficeInformationHelper.getOfficeInformation();
          if(ofc != Null)
              req.Organization_Office__c = ofc.Id;
          reqsToCreate.add(new ReqStructure(reqsToCreate.size(),req));
          // invoke populateAccountAddress method
          ApexPages.currentPage().getParameters().put('rowNumber',string.valueOf(reqsToCreate.size() - 1));
          populateAccountAddress();
    }
    public pageReference cancel()
    {
          return reqsDetail(true);
    }
    public pageReference saveReqs()
    {
           List<Reqs__c> reqsToInsert = new List<Reqs__c>();
          
                

           //loop over the reqStructure
           for(ReqStructure rTemp : reqsToCreate)
           {
              
              reqsToInsert.add(rTemp.requesition);
           }    
           database.insert(reqsToInsert,false);
           reqsToCreate.clear();
           return reqsDetail(false);
    }
    public string reqDetail{get;set;}
    private pageReference reqsDetail(boolean isCancel)
    {
          if(!isCancel) // redirect to Reqs related list
          {
              reqDetail = Label.Req_HomePage;
          }
          else
              reqDetail = '/'+sourceReq.Id;
          
          system.debug('reqDetail :'+reqDetail);    
          return null;
    }
    public void populateAccountAddress()
    {
          string rowNum = ApexPages.currentPage().getParameters().get('rowNumber');
          Reqs__c req;
          if(rowNum != Null)
          {
              //get the associated Req record
              for(ReqStructure rTemp : reqsToCreate)
              {
                 if(rTemp.rowNum == integer.valueOf(rowNum))
                 {
                     req = rTemp.requesition;
                     break;
                 }
              }
              if(req != Null && req.Account__c != Null)
              {
                      Account acc;
                      // get the address values from Account
                      if(associatedAccountMap.get(req.Account__c) == Null)
                      {
                              set<string> SObjectId = new set<string>();
                              SobjectId.add(req.Account__c);
                              acc = database.query(addressHelper.getQueryInformation());
                              //populate the map
                              associatedAccountMap.put(acc.Id,acc);       
                      }
                      else
                              acc = associatedAccountMap.get(req.Account__c);
                      //populate req address accordingly
                      
                      req = addressHelper.processReqAddressInformation(acc,req); 
                  //Added as part of Sprint 18 for Export Control Logic
                  if(acc.Export_Controls_Compliance_required__c==true){
                      req.Export_Controls_compliance_required__c=true;
                      
                      if(req.compliance__c!=null && !string.valueof(req.compliance__c).contains('Export Controls')){
                           
                        req.compliance__c='Export Controls Questionnaire Required;'+req.compliance__c; //CRMX-1238
                       }
                      else if(req.compliance__c==null){
                           req.compliance__c='Export Controls Questionnaire Required;'; // CRMX-1238
                       }
                      
                  }
                  else if(req.compliance__c!=null && string.valueof(req.compliance__c).contains('Export Controls') && acc.Export_Controls_Compliance_required__c==false){
                       req.compliance__c= string.valueof(req.compliance__c.replace('Export Controls Questionnaire Required;',''));// CRMX-1238
                   }
                     
                      
                      
                  }
              //END of-Added as part of Sprint 18 for Export Control Logic
                  
              }               
          }

    
   
    
}