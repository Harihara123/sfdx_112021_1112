global class CreatePosLog implements Database.Batchable<sObject>, Database.stateful
{
global String QueryReq; 
 global integer Summary=0;

global CreatePosLog()
{ 
         // QueryReq = 'select id,Filled__c,Loss__c,Positions__c,Wash__c from reqs__c where lastmodifieddate=today';
      
}

global database.QueryLocator start(Database.BatchableContext BC)  
 {  
    //Create DataSet of Reqs to Batch
     return Database.getQueryLocator(QueryReq);
 } 
 
 global void execute(Database.BatchableContext BC, List<sObject> scope)
 {
 
 Map<String,List<Req_Position_Log__c>> mapReqsWithPosLogs = new Map<String,List<Req_Position_Log__c>>();

   
   List<Req_Position_Log__c> RpLogList=new List<Req_Position_Log__c>();
   List<Req_Position_Log__c> NewRpLogList=new List<Req_Position_Log__c>();
   Map<string,Req_Position_Log__c> ReqPosMap;
  
   
 List<Reqs__c> lstQueriedReqs = (List<Reqs__c>)scope;
 Map<Id,Reqs__c> ReqMap=new map<id,reqs__c>();
 system.debug('lstQueriedReqs'+lstQueriedReqs);
 
 for(Reqs__c rlst: lstQueriedReqs){
                   
          reqmap.put(rlst.id,rlst);          
                   } 
                   
          RpLogList=[select id,Reason__c,Req__c,Position_Change_Type__c,Num_Changed_Positions__c from Req_Position_Log__c where Req__c in:reqmap.keyset()];  
          
          system.debug(rpLogList);
                 
   /*               
 for(Req_Position_Log__c rplog:RpLogList){
 string keyPerposmap=string.valueof(rplog.id)+string.valueof(rplog.Req__c);
 ReqPosMap.put(keyPerposmap,rplog);
 }
 */
 
 
 for(Reqs__c r1 :reqmap.values()){
  Map<id,Req_Position_Log__c> tmplog=new map<id,Req_Position_Log__c>();
  decimal logfilled=0;
  decimal logwashed=0;
  decimal loglost=0;
  for(Req_Position_Log__c rplog:RpLogList){
  
  
  if(rplog.Req__c==r1.id){
  
  if(rplog.Position_Change_Type__c=='Fill'){
  
  logfilled=logfilled+rplog.Num_Changed_Positions__c;
  }
  
  if(rplog.Position_Change_Type__c=='Wash'){
  
  logwashed=logwashed+rplog.Num_Changed_Positions__c;
  }
  
  if(rplog.Position_Change_Type__c=='Lost'){
  
  loglost=loglost+rplog.Num_Changed_Positions__c;
  }
  
  
  tmplog.put(rplog.id,rplog);
  
  system.debug('tmplog'+tmplog);
 
 }
 }
 
 if(tmplog.size()==0 && r1.wash__c>0 ){
 DateTime Created_date=null;
 if(r1.Close_Date__c!=null && r1.Close_Date__c <date.today() ){
 Date closedate = r1.Close_Date__c;
 Time myTime = Time.newInstance(11, 11, 11, 0);
  Created_date = dateTime.newInstance(closedate, myTime);
 }
 else{
 Created_date=r1.lastmodifieddate;
 }
  Req_Position_Log__c positionLog = new Req_Position_Log__c();
  positionLog.Position_Change_Type__c='Wash';
  positionLog.Req__c=r1.id;
   positionLog.Reason__c='-';
   Positionlog.createddate=Created_date;
   
   positionlog.Num_Changed_Positions__c=r1.wash__c;
   NewRpLogList.add(positionLog);
   
  
 }
 
 if(tmplog.size()==0 && r1.Filled__c>0 ){
 DateTime Created_date=null;
 if(r1.Close_Date__c!=null&& r1.Close_Date__c < date.today()){
 Date closedate = r1.Close_Date__c;
 Time myTime = Time.newInstance(11, 11, 11, 0);
  Created_date = dateTime.newInstance(closedate, myTime);
 }
 else{
 Created_date=r1.lastmodifieddate;
 }
  Req_Position_Log__c positionLog = new Req_Position_Log__c();
  positionLog.Position_Change_Type__c='Fill';
  positionLog.Req__c=r1.id;
   positionLog.Reason__c='-';
   Positionlog.createddate=Created_date;
   positionlog.Num_Changed_Positions__c=r1.Filled__c;
   NewRpLogList.add(positionLog);
   
  
 }
 
  if(tmplog.size()==0 && r1.Loss__c>0 ){
 DateTime Created_date=null;
 if(r1.Close_Date__c!=null&& r1.Close_Date__c < date.today()){
 Date closedate = r1.Close_Date__c;
 Time myTime = Time.newInstance(11, 11, 11, 0);
  Created_date = dateTime.newInstance(closedate, myTime);
 }
 else{
 Created_date=r1.lastmodifieddate;
 }
  Req_Position_Log__c positionLog = new Req_Position_Log__c();
  positionLog.Position_Change_Type__c='Lost';
  positionLog.Req__c=r1.id;
   positionLog.Reason__c='-';
   Positionlog.createddate=Created_date;
   positionlog.Num_Changed_Positions__c=r1.Loss__c;
   NewRpLogList.add(positionLog);
   
  
 }
 
 if(tmplog.size()>0 && r1.Loss__c>0  && loglost!=r1.Loss__c ){
 DateTime Created_date=null;
 if(r1.Close_Date__c!=null && r1.Close_Date__c < date.today()){
 Date closedate = r1.Close_Date__c;
 Time myTime = Time.newInstance(11, 11, 11, 0);
  Created_date = dateTime.newInstance(closedate, myTime);
 }
 else{
 Created_date=r1.lastmodifieddate;
 }
  Req_Position_Log__c positionLog = new Req_Position_Log__c();
  positionLog.Position_Change_Type__c='Lost';
  positionLog.Req__c=r1.id;
   positionLog.Reason__c='-';
   Positionlog.createddate=Created_date;
   positionlog.Num_Changed_Positions__c=(r1.Loss__c-loglost);
   NewRpLogList.add(positionLog);
   
  
 }
 
 if(tmplog.size()>0 && r1.wash__c>0  && logwashed!=r1.Wash__c ){
 
 DateTime Created_date=null;
 if(r1.Close_Date__c!=null && r1.Close_Date__c < date.today()){
 Date closedate = r1.Close_Date__c;
 Time myTime = Time.newInstance(11, 11, 11, 0);
  Created_date = dateTime.newInstance(closedate, myTime);
 }
 else{
 Created_date=r1.lastmodifieddate;
 }
  Req_Position_Log__c positionLog = new Req_Position_Log__c();
  positionLog.Position_Change_Type__c='Wash';
  positionLog.Req__c=r1.id;
   positionLog.Reason__c='-';
   Positionlog.createddate=Created_date;
   positionlog.Num_Changed_Positions__c=(r1.Wash__c-logWashed);
   NewRpLogList.add(positionLog);
   
  
 }
 
 if(tmplog.size()>0 && r1.Filled__c>0  && logFilled!=r1.Filled__c ){
 DateTime Created_date=null;
 if(r1.Close_Date__c!=null && r1.Close_Date__c <date.today() ){
 Date closedate = r1.Close_Date__c;
 Time myTime = Time.newInstance(11, 11, 11, 0);
  Created_date = dateTime.newInstance(closedate, myTime);
 }
 else{
 Created_date=r1.lastmodifieddate;
 }
 
  Req_Position_Log__c positionLog = new Req_Position_Log__c();
  positionLog.Position_Change_Type__c='Fill';
  positionLog.Req__c=r1.id;
   positionLog.Reason__c='-';
   Positionlog.createddate=Created_date;
   positionlog.Num_Changed_Positions__c=(r1.Filled__c-logFilled);
   NewRpLogList.add(positionLog);
   
   system.debug('NewRpLogList'+NewRpLogList);
   
  
 }
 
 
 }
 summary=summary+NewRpLogList.size();
 if(NewRpLogList.size()>0 && label.PositionLogInsert=='True'){
 //summary=summary+NewRpLogList.size();
 system.debug('summary'+summary);
 
 
 insert NewRpLogList;
 }
 }
 global void finish(Database.BatchableContext BC)
 {
 system.debug('summary'+summary);
 
  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'hachanta@allegisgroup.com','tmcwhorter@teksystems.com','snayini@allegisgroup.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Important: Position Log email alert.');
           String errorText = 'Position logs.';                   
           mail.setPlainTextBody(string.valueof(summary));
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
 }

}