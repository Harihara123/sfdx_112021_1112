global class CreateProfileWrapper {

	public String tndid;
	public String join_path;
	public String preferred_language;
	public String resume_word_doc;
	public String resume_filename;
	public String resume_origin;
	public String first_name;
	public String last_name;
	public String country_code;
	public String desired_job_title;
	public Custom_questions custom_questions;
	public String email_address;
	public String zip_code;

	public class Custom_questions {
		public String JQKD3M26FPTXN32B153B;
		public String JQKF2SX7945NKZJHY1JJ;
		public String JQKF1Y15XF85K7DZPNXV;
		public String JQKD6CN62YFK3K0K2H3W;
		public String JQKF42F6CKBZ3ZT71RGP;
		public String JQKD0T76TQF6JN6K1VH1;
		public String JQKD6YS5ZQD9XWYJJCDR;
	}

	
	public static CreateProfileWrapper parse(String json) {
		return (CreateProfileWrapper) System.JSON.deserialize(json, CreateProfileWrapper.class);
	}
}