@isTest(seealldata = false)
global class CreateTalentTestData {
    public static Account createTalent() {
        //createCustomSettings();
        createState();
        createCountry();
        Account account = createTalentAccount();
        Contact testContact = createTalentContact(account);//added return type by akshay to create task and event for S-68236
        createTalentEvent(testContact,account);//added by akshay to create task and event for S-68236
        createTalentTask(testContact);//added  by akshay to create task and event for S-68236
        createTalentRecommendation(account); 
        createWorkExperience(account);
        createEducation(account);
        createTraining(account);
        createLicense(account);
        createPublication(account);
        createSpeaking(account);
        createAttResumeDocument(account, true, false, false);
        createMyList(testContact.id);
        createPipeline(testContact.id);
        return account;
    }

   /* public static void createCustomSettings() {
        Map<String, Connected_Data_Export_Switch__c> setting = Connected_Data_Export_Switch__c.getall();
        Connected_Data_Export_Switch__c exportSwitch = setting.get('DataExport');                              
        if(exportSwitch == null){
            Connected_Data_Export_Switch__c cdExportSwitch = new
               Connected_Data_Export_Switch__c(Data_Extraction_Insert_Flag__c = False, Name = 'DataExport',
                                              PubSub_URL__c='TestUrl');
            Insert cdExportSwitch;
        }
    } */

    public static Account createTalentAccount() {
        Account testAccount = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'testAccount',
            Talent_Preference_Internal__c = createPreferenceJSON(),
            Desired_Rate_Frequency__c = 'hourly',
            Desired_Rate__c =200
        );
        insert testAccount;
        return testAccount;
    }
    
    public static Account createTalentAccount(String ownership) {
        Account testAccount = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'testAccount',
            Talent_Preference_Internal__c = createPreferenceJSON(),
            Talent_Ownership__c = ownership
        );
        insert testAccount;
        return testAccount;
    }

    public static Contact getTalentContact(Account act) {
        Contact ct = [Select Id from Contact where AccountId = :act.Id limit 1][0];
        return ct;
    }
    
    public static Contact createTalentContact(Account testAccount) {
        Contact testContact = new Contact(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Contact' limit 1][0].Id,
            AccountId = testAccount.Id,
            Email = 'testContact@email.com',
            LastName = 'testLastName',
            FirstName = 'testFirstName',
            Title = 'ContactTitle',
            MobilePhone = '9696969696',
            HomePhone = '1234569870',
            Phone = '9876543210',
            OtherPhone = '5555555550',
            Other_Email__c = 'bca@bca.bca',
            MailingStreet = '312 ABC Way',
            MailingCity = 'Test City',
            MailingState = 'US.MD',
            MailingCountry = 'US',
            MailingLatitude = 33.386532,
            MailingLongitude = -112.215578
        );
        insert testContact;
        System.debug('Test Account ' + testContact);
        return testContact;
    }
    
    public static Talent_Recommendation__c createTalentRecommendation(Account testAccount) {
        Talent_Recommendation__c testReference = new Talent_Recommendation__c();
        //testReference.Talent__c = testAccount.Id;
        Contact con = CreateTalentTestData.createTalentContact(testAccount);
        testReference.Talent_Contact__c = con.Id;
        insert testReference;
        return testReference;
    }
    
    public static Talent_Experience__c createEducation(Account testAccount) {
        Global_LOV__c eduOrg = createEducationOrg();
        Talent_Experience__c edu = new Talent_Experience__c(
                Type__c = 'Education', 
                Education_Organization__c = eduOrg.Id, 
                Organization_Name__c = 'Org name',
                Degree__c = 'Degree', 
                Degree_Level__c = 'Masters',
                Graduation_Year__c = '2006', 
                Major__c = 'Major'
            );
        edu.Talent__c = testAccount.Id;
        insert edu;
        System.debug('Test Account ' + edu);
        return edu;
    }
    
    /* S-65382: additional function for data setup. date field assigned with system date in place of hardcoded date*/
    public static Talent_Work_History__c createWorkHistory(Account testAccount) {
        Talent_Work_History__c twh = new Talent_Work_History__c( 
                SourceId__c = 'Test'
                , Start_Date__c = System.today()
                , End_Date__c = System.today() + 5
                , Talent_Recruiter__c = 'peoplesoftId'
                , Talent_Account_Manager__c = 'peoplesoftId2'
                , Job_Title__c = 'Job Title' 
                , FInish_Code__c = ''
				, uniqueId__c = 'test123abc'
                , Talent__c = testAccount.Id
            );
        insert twh;
        return twh;
    }

    public static Talent_Experience__c createWorkExperience(Account testAccount) {
        Talent_Experience__c testWorkEx = new Talent_Experience__c(
                Type__c = 'Work', 
                Organization_Name__c = 'Org name', 
                Start_Date__c = System.today(), 
                End_Date__c = System.today(),
                Title__c = 'Title', 
                Location__c = 'Location', 
                Description__c = 'Description'
            );
        testWorkEx.Talent__c = testAccount.Id;
        insert testWorkEx;
        System.debug('Test Account ' + testWorkEx);
        return testWorkEx;
    }
    
    public static Talent_Experience__c createTraining(Account testAccount) {
        Talent_Experience__c trng = new Talent_Experience__c(
                Type__c = 'Training', 
                Certification__c = 'Certification__c',
                Graduation_Year__c = '2004'
            );
        trng.Talent__c = testAccount.Id;
        insert trng;
        System.debug('Test Account ' + trng);
        return trng;
    }
    
    public static Talent_Experience__c createSpeaking(Account testAccount) {
        Talent_Experience__c trng = new Talent_Experience__c(
                Type__c = 'Speaking', 
                Certification__c = 'Certification__c',
                Graduation_Year__c = '2004'
            );
        trng.Talent__c = testAccount.Id;
        insert trng;
        System.debug('Test Account ' + trng);
        return trng;
    }
    
    public static Talent_Experience__c createLicense(Account testAccount) {
        Talent_Experience__c trng = new Talent_Experience__c(
                Type__c = 'License', 
                Certification__c = 'Certification__c',
                Graduation_Year__c = '2004'
            );
        trng.Talent__c = testAccount.Id;
        insert trng;
        System.debug('Test Account ' + trng);
        return trng;
    }
    
    public static Talent_Experience__c createPublication(Account testAccount) {
        Talent_Experience__c trng = new Talent_Experience__c(
                Type__c = 'Publication', 
                Certification__c = 'Certification__c',
                Graduation_Year__c = '2004'
            );
        trng.Talent__c = testAccount.Id;
        insert trng;
        System.debug('Test Account ' + trng);
        return trng;
    }
    
    public static Talent_Document__c createAttOtherDocument(Account testAccount, Boolean committed, Boolean deleted,
                                                      Boolean defaultDoc) {
        return createAttDocument(testAccount, committed, deleted, defaultDoc, 10000, 'Test attachment.docx', 'Other');
    }
    
    public static Talent_Document__c createAttResumeDocument(Account testAccount, Boolean committed, Boolean deleted,
                                                      Boolean defaultDoc) {
        return createAttDocument(testAccount, committed, deleted, defaultDoc, 10000, 'Test attachment.docx', 'Resume');
    }
    
    public static Talent_Document__c createAttDocument(Account testAccount, Boolean committed, Boolean deleted, 
                                                       Boolean defaultDoc, Integer byteSize, String filename, String docType) {
        Talent_Document__c doc = new Talent_Document__c(
                Document_Name_Clean__c = filename, 
                Default_Document__c = defaultDoc,
                Document_Type__c = docType,
                HTML_Version_Available__c = true,
                Committed_Document__c = committed,
                Mark_For_Deletion__c = deleted
            );
        doc.Talent__c = testAccount.Id;
        insert doc;
        
        try {
            createAttachment(doc, byteSize, filename);
        } catch(Exception e) {
            System.debug(LoggingLevel.DEBUG, e);
        }

        return doc;
    }
    
    public static Attachment createAttachment(Talent_Document__c doc, Integer byteSize, String filename) {
        String content = 'a'.repeat(byteSize);

        Attachment att = new Attachment(
            ParentId = doc.Id,
            Name = filename,
            Description = 'File Description',
            Body = Blob.valueOf(content)
        );
        insert att;
        return att;
    }
    
    public static String createPreferenceJSON() {
        //return '{"lastModified":{"modifiedBy":{"userId":"005J0000005d90PIAQ","name":"Nagesh Akula"},"modifiedDate":"2016-04-14T15:45:13.171Z"},"candidateOverview":"Candidate Overview","employabilityInformation":{"reliableTransportation":true,"eligibleIn":[],"drugTest":true,"backgroundCheck":true,"securityClearance":true},"qualifications":{"skills":["ABC","Test","Skill1","Skiil3","ABC Skill4","Java"],"languages":["english"]},"geographicPrefs":{"willingToRelocate":true,"desiredLocation":{"city":"Ellicott City","state":"Maryland","country":"United States"},"commuteLength":{"distance":"20","unit":"miles"},"nationalOpportunities":true},"employmentPrefs":{"goalsAndInterests":["Goals","Goal2","Gial3"],"desiredRate":{"amount":"55","rate":"hourly"},"desiredPlacementType":"contract","desiredSchedule":"full-time","mostRecentRate":{"amount":"55","rate":"hourly"}}}';
        return '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
    }
    
    public static List<Event> createTalentEvent(Contact testContact,Account testAccount) {//changed by akshay for  S - 68236 

        Event event = new Event(
                            Whoid = testContact.id, 
                            Whatid = testAccount.id,
                            Subject = 'Test Event', 
                            startDateTime =  system.Now(),
                            ActivityDate =  system.today(),
                            DurationInMinutes  = 10);
        // added new activities for Interactive facets  by akshay for  S - 68236
        List<Event> IntEvent = new List<Event> ();

        IntEvent.add(event);
        IntEvent.add( new Event(
                            Whoid = testContact.id,
                            Whatid = testAccount.id, 
                            Subject = 'Test Event',
                            Type = 'Meeting', 
                            startDateTime =  system.Now(),
                            EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today()
                            //DurationInMinutes  = 10
                             ) );
        IntEvent.add( new Event(
                            Whoid = testContact.id, 
                            Whatid = testAccount.id,
                            Subject = 'Test Event',
                            Type = 'Internal Interview', 
                            startDateTime =  system.Now(),
                            EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today()
                            //DurationInMinutes  = 10
                            ));

        IntEvent.add( new Event( //Created for EventTriggerHandler code coverage
                            Whoid = testContact.id, 
                            Whatid = testAccount.id,
                            Subject = 'Test Call',
                            Type = 'Meetingw', 
                            startDateTime =  system.Now(),
                            EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
                            Description = 'Test Description'
                            //DurationInMinutes  = 10
                            ));

        Database.insert(IntEvent,false);
        return IntEvent;
    }
    
    public static List<Task> createTalentTask(Contact testContact) {//changed by akshay for  S - 68236 
        Task task = new Task(
                            Whoid = testContact.id, 
                            Subject = 'Test Task', 
                            ActivityDate =  system.today());
        // added new activities for Interactive facets  by akshay for  S - 68236
        List<Task> IntTask = new List<Task>();
        IntTask.add(task);
        IntTask.add( new Task(
                            Whoid = testContact.id, 
                            Subject = 'Test G2',
                            Type = 'G2',
                            Status = 'Completed' ,
                            ActivityDate =  system.today() ) );
        IntTask.add(new Task(
                            Whoid = testContact.id, 
                            Subject = 'Test RC',
                            Type = 'Reference Check',
                            Status = 'Completed' ,
                            ActivityDate =  system.today()));


        Database.insert(IntTask,false);
        return IntTask;
    }
    
    public static ATS_Job__c createATSJob() {
        ATS_Job__c job = new ATS_Job__c(
                                Job_Title__c = 'Test Position',
                                Account_Name__c = 'Test Company',
                                Job_Start_Date__c = System.today(), 
                                Hiring_Manager_Name__c = 'Hiring Manager', 
                                Account_Id__c = '123456789012345678'
                            );
        insert job;
        return job;
    }
    
    public static Order associateTalentToJob(Account candidate, ATS_Job__c job) {
        Contact c = [SELECT Id from Contact where AccountId =: candidate.Id][0];
        Id OSID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();//added RecType Condition by akshay ATS - 3344
        Order submittal = new Order(
                                AccountId = candidate.Id,
                                ShipToContactId = c.Id,
                                ATS_Job__c = job.Id,
                                EffectiveDate = System.today(),
                                Status = 'Linked',
                                RecordTypeId = OSID
                            );
        insert submittal;
        return submittal;
    }
    
    public static Order submitAssociated(Order submittal) {
        submittal.Status = 'Submitted';
        
        update submittal;
        return submittal;
    }
    
    public static Order createOrderEventNotProceeding(Order order, Account talent) {
        order.Status = 'Not Proceeding';
        order.Submittal_Not_Proceeding_Reason__c = 'Candidate Withdrew';
        Contact ct = getTalentContact(talent);
        
        /*Event event = new Event(
                            Whatid = order.Id,
                            Whoid = ct.Id,
                            Subject = 'Not Proceeding', 
                            startDateTime =  system.Now(),
                            DurationInMinutes  = 10,
                            Type = 'Not Proceeding');

                            
        insert event;*/
        update order;
        return order;
    }
    
    private static Global_LOV__c createEducationOrg() {
        Global_LOV__c eduOrg = new Global_LOV__c(
            Text_value__c = 'University of Test', 
            LOV_Name__c = 'Education  Institutions',
            Source_System_Id__c = 'Source_System_Id__c');
        insert eduOrg;
        return eduOrg;
    }
    
    private static Global_LOV__c createState() {
        Global_LOV__c eduOrg = new Global_LOV__c(
            Text_value__c = 'State', 
            LOV_Name__c = 'StateList',
            Source_System_Id__c = 'Source_System_Id__c');
        insert eduOrg;
        return eduOrg;
    }
    
    private static Global_LOV__c createCountry() {
        Global_LOV__c eduOrg = new Global_LOV__c(
            Text_value__c = 'Country', 
            LOV_Name__c = 'CountryList',
            Source_System_Id__c = 'Source_System_Id__c');
        insert eduOrg;
        return eduOrg;
    }
    
    public static void createAcceptedFiletypeLOVs(List<String> extensions, String LOVtype) {
        for (String filetype : extensions) {
            Global_LOV__c ftype = new Global_LOV__c(
                Text_value__c = filetype, 
                LOV_Name__c = LOVtype,
                Source_System_Id__c = 'Source_System_Id__c');
            insert ftype;
            System.debug(LoggingLevel.WARN, 'Creating extension LOV entry%%%%%%%%%%');
        }
    }
    
    public static void createAcceptedFiletypeLOVs() {
        createAcceptedFiletypeLOVs(new List<String> {'doc', 'docx', 'odt', 'txt', 'rtf'}, 'ATSResumeFiletypesAllowed');
        createAcceptedFiletypeLOVs(new List<String> {'doc', 'docx', 'odt', 'txt', 'rtf', 'pdf'}, 'ATSGenericFiletypesAllowed');
    }

    public static void createTalentCompensationPrefs(Account talent){

        talent.Desired_Additional_Compensation__c = 1000.0;
        talent.Desired_Bonus_Percent__c = 10.0;
        talent.Desired_Bonus__c = 11000.0;
        talent.Desired_Rate_Frequency__c = 'hourly';
        talent.Desired_Salary__c = 110000.0;
       
    }

    public static void createMyList(ID contactID){
        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'test list';
        insert testList;

        Contact_Tag__c tagList = new Contact_Tag__c();
        tagList.Tag_Definition__c = testList.Id;
        tagList.Contact__c = contactID;
        insert tagList;     
    }   

    public static void createPipeline(ID contactID) {
        Pipeline__c pipeline = new Pipeline__c();
        pipeline.Contact__c = contactID;
        pipeline.User__c = UserInfo.getUserId();

        System.debug('userid----'+UserInfo.getUserId());

        insert pipeline;
    }
	public static List<Event> createTalentEventWithJobPosting(Contact testContact,Account testAccount, Job_Posting__c testJobPosting) {

        Event event = new Event(
                            Whoid = testContact.id, 
                            Whatid = testAccount.id,
							Job_Posting__c = testJobPosting.Id,
                            Subject = 'Test Event', 
                            startDateTime =  system.Now(),
                            ActivityDate =  system.today(),
                            DurationInMinutes  = 10);
        List<Event> IntEvent = new List<Event> ();

        IntEvent.add(event);
        IntEvent.add( new Event(
                            Whoid = testContact.id,
                            Whatid = testAccount.id,
							Job_Posting__c = testJobPosting.Id, 
                            Subject = 'Test Event',
                            Type = 'Meeting', 
                            startDateTime =  system.Now(),
                            EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today()
                            //DurationInMinutes  = 10
                             ) );
        IntEvent.add( new Event(
                            Whoid = testContact.id, 
                            Whatid = testAccount.id,
							Job_Posting__c = testJobPosting.Id,
                            Subject = 'Test Event',
                            Type = 'Internal Interview', 
                            startDateTime =  system.Now(),
                            EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today()
                            //DurationInMinutes  = 10
                            ));

        IntEvent.add( new Event( //Created for EventTriggerHandler code coverage
                            Whoid = testContact.id, 
                            Whatid = testAccount.id,
							Job_Posting__c = testJobPosting.Id,
                            Subject = 'Test Call',
                            Type = 'Meetingw', 
                            startDateTime =  system.Now(),
                            EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
                            Description = 'Test Description'
                            //DurationInMinutes  = 10
                            ));

        Database.insert(IntEvent,false);
        return IntEvent;
    }
    public static List<Unified_Merge__c> insertUnifiedMergeRecrds(Map<String,String> contactPairs,String status, String survAcc,String vicAcc,String survCont, String victCont){
		List<Unified_Merge__c> results = new List<Unified_Merge__c>();
        for(String cont1:contactPairs.keySet()){
            Unified_Merge__c utm = new Unified_Merge__c();
            utm.Input_ID_1__c = cont1;
            utm.Input_ID_2__c = contactPairs.get(cont1);
            utm.Process_State__c = status;
           
            utm.Survivor_Account_ID__c = survAcc;
            utm.Victim_Account_ID__c  = vicAcc;

            utm.Survivor_Contact_ID__c  = survCont;
            utm.Victim_Contact_ID__c   = victCont;
            
            results.add(utm);
        }
        System.debug('results >>> '+results);
        if(results.size()>0){
            insert results;
        }
        return results;
    }
    

	public static user createUser(string profile, String opco, String division)
     {
         user newUser;
         try{
             Profile userProfile = [select Name from Profile where Name = :profile];
             
             long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
             Integer random = Math.Round(Math.Random() * timeDiff );
             newUser = new User(alias = 'test', 
								email='testuser123@allegisgroup.com',
								emailencodingkey='UTF-8', 
								lastname= profile + ' User', 
								languagelocalekey='en_US',
								localesidkey='en_US', 
								profileid = userProfile.Id,
								timezonesidkey='America/Los_Angeles', 
								OPCO__c = opco,
								Division__c = division,
								Region__c= 'Aerotek MidAtlantic',
								Region_Code__c = 'test',
								Office_Code__c = '10001',
								Peoplesoft_Id__c = string.valueOf(random * 143),
								User_Application__c = 'ATS',
								CompanyName = 'Aerotek',
								username='testusr'+random+'@allegisgroup.com.dev');             
           } Catch(Exception e)
           {  
               system.assertEquals('List has no rows for assignment to SObject',e.getMessage());
           }
           return newUser;
     }

	 public static user createUser(string profile, String opco, String division, String officeCode)
     {
         user newUser;
         try{
             Profile userProfile = [select Name from Profile where Name = :profile];
             
             long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
             Integer random = Math.Round(Math.Random() * timeDiff );
             newUser = new User(alias = 'test', 
								email='testuser123@allegisgroup.com',
								emailencodingkey='UTF-8', 
								lastname= profile + ' User', 
								languagelocalekey='en_US',
								localesidkey='en_US', 
								profileid = userProfile.Id,
								timezonesidkey='America/Los_Angeles', 
								OPCO__c = opco,
								Division__c = division,
								Region__c= 'Aerotek MidAtlantic',
								Region_Code__c = 'test',
								Peoplesoft_Id__c = string.valueOf(random * 143),
								User_Application__c = 'ATS',
								CompanyName = 'Aerotek',
								Office_Code__c = officeCode,
								username='testusr'+random+'@allegisgroup.com.dev');             
           } Catch(Exception e)
           {  
               system.assertEquals('List has no rows for assignment to SObject',e.getMessage());
           }
           return newUser;
     }

	 public static user createUser(string profile, String opco, String division, String officeCode, String companyName)
     {
         user newUser;
         try{
             Profile userProfile = [select Name from Profile where Name = :profile];
             
             long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
             Integer random = Math.Round(Math.Random() * timeDiff );
             newUser = new User(alias = 'test', 
								email='testuser123@allegisgroup.com',
								emailencodingkey='UTF-8', 
								lastname= profile + ' User', 
								languagelocalekey='en_US',
								localesidkey='en_US', 
								profileid = userProfile.Id,
								timezonesidkey='America/Los_Angeles', 
								OPCO__c = opco,
								Division__c = division,
								Region__c= 'Aerotek MidAtlantic',
								Region_Code__c = 'test',
								Peoplesoft_Id__c = string.valueOf(random * 143),
								User_Application__c = 'ATS',
								CompanyName = companyName,
								Office_Code__c = officeCode,
								username='testusr'+random+'@allegisgroup.com.dev');             
           } Catch(Exception e)
           {  
               system.assertEquals('List has no rows for assignment to SObject',e.getMessage());
           }
           return newUser;
     }

}