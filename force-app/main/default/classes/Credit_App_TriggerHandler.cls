/***************************************************************************
Name        : Credit_App_TriggerHandler
Created By  : JFreese (Appirio)
Date        : 20 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : Class that contains all of the functionality called by the
              Credit_App_Trigger. All contexts should be in this class.
*****************************************************************************/

public with sharing class Credit_App_TriggerHandler {

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Credit_App__c> Credit_Apps) {
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Credit_App__c> Credit_Apps) {
        CreditApp_AutomateApprovalStep(Credit_Apps);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Credit_App__c>oldMap, Map<Id, Credit_App__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Credit_App__c>oldMap, Map<Id, Credit_App__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Credit_App__c>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Credit_App__c>oldMap) {
    }

    /***************************************************************************************************************************************
    * Original Header: trigger CreditApp_AutomateApprovalStep on Credit_App__c(after insert)
    *****************************************************************************************************************************************/
    public void CreditApp_AutomateApprovalStep (List<Credit_App__c> Credit_Apps) {
       AutomateApprovalProcessHelper.submitForApproval(Credit_Apps);
    }

}