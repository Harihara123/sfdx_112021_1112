public with sharing class CustomAccountLookupController {
    
  public Account account {get;set;} // new account to create
  public Job_Title__c  jobtitle {get;set;}
  //public List<Account> results{get;set;} // search results
  public List<Job_Title__c> results{get;set;}
  
  public List<Job_Title__c> savedjobTitles {get;set;}
  
  public string searchString{get;set;} // search keyword
  
  public CustomAccountLookupController() {
    account = new Account();
    jobtitle = new Job_Title__c();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
   
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
  
  // run the search and return the records found. 
  private List<Job_Title__c> performSearch(string searchString) {

    String soql = 'select id, name from Job_Title__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where Active__c = true and  name LIKE \'%' + searchString +'%\'';
    soql = soql + '  limit 25';
    System.debug(soql);
    return database.query(soql); 

  }
  
  // save the new account record
  public PageReference saveAccount() {
    //insert account;
    jobtitle.Active__c = true;
    insert jobtitle;
    
    savedjobTitles  = new List<Job_Title__c>();
    savedjobTitles.add(jobtitle);
    // reset the account
    jobtitle = new Job_Title__c();
    
    //account = new Account();
    return null;
  }
  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}