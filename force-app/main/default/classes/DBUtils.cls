global class DBUtils {
    public static List<Database.SaveResult> UpdateRecords(List<SObject> objectList, Boolean allOrNone) {
        System.debug(logginglevel.WARN,'objectList :' + objectList); 
        If (!objectList.isEmpty()) {
            List<Database.SaveResult> results;
            try {
                results = Database.update(objectList, allOrNone); 
                System.debug(logginglevel.WARN,'results :' + results); 
                for (Database.SaveResult result : results) {
                    if (!result.getErrors().isEmpty()) {
                        System.debug(logginglevel.WARN,'Errors :' + result.getErrors());  
                    }
                }
            } catch (Exception e) {               
                System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());    
                System.debug(logginglevel.WARN,'Message: ' + e.getMessage());    
                System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
                System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());    
                System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString()); 
            }
            return results; 
        } else {
            System.debug(logginglevel.WARN,'Errors : List is Empty!!!!!'); 
            return null;
        }
    }
    
    public static List<Database.SaveResult> InsertRecords(List<SObject> objectList, Boolean allOrNone) {
        System.debug(logginglevel.WARN,'objectList :' + objectList); 
        If (!objectList.isEmpty()) {
            List<Database.SaveResult> results;
            try {
                results = Database.insert(objectList, allOrNone); 
                System.debug(logginglevel.WARN,'results :' + results); 
                for (Database.SaveResult result : results) {
                    if (!result.getErrors().isEmpty()) {
                        System.debug(logginglevel.WARN,'Errors :' + result.getErrors());  
                    }
                }
            } catch (Exception e) {               
                System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());    
                System.debug(logginglevel.WARN,'Message: ' + e.getMessage());    
                System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
                System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());    
                System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString()); 
            }
            return results; 
        } else {
            System.debug(logginglevel.WARN,'Errors : List is Empty!!!!!'); 
            return null;
        }
    }

    public static Database.SaveResult UpdateRecord(SObject obj, Boolean allOrNone) {
        System.debug(logginglevel.WARN,'object :' + obj); 
        If (obj != null) {
            Database.SaveResult result;
            try {
                result = Database.update(obj, allOrNone); 
                System.debug(logginglevel.WARN,'result :' + result); 
                if (!result.getErrors().isEmpty()) {
                    System.debug(logginglevel.WARN,'Errors :' + result.getErrors());  
                }
            } catch (Exception e) {               
                System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());    
                System.debug(logginglevel.WARN,'Message: ' + e.getMessage());    
                System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
                System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());    
                System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString()); 
            }
            return result; 
        } else {
            System.debug(logginglevel.WARN,'Error : Object is null!!!!!'); 
            return null;
        }
    }
    
    public static Database.SaveResult InsertRecord(SObject obj, Boolean allOrNone) {
        System.debug(logginglevel.WARN,'object :' + obj); 
        if (obj != null) {
            Database.SaveResult result;
            try {
                result = Database.insert(obj, allOrNone); 
                System.debug(logginglevel.WARN,'result :' + result);
                if (!result.getErrors().isEmpty()) {
                    System.debug(logginglevel.WARN,'Errors :' + result.getErrors());  
                }
            } catch (Exception e) {               
                System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());    
                System.debug(logginglevel.WARN,'Message: ' + e.getMessage());    
                System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
                System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());    
                System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString()); 
            }
            return result;
        } else {
            System.debug(logginglevel.WARN,'Error : Object is null!!!!!'); 
            return null;
        }
    }
}