/**
* File Name: DRZEventPublisher.apxc
* Description:  This class handles "triggers" from process builders to generate platform 
*               events consumed by the digital red zone application.
* Copyright : Allegis Group, Inc. - 2018 
* @author : Mark Tucker
* Modification Log: 
* */ 
public class DRZEventPublisher {
    //Rajeesh added to test event publishing from test class
    @testVisible private static List<OpportunityDRZMessage__e> optyEventList = new List<OpportunityDRZMessage__e>();
    public static void publishOppEvent(List<String> oppId, String BoardId, String eventType) {
        
        List<OpportunityDRZMessage__e> oDrzMsg = new List<OpportunityDRZMessage__e>();
        Map<string,string> OppScoreCardMap = new Map<string,string>();
        
        String otj = getOpportunityTeamJson(oppId);
        
        //Get DRZ Custom Setting 
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        List<Opportunity> opps = [SELECT Id, AccountId, Account.Name, Opportunity_Num__c , Organization_Office__c, Req_Job_Code__c, Req_Can_Use_Approved_Sub_Vendor__c, Req_Exclusive__c, Start_Date__c,Req_Sub_Linked__c,Req_Sub_Submitted__c,Req_Sub_Interviewing__c,Req_Sub_Offer_Accepted__c,
                                  Req_Total_Positions__c, Req_Division__c, Req_Job_Title__c, Req_Red_Zone__c, Req_Segment__c, StageName, LDS_Status__c, LastModifiedDate, LastModifiedById, Req_Interview_Date__c,Immediate_Start__c, Req_Skill_Specialty__c,Req_Sub_Not_Proceeding__c,Req_Sub_Started__c,
                                  Req_Job_Description__c, OwnerId, Owner.Name, Req_Open_Positions__c, isClosed, CreatedDate, CloseDate, Req_Total_Filled__c, Req_Total_Lost__c, Req_Total_Washed__c, Req_Hiring_Manager__r.Name, Req_Qualifying_Stage__c,Delivery_Center_Detailed__c,Req_Team_Value__c 
                                  FROM Opportunity WHERE Id in :oppId AND StageName!= 'Staging' AND RecordTypeId=:reqRecordTypeId AND toLabel(OpCo__c) in:OpCos];
        System.debug('opps---'+opps);
        if(!opps.isEmpty())
            OppScoreCardMap = getOppScoreCardMap(opps);
        for (Opportunity opp : opps) {
            oDrzMsg.add(new OpportunityDRZMessage__e(AccountName__c = opp.Account.Name,
                                                     Can_Use_Approved_Sub_Vendor__c = opp.Req_Can_Use_Approved_Sub_Vendor__c,
                                                     Event_Sub_Type__c = eventType,
                                                     Is_Exclusive__c = opp.Req_Exclusive__c,
                                                     AccountId__c = opp.AccountId,
                                                     JobTitle__c = opp.Req_Job_Title__c,
                                                     Req_Job_Code__c = opp.Req_Job_Code__c,
                                                     Req_Division__c = opp.Req_Division__c,
                                                     Req_Segment__c = opp.Req_Segment__c,
                                                     opp_id__c = opp.Id,
                                                     Opportunity_Num__c = opp.Opportunity_Num__c,
                                                     Organization_Office__c = opp.Organization_Office__c,
                                                     Is_Red_Zone__c = opp.Req_Red_Zone__c,
                                                     Stage_Name__c = opp.StageName,
                                                     Status__c = opp.LDS_Status__c,
                                                     OpportunityTeamJson__c = otj,
                                                     LastModifiedDate__c = opp.LastModifiedDate,
                                                     LastModifiedById__c = opp.LastModifiedById,
                                                     Job_Description__c = opp.Req_Job_Description__c,   //Srini Updated Description and Date 4/8/19
                                                     Total_Positions__c = opp.Req_Total_Positions__c,
                                                     Open_Positions__c = opp.Req_Open_Positions__c,
                                                     Owner_Name__c = opp.Owner.Name,
                                                     OwnerId__c = opp.OwnerId,
                                                     isClosed__c = opp.isClosed,
                                                     CreatedDate__c = opp.CreatedDate,
                                                     CloseDate__c = opp.CloseDate,
                                                     Total_Filled__c = opp.Req_Total_Filled__c,
                                                     Total_Lost__c = opp.Req_Total_Lost__c,
                                                     Total_Washed__c = opp.Req_Total_Washed__c,
                                                     Hiring_Manager__c = opp.Req_Hiring_Manager__r.Name,
                                                     BoardId__c = BoardId,
                                                     Expected_Interview_Date__c = opp.Req_Interview_Date__c,
                                                     Start_Date__c = opp.Start_Date__c,
                                                     Qualifying_Stage__c = opp.Req_Qualifying_Stage__c,
                                                     Immediate_Start__c = opp.Immediate_Start__c,
                                                     Skill_Specialty__c =  opp.Req_Skill_Specialty__c,
                                                     Delivery_Center_Detailed__c = opp.Delivery_Center_Detailed__c,
                                                     Team__c = opp.Req_Team_Value__c,
                                                     Req_Score_Card__c = OppScoreCardMap.get(opp.id)
                                                     ));
        }
        //Rajeesh for unit testing
        optyEventList.addAll(oDrzMsg);
        List<Database.SaveResult> results = EventBus.publish(oDrzMsg);
        checkResults(results);
    }
    
    public static void publishOrderEvent(List<String> orderId, String BoardId, String eventType) {
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        List<OrderDRZMessage__e> oDrzMsg = new List<OrderDRZMessage__e>();
        //Get DRZ Custom Setting 
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        //String otj = getOpportunityTeamJson(oppId);
        List<Order> orders = [SELECT Id, OwnerId, OpportunityId, ShipToContactId, ShipToContact.Full_Name__c, Status, LastModifiedDate, LastModifiedById, CreatedDate, Last_Moved_By__c,Interview_Details_JSON__c,Delivery_Center__c FROM Order
                              WHERE Status NOT IN ('Applicant','Linked') AND Id in :orderId AND toLabel(Opportunity.OpCo__c) in:OpCos and Opportunity.RecordTypeId =:reqRecordTypeId];
        for (Order order : orders) {
            oDrzMsg.add(new OrderDRZMessage__e(OpportunityId__c = order.OpportunityId,
                                               OwnerId__c = order.OwnerId,
                                               TalentName__c = order.ShipToContact.Full_Name__c,
                                               Status__c = order.Status,
                                               OrderId__c = order.Id,
                                               Event_Sub_Type__c = eventType,
                                               LastModifiedDate__c = order.LastModifiedDate,
                                               LastModifiedById__c = order.Last_Moved_By__c,
                                               CreatedDate__c = order.CreatedDate,
                                               BoardId__c = BoardId,
                                               ShipToContactId__c = order.ShiptoContactid,
                                               Interview_Details_JSON__c = order.Interview_Details_JSON__c,
                                               Delivery_Center__c = order.Delivery_Center__c));
        }
        List<Database.SaveResult> results = EventBus.publish(oDrzMsg);
        checkResults(results);
    }
    @TestVisible
    private static String getOpportunityTeamJson(List<String> oppID) {
        String oppTeamJson = '';
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject();
        gen.writeFieldName('OppTeamMember');
        gen.writeStartArray(); 
        DRZSettings__c s = DRZSettings__c.getValues('Feature_Flag_TM_Update');
        String FlagValue = s.Value__c;
        if(FlagValue != 'True'){
            List<OpportunityTeamMember> teamMembers = [SELECT UserId,OpportunityId,TeamMemberRole,LastModifiedById,LastModifiedDate FROM OpportunityTeamMember WHERE OpportunityId in :oppId and TeamMemberRole = 'Allocated Recruiter'];
            
            for (OpportunityTeamMember otm : teamMembers) {
                gen.writeStartObject();
                gen.writeStringField('UserSFId',otm.UserId);
                gen.writeStringField('OpportunityId',otm.OpportunityId);
                gen.writeStringField('TeamMemberRole',otm.TeamMemberRole);
                gen.writeStringField('LastModifiedById',otm.LastModifiedById);
                gen.writeDateTimeField('DateTime',otm.LastModifiedDate);
                gen.writeEndObject();
                //gen.writeObjectField('OppTeamMember', otm);
            }
            gen.writeEndArray();
            gen.writeEndObject();
            if (!teamMembers.isEmpty()) {
                String jsonStr = gen.getAsString();
                if (null != jsonStr && !jsonStr.equals('')) {
                    oppTeamJson = jsonStr;
                }
            }
        }
        return oppTeamJson;
    }
    
    @AuraEnabled
    public static boolean syncToDrz( String opportunityId ) {
        Map<string,string> OppScoreCardMap = new Map<string,string>();        
        DRZSettings__c s    = DRZSettings__c.getValues('OpCo');
        String OpCoList     = s.Value__c;
        List<String> OpCos  = OpCoList.split(';');
        
        String BoardId   = '';
        String eventType = 'UPDATED_OPPORTUNITY';
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        
        List<Opportunity> opp = [SELECT Id, AccountId, Account.Name, Opportunity_Num__c , Organization_Office__c, Req_Job_Code__c, Req_Can_Use_Approved_Sub_Vendor__c, Req_Exclusive__c, Start_Date__c,Req_Sub_Linked__c,Req_Sub_Submitted__c,Req_Sub_Interviewing__c,Req_Sub_Offer_Accepted__c,
                                  Req_Total_Positions__c, Req_Division__c, Req_Job_Title__c, Req_Red_Zone__c, Req_Segment__c, StageName, LDS_Status__c, LastModifiedDate, LastModifiedById, Req_Interview_Date__c, Req_Skill_Specialty__c,Req_Sub_Started__c,Req_Sub_Not_Proceeding__c,
                                  Req_Job_Description__c, OwnerId, Owner.Name, Req_Open_Positions__c, isClosed, CreatedDate, CloseDate, Req_Total_Filled__c, Req_Total_Lost__c, Req_Total_Washed__c, Req_Hiring_Manager__r.Name, Req_Qualifying_Stage__c,Delivery_Center_Detailed__c,Req_Team_Value__c 
                                  FROM Opportunity WHERE Id =: opportunityId AND StageName != 'Staging' AND RecordTypeId=:reqRecordTypeId AND toLabel(OpCo__c) in:OpCos Limit 1];     
        if(opp.size() > 0 ){
            OppScoreCardMap = getOppScoreCardMap(opp);
            List<String> oppId = new List<String>();
            oppId.add(opportunityId);
            String otj = getOpportunityTeamJson(oppId);               
            OpportunityDRZMessage__e opportunityEvt = new OpportunityDRZMessage__e( Can_Use_Approved_Sub_Vendor__c = opp[0].Req_Can_Use_Approved_Sub_Vendor__c,
                                                     Event_Sub_Type__c  = eventType,
                                                     AccountName__c     = opp[0].Account.Name,
                                                     Is_Exclusive__c    = opp[0].Req_Exclusive__c,
                                                     AccountId__c       = opp[0].AccountId,
                                                     JobTitle__c        = opp[0].Req_Job_Title__c,
                                                     Req_Job_Code__c    = opp[0].Req_Job_Code__c,
                                                     Req_Division__c    = opp[0].Req_Division__c,
                                                     Req_Segment__c     = opp[0].Req_Segment__c,
                                                     opp_id__c          = opp[0].Id,
                                                     Opportunity_Num__c = opp[0].Opportunity_Num__c,
                                                     Is_Red_Zone__c     = opp[0].Req_Red_Zone__c,
                                                     Stage_Name__c      = opp[0].StageName,
                                                     Status__c          = opp[0].LDS_Status__c,
                                                     OpportunityTeamJson__c = otj,                               
                                                     Job_Description__c = opp[0].Req_Job_Description__c,   //Srini Updated Description and Date 4/8/19
                                                     Total_Positions__c = opp[0].Req_Total_Positions__c,
                                                     Open_Positions__c  = opp[0].Req_Open_Positions__c,
                                                     Owner_Name__c      = opp[0].Owner.Name,
                                                     OwnerId__c         = opp[0].OwnerId,
                                                     isClosed__c        = opp[0].isClosed,
                                                     CreatedDate__c     = opp[0].CreatedDate,
                                                     CloseDate__c       = opp[0].CloseDate,
                                                     Total_Filled__c    = opp[0].Req_Total_Filled__c,
                                                     Total_Lost__c      = opp[0].Req_Total_Lost__c,
                                                     Total_Washed__c    = opp[0].Req_Total_Washed__c,
                                                     Hiring_Manager__c  = opp[0].Req_Hiring_Manager__r.Name,
                                                     BoardId__c         = BoardId,                                                     
                                                     Start_Date__c      = opp[0].Start_Date__c,
                                                     LastModifiedDate__c= opp[0].LastModifiedDate,
                                                     LastModifiedById__c= opp[0].LastModifiedById,
                                                     Organization_Office__c = opp[0].Organization_Office__c,
                                                     Expected_Interview_Date__c = opp[0].Req_Interview_Date__c,
                                                     Qualifying_Stage__c = opp[0].Req_Qualifying_Stage__c,
                                                     Sync_DRZ_User__c = UserInfo.getUserId(),
                                                     Is_Sync_DRZ__c = true,
                                                     Skill_Specialty__c =  opp[0].Req_Skill_Specialty__c,
                                                     Delivery_Center_Detailed__c = opp[0].Delivery_Center_Detailed__c,
                                                     Team__c = opp[0].Req_Team_Value__c,
                                                     Req_Score_Card__c = OppScoreCardMap.get(opp[0].id));
            Database.SaveResult opportunitySR = EventBus.publish(opportunityEvt);
            
            List<Order> orders = [SELECT Id, OwnerId, OpportunityId, ShipToContactId, ShipToContact.Full_Name__c, Status, LastModifiedDate, LastModifiedById, CreatedDate, Interview_Details_JSON__c,Last_Moved_By__c,Delivery_Center__c FROM Order
                                  WHERE Status NOT IN ('Applicant','Linked') AND opportunityId =: opportunityId AND toLabel(Opportunity.OpCo__c) in:OpCos AND (NOT status LIKE 'NOT PROCEEDING%' )];
            
            if( orders.size() > 0 ){
                List<OrderDRZMessage__e> oDrzMsg = new List<OrderDRZMessage__e>();
                for (Order order : orders) {
                    oDrzMsg.add(new OrderDRZMessage__e(OpportunityId__c = order.OpportunityId,
                                                       OwnerId__c = order.OwnerId,
                                                       TalentName__c = order.ShipToContact.Full_Name__c,
                                                       Status__c = order.Status,
                                                       OrderId__c = order.Id,
                                                       Event_Sub_Type__c = 'UPDATED_ORDER',
                                                       LastModifiedDate__c = order.LastModifiedDate,
                                                       LastModifiedById__c = order.Last_Moved_By__c,
                                                       CreatedDate__c = order.CreatedDate,
                                                       BoardId__c = BoardId,
                                                       ShipToContactId__c = order.ShiptoContactid,
                                                       Interview_Details_JSON__c = order.Interview_Details_JSON__c,
                                                       Sync_DRZ_User__c = UserInfo.getUserId(),
                                                       Is_Sync_DRZ__c = true,
                                                       Delivery_Center__c = order.Delivery_Center__c));
                }
                List<Database.SaveResult> results = EventBus.publish(oDrzMsg);
                checkResults(results);
            }
            
            return opportunitySR.isSuccess();
        
        }
        return false;
         
    }
    public static void ReOpenOpportunityOrderEvents(List<String> orderId, String BoardId, String eventType){
        String OpptyId = '';
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        List<OrderDRZMessage__e> oDrzMsg = new List<OrderDRZMessage__e>();
        //Get DRZ Custom Setting 
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        if(orderId[0].startsWith('006')){
            OpptyId = orderId[0];
        }else{
            for(Order x:[Select id, OpportunityId from Order where id IN:orderId]){
                OpptyId = x.OpportunityId;
            }
        }
        if(OpptyId !=null){
            List<Order> orders = [SELECT Id, OwnerId, OpportunityId, ShipToContactId, ShipToContact.Full_Name__c, Status, LastModifiedDate, LastModifiedById, CreatedDate, Last_Moved_By__c,Interview_Details_JSON__c,Delivery_Center__c FROM Order
                                  WHERE Status NOT IN ('Applicant','Linked','Offer Accepted','Started') AND OpportunityId =:OpptyId AND toLabel(Opportunity.OpCo__c) in:OpCos and Opportunity.RecordTypeId =:reqRecordTypeId AND (NOT status LIKE 'NOT PROCEEDING%' )];
            for (Order order : orders){
                oDrzMsg.add(new OrderDRZMessage__e(OpportunityId__c = order.OpportunityId,
                                                   OwnerId__c = order.OwnerId,
                                                   TalentName__c = order.ShipToContact.Full_Name__c,
                                                   Status__c = order.Status,
                                                   OrderId__c = order.Id,
                                                   Event_Sub_Type__c = eventType,
                                                   LastModifiedDate__c = order.LastModifiedDate,
                                                   LastModifiedById__c = order.Last_Moved_By__c,
                                                   CreatedDate__c = order.CreatedDate,
                                                   BoardId__c = BoardId,
                                                   ShipToContactId__c = order.ShiptoContactid,
                                                   Interview_Details_JSON__c = order.Interview_Details_JSON__c,
                                                   Delivery_Center__c = order.Delivery_Center__c));
            }
            List<Database.SaveResult> results = EventBus.publish(oDrzMsg);
            checkResults(results);
        }
    }
    public static void publishDailyBatchOrderEvents(Map<Id,String> OrderMap, String BoardId, String eventType) {
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        List<OrderDRZMessage__e> oDrzMsg = new List<OrderDRZMessage__e>();
        //Get DRZ Custom Setting 
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        //String otj = getOpportunityTeamJson(oppId);
        List<Order> orders = [SELECT Id, OwnerId, OpportunityId, ShipToContactId, ShipToContact.Full_Name__c, Status, LastModifiedDate, LastModifiedById, CreatedDate, Last_Moved_By__c,Interview_Details_JSON__c,Delivery_Center__c
                              FROM Order
                              WHERE Status NOT IN ('Applicant','Linked') AND Id in :OrderMap.keyset() AND Opportunity.OpCo__c in:OpCos and Opportunity.RecordTypeId =:reqRecordTypeId];
        for(Order order : orders){
            oDrzMsg.add(new OrderDRZMessage__e(OpportunityId__c = order.OpportunityId,
                                               OwnerId__c = order.OwnerId,
                                               TalentName__c = order.ShipToContact.Full_Name__c,
                                               Status__c = order.Status,
                                               OrderId__c = order.Id,
                                               Event_Sub_Type__c = eventType,
                                               LastModifiedDate__c = order.LastModifiedDate,
                                               LastModifiedById__c = order.Last_Moved_By__c,
                                               CreatedDate__c = order.CreatedDate,
                                               BoardId__c = BoardId,
                                               ShipToContactId__c = order.ShiptoContactid,
                                               Interview_Details_JSON__c = OrderMap.get(order.Id),
                                               Delivery_Center__c = order.Delivery_Center__c));
        }
        List<Database.SaveResult> results = EventBus.publish(oDrzMsg);
        checkResults(results);
    }
    public static void publishOppEventForTeamMember(List<String> oppId, Map<Id,String> OpptyTeamMember, String eventType) {
        List<OpportunityDRZMessage__e> oDrzMsg = new List<OpportunityDRZMessage__e>();
        Map<string,string> OppScoreCardMap = new Map<string,string>();        
        //Get DRZ Custom Setting 
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        DRZSettings__c f = DRZSettings__c.getValues('Feature_Flag_TM_Update');
        String FlagValue = f.Value__c;
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        if(FlagValue !='True'){
            List<Opportunity> opps = [SELECT Id, AccountId, Account.Name, Opportunity_Num__c , Organization_Office__c, Req_Job_Code__c, Req_Can_Use_Approved_Sub_Vendor__c, Req_Exclusive__c, Start_Date__c,Req_Sub_Linked__c,Req_Sub_Submitted__c,Req_Sub_Interviewing__c,Req_Sub_Offer_Accepted__c,
                                      Req_Total_Positions__c, Req_Division__c, Req_Job_Title__c, Req_Red_Zone__c, Req_Segment__c, StageName, LDS_Status__c, LastModifiedDate, LastModifiedById, Req_Interview_Date__c,Immediate_Start__c, Req_Skill_Specialty__c,Req_Sub_Started__c,Req_Sub_Not_Proceeding__c,
                                      Req_Job_Description__c, OwnerId, Owner.Name, Req_Open_Positions__c, isClosed, CreatedDate, CloseDate, Req_Total_Filled__c, Req_Total_Lost__c, Req_Total_Washed__c, Req_Hiring_Manager__r.Name, Req_Qualifying_Stage__c,Delivery_Center_Detailed__c,Req_Team_Value__c 
                                      FROM Opportunity WHERE Id in :oppId AND StageName!= 'Staging' AND RecordTypeId=:reqRecordTypeId AND toLabel(OpCo__c) in:OpCos];
            System.debug('opps---'+opps);
            if(!opps.isEmpty())
            OppScoreCardMap = getOppScoreCardMap(opps);
            for (Opportunity opp : opps) {
                oDrzMsg.add(new OpportunityDRZMessage__e(AccountName__c = opp.Account.Name,
                                                         Can_Use_Approved_Sub_Vendor__c = opp.Req_Can_Use_Approved_Sub_Vendor__c,
                                                         Event_Sub_Type__c = eventType,
                                                         Is_Exclusive__c = opp.Req_Exclusive__c,
                                                         AccountId__c = opp.AccountId,
                                                         JobTitle__c = opp.Req_Job_Title__c,
                                                         Req_Job_Code__c = opp.Req_Job_Code__c,
                                                         Req_Division__c = opp.Req_Division__c,
                                                         Req_Segment__c = opp.Req_Segment__c,
                                                         opp_id__c = opp.Id,
                                                         Opportunity_Num__c = opp.Opportunity_Num__c,
                                                         Organization_Office__c = opp.Organization_Office__c,
                                                         Is_Red_Zone__c = opp.Req_Red_Zone__c,
                                                         Stage_Name__c = opp.StageName,
                                                         Status__c = opp.LDS_Status__c,
                                                         OpportunityTeamJson__c = OpptyTeamMember.get(opp.Id),
                                                         LastModifiedDate__c = opp.LastModifiedDate,
                                                         LastModifiedById__c = opp.LastModifiedById,
                                                         Job_Description__c = opp.Req_Job_Description__c,   //Srini Updated Description and Date 4/8/19
                                                         Total_Positions__c = opp.Req_Total_Positions__c,
                                                         Open_Positions__c = opp.Req_Open_Positions__c,
                                                         Owner_Name__c = opp.Owner.Name,
                                                         OwnerId__c = opp.OwnerId,
                                                         isClosed__c = opp.isClosed,
                                                         CreatedDate__c = opp.CreatedDate,
                                                         CloseDate__c = opp.CloseDate,
                                                         Total_Filled__c = opp.Req_Total_Filled__c,
                                                         Total_Lost__c = opp.Req_Total_Lost__c,
                                                         Total_Washed__c = opp.Req_Total_Washed__c,
                                                         Hiring_Manager__c = opp.Req_Hiring_Manager__r.Name,
                                                         BoardId__c = '',
                                                         Expected_Interview_Date__c = opp.Req_Interview_Date__c,
                                                         Start_Date__c = opp.Start_Date__c,
                                                         Qualifying_Stage__c = opp.Req_Qualifying_Stage__c,
                                                         Immediate_Start__c = opp.Immediate_Start__c,
                                                         Skill_Specialty__c =  opp.Req_Skill_Specialty__c,
                                                         Delivery_Center_Detailed__c = opp.Delivery_Center_Detailed__c,
                                                         Team__c = opp.Req_Team_Value__c,
                                                         Req_Score_Card__c = OppScoreCardMap.get(opp.id)
                                                        ));
            }
            //Rajeesh for unit testing
            optyEventList.addAll(oDrzMsg);
            List<Database.SaveResult> results = EventBus.publish(oDrzMsg);
            checkResults(results);
        }
    }
    public static Map<string,string> getOppScoreCardMap(list<Opportunity> OppList){
        Map<string,string> OppScoreCardMap = new Map<string,string>();
        list<Opportunity>OppScoreCardList = new list<Opportunity>();
        for(Opportunity O:OppList){
            //OppScoreCardList.add(O);
            if(OppScoreCardMap.isEmpty()){
                OppScoreCardMap.put(o.id,getOppScoreCard(O));
            }else if(!OppScoreCardMap.containskey(o.id)){
                OppScoreCardMap.put(o.id,getOppScoreCard(O));
            }
        }
        return OppScoreCardMap;
    }
    public static string getOppScoreCard(Opportunity otm){
        string OppScoreCard;
        if(otm !=null){
            JSONGenerator gen = JSON.createGenerator(false);
            gen.writeStartObject();
            gen.writeFieldName('OppScoreCard');        
            gen.writeStartArray();
            gen.writeStartObject();
            if(otm.Req_Sub_Linked__c !=null)
                gen.writeNumberField('Linked',otm.Req_Sub_Linked__c);
            else
                gen.writeNumberField('Linked',0);
            if(otm.Req_Sub_Submitted__c !=null)
                gen.writeNumberField('Submitted',otm.Req_Sub_Submitted__c);
            else
                gen.writeNumberField('Submitted',0);
            if(otm.Req_Sub_Interviewing__c !=null)
            gen.writeNumberField('Interviewing',otm.Req_Sub_Interviewing__c);
            else
                gen.writeNumberField('Interviewing',0);
            if(otm.Req_Sub_Offer_Accepted__c !=null)
                gen.writeNumberField('Offer Accepted',otm.Req_Sub_Offer_Accepted__c);
            else
                gen.writeNumberField('Offer Accepted',0);
            if(otm.Req_Sub_Started__c !=null)
                gen.writeNumberField('Started',otm.Req_Sub_Started__c);
            else
                gen.writeNumberField('Started',0);
            if(otm.Req_Sub_Not_Proceeding__c !=null)
                gen.writeNumberField('Not Proceeding',otm.Req_Sub_Not_Proceeding__c);
            else
                gen.writeNumberField('Not Proceeding',0);
            gen.writeEndObject();
            gen.writeEndArray();
            gen.writeEndObject();
            OppScoreCard = gen.getAsString();
            system.debug('--OppScoreCard--'+OppScoreCard);
        }
        return OppScoreCard;
    }
    private static void checkResults(List<Database.SaveResult> results) {
        System.debug('results--'+results);
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    
                    System.debug('Error returned: ' +
                                 err.getStatusCode() +
                                 ' - ' +
                                 err.getMessage());
                }
            }       
        }
    }
}