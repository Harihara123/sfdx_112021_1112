@RestResource(urlMapping='/DRZLookup/*')
global without sharing class DRZLookup {
    
    public static Blob JSONSerialize(Map<String, String> jsMap){
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject(); 
        gen.writeFieldName('values');
        gen.writeStartArray();
        for(String o: jsMap.keySet()){
            if(o=='Error'){
                gen.writeStartObject();
                gen.writeStringField(o,jsMap.get(o));
                gen.writeEndObject();
            }
            else if(o=='Empty'){
                gen.writeStartObject();
                gen.writeEndObject();
            }
            else{
                gen.writeStartObject();          
                gen.writeStringField('Id',o);
                gen.writeStringField('Name', jsMap.get(o));
                gen.writeEndObject();
            }
        }
        gen.writeEndArray();
        gen.writeEndObject();
        return Blob.valueOf(gen.getAsString());
    } 
    
    @HttpGet
    global static void getDRZLookup(){
        //Get DRZ Custom Setting 
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        
		DRZSettings__c Drz = DRZSettings__c.getValues('Office');
        String OfficeList = Drz.Value__c;
        List<String> Offices = OfficeList.split(';');
		
        String type = RestContext.request.params.get('type');
        String value;
        Integer size;
        Integer page;
        Integer oSet;
        if(type=='account')
            value = RestContext.request.params.get('value');
        else
            value = '%'+RestContext.request.params.get('value')+'%';        
        if(type=='account' || type=='office'){
            size = Integer.valueOf(RestContext.request.params.get('size'));
            page = Integer.valueOf(RestContext.request.params.get('page'))-1;
            oSet = 0; //OFFSET
            if(page!=0)
                oSet = size*page;
        }

        Map<String, String> jsonImpl = new Map<String, String>();
        Blob jsonBody;        
        
        if(type=='account'){
            String clientRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
            try{
                List<Account> acc = new List<Account>([SELECT Id, Siebel_ID__c, Name, BillingCity, BillingState FROM Account WHERE RecordTypeId=:clientRecordType AND 
                                                       Siebel_ID__c=:value and Siebel_id__c!=null ]);
                if(!acc.isEmpty()){
                    for(Account a: acc){
                        if(a.BillingCity!=null && a.BillingState!=null)
                            jsonImpl.put(a.Id, a.Name+' - '+a.BillingCity+' - '+a.BillingState); 
                        else
                            jsonImpl.put(a.Id, a.Name);
                    }
                    RestContext.response.responseBody = JSONSerialize(jsonImpl);
                }else{
                    jsonImpl.put('Empty','');
                    RestContext.response.responseBody = JSONSerialize(jsonImpl); 
                }
            }
            catch(Exception e){
                RestContext.response.statusCode = 500;
                jsonImpl.put('Error',e.getMessage());
                RestContext.response.responseBody = JSONSerialize(jsonImpl);
                System.Debug('Error '+e.getMessage());
            }                
        }
        else if(type=='office'){
            try{              
                List<User_Organization__c> office = new List<User_Organization__c>([SELECT Id, Name FROM User_Organization__c WHERE Name LIKE:value AND Active__c=true AND OpCo_Name__c IN: Offices ORDER BY Name LIMIT: size OFFSET: oSet]);
                if(!office.isEmpty()){
                    for(User_Organization__c o: office){
                        jsonImpl.put(o.Id, o.Name);
                    }
                    RestContext.response.responseBody = JSONSerialize(jsonImpl);
                }else{
                    jsonImpl.put('Empty','');
                    RestContext.response.responseBody = JSONSerialize(jsonImpl); 
                }                    
            }
            catch(Exception e){
                RestContext.response.statusCode = 500;
                jsonImpl.put('Error',e.getMessage());
                RestContext.response.responseBody = JSONSerialize(jsonImpl);  
                System.Debug('Error '+e.getMessage());                
            }                 
        }
        else if(type=='division'){
            JSONGenerator gen = JSON.createGenerator(false);
            String jsonStr;
            PicklistFieldController pck = new PicklistFieldController();
            Map<String,List<String>> valueMap = pck.getDependentOptionsImpl('Opportunity','OpCo__c','Req_Division__c');
            List<String> divisionStr = new List<String>();
            for(String OpD: OpCos){
                divisionStr.addAll(valueMap.get(OpD));
            }
            for(String d: divisionStr){
                jsonImpl.put(d,d);
            }
            RestContext.response.responseBody = JSONSerialize(jsonImpl);          
        }else if(type == 'DeliveryCenter'){            
            jsonImpl = returnPickListMap('Opportunity','Delivery_Office__c');
            RestContext.response.responseBody = JSONSerialize(jsonImpl);
        }else if(type == 'Team'){
            jsonImpl = returnPickListMap('Opportunity','Req_Team_Value__c');
            RestContext.response.responseBody = JSONSerialize(jsonImpl);
        }        
        else{
                    jsonImpl.put('Error','Invalid type selection');
                    RestContext.response.responseBody = JSONSerialize(jsonImpl);
        }
    }
	public static Map<string,string> returnPickListMap(String objectName, String fieldName){
        Map<String,String> picklistValueMap = new Map<String,String>();
        Schema.SObjectType sO = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = sO.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
            picklistValueMap.put(pickListVal.getValue(),pickListVal.getLabel());                
        }
        return picklistValueMap;
    }
}