@RestResource(urlMapping = '/DRZOppData/*')
global without sharing class DRZOppData
{

    @HttpPost
    global static void GetDRZOppData()
    {
        try
        {
            String inputJSON = RestContext.request.requestBody.toString();
            ParseDRZOppDataRequest parsedData = ParseDRZOppDataRequest.parse(inputJSON);

            if (parsedData.data != null)
            {
                string type = 'Create';
                String query = buildQuery(parsedData);      
                String BoardId = parsedData.BoardId;
                if(parsedData.type !=null)
                    type = parsedData.type;
                List<Opportunity> lstOpportunity = Database.query(query);

                if (!lstOpportunity.isEmpty())
                {
                    List<String> lstOpportunityId = new list<String> ();
                    List<String> orderIDs = new List<String>(); 

                    for (Opportunity opp : lstOpportunity)
                    {                       
                        lstOpportunityId.add((String) opp.id);
                        for(Order ord : opp.Orders) {
                            orderIDs.add(ord.id);
                        }
                    }
                    if(type == 'Create'){
                        DRZEventPublisher.publishOppEvent(lstOpportunityId, BoardId, 'CREATE_BOARD_OPPORTUNITY');//get
                        if(!orderIDs.isEmpty()){
                            DRZEventPublisher.publishOrderEvent(orderIDs, BoardId, 'CREATE_BOARD_ORDER'); //get
                        }
                    }else if(type == 'Update'){
                        DRZEventPublisher.publishOppEvent(lstOpportunityId, BoardId, 'UPDATE_BOARD_OPPORTUNITY');
                        if(!orderIDs.isEmpty()){
                            DRZEventPublisher.publishOrderEvent(orderIDs, BoardId, 'UPDATE_BOARD_ORDER'); //get
                        }
                    }

                    RestContext.response.statusCode = 200;
                    RestContext.response.responseBody = blob.valueof('Matching records found. Will be published through platform events.');
                }
                else
                {

                    RestContext.response.statusCode = 204;
                    RestContext.response.responseBody = blob.valueof('No records found for search criteria');
                }

            }
            else {
                RestContext.response.statusCode = 400;
                RestContext.response.responseBody = blob.valueof('Malformed request');
            }
        }
        catch(System.JSONException ex)
        {
            system.debug('@@@JSON Exception:::' + ex.getStackTraceString());
            RestContext.response.statusCode = 400;
            RestContext.response.responseBody = Blob.valueOf('JSON Exception ::: ' + ex.getMessage());
        }
        catch(System.NullPointerException ex)
        {
            system.debug('@@@NullPointerException:::' + ex.getStackTraceString());
            RestContext.response.statusCode = 500;
            RestContext.response.responseBody = Blob.valueOf('Null Pointer Exception ::: ' + ex.getMessage());
        }
        catch(System.QueryException ex)
        {
            system.debug('@@@QueryException:::' + ex.getStackTraceString());
            RestContext.response.statusCode = 500;
            RestContext.response.responseBody = Blob.valueOf('Query Exception ::: ' + ex.getMessage());
        }
        catch(System.exception ex)
        {
            system.debug('@@@Exception:::' + ex.getStackTraceString());
            RestContext.response.statusCode = 500;
            RestContext.response.responseBody = Blob.valueOf('Exception ::: ' + ex.getMessage());
        }

    }

    private static String buildQuery(ParseDRZOppDataRequest parsedData) {
        Map<String, DRZ_WS_Request_Mapping__mdt> wsRequestMap = new Map<String, DRZ_WS_Request_Mapping__mdt>();
        for(DRZ_WS_Request_Mapping__mdt wsMapping : [Select DeveloperName, Field_API_Name__c, Field_Label__c, Is_In_Use__c from DRZ_WS_Request_Mapping__mdt where Object_Name__c='Opportunity' and Is_In_Use__c=true]) {
            wsRequestMap.put(wsMapping.DeveloperName, wsMapping);
        }

        String query = 'select id, (select id from Orders WHERE Status NOT IN (\'Applicant\',\'Linked\')) from opportunity where id != null and IsClosed= false ';//Rajeesh 3/28/19 updated isclosed false.

        for (ParseDRZOppDataRequest.FieldNameValues fnv : parsedData.data) {
            set<String> setValues = new set<String> ();
            if (fnv.fieldValues != null && fnv.fieldValues.size() > 0) {
                setValues.addAll(fnv.fieldValues); //for removing duplicates
                
                DRZ_WS_Request_Mapping__mdt wsMDT = wsRequestMap.get(fnv.fieldName);
                if (wsMDT != null) {
                    if(wsMDT.Field_API_Name__c == 'Delivery_Office__c'){
                        //query += ' and '+ wsMDT.Field_API_Name__c + ' includes ' + buildInClauseOfQuery(setValues);
                        string queryBefore = query.substringBefore(') from');
                        string queryAfter = query.substringAfter('(\'Applicant\',\'Linked\')');
                        string queryAfterNew = queryBefore+' and Delivery_Center__c in '+ buildInClauseOfQuery(setValues) + queryAfter;
                        queryAfterNew += ' and '+ wsMDT.Field_API_Name__c + ' includes ' + buildInClauseOfQuery(setValues);
                        query = '';
                        query = queryAfterNew;
                    }
                    if(wsMDT.Field_API_Name__c != 'Delivery_Office__c'){
                        query += ' and '+ wsMDT.Field_API_Name__c + ' in ' + buildInClauseOfQuery(setValues);
                    }
                }               

            }

        }
        System.debug('Query---'+query);
        return query;
    }

    private Static String buildInClauseOfQuery(Set<String> vals) {
        Set<String> reqAttr = new Set<String>();
        for(String attr : vals) {
            reqAttr.add(String.escapeSingleQuotes(attr));
        }
        String inClause = String.format('(\'\'{0}\'\')', new List<String> { String.join(new List<String> (reqAttr), '\',\'') });
        System.debug('---inclause---' + inClause);
        return inClause;
    }

    
}