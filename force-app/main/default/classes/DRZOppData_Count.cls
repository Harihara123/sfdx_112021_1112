@RestResource(urlMapping = '/DRZOppDataCount/*')
global without sharing class DRZOppData_Count
{

	@HttpPost
	global static void GetDRZOppDataCount()
	{
		try{
			String inputJSON = RestContext.request.requestBody.toString();
			ParseDRZOppDataRequest parsedData = ParseDRZOppDataRequest.parse(inputJSON);

			if(parsedData.data != null){
				String query = buildQuery(parsedData);				
				List<AggregateResult> lstOpportunity = Database.query(query);
                Object count = lstopportunity[0].get('expr0');
				if (!lstOpportunity.isEmpty())
                    RestContext.response.responseBody = blob.valueOf('[{"count":'+count+'}]');
				else
                    RestContext.response.responseBody = blob.valueof('[{"count":0}]');
			}
			else {
				RestContext.response.statusCode = 400;
				RestContext.response.responseBody = blob.valueof('Malformed request');
			}
		}
		catch(System.QueryException ex)
		{
			system.debug('@@@QueryException:::' + ex.getStackTraceString());
			RestContext.response.statusCode = 500;
			RestContext.response.responseBody = Blob.valueOf('Query Exception ::: ' + ex.getMessage());
		}
		catch(System.exception ex)
		{
			system.debug('@@@Exception:::' + ex.getStackTraceString());
			RestContext.response.statusCode = 500;
			RestContext.response.responseBody = Blob.valueOf('Exception ::: ' + ex.getMessage());
		}

	}

	private static String buildQuery(ParseDRZOppDataRequest parsedData) {
		Map<String, DRZ_WS_Request_Mapping__mdt> wsRequestMap = new Map<String, DRZ_WS_Request_Mapping__mdt>();
		for(DRZ_WS_Request_Mapping__mdt wsMapping : [Select DeveloperName, Field_API_Name__c, Field_Label__c, Is_In_Use__c from DRZ_WS_Request_Mapping__mdt where Object_Name__c='Opportunity' and Is_In_Use__c=true]) {
			wsRequestMap.put(wsMapping.DeveloperName, wsMapping);
		}

        String reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
		String query = 'select COUNT(id) from opportunity where id != null and StageName!=\'Staging\' and IsClosed= false and RecordTypeId=\''+reqRecordTypeId+'\'';

		for (ParseDRZOppDataRequest.FieldNameValues fnv : parsedData.data) {
			set<String> setValues = new set<String> ();
			if (fnv.fieldValues != null && fnv.fieldValues.size() > 0) {
				setValues.addAll(fnv.fieldValues); //for removing duplicates
				
				DRZ_WS_Request_Mapping__mdt wsMDT = wsRequestMap.get(fnv.fieldName);
				if (wsMDT != null) {
					if(wsMDT.Field_API_Name__c == 'Delivery_Office__c'){
                        query += ' and '+ wsMDT.Field_API_Name__c + ' includes ' + buildInClauseOfQuery(setValues);
                        /*string queryBefore = query.substringBefore(') from');
                        string queryAfter = query.substringAfter('(\'Applicant\')');
                        string queryAfterNew = queryBefore+' and '+ wsMDT.Field_API_Name__c + ' in '+ buildInClauseOfQuery(setValues) + queryAfter;
                        queryAfterNew += ' and '+ wsMDT.Field_API_Name__c + ' includes ' + buildInClauseOfQuery(setValues);
                        query = '';
                        query = queryAfterNew;*/                    	
                    }
                    if(wsMDT.Field_API_Name__c != 'Delivery_Office__c'){
                        query += ' and '+ wsMDT.Field_API_Name__c + ' in ' + buildInClauseOfQuery(setValues);
                    }
				}				

			}

		}
        //Get DRZ Custom Setting 
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        OpCoList = '(\''+OpCoList.replace(';', '\',\'')+'\')';
        query += ' and toLabel(OpCo__c) IN '+OpCoList;
        
		System.debug('Query---'+query);
		return query;
	}

	private Static String buildInClauseOfQuery(Set<String> vals) {
		Set<String> reqAttr = new Set<String>();
		for(String attr : vals) {
			reqAttr.add(String.escapeSingleQuotes(attr));
		}
		String inClause = String.format('(\'\'{0}\'\')', new List<String> { String.join(new List<String> (reqAttr), '\',\'') });
		System.debug('---inclause---' + inClause);
		return inClause;
	}

	
}