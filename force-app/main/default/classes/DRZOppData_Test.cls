@isTest
public class DRZOppData_Test {
  /*Rajeesh creating unit test class for the rest class.
   *DRZ 3/29/3019 
   * 
   */
    static testmethod void testGetDRZOppData(){
       Account newAccount = new Account(
            Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;
       
     //  string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
       opportunity  TestOpp=new opportunity ();
       date mydate = date.parse('12/31/2030');
       TestOpp.name='TestOpp1';
     //  TestOpp.RecordTypeId = reqRecordTypeId;
       TestOpp.StageName='Test';
       TestOpp.CloseDate=mydate;
       TestOpp.AccountId = newAccount.Id; 
       TestOpp.OpCo__c='Aerotek, Inc';
       insert TestOpp;
       Order testOrder = new Order();
       testOrder.AccountId=newAccount.Id;
       testorder.EffectiveDate= mydate;
       testOrder.Status='Draft';
       testOrder.isDRZUpdate__c=true;
       testOrder.OpportunityId = TestOpp.Id;
       testOrder.Bill_Rate__c = 123.0;
       insert testOrder;
        
       //custom setting
       //DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        //String OpCoList = s.Value__c;
        //List<String> OpCos = OpCoList.split(';');
       insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.');

       ParseDRZOppDataRequest drzRequest=new ParseDRZOppDataRequest();
      //String testReq = '{      "data": [          {              "fieldName": "jobCode",              "fieldValues": [      "0010Q000007tvSFQAY",                  "01t24000004XYRWAA4",                  "01tU0000000y4QCIAY",                  "01tU0000000ycrOIAQ"              ]          },          {              "fieldName": "account",              "fieldValues": [                  "0010Q000007tvHYQAY",                  "0010Q000007tvOmQAI",                  "0010Q000006CtDbQAK"              ]          },          {              "fieldName": "division",              "fieldValues": [                  "TEKsystems"              ]          },          {              "fieldName": "office",              "fieldValues": [                  "a1a0Q0000009t6RQAQ",                  "a1a0Q0000009t1NQAQ"              ]          },          {              "fieldName": "segment",              "fieldValues": [                  "Staffing"              ]          }      ] , "BoardId":"XYZ"}';
       String testReq = '{"data": [{"fieldName": "account","fieldValues": ["'+newAccount.Id+'"]}]}';
        drzRequest = ParseDRZOppDataRequest.parse(testReq);
       String JsonMsg=JSON.serialize(drzRequest);
       System.debug('Json:'+JsonMsg);
       Test.startTest();
       
       RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
    
        req.requestURI = '/services/apexrest/DRZOppData';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        DRZOppData.GetDRZOppData();
        update TestOpp;

     //   system.assertEquals(1, DRZEventPublisher.optyEventList.size());

    //Check DRZOppDataCount        
       RestRequest req1 = new RestRequest(); 
       RestResponse res1 = new RestResponse();
    
        req1.requestURI = '/services/apexrest/DRZOppDataCount';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        DRZOppData_Count.GetDRZOppDataCount();
     //   update TestOpp;       
        
    //Check DRZOppOrderDataCount        
       RestRequest req2 = new RestRequest(); 
       RestResponse res2 = new RestResponse();
    
        req2.requestURI = '/services/apexrest/DRZOppOrderDataCount';  //Request URL
        req2.httpMethod = 'POST';//HTTP Request Type
        req2.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req2;
        RestContext.response= res2;
        DRZOppOrderData_Count.GetDRZOppOrderDataCount();
      //  update TestOpp;
        Test.stopTest();           
        
    }
}