public class DRZPublishChatterEvents {
    
    public void createChatterCommentEvent( List<FeedComment> newRecs, String eventType ) {
        
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) {
            
            DRZSettings__c s = DRZSettings__c.getValues('OpCo');
            String OpCoList = s.Value__c;
            List<String> OpCos = OpCoList.split(';');
            
            string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            List<OpportunityChatterDRZMessage__e> drzMsgs = new List<OpportunityChatterDRZMessage__e>();
            
            set<string> opptyIds = new set<string>();
            for(FeedComment feedCmt:newRecs){
                opptyIds.add( feedCmt.ParentId );
            }
            
            if( newRecs.size() > 0 ){
                
                Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>([SELECT Id, Status__c, RecordTypeId, toLabel(OpCo__c) FROM 
                                                                              Opportunity WHERE Id in :opptyIds /*AND isClosed  != true*/
                                                                              AND toLabel(OpCo__c) in:OpCos AND RecordTypeId =: reqRecordTypeId]);
                
                if( opportunityMap.size() > 0 ){
                    for(FeedComment feedCmt:newRecs){
                        if (opportunityMap.containsKey(feedCmt.ParentId) ){
                            OpportunityChatterDRZMessage__e msg = new OpportunityChatterDRZMessage__e(
                                Event_Sub_Type__c = eventType,
                                FeedCommentId__c= feedCmt.Id,
                                FeedBody__c		= feedCmt.CommentBody,
                                feedItemId__c 	= feedCmt.FeedItemId,
                                parentOppId__c 	= feedCmt.ParentId
                            );
                            drzMsgs.add(msg);
                        }
                    }
                    
                    List<Database.SaveResult> results = EventBus.publish(drzMsgs);
                    checkResults(results);
                }
            }
        }
    }
    
    public void createChatterPostEvent( List<FeedItem> newRecs, String eventType ) {
        
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) { 
            
            DRZSettings__c s = DRZSettings__c.getValues('OpCo');
            String OpCoList = s.Value__c;
            List<String> OpCos = OpCoList.split(';');
            
            string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            List<OpportunityChatterDRZMessage__e> drzMsgs = new List<OpportunityChatterDRZMessage__e>();
            
            set<string> opptyIds = new set<string>();
            for(FeedItem feed:newRecs){
                opptyIds.add( feed.ParentId );
            }
            
            if( newRecs.size() > 0 ){
                
                Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>([SELECT Id, Status__c, RecordTypeId, toLabel(OpCo__c) FROM Opportunity 
                                                                              WHERE Id in :opptyIds /*AND isClosed  != true*/
                                                                              AND toLabel(OpCo__c) in:OpCos AND RecordTypeId =: reqRecordTypeId]);
                
                if( opportunityMap.size() > 0 ){
                    
                    for(FeedItem feed:newRecs){
                        
                        if (opportunityMap.containsKey(feed.ParentId) && feed.Body != null){
                            OpportunityChatterDRZMessage__e msg = new OpportunityChatterDRZMessage__e(
                                Event_Sub_Type__c = eventType,
                                feedBody__c = feed.Body,
                                feedItemId__c = feed.Id,
                                lastModifiedDate__c = feed.LastModifiedDate,
                                parentOppId__c = feed.ParentId
                            );
                            drzMsgs.add(msg);
                        }
                        if (opportunityMap.containsKey(feed.ParentId) && feed.Type == 'ContentPost' && feed.Body ==null){
                            OpportunityChatterDRZMessage__e msg = new OpportunityChatterDRZMessage__e(
                                Event_Sub_Type__c = eventType,
                                feedBody__c = 'File posted in Connected',
                                feedItemId__c = feed.Id,
                                lastModifiedDate__c = feed.LastModifiedDate,
                                parentOppId__c = feed.ParentId
                            );
                            drzMsgs.add(msg);
                        }
                    }
                    
                    List<Database.SaveResult> results = EventBus.publish(drzMsgs);
            		checkResults(results);
                }
            }
        }
    }
    
    private static void checkResults(List<Database.SaveResult> results) {
        System.debug('results------>'+results);
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' +
                                 err.getStatusCode() +
                                 ' - ' +
                                 err.getMessage());
                }
            }       
        }
    }
    
}