public class DRZPublishNewOpportunity {
	@InvocableMethod(label='Publish New Opportunity Event' description='Publishes a Platform Event for added Opportunities.')
    public static void publishNewOpportunityEvent(List<String> oppId) {
        DRZEventPublisher.publishOppEvent(oppId,'','NEW_OPPORTUNITY');
    }
}