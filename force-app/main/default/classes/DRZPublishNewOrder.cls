public class DRZPublishNewOrder {
	@InvocableMethod(label='Publish New Order Event' description='Publishes a Platform Event for added Orders.')
    public static void publishNewOrderEvent(List<String> orderId) {
        DRZEventPublisher.publishOrderEvent(orderId,'','NEW_ORDER');
    }
}