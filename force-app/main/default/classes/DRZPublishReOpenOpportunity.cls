public class DRZPublishReOpenOpportunity {
    @InvocableMethod(label='Publish ReOpen Opportunity Event' description='Publishes a Platform Event for ReOpen Opportunities.')
    public static void publishUpdatedOpportunityEvent(List<String> oppId) {
        DRZEventPublisher.publishOppEvent(oppId,'','REOPEN_OPPORTUNITY');
    }
}