public class DRZPublishUpdatedOpportunity {
	@InvocableMethod(label='Publish Updated Opportunity Event' description='Publishes a Platform Event for updated Opportunities.')
    public static void publishUpdatedOpportunityEvent(List<String> oppId) {
        DRZEventPublisher.publishOppEvent(oppId,'','UPDATED_OPPORTUNITY');
    }
}