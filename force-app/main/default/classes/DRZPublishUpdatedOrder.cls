public class DRZPublishUpdatedOrder {
	@InvocableMethod(label='Publish Updated Order Event' description='Publishes a Platform Event for updated Orders.')
    public static void publishUpdatedOrderEvent(List<String> orderId) {
        DRZEventPublisher.publishOrderEvent(orderId,'','UPDATED_ORDER');
    }
}