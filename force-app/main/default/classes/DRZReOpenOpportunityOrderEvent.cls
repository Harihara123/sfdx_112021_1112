public class DRZReOpenOpportunityOrderEvent {
  @InvocableMethod(label='ReOpen Opportunity Order Event' description='ReOpen Opportunity Order Event')
    public static void ReOpenOpportunitiyOrder(List<String> orderId) {
        DRZEventPublisher.ReOpenOpportunityOrderEvents(orderId,'','REOPEN_ORDER');
    }
}