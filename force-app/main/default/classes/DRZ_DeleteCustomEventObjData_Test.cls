@isTest(seealldata = false)
public class DRZ_DeleteCustomEventObjData_Test{
    static testMethod void ScheduleNBatchTest(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'DRZMyOppty_Object_Query',Value__c='Select id from MyOpptyEvent__c WHERE CreatedDate = today');
        insert DRZSettings;   
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'DRZMyOrder_Object_Query',Value__c='Select id from MyOrderEvent__c WHERE CreatedDate = today');
        insert DRZSettings1;
        
         MyOpptyEvent__c OpptyEvent = new MyOpptyEvent__c(Name = 'TestOppty1');
        insert OpptyEvent;
         MyOrderEvent__c OrderEvent = new MyOrderEvent__c(Name ='TestOrder1');
        insert OrderEvent;   
        
        Test.startTest();
        DRZ_DeleteOpptyEventObjectDataSchedule b = new DRZ_DeleteOpptyEventObjectDataSchedule();
        String sch = '0 0 2 * * ?';
        System.schedule('myJobTestJobName', sch, b);
        
        DRZ_DeleteOderEventObjectDataSchedule c = new DRZ_DeleteOderEventObjectDataSchedule();
        String schs = '0 0 2 * * ?';
        System.schedule('myJobTestJobName1', schs, c);
        Test.stopTest();
    }
}