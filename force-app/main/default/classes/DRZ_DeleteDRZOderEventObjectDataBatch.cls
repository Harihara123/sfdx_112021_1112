global class DRZ_DeleteDRZOderEventObjectDataBatch implements Database.Batchable<Sobject>{
	 global database.Querylocator start(Database.BatchableContext context){
         DRZSettings__c s = DRZSettings__c.getValues('DRZMyOrder_Object_Query');
         String strQuery = s.Value__c;
         return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext context , list<MyOrderEvent__c> scope){
        list<MyOrderEvent__c> Orderdelete = new list<MyOrderEvent__c>();
        for(MyOrderEvent__c x:scope){
            Orderdelete.add(x);
        }
        if(!Orderdelete.isEmpty())
        delete Orderdelete;
    }
    global void finish(Database.BatchableContext context){
    }
}