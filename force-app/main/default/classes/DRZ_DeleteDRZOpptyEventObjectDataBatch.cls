global class DRZ_DeleteDRZOpptyEventObjectDataBatch implements Database.Batchable<Sobject>{
	global database.Querylocator start(Database.BatchableContext context){
        DRZSettings__c s = DRZSettings__c.getValues('DRZMyOppty_Object_Query');
        String strQuery = s.Value__c;
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext context , list<MyOpptyEvent__c> scope){
        list<MyOpptyEvent__c> Opptydelete = new list<MyOpptyEvent__c>();
        for(MyOpptyEvent__c x:scope){
            Opptydelete.add(x);
        }
        if(!Opptydelete.isEmpty())
        delete Opptydelete;
    }
    global void finish(Database.BatchableContext context){
    }
}