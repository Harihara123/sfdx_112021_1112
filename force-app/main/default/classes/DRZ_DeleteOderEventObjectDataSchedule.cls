global class DRZ_DeleteOderEventObjectDataSchedule implements Schedulable{
	global void execute(SchedulableContext sc){        
        DRZ_DeleteDRZOderEventObjectDataBatch batchJob = new DRZ_DeleteDRZOderEventObjectDataBatch();
        database.executebatch(batchJob);
    }
}