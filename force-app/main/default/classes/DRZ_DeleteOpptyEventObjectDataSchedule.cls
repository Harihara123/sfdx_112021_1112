global class DRZ_DeleteOpptyEventObjectDataSchedule implements Schedulable{
    global void execute(SchedulableContext sc){        
        DRZ_DeleteDRZOpptyEventObjectDataBatch batchJob = new DRZ_DeleteDRZOpptyEventObjectDataBatch();
        database.executebatch(batchJob);
    }
}