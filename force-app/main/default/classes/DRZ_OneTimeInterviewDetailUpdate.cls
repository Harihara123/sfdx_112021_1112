global class DRZ_OneTimeInterviewDetailUpdate implements Database.Batchable<Sobject>{
    global database.Querylocator start(Database.BatchableContext context){
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        return Database.getQueryLocator([
            Select Id, OwnerId, OpportunityId, ShipToContactId, ShipToContact.Full_Name__c, Status, LastModifiedDate, LastModifiedById, CreatedDate, Last_Moved_By__c,Interview_Details_JSON__c
            from Order 
            where Opportunity.isclosed = false and recordtype.name = 'OpportunitySubmission' and status = 'Interviewing' and Opportunity.recordtype.Name = 'Req' and tolabel(Opportunity.OpCo__c) in:OpCos
        ]);
    }
    global void execute(Database.BatchableContext context , list<Order> scope){
        Map<Id,Order> OrderMap = new Map<Id,Order>();
        Map<Id,Event> EventMap = new Map<Id,Event>();
        Map<Id,String> OrderJSONMap = new Map<Id,String>();
        string lt = '"InterviewDetails" : [ ]';
        for(Order O:scope){
            if(O.Interview_Details_JSON__c == null || O.Interview_Details_JSON__c.contains(lt)){
                OrderMap.put(O.id,O);
            }
        }
        if(!OrderMap.isEmpty()){
            for(Event Ev:[Select Id,StartDateTime,EndDateTime,Interview_Status__c,Interview_Type__c,Submittal_Interviewers__c,CreatedById, WhatId, LastModifiedById,Createddate 
                          from Event 
                          where WhatId IN:OrderMap.keyset() and Type = 'Interviewing' 
                          Order By StartDateTime DESC]){
                if(Ev.StartDateTime > Ev.Createddate){
                    if(!EventMap.isEmpty() && EventMap.containskey(Ev.WhatId) && EventMap.get(Ev.WhatId).StartDateTime > Ev.StartDateTime){
                        EventMap.put(EventMap.get(Ev.WhatId).WhatId,EventMap.get(Ev.WhatId));
                        OrderJSONMap.put(EventMap.get(Ev.WhatId).WhatId,DRZ_SendDailyInterviewDetailsBatch.GenerateJSON(EventMap.get(Ev.WhatId)));
                    }else{
                        EventMap.put(Ev.WhatId,Ev);
                        OrderJSONMap.put(EventMap.get(Ev.WhatId).WhatId,DRZ_SendDailyInterviewDetailsBatch.GenerateJSON(EventMap.get(Ev.WhatId)));
                    }
                }
                if(!EventMap.containskey(Ev.WhatId)){
                    if(Ev.Createddate > Ev.StartDateTime){
                        if(!EventMap.isEmpty() && EventMap.containskey(Ev.WhatId) && EventMap.get(Ev.WhatId).StartDateTime > Ev.StartDateTime){
                            EventMap.put(EventMap.get(Ev.WhatId).WhatId,EventMap.get(Ev.WhatId));
                            OrderJSONMap.put(EventMap.get(Ev.WhatId).WhatId,DRZ_SendDailyInterviewDetailsBatch.GenerateJSON(EventMap.get(Ev.WhatId)));
                        }else{
                            EventMap.put(Ev.WhatId,Ev);
                            OrderJSONMap.put(EventMap.get(Ev.WhatId).WhatId,DRZ_SendDailyInterviewDetailsBatch.GenerateJSON(EventMap.get(Ev.WhatId)));
                        }
                    }
                }
            }
            if(!OrderJSONMap.isEmpty()){
                DRZEventPublisher.publishDailyBatchOrderEvents(OrderJSONMap,'','UPDATED_ORDER');
            }
        }
    }
    global void finish(Database.BatchableContext context){
        
    }
}