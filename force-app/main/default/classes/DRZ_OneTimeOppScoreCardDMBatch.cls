global class DRZ_OneTimeOppScoreCardDMBatch implements Database.Batchable<Sobject>{
    global database.Querylocator start(Database.BatchableContext context){
        DRZSettings__c s = DRZSettings__c.getValues('OpCo');
        String OpCoList = s.Value__c;
        List<String> OpCos = OpCoList.split(';');
        return Database.getQueryLocator([Select Id, Req_Sub_Linked__c, Req_Sub_Submitted__c, Req_Sub_Interviewing__c, Req_Sub_Offer_Accepted__c, Req_Sub_Started__c, Req_Sub_Not_Proceeding__c
                                         from Opportunity 
                                         where recordtype.Name = 'Req' and tolabel(OpCo__c) in:OpCos and isclosed = false and StageName != 'Staging'
                                        ]);    
    }
    global void execute(Database.BatchableContext context , list<Opportunity> scope){
        list<string> OPId = new list<string>();
        for(Opportunity O:scope){
            OPId.add(O.Id);
        }
        system.debug('--OPId size--'+OPId.size()+'--OPId--'+OPId);
        if(!OPId.isEmpty())
            DRZPublishUpdatedOpportunity.publishUpdatedOpportunityEvent(OPId);
    }
    global void finish(Database.BatchableContext context){
        
    }
}