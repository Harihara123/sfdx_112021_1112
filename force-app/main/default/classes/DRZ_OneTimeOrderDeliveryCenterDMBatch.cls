global class DRZ_OneTimeOrderDeliveryCenterDMBatch implements Database.Batchable<Sobject>{
    global String strQuery;
	global database.Querylocator start(Database.BatchableContext context){
        strQuery = System.Label.DRZ_Order_Delivery_Center_Query;
        system.debug('--strQuery--'+strQuery);
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext context , list<Order> scope){
        list<Order> DCOrderUpdate = new list<Order>();
        for(Order OD:scope){
            if(OD.Delivery_Center__c == null && OD.Last_Moved_By__r.Team__c !=null){
                OD.Delivery_Center__c = OD.Last_Moved_By__r.Team__c;
                DCOrderUpdate.add(OD);
            }
        }
        try{
            system.debug('--DCOrderUpdate size--'+DCOrderUpdate.size()+'--DCOrderUpdate--'+DCOrderUpdate);
            if(!DCOrderUpdate.isEmpty())
                database.update(DCOrderUpdate,false);
        }catch(exception e){
            system.debug('---exception msg--'+e.getMessage()+'--line number--'+e.getLineNumber());
        }
    }
    global void finish(Database.BatchableContext context){
        
    }
}