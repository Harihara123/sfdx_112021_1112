global class DRZ_OneTimeSupportRequestDMBatch  implements Database.Batchable<Sobject>{
    global String strQuery;
	global database.Querylocator start(Database.BatchableContext context){
        strQuery = System.Label.DRZ_Support_Request_Query;
        system.debug('--strQuery--'+strQuery);
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext context , list<Support_Request__c> scope){
        list<Support_Request__c> DeliveryCenterList = new list<Support_Request__c>();
        Map<string,string> DeliveryCenterDetailed = new Map<string,string>();
        list<Opportunity> OppSRUpdate = new list<Opportunity>();
        for(Support_Request__c SR:scope){
            if(SR.Alternate_Delivery_Positions_approved__c != null){
                if(DeliveryCenterDetailed.containskey(SR.Opportunity__c)){
                    DeliveryCenterList.add(SR);
                    DeliveryCenterDetailed.put(SR.Opportunity__c,SupportRequestTriggerHandler.GenerateJSON(DeliveryCenterList));
                }else{
                    DeliveryCenterList = new list<Support_Request__c>();
                    DeliveryCenterList.add(SR);
                    DeliveryCenterDetailed.put(SR.Opportunity__c,SupportRequestTriggerHandler.GenerateJSON(DeliveryCenterList));
                }
            }   
        }
        if(!DeliveryCenterDetailed.isEmpty()){
            for(Opportunity OP:[Select Id, Delivery_Center_Detailed__c 
                                from Opportunity 
                                where Id in:DeliveryCenterDetailed.keyset() and recordtype.name = 'Req']){
                if(DeliveryCenterDetailed.containskey(OP.Id)){
                    OP.Delivery_Center_Detailed__c = DeliveryCenterDetailed.get(OP.Id);
                    OppSRUpdate.add(OP);
                }
            }
        }
        try{
            system.debug('--OppSRUpdate size--'+OppSRUpdate.size()+'--OppSRUpdate--'+OppSRUpdate);
            if(!OppSRUpdate.isEmpty())
                database.update(OppSRUpdate,false);
        }catch(exception e){
            system.debug('---exception msg--'+e.getMessage()+'--line number--'+e.getLineNumber());
        }        
    }
    global void finish(Database.BatchableContext context){
        
    }
}