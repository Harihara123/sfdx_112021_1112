@RestResource(urlMapping='/DRZ_ReqOpportunityUpdate/*')
global without sharing class DRZ_ReqOpportunityUpdate{

    @HttpPost
    global static string getDRZ_ReqOpportunityUpdate(){
        DRZ_ImmediateErrorWrapper listDRZ_IR = new DRZ_ImmediateErrorWrapper();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

		//Changing from param to untyped 11/4/2019 - Srini        
        String inputJSON = RestContext.request.requestBody.toString();
        Map<String, Object> r = (Map<String, Object>) JSON.deserializeUntyped(inputJSON);        
        String OpportunityId = (String)r.get('OpportunityId');
        String StageName = (String)r.get('StageName');
        Boolean RedZone = (Boolean)r.get('RedZone');
        
        
        List<String> dataErrorList = new List<String>();
        list<DRZ_ToolingApiforValidation.DRZ_ValidationWrapper> possibleError = new list<DRZ_ToolingApiforValidation.DRZ_ValidationWrapper>();
        list<Opportunity> UpOpp = new list<Opportunity>();
		List<Log__c> errorLogs = new List<Log__c>();
        try{            
            Decimal totalFillLossWash;
            possibleError = DRZ_ToolingApiforValidation.DRZ_ReqOpp_Validations();
            for(Opportunity x: [Select id,StageName,Req_Red_Zone__c,isDRZUpdate__c, Req_Total_Filled__c,Req_Total_Lost__c,Req_Total_Positions__c,Req_Total_Washed__c from Opportunity where id =: OpportunityId]){                    
                if(StageName == 'Closed Won'){
                    totalFillLossWash = ((x.Req_Total_Lost__c==null ? 0 : x.Req_Total_Lost__c) + (x.Req_Total_Washed__c==null ? 0 : x.Req_Total_Washed__c) + (x.Req_Total_Filled__c == null ? 0 : x.Req_Total_Filled__c));
                    if(x.Req_Total_Positions__c != totalFillLossWash){
                        dataErrorList.add('"Immediate Errors":'+'"All the Positions should be Offer Accepted to mark Opportunity as Closed Won"');
                        listDRZ_IR.Success = false;
                        listDRZ_IR.Immediate_Error = 'All the Positions should be Offer Accepted to mark Opportunity as Closed Won';
                        listDRZ_IR.ValidationErrors = null;
                    }else if(x.Req_Total_Positions__c == totalFillLossWash){
                        x.StageName = StageName;
                        x.Req_Red_Zone__c = RedZone;
						x.isDRZUpdate__c = true;
                        UpOpp.add(x);
                    }
                }else if(StageName == 'Closed Won (Partial)'){
                    if(x.Req_Total_Filled__c == null || x.Req_Total_Filled__c <= 0){
                        dataErrorList.add('"Immediate Errors":'+'"At least one of your position needs to be in Offer Accepted status to mark Opportunity as Closed Won (Partial)"');
                        listDRZ_IR.Success = false;
                        listDRZ_IR.Immediate_Error = 'At least one of your position needs to be in Offer Accepted status to mark Opportunity as Closed Won (Partial)';
                        listDRZ_IR.ValidationErrors = null;
                    }else{
                        x.StageName = StageName;
                        x.Req_Red_Zone__c = RedZone;
						x.isDRZUpdate__c = true;
                        UpOpp.add(x);
                    }
                }else if(StageName == 'Closed Wash' || StageName == 'Closed Lost'){
                    if(x.StageName != StageName){
                        dataErrorList.add('"Immediate Errors":'+'"Please loss and/or wash positions from the Position Modal in Connected"');
                        listDRZ_IR.Success = false;
                        listDRZ_IR.Immediate_Error = 'Please loss and/or wash positions from the Position Modal in Connected.';
                        listDRZ_IR.ValidationErrors = null;                        
                    }else if(x.StageName == StageName){
                        x.StageName = StageName;
                        x.Req_Red_Zone__c = RedZone;
						x.isDRZUpdate__c = true;
                        UpOpp.add(x);
                    }
                }else{
                    x.StageName = StageName;
                    x.Req_Red_Zone__c = RedZone;
					x.isDRZUpdate__c = true;
                    UpOpp.add(x);
                }                
            }
            Database.SaveResult[] updatedResults = database.update(UpOpp,false);
            for(Integer i=0;i<updatedResults.size();i++){
                if(!updatedResults.get(i).isSuccess()){
                    Database.Error error = updatedResults.get(i).getErrors().get(0);
                    dataErrorList.add('"Immediate Error":'+'"'+string.valueof(error.getMessage())+'"');
                    listDRZ_IR.Success = false;
                    listDRZ_IR.Immediate_Error = string.valueof(error.getMessage());
                    listDRZ_IR.ValidationErrors = possibleError;
                }
            }
        }catch(Exception e){
			errorLogs.add(Core_Log.logException(e));
        }
        if(!dataErrorList.isEmpty()){
            return JSON.serialize(listDRZ_IR).replaceAll('_',' ');            
        }else{
            listDRZ_IR.Success = true;
            listDRZ_IR.Immediate_Error = '';
            listDRZ_IR.ValidationErrors = null;
            //dataErrorList.add('"Success"'+':'+'"true"'+','+'"Message"'+':'+'"Opportunity updated successfully"');
            return JSON.serialize(listDRZ_IR).replaceAll('_',' ');
        }        
    }
    public class DRZ_ImmediateErrorWrapper{
        public boolean Success;
        public String Immediate_Error;
        public list<DRZ_ToolingApiforValidation.DRZ_ValidationWrapper> ValidationErrors;    
    }
}