@isTest
global class DRZ_ReqOpportunityUpdate_Test implements HttpCalloutMock{
       
    static testMethod void myUnitTest() {
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(
        
        );
        DRZSettings__c setting = new DRZSettings__c();
        setting.Name = 'OpCo';
        setting.value__c = 'Aerotek, Inc;TEKsystems, Inc.';
        insert setting;
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
            Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
            Jobcode_Id__c   ='700009',  OpCo_Id__c='ONS', Segment_Id__c ='A_AND_E', Segment__c = 'Architecture and Engineering',
            OpCo_Status__c = 'A', Skill_Id__c   ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        String StageName;
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 3; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                
                if(i==0){
                    StageName = 'Closed Won';
                }else if(i==1){
                    StageName = 'Closed Won (Partial)';
                }else if(i==2){
                    StageName = 'Closed Wash';
                }else{
                    StageName = 'Draft';
                }
                
                newOpp.StageName = StageName;
                newOpp.Req_Total_Positions__c = 10;
                newOpp.Req_Total_Lost__c = 4;
                newOpp.Req_Total_Washed__c = 4;
                newOpp.Req_Total_Filled__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Aerotek';
                newOpp.Sales_Organization__c = 'HP';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                newOpp.Amount = 1000;
                newOpp.Number_of_Resources__c = 500;
                newOpp.Pricing_Billing_Type__c = 'Fixed Price';
                newOpp.Primary_Service_Type__c ='Biometrics';
                newOpp.Revenue_for_Current_Year_Only__c =2000;
                newOpp.Value_Added_Model__c ='Level 1 ? Selection of Resources';
                newOpp.Account_Prioritization__c = 'A';
                newOpp.Billing_Information__c = 'Billing_Information';
                newOpp.Client_Relationship__c = 'New GS Client';
                newOpp.DeliveryModel__c = 'Advisory';
                newOpp.Description = 'Description Goes...';
                newOpp.Type_of_Service_Period__c = 'Open';
                newOpp.Global_Services_Opportunity_Type__c = 'Open';
                newOpp.Type='New Business';
                newOpp.Global_Services_Opportunity_Type__c='New Business';
                newOpp.Pricing_Approved_By_SSG__c =true;
                newOpp.Project_Length_Duration_Weeks__c=100;
                newOpp.Describe_Customer_Pain__c='Testing';
                newOpp.Desired_Business_Outcomes__c='Testing';
                newOpp.How_will_we_win__c='Testing';
                newOpp.What_is_our_Value_message__c = 'Testing';
                newOpp.Start_Date__c = Date.today();
                newOpp.Services_GP__c = 10;
                newOpp.Document_Request_Date__c = Date.today();
                newOpp.Level_of_Scope_Definition__c = 'Precisely Defined';
                newOpp.Billing_Information__c = 'New York USA';
                newOpp.Delivery_Organization__c = 'Deployment';
                newOpp.Delivery_Readiness__c = 'Early Qualification ? Delivery Not Yet Engaged';
                newOpp.Discipline_Skills_Required__c = 'Product Engineering';
                newOpp.Number_of_Resources__c = 500;
                newOpp.Pricing_Billing_Type__c = 'Fixed Price';
                newOpp.Primary_Service_Type__c ='Biometrics';
                newOpp.Revenue_for_Current_Year_Only__c =2000;
                newOpp.Value_Added_Model__c ='Level 1 ? Selection of Resources';
                newOpp.Account_Prioritization__c = 'A';
                newOpp.Req_Skill_Specialty__c='Communications';
                newOpp.Legacy_Product__c = prd.Id;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        Test.startTest(); 
        OpportunityTeamMember otm = new OpportunityTeamMember (OpportunityId = lstOpportunitys[0].id,UserId = userinfo.getuserid(),TeamMemberRole = 'Recruiter');
        insert otm;
           
        Test.setMock(HttpCalloutMock.class,new DRZ_ReqOpportunityUpdate_Test() );
        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
      /*  req.params.put('OpportunityId', lstOpportunitys[0].id);
        req.params.put('StageName', 'Closed Won');
        req.params.put('RedZone', 'true');           
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated */
        String oppId = (String)lstOpportunitys[0].id;
        String stage = 'Closed Won';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        req.requestURI = '/services/apexrest/DRZ_ReqOpportunityUpdate/all';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        stage='Closed Won (Partial)';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        RestContext.response = res;        
        string results1 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        stage='Closed Wash';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        RestContext.response = res;        
        string results2 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        lstOpportunitys[0].Req_Total_Filled__c = null;
        update lstOpportunitys;
        
        stage='Closed Won (Partial)';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        RestContext.response = res;        
        string results3 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        stage='Closed Won';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');   
        RestContext.response = res;        
        string results4 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();


        stage='Qualified';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');        
        RestContext.response = res;        
        string results5 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();    

        System.assertEquals(results, '{"ValidationErrors":null,"Success":true,"Immediate Error":""}');              
        Test.stopTest();        
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        
      //  System.assertEquals('POST', req.getMethod());
        
        HttpResponse resp = new HTTpResponse();
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        
        return res;
    }
    
     static testMethod void myUnitTest2() {
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(
        
        );
        DRZSettings__c setting = new DRZSettings__c();
        setting.Name = 'OpCo';
        setting.value__c = 'Aerotek, Inc;TEKsystems, Inc.;AG EMEA';
        insert setting;
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings;
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
            Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
            Jobcode_Id__c   ='700009',  OpCo_Id__c='ONS', Segment_Id__c ='A_AND_E', Segment__c = 'Architecture and Engineering',
            OpCo_Status__c = 'A', Skill_Id__c   ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        String StageName;
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 3; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                StageName = 'Draft';                
                newOpp.StageName = StageName;
                newOpp.Req_Total_Positions__c = 10;
                newOpp.Req_Total_Lost__c = 4;
                newOpp.Req_Total_Washed__c = 4;
                newOpp.Req_Total_Filled__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Aerotek';
                newOpp.Sales_Organization__c = 'HP';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                newOpp.Amount = 1000;
                newOpp.Number_of_Resources__c = 500;
                newOpp.Pricing_Billing_Type__c = 'Fixed Price';
                newOpp.Primary_Service_Type__c ='Biometrics';
                newOpp.Revenue_for_Current_Year_Only__c =2000;
                newOpp.Value_Added_Model__c ='Level 1 ? Selection of Resources';
                newOpp.Account_Prioritization__c = 'A';
                newOpp.Billing_Information__c = 'Billing_Information';
                newOpp.Client_Relationship__c = 'New GS Client';
                newOpp.DeliveryModel__c = 'Advisory';
                newOpp.Description = 'Description Goes...';
                newOpp.Type_of_Service_Period__c = 'Open';
                newOpp.Global_Services_Opportunity_Type__c = 'Open';
                newOpp.Type='New Business';
                newOpp.Global_Services_Opportunity_Type__c='New Business';
                newOpp.Pricing_Approved_By_SSG__c =true;
                newOpp.Project_Length_Duration_Weeks__c=100;
                newOpp.Describe_Customer_Pain__c='Testing';
                newOpp.Desired_Business_Outcomes__c='Testing';
                newOpp.How_will_we_win__c='Testing';
                newOpp.What_is_our_Value_message__c = 'Testing';
                newOpp.Start_Date__c = Date.today();
                newOpp.Services_GP__c = 10;
                newOpp.Document_Request_Date__c = Date.today();
                newOpp.Level_of_Scope_Definition__c = 'Precisely Defined';
                newOpp.Billing_Information__c = 'New York USA';
                newOpp.Delivery_Organization__c = 'Deployment';
                newOpp.Delivery_Readiness__c = 'Early Qualification ? Delivery Not Yet Engaged';
                newOpp.Discipline_Skills_Required__c = 'Product Engineering';
                newOpp.Number_of_Resources__c = 500;
                newOpp.Pricing_Billing_Type__c = 'Fixed Price';
                newOpp.Primary_Service_Type__c ='Biometrics';
                newOpp.Revenue_for_Current_Year_Only__c =2000;
                newOpp.Value_Added_Model__c ='Level 1 ? Selection of Resources';
                newOpp.Account_Prioritization__c = 'A';
                newOpp.OpCo__c = 'Aerotek, Inc';
                newOpp.Req_Skill_Specialty__c='Communications';
                newOpp.Legacy_Product__c = prd.Id;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        Test.startTest(); 
        OpportunityTeamMember otm = new OpportunityTeamMember (OpportunityId = lstOpportunitys[0].id,UserId = UserInfo.getUserId(),TeamMemberRole = 'Recruiter');
        insert otm;
           
        Test.setMock(HttpCalloutMock.class,new DRZ_ReqOpportunityUpdate_Test() );
        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
      /*  req.params.put('OpportunityId', lstOpportunitys[0].id);
        req.params.put('StageName', 'Closed Won');
        req.params.put('RedZone', 'true');           
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated */
        String oppId = (String)lstOpportunitys[0].id;
        String stage = 'Closed Won';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        req.requestURI = '/services/apexrest/DRZ_ReqOpportunityUpdate/all';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        stage='Closed Won (Partial)';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        RestContext.response = res;        
        string results1 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        stage='Closed Wash';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        RestContext.response = res;        
        string results2 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        lstOpportunitys[0].Req_Total_Filled__c = null;
        update lstOpportunitys;
        
        stage='Closed Won (Partial)';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');
        RestContext.response = res;        
        string results3 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        
        stage='Closed Won';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');   
        RestContext.response = res;        
        string results4 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();
        DRZEventPublisher.syncToDrz(lstOpportunitys[0].Id);

        stage='Qualified';
        req.requestBody = Blob.valueOf('{"OpportunityId":"'+oppId+'","StageName":"'+stage+'","RedZone":true}');        
        RestContext.response = res;        
        string results5 =  DRZ_ReqOpportunityUpdate.getDRZ_ReqOpportunityUpdate();    

        //System.assertEquals(results, '{"ValidationErrors":null,"Success":true,"Immediate Error":""}');              
        Test.stopTest();        
    }
    
}