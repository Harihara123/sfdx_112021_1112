@RestResource(urlMapping='/DRZ_ReqTeamMemberUpdate/*')
global class DRZ_ReqTeamMemberUpdate {
    @HttpPost
    global static void getDRZ_ReqTeamMemberUpdate(){
        String inputJSON = RestContext.request.requestBody.toString();
        DRZ_ParseReqTeamMembers parsedData = DRZ_ParseReqTeamMembers.parse(inputJSON);
        String TransactionId = parsedData.TransactionId;
        try{
            String oppId = parsedData.OpportunityId;
            Set<Id> oppTeamMemberDeleteIds = new Set<Id>();
            List<OpportunityTeamMember> oppTeamMembersToInsert = new List<OpportunityTeamMember>();
            List<OpportunityTeamMember> oppTeamMembersToUpdate = new List<OpportunityTeamMember>();
            for(DRZ_ParseReqTeamMembers.TeamMembers rtm: parsedData.TeamMembers){
                if(rtm.Operation=='create'){
                    OpportunityTeamMember otm = new OpportunityTeamMember();
                    otm.OpportunityId = oppId;
                    otm.UserId = rtm.UserId;
                    otm.TeamMemberRole = 'Allocated Recruiter';
                    otm.OpportunityAccessLevel = 'Edit';
                    otm.isDRZUpdate__c = True;
                    otm.Source__c='DRZ';
                    oppTeamMembersToInsert.add(otm);
                }
                if(rtm.Operation=='delete'){
                    oppTeamMemberDeleteIds.add(rtm.UserId);
                }
            }
            if(!oppTeamMembersToInsert.isEmpty()){
                try{
                    insert oppTeamMembersToInsert;

                    sendResponse(200,'Success',TransactionId);
                }
                catch(System.DmlException e){
                    sendResponse(400,e.getMessage(),TransactionId);
                }
            }
            if(!oppTeamMemberDeleteIds.isEmpty()){
                try{
                    for(OpportunityTeamMember OTM:[SELECT Id, UserId, OpportunityId,TeamMemberRole,isDRZUpdate__c FROM OpportunityTeamMember WHERE OpportunityId=:oppId AND UserId IN:oppTeamMemberDeleteIds]){
                        OTM.TeamMemberRole = 'Recruiter';
                        OTM.isDRZUpdate__c = True;
                        oppTeamMembersToUpdate.add(OTM);
                    }
                    if(!oppTeamMembersToUpdate.isEmpty()){
                        update oppTeamMembersToUpdate; 
                        sendResponse(200,'Success',TransactionId); 
                    }else if(oppTeamMembersToUpdate.isEmpty()){
                        sendResponse(200,'Success/No records found in Connected',TransactionId); 
                    }
                }
                catch(System.DmlException e){
                    sendResponse(400,e.getMessage(),TransactionId);
                }
            }
        }
        catch(Exception e){
            sendResponse(400,e.getMessage(),TransactionId);
        }
    }
    public static void sendResponse(Integer statusCode, String message, String TransactionId){
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject();
        if(message.substring(0,7) == 'Success'){
            gen.writeStringField('Success','true');
            gen.writeStringField('Error','');
        }else{
            gen.writeStringField('Success','false');
            gen.writeStringField('Error',message);
        }
        gen.writeStringField('TransactionId',TransactionId);
        gen.writeEndObject();
        
        RestContext.response.statusCode = statusCode;
        RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
    }
}