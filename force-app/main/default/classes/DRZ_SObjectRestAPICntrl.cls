@RestResource(urlMapping='/DRZ_SObjectRestAPICntrl/*')
global without sharing class DRZ_SObjectRestAPICntrl{
    
    @HttpGet
    global static string getDRZ_SObjectNameData(){
        list<User> UserList = new list<User>();
        list<Opportunity> OpportunityList = new list<Opportunity>();
        List<List<User>> UserSOSLList = new List<List<User>>();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String SObjectName = RestContext.request.params.get('SObjectName');
        String Value = RestContext.request.params.get('Value');
        try{
            if(SObjectName !=null){
                if(SObjectName == 'User' && Value !=null){
                    if(Value.startsWith('005')){
                        UserList = [Select Id,Department,Division,Email,EmployeeNumber,FirstName,LastName,MediumPhotoUrl,ODS_Application_Role__c,
                                    ODS_Jobcode__c,Office_Code__c,Office__c,ODS_Job_Description__c,OPCO__c 
                                    from User 
                                    where Id =:Value];
                    }
                    if(!Value.startsWith('005')){
                        String searchQuery = 'FIND \'' + Value + '\' IN NAME FIELDS RETURNING User(Id, FirstName, LastName, Email, Office_Code__c, ODS_Application_Role__c, MediumPhotoUrl, Office__c WHERE UserType=\'Standard\' And IsActive = True) LIMIT 50';
                        UserSOSLList = search.query(searchQuery);
                    }
                }
                if(SObjectName == 'Opportunity'){
                    if(Value.startsWith('006')){
                        OpportunityList = [SELECT Id,AccountId,Account.Name,Opportunity_Num__c,Organization_Office__c,Req_Job_Code__c,Req_Can_Use_Approved_Sub_Vendor__c,
                                           Req_Exclusive__c,Start_Date__c,Req_Total_Positions__c,Req_Division__c,Req_Job_Title__c,Req_Red_Zone__c,Req_Segment__c,StageName,
                                           Status__c,LastModifiedDate,LastModifiedById,Req_Interview_Date__c,Immediate_Start__c,Req_Job_Description__c,OwnerId,Owner.Name,
                                           Req_Open_Positions__c,isClosed,CreatedById,CreatedDate,CloseDate,Req_Total_Filled__c,Req_Total_Lost__c,Req_Total_Washed__c,
                                           Req_Hiring_Manager__r.Name,Req_Qualifying_Stage__c 
                                           FROM Opportunity WHERE Id=:Value];
                    }
                }
            }
            system.debug(JSON.serialize(UserSOSLList));
            if(!UserList.isEmpty()){
                return JSON.serialize(UserList);
            }else if(!UserSOSLList.isEmpty()){
                return JSON.serialize(UserSOSLList);
            }else if(!OpportunityList.isEmpty()){
                return JSON.serialize(OpportunityList);
            }else{
                return JSON.serialize('No data found');
            }
        }catch(exception ex){
            return JSON.serialize(ex);
        }
    }
    
    @HttpPost
    global static string InsertorUpdateSObjectName(){
        string res;
        String inputJSON = RestContext.request.requestBody.toString();
        system.debug('--inputJSON--'+inputJSON);
        JsonParser pData = parse(inputJSON);
        system.debug('--pData--'+pData);
        try{
            if(pData !=null){
                if(pData.ActionType == 'Add'){
                    If(pData.SObjectName == 'FeedItem' && pData.Value !=null && pData.Value.startswith('006') && pData.Body !=null){
                        string x = string.valueof(ChatterApiHelper.postFeedItemWithRichText(null,pData.Value,pData.Body));
                        res = x.substringAfter('elements/').substringBefore('/capabilities');
                    }else If(pData.SObjectName == 'FeedComment' && pData.Value !=null && pData.Value.startswith('0D5') && pData.Body !=null){
                        string x = string.valueof(ChatterApiHelper.postCommentWithMentions(null,pData.Value,pData.Body));
                        res = x.substringAfter('comments/').substringBefore('/capabilities');
                    }else{
                        res = 'No record found';
                    }
                }else if(pData.ActionType == 'Update'){
                    If(pData.SObjectName == 'FeedItem' && pData.Value !=null && pData.Value.startswith('0D5') && pData.Body !=null){
                        ChatterApiHelper.updateFeedItemWithRichText(null,pData.Value,pData.Body);
                        res = 'Success';
                    }else If(pData.SObjectName == 'FeedComment' && pData.Value !=null && pData.Value.startswith('0D7') && pData.Body !=null){
                        ChatterApiHelper.updateCommentWithMentions(null,pData.Value,pData.Body);
                        res = 'Success';
                    }else If(pData.SObjectName == 'Opportunity' && pData.Value !=null && pData.Value.startswith('006') && pData.Body !=null){
                        list<Opportunity> OpptyExclusiveUpdate = [Select Id,Req_Exclusive__c from Opportunity where Id=:pData.Value];
                        if(pData.Body == 'True' || pData.Body == 'False'){
                            OpptyExclusiveUpdate[0].Req_Exclusive__c = boolean.valueof(pData.Body);
                        }
                        if(!OpptyExclusiveUpdate.isEmpty()){
                            update OpptyExclusiveUpdate;
                            res = 'Success';
                        }else{
                            system.debug('--no4--');
                            res = 'No record found';
                        }
                    }else{
                        res = 'No record found';
                    }
                }else if(pData.ActionType == 'Delete'){
                    Integer FeedCmtCount = 0;
                    if(pData.SObjectName == 'FeedItem' && pData.Value !=null && pData.Value.startsWith('0D5')){//FeedItem Id
                        list<FeedItem> FeedList = [Select Id,(Select Id from FeedComments) from FeedItem where Id=:pData.Value];
                        if(!FeedList.isEmpty()){
                            for(FeedItem f:FeedList){
                                FeedCmtCount += f.FeedComments.size();
                            }
                            if(FeedCmtCount !=0){
                                res = 'Feed Item cannot be deleted as a feed comment exists';
                            }else{
                                delete FeedList;
                                res = 'Feed Item deleted successfully';
                            }
                        }
                    }else if(pData.SObjectName == 'FeedComment' && pData.Value !=null && pData.Value.startsWith('0D7')){//FeedComment Id
                        list<FeedComment> FeedCmtList = [Select Id from FeedComment where Id=:pData.Value];
                        if(!FeedCmtList.isEmpty()){
                            delete FeedCmtList;
                            res = 'Feed Comment deleted successfully';
                        }
                    }
                }else{
                    res = 'Response body not correct';
                }
            }else{
                res = 'Response body not correct';
            }
        }catch(exception ex){
            res = ex.getMessage();
        }
        system.debug('--res--'+res);
        return res;
    }
    public class JsonParser{
        string SObjectName{get;set;}
        string ActionType{get;set;}
        string Value{get;set;}
        string Body{get;set;}
    }
    public static JsonParser parse(String json){
        return (JsonParser) System.JSON.deserialize(json, JsonParser.class);
    }
}