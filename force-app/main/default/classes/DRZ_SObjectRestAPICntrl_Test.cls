@isTest
global class DRZ_SObjectRestAPICntrl_Test implements HttpCalloutMock{
    static testmethod void getDRZ_SObjectNameData_Test1(){
        User user = TestDataHelper.createUser('System Integration');
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl?';
        req.addParameter('SObjectName', 'User');
        req.addParameter('Value', 'User');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        System.debug(req);
        RestContext.request = req;
        RestContext.response= res;        
        DRZ_SObjectRestAPICntrl.getDRZ_SObjectNameData();
        Test.stopTest();
    }
    static testmethod void getDRZ_SObjectNameData_Test2(){
        List<User> user = TestDataHelper.createUsers(1,'System Integration');
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl?';
        req.addParameter('SObjectName', 'User');
        req.addParameter('Value', user[0].Id);
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        System.debug(req);
        RestContext.request = req;
        RestContext.response= res;        
        DRZ_SObjectRestAPICntrl.getDRZ_SObjectNameData();
        Test.stopTest();
    }
    static testmethod void getDRZ_SObjectNameData_Test3(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl?';
        req.addParameter('SObjectName', 'Opportunity');
        req.addParameter('Value', TestOpp.Id);
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        System.debug(req);
        RestContext.request = req;
        RestContext.response= res;        
        DRZ_SObjectRestAPICntrl.getDRZ_SObjectNameData();
        Test.stopTest();
    }
    static testMethod void InsertorUpdateSObjectName1(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"FeedItem","ActionType":"Add","Value":"'+TestOpp.Id+'","Body":"Hi Test Feed"}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    static testMethod void InsertorUpdateSObjectName2(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        FeedItem f = new FeedItem();
        f.Body = 'Test Feed';
        f.ParentId = TestOpp.Id;
        insert f;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"FeedComment","ActionType":"Add","Value":"'+f.Id+'","Body":"Hi Test Feed Cmt"}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    static testMethod void InsertorUpdateSObjectName3(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        FeedItem f = new FeedItem();
        f.Body = 'Test Feed';
        f.ParentId = TestOpp.Id;
        insert f;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"FeedItem","ActionType":"Update","Value":"'+f.Id+'","Body":"Hi Test Feed Update"}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    static testMethod void InsertorUpdateSObjectName4(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        FeedItem f = new FeedItem();
        f.Body = 'Test Feed';
        f.ParentId = TestOpp.Id;
        insert f;
        FeedComment fc = new FeedComment();
        fc.CommentBody = 'Test FC';
        fc.FeedItemId = f.Id;
        insert fc;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"FeedComment","ActionType":"Update","Value":"'+fc.Id+'","Body":"Hi Test Cmt Feed"}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    static testMethod void InsertorUpdateSObjectName5(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        FeedItem f = new FeedItem();
        f.Body = 'Test Feed';
        f.ParentId = TestOpp.Id;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"Opportunity","ActionType":"Update","Value":"'+TestOpp.Id+'","Body":"True"}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    static testMethod void InsertorUpdateSObjectName6(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        FeedItem f = new FeedItem();
        f.Body = 'Test Feed';
        f.ParentId = TestOpp.Id;
        insert f;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"FeedItem","ActionType":"Delete","Value":"'+f.Id+'","Body":""}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    static testMethod void InsertorUpdateSObjectName7(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        FeedItem f = new FeedItem();
        f.Body = 'Test Feed';
        f.ParentId = TestOpp.Id;
        insert f;
        FeedComment fc = new FeedComment();
        fc.CommentBody = 'Test FC';
        fc.FeedItemId = f.Id;
        insert fc;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"FeedComment","ActionType":"Delete","Value":"'+fc.Id+'","Body":""}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    static testMethod void InsertorUpdateSObjectName8(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;        
        DRZSettings__c DRZSettings3 = new DRZSettings__c(Name = 'Office',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG EMEA;Allegis Group Limited UK');
        insert DRZSettings3;
        //User user = TestDataHelper.createUser('System Integration');
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        FeedItem f = new FeedItem();
        f.Body = 'Test Feed';
        f.ParentId = TestOpp.Id;
        insert f;
        FeedComment fc = new FeedComment();
        fc.CommentBody = 'Test FC';
        fc.FeedItemId = f.Id;
        insert fc;
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class,new DRZ_SObjectRestAPICntrl_Test());        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();       
        req.httpMethod = 'POST';        
        req.requestBody = Blob.valueOf('{"SObjectName":"FeedComment","ActionType":"Delete","Value":"'+f.Id+'","Body":""}');
        req.requestURI = '/services/apexrest/DRZ_SObjectRestAPICntrl';  
        RestContext.request = req;
        RestContext.response = res;        
        string results = DRZ_SObjectRestAPICntrl.InsertorUpdateSObjectName();
        Test.stopTest();  
    }
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HTTpResponse();
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        return res;
    }
}