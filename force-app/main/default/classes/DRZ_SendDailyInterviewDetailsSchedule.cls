global class DRZ_SendDailyInterviewDetailsSchedule implements Schedulable{
    global void execute(SchedulableContext sc){        
		DRZ_SendDailyInterviewDetailsBatch batchJob = new DRZ_SendDailyInterviewDetailsBatch();
        database.executebatch(batchJob);
	}
}