@RestResource(urlMapping='/DRZ_ReqOrderUpdate/*')
global without sharing class  DRZ_SubmittalStatusMessages{
    global class SubmittalResponse {
        public Boolean Success;
        public String Immediate_Error;
        public list<String> ValidationErrors;

        public SubmittalResponse(Boolean theSuccess, String theMessage) {
            this.Success = theSuccess;
            this.Immediate_Error = theMessage;
            this.ValidationErrors = null;
        }
    }
    
    @HttpPost
    global static String complianceErrorCheck() {
        String theMessage = '';
        Boolean theSuccess = true;
        String oldVal = '';
        String theType = 'immediate error'; // Hardcoded since only one type of error for submittal
        
        //Changing from param to untyped 11/4/2019 - Srini        
        String inputJSON = RestContext.request.requestBody.toString();
        Map<String, Object> r = (Map<String, Object>) JSON.deserializeUntyped(inputJSON);
        String submittalID = (String)r.get('OrderId');
        String newVal = (String)r.get('Status');
        system.debug('@#@#@#'+submittalID);
        system.debug('@#@#@#'+newVal);
        // get submittal id from DB if found proceed else Error message
        List<Order> orderToUpdate = [Select id, Status, Opportunity.Req_Open_Positions__c from Order where Id =: submittalID];
        
        if(orderToUpdate.size() > 0){
            oldVal = orderToUpdate[0].Status; 
            try{
                // decide if you want to update submittal or not
                Boolean shouldUpdateSubmittal = false;
                For(drz_Submittal_Status_settings__mdt submittal_Status : [select id, Skip_Validation__c, Old_submittal_Status__c, New_submittal_Status__c, Message__c, isActive__c from drz_Submittal_Status_settings__mdt where isActive__c =: true]){
                    if(submittal_status.Old_submittal_Status__c == oldVal && submittal_status.New_submittal_Status__c == newVal && submittal_Status.Skip_Validation__c == false){
                        theSuccess = false;
                        theMessage = submittal_status.Message__c;
                    }
                    if(submittal_status.Old_submittal_Status__c == oldVal && submittal_status.New_submittal_Status__c == newVal && submittal_Status.Skip_Validation__c == true){
                        theMessage = '';
                        shouldUpdateSubmittal = true;
                        if(orderToUpdate[0].Opportunity.Req_Open_Positions__c == 0 && newVal == 'Offer Accepted'){
                            theMessage = 'Please ensure you click the option to increase positions on the req opportunity in Connected.';
                            theSuccess = false;
                            shouldUpdateSubmittal = false;
                        }
                    }
                }
                if(shouldUpdateSubmittal){
                    orderToUpdate[0].Status = newVal;
                    orderToUpdate[0].isDRZUpdate__c = true;
                    update orderToUpdate;
                }else{
                    if(newVal == 'Started'){
                        theSuccess = false;
                        theMessage = 'Connected not updated since talent status is not Offer Accepted.'; 
                    }
                }
                
             }
             catch(Exception ex){
                 theSuccess = false;
                 theMessage = String.valueOf(ex.getMessage()); 
             }   
        }else{
            theSuccess = false;
            theMessage = 'No record found in connected for this submittal.';
        }
        SubmittalResponse theResponse = new SubmittalResponse(theSuccess, theMessage);
        return JSON.serialize(theResponse).replaceAll('_', ' ');
    }
  
  
}