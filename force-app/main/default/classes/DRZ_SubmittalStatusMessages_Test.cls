@isTest
public class DRZ_SubmittalStatusMessages_Test{
    public static void createDummyData(){
		insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.');
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
		insert new OrderDeletePlatformEvent__c(Name='OrderDeletePlatformEvent',Disable__c = True);
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes);
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = lstNewAccounts[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp1;

        Opportunity opp2 = new Opportunity();
        opp2.Name = 'TESTOPP2';
        opp2.StageName = 'Offer Accepted';
        opp2.AccountId = lstNewAccounts[0].id;
        opp2.Req_Product__c = 'Permanent';
        opp2.CloseDate = System.today();
        opp2.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp2;
        Contact ct = TestData.newContact(lstNewAccounts[0].id, 1, 'Talent');  
         
        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;

        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = lstNewAccounts[0].id;
        o.ShipToContactId = ct.Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Linked';
        o.EffectiveDate = System.today();
        o.RecordTypeId = recTypeId;
        o.isDRZUpdate__c = true;
        try{
            insert o;
        }
        catch(DMLException e){
            System.Debug('Order Creation Error '+e);
        }

        Order o2 = new Order();
        o2.Name = 'TestOrder';
        o2.AccountId = lstNewAccounts[0].id;
        o2.ShipToContactId = ct.Id;
        o2.OpportunityId = opp2.Id;
        o2.Status = 'Linked';
        o2.EffectiveDate = System.today();
        o2.RecordTypeId = recTypeId;
        o2.isDRZUpdate__c = true;
        try{
            insert o2;
        }
        catch(DMLException e){
            System.Debug('Order Creation Error '+e);
        }
        
        list<drz_Submittal_Status_settings__mdt> statusData = new list<drz_Submittal_Status_settings__mdt>();
        statusData = [select id, Skip_Validation__c, Old_submittal_Status__c, New_submittal_Status__c, Message__c, isActive__c from drz_Submittal_Status_settings__mdt where isActive__c =: true];
    }
    
    public static testMethod void NotProceedingTest(){
        DRZ_SubmittalStatusMessages_Test.createDummyData();
        Order newOrder = [Select id from Order LIMIT 1];
        Test.startTest();
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();
    //    req.params.put('submittalId', newOrder.id);
    //    req.params.put('newVal', 'Not Proceeding');
        req.requestBody = Blob.valueOf('{"OrderId":"'+newOrder.Id+'","Status":"Not Proceeding"}');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        req.requestURI = '/services/apexrest/DRZ_ReqOrderUpdate/';
        RestContext.request = req;
        RestContext.response = res;
        string results = DRZ_SubmittalStatusMessages.complianceErrorCheck();
       // System.assertEquals(results, '{"ValidationErrors":"","Success":false,"Immediate Errors":"No record found in connected for this submittal."}');
        Test.stopTest();
    }
    
    public static testMethod void OfferAcceptedTest(){
        DRZ_SubmittalStatusMessages_Test.createDummyData();
        Order newOrder = [Select id from Order LIMIT 1];
        Test.startTest();
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();
      //  req.params.put('submittalId', newOrder.id);
      //  req.params.put('newVal', 'Offer Accepted');
      	req.requestBody = Blob.valueOf('{"OrderId":"'+newOrder.Id+'","Status":"Offer Accepted"}');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        req.requestURI = '/services/apexrest/DRZ_ReqOrderUpdate/';
        RestContext.request = req;
        RestContext.response = res;
        string results = DRZ_SubmittalStatusMessages.complianceErrorCheck();
       // System.assertEquals(results, '{"ValidationErrors":"","Success":false,"Immediate Errors":"No record found in connected for this submittal."}');
        Test.stopTest();
    }
    
    public static testMethod void NoRecordFoundTest(){
        DRZ_SubmittalStatusMessages_Test.createDummyData();
        Order newOrder = [Select id from Order LIMIT 1];
        Test.startTest();
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();
     //   req.params.put('submittalId', 'qweqwe12ddasdx');
     //   req.params.put('newVal', 'Offer Accepted');
        req.requestBody = Blob.valueOf('{"OrderId":"qweqwe12ddasdx","Status":"Offer Accepted"}');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        req.requestURI = '/services/apexrest/DRZ_ReqOrderUpdate/';
        RestContext.request = req;
        RestContext.response = res;
        string results = DRZ_SubmittalStatusMessages.complianceErrorCheck();
       // System.assertEquals(results, '{"ValidationErrors":"","Success":false,"Immediate Errors":"No record found in connected for this submittal."}');
        Test.stopTest();
    }
    
    public static testMethod void NoPositionTest(){
        DRZ_SubmittalStatusMessages_Test.createDummyData();
        List<Order> newOrder = [Select id from Order LIMIT 2];
        Test.startTest();
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();
       // req.params.put('submittalId', newOrder[0].id);
       // req.params.put('newVal', 'Offer Accepted');
        
       // req.params.put('OrderId', newOrder[0].id);
       // req.params.put('Status', 'Offer Accepted');
        
        req.requestBody = Blob.valueOf('{"OrderId":"'+newOrder[0].Id+'","Status":"Offer Accepted"}');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        req.requestURI = '/services/apexrest/DRZ_ReqOrderUpdate/';
        RestContext.request = req;
        RestContext.response = res;
        string results = DRZ_SubmittalStatusMessages.complianceErrorCheck();
 
       // req.params.put('submittalId', newOrder[1].id);
       // req.params.put('newVal', 'Offer Accepted');
		newOrder[0].Status = 'Offer Accepted';
        update newOrder[0];
        req.requestBody = Blob.valueOf('{"OrderId":"'+newOrder[0].Id+'","Status":"Offer Accepted"}');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        req.requestURI = '/services/apexrest/DRZ_ReqOrderUpdate/';
        RestContext.request = req;
        RestContext.response = res;
        string results2 = DRZ_SubmittalStatusMessages.complianceErrorCheck();
      
      
        Test.stopTest();
    }
}