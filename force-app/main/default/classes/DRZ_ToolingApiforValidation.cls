Public class DRZ_ToolingApiforValidation{
    
    public static list<DRZ_ValidationWrapper> DRZ_ReqOpp_Validations(){
        string valNames = system.label.DRZ_Req_Opportunity_Validations;
        list<DRZ_ValidationWrapper> listDRZ_V = new list<DRZ_ValidationWrapper>();
		List<Log__c> errorLogs = new List<Log__c>();
        try{
            HttpRequest req = new HttpRequest();
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
            req.setHeader('Content-Type', 'application/json');
            String domainUrl=URL.getSalesforceBaseUrl().toExternalForm();
            req.setEndpoint(domainUrl+'/services/data/v45.0/tooling/query?q=Select+Id,Active,ValidationName,+ErrorMessage+From+ValidationRule+where+EntityDefinition.DeveloperName+=\''+'Opportunity'+'\'and+active+=+true+and+ValidationName+IN+('+valNames+')');
            req.setMethod('GET');
            Http h = new Http();
            HttpResponse res = h.send(req);
            String returnValuestr = res.getBody();            
            Map<String, Object> jSonDes = (Map<String, Object>) JSON.deserializeUntyped(returnValuestr);
            List<Map<String, Object>> ValidRecord = new List<Map<String, Object>>();
            for (Object instance : (List<Object>)jSonDes.get('records')){
                ValidRecord.add((Map<String, Object>)instance);
            }            
            for(Map<String, Object> ErrorDescp : ValidRecord){
                DRZ_ValidationWrapper DRZ_V = new DRZ_ValidationWrapper();
                DRZ_V.Possible_Errors = string.valueof(ErrorDescp.get('ErrorMessage'));
                listDRZ_V.add(DRZ_V);
            }            
        }catch(Exception ex){
            errorLogs.add(Core_Log.logException(ex));
            return null;
        }
        return listDRZ_V;
        //return JSON.serialize(listDRZ_V).replaceAll('Possible_Errors','Possible Errors');
    }
    public class DRZ_ValidationWrapper{
        public String Possible_Errors;
    }
}