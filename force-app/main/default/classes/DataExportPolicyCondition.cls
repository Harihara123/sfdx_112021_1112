global class DataExportPolicyCondition implements TxnSecurity.EventCondition {
// Condition for Diffrent kind of Events can be added bellow
    public boolean evaluate(SObject event) {
        switch on event {
			//For calling the Report Events
            when ReportEvent reportEvent {
                return evaluateReport(reportEvent);
            }
			//For calling the API Events
            when ApiEvent apiEvent {                
                return evaluateApi(apiEvent);
            }
			//Handling NULL
            when null {
                return false;
            } 
			//Any event other than Report or API
            when else {
                return false;
            }
        }
    }  
	//Currently we are handling for System Admin, Integration User and IS Admin - Apart from these we will get Notification Email
	//To add / remove diffrent kind of users please check "TransactionSecurityLoginPolicy" class
	//Event for API Type   
    public boolean evaluateApi(ApiEvent apiEvent){
        if (!TransactionSecurityLoginPolicy.verifyProfile(apiEvent.UserId)){
            return false;
        }
         else if(apiEvent.apiType.contains('Bulk')){
            return true;
        }
        return false;
    }  
	//Event for Report Type      
    public boolean evaluateReport(ReportEvent reportEvent){
        if (!TransactionSecurityLoginPolicy.verifyProfile(reportEvent.UserId)){
            return false;
        }
         else if(reportEvent.Operation.contains('ReportExported')){
            return true;
        }
        return false;
    }
}