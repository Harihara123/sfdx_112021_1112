@isTest
public class DataExportPolicyConditionTest {
    // Test Case 1 for Api Event with default settings
    static testMethod void testApiEventPositiveTestCase1() {
          // set up our event and its field values          
          ApiEvent testEvent = new ApiEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
		  testEvent.apiType = 'Bulk API';
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    // Test Case 2 for Report Event with difault settings
    static testMethod void testApiEventPositiveTestCase2() {
          // set up our event and its field values
          ReportEvent testEvent = new ReportEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent); 
      }
      // for checking if its an Integration User Report event
    static testMethod void testApiEventPositiveTestCase3() {
	  User u2 =TestDataHelper.createUser('System Integration');
       insert u2;
      System.runAs(u2) {
          // set up our event and its field values
          ReportEvent testEvent = new ReportEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    }
      // for checking if its an Integration User For APIEvent
    static testMethod void testApiEventPositiveTestCase4() {
	  User u2 =TestDataHelper.createUser('System Integration');
        insert u2;
      System.runAs(u2) {
          // set up our event and its field values
          ApiEvent testEvent = new ApiEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
		  testEvent.apiType = 'Bulk API';
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    }
    
    // To cover if it is not an Is Admin, Integration user or System Admin  
    static testMethod void testApiEventPositiveTestCase5() {
	  User u2 =TestDataHelper.createUser('Standard User');
      System.runAs(u2) {
          // set up our event and its field values
          ApiEvent testEvent = new ApiEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 100;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
          testEvent.apiType = 'Bulk API';
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    }
    // To cover if it is not an Is Admin, Integration user or System Admin  
    static testMethod void testApiEventPositiveTestCase6() {
	  User u2 =TestDataHelper.createUser('Standard User');
      System.runAs(u2) {
          // set up our event and its field values
          ReportEvent testEvent = new ReportEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 100;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
          testEvent.Operation = 'ReportExported';
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    }
    // To Cover Event which is not Report or API
    static testMethod void testApiEventPositiveTestCase7() {
          // set up our event and its field values
          listViewEvent testEvent = new listViewEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 100;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);      
    }
    // for checking if its an IS Admin User Report Event
    static testMethod void testApiEventPositiveTestCase8() {
	  User u2 =TestDataHelper.createUser('IS Admin');
      System.runAs(u2) {
          // set up our event and its field values
          ReportEvent testEvent = new ReportEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    }
      // for checking if its an IS Admin User For APIEvent
    static testMethod void testApiEventPositiveTestCase9() {
	  User u2 =TestDataHelper.createUser('IS Admin');
      System.runAs(u2) {
          // set up our event and its field values
          ApiEvent testEvent = new ApiEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
		  testEvent.apiType = 'Bulk API';
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    }
    // To cover if it is not an Integration user or System Admin  
    static testMethod void testApiEventPositiveTestCase10() {
	  User u2 =TestDataHelper.createUser('Standard User');
      System.runAs(u2) {
          // set up our event and its field values
          ApiEvent testEvent = new ApiEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
          testEvent.apiType = 'REST API';
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);		  
      }
    }
	// for checking if its an IS Admin User Report Event
    static testMethod void testApiEventPositiveTestCase11() {
	  User u2 =TestDataHelper.createUser('Standard User');
      System.runAs(u2) {
          // set up our event and its field values
          ReportEvent testEvent = new ReportEvent();
          testEvent.QueriedEntities = 'Account, Contact, Opportunity';
          testEvent.RowsProcessed = 2001;
          testEvent.UserId = UserInfo.getUserId();
          testEvent.UserName = UserInfo.getUserName();
		  testEvent.Operation = 'ReportPreviewed';
          // test that the Apex returns true for this event
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent);
      }
    }
	  
    static testMethod void testApiEventNegetiveTestCase1() {
          // set up our event and its field values
          //For Null Check
          // test that the Apex returns true for this event
		  listViewEvent testEvent;
          DataExportPolicyCondition  eventCondition = new DataExportPolicyCondition();
          eventCondition.evaluate(testEvent); 
      }
}