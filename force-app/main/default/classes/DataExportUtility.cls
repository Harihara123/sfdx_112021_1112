public class DataExportUtility  {

    public static final String[] accountIgnoreFields = new List<String>{'Talent_HRXML__c', 'Talent_HRXML2__c', 'Talent_HRXML3__c'};
    public static final String[] contactIgnoreFields = new List<String>{};
    public static final String[] activityIgnoreFields = new List<String>{};
    public static final String[] documentIgnoreFields = new List<String>{};
    public static final String[] orderIgnoreFields = new List<String>{};
    public static final String[] talentDocIgnoreFields = new List<String>{};
    public static final String[] attachmentIgnoreFields = new List<String>{};
    public static final String[] opportunityIgnoreFields = new List<String>{};
    public static final String[] talentExperienceIgnoreFields = new List<String>{};
    public static final String[] talentWorkHistoryIgnoreFields = new List<String>{};
    public static final String[] reqIgnoreFields = new List<String>{};
    public static final String[] userIgnoreFields = new List<String>{'lastpasswordchangedate', 'owner_to_transfer__c', 'owner_to_transf', 'ambition__isambitionuser__c'};
    public static final String[] userActivityIgnoreFields = new List<String>{};
    public static final String[] userSearchFeedbackIgnoreFields = new List<String>{};

    public static final String[] userOrganizationIgnoreFields = new List<String>{};

    public static final String[] contactTagIgnoreFields = new List<String>{};
    public static final String[] definationTagIgnoreFields = new List<String>{};
	public static final String[] jobPostingIgnoreFields = new List<String>{};
    public static final String[] pipelineIgnoreFields = new List<String>{};
	public static final String[] jobTitleIgnoreFields = new List<String>{};

    public static final String[] opportunityContactRoleIgnoreFields = new List<String>{};
    public static final String[] searchAlertCriteriaIgnoreFields = new List<String>{};

    public static final String[] noFieldCommonIgnoreFields = new List<String>{}; 

    public static final String[] utmIgnoreFields = new List<String>{};

    public Static final String ATS_DATS_MIGRATION = 'ATS Data Migration';   
    public Static final String TALENT_INTEGERATION = 'Talent Integration';

    public static final Map<String, String[]> ObjectMap = new Map<String, String[]>();

    Static 
            {
               ObjectMap.put('Account', accountIgnoreFields);
               ObjectMap.put('Contact', contactIgnoreFields);
               ObjectMap.put('Event', activityIgnoreFields);
               ObjectMap.put('Task', activityIgnoreFields);
               ObjectMap.put('Order', orderIgnoreFields);
               ObjectMap.put('Reqs__c', reqIgnoreFields);
               ObjectMap.put('Opportunity', opportunityIgnoreFields);
               ObjectMap.put('Talent_Experience__c', talentExperienceIgnoreFields);
               ObjectMap.put('Talent_Work_History__c', talentWorkHistoryIgnoreFields);
               ObjectMap.put('User', userIgnoreFields); 
               ObjectMap.put('Talent_Document__c', talentDocIgnoreFields); 
               ObjectMap.put('User_Activity__c', userActivityIgnoreFields); 
			   ObjectMap.put('User_Search_Feedback__c', userSearchFeedbackIgnoreFields);
               ObjectMap.put('Tag_Definition__c', definationTagIgnoreFields);
               ObjectMap.put('Contact_Tag__c', contactTagIgnoreFields);
               ObjectMap.put('User_Organization__c', userOrganizationIgnoreFields);
			   ObjectMap.put('Job_Posting__c', jobPostingIgnoreFields);
			   ObjectMap.put('Pipeline__c', pipelineIgnoreFields);
			   ObjectMap.put('Job_Title__c', jobTitleIgnoreFields);
               ObjectMap.put('OpportunityContactRole', opportunityContactRoleIgnoreFields);
               ObjectMap.put('Search_Alert_Criteria__c', searchAlertCriteriaIgnoreFields);
               ObjectMap.put('Careersite_Application_Details__c', noFieldCommonIgnoreFields);
               ObjectMap.put('Talent_Recommendation__c', noFieldCommonIgnoreFields);
               ObjectMap.put('Global_LOV__c', noFieldCommonIgnoreFields);
               ObjectMap.put('TalentFitment__c', noFieldCommonIgnoreFields);
			   ObjectMap.put('ApplicantPriorityTracking__c', noFieldCommonIgnoreFields);
			   ObjectMap.put('OpportunityTeamMember', noFieldCommonIgnoreFields);
			   ObjectMap.put('Unified_Merge__c', utmIgnoreFields);
               ObjectMap.put('Req_Router_Action__c', noFieldCommonIgnoreFields);
               ObjectMap.put('Support_Request__c', noFieldCommonIgnoreFields);
               ObjectMap.put('Case', noFieldCommonIgnoreFields);
            }

   }