global class DataExtractionEAP_Cleanup_Batch implements Database.Batchable<SObject> {
	
	global DataExtractionEAP_Cleanup_Batch() { }

	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id FROM Data_Extraction_EAP__c where isDeleted = true ALL ROWS '); 
	}

   	global void execute(Database.BatchableContext context, List<Data_Extraction_EAP__c> scope) {  
		System.debug('size:'+  scope.size());
        DataBase.emptyRecycleBin(scope);       	 
	}
	
	global void finish(Database.BatchableContext BC) {
	   AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       String subject = 'Clean Up of Data Extraction EAP from Recycle bin ';       
       mail.setSubject(subject + a.Status);
       string s = 'The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures';
       mail.setPlainTextBody(s);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}