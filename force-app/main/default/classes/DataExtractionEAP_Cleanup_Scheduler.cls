global class DataExtractionEAP_Cleanup_Scheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		DataExtractionEAP_Cleanup_Batch ua = new DataExtractionEAP_Cleanup_Batch(); 
        database.executeBatch(ua);		
	}
}