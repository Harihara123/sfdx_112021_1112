@isTest 
private class DataExtractionEAP_Cleanup_Test {
	
	static testmethod void scheduleDeleteDataExtractionEAP(){ 
        Test.startTest(); 
        DataExtractionEAP_Cleanup_Scheduler dataExtractionEAP = new DataExtractionEAP_Cleanup_Scheduler();
        String sch = '23 30 8 10 2 ?';
        system.schedule('DataExtractionEAP_Cleanup_Scheduler test class', sch, dataExtractionEAP);
        Test.stopTest();        
    }
}