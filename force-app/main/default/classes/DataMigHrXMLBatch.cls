// *********************************************************************
// Execute Anonynous code
// Make sure to run as Siebel Integration user
/*
string query ='select id,Req_HRXML_Status_Flag__c from opportunity where recordtype.name=\'req\'------------';
Id batchJobId = Database.executeBatch(new DataMigHrXMLBatch(query));
*/
// ************************************************************************
global class DataMigHrXMLBatch implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
    global DateTime dtLastBatchRunTime;
    public DateTime newBatchRunTime;
    Public string Queryopp;  
    
    global DataMigHrXMLBatch(Datetime lastBatchRunTime)
    {
        Datetime latestBatchRunTime = System.now();
           String strJobName = 'BatchLegacyReqLastRunTime';
           //Get the ProcessLegacyReqs Custom Setting
           ProcessLegacyReqs__c p = ProcessLegacyReqs__c.getValues(strJobName);
             
           //Store the Last time the Batch was Run in a variable
           dtLastBatchRunTime = lastBatchRunTime; //p.LastExecutedBatchTime__c;
           
           newBatchRunTime = latestBatchRunTime;
           
           
           Queryopp = System.Label.DataMigHRXMLInsertQuery;
    }
        
     global database.QueryLocator start(Database.BatchableContext BC)  
     {  
         system.debug('Inside start method');
         System.debug('dtLastBatchRunTime DataMigHrXMLBatch' + dtLastBatchRunTime);
           System.debug('newBatchRunTime DataMigHrXMLBatch' + newBatchRunTime);
        //Create DataSet of Accounts to Batch
        return Database.getQueryLocator(Queryopp);
     } 

        
        global void execute(Database.BatchableContext BC, List<sObject> scope)
     {
       
         set<id> oppids=new set<id>();
       Map<Id, Opportunity> opportunityMap = new map<id,Opportunity>();
         
         
        List<Opportunity> lstQueriedopps = (List<Opportunity>)scope;
         for(Opportunity oplst:lstQueriedopps){
             oppids.add(oplst.id);
             opportunitymap.put(oplst.id,oplst);
             
                    }
         system.debug('oppmap'+opportunitymap);
         system.debug('keyset'+opportunityMap.keyset());
         
         /* HRXMLUtil hr = new HRXMLUtil(opportunityMap.keyset());
          hr.updateOpportunitiesWithHRXML();*/
          
          HRXMLUtil hr = new HRXMLUtil(opportunityMap.keyset());
          hr.getGeoLocationFromAPI();
          hr.updateOpportunitiesWithHRXML();
         
        
     }             
        global void finish(Database.BatchableContext BC)
     {
         
          
     }
    
}