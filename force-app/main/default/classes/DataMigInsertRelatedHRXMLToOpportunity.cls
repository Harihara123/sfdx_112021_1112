// *********************************************************************
// Execute Anonynous code
// Make sure to run as Siebel Integration user
/*

Id batchJobId = Database.executeBatch(new DataMigHrXMLBatch());
*/
// ************************************************************************
global class DataMigInsertRelatedHRXMLToOpportunity implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
    global DateTime dtLastBatchRunTime;
    public DateTime newBatchRunTime;
    Public string Queryopp;  
    
    global DataMigInsertRelatedHRXMLToOpportunity(Datetime lastBatchRunTime)
    {
          Datetime latestBatchRunTime = System.now();
           String strJobName = 'BatchLegacyReqLastRunTime';
           //Get the ProcessLegacyReqs Custom Setting
           ProcessLegacyReqs__c p = ProcessLegacyReqs__c.getValues(strJobName);
             
           //Store the Last time the Batch was Run in a variable
           dtLastBatchRunTime = lastBatchRunTime; //p.LastExecutedBatchTime__c;
           
           newBatchRunTime = latestBatchRunTime;
           
           System.debug('dtLastBatchRunTime DataMigInsertRelatedHRXMLToOpportunity' + dtLastBatchRunTime);
           System.debug('newBatchRunTime DataMigInsertRelatedHRXMLToOpportunity' + newBatchRunTime);
           Queryopp =  System.Label.DataMigHRXMLInsertQuery2;
    }
        
     global database.QueryLocator start(Database.BatchableContext BC)  
     {  
        //Create DataSet of Accounts to Batch
        return Database.getQueryLocator(Queryopp);
     } 

        
     global void execute(Database.BatchableContext BC, List<sObject> scope)
     {
       
        set<id> setOpportunityIds=new set<id>();
        List<Req_Hrxml__c> lstHrxml = new List<Req_Hrxml__c>();


        List<Opportunity> lstQueriedopps = (List<Opportunity>)scope;
        for(Opportunity op:lstQueriedopps)
        {
            setOpportunityIds.add(op.id);
        }
        
        system.debug('setOpportunityIds:::: ' + setOpportunityIds);
        
        List<Opportunity> lstOpportunity = [select id,(Select id, Req_Hrxml_Opportunity__c  from Hrxmls__r) from Opportunity where id =: setOpportunityIds];
        system.debug('lstOpportunity::::: ' + lstOpportunity );
        for(Opportunity opp : lstOpportunity)
        {
            System.debug('opp.Hrxmls__r::::' + opp.Hrxmls__r);
            System.debug('opp.Hrxmls__r.size()::::' + opp.Hrxmls__r.size());
            if(opp.Hrxmls__r == null || (opp.Hrxmls__r != null && opp.Hrxmls__r.size() == 0))
            {
                Req_Hrxml__c hrxml = new Req_Hrxml__c();
                hrxml.Req_Hrxml_Opportunity__c = opp.Id;
                lstHrxml.add(hrxml);
                
            }
        }
        
        
        System.debug('lstHrxml::::: ' + lstHrxml);
        if(lstHrxml != null && lstHrxml.size() > 0)
        {
            insert lstHrxml;
        }
        
     }             
        global void finish(Database.BatchableContext BC)
     {
          DataMigHrXMLBatch batch = new DataMigHrXMLBatch(dtLastBatchRunTime);
          Database.executeBatch(batch, 50);
          
     }
    
}