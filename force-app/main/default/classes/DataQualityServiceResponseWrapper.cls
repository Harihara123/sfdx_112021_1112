public class DataQualityServiceResponseWrapper {

	public class ResumeExtractions {
		public String status;
        public String firstName;
        public String lastName;
        public Integer monthsOfWorkExperience;
	}

	public Request request;
	public Response response;

	public class Duplicates {
		public String status;
		public List<Records> records;
	}

	public class Records {
		public DuplicateTalent duplicateTalent;
		public Double similarity;
		public List<String> duplicateMatchFlags;
	}

	public class HashBlocks {
	}

	

	public class Request {
		public String appId;
		public String sourceSystemId;
		public String vendorId;
		public Talent talent;
	}

	

	public class Talent {
		public String sourceSystemId;
		public String vendorId;
		public String vendorCandidateId;
		public String firstName;
		public String lastName;
		public List<String> email;
		public List<String> phone;
		public String street;
		public String city;
		public String state;
		public String country;
		public String zipCode;
		public Resume resume;
		public List<List<Integer>> hashBlocks;
		public List<Long> lshCodes;
		public String taxonomy;
		public Integer totalHashBlocks;
        public String talentId;
		public String fullName;
	}

	

	public class Response {
		public Duplicates duplicates;
		public ResumeExtractions resumeExtractions;
	}

	

	public class DuplicateTalent {
		public String sourceSystemId;
		public String vendorId;
		public String vendorCandidateId;
		public String firstName;
		public String lastName;
		public String email;
		public List<String> phone;
		public String street;
		public String city;
		public String state;
		public String country;
		public String zipCode;
		public Resume resume;
		public List<List<Integer>> hashBlocks;
		public List<Long> lshCodes;
		public String taxonomy;
		public Integer totalHashBlocks;
		public String candidateId;
		public String contactId;
		public String talentId;
		public String personalEmail;
		public String workEmail;
		public String otherEmail;
		public String homePhone;
		public String mobilePhone;
		public String workPhone;
		public String otherPhone;
		public Double elasticScore;
		public Integer elasticRank;
		public String resumeModifiedDate;
		public String candidateStatus;
		public String currentEmployerName;
		public String employmentPositionTitle;
		public String qualificationsLastActivityDate;
		public String qualificationsLastContactDate;
		
	}

	public class Resume {
		public String text;
		public String filename;
		public Boolean encoded;
		public Boolean parsed;
		public String xml;
	}

	
	public static DataQualityServiceResponseWrapper parse(String json) {
		return (DataQualityServiceResponseWrapper) System.JSON.deserialize(json, DataQualityServiceResponseWrapper.class);
	}
}