public class DdupeClientController {
	public static HttpCalloutMock mock = null;
    public static List<List<Sobject>> callGetService(String jsonString){

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Person/dupeMatch/V1';  
        
        //req.addParameter('Criteria', '[{"name":"linn","email":["sallyjbergeron@cuvox.de"],"phone":["309"],"linkedInUrl":"","externalSourceId":"","recordTypes":["Talent","Communities"],"returnCount":"one","IsAtsTalent":true,"City":[],"State":[],"Country":[],"Zip":[]}]');
        req.addParameter('Criteria', jsonString);
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        List<List<Sobject>> result = TalentDedupeSearch.getDedupTalents(); 
     
        return result;
    } 

    // calling this method from  console Anonymous block passing all parameters.
    public static void callPostService(String name, List<String> EmailList, List<String> PhoneList,String LinkedInUrl, String ExternalID,List<String> recordtypeList,String Returns, boolean isAts){
        //Getting Base URL          
        String base = URL.getSalesforceBaseUrl().toExternalForm() ;
        system.debug('base Url'+base);
        
        //Converting List to Json-String 
        String JsonEmailList = json.serialize(EmailList);
        String JsonPhoneList = json.serialize(PhoneList);
        String JsonrecordtypeList = json.serialize(recordtypeList);
        
        String Body= '{"name":"'+name+'","email":'+JsonEmailList+',"phone":'+JsonPhoneList+', "linkedInUrl":"'+LinkedInUrl+'", "externalSourceId" : "'+ExternalID+'", "recordTypes":'+JsonrecordtypeList+', "returnCount":"'+Returns+'" ,"IsAtsTalent":"'+isAts+'"}';
        
        if (Test.isRunningTest() ) {
            system.debug('Hello...');
        } else {
            //calling service
            HttpRequest req=new HttpRequest();
            req.setendpoint(base+'/services/apexrest/Person/dupeMatch/V1');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            req.setmethod('POST'); 
            req.setHeader('Content-Type', 'application/json');
            req.setbody(Body);
            
            
            HttpResponse resp = new Http().send(req);
            system.debug('response----->' + resp.getBody() );
        }

        
    }
    
    
    // for testing from console with hardcoded parameters
    /*
     
    public static void callServiceTest(){
        Http http1 =new Http();
        HttpRequest req1=new HttpRequest();
        req1.setendpoint('https://allegisgroup--augdev18.lightning.force.com/services/apexrest/dedupe/ ');
        req1.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req1.setmethod('POST'); 
        req1.setHeader('Content-Type', 'application/json');
        String Body= '{"name":"22","email":["1@mail","2@mail","3@mail"],"phone":["999","222"], "linkedInUrl":"www.linked.com", "externalSourceId" : "null", "recordTypes":["1","2"], "returnCount":"many" ,"is_ats_talent":"true"}';
        String Body1 ='{"name":"pranav","email":["w@yopmail.com","o@yopmail.com","e@yopmail.com"],"phone":["1234567890","2345678901","3456789012","3456789012"], "linkedInUrl":"www.linkedIn.com", "externalSourceId" : "12345678", "recordTypes":["1","2"], "returnCount":"many","is_ats_talent":"true"}' ; 
        req1.setbody(Body1);
        HttpResponse res1;
        if(!Test.isRunningTest()){
            res1 = http1.send(req1);    
            system.debug('response status-->'+res1);
            system.debug('Response Body-->'+res1.getBody());
        }
    
    }

	*/
}