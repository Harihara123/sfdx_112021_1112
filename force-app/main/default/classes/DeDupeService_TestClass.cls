@isTest
public class DeDupeService_TestClass {
    
    public static testMethod void coverDedupeService(){
        
        Contact con1 = new Contact();
        con1.firstName='First';
        con1.LastName='Name';
        con1.Email ='abc@email.com';
        con1.phone = '9045678980';
        con1.Other_Email__c ='abc@email.co.in';
        con1.externalSourceId__c = 'externalSourceId__c';
        insert con1;
        
        Contact con2 = new Contact();
        con2.firstName='First';
        con2.LastName='Second';
        //con2.IS_ATS_Talent__c = false;
        //con2.RecordTypeId =ContactType[0].id;
        con2.Peoplesoft_ID__c = 'Psoft_ID';
        con2.Email ='abc@email.com';
        con2.phone = '9045678980';
        con2.Other_Email__c ='abc@email.co.uk';
        insert con2;
        
        
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
        fixedSearchResults.add(con2.Id);
        
        Test.setFixedSearchResults(fixedSearchResults);
        
        String name ='First Second';
        String LinkedInUrl ='';
        String ExternalID ='externalSourceId__c';
        String ReturnType ='ONE';
        List<String> EmailList = new List<String>();
        List<String> PhoneList = new List<String>();
        List<String> RecordTypeList = new List<String>();
        
        EmailList.add('abc@email.com');
        EmailList.add('abc@email.co.in');
        RecordTypeList.add('') ;
        
        PhoneList.add('9045678980');
        
        
        Test.startTest();
        List<List<Sobject>> result =TalentDedupeSearch.postDedupTalents(name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,true);
        
        
        RestRequest req  = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Person/dupeMatch/V1';  
        
        String jsonString = '[{"name":"First Name","email":["abc@email.com","abc@email.co.in"],"phone":["1234555890","555"],"linkedInUrl":"","externalSourceId":"","recordTypes":[],"returnCount":"one","IsAtsTalent":false,"City":[],"State":[],"Country":[],"Zip":[]}]';
        req.addParameter('Criteria', jsonString);
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        TalentDedupeSearch.getDedupTalents(); 
        
        RestRequest req1  = new RestRequest(); 
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/Person/dupeMatch/V1';  
        
        String jsonString1 = '[{"name":"First Name","email":["abc@email.com","abc@email.co.in"],"phone":["1234555890","555"],"linkedInUrl":"","externalSourceId":"","recordTypes":["Talent","Communities"],"returnCount":"many","IsAtsTalent":true,"City":[],"State":[],"Country":[],"Zip":[]}]';
        req1.addParameter('Criteria', jsonString1);
        
        req1.httpMethod = 'GET';
        RestContext.request = req1;
        RestContext.response = res1;
        
        TalentDedupeSearch.getDedupTalents(); 
        
        Test.stopTest();
        
	}


    /* Covering Various Scenarios */

    /* Scenario : When external_source_id is passed as a paramater.
       Get record with an exact match for external source id - SOQL  
       Expected one record in return for both ONE and MANY ReturnType */
   public static testMethod void returnExternalSourceIDMatch(){

        Contact con1 = new Contact();
        con1.firstName='Contact';
        con1.LastName='With PSoft_ID';
        con1.Email ='mail@email.com';
        con1.phone = '8952321741';
        con1.Other_Email__c ='abc@email.co.in';
        con1.externalSourceId__c = 'ex_SourceId__1';
        con1.Peoplesoft_ID__c = 'Psoft_ID';
        insert con1;

        Contact con2 = new Contact();
        con2.firstName='Contact';
        con2.LastName='WithOut PSoft_ID';
        con2.Email ='mail@email.com';
        con2.phone = '8952321741';
        con2.Other_Email__c ='abc@email.co.in';
        con2.externalSourceId__c = 'ex_SourceId__2';
        insert con2;
        TalentVendor_Xref__c tv = new TalentVendor_Xref__c();
        tv.VendorTalentId__c = 'ex_SourceId__2';
        tv.vendorId__c = 'ex_SourceId__2';
        tv.contactId__c = con2.id;
        insert tv;

        String Name = 'Contact';
        String LinkedInUrl  = '';
        String ExternalID   ='ex_SourceId__2';
        String ReturnType   ='One';
        List<String> EmailList = new List<String>();
        List<String> PhoneList = new List<String>();
        List<String> RecordTypeList = new List<String>();
        
        EmailList.add('mail@email.com');
        PhoneList.add('8952321741');
        
        Test.startTest();
        List<List<Sobject>> one_result =TalentDedupeSearch.postDedupTalents(name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,false);
        System.debug('===========> One-Response/returnExternalSourceIDMatch : ' + one_result );

        ReturnType   ='Many';
        List<List<Sobject>> many_results =TalentDedupeSearch.postDedupTalents(name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,false);
        System.debug('===========> Many-Response/returnExternalSourceIDMatch : ' + many_results );
        Test.stopTest();
        
        
    }

    /* Scenario : When one of the record has peoplesoft id.
       This Should return record with PeopleSoft Id  
       Expected record with peopleSoft_id in return for ONE ReturnType and
       Expected both records in return for MANY ReturnType */

  public static testMethod void returnPeopleSoftRecord(){
      	Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
      	Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
      
      	Account a = new Account();
      	a.Name='Contact WithPsoft';
      	a.recordTypeId = RecordTypeIdAccount;
      	a.Talent_Ownership__c='TEK';
      	insert a;

        Contact con1 = new Contact();
        con1.firstName='Contact';
        con1.LastName='WithPsoft';
      	con1.AccountId = a.Id;
        con1.Email ='mail@email.com';
        con1.phone = '8952321741';
        con1.Other_Email__c ='abc@email.co.in';
        con1.externalSourceId__c = 'ex_SourceId__1';
        con1.Peoplesoft_ID__c = 'Psoft_ID';
      	con1.RecordTypeId = RecordTypeIdContact;
        insert con1;
		
      	Account a2 = new Account();
      	a2.Name='Contact WithPsoft';
      	a2.recordTypeId = RecordTypeIdAccount;
      	a2.Talent_Ownership__c='TEK';
      	insert a2;
      
        Contact con2 = new Contact();
        con2.firstName='Contact';
        con2.LastName='WithoutPSoft';
      	con2.AccountId =a2.Id;
        con2.Email ='mail@email.com';
        con2.phone = '8952321741';
        con2.Other_Email__c ='abc@email.co.in';
      	con2.RecordTypeId = RecordTypeIdContact;
        con2.externalSourceId__c = 'ex_SourceId__2';
        insert con2;
      
		List<Contact> tempList  =[select Id,Name, Phone,Email, Record_Type_Name__c from Contact];
      
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
        fixedSearchResults.add(con2.Id);
        
        Test.setFixedSearchResults(fixedSearchResults);

	
        String Name = 'Contact';
        String LinkedInUrl  = '';
        String ExternalID   ='';
        String ReturnType   ='One';
        List<String> EmailList = new List<String>();
        List<String> PhoneList = new List<String>();
        List<String> RecordTypeList = new List<String>();
        
        EmailList.add('mail@email.com');
        EmailList.add('abc@email.co.in');
        PhoneList.add('8952321741');
        
        RecordTypeList.add('Talent') ;
        
        Test.startTest();
        List<List<Sobject>> one_result =TalentDedupeSearch.postDedupTalents(Name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,true);
        System.debug('===========> One-Response/returnPeopleSoftRecord : ' + one_result );

        ReturnType   ='Many';
        List<List<Sobject>> many_results =TalentDedupeSearch.postDedupTalents(Name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,true);
        System.debug('===========> Many-Response/returnPeopleSoftRecord : ' + many_results );
        Test.stopTest();
        
        
    }

    /* Scenario : When neither of the records have activity or peoplesoft id.
       This Should return record Which is created last
       Expected record with Last name = Recent in return for ONE ReturnType and
       Expected both records in return for MANY ReturnType */

   public static testMethod void returnRecentlyCreated(){

        Contact con1 = new Contact();
        con1.firstName='Contact';
        con1.LastName='Older';
        con1.Email ='mail@email.com';
        con1.phone = '9090909090';
        con1.Other_Email__c ='abc@email.co.in';
        con1.externalSourceId__c = 'ex_SourceId__1';
        insert con1;

        // these lines are sometimes causing Apex CPU time limit exceeded
        //Integer start = System.Now().millisecond() + 300;
        //while(System.Now().millisecond() < start){ 
        //}

        Contact con2 = new Contact();
        con2.firstName='Contact';
        con2.LastName='Older';
        con2.Email ='mail@email.com';
        con2.phone = '9090909090';
        con2.Other_Email__c ='abc@email.co.in';
        con2.externalSourceId__c = 'ex_SourceId__2';
        insert con2;

        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
        fixedSearchResults.add(con2.Id);
        
        Test.setFixedSearchResults(fixedSearchResults);
        

        String Name = 'Contact Older';
        String LinkedInUrl  = '';
        String ExternalID   ='';
        String ReturnType   ='One';
        List<String> EmailList = new List<String>();
        List<String> PhoneList = new List<String>();
        List<String> RecordTypeList = new List<String>();
        
        EmailList.add('mail@email.com');
        EmailList.add('abc@email.co.in');
        PhoneList.add('9090909090');
        RecordTypeList.add('Talent') ; 
        Test.startTest();
        List<List<Sobject>> one_result =TalentDedupeSearch.postDedupTalents(Name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,true);
        System.debug('===========> One-Response/returnRecentlyCreated : ' + one_result );

        ReturnType   ='Many';
        List<List<Sobject>> many_results =TalentDedupeSearch.postDedupTalents(Name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,true);
        System.debug('===========> Many-Response/returnRecentlyCreated : ' + many_results );
        Test.stopTest();
        
        
    }


    /* Scenario : When both the records have peoplesoft id.
       This Should return record Which is created last
       Expected record with Last name = Recent in return for ONE ReturnType and
       Expected both records in return for MANY ReturnType ...*/

    public static testMethod void returnRecentPeopleSoftRecord(){
        List<Contact> conlist = new List<Contact>();
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        Contact con1 = new Contact();
        con1.firstName='Contact';
        con1.LastName='Older';
        con1.Email ='mail@email.com';
        con1.phone = '9090909090';
        con1.Other_Email__c ='abc@email.co.in';
        con1.externalSourceId__c = 'ex_SourceId__1';
        //con1.Peoplesoft_ID__c = 'Psoft_ID';
        con1.Peoplesoft_ID__c = '';
        con1.RecordTypeId = RecordTypeIdContact;
        conlist.add(con1);
        insert con1;

        // these lines are sometimes causing Apex CPU time limit exceeded
        //Integer start = System.Now().millisecond() + 100;
        //while(System.Now().millisecond() < start){ 
        //}

        Contact con2 = new Contact();
        con2.firstName='Contact';
        con2.LastName='Recent';
        con2.Email ='mail@email.com';
        con2.phone = '9090909090';
        con2.Other_Email__c ='abc@email.co.in';
        con2.externalSourceId__c = 'ex_SourceId__2';
        con2.Peoplesoft_ID__c = '';
        con2.RecordTypeId = RecordTypeIdContact;
        conlist.add(con2);
        insert con2;
		
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
        fixedSearchResults.add(con2.Id);
        
        Test.setFixedSearchResults(fixedSearchResults);


        String Name = 'Contact Recent';
        String LinkedInUrl  = '';
        String ExternalID   ='';
        String ReturnType   ='Many';
        List<String> EmailList = new List<String>();
        List<String> PhoneList = new List<String>();
        List<String> RecordTypeList = new List<String>();
        
        EmailList.add('mail@email.com');
        EmailList.add('abc@email.co.in');
        PhoneList.add('9090909090');
        RecordTypeList.add('Talent');
        
        Test.startTest();
        List<List<Sobject>> one_result =TalentDedupeSearch.postDedupTalents(Name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,true);
        System.debug('===========> One-Response/returnPeopleSoftRecord : ' + one_result );
        TalentDedupeSearch.applyDedupeRules(conlist,'ONE');
        ReturnType   ='Many';
        List<List<Sobject>> many_results =TalentDedupeSearch.postDedupTalents(Name, EmailList, PhoneList,LinkedInUrl,ExternalID, RecordTypeList,ReturnType,true);
        System.debug('===========> Many-Response/returnPeopleSoftRecord : ' + many_results );
        TalentDedupeSearch.applyDedupeRules(conlist,'MANY');
         
        Test.stopTest();
        
        
    }
  
}