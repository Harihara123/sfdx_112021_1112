global class DeleteAccountsWithNoContactBatch implements Database.Batchable<SObject> {
	
	global String query;
	
	global Database.QueryLocator start(Database.BatchableContext context) {
		query = 'select ID from account where Record_Type_Name__c = \'Talent\' AND Talent_Committed_Flag__c = true AND id not in (select accountid from contact)';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext context, List<Account> accountBatch) {
		if (accountBatch.size() > 0 ) {
			System.debug('accountBatch size:' + accountBatch.size());
			Database.delete(accountBatch);
			
		}
	}
	
	global void finish(Database.BatchableContext BC) {
	  AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       String subject = 'Clean Up of Uncommitted SOA Created Accounts';       
       mail.setSubject(subject + a.Status);
	   string s = 'The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures';
       mail.setPlainTextBody(s);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}