/**********************************************************************
Class Name:  DeleteReqController
Version     : 1.0 
Function    : Delete the req based on profile permission
 
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
*                              05/26/2012              Created
*Rajkumar                   5/17/2013                Add validation for staging Req                                                
******************************************************************************/

public class DeleteReqController {
    Reqs__c req;
    
    Boolean Req_Deletesetup = False;
                          
    // Get current profile custom setting.
    Profile_Extension__c Req_profile = Profile_Extension__c.getvalues(UserInfo.getProfileId()); 
    // Get current Organization custom setting.
    Profile_Extension__c Req_Org = Profile_Extension__c.getvalues(UserInfo.getOrganizationId());
    // Get current User custom setting.
    Profile_Extension__c Req_user = Profile_Extension__c.getValues(UserInfo.getUserId());           
            
    public DeleteReqController(ApexPages.StandardController controller) 
    {
            if(Req_user != Null) 
                Req_Deletesetup = Req_user.Allow_Delete_Reqs__c;
            else if(Req_profile != Null)
                Req_Deletesetup = Req_profile.Allow_Delete_Reqs__c;
            else if(Req_Org != Null)
                Req_Deletesetup = Req_Org.Allow_Delete_Reqs__c;
          
            req = (Reqs__c) controller.getRecord();    
            Req = [select id,Stage__c,Deleted__c,Status__c from Reqs__c where Id = :req.id];            
    }    
    public pagereference redirect() 
    {
        pageReference pg;
       string ProfileName= [Select Id,Name From Profile where Id=:UserInfo.getProfileId()].Name;
        // Verify the user/profiels has a permission to delete Req in Qualified stage.
        if(req != Null && (req.Stage__c != Label.Req_Stage_Not_Delete || ProfileName =='System Administrator') )
        {       
                if(Req_Deletesetup && (req.Stage__c == Label.Req_Stage_for_Delete || req.Status__c == Label.Req_Stage_Closed)) 
                {
                      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Req_Delete_Error_Message));
                }
                else 
                {
                    req.Deleted__c = true;
                    req.UndeleteWFRetrigger__c = false;
                    try {
                            update req;
                            // delete req
                            deleteReqs();
                            pg = new PageReference(Label.Req_HomePage);
                        }
                        catch(exception e)
                        {
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,e.getMessage()));
                        }       
                }                                 
        } else{
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Req_Staging_Delete_Error_Message));
        }                                        
        return pg;
    }
    
    // Redirect to Detail page of the Req
    public pagereference BacktoReq() {
        pageReference pgback;
        pgback = new PageReference('/'+req.id);
        return pgback;      
    }  
    
    @future(callout=true)
    public static void deleteReqs()
    {
    List<Opportunity> Entreqset= new List<Opportunity>();
    set<id> entreqid =new set<id>();
        List<Reqs__c> reqsToBeDeleted = [Select Id,EnterpriseReqId__c, Deleted__c from Reqs__c where Deleted__c =: true];
        
        for(Reqs__c rq :reqsToBeDeleted){
            
            if(rq.EnterpriseReqId__c!=null){
                entreqid.add(rq.EnterpriseReqId__c);
            }
            }
        
        Entreqset=[select id from Opportunity where Req_origination_System_Id__c =:reqsToBeDeleted[0].id or Req_Legacy_Req_Id__c =:reqsToBeDeleted[0].id or id in:entreqid];
        system.debug('raja'+Entreqset);
        
        if(Entreqset.size()>0){
            NOtifyApigeeOnOpportunityDelete.deleteOpportunity(Entreqset[0].id);
            Delete Entreqset;
            

        }
        
        if(reqsToBeDeleted  != null){
        delete reqsToBeDeleted ;
    } 
    
     
    
       
}
}