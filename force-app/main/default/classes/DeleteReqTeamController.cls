public with sharing class DeleteReqTeamController {
    Req_Team_Member__c reqteam ;
    public string idUrl;     //[Dhruv 07/05/2012]Changed the String variable from reqteam_parentReqId to idUrl.
     
    public string partialURL;   
 
    public DeleteReqTeamController(ApexPages.StandardController controller) 
    {
        reqteam = (Req_Team_Member__c) controller.getRecord();
        reqteam  = [select Id,User__c,Deleted__c,Requisition__c,Requisition__r.ownerId,Requisition__r.Organization_Office__r.Active__c from Req_Team_Member__c where id = :reqteam.id];       
    }
    
    public pagereference redirect()
    {
            if(reqteam.Requisition__r.ownerId != reqteam.User__c)
            {
                reqteam.Deleted__c = true;
                idUrl = reqteam.Requisition__c ;        //We need to point the Id towards the Id of the related Req and not the Req Team Member record because this record will get deleted.
            }
            else
            {
                 processErrorMessages(Label.ReqTeamMemberErrorMessage);
            }
            // check for associated Req Office
            if(!reqteam.Requisition__r.Organization_Office__r.Active__c)// pavan [09/10]
            {
                 reqteam.Deleted__c = false;
                 processErrorMessages(Label.ReqTeamMember_AssociatedReqOfficeErrorMsg);
            }
                    
            pageReference pg;
            if(reqteam.Deleted__c)
            {
                try
                {
                    delete reqteam;
                    // update the parent Req
                    deleteReqTeamMembers(idUrl); 
                    partialURL = '/'+ idUrl;            //Now the partial Url is appended with the Id of the required record i.e. the Id of the Req record with which this ReqTeam Member is related.
                    pg = new PageReference(partialURL);
                }
                catch(exception e)
                {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, e.getMessage());
                    ApexPages.addMessage(msg);
                }
          
            }
               
            return pg;
    }  
    
     private void deleteReqTeamMembers(string idUrl)
     {
         // gather parent Req for the deleted Team Meber record
         List<Reqs__c> ParentReq= [select Id, Proactive_Req__c from reqs__c where id = :idUrl];

         // blanket update parent Req for push to IR
          update ParentReq;
     }
     // Method to process error messages
     private void processErrorMessages(string errorMsg)
     {
          ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
          ApexPages.addMessage(msg);
     }
    
}