public class DetailSearchLookupController  {
	@AuraEnabled(cacheable=false)
    public static List<List<SObject>> searchResult(String featureItem, String searchString, String searchInFields, String returningFields,String targetObject, List<ConditionWrapper> condition, String sortString,String limitValue){
        List<List<SObject>> result = null;
		String soslQuery = formatSOSLQuery(searchString, searchInFields, returningFields, targetObject, condition, sortString,limitValue);
		if(String.isNotBlank(soslQuery)) {
			try{
				System.debug('searchInFields---'+searchInFields+'---condition----'+condition + '--sortString--'+sortString);
				System.debug('soslQuery ===='+soslQuery);
				result = Search.query(soslQuery);


				return result;
			}catch(Exception ex){
				System.debug('DetailSearchLookupController:::searchResult:::'+ex.getCause());
			}
		}
        return result;
    }

	public class ConditionWrapper {
		@AuraEnabled public  String fieldName{get;Set;}
		@AuraEnabled public  String fieldValue{get;Set;}
	}

	private static String formatSOSLQuery(String searchString, String searchInFields, String returningFields,String targetObject, List<ConditionWrapper> queryCondition, String sortString,String limitValue) {
		String soslQuery;

		String condition = stringifyCondition(queryCondition);
		System.debug('searchString---'+searchString+'---searchInFields---'+searchInFields+'---returningFields---'+returningFields+'---targetObject---'+targetObject+'---condition----'+condition + '--sortString--'+sortString);
	
			
		if(String.isNotBlank(searchString) && String.isNotBlank(targetObject)) {
			Boolean isReturningFileds = false;
			Boolean isCondition =false;
			Boolean isSortString = false;

			searchString = String.escapeSingleQuotes(searchString);
			targetObject = String.escapeSingleQuotes(targetObject);
			if(String.isNotBlank(returningFields)) {
				returningFields = String.escapeSingleQuotes(returningFields);
				isReturningFileds = true;
			}

			if(String.isNotBlank(condition)) {
				condition = String.escapeSingleQuotes(condition);
				isCondition = true;
			}

			if(String.isNotBlank(sortString)) {
				sortString = String.escapeSingleQuotes(sortString);
				isSortString = true;
			}
	
			soslQuery = 'FIND {' + searchString + '} IN ';
			
			if(String.isNotBlank(searchInFields)) {
				searchInFields = String.escapeSingleQuotes(searchInFields);
				 soslQuery = 'FIND {' + searchString + '} IN ' + searchInFields + ' FIELDS RETURNING ' + targetObject;	
			}
			else {
				soslQuery = 'FIND {' + searchString + '} IN ALL FIELDS RETURNING ' + targetObject;	
			}
			
			if(isReturningFileds && isCondition && isSortString) {
				 soslQuery += '('+ returningFields +' where ' +condition + ' order by ' + sortString + ')';
			}
			else if(isReturningFileds && isCondition) {
				soslQuery += '('+ returningFields +' where ' +condition + ')';
			}
			else if(isReturningFileds && isSortString) {
				soslQuery += '('+ returningFields +' order by ' + sortString + ')';
			}
			else if(isCondition && isSortString) {
				soslQuery += '( NAME ' + ' WHERE ' + condition  +' ORDER BY ' + sortString + ')';
			}
			else if(isReturningFileds) {
				soslQuery += '('+ returningFields + ')';
			}
			else if(isCondition) {
				soslQuery += '( NAME ' + ' WHERE ' + condition + ')';
			}
			else if(isSortString) {
				soslQuery += '( NAME ' + ' ORDER BY ' + sortString +  ')';
			}
			
		}
        if(limitValue != NULL && String.isNotBlank(limitValue))
        {
            soslQuery = soslQuery.remove('\\') + ' LIMIT ' + limitValue;
        }
        else
        {
            soslQuery = soslQuery.remove('\\') + ' LIMIT 200 ';
        }
		System.debug('query ::::'+soslQuery);

		return soslQuery; 
	}

	private static String stringifyCondition( List<ConditionWrapper> conditions) {
		String condition='';
		System.debug('conditions==='+conditions);
		if(conditions != null && conditions.size() > 0) {
			for(ConditionWrapper cond : conditions) {
				if(String.isBlank(condition)) {
					condition = cond.fieldName + ' = ' + '\''+cond.fieldValue+'\'';
				}
				else {
					condition += ' and ' + cond.fieldName + ' = ' + '\''+cond.fieldValue+'\'';
				}
			}
		}
		System.debug(condition);
		return condition;
	}	

	/*private static List<List<SObject>> updateProactiveSubmittalData( List<List<SObject>> results) {
		//List<SObject> updatedResult = new List<List<SObject>>();

		for(SObject sObj : results.get(0)) {
			Account accSobj	= (Account)sObj;
			if(String.valueOf(accSobj.Customer_Status__c).contains('Red') ) {
				accSobj.Customer_Status__c = 'Never had Talent';
			}
			else if(String.valueOf(sObj.Customer_Status__c).contains('Yellow') ) {
				accSobj.Customer_Status__c = 'Previously Had Talent';
			}
			else if(String.valueOf(sObj.Customer_Status__c).contains('Green') ) {
				accSobj.Customer_Status__c = 'Currently Has Talent';
			}
		}

		return results;
	} */
}