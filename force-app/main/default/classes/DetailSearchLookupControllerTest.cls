@isTest
public class DetailSearchLookupControllerTest  {
	static testMethod void searcResultTest() {
		Test.startTest();
			List<Account> accList = new List<Account>();
			for(Integer i=0; i<5; i++) {
				TestData.newAccount(i);
				accList.add(TestData.newAccount(i, 'Talent'));
			}

			Insert accList;
			String recType= 'Talent';
			System.debug([select id, name from account where recordtype.name=:recType]);

			DetailSearchLookupController.ConditionWrapper cw = new DetailSearchLookupController.ConditionWrapper();
			cw.fieldName = 'recordtype.name';
			cw.fieldValue = 'Talent';
			System.debug(Search.query('FIND \'TEST*\' IN ALL FIELDS RETURNING Account(Name, BillingState, BillingCountry where recordtype.name = \'Talent\' order by Name desc)'));
			List<DetailSearchLookupController.ConditionWrapper> cwList = new List<DetailSearchLookupController.ConditionWrapper>(); 
			cwList.add(cw);

			 List<List<SObject>> searchResults = DetailSearchLookupController.searchResult('ProactiveSubmittal','TEST','Name', 'Name, BillingState, BillingCountry', 'Account', cwList, 'Name desc',NULL);
			 List<List<SObject>> searchResults1 = DetailSearchLookupController.searchResult('ProactiveSubmittal','TEST','Name', '', 'Account', cwList, 'Name desc',NULL);
			 List<List<SObject>> searchResults2 = DetailSearchLookupController.searchResult('ProactiveSubmittal','TEST','Name', 'Name, BillingState, BillingCountry', 'Account', cwList, '',NULL);
			 System.debug(searchResults);
		Test.stopTest();
	}
}