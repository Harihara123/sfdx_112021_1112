public class DrzRedZoneCtrl {
    
    @AuraEnabled
    public static string getRedzoneLink(){
        
        string orgType = Utilities.getSFEnv();
        
        boolean isSandboxOrg = orgType=='prod'?false:true;
        String sandbozRedzoneLink = system.Url.getSalesforceBaseUrl().toExternalForm()+'/apex/sf_drz';
        String redZoneLink  = isSandboxOrg == false? System.Label.DrzRedZone:sandbozRedzoneLink;
        return redZoneLink;
    }

}