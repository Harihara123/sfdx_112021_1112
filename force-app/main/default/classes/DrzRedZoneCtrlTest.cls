@isTest
public class DrzRedZoneCtrlTest {

    public testMethod static void getRedzoneUrl(){
        
        Test.startTest();
        
        string urlFromMethod = DrzRedZoneCtrl.getRedzoneLink();
        
         boolean isSandboxOrg = [select isSandbox from Organization Limit 1].isSandbox;
        String sandbozRedzoneLink = system.Url.getSalesforceBaseUrl().toExternalForm()+'/apex/sf_drz';
        String redZoneLink  = isSandboxOrg == false? System.Label.DrzRedZone:sandbozRedzoneLink;
        system.assert(urlFromMethod == redZoneLink);
        Test.stopTest();
        
    }
}