public with sharing class DupeSearchController {
    @AuraEnabled
    public static user getUser(){
        return [SELECT Id, Opco__c, title, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
    }
  
    @AuraEnabled
      public static ATS_UserOwnershipModal getCurrentUserWithOwnership() {
        ATS_UserOwnershipModal uoModal = new ATS_UserOwnershipModal();
        User user = [select id, Name, Title, CompanyName, OPCO__c, Country, Profile.Name, Profile.PermissionsModifyAllData from User where id = :Userinfo.getUserId()];
        uoModal.usr = user;

        if(!String.isBlank(user.OPCO__c) ) {
            Talent_Role_to_Ownership_Mapping__mdt[] Ownership = [SELECT Opco_Code__c,Talent_Ownership__c,LinkedIn_Sync__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: user.OPCO__c];      
            if(Ownership!=null && !Ownership.isEmpty()) {
                uoModal.userOwnership = Ownership[0].Talent_Ownership__c;
                uoModal.haslinkedInSync = Ownership[0].LinkedIn_Sync__c;            
            }
        }
        return uoModal;
    }
    
    //SOSL for Name Fields
    @AuraEnabled
    public static String searchObjectByName(String searchText, String whereClause, String searchObject, String sortField, String contactRecordType, Boolean sortAsc, String transactionId) {
        String recTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactRecordType).getRecordTypeId();
        //searchText = '\"' + searchText + '\"';
        System.debug('searchtext::'+searchText);
        String soslQuery = searchObjectByType('NAME', searchText, searchObject, whereClause, sortField, sortAsc, contactRecordType, recTypeID);
        String result = JSONGeneratedSOSLResults(Search.query(soslQuery));
		String request ='{"searchText":"'+searchText+'","whereClause":"'+whereClause+'","query":"'+soslQuery+'"}';
		if(result == '' || result =='[]'){
			logInformation('searchObjectByName',request,'No_Result_Found','',transactionId);
		}else{
			logInformation('searchObjectByName',request,'Result Found','',transactionId);
		}
		
		return result;
    }

    //SOSL for Phone Field
    @AuraEnabled
    public static String searchObjectByPhone(String searchText, String whereClause, String searchObject, String sortField, String contactRecordType, Boolean sortAsc) {
        String recTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactRecordType).getRecordTypeId();
        searchText = '\"' + searchText + '\"';
        System.debug('searchtext::'+searchText);
        String soslQuery = searchObjectByType('PHONE', searchText, searchObject, whereClause, sortField, sortAsc, contactRecordType, recTypeID);
        return JSONGeneratedSOSLResults(Search.query(soslQuery));
    }

    //SOSL for Email Field
    @AuraEnabled
    public static String searchObjectByEmail(String searchText, String whereClause, String searchObject, String sortField, String contactRecordType, Boolean sortAsc) {
        String recTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactRecordType).getRecordTypeId();
        searchText = '\"' + searchText + '\"';
        System.debug('searchtext::'+searchText);
        String soslQuery = searchObjectByType('EMAIL', searchText, searchObject, whereClause, sortField, sortAsc, contactRecordType, recTypeID);
        return JSONGeneratedSOSLResults(Search.query(soslQuery));
        }


    //SOSL for Phone -Email- Name Field
    @AuraEnabled
    public static String searchObjectByPhoneEmailName(String searchText, String whereClause, String searchObject, String sortField, String contactRecordType, Boolean sortAsc, String transactionId) {
        String recTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactRecordType).getRecordTypeId();
        //searchText = '\"' + searchText + '\"';
        System.debug('searchtext::'+searchText);
        String soslQuery = searchObjectByType('', searchText, searchObject, whereClause, sortField, sortAsc, contactRecordType, recTypeID);
        String result = JSONGeneratedSOSLResults(Search.query(soslQuery));
		String request ='{"searchText":"'+searchText+'","whereClause":"'+whereClause+'","query":"'+soslQuery+'"}';
		if(result == '' || result =='[]'){
			logInformation('searchObjectByPhoneEmailName',request,'No_Result_Found','',transactionId);
		}else{
			logInformation('searchObjectByPhoneEmailName',request,'Result Found','',transactionId);
		}
		
		return  result;
       }
    
    @AuraEnabled
    public static String queryObject(String searchText, String whereClause, String searchObject, String sortField, String contactRecordType, Boolean sortAsc, String transactionId) {
        String recTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactRecordType).getRecordTypeId();
        String soqlQuery = baseSOQLQuery(searchText, searchObject, whereClause, sortField, sortAsc, contactRecordType, recTypeID, null);  
        String result = JSONGeneratedSOQLResults(Database.query(soqlQuery));
		String request ='{"searchText":"'+searchText+'","whereClause":"'+whereClause+'","query":"'+soqlQuery+'"}';
		if(result == '' || result =='[]'){
			logInformation('queryObject',request,'No_Result_Found','',transactionId);
		}else{
			logInformation('queryObject',request,'Result Found','',transactionId);
		}
		return  result;
		 
        }
        
    @AuraEnabled
    public static String queryRecentObject(String searchText, String whereClause, String searchObject, String sortField, String contactRecordType, Boolean sortAsc) {
        String recTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactRecordType).getRecordTypeId();
        List<Id> recentRecordIds = new List<Id>();
        for(RecentlyViewed record : [SELECT Id FROM RecentlyViewed WHERE Type =: searchObject ORDER BY LastViewedDate DESC]) {
            recentRecordIds.add(record.id);
        }
        String soqlQuery = baseSOQLQuery(searchText, searchObject, whereClause, sortField, sortAsc, contactRecordType, recTypeID, recentRecordIds);  
        return JSONGeneratedSOQLResults(Database.query(soqlQuery));
    }

    @AuraEnabled
    public static String talentDeDupeSearchService(String searchText, String emails, String phones, String contactRecordType) {
        //for reference 
        //global static List<List<SObject>> postDedupTalents(String name, list<String> email, list<String> phone, String linkedInUrl, String externalSourceId, list<String> recordTypes, String returnCount, Boolean IsAtsTalent ) {
        return JSONGeneratedSOSLResults(
            TalentDedupeSearch.postDedupTalents(
                searchText
                , (List<String>)JSON.deserialize(emails, List<String>.class)
                , (List<String>)JSON.deserialize(phones, List<String>.class)
                , ''
                , ''
                , new List<String>{contactRecordType}
                , 'Many'
                , (contactRecordType == 'Client') ? false : true));
    }

    private static String getSortClause(String sortFields, Boolean sortAsc) {
        String clause = ' ORDER BY ';
        String dir = sortAsc ? ' ASC' : ' DESC';
        String[] flds = sortFields.split(',');
        System.debug(flds);
        Boolean firstIter = true;
        for (String field : flds) {
            clause += (firstIter ? '' : ', ') + field.trim() + dir;
            firstIter = false;
        }
        clause += ' NULLS LAST';
        return clause;
    }

    @AuraEnabled
    public static String createUncommittedTalent() {
        Account talent = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'ATS Resume Match Temp Account',
            Talent_Committed_Flag__c = false,
            Talent_Visibility__c = 'Restricted' // Added to Hide uncommited talent from all users - Nidish
        );
        insert talent;
        return talent.Id;
    }

    private static Contact getClientContact(String clientContactId) {
        Contact c;

        try {
            c = [SELECT Id, FirstName, LastName, MobilePhone, Phone, WorkExtension__c, HomePhone, OtherPhone, Email, Other_Email__c, MailingCity, MailingState, 
                        MailingCountry, MailingPostalCode, Mailing_Address_Formatted__c, AccountId, Account.Name, Title 
                     FROM Contact
                     WHERE Id =: clientContactId];
        } catch (Exception e) {
            System.debug('No client contact found for given ID');
        }

        return c;
    }

    @AuraEnabled
    public static String getClientContactData(String clientContactId) {
        return JSON.serialize(getClientContact(clientContactId));
    }

    @AuraEnabled
    public static String relateTalent(String clientContactId, String talentContactId) {
        Contact clientContact = getClientContact(clientContactId);
        clientContact.Related_Contact__c = Id.valueOf(talentContactId);

        update clientContact;
        return 'SUCCESS';
    }

    @AuraEnabled
    public static List<Contact> addAndRelateTalent(String clientContactId) {
        List<Contact> contactList = new List<Contact>();
        Contact clientContact = getClientContact(clientContactId);
        Account talentAccount = createTalentAccount(clientContact);
        Contact talentContact = createTalentContact(talentAccount, clientContact);
        contactList.add(clientContact);
        contactList.add(talentContact);
        return contactList;    
    }

    private static Account createTalentAccount(Contact c) {
        Account talent = new Account(
            RecordTypeId = [select Id, DeveloperName 
            from RecordType where DeveloperName = 'Talent' 
            AND SObjectType = 'Account' limit 1][0].Id,
            Name = c.FirstName + ' ' + c.LastName,
            Talent_Status__c = 'Active'
        );

        insert talent;
        return talent;
    }

    private static Contact createTalentContact(Account ta, Contact cc) {
        Contact tc = new Contact(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Contact' limit 1][0].Id,
            FirstName = cc.FirstName,
            LastName = cc.LastName,
            MobilePhone = cc.MobilePhone,
            HomePhone = cc.HomePhone,
            OtherPhone = cc.OtherPhone,
            Phone = cc.Phone,
            WorkExtension__c = cc.WorkExtension__c,
            // Email = cc.Email, D-06168 : Commented because of this defect
            Other_Email__c = cc.Other_Email__c,
            work_Email__c = cc.Email,
            Related_Contact__c = cc.Id,
            AccountId = ta.Id
        );

        insert tc;
        return tc;
    }

    @AuraEnabled
    public static Talent_Experience__c createTalentExperience(Contact clientContact, Contact talentContact) {
        Talent_Experience__c workEx = new Talent_Experience__c(
            Talent__c = talentContact.AccountId,
            Current_Assignment__c = true,
            Client__c = clientContact.AccountId,
            Title__c = clientContact.Title,
            Organization_Name__c = clientContact.Account.Name,
            Type__c = 'Work'
        );
        insert workEx;
        
        return workEx;
    }

    @AuraEnabled
    public static Map<String,String> getCountryMappings(){
        Map<String,String> countriesMap = new Map<String,String>();
        for(CountryMapping__mdt country:[SELECT CountryCode__c,CountryName__c,Id,Label FROM CountryMapping__mdt]) {
            countriesMap.put(country.Label,country.CountryCode__c);
        }
        return countriesMap;
    }

    private static String JSONGeneratedSOQLResults(List<SObject> soqlQueryResults) {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        System.debug(soqlQueryResults);
        if (soqlQueryResults.size() > 0) {
            jsonGen.writeStartArray();
            for (SObject sobj : soqlQueryResults) {
                jsonGen.writeObject(sobj);
            }
            jsonGen.writeEndArray();
        }
        return jsonGen.getAsString();
    }

    private static String JSONGeneratedSOSLResults(List<List<SObject>> soslQueryResults) {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        System.debug(soslQueryResults[0]);
        if (soslQueryResults.size() > 0) {
            jsonGen.writeStartArray();
            for (SObject sobj : soslQueryResults[0]) {
                jsonGen.writeObject(sobj);
            }
            jsonGen.writeEndArray();
        }
        return jsonGen.getAsString();
    }

    private static String searchObjectByType(String fieldType, String searchText, String searchObject, String whereClause, String sortField, Boolean sortAsc, String contactRecordType, String recTypeID) {
         String soslQuery;
        if (String.isEmpty(fieldType)) {
            soslQuery = 'FIND :searchText RETURNING '
                                + searchObject 
                                + '(Id, Name,TC_Name__c, Preferred_Name__c, Talent_Id__c, MobilePhone, Phone, HomePhone, OtherPhone,TC_Phone__c, Work_Email__c, Email, Other_Email__c,TC_Email__c, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, Title,TC_Title__c, MailingCity, MailingCountry, MailingPostalCode, Mailing_Address_Formatted__c, MailingState,Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, WorkExtension__c'
                                + ' WHERE RecordTypeId=' + '\'' + recTypeID + '\'' ;
        } else {
        soslQuery = 'FIND :searchText IN ' + fieldType + ' FIELDS RETURNING '
                            + searchObject 
                            + '(Id, Name,TC_Name__c, Preferred_Name__c, Talent_Id__c, MobilePhone, Phone, HomePhone, OtherPhone,TC_Phone__c, Work_Email__c, Email, Other_Email__c,TC_Email__c, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, Title,TC_Title__c, MailingCity, MailingCountry, MailingPostalCode, Mailing_Address_Formatted__c, MailingState,Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, WorkExtension__c'
                            + ' WHERE RecordTypeId=' + '\'' + recTypeID + '\'' ;
        }
        if(contactRecordType == 'Talent') {
            soslQuery += ' AND Talent_Committed_Flag__c=true';
        }
        if(String.isEmpty(whereClause)){
            soslQuery += (String.isNotBlank(sortField) ? getSortClause(sortField, sortAsc) : '') 
                            + ') LIMIT 200';
        }
        else{
            soslQuery += ' AND ' + whereClause
                            + (String.isNotBlank(sortField) ? getSortClause(sortField, sortAsc) : '')
                            + ') LIMIT 200';
        }
        System.debug('SOSL QUERY: ' + soslQuery);
        return soslQuery;
    }

    private static String baseSOQLQuery(String searchText, String searchObject, String whereClause, String sortField, Boolean sortAsc, String contactRecordType, String recTypeID, List<Id> recentRecordIds) {
        String soqlQuery = 'select Id, Name, TC_Phone__c, TC_Email__c, Preferred_Name__c, Talent_Id__c, MobilePhone, Phone, HomePhone, OtherPhone, Work_Email__c, Email, Other_Email__c, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, Title, MailingCity, Mailing_Address_Formatted__c, MailingState, MailingCountry, MailingPostalCode, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name from '
                            + searchObject 
                            + ' WHERE RecordTypeId=' + '\'' + recTypeID + '\'';                   
        if(contactRecordType == 'Talent') {
            soqlQuery += ' AND Talent_Committed_Flag__c=true';
        }
        if(recentRecordIds != null) {
            soqlQuery += ' AND Id IN: recentRecordIds';
        }
        if(String.isEmpty(whereClause)){
            soqlQuery += (String.isNotBlank(sortField) ? getSortClause(sortField, sortAsc) : '')
                            + ' LIMIT 200';
        }
        else {
            soqlQuery += ' AND ' + whereClause
                            + (String.isNotBlank(sortField) ? getSortClause(sortField, sortAsc) : '')
                            + ' LIMIT 200';
        }
        System.debug('SOQL'+soqlQuery);
        return soqlQuery;
    }
    
    //Story-S-138881
    @AuraEnabled
    public static String attachResumeToTalent(String accId, String conId ) {
       
       Savepoint savePoint = null;
       try{
            savePoint = Database.setSavepoint();  
            List<Contact> conList = [SELECT Id, AccountId FROM Contact WHERE Talent_Id__c =: conId];
            List<Talent_Document__c> talDocList = [SELECT Id, Talent__C, Committed_Document__c from Talent_Document__c where Talent__c =: accId];
            talDocList[0].Committed_Document__c=true;
            talDocList[0].Talent__C = conList[0].AccountId;
           
            update talDocList;
            return conList[0].Id;
        }catch(Exception e){
            Database.rollback(savePoint);
            return null;
        }
    }
    @AuraEnabled
    public static List<String> talentResumeCount(String conId) {  
       try{ 
           List<String> resultList = new List<String>();
           Contact conObj = [SELECT AccountId FROM Contact WHERE Talent_Id__c =: conId LIMIT 1];
           Account account = [select Talent_Internal_Resume_Count__c from Account where id = :conObj.AccountId];
           resultList.add(string.valueOf(account.Talent_Internal_Resume_Count__c));
           resultList.add(account.Id);
            
           return resultList;
       }catch(Exception e){
            return null;
       }  
    }
    @AuraEnabled
    public static string deleteDocument(String replaceResumeId){
       
       Savepoint savePoint = null;
       try{
            if (replaceResumeId != '' && replaceResumeId != null) {
                String[] parts = replaceResumeId.split('-');
                String oldTalentDocumentId = parts[0];
                String attachmentId = parts[1];
                Attachment attachment = new Attachment(Id = attachmentId); 
                delete attachment;
                
                Talent_Document__c talentDocument = [SELECT Id FROM Talent_Document__c where Id =:oldTalentDocumentId];
                talentDocument.Mark_For_Deletion__c = true;
                update (talentdocument);
            }
            return 'Success';
        }catch(Exception e){
            Database.rollback(savePoint);
            return null;
        }  
    }
    @AuraEnabled
    public static List<List<SObject>> searchClientContacts(String searchString, String searchFields, String targetObject){
        List<List<SObject>> result = null;
        try{
            String recType = 'Talent';
           // String targetObject = 'Account';

            String soslQuery = 'FIND {' + searchString + '} IN ALL FIELDS RETURNING '+ targetObject +'('+ searchFields +' where recordtype.Name = :recType )';
            System.debug('soslQuery '+soslQuery);
            //'Account(Name,BillingState,BillingCity,billingstreet where recordtype.Name = :recType), Contact(Name,Account.Name,account.BillingCity,phone,Email where recordtype.Name = :recType)';        
            result = Search.query(soslQuery);
            return result;
        }catch(Exception ex){
            System.debug(ex.getCause());
        }
        return result;
    }

		@AuraEnabled
		public static String generateTransactionID(){
			final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
			String randStr = '';
			while (randStr.length() < 6) {
			   Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
			   randStr += chars.substring(idx, idx+1);
			}
			return randStr+'|'+String.valueOf(UserInfo.getUserId());
    	
		} 
		
		@AuraEnabled
		public static void logInformation(String methodName, String request, String result, String nxtAction,String transactionId){
			System.debug('LogInformation_request'+request);
			System.debug('LogInformation_result'+result);
			System.debug('LogInformation_Action'+nxtAction);
			System.debug('LogInformation_transactionId'+transactionId);


			Map<String,String> infoMap = new Map<String,String>();
			//infoMap.put('TransactionId',transactionId);
			
			if(nxtAction!= null && nxtAction !=''){
				infoMap.put('Next Action',nxtAction);
			}else{
				infoMap.put('Type','Field Search');
				infoMap.put('Request',request);
				infoMap.put('Result',result);
			}
			

			ConnectedLog.LogInformation('Find/Add Page','DupeSearchController',methodName,transactionId,'','logging',infoMap);
		}   
}