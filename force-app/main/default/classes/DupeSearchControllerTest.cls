@isTest
public class DupeSearchControllerTest {
    @testSetup
    static void setupTestData() {
        List<Account> accList = new List<Account>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
		accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;		
        accList.add(talentAcc);
        
		Insert accList;
        
        //create the custom setting
			insert new ApigeeOAuthSettings__c(name = 'SOALatLong', Client_Id__c='1234567', Service_Http_Method__c='POST', Service_URL__c='http://allegisgrouptest.com',Token_URL__c='http://test.com');
		
        List<Contact> clientConList = new List<Contact>();
        
        for(Integer i=0; i<10 ; i++) {
            Contact ct = TestData.newContact(accList[0].id, 1, 'Client'); 
            ct.Other_Email__c = 'other'+i+'@testemail.com';
            ct.Title = 'test title'+i;
            ct.MailingState = 'test text'+i;
            
            clientConList.add(ct);
		}
        insert clientConList;
        
        List<Contact> talentConList = new List<Contact>();
        for(Integer i=0; i<10 ; i++) {
            Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
            ct.Other_Email__c = 'other'+i+'@testemail.com';
            ct.Title = 'test title'+i;
            ct.MailingState = 'test text'+i;
            
            talentConList.add(ct);
		}
        insert talentConList;
	}  
     Static testmethod void getCurrentUserWithOwnershipTest(){
        
        Test.startTest();
        	DupeSearchController.getUser();
        	DupeSearchController.getCurrentUserWithOwnership();
         	DupeSearchController.getCountryMappings();
        Test.StopTest();
    }
    static testmethod void searchObjectByName() {
        Test.startTest();        
        	DupeSearchController.searchObjectByName('ABC', '', 'Contact', 'Account.Talent_Profile_Last_Modified_Date__c', 'Talent', false,'abc123');
        Test.stopTest();
    }
    
        static testmethod void queryRecentObject() {
        Test.startTest();        
        	DupeSearchController.queryRecentObject('ABC', '', 'Contact', 'Account.Talent_Profile_Last_Modified_Date__c', 'Talent', false);
        Test.stopTest();
    }

    static testmethod void searchObjectByPhone() {
        Test.startTest();        
            DupeSearchController.searchObjectByPhone('ABC', '', 'Contact', 'Account.Talent_Profile_Last_Modified_Date__c', 'Talent', false);
        Test.stopTest();
    }

    static testmethod void searchObjectByEmail() {
        Test.startTest();        
            DupeSearchController.searchObjectByEmail('ABC', '', 'Contact', 'Account.Talent_Profile_Last_Modified_Date__c', 'Talent', false);
        Test.stopTest();
    }

    static testmethod void queryObject() {
        Test.startTest();        
            DupeSearchController.queryObject('ABC', 'Talent_Id__c=\'1234567\'', 'Contact', 'Account.Talent_Profile_Last_Modified_Date__c', 'Talent', false,'abc123');
        Test.stopTest();
        System.assertEquals(1,1);
    }
    
    static testmethod void searchObjectByPhoneEmailName() {
        Test.startTest();        
            DupeSearchController.searchObjectByPhoneEmailName('ABC', '', 'Contact', 'Account.Talent_Profile_Last_Modified_Date__c', 'Talent', false,'abc123');
        Test.stopTest();
        System.assertEquals(1,1);
    }
    
    static testMethod void generateTransactionID() {
        Test.startTest();
        	String transId = DupeSearchController.generateTransactionID();
        Test.stopTest();
        System.assert(transId != Null);
    }
    
    static testMethod void createUncommittedTalent() {
        Test.startTest();
        	DupeSearchController.createUncommittedTalent();
        Test.stopTest();
    }
    
    static testMethod void addAndRelateTalent() {
        Test.startTest();
            Id conRecTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
            Contact ct = [select id from Contact where recordTypeId =: conRecTypeID LIMIT 1];
            DupeSearchController.addAndRelateTalent(ct.id);
        Test.stopTest();
        
    }
    
    static testMethod void createTalentExperience() {
        Test.startTest();
            Id clinetConRecTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
            Id talentConRecTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
            Contact clientContact = [select id, AccountId, Title, Account.name from Contact where recordTypeId =: clinetConRecTypeID LIMIT 1];
            Contact talentContact = [select id, AccountId from Contact where recordTypeId =: talentConRecTypeID LIMIT 1];
            
            DupeSearchController.createTalentExperience(clientContact, talentContact);
            DupeSearchController.getClientContactData(clientContact.id);
            DupeSearchController.relateTalent(clientContact.id, talentContact.id);
            DupeSearchController.attachResumeToTalent(talentContact.id,talentContact.AccountId);
        	DupeSearchController.talentResumeCount(talentContact.Id);
        	DupeSearchController.searchClientContacts('Test','Account','Name');
        Test.stopTest();
    }
   
    
}