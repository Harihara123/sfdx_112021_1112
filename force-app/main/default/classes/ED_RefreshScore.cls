public class ED_RefreshScore {

    @AuraEnabled
    public static void updateCheck(Id recordId){
        //system.debug('--recordId--'+recordId);
        list<Opportunity> Pl_list = new list<Opportunity>();
        pl_list = [select id, ED_RefreshCount__c from Opportunity where id=:recordId Limit 1];
        Opportunity p =new Opportunity();
        //System.debug('This is the Id' + recordId);
        p.id=pl_list[0].id;
        if(pl_list[0].ED_RefreshCount__c == null){
            p.ED_RefreshCount__c=1;
        }
        else{
            p.ED_RefreshCount__c= pl_list[0].ED_RefreshCount__c + 1;
           
        }
        update p;
    }
}