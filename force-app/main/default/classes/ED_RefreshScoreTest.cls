@isTest(seealldata = false)
public class ED_RefreshScoreTest {
    static testMethod Void Test_updateCheck(){     
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Interest';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                newOpp.Status__c = 'Open';
                newOpp.Location__c = 'Local Field Office';
                newOpp.Organization_Office__c = orgList[0].id;
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys; 
        lstOpportunitys[0].Req_Job_Description__c = 'Qualifying';
        update lstOpportunitys;
        
        Test.StartTest();
        ED_RefreshScore.updateCheck(string.valueof(lstOpportunitys[0].id));
        Test.StopTest(); 
    }
}