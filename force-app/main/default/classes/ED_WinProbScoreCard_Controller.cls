public class ED_WinProbScoreCard_Controller {
  
    @AuraEnabled
    public static Win_Probability_Dashboard_Mapping__mdt getDashboardId(string Division)
    {
        List<Win_Probability_Dashboard_Mapping__mdt> dashboardDetails = [SELECT Dashboard_Id__c,DataSet_Name__c from Win_Probability_Dashboard_Mapping__mdt where Division_Name__c= :Division limit 1];
        //List<Win_Probability_Dashboard_Mapping__mdt> dashboardDetails = [SELECT Dashboard_Id__c from Win_Probability_Dashboard_Mapping__mdt where Division_Id__c= :Division limit 1];
        Win_Probability_Dashboard_Mapping__mdt dashId = dashboardDetails[0];
        return dashId;
    }
}