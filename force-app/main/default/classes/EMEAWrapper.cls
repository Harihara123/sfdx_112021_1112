public class EMEAWrapper {
    public Integer id;	//0
	public String requisition_number;	//123456
	public String return_url;//https://t.co/ret_1234
	public List<cls_specificvalues> specific_values;
	public string BusinessUnit;
	Public cls_position position;
    public cls_company company;
    public cls_candidate_response candidate_response;
   // public clslinks  links;
	Public class cls_specificvalues {
		public String name;
		public String value;
	}
   Public class cls_position {
		public String title;	//Test Job
		public String description;	//This is a test
		public String skills;	//Test skills
		public String education;	//EQUIVALENTEXPERIENCE
		public String benefits;	//Test benefits
		public cls_compensation compensation;
		public cls_location location;
		public String travel_percentage;	//0
		public String telecommute_percentage;	//25
		public cls_classification classification;
	}
    
	Public class cls_compensation {
		public cls_range range;
		public String type;	//FULLTIME
		public String curency;	//USD
	}
   Public class cls_range {
		public Integer min;	//1000
		public Integer max;	//2000
	}
   Public class cls_location {
		public String country;	//US
		public String state;	//US-CA
		public String city;	//San Ramon
		public String zip;	//94583
	}
  Public class cls_company {
		public String name;	//Test Company
		public String description;	//Test Company Description
		public String division;	//
		public String department;	//
		public cls_account account;
	}
   Public class cls_account {
		public String name;	//Test Account
		public String email;	//test@acme.com
		public String organization;	//Test organization
	}
    Public class cls_candidate_response {
		public String url;	//https://t.co/1234
		public String email;	//apply@test.com
	}
    Public class cls_classification {
		public String type;	//PERMANENT
		public String etime;	//FULLTIME
		public String function;	//13000000
		public Integer industry;	//1
	}
    /*
   	Public class clslinks {
	}
    public static EMEAWrapper parse(String json){
		return (EMEAWrapper) System.JSON.deserialize(json, EMEAWrapper.class);
	}
	*/
}