/**********************************************************************
Class Name:  EditReqController
Version     : 1.0 
Function    : Edit the req based on profile permission and stage type
 
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
*Rajkumar					6/19/2013				 Created                                           
******************************************************************************/

public with sharing class EditReqController {
		
		Reqs__c req;
		public EditReqController(ApexPages.StandardController controller) {
			
			Req = (Reqs__c) controller.getRecord();    
            Req = [select id,Stage__c from Reqs__c where Id = :req.id];   
		}
		
		public PageReference redirect() 
    	{
        	pageReference   pg;        	
        	if(Req.Stage__c == 'Staging'){
        		pg = page.Req_Creation;       				
        		pg.getParameters().put('id',Req.id);
        		pg.getParameters().put('RecordType',Label.Req_Staffing_RecordType);
        		pg.getParameters().put('Staging','1'); 
        		pg.getParameters().put('retURL',ApexPages.currentPage().getParameters().get('retURL'));
        		return pg.setRedirect(true);        		
        	}else
        	{       		
				string param = '';
		    	Map<String, String> strMap = ApexPages.currentPage().getParameters();
		    	String[] keys = new String[]{'Id','RecordType', 'retURL', 'cancelURL'};
		    	for(String s : keys){
		    		if(strMap.containsKey(S)) param += s + '=' +  strMap.get(s) + '&';
		    	}
		    	if(param.length() > 0) param = param.substring(0, param.length()-1);
        		return new PageReference('/'+Req.id+'/e?nooverride=1&'+param);
        	}
    	}   
}