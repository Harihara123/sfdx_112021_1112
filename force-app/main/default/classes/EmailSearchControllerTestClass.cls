/******************************************************
 * Description      : Test Class for EmailSearchController
 * @author          : Nikhita Reddy S
 * @since           : September 04, 2012  
 ******************************************************/

@isTest 
public class  EmailSearchControllerTestClass {
    static testMethod void TestEmailSearchController() {
    
        EmailSearchController email= new EmailSearchController();
        
        String AccountName = 'Test email';
               Contact c = new Contact(LastName='Test',Email='abc@xyz.com');
               System.debug('***********'+c.LastName);
               System.debug('***********'+c.email);
              
               insert c;
               c = [select email from Contact where Id = :c.Id];
               System.debug('**********'+c.email);
               System.assertEquals('abc@xyz.com',c.email);
               
               email.doSearch();
    }
}