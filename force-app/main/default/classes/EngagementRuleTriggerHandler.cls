public class EngagementRuleTriggerHandler {

	public void OnAfterInsert(Id clientAccountId){
		 if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
			System.debug('Triggered Afer Insert EngagementRuleTriggerHandler');
			updateTalentAccountModifieDateTime(clientAccountId);
		}
	}

	public void OnAfterUpdate(Id clientAccountId){
		 if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration){
			System.debug('Triggered Afer update EngagementRuleTriggerHandler');
			updateTalentAccountModifieDateTime(clientAccountId);
		 }
	}

	public void OnAfterDelete(Id clientAccountId){
		 if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
			 	System.debug('Triggered Afer Delete EngagementRuleTriggerHandler');
				updateTalentAccountModifieDateTime(clientAccountId);
		 }
	}

	private void updateTalentAccountModifieDateTime(Id clientAccountId){
        System.debug('EngagementRuleTriggerHandler method updateTalentAccountModifieDateTime');
    if(!TriggerStopper.engagementRule){
        List <Account> accountList = new List<Account>();
        
        String talentQuery = 'SELECT Id, Do_Not_recruit_talent__c, Name,Talent_Profile_Last_Modified_Date__c FROM Account WHERE Current_Employer__c = :clientAccountId';
		accountList = Database.query(talentQuery);
		System.debug(accountList);
        
        //Added by Karthik
        for(Account a : accountList){
            a.Talent_Profile_Last_Modified_Date__c = DateTime.now();
        }
        update accountList;
         TriggerStopper.engagementRule = true;
    }
	}

}