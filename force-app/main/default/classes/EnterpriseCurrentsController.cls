public class EnterpriseCurrentsController {
    
 @AuraEnabled
 public static String getEnterpriseCurrentsJSON(string acctId){
     
     talentwrapper talentData = new talentwrapper();
     List<DataWrapper> currents = new List<DataWrapper>();
     List<DataWrapper> formers = new List<DataWrapper>();
     List<order> submittallist = new List<order>();
     List<order> formerList = new List<order>();
     try{
         
         List<Account> acctList = [select Id, Master_Global_AccountID_formula_field__c from Account where Id =: acctId];
         
         
          submittallist = [SELECT Opportunity.OpCo__c ,Opportunity.Owner.Name,Opportunity.OwnerId, Opportunity.Req_Division__c, Opportunity.Req_Hiring_Manager__c,
                                  Opportunity.Req_Hiring_Manager__r.Name ,ShipToContact.Id, ShipToContact.Name, Opportunity.Req_Job_Title__c, 
                                  ShipToContact.Account.Talent_Start_Date__c, ShipToContact.Account.Talent_End_Date__c,Bill_Rate__c, 
                                  ShipToContact.Last_Activity_Date__c from Order
                                  where Account.Master_Global_AccountID_formula_field__c =: acctList[0].Master_Global_AccountID_formula_field__c and Status = 'Started' and ShipToContact.Account.Talent_End_Date__c > LAST_N_DAYS:7 
                                  ORDER BY  ShipToContact.Account.Talent_End_Date__c DESC LIMIT 10];
         
         formerList = [SELECT Opportunity.OpCo__c ,Opportunity.Owner.Name,Opportunity.OwnerId, Opportunity.Req_Division__c, Opportunity.Req_Hiring_Manager__c,
                                  Opportunity.Req_Hiring_Manager__r.Name ,ShipToContact.Id, ShipToContact.Name, Opportunity.Req_Job_Title__c, 
                                  ShipToContact.Account.Talent_Start_Date__c, ShipToContact.Account.Talent_End_Date__c,Bill_Rate__c, 
                                  ShipToContact.Last_Activity_Date__c from Order
                                  where Account.Master_Global_AccountID_formula_field__c =: acctList[0].Master_Global_AccountID_formula_field__c and Status = 'Started' and ShipToContact.Account.Talent_End_Date__c < LAST_N_DAYS:8 
                                  ORDER BY  ShipToContact.Account.Talent_End_Date__c DESC LIMIT 10];
 
 
      if(submittallist.size() > 0){
          for (Order ord: submittallist){
            DataWrapper dw = new DataWrapper();
              dw.opco = String.Valueof(ord.Opportunity.OpCo__c);
              dw.ownerId = String.Valueof(ord.Opportunity.OwnerId);
              dw.ownerName = String.Valueof(ord.Opportunity.Owner.Name);
              dw.division = String.Valueof(ord.Opportunity.Req_Division__c);
              dw.hiringmanager = String.Valueof(ord.Opportunity.Req_Hiring_Manager__r.Name);
              dw.hiringmanagerId = String.Valueof(ord.Opportunity.Req_Hiring_Manager__c);
              dw.candidatename = String.Valueof(ord.ShipToContact.Name);
              dw.candidateId = String.Valueof(ord.ShipToContact.Id);
              dw.jobtitle =  String.isBlank(ord.Opportunity.Req_Job_Title__c) ? '': String.Valueof(ord.Opportunity.Req_Job_Title__c);
              dw.startdate = ord.ShipToContact.Account.Talent_Start_Date__c == null ? '': String.Valueof(ord.ShipToContact.Account.Talent_Start_Date__c);
              dw.enddate = ord.ShipToContact.Account.Talent_End_Date__c == null ? '' : String.Valueof(ord.ShipToContact.Account.Talent_End_Date__c);
              dw.billrate = String.Valueof(ord.Bill_Rate__c);
              currents.add(dw);
          }
          talentData.entcurrentTalents = currents;
      }
         
       if(formerList.size() > 0){
          for (Order ord: formerList){
             DataWrapper dw = new DataWrapper();
              dw.opco = String.Valueof(ord.Opportunity.OpCo__c);
              dw.ownerId = String.Valueof(ord.Opportunity.OwnerId);
              dw.ownerName = String.Valueof(ord.Opportunity.Owner.Name);
              dw.division = String.Valueof(ord.Opportunity.Req_Division__c);
              dw.hiringmanager = String.Valueof(ord.Opportunity.Req_Hiring_Manager__r.Name);
              dw.hiringmanagerId = String.Valueof(ord.Opportunity.Req_Hiring_Manager__c);
              dw.candidatename = String.Valueof(ord.ShipToContact.Name);
              dw.candidateId = String.Valueof(ord.ShipToContact.Id);
              dw.jobtitle =  String.isBlank(ord.Opportunity.Req_Job_Title__c) ? '': String.Valueof(ord.Opportunity.Req_Job_Title__c);
              dw.startdate = ord.ShipToContact.Account.Talent_Start_Date__c == null ? '': String.Valueof(ord.ShipToContact.Account.Talent_Start_Date__c);
              dw.enddate = ord.ShipToContact.Account.Talent_End_Date__c == null ? '' : String.Valueof(ord.ShipToContact.Account.Talent_End_Date__c);
              dw.billrate = String.Valueof(ord.Bill_Rate__c);
              formers.add(dw);
          }
         talentData.entformerTalents = formers;
         talentData.masterGlobalAccountId = acctList[0].Master_Global_AccountID_formula_field__c;
      }
         
     }catch(Exception e)
    {
      System.debug('Error Response: ' + e.getMessage());
    }
   return System.json.serialize(talentData);
 }  
    
 public class DataWrapper{
       @AuraEnabled
       public String opco;
       @AuraEnabled
       public String ownerId;
       @AuraEnabled
       public String ownerName;
       @AuraEnabled
       public String division;
       @AuraEnabled
       public String hiringmanager;
       @AuraEnabled
       public String hiringmanagerId;
       @AuraEnabled
       public String candidatename;
       @AuraEnabled
       public String candidateId;
       @AuraEnabled
       public String jobtitle;
       @AuraEnabled
       public String startdate;
       @AuraEnabled
       public String enddate;
       @AuraEnabled
       public String billrate;
       @AuraEnabled
       public String lastactivitydate;
   }
    
 public class talentwrapper{
        @AuraEnabled
        public List<DataWrapper> entcurrentTalents;
        @AuraEnabled
        public List<DataWrapper> entformerTalents;
        @AuraEnabled
        public String masterGlobalAccountId;
    }

}