public class EnterpriseReqExtension {

    private final Opportunity opp;
    public List<Req_Skill__c> skills{get;set;}
    public Req_Skill__c skill{get;set;}
    public Job_Title__c jobtitle {get;set;}
    public String skillParamName {get;set;}
    public List<Req_Skill__c> tobeInsertedSkills {get;set;}
    public String levelParamName {get;set;}
    public String jobTitleValue {get;set;}
    public String newJobTitleValue {get; set;}
    public boolean contractflag {get;set;}
    public boolean permflag {get;set;}
    public String skillLabel {get;set;}
    public static String toDelIdent {get; set;}
    public Id startId {get; set;}
    
    public EnterpriseReqExtension (ApexPages.StandardController stdController){
        
        this.opp = (Opportunity)stdController.getRecord();
        Id AccId = System.currentPageReference().getParameters().get('accID');
        Id contId = System.currentPageReference().getParameters().get('conId');
        skillLabel = 'Add New Skill';
        
        jobtitle = new Job_Title__c();
        List<User> usrList = [select Id,CompanyName, Office__c from User where Id =: UserInfo.getUserId()];
        
        permflag = false;
        contractflag = false;
         
        if(usrList.size() > 0){
          this.opp.OpCo__c = usrList[0].CompanyName ;
          
          if(!string.isblank(usrList[0].Office__c)){
            List<User_Organization__c> usrOrgs = [select Id from User_Organization__c where Name =: usrList[0].Office__c];
            this.opp.Organization_Office__c = usrOrgs.size() > 0 ? usrOrgs[0].Id : null;
          }
          
        } 
        
        
        if(AccId != null){ 
            List<Account> accounts = [SELECT Name, Id, Account_Street__c, Account_City__c,Account_State__c, 
                                              Account_Country__c, Account_Zip__c FROM Account WHERE Id =:AccId]; 
            if (!accounts.isEmpty()) { //if the query resulted in a record
                this.opp.AccountId = accounts[0].Id;
                this.opp.StageName = 'Draft';
                startId = this.opp.AccountId;
                this.opp.Req_Worksite_Street__c = accounts[0].Account_Street__c;
                this.opp.Req_Worksite_City__c = accounts[0].Account_City__c;
                this.opp.Req_Worksite_State__c = accounts[0].Account_State__c;
                this.opp.Req_Worksite_Country__c = accounts[0].Account_Country__c;
                this.opp.Req_Worksite_Postal_Code__c  = accounts[0].Account_Zip__c;
          }
        }else if(contId != null){
               List<Contact> contacts = [SELECT Name, Id, AccountId, Account.Account_Street__c,Account.Account_City__c,
                                         Account.Account_State__c,Account.Account_Country__c, Account.Account_Zip__c
                                         FROM Contact WHERE Id =:contId];
               if(!contacts.isEmpty()){
                  this.opp.AccountId = contacts[0].AccountId;
                  this.opp.Req_Hiring_Manager__c = contacts[0].Id;
                  
                  startId = contacts[0].Id;
                  this.opp.StageName = 'Draft';
                  
                    this.opp.Req_Worksite_Street__c = contacts[0].Account != null ? contacts[0].Account.Account_Street__c : '';
                    this.opp.Req_Worksite_City__c = contacts[0].Account != null ? contacts[0].Account.Account_City__c : '';
                    this.opp.Req_Worksite_State__c = contacts[0].Account != null ? contacts[0].Account.Account_State__c : '';
                    this.opp.Req_Worksite_Country__c = contacts[0].Account != null ? contacts[0].Account.Account_Country__c : '';
                    this.opp.Req_Worksite_Postal_Code__c  = contacts[0].Account != null ? contacts[0].Account.Account_Zip__c : '';
                  
               }
     }
     
      this.opp.RecordTypeId = '01224000000kMQ4AAM';
      this.opp.OwnerId = Userinfo.getUserId();
      
       skills = new List<Req_Skill__c>();
        tobeInsertedSkills  = new List<Req_Skill__c>();
        skills.add(new Req_Skill__c());
     
    }
    
    public void delSkill(){
     Integer toDelPos=-1;
     for (Integer idx=0; idx<skills.size(); idx++)
     {
       if(skills[idx].Skill__c == toDelIdent)
       {
         toDelPos=idx;
         break;
       }else if(string.isblank(skills[idx].Skill__c)){
         toDelPos=idx;
         break;
       }
     }
      
      if (-1!=toDelPos)
      {
       skills.remove(toDelPos);
      }
    
    }
    
    public PageReference  hideSectionOnChange()
    {
       System.debug('EVENT'+opp.Req_Product__c + 'FLAGS'+ permflag);
        
        if(opp.Req_Product__c  == null){
          contractflag = false;
          permflag = false;
        }else if(opp.Req_Product__c == 'Contract'){
          permflag = false;
          contractflag = true;
        }else if(opp.Req_Product__c == 'Permanent'){
          permflag = true;
          contractflag = false;
        }else{
          contractflag = true;
          permflag = true;
        } 
            
       return null;
    }
   
     public void addNewSkill()
     {
       //PageReference 
       skills.add(new Req_Skill__c());
       //return null;
     }
     
     public Pagereference setJobTitle(){
     
        return null;
     }
     
     public boolean isValidated(Opportunity op){
      Boolean isDone = true;
       if(op != null){
               if(string.isblank(jobTitleValue)){
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Job Title is Required'));
                 isDone = false;
               }    
       }
       return isDone;
     }
     
     public List<Req_Skill__c> validatedSkills(List<Req_Skill__c> sks){
        Boolean isValidated = true;
        if(sks.size() > 0){
        System.debug('SKILLS'+sks);
           for(Req_Skill__c r: sks){
               if(!string.isblank(r.Skill__c))
                 tobeInsertedSkills.add(r);
             }
       }
      return tobeInsertedSkills;
     }
     
     public PageReference doCancel(){
        PageReference p = new PageReference('/' + startId);
        p.setRedirect(true);
        return p;
     }
     
     public PageReference save()
     {
      PageReference p = null;
      List<Req_Skill__c> tobeInsertedSkills = new List<Req_Skill__c>();
      
       System.debug('NEW TITLE'+newJobTitleValue);
      
       System.debug('TITLE SAVE'+jobTitleValue);
      
        // set up a transaction so that we can roll back in the event anything fails
        System.Savepoint sp1 = Database.setSavepoint();
     
        try{
           if(isValidated(opp)){
                
             //Query Job Title  
              List<Job_Title__c> jts = [select Id from Job_Title__c where Name =: jobTitleValue LIMIT 1];
              //Save Job Title 
              if(jts.size() > 0){
                 opp.Req_Client_Job_Title__c = jts[0].Id;
               }else{
                Job_Title__c jobTitle = new Job_Title__c();
                jobTitle.Name = jobTitleValue;
                jobTitle.Active__c = true;
                  
                insert jobTitle;
                opp.Req_Client_Job_Title__c = jobTitle.Id;
               }
              
              insert opp;
              
              if(skills != null && skills.size() > 0){
                 for(Req_Skill__c r: skills){
                  if(!string.isblank(r.Skill__c))
                       r.Req__c = opp.Id;
                 }
               }
               
              tobeInsertedSkills = validatedSkills(skills);
               
              if(tobeInsertedSkills.size() >= 1){
                 
                 insert tobeInsertedSkills;
              
                 p = new PageReference('/' + opp.Id);
                 p.setRedirect(true);
               
               }else if(tobeInsertedSkills.size() == 0){
                 
                 p = new PageReference('/' + opp.Id);
                 p.setRedirect(true);
               }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Skill is Required'));
                Database.rollback(sp1);
                p = null;
             }
         }
           
      }catch(DmlException ex){
        ApexPages.addMessages(ex);
        // something failed, roll back the entire transaction
         System.debug('OPP'+ex);
          Database.rollback(sp1);
          p = null;
      } 
      return p;
     }
    
}