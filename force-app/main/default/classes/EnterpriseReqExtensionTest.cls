@isTest
private class EnterpriseReqExtensionTest
{
     static User user = TestDataHelper.createUser('System Administrator');
     
    static testMethod void Test_EnterpriseReqExtensionAccount()
    {
        List<Opportunity> lstOpportunity = new List<Opportunity>();        
        
        // insert OA Calculator Data
        TestDataHelper.createOACaculatorData();
        Test.startTest(); 
       
        if(user != Null) 
        {
            system.runAs(user) 
            {    
                
                TestData TdAcc = new TestData(1);
                testdata tdcont = new Testdata();
                // Get the TEK recordtype Id from a name        
                string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                Account ac;
                
                for(Account Acc : TdAcc.createAccounts()) 
                {          
                    ac = new Account(id = Acc.Id);           
                    Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                         RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                        Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly');  
                        
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                }                
                // Insert list of Opportunity
                //insert lstOpportunity;  
                
                string rectype='Talent';
                List<Contact> TalentcontLst=tdcont.createContacts(rectype);
                
                Job_Title__c jt = new Job_Title__c(Name ='Developer');
                insert jt;
                
                PageReference pageRef = Page.EnterpriseReqVF;
                Test.setCurrentPage(pageRef);
                //pageRef.getParameters().put('Id', String.valueOf(lstOpportunity[0].Id));
                pageRef.getParameters().put('accID', String.valueOf(ac.Id));
                ApexPages.StandardController sc = new ApexPages.StandardController(lstOpportunity[0]);
                EnterpriseReqExtension testExtController = new EnterpriseReqExtension(sc);
                
                testExtController.jobTitleValue = 'Developer';
                testExtController.setJobTitle();
                testExtController.delSkill();
                testExtController.hideSectionOnChange();
                testExtController.addNewSkill();
                testExtController.isValidated(lstOpportunity[0]);
                testExtController.doCancel();
                testExtController.save();
            }
        }
    }
    
    static testMethod void Test_EnterpriseReqExtensionContact()
    {
        List<Opportunity> lstOpportunity = new List<Opportunity>();        
        
        // insert OA Calculator Data
        TestDataHelper.createOACaculatorData();
        Test.startTest(); 
       
        if(user != Null) 
        {
            system.runAs(user) 
            {    
                
                TestData TdAcc = new TestData(1);
                testdata tdcont = new Testdata();
                // Get the TEK recordtype Id from a name        
                string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                Account ac;
                
                for(Account Acc : TdAcc.createAccounts()) 
                {          
                    ac = new Account(id = Acc.Id);           
                    Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                         RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                        Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Permanent',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly');  
                        
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                }                
                // Insert list of Opportunity
                //insert lstOpportunity;  
                
                string rectype='Talent';
                List<Contact> TalentcontLst=tdcont.createContacts(rectype);
                
                Job_Title__c jt = new Job_Title__c(Name ='Developer');
                insert jt;
                
                PageReference pageRef = Page.EnterpriseReqVF;
                Test.setCurrentPage(pageRef);
                //pageRef.getParameters().put('Id', String.valueOf(lstOpportunity[0].Id));
                pageRef.getParameters().put('conId', String.valueOf(TalentcontLst[0].Id));
                ApexPages.StandardController sc = new ApexPages.StandardController(lstOpportunity[0]);
                EnterpriseReqExtension testExtController = new EnterpriseReqExtension(sc);
                
                testExtController.jobTitleValue = 'Developer';
                testExtController.setJobTitle();
                testExtController.delSkill();
                testExtController.hideSectionOnChange();
                testExtController.addNewSkill();
                testExtController.isValidated(lstOpportunity[0]);
                testExtController.doCancel();
                testExtController.save();
            }
        }
    }
}