public with sharing class EnterpriseReqSkills{

@AuraEnabled
 public static CRMSkillsModel getEnterpriseReqSkills(String oppId){
    
    CRMSkillsModel model = new CRMSkillsModel();
    
    List<Object> jsonList = new List<Object>();
    //Map<String,String> skillMap = new Map<String,String>();
   
    Opportunity opp = [select EnterpriseReqSkills__c from Opportunity where Id=: oppId];
    if(opp != null && !String.IsBlank(opp.EnterpriseReqSkills__c)){
      jsonList = (List<Object>)JSON.deserializeUntyped(opp.EnterpriseReqSkills__c);
    }
    
    System.debug('ENTER'+jsonList);
    model.skillList = jsonList;
    
     User CUser = [Select Id,Opco__c from User where Id =:UserInfo.getUserId()];
     Talent_Role_to_Ownership_Mapping__mdt Ownership = [SELECT Opco_Code__c,Talent_Ownership__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: CUser.OPCO__c];
     model.Opco =  Ownership.Talent_Ownership__c;  
    
    return model;
  }
  
 @AuraEnabled
  public static CRMSkillsModel updateSkillsforReq(String oppId, String skills){
   List<String> result = new List<String>();
   List<Object> jsonList = new List<Object>();
    CRMSkillsModel model = new CRMSkillsModel();
    
    if(!String.IsBlank(oppId)){
         Opportunity opp = [select EnterpriseReqSkills__c from Opportunity where Id=: oppId];
         if(opp != null){
           opp.EnterpriseReqSkills__c = skills;
           opp.Apex_Context__c = true;
           Update opp;
           
        String skillsStr ;
        if(opp != null && !String.IsBlank(opp.EnterpriseReqSkills__c)){
            jsonList = (List<Object>)JSON.deserializeUntyped(opp.EnterpriseReqSkills__c);
        }
       
      }   
    }
    model.skillList = jsonList;
    User CUser = [Select Id,Opco__c from User where Id =:UserInfo.getUserId()];
     Talent_Role_to_Ownership_Mapping__mdt Ownership = [SELECT Opco_Code__c,Talent_Ownership__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: CUser.OPCO__c];
     model.Opco =  Ownership.Talent_Ownership__c;  
     
    return model;
  }

}