//Author: Preetham Uppu
//Enterprise Req to Legacy Team Members Sync

public class EnterprisetolegacyTeamMembersUtil{

  public Map<Id, Opportunity> EntMap;
   Set<Id> EntReqIds = new Set<Id>();
   public List<Reqs__c> Reqlst;
   List<opportunityTeamMember> lstoppTeamMembers = new List<opportunityTeamMember>();
   public Set<Id> errorTeamMemberIdSet = new Set<Id>();
   Map<Id,String> errorLogMap = new Map<Id,String>();
   List<String> errorLst = new List<String>();
   public Map<Id,String> insertErrorsMap = new Map<Id,String>();
   public Map<Id,String> exceptionMap = new Map<Id,String>();
   
  public EnterprisetolegacyTeamMembersUtil(List<opportunityTeamMember> lstQueriedoppTeamMembers){
    lstoppTeamMembers = lstQueriedoppTeamMembers;
  }

   public ReqSyncErrorLogHelper CopyEnterprisetoLegacyMembers() {
     Map<String,Id> EntReqMap = new Map<String,Id>();
     Set<String> reqSet = new Set<String>();
     if(lstoppTeamMembers.size() > 0){
       for(OpportunityTeamMember oTm: lstoppTeamMembers){
          if(oTm.Opportunity.Req_origination_System_Id__c != null)
              reqSet.add(oTm.Opportunity.Req_origination_System_Id__c);
       }
     }
     
     List<Req_Team_Member__c> ReqTeamLst = new List<Req_Team_Member__c>();
      //Query for Reqs
      Map<Id,Reqs__c> Reqs = new Map<Id,Reqs__c>([select Id, Siebel_ID__c  from Reqs__c where Siebel_ID__c =: reqSet]);
      //Loop through Reqs to Create Map
      for(Reqs__c Rq : Reqs.values()){
          EntReqMap.put(Rq.Siebel_ID__c, Rq.Id);
      }
      
      //Create Req Team Members from Opp Team Members
      if(lstoppTeamMembers.size() > 0 && EntReqMap.size() > 0){
        for(OpportunityTeamMember OppTm: lstoppTeamMembers){
           if(EntReqMap.containskey(OppTm.Opportunity.Req_origination_System_Id__c)){
              Id Reqid =  EntReqMap.get(OppTm.Opportunity.Req_origination_System_Id__c);
               Req_Team_Member__c  ReqTeamMember =new Req_Team_Member__c(user__c=oppTm.userid,Requisition_Team_Role__c=OppTm.teammemberrole,Requisition__c=Reqid);
               ReqTeamLst.add(ReqTeamMember);
           } 
        }
      }
      
      //Insert Opp Team Members
      if(ReqTeamLst.size() > 0){
                 List<database.SaveResult> results = database.insert(ReqTeamLst,false);
                 
                 for(Integer j=0; j < ReqTeamLst.size(); j++){
                   String errorRecord = 'Failed Team Member Sync ';
                    if(!results[j].isSuccess()){
                      errorRecord += ReqTeamLst[j].user__c  +' \r\n';
                      insertErrorsMap.put(ReqTeamLst[j].user__c,errorRecord);
                    }
                 }
               //Insert the Error Log Map     
                  if(insertErrorsMap.size() > 0)
                      Core_Data.logBatchRecord('Enterprise to Legacy Team Members', insertErrorsMap);
       }
      
       ReqSyncErrorLogHelper errorWrapper = new ReqSyncErrorLogHelper(exceptionMap,errorLst);
       return errorWrapper;
      
   } // End of Copy method

}