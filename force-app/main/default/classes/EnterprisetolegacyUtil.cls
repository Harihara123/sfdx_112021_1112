//Author: Hareesh Achanta
//Last made change on product hierarchy piece
// Preetham Uppu --- Req Sync Batch Chaining
public class EnterprisetolegacyUtil{

  Set<Id> EntReqIds = new Set<Id>();
  Public Map<Id,Id> EntReqIds2 = new Map<Id,Id>();
  public Map<Id, Opportunity> EntMap;
  public Map<Id, product2> Productmap;
  public List<Reqs__c> Reqlst;
  Map<Id,Id> Reqmapchk = new Map<Id,Id>();
  Public  Map<Id,Id> Reqmap;
  List<Log__c> errorLogs = new List<Log__c>();
  public Set<Id> errorIdSet = new Set<Id>();
  public Map<Id,String> errorLogMap = new Map<Id,String>();
  public Map<Id,String> errorLogMap2 = new Map<Id,String>();
  public Map<Id,String> exceptionMap = new Map<Id,String>();
  public Map<Id,String> insertErrorsMap = new Map<Id,String>();
  List<String> errorLst = new List<String>();
  
   Public string Stagerecdtypeid=Schema.SObjectType.Reqs__c.getRecordTypeInfosByName().get('Staging Req').getRecordTypeId();
   public string Staffrecdtypeid=Schema.SObjectType.Reqs__c.getRecordTypeInfosByName().get('Staffing Req').getRecordTypeId();
   public string TGSrecordTypeid=Schema.SObjectType.Reqs__c.getRecordTypeInfosByName().get('Global Services Req').getRecordTypeId();

 
 public EnterprisetolegacyUtil(Set<Id> EntIds){
 
     EntReqIds = EntIds;
     Productmap = new map<id,Product2>([select id,opco__c,Division_Name__c,Segment__c from product2 where Segment__c='Enterprise Req']);
     EntMap =  new Map<Id, Opportunity>([select id,Accountid,Req_Client_Job_Title__r.name,Req_Hiring_Manager__c,Req_Product__c,StageName,Req_Total_Positions__c,Req_Worksite_City__c,
                                         Req_Worksite_Street__c,Req_Worksite_Postal_Code__c,Req_Skill_Details__c,Req_Origination_System__c,
                                         Organization_Office__c,Start_Date__c,Req_Worksite_State__c,Req_Job_Description__c,
                                         CloseDate,Req_Worksite_Country__c,Req_Background_Check_Required__c,Req_Payroll__c,Req_Bill_Rate_Max__c,Req_Bill_Rate_Min__c,Req_Additional_Information__c,Req_RRC_Open_Date__c,Req_Date_Client_Opened_Position__c,Req_Decline_Reason__c,Req_EVP__c,Req_External_Job_Description__c,Req_Flat_Fee__c,Req_Impact_to_Internal_External_Customer__c,
                                         Req_Can_Use_Approved_Sub_Vendor__c,Req_Compliance__c,Req_Drug_Test_Required__c,Req_Duration__c,Req_Origination_Partner_System_Id__c,Req_Secondary_Delivery_Office__c,Req_Competition_Info__c,
                                         Req_Duration_Unit__c,Req_Exclusive__c,Req_Government__c,Immediate_Start__c,Req_Loss_Wash_Reason__c,Req_OFCCP_Required__c,Req_RRC_Positions__c,Req_Interview_Date__c,
                                         Ownerid,Req_Pay_Rate_Max__c,Req_Pay_Rate_Min__c,Req_Qualification__c,Req_Red_Zone__c,Req_Salary_Max__c,Req_Salary_Min__c,Req_Delivery_Type__c,Req_Business_Challenge__c,Req_Max_Submissions_Per_Supplier__c,Req_Merged_Req_ID__c,Req_Merged_Req_System__c,
                                         Req_Internal_External_Customer__c,Req_Why_Position_Open_Details__c,Req_Work_Environment__c,Opportunity_Num__c,Req_End_Client__c,Req_Proactive_Req_Link__c,Req_Opportunity__c,
                                         Req_Merged_with_VMS_Req__c,REQ_Opportunity_driven_mostly_by_LOB__c,Req_System_SourceReq__c,OT_Multiplier__c,Legacy_Record_Type__c ,
                                         Req_Approval_Process__c,Req_Service_Product_Offering__c,Req_SourceReq__c,Req_Template__c,Req_Is_Auto_Closed__c,Req_Disposition__c,
                                         Req_Performance_Expectations__c,Req_Project_Stage_Lifecycle_Info__c,Req_Qualified_Date__c,Req_Draft_Reason__c,
                                         Req_Search_Tags_Keywords__c,Req_Standard_Burden__c,Req_Total_Filled__c,Req_Total_Lost__c,Req_Total_Washed__c,CreatedByID,Req_Client_working_on_Req__c,Req_Interview_Information__c,
                                         CreatedDate,LastModifiedByID,LastModifiedDate,Req_Export_Controls_Questionnaire_Reqd__c,Legacy_Product__c,EnterpriseReqSkills__c,
                                         Req_origination_System_Id__c,BusinessUnit__c,OpCo__c,Req_Legacy_Req_Id__c,Req_Job_Title__c,Req_OPT__c, Is_International_Travel_Required__c, National__c, Internal_Hire__c, Backfill__c, Employment_Alignment__c, Final_Decision_Maker__c, GlobalServices_Location__c,Req_Qualifying_Stage__c
                                         from opportunity where id IN: EntReqIds]);

     system.debug(EntMap);
 }
    
    
   public ReqSyncErrorLogHelper CopyEnterprisetoLegacy() {
       
       Map<String,String> ReqMap = new Map<String,String>();
       List<Req_Team_Member__c> ReqTeamLst = new List<Req_Team_Member__c>();
       
       Reqlst =  [select id, EnterpriseReqId__c from Reqs__c where EnterpriseReqId__c in:EntreqIds ];
   
  if(Reqlst.size()>0){
     for(Reqs__c rq :Reqlst){
         Reqmapchk.put(rq.EnterpriseReqId__c,rq.id);
         Reqmap.put(rq.id,rq.EnterpriseReqId__c);
     }
     }
   List<Reqs__c>Legacyreqs = new List<Reqs__c>();
   List<Reqs__c>Legacyreqs2 = new List<Reqs__c>();
   List<Log__c> errors = new List<Log__c>();
      
        if(EntMap.size()>0){
            for(Opportunity opp :EntMap.values()){
                Reqs__c req=new Reqs__c();
                if(opp.Req_Legacy_Req_Id__c!=null || (Reqmapchk.size()>0 && Reqmapchk.containskey(opp.id))){
                    req.id = opp.Req_Legacy_Req_Id__c!=null ? opp.Req_Legacy_Req_Id__c :Reqmapchk.get(opp.id);
                }
                
                req.Account__c=opp.Accountid;
                req.CLIENT_JOB_TITLE__C=opp.Req_Job_Title__c;
                req.PRIMARY_CONTACT__C=opp.Req_Hiring_Manager__c;
                req.POSITIONS__C=opp.Req_Total_Positions__c;
                req.FILLED__C=opp.Req_Total_Filled__c;
                req.LOSS__C = opp.Req_Total_Lost__c;
                req.WASH__C = opp.Req_Total_Washed__c;
                
                if(!String.isBlank(opp.Internal_Hire__c) && string.valueof(opp.Internal_Hire__c) == 'Yes'){
                    req.Placement_Type__c = 'Internal Hire';
                }else if(string.valueof(opp.Internal_Hire__c) == 'No' || string.isblank(opp.Internal_Hire__c) || string.valueof(opp.Internal_Hire__c) == '--None--'){
                    if(opp.Req_Product__c=='Contract'||opp.Req_Product__c=='Contract to Hire'){
                        req.PLACEMENT_TYPE__C=opp.Req_Product__c;
                    }else if(opp.Req_Product__c=='Permanent'){
                        req.PLACEMENT_TYPE__C='Direct Placement';
                    }
                }
                
                if(opp.stageName=='Staging' && opp.Req_Decline_Reason__c==null){
                    req.stage__c='Staging';
                    req.status__c='Open';     
                 }
                
                if(opp.StageName=='Closed Wash' && opp.Req_Decline_Reason__c!=null){
                    req.stage__c='Staging';
                    req.status__c='Closed';
                }                
                
                 if(opp.stageName=='Draft'){
                    req.stage__c='Draft';
                    req.status__c='Open';
                    //req.Draft_Reason__c='Other';
                    //req.Draft_Other_Reason__c='Enterprise Req';
                 }
                 if(opp.stageName=='Qualified' || opp.stageName=='Presenting' || opp.stageName=='Interviewing' || opp.stageName=='Pending Start'){                
                    req.stage__c='Qualified';
                    req.status__c='Open';
                 }
                if(opp.stageName=='Closed Lost'){
                    if(!String.isBlank(opp.Req_Qualifying_Stage__c) && opp.Req_Qualifying_Stage__c!= 'RedZone'){
                        req.stage__c= opp.Req_Qualifying_Stage__c;
                    }
                    else{
                        req.stage__c= 'Qualified';
                    }
                    req.status__c='Closed';
                    req.Loss__c = opp.Req_Total_Lost__c;
                    req.Wash__c= opp.Req_Total_Washed__c;
                    req.Filled__c=0;                     
                }
                if(opp.stageName=='Closed Wash' && opp.Req_Decline_Reason__c==null){
                    if(!String.isBlank(opp.Req_Qualifying_Stage__c) && opp.Req_Qualifying_Stage__c!= 'RedZone'){
                        req.stage__c= opp.Req_Qualifying_Stage__c;
                    }
                    else{
                        req.stage__c= 'Qualified';
                    }
                    req.status__c='Closed';
                    req.Loss__c = opp.Req_Total_Lost__c;
                    req.Wash__c= opp.Req_Total_Washed__c;
                    req.Filled__c=0;
                }
                
                if(opp.stageName=='Closed Won'){
                    if(!String.isBlank(opp.Req_Qualifying_Stage__c) && opp.Req_Qualifying_Stage__c!= 'RedZone'){
                        req.stage__c= opp.Req_Qualifying_Stage__c;
                    }
                    else{
                        req.stage__c= 'Qualified';
                    }
                    req.status__c='Closed';
                    req.Filled__c = opp.Req_Total_Positions__c;
                 }
                
                if( opp.stageName=='Closed Won (Partial)'){
                    if(!String.isBlank(opp.Req_Qualifying_Stage__c) && opp.Req_Qualifying_Stage__c!= 'RedZone'){
                        req.stage__c= opp.Req_Qualifying_Stage__c;
                    }
                    else{
                        req.stage__c= 'Qualified';
                    }
                    req.status__c='Closed';
                    req.Filled__c = opp.Req_Total_Filled__c;
                    req.Loss__c =opp.Req_Total_Lost__c;
                    req.Wash__c=opp.Req_Total_Washed__c;
                 }
                
                req.CITY__C = opp.Req_Worksite_City__c;
                req.ADDRESS__C=opp.Req_Worksite_Street__c;
                //req.ZIP__C=opp.Req_Worksite_Postal_Code__c;
                
                 String worksiteZipCode;
                if(!string.isBlank(opp.Req_Worksite_Postal_Code__c)){
                 worksiteZipCode= String.valueOf(opp.Req_Worksite_Postal_Code__c);
                 if(worksiteZipCode.length() > 30){
                    worksiteZipCode= worksiteZipCode.substring(0,30);                 
                    req.ZIP__C = string.valueOf(worksiteZipCode);
                 }else{
                      req.ZIP__C=opp.Req_Worksite_Postal_Code__c;
                 }
                 }
                 
                string JobDescription;
                if(!string.isBlank(opp.Req_Job_Description__c)){
                 JobDescription = String.valueOf(opp.Req_Job_Description__c);
                if(JobDescription.length() > 32000){
                    JobDescription = JobDescription.substring(0,32000);                 
                    req.JOB_DESCRIPTION__C = string.valueOf(JobDescription);
                 }else{
                   req.JOB_DESCRIPTION__C=opp.Req_Job_Description__c;
                 }
                }
                
                req.ORGANIZATION_OFFICE__C=opp.Organization_Office__c;
                if(opp.Start_Date__c!=null){
                    req.START_DATE__C=opp.Start_Date__c;
                }
                else{
                    req.START_DATE__C= opp.CloseDate;
                }
                
                //Additional E2L Fields
                //Req_Additional_Information__c
                   if(!String.isBlank(opp.Req_Additional_Information__c)){
                 req.Additional_Information__c  = opp.Req_Additional_Information__c;
                 }
                 
                 //Req_Secondary_Delivery_Office__c
                 if(opp.Req_Secondary_Delivery_Office__c !=null){
                 req.Secondary_Delivery_Office__c  = opp.Req_Secondary_Delivery_Office__c;
                 }
                 
                 //Req_RRC_Open_Date__c
                  if(opp.Req_RRC_Open_Date__c !=null){
                 req.RRC_Open_Date__c  = opp.Req_RRC_Open_Date__c;
                 }
                 
                 //Req_RRC_Positions__c
                 if(opp.Req_RRC_Positions__c !=null){
                 req.RRC_Positions__c  = opp.Req_RRC_Positions__c;
                 }
                 
                 //Req_Delivery_Type__c
                  if(opp.Req_Delivery_Type__c !=null){
                 req.Delivery_Type__c  = opp.Req_Delivery_Type__c;
                 }
                 
                 //Req_Business_Challenge__c
                     if(!string.isBlank(opp.Req_Business_Challenge__c)){
                    if(opp.Req_Business_Challenge__c.length() > 2000){
                       req.Business_Challenge__c  = opp.Req_Business_Challenge__c.substring(0,2000);
                     }else{
                       req.Business_Challenge__c  = opp.Req_Business_Challenge__c;
                       }
                 }else{
                      req.Business_Challenge__c  = opp.Req_Business_Challenge__c;
                 }
                 
                 //Req_Client_working_on_Req__c
                  if(opp.Req_Client_working_on_Req__c !=null){
                 req.Client_working_on_Req__c  = opp.Req_Client_working_on_Req__c;
                 }
                 
                 //Req_Competition_Info__c
                  if(!String.isBlank(opp.Req_Competition_Info__c)){
                 req.Competition_Info__c  = opp.Req_Competition_Info__c;
                 }
                 
                 //Req_Date_Client_Opened_Position__c
                  if(opp.Req_Date_Client_Opened_Position__c !=null){
                 req.Date_Customer_Opened_Position__c  = opp.Req_Date_Client_Opened_Position__c;
                 }
                 
                 //Req_Decline_Reason__c
                 if(opp.Req_Decline_Reason__c !=null){
                 req.Decline_Reason__c  = opp.Req_Decline_Reason__c;
                 }
                 
                 if(String.isNotBlank(opp.Req_Disposition__c)){
                  req.Req_Disposition__c = opp.Req_Disposition__c;
                 }

                 //Req_EVP__c
                  if(!String.isBlank(opp.Req_EVP__c)){
                 req.EVP__c  = opp.Req_EVP__c;
                 }
                 
                 //Req_Interview_Date__c
                 if(opp.Req_Interview_Date__c !=null){
                 req.Interview_Date__c  = opp.Req_Interview_Date__c;
                 }
                 
                 //Req_External_Job_Description__c
                 if(!String.isBlank(opp.Req_External_Job_Description__c)){
                 req.External_Job_Description__c  = opp.Req_External_Job_Description__c;
                 }
                 
                 //Req_Flat_Fee__c
                 if(opp.Req_Flat_Fee__c !=null){
                 req.Direct_Placement_Fee__c  = opp.Req_Flat_Fee__c;
                 }
                 
                 //Req_Impact_to_the_Internal_External_Customer__c
                 if(!String.isBlank(opp.Req_Impact_to_Internal_External_Customer__c)){
                 req.Impact_to_the_Internal_External_Customer__c  = opp.Req_Impact_to_Internal_External_Customer__c;
                 }
                 
                 //Req_Interview_Information__c
                  if(!String.isBlank(opp.Req_Interview_Information__c)){
                 req.Interview_Information__c  = opp.Req_Interview_Information__c;
                 }
                 
                 //Req_Max_Submissions_Per_Supplier__c
                 if(opp.Req_Max_Submissions_Per_Supplier__c !=null){
                 req.Max_Submissions_Per_Supplier__c  = opp.Req_Max_Submissions_Per_Supplier__c;
                 }
                 
                 //Req_Merged_Req_ID__c
                 if(!String.isBlank(opp.Req_Merged_Req_ID__c)){
                 req.Merged_Req_ID__c  = opp.Req_Merged_Req_ID__c;
                 }
                 
                 //Req_Merged_Req_System__c
                 if(!String.isBlank(opp.Req_Merged_Req_System__c)){
                 req.Merged_Req_System__c  = opp.Req_Merged_Req_System__c;
                 }
                 
                 //Req_Merged_with_VMS_Req__c
                  if(opp.Req_Merged_with_VMS_Req__c==True){
                    req.Merged_with_VMS_Req__c= True;
                }else{
                    req.Merged_with_VMS_Req__c = False;
                }
                 
                 
                //REQ_Opportunity_driven_mostly_by_LOB__c
                 if(opp.REQ_Opportunity_driven_mostly_by_LOB__c !=null){
                 req.Opportunity_driven_mostly_by_LOB_or_EIT__c  = opp.REQ_Opportunity_driven_mostly_by_LOB__c;
                 }
                 
                 //Req_Performance_Expectations__c
                 if(!String.isBlank(opp.Req_Performance_Expectations__c)){
                 req.Performance_Expectations__c  = opp.Req_Performance_Expectations__c;
                 }
                 
                 //Req_Project_Stage_Lifecycle_Info__c
                  if(!String.isBlank(opp.Req_Project_Stage_Lifecycle_Info__c)){
                 req.Project_Stage_Lifecycle_Info__c  = opp.Req_Project_Stage_Lifecycle_Info__c;
                 }
                 
                 //Req_Qualified_Date__c
                 if(opp.Req_Qualified_Date__c !=null){
                 req.Qualified_Date__c  = opp.Req_Qualified_Date__c;
                 }
                
                //Req_Approval_Process__c
                if(!String.isBlank(opp.Req_Approval_Process__c)){
                 req.Req_Approval_Process__c  = opp.Req_Approval_Process__c;
                 }
                 
                //Req_Service_Product_Offering__c
                 if(opp.Req_Service_Product_Offering__c !=null){
                 req.Service_Product_Offering__c  = opp.Req_Service_Product_Offering__c;
                 }
                 
                //Req_SourceReq__c
                if(!String.isBlank(opp.Req_SourceReq__c)){
                 req.SourceReq__c  = opp.Req_SourceReq__c;
                 }
                 
                //Req_System_SourceReq__c
                if(!String.isBlank(opp.Req_System_SourceReq__c)){
                 req.System_SourceReq__c  = opp.Req_System_SourceReq__c;
                 }
                 
                //Req_Template__c
                if(opp.Req_Template__c==True){
                    req.Template__c= True;
                }else{
                    req.Template__c = False;
                }
                 
                //Req_Internal_External_Customer__c
                if(!String.isBlank(opp.Req_Internal_External_Customer__c)){
                 req.Internal_External_Customer__c  = opp.Req_Internal_External_Customer__c;
                 }
                 
                //Req_Why_Position_Open_Details__c
                if(!String.isBlank(opp.Req_Why_Position_Open_Details__c)){
                 req.Why_Position_Open__c  = opp.Req_Why_Position_Open_Details__c;
                 }
                
                //Req_Work_Environment__c
                if(!String.isBlank(opp.Req_Work_Environment__c)){
                 req.Work_Environment__c  = opp.Req_Work_Environment__c;
                 }
                 
                 //Req_End_Client__c
                 if(opp.Req_End_Client__c !=null){
                 req.End_Client__c  = opp.Req_End_Client__c;
                 }
                 
                 //Req_Proactive_Req_Link__c
                 if(opp.Req_Proactive_Req_Link__c !=null){
                 req.Proactive_Req_Link__c  = opp.Req_Proactive_Req_Link__c;
                 }
                 
                 //Req_Opportunity__c
                  if(opp.Req_Opportunity__c !=null){
                 req.Opportunity__c  = opp.Req_Opportunity__c;
                 }
                 
                 //Opportunity_Num__c
                 if(opp.Opportunity_Num__c != null){
                  req.Req_Opportunty__c = String.ValueOf(opp.Opportunity_Num__c);
                 }
                 
                 //Req_Draft_Reason__c
                 if(opp.Req_Draft_Reason__c!=null && opp.Req_Draft_Reason__c == 'Other'){
                   req.Draft_Reason__c = 'Other';
                   req.Draft_Other_Reason__c='Enterprise Req';
                 }else if(opp.Req_Draft_Reason__c!=null){
                   req.Draft_Reason__c= opp.Req_Draft_Reason__c;
                 }else{
                  req.Draft_Reason__c = 'Other';
                  req.Draft_Other_Reason__c='Enterprise Req';
                 }
                 
                 
                 //OT Multiplier
                 decimal BillRateMax1   = opp.Req_Bill_Rate_Max__c== null ? 0 :opp.Req_Bill_Rate_Max__c;
                 decimal BillRateMin1   = opp.Req_Bill_Rate_Min__c== null ? 0 :opp.Req_Bill_Rate_Min__c;
                 //Added by Hareesh to fix the Argument cannot be null
                 if(opp.OT_Multiplier__c!=null){
                 
                 string OTMultiplier = opp.OT_Multiplier__c;
                 Decimal OTMultiplier1;
                 if(OTMultiplier.containsIgnoreCase('x')){
                   OTMultiplier1 = decimal.valueOf(OTMultiplier.substringAfterLast('x')); 
                   req.OT_Bill_Rate_Max__c = BillRateMax1  * OTMultiplier1;
                   req.OT_Bill_Rate_Min__c = BillRateMin1  * OTMultiplier1;
                  }else if(OTMultiplier.containsIgnoreCase('--None--')){
                   //
                   }else{
                       OTMultiplier1 = decimal.valueOf(OTMultiplier);
                       req.OT_Bill_Rate_Max__c = BillRateMax1  * OTMultiplier1;
                       req.OT_Bill_Rate_Min__c = BillRateMin1  * OTMultiplier1;
                   }
                 
                 
                 }
                 
                 
                 String worksiteState;
                 if(!string.isBlank(opp.Req_Worksite_State__c)){
                 worksiteState= String.valueOf(opp.Req_Worksite_State__c);
                 if(worksiteState.length() > 15){
                    worksiteState= worksiteState.substring(0,15);                 
                    req.STATE__C = string.valueof(worksiteState);
                 }else{
                      req.STATE__C = opp.Req_Worksite_State__c;
                 }}
                
 
               if(opp.stageName=='Closed Won' || opp.stageName=='Closed Won (Partial)' || opp.stageName== 'Closed Wash' || opp.stageName == 'Closed Lost'){
                  req.CLOSE_DATE__C =opp.CloseDate;
               }
               
                 String worksiteCountry;
                 if(!string.isBlank(opp.Req_Worksite_Country__c)){
                 worksiteCountry= String.valueOf(opp.Req_Worksite_Country__c);
                 if(worksiteCountry.length() > 40){
                    worksiteCountry= worksiteCountry.substring(0,40);                 
                    req.WORKSITE_COUNTRY__C= string.valueof(worksiteCountry);
                 }else{
                     req.WORKSITE_COUNTRY__C=opp.Req_Worksite_Country__c;
                 }}
                
                if(opp.Req_Background_Check_Required__c==True){
                    req.BACKGROUND_CHECK__C='Yes';
                }
                
                 String billRateMax;
                 if(opp.Req_Bill_Rate_Max__c !=null){
                 billRateMax= String.valueOf(opp.Req_Bill_Rate_Max__c);
                 if(billRateMax.length() > 7){
                    billRateMax= billRateMax.substring(0,7);                 
                    req.BILL_RATE_MAX__C= Integer.valueOf(billRateMax);
                 }else{
                     req.BILL_RATE_MAX__C=opp.Req_Bill_Rate_Max__c;
                 }}
                
                 String billRateMin;
                 if(opp.Req_Bill_Rate_Min__c!=null){
                 billRateMin= String.valueOf(opp.Req_Bill_Rate_Min__c);
                 if(billRateMin.length() > 7){
                    billRateMin= billRateMin.substring(0,7);                 
                    req.BILL_RATE_MIN__C= Integer.valueOf(billRateMin);
                 }else{
                     req.BILL_RATE_MIN__C=opp.Req_Bill_Rate_Min__c;
                 }}
                
                req.SUB_VENDOR__C =opp.Req_Can_Use_Approved_Sub_Vendor__c;
                req.COMPLIANCE__C= opp.Req_Compliance__c;
                req.Payroll__c = opp.Req_Payroll__c;
                
                if(opp.Req_Drug_Test_Required__c==True){
                    req.DRUG_TEST__C='Yes';
                }
                
                 
                //Req_Loss_Wash_Reason__c
                  if(!String.isBlank(opp.Req_Loss_Wash_Reason__c)){
                 req.AM_Loss_Wash_Reason__c=opp.Req_Loss_Wash_Reason__c;
                 req.Loss_Wash_Reason_Selected__c= opp.Req_Loss_Wash_Reason__c;
                 } 

                
                req.DURATION__C=opp.Req_Duration__c;
                if(opp.Req_Duration_Unit__c==   'Day(s)'){
                    req.DURATION_UNIT__C='Days';
                }
                else if(opp.Req_Duration_Unit__c==  'Hour(s)'){
                    req.DURATION_UNIT__C='Hours';
                }
                else if(opp.Req_Duration_Unit__c==  'week(s)'){
                    req.DURATION_UNIT__C='Weeks';
                }
                else if(opp.Req_Duration_Unit__c==  'Month(s)'){
                    req.DURATION_UNIT__C='Months';
                }
                else if(opp.Req_Duration_Unit__c==  'Year(s)'){
                    req.DURATION_UNIT__C='Years';
                }
                
                req.EXCLUSIVE__C=opp.Req_Exclusive__c;
                req.GOVERNMENT__C = opp.Req_Government__c;
                req.GO_TO_WORK__C = opp.Immediate_Start__c;
                req.DBO_LOSS_WASH_REASON__C=opp.Req_Loss_Wash_Reason__c;
                req.OFCCP_REQUIRED__C=opp.Req_OFCCP_Required__c;
                if(opp.stagename=='Staging'){
                    req.recordtypeid=Stagerecdtypeid;
                }
                else if(opp.stagename=='Closed Wash' && opp.Req_Decline_Reason__c!=null){
                    req.RecordTypeId=Stagerecdtypeid;
                }
                else if(opp.Legacy_Record_Type__c == 'Global Services Req'){
                    req.RecordTypeId = TGSrecordTypeid;
                }
                else{
                    req.recordtypeid=Staffrecdtypeid;
                }
                
                //Fix for Prod
                 if(!string.isBlank(opp.Req_Skill_Details__c)){
                    if(opp.Req_Skill_Details__c.length() > 2000){
                       req.Top_3_Skills__c  = opp.Req_Skill_Details__c.substring(0,2000);
                     }else{
                       req.Top_3_Skills__c  = opp.Req_Skill_Details__c;
                       }
                 }
                 /*else{
                      req.Top_3_Skills__c  = opp.Req_Skill_Details__c;
                 }*/
                 
                
                req.ownerid=opp.ownerid;
                //req.PAY_RATE_MAX__C=opp.Req_Pay_Rate_Max__c;
                
                 String payRateMax;
                 if(opp.Req_Pay_Rate_Max__c !=null){
                 payRateMax= String.valueOf(opp.Req_Pay_Rate_Max__c);
                 if(payRateMax.length() > 7){
                    payRateMax= payRateMax.substring(0,7);                 
                    req.PAY_RATE_MAX__C= Integer.valueOf(payRateMax);
                 }else{
                     req.PAY_RATE_MAX__C=opp.Req_Pay_Rate_Max__c;
                 }}
                
                //req.PAY_RATE_MIN__C= opp.Req_Pay_Rate_Min__c;
                 String payRateMin;
                 if(opp.Req_Pay_Rate_Min__c!=null){
                 payRateMin= String.valueOf(opp.Req_Pay_Rate_Min__c);
                 if(payRateMin.length() > 7){
                    payRateMin= payRateMin.substring(0,7);                 
                    req.PAY_RATE_MIN__C= Integer.valueOf(payRateMin);
                 }else{
                     req.PAY_RATE_MIN__C=opp.Req_Pay_Rate_Min__c;
                 }}
                
                string ReqQualification;
                if(opp.Req_Qualification__c!=null ){
                 ReqQualification = String.valueOf(opp.Req_Qualification__c);
                if(ReqQualification.length() > 2000){
                    ReqQualification = ReqQualification.substring(0,2000);                 
                    req.QUALIFICATIONS__C = string.valueOf(ReqQualification);
                 }else{
                   req.QUALIFICATIONS__C=opp.Req_Qualification__c;
                 }
                }
                
                 string ReqQualification1;
                if(opp.Req_Qualification__c!=null ){
                 ReqQualification1 = String.valueOf(opp.Req_Qualification__c);
                if(ReqQualification.length() > 254){
                    ReqQualification1 = ReqQualification1.substring(0,254);                 
                    req.Nice_to_Have_Skills__c = string.valueOf(ReqQualification1);
                 }else{
                   req.Nice_to_Have_Skills__c = opp.Req_Qualification__c;
                 }
                }
                
                
               if(opp.Req_Qualification__c!=null ){
               req.Non_technical_Skills__c = opp.Req_Qualification__c;
               }
                
                req.RED_ZONE__C =opp.Req_Red_Zone__c;
                 String salaryMax;
                 if(opp.Req_Salary_Max__c!=null){
                 salaryMax= String.valueOf(opp.Req_Salary_Max__c);
                 if(salaryMax.length() > 8){
                    salaryMax= salaryMax.substring(0,8);                 
                    req.MAX_SALARY__C= Integer.valueOf(salaryMax);
                 }else{
                     req.MAX_SALARY__C=opp.Req_Salary_Max__c;
                 }}
                 
                String salaryMin;
                if(opp.Req_Salary_Min__c!=null){
                salaryMin= String.valueOf(opp.Req_Salary_Min__c);
                if(salaryMin.length() > 8){
                    salaryMin= salaryMin.substring(0,8);                 
                    req.MIN_SALARY__C= Integer.valueOf(salaryMin);
                 }else{
                     req.MIN_SALARY__C=opp.Req_Salary_Min__c;
                 }  }
               
               if(opp.Req_Search_Tags_Keywords__c!=null){
                req.KEYWORDS__C=opp.Req_Search_Tags_Keywords__c;
                }
               else{
                 String keywords;
                 List<SkillsWrapper> skillPills = new List<SkillsWrapper>();
                 String reqFinalSkills = '';
                 if(!string.isBlank(opp.EnterpriseReqSkills__c)){
                 skillPills = (List<SkillsWrapper>)JSON.deserializeStrict(opp.EnterpriseReqSkills__c, List<SkillsWrapper>.Class);
                 
                 if(skillPills.size() > 0){
                   for(Integer p=0; p < skillPills.size() ; p++){
                     if(p == skillPills.size()- 1){
                       reqFinalSkills += skillPills[p].name;
                      }else{
                       reqFinalSkills += skillPills[p].name + ',';
                     }
                   }
                   
                     if(reqFinalSkills.length() > 250){
                        keywords = reqFinalSkills.substring(0,250);                 
                        req.KEYWORDS__C= string.valueOf(keywords);
                        if(string.isBlank(opp.Req_Skill_Details__c)){
                        req.Top_3_Skills__c = reqFinalSkills;
                        }

                     }else{
                          req.KEYWORDS__C= reqFinalSkills;
                      if(string.isBlank(opp.Req_Skill_Details__c)){
                        req.Top_3_Skills__c = reqFinalSkills;
                        }
                     }
                  }
                   
                 }
                 }
                 
                if(!String.isBlank(opp.Is_International_Travel_Required__c)){
                    req.Is_International_Travel_Required__c= opp.Is_International_Travel_Required__c;
                } 
                                
                if(!String.isBlank(opp.Employment_Alignment__c)){
                    req.Employment_Alignment__c = opp.Employment_Alignment__c;
                } 
                if(!String.isBlank(opp.Final_Decision_Maker__c)){
                    req.Final_Decision_Maker__c = opp.Final_Decision_Maker__c;
                } 
                if(!String.isBlank(opp.GlobalServices_Location__c)){
                    req.Location__c = opp.GlobalServices_Location__c;
                }
                req.Backfill__c = opp.Backfill__c;                 
                req.Orig_Partner_System_Id__c = opp.Req_Origination_Partner_System_Id__c;
                req.Req_Origination_System__c = opp.Req_Origination_System__c;
                req.STANDARD_BURDEN__C=opp.Req_Standard_Burden__c;
                req.Is_Auto_Closed__c=opp.Req_Is_Auto_Closed__c;
                system.debug('reqmapchk'+Reqmapchk);
                if( (Reqmapchk.size()==0 && opp.Req_Legacy_Req_Id__c == null) || ( opp.Req_Legacy_Req_Id__c==null && Reqmapchk.size()>0 && !Reqmapchk.containskey(opp.id))   ){
                 req.CreatedByID = opp.CreatedByID;
                 req.CreatedDate = opp.CreatedDate;
                 req.LastModifiedByID = opp.LastModifiedByID;
                 req.LastModifiedDate = opp.LastModifiedDate;
                }
                req.EXPORT_CONTROLS_COMPLIANCE_REQUIRED__C=opp.Req_Export_Controls_Questionnaire_Reqd__c;
                req.EnterpriseReqId__c=opp.id;
                
                req.Product__c=opp.Legacy_Product__c;
                req.Can_support_OPT_STEM_OPT__c = opp.Req_OPT__c;
                reqmap.put(opp.Req_origination_System_Id__c,req.name);
      
                system.debug('Reqmap'+reqmap);
                
                 if(opp.Req_Legacy_Req_Id__c!=null || (Reqmapchk.size()>0 && Reqmapchk.containskey(opp.id))){
                      Legacyreqs2.add(req);
                 }else{  
                    req.Siebel_ID__c = opp.Req_origination_System_Id__c;     
                       Legacyreqs.add(req);
                 }
                   system.debug('LegacyReqs'+Legacyreqs);
            }
        }
    
    if(Legacyreqs.size() > 0){
           Integer numOfUpserts = 0;
            String tempString = '';            
            integer i = 1 ; 
            Integer totalRecords = 0;
            
             List<database.SaveResult> OppSaveResults = database.insert(Legacyreqs,false);
             System.debug('RESULTS'+OppSaveResults);
             for(Integer j=0; j < Legacyreqs.size();j++){
                String errorRecord = 'Failed Opportunity Upsert Sync:';
                String exceptionMsg = '';
                if(!OppSaveResults[j].isSuccess()){
                  if(OppSaveResults[j].getErrors()[0].getStatusCode() != null && OppSaveResults[j].getErrors()[0].getFields()!= null){
                    exceptionMsg += 'Error Reason: '+ String.ValueOf(OppSaveResults[j].getErrors()[0].getStatusCode())+ ' '+ ' Error Fields ' +  OppSaveResults[j].getErrors()[0].getFields();
                  }
                  errorRecord += exceptionMsg;
                  insertErrorsMap.put(Legacyreqs[j].EnterpriseReqId__c,errorRecord);
                }else if(!OppSaveResults[j].isSuccess()){
                  errorLogMap.put(Legacyreqs[j].EnterpriseReqId__c, OppSaveResults[j].getErrors()[0].getStatusCode() != null && OppSaveResults[j].getErrors()[0].getFields()!= null ? String.ValueOf(OppSaveResults[j].getErrors()[0].getStatusCode())+' ' +  ' Error Fields ' +  OppSaveResults[j].getErrors()[0].getFields(): ' Either Status Code or Fields are Null ');
                }
             }
            
            if(errorLogMap.size() > 0){
                Core_Data.logBatchRecord('Ent-Legacy Updates',errorLogMap);
             }
              if(insertErrorsMap.size() > 0)
                Core_Data.logBatchRecord('Ent-Legacy Inserts', insertErrorsMap);
          }
                 
                 
       if(Legacyreqs2.size() > 0){
            Integer numOfUpserts = 0;
            String tempString = '';            
            integer i = 1 ; 
            Integer totalRecords = 0;
           
           List<database.SaveResult> OppUpdateResults = database.update(Legacyreqs2 ,false);
           
             for(Integer y=0; y < Legacyreqs2.size();y++){
                String errorRecord = 'Failed Opportunity Update Sync:';
                String exceptionMsg = '';
                if(!OppUpdateResults[y].isSuccess()){
                  errorLogMap2.put(Legacyreqs2[y].EnterpriseReqId__c, OppUpdateResults[y].getErrors()[0].getStatusCode() != null && OppUpdateResults[y].getErrors()[0].getFields()!= null ? String.ValueOf(OppUpdateResults[y].getErrors()[0].getStatusCode()) +' '+ ' Error Fields ' +  OppUpdateResults[y].getErrors()[0].getFields(): ' Either Status Code or Fields are Null ');
                }
             }
           if(errorLogMap2.size() > 0){
                Core_Data.logBatchRecord('Enterprise to Legacy Sync',errorLogMap2);
            }
         }
         
         if(errorLogMap.size() > 0){
           exceptionMap.putall(errorLogMap);
         }
         if(errorLogMap2.size() > 0){
           exceptionMap.putall(errorLogMap2);
         }
         if(insertErrorsMap.size() > 0){
           exceptionMap.putall(insertErrorsMap);
         }
       
       ReqSyncErrorLogHelper errorWrapper = new ReqSyncErrorLogHelper(exceptionMap,errorLst);
       return errorWrapper;
   }
}