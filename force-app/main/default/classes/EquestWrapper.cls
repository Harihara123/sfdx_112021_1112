public class EquestWrapper {
    public class Board {
		public String name;
		public Integer id;
	}

	public String equestid;
	public String refenceid;
	public List<BoardDetails> boardDetails;

	public class BoardDetails {
		public Integer id;
        public String delete_status;// for delete status
		public Board_status board_status;
		public Board board;
	}

	public class Board_status {
		public String expires_at;
		public String posted_at;
		public String state;
		public String live_url;
        public String deferred_reason;
        public string queued_at;
        
	}

	
	public static EquestWrapper parse(String json) {
		return (EquestWrapper) System.JSON.deserialize(json, EquestWrapper.class);
	}

}