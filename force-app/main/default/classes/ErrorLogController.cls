public class ErrorLogController {
    @AuraEnabled
    public static void saveErrorLog(String errMsg, string errStackTrace, string errType) {
        Error_Log__c log = new Error_Log__c();
        log.Name = errType;
        log.Message__c = errMsg;
        log.Stack_Trace__c = errStackTrace;
        insert log;
    }
}