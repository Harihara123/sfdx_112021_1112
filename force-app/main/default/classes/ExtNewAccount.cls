public class ExtNewAccount {
    
    public Boolean accessDBHooversButton{get;set;}

	public ExtNewAccount(ApexPages.StandardController controller) {
    	
        List<groupmember> groupMembers = [SELECT userorgroupid FROM groupmember 
                                          	WHERE group.DeveloperName  = 'D_B_Hoovers'
                                          	AND userorgroupid =: UserInfo.getUserId()];
        
        if( groupMembers.size() == 0 ){
            accessDBHooversButton = false;
        }else{
            accessDBHooversButton = true;
        }
        
    }

	public PageReference NewHistoric() {
        
        PageReference secondPage = new PageReference ('/DataDotComSearch?save_new=1&nooverride=1');
        secondPage.setRedirect(true);
        
        return secondPage; 
     }

	public PageReference NewImplementation() {
        
      //   PageReference secondPage = new PageReference ('/001/e?nooverride=1'); // commented to redirect manual button to VF page
        PageReference secondPage = new PageReference ('/apex/Account_New_Page_Layout');
        secondPage.setRedirect(true);        
        return secondPage; 
     }
    
    
}