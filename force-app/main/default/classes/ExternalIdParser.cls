global class ExternalIdParser {
    
    //public List<Talent> Talents;
    
    //public class Talent{
        public String peoplesoftId{get;set;}
        public String esfId{get;set;}
        public String rwsId{get;set;}
		public String orderId{get;set;}	// 8012400000Bj9ZEAAZ
		public String status{get;set;}	//
		public String billRate{get;set;}	//23.50
		public String burden{get;set;}	//47.0
		public String payRate{get;set;}	//22.25
		public String contractType{get;set;}	//Hourly
		public String salary{get;set;}	//66000
		public String rateFrequency{get;set;}
		
   // }
  
    public ExternalIdParser parse(String json) {
		return (ExternalIdParser) System.JSON.deserialize(json, ExternalIdParser.class);
    }
}