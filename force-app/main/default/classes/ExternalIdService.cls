@RestResource(urlMapping='/talent/ExternalId/*')
global class ExternalIdService {
   
    @HttpPost
    global static Map<String, String> updateExternalId() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        ExternalIdParser parser, parsedData;
		Map<String, String> result = new Map<String, String>();
        String jsonInput ='';
		List<User> usersToBeUpdated = new List<User>();
		List<User> userlist ;
		Savepoint sp = Database.setSavepoint();
		Boolean isAccounChanged = false;
		Boolean isContactChanged = false;
		Boolean isUserChanged = false;
		String tempMessage ='';
        try {
            jsonInput = req.requestBody.toString();
            parser = new ExternalIdParser();
			parsedData= new ExternalIdParser();
            parsedData = parser.parse(jsonInput); 
            
            Map<String, String> rwsIdPsIdMap = new Map<String, String>();
			List<String> orderIdFromRequest = new List<String>();
            
			
			//ExternalIdParser.Talent talent = new ExternalIdParser.Talent();
			ExternalIdParser talent = new ExternalIdParser();

			//parsedData.Talents = new List<ExternalIdParser.Talent>();
			
            //List of Talents RWSId in the Request
           // for (ExternalIdParser t : parsedData.Talents) {

				
                //Exclude the records without Peoplesoft Id
                if (!String.isBlank(parsedData.peoplesoftId)) {
                    rwsIdPsIdMap.put(('R.'+parsedData.rwsId), parsedData.peoplesoftId);
					
                }
				if(!String.isBlank(parsedData.orderId)) {
					orderIdFromRequest.add(parsedData.orderId);
				}
                
            //}
            
      List<Account> accountToBeUpdated = new List<Account>();
      List<Contact> contactToBeUpdated = new List<Contact>();
      List<Order> orderToBeUpdated = new List<Order>();
      Map<Id,Contact> contactMap = new Map<Id,Contact>();      
      if(rwsIdPsIdMap.size() > 0) {

      //Get list of accounts based on RWSId

        List<Account> accountList = [select id, Source_System_Id__c, Peoplesoft_Id__c from Account where Source_System_Id__c in : rwsIdPsIdMap.keySet()];
        if(accountList.size() > 0) {
          for (Account a : accountList) {
                        if(a.Peoplesoft_ID__c != rwsIdPsIdMap.get(a.Source_System_Id__c)  ){
                         a.Peoplesoft_ID__c = rwsIdPsIdMap.get(a.Source_System_Id__c);   
                         isAccounChanged = true;   
                        }
            
            accountToBeUpdated.add(a);
          }
        }
            //Get list of contacts based on RWSId
        contactMap = new Map<Id,Contact>([select id, accountId, Source_System_Id__c, Peoplesoft_Id__c, 
                                  account.Source_System_Id__c, account.Peoplesoft_Id__c, Talent_Ownership__c
                                  from contact where source_system_id__c in :rwsIdPsIdMap.keySet()]);
        
				//Get list of users based on contacts
				 userlist = new List<User>([select id,Peoplesoft_id__c,Federationidentifier,contact.source_system_Id__c from user 
									   where contactId in :contactMap.keySet()]);
				
               
				


        for(Contact c : contactMap.values()) {
                    if(c.Peoplesoft_ID__c != rwsIdPsIdMap.get(c.Source_System_Id__c)  ){
                        c.Peoplesoft_Id__c = rwsIdPsIdMap.get(c.Source_System_Id__c);
                        //c.Account.Peoplesoft_Id__c = c.Peoplesoft_Id__c;
                        c.Account.Peoplesoft_Id__c = rwsIdPsIdMap.get(c.Source_System_Id__c);
                        contactToBeUpdated.add(c);
                        isContactChanged = true;  
                        if(!accountToBeUpdated.contains(c.Account)) {
                            accountToBeUpdated.add(c.Account);
                        }
                    }
        }

        if(userlist.size() > 0) {
          for(User u : userlist){
                        if(u.Peoplesoft_ID__c != parsedData.peoplesoftId ){
                            u.Peoplesoft_Id__c = parsedData.peoplesoftId;
                            u.FederationIdentifier = parsedData.peoplesoftId;
                            usersToBeUpdated.add(u);
                            isUserChanged = true;  
                        }    
          }
        }
        List<String> tempMessageList = new List<String>();
		if(isAccounChanged && (accountToBeUpdated.size() > 0)){
			update accountToBeUpdated;
			tempMessageList.add('Account');
		}
		
		if(isContactChanged && (accountToBeUpdated.size() > 0)){
			update contactToBeUpdated;
			tempMessageList.add('Contact');
		}
		
		if(isUserChanged && (accountToBeUpdated.size() > 0)){
			update usersToBeUpdated;
			tempMessageList.add('User');
		}
		
		if (tempMessageList.size() > 0){
				for(integer i=0;i < tempMessageList.size(); i++){
					if(i == (tempMessageList.size() - 1 )){
						tempMessage += tempMessageList[i]+' updated successfully';
					}else{
						tempMessage += tempMessageList[i]+', ';
					}
				}
			result.put('message', tempMessage);
        }
		
      }

      if(orderIdFromRequest.size() > 0 && !(String.isBlank(parsedData.status) && String.isBlank(parsedData.billRate) && String.isBlank(parsedData.burden) && String.isBlank(parsedData.payRate) && String.isBlank(parsedData.contractType) && String.isBlank(parsedData.salary))) {
        List<Order> orderIds = new List<Order>([select id,AccountId,status,Bill_Rate__c,Burden__c,Pay_Rate__c,Rate_Frequency__c,Salary__c,ShipToContactId from Order where id in :orderIdFromRequest]);
        List<Order> orderEventList = new List<Order>();
			if(orderIds.size() > 0) {
                tempMessage = '';
				for(Order o : orderIds) {
                    if(contactMap.containsKey(o.ShipToContactId)){
                   /*     if(parsedData.status.equalsIgnoreCase('PCP')){
                            String tempOrderStatus = o.status;
                            o.status = 'Started';   
                            If(o.status != tempOrderStatus){
                                orderEventList.add(o);
                            }   
                        }
                        */
						if (!String.isBlank(parsedData.billRate)){
							o.Bill_Rate__c = Decimal.valueOf(String.valueOf(parsedData.billRate));
						}
						if (!String.isBlank(parsedData.burden)){
							o.Burden__c = Decimal.valueOf(String.valueOf(parsedData.burden));
						}
						if (!String.isBlank(parsedData.payRate)){
							o.Pay_Rate__c = Decimal.valueOf(String.valueOf(parsedData.payRate));
						}
						if (!String.isBlank(parsedData.contractType)){
							o.Rate_Frequency__c = parsedData.contractType;
						}
						if (!String.isBlank(parsedData.salary)){
						  o.Salary__c = Decimal.valueOf(String.valueOf(parsedData.salary));
						}
					orderToBeUpdated.add(o);
					}else{
                        tempMessage = 'Order '+o.Id+' is not associated with Contact ';
						List<Id> contactIds = new list<Id>(contactMap.KeySet());
							//contactIds = contactMap.KeySet();
							if(contactIds.Size() > 0){
								tempMessage += contactIds.get(0);
							}
					}
			   }
			
			if(orderToBeUpdated.size() > 0){
				update orderToBeUpdated;
				result.put('message ', 'Order updated successfully');
                if(orderEventList.size() > 0){
                    generateEvents(orderEventList);
                }
           }else{
				result.put('message ', tempMessage);
				ATSTalentActivityFunctions.saveErrorLog(tempMessage, '', 'PSIDConflictError', jsonInput);
			}

			}
		}            
			
			if(!(result.size() > 0)) {
				result.put('message', 'No Contact, Account, User, Order Updated');
			}
            res.statusCode = 200;

        } catch (Exception ex) {
            res.statusCode = 500;
			
			System.debug('ex.getMessage()'+ex.getMessage());
			//S-142539- Rollback when user update failed.
			
			if(ex.getTypeName() == 'System.DmlException' && usersToBeUpdated.size() > 0 && (ex.getMessage().contains('FederationIdentifier') || ex.getMessage().contains('Peoplesoft_Id__c'))){
        
        
        Database.rollback(sp);
        List<User> tempUserlist = new List<User>();
		tempUserlist = [Select Id from User where Peoplesoft_Id__c =:parsedData.peoplesoftId LIMIT 1];
         
        if(tempUserlist.size() > 0 ){
			tempMessage ='PS ID '+parsedData.peoplesoftId+' could not be updated on User '+usersToBeUpdated[0].Id+' since the PS ID already exists for '+tempUserlist[0].Id;
        }else{
			tempMessage = 'Two user records exist for the given Source System ID '+parsedData.rwsId;
        }
        result.put('Error',tempMessage);
        ATSTalentActivityFunctions.saveErrorLog(tempMessage, ex.getDmlMessage(0), 'PSIDConflictError', jsonInput);
      
      } else {
        Database.rollback(sp);
				result.put('Error', ' '+ex.getMessage());
			}
			
        }
        return result;
        
    }

 public static List<Event> generateEvents(List<Order> orderList){

        List<Event> eventList = new List<Event>();
        try{
		
			for(Order o : orderList){
				Event event = new Event();
					event.WhoId = o.ShipToContactId;
					event.WhatId = o.Id;
					event.Type = o.Status;
					event.Subject = 'Started by '+Userinfo.getName();  //Order ownerName
					event.StartDateTime = DateTime.now();
					event.OwnerId = '005240000038GjfAAE';
					event.EndDateTime = System.Now().addMinutes(1);
					//event.EndDate = System.today();
					event.DurationInMinutes = 1;
					//event.DB_Activity_Type__c = 'Other';
					event.Activity_Type__c  = o.Status;
					event.ActivityDateTime  = System.Now();
					event.ActivityDate  = System.today();
				//	event.AccountId  = o.AccountId;
				eventList.add(event);	
			}
			insert eventList;
        }catch(Exception e){
            System.debug('generateEvent >> '+e.getMessage());
            throw e;
        }
        return eventList;
    }
    
}