@isTest
public class ExternalIdService_Test  {

	@TestSetup
	public static void testData() {

        List<Account> acclist = new List<Account>();
        List<Contact> conlist = new List<Contact>();
        Account acc = new Account();
		acc.Name = 'TestAcc';
        acc.Peoplesoft_ID__c = '334456';
		acc.Source_System_Id__c = 'R.123451';
		acclist.add(acc);
        
        Account acc1 = new Account();
		acc1.Name = 'TestAcc1';
        acc1.Peoplesoft_ID__c = '334457';
		acc1.Source_System_Id__c = 'R.123452';
		acclist.add(acc1);
		insert acclist;

		Contact c = new Contact();
		c.LastName = 'TestContaxt';
		c.AccountId = acclist[0].Id;
		c.Source_System_Id__c = 'R.123451';
        c.Peoplesoft_ID__c = '334456';
		conlist.add(c);

		Contact cs = new Contact();
		cs.LastName = 'TestContaxt1';
		cs.AccountId =  acclist[1].Id;
		cs.Source_System_Id__c = 'R.123452';
        cs.Peoplesoft_ID__c = '334457';
		conlist.add(cs);
		insert conlist;

		insert new DRZSettings__c(Name = 'OpCo', Value__C ='Aerotek, Inc;TEKsystems, Inc.');
		Order o = new Order();
		o.Status = 'Offer Accepted';
		o.Bill_Rate__c = 32.5;
		o.AccountId = acc.Id;
		o.EffectiveDate = System.today();

		insert o;
	}

	@IsTest
	public static void testUpdateExternalId() {
		 Test.startTest();
	        Order o = [select id from order Limit 1];	
	    //Account acc = [Select Id from Account limit 1 ];	
        List<Contact> conlist = [Select Id, Peoplesoft_Id__c from Contact];
        	RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestBody = Blob.valueOf('{"peoplesoftId":"334458","esfId":"","rwsId":"123451","orderId":"'+o.Id+'","status":"PCP","billrate":"23.5","burden":"47","payrate":"22.25","contracttype":"Hourly","salary":"66000"}');
			req.httpMethod = 'POST';
			req.requestURI = '/services/apexrest/talent/ExternalId/*';

			RestContext.request = req;
			RestContext.response = res;
		
			Map<String, String> mapToDisplay = new Map<String, String>(ExternalIdService.updateExternalId());
     		
        	//req.requestBody = Blob.valueOf('{"peoplesoftId":"334457","esfId":"","rwsId":"123451","orderId":"","status":"PCP","billrate":"23.5","burden":"47","payrate":"22.25","contracttype":"Hourly","salary":"66000"}');
          //  mapToDisplay = new Map<String, String>(ExternalIdService.updateExternalId());
       	Test.stopTest();
		System.debug('Output--->' +mapToDisplay);
      
        User usr = [Select Id, name From User Where Name ='Talent Integration' LIMIT 1];
        usr.OPCO__c = 'ONS';
        update(usr);
        System.runAs(usr){ 
        Profile p = [SELECT Id FROM Profile WHERE Name='Aerotek Community User'];
		List<User> userlist = new List<User>();
		User u = new User(Alias = 'ADMTest', Email='ADMTest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='UTest', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, ContactId = conlist[0].Id,
            TimeZoneSidKey='America/Los_Angeles', Username='standarduser1@testorg.com');

		userlist.add(u);
       User u1 = new User(Alias = 'standt2', Email='standarduser2@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, ContactId = conlist[1].Id,
            TimeZoneSidKey='America/Los_Angeles', Username='standarduser2@testorg.com');
		userlist.add(u1);
        
        insert userlist;
            
        
        }
	
        RestRequest req1 = new RestRequest();
			RestResponse res1 = new RestResponse();
			req1.requestBody = Blob.valueOf('{"peoplesoftId":"334458","esfId":"","rwsId":"123451","orderId":"030","status":"PCP","billrate":"23.5","burden":"47","payrate":"22.25","contracttype":"Hourly","salary":"66000"}');
			req1.httpMethod = 'POST';
			req1.requestURI = '/services/apexrest/talent/ExternalId/*';

			RestContext.request = req1;
			RestContext.response = res1;
		
			Map<String, String> mapToDisplay1 = new Map<String, String>(ExternalIdService.updateExternalId());
   
	}
}