public without sharing class FeedCommentTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeInsertCommentItem = 'beforeInsertCommentItem';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, FeedComment> oldMap = new Map<Id, FeedComment>();
    Map<Id, FeedComment> newMap = new Map<Id, FeedComment>();
    
    //-------------------------------------------------------------------------
    // Boolean Check Method to Avoid Recursion on After Update Event - Akshay ATS - 2819
    //-------------------------------------------------------------------------
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }

    DRZPublishChatterEvents DRZEventHandler = new DRZPublishChatterEvents();

    
    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<FeedComment> newRecs) {
        TC_checkForAtMentionController.TC_checkForAtMention(beforeInsertCommentItem, beforeInsert, newRecs, null);
    }
    
    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<FeedComment> newRecs) {

        DRZEventHandler.createChatterCommentEvent(newRecs, 'NEW_OPPORTUNITY_COMMENT_REPLY');

    }
    
    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, FeedComment>oldMap, Map<Id, FeedComment>newMap) {
        
    }
    
    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, FeedComment>oldMap, Map<Id, FeedComment>newMap) {

        List<FeedComment> lstFeedComments = newMap.values();
        DRZEventHandler.createChatterCommentEvent(lstFeedComments, 'UPDATED_OPPORTUNITY_COMMENT_REPLY');

    }
    
    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, FeedComment>oldMap) {
    }
    
    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, FeedComment>oldMap) {
        List<FeedComment> lstFeedComments = oldMap.values();

        DRZEventHandler.createChatterCommentEvent(lstFeedComments, 'DELETED_OPPORTUNITY_COMMENT_REPLY');

    }
    
    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, FeedComment>oldMap) {
    }
    /*Public void checkForAtMention(String Context, List<FeedComment> newRecs){
        Set<String> networkIds = new Set<String>();
        String networkIdValues = System.Label.AtMentionCommId;
        networkIds.addAll(networkIdValues.split(','));
        if(Context.equals(beforeInsert)){
            for(FeedComment fc:newRecs){
                Id acLighId = [Select id, NetworkScope from FeedItem where Id =:fc.FeedItemId].NetworkScope;
                if(networkIds.contains(String.valueOf(acLighId)) && fc.CommentBody.contains('@')){
                  fc.Status = 'PendingReview'; 
                }
            }
    }   
    }*/
}