@isTest
public class FeedCommentTriggerHandler_Test
{
    static testMethod void FeedComment_TH_Test()
    {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Test.StartTest();
        FeedItem f = new FeedItem();
        f.ParentId = UserInfo.getUserId();
        f.body = 'test';
        insert f;
        FeedComment fc = new FeedComment();
        fc.CommentBody = 'legal test';
        fc.FeedItemId = f.Id;   // please add this
        insert fc;
        FeedCommentTriggerHandler.runOnce();
        fc.CommentBody = 'Test123343';
        Update fc;
        delete fc;
        Test.StopTest();
        //System.assertEquals ('legal test', fc.commentbody);
    }
}