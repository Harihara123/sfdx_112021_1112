public without sharing class FeedItemTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String beforeInsertFeedItem = 'beforeInsertFeedItem';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, FeedItem> oldMap = new Map<Id, FeedItem>();
    Map<Id, FeedItem> newMap = new Map<Id, FeedItem>();
    
    //-------------------------------------------------------------------------
    // Boolean Check Method to Avoid Recursion on After Update Event - Akshay ATS - 2819
    //-------------------------------------------------------------------------
    private static boolean run = true;
        public static boolean runOnce(){
            if(run){
                run=false;
                return true;
            }else{
                return run;
            }
        }

    DRZPublishChatterEvents DRZEventHandler = new DRZPublishChatterEvents();

    
    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<FeedItem> newRecs) {
        TC_checkForAtMentionController.TC_checkForAtMention(beforeInsertFeedItem, beforeInsert,null, newRecs);
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<FeedItem> newRecs) {
        if(Userinfo.getUserType() =='CspLitePortal'){
        TC_CreateCaseOnFeedItem.createCase(newRecs);
        }

        DRZEventHandler.createChatterPostEvent(newRecs, 'NEW_OPPORTUNITY_COMMENT');
		CSC_FeedItemHelper.postFeedItemOnChild(newRecs);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, FeedItem>oldMap, Map<Id, FeedItem>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, FeedItem>oldMap, Map<Id, FeedItem>newMap) {

        List<FeedItem> lstFeeds = newMap.values();
        DRZEventHandler.createChatterPostEvent( lstFeeds, 'UPDATED_OPPORTUNITY_COMMENT');

    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, FeedItem>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, FeedItem>oldMap) {

      List<FeedItem> lstFeeds = oldMap.values();
      DRZEventHandler.createChatterPostEvent(lstFeeds, 'DELETED_OPPORTUNITY_COMMENT');    

    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, FeedItem>oldMap) {
    }
    /*Public void checkForAtMention(String Context, List<FeedComment> newRecs){
        Set<String> networkIds = new Set<String>();
        String networkIdValues = System.Label.AtMentionCommId;
        networkIds.addAll(networkIdValues.split(','));
        if(Context.equals(beforeInsert)){
            for(FeedComment fc:newRecs){
            Id acLighId = [Select id, NetworkScope from FeedItem where Id =:fc.FeedItemId].NetworkScope;
                if(networkIds.contains(String.valueOf(acLighId)) && fc.CommentBody.contains('@')){
                        fc.Status = 'PendingReview'; 
                }
            }
        }   
    }*/
        /* public void createChatterPostEvent( List<FeedItem> newRecs, String eventType ) {
        
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) { 
        
            DRZSettings__c s = DRZSettings__c.getValues('OpCo');
            String OpCoList = s.Value__c;
            List<String> OpCos = OpCoList.split(';');
            
            string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            List<OpportunityChatterDRZMessage__e> drzMsgs = new List<OpportunityChatterDRZMessage__e>();
            
            set<string> opptyIds = new set<string>();
            for(FeedItem feed:newRecs){
                opptyIds.add( feed.ParentId );
            }
            
            if( newRecs.size() > 0 ){
                List<Opportunity> opps = [SELECT Id, Status__c, RecordTypeId, OpCo__c FROM Opportunity 
                                      WHERE Id in :opptyIds AND isClosed  != true
                                        AND OpCo__c in:OpCos AND RecordTypeId =: reqRecordTypeId];
            
                if( opps.size() > 0 ){
                    
                    for(Opportunity opp:opps){
                      for(FeedItem feed:newRecs){
                            if( feed.ParentId == opp.Id ){
                                OpportunityChatterDRZMessage__e msg = new OpportunityChatterDRZMessage__e(
                                                                            Event_Sub_Type__c = eventType,
                                                                            feedBody__c = feed.Body,
                                                                            feedItemId__c = feed.Id,
                                                                            lastModifiedDate__c = feed.LastModifiedDate,
                                                                            parentOppId__c = feed.ParentId
                                                                         );
                                drzMsgs.add(msg);
                            }
                        }
                    }
                }
            }
            
            List<Database.SaveResult> results = EventBus.publish(drzMsgs);
        }
    } */
}