// This controller is used by the fileUpload lightning component to manage
// the actual transfer of the file.
public with sharing class FileController {
    
    @AuraEnabled
    public static Boolean validateFileType (string fileName) { 
		Boolean validFileType = false;
		system.debug('fileName in apex =====>>>>' + fileName);
        String extn = fileName.right(fileName.length() - fileName.lastIndexOf('.') - 1);
        system.debug('extn =====>>>>' + extn);

        List<String> resumeTypes = new List<String>();
        Global_LOV__c[] fileTypes = [SELECT Id, LOV_Name__c, Text_Value__c from Global_LOV__c where LOV_Name__c in ('ATSResumeFiletypesAllowed')];

         // Load allowed file types list from Global LOV.
        for (Global_LOV__c lov : fileTypes) {
             resumeTypes.add(lov.Text_Value__c);
        }   

        system.debug('resumeTypes =====>>>>' + resumeTypes);

        for (String ftype : resumeTypes) {
            if (extn == ftype) {
                validFileType = true;
                break;
            }
        }
        return validFileType;
    }

    // Even though this is tagged @AuraEnabled, it is only used within this class at the moment.
    // This should probably be made private.
    // File save starts here which creates the attachment object and associates it with the parentId.
    @AuraEnabled
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) { 
		
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');        
        
        Attachment a = new Attachment();
        a.parentId = parentId;

        a.Body = EncodingUtil.base64Decode(base64Data);
        
        try {
                fileName  = fileName.replaceAll('[^a-zA-Z0-9\\s-_.]', '');
        } catch(Exception e){
               system.debug('Fail to remvoe Special Chars from Resume name: ' + e.getMessage());
        }
                
        a.Name = fileName;
        a.ContentType = contentType;
        
        insert a;
        
        return a.Id;
    }
    
    // Files are broken into manageable chuncks and base64 encoded. This method receives chuncks and 
    // reassembles them into a file.
    @AuraEnabled
    public static Id saveTheChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) { 
        
            // If we don't have an Id for the attachment yet, we are processing the first chunck
            // create the attachment object.
        try {    
                if (fileId == null || fileId == '') {
                    fileId = saveTheFile(parentId, fileName, base64Data, contentType);
                
                // Subsequent chunks are processed here
                } else {
                    appendToFile(fileId, base64Data);
                }
        } catch (Exception e){
			system.debug('Error Message ========' + e.getMessage());
        }             
        return Id.valueOf(fileId);
    }
    

    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id = :fileId
        ];
        
     	String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        
        update a;
    }
}