@isTest
private class FileController_Test {

    static testmethod void testValidateFile() {
                
		Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='doc',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
		insert glob;

		Test.startTest();
			Boolean result = FileController.validateFileType ('abc.doc');
			System.assertEquals(true, result);

			result = FileController.validateFileType ('abc.png');
			System.assertEquals(false, result);
		Test.stopTest();
	}

	private static testmethod void testSaveFile() {
		Account testAccount = CreateTalentTestData.createTalentAccount();
		Contact testContact = CreateTalentTestData.createTalentContact(testAccount);

		Talent_Document__c doc = new Talent_Document__c(
                Document_Name_Clean__c = 'testdocument.doc', 
                Default_Document__c = false,
                Document_Type__c = 'Resume',
                HTML_Version_Available__c = true,
                Committed_Document__c = true,
                Mark_For_Deletion__c = false
            );
        doc.Talent__c = testAccount.Id;
        insert doc;

		Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='doc',Source_System_Id__c='ATSResumeFiletypesAllowed.005');
		insert glob;

		Test.startTest();
			/** Test writing initial file chunk */
			Id result = Filecontroller.saveTheChunk(doc.Id,'testdocument.doc','VGhlc2UgYXJlIHRoZSBmaWxlIGNvbnRlbnRzIGZvciB0aGUgdGVzdCBjYXNl', 'application/doc', null);
			// Assert returned ID is an attachment ID
			System.assertEquals(result.getSobjectType().getDescribe().getName(), 'Attachment');

			/** Test appending to initial file chunk */
			Id result2 = Filecontroller.saveTheChunk(doc.Id,'testdocument.doc','VGhlc2UgYXJlIHRoZSBmaWxlIGNvbnRlbnRzIGZvciB0aGUgdGVzdCBjYXNl', 'application/doc', result);
			// Assert same attachment Id is returned
			System.assertEquals(result, result2);
		Test.stopTest();
	} 
    
}