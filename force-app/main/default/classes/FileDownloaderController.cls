public class FileDownloaderController  {
    public static String INVALID_INPUT_MSG = 'Invalid input';

    public class DownloadResponse {
        public String filename {get;set;}
        public String fileContent {get;set;}
        public String errorMsg {get;set;}
    }

    @AuraEnabled
    public static String downloadTheFile(String fileSrc, String fileLocator) {
        Blob fileContent;
        DownloadResponse dr = new DownloadResponse();

        try {
            if (fileSrc == 'Azure') {
                return downloadAzureFile(fileLocator);
            } 
            dr.errorMsg = INVALID_INPUT_MSG;
        } catch (Exception ex) {
            System.debug(ex);
            dr.errorMsg = 'Exception downloading file: ' + ex.getMessage();
        }

        return JSON.serialize(dr);
    }

  private static String downloadAzureFile(String fileLocator) {
    DownloadResponse dr = new DownloadResponse();
    String proxySettingName = 'Document Blob Service';
    try {
    //  MuleSoftOAuth_Connector.OAuthWrapper tstOAuth =  MuleSoftOAuth_Connector.getAccessToken(proxySettingName);
      
    //  Mulesoft_OAuth_Settings__c serviceSettings = Mulesoft_OAuth_Settings__c.getValues(proxySettingName);
      Map<String,String> serviceSettings = WebServiceSetting.getMulesoftOAuth(proxySettingName);
      Http connection = new Http();
      HttpRequest req = new HttpRequest();

      req.setEndpoint(serviceSettings.get('endPointUrl') + '?blobpath=' + EncodingUtil.urlEncode(fileLocator, 'UTF-8'));
      req.setMethod('GET');
      req.setTimeout(Integer.valueOf(Label.ATS_SEARCH_TIMEOUT));
      req.setHeader('Authorization', 'Bearer '+serviceSettings.get('oToken'));
      System.debug(req);

      HttpResponse response = connection.send(req);
        system.debug(response);
      if (response.getStatusCode() == 200) {
         
        dr.fileContent = EncodingUtil.base64Encode(response.getBodyAsBlob());
        String cdisp = response.getHeader('Content-Disposition');
           system.debug(cdisp);
        Integer st = cdisp.indexOf('filename=') + 9;
        dr.filename = cdisp.substring(st, cdisp.length());
      } else {
        dr.errorMsg = 'Error downloading file: ' + response.getStatus();
      }
    } catch (CalloutException ex) {
      System.debug(ex);
      dr.errorMsg = 'Exception calling Azure service: ' + ex.getMessage();
    }

    return JSON.serialize(dr);
  }
}