@isTest
global class FileDownloaderControllerTest  {

	@testSetup static void createApigeeSetting() {
		ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c(
			Service_URL__c = 'Service_URL__c',
			Service_Http_Method__c = 'GET',
			OAuth_Token__c = 'OAuth_Token__c',
			Client_Id__c = 'Client_Id__c',
			Token_URL__c = 'Token_URL__c',
			Name = 'Document Blob Service'
		);

		insert serviceSettings;
	}

	static testMethod void testDownloadAzureFile() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new AzureDownloadMock());
		String response = FileDownloaderController.downloadTheFile('Azure', 'Good file path');
		FileDownloaderController.DownloadResponse theFile = (FileDownloaderController.DownloadResponse) JSON.deserialize(response, FileDownloaderController.DownloadResponse.class);
		System.assertEquals('Good downloaded file.txt', theFile.filename);
		System.assertEquals(EncodingUtil.base64Encode(Blob.valueOf('This is the file content')), theFile.fileContent);
		Test.stopTest();
	}

	static testMethod void testInvalidFileSrc() {
		Test.startTest();
		String response = FileDownloaderController.downloadTheFile('InvalidSource', 'IrrelevantLocator');
		FileDownloaderController.DownloadResponse theFile = (FileDownloaderController.DownloadResponse) JSON.deserialize(response, FileDownloaderController.DownloadResponse.class);
		System.assertEquals(FileDownloaderController.INVALID_INPUT_MSG, theFile.errorMsg);
		Test.stopTest();
	}

	static testMethod void testInvalidBlobpath() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new AzureDownloadMock());
		String response = FileDownloaderController.downloadTheFile('Azure', 'BadLocator');
		FileDownloaderController.DownloadResponse theFile = (FileDownloaderController.DownloadResponse) JSON.deserialize(response, FileDownloaderController.DownloadResponse.class);
		System.assertEquals('Error downloading file: NOT FOUND', theFile.errorMsg);
		Test.stopTest();
	}

	static testMethod void testCalloutException() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new AzureDownloadMock());
		String response = FileDownloaderController.downloadTheFile('Azure', 'CalloutException');
		FileDownloaderController.DownloadResponse theFile = (FileDownloaderController.DownloadResponse) JSON.deserialize(response, FileDownloaderController.DownloadResponse.class);
		System.assertEquals('Exception calling Azure service: Generated callout exception', theFile.errorMsg);
		Test.stopTest();
	}

	global class AzureDownloadMock implements HttpCalloutMock {
		global HTTPResponse respond(HTTPRequest req) {
			HttpResponse resp = new HttpResponse();

			String endpoint = req.getEndpoint();
			List<String> lst = endpoint.split('blobpath=');
			if (lst[1] == 'BadLocator') {
				resp.setStatus('NOT FOUND');
				resp.setStatusCode(404);
			} else if (lst[1] == 'CalloutException') {
				throw new CalloutException('Generated callout exception');
			} else {
				resp.setStatus('OK');
				resp.setStatusCode(200);
				resp.setHeader('content-disposition', 'attachment; filename="Good downloaded file.txt"');
				resp.setBodyAsBlob(Blob.valueOf('This is the file content'));
			}

			return resp;

		}
	}

}