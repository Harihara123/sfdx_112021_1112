global class FixEventOwner implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('select Id, Type, Owner.Name from Event where Type in (\'Applicant\', \'Linked\', \'Submitted\', \'Interviewing\', \'Offer Accepted\', \'Not Proceeding\', \'Placed\') and Owner.Name != \'Talent Owner\'');
    }

    global void execute(Database.BatchableContext BC, List<Event> scope) {
        List<Event> eventList = new List<Event>();
        for (Event evt : scope){
            evt.OwnerId = '005240000038Gjf';
            eventList.add(evt);
        }
        
        DBUtils.UpdateRecords(eventList, false);
    }

    global void finish(Database.BatchableContext BC){
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Submittal events owner updates - ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}