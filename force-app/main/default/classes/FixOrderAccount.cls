global class FixOrderAccount implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id, AccountId, Opportunity.AccountId, LastModifiedById, LastModifiedBy.Profile.Name, '
                + ' Source_System_LastModifiedBy__c, Source_System_LastModifiedDate__c, LastModifiedDate, Next_Status__c, Status,'
                +'Submittal_Applicant_Source__c,Submittal_Not_Proceeding_Reason__c,Submittal_Placement_Source__c,'//added by Akshay ATS-2914
                +'(Select Type,Id,CreatedDate,Subject,Source__c,Not_Proceeding_Reason__c from Events )'//added by Akshay ATS-2914
                + ' FROM Order WHERE Account.RecordType.Name = \'Talent\'  and OpportunityId != Null  ');
    }

    global void execute(Database.BatchableContext BC, List<Order> scope) {
        List<Order> orderList = new List<Order>();
        for (Order o : scope){
            o.AccountId = o.Opportunity.AccountId;
            o.Next_Status__c = o.Status; // Added by Nidish to sync status and next status
            if (!(o.LastModifiedBy.Profile.Name.equalsIgnoreCase('System Integration'))) {
                o.Source_System_LastModifiedBy__c = o.LastModifiedById;
                o.Source_System_LastModifiedDate__c = o.LastModifiedDate;
            }
            
            //added by Akshay ATS-2914 start
            dateTime AppDT;
            dateTime NPDT;
            dateTime PlDT;
            for(Event E: O.Events ){
                if(E.Type == 'Applicant'){
                    if(String.isBlank(o.Submittal_Applicant_Source__c) || (AppDT < E.CreatedDate ) ){
                       o.Submittal_Applicant_Source__c =  E.Source__c;
                        AppDT = E.CreatedDate;
                       }
                }else if(E.Type == 'Not Proceeding'){
                     if(String.isBlank(o.Submittal_Not_Proceeding_Reason__c) || (NPDT < E.CreatedDate ) ){
                        o.Submittal_Not_Proceeding_Reason__c =  E.Not_Proceeding_Reason__c;
                        NPDT = E.CreatedDate;
                       }
                   
                }else if(E.Type == 'Placed'){
                     if(String.isBlank(o.Submittal_Placement_Source__c) || (PLDT < E.CreatedDate ) ){
                        o.Submittal_Placement_Source__c =  E.Source__c;
                        PLDT = E.CreatedDate;
                       }
                   
                }    
            }
            //added by Akshay ATS-2914 end
            orderList.add(o);
        }
        
        DBUtils.UpdateRecords(orderList, false);
    }

    global void finish(Database.BatchableContext BC){
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Order account ID updates - ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}