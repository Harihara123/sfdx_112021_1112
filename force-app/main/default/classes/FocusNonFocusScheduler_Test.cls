@isTest
public class FocusNonFocusScheduler_Test  {

	  @IsTest
	  static void scheduleNextRunTest(){
		Test.startTest();
          Id recordId = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Strategic Initiative').getRecordTypeId();
          
          Id recordId1 = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Account Plan').getRecordTypeId();
          
          Focus_NonFocus__c myCS2 =new Focus_NonFocus__c(name='Event Query',Query__c='select id from Event where Accountid in :AccountStrIdset AND ownerid in:strownerid');
          insert myCS2;
          
          FocusNonFocusBatchDate__c mycs1 = new FocusNonFocusBatchDate__c(name='BatchDate',BatchranDate__c=date.today());
          insert mycs1;
          //create account
          Account acc = TestDataHelper.createAccount();//name= Test Account;
          insert acc;
          // create strategic initiatives
          Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative1',Start_Date__c = system.Today() - 30,
                                                                   recordTypeId = recordId1,
                                                                   Impacted_Region__c = 'test',
                                                                   Account_Prioritization__c ='A',
                                                                   Primary_Account__c= acc.Id,
                                                                   OpCo__c='Aerotek');
          insert si;
          
          
          Event event = new Event(subject='Meeting - +i',DurationInMinutes = 5,ActivityDateTime = system.now(),whatid=acc.id);
          
          
          insert event;
          update event;
          
          Event event2 = new Event(subject='Meeting - +i',DurationInMinutes = 5,ActivityDateTime = system.now(), Priority__c='');
          insert event2;
          update event2;
        
        batchFocusNonFocusUpdate bclean = new batchFocusNonFocusUpdate();
		FocusNonFocusScheduler fnfs = new FocusNonFocusScheduler();
        fnfs.execute(null);
		Test.stopTest();
		}
	}