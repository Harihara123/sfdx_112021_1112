/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class ForgotPasswordController {
    public String username {get; set;}   
    public String dispLang {get; set;}   
    
    public ForgotPasswordController() {
        String cbParameter = ApexPages.currentPage().getParameters().get('expid');
        if(cbParameter != null){
            List<String> strngs = cbParameter.split('LANGCODE');
            if(strngs.size() > 1){
                dispLang = strngs[1];
            }
        }
        if(dispLang == null){
            dispLang = 'en_US';
        }        
    }
	
  	public PageReference forgotPassword() {
  		boolean success = Site.forgotPassword(username);
  		PageReference pr = Page.JobSeeker_ForgotPasswordConfirm;
        pr.getParameters().put('langCode',dispLang);
  		pr.setRedirect(true);
  		
  		if (success) {  			
  			return pr;
  		}
  		return null;
  	}
  	
  	 @IsTest(SeeAllData=true) public static void testForgotPasswordController() {
    	// Instantiate a new controller with all parameters in the page
    	ForgotPasswordController controller = new ForgotPasswordController();
    	controller.username = 'test@salesforce.com';     	
    
    	System.assertEquals(controller.forgotPassword(),null); 
    }
}