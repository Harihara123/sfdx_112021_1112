public class FyreSyncTriggerHandler {
    public Map<String,String>CurrValueMap = new Map<String,String>();
    public Map<String,String> twoDigitCountryMap= new Map<String,String>();
	public Map<String,String> threDigitCountryMap=new Map<String,String>();
    public Map<String,String> VmsTypeMap= new Map<String,String>();
    public void onAfterInsert(List<FyreSync__Requisition__c> TriggerNewList){

        try {
			List<Opportunity> OppListToInsert=new List<Opportunity>();
            
			Schema.DescribeFieldResult CurrencyValues = Opportunity.Currency__c.getDescribe();
			List<Schema.PicklistEntry> currencyPicklistList = CurrencyValues.getPicklistValues();
			for (Schema.PicklistEntry currValue: currencyPicklistList) {
    			String Currency3Digit=(currValue.getValue().split('-').get(0)).trim();
    			CurrValueMap.put(Currency3Digit,currValue.getValue().trim());
			}
            
			Map<String,CountryMapping__mdt> cur= CountryMapping__mdt.getAll();
			for (String str: cur.keySet()) {
    			twoDigitCountryMap.put(cur.get(str).Label,cur.get(str).CountryName__c);
    			threDigitCountryMap.put(cur.get(str).CountryCode__c,cur.get(str).CountryName__c);
			}
            
            Map<String,FyreSyncVMSTypes__mdt> VmsType= FyreSyncVMSTypes__mdt.getAll();
			System.debug('*********VmsType****'+VmsType);
			for (String str: VmsType.keySet()) {
    			VmsTypeMap.put(str,VmsType.get(str).Value__c);    
			}
            
        	for (FyreSync__Requisition__c fyreSyncRec: TriggerNewList) {            
            	OppListToInsert.add(OpportunityMapping(fyreSyncRec));            
        	}
            System.debug('**********OppListToInsert***'+OppListToInsert);
        	if (OppListToInsert.size() >0) {
            	Database.SaveResult[] saveResultList =Database.insert(OppListToInsert,false);
            	for (Database.SaveResult sr : saveResultList) {
    				if (sr.isSuccess()) {
                		System.debug('Successfully inserted opportunity. Opportunity ID: ' + sr.getId());
    				} else {
        				for(Database.Error err : sr.getErrors()) {
            				System.debug('****The following error has occurred.');                    
            				System.debug(err.getStatusCode() + ': ' + err.getMessage());
            				System.debug('****fields that affected this error: ' + err.getFields());
        				}
    				}
        		}
        	}            
        } catch(Exception excp) {
            ConnectedLog.LogException('FyreSyncTriggerHandler','onAfterInsert',excp);
        }        
    }
    public Opportunity OpportunityMapping(FyreSync__Requisition__c fyreSyncRec){
        Opportunity FyreSyncOpportunity= new Opportunity();
        FyreSync_Defaults__mdt fsDefaults = [Select Office_Id__c, Owner_Id__c from FyreSync_Defaults__mdt where DeveloperName = 'Defaults'];
        
        FyreSyncOpportunity.AccountId = fyreSyncRec.FyreSync__Account__c;
        String AddressLine=fyreSyncRec.FyreSync__ReqDetails_AddressLine1__c;

        if (AddressLine.length() > 0 &&  fyreSyncRec.FyreSync__ReqDetails_AddressLine2__c != null && fyreSyncRec.FyreSync__ReqDetails_AddressLine2__c != '') {
            AddressLine=AddressLine + ', ' + fyreSyncRec.FyreSync__ReqDetails_AddressLine2__c;
        }

        FyreSyncOpportunity.Req_Worksite_Street__c = AddressLine;      
        FyreSyncOpportunity.Req_Bill_Rate_Max__c = fyreSyncRec.FyreSync__Rates_BillRateMax__c;        
        FyreSyncOpportunity.Req_Bill_Rate_Min__c = fyreSyncRec.FyreSync__Rates_BillRateMin__c;        
        FyreSyncOpportunity.Req_Worksite_City__c = fyreSyncRec.FyreSync__ReqDetails_City__c;        
        FyreSyncOpportunity.Req_VMS_Additional_Information__c = fyreSyncRec.FyreSync__ReqDetails_Comments__c;
        FyreSyncOpportunity.Req_VMS_END_CLIENT__c = fyreSyncRec.FyreSync__BuyerDetails_Company__c;        
        
        String countryCode=fyreSyncRec.FyreSync__ReqDetails_CountryCode__c;
        if (countryCode.length()==2) {
            FyreSyncOpportunity.Req_Worksite_Country__c = twoDigitCountryMap.get(countryCode);
        } else if (countryCode.length()==3) {
            FyreSyncOpportunity.Req_Worksite_Country__c = threDigitCountryMap.get(countryCode);
        } else {
            FyreSyncOpportunity.Req_Worksite_Country__c = fyreSyncRec.FyreSync__ReqDetails_CountryCode__c;
        }  

        FyreSyncOpportunity.Currency__c = CurrValueMap.get(fyreSyncRec.FyreSync__Rates_Currency__c);        
        FyreSyncOpportunity.Req_Job_Description__c = fyreSyncRec.FyreSync__ReqDetails_Description__c;
        FyreSyncOpportunity.Req_VMS_Job_Description__c = fyreSyncRec.FyreSync__ReqDetails_Description__c;       
        FyreSyncOpportunity.Req_Duration__c = fyreSyncRec.FyreSync__ReqDetails_Duration__c;       
        FyreSyncOpportunity.Req_Minimum_Education_Required__c = fyreSyncRec.FyreSync__ReqDetails_Education__c;        
        FyreSyncOpportunity.OwnerId = fyreSyncRec.FyreSync__AtsFields_CustomText2__c != null ? fyreSyncRec.FyreSync__AtsFields_CustomText2__c : fsDefaults.Owner_Id__c;        
        FyreSyncOpportunity.Req_Pay_Rate_Max__c = fyreSyncRec.FyreSync__Rates_PayRateMax__c;        
        FyreSyncOpportunity.Req_Pay_Rate_Min__c = fyreSyncRec.FyreSync__Rates_PayRateMin__c;
        //All reqs rom VMS will have the type 'Contract'
        FyreSyncOpportunity.Req_Product__c = 'Contract'; //fyreSyncRec.FyreSync__ReqDetails_PositionType__c;        
        FyreSyncOpportunity.Req_Worksite_Postal_Code__c = fyreSyncRec.FyreSync__ReqDetails_PostalCode__c;
        FyreSyncOpportunity.Req_Top_Skills__c = fyreSyncRec.FyreSync__ReqDetails_Skills__c;   
        // FyreSyncOpportunity.Req_Skill_Specialty__c =  fyreSyncRec.FyreSync__AtsFields_CustomText4__c;
        FyreSyncOpportunity.Req_Worksite_State__c = fyreSyncRec.FyreSync__ReqDetails_State__c;        
        FyreSyncOpportunity.Req_VMS_Requirement_Status__c = fyreSyncRec.FyreSync__Status__c;        
        FyreSyncOpportunity.Req_Max_Submissions_Per_Supplier__c = fyreSyncRec.FyreSync__submittalMax__c;        
        FyreSyncOpportunity.Req_Job_Title__c = fyreSyncRec.FyreSync__ReqDetails_Title__c;        
        FyreSyncOpportunity.Req_Total_Positions__c = fyreSyncRec.FyreSync__ReqDetails_TotalOpenings__c;
        FyreSyncOpportunity.Req_Origination_System__c = fyreSyncRec.FyreSync__VmsType__c;
        FyreSyncOpportunity.Req_SourceReq__c = fyreSyncRec.Name;
        FyreSyncOpportunity.Source_System_Id__c = fyreSyncRec.Name;
        FyreSyncOpportunity.External_Oppty_Id__c = fyreSyncRec.Name;
        
        /*FyreSyncOpportunity.Req_Total_Filled__c=0;
        FyreSyncOpportunity.Req_Total_Lost__c=0;
        FyreSyncOpportunity.Req_Total_Washed__c=0;*/
		FyreSyncOpportunity.Req_Rate_Frequency__c = 'Hourly';
        FyreSyncOpportunity.Req_Duration_Unit__c = 'Weekly';
        
        //Other fields on opportunity  
        List<Product2> legacyProdId = [SELECT Id FROM Product2 WHERE Division_Name__c = 'VMS' AND IsActive = true AND name = 'VMS' AND OpCo__c = 'TEKsystems, Inc.'];
        List<User_Organization__c> userOrg = [SELECT Id FROM User_Organization__c WHERE 
                                                Office_Code__c =: fyreSyncRec.FyreSync__AtsFields_CustomText1__c AND OpCo_Code__c = 'TEK'];

        FyreSyncOpportunity.Req_Division__c = 'VMS';        
        FyreSyncOpportunity.Req_Merged_with_VMS_Req__c = false;             
        FyreSyncOpportunity.Legacy_Record_Type__c = 'Staging Req';
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        FyreSyncOpportunity.RecordTypeId = reqRecordTypeId;
        FyreSyncOpportunity.Legacy_Product__c = legacyProdId[0].Id;
        FyreSyncOpportunity.OpCo_Id__c = 'TEK';
        FyreSyncOpportunity.OpCo__c = 'TEKsystems, Inc.';
        FyreSyncOpportunity.Type_of_Business__c = 'Staffing';
        FyreSyncOpportunity.Req_Origination_Partner_System_Id__c = VmsTypeMap.get((fyreSyncRec.FyreSync__VmsType__c).toUpperCase());
        FyreSyncOpportunity.Organization_Office__c = !userOrg.isEmpty() ? userOrg[0].Id : fsDefaults.Office_Id__c;
        FyreSyncOpportunity.Name = 'Fyre Sync Opportunity - ' + DateTime.now();
        FyreSyncOpportunity.StageName = 'Staging';        
        FyreSyncOpportunity.CloseDate = Date.today().addDays(30);
        FyreSyncOpportunity.Req_VMS__c = true;
        
        return FyreSyncOpportunity;
    }
}