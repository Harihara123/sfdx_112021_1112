public class GenerateJSON{
	public String ContactId;
	public Profile Profile;

	public class Profile{
		public String Name;
		public String Email;
		public LastUpdate LastUpdate;
		public List<Phone> Phone;
		public Address Address;
		public JobTitle JobTitle;
		public List<Skills> Skill;
	}
	 public class LastUpdate{
		public String Source;
		public DateTime UpdateDate;
	 }

	 public class Phone{
		public String ContactNumber;
		public String Type;
	 }

	 public class Address{
		public String Street;
		public String City;
		public String State;
		public String Country;
		public Decimal Longitude;
		public Decimal Latitude;
	 }

	 public class JobTitle{
		public String Value;
		public String ID;
		public String Status;
		public Datetime UpdateDate;
		public Source Source;
	 }

	 public class Source{
		public String Value;
		public String User;
	 }

	public class Skills{		
		public String Skills;
		public String ID;
		public String Status;
		public Datetime UpdateDate;
		public Source Source;
	 }

}