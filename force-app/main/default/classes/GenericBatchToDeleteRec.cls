/**
  @author : Santhosh Gundu
  @Date : 09/20/2021
  @Description : This is generic batch class which is perform delete operation on objects
*/
public class GenericBatchToDeleteRec implements Database.Batchable<SObject>,Database.Stateful,Schedulable{
    private List<String> objNameList = new list<string>();
    Integer mdtSize=0;
    String query = '';
    public GenericBatchToDeleteRec(List<String> finishedObjList) {
        objNameList.addAll(finishedObjList);
        init();
    }
   public GenericBatchToDeleteRec() {
        init();
    }
	 /**
	 * @description gets invoked when the batch class called
	 * @dliList contains the custom metadata values
	 * @returns the record set as a Query that will be batched for start
	 */ 
    public void init(){
        Deletable_Log_Info__mdt selctedObj;
        String fnishedObj = objNameList.size() >0 ? String.join(objNameList, ';') :'';
        Boolean flag=false;
        List<Deletable_Log_Info__mdt> dliList = new  List<Deletable_Log_Info__mdt>();
       dliList= Deletable_Log_Info__mdt.getall().values();
        mdtSize = dliList.size();
        for(Deletable_Log_Info__mdt dli : dliList){
            if(fnishedObj.contains(dli.Label)) {
                flag = false;              
            }
            else {
                flag = true;
                selctedObj = dli;
                break;
            }
        } 
        if(flag) {
            query = 'Select id from '+selctedObj.label +' where  CreatedDate < LAST_N_DAYS: ' + selctedObj.Older_Days__c +' WITH SECURITY_ENFORCED'; 
             objNameList.add(selctedObj.label);
         }
    }
    
    /**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
    public Database.QueryLocator start(Database.BatchableContext context){
        return Database.getQueryLocator(query);
    }
    
    /**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */
    public void execute(Database.BatchableContext context,List<SObject> scope){
        try {
           if (!scope.isEmpty() && scope != null)
			{
    			delete scope;
                Database.emptyRecycleBin(scope);
			}	
		}
		catch(DmlException e) {
			ConnectedLog.LogException('GenericBatchToDeleteRec', 'execute', e);	
		}
		catch(Exception e) {
			ConnectedLog.LogException('GenericBatchToDeleteRec', 'execute', e);
		}   
    }
    
    /**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */
    public void finish(Database.BatchableContext context){
        if(objNameList.size() < mdtSize) {  
          Database.executeBatch(new GenericBatchToDeleteRec(objNameList),2000);
        }   
    }
    
    public void execute(SchedulableContext sc)
	{
		GenericBatchToDeleteRec b = new GenericBatchToDeleteRec();
		database.executebatch(b);
	}
}