public class GeoLocationQueueable  implements Queueable, Database.AllowsCallouts {
    private Map<Id,Opportunity> opportunityMap {get; set;}
    private boolean isGeolocationRequired {get; set;}
    
    public GeoLocationQueueable(Map<Id,Opportunity> oppMap, boolean isGeolocationRequired)
    	{
        this.opportunityMap = oppMap;
        this.isGeolocationRequired = isGeolocationRequired;
    }    
    public void execute(QueueableContext context){ 
		System.debug('entering in q');
        OpportunityGeoLocHelper geo = new OpportunityGeoLocHelper(opportunityMap.keyset());
        geo.getGeoLocationFromAPI();
        geo.updateOpportunitiesWithLoc();
    }    
}