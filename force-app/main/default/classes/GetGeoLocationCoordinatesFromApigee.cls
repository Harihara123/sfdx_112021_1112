public class GetGeoLocationCoordinatesFromApigee
{
    static ApigeeOAuthSettings__c settings;
     public class InvocableApiException extends Exception {}
     
    public GetGeoLocationCoordinatesFromApigee() 
    {
        settings = new ApigeeOAuthSettings__c();
        settings = ApigeeOAuthSettings__c.getValues('GetGeoLocation');
        System.debug('settings::::'  + settings);
    }
    
    public static void getAccessToken()
    {        
        oAuth_Connector oauth = new oAuth_Connector();
        oauth.token_url = settings.Token_URL__c;
        oauth.client_Id = settings.Client_Id__c;
        oauth.client_Secret = settings.Client_Secret__c;
        system.debug('****TOKEN B4**** ' + oauth);
        oauth = oauth.getAccessToken(oauth);
        settings.OAuth_Token__c = oauth.access_Token;
        settings.Token_Expiration__c = oauth.expires_In;
        system.debug('****settings**** ' + settings);
       
        
    }
    
   
    static public Opportunity getLocationOpportunityByZip(Opportunity objRecs, string zipName, string country)
    {
        settings = new ApigeeOAuthSettings__c();
        settings = ApigeeOAuthSettings__c.getValues('GetGeoLocation');
        System.debug('settings::::'  + settings);
        
        string zipCode = zipName.split('[\\s@&.?$+-]+')[0];
        
        system.debug('zipCode::: ' + zipCode);
        
        if ((settings.Token_Expiration__c == null) || (settings.Token_Expiration__c < System.now())) 
        {
            getAccessToken();
        }
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
        zipcode= EncodingUtil.urlEncode(+zipCode, 'UTF-8');
        country= EncodingUtil.urlEncode(+country,'UTF-8');
        
          string queryString = 'zipcode='+zipCode+'&country='+country;
        
        


        
        System.debug('settings.OAuth_Token__c >>>>>>>> ' + settings.OAuth_Token__c);
        System.debug('settings>>>>>>>> ' + settings.Service_URL__c + queryString);
        req.setEndpoint(settings.Service_URL__c + queryString);
        req.setMethod(settings.Service_Http_Method__c);
        req.setTimeout(120000);
        
        req.setHeader('Authorization', 'Bearer ' + settings.OAuth_Token__c);
        String responseBody;
        try 
        {
            HttpResponse response = connection.send(req);
            System.debug('response >>>>>>>> ' + response);
            responseBody = response.getBody();
            System.debug('responseBody >>>>>>>> ' + responseBody);
            
            if (String.isBlank(responseBody)) 
            {
                 objRecs.Req_Geolocation__Latitude__s = null;
                objRecs.Req_Geolocation__Longitude__s = null;   
                throw new InvocableApiException('Apigee search service call returned empty response body!');
            }
           
            else
            {
                Decimal latitude = null;
                Decimal longitude = null;
                JSONParser parser = JSON.createParser(responseBody);
                while (null != parser.nextToken()) 
                {
                    if (parser.getCurrentName() == 'Latitude')
                    {
                        parser.nextToken();
                        latitude = Decimal.valueof(parser.getText());
                        //objRecs.Req_Geolocation__Latitude__s = Decimal.valueof(parser.getText());
                    } 
                    else if (parser.getCurrentName() == 'Longitude') 
                    {
                        parser.nextToken();
                        longitude = Decimal.valueof(parser.getText());
                        //objRecs.Req_Geolocation__Longitude__s = Decimal.valueof(parser.getText());
                    }
                }
                objRecs.Req_Geolocation__Latitude__s = latitude;
                objRecs.Req_Geolocation__Longitude__s = longitude;
                
            }
             update settings;
        } 
        catch (Exception ex) 
        {
            // Callout to apigee failed
            System.debug(LoggingLevel.ERROR, 'Apigee call to Search Service failed! ' + ex.getMessage());
             objRecs.Req_Geolocation__Latitude__s = null;
             objRecs.Req_Geolocation__Longitude__s = null;
             return objRecs;
            //throw ex;
        }
        
        return objRecs;
    }
    
}