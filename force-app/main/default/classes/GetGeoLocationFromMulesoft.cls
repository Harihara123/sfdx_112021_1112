public with sharing class GetGeoLocationFromMulesoft{
    static Mulesoft_OAuth_Settings__c settings;
    public class InvocableApiException extends Exception {}
    public static String accessToken;

    public GetGeoLocationFromMulesoft(){
    	settings = new Mulesoft_OAuth_Settings__c();
        settings = Mulesoft_OAuth_Settings__c.getValues('GetGeoService');
        System.debug('settings::::'  + settings);
    }   
    public static String getAccessToken(){      
        MuleSoftOAuth_Connector.OAuthWrapper oauthwrap=MuleSoftOAuth_Connector.getAccessToken('GetGeoService');       
        system.debug('****oauthwrap**** ' + oauthwrap);
        return(oauthwrap.token);
    }    
	public static Map<String,Opportunity> getLocationOpportunityByZip(List<Opportunity> objRecs){
    	List<locationPoint> geoList = new List<locationPoint>();
        Map<String,Opportunity> geoResponseOppMap = new Map<String,Opportunity>();        
        if(objRecs.size() > 0){
            Map<String,locationPoint> geoMap = new Map<String,locationPoint>();
            for(Opportunity opp: objRecs){
                String zip = !String.IsBlank(opp.Req_Worksite_Postal_Code__c) ? opp.Req_Worksite_Postal_Code__c : '00000' ;
                String country = !String.IsBlank(opp.Req_Worksite_Country__c) ? opp.Req_Worksite_Country__c : 'US';
                geoList.add(new locationPoint(zip, country , String.ValueOf(opp.Id)));
            }
        }      
        if(geoList.size() > 0){         
        	JSONGenerator gen = JSON.createGenerator(true);
         	gen.writeStartObject();
          	gen.writeFieldName('request');
            gen.writeStartArray();
            for (integer i = 0; i < geoList.size(); i++){
            	gen.writeStartObject();
                gen.writeStringField('zipcode',geoList[i].zipcode);
                gen.writeStringField('country',geoList[i].country);
                gen.writeStringField('id',geoList[i].id);
                gen.writeEndObject();
            }
            gen.writeEndArray();
          	gen.writeEndObject();         
            settings = new Mulesoft_OAuth_Settings__c();
            settings = Mulesoft_OAuth_Settings__c.getValues('GetGeoService');
            System.debug('settings::::'  + settings);
            System.debug('JSON REQUEST '+JSON.serialize(geoList));        	        	
            accessToken=getAccessToken();
            Http connection = new Http();
            HttpRequest req = new HttpRequest();
            //settings.OAuth_Token__c=accessToken;
            System.debug('accessToken >>>>>>>> ' + accessToken);
            //req.setEndpoint(settings.Service_URL__c);
            req.setEndpoint(settings.Endpoint_URL__c);
            //req.setMethod(settings.Service_Http_Method__c);
            req.setMethod(settings.Service_Method__c);
            req.setbody(gen.getAsString());
            req.setHeader('Content-Type', 'application/json');
            //req.setHeader('Authorization', 'Bearer ' + settings.OAuth_Token__c);
            req.setHeader('Authorization', 'Bearer ' + accessToken);            
            req.setTimeout(120000);
        
        	String responseBody;
        	String responseBodyStatus;
        	System.debug('JSON STRING '+gen.getAsString());
        	try{
            	HttpResponse response = connection.send(req);
            	System.debug('response >>>>>>>> ' + response);
            	responseBody = response.getBody();            
                System.debug('responseBody >>>>>>>> ' + responseBody);
            
            	if (String.isBlank(responseBody)){
                	throw new InvocableApiException('Mulesoft search service call returned empty response body!');
            		//}else if(response.getStatus() == 'OK' && response.getStatusCode() ==200){
            	}else if(response.getStatusCode() ==200){
                	JsonGeoParser r = JsonGeoParser.parse(responseBody);
                	responseBodyStatus=r.response.header.status;
                	system.debug('***********responseBodyStatus*****'+responseBodyStatus);
                	if(responseBodyStatus == 'OK'){
                    	Opportunity opp;
                 		for(JsonGeoParser.result rlt : r.response.result){
                       		opp = new Opportunity();
                       		opp.Id = !String.isBlank(rlt.id) ? Id.valueOf(rlt.id) : null;
                       		opp.Req_GeoLocation__Latitude__s = String.isBlank(rlt.latlong) ? null :Decimal.valueOf(rlt.latlong);
                       		opp.Req_GeoLocation__Longitude__s = String.isBlank(rlt.longitude) ? null: Decimal.valueOf(rlt.longitude);
                       		geoResponseOppMap.put(rlt.id, opp);
                    	}
                 		System.debug('FINAL LIST '+geoResponseOppMap);
                	}           
            	}else{
              	System.debug('GATEWAY TIMEOUT');   
            	}
            	//update settings;
        	} 
        	catch (Exception ex){
            	// Callout to apigee failed
            	System.debug(LoggingLevel.ERROR, 'Mulesoft call to Search Service failed! ' + ex.getMessage());
             	return geoResponseOppMap;
        	}
		}
    	return geoResponseOppMap;
	}
    
	public class locationPoint{
		public String zipcode;
    	public String country;
    	public String id;
    
    	public locationPoint(String zip, String countryName,String oppId) {
       		zipcode = zip;
       		country = countryName;
       		id = oppId;
    	}
	}    
}