/***************************************************************************
Name        : GetLatLongFromSOA
Created By  : Karthik Arapalli
Date        : 08/27/2017
Purpose     : To make a callout to SOA to get Mailing Latitude and Mailing Longitude

*****************************************************************************/

public class GetLatLongFromSOA  {
	static ApigeeOAuthSettings__c apisettings;
	static Mulesoft_OAuth_Settings__c muleSettings; 
	public static String accessToken;

	public class InvocableApiException extends Exception {}

	/*public GetLatLongFromSOA(){
		apisettings = new ApigeeOAuthSettings__c();
        apisettings = ApigeeOAuthSettings__c.getValues('GetGeoLocation');
		System.debug('apisettings::::'  + apisettings);
	}*/

	public static List<Contact> getLatLong (List<ID> accIds){
		List<Contact> contactList = new List<Contact>();
		List<latlongRequest> reqList = new List<latlongRequest>();
		if(accIds.size() > 0){
			for(Contact c : [SELECT Id, AccountId, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, Talent_Country_Text__c, MailingLatitude, MailingLongitude, GoogleMailingLatLong__Latitude__s, GoogleMailingLatLong__Longitude__s FROM Contact WHERE Contact.AccountId IN : accIds]){
				reqList.add(new latlongRequest(c.MailingPostalCode, c.MailingCity, c.MailingState, c.MailingCountry, String.ValueOf(c.Id)));
			}
		}

		if(reqList.size() > 0){
			JSONGenerator gen = JSON.createGenerator(true);
				gen.writeStartObject();
				gen.writeFieldName('request');
				gen.writeStartArray();
				for (integer i = 0; i < reqList.size(); i++){
						gen.writeStartObject();
						if(reqList[i].zipcode != null){
							gen.writeStringField('zipcode',reqList[i].zipcode);
						}
						if(reqList[i].city != null){
							gen.writeStringField('city',reqList[i].city);
						}
						if(reqList[i].state != null){
							gen.writeStringField('state',reqList[i].state);
						}
						if(reqList[i].country != null){
							gen.writeStringField('country',reqList[i].country);
						}
						gen.writeStringField('id',reqList[i].acctid);
						gen.writeEndObject();
					}
				gen.writeEndArray();
				gen.writeEndObject();

				apisettings = new ApigeeOAuthSettings__c();
				apisettings = ApigeeOAuthSettings__c.getValues('SOALatLong');
				System.debug('apisettings::::'  + apisettings);
		
				Http connection = new Http();
				HttpRequest req = new HttpRequest();
				
				req.setEndpoint(apisettings.Service_URL__c);
				req.setMethod(apisettings.Service_Http_Method__c);
				req.setbody(gen.getAsString());
				System.debug('Request String !!!!' +gen.getAsString());
				req.setHeader('Content-Type', 'application/json');
				req.setHeader('Authorization', 'Bearer ' + apisettings.OAuth_Token__c);
				String timeOutValue = Label.SOALatLongTimeout;
				req.setTimeout(Integer.valueOf(timeOutValue));

				String responseBody;

				try 
				{
					HttpResponse response = connection.send(req);
					System.debug('Response !!!! ' + response);
					responseBody = response.getBody();
					System.debug('ResponseBody !!!! ' + responseBody);
            
					if (String.isBlank(responseBody)) {
						System.debug('No Response !!!!');
						throw new InvocableApiException('Apigee search service call returned empty response body!');
					}
					else if(response.getStatus() == 'OK' && response.getStatusCode() == 200){
						JsonGeoParser r = JsonGeoParser.parse(responseBody);
						Contact c;
						for(JsonGeoParser.result rlt : r.response.result){
							c = new Contact();
							c.Id = !String.isBlank(rlt.id) ? Id.valueOf(rlt.id) : null;
							c.MailingLatitude  = !String.isBlank(rlt.latlong) ? Decimal.valueOf(rlt.latlong) : 0.00;
							c.MailingLongitude = !String.isBlank(rlt.longitude) ? Decimal.valueOf(rlt.longitude) : 0.00;
							c.LatLongSource__c = 'GeoNames';
							contactList.add(c);
						}
						System.debug('Mailing Lat and Long !!! '+ c.MailingLatitude +',' + c.MailingLongitude);	
					}
					else{
						System.debug('GATEWAY TIMEOUT');   
					}
				}
				catch(System.CalloutException e) {	
					return populatEmptyLatLong(accIds);			
				}
				catch (Exception ex) {
					// Callout to apigee failed
					System.debug(LoggingLevel.ERROR, 'Apigee call to Search Service failed! ' + ex.getMessage());
					return populatEmptyLatLong(accIds);	
					//return contactList;
				}
		}
		
		return contactList;
	}

	public static List<Contact> getLatLongFromMulesoft (List<Id> accIds){
		List<Contact> contactList = new List<Contact>();
		List<latlongRequest> reqList = new List<latlongRequest>();
		if(accIds.size() > 0){
			for(Contact c : [SELECT Id, AccountId, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, Talent_Country_Text__c, MailingLatitude, MailingLongitude, GoogleMailingLatLong__Latitude__s, GoogleMailingLatLong__Longitude__s FROM Contact WHERE Contact.AccountId IN : accIds]){
				reqList.add(new latlongRequest(c.MailingPostalCode, c.MailingCity, c.MailingState, c.MailingCountry, String.ValueOf(c.Id)));
			}
		}

		if(reqList.size() > 0){
			JSONGenerator gen = JSON.createGenerator(true);
				gen.writeStartObject();
				gen.writeFieldName('request');
				gen.writeStartArray();
				for (integer i = 0; i < reqList.size(); i++){
						gen.writeStartObject();
						if(reqList[i].zipcode != null){
							gen.writeStringField('zipcode',reqList[i].zipcode);
						}
						if(reqList[i].city != null){
							gen.writeStringField('city',reqList[i].city);
						}
						if(reqList[i].state != null){
							gen.writeStringField('state',reqList[i].state);
						}
						if(reqList[i].country != null){
							gen.writeStringField('country',reqList[i].country);
						}
						gen.writeStringField('id',reqList[i].acctid);
						gen.writeEndObject();
					}
				gen.writeEndArray();
				gen.writeEndObject();

				muleSettings = new Mulesoft_OAuth_Settings__c();
				muleSettings = Mulesoft_OAuth_Settings__c.getValues('GetGeoService');
				System.debug('muleSettings::::'  + muleSettings);

				accessToken=GetGeoLocationFromMulesoft.getAccessToken();
				System.debug('accessToken >>>>>>>> ' + accessToken);

				Http connection = new Http();
				HttpRequest req = new HttpRequest();
				
				req.setEndpoint(muleSettings.Endpoint_URL__c);
				req.setMethod(muleSettings.Service_Method__c);
				req.setbody(gen.getAsString());
				System.debug('Request String !!!!' +gen.getAsString());
				req.setHeader('Content-Type', 'application/json');
				req.setHeader('Authorization', 'Bearer ' +accessToken);
				String timeOutValue = Label.SOALatLongTimeout;
				req.setTimeout(Integer.valueOf(timeOutValue));

				String responseBody;

				try 
				{
					HttpResponse response = connection.send(req);
					System.debug('Response !!!! ' + response);
					responseBody = response.getBody();
					System.debug('ResponseBody !!!! ' + responseBody);
            
					if (String.isBlank(responseBody)) {
						System.debug('No Response !!!!');
						throw new InvocableApiException('muleSoft search service call returned empty response body!');
					}
					else if(response.getStatus() == 'OK' && response.getStatusCode() == 200){
						JsonGeoParser r = JsonGeoParser.parse(responseBody);
						Contact c;
						for(JsonGeoParser.result rlt : r.response.result){
							c = new Contact();
							c.Id = !String.isBlank(rlt.id) ? Id.valueOf(rlt.id) : null;
							c.MailingLatitude  = !String.isBlank(rlt.latlong) ? Decimal.valueOf(rlt.latlong) : 0.00;
							c.MailingLongitude = !String.isBlank(rlt.longitude) ? Decimal.valueOf(rlt.longitude) : 0.00;
							c.LatLongSource__c = 'GeoNames';
							contactList.add(c);
						}
						System.debug('Mailing Lat and Long !!! '+ c.MailingLatitude +',' + c.MailingLongitude);	
					}
					else{
						System.debug('GATEWAY TIMEOUT');   
					}
				}
				catch(System.CalloutException e) {	
					return populatEmptyLatLong(accIds);			
				}
				catch (Exception ex) {
					// Callout to apigee failed
					System.debug(LoggingLevel.ERROR, 'Apigee call to Search Service failed! ' + ex.getMessage());
					return populatEmptyLatLong(accIds);	
					//return contactList;
				}
		}
		
		return contactList;
	}

	private static List<Contact> populatEmptyLatLong(List<ID> accIds) {
		List<Contact> contacts = new List<Contact>();
		for(Contact c : [SELECT Id,MailingLatitude, MailingLongitude,LatLongSource__c FROM Contact WHERE Contact.AccountId IN : accIds]){
						c.MailingLatitude = 0.00;
						c.MailingLongitude = 0.00;
						c.LatLongSource__c = 'GeoNames';
						contacts.add(c);
		}
		return contacts;
	}

	public class latlongRequest{
		public String zipcode;
		public String city;
		public String state;
		public String country;
		public String acctid;
    
		public latlongRequest(String zip, String cityName, String stateName, String countryName,String accId) {
		   zipcode = zip;
		   city = cityName;
		   state = stateName;
		   country = countryName;
		   acctid = accId;
		}
	 }  
 
	public class latlongResponse{
	   public String acctid;
	   public String latitute;
	   public String longitude;
  
	   public latlongResponse(String accId, String lat, String lng){
		 acctid = accId;
		 latitute = lat;
		 longitude = lng;
	   }
	 }
}