@isTest
public class GetLatLongFromSOA_Test  {
	static testmethod void testGetLatLongFromSOA(){
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
		Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
		List<Id> accIds = new List<Id>(); 
        
		Account a = new Account();
        a.name = 'Test Account';
        a.RecordTypeId = accRecordTypeId;
        insert a;
        accIds.add(a.Id);
		System.debug('created account---'+a.id);

        Contact c = new Contact();
        c.accountId = a.Id;
        c.firstname = 'Test';
        c.lastname = 'Contact';
        c.Phone = '1112223330';
        c.MailingCity = 'Elkridge';
		c.MailingState = 'Maryland';
        c.MailingCountry = 'USA';
        c.MailingPostalCode = '21075';
        c.RecordTypeId = conRecordTypeId;
        insert c;
		System.debug('created contact---'+c.id);

        String profileName = 'System Administrator';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'Test LastName';
        testUser.Firstname = 'Test FirstName';
        testUser.Alias = 'test';
        testUser.CommunityNickname = '12345';
        profile prof = [select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
        insert testUser;
		System.debug('created user---'+testUser.id);

        //create the custom setting
        insert new ApigeeOAuthSettings__c(name = 'SOALatLong', Client_Id__c='1234567', Service_Http_Method__c='POST', Service_URL__c='http://allegisgrouptest.com',Token_URL__c='http://test.com');

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('SOALatLong_Test');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
		Test.startTest();
        System.runAs(testUser) {
			GetLatLongFromSOA.getLatLong(accIds);
		}
        Test.stopTest();

	}

	static testmethod void testGetLatLongFromMulesoft(){
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
		Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
		List<Id> accIds = new List<Id>(); 
        
		Account a = new Account();
        a.name = 'Test Account';
        a.RecordTypeId = accRecordTypeId;
        insert a;
        accIds.add(a.Id);
		System.debug('created account---'+a.id);

        Contact c = new Contact();
        c.accountId = a.Id;
        c.firstname = 'Test';
        c.lastname = 'Contact';
        c.Phone = '1112223330';
        c.MailingCity = 'Elkridge';
		c.MailingState = 'Maryland';
        c.MailingCountry = 'USA';
        c.MailingPostalCode = '21075';
        c.RecordTypeId = conRecordTypeId;
        insert c;
		System.debug('created contact---'+c.id);

        String profileName = 'System Administrator';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'Test LastName';
        testUser.Firstname = 'Test FirstName';
        testUser.Alias = 'test';
        testUser.CommunityNickname = '12345';
        profile prof = [select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
        insert testUser;
		System.debug('created user---'+testUser.id);

        //create the custom setting
        insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
				Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
				OAuth_Token__c='123456788889',
				ClientID__c ='abe1234',
				Client_Secret__c ='hkbkheml',
				Service_Method__c='POST',
				Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
				Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
			);

        
         Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
		Test.startTest();
        System.runAs(testUser) {
			//UpdateTalentMailingLatLong.updateMailingLatLong(accIds);
			GetLatLongFromSOA.getLatLongFromMulesoft(accIds);
		}
        Test.stopTest();

	}


	static testmethod void testUpdateTalentMailingLatLong(){
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
		Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
		List<Id> accIds = new List<Id>(); 
		Test.startTest();
		insert new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
			Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication?',
			OAuth_Token__c='123456788889',
			ClientID__c ='abe1234',
			Client_Secret__c ='hkbkheml',
			Service_Method__c='POST',
			Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
			Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
		);
        
		Account a = new Account();
        a.name = 'Test Account';
        a.RecordTypeId = accRecordTypeId;
        insert a;
        accIds.add(a.Id);
		System.debug('created account---'+a.id);

        Contact c = new Contact();
        c.accountId = a.Id;
        c.firstname = 'Test';
        c.lastname = 'Contact';
        c.Phone = '1112223330';
        c.MailingCity = 'Elkridge';		
        c.MailingCountry = 'USA';
        c.MailingPostalCode = '21075';
		c.Talent_Country_Text__c = 'USA';
        c.RecordTypeId = conRecordTypeId;
        insert c;
		System.debug('created contact---'+c.id);

        /*String profileName = 'System Administrator';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'Test LastName';
        testUser.Firstname = 'Test FirstName';
        testUser.Alias = 'test';
        testUser.CommunityNickname = '12345';
        profile prof = [select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
        insert testUser;
		System.debug('created user---'+testUser.id);*/

        //create the custom setting
        
        
		
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
       
        Test.stopTest();

	}
}