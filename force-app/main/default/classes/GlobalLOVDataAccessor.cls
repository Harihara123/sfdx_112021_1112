public with sharing class GlobalLOVDataAccessor {
	
	static final String LOVNAME_COUNTRYLIST = 'CountryList';
	static final String LOVNAME_STATELIST = 'StateList';

	public static Map<String, String> getStateMap(String keyField, String valueField) {
		Map<String, String> stateMap = new Map<String, String>();

		String query = 'SELECT Id, ' + 
						keyField + ', ' + 
						valueField +
						' FROM Global_LOV__c' + 
						' WHERE LOV_Name__c = \'' + LOVNAME_STATELIST + '\''; 

		for(SObject state : Database.query(query)) {
			stateMap.put(String.valueOf(state.get(keyField)), String.valueOf(state.get(valueField)));
		}

		return stateMap;
	}

	public static Map<String, String> getCountryMap(String keyField, String valueField) {
		Map<String, String> countryMap = new Map<String, String>();

		String query = 'SELECT Id, ' + 
						keyField + ', ' +
						valueField + 
						' FROM Global_LOV__c' + 
						' WHERE LOV_Name__c = \'' + LOVNAME_COUNTRYLIST + '\'';

		for(SObject country : Database.query(query)) {
			countryMap.put(String.valueOf(country.get(keyField)), String.valueOf(country.get(valueField)));
		}

		return countryMap;
	}
    /*         
        The method will return Text_Value__c for a given Text_Value_2__c  
    */
    public static Map<String, String> getCountryCode(String countryValue){
        Map<String, String> countryCodes;
        try{
            List<Global_LOV__c> globalLovs = [SELECT Id,Text_Value__c,Text_Value_2__c, Text_Value_3__c, Text_Value_4__c FROM Global_LOV__c 
                                              WHERE LOV_Name__c =: LOVNAME_COUNTRYLIST 
                                              AND (Text_Value_2__c =: countryValue OR Text_Value__c =: countryValue OR Text_Value_3__c =: countryValue OR Text_Value_4__c =: countryValue)];
            if(globalLovs.size()>0){
                countryCodes = new Map<String, String>();
                Global_LOV__c globalLov = globalLovs[0];

                countryCodes.put('countryCode2', globalLov.Text_Value__c);
                countryCodes.put('countryCode3', globalLov.Text_Value_2__c);
                countryCodes.put('countryFullName', globalLov.Text_Value_3__c);

            }
        }catch(Exception ex){}
        
        return countryCodes;
    }
    /*         
        The method will return state full name for the given state and country code
    */
    public static String getStateCode(String countryCode, String stateCode){
        String stateCodes;
        try{
            String countryStateCode = countryCode + '.' + stateCode;
            List<Global_LOV__c> globalLovs = [SELECT Id,Text_Value_3__c FROM Global_LOV__c where LOV_Name__c =: LOVNAME_STATELIST AND Text_Value_2__c  =: countryStateCode];
            if(globalLovs.size()>0){
                Global_LOV__c globalLov = globalLovs[0];
                stateCodes = globalLov.Text_Value_3__c;
            }
        }catch(Exception ex){}
        return stateCodes;
    }
}