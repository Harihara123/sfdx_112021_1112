public class GlobalLOVTriggerHandler {

    public void OnAfterInsert(List<Global_LOV__c> newRecords) {
       callDataExport(newRecords);

    }
    public void OnAfterUpdate (List<Global_LOV__c> newRecords) {        
       callDataExport(newRecords);

    }
    
    public void OnAfterDelete(Map<Id,Global_LOV__c> oldMap){
        callDataExport(oldMap.values());
    }
    private static void callDataExport(List<Global_LOV__c> newRecords) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'GlobalLOVTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' Global_LOV__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Global_LOV__c');
        }

    }

}