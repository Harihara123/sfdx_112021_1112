//Initial Setup Steps
//-------------------
//1) Setup->Security->Remote site settings. endpoints = https://accounts.google.com, https://jobs.googleapis.com
//2) GoogleJobsConnector.PerformInitialOneTimeSetup();  

public with sharing class GoogleCloudProfileConnector {
    
	  private static String CALLOUT_EXCEPTION_RESPONSE =  '{"fault": {"faultstring": "Call to search failed - &&&&!"}}';

	  public static string getResponse(string body,Google_Search_Settings__c apiDetails, string reqId) {
		System.debug('body:'+body);
		System.debug('reqId:'+reqId);
		http h = new Http();
        Httprequest req = new HttpRequest();
        HttpResponse response = new HttpResponse();
		String timeOutvalue = Label.ATS_GOOGLE_SEARCH_TIMEOUT;
        req.setTimeout(Integer.valueOf(timeOutvalue));
        //req.setTimeout(60000);
		string url = apiDetails.Middleware_URL__c+apiDetails.Project_Name__c+'/tenants/'+UserInfo.getOrganizationId();
		if (reqId != null) { // R2C
			req.setEndpoint(url+'/jobs/'+reqId+':recommendProfiles?key='+apiDetails.key__c);
		} else { // C
			req.setEndpoint(url+':search?key='+apiDetails.key__c);
		}
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
		req.setHeader('Content-Length',String.valueOf(body.length()));
        //req.setCompressed(boolean.valueOf(Label.ATS_GOOGLE_COMPRESSION_FLAG));     
        if(body != ''){
            req.setBody(body);
        }
        System.debug('request:'+req);
		String responseBody;
		//response = h.send(req);
		//responseBody = response.getBody();
		// below part commented only for Development. Must be ON before commit.
		 try {
            response = h.send(req);
            if (response.getStatusCode() == 200) {
                responseBody = response.getBody();
            } else {
				System.debug('actual Error responseBody >>>>>>>> ' + responseBody);
                responseBody = CALLOUT_EXCEPTION_RESPONSE.replace('&&&&', response.getStatus());
            }
            System.debug('responseBody >>>>>>>> ' + responseBody);
        } catch (CalloutException ex) {
            System.debug(LoggingLevel.ERROR, 'Google Search Service failed! ' + ex.getMessage());
            responseBody = CALLOUT_EXCEPTION_RESPONSE.replace('&&&&', ex.getMessage());
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Google Search Service failed! ' + ex.getMessage());
            throw ex;
        }
		
		System.debug('final response:'+ responseBody);
		return responseBody; 
	}

	public static string createEvent(string eventBody, Google_Search_Settings__c apiDetails, string resultSetId) {
		System.debug('body:'+eventBody);
		http h = new Http();
        Httprequest req = new HttpRequest();
        HttpResponse response = new HttpResponse();
		String timeOutvalue = Label.ATS_GOOGLE_SEARCH_TIMEOUT;
        req.setTimeout(Integer.valueOf(timeOutvalue));
        
		string url = apiDetails.Middleware_URL__c+apiDetails.Project_Name__c+'/tenants/'+UserInfo.getOrganizationId() + '/clientEvents';
		req.setEndpoint(url+'?key='+apiDetails.key__c+'&resultSetId='+resultSetId);
		//req.setCompressed(boolean.valueOf(Label.ATS_GOOGLE_COMPRESSION_FLAG));  
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
		req.setHeader('Content-Length',String.valueOf(eventBody.length()));
            
        if(eventBody != ''){
            req.setBody(eventBody);
        }
        System.debug('request:'+req);
		String responseBody;
		response = h.send(req);
		responseBody = response.getBody();
		
		System.debug('final response:'+ responseBody);
		return responseBody; 
	}

	//Added w.r.t S-146093 ApexContinuation
	@AuraEnabled
	public static Object getResponseviaContinuation(string body,Google_Search_Settings__c apiDetails, string reqId,Continuation con) {
		System.debug('*****ApexContinuation>>>>body:*****'+body);
		System.debug('*****ApexContinuation>>>>reqId:*****'+reqId);
		http h = new Http();
        Httprequest req = new HttpRequest();        
		string url = apiDetails.Middleware_URL__c+apiDetails.Project_Name__c+'/tenants/'+UserInfo.getOrganizationId();
		if (reqId != null) { // R2C
			req.setEndpoint(url+'/jobs/'+reqId+':recommendProfiles?key='+apiDetails.key__c);
		} else { // C
			req.setEndpoint(url+':search?key='+apiDetails.key__c);
		}
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
		req.setHeader('Content-Length',String.valueOf(body.length())); 
		
		if(body != ''){
            req.setBody(body);			     
        }
		System.debug('*****ApexContinuation>>>>request:*****'+req);
        con.state = con.addHttpRequest(req);
		System.debug('*****ApexContinuation>>>>Continuation:*****'+con);			
		return con;	
	}

	public static string getProfile(Google_Search_Settings__c apiDetails, string contactId) {
		http h = new Http();
        Httprequest req = new HttpRequest();
        HttpResponse response = new HttpResponse();
		String timeOutvalue = Label.ATS_GOOGLE_SEARCH_TIMEOUT;
        req.setTimeout(Integer.valueOf(timeOutvalue));
        
		string url = apiDetails.Middleware_URL__c+apiDetails.Project_Name__c+'/tenants/'+UserInfo.getOrganizationId() + '/profiles/'+contactId;
		req.setEndpoint(url+'?key='+apiDetails.key__c);
		//req.setCompressed(boolean.valueOf(Label.ATS_GOOGLE_COMPRESSION_FLAG));  
        req.setMethod('GET');
        req.setHeader('Content-Type','application/json');
		//req.setHeader('Content-Length','1');            
        System.debug('request:'+req);
		String responseBody;
		response = h.send(req);
		responseBody = response.getBody();
		
		System.debug('final response:'+ responseBody);
		return responseBody; 
	}

	/* public class AuthResponse{
        public String access_token {get;set;}
        public String expires_in {get;set;}
        public String token_type {get;set;}
    }
   
    public static string Authenticate(){
        List<Google_Cloud_Accounts__mdt> jobApiCredential = [select label,UserName__c,PrivateKey__c from Google_Cloud_Accounts__mdt 
                                                             where label='Search Profiles'];
        if (jobApiCredential == null || jobApiCredential.size() == 0) {
            return '';
        }
        String clientId = jobApiCredential[0].UserName__c;
        String key = jobApiCredential[0].PrivateKey__c;
        String Header = '{"alg":"RS256","typ":"JWT"}';
        String Header_Encode = EncodingUtil.base64Encode(blob.valueof(Header));
       
        String claim_set = '{"iss":"' + clientId + '"';
        claim_set += ',"scope":"https://www.googleapis.com/auth/jobs"'; //https://www.googleapis.com/auth/cloudprofile
        claim_set += ',"aud":"https://accounts.google.com/o/oauth2/token"'; 
        claim_set += ',"exp":"'+datetime.now().addHours(1).getTime()/1000;
        claim_set += '","iat":"'+datetime.now().getTime()/1000+'"}';
       
        String claim_set_Encode = EncodingUtil.base64Encode(blob.valueof(claim_set));
        String Singature_Encode = Header_Encode+'.'+claim_set_Encode;        
               
        blob privateKey = EncodingUtil.base64Decode(key);
        Singature_Encode = Singature_Encode.replaceAll('=','');
        String Singature_Encode_Url = EncodingUtil.urlEncode(Singature_Encode,'UTF-8');
        blob Signature_b =   blob.valueof(Singature_Encode_Url);
       
        String Signatute_blob = base64(Crypto.sign('RSA-SHA256', Signature_b , privateKey));
              
        String JWT = Singature_Encode+'.'+Signatute_blob;
        JWT = JWT.replaceAll('=','');
        String grt = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
        
        String response = PerformHTTPSend('POST', 'https://accounts.google.com/o/oauth2/token', 'grant_type='+grt+'&assertion='+JWT,'application/x-www-form-urlencoded', null );
        if(response != null){
            AuthResponse a = (AuthResponse)JSON.deserialize(response, AuthResponse.class);			
            return a.access_token;
        }else{
            return '';
        }
       
    }

	public static string sampleRequestJSON() {
		string s = '{ "requestMetadata":  { "userId": "UNKNOWN", "sessionId": "UNKNOWN","userAgent": "UNKNOWN","domain": "UNKNOWN"}, "profileQuery": { "query" : "Java"} }';
		return s;
	}   
     
    public static String base64(Blob b) {
        String ret = EncodingUtil.base64Encode(b);
        ret = ret.replaceAll('\\+', '-');
        ret = ret.replaceAll('/', '_');
        ret = ret.replaceAll('=', '');
        return ret;
    }
        
    public static String PerformHTTPSend(String method, String endpoint, String body, String contentType, String accessToken){  
        if(Test.isRunningTest()){
            return null;
        }else{
            http h = new Http();
            Httprequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            req.setTimeout(60000); // timeout in milliseconds
            req.setEndpoint(endpoint);
            req.setMethod(method);
            req.setHeader('Content-Type',contentType);
			req.setHeader('Content-Length',String.valueOf(body.length()));
            
            if(accessToken != null){
                req.setHeader('Authorization', 'Bearer ' + accessToken);
            }
            if(body != ''){
                req.setBody(body);
            }
            res = h.send(req);
                
            return res.getBody();
        }
    }
    
		/*  string token = Authenticate();
		System.debug('inside1:'+ inputJSON +  ' ' + token);
		// old request : https://cloudprofile.googleapis.com/v2alpha1/tenants/2d3e98fd-a73f-41c4-b785-0ee532d123c8:search

		String response = PerformHTTPSend('POST', 'https://jobs.googleapis.com/v4beta1/projects/connected-ingest-dev/tenants/34dc8cbc-91aa-494d-b35c-1a92df20e68f:search', inputJSON, 'application/json', token);
		System.debug('response:'+response);   //  34dc8cbc-91aa-494d-b35c-1a92df20e68f
		return response; //f0ca94a1-ff39-48b0-b330-801fb829dce5   e0b8fb19-964a-48eb-b42a-8e115ee570d4
		*/

	/* public static string searchTalent(string inputJSON,Google_Search_Settings__c apiDetails) {
	
		string response = getResponse(string inputJSON,Google_Search_Settings__c apiDetails, null);
		 http h = new Http(); 
        Httprequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setTimeout(60000); // timeout in milliseconds
		req.setEndpoint('https://search-middleware-dot-connected-ingest-dev.appspot.com/v4beta1/projects/connected-ingest-dev/tenants/'+UserInfo.getOrganizationId()+':search?key=AIzaSyBNqzkU2FOCgRXdtfgRc--VVRNDPNduWbo');

        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
		req.setHeader('Content-Length',String.valueOf(inputJSON.length()));
            
        if(inputJSON != ''){
            req.setBody(inputJSON);
        }
        res = h.send(req);	
	System.debug('response:'+ res.getBody());
	return res.getBody(); //f0ca94a1-ff39-48b0-b330-801fb829dce5  
	
	}     

	public static string searchReqToTalent(string body, string reqId,Google_Search_Settings__c apiDetails) {
			System.debug('json:'+ body);
			http h = new Http();
            Httprequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            req.setTimeout(60000); // timeout in milliseconds ///0061l00000Gx41CAAR
			req.setEndpoint('https://search-middleware-dot-connected-ingest-dev.appspot.com/v4beta1/projects/connected-ingest-dev/tenants/'+UserInfo.getOrganizationId()+'/jobs/'+reqId+':recommendProfiles?key=AIzaSyBNqzkU2FOCgRXdtfgRc--VVRNDPNduWbo');
            req.setMethod('POST');
            req.setHeader('Content-Type','application/json');
			req.setHeader('Content-Length',String.valueOf(body.length()));
            
            if(body != ''){
                req.setBody(body);
            }
            res = h.send(req);	
		System.debug('response:'+ res.getBody());
		return res.getBody(); //f0ca94a1-ff39-48b0-b330-801fb829dce5  
	}  */
	/*	string token = Authenticate();
		System.debug('inside1:'+ body +  ' ' + token);

		String response = PerformHTTPSend('POST', 'https://jobs.googleapis.com/v4beta1/projects/connected-ingest-dev/tenants/34dc8cbc-91aa-494d-b35c-1a92df20e68f/jobs/115104146513633990:recommendProfiles', body, 'application/json', token);
		System.debug('response:'+response); 
		return response; */      
}