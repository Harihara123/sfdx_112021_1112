global class GoogleJobsBatchable implements Schedulable,Database.AllowsCallouts {
	global String method{get; set;} 
    global String nextPageToken{get; set;}
	global List<Opportunity> opps{get; set;}
    global List<String> jobIdList{get; set;}
    global String operationType{get; set;} 
    global String companyId{get; set;} 
    global Integer iteration{get; set;} 

	global GoogleJobsBatchable(List<Opportunity> opprList, String oprType){
		this.opps = opprList;
		this.method = 'LoadJobs';
        this.operationType = oprType;
	}

	global GoogleJobsBatchable(Boolean cleanUp){
		if(cleanUp){
			this.method = 'PerformJobCleanup';
		}
	}
    
    global GoogleJobsBatchable(String oprType){	
        this.operationType = oprType;
	}
    
    global GoogleJobsBatchable(String compId, List<String> jobIds, String nextPgToken, Integer itr){	
        this.operationType = 'DELETE';
        this.nextPageToken = nextPgToken;
        this.companyId = compId;
        this.jobIdList = jobIds;
        this.iteration = itr;        
	}

	global void execute(SchedulableContext sc) {
        
		if((this.method !=null) && (this.method == 'LoadJobs')){
			Set<Id> ids = (new Map<Id, Opportunity>(this.opps)).keySet();
			GoogleJobsConnector.PerformJobUpdates(ids, false, operationType);
        } else if (this.operationType != null && this.operationType.equalsIgnoreCase('UPDATE')) {
            System.debug('Execute Job update');
            Integer iteration = GoogleJobsConnector.BulkLoadJobs('UPDATE', '');
            GoogleJobsConnector.scheduleJobCleanUp(iteration);
        } else if (this.operationType != null && this.operationType.equalsIgnoreCase('DELETE')) { 
            GoogleJobsConnector.cleanUpJobs(jobIdList, companyId, nextPageToken, iteration);
        } else{
			GoogleJobsConnector.JobCleanup();
			
		}
	}
}