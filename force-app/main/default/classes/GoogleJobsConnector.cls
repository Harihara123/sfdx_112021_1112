//Initial Setup Steps
//-------------------
//1) Setup->Security->Remote site settings. endpoints = https://accounts.google.com, https://jobs.googleapis.com
//2) GoogleJobsConnector.PerformInitialOneTimeSetup();  

public with sharing class GoogleJobsConnector {
    
    public static DateTime emeaCutOffDate = DateTime.newInstance(2018, 6, 1);
    public class JobSearchResponse {
        public String estimatedTotalSize {get;set;}
        public List<GoogleJobsConnector.MatchingJob> matchingJobs {get;set;}
        public String nextPageToken {get;set;}
        public GoogleJobsConnector.SpellingCorrection spellResult {get;set;}
        public GoogleJobsConnector.JobView jobView {get;set;}
        public List<GoogleJobsConnector.JobLocation> appliedJobLocationFilters {get;set;}
        public String totalSize {get;set;}
        public GoogleJobsConnector.ResponseMetadata metadata {get;set;}
        public Integer numJobsFromBroadenedQuery {get;set;}
    }
    
    public class ResponseMetadata {
        public String requestId {get;set;}
        public List<Decimal> experimentIdList {get;set;}
        public GoogleJobsConnector.Mode mode {get;set;}
    }
    
    public class SpellingCorrection {
        public Boolean corrected {get;set;}
        public String correctedText {get;set;}
    }
    
    public class MatchingJob {
        public GoogleJobsConnector.Job job {get;set;}
        public GoogleJobsConnector.CommuteInfo commuteInfo {get;set;}
        public String jobTitleSnippet {get;set;}
        public String jobSummary {get;set;}
        public String searchTextSnippet {get;set;}
    }
    
    public class JobSearchRequest{
        public GoogleJobsConnector.RequestMetadata requestMetadata {get;set;}
        public JobFilters filters {get;set;}
        public Integer pageSize {get;set;}
        public String pageToken {get;set;}
        public GoogleJobsConnector.JobView jobView {get;set;}
        public GoogleJobsConnector.SortBy orderBy {get;set;}
        public Boolean enableBroadening {get;set;}
        public Integer offset {get;set;}
        public Boolean enablePreciseResultSize {get;set;}
        public Boolean disableRelevanceThresholding {get;set;}
        public GoogleJobsConnector.Mode mode {get;set;}
    }
    
    public enum Mode {
        MODE_UNSPECIFIED
        ,JOB_SEARCH
        ,FEATURED_JOB_SEARCH
        ,EMAIL_ALERT_SEARCH
    }
    
    public enum SortBy {
        SORT_BY_UNSPECIFIED
        ,RELEVANCE_DESC
        ,PUBLISHED_DATE_DESC
        ,UPDATED_DATE_DESC
        ,TITLE
        ,TITLE_DESC
    }
    
    public enum JobView {
        JOB_VIEW_UNSPECIFIED
        ,SMALL
        ,MINIMAL
        ,FULL
    }
    
    public class RequestMetadata {
        public String userId {get;set;}
        public String sessionId {get;set;}
        public GoogleJobsConnector.DeviceInfo deviceInfo {get;set;}
        public String domain {get;set;}
    }
    
    public class DeviceInfo {
        public String id {get;set;}
        public GoogleJobsConnector.DeviceType deviceType {get;set;}
    }
    
    public enum DeviceType {
        DEVICE_TYPE_UNSPECIFIED
        ,WEB
        ,MOBILE_WEB
        ,ANDROID
        ,IOS
        ,OTHER
    }
    
    public class JobFilters{
        public List<LocationFilter> locationFilters {get;set;}
        public List<String> categories {get;set;} // enum(Category)
        public CommutePreference commuteFilter {get;set;}
        public List<GoogleJobsConnector.EmploymentType> employmentTypes {get;set;}
        public String query {get;set;}
        public List<String> companyNames {get;set;} //These will be the names of the TEK / Aerotek companies in Google
        public GoogleJobsConnector.DateRange publishDateRange {get;set;}
        public Boolean disableSpellCheck {get;set;}
        public Map<Integer, Object> customFieldFilters {get;set;}
        public Boolean tenantJobOnly {get;set;}
        public List<String> languageCodes {get;set;}
        public List<String> companyTitles {get;set;}
    }
    
    public class LocationFilter{
        public String name {get;set;}
        public Decimal distanceInMiles {get;set;}
        public GoogleJobsConnector.LatLng latLng {get;set;}
        public Boolean isTelecommute {get;set;} 
    }
    
    public class CommutePreference{     
        public String method {get;set;}     
        public String travelTime {get;set;}     
        public String roadTraffic {get;set;}        
        public Boolean allowNonStreetLevelAddress{get;set;}     
        public Decimal departureHourLocal{get;set;}     
        public GoogleJobsConnector.LatLng startLocation{get;set;}      
                
    }
    
    public enum DateRange{
        DATE_RANGE_UNSPECIFIED
        ,PAST_24_HOURS
        ,PAST_WEEK
        ,PAST_MONTH
        ,PAST_YEAR
        ,PAST_3_DAYS
    }
    
    public enum EmploymentType {
        EMPLOYMENT_TYPE_UNSPECIFIED
        ,FULL_TIME
        ,PART_TIME
        ,CONTRACTOR
        ,TEMPORARY
        ,INTERN
        ,VOLUNTEER
        ,PER_DIEM
        ,CONTRACT_TO_HIRE
        ,FRANCHISES
        ,OTHER
    }
    
    public enum EducationLevel{
        EDUCATION_LEVEL_UNSPECIFIED
        ,HIGH_SCHOOL
        ,ASSOCIATE
        ,BACHELORS
        ,MASTERS
        ,DOCTORATE
        ,NO_DEGREE_REQUIRED
    }
    
    public enum JobLevel{
        JOB_LEVEL_UNSPECIFIED
        ,ENTRY_LEVEL
        ,EXPERIENCED
        ,MANAGER
        ,DIRECTOR
        ,EXECUTIVE
    }
    
    public enum JobCompensationType{
        JOB_COMPENSATION_TYPE_UNSPECIFIED
        ,HOURLY
        ,SALARY
        ,PER_PROJECT
        ,COMMISSION
        ,OTHER_TYPE
    }
    
    public enum JobBenefitType{
        JOB_BENEFIT_TYPE_UNSPECIFIED
        ,CHILD_CARE
        ,DENTAL
        ,DOMESTIC_PARTNER
        ,FLEXIBLE_HOURS
        ,MEDICAL
        ,LIFE_INSURANCE
        ,PARENTAL_LEAVE
        ,RETIREMENT_PLAN
        ,SICK_DAYS
        ,TELECOMMUTE
        ,VACATION
        ,VISION
    }
    
    public enum Region{
        REGION_UNSPECIFIED
        ,STATE_WIDE
        ,NATION_WIDE
        ,TELECOMMUTE
    }
    
    public class GJDate {
        public Integer year {get;set;}
        public Integer month {get;set;}
        public Integer day {get;set;}   
        
        public GJDate(Date input){
            year = input.year();
            month = input.month();
            day = input.day();
        }
        
        public date getDate(){
            return date.newinstance(this.year, this.month, this.day);
        }
    }
    
    public class GJTime {
        public Long seconds{get;set;}
        public Integer nanos{get;set;}
        public GJTime(DateTime dt) {
            seconds = dt.getTime();
            nanos = 0;
        }
    }
    
    public enum LocationType {
        LOCATION_TYPE_UNSPECIFIED
        ,COUNTRY
        ,ADMINISTRATIVE_AREA
        ,SUB_ADMINISTRATIVE_AREA
        ,LOCALITY
        ,POSTAL_CODE
        ,SUB_LOCALITY
        ,SUB_LOCALITY_1
        ,SUB_LOCALITY_2
        ,NEIGHBORHOOD
        ,STREET_ADDRESS
    }
    
    public class PostalAddress{
        public Decimal revision {get;set;}
        public String regionCode {get;set;}
        public String languageCode {get;set;}
        public String postalCode {get;set;}
        public String sortingCode {get;set;}
        public String administrativeArea {get;set;}
        public String locality {get;set;}
        public String sublocality {get;set;}
        public List<String> addressLines {get;set;}
        public List<String> recipients {get;set;}
        public String organization {get;set;}
    }
    
    
    public class LatLng{
        public Decimal latitude {get;set;}
        public Decimal longitude {get;set;}
        
        public LatLng(Decimal lat, Decimal lng){
            this.latitude = lat;
            this.longitude = lng;
        }
    }
    
    public class JobLocation{
        public LocationType locationType {get;set;}
        public PostalAddress postalAddress {get;set;}
        public LatLng latLng {get;set;}
        public Decimal radiusMeters {get;set;}
    }
    
    public class Money{
        public String currencyCode {get;set;}
        public String units {get;set;}
        public Decimal nanos {get;set;}
        
        public Money(Decimal amount, String curr){
            currencyCode = curr;
            units = String.valueOf(amount.intValue());
            nanos = 0;
            
            Decimal rem = 0;
            if(amount > 0){
                rem = amount - (amount.intValue());
            }else{
                rem = amount + (amount.intValue());
            }
            
            nanos = rem * 1000000000;
        }
    }
    
    public class CompensationInfo{
        public GoogleJobsConnector.JobCompensationType type {get;set;}
        public GoogleJobsConnector.Money amount {get;set;}
        public GoogleJobsConnector.Money min {get;set;}
        public GoogleJobsConnector.Money max {get;set;}
    }
    
    public class Company{
        public String name {get;set;}
        public String title {get;set;}
        public String distributorCompanyId {get;set;}
    }
    
    public class CommuteInfo{
        public String travelDuration {get;set;}
        
    }
    
    public class Job {
        public String name {get;set;}
        public String requisitionId {get;set;}
        public String jobTitle {get;set;}
        public List<GoogleJobsConnector.EmploymentType> employmentTypes {get;set;}
        public String description {get;set;}
        public List<String> applicationUrls {get;set;}
        public List<String> applicationEmailList {get;set;}
        public String applicationInstruction {get;set;}
        public String responsibilities {get;set;}
        public String qualifications {get;set;}
        public GoogleJobsConnector.EducationLevel educationLevels {get;set;}
        public GoogleJobsConnector.JobLevel level {get;set;} 
        public String department {get;set;}
        public GoogleJobsConnector.GJDate startDate {get;set;}
        public GoogleJobsConnector.GJDate endDate {get;set;}
        public GoogleJobsConnector.CompensationInfo compensationInfo {get;set;}
        public String incentives {get;set;}
        public String languageCode {get;set;}
        public GoogleJobsConnector.JobBenefitType benefits {get;set;} 
        public GoogleJobsConnector.GJDate expiryDate {get;set;}
        public GoogleJobsConnector.GJTime expiryTime {get;set;}
        public String distributorCompanyId {get;set;}
        public String referenceUrl {get;set;}
        public String companyTitle {get;set;}
        public String companyName {get;set;}
        public Map<Integer, CustomField> filterableCustomFields {get;set;}
        public Map<String, CustomField> unindexedCustomFields {get;set;}
        public String visibility {get;set;} 
        public GoogleJobsConnector.GJDate publishDate {get;set;}
        public String createTime {get;set;}
        public String updateTime {get;set;}
        public Decimal promotionValue {get;set;}
        public List<String> locations {get;set;}
        public List<GoogleJobsConnector.JobLocation> jobLocations {get;set;}
        public GoogleJobsConnector.Region region {get;set;} 
    }

    public class CustomField{
            public List<String> values {get;set;}
            
            public CustomField(List<String> vals){
                values = vals;
            }
    }
    
    public class CustomFilterField{
            public List<String> queries {get;set;}
            public String type {get;set;}
            
            public CustomFilterField(List<String> vals, String searchType){
                this.queries = vals;
                this.type = searchType;
            }
    }
    
    public class CompanyResult{
            public List<GoogleJobsConnector.Company> companies {get;set;}
            public String nextPageToken;
    }

    public class JobRequest{
        public JobRequest(GoogleJobsConnector.Job inputJob){
            this.job = inputJob;
            this.disableStreetAddressResolution = false;
        }

        public GoogleJobsConnector.Job job {get;set;}
        public Boolean disableStreetAddressResolution {get;set;} 
    }    
   
    public class AuthResponse{
        public String access_token {get;set;}
        public String expires_in {get;set;}
        public String token_type {get;set;}
    }
    
    public class CompanyJobList{
        public List<GoogleJobsConnector.Job> jobs {get;set;}
        public String totalSize {get;set;}
        public GoogleJobsConnector.ResponseMetadata metadata {get;set;}
        public String nextPageToken {get;set;}
    }
   
    public class DeleteRequest{
        public DeleteRequest(String reqID){
            this.filter = new DeleteFilter();
            this.filter.requisitionId = reqID;
            this.disableFastProcess = false;
        }
        public DeleteFilter filter {get;set;}
        public boolean disableFastProcess {get;set;}
    }

    public class DeleteFilter{
        public string requisitionId {get;set;}
    }
    
    public static void PutCompanies(){
            String accesstoken = Authenticate();    
            System.debug(accesstoken);
            List<Product_OPCO_List__c> oList = [SELECT Name FROM Product_OPCO_List__c];
            
            for(Product_OPCO_List__c opCo : oList){
                GoogleJobsConnector.Company c = new GoogleJobsConnector.Company();
                c.title = opCo.Name;
                c.distributorCompanyId = opCo.Name;
                
                String body = JSON.Serialize(c);
                String response = PerformHTTPSend('POST', 'https://jobs.googleapis.com/v2/companies', body, 'application/json', accesstoken );
            }
    }

    public static void PerformInitialOneTimeSetup(String opco){
        PutCompanies();
        Integer iteration = BulkLoadJobs('INSERT', opco);
        scheduleJobCleanUp(iteration);
    }
    
    public static void scheduleJobCleanUp(Integer iteration) {
        DateTime now = DateTime.now().addMinutes(iteration * 3);
        Integer min = now.minute();
        Integer hour = now.hour();
        Integer day = now.day();
        String month = now.format('MMM').toUpperCase();
        Integer year = now.year();

        String sch = '0 ' + String.valueOf(min) + ' ' + String.valueOf(hour) + ' ' + String.valueOf(day)  +' '+ month + ' ? ' + String.valueOf(year);
        GoogleJobsBatchable s = new GoogleJobsBatchable(true);
        System.schedule('Google Job Clean Up',sch, s);

        System.debug('Google Jobs Inital Load Scheduled');
    }

    public static Integer BulkLoadJobs(String operationType, String opco){
        String maxID = '';
        Boolean cont = true;
        Integer iteration = 1;
        while(cont){
            maxID = loadJobs(maxID, iteration, operationType, opco);
            iteration = iteration + 1;

            if(Test.isRunningTest()){
                maxID = '';
            }

            if(maxID == ''){
                cont = false;
            }
        }
        return iteration;
    }
   
    public static String loadJobs(String maxID, Integer iteration, String operationType, String opco){
        String newMaxID = '';
        String dbQuery = 'SELECT Id ';
        dbQuery += ' FROM Opportunity '; 
        dbQuery += ' WHERE recordtype.developerName = \'Req\' AND Req_Qualified_Date__c != null AND Opco__c != null ';
        
        if (operationType.equalsIgnoreCase('UPDATE')) {            
            dbQuery += ' AND LastModifiedDate = yesterday ';            
            // AND LastModifiedby.Name !=\'Account Integration\'           
        } else if (operationType.equalsIgnoreCase('INSERT')) {
            dbQuery += ' AND isClosed = false AND Req_OFCCP_Required__c = false AND StageName in(\'Qualified\',\'Presenting\',\'Interviewing\',\'Pending Start\') ';
            if (!String.isBlank(opco) && opco.equals('AG_EMEA')) {
                dbQuery += ' AND (Organization_Office__r.office_code__c = \'00993\' OR Organization_Office__r.office_code__c = \'01262\' OR Organization_Office__r.office_code__c = \'01271\' OR Organization_Office__r.office_code__c = \'01078\') AND Req_Qualified_Date__c >= N_DAYS_AGO:90';
            }
            if (!String.isBlank(opco)) {
                dbQuery += ' AND OpCo__c = \''+ opco +'\' ';
            }            
        }
        
        if(maxID != ''){
            dbQuery += ' AND ID < \'' + maxID + '\'';
        }
        
        /*
         * OPCOs:
         * Aerotek, Inc
         * TEKsystems, Inc.
         * AG_EMEA 
         * 
         * Full SOQL Query:
         * NA UPDATE: SELECT Id FROM Opportunity WHERE recordtype.developerName = 'Req' AND Req_Qualified_Date__c != null AND Opco__c != null AND LastModifiedDate = yesterday AND OpCo__c = 'Aerotek, Inc'
         * NA INSERT: SELECT Id FROM Opportunity WHERE recordtype.developerName = 'Req' AND Req_Qualified_Date__c != null AND Opco__c != null AND isClosed = false AND Req_OFCCP_Required__c = false AND StageName in('Qualified','Presenting','Interviewing','Pending Start') AND OpCo__c = 'Aerotek, Inc'
         * 
         * EMEA: SELECT Id FROM Opportunity WHERE recordtype.developerName = 'Req' AND Req_Qualified_Date__c != null AND Opco__c != null AND isClosed = false AND Req_OFCCP_Required__c = false AND StageName in('Qualified','Presenting','Interviewing','Pending Start') AND (Organization_Office__r.office_code__c = \'00993\' OR Organization_Office__r.office_code__c = \'01262\' OR Organization_Office__r.office_code__c = \'01271\' OR Organization_Office__r.office_code__c = \'01078\') AND Req_Qualified_Date__c >= N_DAYS_AGO:90
         */
        
        dbQuery += ' order by ID DESC LIMIT 200 ';
        System.debug(dbQuery);
        System.debug(iteration); 
        List<Opportunity> opps = Database.query(dbQuery);
        System.debug(opps.size());
        DateTime now = DateTime.now().addMinutes(1 + (iteration * 3));      
        Integer min = now.minute();
        Integer hour = now.hour();
        Integer day = now.day();
        String month = now.format('MMM').toUpperCase();
        Integer year = now.year();
        
        if(opps != null){ 
       // if(opps != null && iteration > 29){
            String sch = '0 ' + String.valueOf(min) + ' ' + String.valueOf(hour) + ' ' + String.valueOf(day)  +' '+ month + ' ? ' + String.valueOf(year);
            
    
            if(Test.isRunningTest()){
                GoogleJobsBatchable s = new GoogleJobsBatchable(new List<Opportunity>(), operationType);
                System.schedule('Google Job Upload - ' + String.valueOf(iteration),sch, s);
            }else{                
                GoogleJobsBatchable s = new GoogleJobsBatchable(opps, operationType);
                System.schedule('Google Job Upload - ' + String.valueOf(iteration),sch, s);
            }
            //}
            for(Opportunity o : opps){
                newMaxID = o.Id; 
            }
            
        }
        opps.clear();   
        
        return newMaxID;            
    }
   
    public static string Authenticate(){
        List<Google_Cloud_Accounts__mdt> jobApiCredential = [select label,UserName__c,PrivateKey__c from Google_Cloud_Accounts__mdt 
                                                             where label='Job Discovery'];
        if (jobApiCredential == null || jobApiCredential.size() == 0) {
            return '';
        }
        String clientId = jobApiCredential[0].UserName__c;
        String key = jobApiCredential[0].PrivateKey__c;
        String Header = '{"alg":"RS256","typ":"JWT"}';
        String Header_Encode = EncodingUtil.base64Encode(blob.valueof(Header));
       
        String claim_set = '{"iss":"' + clientId + '"';
        claim_set += ',"scope":"https://www.googleapis.com/auth/jobs"';
        claim_set += ',"aud":"https://accounts.google.com/o/oauth2/token"';
        claim_set += ',"exp":"'+datetime.now().addHours(1).getTime()/1000;
        claim_set += '","iat":"'+datetime.now().getTime()/1000+'"}';
       
        String claim_set_Encode = EncodingUtil.base64Encode(blob.valueof(claim_set));
        String Singature_Encode = Header_Encode+'.'+claim_set_Encode;        
               
        blob privateKey = EncodingUtil.base64Decode(key);
        Singature_Encode = Singature_Encode.replaceAll('=','');
        String Singature_Encode_Url = EncodingUtil.urlEncode(Singature_Encode,'UTF-8');
        blob Signature_b =   blob.valueof(Singature_Encode_Url);
       
        String Signatute_blob = base64(Crypto.sign('RSA-SHA256', Signature_b , privateKey));
              
        String JWT = Singature_Encode+'.'+Signatute_blob;
        JWT = JWT.replaceAll('=','');
        String grt = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
        
        String response = PerformHTTPSend('POST', 'https://accounts.google.com/o/oauth2/token', 'grant_type='+grt+'&assertion='+JWT,'application/x-www-form-urlencoded', null );
        if(response != null){
            AuthResponse a = (AuthResponse)JSON.deserialize(response, AuthResponse.class);
            return a.access_token;
        }else{
            return '';
        }
       
    }

    @future(callout=true)
    public static void PerformJobUpdates(Set<Id> ids, Boolean deleteOnly, String operationType){
        List<Opportunity> oppList;
        
        if(operationType.equalsIgnoreCase('INSERT')) {
            oppList = [SELECT Id, Name, Req_Job_Title__c,Req_Worksite_City__c ,Req_Worksite_State__c ,Req_Worksite_Country__c ,
                    Req_Search_Tags_Keywords__c,Req_Skill_Details__c,Req_Qualified_Date__c, Req_Product__c,EnterpriseReqSkills__c,
                    Req_Red_Zone__c,OpCo__c,Req_External_Job_Description__c,status__c, StageName, Req_OFCCP_Required__c, CreatedDate,
                    Organization_Office__r.office_code__c, IsClosed, Req_Worksite_Street__c, Req_Worksite_Postal_Code__c FROM Opportunity WHERE Id IN :ids ORDER BY Req_Qualified_Date__c];
        } else if (operationType.equalsIgnoreCase('UPDATE')){
            oppList = [SELECT Id, Name, Req_Job_Title__c,Req_Worksite_City__c ,Req_Worksite_State__c ,Req_Worksite_Country__c ,
                    Req_Search_Tags_Keywords__c,Req_Skill_Details__c,Req_Qualified_Date__c, Req_Product__c,EnterpriseReqSkills__c,
                    Req_Red_Zone__c,OpCo__c,Req_External_Job_Description__c,status__c, StageName, Req_OFCCP_Required__c, CreatedDate, 
                    Organization_Office__r.office_code__c, IsClosed, Req_Worksite_Street__c, Req_Worksite_Postal_Code__c FROM Opportunity WHERE Id IN :ids ORDER BY Req_Qualified_Date__c];
        }
        
        if(oppList != null){
            if(oppList.size() > 0){
                String accessToken = Authenticate();
                List<String> deleteBatch = new List<String>();
                for(Id jobID : ids){
                    String dr  = JSON.serialize( new DeleteRequest(String.valueOf(jobID)));
                    deleteBatch.add(dr);
                }
                GoogleJobsConnector.PerformBatchSend(deleteBatch, accessToken, ':deleteByFilter');
                
                if(deleteOnly != true){
                    List<String> jobBatch = new List<String>();
                    String companies = JSON.serialize(GetCompanies(accessToken));

                    for(Opportunity o : oppList){
                        if(o != null){
                            String theReq = GoogleJobsConnector.ConvertReqToJob(o, companies);
                            if (!String.isBlank(theReq)) {
                                jobBatch.add(theReq);
                            }                           
                        }
                    }
                    GoogleJobsConnector.PerformBatchSend(jobBatch, accessToken, '');
                }
            
            }
        }                           
    }
    
    @future(callout=true)
    public static void JobCleanup(){
        for(CronTrigger cron : [SELECT Id FROM CronTrigger WHERE State = 'DELETED' and CronJobDetail.Name like 'Google Job%']) 
        {
            System.abortJob(cron.id);
        }
    }
     
    public static String base64(Blob b) {
        String ret = EncodingUtil.base64Encode(b);
        ret = ret.replaceAll('\\+', '-');
        ret = ret.replaceAll('/', '_');
        ret = ret.replaceAll('=', '');
        return ret;
    }
        
    public static String PerformHTTPSend(String method, String endpoint, String body, String contentType, String accessToken){  
        if(Test.isRunningTest()){
            return null;
        }else{
            http h = new Http();
            Httprequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            req.setTimeout(60000); // timeout in milliseconds
            req.setEndpoint(endpoint);
            req.setMethod(method);
            req.setHeader('Content-Type',contentType);
            
            if(accessToken != null){
                req.setHeader('Authorization', 'Bearer ' + accessToken);
            }
            if(body != ''){
                req.setBody(body);
            }
            res = h.send(req);
                
            return res.getBody();
        }
    }
    
    public static void PerformBatchSend(List<String> jobBatch, String accessToken, String postMethod){
        String body = '';
        
        for(String request : jobBatch){
            body += '--batch_job \n';
            body += 'Content-Type: application/http \n\n';
            body += 'POST /v2/jobs' + postMethod + ' \n';
            body += 'Content-Type: application/json \n\n';
            body += request;
            body += '\n';
        }   
        
        body += '--batch_job--';
        //system.debug(body);
        PerformHTTPSend('POST', 'https://jobs.googleapis.com/batch', body, 'multipart/mixed; boundary=batch_job', accesstoken );
        jobBatch = null;
    
    }
    
    public static string getEmploymentType(Job job){
        if(job.employmentTypes[0] == GoogleJobsConnector.EmploymentType.CONTRACT_TO_HIRE){
                return 'Contract to Hire';
        }else if(job.employmentTypes[0] == GoogleJobsConnector.EmploymentType.FULL_TIME){
            if (job.distributorCompanyId == 'AG_EMEA') {
                return 'Permanent';
            } else {
                return 'Direct Placement';
            }
        }else if(job.employmentTypes[0] == GoogleJobsConnector.EmploymentType.TEMPORARY){
            return 'Contract';
        }else{
            return 'Unknown';
        }
    }
    
    public static List<GoogleJobsConnector.Company> GetCompanies(String accessToken){
        if(accessToken == ''){
            accessToken = Authenticate();   
        }
        List<GoogleJobsConnector.Company> result = new List<GoogleJobsConnector.Company>();
        String response = PerformHTTPSend('GET', 'https://jobs.googleapis.com/v2/companies', '', 'application/json', accesstoken );
        
        if(response != null){
            CompanyResult r = (CompanyResult)JSON.Deserialize(response,CompanyResult.class);
            result = r.companies;
        }
        
        return result;
    }
           
    public static String ConvertReqToJob(Opportunity opp, String companies){
        DateTime dt = DateTime.now();
        GoogleJobsConnector.Job job = new GoogleJobsConnector.Job();
        
        List<GoogleJobsConnector.Company> cList = (List<GoogleJobsConnector.Company>)JSON.Deserialize(companies, List<GoogleJobsConnector.Company>.class);
        String reqOpco = opp.OpCo__c;
        reqOpco = reqOpco.replaceAll('\\.', '').replaceAll(' ', '').replaceAll(',', '');
        
        String enterpriseReqSkills = parseSkillJson(opp.EnterpriseReqSkills__c);
        String reqSkills = constructReqSkillTags(enterpriseReqSkills, opp.Req_Search_Tags_Keywords__c);
        
        //Get Company From Req.OpCo__c
        for(GoogleJobsConnector.Company c : cList){
            if(!String.isBlank(reqOpco) && c.distributorCompanyId.contains(opp.OpCo__c)){
                job.distributorCompanyId = c.distributorCompanyId;
                job.companyName = c.name;
            }
        } 
        
        if (String.isBlank(job.distributorCompanyId)) {
            return '';
        }
        
        job.requisitionID = opp.ID;
        job.JobTitle = opp.Req_Job_Title__c;
        job.visibility = 'PRIVATE';
        job.employmentTypes = new List<GoogleJobsConnector.EmploymentType>();
        
        if (!((opp.StageName == 'Qualified') || (opp.StageName == 'Presenting') || (opp.StageName == 'Interviewing')) || 
                (opp.isClosed == true) || (opp.Req_OFCCP_Required__c == true)) {            
            return '';                        
        } else if ((opp.Opco__c == 'AG_EMEA' ) && !evalEMEAFilter(opp)) {
            return '';    
        } else {
            job.expiryDate = new GoogleJobsConnector.GJDate(Date.today().addDays(90));
        }
        
        if(opp.Req_Product__c == 'Contract to Hire'){
            job.employmentTypes.add(EmploymentType.CONTRACT_TO_HIRE);
        } else if(opp.Req_Product__c == 'Permanent'){
            job.employmentTypes.add(EmploymentType.FULL_TIME);
        } else if(opp.Req_Product__c == 'Full Time Consultant'){
            job.employmentTypes.add(EmploymentType.FULL_TIME);
        }  else if(opp.Req_Product__c == 'Contract'){
            job.employmentTypes.add(EmploymentType.TEMPORARY);
        } else{
            job.employmentTypes.add( EmploymentType.OTHER);
        }

        job.description = '';

        if(opp.Req_External_Job_Description__c != null){
            job.description += '\n' + opp.Req_External_Job_Description__c + '\n';
        }
        if(opp.Req_Job_Title__c != null){
            job.description += '\n' + opp.Req_Job_Title__c + '\n';
        }
        if(reqSkills != null){
            job.description += '\n' + reqSkills + '\n';
        }
        if(enterpriseReqSkills != null){
            job.description += '\n' + enterpriseReqSkills + '\n';
        }
        if(opp.Req_Skill_Details__c != null){
            job.qualifications += '\n' + opp.Req_Skill_Details__c + '\n';
        }
        
        /**if(opp.Main_Skill__c != null){
            job.qualifications += '\n' + opp.Main_Skill__c.replace('Uncategorized','') + '\n';
        }
        if(opp.Job_Code__c != null){
            job.qualifications += '\n' + opp.Job_Code__c + '\n';
        }*/

        job.jobLocations = new List<GoogleJobsConnector.JobLocation>();
        job.locations = new List<String>();
        job.locations.add(opp.Req_Worksite_Street__c + ', '+ opp.Req_Worksite_City__c + ', ' + opp.Req_Worksite_State__c + ', ' + opp.Req_Worksite_Country__c + ', ' + opp.Req_Worksite_Postal_Code__c);
        job.region = GoogleJobsConnector.Region.REGION_UNSPECIFIED;

        job.publishDate = new GoogleJobsConnector.GJDate(opp.Req_Qualified_Date__c);
                
        job.filterableCustomFields = new Map<Integer, CustomField>();
        
        List<String> m1 = new List<String>(); //Keywords
        m1.add(reqSkills);
        job.filterableCustomFields.put(1, new CustomField(m1));
                
        List<String> m2 = new List<String>(); //Job Description to Display
        if(opp.Req_External_Job_Description__c != null){
            m2.add(opp.Req_External_Job_Description__c);
        }else{
            m2.add(opp.Req_Job_Title__c);   
        }
        job.filterableCustomFields.put(3, new CustomField(m2));
        job.applicationInstruction = 'Apply via Communities';
            
        JobRequest jr = new JobRequest(job);
        system.debug(jr);
        return JSON.Serialize(jr,true);
    }

    public static JobSearchResponse PerformSearch(JobSearchRequest req){
        JobSearchResponse resp = new JobSearchResponse();
        String accesstoken = Authenticate();        
        system.debug(JSON.serialize(req));
        String response = PerformHTTPSend('POST', 'https://jobs.googleapis.com/v2/jobs:search', JSON.serialize(req), 'application/json', accesstoken );
        if(response != null){
            resp = (JobSearchResponse)JSON.Deserialize(response,JobSearchResponse.class);
        }
        
        return resp;
    }
    
   
    
    public static void cleanUpAllJobs(String companyId) {
        GoogleJobsConnector.CompanyJobList companyJobList = queryCompanyJobs(companyId, '');
        
        List<String> jobIds = new List<String>();
        if (companyJobList != null) {
            for(GoogleJobsConnector.Job job: companyJobList.jobs) {
                jobIds.add(job.requisitionId);
            }
            GoogleJobsBatchable gjb = new GoogleJobsBatchable(companyId, jobIds, companyJobList.nextPageToken, 2);
            System.schedule('Google Job Cleanup - 1', getCronExpression(1), gjb);
        }        
    }
    
    @future(callout = true)
    public static void cleanUpJobs(List<String> jobIds, String companyId, String nextPageToken, Integer iteration) {
        GoogleJobsConnector.CompanyJobList companyJobList = queryCompanyJobs(companyId, nextPageToken);
        GoogleJobsBatchable gjb;
        
        deleteJobsById(jobIds);
        
        List<String> nextPageJobIds = new List<String>();
        if ((companyJobList != null) && (companyJobList.jobs != null)) {
            for(GoogleJobsConnector.Job job: companyJobList.jobs) {
                nextPageJobIds.add(job.requisitionId);
            }           
            gjb = new GoogleJobsBatchable(companyId, nextPageJobIds, companyJobList.nextPageToken, (iteration+1));
            System.schedule('Google Job Cleanup - ' + iteration, getCronExpression(iteration), gjb);
        } else {
            gjb = new GoogleJobsBatchable(true);
            System.schedule('Google Job Clean Up', getCronExpression(iteration), gjb);
        }
        
    }
    
    public static GoogleJobsConnector.CompanyJobList queryCompanyJobs(String companyId, String nextPageToken) {
        String accesstoken = GoogleJobsConnector.Authenticate(); 
        String url = 'https://jobs.googleapis.com/v2/' + companyId + '/jobs?idsOnly=true&pageSize=400&pageToken=' + nextPageToken;
        String response = GoogleJobsConnector.PerformHTTPSend('GET', url, '', 'application/json', accesstoken );
        GoogleJobsConnector.CompanyJobList cjlist;
        if (response != null) {
            cjlist = (GoogleJobsConnector.CompanyJobList) JSON.deserialize(response, GoogleJobsConnector.CompanyJobList.class);
        }        
        return cjlist;
    }
    
    public static String getCronExpression(Integer iteration) {
        DateTime now = DateTime.now().addMinutes(2);
        Integer min = now.minute();
        Integer hour = now.hour();
        Integer day = now.day();
        String month = now.format('MMM').toUpperCase();
        Integer year = now.year();

        String sch = '0 ' + String.valueOf(min) + ' ' + String.valueOf(hour) + ' ' + String.valueOf(day)  +' '+ month + ' ? ' + String.valueOf(year);
        return sch;
    }
    
    @testVisible
    private static void deleteJobsById(List<String> jobIds) {
        String accesstoken = GoogleJobsConnector.Authenticate();
        List<String> deleteBatch = new List<String>();
        for(String jobID : jobIds){
            String dr  = JSON.serialize( new DeleteRequest(jobID));
            deleteBatch.add(dr);
        }
        GoogleJobsConnector.PerformBatchSend(deleteBatch, accessToken, ':deleteByFilter');
    }
    
    @testVisible
    private static String parseSkillJson(String skillJson) {
        String parsedVal = '';
        if (!String.isBlank(skillJson)) {
            JSONParser parser = JSON.createParser(skillJson);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'name')) {
                    parser.nextToken();
                    if(!String.isBlank(parsedVal)) {
                        parsedVal = parsedVal + ', ' + parser.getText();
                    } else {
                        parsedVal = parser.getText();
                    }
                }
            }
        }        
        return parsedVal;
    }
    
    @testVisible
    private static String constructReqSkillTags(String enterpriseReqSkills, String classicReqSkills) {
        String skillTag = '';
        /**if (!String.isBlank(enterpriseReqSkills) && !String.isBlank(reqSkills)) {
            skillTag = reqSkills + ', ' + enterpriseReqSkills;
        } else if (!String.isBlank(enterpriseReqSkills) && String.isBlank(reqSkills)) {
            skillTag = enterpriseReqSkills;
        } else {
            skillTag = reqSkills;
        }*/
         if (!String.isBlank(enterpriseReqSkills)) {
            skillTag = enterpriseReqSkills;
        } else {
            skillTag = classicReqSkills;
        }
        return skillTag;
    }
    
    @testVisible
    private static boolean evalEMEAFilter(Opportunity o) {
        boolean result = false;
        if ((o.Opco__c == 'AG_EMEA') && !String.isBlank(o.Organization_Office__r.office_code__c) 
                   && ((o.Organization_Office__r.office_code__c == '00993') || (o.Organization_Office__r.office_code__c == '01262')
                      || (o.Organization_Office__r.office_code__c == '01271') || (o.Organization_Office__r.office_code__c == '01078')
                      || (o.Organization_Office__r.office_code__c == '00995') || (o.Organization_Office__r.office_code__c == '00992')
                      || (o.Organization_Office__r.office_code__c == '00991') || (o.Organization_Office__r.office_code__c == '01081')
                      || (o.Organization_Office__r.office_code__c == '01082'))
                   && (o.CreatedDate > emeaCutOffDate)) {
            result = true;      
        }
        return result;
    }
    
}