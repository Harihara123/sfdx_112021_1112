@isTest(SeeAllData=true) public class GoogleJobsConnector_Test{
    @isTest static void Test_PerformSearch(){ 
        //Run as Communities User - Required to get the correct Op Co.
        User u = [select Id from User where isActive=true and Profile_Name__C = 'Aerotek Community User' LIMIT 1];
        System.runAs(u){  
            
            TC_JobSearchGoogleJobs js = (TC_JobSearchGoogleJobs)TC_JobSearchFactory.getInstance('GoogleJobs');

            TC_JobSearchCriteria searchCriteria = new TC_JobSearchCriteria();
            searchCriteria.city = 'Baltimore';
            searchCriteria.state = 'MD';
            searchCriteria.radius = 'EveryWhere';
            searchCriteria.keywords = 'Developer';
            searchCriteria.opco = 'Aerotek'; 
            searchCriteria.pagesize ='10'; 
            searchCriteria.pgnum = '1';     

            TC_JobSearchResultWrapper r = js.getJobResult(searchCriteria);
            System.assertNotEquals(null, r);
        }

    }

    @isTest static void Test_PerformInitialOneTimeSetup(){
        GoogleJobsConnector.PerformInitialOneTimeSetup('');
    }

    @isTest static void Test_PerformJobUpdates(){
        mockJobSearchRequest();
        //Run as an Account Manager (Someone who will be triggering the update off a Req)
        User u = [select Id from User where isActive=true and Profile_Name__C = 'Aerotek AM' LIMIT 1];  
        System.runAs(u){

        }
    }

    @isTest static void Test_GoogleJobsBatchable(){
        
        GoogleJobsBatchable gjb = new GoogleJobsBatchable(true);
        system.assertEquals('PerformJobCleanup', gjb.method);

        List<Opportunity> rList = new List<Opportunity>();
        gjb = new GoogleJobsBatchable(rList, 'INSERT');
        system.assertEquals('LoadJobs', gjb.method);
    }
    
    @istest
    public static void testCleanUpScheduledJobs() {
		GoogleJobsConnector.JobCleanup();
		List<CronTrigger> jobs = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name like 'Google Job%'];
        System.assertEquals(0, jobs.size());        
    }
    
    @isTest 
    public static void testConvertReqToJob() {
        List<Opportunity> reqs = [select Id, Name,EnterpriseReqSkills__c, Req_Job_Title__c,Req_Worksite_City__c ,Req_Worksite_State__c ,Req_Worksite_Country__c ,
                    Req_Search_Tags_Keywords__c,Req_Skill_Details__c,Req_Qualified_Date__c, Req_Product__c,
                    Req_Red_Zone__c,OpCo__c,Req_External_Job_Description__c,status__c, StageName, Req_OFCCP_Required__c, 
                    IsClosed FROM Opportunity WHERE  StageName ='Qualified' AND Req_OFCCP_Required__c != true 
                      and Req_Qualified_Date__c != null and opco__c like 'TEK%' ORDER BY Req_Qualified_Date__c limit 5];
        String companyDataResp = '{"companies":[{"name":"companies/967067cd-c855-43f9-83f8-34bc65e052ad","title":"Aerotek,Inc","distributorCompanyId":"Aerotek,Inc","structuredCompanyHqLocation":{"postalAddress":{"addressLines":[""]},"latLng":{}}},{"name":"companies/ee767694-2f8a-4eea-af91-e23ea3b268db","title":"TEKsystems,Inc.","distributorCompanyId":"TEKsystems,Inc.","structuredCompanyHqLocation":{"postalAddress":{"addressLines":[""]},"latLng":{}}}],"metadata":{"requestId":"03e2cc7c-0b17-4c34-9674-a41584a38eae"}}';
        GoogleJobsConnector.CompanyResult result = (GoogleJobsConnector.CompanyResult)JSON.Deserialize(companyDataResp,GoogleJobsConnector.CompanyResult.class);
        
        for(Opportunity r : reqs) {
            try {
            	GoogleJobsConnector.ConvertReqToJob(r, JSON.serialize(result.companies));    
            } catch (Exception ex) {                
                System.assertEquals(1, 2);
            }
            
        }
    }
    
    @isTest
    public static void testGetEmploymentType() {
        GoogleJobsConnector.Job job = new GoogleJobsConnector.Job();
        
        
        
        List<GoogleJobsConnector.EmploymentType> employmentTypes = new List<GoogleJobsConnector.EmploymentType>();
        employmentTypes.add(GoogleJobsConnector.EmploymentType.CONTRACT_TO_HIRE);
        job.employmentTypes = employmentTypes;
        System.assertEquals('Contract to Hire', GoogleJobsConnector.getEmploymentType(job));
        
        employmentTypes = new List<GoogleJobsConnector.EmploymentType>();
        employmentTypes.add(GoogleJobsConnector.EmploymentType.FULL_TIME);
        job.employmentTypes = employmentTypes;
        System.assertEquals('Direct Placement', GoogleJobsConnector.getEmploymentType(job));
        
        employmentTypes = new List<GoogleJobsConnector.EmploymentType>();
        employmentTypes.add(GoogleJobsConnector.EmploymentType.TEMPORARY);
        job.employmentTypes = employmentTypes;
        System.assertEquals('Contract', GoogleJobsConnector.getEmploymentType(job));
        
        employmentTypes = new List<GoogleJobsConnector.EmploymentType>();
        employmentTypes.add(GoogleJobsConnector.EmploymentType.INTERN);
        job.employmentTypes = employmentTypes;
        System.assertEquals('Unknown', GoogleJobsConnector.getEmploymentType(job));
    }
    
    @isTest
    public static void testGetCompaniesForException() {
        try {
            	GoogleJobsConnector.GetCompanies('dummyToken');    
            } catch (Exception ex) {                
                System.assertEquals(1, 2);
            }
    }
    
    @isTest
    public static void testPerformBatchSendForException() {
        mockJobSearchResponse();
        List<String> jobBatch = new List<String>();
        jobBatch.add('{"jobId":"dummyId"}');
        try {
            	GoogleJobsConnector.PerformBatchSend(jobBatch, '', 'POST');    
            } catch (Exception ex) {                
                System.assertEquals(1, 2);
            }
    }
    
    
    private static void mockJobSearchRequest() {
        GoogleJobsConnector.JobSearchRequest req = new GoogleJobsConnector.JobSearchRequest();
        GoogleJobsConnector.RequestMetadata requestMetadata = new GoogleJobsConnector.RequestMetadata();
        
        GoogleJobsConnector.DeviceInfo deviceInfo = new GoogleJobsConnector.DeviceInfo();
        deviceInfo.deviceType = GoogleJobsConnector.DeviceType.WEB;
        deviceInfo.id = 'dummyDeviceId';
        
        GoogleJobsConnector.JobFilters filters = new GoogleJobsConnector.JobFilters();
        
        List<GoogleJobsConnector.LocationFilter> locationFilters = new List<GoogleJobsConnector.LocationFilter>();
        
        GoogleJobsConnector.LocationFilter locationFilter = new GoogleJobsConnector.LocationFilter();
        locationFilter.name = 'dummyLocation';
        locationFilter.distanceInMiles = 25.0;
        
        GoogleJobsConnector.LatLng latLng = new GoogleJobsConnector.LatLng(56.0,177.0);
        
        locationFilter.latLng = latLng;
        locationFilter.isTelecommute = false;
        locationFilters.add(locationFilter);
        
        filters.locationFilters = locationFilters;
        
        req.filters = filters;
        
        filters.categories = null;
        
        requestMetadata.deviceInfo = deviceInfo;
        requestMetadata.domain = 'https://connect.teksystems.com';
        
        req.requestMetadata = requestMetadata;
        req.jobView = GoogleJobsConnector.JobView.FULL;
    }
    
    private static void mockJobSearchResponse() {
        
        GoogleJobsConnector.JobSearchResponse response = new GoogleJobsConnector.JobSearchResponse();
        response.nextPageToken = '2';
        response.totalSize = '56';
        response.estimatedTotalSize = '50';
        
        GoogleJobsConnector.SpellingCorrection spellResult = new GoogleJobsConnector.SpellingCorrection();
        spellResult.corrected = true;
        spellResult.correctedText = 'Corrected';
        response.spellResult = spellResult;
        
        List<GoogleJobsConnector.MatchingJob> matchingJobs = new List<GoogleJobsConnector.MatchingJob>();
        response.matchingJobs = matchingJobs;
        GoogleJobsConnector.MatchingJob matchingJob = new GoogleJobsConnector.MatchingJob();
        matchingJob.jobSummary = 'this is best matched job';
        matchingJob.jobTitleSnippet = 'dummy title';
        matchingJob.searchTextSnippet = 'dummy...text..job';
        matchingJob.job = mockJob();
        response.matchingJobs.add(matchingJob);
        
        GoogleJobsConnector.ResponseMetadata metdata = new GoogleJobsConnector.ResponseMetadata();
        metdata.mode = GoogleJobsConnector.Mode.JOB_SEARCH;
        metdata.requestId = 'test_response';
        metdata.experimentIdList = null;
        response.metadata = metdata;
        response.numJobsFromBroadenedQuery = 30;
        
        List<GoogleJobsConnector.JobLocation> appliedJobLocationFilters = new List<GoogleJobsConnector.JobLocation>();
        GoogleJobsConnector.JobLocation jobLocation = new GoogleJobsConnector.JobLocation();
        jobLocation.locationType = GoogleJobsConnector.LocationType.LOCATION_TYPE_UNSPECIFIED;
        jobLocation.postalAddress = mockPostalAddress();
        jobLocation.latLng = new GoogleJobsConnector.LatLng(77.0,145.0);
        jobLocation.radiusMeters = 20000;
        appliedJobLocationFilters.add(jobLocation);
        response.appliedJobLocationFilters = appliedJobLocationFilters;
        
        response.jobView = GoogleJobsConnector.JobView.JOB_VIEW_UNSPECIFIED;
    }
    
    private static GoogleJobsConnector.PostalAddress mockPostalAddress() {
        GoogleJobsConnector.PostalAddress postalAddress = new GoogleJobsConnector.PostalAddress();
        postalAddress.revision = 2.1;
        postalAddress.regionCode  = 'dummyRegion';
        postalAddress.languageCode = 'en';
        postalAddress.postalCode = '21045';
        postalAddress.sortingCode = 'Title';
        postalAddress.administrativeArea = 'US_EAST';
        postalAddress.locality = 'Baltimore';
        postalAddress.subLocality = 'Northern Baltimore';
        postalAddress.organization = 'Allegis';
        postalAddress.addressLines  = null;
        postalAddress.recipients  = null;
        return postalAddress;
    }
    
    
    private static GoogleJobsConnector.Job mockJob() {
        GoogleJobsConnector.Job job = new GoogleJobsConnector.Job();
        job.name  = 'test job';
        job.requisitionId  = 'test req';
        job.jobTitle = 'test title';
        List<GoogleJobsConnector.EmploymentType> employmentTypes = new  List<GoogleJobsConnector.EmploymentType>();
        employmentTypes.add(GoogleJobsConnector.EmploymentType.CONTRACT_TO_HIRE);
        job.employmentTypes = employmentTypes;
        job.description = 'test description';
        job.applicationUrls = null;
        job.applicationEmailList = null;
        job.applicationInstruction = 'test instruction';
        job.responsibilities = 'test responseibilities';
        job.qualifications = 'test qualification';
        job.department = 'test department';
        job.educationLevels = GoogleJobsConnector.EducationLevel.EDUCATION_LEVEL_UNSPECIFIED;
        job.level = GoogleJobsConnector.JobLevel.EXPERIENCED;        
        job.startDate = new GoogleJobsConnector.GJDate(Date.newInstance(2018, 01, 31));
        job.endDate = new GoogleJobsConnector.GJDate(Date.newInstance(2018, 12, 31));
        
        GoogleJobsConnector.CompensationInfo compensationInfo = new GoogleJobsConnector.CompensationInfo();
        compensationInfo.amount = new GoogleJobsConnector.Money(6700.5, 'USD');
        compensationInfo.max = new GoogleJobsConnector.Money(7000, 'USD');
        compensationInfo.min = new GoogleJobsConnector.Money(6000, 'USD');
        compensationInfo.type = GoogleJobsConnector.JobCompensationType.SALARY;
        job.compensationInfo = compensationInfo;
        job.incentives = 'test incentive';
        job.languageCode = 'en';
        job.benefits = GoogleJobsConnector.JobBenefitType.JOB_BENEFIT_TYPE_UNSPECIFIED;
        job.expiryDate = new GoogleJobsConnector.GJDate(Date.newInstance(2018, 12, 31));
        job.distributorCompanyId = null;
        job.referenceUrl = null;
        job.companyTitle = 'test title';
        job.companyName = 'test Company';
        
        Map<Integer, GoogleJobsConnector.CustomField> filterableCustomFields = new  Map<Integer, GoogleJobsConnector.CustomField>();
        List<String> cf1StrList = new List<String>();
        cf1StrList.add('test');
        GoogleJobsConnector.CustomField customField1 = new GoogleJobsConnector.CustomField(cf1StrList);
        filterableCustomFields.put(1, customField1);
        
        List<String> cf3StrList = new List<String>();
        cf3StrList.add('test external description');
        GoogleJobsConnector.CustomField customField3 = new GoogleJobsConnector.CustomField(cf3StrList);
        filterableCustomFields.put(3, customField3);
        job.filterableCustomFields = filterableCustomFields;
        job.unindexedCustomFields = null;
        job.visibility = null; 
        job.publishDate = new GoogleJobsConnector.GJDate(Date.newInstance(2018, 12, 31));
        job.createTime = null;
        job.updateTime = null;
        job.promotionValue = 0.0;
        job.locations = null;
        List<GoogleJobsConnector.JobLocation> jobLocations = new List<GoogleJobsConnector.JobLocation>();
        GoogleJobsConnector.JobLocation jobLocation = new GoogleJobsConnector.JobLocation();
        jobLocation.locationType = GoogleJobsConnector.LocationType.LOCATION_TYPE_UNSPECIFIED;
        jobLocation.postalAddress = mockPostalAddress();
        jobLocation.latLng = new GoogleJobsConnector.LatLng(77.0,145.0);
        jobLocation.radiusMeters = 20000;
        jobLocations.add(jobLocation);
        job.jobLocations = jobLocations;
        job.region = GoogleJobsConnector.Region.REGION_UNSPECIFIED; 
        
        return job;
    }
    
    @isTest public static void testGetJobFilters() {
		User u = [select Id from User where isActive=true and Profile_Name__C = 'Aerotek Community User' LIMIT 1];
        System.runAs(u){ 
            
            TC_JobSearchGoogleJobs js = (TC_JobSearchGoogleJobs)TC_JobSearchFactory.getInstance('GoogleJobs');
            String filters = 'Employment Type:Contract,Employment Type:Contract To Hire';
            GoogleJobsConnector.JobFilters jobFilters;
            jobFilters = js.getJobFilters(filters, new GoogleJobsConnector.JobFilters());
            system.debug(jobFilters);
        }
    }
    
    @isTest
    public static void testBuildJobList() {
        User u = [select Id from User where isActive=true and Profile_Name__C = 'Aerotek Community User' LIMIT 1];
        List<Reqs__c> req = [select id from reqs__C where Opco__c like 'Aero%' limit 1];
        System.runAs(u){ 
            
            TC_JobSearchGoogleJobs js = (TC_JobSearchGoogleJobs)TC_JobSearchFactory.getInstance('GoogleJobs');
            List<GoogleJobsConnector.MatchingJob> matchingJobs = new List<GoogleJobsConnector.MatchingJob>();
            GoogleJobsConnector.MatchingJob matchingJob = new GoogleJobsConnector.MatchingJob();
            matchingJob.job = mockJob();
            matchingJob.job.requisitionId = req[0].Id;
            matchingJobs.add(matchingJob);
            js.buildJobList(matchingJobs);
            
        }
    }
    
    @isTest
    public static void testGetCronExpression() {
        DateTime now = DateTime.now().addMinutes(2);
		Integer min = now.minute();
		Integer hour = now.hour();
		Integer day = now.day();
		String month = now.format('MMM').toUpperCase();
		Integer year = now.year();
        
        String sch = '0 ' + String.valueOf(min) + ' ' + String.valueOf(hour) + ' ' + String.valueOf(day)  +' '+ month + ' ? ' + String.valueOf(year);
        System.assertEquals(sch, GoogleJobsConnector.getCronExpression(1));
    }
    
    @isTest
    public static void testDeleteJobsById() {
        List<String> jobIds = new List<String>();
        jobIds.add('test1');
        jobIds.add('test2');
        try {
            GoogleJobsConnector.deleteJobsById(jobIds);
        } catch (Exception ex) {
            System.assertEquals(1,2);
        }        
    }
    
    @isTest
    public static void testCleanUpAllJobs() {
        Test.startTest();
        GoogleJobsConnector.cleanUpAllJobs('test');
        GoogleJobsBatchable gjb = new GoogleJobsBatchable('testCompany', null, 'dummyToken', 2);
        Test.stopTest();
    }
}