/*
* @author : Neel Kamal
* @Description : This class is the wrapper class for Google Place detail reponse payload.
*/
public class GooglePlaceDetail {

	public class Address_components {
		public String long_name;
		public String short_name;
		public List<String> types;
	}

	public class Geometry {
		public Location location;
		public Viewport viewport;
	}

	public class Photos {
		public Integer height;
		public List<String> html_attributions;
		public String photo_reference;
		public Integer width;
	}

	public class Viewport {
		public Location northeast;
		public Location southwest;
	}

	public List<Html_attributions> html_attributions;
	public Result result;
	public String status;

	public class Html_attributions {
	}

	public class Location {
		public Double lat;
		public Double lng;
	}

	public class Result {
		public List<Address_components> address_components;
		public String adr_address;
		public String formatted_address;
		public Geometry geometry;
		public String icon;
		public String name;
		public List<Photos> photos;
		public String place_id;
		public String reference;
		public List<String> types;
		public String url;
		public Integer utc_offset;
		public String vicinity;
		public String website;
	}

	
	public static GooglePlaceDetail parse(String json) {
		return (GooglePlaceDetail) System.JSON.deserialize(json, GooglePlaceDetail.class);
	}
}