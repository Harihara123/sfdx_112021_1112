
@IsTest
public class GooglePlaceDetail_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'   \"html_attributions\" : [],'+
		'   \"result\" : {'+
		'      \"address_components\" : ['+
		'         {'+
		'            \"long_name\" : \"Hanover\",'+
		'            \"short_name\" : \"Hanover\",'+
		'            \"types\" : [ \"locality\", \"political\" ]'+
		'         },'+
		'         {'+
		'            \"long_name\" : \"Hanover\",'+
		'            \"short_name\" : \"Hanover\",'+
		'            \"types\" : [ \"administrative_area_level_3\", \"political\" ]'+
		'         },'+
		'         {'+
		'            \"long_name\" : \"Grafton County\",'+
		'            \"short_name\" : \"Grafton County\",'+
		'            \"types\" : [ \"administrative_area_level_2\", \"political\" ]'+
		'         },'+
		'         {'+
		'            \"long_name\" : \"New Hampshire\",'+
		'            \"short_name\" : \"NH\",'+
		'            \"types\" : [ \"administrative_area_level_1\", \"political\" ]'+
		'         },'+
		'         {'+
		'            \"long_name\" : \"United States\",'+
		'            \"short_name\" : \"US\",'+
		'            \"types\" : [ \"country\", \"political\" ]'+
		'         }'+
		'      ],'+
		'      \"adr_address\" : \"\\u003cspan class=\\\"locality\\\"\\u003eHanover\\u003c/span\\u003e, \\u003cspan class=\\\"region\\\"\\u003eNH\\u003c/span\\u003e, \\u003cspan class=\\\"country-name\\\"\\u003eUSA\\u003c/span\\u003e\",'+
		'      \"formatted_address\" : \"Hanover, NH, USA\",'+
		'      \"geometry\" : {'+
		'         \"location\" : {'+
		'            \"lat\" : 43.7022451,'+
		'            \"lng\" : -72.28955259999999'+
		'         },'+
		'         \"viewport\" : {'+
		'            \"northeast\" : {'+
		'               \"lat\" : 43.77095504148843,'+
		'               \"lng\" : -72.07567296226443'+
		'            },'+
		'            \"southwest\" : {'+
		'               \"lat\" : 43.65859105220848,'+
		'               \"lng\" : -72.30598690571809'+
		'            }'+
		'         }'+
		'      },'+
		'      \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/geocode-71.png\",'+
		'      \"name\" : \"Hanover\",'+
		'      \"photos\" : ['+
		'         {'+
		'            \"height\" : 2268,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/114115147494628758010\\\"\\u003ePrasad V\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwIVq2J-ytDATV2RqR9DLzpHgUMUUS-Di8Ek1TXj7FHOOZwXOje2aUXnQez4tU7jG-sSN4130a7xszWRXRFfstA5hWe9KXr88vlBt0iwan_HnRAExES7q5_-4QSIQ_w6tOJLnqh5y_kbWmhMySZ6r7z-NEwM3KxKZoaxr6inedvNTqoX\",'+
		'            \"width\" : 4032'+
		'         },'+
		'         {'+
		'            \"height\" : 2560,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/106867091746957148060\\\"\\u003eVirginia Gabert\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwLrvK7-_wslDwo93AOs0_2DL6MrMbVT7IgT-dzb2C5b1NAJgY78cOfzYo6EySONiSETTdHxWy20M6IK8wxcSi_ILWOfGGlMYGOphnJp61cDg9kl8aNc9KNH5mJdDk5vrT1R8gKnl7wEqZglt53-8Nkzt4kobkpKLmADSHsb6b86SvTS\",'+
		'            \"width\" : 1440'+
		'         },'+
		'         {'+
		'            \"height\" : 3024,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/102177036434450364614\\\"\\u003eThomas Sherwood\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwIzO26vNrJMolqzYS5uupmpfmnC_O4v9Y2TxqCncSFuMNKBB6NQwqLPvgc-MfHT7-tNZsok4R5_WCbDw0H1QiN-Au_zSNJTM3Lx12JCxEfHg_cAWjeyklsSr-ONF3k4g4QknZ_cYHry8raTgUgvjFb_sygVXBXkEHK_17CdzhcTYq0-\",'+
		'            \"width\" : 4032'+
		'         },'+
		'         {'+
		'            \"height\" : 3024,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/112868115811905043212\\\"\\u003eJason Schackai\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwIwBiinJU0B9e5EIgCU0ZToQFRaSpY6OOreexzWtP4DTNcgr9Xmcz4l_4r6fH4GEwvl7WwNroQrHKl_BGaaEeKwpFJ_vB_5tXrDsj2So5XJVfYr6E9GAxhRd98PlR1kQOduGR_q9hil4-S9Nz_NmliZfCxcgkCmDqs54JiLhAzqGGMg\",'+
		'            \"width\" : 4032'+
		'         },'+
		'         {'+
		'            \"height\" : 2988,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/111357251787849434865\\\"\\u003eCaroline Raven\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwIbF9IZ1MI6FXMj028QuL87zXUJi-owQeZrCzZag3wHxW_g5kZFhMbdrh-ovLuRyYdJDX2lb5rcAnRoTZKpYdPJv_CnmPUzWdUqWSK7MsbfNL5gFB4qxmS-U72wjLy94k326S2vgpXkkCToGEOH3nvYsMYSuWhCdxN6EhLucz0dgZdO\",'+
		'            \"width\" : 5312'+
		'         },'+
		'         {'+
		'            \"height\" : 3496,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/105925611283117879031\\\"\\u003eGriffin Stotland\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwKN3yGiaLN_FqWhta11uuEUHqwlLpTVNyPTgyikmmO3k9YMvZ9hkHE6PRNIPOzI2F_QijABEw5eqnsVc15y8qNnabq_zZUCtec2nnCz6S_5mgXXn2_ZpaqwXR1JRqmfvM3dvR5GSPzaREUwEKzYb8gZ3QgutBD9zJQ-CzO8fFsgXCIq\",'+
		'            \"width\" : 4656'+
		'         },'+
		'         {'+
		'            \"height\" : 2340,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/106575870187251893777\\\"\\u003eBJ Premore\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwJm-a7Xhau8c1C0O_nZBGcOP-M7OXMeBJhNGzt-R6YGN8RzjbCIWhqqMBfeS2cGTNEVtONoG8HmHAZbezNDExf14c7lTdgQ1Sg0W8KpHRnne8WuhFjPR7z13QhAZST_iHBjHJPZDLcLJS7ojBpqlycZGoXBs7EPJIqnLox8rqtzfWx9\",'+
		'            \"width\" : 4160'+
		'         },'+
		'         {'+
		'            \"height\" : 3024,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/103289142551227761011\\\"\\u003eJohn wallace\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwKdsapbUTst4FbA_UC0MQCCqQn2LlLHl9EK3LcfEIZ2Hb_NHbruE9ub-I-XhZQDyWsQX2U2lAjklTPmkj18vZEx5k01ApjrmGTinqqyVge199znk-wlBQgHQ98D30s-5s1PnoTvmfO5_fcG1yjdd3EGJloyO5m-sj-sjeT_g2ST9sJY\",'+
		'            \"width\" : 4032'+
		'         },'+
		'         {'+
		'            \"height\" : 4000,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/113400156772049972199\\\"\\u003eNihal Velpanur\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwLLQ--tR_xytWIY3oJTvSQvnmGCZED5-jXrHTRtNKtPOk0WFWn9cEYrZ0mBMgwsn46FxYEwTw6YO_E8a2h6BzfputGsXM9issk1D0k8ow0JmxhtHpkSP1OWgLcdFC4OVmZH8rmSc4sLtKpVjOHpBO5JKng4xTMFWz-3UoagSFsX-IR_\",'+
		'            \"width\" : 2992'+
		'         },'+
		'         {'+
		'            \"height\" : 4000,'+
		'            \"html_attributions\" : ['+
		'               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/117502500666591140652\\\"\\u003eAhmad Dbouk\\u003c/a\\u003e\"'+
		'            ],'+
		'            \"photo_reference\" : \"ATtYBwIaMvMnsG5aD7n2Nl4PsMkc21FjH0jC5c4quXpYItbeW9cXG-lAWiMMVpj1tulXzhMYH8xZnfX08vqjQOnNjJGP0VJsu4DMmhL-9o9-HV_xkrLvpiXPFBBYTMjn4mCCMQnLQuuCWin2qrC5fDI5QV5bd_uAD6N0Z5diCJnc_8xZg_BP\",'+
		'            \"width\" : 3000'+
		'         }'+
		'      ],'+
		'      \"place_id\" : \"ChIJ5cyscCK3tEwRT_DsE-vcGn8\",'+
		'      \"reference\" : \"ChIJ5cyscCK3tEwRT_DsE-vcGn8\",'+
		'      \"types\" : [ \"locality\", \"political\" ],'+
		'      \"url\" : \"https://maps.google.com/?q=Hanover,+NH,+USA&ftid=0x4cb4b72270accce5:0x7f1adceb13ecf04f\",'+
		'      \"utc_offset\" : -240,'+
		'      \"vicinity\" : \"Hanover\",'+
		'      \"website\" : \"http://www.hanovernh.org/\"'+
		'   },'+
		'   \"status\" : \"OK\"'+
		'}';
		GooglePlaceDetail obj = GooglePlaceDetail.parse(json);
		System.assert(obj != null);
	}
}