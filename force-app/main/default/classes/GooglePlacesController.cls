public with sharing class GooglePlacesController {
	
    private class GooglePredictions {
        public GooglePrediction[] predictions {get;set;}
    }

    private class GooglePrediction {
        public String place_id {get;set;}
    }

    public class GooglePlace {
        public GooglePlaceResult result {get;set;}
    }

    public class GooglePlaceResult {
        public List<AddressComponent> address_components {get;set;}
        public String formatted_address {get;set;}
        public GooglePlaceGeometry geometry {get;set;}
        public List<String> types {get;set;}
    }

    public class GooglePlaceGeometry {
        public Viewport viewport {get;set;}
        public LatLong location {get;set;}
    }

    public class Viewport {
        public Latlong northeast {get;set;}
        public Latlong southwest {get;set;}
    }

    public class LatLong {
        public String lat {get;set;}
        public String lng {get;set;}
    }

    public class AddressComponent {
        public String long_name;
    }  

    @AuraEnabled
    public static String autocomplete(String input, String myLocation) {
        return autocomplete(input, myLocation, false);
    }

	@AuraEnabled
	public static String autocomplete(String input, String myLocation, Boolean getAddress) {
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
        String typesParam = '(regions)';
        if(getAddress){
            typesParam = 'geocode';
        }
        String url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?key='
                    + System.Label.ATS_GOOGLE_API_KEY 
                    + '&types='+typesParam
                    + '&input='
                    + EncodingUtil.urlEncode(input, 'UTF-8');
		// If location was made available from the client, pass the parameter to bias results within 300km of user.
		if (String.isNotBlank(myLocation)) {
			 url += '&radius=300000&location=' + myLocation;
		}
		req.setEndpoint(url);

		Http h = new Http();
		HttpResponse res = h.send(req);
		System.debug(res.getBody());

		return res.getBody();
	}

	@AuraEnabled(cacheable=true)
	public static String getPlaceDetails(String placeId) {
		
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
        String url = 'https://maps.googleapis.com/maps/api/place/details/json?key='
                    + System.Label.ATS_GOOGLE_API_KEY 
                    + '&placeid=' + placeId;
		req.setEndpoint(url);

		Http h = new Http();
		HttpResponse res = h.send(req);
		//System.debug(res.getBody());

		return res.getBody();
	}

    @AuraEnabled
    public static String getGeoCoordinates(String location) {
        String options = GooglePlacesController.autocomplete(location, null);
        GooglePredictions preds = (GooglePredictions) JSON.deserialize(options, GooglePredictions.class);
        
        if (preds != null && preds.predictions.size() > 0) {
            String place = GooglePlacesController.getPlaceDetails(preds.predictions[0].place_id);
            GooglePlace gp = (GooglePlace) JSON.deserialize(place, GooglePlace.class);
            if (gp != null) {
                LatLong loc = gp.result.geometry.location;
                return loc.lat + ',' + loc.lng;
            }
        }

        return null;
    }

    @AuraEnabled
    public static GooglePlace getGoogleGeodata(String location) {
        String options = GooglePlacesController.autocomplete(location, null);
        GooglePredictions preds = (GooglePredictions) JSON.deserialize(options, GooglePredictions.class);
        
        if (preds != null && preds.predictions.size() > 0) {
            String place = GooglePlacesController.getPlaceDetails(preds.predictions[0].place_id);
            GooglePlace gp = (GooglePlace) JSON.deserialize(place, GooglePlace.class);
            if (gp != null) {
                return gp;
            }
        }

        return null;
    }

    public static Long calculateViewportDiagonal(Viewport viewport) {
        Double swlat = Double.valueOf(viewport.southwest.lat) / 180 * Math.PI;
        Double swlng = Double.valueOf(viewport.southwest.lng) / 180 * Math.PI;
        Double nelat = Double.valueOf(viewport.northeast.lat) / 180 * Math.PI;
        Double nelng = Double.valueOf(viewport.northeast.lng) / 180 * Math.PI;

        Double r = 3959.9; // miles
        Double dLat = nelat - swlat;
        Double dLon = nelng - swlng;
        Double a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLon / 2) * Math.sin(dLon /2) * Math.cos(swlat) * Math.cos(nelat);
        Double c = 2 * Math.asin(Math.sqrt(a));
        return Math.roundToLong(r * c);
    }

}