public with sharing class GooglePubSubHelper {
 
    private static String PubSub_URL = Connected_Data_Export_Switch__c.getInstance('DataExport').PubSub_URL__c;
    private static String INGEST10X = 'ingest10x';
    
    private class GooglePubSubException extends Exception {}

    public static String get_access_token(){
       
        if (Cache.Org.contains('local.Ingest10x.GoogleIngestPubSubToken')) {
            return (String)Cache.Org.get('local.Ingest10x.GoogleIngestPubSubToken');
        } else {
           
            String resToken = [SELECT token__c FROM IngestPubSub__mdt where label = 'Prod'].token__c;
           
            JSONParser tokenParser = JSON.createParser(resToken);
            Map<String,String> tokenMap = new Map<String,String>();
           
            while (tokenParser.nextToken() != null) {
                if (tokenParser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String key = tokenParser.getText();
                   
                    tokenParser.nextToken();
                    tokenMap.put(key,tokenParser.getText());
                }
                //tokenParser.nextToken();
            }  
            
            resToken = '';
            tokenParser = null;
           
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            req.setEndpoint('https://accounts.google.com/o/oauth2/token');
            req.setMethod('POST');
           
            req.setHeader('ContentType','application/x-www-form-urlencoded');
           
            String header = '{"alg":"RS256","typ":"JWT"}';
            String header_encoded = EncodingUtil.base64Encode(blob.valueof(header));
           
            String claim_set = '{"iss":"' +tokenMap.get('client_email') +'"';
            claim_set += ',"scope":"https://www.googleapis.com/auth/pubsub"';
            claim_set += ',"aud":"https://accounts.google.com/o/oauth2/token"';
            claim_set += ',"exp":"' + datetime.now().addHours(1).getTime()/1000;
            claim_set += '","iat":"' + datetime.now().getTime()/1000 + '"}';
           
            String claim_set_encoded = EncodingUtil.base64Encode(blob.valueof(claim_set));
            String signature_encoded = header_encoded + '.' + claim_set_encoded;
            String key = tokenMap.get('private_key').remove('-----BEGIN PRIVATE KEY-----\n').remove('\n-----END PRIVATE KEY-----\n');
           
            blob private_key = EncodingUtil.base64Decode(key);
            signature_encoded = signature_encoded.replaceAll('=','');
            String signature_encoded_url = EncodingUtil.urlEncode(signature_encoded,'UTF-8');
            blob signature_blob =   blob.valueof(signature_encoded_url);
           
            String signature_blob_string = EncodingUtil.base64Encode(Crypto.sign('RSA-SHA256', signature_blob, private_key));
           
            String JWT = signature_encoded + '.' + signature_blob_string;
           
            JWT = JWT.replaceAll('=','');
           
            String grant_string= 'urn:ietf:params:oauth:grant-type:jwt-bearer';
            req.setBody('grant_type=' + EncodingUtil.urlEncode(grant_string, 'UTF-8') + '&assertion=' + EncodingUtil.urlEncode(JWT, 'UTF-8'));
            res = h.send(req);
            String response_debug = res.getBody() +' '+ res.getStatusCode();
            if(res.getStatusCode() == 200) {
                JSONParser parser = JSON.createParser(res.getBody());
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                        // Move to the value.
                        parser.nextToken();
                       
                        if (!Test.isRunningTest()) {
                            Cache.Org.put('local.Ingest10x.GoogleIngestPubSubToken', parser.getText(), 1800); //ttl  1/2 hour (can be an hour - but this give good margin for error)
                        } /*else {
                            Cache.Org.put('local.Ingest10x.GoogleIngestPubSubToken', parser.getText(), 60); //ttl just off of a minute
                        }*/
                       
                        return parser.getText();
                    }
                }
            } else {
                throw new GooglePubSubException('HTTP error response in get_access_token. Status code ' + res.getStatusCode());
            }
           
        }
        return 'error';
    }
 
    @future(callout=true)
    public static void RaiseQueueEvents (String[] ids, String sObjectName, String[] ignoreFields, String topicName) {
        // Perform long-running code
        List<String> processedRecords = new List<String>();

        String pubSub_Call_URL = PubSub_URL; 

        if (String.isNotBlank(topicName)) {
                  String strReplace = INGEST10X + topicName; 
                 pubSub_Call_URL = pubSub_Call_URL.replace(INGEST10X, strReplace);
        } 
        ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();
        logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseQueueEvents', 
                                    'Method input ' + ids.size() + ' records: ' + String.join(ids, ','));

        try {
            Decimal maxHeapSize = Limits.getLimitHeapSize()*0.9;
            Integer pubSubSize = 0;
            List<String> payloads = new List<String>();
            List<Id> recIds = new List<Id>();
 
            for(sObject obj : Database.Query('SELECT ' + HerokuConnectQueueHelper.fetchFieldNames(sObjectName,ignoreFields) + ' FROM ' + sObjectName + ' WHERE Id IN :ids ALL ROWS'))
            {
                String newItem = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize((obj))));
                pubSubSize += newItem.length();
               
                payloads.add(newItem); //Serialize and create Base64String of Object
                recIds.add(obj.Id);
                newItem = ''; //Blank it   
                if(Limits.getHeapSize()>=maxHeapSize || pubSubSize >= 9500000 || payloads.size()>999) { //To prevent occurances of Heap overrun or PubSub 10Mb Limit or PubSub 1000 item limit
                    GooglePubSubHelper.makeCallout(payloads, pubSub_Call_URL);
                    logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseQueueEvents', 
                            'Pub-subbed ' + recIds.size() + ' records: ' + recIds);
                    payloads.clear();
                    recIds.clear();
                    pubSubSize = 0;
                }
                processedRecords.add(String.valueOf(obj.Id));
            }
            if (payloads.size() > 0) {
                GooglePubSubHelper.makeCallout(payloads, pubSub_Call_URL);
                logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseQueueEvents', 
                            'Pub-subbed ' + recIds.size() + ' records: ' + String.join(recIds, ','));
            }
          
        } catch (Exception e) {
            // Here to ensure failure doesn't affect trigger flows
            Set<String> idSet = new Set<String>(ids);
            idSet.removeAll(processedRecords);
            logSack.AddException('GCPSync/PubSub/Exception', 'GooglePubSubHelper', 'RaiseQueueEvents', e);
            PubSubBatchHandler.insertDataExteractionRecordOnFailedPubSubCalls(idSet, sObjectName);
        } finally {
            logSack.Write();
        }
    }

     public static void RaiseQueueEventsWithTopicName( List<sObject> objs, String topicName) {
         System.debug('Alex-RaiseQueueEventsWithTopicName : ' + objs);
         String pubSub_Call_URL = PubSub_URL; 
         ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();
         List<Id> recIds = new List<Id>();

         try {
            if (String.isNotBlank(topicName)) {
                String strReplace = INGEST10X + topicName; 
                pubSub_Call_URL = pubSub_Call_URL.replace(INGEST10X, strReplace);
            } 

            Decimal maxHeapSize = Limits.getLimitHeapSize()*0.9;
            Integer pubSubSize = 0;
            List<String> payloads = new List<String>();
         
            for(Sobject obj : objs){
                String newItem = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize(obj)));
                pubSubSize += newItem.length();
                recIds.add(obj.Id);
               
                payloads.add(newItem); //Serialize and create Base64String of Object
                newItem = ''; //Blank it   
                if(Limits.getHeapSize()>=maxHeapSize || pubSubSize >= 9500000 || payloads.size()>999) { //To prevent occurances of Heap overrun or PubSub 10Mb Limit or PubSub 1000 item limit
                    GooglePubSubHelper.makeCallout(payloads, pubSub_Call_URL);
                    logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseQueueEventsWithTopicName', 
                            'Pub-subbed ' + recIds.size() + ' records: ' + recIds);
                    payloads.clear();
                    recIds.clear();
                    pubSubSize = 0;
                } 
         	}
           /*
            while ( objs.size() > 0) {
                String newItem = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize((objs.remove(0)))));
                pubSubSize += newItem.length();
               
                payloads.add(newItem); //Serialize and create Base64String of Object
                newItem = ''; //Blank it   
                if(Limits.getHeapSize()>=maxHeapSize || pubSubSize >= 9500000 || payloads.size()>999) { //To prevent occurances of Heap overrun or PubSub 10Mb Limit or PubSub 1000 item limit
                    GooglePubSubHelper.makeCallout(payloads, pubSub_Call_URL);
                    payloads.clear();
                    pubSubSize = 0;
                }
            }
         */
            if (payloads.size() > 0) {
                GooglePubSubHelper.makeCallout(payloads, pubSub_Call_URL);
                logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseQueueEventsWithTopicName', 
                            'Pub-subbed ' + recIds.size() + ' records: ' + String.join(recIds, ','));
            }
         } catch (Exception ex) {
            logSack.AddException('GCPSync/PubSub/Exception', 'GooglePubSubHelper', 'RaiseQueueEventsWithTopicName', ex);
         } finally {
            logSack.Write();
        }
    }

    public static void RaiseQueueEvents( List<sObject> objs) {
        Decimal maxHeapSize = Limits.getLimitHeapSize()*0.9;
        Integer pubSubSize = 0;
        List<String> payloads = new List<String>();
        ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();
        List<Id> recIds = new List<Id>();
           
        try {
            while ( objs.size() > 0) {
                recIds.add(objs.get(0).Id);
                String newItem = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize((objs.remove(0)))));
                pubSubSize += newItem.length();
               
                payloads.add(newItem); //Serialize and create Base64String of Object
                newItem = ''; //Blank it   
                if(Limits.getHeapSize()>=maxHeapSize || pubSubSize >= 9500000 || payloads.size()>999) { //To prevent occurances of Heap overrun or PubSub 10Mb Limit or PubSub 1000 item limit
                    GooglePubSubHelper.makeCallout(payloads, PubSub_URL);
                    logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseQueueEvents', 
                            'Pub-subbed ' + recIds.size() + ' records: ' + recIds);
                    payloads.clear();
                    recIds.clear();
                    pubSubSize = 0;
                }
            }
            if (payloads.size() > 0) {
                GooglePubSubHelper.makeCallout(payloads, PubSub_URL);
                logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseQueueEvents', 
                            'Pub-subbed ' + recIds.size() + ' records: ' + recIds);
            }
        } catch (Exception ex) {
            logSack.AddException('GCPSync/PubSub/Exception', 'GooglePubSubHelper', 'RaiseQueueEvents', ex);
        } finally {
            logSack.Write();
        }
    }

    public static void RaiseJsonQueueEventsWithTopicName(List<String> recordJsons, String topicName) {
        String pubSub_Call_URL = PubSub_URL; 
        ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();

        try {
            if (String.isNotBlank(topicName)) {
                String strReplace = INGEST10X + topicName; 
                pubSub_Call_URL = pubSub_Call_URL.replace(INGEST10X, strReplace);
            } 

            Decimal maxHeapSize = Limits.getLimitHeapSize()*0.9;
            Integer pubSubSize = 0;
            List<String> payloads = new List<String>();
         
            for (String recordJson : recordJsons) {
                String newItem = EncodingUtil.base64Encode(Blob.valueof(recordJson));
                pubSubSize += newItem.length();
               
                payloads.add(newItem); //Serialize and create Base64String of Object
                newItem = ''; //Blank it   
                if(Limits.getHeapSize()>=maxHeapSize || pubSubSize >= 9500000 || payloads.size()>999) { //To prevent occurances of Heap overrun or PubSub 10Mb Limit or PubSub 1000 item limit
                    GooglePubSubHelper.makeCallout(payloads, pubSub_Call_URL);
                    logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseJsonQueueEventsWithTopicName', 
                            'Pub-subbed ' + payloads.size() + 'records');
                    payloads.clear();
                    pubSubSize = 0;
                } 
         	}

            if (payloads.size() > 0) {
                GooglePubSubHelper.makeCallout(payloads, pubSub_Call_URL);
                logSack.AddInformation('GCPSync/PubSub', 'GooglePubSubHelper', 'RaiseJsonQueueEventsWithTopicName', 
                            'Pub-subbed ' + payloads.size() + 'records');
            }
        } catch (Exception ex) {
            logSack.AddException('GCPSync/PubSub/Exception', 'GooglePubSubHelper', 'RaiseJsonQueueEventsWithTopicName', ex);
        } finally {
            logSack.Write();
        }
    }
 
    private static void makeCallout(List<String> base64DataPayloads, String endpoint_raw) {
        try {
            String access_token = get_access_token();
            Set<Object> messages = new Set<Object>();
            Map<String, Object> payload_json = new Map<String, Object>();
            Map<String, String> orgMap = new Map<String, String>();
            orgMap.put('orgId', UserInfo.getOrganizationId());
        
            for(Integer i = 0; i<base64DataPayloads.size(); i++) {
                Map<String, Object> data = new Map<String, Object>();
                data.put('data', base64DataPayloads[i]);
                data.put('attributes', orgMap);
                messages.add(data);
            }
            payload_json.put('messages', messages);
            String endpoint = endpoint_raw + access_token;
            HttpRequest request = new HttpRequest();
            request.setEndPoint(endpoint);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setTimeout(10000);
            request.setBody(JSON.serialize(payload_json));
        
            if (!Test.isRunningTest()) {
                HttpResponse response = new HTTP().send(request);
                ConnectedLog.LogInformation(
                    response.getStatusCode() == 200 ? 'GCPSync/PubSub/Success' : 'GCPSync/PubSub/HTTPError', 
                    'GooglePubSubHelper', 
                    'makeCallout', 
                    response.getStatusCode() == 200 ? 'Pub/sub callout successful' : 'Pub/sub callout error',
                    new Map<String, String> {
                        'RecordCount' => String.valueOf(messages.size()),
                        'ResponseCode' => String.valueOf(response.getStatusCode()),
                        'Object' => 'N/A'
                    });
            }

        } catch (Exception ex) {
            ConnectedLog.LogException('GCPSync/PubSub/Exception', 'GooglePubSubHelper', 'makeCallout', ex);
        } 
    }

    @future(callout=true)
    public static void makeCalloutAsync(String payload, String endpoint_raw) {
        GooglePubSubHelper.makeCallout(payload, endpoint_raw);
    }

    @future(callout=true)
    @AuraEnabled
    public static void makeCalloutDirectCall(String payload) {
        System.debug('Alex-makeCalloutDirectCall : ' + payload);
        GooglePubSubHelper.makeCallout(payload, PubSub_URL);
    }
   
    private static void makeCallout(String payload, String endpoint_raw) {
        try {
            String access_token = get_access_token();
            Blob myBlob = Blob.valueof(payload);
            String payload64 = EncodingUtil.base64Encode(myBlob);
            Map<String, Object> payload_json = new Map<String, Object>();
            Set<Object> messages = new Set<Object>();
            Map<String, Object> data = new Map<String, Object>();
            Map<String, String> orgMap = new Map<String, String>();
            orgMap.put('orgId', UserInfo.getOrganizationId());
            data.put('data', payload64);
            data.put('attributes', orgMap);
            messages.add(data);
            payload_json.put('messages', messages);
           
            String endpoint = endpoint_raw + access_token;
            HttpRequest request = new HttpRequest();
            request.setEndPoint(endpoint);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setTimeout(10000);
            request.setBody(JSON.serialize(payload_json));
            if (!Test.isRunningTest()) {
                HttpResponse response = new HTTP().send(request);
                ConnectedLog.LogInformation(
                    response.getStatusCode() == 200 ? 'GCPSync/PubSub/Success' : 'GCPSync/PubSub/HTTPError', 
                    'GooglePubSubHelper', 
                    'makeCallout', 
                    response.getStatusCode() == 200 ? 'Pub/sub callout successful' : 'Pub/sub callout error',
                    new Map<String, String> {
                        'RecordCount' => '1',
                        'ResponseCode' => String.valueOf(response.getStatusCode()),
                        'Object' => 'User_Activity__c'
                    });
            }
        } catch (Exception ex) {
            ConnectedLog.LogException('GCPSync/PubSub/Exception', 'GooglePubSubHelper', 'makeCallout', ex);
        }
    } 
}