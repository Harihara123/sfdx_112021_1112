/**
This class was taken AS IS silver-labs\Ingest10x project except for the one line marked CARLCHANGED in comments
(need to talk to Neil to determine how we want to handle)
*/
public with sharing class GooglePubSubHelperNotes {
    public static String get_access_token(){
        
        if (Cache.Org.contains('local.Ingest10x.GoogleIngestPubSubToken')) {
            return (String)Cache.Org.get('local.Ingest10x.GoogleIngestPubSubToken');
        } else {
            
            //CARLCHANGED (should we use the approach of pushing the token to the org, or can this be done through a config somehow?)
            //String resToken = [SELECT token__c FROM IngestPubSub__mdt where label = 'Prod'].token__c;
            //needed to remove newlines from the JSON.. then "escaped" newlines in the base 64 encoded private_key.. but need to leave \n as a \\n after BEGIN PR.. because of code below:
            //String key = tokenMap.get('private_key').remove('-----BEGIN PRIVATE KEY-----\n').remove('\n-----END PRIVATE KEY-----\n');
            String resToken = '{   "type": "service_account",   "project_id": "moneypenny-224413",   "private_key_id": "509d48242bdde2c4d41231d1c38a9a237f750480",   "private_key": "-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDhIM9O4OwLuukW+xpBkunn2LOKVAhrCVc0Sw+Bvi9YjsppmuNBaIC79m8Xeofv6R3aDQEiH32iCQnRc2mFcVRgJxwp2BB+mDukC6oICWk/ax+Z82OWnWdLmVXVGj56h7F2kG+jxr9K2NggAbj6BvLPjRMJ56f/k7aRTwcgQ3M2CLF+BOu3+MzZTZHZ/rECAC3ZmC0EKXa8+GD4Ih+owrgw9MKLbTrn976pIpZqe9KBZyMQn6tNvBRCF1ZdlmKV0eFC4YMtIGqnK3ROmbL4CQD8xvbFUZU2boMvq64Ch9lrCaH0lPl+Ut753p3uzaQhlp/bDaiJl2l3vmvVJETg2rPpAgMBAAECggEALgffW2lx2hQlhKVjAPVhDh6o1yfHcNtAEVknmoegdqL7kAwzWzRa3hPNMyfCk7M+f08ZZDi8H11pvIG0maxKbD6S65taBAhsaWZZJbuG4YittXSmcC1O6Y44lpH/JF6KUovFx97jThJ4XlL712OEUhuuQOA04XT6Z0uZUWd5LxrFwZB8LbffdNjfdVhkG6fcelOynliMWDohOSwwSpqTLfJdpaiunkbVYAntiblYDcgIEPvPVzCuBNFKgKEwnOKoEytY4G+KO4usqeajqh1qssBv0OZk6Z35VWpEw+cItnqzGeMcQVUWi6hJOr/quGmk/ir8g5g+TEOjQdWVrHtw4QKBgQDzNMJXotSUOqqQwutG9RRgR6DzKj9+dNsYVB7w9sKpStgcw8xzN2/yPKgw0T8jVuFgDkuq9DWSMDGFWPh+oL7Muf9Aoa2ySgPvNM6O+QQcSmeCc0raeeeSvqkNF/sH1qbMRCMPeL4AH5YWSlRJDPlgRPncN977bLeJ4Y+SMeRC1wKBgQDs+JirCtL3a+OSUXZFcgXIVPsa8HkgKDNq0BQehI2gY3vyzv7/fpoRrg7YxFutCwlk/uRFrgsR76z5e5r2fLrzt27xxm9OiuRrxr2VsQ1cNg61QAaeTwqgmK3DVtaM2dx2O2grC4Q2uBIG5Y9pl/g8OjkxEKygFky83FVu8AynPwKBgQClN10ivaod/HCsoM3GSW2LLn3HUxcJKAdC0rqmRWveJ8sCV30HdEynSc+UQkHbFwVKBMwgmX/fDMBHbzZ4h2i2mVAurubZrHBZywzbkYzaTy1cMEhUfOX1CXK8Q2M0Yvyy3ULYGtUwLkT+ZYY2jgrG/HSkxovDT4qrr0CiNyRyxwKBgQCIwSpxwwxexz+MyEBF8XiO1SQYtuBgA3Mw2CZUOlD1V5RHWtfkIG7PdzCQPjld0rm4NDUOVVOEJVognAmg5/iH/7siAk9n91W9MPNl+MhqB7RTO0DWiWzro1DcIzgsDupgvQXvmXTSk0Wr/zzVRmN2dm9c9m9474WXlFwlapkSxwKBgHI8FZTn06HCah6IF/zIL/3oURry99YCODUrRkB7oEdyo89+lVmA4Fhpd5ZuLmb5cNylmzxarC88CCXyg/3u4TD9yYJtgmljmhHQHNT+Obh1PAR2XiFoRXeYTxpajWUG3d0T8coxBzGDH6T6PyiwTi3vPuh60Ub5DF61Jjx1F3+5\\n-----END PRIVATE KEY-----\\n",   "client_email": "867654015548-compute@developer.gserviceaccount.com",   "client_id": "117663235136179602911",   "auth_uri": "https://accounts.google.com/o/oauth2/auth",   "token_uri": "https://oauth2.googleapis.com/token",   "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",   "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/867654015548-compute%40developer.gserviceaccount.com" }';
            System.debug('resToken:' + resToken);
            JSONParser tokenParser = JSON.createParser(resToken);
            Map<String,String> tokenMap = new Map<String,String>();
            
            while (tokenParser.nextToken() != null) {
                if (tokenParser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String key = tokenParser.getText();
                    
                    tokenParser.nextToken();
                    tokenMap.put(key,tokenParser.getText());
                }
                system.debug(tokenMap);
                //tokenParser.nextToken();
                
            }
            
            resToken = '';
            tokenParser = null;
            System.debug('before access token call');
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            req.setEndpoint('https://accounts.google.com/o/oauth2/token');
            req.setMethod('POST');
            
            req.setHeader('ContentType','application/x-www-form-urlencoded');
            
            String header = '{"alg":"RS256","typ":"JWT"}';
            String header_encoded = EncodingUtil.base64Encode(blob.valueof(header));
            
            String claim_set = '{"iss":"' +tokenMap.get('client_email') +'"';
            claim_set += ',"scope":"https://www.googleapis.com/auth/pubsub"';
            claim_set += ',"aud":"https://accounts.google.com/o/oauth2/token"';
            claim_set += ',"exp":"' + datetime.now().addHours(1).getTime()/1000;
            claim_set += '","iat":"' + datetime.now().getTime()/1000 + '"}';
            System.debug('claim_set + ' + claim_set);
            String claim_set_encoded = EncodingUtil.base64Encode(blob.valueof(claim_set));
            
            String signature_encoded = header_encoded + '.' + claim_set_encoded;
            system.debug(tokenMap);
            String key = tokenMap.get('private_key').remove('-----BEGIN PRIVATE KEY-----\n').remove('\n-----END PRIVATE KEY-----\n');
            
            blob private_key = EncodingUtil.base64Decode(key);
            signature_encoded = signature_encoded.replaceAll('=','');
            String signature_encoded_url = EncodingUtil.urlEncode(signature_encoded,'UTF-8');
            blob signature_blob =   blob.valueof(signature_encoded_url);
            
            String signature_blob_string = EncodingUtil.base64Encode(Crypto.sign('RSA-SHA256', signature_blob, private_key));
            
            String JWT = signature_encoded + '.' + signature_blob_string;
            
            JWT = JWT.replaceAll('=','');
            
            String grant_string= 'urn:ietf:params:oauth:grant-type:jwt-bearer';
            req.setBody('grant_type=' + EncodingUtil.urlEncode(grant_string, 'UTF-8') + '&assertion=' + EncodingUtil.urlEncode(JWT, 'UTF-8'));
            res = h.send(req);
            String response_debug = res.getBody() +' '+ res.getStatusCode();
            System.debug('Response =' + response_debug );
            if(res.getStatusCode() == 200) {
                JSONParser parser = JSON.createParser(res.getBody());
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                        // Move to the value.
                        parser.nextToken();
                        
                        if (!Test.isRunningTest()) {
                            Cache.Org.put('local.Ingest10x.GoogleIngestPubSubToken', parser.getText(), 1800); //ttl  1/2 hour (can be an hour - but this give good margin for error)
                        } 
                        
                        return parser.getText();
                    }
                }
            }
            
        }
        return 'error';
    }

    /*@future(callout=true)
    public static void RaiseQueueEvents (Id[] ids, String sObjectName, String[] ignoreFields) {
        // Perform long-running code
        try {
            Decimal maxHeapSize = Limits.getLimitHeapSize()*0.9;
            Integer pubSubSize = 0;
            List<String> payloads = new List<String>();

            for(sObject obj : Database.Query('SELECT ' + HerokuConnectQueueHelper.fetchFieldNames(sObjectName,ignoreFields) + ' FROM ' + sObjectName + ' WHERE Id IN :ids'))
            {
                String newItem = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize((obj))));
                pubSubSize += newItem.length();
                
                payloads.add(newItem); //Serialize and create Base64String of Object
                newItem = ''; //Blank it    
                if(Limits.getHeapSize()>=maxHeapSize || pubSubSize >= 9500000 || payloads.size()>999) { //To prevent occurances of Heap overrun or PubSub 10Mb Limit or PubSub 1000 item limit
                    GooglePubSubHelperNotes.makeCallout(payloads,'https://pubsub.googleapis.com/v1/projects/connected-ingest-test/topics/ingest10x:publish?access_token=');
                    
                    payloads.clear();
                    pubSubSize = 0;
                }
            }
            if (payloads.size() > 0) {
                GooglePubSubHelperNotes.makeCallout(payloads,'https://pubsub.googleapis.com/v1/projects/connected-ingest-test/topics/ingest10x:publish?access_token=');
            }
        } catch (Exception e) {
            // Here to ensure failure doesn't affect trigger flows
            System.debug(e);
        }
    }*/
    
    public static void RaiseQueueEvents( List<sObject> objs) {
        try {
            
            Decimal maxHeapSize = Limits.getLimitHeapSize()*0.9;
            Integer pubSubSize = 0;
            
            List<String> payloads = new List<String>();
            
            while ( objs.size() > 0) {
                String newItem = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize((objs.remove(0)))));
                pubSubSize += newItem.length();
                
                payloads.add(newItem); //Serialize and create Base64String of Object
                newItem = ''; //Blank it    
                if(Limits.getHeapSize()>=maxHeapSize || pubSubSize >= 9500000 || payloads.size()>999) { //To prevent occurances of Heap overrun or PubSub 10Mb Limit or PubSub 1000 item limit
                    
                    GooglePubSubHelperNotes.makeCallout(payloads,'https://pubsub.googleapis.com/v1/projects/connected-ingest-test/topics/ingest10x:publish?access_token=');
                    
                    payloads.clear();
                    pubSubSize = 0;
                }
                
            }
            if (payloads.size() > 0) {
                GooglePubSubHelperNotes.makeCallout(payloads,'https://pubsub.googleapis.com/v1/projects/connected-ingest-test/topics/ingest10x:publish?access_token=');
            }
        } catch (Exception e){
            // Here to ensure failure doesn't affect trigger flows
            System.debug(e);
        }
        
        
        
    }
    
    private static void makeCallout(List<String> base64DataPayloads, String endpoint_raw) {
        String access_token = get_access_token();
        
        
        Set<Object> messages = new Set<Object>();
        Map<String, Object> payload_json = new Map<String, Object>();
        
        for(Integer i = 0; i<base64DataPayloads.size(); i++) {
            Map<String, String> data = new Map<String, String>();
            data.put('data', base64DataPayloads[i]);
            messages.add(data);
        }
        
        payload_json.put('messages', messages);
        
        
        String endpoint = endpoint_raw + access_token;
        HttpRequest request = new HttpRequest();
        request.setEndPoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setTimeout(10000);
        request.setBody(JSON.serialize(payload_json));
        
        if (!Test.isRunningTest()) {
            HttpResponse response = new HTTP().send(request);
        }
    }
    
    @future(callout=true)
    public static void makeCalloutAsync(String payload, String endpoint_raw) {
        GooglePubSubHelperNotes.makeCallout(payload, endpoint_raw);
    }
    
    private static void makeCallout(String payload, String endpoint_raw) {
        String access_token = get_access_token();        
        System.debug('access_token:' + access_token);
        Blob myBlob = Blob.valueof(payload);
        String payload64 = EncodingUtil.base64Encode(myBlob);
        
        Map<String, Object> payload_json = new Map<String, Object>();
        Set<Object> messages = new Set<Object>();
        Map<String, String> data = new Map<String, String>();
        
        data.put('data', payload64);
        messages.add(data);
        payload_json.put('messages', messages);
        
        String endpoint = endpoint_raw + access_token;
        HttpRequest request = new HttpRequest();
        request.setEndPoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setTimeout(10000);
        request.setBody(JSON.serialize(payload_json));
        if (!Test.isRunningTest()) {
            HttpResponse response = new HTTP().send(request);
        }
    }   
}