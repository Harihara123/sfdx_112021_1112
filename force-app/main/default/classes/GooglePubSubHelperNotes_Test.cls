@IsTest
public class GooglePubSubHelperNotes_Test {
    
    
    @isTest(OnInstall=true) static void calloutTest() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockGooglePubSubCallout());
        GooglePubSubHelperNotes.makeCalloutAsync('{"xyz":"abc"}','https://pubsub.googleapis.com/v1/projects/connected-ingest'); 
        Test.stopTest();
        
        System.assertEquals(1, 1);
    }
    
    static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    } 
    
    @isTest(OnInstall=true) static void validateRaiseQueueEventswithTopic() {
        
        Integer listSize = 10;
        List<Account>  accounts = new List<Account>();
        String testString = generateRandomString(32000);
        
        for (Integer i = 0;i<listSize;i++){
            Account a = new Account(Name='Acme Inc', Description=testString);
            accounts.add(a);
        }
        insert accounts;  
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockGooglePubSubCallout());
        GooglePubSubHelperNotes.RaiseQueueEvents(accounts);
        Test.stopTest();
        System.assertEquals(1, 1);
    }
    
}