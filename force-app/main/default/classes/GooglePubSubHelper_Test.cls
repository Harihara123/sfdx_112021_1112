@IsTest
public class GooglePubSubHelper_Test {
	
    @TestSetup
    public static void testDataSetup() {
        Connected_Data_Export_Switch__c serviceSetting2 = new Connected_Data_Export_Switch__c(Name='DataExport',
                                                                                                   PubSub_URL__c='Https://www.google.com');
        insert serviceSetting2;
        
        Integer listSize = 200;
        List<Account>  accounts = new List<Account>();
    	 String testString = generateRandomString(32000);

       for (Integer i = 0;i<listSize;i++){
           Account a = new Account(Name='Acme Inc', Description=testString);
           accounts.add(a);
       }
        insert accounts;     
    }
    static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
    while (randStr.length() < len) {
       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
       randStr += chars.substring(idx, idx+1);
    }
    return randStr; 
    }

   @isTest(OnInstall=true) static void validateRaiseQueueEvents_List() {
	   List<Account> accounts = new List<Account>();
	   //accounts =[select id,name,Description from Account limit 200];
       List<String> ignoreFields = new List<String>();
       ignoreFields.add('Site');
       Test.startTest();
       Test_PubSubBatchHandler.stageData(true); 
       Test.setMock(HttpCalloutMock.class, new MockGooglePubSubCallout());

       GooglePubSubHelper.RaiseQueueEvents(accounts);
       System.assertEquals(1, 1);
       Test.stopTest();
    }

   @isTest(OnInstall=true) static void validateRaiseQueueEvents_ASync() {
       Test.startTest();

       Test_PubSubBatchHandler.stageData(true);
       Integer listSize = 200;

       String objectName = 'Account';

       List<Account>  accounts = new List<Account>();
       String testString = generateRandomString(32000);

       for (Integer i = 0;i<listSize;i++){
           Account a = new Account(Name='Acme Inc', Description = testString);
           accounts.add(a);
       }

       Insert accounts;

       List<STRING> ids = new List<STRING>();
       for(Account account : [SELECT Id FROM Account Limit 200]){
                ids.add(STRING.valueOf(account.Id));
       }

       List<String> ignoreFields = new List<String>();
       ignoreFields.add('Site');

       GooglePubSubHelper.RaiseQueueEvents(ids,objectName, ignoreFields,'');
       Test.stopTest();
       
       System.assertEquals(1, 1); 
    }
    
    @isTest(OnInstall=true) static void validateRaiseQueueEventswithTopic() {
       Connected_Data_Export_Switch__c serviceSetting2 = new Connected_Data_Export_Switch__c(Name='DataExport',
                                                                                                   PubSub_URL__c='Https://www.google.com');
        insert serviceSetting2;
       Set<STRING> ids = new Set<STRING>();
       for(Account account : [SELECT Id FROM Account Limit 200]){
                ids.add(STRING.valueOf(account.Id));
       }
       System.debug('SIZE::'+ids.Size()); 
       List<String> ignoreFields = new List<String>();
       ignoreFields.add('Site');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockGooglePubSubCallout());
        CDCDataExportUtility.getRecords(ids,'Account', ignoreFields);
        Test.stopTest();
    }
    
     @isTest(OnInstall=true) static void coverOtherMethods(){
       
         Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockGooglePubSubCallout());
         GooglePubSubHelper.makeCalloutAsync('{"Id":"123456"}','https://www.google.com');
         GooglePubSubHelper.makeCalloutDirectCall('{"Id":"123456"}');
        Test.stopTest();
    }

}