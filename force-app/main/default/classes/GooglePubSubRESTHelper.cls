@RestResource(urlMapping='/GooglePubSubRESTHelper/*')
global with sharing class GooglePubSubRESTHelper {
    private class PubSubRESTJSON {
        public Id[] ids;
        public String sObjectName;
        public String[] ignoreFields;
        public String source;
        public String topicName; 
    }
        
    @HttpPost
    global static void doPost(){     
        String body = RestContext.request.requestBody.toString();
        system.debug(body);
        if(body != null){ 
            body = JSON.serializePretty(JSON.deserializeUntyped(body));
            PubSubRESTJSON req = (PubSubRESTJSON)JSON.deserialize(body, PubSubRESTJSON.class);
            
            if(req.ids != null && req.sObjectName != null) {
                if(req.ids.size() > 0) {
                    GooglePubSubHelper.RaiseQueueEvents(req.ids, req.sObjectName, req.ignoreFields, req.topicName);
                }
            }              
        }
    }
}