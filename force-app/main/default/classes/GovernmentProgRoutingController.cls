// in  controller
public with sharing class GovernmentProgRoutingController{
    public GovernmentProgRoutingController(ApexPages.StandardController controller){
    }
    public pagereference getRouter(){
        User usr = [Select Id,Office__c,OPCO__c FROM User Where Id=: userinfo.getuserid()];
        String Opco_Mapping;
        OPCO_Picklist_Mapping__c OPCO_Mapping_Setting = OPCO_Picklist_Mapping__c.getValues(usr.OPCO__c);    
        if(OPCO_Mapping_Setting <> Null)
        Opco_Mapping = OPCO_Mapping_Setting.Picklist_Value__c;
        
        //String chosenRT = ApexPages.CurrentPage().GetParameters().Get('RecordType');
        String Url ='/a0c/e?retURL=%2Fa0c%2Fo&00NU00000035KA4='+Opco_Mapping+ '&nooverride=1';
    
        PageReference p ;   
        p = new PageReference(Url);
        return p;
    }
}