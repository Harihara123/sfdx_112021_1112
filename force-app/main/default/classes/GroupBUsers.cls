/**
 * To execute the batch, use following anonymous apex. 
 * Sample will execute for every user.
 * GroupBUsers bt = new GroupBUsers();
 * Database.executeBatch(bt);
 * 
 * Sample will execute for specific users.
 * List<String> userid=new List<String>();
 * userid.add('0051p00000AWLrCAAX');
 * GroupBUsers bt = new GroupBUsers(userid);
 * Database.executeBatch(bt);
 */

global class GroupBUsers implements Database.Batchable<sObject>, Database.Stateful {
	
    private String UserLog;
    
    global GroupBUsers() {
        UserLog='';        
    }
    
    global GroupBUsers(List<String> userId) {
        // this.whereClause = whereClause;
        UserLog=' and User__c IN :userId';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        list<String> UserId=new list<String>();        
        //User ids to be added
		//UserId.add('0051p00000AWLrCAAX'); 
        UserId.add(UserInfo.getUserId());   
        
        List<String> SearchType=new List<String>{'AllLastSearches','AllSavedSearches','SharedByMe','SharedWithMe'};
        return Database.getQueryLocator(
            'SELECT ID, LastModifiedDate, Search_Params__c,Search_Params_ESR2C__c,Search_Params_Continued_ESR2C__c,Search_Params_Continued__c, ' +
            'Type__c FROM User_Search_Log__c WHERE Type__c in :SearchType'+UserLog);
    }
    global void execute(Database.BatchableContext bc, List<User_Search_Log__c> scope){
        system.debug('***********scope*****'+scope);
        List<User_Search_Log__c> EngineUpdateList = new List<User_Search_Log__c>();
        if(scope.size() > 0){
        	for(User_Search_Log__c SearchLog: scope){
            	//if(SearchLog.Search_Params__c != null ){
            	if(SearchLog.Search_Params__c != null 
                   && (!(SearchLog.Search_Params_Continued__c != null && SearchLog.Search_Params__c.length() < 131071))) {
                    String SearchParams; 
                    if (SearchLog.Search_Params_Continued__c == null) {
            			SearchParams=SearchLog.Search_Params__c;
        			} else {
            			Integer len = SearchLog.Search_Params_Continued__c.length();
            			SearchParams=SearchLog.Search_Params__c.substring(1, 131071) + SearchLog.Search_Params_Continued__c.substring(1, len-1);
        			}
					Map<String, Object> deserializeSearch = (Map<String, Object>) JSON.deserializeUntyped(SearchParams);
                	List<Object> deserializeSearchList = (List<Object>) deserializeSearch.get('searches');
					List<Object> outputSearchList = new List<Object>();
                    Boolean EngineUpdated=false;
                    for(Object obj1:deserializeSearchList){
						Map<String, Object> deserializeType = (Map<String, Object>) JSON.deserializeUntyped(obj1.toString());
						if(deserializeType.get('engine') == null || deserializeType.get('engine') == '') {
                            //if(deserializeType.get('engine')!= 'ES' && deserializeType.get('engine')!='EAP'){
                                deserializeType.put('engine','ES');
                            	outputSearchList.add(JSON.serialize(deserializeType));
                            	EngineUpdated = true;
                                system.debug('********engine found with empty data*****'+deserializeType);
                            //}
                        } else {
                            outputSearchList.add(obj1);
                        }                        					
                    }
                    if(EngineUpdated){
                        Map<String, Object> outMap = new Map<String, Object>();
						outMap.put('searches', outputSearchList);
						String R2TRemovedString = JSON.serialize(outMap);
                        if (R2TRemovedString.length() > 131072) {
            				SearchLog.Search_Params__c = '"' + R2TRemovedString.substring(0, 131070) + '"';
            				SearchLog.Search_Params_Continued__c = '"' + R2TRemovedString.substring(131070) + '"';
        				} else {
            				SearchLog.Search_Params__c = R2TRemovedString;
            				SearchLog.Search_Params_Continued__c = null;
        				}
                        EngineUpdateList.add(SearchLog);
                        system.debug('**********EngineUpdateList*****'+EngineUpdateList);
                    }                                    
                }
            }
        }
        if (EngineUpdateList.size() > 0) {
            try{
                //Update EngineUpdateList;
                Database.SaveResult[] lsr = Database.update(EngineUpdateList, false);
                system.debug('***********After update*******'+lsr);
            }catch(Exception excp){
                system.debug('***********Exception*******'+excp);
            }                
        } 
    }
    global void finish(Database.BatchableContext BC) {
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, CompletedDate FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Group B saved/recent JSON updates ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    } 
}