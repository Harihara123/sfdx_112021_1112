global class HRXMLBatch implements Database.Batchable<sObject> {
    Set<id>Accids = new Set<Id>();
    String query;
    boolean useV2 = false;
    boolean isCandidateStatusBatch = false;
    boolean isSubmittalBatch = false;


    global HRXMLBatch(String q) {
        if (String.isBlank(q)) {
            query = 'SELECT Account__c from Generic_HRXML__c WHERE RecordType.Name = \'Talent\'';
        } else {
            this.query = q;
        }
    }

    global HRXMLBatch(String q, boolean useV2) {
        this.useV2 = useV2;
        if (String.isBlank(q)) {
            query = 'SELECT Account__c from Generic_HRXML__c WHERE RecordType.Name = \'Talent\'';
        } else {
            this.query = q;
        }
    }

    global HRXMLBatch(String q, boolean useV2, boolean isCandidateStatusBatch) {
        this.useV2 = useV2;
        this.isCandidateStatusBatch = isCandidateStatusBatch;
        if (String.isBlank(q)) {
            query = 'SELECT Account__c from Generic_HRXML__c WHERE RecordType.Name = \'Talent\'';
        } else {
            this.query = q;
 		    }
    }

    global HRXMLBatch(Set<ID> idSet, String q) {
        Accids = idSet;
        if (String.isBlank(q)) {
            query = 'SELECT Account__c from Generic_HRXML__c WHERE RecordType.Name = \'Talent\'';
        } else {
            this.query = q +' :AccIds';
        }
    }

    global HRXMLBatch(Set<ID> idSet, String q, boolean isSubmittalBatch) {
        system.debug('!!2222!!!' + idSet);
        this.isSubmittalBatch = isSubmittalBatch;
        Accids = idSet;
        if (String.isBlank(q)) {
            query = 'SELECT Account__c from Generic_HRXML__c WHERE RecordType.Name = \'Talent\'';
        } else {
            this.query = q +' :AccIds';
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('Query>>>>>>>' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Generic_HRXML__c> scope) {
        system.debug ('with in execute >>>>>>>>>>>>>>>' + scope);
        List<Id> acctIds = new List<Id>();
        for (Generic_HRXML__c acct : scope){
            acctIds.add(acct.account__c);
        }
        if (!useV2) {
            system.debug ('with in v1');
            HRXMLService.updateHRXML(acctIds);
        } 
    }

    global void finish(Database.BatchableContext BC){
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, CompletedDate FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('HRXML Updates ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

      System.debug('isCandidateStatusBatch>>>>>>>>>>>' + isCandidateStatusBatch);
      System.debug('isSubmittalBatch>>>>>>>>>>>' + isSubmittalBatch);

      if (isCandidateStatusBatch) {
          HRXML_Email_Service__c candStatusBatchCustomSetting = HRXML_Email_Service__c.getvalues('HRXMLCandidateStatusBatchLastRun');
          candStatusBatchCustomSetting.Last_Executed_Batch_Timestamp__c = a.CompletedDate;
          update candStatusBatchCustomSetting;
          System.debug('Email service called with after batch date >>>' + candStatusBatchCustomSetting.Last_Executed_Batch_Timestamp__c);
       } else if (isSubmittalBatch) {
          HRXML_Email_Service__c submittalBatchCustomSetting = HRXML_Email_Service__c.getvalues('HRXMLSubmittalBatchLastRun');
          submittalBatchCustomSetting.Last_Executed_Batch_Timestamp__c = a.CompletedDate;
          update submittalBatchCustomSetting;
          System.debug('Email service called with after batch date >>>' + submittalBatchCustomSetting.Last_Executed_Batch_Timestamp__c);
       }

    }
}