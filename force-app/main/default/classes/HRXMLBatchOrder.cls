global class HRXMLBatchOrder implements Database.Batchable<sObject>, Database.stateful {
    String query;
    global Set<ID> talentAcctsForOrder = new Set<Id>();
    global Boolean jobExceptionFlag = false;


    global HRXMLBatchOrder(String q) {
        this.query = q;
    }    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug ('with in start >>>>>>>>>>>>>>>' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Order> scope) {
        system.debug ('with in execute >>>>>>>>>>>>>>>' + scope);
         if(scope.size()>0)
        {
            try{
                for(Order order : scope){
                   talentAcctsForOrder.add(order.ShiptoContact.accountId);
                }
                system.debug('TalentAccts::'+talentAcctsForOrder);
            }
            catch(Exception e){
                jobExceptionFlag = True;
            }
        }
    }

    global void finish(Database.BatchableContext BC){
        system.debug ('with in finish >>>>>>>>>>>>>>>' + talentAcctsForOrder);
        
        if(!jobExceptionFlag){

	        //Trigger HRXML batch
	        String query = 'SELECT Id FROM Account WHERE ID IN';
	        
	        if(talentAcctsForOrder != null && talentAcctsForOrder.size() > 0){
	            HRXMLBatch hBatch = new HRXMLBatch(talentAcctsForOrder, query, true);
	            Database.executeBatch(hBatch, 100);
	        }
	    }    
    }
}