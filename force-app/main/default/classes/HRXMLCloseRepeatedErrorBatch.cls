/*****************************************************************************************************************
@author : Neel Kamal
@Date : 07/30/2018
@Description : This batch class fetch those records that error out again and change status to failed.
*****************************************************************************************************************/
public class HRXMLCloseRepeatedErrorBatch  implements Database.Batchable<sObject>{
	Datetime retryBatchStartTime;
	public HRXMLCloseRepeatedErrorBatch(Datetime retryBatchStartTime){
		this.retryBatchStartTime = retryBatchStartTime;
	}
	public Database.QueryLocator start(Database.BatchableContext bc) {
		if(Test.isRunningTest()) {
			return Database.getQueryLocator([select id, status__c from Talent_Retry__c where status__c='Open']);
		}
		else {
			return Database.getQueryLocator([select id, status__c from Talent_Retry__c where status__c='Open' and CreatedDate >=: retryBatchStartTime]);
		}
		
	}

	public void execute(Database.BatchableContext bc, List<Talent_Retry__c> scope) {
		for(Talent_Retry__c talRetry : scope) {
			talRetry.Status__c = 'Failed';
		}

		update scope;
	}

	public void finish(Database.BatchableContext bc) {
	}
}