@isTest
public class HRXMLCloseRepeatedErrorBatch_Test  {
	static testmethod void closeRepeatedErrorBatchTest() {
		Account testAccount = CreateTalentTestData.createTalent();
		
		Talent_Retry__c tr = new Talent_Retry__c(Action_Name__c='HRXML', Action_Record_Id__c=testAccount.id, Status__c='Open');
		insert tr;

		Test.startTest();
			HRXMLCloseRepeatedErrorBatch batch = new HRXMLCloseRepeatedErrorBatch(System.now());
			Database.executeBatch(batch, 200);


		Test.stopTest();
	}
}