global class HRXMLEmailService implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		String query = null; 
        String batchLastRun;
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try{
	        System.debug('Email service called with Subject >>>' + email.subject);
	        System.debug('Label.DATA_MIGRATION_USER >>>' + System.Label.DATA_MIGRATION_USER);
			List<User> users = [select id from user where alias = :Label.DATA_MIGRATION_USER limit 1];
			System.debug('users >>>' + users[0].Id);
	        
	        if (email.subject.contains('CANDIDATE_STATUS')) {
		        HRXML_Email_Service__c candStatusBatchCustomSetting = HRXML_Email_Service__c.getvalues('HRXMLCandidateStatusBatchLastRun');
	            System.debug('Email service for Candidate Status called with before batch date >>>' + candStatusBatchCustomSetting.Last_Executed_Batch_Timestamp__c);
	 			batchLastRun = candStatusBatchCustomSetting.Last_Executed_Batch_Timestamp__c.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
	 			query = 'SELECT Id FROM Account WHERE LastModifiedById =\'' + users[0].Id + '\' and LastModifiedDate >= ' + batchLastRun;
		        HRXMLBatch hrxmlBatch = new HRXMLBatch(query, false, true);
		    	Database.executeBatch(hrxmlBatch, 100);
		    } else {
	            HRXML_Email_Service__c submittalBatchCustomSetting = HRXML_Email_Service__c.getvalues('HRXMLSubmittalBatchLastRun');
	            System.debug('Email service for Submittal called with before batch date >>>' + submittalBatchCustomSetting.Last_Executed_Batch_Timestamp__c);
	 			batchLastRun = submittalBatchCustomSetting.Last_Executed_Batch_Timestamp__c.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
				query = 'SELECT ShipToContact.AccountId FROM Order WHERE LastModifiedById =\'' + users[0].Id + '\' and LastModifiedDate >= ' + batchLastRun;
		        HRXMLBatchOrder hrxmlBatchOrder = new HRXMLBatchOrder(query);
		        Database.executeBatch(hrxmlBatchOrder, 100);
		    }    
		    
    		result.success = true;
		} catch (Exception ex) {
			result.success = false;
			result.message = 'Error Occured, Email Processing Failed!!!';
            System.debug(logginglevel.Error,'Error Occured >>>' + ex.getMessage());
 		}    
	    return result;
    }

  }