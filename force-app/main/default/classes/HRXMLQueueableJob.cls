 public class HRXMLQueueableJob implements Queueable, Database.AllowsCallouts {

     private Map<Id,Opportunity> opportunityMap {get; set;}
     private boolean isGeolocationRequired {get; set;}

      public HRXMLQueueableJob(Map<Id,Opportunity> oppMap, boolean isGeolocationRequired)
       {
         this.opportunityMap = oppMap;
         this.isGeolocationRequired = isGeolocationRequired;
       }

      public void execute(QueueableContext context){ 
          HRXMLUtil hr = new HRXMLUtil(opportunityMap.keyset());
          hr.getGeoLocationFromAPI();
          hr.updateOpportunitiesWithHRXML();
      }

}