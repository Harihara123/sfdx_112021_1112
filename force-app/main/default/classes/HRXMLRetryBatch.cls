/*****************************************************************************************************************
@author : Neel Kamal
@Date : 07/30/2018
@Description : Functionality is if any HRXML failed then retry one more time. 
			   This batch fetch records from Talent_retry__c object and call HRXML service and try one more time.
			   Retried reord may chances of fail again so need to update those record so that this batch won't fetch that record again.
			   Another batch called from Finish method to update record with status 'Failed'.
*****************************************************************************************************************/
public class HRXMLRetryBatch implements Database.Batchable<sObject>, Schedulable{
	String query; //Used batch Query
	Boolean useV2 = false; // If anytime need to HRXML Service V2
	Integer lastNDays = 1; //Though batch will by default run daily but It is just flexibility to run batch any number of days
	Datetime batchStartTime; //Start date and time will be store and will pass into batch in finish method.

	public HRXMLRetryBatch() {
		this.lastNDays = 1;
		batchStartTime = System.now();
	}

	public HRXMLRetryBatch(Boolean useV2, Integer  lastNDays) {
		this.useV2 = useV2;
		this.lastNDays = lastNDays;
		batchStartTime = System.now();
	}

	public HRXMLRetryBatch(Integer  lastNDays) {
		this.lastNDays = lastNDays;
		batchStartTime = System.now();
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		if(Test.isRunningTest()) {
			query = 'select id, Action_Name__c, Action_Record_Id__c, Status__c from Talent_Retry__c where Status__c=\'Open\' and Action_Name__c=\'HRXML\'';
		}	    
		else {
			query = 'select id, Action_Name__c, Action_Record_Id__c, Status__c from Talent_Retry__c where Status__c=\'Open\' and Action_Name__c=\'HRXML\' and CreatedDate = LAST_N_DAYS:'+lastNDays;
		}
	    
		System.debug('Query ---'+query);
		return  Database.getQueryLocator(query);
	} 

	public void execute(Database.BatchableContext bCtx, List<Talent_Retry__c> talentRetryList) {
		List<Id> talentretryIDList = new List<Id>();
        for (Talent_Retry__c talentRetry : talentRetryList){
            talentretryIDList.add(talentRetry.Action_Record_Id__c);
			talentRetry.Status__c = 'Success';
        }
        if (!useV2) {
            system.debug ('with in v1');
            HRXMLService.updateHRXML(talentretryIDList);
        }

		update talentRetryList;
	}

	public void finish(Database.BatchableContext bc) {
		HRXMLCloseRepeatedErrorBatch batch = new HRXMLCloseRepeatedErrorBatch(batchStartTime);
		Database.executeBatch(batch, 200);
	}

	public void execute(SchedulableContext sc) {
		HRXMLRetryBatch batch = new HRXMLRetryBatch();
		Database.executeBatch(batch, 200);
	}

}