@isTest
public class HRXMLRetryBatch_Test  {
	static testmethod void retryTest() {
		Account testAccount = CreateTalentTestData.createTalent();
		
		Talent_Retry__c tr = new Talent_Retry__c(Action_Name__c='HRXML', Action_Record_Id__c=testAccount.id, Status__c='Open');
		insert tr;

		Test.startTest();
			HRXMLRetryBatch batch = new HRXMLRetryBatch();
			Database.executeBatch(batch, 200);

			HRXMLRetryBatch batch1 = new HRXMLRetryBatch(true, 0);
			Database.executeBatch(batch1, 200);

			HRXMLRetryBatch batch2 = new HRXMLRetryBatch(0);
			Database.executeBatch(batch2, 200);

			//List<Talent_Retry__c> trList = [select id, Status__c from Talent_Retry__c where Action_Record_Id__c =:testAccount.id];

			//System.assertEquals(trList.get(0).Status__c, 'Success');

		Test.stopTest();
	}
}