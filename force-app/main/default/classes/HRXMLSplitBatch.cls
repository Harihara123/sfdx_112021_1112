/**
 * @author: Ajay Panachickal, 17-Nov-2016, User story: ATS-2672
 * 
 * Splits the input query into 10 sets and starts 10 HRXMLBatch jobs.
 * Input parameters are the query and required size of each batch.
 * Batch size defaults to 50 if not provided.
 */
global class HRXMLSplitBatch {
    
    public static void run(String q, Integer batchSize, boolean useV2) {
        Integer sz = (batchSize != null && batchSize > 0)? batchSize : 50;
        if (!String.isBlank(q)) {
            String splitter = q.containsIgnoreCase(' where ')? ' AND Talent_Id__c LIKE ' : ' WHERE Talent_Id__c LIKE ';
            HRXMLBatch bt;
            for (Integer i = 0; i < 10; i++) {
                bt = new HRXMLBatch(q + splitter + '\'%' + i + '\'', useV2, false);
                Database.executeBatch(bt, sz);
                System.debug(LoggingLevel.DEBUG, 'Batch #' + (i+1) + ' initiated ... Size = ' + sz);
                System.debug(LoggingLevel.DEBUG, 'Query: ' + q + splitter + '\'%' + i + '\'');
            }
		} 
    }

}