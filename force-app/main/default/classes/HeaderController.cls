//Hareesh-Implemented continuation method to do the Asynch call outs-sprint2
public with sharing class HeaderController{
    public boolean callOutMade{get;set;}
    public boolean FromAccount{get;set;}
    public boolean FromContact{get;set;}
    public boolean TekUser{get;set;}
    public Account accObj;
    /***** Private Variables *****/
    @testVisible
    private String SiebelId;
    Private List<string> ContactSiebelID;
    Private String OPCO;
    Private String EmployeeID;
    Private String GlobalAccountID;

    /***** Visualforce Properties *****/
    public boolean calloutfault { get; set; } 
    public String exceptionMessage { get;set; }
    public String exceptionDescription { get;set; }
    public HeaderWrapper listHeaderPage { get; set; } 
List<WS_Header_Service.getSFDCHeaderResponseType> Header = new List<WS_Header_Service.getSFDCHeaderResponseType>(); 

    
    
    //=============================================================================================
    //Standard Controller Method(Constructor)
    //=============================================================================================
    public HeaderController(ApexPages.StandardController controller) {
        System.debug('------I am here in constructor');
        callOutMade =FALSE;
        FromAccount =FALSE;
        FromContact =FALSE;
        TekUser =FALSE;
        ContactSiebelId = new List<string>();
        Map<String, String> UrlParameterMap = ApexPages.currentPage().getParameters();        
        // Call made from Contact
        if(ApexPages.currentPage().getUrl().contains('Contact')){
            FromContact= TRUE;            
            Contact con = (Contact) controller.getRecord();
            ContactSiebelId.add(con.id);
            list<Contact> tempList = [select  Siebel_ID__c, Account.Siebel_ID__c from Contact where id =: con.id];
            SiebelId = tempList[0].Account.Siebel_ID__c;
            if(tempList.size()>0 && tempList[0].Siebel_ID__c <> NULL){
                ContactSiebelId.add(tempList[0].Siebel_ID__c);
            }           
            
        }
        
        //Call made from Account 
        if(ApexPages.currentPage().getUrl().contains('Account')){
            FromAccount =TRUE;
            Account acc = (Account) controller.getRecord();
            accObj = acc ;
            list<account> tempList = [select  Siebel_ID__c from Account where id =: acc.id];
            if(tempList.size()>0)
                SiebelId = tempList[0].Siebel_ID__c;        
        }               
        
       // Logged in user details  
       User u = [select Id, OPCO__c, Peoplesoft_Id__c from User where Id = :UserInfo.getUserId()];
       EmployeeID = u.Peoplesoft_Id__c;
       OPCO = u.OPCO__c;
       
       if(OPCO == Label.TEK_OPCO){
       TekUser =TRUE;
       }
       
       System.debug('**************TekUser ' +TekUser);
       System.debug('**************OPCO ' +OPCO);
       System.debug('**************EmployeeID ' +EmployeeID);
       System.debug('**************ContactSiebelId ' +ContactSiebelId);
       System.debug('**************SiebelId ' +SiebelId); 
    }
    
    public void HeaderInformation(){         
        System.debug('---in Action method---');
        calloutfault = false;
        callOutMade = true;   
        listHeaderPage = new HeaderWrapper();        
        listHeaderPage = getHeader(SiebelId, OPCO, EmployeeID, ContactSiebelID);
        
    }
    //==================================================================================================
    //                  Data Access
    //==================================================================================================
    
    
    //Wrapper class for Header
    
    public class HeaderWrapper  {        
        public Decimal FillRatio{get;set;}
        public String WentDirectClient{get;set;}
        public Decimal ProducerSpread{get;set;}
        public Decimal OpCoSpread{get;set;}
        public Decimal GlobalAccountSpread{get;set;}       
    }

    // This method calls the Tibco Webservice to fetch the Header Information.    
    public HeaderWrapper getHeader(String ReqID, String OPCO, String EmployeeID, List<string>ContactSiebelID){
        HeaderWrapper lstReturn = new HeaderWrapper();
        List<WS_Header_Service.getSFDCHeaderResponseType> Header = new List<WS_Header_Service.getSFDCHeaderResponseType>();
          
                   try{                    
                    if(Test.isRunningTest()) {    
                        WS_Header_Service.getSFDCHeaderResponseRoot_element RespRoot= new WS_Header_Service.getSFDCHeaderResponseRoot_element();
                        WS_Header_Service.getSFDCHeaderRequestType ReqType = new WS_Header_Service.getSFDCHeaderRequestType();
                        WS_Header_Service.Fault_element fltType = new WS_Header_Service.Fault_element();
                        WS_Header_Service.getSFDCHeaderResponseType Head = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head.FillRatio= '12345';
                        Head.WentDirectClient= '123451234';
                        Head.ProducerSpread= '123';
                        Head.OpCoSpread= '12354';
                        Head.GlobalAccountSpread = '12358';
                        Header.add(Head);
                        WS_Header_Service.getSFDCHeaderResponseType Head1 = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head1.FillRatio= '12345';
                        Head1.WentDirectClient= '123451234';
                        Head1.ProducerSpread= '123';
                        Head1.OpCoSpread= '12354';
                        Head1.GlobalAccountSpread = '12358';
                        Header.add(Head1); 
                        WS_Header_Service.getSFDCHeaderResponseType Head2 = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head2.FillRatio= '1234-5';
                        Header.add(Head2); 
                    }
                    else{
                        WS_Header_ServiceType.GetSFDCHeaderEndpoint1 WSHeader = new WS_Header_ServiceType.GetSFDCHeaderEndpoint1 ();                        
                        WSHeader.timeout_x = Integer.valueof(System.Label.SyncWebServiceTimeout);
                        String GlobalAccountID;
                    
                        try {
                                
                                  Header = WSHeader.getHeaders(ReqID,GlobalAccountID,OPCO ,EmployeeID,ContactSiebelID);
                        }
                        catch(CalloutException ex) {                    
                            exceptionDescription = ex.getMessage();
                        }
                    }
                 if(Header <> Null){                  
                    HeaderWrapper obj;
                    System.debug('==>Header'+Header);
                    for(WS_Header_Service.getSFDCHeaderResponseType respRecord : Header){
                        obj = new HeaderWrapper();
                        if(respRecord.FillRatio <> NULL && respRecord.FillRatio <> ''){
                        system.debug('*******************************fillratio**' +respRecord.FillRatio); 
                        obj.FillRatio = (decimal.valueOf(respRecord.FillRatio)*100).setScale(2);
                        }                        
                        obj.WentDirectClient = respRecord.WentDirectClient;
                        if(respRecord.ProducerSpread <> NULL && respRecord.ProducerSpread <> ''){
                        obj.ProducerSpread = (decimal.valueOf(respRecord.ProducerSpread)).setScale(0);
                        }if(respRecord.OpCoSpread <> NULL && respRecord.OpCoSpread <> ''){
                        obj.OpCoSpread = (decimal.valueOf(respRecord.OpCoSpread)).setScale(0);
                        }if(respRecord.GlobalAccountSpread <> NULL && respRecord.GlobalAccountSpread <> ''){
                        obj.GlobalAccountSpread = (decimal.valueOf(respRecord.GlobalAccountSpread)).setScale(0); 
                        }                       
                        lstReturn = obj;                        
                    }
                 }
            } catch(CalloutException ex) {                
                exceptionDescription = ex.getMessage();
            }catch(Exception ex)  {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, ex.getMessage()));
                exceptionDescription = ex.getMessage();
                system.debug('***: '+exceptionDescription);
            }
            return lstReturn;
    }
    
    //Start woraround for Asynch callouts
    
    AsyncWS_Header_Service.getSFDCHeaderResponseRoot_elementFuture WS_Resp_toProcess = new AsyncWS_Header_Service.getSFDCHeaderResponseRoot_elementFuture(); 
        public Continuation startRequest() {
            System.debug('!!!!!!!!!!!!!In request method!!!!!');
            Continuation con = new Continuation(60);
            con.continuationMethod='processResponse';
            calloutfault = false;
             callOutMade = true;
             
             
             
             if(Test.isRunningTest()) {    
                        WS_Header_Service.getSFDCHeaderResponseRoot_element RespRoot= new WS_Header_Service.getSFDCHeaderResponseRoot_element();
                        WS_Header_Service.getSFDCHeaderRequestType ReqType = new WS_Header_Service.getSFDCHeaderRequestType();
                        WS_Header_Service.Fault_element fltType = new WS_Header_Service.Fault_element();
                        WS_Header_Service.getSFDCHeaderResponseType Head = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head.FillRatio= '12345';
                        Head.WentDirectClient= '123451234';
                        Head.ProducerSpread= '123';
                        Head.OpCoSpread= '12354';
                        Head.GlobalAccountSpread = '12358';
                        Header.add(Head);
                        WS_Header_Service.getSFDCHeaderResponseType Head1 = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head1.FillRatio= '12345';
                        Head1.WentDirectClient= '123451234';
                        Head1.ProducerSpread= '123';
                        Head1.OpCoSpread= '12354';
                        Head1.GlobalAccountSpread = '12358';
                        Header.add(Head1); 
                        WS_Header_Service.getSFDCHeaderResponseType Head2 = new WS_Header_Service.getSFDCHeaderResponseType();
                        Head2.FillRatio= '1234-5';
                        Header.add(Head2); 
                        processresponse();
                    }
                    
                    else{
             
              AsyncWS_Header_ServiceType.AsyncGetSFDCHeaderEndpoint1 WSHeader = new  AsyncWS_Header_ServiceType.AsyncGetSFDCHeaderEndpoint1();                    
                        //WSHeader.timeout_x = 119999;
                        String GlobalAccountID;
                    
                        try {
                        system.debug('I am in the try blog');
                                
                        WS_Resp_toProcess = WSHeader.beginGetHeaders(con,SiebelId,GlobalAccountID,OPCO ,EmployeeID,ContactSiebelID);
                         system.debug('Result'+WS_Resp_toProcess);
                        }
                        catch(CalloutException ex) {                    
                            exceptionDescription = ex.getMessage();
                          //  system.debug('exception'+exceptionDescription);
                        }
                        }
                        return con;
                        
                        }
                        
       Public Object processResponse(){
        
        HeaderWrapper lstReturn = new HeaderWrapper();
        listHeaderPage = new HeaderWrapper();
        try{
         if(!Test.isRunningTest()) {
        Header = WS_Resp_toProcess.getvalue();
        }
        
        system.debug('Response is here'+Header);
       } catch(CalloutException ex){          
                exceptionDescription = ex.getMessage();
            if(exceptionDescription.containsIgnoreCase('SOAP Fault') || exceptionDescription.containsIgnoreCase('IO Exception')){
                System.debug('!!!!!Callout Exception!!!!!!!! '+exceptionDescription);
             }else{
                 HeaderInformation();  
             }                
        }
       
      
            
            
        try{
        if(Header <> Null){                  
                    HeaderWrapper obj;
                    System.debug('==>Header'+Header);
                    for(WS_Header_Service.getSFDCHeaderResponseType respRecord : Header){
                        obj = new HeaderWrapper();
                        if(respRecord.FillRatio <> NULL && respRecord.FillRatio <> ''){
                        system.debug('*******************************fillratio**' +respRecord.FillRatio); 
                        obj.FillRatio = (decimal.valueOf(respRecord.FillRatio)*100).setScale(2);
                        }                        
                        obj.WentDirectClient = respRecord.WentDirectClient;
                        if(respRecord.ProducerSpread <> NULL && respRecord.ProducerSpread <> ''){
                        obj.ProducerSpread = (decimal.valueOf(respRecord.ProducerSpread)).setScale(0);
                        }if(respRecord.OpCoSpread <> NULL && respRecord.OpCoSpread <> ''){
                        obj.OpCoSpread = (decimal.valueOf(respRecord.OpCoSpread)).setScale(0);
                        }if(respRecord.GlobalAccountSpread <> NULL && respRecord.GlobalAccountSpread <> ''){
                        obj.GlobalAccountSpread = (decimal.valueOf(respRecord.GlobalAccountSpread)).setScale(0); 
                        }                       
                        listHeaderPage = obj;                        
                    }
                 }
            } 
            catch(Exception ex)  {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, ex.getMessage()));
                exceptionDescription = ex.getMessage();
                system.debug('***: '+exceptionDescription);
            }
            
            return null;
    }
            
//End workaround for Asynch call outs
}