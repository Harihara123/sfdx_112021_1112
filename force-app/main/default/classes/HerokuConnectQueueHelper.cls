/***************************************************************************
Name        : HerokuConnectQueueHelper
Created By  : Ncains
Date        : 24 Oct 2018
Purpose     : Standard Class to help create Heroku Queue platform events
*****************************************************************************/

public without sharing class HerokuConnectQueueHelper {
	static String orgId = UserInfo.getOrganizationId();
    public static String fetchFieldNames(String sObjectName, List<String> ignoreFields){
        List<String> lcaseIF = new List<String>();
        for(String s : ignoreFields){
            lcaseIF.add(s.toLowerCase());
        }
        ignoreFields = null; 
        
        String SobjectApiName = sObjectName;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(!lcaseIF.contains(fieldName)){
                if(commaSepratedFields == null || commaSepratedFields == ''){
                    commaSepratedFields = fieldName;
                }else{
                    commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                }
            }
        }
        return commaSepratedFields;
    }
    public static String fetchFieldNamesCS(String sObjectName, List<String> ignoreFields){
        List<String> lcaseIF = new List<String>();
        for(String s : ignoreFields){
            lcaseIF.add(s.toLowerCase());
        }
        ignoreFields = null; 
        
        String commaSepratedFields = '';
        for(Schema.SObjectField fld: Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().values()){
            String fieldName = fld.getDescribe().getName();
            if (!lcaseIF.contains(fieldName.toLowerCase())) {
                if(commaSepratedFields == null || commaSepratedFields == ''){
                    commaSepratedFields = fieldName;
                }else{
                    commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                }
            }
        }
        return commaSepratedFields;
    }  
    private static HerokuConnectQueueItem__c CreateQueueItem(sObject obj) { 
        List <String> dataOut = new List <String>();
        Integer dataCounter = 1;
        Integer dataFieldLength = 131072; //Data Field Maximum Size
        Boolean Data_Truncated_Error = false;
        String Data = JSON.serialize(obj,true);
        for (Integer i = 1; i*dataFieldLength <= Math.ceil(double.valueOf(Data.length())/double.valueOf(dataFieldLength)) * dataFieldLength; i++) {
            
            if (((i-1)*dataFieldLength) + dataFieldLength < Data.length()-1 ) {
                dataOut.add(Data.substring((i-1)*dataFieldLength,i*dataFieldLength));
            } else {
                dataOut.add(Data.substring((i-1)*dataFieldLength));
            }
        } 
        
        //Size Issue - need to raise error, but we still log what we can -> 
        //If a regular occurance adjust event to accomodate more Data 
        //fields, consumers would need to be updated accordingly - 8Fields is ~1024k of data 
        
        if (dataOut.size() > 8) {     System.debug('RaiseHerokuConnectQueueEventWithData: Data Size more than 8 fields---');
                                 Data_Truncated_Error = true;
                                }
        
        String data1 = '';
        String data2 = '';
        String data3 = '';
        String data4 = '';
        String data5 = '';
        String data6 = '';
        String data7 = '';
        String data8 = '';
        
        Integer j = 0;
        while (j< dataOut.size() && j < 8){
            if (j==0) {
                data1 = dataOut[j];
            } else if (j ==1) {
                data2 = dataOut[j];
            }else if (j ==2) {
                data3 = dataOut[j];
            }else if (j ==3) {
                data4 = dataOut[j];
            }else if (j ==4) {
                data5 = dataOut[j];
            }else if (j ==5) {
                data6 = dataOut[j];
            }else if (j ==6) {
                data7 = dataOut[j];
            }else if (j ==7) {
                data8 = dataOut[j];
            }
            j=j+1;
        }
        
        
        
        HerokuConnectQueueItem__c qItem = New HerokuConnectQueueItem__c();
        return qItem; 
    }
    @future 
    public static void RaiseQueueEvents (Id[] ids, String sObjectName, String[] ignoreFields) {
        // Perform long-running code
        try {
            List<sObject> rows = Database.query('SELECT ' + HerokuConnectQueueHelper.fetchFieldNames(sObjectName,ignoreFields) + ' FROM ' + sObjectName + ' WHERE Id IN :ids');
            RaiseQueueEvents(rows);
        } catch (Exception e) {
            // Here to ensure failure doesn't affect trigger flows
            System.debug(e);
        }
    }
    public static void RaiseQueueEvents( List<sObject> objs) {
        try { 
            Decimal maxHeapSize = Limits.getLimitHeapSize()*0.9;
            List<HerokuConnectQueueItem__c> items = new List<HerokuConnectQueueItem__c>();
            while ( objs.size() > 0) {
                items.add(HerokuConnectQueueHelper.CreateQueueItem(objs.remove(0)));
                if(Limits.getHeapSize()>=maxHeapSize) { //To prevent occurances of Heap overrun
                    insert items;
                    items.clear();
                }
            }
            insert items;
        } catch (Exception e){
            // Here to ensure failure doesn't affect trigger flows
            System.debug(e);
        }
    }
    public static void RaiseQueueEvents( sObject obj) {
        try {
            insert HerokuConnectQueueHelper.CreateQueueItem(obj);
        } catch (Exception e){
            // Here to ensure failure doesn't affect trigger flows
            System.debug(e);
        }
    }
}