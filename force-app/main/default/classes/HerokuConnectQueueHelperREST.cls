@RestResource(urlMapping='/HerokuConnectQueueHelper/*')
global without sharing class HerokuConnectQueueHelperREST {
    public without sharing class Request {
        public List<String> sObjectNames {get;set;}
        public List<String> ignoreFields {get;set;}
    }
    public without sharing class Result {
        public Map<String, String> fields {get;set;}
    }

    @HttpPost
    global static void doPost(){
        RestResponse response = RestContext.response;
       	try{
            String body = RestContext.request.requestBody.toString();
    
            if(body != null){ 
                body = JSON.serializePretty(JSON.deserializeUntyped(body));
            }
            HerokuConnectQueueHelperREST.Request req = (HerokuConnectQueueHelperREST.Request)JSON.deserialize(body, HerokuConnectQueueHelperREST.Request.class);
            HerokuConnectQueueHelperREST.Result result = new HerokuConnectQueueHelperREST.Result();
            
            List<String> ignoreFields = req.ignoreFields;
            if (ignoreFields == null) {
                ignoreFields = new List<String>();
            }
            result.fields = new Map<String, String>();
            for(String sObjectName : req.sObjectNames){
                String output = HerokuConnectQueueHelper.fetchFieldNamesCS(sObjectName, ignoreFields);
                result.fields.put(sObjectName, output);
            }
                    
            response.statusCode = 200;
            response.addHeader('Content-Type', 'application/json');
            response.responseBody = Blob.valueOf(JSON.serialize(result));
        } catch(Exception e){
            response.statusCode = 500;
            response.responseBody = Blob.valueOf(e.getMessage()+'\n'+e.getStackTraceString());
        }
    }
}