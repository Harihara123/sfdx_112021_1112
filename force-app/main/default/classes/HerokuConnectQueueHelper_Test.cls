@isTest
public class HerokuConnectQueueHelper_Test  {
    @isTest(SeeAllData=true)
    static void testfetchFieldNames() {
        String output = HerokuConnectQueueHelper.fetchFieldNames('Event', new List<String>{'HRXML'});
		System.assertNotEquals(0, output.length());
    }
    @isTest(SeeAllData=true)
    static void testfetchFieldNamesCS() {
        String output = HerokuConnectQueueHelper.fetchFieldNamesCS('Event', new List<String>{'HRXML'});
		System.assertNotEquals(0, output.length());
    }
    @isTest(SeeAllData=true)
    static void testRaiseQueueEvents() {
        List<Account> a = [SELECT Id From Account LIMIT 10];
       	HerokuConnectQueueHelper.RaiseQueueEvents(a);
		System.assertEquals(1, 1);
    }
    @isTest(SeeAllData=true)
    static void testRaiseQueueEventsSingleItem() {
        Account a = [SELECT Id From Account LIMIT 1];
       	HerokuConnectQueueHelper.RaiseQueueEvents(a);
		System.assertEquals(1, 1);
    }
    @isTest(SeeAllData=true)
    static void testRaiseQueueEventsIds() {
        Account a = [SELECT Id From Account LIMIT 1];
        Id[] ids = new Id[]{a.Id};
        HerokuConnectQueueHelper.RaiseQueueEvents(ids, 'Account', new List<String>{'HRXML'});
		System.assertEquals(1, 1);
    }
    // Perform Tests for the REST Service
    @isTest(SeeAllData=true)
    static void testRESTPost() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/HerokuConnectQueueHelper';
        req.httpMethod = 'POST';
        
        HerokuConnectQueueHelperREST.Request creq = new HerokuConnectQueueHelperREST.Request();
        creq.sObjectNames = new List<String>();
        creq.sObjectNames.add('Event');
        creq.ignoreFields = new List<String>();
        
        String rBody = JSON.serialize(creq);
        req.requestBody = Blob.valueOf(rBody);
        
        RestContext.request = req;
        RestContext.response= res;
        
        HerokuConnectQueueHelperREST.doPost();
        Integer r = res.statusCode;
        System.assertEquals(200, r);
    }
    @isTest(SeeAllData=true)
    static void testRESTPostNoIgnoreFields() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/HerokuConnectQueueHelper';
        req.httpMethod = 'POST';
        
        HerokuConnectQueueHelperREST.Request creq = new HerokuConnectQueueHelperREST.Request();
        creq.sObjectNames = new List<String>();
        creq.sObjectNames.add('Event');
        
        String rBody = JSON.serialize(creq);
        req.requestBody = Blob.valueOf(rBody);
        
        RestContext.request = req;
        RestContext.response= res;
        
        HerokuConnectQueueHelperREST.doPost();
        Integer r = res.statusCode;
        System.assertEquals(200, r);
    }
    @isTest(SeeAllData=true)
    static void testRESTPostErrorResponse() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/HerokuConnectQueueHelper';
        req.httpMethod = 'POST';
        
        HerokuConnectQueueHelperREST.Request creq = new HerokuConnectQueueHelperREST.Request();

        String rBody = JSON.serialize(null);
        req.requestBody = Blob.valueOf(rBody);
        
        RestContext.request = req;
        RestContext.response= res;
        
        HerokuConnectQueueHelperREST.doPost();
        Integer r = res.statusCode;
        System.assertEquals(500, r);
    }
}
