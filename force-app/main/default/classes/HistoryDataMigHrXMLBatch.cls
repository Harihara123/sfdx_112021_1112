// *********************************************************************
// Execute Anonynous code
// Make sure to run as Siebel Integration user
/*
Id batchJobId = Database.executeBatch(new HistoryDataMigHrXMLBatch(query));
*/
// ************************************************************************
global class HistoryDataMigHrXMLBatch implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
    Public string Queryopp;  
    
    global HistoryDataMigHrXMLBatch()
    {
         if(!Test.isRunningTest())
        {
           Queryopp =  System.Label.HIstoryDataMigHRXMLInsertQuery;
        }
        else
        {
            Queryopp =  'select id,Req_HRXML_Status_Flag__c from opportunity';
        }
    }
        
     global database.QueryLocator start(Database.BatchableContext BC)  
     {  
        //Create DataSet of Accounts to Batch
        return Database.getQueryLocator(Queryopp);
     } 

        
     global void execute(Database.BatchableContext BC, List<sObject> scope)
     {
       
         set<id> oppids=new set<id>();
       Map<Id, Opportunity> opportunityMap = new map<id,Opportunity>();
         
         
        List<Opportunity> lstQueriedopps = (List<Opportunity>)scope;
         for(Opportunity oplst:lstQueriedopps){
             oppids.add(oplst.id);
             opportunitymap.put(oplst.id,oplst);
             
                    }
         system.debug('oppmap'+opportunitymap);
         system.debug('keyset'+opportunityMap.keyset());
         
         /*
          HRXMLUtil hr = new HRXMLUtil(opportunityMap.keyset());
          hr.updateOpportunitiesWithHRXML();
         */
         
         HRXMLUtil hr = new HRXMLUtil(opportunityMap.keyset());
          hr.getGeoLocationFromAPI();
          hr.updateOpportunitiesWithHRXML();
        
     }             
        global void finish(Database.BatchableContext BC)
     {
         
          
     }
    
}