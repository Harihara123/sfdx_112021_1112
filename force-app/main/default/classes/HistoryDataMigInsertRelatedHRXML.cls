// *********************************************************************
// Execute Anonynous code
// Make sure to run as Siebel Integration user
/*

Id batchJobId = Database.executeBatch(new HistoryDataMigInsertRelatedHRXML());
*/
// ************************************************************************
global class HistoryDataMigInsertRelatedHRXML implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
    Public string Queryopp;  
    
    global HistoryDataMigInsertRelatedHRXML()
    {
        if(!Test.isRunningTest())
        {
           Queryopp =  System.Label.HIstoryDataMigHRXMLInsertQuery;
        }
        else
        {
            Queryopp =  'select id,Req_HRXML_Status_Flag__c from opportunity';
        }
    }
        
     global database.QueryLocator start(Database.BatchableContext BC)  
     {  
        //Create DataSet of Accounts to Batch
        return Database.getQueryLocator(Queryopp);
     } 

        
     global void execute(Database.BatchableContext BC, List<sObject> scope)
     {
       
        set<id> setOpportunityIds=new set<id>();
        List<Req_Hrxml__c> lstHrxml = new List<Req_Hrxml__c>();


        List<Opportunity> lstQueriedopps = (List<Opportunity>)scope;
        for(Opportunity op:lstQueriedopps)
        {
            setOpportunityIds.add(op.id);
        }
        
        system.debug('setOpportunityIds:::: ' + setOpportunityIds);
        
        List<Opportunity> lstOpportunity = [select id,(Select id, Req_Hrxml_Opportunity__c  from Hrxmls__r) from Opportunity where id =: setOpportunityIds];
        system.debug('lstOpportunity::::: ' + lstOpportunity );
        for(Opportunity opp : lstOpportunity)
        {
            System.debug('opp.Hrxmls__r::::' + opp.Hrxmls__r);
            System.debug('opp.Hrxmls__r.size()::::' + opp.Hrxmls__r.size());
            if(opp.Hrxmls__r == null || (opp.Hrxmls__r != null && opp.Hrxmls__r.size() == 0))
            {
                Req_Hrxml__c hrxml = new Req_Hrxml__c();
                hrxml.Req_Hrxml_Opportunity__c = opp.Id;
                lstHrxml.add(hrxml);
                
            }
        }
        
        
        System.debug('lstHrxml::::: ' + lstHrxml);
        if(lstHrxml != null && lstHrxml.size() > 0)
        {
            insert lstHrxml;
        }
        
     }             
        global void finish(Database.BatchableContext BC)
     {
          HistoryDataMigHrXMLBatch batch = new HistoryDataMigHrXMLBatch();
          Database.executeBatch(batch, 50);
          
     }
    
}