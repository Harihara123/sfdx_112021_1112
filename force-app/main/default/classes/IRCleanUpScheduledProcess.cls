/***************************************************************************************************************************************
* Name        - IRCleanUpScheduledProcess 
* Description - This class is used for scheduling the logic for cleaning up Integration Response records older than 30 days 
                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       1/22/2013             Created
* Pratz                       1/25/2013             Updated to make the logic for Clean up Batchable
*****************************************************************************************************************************************/
global class IRCleanUpScheduledProcess implements Schedulable
{
     
         global void execute(SchedulableContext ctx)
     {  
         //Adding logic for Batch [Pratz 01/25/2013]
         BatchIRCleanUp batchCleanUp = new BatchIRCleanUp();
         
         Date d;
         system.debug(d);
         
         batchCleanUp.query = 'SELECT Id, CreatedDate FROM Integration_Response__c WHERE CreatedDate < :d';
         system.debug(batchCleanUp.query);
         
         ID batchprocessid = Database.executeBatch(batchCleanUp,200); 
         System.debug('Returned batch process ID: ' + batchProcessId);
                             
     }
     
}