public with sharing class InsightLinkController{
    
    
    
    
    @AuraEnabled
    public static String geturlGlobalAccountPlans(string accId ,string ReportLabel, Boolean isEnterprise){
        String result= '';
        string globaleventsreportid='';
        system.debug('reportlabel'+ReportLabel);
        system.debug('accid'+accId);
        List<Account> accList = [Select id,Master_Global_AccountID_formula_field__c from account where  id = :accId ];
        
        String appURL = '';
        // changes for lightning URL critical updates                 
        String urlChangesOn = System.Label.CONNECTED_Summer18URLOn;
        String newUrl = System.Label.CONNECTED_Summer18URL;
        if(urlChangesOn != null && (urlChangesOn.equalsIgnoreCase('true'))){
           // Critical updates are not affected for reports.
           appURL = '/one/one.app#/sObject';
        }else{
           appURL = newUrl + '/sObject';
        }
        
        if (accList.size() >0){
            if(ReportLabel=='Global Account Plans'){
            
                result = appURL +'/00OU0000002rGKi/view?a:t=1487086515856&fv0='+accList[0].Master_Global_AccountID_formula_field__c;      
            }
            
            if(ReportLabel=='Global Account Events'){
                globaleventsreportid=Label.RPT_GlobalEvents;
            
                result = appURL +'/'+globaleventsreportid+'/view?a:t=1487086515856&fv0='+accList[0].Master_Global_AccountID_formula_field__c;      
            }
            
             if(ReportLabel=='Global Account Opportunities'){
                 globaleventsreportid=Label.RPT_ID_GLOBAL_OPPTY;
            
                result = appURL +'/'+globaleventsreportid+'/view?a:t=1487086515856&fv0='+accList[0].Master_Global_AccountID_formula_field__c;      
            }
            
            if(ReportLabel=='Global Account Reqs'){
                 globaleventsreportid=Label.RPT_ID_GLOBAL_REQ;
            
                result = appURL +'/'+globaleventsreportid+'/view?a:t=1487086515856&fv0='+accList[0].Master_Global_AccountID_formula_field__c;      
            }
            
             if(ReportLabel=='Associated Contacts'){
                 
                 result = appURL +'/00OU00000015ThLMAU/view?a:t=1488818621813&fv0='+ accList[0].id;    
                 
            
                 
                 
                
            }
            
             if(ReportLabel=='Started Candidates'){
                  globaleventsreportid=Label.RPT_ID_ACCOUNT_TALENTS;
                 if(isEnterprise){
                     globaleventsreportid = Label.RPT_ID_GLOBAL_TALENTS;
                     result = appURL +'/'+ globaleventsreportid +'/view?a:t=1492093787858&fv0='+accList[0].Master_Global_AccountID_formula_field__c; 
                 }else{
                     result = appURL +'/'+ globaleventsreportid +'/view?a:t=1492093787858&fv0='+accList[0].Id;  
                 }
                   
            }
            
            
             
            
            
            
        }
        return  result;
    }
}