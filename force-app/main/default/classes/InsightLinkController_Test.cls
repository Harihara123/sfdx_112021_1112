@isTest
public class InsightLinkController_Test {
    @isTest static void test_geturlGlobalAccountPlans(){
        Account account =  new Account(RecordTypeId = System.Label.CRM_REFERENCEACCOUNTRECTYPE,
                                    Name = 'TestBWAccount' + String.Valueof(Math.random()));
        Insert account;
        
        Boolean isEnterprise = false;
        
        List<String> reportLabels = new List<String>{'Global Account Plans','Global Account Events','Global Account Opportunities','Global Account Reqs','Associated Contacts','Started Candidates'};
            for(String reportLabel : reportLabels){
                String resp = InsightLinkController.geturlGlobalAccountPlans(account.id, reportLabel, isEnterprise);
                System.debug('test_geturlGlobalAccountPlans '+resp);
                System.assert(resp!='');
            } 
    }
    
}