public class IntegrationOpenPageController {
    
    public final static String SYMBOL_QUESTION = '?';
    public final static String SYMBOL_AMPERSAND = '&';
    public final static String SYMBOL_EQUALS = '=';
    public final static String SYMBOL_MERGEFIELD = '{!';
    public final static String SYMBOL_CLOSEMERGEFIELD = '}';
    public final static String BLANK = '';
    
    string targetUrl;
   //Map<string,Integration_Endpoints__c> endPoint = Integration_Endpoints__c.getAll();
    Map<string,string> queryStringParams = ApexPages.CurrentPage().getParameters();
     
    // constructor
    public IntegrationOpenPageController()
    {
       
       //get the queryString parameter for the type of search
       string searchType = queryStringParams.get('searchType');
       System.debug('&&&&&&'+searchType);
     
       if(searchType != Null)
       {
              Integration_URL_Pattern__c IntURLPattern  = Integration_URL_Pattern__c.getValues(searchType); 
              if(IntURLPattern != Null)
              {
                  //build the query dynamically
                  targetUrl = '';  //endPoint.get(Label.RWS_App_URL).Endpoint__c;
                  
                  string searchUrlInfo = IntURLPattern.QueryString__c;
                  System.debug('%%%%%searchUrlInfo'+searchUrlInfo);
                  string processedUrl;
                  // process the search parameters accordingly
                  if(searchUrlInfo != Null)
                  {
                        // get the webservice method name
                        processedUrl = searchUrlInfo.subString(0,searchUrlInfo.indexOf(SYMBOL_QUESTION));
                        // loop over the query string parameters
                        for(string searchParams : searchUrlInfo.subString(searchUrlInfo.indexOf(SYMBOL_QUESTION)).split(SYMBOL_AMPERSAND))
                        {
                                string placeHolder = searchParams.subString(searchParams.indexOf(SYMBOL_EQUALS) + 1);
                                string givenValue = placeHolder;
                                placeHolder = placeHolder.replace(SYMBOL_MERGEFIELD,BLANK);
                               
                                placeHolder = placeHolder.replace(SYMBOL_CLOSEMERGEFIELD,BLANK);
                                system.debug('placeHolder:'+placeHolder);
                                //replace place holderwith the value
                                if(placeHolder != Null && queryStringParams.get(placeHolder) != Null)
                                    placeHolder = queryStringParams.get(placeHolder);
                                else
                                    placeHolder = givenValue;    
                                    System.debug('===>placeHolder'+placeHolder);
                                    
                                processedUrl = processedUrl + searchParams.subString(0,searchParams.indexOf(SYMBOL_EQUALS) + 1) + EncodingUtil.urlEncode(placeHolder, 'UTF-8');
                                processedUrl += SYMBOL_AMPERSAND; 
                                System.debug('===>processedUrl'+processedUrl);
                        }
                  }
                  targetUrl += processedUrl;
                  targetUrl = targetUrl.substring(0,targetUrl.LastIndexof('&'));
                  
                  System.debug('==>targetUrl'+targetUrl);
              }
              else
              {
                    // display error message on the VF page
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.RWS_ContactSearch_Error));
              }
      }
       
    }
    // method for redirection
    public pageReference redirect()
    {
           pageReference pg;
           if(targetUrl != Null)
           {
               System.debug('===Redirect'+targetUrl);
               pg = new PageReference(targetUrl);
              // pg.setRedirect(true);
           }
           return pg;
    }
}