/***************************************************************************************************************************************
* Name        - IntegrationResponseOuter_Scheduled_Click 
* Description - Class used to invoke Batch_IdentifyOutOfSyncAccounts class in both contexts - On-demand and Automatic
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       11/15/2012             Created
* Dhruv                       12/30/2012             Modified the class to move the logic to a common class:Batch_IdentifyOutOfSyncAccounts
* Pratz                        1/29/2013             Clubbed the logic to execute jobs in ExecuteChainedJobs() method
*****************************************************************************************************************************************/



global class IntegrationResponseOuter_Scheduled_Click implements Schedulable
{
      
      global void  ExecuteChainedJobs()          
     {
         //Declare and initialise a string variable with the job name
         String strJobName = 'IdentifyOutOfSyncLastRunTime';
         
         //Get the ProcessIntegrationResponseRecords Custom Setting Record
         ProcessIntegrationResponseRecords__c mc = ProcessIntegrationResponseRecords__c.getValues(strJobName);
         
         //Store the Last time the Batch was Run in a variable
         DateTime dtLastBatchRunTime = mc.Start_Time__c; 
                 
         //Declare an object of the Batch Class Batch_IdentifyOutOfSyncAccounts
         Batch_IdentifyOutOfSyncAccounts IdentifyOutOfSyncAccountsBatch = new Batch_IdentifyOutOfSyncAccounts('Select Id, SystemModstamp, AccountServicesChange__c From Account',system.now());
         
         //Execute the BatchAccountIntegrationResponse Batch, with 20 records per batch
         //Performs Start(), Excecute() and Finish() methods
         Database.executeBatch(IdentifyOutOfSyncAccountsBatch,20); 
         
         //At the end of the above statement, it executes Req Batch
         //Chained jobs are being executed so that OutOfSyncAccounts integer value can be accessed in the Req Batch                           
     }
      
      
     //Call from Button Click : 'Identify Out of Sync'      
     webservice static void  execute()          
     {
          IntegrationResponseOuter_Scheduled_Click  sp = new IntegrationResponseOuter_Scheduled_Click ();
          sp.ExecuteChainedJobs();                        
     }
     
     //Call from Scheduled Job
     global void execute(SchedulableContext ctx)     
     {  
          IntegrationResponseOuter_Scheduled_Click  sp = new IntegrationResponseOuter_Scheduled_Click ();
          sp.ExecuteChainedJobs();                                        
     }
     
     
       
   
}