public with sharing class JobApplicationCase {
    
    public static Case c;
    private static ID accountId;
    
    @AuraEnabled
    public static String createJobApplicationCase(string fn, string ln, string ph, string email, string jobId, string source,boolean optin) {
        c = new Case();
       /* String OppName;
        c.SuppliedName = ln + ', ' + fn;
        c.SuppliedPhone = ph;
        c.SuppliedEmail = email;
        c.SuppliedCompany = jobId;
        c.Origin = 'Web';
        if(!String.isBlank(jobId) ){
            OppName = [Select Id,Name from Opportunity where id=:jobId ].Name;
        }
        c.Description = source+'\r\n'+'OptIn:'+String.valueOf(optin);
        c.Subject = 'Web Application of ' + c.SuppliedName + ' to ' + OppName;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('JobApplication').getRecordTypeId();
        
        ph = (ph == null || ph == '') ? '' : ph;
        email = (email == null || email == '') ? '' : email;
            
        try{ 
            insert (c);
        	checkJobApplicationDuplicate(fn, ln, email, ph, c.id, jobId);
            
        }catch(Exception e ){
            System.debug('error'+e.getMessage());
            if(c.id != null){
                c.Description__c = e.getMessage();
                c.IsVisibleInSelfService = true;
                update c;
            }
            
        }*/
        return c.id;
    }
 
    /*private static boolean checkJobApplicationDuplicate(String fn, String ln, String email, String ph, Id caseId, String jobId){
       /* boolean isContact = false;
        String fnameEscaped = String.escapeSingleQuotes(fn);
        String lnameEscaped = String.escapeSingleQuotes(ln);
        String emailEscaped = String.escapeSingleQuotes(email);
        String phoneNumberEscaped = String.escapeSingleQuotes(ph); 
     
        
       /* String soqlContactCheck = 'SELECT Id, FirstName, lastName, Email FROM Contact where '
            					  + 'FirstName LIKE \'%' + fnameEscaped + '%\' AND LastName LIKE \'%' + lnameEscaped + '%\' ' 
            					  + 'AND (Email = :' + emailEscaped 
            					  + 'OR Other_Email__c = :' +emailEscaped  
            					  + 'OR MobilePhone = :' +phoneNumberEscaped  
            					  + 'OR HomePhone = :' +phoneNumberEscaped  
            					  + 'OR OtherPhone = :' +phoneNumberEscaped +')'; **
        
         //system.debug('soqlContactCheck ' + soqlContactCheck);
        List<Contact> matchedContactList =[SELECT Id, FirstName, lastName, Email FROM Contact where 
            					           FirstName LIKE :fnameEscaped AND 
                                           LastName LIKE :lnameEscaped  AND (Email = : emailEscaped 
                                                                             OR Other_Email__c = :emailEscaped) 
                                                                             AND( MobilePhone = :phoneNumberEscaped 
                                                                             OR HomePhone = :phoneNumberEscaped
                                                                             OR OtherPhone = :phoneNumberEscaped )];// Database.query(soqlContactCheck);
       
        
        ID contactId;
        system.debug('************** Matched Candidate List Size: ' + matchedContactList.size() + ' *******************');
       
/*        if (matchedContactList.size() == 1){
            system.debug('ONLY 1 Conatct Exists! ---> creating ORDER');

            for(Contact cont : matchedContactList){
                contactId = cont.Id;
            }
			system.debug('************** Contact ID: ' + contactId + ' *****************');
            jobApplicationCreateOrder(jobId, contactId);
  
        } else **
        if (matchedContactList.size() > 0){
            system.debug(' DUPLICATE Contact(s) found ----- ' + matchedContactList.size() + ' Contacts exist!');
            isContact = true;
            String chatterMessage = 'Has been stopped due to a possible duplicate record found. Please review and remediate the case.';
            //sendChatterNotification(caseId, chatterMessage);
            case updateCaseForChatterNotification = new case(Id=caseId,Description__c = chatterMessage,IsVisibleInSelfService = true );
            update updateCaseForChatterNotification;
        } else {
            system.debug(' ** No Duplicate candidates found ** ');
            // create contact
        	Map<String, ID> contactMap = jobApplicationCreatTalentContact(fn, ln, email, ph);    
            contactId = contactMap.get('talentContactId');
 			jobApplicationCreateOrder(jobId, contactId);            
        }

        return isContact;
   }
    
     private static Map<String,ID> jobApplicationCreatTalentContact(String fname, String lname, String email, String phone){
        
        /* Map<String, ID> jobAppMap = new Map<String, ID>();
        
         try{
           
            Id talentAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
            Id talentContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();

            Account account = new Account(Name=fname + ' ' + lname, RecordTypeId=talentAccountId, talent_ownership__c='RPO');
			insert account;
        
        	accountId = account.ID;
			jobAppMap.put('talentAccountId', accountId);
			
            string Opt = string.valueOf(c.Description.right(c.Description.length()-c.Description.lastIndexOf(':')-1)); 
            Contact contact = new Contact(
                FirstName =fname,
                LastName = lname,
                MobilePhone	= phone,
                Email = email,
                AccountId = accountId,
                PrefCentre_Brand__c = 'AGS',
                PrefCentre_CapturedDate__c = System.now(),
                PrefCentre_Country__c = '',
                PrefCentre_CMSource__c = 'Lloyds Landing Page',
                PrefCentre_CapturedSource__c = c.Description.left(c.Description.lastIndexOf('\r\n')),
                RecordTypeId = talentContactId);
       		 insert contact;
             
             if(!String.isBlank(phone) && !String.isBlank(email)){
                 contact.PrefCentre_AGS_OptOut_Email__c = Opt;
                 contact.PrefCentre_AGS_OptOut_Mobile__c = Opt;
                 contact.PrefCentre_ContactMethodType__c = 'Phone';
                 contact.PrefCentre_ContactMethodValue__c = contact.MobilePhone;
                 contact.PrefCentre_Structured__c = PreferenceCentreClass.GetUpdatedStructuredJSON(contact,'OptIn',contact.PrefCentre_AGS_OptOut_Channels__c, contact.PrefCentre_AGS_OptOut_Email__c, contact.PrefCentre_AGS_OptOut_Mobile__c);
                 contact.PrefCentre_ContactMethodType__c = 'Email';
                 contact.PrefCentre_ContactMethodValue__c = contact.Email;
                 contact.PrefCentre_Structured__c = PreferenceCentreClass.GetUpdatedStructuredJSON(contact,'OptIn',contact.PrefCentre_AGS_OptOut_Channels__c, contact.PrefCentre_AGS_OptOut_Email__c, contact.PrefCentre_AGS_OptOut_Mobile__c);
                  
             }
             else if(!String.isBlank(phone) && String.isBlank(email)){
                 contact.PrefCentre_AGS_OptOut_Email__c = '';
                 contact.PrefCentre_AGS_OptOut_Mobile__c = Opt;
                 contact.PrefCentre_ContactMethodType__c = 'Phone';
                 contact.PrefCentre_ContactMethodValue__c = contact.MobilePhone;
                 contact.PrefCentre_Structured__c = PreferenceCentreClass.GetUpdatedStructuredJSON(contact,'OptIn',contact.PrefCentre_AGS_OptOut_Channels__c, contact.PrefCentre_AGS_OptOut_Email__c, contact.PrefCentre_AGS_OptOut_Mobile__c);
                                  
             }
             else if(String.isBlank(phone) && !String.isBlank(email)){
                 contact.PrefCentre_AGS_OptOut_Email__c = Opt;
                 contact.PrefCentre_AGS_OptOut_Mobile__c = '';
                 contact.PrefCentre_ContactMethodType__c = 'Email';
                 contact.PrefCentre_ContactMethodValue__c = contact.Email;
                 contact.PrefCentre_Structured__c = PreferenceCentreClass.GetUpdatedStructuredJSON(contact,'OptIn',contact.PrefCentre_AGS_OptOut_Channels__c, contact.PrefCentre_AGS_OptOut_Email__c, contact.PrefCentre_AGS_OptOut_Mobile__c);
                                     
             }
             
             update contact;
             
             ID contactId = contact.Id;
             jobAppMap.put('talentContactId', contactId);
            
             c.AccountId = accountId;
             c.Status = 'Closed';
             update c;
            
        } catch(Exception exp) {
            String chatterMessage = 'Has been stopped due to an issue with creating the talent record. Please review and remediate the case.';
            //sendChatterNotification(c.Id, chatterMessage);
            if(c.id != null){
                c.Description__c = chatterMessage;//+exp.getMessage();
                c.IsVisibleInSelfService = true;
                update c;
            }
        }
         return jobAppMap;
    }
    
    @AuraEnabled
    public static void populateTD(Id cId){
        
       /* List<Case> tdcase =  [Select Id,AccountId from Case where Id = :cId];
        List<attachment> caseatt = [Select Id,name,body,parentId,ContentType from Attachment where parentId =:tdcase[0].id ];
        
        try{
            if(tdcase[0].AccountId != null){
            Talent_Document__c td = new Talent_Document__c(
                Committed_Document__c = true,
                Document_Conversion__c = true,
                Talent__c = tdcase[0].AccountId,
                Document_Name__c = caseatt[0].Name,
                Document_Type__c = 'Resume'
                
            ) ; 
            insert td;
            
            if(!caseatt.isEmpty()){
                attachment tdatt = new attachment ();
                tdatt.Body = caseatt[0].body;
                tdatt.ContentType = caseatt[0].ContentType;
                tdatt.Name = caseatt[0].Name;
                tdatt.parentId = td.id;
                
                
                Database.SaveResult srList = Database.insert(tdatt,false);
                System.debug(srList);
                if (srList.isSuccess()) {
                    delete caseatt; 
                    
                }
              } 
            }
        }catch(exception e){
            if(c.id != null){
                c.Description__c = e.getMessage();
                c.IsVisibleInSelfService = true;
                update c;
            }
        }**
    }
    
   /* public static void sendChatterNotification(Id caseId, String messageText){

        Id chatterGroupId = getAgsLloydChatterGroupId();
        system.debug('chatterGroupId = ' + chatterGroupId);
        if (chatterGroupId !=null) {
            try {
                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        
                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
                mentionSegmentInput.id = chatterGroupId;
                messageBodyInput.messageSegments.add(mentionSegmentInput);
                textSegmentInput.text = messageText;
                messageBodyInput.messageSegments.add(textSegmentInput);
                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = chatterGroupId;
                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, feedItemInput);
                system.debug('post ConnectApi');
            } catch (Exception exp) {
                System.debug('Error in sendChatterNotification: ' + exp.getMessage() + ' Unable to post to Chatter');
            }    
        } else {
            System.debug('Unable to post to Chatter');
        }  
    } */  
    
    /*private static Id getAgsLloydChatterGroupId(){
    	CollaborationGroup chatterGroup = null;
        try{ 
        	chatterGroup = [select id from CollaborationGroup where Name = 'AGS Lloyds Web Applications'];
        } catch (DmlException dmlexp){
            System.debug('Error in getAgsLloydChatterGroupId : ' + dmlexp.getMessage() + ' Unable to post to Chatter');
            return null;
        }
        return chatterGroup.Id;	
    }  **/
    
    
    /* @AuraEnabled
    public static Boolean validateFileType (string namefile) {
        boolean validateFileType = FileController.validateFileType(namefile);
        return validateFileType;
    }*/
    
    /* private static void jobApplicationCreateOrder(Id jobId, ID contactId){
      
   /*    try{
            Id recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
	        Opportunity opportunityAccountId = [Select AccountId FROM Opportunity WHERE id=:jobId];
	            
	       	Order jobOrder = new Order(OpportunityId = jobId,
	                                   ShipToContactId = contactId,
                                       EffectiveDate = Date.valueOf(system.now()),
	                                   Status ='Applicant',
	                                   AccountId = opportunityAccountId.AccountId,
	                                   RecordTypeId = recordTypeId);
	                                   
        	insert(jobOrder);
       	
       } catch (Exception exp) {
            system.debug('Error occured: ' + exp.getMessage());
            String chatterMessage = 'Has been stopped because the submittal process could not be completed. Please review and remediate the case.';
            //sendChatterNotification(c.Id, chatterMessage);
            if(c.id != null){
                c.Description__c = chatterMessage;// exp.getMessage();
                c.IsVisibleInSelfService = true;
                update c;
            }
       } 
        
        
    }*/
    
}