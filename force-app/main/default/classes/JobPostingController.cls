public with Sharing class JobPostingController {

	@AuraEnabled
	public static List<Job_Posting__c> getMyJobPostings() {
		List<Job_Posting__c> myPostings = [select Id, Name, City__c, State__c, Posting_Id__c, Opportunity__r.Name,
		                                   Job_Title__c, Expiration_Date__c, CreatedDate
		                                   from Job_Posting__c
		                                   WHERE OwnerId = :UserInfo.getUserId() and Is_Active_New__c = true
		                                   ORDER BY CreatedDate desc];

		return myPostings;
	}

	@AuraEnabled
	public static List<Job_Posting__c> getRecentJobPostings() {


		List<RecentlyViewed> recentList = [SELECT Id, Name, Type, UserRoleId FROM RecentlyViewed
		                                   WHERE LastViewedDate != null AND Type = 'Job_Posting__c'
		                                   AND UserRoleId = :UserInfo.getUserId()
		                                   ORDER BY LastViewedDate DESC];

		Set<Id> RecentlyViewedIds = new Map<Id, RecentlyViewed> ([SELECT Id FROM RecentlyViewed
		                                                         WHERE LastViewedDate != null
		                                                         AND Type = 'Job_Posting__c']).keySet();

		List<Job_Posting__c> recentPostings = [select Id, Name, City__c, State__c, Posting_Id__c, Opportunity__r.name,
		                                       Job_Title__c, Expiration_Date__c, CreatedDate, CreatedBy.Name
		                                       from Job_Posting__c
		                                       where id IN :RecentlyViewedIds
		                                       and Is_Active_New__c = true
		                                       Order BY CreatedDate desc];

		return recentPostings;
	}



	@AuraEnabled
	public static boolean updateContact(Contact contact) {

		try {
			update contact;
			return true;
		} catch(Exception e) {
			return false;
		}

	}


	/* Below Method for testing purpose to test service from developer console do not delete* /
	  /* URL
	  Send OFCCP invite
	  1.	Prod : https://rws.allegisgroup.com/RWS/rest/posting/ofccpInvite
	  2.	Test :  https://rws.allegistest.com/RWS/rest/posting/ofccpInvite
	  3.	Load : https://rwslod.allegistest.com/RWS/rest/posting/ofccpInvite
	 * /
	  @Future(callout=true)
	  public static void callService (){
	 
	 
	  String jsonRequest='{"postingId": "6416896",'+
	  '"hrEmplId": "01111111",'+
	  '"candidateList": [{"candidateId": "30829650","talentId": "talent123","emailId": "pkamble@allegisgroup.com"}]}';  
	  HttpRequest req = new HttpRequest();
	  HttpResponse res = new HttpResponse();
	 
	  Http http = new Http();
	  req.setEndpoint('https://rws.allegistest.com/RWS/rest/posting/ofccpInvite');
	  req.setMethod('POST');
	  req.setHeader('Content-Type', 'application/json;charset=UTF-8');
	  req.setBody(jsonRequest);
	  req.setTimeout(5000);
	  res = http.send(req);
	  System.debug(res.getBody());
	 
	  }
	 */

	@AuraEnabled
	public static String callJobPostingService(String postingId, String toEmail, String talentId) {
		User_Organization__C uOrg = new User_Organization__C();
		String opcoValue = '';
		User usr = [select id, Peoplesoft_Id__c, Name, Email, Title, Phone, Office__c from User where id = :UserInfo.getUserId()];


		List<Account> TalentList = new List<Account> ([SELECT Id, Name, (Select Id, Name, Salutation, FirstName, LastName, Email, Other_Email__c, Work_email__c from Contacts)
		                                              FROM Account where id = :talentId WITH SECURITY_ENFORCED]);

		Job_Posting__c jobPosting = [select id, Name, Source_System_id__c, Job_Title__c,  Account_Name__c, City__c, State__c, Country__c, Expiration_Date__c, 
										 Opportunity__c, Office__c, CreatedDate, LastModifiedDate, LastModifiedById, Invitations_Count__c, opco__c
		                                 from Job_Posting__c where id = :postingId WITH SECURITY_ENFORCED];
		
		//S-234556 : 
		opcoValue = [Select MC_Opco__c from OPCO_Mapping__mdt where developerName =: jobPosting.opco__c LIMIT 1].MC_Opco__c;

		If (String.isNotBlank(usr.Office__c)) {
			uOrg = [SELECT Id, Name, Office_City__c, Office_Postal_Code__c, Office_State__c FROM User_Organization__c where name = :usr.Office__c  WITH SECURITY_ENFORCED limit 1];
		}
		String oAuthTokenMDTName = 'JobPostingInviteNonProd';
		if (Utilities.getSFEnv() == 'prod') {
			oAuthTokenMDTName = 'JobPostingInviteProd';
		}

		Map<String, Object> oTokenMap = WebServiceSetting.oAuthTokenUsingMDT(oAuthTokenMDTName);
		String payload;
		String accessToken = (String) oTokenMap.get('access_token');


		if (String.isNotBlank(accessToken)) {
			String applyUrl = '';
			String websiteUrl = '';
			String jobPostingIdValue = '';
			payload = '';

			List<Job_Posting_Invite_Opco__mdt> cmdts = jobPostingMDT(jobPosting.opco__c);
			if (cmdts.size() > 0) {
				applyUrl = cmdts[0].Apply_Url__c;
				websiteUrl = cmdts[0].Website_Url__c;
			}
			jobPostingIdValue = jobPosting.Name;


			//S-166056-Private Posting Invitation -Enahancements.
			String s = string.valueOfGmt(system.now());
			String newStr = s.replaceAll('\\D', '');




			payload += '{ "ContactKey":"' + TalentList[0].Contacts[0].Id + '",';
			payload += '"EventDefinitionKey":"' + String.valueOf(oTokenMap.get('apievent_key')) + '", "Data": { ';
			// payload += '"UniqueID__c":"'+TalentList[0].Contacts[0].Id+''+System.Now()+'", ';
			payload += '"UniqueID__c":"' + TalentList[0].Contacts[0].Id + '' + jobPostingIdValue + '' + newStr + '", ';
			payload += '"UserID__c":"' + usr.Id + '", "Salutation__c":"' + validateText(TalentList[0].Contacts[0].Salutation) + '", ';
			payload += '"FirstName__c":"' + validateText(TalentList[0].Contacts[0].FirstName) + '", "LastName__c":"' + TalentList[0].Contacts[0].LastName + '", ';
			payload += '"ContactID__c":"' + TalentList[0].Contacts[0].Id + '", "UserFullName__c":"' + usr.Name + '","UserEmail__c":"' + usr.Email + '", ';
			payload += '"UserTitle__c":"' + validateText(usr.Title) + '", "UserPhone__c":"' + validateText(usr.phone) + '", "OpCo__c":"' + opcoValue + '", "Contactemail__c":"' + toEmail + '", ';
			payload += '"Contactmobile__c":"", "ContactState__c":"", "ContactCountry__c":"", "ResponseEmailID__c":"Private_Posting_Invitation", ';
			payload += '"CreatedDate":"' + System.now() + '", "LastModifiedDate":"' + System.now() + '", ';
			payload += '"LastModifiedById":"' + jobPosting.LastModifiedById + '", "PostingId__c":"' + jobPostingIdValue + '", ';
			payload += ' "Title__c": "' + jobPosting.Job_Title__c + '",  "Company__c":"' + opcoValue + '", ';
			payload += '"Location__c": "' + validateText(jobPosting.City__c) + ' ' + validateText(jobPosting.State__c) + ', ' + validateText(jobPosting.Country__c) + '", ';
			payload += '"Posting_Close_Date__c":"' + jobPosting.Expiration_Date__c + '", ';
			payload += '"Apply_Url__c": "' + applyUrl + jobPostingIdValue + '", ';
			payload += '"UserAddress__c": "' + validateText(uOrg.Office_City__c) + ', ' + validateText(uOrg.Office_State__c) + ' ' + validateText(uOrg.Office_Postal_Code__c) + '",';
			payload += '"OPCOWebsite__c":"' + websiteUrl + '" } }';



			Http httpCon = new Http();
			HttpRequest request = new HttpRequest();
			HttpResponse response = new HttpResponse();
			Integer timeoutInMilli = Integer.valueOf(oTokenMap.get('timeout')) * 1000;
			request.setHeader('Authorization', 'Bearer ' + oTokenMap.get('access_token'));
			request.setHeader('Content-type', 'application/json');
			request.setEndpoint((String) oTokenMap.get('endPointUrl'));
			request.setMethod('POST');
			request.setTimeout(timeoutInMilli);
			request.setBody(payload);
			
			try {
				response = httpCon.send(request);

				System.debug('httpResponse.getBody()----' + response.getBody());
				if (response.getStatusCode() == 200 || response.getStatusCode() == 201)
				{
					System.debug('httpResponseGetBody-->' + response.getBody());
				}
			} catch(CalloutException cExp) {
				System.debug('Exception-->' + cExp);
				ConnectedLog.LogException('JobPostingInvite', 'JobPostingController', 'callJobPostingService_V2', TalentList[0].Contacts[0].Id, cExp);
			}

			System.debug(response.getBody());
			if (response.getStatusCode() == 200 || response.getStatusCode() == 201)
			{
				MC_Email__c mcEmail = new MC_Email__c();
				mcEmail.ContactID__c = TalentList[0].Contacts[0].Id;
				mcEmail.Contactemail__c = toEmail;
				mcEmail.FirstName__c = TalentList[0].Contacts[0].FirstName;
				mcEmail.LastName__c = TalentList[0].Contacts[0].LastName;
				mcEmail.OpCo__c = opcoValue;
				mcEmail.PostingId__c = jobPostingIdValue;
				mcEmail.PostingJobTitle__c = jobPosting.Job_Title__c;
				mcEmail.UniqueID__c = TalentList[0].Contacts[0].Id + jobPostingIdValue + newStr;
				mcEmail.ResponseEmailID__c = 'Private_Posting_Invitation';
				mcEmail.UserEmail__c = usr.Email;
				mcEmail.UserFullName__c = usr.Name;
				mcEmail.UserID__c = usr.Id;
				mcEmail.UserPhone__c = usr.Phone;
				mcEmail.UserTitle__c = usr.Title;
				mcEmail.Email_Status__c = 'Email not sent';

				try {
					insert mcEmail;
					if (jobPosting.Invitations_Count__c == null) {
						jobPosting.Invitations_Count__c = 1;
					} else {
						jobPosting.Invitations_Count__c = jobPosting.Invitations_Count__c + 1;
					}
					update jobPosting;
					return 'Success';
				} catch(DmlException dExp) {
					ConnectedLog.LogException('JobPostingController', 'callJobPostingService_V2', dExp);
					return 'Error';
				}
			}
			else {
				return 'Error';
			}
		} else {
			return 'Error';
		}

	}

	private static List<Job_Posting_Invite_Opco__mdt> jobPostingMDT(String opco) {
		String sfEnv = Utilities.getSFEnv();
		if (sfEnv.containsIgnoreCase('prod')) {
			sfEnv = 'Prod';
		} else {
			sfEnv = 'Test';
		}

		List<Job_Posting_Invite_Opco__mdt> jpInviteOpcoList = [select OPCO__c, Apply_Url__c, PP_Enabled__c, Website_Url__c
		                                                       from Job_Posting_Invite_Opco__mdt
		                                                       where SF_Env__c = :sfEnv and OPCOCode__c = :opco];

		return jpInviteOpcoList;
	}



	public static String validateText(String str) {
		if (String.isBlank(str)) {
			return '';
		} else {
			return str;
		}
	}
}