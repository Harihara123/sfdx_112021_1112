@isTest
public class JobPostingController_Test {
    
    @TestSetup
	public static void testDataSetup() {
		JobPostingTestDataFactory.northAmericaTestDataSetup();
    }
    public static testMethod void getMyJobPostingsTest(){
        JobPostingController.getMyJobPostings();
    }
    
    public static testMethod void getRecentJobPostingsTest(){
        JobPostingController.getRecentJobPostings();
    }
    
    public static testMethod void callJobPostingServiceTest(){
       // Job_Posting__c jb = new Job_Posting__c();
        //jb.Source_System_id__c ='R.122345456';
       // insert jb;
        List<User_Organization__c> orgList = new List<User_Organization__c>();
        User_Organization__c org1 = new User_Organization__c();
        Set<String> officeList = new Set<String>();
        org1.Active__c = true;
        org1.Office_Code__c = 'Office1';
        org1.Name = 'Office1';
        org1.OpCo_Code__c = 'Opco1';
        officeList.add(org1.Office_Code__c);
        orgList.add(org1);
        insert orgList;
        User usr = [select id,Peoplesoft_Id__c,Name,Email,Title,Phone,Office__c  from User where id =: UserInfo.getUserId()];
        usr.Office__c = orgList[0].Name;
        update usr;
        
        Job_Posting__c jb =[select id, Name, Source_System_id__c, Job_Title__c,  Account_Name__c, City__c, State__c, Country__c, Expiration_Date__c, 
						   Opportunity__c, Office__c, CreatedDate, LastModifiedDate, LastModifiedById, Invitations_Count__c, opco__c
            			   from Job_Posting__c Limit 1];
        
		 System.debug(jb);
        Account ac = JobPostingController_Test.createTalentAccount();
        Contact talCont = CreateTalentTestData.createTalentContact(ac);
		
        ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c(name ='Job Posting Invite',
                                                                            Service_URL__c='https://api-tst.allegistest.com/v1/jobposting/RWS/rest/posting/ofccpInvite',
                                                                            OAuth_Token__c='123456788889',
                                                                            Client_Id__c ='abe1234',
                                                                            Service_Http_Method__c ='POST',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                            );
        insert serviceSettings;
        Test.startTest();
       	 	Test.setMock(HttpCalloutMock.class, new JobPostingMockHttpResponseGenerator());	
 			JobPostingController.callJobPostingService(String.valueof(jb.id),'abc@yopmail.com',String.valueof(ac.id));
        Test.stopTest();
    }
    
    public static testMethod void updateContactTest(){
        Account ac = JobPostingController_Test.createTalentAccount();
        Contact con = JobPostingController_Test.createTalentContact(ac.Id);
        JobPostingController.updateContact(con);
        JobPostingController.updateContact(null);
    }
    
    public static Account createTalentAccount(){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=false,Source_System_id__c ='R.9090909090', RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        Database.insert(newAcc);
        return newAcc;
    }
    public static Contact createTalentContact(String accountID){
        Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(newC);
        return newC;
    }
}