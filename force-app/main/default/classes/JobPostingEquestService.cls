@RestResource(urlMapping = '/JobPosting/Status/V1/*')
/*
  {
  "equestid": "123456",
  "refenceid": "12356",
  "boardDetails": [
  {
  "id": 356194,
  "delete_status":"deleted",
  "board_status": {
  "queued_at": "2021-07-08T14:30:04Z",
  "expires_at": "2021-07-08T14:30:04Z",
  "posted_at": "2020-07-08T14:30:03Z",
  "state": "posted",
  "deferred_reason": "xyz",
  "live_url": "https://teksystems-stg.phenompro.com/us/en/job/JP-781029/JobTitle"
  },
  "board": {
  "name": "PhenomPeople (TEKsystems)",
  "id": 7305
  }
  }
  ]
  } 
 
 */
global class JobPostingEquestService {
	private static final String PHENOM_PEOPLE = 'PhenomPeople';
	private static final String EQUEST_EMEA = 'EMEA';

	@HttpPost
	global static void jobBoardStatusUpdate() {
		Boolean updateJobPosting;
		String responseMessage = '';
		RestRequest req = RestContext.request;
		RestResponse response = RestContext.response;
		List<Job_Posting__c> jp = new List<Job_Posting__c> ();
		List<Job_Posting_Channel_Details__c> jobPostingChannelList = new List<Job_Posting_Channel_Details__c> ();
		Map<String, Integer> boardExpDateMap = null;
		response.addHeader('Content-Type', 'application/json');
		Blob body = req.requestBody;
		String requestJSONBody = body.toString();


		try {
			EquestWrapper wrapObject = EquestWrapper.parse(requestJSONBody);
			
			jp = [Select id, Name, eQuest_Job_Id__c, Posting_Status__c, opco__c, Expiration_Date__c, CreatedById, Next_Schedule_Flag__c, 
				  No_of_Retried__c, CreatedBy.Office_Code__c, Job_Boards__c 
			      from Job_Posting__c where Name = :wrapObject.refenceid WITH SECURITY_ENFORCED limit 1];

			if (jp.size() > 0) {
				if(jp[0].opco__c == 'AG_EMEA') {
					boardExpDateMap = calcEMEABoardAutoExpiryDate(jp[0]);
				}
				else {
					boardExpDateMap = calcBoardAutoExpiryDate(jp[0]);
				}
				Map<String, String> existingJPCDMap = getExistingJPCD(wrapObject.boardDetails);
				for (EquestWrapper.BoardDetails eb : wrapObject.boardDetails) {
					DateTime postedDate = Null;
					DateTime expiryDate = Null;
					DateTime queuedDate = Null;
					
					if (String.isNotBlank(eb.board_status.posted_at)) {
						postedDate = (DateTime) JSON.deserialize('"' + eb.board_status.posted_at + '"', DateTime.class);
					}
					if (String.isNotBlank(eb.board_status.expires_at)) {
						expiryDate = (DateTime) JSON.deserialize('"' + eb.board_status.expires_at + '"', DateTime.class);
					}
					
					if (String.isNotBlank(eb.board_status.queued_at)) {
						queuedDate = (DateTime) JSON.deserialize('"' + eb.board_status.queued_at + '"', DateTime.class);
					}
					

					Job_Posting_Channel_Details__c jpcObj = new Job_Posting_Channel_Details__c();
					jpcObj.Board_Posting_Id__c = String.valueOf(eb.id);
					jpcObj.Board_Id__c = String.valueof(eb.board.id);
					jpcObj.Board_Name__c = eb.board.name;
					jpcObj.Job_Posting_Id__c = jp[0].Id;
					if (eb.delete_status != null && eb.delete_status == 'deleted') {
						jpcObj.Board_Status__c = 'Expired';
					} else {
						jpcObj.Board_Status__c = eb.board_status.state;
					}
					jpcObj.Posting_URL__c = eb.board_status.live_url;
					jpcObj.Board_Message__c = eb.board_status.deferred_reason;
					jpcObj.Posted_Date__c = postedDate;
					jpcObj.Board_Expiration_Date__c = expiryDate;
					jpcObj.Queued_Date__c = queuedDate;
					jpcObj.Next_Schedule_Flag__c = false;
					//jpcObj.Attempts__c = 0; //Neel 10/14- this was making attemps everytime 0

					if (jpcObj.Board_Status__c == 'Queued' || jpcObj.Board_Status__c == 'deferred') {
						jpcObj.Next_Schedule_Flag__c = true; //if Next_Schedule_Flag__c = true then Poller will process this record for next time.
					}

					//Updating Job Posting as per Phenom People status
					if ((jpcObj.Board_Name__c.containsIgnoreCase(PHENOM_PEOPLE) || jpcObj.Board_Name__c.containsIgnoreCase(EQUEST_EMEA)) && jpcObj.Board_Status__c == 'Queued') {
						jp[0].Next_Schedule_Flag__c = false;
						updateJobPosting = true;
					}
					else if ((jpcObj.Board_Name__c.containsIgnoreCase(PHENOM_PEOPLE) || jpcObj.Board_Name__c.containsIgnoreCase(EQUEST_EMEA)) && jpcObj.Board_Status__c == 'Posted') {
						jp[0].Posting_Status__c = 'Active';
						jp[0].Next_Schedule_Flag__c = false;
						jp[0].No_of_Retried__c = 0;
						jpcObj.Attempts__c = 0;
						updateJobPosting = true;
					}
					else if ((jpcObj.Board_Name__c.containsIgnoreCase(PHENOM_PEOPLE) || jpcObj.Board_Name__c.containsIgnoreCase(EQUEST_EMEA)) && jpcObj.Board_Status__c == 'Expired') {
						jp[0].Posting_Status__c = 'Inactive';
						updateJobPosting = true;
					}
					else if ((jpcObj.Board_Name__c.containsIgnoreCase(PHENOM_PEOPLE) || jpcObj.Board_Name__c.containsIgnoreCase(EQUEST_EMEA)) && jpcObj.Board_Status__c == 'deferred') {
						jp[0].Next_Schedule_Flag__c = true;
						updateJobPosting = true;
					}

					if (boardExpDateMap != null && boardExpDateMap.get(jpcObj.Board_Id__c) != null && jpcObj.Board_Status__c == 'Posted') {
						Integer expIntvl = Integer.valueOf(boardExpDateMap.get(jpcObj.Board_Id__c));
						//S-233513 - AG Portal is the driving board to make the posting record active 
						//So this board record should have longest expiration date among selected board.
						if (jpcObj.Board_Name__c.containsIgnoreCase(EQUEST_EMEA)) {
							expIntvl = longestExpInterval(boardExpDateMap);
						}
						//Neel : Only update the Auto Expiry date for the record, which got board_status = 'Posted' first time. 
						if(existingJPCDMap != null) {
							if(existingJPCDMap.get(jpcObj.Board_Posting_Id__c) != jpcObj.Board_Status__c ) {
								jpcObj.Auto_Expiry_Date__c = System.now().addDays(expIntvl);
						  	}
						}
						else {
							jpcObj.Auto_Expiry_Date__c = System.now().addDays(expIntvl);
						}
						
						
						if (jp[0].Expiration_Date__c == null && jpcObj.Board_Name__c.containsIgnoreCase(PHENOM_PEOPLE)) {
							jp[0].Expiration_Date__c = System.today().addDays(expIntvl);
							updateJobPosting = true;
						}
						/*S-233513 - EMEA Job Posting record expiration date will set on Posting record creation.
						if (jp[0].Expiration_Date__c == null && jpcObj.Board_Name__c.containsIgnoreCase(EQUEST_EMEA)) {
							Integer longExpIntvl = longestExpInterval(boardExpDateMap);
							jp[0].Expiration_Date__c = System.today().addDays(longExpIntvl);
							updateJobPosting = true;
						}*/
					}

					jobPostingChannelList.add(jpcObj);
				}

				if (jp[0].eQuest_Job_Id__c == '' || jp[0].eQuest_Job_Id__c == Null) {
					jp[0].eQuest_Job_Id__c = String.valueOf(wrapObject.equestid);
					updateJobPosting = true;
				}

				if (updateJobPosting == true) {
					update jp;
				}

				if (jobPostingChannelList.size() > 0) {
					//Database.upsert(jobPostingChannelList, Job_Posting_Channel_Details__c.Board_Posting_Id__c, true);
					JobPostingScheduler.scheduleBoardNextRun(jobPostingChannelList); //
				}

				responseMessage = '{"Message":"Request processed successfully"}';
			} else {
				responseMessage = '{"Message":"Job Posting Record not found in Connected, Kindly validate the request."}';
			}
			response.statusCode = 200;

		} Catch(JSONException e) {
			System.debug('Exception::' + e.getMessage() + '::line Number::' + e.getLineNumber());
			ConnectedLog.LogException('JPF/JobPostingForm', 'JobPostingEquestService', 'jobBoardStatusUpdate', e);
			response.statusCode = 400;
			responseMessage = '{"Message":"Request process failed. Exception message: ' + e.getMessage() + '"}';
		} catch(DmlException e) {
			System.debug('Exception::' + e.getMessage() + '::line Number::' + e.getLineNumber());
			ConnectedLog.LogException('JPF/JobPostingForm', 'JobPostingEquestService', 'jobBoardStatusUpdate', jp[0].id, e);
			response.statusCode = 400;
			responseMessage = '{"Message":"Request process failed. Exception message: ' + e.getMessage() + '"}';
		}
		finally {
			if (jp.size() > 0) {
				ConnectedLog.LogInformation('JPF/JobPostingForm', 'JobPostingEquestService', 'jobBoardStatusUpdate', jp[0].id, responseMessage);
				createServiceLog(jp[0], requestJSONBody, responseMessage, String.valueOf(response.statusCode), '/JobPosting/Status/V1/*', 'Job Board Status', System.now(), System.now());
			}

		}
		response.responseBody = Blob.valueOf(responseMessage);
	}

	/**
	 * @author : Neel Kamal
	 * @Date : 08/31/2020
	 * Description : This method will calculate Expiry Interval for each Job Boards and return map of Board Id and Expiry Interval
	 */
	public static Map<String, Integer> calcBoardAutoExpiryDate(Job_Posting__c jp) {
		Map<String, Integer> expIntervalMap = new Map<String, Integer> ();
		
		Integer expIntvl;
		List<Job_Posting_Channel_List__c> jpclList = new List<Job_Posting_Channel_List__c> ();
		jpclList = [Select id, Board_Id__c, Expiry_Interval__c from Job_Posting_Channel_List__c where OPCO__c =: jp.opco__c WITH SECURITY_ENFORCED];
		for (Job_Posting_Channel_List__c jpcl :jpclList) {
			if (jpcl.Expiry_Interval__c != null) {
				expIntvl = Integer.valueOf(jpcl.Expiry_Interval__c);
				expIntervalMap.put(jpcl.Board_Id__c, expIntvl);
			}
		}
		return expIntervalMap;
	}

	/**
	 * @author : Neel Kamal
	 * @Date : 07/26/2021
	 * Description : This method will calculate Expiry Interval for each Job Boards and return map of Board Id and Expiry Interval
	 * It only for EMEA opco because EMEA expiry interval will work on selected Job Boards only.
	 * 
	 */
	public static Map<String, Integer> calcEMEABoardAutoExpiryDate(Job_Posting__c jp) {
		List<String> selectedBoardIds = new List<String>();
		//Deserialize selected Boards data into Wrapper list
		List<JobPostingFormEMEAHelper.EMEABoardsWrapper> emeaBoardList = (List<JobPostingFormEMEAHelper.EMEABoardsWrapper>) JSON.deserialize(jp.Job_Boards__c, List<JobPostingFormEMEAHelper.EMEABoardsWrapper>.CLASS);
		
		for(JobPostingFormEMEAHelper.EMEABoardsWrapper board : emeaBoardList) {
			if(board.selected) {
				selectedBoardIds.add(board.boardId);
			}
		}

		Map<String, Integer> expIntervalMap = new Map<String, Integer> ();

		
		Integer expIntvl;
		List<Job_Posting_Channel_List__c> jpclList = new List<Job_Posting_Channel_List__c>();
		//Get data only for selected board
		jpclList = [Select id, Board_Id__c, Expiry_Interval__c from Job_Posting_Channel_List__c where OPCO__c =: jp.opco__c
					AND User_Office_Code__c =: jp.CreatedBy.Office_Code__c and Board_Id__c IN : selectedBoardIds WITH SECURITY_ENFORCED];

		for (Job_Posting_Channel_List__c jpcl :jpclList) {
			if (jpcl.Expiry_Interval__c != null) {
				expIntvl = Integer.valueOf(jpcl.Expiry_Interval__c);
				expIntervalMap.put(jpcl.Board_Id__c, expIntvl);
			}
		}
		return expIntervalMap;
	}
	/**
	 * @author : Neel Kamal
	 * @Date : 07/26/2021
	 * Description : return the longest expiry interval among selected boards.
	 * 
	 */
	public static Integer longestExpInterval(Map<String, Integer> expIntervalMap) {
		Integer longestInterval = 0;
		for(Integer intval : expIntervalMap.values()) {
			if(intval > longestInterval) {
				longestInterval = intval;
			}
		}
		return longestInterval;
	}
	/**
	 * @author : Neel Kamal
	 * @Date : 08/19/2021
	 * Description : return Job Posting Channel Detail records if exists.
	 */
	private static Map<String, String> getExistingJPCD(List<EquestWrapper.BoardDetails> boardDetailList){
		Map<String, String> boardIdJPCDMap = null;
		List<String> boardIds = new List<String>();
		for(EquestWrapper.BoardDetails bd : boardDetailList) {
			boardIds.add(String.valueOf(bd.id));
		}

		List<Job_Posting_Channel_Details__c> jpcdList = new List<Job_Posting_Channel_Details__c>();
		jpcdList = [Select Id, Board_Posting_Id__c, Board_Status__c  from Job_Posting_Channel_Details__c where Board_Posting_Id__c IN : boardIds WITH SECURITY_ENFORCED];	
		if(jpcdList.size() > 0) {
			boardIdJPCDMap = new Map<String, String>();
			for(Job_Posting_Channel_Details__c jpcd : jpcdList) {
				boardIdJPCDMap.put(jpcd.Board_Posting_Id__c, jpcd.Board_Status__c);
			}
		}
		
		return boardIdJPCDMap;
	}
	/*Author: Pranav D
	  Description : Below Method is to log all job board status request from Equest
	 */
	private static void createServiceLog(Job_Posting__c jp, String req, String rep, String respStatus, String serviceName, String recTypeName, DateTime reqTime, Datetime repTime) {
		Job_Posting_Service_log__c jpsl = new Job_Posting_Service_log__c();

		jpsl.Job_Posting__c = jp.Id;
		jpsl.JP__c = jp.Name;
		jpsl.opco__c = jp.opco__c;
		jpsl.recordTypeId = Utility.getRecordTypeId('Job_Posting_Service_log__c', recTypeName);
		jpsl.request__c = req;
		jpsl.Request_Date_Time__c = reqTime;
		jpsl.response__c = rep;
		jpsl.Response_Date_Time__c = repTime;
		jpsl.Service_Name__c = serviceName;
		jpsl.Status__c = respStatus;
		try {
			insert jpsl;
		} catch(DmlException exp) {
			ConnectedLog.LogException('JPF/JobPostingForm', 'JobPostingEquestService', 'createServiceLog', jp.Id, exp);
		}
	}
}