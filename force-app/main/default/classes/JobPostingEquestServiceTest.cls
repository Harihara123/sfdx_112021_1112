@isTest
public class JobPostingEquestServiceTest {
@isTest 
	public static void method1() {
		Job_Posting__c jp = new Job_Posting__c();
		jp.Job_Title__c  = 'Architect';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Contract';
		jp.Industry_Sector__c ='Accounting';
		jp.External_Job_Description__c = 'Test Administrator'; 
        jp.Allegis_Employment_Type__c='Permanent';
		jp.City__c = 'Hanover';
		jp.State__c = 'MD';
		jp.Country__c = 'US';
		jp.External_Job_Description__c = 'Job Description';
		jp.Salary_max__c = 20000000;
		jp.Salary_min__c = 30000000;
		jp.Salary_Frequency__c = 'Salary';
		jp.opco__c = 'TEK';
		jp.ONET_Codes__c = 	'Accounting';
		insert jp;
		
        List<Job_Posting__c> jpList = [Select Id, Name from Job_Posting__c limit 1];
        	
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{"equestid": "123456",'
            +'"refenceid": "'+jpList[0].Name+'",'
            +'"boardDetails": [{'
            +'"id": 356194,'
            +'"board_status": {'
            +'"queued_at": "2021-07-08T14:30:04Z",'
            +'"expires_at": "2021-07-08T14:30:04Z",'
            +'"posted_at": "2020-07-08T14:30:03Z",'
            +'"state": "posted",'
            +'"deferred_reason": "xyz",'
            +'"live_url": "https://teksystems-stg.phenompro.com/us/en/job/JP-781029/JobTitle"'
            +'},'
            +'"board": {'
            +'"name": "PhenomPeople (TEKsystems)",'
            +'"id": 7305'
            +'}}]}';
            
            req.requestURI = '/services/apexrest/JobPosting/Status/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            JobPostingEquestService.jobBoardStatusUpdate();
        	
	}
    
    @isTest
    public static void method2_exception() {
		Job_Posting__c jp = new Job_Posting__c();
		jp.Job_Title__c  = 'Architect';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Contract';
		jp.Industry_Sector__c ='Accounting';
		jp.External_Job_Description__c = 'Test Administrator'; 
        jp.Allegis_Employment_Type__c='Permanent';
		jp.City__c = 'Hanover';
		jp.State__c = 'MD';
		jp.Country__c = 'US';
		jp.External_Job_Description__c = 'Job Description';
		jp.Salary_max__c = 20000000;
		jp.Salary_min__c = 30000000;
		jp.Salary_Frequency__c = 'Salary';
		jp.opco__c = 'TEK';
		jp.ONET_Codes__c = 	'Accounting';
		insert jp;
		
        List<Job_Posting__c> jpList = [Select Id, Name from Job_Posting__c limit 1];
        	
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{"equetid" "123456"}';
            
            req.requestURI = '/services/apexrest/JobPosting/Status/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            JobPostingEquestService.jobBoardStatusUpdate();
	}

	@isTest 
	public static void expiredJobPosting() {
		Test.startTest();
			Job_Posting__c jp = new Job_Posting__c();
			jp.Job_Title__c  = 'Architect';
			jp.Skills__c = 'Core Java and Python';
			jp.Allegis_Employment_Type__c = 'Contract';
			jp.Industry_Sector__c ='Accounting';
			jp.External_Job_Description__c = 'Test Administrator'; 
			jp.Allegis_Employment_Type__c='Permanent';
			jp.City__c = 'Hanover';
			jp.State__c = 'MD';
			jp.Country__c = 'US';
			jp.External_Job_Description__c = 'Job Description';
			jp.Salary_max__c = 20000000;
			jp.Salary_min__c = 30000000;
			jp.Salary_Frequency__c = 'Salary';
			jp.opco__c = 'TEK';
			jp.ONET_Codes__c = 	'Accounting';
			insert jp;
		
			List<Job_Posting__c> jpList = [Select Id, Name from Job_Posting__c limit 1];
        	
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{"equestid": "123456",'
            +'"refenceid": "'+jpList[0].Name+'",'
            +'"boardDetails": [{'
			+ '"delete_status":"deleted",'
            +'"id": 356194,'
            +'"board_status": {'
            +'"queued_at": "2021-07-08T14:30:04Z",'
            +'"expires_at": "2021-07-08T14:30:04Z",'
            +'"posted_at": "2020-07-08T14:30:03Z",'
            +'"state": "Posted",'
            +'"deferred_reason": "xyz",'
            +'"live_url": "https://teksystems-stg.phenompro.com/us/en/job/JP-781029/JobTitle"'
            +'},'
            +'"board": {'
            +'"name": "PhenomPeople (TEKsystems)",'
            +'"id": 7305'
            +'}}]}';
            
            req.requestURI = '/services/apexrest/JobPosting/Status/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            JobPostingEquestService.jobBoardStatusUpdate();

			List<Job_Posting_Channel_Details__c> jpcdList = [select id, Board_Id__c, Board_Status__c from Job_Posting_Channel_Details__c Limit 1];
			System.assertEquals(jpcdList[0].Board_Status__c, 'Expired');
		Test.stopTest();
	}

	@isTest 
	public static void queuedJobPosting() {
		Test.startTest();
			User usr = CreateTalentTestData.createUser('Single Desk 1', 'TEK', 'Applications');
			System.runAs(usr){
				List<Job_Posting_Channel_List__c> jpclList = new List<Job_Posting_Channel_List__c>();			
				Job_Posting_Channel_List__c jpcl = new Job_Posting_Channel_List__c();
				jpcl.Board_Id__c = '7305';
				jpcl.Board_Name__c = 'PhenomPeople (TEKsystems)';
				jpcl.OPCO__c = 'TEK';
				jpcl.Schedule_Time_In_UTC__c = '10:00';
				jpcl.Retry_Limit__c = 3;
				jpcl.Frequency_In_Minute__c = 0;
				jpcl.Expiry_Interval__c = 10;
				jpclList.add(jpcl);

				insert jpclList;

				Job_Posting__c jp = new Job_Posting__c();
				jp.Job_Title__c  = 'Architect';
				jp.Skills__c = 'Core Java and Python';
				jp.Allegis_Employment_Type__c = 'Contract';
				jp.Industry_Sector__c ='Accounting';
				jp.External_Job_Description__c = 'Test Administrator'; 
				jp.Allegis_Employment_Type__c='Permanent';
				jp.City__c = 'Hanover';
				jp.State__c = 'MD';
				jp.Country__c = 'US';
				jp.External_Job_Description__c = 'Job Description';
				jp.Salary_max__c = 20000000;
				jp.Salary_min__c = 30000000;
				jp.Salary_Frequency__c = 'Salary';
				jp.opco__c = 'TEK';
				jp.ONET_Codes__c = 	'Accounting';
				insert jp;
		
				List<Job_Posting__c> jpList = [Select Id, Name from Job_Posting__c limit 1];
        	
				RestRequest req = new RestRequest(); 
				RestResponse res = new RestResponse();
				String requestJSONBody = '{"equestid": "123456",'
				+'"refenceid": "'+jpList[0].Name+'",'
				+'"boardDetails": [{'
				+'"id": 356194,'
				+'"board_status": {'
				+'"queued_at": "2021-07-08T14:30:04Z",'
				+'"expires_at": "2021-07-08T14:30:04Z",'
				+'"posted_at": "2020-07-08T14:30:03Z",'
				+'"state": "Posted",'
				+'"deferred_reason": "xyz",'
				+'"live_url": "https://teksystems-stg.phenompro.com/us/en/job/JP-781029/JobTitle"'
				+'},'
				+'"board": {'
				+'"name": "PhenomPeople (TEKsystems)",'
				+'"id": 7305'
				+'}}]}';
            
				req.requestURI = '/services/apexrest/JobPosting/Status/V1/*';
				req.httpMethod = 'POST';
				req.requestBody = Blob.valueOf(requestJSONBody);
				RestContext.request = req;
				RestContext.response= res;
				JobPostingEquestService.jobBoardStatusUpdate();
			}
			List<Job_Posting_Channel_Details__c> jpcdList = [select id, Board_Id__c, Auto_Expiry_Date__c from Job_Posting_Channel_Details__c Limit 1];
			System.assertNotEquals(jpcdList[0].Auto_Expiry_Date__c, null);
		Test.stopTest();
	}

	@isTest 
	public static void queuedJobPostingEMEA() {
		Test.startTest();
			User usr = CreateTalentTestData.createUser('system administrator', 'GB1', 'Applications');
			usr.Office_Code__c = '01271';
			System.debug('queuedJobPostingEMEA---'+usr);
			System.runAs(usr){
				JobPostingTestDataFactory.emeaChannelListTestData();
				JobPostingTestDataFactory.emeaTestDataSetup();
		
				List<Job_Posting__c> jpList = [Select Id, Name from Job_Posting__c limit 1];
				/*List<job_posting_channel_List__c> jpclList = new List<job_posting_channel_List__c>();
				for(job_posting_channel_List__c jpcl :[Select FIELDS(ALL) FROM  job_posting_channel_List__c]){
					if(jpcl.Board_Id__c == '3025') {
						jpcl.User_Office_Code__c = '0127'
					}
					jpclList.add(jpcl);
				}*/
        		
				RestRequest req = new RestRequest(); 
				RestResponse res = new RestResponse();
				String requestJSONBody = '{"equestid": "123356",'
				+'"refenceid": "'+jpList[0].Name+'",'
				+'"boardDetails": [{'
				+'"id": 357194,'
				+'"board_status": {'
				+'"queued_at": "2021-07-08T14:30:04Z",'
				+'"expires_at": "2021-07-08T14:30:04Z",'
				+'"posted_at": "2020-07-08T14:30:03Z",'
				+'"state": "Posted",'
				+'"deferred_reason": "xyz",'
				+'"live_url": "https://teksystems-stg.phenompro.com/us/en/job/JP-781029/JobTitle"'
				+'},'
				+'"board": {'
				+'"name": "EMEA_TEKsystems",'
				+'"id": 80129'
				+'}}]}';
            
				req.requestURI = '/services/apexrest/JobPosting/Status/V1/*';
				req.httpMethod = 'POST';
				req.requestBody = Blob.valueOf(requestJSONBody);
				RestContext.request = req;
				RestContext.response= res;
				JobPostingEquestService.jobBoardStatusUpdate();
			}
			List<Job_Posting_Channel_Details__c> jpcdList = [select id, Board_Id__c, Auto_Expiry_Date__c from Job_Posting_Channel_Details__c Limit 1];
			System.assertNotEquals(jpcdList[0].Auto_Expiry_Date__c, null);
		Test.stopTest();
	}
}