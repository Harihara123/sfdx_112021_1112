
/**
@author : Neel Kamal
@date : 04/02/2020
@Description : This class is used to display the data for Posting Invitation tab of Job Psoting.
**/
with sharing public class JobPostingInvitationController {
	//This method get data from MC Email object and group the all invitation related to same contact and send data to UI. 
	@AuraEnabled 
	public static JobPostingWrapper jobPostingInvitations(String recordId) {
		recordId = String.escapeSingleQuotes(recordId);
		String sourceSystemId;
		List<JobPostingWrapper> ppwList = new List<JobPostingWrapper>();
		JobPostingWrapper ppw = new JobPostingWrapper();
		
		List<Job_Posting__c> jobPostList = [select id, OFCCP_Flag__c, Source_System_id__c, Posting_Id_Text__c, Is_Active_New__c from Job_Posting__c where id =: recordId ];
		if(jobPostList.size() > 0) {
			Job_Posting__c jobPost = jobPostList.get(0);
			/*
            sourceSystemId = jobPost.Source_System_id__c;	
			
			if(sourceSystemId.startsWith('R.')) {
					sourceSystemId = sourceSystemId.substring(2);
			}
			*/
            sourceSystemId = jobPost.Posting_Id_Text__c;
			ppw.sourceSysId = sourceSystemId;
			ppw.isOFCCP = jobPost.OFCCP_Flag__c;
			ppw.isPostingActive = jobPost.Is_Active_New__c;
			//if(jobPost.OFCCP_Flag__c) {
				ppw.jpiWraprList = builtInvitList(sourceSystemId);
			//}
		}
		return ppw;
	}

	//This method invoked from UI t sedn the email invitation to Marketing Cloud by using MC Service API. 
	@AuraEnabled
	public static JobPostingWrapper sendInvitation(String jobPostingSFId, String contactEmailId, String contactId) {
		List<Contact> contacts = [select id, AccountId from Contact where id =: contactId];
		JobPostingWrapper ppw = new JobPostingWrapper();
		String status;
		try{
			status = JobPostingController.callJobPostingService(jobPostingSFId, contactEmailId, contacts[0].AccountId);
		}catch(DMLException dexp) {
			status = 'Error';
			ConnectedLog.LogException('JobPostingEmailTracking','sendInvitation',dexp);
			throw new AuraHandledException(Label.Error);
		}
		ppw = jobPostingInvitations(jobPostingSFId);
		ppw.mcServiceStatus = status;
		return ppw;
	}

	private static Map<String, Integer> invitationCount(String sourceSystemId) {
		List<AggregateResult> invitationCount = [select count(id), ContactID__c from MC_Email__c where PostingId__c =: sourceSystemId and ResponseEmailID__c= 'Private_Posting_Invitation' group by ContactID__c ];
		Map<String, Integer> invtCountMap = new Map<String, Integer>();
		for(AggregateResult ar : invitationCount) {
			invtCountMap.put(String.valueOf(ar.get('ContactID__c')), Integer.valueOf(ar.get('expr0')));
		}

		return invtCountMap;
	}

	private static List<JPInvitationWrapper> builtInvitList(String sourceSystemId) {
		List<MC_Email__c> mcEmailList = [select id, PostingId__c,  FirstName__c, LastName__c, ContactID__c, Contactemail__c, UserFullName__c, CreatedDate, Email_Status__c from MC_Email__c where PostingId__c =: sourceSystemId and ResponseEmailID__c= 'Private_Posting_Invitation' order by ContactID__c, CreatedDate desc];
		List<JPInvitationWrapper> jpiWraprList = new List<JPInvitationWrapper>();
		Map<String, Integer> invitCountMap = invitationCount(sourceSystemId);
		List<MC_Email__c> groupedMcEmailList = gropuByResults(mcEmailList);
		JPInvitationWrapper jpiWrapper; 

		for(MC_Email__c mce : groupedMcEmailList) {
			jpiWrapper = new JPInvitationWrapper(); 	
			Datetime ctDate = mce.CreatedDate;
			jpiWrapper.id = mce.Id; 
			jpiWrapper.postingId = mce.PostingId__c;
			jpiWrapper.contactName = mce.FirstName__c + ' ' + mce.LastName__c;
			jpiWrapper.contactEmail = mce.Contactemail__c;
			jpiWrapper.dateSent = Date.newInstance(ctDate.year(), ctDate.month(), ctDate.day());
			jpiWrapper.userName = mce.UserFullName__c;
			jpiWrapper.emailStatus = mce.Email_Status__c;
			jpiWrapper.contactId = mce.ContactID__c;
			jpiWrapper.invtCount = invitCountMap.get(mce.ContactID__c);

			jpiWraprList.add(jpiWrapper);
		}

		return jpiWraprList;
	}

	private static List<MC_Email__c> gropuByResults(List<MC_Email__c> mcEmailList) {
		List<MC_Email__c> groupedMcEmailList = new List<MC_Email__c>();
		String tmpCntctId='';
		for(MC_Email__c mce :mcEmailList) {			
			if(mce.ContactID__c != tmpCntctId) {
				groupedMcEmailList.add(mce);
			}

			tmpCntctId = mce.ContactID__c;
		}

		return groupedMcEmailList;
	}

	public class JPInvitationWrapper {
		 @AuraEnabled public String id{get;set;}
		 @AuraEnabled public String postingId{get;set;}
		 @AuraEnabled public String contactId{get;set;}
		 @AuraEnabled public String contactName{get;set;}
		 @AuraEnabled public String contactEmail{get;set;}
		 @AuraEnabled public String userName{get;set;}
		 @AuraEnabled public Datetime dateSent{get; set;} 
		 @AuraEnabled public String emailStatus{get;set;}
		 @AuraEnabled public Integer invtCount{get;set;}
	}

	public class JobPostingWrapper {
		@AuraEnabled public List<JPInvitationWrapper> jpiWraprList{get;set;}
		@AuraEnabled public Boolean isOFCCP{get;set;}
		@AuraEnabled public String sourceSysId{get;set;}
		@AuraEnabled public String mcServiceStatus{get;set;}
		@AuraEnabled public Boolean isPostingActive{get;set;}
	}

}