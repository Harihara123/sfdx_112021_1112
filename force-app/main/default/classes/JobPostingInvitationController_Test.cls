@isTest
public class JobPostingInvitationController_Test  {
	@TestSetup
	public static void dataSetup(){
		Contact contact = CreateTalentTestData.createTalentContact(CreateTalentTestData.createTalentAccount());
		List<Job_Posting__c> jpList = new List<Job_Posting__c>();
		Job_Posting__c jp = new Job_Posting__c(Source_System_id__c='R.567887', OFCCP_Flag__c=true, Job_Title__c='Warehouse Associate');
		jpList.add(jp);
		jp = new Job_Posting__c(Source_System_id__c='R.567888', OFCCP_Flag__c=false, Job_Title__c='Warehouse Associate urgent');
		jpList.add(jp);
		jp = new Job_Posting__c(Source_System_id__c='R.567999', OFCCP_Flag__c=true, Job_Title__c='Architect urgent');
		jpList.add(jp);

		insert jpList;

		MC_Email__c mc = new MC_Email__c();
		mc.PostingId__c = '567887';
		mc.FirstName__c = contact.FirstName;
		mc.LastName__c = contact.LastName;
		mc.ContactID__c = contact.Id;
		mc.Contactemail__c = contact.Email;
		mc.UserFullName__c = UserInfo.getName();
		mc.Email_Status__c = 'Sent';
		mc.ResponseEmailID__c= 'Private_Posting_Invitation';

		insert mc;
	}

	@isTest
	public static  void jobPostingInvitations_test() {
		Test.startTest();
			Job_Posting__c jp = [select id from Job_Posting__c where Source_System_id__c='R.567887' limit 1];

			JobPostingInvitationController.JobPostingWrapper ppw = JobPostingInvitationController.jobPostingInvitations(jp.Id);
			System.assertEquals(ppw.isOFCCP, true); 
		
			Job_Posting__c jp1 = [select id from Job_Posting__c where Source_System_id__c='R.567888' limit 1];

			JobPostingInvitationController.JobPostingWrapper ppw1 = JobPostingInvitationController.jobPostingInvitations(jp1.Id);
			System.assertEquals(ppw1.isOFCCP, false);
		Test.stopTest();
	}
	@isTest
	public static void sendInvitationTest() {
		 ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c(name ='Job Posting Invite',
                                                                            Service_URL__c='https://api-tst.allegistest.com/v1/jobposting/RWS/rest/posting/ofccpInvite',
                                                                            OAuth_Token__c='123456788889',
                                                                            Client_Id__c ='abe1234',
                                                                            Service_Http_Method__c ='POST',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                            );
        insert serviceSettings;
		Test.startTest();
			Job_Posting__c jp = [select id from Job_Posting__c where Source_System_id__c='R.567887' limit 1];
			Contact ct = [select id, Email from contact where Record_Type_Name__c='Talent'];

			JobPostingInvitationController.sendInvitation((String)jp.Id, ct.Email, (String)ct.Id);
		Test.stopTest();
	}
}