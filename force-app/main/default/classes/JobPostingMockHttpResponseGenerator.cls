@isTest
global class JobPostingMockHttpResponseGenerator implements HttpCalloutMock {
     
    global HTTPResponse respond(HTTPRequest req) {
        
		String endpoint = req.getEndpoint();
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(generateResopnse(endpoint));
        res.setStatusCode(200);
        return res;
    }
    
	public string generateResopnse(String endpoint){
	 
    ACE_PP_Config__mdt ppConfig = [Select Id, DeveloperName, Auth_URL__c,Client_Id__c,Client_Secret__c,REST_URL__c,Event_Definition_Key__c from ACE_PP_Config__mdt Where DeveloperName =:MCJourneyController.IsProductionOrg() LIMIT 1]; 
	String jsonResponse = '';
		if(endpoint == ppConfig.Auth_URL__c){
			jsonResponse = '{"access_token":"1234567890"}';
        }else if(endpoint == ppConfig.REST_URL__c){
			jsonResponse ='{"eventInstanceId":"95efdfed-eb05-47c1-ab8a-b23159966afc"}';
        }else if(endpoint == 'https://api-tst.allegistest.com/v1/jobposting/RWS/rest/posting/ofccpInvite'){
			jsonResponse ='{"eventInstanceId":"95efdfed-eb05-47c1-ab8a-b23159966afc"}';
        }
	return jsonResponse;
    }
	
}