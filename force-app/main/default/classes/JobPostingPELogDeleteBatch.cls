global class JobPostingPELogDeleteBatch implements Database.Batchable<SObject>,Database.Stateful,Schedulable  {

	List<String> error= new List<String>() ;

	global JobPostingPELogDeleteBatch() {

	}

	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */
	global Database.QueryLocator start(Database.BatchableContext context) {
        if(Test.isRunningTest())
        {
            error.add('This is to Test');
        }
        return Database.getQueryLocator('SELECT Id FROM Job_Posting_PE_Log__c where  CreatedDate < LAST_N_DAYS:' + Label.ATS_NumberOf_Days);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */
	global void execute(Database.BatchableContext context, List<Job_Posting_PE_Log__c> scope) {
		try {
           if (!scope.isEmpty() && scope != null)
			{
			delete scope;
		
			}
			
		}
		catch(DmlException e)
		{
			ConnectedLog.LogException('JPF/JobPostingForm', 'JobPostingPELogDeleteBatch', 'execute', e);
			error.add(e.getMessage());
		}
		catch(Exception e)
		{
			ConnectedLog.LogException('JPF/JobPostingForm', 'JobPostingPELogDeleteBatch', 'execute', e);
			error.add(e.getMessage());
		}

	}

	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */
	global void finish(Database.BatchableContext context) {
	if (!error.isEmpty()){
	AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :context.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       String subject = 'Delete JobPosting Service Log Records';       
       mail.setSubject(subject + a.Status);
       string s = 'The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures';
       mail.setPlainTextBody(s);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	   }
		
	}

	global void execute(SchedulableContext sc)
	{
		JobPostingPELogDeleteBatch b = new JobPostingPELogDeleteBatch();
		database.executebatch(b);
	}
}