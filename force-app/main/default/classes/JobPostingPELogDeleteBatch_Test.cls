@isTest
private class JobPostingPELogDeleteBatch_Test {

	@isTest
	private static void JobPostingPELogDeleteTest() {
		List<Job_Posting_PE_Log__c> lstJPServiceLog = new List<Job_Posting_PE_Log__c> ();
		for (Integer i = 0; i< 200; i++)
		{
			Job_Posting_PE_Log__c JP = new Job_Posting_PE_Log__c();
			JP.Opco__c = 'TEK';
			JP.CreatedDate = System.Date.today().addDays(- 31);
			lstJPServiceLog.add(JP);

		}
		insert lstJPServiceLog;

		Test.startTest();
		String schTime = '0 0 12 * * ?';
		JobPostingPELogDeleteBatch objjp = new JobPostingPELogDeleteBatch();
		String jobId = system.schedule('TestUpdateConAccJob', schTime, objjp);
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
		System.assertEquals(0, ct.TimesTriggered);
		Test.stopTest();
	}
}