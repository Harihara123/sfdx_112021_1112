@isTest
private class JobPostingServiceLogDeleteBatch_Test {
	@TestSetup
	static void TestDataSetup() {
		List<Job_Posting_Service_log__c> lstJPServiceLog = new List<Job_Posting_Service_log__c> ();
		for (Integer i = 0; i< 200; i++) {
			Job_Posting_Service_log__c JP = new Job_Posting_Service_log__c();
			JP.Opco__c = 'TEK';
			JP.CreatedDate = System.Date.today().addDays(- 31);
			lstJPServiceLog.add(JP);

		}
		insert lstJPServiceLog;
	}



	@IsTest
	static void testExceptionBlock() {

		Test.startTest();
		String schTime = '0 0 12 * * ?';
		JobPostingServiceLogDeleteBatch b = new JobPostingServiceLogDeleteBatch();
		String jobId= system.schedule('TestUpdateConAccJob', schTime, b);
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
		System.assertEquals(0, ct.TimesTriggered);
		Test.stopTest();
	}

}