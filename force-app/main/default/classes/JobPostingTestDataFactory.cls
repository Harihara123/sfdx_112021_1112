/*
	@auther : Neel Kamal
	@Date : 03-MAR-2021
	@Desc : This Test Data Factory is for Job Posting feature.
		    Please mindfull to change any data.  
*/
@IsTest
public class JobPostingTestDataFactory {
	@Istest
	public static void northAmericaTestDataSetup() {
		DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo', Value__c = 'Aerotek, Inc;TEKsystems, Inc.');
		insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update', Value__c = 'False');
		insert DRZSettings1;
		List<Opportunity> lstOpportunitys = new List<Opportunity> ();
		Account clientAcc = TestData.newAccount(1, 'Client');
		insert clientAcc;

		Contact con = TestData.newContact(clientAcc.id, 1, 'Recruiter');
		Insert con;

		
		Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'Aerotek CE',
		Name = 'TIVOLI',
		Category_Id__c = 'FIN_RM',
		Category__c = 'Finance Resource Management',
		Job_Code__c = 'Developer',
		Jobcode_Id__c	='700009',
		OpCo_Id__c='ONS',
		Segment_Id__c	='A_AND_E',
		Segment__c = 'Architecture and Engineering',
		OpCo_Status__c = 'A',
		Skill_Id__c	='TIVOLI',
		Skill__c = 'Tivoli' );
		insert prd;

			
	
		String reqRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

		Opportunity newOpp = new Opportunity();
		newOpp.Name = 'New ReqOpportunities';
		newOpp.Accountid = clientAcc.Id;
		newOpp.RecordTypeId = reqRecordTypeID;
		Date closingdate = system.today();
		newOpp.CloseDate = closingdate.addDays(25);
		newOpp.StageName = 'Draft';
		newOpp.Req_Total_Positions__c = 2;
		newOpp.Req_Job_Title__c = 'Salesforce Architect';
		newOpp.EnterpriseReqSkills__c = '[{"name":"Salesforce.com","favorite":false},{"name":"Lightning","favorite":false},{"name":"Apex","favorite":false}]';
		newOpp.Req_Skill_Details__c = 'Salesforce';
		newOpp.Req_Job_Description__c = 'Testing';
		newOpp.Req_Pay_Rate_Max__c = 900;
		newOpp.Req_Pay_Rate_Min__c = 901;
		newOpp.Pay_Rate_Max_Multi_Currency__c = 900;
		newOpp.Pay_Rate_Min_Multi_Currency__c = 901;
		newOpp.Req_Bill_Rate_Max__c = 300000;
		newOpp.Req_Bill_Rate_Min__c = 100000;
		newOpp.Currency__c = 'USD - U.S Dollar';
		newOpp.Req_EVP__c = 'test EVP';
		newOpp.Req_Job_Level__c = 'Expert Level';
		newOpp.Req_Rate_Frequency__c = 'Hourly';
		newOpp.Req_Duration_Unit__c = 'Year(s)';
		newOpp.Req_Qualification__c = 'Additional Skill - database';
		newOpp.Req_Hiring_Manager__c = con.Name;
		newOpp.Opco__c = 'Aerotek, Inc';
		newOpp.Req_Division__c = 'Aerotek CE';
		newOpp.BusinessUnit__c = 'Board Practice';
		newOpp.Req_Product__c = 'Permanent';
		newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
		newOpp.Req_Worksite_Street__c = '987 Hidden St';
		newOpp.Req_Worksite_City__c = 'Baltimore';
		newOpp.Req_Worksite_Postal_Code__c = '21228';
		newOpp.Req_Worksite_Country__c = 'United States';
		newOpp.Req_Duration_Unit__c = 'Day(s)';
        newOpp.Legacy_Product__c =prd.Id;
		insert newOpp;

		List<Job_Posting__c> jpList = new List<Job_Posting__c> ();
		Job_Posting__c jp = new Job_Posting__c();

		jp.Job_Title__c = 'Test Administrator';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Contract';
		jp.Industry_Sector__c = 'Accounting';
		jp.External_Job_Description__c = 'Test Administrator';
		jp.Salary_Frequency__c = null;
		jp.Posting_Status__c = 'Delivery In Progress';
		jp.Opportunity__c = newOpp.Id;
		jp.opco__c = 'AERO';
		jpList.add(jp);

		jp = new Job_Posting__c();
		jp.Job_Title__c = 'Test Architect';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Permanent';
		jp.Industry_Sector__c = 'Accounting';
		jp.External_Job_Description__c = 'Design Salesfoce solution';
		jp.Salary_Frequency__c = null;
		jp.Posting_Status__c = 'Expiry In Progress';
		jp.opco__c = 'AERO';
		jp.Opportunity__c = newOpp.Id;
		jpList.add(jp);

		jp = new Job_Posting__c();
		jp.Job_Title__c = 'Test Engineer';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Permanent';
		jp.Industry_Sector__c = 'Accounting';
		jp.External_Job_Description__c = 'Design Salesfoce solution';
		jp.Salary_Frequency__c = null;
		jp.Posting_Status__c = 'Active';
		jp.opco__c = 'AERO';
		jp.Opportunity__c = newOpp.Id;
		jp.OFCCP_Flag__c = true;
		jpList.add(jp);

		jp = new Job_Posting__c();
		jp.Job_Title__c = 'Test Architect';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Permanent';
		jp.Industry_Sector__c = 'Accounting';
		jp.External_Job_Description__c = 'Design Salesfoce solution';
		jp.Salary_Frequency__c = null;
		jp.Posting_Status__c = 'Inactive';
		jp.opco__c = 'AERO';
		jp.Opportunity__c = newOpp.Id;
		jpList.add(jp);
		
		//Job Posting without Opportunity 
		jp = new Job_Posting__c();
		jp.Job_Title__c = 'Quality Analyst';
		jp.Skills__c = 'Selenium';
		jp.Allegis_Employment_Type__c = 'Permanent';
		jp.Industry_Sector__c = 'Accounting';
		jp.External_Job_Description__c = 'Design Automation Framework';
		jp.Salary_Frequency__c = null;
		jp.Posting_Status__c = 'Active';
		jp.opco__c = 'AERO';
		jpList.add(jp);

		insert jpList;
		Map<String, ID> jpMap = new Map<String, Id>(); 
		for(Job_Posting__c jpost : [select Id, Posting_Status__c, Name, Opportunity__c from Job_Posting__c  ]) {
			System.debug('`````'+jpost);
			if(jpost.Posting_Status__c == 'Active' && jpost.Opportunity__c == null) {
				jpMap.put('Active_Opp_Del', jpost.Id);
			}
			else {
				jpMap.put(jpost.Posting_Status__c, jpost.Id);
			}
			
		}

		List<Job_Posting_Channel_Details__c> jpcdList = new List<Job_Posting_Channel_Details__c> ();
		Job_Posting_Channel_Details__c jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '7302';
		jpcd.Board_Name__c = 'PhenomPeople (Aerotek)';
		jpcd.Board_Posting_Id__c = '7302234';
		jpcd.Board_Status__c = 'Queued';
		jpcd.Job_Posting_Id__c = jpMap.get('Delivery In Progress');
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(- 10);
		jpcd.Attempts__c = 0;
		jpcd.Auto_Expiry_Date__c = System.now().addMinutes(- 5);

		jpcdList.add(jpcd);

		jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '1235';
		jpcd.Board_Name__c = 'Recruitics';
		jpcd.Board_Posting_Id__c = '7302294';
		jpcd.Board_Status__c = 'Queued';
		jpcd.Job_Posting_Id__c = jpMap.get('Delivery In Progress');
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(- 10);
		jpcd.Attempts__c = 0;

		jpcdList.add(jpcd);

		jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '1235';
		jpcd.Board_Name__c = 'Recruitics';
		jpcd.Board_Posting_Id__c = '73033294';
		jpcd.Board_Status__c = 'Expiry In Progress';
		jpcd.Job_Posting_Id__c = jpMap.get('Expiry In Progress');
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(- 10);
		jpcd.Attempts__c = 0;
		jpcdList.add(jpcd);

		jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '1235';
		jpcd.Board_Name__c = 'Recruitics';
		jpcd.Board_Posting_Id__c = '73066296';
		jpcd.Board_Status__c = 'Posted';
		jpcd.Job_Posting_Id__c = jpMap.get('Active');
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(- 10);
		jpcd.Attempts__c = 0;
		jpcdList.add(jpcd);

		jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '1235';
		jpcd.Board_Name__c = 'Recruitics';
		jpcd.Board_Posting_Id__c = '73099299';
		jpcd.Board_Status__c = 'Posted';
		jpcd.Job_Posting_Id__c = jpMap.get('Active_Opp_Del');
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(- 10);
		jpcd.Attempts__c = 0;

		jpcdList.add(jpcd);

		insert jpcdList;



	}

	@Istest
	public static void emeaTestDataSetup() {
		DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo', Value__c = 'AG_EMEA');
		insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update', Value__c = 'False');
		insert DRZSettings1;
		List<Opportunity> lstOpportunitys = new List<Opportunity> ();
		Account clientAcc = TestData.newAccount(1, 'Client');
		insert clientAcc;

		Contact con = TestData.newContact(clientAcc.id, 1, 'Recruiter');
		Insert con;

		String reqRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

		Opportunity newOpp = new Opportunity();
		newOpp.Name = 'New EMEA Req';
		newOpp.Accountid = clientAcc.Id;
		newOpp.RecordTypeId = reqRecordTypeID;
		Date closingdate = system.today();
		newOpp.CloseDate = closingdate.addDays(25);
		newOpp.StageName = 'Draft';
		newOpp.Req_Total_Positions__c = 2;
		newOpp.Req_Job_Title__c = 'Salesforce Architect';
		newOpp.EnterpriseReqSkills__c = '[{"name":"Salesforce.com","favorite":false},{"name":"Lightning","favorite":false},{"name":"Apex","favorite":false}]';
		newOpp.Req_Skill_Details__c = 'Salesforce';
		newOpp.Req_Job_Description__c = 'Testing';
		newOpp.Req_Pay_Rate_Max__c = 900;
		newOpp.Req_Pay_Rate_Min__c = 901;
		newOpp.Pay_Rate_Max_Multi_Currency__c = 900;
		newOpp.Pay_Rate_Min_Multi_Currency__c = 901;
		newOpp.Req_Bill_Rate_Max__c = 300000;
		newOpp.Req_Bill_Rate_Min__c = 100000;
		newOpp.Currency__c = 'GBP - British Pound';
		newOpp.Req_EVP__c = 'test EVP';
		newOpp.Req_Job_Level__c = 'Expert Level';
		newOpp.Req_Rate_Frequency__c = 'Hourly';
		newOpp.Req_Duration_Unit__c = 'Year(s)';
		newOpp.Req_Qualification__c = 'Additional Skill - database';
		newOpp.Req_Hiring_Manager__c = con.Name;
		newOpp.Opco__c = 'AG_EMEA';
		newOpp.Req_Division__c = 'TEKsystems';
		newOpp.BusinessUnit__c = 'Board Practice';
		newOpp.Req_Product__c = 'Permanent';
		newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
		newOpp.Req_Worksite_Street__c = '2 Brunswick Square';
		newOpp.Req_Worksite_City__c = 'Birmingham';
		newOpp.Req_Worksite_Postal_Code__c = '21228';
		newOpp.Req_Worksite_Country__c = 'United Kingdom';
		newOpp.Req_Duration_Unit__c = 'Day(s)';

		insert newOpp;

		List<Job_Posting__c> jpList = new List<Job_Posting__c> ();
		Job_Posting__c jp = new Job_Posting__c();
		jp.eQuest_Job_Id__c = '333333';
		jp.Job_Title__c = 'EMEA Test';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Contract';
		jp.Job_Category__c = 'Applications - Database';
		//jp.Job_Sub_Category__c= 'Data Architect';
		jp.Industry_Sector__c = 'Accounting';
		jp.Req_Division__c='TEKsystems';
		jp.External_Job_Description__c = 'Test Administrator';
		jp.Salary_Frequency__c = null;
		jp.Posting_Status__c = 'Active';
		jp.Opportunity__c = newOpp.Id;
		jp.opco__c = 'AG_EMEA';
		jp.Country__c = 'United Kingdom';
		jp.State__c =  'Birmingham';
		jp.City__c = 'Birmingham';
		jp.ONET_Codes__c = 	'Accounting';
		jp.Industry_Code__c = 'Accounting';
		jp.Job_Boards__c = '[{"userOfficeCode":"01271","tooltipTxt":" (60 of 80 postings available)","selected":true,"isslot":true,"isOverLimit":true,"isBSD":true,"disabled":"false","boardName":"Seek","boardImgId":"SEEK_LOGO","boardId":"3075","boardIconUrl":"/resource/1620227022000/JOB_BOARD_LOGOS/JOB_BOARD_LOGOS/SEEK_LOGO.png"},{"userOfficeCode":"01271","tooltipTxt":" (No board limit)","selected":true,"isslot":null,"isOverLimit":true,"isBSD":false,"disabled":"true","boardName":"EMEA_TEKsystems","boardImgId":"TEK_SYSTEMS_LOGO","boardId":"80129","boardIconUrl":"/resource/1620227022000/JOB_BOARD_LOGOS/JOB_BOARD_LOGOS/TEK_SYSTEMS_LOGO.png"},{"userOfficeCode":"01271","tooltipTxt":" (986 of 999 postings available)","selected":false,"isslot":null,"isOverLimit":true,"isBSD":false,"disabled":"false","boardName":"CV Library (Ireland)","boardImgId":"CV_LIBRARY","boardId":"3002","boardIconUrl":"/resource/1620227022000/JOB_BOARD_LOGOS/JOB_BOARD_LOGOS/CV_LIBRARY.png"}]';
		jpList.add(jp);


		insert jpList;
		emeaChannelDetailTestData(jpList.get(0).id);


	}

	public static void emeaChannelDetailTestData(Id jpId) {
		List<Job_Posting_Channel_Details__c>  jpcdList = new List<Job_Posting_Channel_Details__c>();
		Job_Posting_Channel_Details__c jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '3025';
		jpcd.Board_Name__c = 'Sleek';
		jpcd.Board_Posting_Id__c = '302511';
		jpcd.Board_Status__c = 'Queued';
		jpcd.Job_Posting_Id__c = jpId;
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(-10);
		jpcd.Attempts__c = 0;
        jpcd.Retry_Limit__c = 5;
		jpcd.Auto_Expiry_Date__c = System.now().addMinutes(-5);

		jpcdList.add(jpcd);

		jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '3001';
		jpcd.Board_Name__c = 'Monster (UK)';
		jpcd.Board_Posting_Id__c = '300111';
		jpcd.Board_Status__c = 'Queued';
		jpcd.Job_Posting_Id__c = jpId;
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(-10);
		jpcd.Attempts__c = 0;

		jpcdList.add(jpcd);

		jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '3002';
		jpcd.Board_Name__c = 'CV Library (Ireland)';
		jpcd.Board_Posting_Id__c = '300211';
		jpcd.Board_Status__c = 'Expiry In Progress';
		jpcd.Job_Posting_Id__c = jpId;
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(-10);
		jpcd.Attempts__c = 0;

		jpcdList.add(jpcd);

		jpcd = new Job_Posting_Channel_Details__c();
		jpcd.Board_Expiration_Date__c = System.today().addDays(14);
		jpcd.Board_Id__c = '3024';
		jpcd.Board_Name__c = 'Monster (UK)';
		jpcd.Board_Posting_Id__c = '302411';
		jpcd.Board_Status__c = 'deferred';
		jpcd.Job_Posting_Id__c = jpId;
		jpcd.Posted_Date__c = System.today();
		jpcd.Next_Schedule_Flag__c = true;
		jpcd.Next_Scheduled_Run__c = System.now().addMinutes(-10);
		jpcd.Attempts__c = 0;

		jpcdList.add(jpcd);

		insert jpcdList;
	}

	@Istest
	public static void emeaChannelListTestData() {
		List<Job_Posting_Channel_List__c> jpclList = new List<Job_Posting_Channel_List__c> ();
		Id emeaRecordTypeId = Utility.getRecordTypeId('job_posting_channel_List__c', 'EMEA');

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3001', Board_Name__c = 'Monster (UK)',
		                                             Board_Limit__c = 5, User_Office_Code__c = '01077', Permission_Set__c = 'EMEAMonster',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'Aston Carter',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3001', Board_Name__c = 'Monster (UK)',
		                                             Board_Limit__c = 5, User_Office_Code__c = '00993', Permission_Set__c = 'EMEAMonster',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'Aston Carter',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3002', Board_Name__c = 'CV Library (Ireland)',
		                                             Board_Limit__c = 2, User_Office_Code__c = '00992', Permission_Set__c = 'EMEACVLibrary',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3003', Board_Name__c = 'Computerjobs.ie',
		                                             Board_Limit__c = 3, User_Office_Code__c = '00991', Permission_Set__c = 'EMEAComputerJobs',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3003', Board_Name__c = 'Computerjobs.ie',
		                                             Board_Limit__c = 3, User_Office_Code__c = '00992', Permission_Set__c = 'EMEAComputerJobs',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3002', Board_Name__c = 'CV Library (UK)',
		                                             Board_Limit__c = 13, Office_Limit__c = 5, User_Office_Code__c = '00993', Permission_Set__c = '',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'Aston Carter',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 5, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3002', Board_Name__c = 'CV Library (UK)',
		                                             Board_Limit__c = 13, Office_Limit__c = 5, User_Office_Code__c = '01077', Permission_Set__c = '',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'Aston Carter',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 13,
		                                             Consumed_Office_Limit__c = 5, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3002', Board_Name__c = 'CV Library (UK)',
		                                             Board_Limit__c = 13, User_Office_Code__c = '01271', Permission_Set__c = '',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'Aston Carter',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 13,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3024', Board_Name__c = 'CW Jobs', Type__c = 'Inventory',
													 Board_Limit__c = 13, Office_Limit__c = 5, User_Office_Code__c = '00991', Permission_Set__c = '',													 
		                                             JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0, Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '30224', Board_Name__c = 'efc',Type__c = 'Inventory',
													 Board_Limit__c = 13, Office_Limit__c = 5, User_Office_Code__c = '00991', Permission_Set__c = '',													 
		                                             JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00', isBSD__C = true,
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0, Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3025', Board_Name__c = 'Seek',
		                                             User_Office_Code__c = '01271', Permission_Set__c = '', Type__c = 'Slot',
		                                             JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 3, Consumed_Office_Limit__c = 4, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3004', Board_Name__c = 'CW Jobs',
		                                             User_Office_Code__c = '00993', Permission_Set__c = '', Type__c = 'Inventory',
		                                             JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0, Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3004', Board_Name__c = 'CW Jobs',
		                                             User_Office_Code__c = '00995', Permission_Set__c = '', Type__c = 'Inventory',
		                                             JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0, Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3004', Board_Name__c = 'CW Jobs',
		                                             User_Office_Code__c = '01271', Permission_Set__c = '', Type__c = 'Inventory',
		                                             JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0, Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3005', Board_Name__c = 'Jobcloud.ch (Jobs.ch)',
		                                             Board_Limit__c = 5, User_Office_Code__c = '01383', Permission_Set__c = '', Type__c = 'Slot',
		                                             JobBoard_Frequency__c = 'NA', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0, Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3005', Board_Name__c = 'Jobcloud.ch (Jobs.ch)',
		                                             Board_Limit__c = 3, User_Office_Code__c = '01384', Permission_Set__c = '', Type__c = 'Slot',
		                                             JobBoard_Frequency__c = 'NA', Division__c = 'Aerotek', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0, Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3002', Board_Name__c = 'CV Library (Ireland)',
		                                             Board_Limit__c = 2, User_Office_Code__c = '00991', Permission_Set__c = 'EMEACVLibrary',
		                                             Type__c = 'Inventory', JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Board_Image_Id__c = 'ALLEGIS_GROUP_LOGO',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'All', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));
		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '80130', Board_Name__c = 'EMEA_Aerotek', Type__c = 'Inventory',
		                                             Board_Limit__c = 100, User_Office_Code__c = 'NOCOD', Division__c = 'Aerotek', Expiry_Interval__c = 14,
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'AG_EMEA', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '80129', Board_Name__c = 'EMEA_TEKsystems', Type__c = 'Inventory',
		                                             Board_Limit__c = 100, User_Office_Code__c = '01271', Division__c = 'TEKsystems', Expiry_Interval__c = 21, 
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'AG_EMEA', Retry_Limit__c = 3, Consumed_Board_Limit__c = 0,
		                                             Consumed_Office_Limit__c = 0, isActive__c=true));

		jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3075', Board_Name__c = 'Seek',
		                                             User_Office_Code__c = '01271', Permission_Set__c = '', Type__c = 'Slot',
		                                             JobBoard_Frequency__c = 'Weekly', Division__c = 'TEKsystems', Schedule_Time_In_UTC__c = '00:00',
		                                             OPCO__c = 'AG_EMEA', Retry_Limit__c = 3, Consumed_Board_Limit__c = 3, Consumed_Office_Limit__c = 4, isActive__c=true));
        
        jpclList.add(new job_posting_channel_List__c(RecordTypeId = emeaRecordTypeId, Board_Id__c = '3002', Board_Name__c = 'CV Library (Ireland))',
		                                             Board_Limit__c = 100, User_Office_Code__c = '00991', Division__c = 'TEKsystems',
		                                             Schedule_Time_In_UTC__c = '00:00', OPCO__c = 'AG_EMEA', Retry_Limit__c = 3,Type__c = 'Slot', Consumed_Board_Limit__c = 1,
		                                             Consumed_Office_Limit__c = 1, isActive__c=true));
		insert jpclList;
	}

	public static void mockCallout(String googlPlaceId, Integer draftStatusCode) {

		String oAuthTokenMDTName = 'eQuestDraftNonProd';
		if (Utilities.getSFEnv() == 'prod') {
			oAuthTokenMDTName = 'eQuestDraftProd';
		}
		oAuth_Setting__mdt oAuthSetting = oAuth_Setting__mdt.getInstance(oAuthTokenMDTName);

		String oAuthResponse = '{"token_type":"Bearer","expires_in":"35","ext_expires_in":"35","expires_on":"15","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
		+ '"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';
		String googleRespnse = '{"html_attributions":[],"result":{"address_components":[{"long_name":"Peterhead","short_name":"Peterhead","types":["locality","political"]},{"long_name":"Aberdeenshire","short_name":"Aberdeenshire","types":["administrative_area_level_2","political"]},{"long_name":"Scotland","short_name":"Scotland","types":["administrative_area_level_1","political"]},{"long_name":"United Kingdom","short_name":"GB","types":["country","political"]}]},"status":"OK"}';
		Integer respStatusCode = 200;
		Integer commitStatusCode = 202;
		String contentType = 'application/json';
		String draftResp = '{"id":"78965"}';
		String commitCallResp = '{"SUCCESS":"SUCCESS"}';
		//String oauthResponse = JSON.serialize(oTokenMap);

		List<String> boardList = new List<String> ();
		boardList.add('3003');

		String googleUrl = 'https://maps.googleapis.com/maps/api/place/details/json?key='
		+ System.Label.ATS_GOOGLE_API_KEY
		+ '&placeid=' + googlPlaceId;
		String draftURL = oAuthSetting.Endpoint_URL__c;
		String commitCallURL = draftURL + '/78965/postings';
		String oauthTokenUrl = oAuthSetting.Token_URL__c;

		CalloutMockService oAuthTokenMock = new CalloutMockService(oAuthResponse, contentType, respStatusCode);
		CalloutMockService googleMock = new CalloutMockService(googleRespnse, contentType, respStatusCode);
		CalloutMockService draftMock = new CalloutMockService(draftResp, contentType, draftStatusCode);
		CalloutMockService commitCallMock = new CalloutMockService(commitCallResp, contentType, commitStatusCode);

		MultipleCalloutMockService multiMockSerivce = new MultipleCalloutMockService();
		multiMockSerivce.addCalloutToMock(oauthTokenUrl, oAuthTokenMock);
		multiMockSerivce.addCalloutToMock(googleUrl, googleMock);
		multiMockSerivce.addCalloutToMock(draftURL, draftMock);
		multiMockSerivce.addCalloutToMock(commitCallURL, commitCallMock);
		Test.setMock(HttpCalloutMock.CLASS, multiMockSerivce);

	}

	public static void emeaCommitCallMock(String eQuestDraftId, Integer commitStatusCode) {

		String oAuthTokenMDTName = 'eQuestDraftNonProd';
		if (Utilities.getSFEnv() == 'prod') {
			oAuthTokenMDTName = 'eQuestDraftProd';
		}
		oAuth_Setting__mdt oAuthSetting = oAuth_Setting__mdt.getInstance(oAuthTokenMDTName);

		String oAuthResponse = '{"token_type":"Bearer","expires_in":"35","ext_expires_in":"35","expires_on":"15","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
		+ '"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';
		Integer respStatusCode = 200;
		String contentType = 'application/json';
		String commitCallResp = '{"SUCCESS":"SUCCESS"}';
		String draftURL = oAuthSetting.Endpoint_URL__c;
		String commitCallURL = draftURL + '/' + eQuestDraftId + '/postings';
		String oauthTokenUrl = oAuthSetting.Token_URL__c;

		CalloutMockService oAuthTokenMock = new CalloutMockService(oAuthResponse, contentType, respStatusCode);
		CalloutMockService commitCallMock = new CalloutMockService(commitCallResp, contentType, commitStatusCode);

		MultipleCalloutMockService multiMockSerivce = new MultipleCalloutMockService();
		multiMockSerivce.addCalloutToMock(oauthTokenUrl, oAuthTokenMock);
		multiMockSerivce.addCalloutToMock(commitCallURL, commitCallMock);
		Test.setMock(HttpCalloutMock.CLASS, multiMockSerivce);
	}
}