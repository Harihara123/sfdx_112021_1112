public with sharing class JobTitleTriggerHandler  {

private static boolean hasBeenProccessed = false;

   public void OnAfterInsert(List<Job_Title__c> newRecords) {
       callDataExport(newRecords);
    }
     
     
    public void OnAfterUpdate (List<Job_Title__c> newRecords) {        
       callDataExport(newRecords);
    }

	public void OnAfterDelete (Map<Id, Job_Title__c>oldMap) {    
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'JobTitleTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' Job_Title__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Job_Title__c');
            hasBeenProccessed = true; 
        }
    }

    private static void callDataExport(List<Job_Title__c> newRecords) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'JobTitleTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' Job_Title__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Job_Title__c');
            hasBeenProccessed = true; 
        }

    } 
}