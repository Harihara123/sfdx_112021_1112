@isTest
private class Job_PostingTrigger_Test{
  static testMethod void test_Job_PostingTrigger(){
   test.startTest();
    Job_Posting__c job_posting_Obj = new Job_Posting__c();
    Insert job_posting_Obj; 
   test.stopTest();
  }

  static testMethod void test_UseCase1(){
   test.startTest();
    Job_Posting__c job_posting_Obj = new Job_Posting__c(); 
    job_posting_Obj.Job_Title__c = 'Test Record';
    Insert job_posting_Obj;
    System.assertEquals('Test Record',[SELECT id, Job_Title__c FROM Job_Posting__c where Id=:job_posting_Obj.Id].Job_Title__c);
    job_posting_Obj.Job_Title__c = 'test123';
    update job_posting_Obj; 
    System.assertEquals('test123',[SELECT id, Job_Title__c FROM Job_Posting__c where Id=:job_posting_Obj.Id].Job_Title__c);
    job_posting_Obj.Job_Title__c = 'test123';
    Delete job_posting_Obj; 
    test.stopTest();
}
}