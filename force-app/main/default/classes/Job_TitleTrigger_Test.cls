@isTest
private class Job_TitleTrigger_Test{
  static testMethod void test_Job_TitleTrigger(){
   test.startTest();
    Job_Title__c job_title_Obj = new Job_Title__c();
    job_title_Obj.Active__c = true;
    Insert job_title_Obj; 
   test.stopTest();
  }

  static testMethod void test_UseCase1(){
   test.startTest();
    Job_Title__c job_title_Obj = new Job_Title__c();
    job_title_Obj.Active__c = false;
    insert job_title_Obj;
    System.assertEquals(false,[SELECT id, Active__c FROM Job_Title__c where Id=:job_title_Obj.Id].Active__c);
    job_title_Obj.Active__c = true;
    update job_title_Obj; 
    System.assertEquals(true,[SELECT id, Active__c FROM Job_Title__c where Id=:job_title_Obj.Id].Active__c);
    job_title_Obj.Active__c = true;
    Delete job_title_Obj; 
    test.stopTest();
}
}