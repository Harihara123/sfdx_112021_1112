@IsTest
public class LInkedInProspectNotesWSTest  {
    public static String seatHolderId = '123456789';

    @isTest
    public static void buildRequestParams_givenParams_shouldReturnRequest() {
        LinkedInProspectNotesWS ws = new LinkedInProspectNotesWS('test', seatHolderId);
        ws.beforeDatetime = 'SomeDate';

        String expected = 'callout:LinkedInRecruiterEndpoint/prospectNotes?q=criteria&contract=urn:li:contract:245756743&owners=urn:li:seat:123456789&beforeDate=SomeDate&start=0&count=25';
        String result;

        Test.startTest();
            result = ws.buildRequestParams();
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return request');
    }
}