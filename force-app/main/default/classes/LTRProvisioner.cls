public class LTRProvisioner implements Database.Batchable<sObject> {
    public String query;

    public class LTRProvisionerException extends Exception {}
    
    public LTRProvisioner() {
        this.query = System.Label.LTR_Provisioning_Query;
    }

    public LTRProvisioner(String query) {
        this.query = query;
    }

    public Database.querylocator start(Database.BatchableContext bc) { 
        return Database.getQueryLocator(query);  
    } 

    public void execute(Database.BatchableContext bc, List<User> scope) {
        System.debug('Scope: '+scope);
        if (scope.size() > 0) {
            try {
                PermissionSet grpBPermSet = [select Id from PermissionSet where Label = 'User Group B'][0];

                List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
                for (User u: scope) {
                    PermissionSetAssignment psa = new PermissionSetAssignment(
                        PermissionSetId = grpBPermSet.Id, 
                        AssigneeId = u.Id
                    );
                    psaList.add(psa);
                }

                System.debug('Size = ' + psaList.size());

                ConnectedLog.LogSack sack = new ConnectedLog.LogSack();
                Database.SaveResult[] srList = Database.insert(psaList, false);
                for (Integer i=0; i<srList.size(); i++) {
                    if (!srList[i].isSuccess()) {
                        Database.Error err = srList[i].getErrors()[0];
                        sack.AddException('LTRProvisioner', 'execute', psaList[i].AssigneeId, new LTRProvisionerException(err.getMessage()));
                    }
                }

                sack.Write();
            } catch(Exception e) {
                ConnectedLog.LogException('LTRProvisioner', 'execute', e);
            }
        }
    }

    public void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CompletedDate
                                          FROM AsyncApexJob WHERE Id = :bc.getJobId()];
        Map<String, String> mss = new Map<String, String>{
            'JobId' => a.Id,
            'Status' => a.Status,
            'NumberOfErrors' => String.valueOf(a.NumberOfErrors),
            'JobItemsProcessed' => String.valueOf(a.JobItemsProcessed),
            'TotalJobItems' => String.valueOf(a.TotalJobItems),
            'CompletedDate' => String.valueOf(a.CompletedDate)
        };
        ConnectedLog.LogInformation('LTRProvisioner', 'execute', 'LTR Provisioning batch complete', mss);
    }

}