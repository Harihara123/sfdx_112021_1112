public without sharing class LWCSubmittalController {

public class SubmittalWrapper{
@AuraEnabled
public List<Order> submittalList;
@AuraEnabled
public Map<String, List<String>> statusMap;
@AuraEnabled
public Boolean recordUpdated;

@AuraEnabled
public Boolean exceptionFlag;
}
@AuraEnabled
    public static SubmittalWrapper getOrderRecords(String recordId, String pageRef, String currentTab){
        system.debug('--record Id for OrderRecords--'+recordId);
        String filterClause;
        try{
            if(pageRef=='Opportunity' && ! currentTab.containsIgnoreCase('application')){
                
                filterClause='OpportunityId =: recordId AND Status != \'Applicant\'';
                
            }else if(pageRef!='Opportunity' && ! currentTab.containsIgnoreCase('application')){
                
                filterClause='ShipToContactId =: recordId AND Status != \'Applicant\'';           
                
            }else if(pageRef=='Opportunity' &&  currentTab.containsIgnoreCase('application')){
                
                filterClause='OpportunityId =: recordId AND (Has_Application__c = true OR Status = \'Applicant\')';           
                
            }else if(pageRef!='Opportunity' &&  currentTab.containsIgnoreCase('application')){
                
                filterClause='ShipToContactId =: recordId AND (Has_Application__c = true OR Status = \'Applicant\')';           
            }
            
                
            
            String soql = 'SELECT Id, Job_Title__c, Account.Name, Hiring_Manager__c, Hiring_Manager__r.Name, Opportunity.Req_Job_Title__c,Status,Opportunity.Name,'
                            +'Submittal_Not_Proceeding_Reason__c,Comments__c,Start_Date__c,ShipToContactId, ShipToContact.Related_Contact__c, ShipToContact.Source_System_Id__c,AccountId,'
                            +'Opportunity.Req_OFCCP_Required__c, Has_Application__c, ShipToContact.MailingState, ShipToContact.MailingCountry,ShipToContact.MailingPostalCode,'
                            +'Bill_Rate__c, Pay_Rate__c, Burden__c, Rate_Frequency__c, Salary__c, Bonus__c,Submittal_Placement_Source__c, Source_system_id__c, Start_Form_ID__c,' 
                            +'Additional_Compensation__c, Opportunity.Currency__c, Opportunity.Req_Total_Filled__c,'
                            +'Opportunity.Req_Open_Positions__c, Opportunity.Req_Total_Positions__c,Opportunity.Account_City__c,'
                            +'Opportunity.Req_Total_Lost__c, Display_LastModifiedDate__c,LastModifiedBy.Name,CreatedById,'
                            +'Opportunity.Account_name_Fourmula__c,OpportunityId, Opportunity.AccountId,Opportunity.RecordTypeId,'
                            +'Opportunity.Req_OpCo_Code__c, Opportunity.Req_Total_Washed__c,Opportunity.Req_Product__c,'
                            +'Opportunity.RecordType.Name, Opportunity.Req_origination_System_Id__c, Opportunity.Req_Origination_Partner_System_Id__c,'
                            +'Opportunity.OpCo__c,Opportunity.OpCo_Id__c, Opportunity.StageName, RecordType.DeveloperName, RecordTypeId, Delivery_Center__c,'
                            +'DeliveryOffice__c,DeliveryOffice__r.Name,ShipToContact.Name, ShipToContact.AccountId, EffectiveDate, Submittal_Applicant_Source__c,'  
                            +' (select Id,Source__c,JB_Source_Name__c from Events) from Order'
                            +' where '+filterClause+'  order by Display_LastModifiedDate__c desc';
            
            List<Order> orderList = Database.query(soql);
                if(orderList.size()>0){
                    Set<String> opcoSet = new Set<String>();
                    for(Order o : orderList){
                        // Need to update OpCo__c to OpCo_Id__c for US S-216852
                        if(o.Opportunity.OpCo__c != null){                           
                            opcoSet.add(o.Opportunity.OpCo__c);
                        }
                    }
                    opcoSet.add('Proactive');
					 // Need to update OpCo__c to OpCo_Id__c for US S-216852
                    List<Submittal_Status_Matrix__mdt> statusMatrix =[SELECT Id, Label, OpCo__c, Current_Status__c, Available_Status__c 
                                                                        FROM Submittal_Status_Matrix__mdt  
                                                                        where OpCo__c IN: opcoSet];
                    SubmittalWrapper wrapObj = new SubmittalWrapper();
                    wrapObj.submittalList = new List<Order>();
                    wrapObj.statusMap = new Map<String,List<String>>();
                    for(Submittal_Status_Matrix__mdt  obj : statusMatrix){
                        
                        wrapObj.statusMap.put(obj.OpCo__c+'-'+obj.Current_Status__c,LWCSubmittalController.getListofString(String.valueOf(obj.Available_Status__c)));
                    }
                    wrapObj.submittalList.addAll(orderList);
                    
                    return wrapObj;
                }else{
                    return null;
                }

           }catch(exception ex){
               system.debug(ex.getMessage() + ex.getStackTraceString());
               return null;
           } 
    }

    @AuraEnabled    
    public static SubmittalWrapper updateSubmittalRecord(Order orderObj, String recordId, String pageRef, Integer recordCount, String currentTab){
        Savepoint sp = Database.setSavepoint(); 
        SubmittalWrapper wrapObj = new  SubmittalWrapper();
        try{
            String filterClause;
            if(pageRef=='Opportunity' && ! currentTab.containsIgnoreCase('application')){
                
                filterClause='OpportunityId =: recordId AND Status != \'Applicant\'';
                
            }else if(pageRef!='Opportunity' && ! currentTab.containsIgnoreCase('application')){
                
                filterClause='ShipToContactId =: recordId AND Status != \'Applicant\'';           
                
            }else if(pageRef=='Opportunity' &&  currentTab.containsIgnoreCase('application')){
                
                filterClause='OpportunityId =: recordId AND (Has_Application__c == true OR Status == \'Applicant\')';           
                
            }else if(pageRef!='Opportunity' &&  currentTab.containsIgnoreCase('application')){
                
                filterClause='ShipToContactId =: recordId AND (Has_Application__c == true OR Status == \'Applicant\')';           
            }       
            if(orderObj != null){
                update orderObj;
            }
            String soql = 'select count(Id) total from Order'
                            +' where '+filterClause;
            List<AggregateResult> result  = Database.query(soql);
            System.debug(result[0].get('total'));
            if( Integer.valueOf(result[0].get('total')) != recordCount){
                wrapObj = LWCSubmittalController.getOrderRecords(recordId, pageRef, currentTab);
                wrapObj.recordUpdated = true;               
            }
            return wrapObj;
                
        }catch(Exception e){
            System.debug(e.getMessage());
            Database.rollback(sp);
            wrapObj.exceptionFlag = true;
            return wrapObj;
        }

    }

    @AuraEnabled
    public static List<Event> getOrderHistoryRecords(String recordId){
        system.debug('--record Id for History OrderRecords--'+recordId);
        List<Event> orderHistoryList = new List<Event>();
        orderHistoryList =[SELECT Id, AccountId,CreatedById,CreatedDate,Description,EndDate,CreatedBy.Name,
                                    Interview_Candidate_Comments__c,Interview_Client_Comments__c,Interview_Status__c,
                                    Interview_Type__c,Not_Proceeding_Reason__c,OwnerId,StartDateTime,EndDateTime,
                                    Subject,Submittal_Additional_Field__c,Source__c,
                                    Submittal_Interviewers__c,Type,WhatId,WhoId 
                                    FROM Event where WhatId=: recordId order by CreatedDate desc];

        return orderHistoryList;
        
    }
    @AuraEnabled
    public static boolean updateOrderHistoryRecord(Event ev){
    //  System.debug('ev-->'+ev);
        Savepoint sp = Database.setSavepoint(); 
        
     try{
            update ev;
            return true;    
        }catch(Exception e){
            System.debug(e.getMessage());
            Database.rollback(sp);
            return false;
        }

    }

    public static List<String> getListofString(String targets){
        List<String> lst = new List<String>();
            if (targets.length() > 0) {
                String formattedStr = targets.substring(1, targets.length() - 1);
                List<String> lstStatuses = formattedStr.split(',');
                for(String s : lstStatuses) {
                    lst.add(s);          
                }
            }
            return lst;
    }
}