public with sharing class LeadTriggerHandler {
    String afterInsert  = 'afterInsert';
    String afterUpdate  = 'afterUpdate';
    Map<Id, Lead> oldMap = new Map<Id, Lead>();
    Map<Id, Lead> newMap = new Map<Id, Lead>();

     //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Lead> newRecords) {
        
        // LeadTriggeredSends(afterInsert,oldMap,newMap,newRecords);
        
    }
 
   
    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Lead>oldMap, Map<Id, Lead>newMap) {
        List<Lead> newRecords = newMap.values();
       // LeadTriggeredSends(afterUpdate,oldMap,newMap,newRecords);
    }


   /*public void LeadTriggeredSends(String Context, Map<Id, Lead>oldMap, Map<Id, Lead>newMap,List<Lead> newRecords) {
        if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
            System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
            et4ae5.triggerUtility.automate('Lead');
       } 
    }*/ 
}