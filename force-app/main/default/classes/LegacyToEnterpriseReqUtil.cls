/***************************************************************************
Name        : LegacyToEnterpriseReqUtil
Created By  : Preetham Uppu 
Date        : 25 May 2017
Story       : CRMX-2694
Purpose     : Class that contains all of the functionality called by 
              Trigger/Batch. 
*****************************************************************************/
public class LegacyToEnterpriseReqUtil{

 Set<Id> legacyReqIds = new Set<Id>();
 public Map<Id, Reqs__c> reqMap;
 List<Log__c> errorLogs = new List<Log__c>();
   public Set<Id> errorIdSet = new Set<Id> ();
  public Map<Id,String> errorLogMap = new Map<Id,String>(); 
  public Map<Id,String> errorLogMap2 = new Map<Id,String>();
  List<String> errorLst = new List<String>();
  public Map<Id,String> insertErrorsMap = new Map<Id,String>();
  public Map<Id,String> exceptionMap = new Map<Id,String>();
  
 
 public LegacyToEnterpriseReqUtil(Set<Id> legacyIds){
     legacyReqIds = legacyIds;
     reqMap =  new Map<Id, Reqs__c>([select Id, Name, Orig_Partner_System_Id__c,Origination_System__c, VMS_End_Client__c,
                                        VMS__c, VMS_Requirement_Status__c, VMS_Requirement_Closed_Reason__c, Product__r.OpCo__c,Product__r.Job_Code__c,
                                        VMS_Additional_information__c, VMS_Job_Description__c,Job_Code__c, Max_Submissions_Per_Supplier__c,
                                        Req_Origination_System__c,ACCOUNT__C,ACCOUNT__r.Name,ACCOUNT__r.Account_Country__c,ACCOUNT__r.Account_Zip__c,ACCOUNT__r.Account_Latitude__c,ACCOUNT__r.Account_Longitude__c, CLIENT_JOB_TITLE__C, PRIMARY_CONTACT__C, 
                                        POSITIONS__C, CITY__C, ADDRESS__C, ZIP__C, JOB_DESCRIPTION__C,Req_Approval_Process__c,Merged_Req_ID__c, Merged_Req_System__c,Merged_with_VMS_Req__c,Template__c,Decline_Reason__c,SourceReq__c,System_SourceReq__c,Additional_Information__c,Client_working_on_Req__c,Performance_Expectations__c,Work_Environment__c,Interview_Information__c,End_Client__c,Project_Stage_Lifecycle_Info__c,External_Job_Description__c,Direct_Placement_Fee__c,Draft_Reason__c,Qualified_Date__c,EVP__c,Proactive_Req_Link__c,PLACEMENT_TYPE__C,STAGE__C,Opportunity__c,Additional_Burden__c,OT_Bill_Rate_Max__c,OT_Bill_Rate_Min__c,
                                        ORGANIZATION_OFFICE__C, START_DATE__C,Interview_Date__c, STATE__C, JOB_CATEGORY__C, LOCATION__C,
                                        ORGANIZATIONAL_ROLE__C, REQ_PRIORITY__C,PRODUCT__C,STATUS__C,CLOSE_DATE__C,Service_Product_Offering__c,
                                        OPCO__C,WORKSITE_COUNTRY__C, DIVISION_NAME__C,Export_Controls_Compliance_required__c,Delivery_Type__c,Siebel_ID__c,
                                        BACKGROUND_CHECK__C,Payroll__c,BILL_RATE_MAX__C,BILL_RATE_MIN__C,SUB_VENDOR__C,COMPLIANCE__C,Secondary_Delivery_Office__c,
                                        DAYS_OPEN__C,DRUG_TEST__C,DURATION__C,DURATION_UNIT__C,EXCLUSIVE__C,GOVERNMENT__C,Top_3_Skills__c,Date_Customer_Opened_Position__c,
                                        GO_TO_WORK__C,MAX_SPREAD__C,MIN_SPREAD__C,OFCCP_REQUIRED__C,Business_Challenge__c,Is_Auto_Closed__c,RecordType.Name,
                                        OPEN_POSITIONS__C,PAY_RATE_MAX__C,PAY_RATE_MIN__C,QUALIFICATIONS__C,Non_technical_Skills__c,Nice_to_Have_Skills__c,Competition_Info__c,Internal_External_Customer__c,RRC_Positions__c,Impact_to_the_Internal_External_Customer__c,Why_Position_Open__c,RRC_Open_Date__c,
                                        RED_ZONE__C,MAX_SALARY__C,MIN_SALARY__C,KEYWORDS__C,Origination_System_Id__c,Opportunity_driven_mostly_by_LOB_or_EIT__c,
                                        STANDARD_BURDEN__C,FILLED__C,LOSS__C,WASH__C, createdbyId, createddate, lastmodifiedbyId, 
                                        LASTMODIFIEDDATE, ownerId, recordtype.Id,EnterpriseReqId__c,Req_Disposition__c,Can_support_OPT_STEM_OPT__c,Is_International_Travel_Required__c,Organization_Office__r.name,Backfill__c,Employment_Alignment__c,Final_Decision_Maker__c,
                                        (select Id,Top_Match__c,Code__c from ONET_Matches__r where Top_Match__c = TRUE LIMIT 1)
                                        from Reqs__c where Id IN : legacyReqIds]);
 }
 
 public  ReqSyncErrorLogHelper  copyLegacyReqToEnterpriseReq(){
   List<Opportunity> enterpriseReqs = new List<Opportunity>();
   List<Opportunity> enterpriseExistingReqs = new List<Opportunity>();
   List<Log__c> errors = new List<Log__c>();
   Map<String,String> oppMap = new Map<String,String>();
    
      Map<String,Id> reqSourceSystemMap = new Map<String,Id>();
      Map<Id,Id> existingOppMap = new Map<Id,Id>();
      Map<Id,Opportunity> existingMapOpportunity = new Map<Id,Opportunity>();
      
       if(reqMap.size() > 0){ 
            
        Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>([select Id,Req_Legacy_Req_Id__c,Req_Worksite_Country__c,Req_Worksite_Postal_Code__c,Req_GeoLocation__Latitude__s,Req_GeoLocation__Longitude__s ,account.Account_Latitude__c,account.Account_Longitude__c,account.Account_Country__c,account.Account_Zip__c from Opportunity where Req_Legacy_Req_Id__c =:reqMap.keyset()]);
            if(opportunityMap.size() > 0){
             for(Opportunity opty: opportunityMap.values()){
               if(opty.Req_Legacy_Req_Id__c!= null)
                  existingOppMap.put(opty.Req_Legacy_Req_Id__c,opty.Id);
                    existingMapOpportunity.put(opty.Req_Legacy_Req_Id__c,opty);
             }
            }
              for(Reqs__c r : reqMap.values()){
                  Opportunity opp = new Opportunity();
                  
                  opp.AccountId = r.ACCOUNT__C;
                  if(existingMapOpportunity  == null || (existingMapOpportunity != null && !existingMapOpportunity.ContainsKey(r.Id)))
                  {
                      system.debug('r.ZIP__C:::'+r.ZIP__C);
                      system.debug('r.ACCOUNT__r.Account_Zip__c:::::'+r.ACCOUNT__r.Account_Zip__c);
                      system.debug('r.WORKSITE_COUNTRY__C::::'+r.WORKSITE_COUNTRY__C);
                      system.debug('r.ACCOUNT__r.Account_Country__c::::'+r.ACCOUNT__r.Account_Country__c);
                      if(r.ZIP__C == r.ACCOUNT__r.Account_Zip__c && r.WORKSITE_COUNTRY__C == r.ACCOUNT__r.Account_Country__c)
                      {
                      system.debug('inside 1st condition' );
                            opp.Req_GeoLocation__Latitude__s  = r.ACCOUNT__r.Account_Latitude__c;
                            opp.Req_GeoLocation__Longitude__s  = r.ACCOUNT__r.Account_Longitude__c;
                      }
                      else
                      {
                            opp.Req_GeoLocation__Latitude__s  = null;
                            opp.Req_GeoLocation__Longitude__s  = null;
                      }
                  }
                  else
                  {
                      if(existingMapOpportunity.ContainsKey(r.Id))
                      {
                          if(r.WORKSITE_COUNTRY__C == existingMapOpportunity.get(r.id).account.Account_Country__c && r.ZIP__C == existingMapOpportunity.get(r.id).account.Account_Zip__c)
                            {
                                system.debug('inside 2nd condition');
                                opp.Req_GeoLocation__Latitude__s  = existingMapOpportunity.get(r.id).account.Account_Latitude__c;
                                opp.Req_GeoLocation__Longitude__s  = existingMapOpportunity.get(r.id).account.Account_Longitude__c;
                            }
                            else 
                            {
                                opp.Req_GeoLocation__Latitude__s  = null;
                                opp.Req_GeoLocation__Longitude__s  = null;
                            }
                      }
                  }
                  //Query Job Title
                  String legacyTitle;
                  if(r.CLIENT_JOB_TITLE__C != null){
                     if(r.CLIENT_JOB_TITLE__C.length() > 80){
                        legacyTitle = r.CLIENT_JOB_TITLE__C.substring(0,80);
                      }else{
                        legacyTitle = r.CLIENT_JOB_TITLE__C;
                      }
                  }else{
                      legacyTitle = r.CLIENT_JOB_TITLE__C;
                  }
                  
                  if(!String.isBlank(legacyTitle)){
                 opp.Req_Job_Title__c = legacyTitle;
                 }
                   
                  
                  if(r.EnterpriseReqId__c != null || (existingOppMap.size() > 0 && existingOppMap.ContainsKey(r.Id))){
                    opp.Id = r.EnterpriseReqId__c != null ? r.EnterpriseReqId__c : existingOppMap.get(r.Id);
                  }
                  
                  if(r.PLACEMENT_TYPE__C == 'Direct Placement' || r.PLACEMENT_TYPE__C == 'Internal Hire'){
                     opp.Req_Product__c = 'Permanent';
                   }else{
                     opp.Req_Product__c = r.PLACEMENT_TYPE__C;
                  }
                 opp.Req_Hiring_Manager__c = r.PRIMARY_CONTACT__C;
                 
                 opp.Currency__c = 'USD - U.S Dollar';
                 String totalPositions;
                 if(r.POSITIONS__C != null ){
                   totalPositions = String.valueOf(r.POSITIONS__C);
                   if(totalPositions.length() > 4){
                    totalPositions = totalPositions.substring(0,4);                 
                    opp.Req_Total_Positions__c = Integer.valueOf(totalPositions);
                 }else{
                   opp.Req_Total_Positions__c = r.POSITIONS__C;
                 }}
                 
                 //Opportunity_driven_mostly_by_LOB_or_EIT__c
                 if(!String.isBlank(r.Opportunity_driven_mostly_by_LOB_or_EIT__c)){
                 opp.REQ_Opportunity_driven_mostly_by_LOB__c= r.Opportunity_driven_mostly_by_LOB_or_EIT__c;
                 }
                 
                 if(!string.isBlank(r.CITY__C)){
                    if(r.CITY__C.length() > 80){
                       opp.Req_Worksite_City__c  = r.CITY__C.substring(0,80);
                     }else{
                       opp.Req_Worksite_City__c  = r.CITY__C;
                       }
                 }else{
                      opp.Req_Worksite_City__c  = r.CITY__C;
                 }
                 
                 opp.Req_Worksite_Street__c = r.ADDRESS__C;
                 opp.Req_Worksite_Postal_Code__c = r.ZIP__C;
                 opp.Req_Job_Description__c = r.JOB_DESCRIPTION__C;
                 opp.Organization_Office__c = r.ORGANIZATION_OFFICE__C;
                 
                  if(!String.isBlank(r.Competition_Info__c)){
                 opp.Req_Competition_Info__c = r.Competition_Info__c;
                 }
                 
                  if(!String.isBlank(r.Internal_External_Customer__c)){
                 opp.Req_Internal_External_Customer__c = r.Internal_External_Customer__c;
                 }
                 
                 if(r.Date_Customer_Opened_Position__c !=null){
                 opp.Req_Date_Client_Opened_Position__c= r.Date_Customer_Opened_Position__c;
                 }
                
                 
                  if(!String.isBlank(r.Business_Challenge__c)){
                 opp.Req_Business_Challenge__c = r.Business_Challenge__c;
                 }
                 
                  if(!String.isBlank(r.Impact_to_the_Internal_External_Customer__c)){
                 opp.Req_Impact_to_Internal_External_Customer__c = r.Impact_to_the_Internal_External_Customer__c;
                 }
                 
                  if(!String.isBlank(r.Why_Position_Open__c)){
                 opp.Req_Why_Position_Open_Details__c = r.Why_Position_Open__c;
                 }
                 
                 if(r.RRC_Open_Date__c !=null){
                 opp.Req_RRC_Open_Date__c = r.RRC_Open_Date__c;
                 }
                 
                 if(r.RRC_Positions__c !=null){
                     opp.Req_RRC_Positions__c = r.RRC_Positions__c;
                     
                 }
                 
                 if(r.Service_Product_Offering__c !=null){
                     opp.Req_Service_Product_Offering__c = r.Service_Product_Offering__c;
                 }
                 
                 if(r.Delivery_Type__c !=null){
                     opp.Req_Delivery_Type__c =r.Delivery_Type__c;
                 }   
            
                 if(r.Secondary_Delivery_Office__c !=null){
                     opp.Req_Secondary_Delivery_Office__c = r.Secondary_Delivery_Office__c;
                     
                 }
                 
                  if(!String.isBlank(r.Top_3_Skills__c)){
                 opp.Req_Skill_Details__c = r.Top_3_Skills__c;
                 }
                 
                 //New fields for mapping
                  if(!String.isBlank(r.Req_Approval_Process__c)){
                 opp.Req_Approval_Process__c = r.Req_Approval_Process__c;
                 }
                 
                  if(!String.isBlank(r.Merged_Req_ID__c)){
                 opp.Req_Merged_Req_ID__c = r.Merged_Req_ID__c;
                 }
                 
                 if(!String.isBlank(r.Merged_Req_System__c)){
                 opp.Req_Merged_Req_System__c = r.Merged_Req_System__c;
                 }
                 
                 //checkbox field null handling
                 opp.Req_Merged_with_VMS_Req__c = r.Merged_with_VMS_Req__c == true? true:false;
                 opp.Req_Template__c = r.Template__c == true? true:false;
                 opp.Legacy_Record_Type__c = r.RecordType.Name;
                 
                 if(r.Decline_Reason__c != null){
                 opp.Req_Decline_Reason__c = r.Decline_Reason__c;
                 }
                 
                  if(!String.isBlank(r.SourceReq__c)){
                 opp.Req_SourceReq__c = r.SourceReq__c;
                 }
                 
                 if(!String.isBlank(r.System_SourceReq__c)){
                 opp.Req_System_SourceReq__c = r.System_SourceReq__c;
                 }
                 
                  if(!String.isBlank(r.Additional_Information__c)){
                 opp.Req_Additional_Information__c = r.Additional_Information__c;
                 }
                 
                 if(r.Client_working_on_Req__c != null){
                 opp.Req_Client_working_on_Req__c = r.Client_working_on_Req__c;
                 }
                 
                  if(!String.isBlank(r.Performance_Expectations__c)){
                 opp.Req_Performance_Expectations__c = r.Performance_Expectations__c;
                 }
                 
                  if(!String.isBlank(r.Work_Environment__c)){
                 opp.Req_Work_Environment__c = r.Work_Environment__c;
                 }
                 
                 if(!String.isBlank(r.Interview_Information__c)){
                 opp.Req_Interview_Information__c  = r.Interview_Information__c;
                 }
                 
                 if(r.End_Client__c != null){
                 opp.Req_End_Client__c = r.End_Client__c;
                 }
                 
                  if(!String.isBlank(r.Project_Stage_Lifecycle_Info__c)){
                 opp.Req_Project_Stage_Lifecycle_Info__c = r.Project_Stage_Lifecycle_Info__c;
                 }
                 
                  if(!String.isBlank(r.External_Job_Description__c)){
                 opp.Req_External_Job_Description__c= r.External_Job_Description__c;
                 }
                 
                 if(r.Direct_Placement_Fee__c!= null){
                 opp.Req_Flat_Fee__c =  r.Direct_Placement_Fee__c;
                 }
                 
                 if(r.Draft_Reason__c != null){
                 opp.Req_Draft_Reason__c  = r.Draft_Reason__c ;
                 }
                 
                 if(!String.isBlank(r.EVP__c)){
                 opp.Req_EVP__c  = r.EVP__c;
                 }
                                   
                  if(r.Proactive_Req_Link__c != null){
                 opp.Req_Proactive_Req_Link__c = r.Proactive_Req_Link__c;
                 }
                 
                 if(r.Opportunity__c != null){
                 opp.Req_Opportunity__c = r.Opportunity__c;
                 }
                 
                 decimal additionalBurden= r.Additional_Burden__c == null? 0 :r.Additional_Burden__c;
                 decimal standardBurden= r.Standard_Burden__c == null? 0 :r.Standard_Burden__c ;
                 
                 opp.Req_Standard_Burden__c = additionalBurden + standardBurden ;
                 
                 decimal OTBillRateMax = r.OT_Bill_Rate_Max__c == null? 0 :r.OT_Bill_Rate_Max__c ;
                 decimal OTBillRateMin = r.OT_Bill_Rate_Min__c == null ? 0 :r.OT_Bill_Rate_Min__c ;
                 decimal BillRateMax   = r.Bill_Rate_Max__c == null ? 0 :r.Bill_Rate_Max__c;
                 decimal BillRateMin   = r.Bill_Rate_Min__c== null ? 0 :r.Bill_Rate_Min__c;
                 
                 decimal denominator = BillRateMax + BillRateMin ; 
                 
                 if(denominator !=0){
                 opp.OT_Multiplier__c = String.valueOf(((OTBillRateMax + OTBillRateMin)/(BillRateMax + BillRateMin)).setscale(2));
                 }

                 
                 if(r.Qualified_Date__c !=null){
                 opp.Req_Qualified_Date__c = r.Qualified_Date__c;
                 }
                 
                 
                 //Interview Date
                 if(r.Interview_Date__c != null){
                   opp.Req_Interview_Date__c = r.Interview_Date__c;
                 }
                 
                 if(r.START_DATE__C != null){
                   opp.Start_Date__c = r.START_DATE__C;
                 }
                 
                 if(r.Siebel_ID__c != null){
                   opp.External_Oppty_Id__c = r.Siebel_ID__c;
                 }                
                  
                 opp.Req_Is_Auto_Closed__c = r.Is_Auto_Closed__c; 
                  
                 
                 opp.Req_Worksite_State__c = r.STATE__C;
                 
                 if(r.CLOSE_DATE__C != null){
                   opp.CloseDate = r.CLOSE_DATE__C;
                 }else if(r.CLOSE_DATE__C == null && r.START_DATE__C != null){
                   opp.CloseDate = r.START_DATE__C;
                 }else if(r.CLOSE_DATE__C == null && r.START_DATE__C == null){
                    DateTime startDate = r.createddate;
                    startDate = startDate.adddays(90);
                    opp.CloseDate = date.newinstance(startDate.year(), startDate.month(), startDate.day());
                 }
                 
                 //opco
              	if(r.Stage__c == 'Staging' && r.Status__c == 'Open'){
                  opp.StageName = 'Staging';
                 }else if(r.Stage__c == 'Staging' && r.Status__c == 'Closed'){
                  opp.StageName = 'Closed Wash';
                 }else if(r.Stage__c == 'Draft' && r.Status__c == 'Open'){
                  opp.StageName = 'Draft';
                 }else if(r.Stage__c == 'Qualified' && r.Status__c == 'Open'){
                  opp.StageName = 'Qualified';
                 }else if(((r.Stage__c == 'Draft' && r.Status__c == 'Closed') || (r.Stage__c == 'Qualified' && r.Status__c == 'Closed'))
                                            && (r.Filled__c  > = r.Positions__c)){
                   opp.StageName = 'Closed Won';
                 }else if(((r.Stage__c == 'Draft' && r.Status__c == 'Closed') || (r.Stage__c == 'Qualified' && r.Status__c == 'Closed'))
                                            && (r.Filled__c  > 0 && r.Filled__c < r.Positions__c)){
                   opp.StageName = 'Closed Won (Partial)';
                 }else if(((r.Stage__c == 'Draft' && r.Status__c == 'Closed') || (r.Stage__c == 'Qualified' && r.Status__c == 'Closed'))
                                            && (r.Filled__c  == 0)){
                       if(r.Loss__c > 0){
                               opp.StageName = 'Closed Lost';
                       }else{
                               opp.StageName = 'Closed Wash';
                       }
                 }
                 
                 if(r.Product__r != null){
                     if(r.Product__r.OpCo__c == 'Aerotek' || r.Product__r.OpCo__c == 'Aerotek, Inc' || r.Product__r.OpCo__c == 'Aerotek, Inc.' ){
                       opp.OpCo__c = 'Aerotek, Inc';
                     }else if(r.Product__r.OpCo__c == 'Allegis Global Solutions, Inc.' || r.Product__r.OpCo__c == 'Allegis Global Solutions, Inc'){
                       opp.OpCo__c = 'Allegis Global Solutions, Inc';
                     }else if(r.Product__r.OpCo__c == 'Allegis Group, Inc.' || r.Product__r.OpCo__c == 'Allegis Group, Inc'){
                       opp.OpCo__c = 'Allegis Group, Inc';
                     }else if(r.Product__r.OpCo__c == 'TEKsystems, Inc.' || r.Product__r.OpCo__c == 'TEKsystems, Inc'){
                       opp.OpCo__c = 'TEKsystems, Inc.';
                     }
                   }
                 
                 opp.Req_Worksite_Country__c = r.WORKSITE_COUNTRY__C;
                 //Terms of engagement
                 if(r.Placement_Type__c == 'Contract' && r.Exclusive__c == true){
                   opp.Req_Terms_of_engagement__c  = 'Contingent Exclusive';
				   opp.Internal_Hire__c = 'No';
                 }else if(r.Placement_Type__c == 'Contract' && r.Exclusive__c == false){
                   opp.Req_Terms_of_engagement__c  = 'Contingent Non-Exclusive';
				   opp.Internal_Hire__c = 'No';
                 }else if(r.Placement_Type__c == 'Contract to Hire' && r.Exclusive__c == true){
                   opp.Req_Terms_of_engagement__c  = 'Contingent Exclusive';
				   opp.Internal_Hire__c = 'No';
                 }else if(r.Placement_Type__c == 'Contract to Hire' && r.Exclusive__c == false){
                   opp.Req_Terms_of_engagement__c  = 'Contingent Non-Exclusive';
				   opp.Internal_Hire__c = 'No';
                 }else if(r.Placement_Type__c == 'Direct Placement' && r.Exclusive__c == true){
                   opp.Req_Terms_of_engagement__c  = 'Retained Exclusive';
				   opp.Internal_Hire__c = 'No';
                 }else if(r.Placement_Type__c == 'Direct Placement' && r.Exclusive__c == false){
                   opp.Req_Terms_of_engagement__c  = 'Retained Non-Exclusive';
				   opp.Internal_Hire__c = 'No';
                 }else if(r.Placement_Type__c == 'Internal Hire'){
                     opp.Internal_Hire__c = 'Yes';
                 }
                 
                 
                 /*
                if(String.isBlank(r.EnterpriseReqId__c)){
                 String opportunityName ='';
                 opportunityName = 'LEGACY - '+r.ACCOUNT__r.Name+' - '+r.CLIENT_JOB_TITLE__C;
                 if(opportunityName.length() > 80){
                  opp.Name = opportunityName.substring(0,80);
                 }else{
                   opp.Name = opportunityName;
                 }}*/
                 
                 
                 opp.Req_Division__c= !String.IsBlank(r.DIVISION_NAME__C) && r.DIVISION_NAME__C == 'RPO' ? 'RPO (Recruitment Process Outsourcing)':r.DIVISION_NAME__C;
                
                 
                  //opp.Req_Days_Open__c = r.Days_Open__c;
                 if(r.BACKGROUND_CHECK__C == 'Yes'){
                   opp.Req_Background_Check_Required__c = true;
                 }
                 
                 
                 opp.Req_Bill_Rate_Max__c = r.BILL_RATE_MAX__C;
                 opp.Req_Bill_Rate_Min__c = r.BILL_RATE_MIN__C;
                 opp.Req_Can_Use_Approved_Sub_Vendor__c = r.Sub_Vendor__c;
                 
                 if(!string.isBlank(r.Compliance__c)) {
                    if(r.Compliance__c.length() > 255){
                           opp.Req_Compliance__c = r.Compliance__c.substring(0,255);
                        }else{
                            opp.Req_Compliance__c = r.Compliance__c;
                        }
                 }else{
                   opp.Req_Compliance__c = r.Compliance__c;
                 }
                    
                
                 //opp.Req_Days_Open__c = r.Days_Open__c;
                 if(r.Drug_Test__c == 'Yes'){
                   opp.Req_Drug_Test_Required__c = true;
                 }
                 
                  opp.Req_Payroll__c=r.Payroll__c;
                 
                 string durationstr;
                 if(r.Duration__c != null){
                    durationstr = string.valueOf(r.Duration__c);
                   if(durationstr.length() > 4){
                    opp.Req_Duration__c = Decimal.valueOf(durationstr.substring(0,4));
                   }else{
                     opp.Req_Duration__c = r.Duration__c;
                   }
                 }else{
                    opp.Req_Duration__c = r.Duration__c;
                 }
                 
                 
                 if(r.Duration_Unit__c == 'Days'){
                   opp.Req_Duration_Unit__c = 'Day(s)';
                 }else if(r.Duration_Unit__c == 'Hours'){
                   opp.Req_Duration_Unit__c = 'Hour(s)';
                 }else if(r.Duration_Unit__c == 'Weeks'){
                   opp.Req_Duration_Unit__c = 'Week(s)';
                 }else if(r.Duration_Unit__c == 'Months'){
                   opp.Req_Duration_Unit__c = 'Month(s)';
                 }else if(r.Duration_Unit__c == 'Years'){
                   opp.Req_Duration_Unit__c = 'Year(s)';
                 }else if(r.Duration_Unit__c == 'None'){
                   opp.Req_Duration_Unit__c = 'None';
                 }else if(r.Duration_Unit__c == null){
                   opp.Req_Duration_Unit__c = '';
                 }
                 
                 opp.Req_Exclusive__c = r.EXCLUSIVE__C;
                 opp.Req_Government__c = r.GOVERNMENT__C;
                 opp.Immediate_Start__c = r.GO_TO_WORK__C;
                 
                 
                 //opp.Req_Max_Spread__c = r.MAX_SPREAD__C;
                 opp.Req_OFCCP_Required__c = r.OFCCP_REQUIRED__C;
                 opp.RecordTypeID ='01224000000kMQ4AAM' ;
                 opp.OwnerId = r.OwnerId;
                 opp.Req_Pay_Rate_Max__c = r.PAY_RATE_MAX__C;
                 opp.Req_Pay_Rate_Min__c = r.PAY_RATE_MIN__C;
                 opp.Req_Rate_Frequency__c='Hourly';
                 
                 if(!String.isBlank(r.QUALIFICATIONS__C)){
                 opp.Req_Qualification__c = r.QUALIFICATIONS__C;
                 }
                 
                  if(!String.isBlank(r.Non_technical_Skills__c)){
                 opp.Req_Qualification__c = r.Non_technical_Skills__c;
                 }
                 
                  if(!String.isBlank(r.Nice_to_Have_Skills__c)){
                 opp.Req_Qualification__c = r.Nice_to_Have_Skills__c;
                 }
                 
				 if(String.isNotBlank(r.Req_Disposition__c)){
				  opp.Req_Disposition__c = r.Req_Disposition__c;
				 }
				
				if(String.isNotBlank(r.Is_International_Travel_Required__c)){
                     opp.Is_International_Travel_Required__c = r.Is_International_Travel_Required__c;
				}
                 
                if(String.isNotBlank(r.Organization_Office__c)){
					if(r.Organization_Office__r.Name == System.Label.TEK_Radnor_PA_Office && r.RecordTypeId == '012U0000000DWpJIAW'){
						opp.National__c = true;
					}else if(r.Organization_Office__r.Name != System.Label.TEK_Radnor_PA_Office && r.RecordTypeId == '012U0000000DWpJIAW'){
						opp.National__c = false;
					}
				}
                  
				if(String.isNotBlank(r.Employment_Alignment__c)){
					opp.Employment_Alignment__c = r.Employment_Alignment__c;
				}
                  
				if(String.isNotBlank(r.Final_Decision_Maker__c)){
					opp.Final_Decision_Maker__c = r.Final_Decision_Maker__c;
				}
                  
				if(String.isNotBlank(r.Location__c)){
					opp.GlobalServices_Location__c = r.Location__c;
				}
				
				if(r.recordtype.name == 'Global Services Req'){
                    opp.Req_TGS_Requirement__c = 'Yes';
                }else if(r.recordtype.name == 'Staffing Req'){
                    opp.Req_TGS_Requirement__c = 'No';
                }
                 opp.Backfill__c = r.Backfill__c; 
                 opp.Req_Red_Zone__c = r.RED_ZONE__C;
                 opp.Req_Salary_Max__c = r.MAX_SALARY__C;
                 opp.Req_Salary_Min__c = r.MIN_SALARY__C;
                 opp.Req_Search_Tags_Keywords__c = r.KEYWORDS__C;
                 
                 String reqSkillTags = r.KEYWORDS__C;
                 List<SkillsWrapper> entReqSkills = new List<SkillsWrapper>();
                 
                 if(!String.IsBlank(reqSkillTags)){
                 
                  if(!string.isBlank(opp.EnterpriseReqSkills__c)){
                     entReqSkills = (List<SkillsWrapper>)JSON.deserializeStrict(opp.EnterpriseReqSkills__c, List<SkillsWrapper>.Class);
                  }
                   
                    if(reqSkillTags.containsAny(',') || reqSkillTags.containsAny(';')){
                      List<String> skList = reqSkillTags.split('[,;]');
                      for(String s: skList){
                       SkillsWrapper pill = new SkillsWrapper(); 
                        if(!String.isBlank(s)) {
                            if(s.toLowerCase().equalsIgnoreCase('and') || s.toLowerCase().equalsIgnoreCase('or')){
                               //System.debug('each and or string '+s);
                             }else{
                                  pill.name = s;
                                  pill.favorite = true;
                                  pill.index = 0;
                                  pill.suggestedSkill = false;
                                  entReqSkills.add(pill);
                            }   
                         } 
                      }
                    }else{
                    
                       String reqProperSkills = reqSkillTags.replaceAll('[^a-zA-Z0-9#.]', ' ');
                       List<String> reqList = reqProperSkills.split('[\\s]');
                       for(String s: reqList){
                         SkillsWrapper pill = new SkillsWrapper(); 
                          if(!String.isBlank(s)){
                            if(s.toLowerCase().equalsIgnoreCase('and') || s.toLowerCase().equalsIgnoreCase('or')){
                               //System.debug('each and or string '+s);
                            }else{
                                  pill.name = s;
                                  pill.favorite = true;
                                  pill.index = 0;
                                  pill.suggestedSkill = false;
                                  entReqSkills.add(pill);
                            }   
                          } 
                      }
                    
                     }
                 }
                 
                  
                 if(entReqSkills.size() > 0){
                   opp.EnterpriseReqSkills__c = JSON.serialize(entReqSkills);
                 }
                 
                 //formula source system Id
                 if(r.EnterpriseReqId__c == null)
                    opp.Source_System_Id__c = r.Origination_System_Id__c;
                 opp.Req_Standard_Burden__c = r.STANDARD_BURDEN__C;
                 if(r.FILLED__C != null){
                   string totalFilledValue;
                   totalFilledValue = string.valueOf(r.FILLED__C);
                   opp.Req_Total_Filled__c =!string.isBlank(totalFilledValue) && totalFilledValue.length()> 4 ? Decimal.valueOf(totalFilledValue .substring(0,4)) : r.FILLED__C;
                   
                 }else{
                    opp.Req_Total_Filled__c = r.FILLED__C;
                 }
                 
                 if(r.LOSS__C != null){
                    string lossValue;
                    lossValue = string.valueOf(r.LOSS__C);
                    opp.Req_Total_Lost__c = !string.isBlank(lossValue) && lossValue.length()> 4 ? Decimal.valueOf(lossValue.substring(0,4)) : r.LOSS__C;
                 }else{
                    opp.Req_Total_Lost__c = r.LOSS__C;
                 }
                 
                 if(r.WASH__C != null){
                  string washValue;
                  washValue = string.valueOf(r.WASH__C);
                  opp.Req_Total_Washed__c =!string.isBlank(washValue) && washValue.length()> 4 ? Decimal.valueOf(washValue.substring(0,4)):  r.WASH__C;
                 }else{
                   opp.Req_Total_Washed__c = r.WASH__C;
                 }
                 
                 System.debug('EXISTING'+existingOppMap.keyset());
                 
                 //(existingOppMap.size() == 0 && r.EnterpriseReqId__c==null) || (existingOppMap.size() > 0 && !existingOppMap.ContainsKey(r.Id))
                 
                 if((existingOppMap.size() == 0 && r.EnterpriseReqId__c==null) || (r.EnterpriseReqId__c==null && existingOppMap.size() > 0 && !existingOppMap.ContainsKey(r.Id))){
                     opp.CreatedByID = r.CreatedByID;
                     opp.CreatedDate = r.CreatedDate;
                     opp.LastModifiedByID = r.LastModifiedByID;
                     opp.LastModifiedDate = r.LastModifiedDate;
                 }
                 
                 opp.Req_Export_Controls_Questionnaire_Reqd__c = r.EXPORT_CONTROLS_COMPLIANCE_REQUIRED__C;
                 //SOC O NET
                 if(r.ONET_Matches__r.size() > 0){
                    opp.Req_ONet_SOC_Code__c = r.ONET_Matches__r[0].Code__c;
                 }else{
                    opp.Req_ONet_SOC_Code__c = null;
                 }
                 
                 //VMS or Legacy
                 opp.Req_Legacy_Req_Id__c = r.Id;
                 opp.Req_Legacy_Req_Name__c = r.Name;
                 opp.Req_Origination_Partner_System_Id__c = r.Orig_Partner_System_Id__c;
                 opp.Req_Origination_System__c = r.Req_Origination_System__c;
                 opp.Req_VMS_END_CLIENT__c = r.VMS_End_Client__c;
                 opp.Req_VMS__c = r.VMS__c;
                 opp.Req_VMS_Requirement_Status__c = r.VMS_Requirement_Status__c;
                 opp.Req_VMS_Requirement_Closed_Reason__c = r.VMS_Requirement_Closed_Reason__c;
                 opp.Req_Max_Submissions_Per_Supplier__c = r.Max_Submissions_Per_Supplier__c;
                 opp.Req_VMS_Additional_Information__c = r.VMS_Additional_information__c;
                 opp.Req_VMS_Job_Description__c = r.VMS_Job_Description__c;
                 opp.Legacy_Product__c = r.Product__c;
                 opp.Req_OPT__c = r.Can_support_OPT_STEM_OPT__c;
                 
                 oppMap.put(r.Origination_System_Id__c,opp.Name);
                 
                 if(r.EnterpriseReqId__c != null || (existingOppMap.size() > 0 && existingOppMap.ContainsKey(r.Id))){
                   enterpriseExistingReqs.add(opp);
                 }else{
                    opp.Name = '.';
                    enterpriseReqs.add(opp);
                 }
                System.debug('REQ FINAL'+enterpriseReqs.size() +'EXISTING'+enterpriseExistingReqs.size());
           }  
      }
    
    
    if(enterpriseReqs.size() > 0){
           Integer numOfUpserts = 0;          
            integer i = 1 ; 
            Integer totalRecords = 0;
            
           
            List<database.SaveResult> reqSaveResults = database.insert(enterpriseReqs ,false);
             
             for(Integer j=0; j < enterpriseReqs.size();j++){
                String errorRecord = 'Failed Legacy-EntReq Upsert Sync:';
                String exceptionMsg = '';
                if(!reqSaveResults[j].isSuccess()){
                 
                  if(reqSaveResults[j].getErrors()[0].getStatusCode() != null && reqSaveResults[j].getErrors()[0].getFields()!= null){
                    exceptionMsg += 'Error Reason: '+ String.ValueOf(reqSaveResults[j].getErrors()[0].getStatusCode())+ ' '+ ' Error Fields ' +  reqSaveResults[j].getErrors()[0].getFields();
                  }
                  errorRecord += exceptionMsg;
                  insertErrorsMap.put(enterpriseReqs[j].Req_Legacy_Req_Id__c,errorRecord);
                  }else if(!reqSaveResults[j].isSuccess()){
                   errorLogMap.put(enterpriseReqs[j].Req_Legacy_Req_Id__c, reqSaveResults[j].getErrors()[0].getStatusCode() != null && reqSaveResults[j].getErrors()[0].getFields()!= null ? String.ValueOf(reqSaveResults[j].getErrors()[0].getStatusCode())+' ' +  ' Error Fields ' +  reqSaveResults[j].getErrors()[0].getFields(): ' Either Status Code or Fields are Null ');
                }
              
              }
             
             if(errorLogMap.size() > 0)
                Core_Data.logBatchRecord('Legacy to Enterprise Sync',errorLogMap);
             
             if(insertErrorsMap.size() > 0)
                  Core_Data.logBatchRecord('Enterprise to Legacy Req inserts', insertErrorsMap);
  } // End of Insert If
  
 if(enterpriseExistingReqs.size() > 0){
        Integer numOfUpserts = 0;          
        integer i = 1 ; 
        Integer totalRecords = 0;
          Map<Id,String> errorLogMap2 = new Map<Id,String>();

             List<database.SaveResult> reqUpdateResults =  database.update(enterpriseExistingReqs,false);
             
              for(Integer y=0; y < enterpriseExistingReqs.size();y++){
                String errorRecord = 'Failed Legacy-Ent Update Sync:';
                String exceptionMsg = '';
                if(!reqUpdateResults[y].isSuccess()){
                  errorLogMap2.put(enterpriseExistingReqs[y].Req_Legacy_Req_Id__c, reqUpdateResults[y].getErrors()[0].getStatusCode() != null && reqUpdateResults[y].getErrors()[0].getFields()!= null ? String.ValueOf(reqUpdateResults[y].getErrors()[0].getStatusCode()) +' '+ ' Error Fields ' +  reqUpdateResults[y].getErrors()[0].getFields(): ' Either Status Code or Fields are Null ');
                }
             }
             
              if(errorLogMap2.size() > 0)
                 Core_Data.logBatchRecord('Legacy to Enterprise Sync',errorLogMap2);
            
             
   } // End of Update of Existing Reqs If Block
   
     if(errorLogMap.size() > 0){
           exceptionMap.putall(errorLogMap);
         }
         if(errorLogMap2.size() > 0){
           exceptionMap.putall(errorLogMap2);
         }
         if(insertErrorsMap.size() > 0){
           exceptionMap.putall(insertErrorsMap);
         }
       
        System.debug('ERROR MAP '+exceptionMap + 'LOG MAP'+errorLogMap +'LOG M2'+errorLogMap2);
        ReqSyncErrorLogHelper errorWrapper = new ReqSyncErrorLogHelper(exceptionMap,errorLst);
        System.debug('ERROR WRAPPER'+errorWrapper);
        return errorWrapper;
   

 } // End of Copy Legacy to ent Req Method

}