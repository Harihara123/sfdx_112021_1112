//Author: Krishna Chitneni
//Legacy Req to Enterprise Team Members Sync

public class LegacyToEnterpriseTeamMembersUtil{

  public Map<Id, Reqs__c> LegacyMap;
   Set<Id> EntReqIds = new Set<Id>();
   public List<Reqs__c> Reqlst;
   List<Req_Team_Member__c> lstReqTeamMembers = new List<Req_Team_Member__c>();
   public Set<Id> errorTeamMemberIdSet = new Set<Id>();
   public Map<Id,String> errorLogMap = new Map<Id,String>(); 
   List<String> errorLst = new List<String>();
   List<String> errorShareLst = new List<String>();
    List<String> errorMasterLst = new List<String>();
    public Map<Id,String> insertErrorsMap = new Map<Id,String>();
    public Map<Id,String> insertErrorShareMap = new Map<Id,String>();
    
   
  public LegacyToEnterpriseTeamMembersUtil(List<Req_Team_Member__c> lstQueriedReqTeamMembers){
    lstReqTeamMembers = lstQueriedReqTeamMembers;
  }

   public ReqSyncErrorLogHelper CopyLegacytoEnterpriseMembers() {
     Map<String,Id> ReqEntMap = new Map<String,Id>();
     Set<String> entSet = new Set<String>();
     if(lstReqTeamMembers.size() > 0){
       for(Req_Team_Member__c oTm: lstReqTeamMembers){
          if(oTm.Requisition__r.Origination_System_Id__c != null)
              entSet.add(oTm.Requisition__r.Origination_System_Id__c);
       }
     }
     
     List<OpportunityTeamMember> EntTeamLst = new List<OpportunityTeamMember>();
      //Query for Reqs
      Map<Id,Opportunity> mapENT = new Map<Id,Opportunity>([select Id, Req_origination_System_Id__c  from Opportunity where Req_origination_System_Id__c =: entSet]);
      //Loop through Reqs to Create Map
      for(Opportunity Ent : mapENT.values()){
          ReqEntMap.put(Ent.Req_origination_System_Id__c, Ent.Id);
      }
      
      //Create Opportunity Team Members from Req Team Members
      if(lstReqTeamMembers.size() > 0 && ReqEntMap.size() > 0){
        for(Req_Team_Member__c OppTm: lstReqTeamMembers){
           if(ReqEntMap.containskey(OppTm.Requisition__r.Origination_System_Id__c)){
              Id Oppid =  ReqEntMap.get(OppTm.Requisition__r.Origination_System_Id__c);
               OpportunityTeamMember  OppTeamMember =new OpportunityTeamMember(userid=oppTm.User__c,teammemberrole=OppTm.Requisition_Team_Role__c,opportunityid=Oppid);
               EntTeamLst.add(OppTeamMember);
           } 
        }
      }
      
      //Insert Opp Team Members
      if(EntTeamLst.size() > 0)
      {
              List<OpportunityShare> shares = new List<OpportunityShare>();
              Integer counter = 0;
              
                
                List<database.SaveResult> results =  database.insert(EntTeamLst,false);
                
                if(results.size() > 0){
                   for(Integer j=0; j < EntTeamLst.size(); j++){
                       String errorRecord = 'Failed Ent Member Sync ';
                       String exceptionMsg = 'Reason: ';
                         if(!results[j].isSuccess()){
                           errorRecord += EntTeamLst[j].userid +' \r\n';
                            if(results[j].getErrors()[0].getStatusCode() != null){
                             exceptionMsg  += results[j].getErrors()[0].getStatusCode();
                            }
                           errorRecord += exceptionMsg;
                           insertErrorsMap.put(EntTeamLst[j].userid,errorRecord);
                         }else{
                           shares.add(new OpportunityShare(UserOrGroupId = EntTeamLst[j].UserId,OpportunityId = EntTeamLst[j].OpportunityId,OpportunityAccessLevel = 'Edit'));
                         }
                   
                   }
                }
                
                if(shares.size() > 0)
                    {
                       
                       List<database.SaveResult> resultsShares = database.insert(shares,false);
                        
                        if(resultsShares.size() > 0){
                            for(Integer y=0; y < shares.size(); y++){
                               String errorRecord = 'Failed Ent Share Sync ';
                               String exceptionMsg = 'Reason: ';
                                 if(!resultsShares[y].isSuccess()){
                                   errorRecord += shares[y].UserOrGroupId +' \r\n';
                                    if(resultsShares[y].getErrors()[0].getStatusCode() != null){
                                     exceptionMsg  += resultsShares[y].getErrors()[0].getStatusCode();
                                    }
                                    errorRecord += exceptionMsg;
                                    insertErrorShareMap.put(shares[y].UserOrGroupId,errorRecord);
                                 }
                           }
                        }
                         
                   }
               //Insert the Error Log Map     
                  if(insertErrorsMap.size() > 0){
                     Core_Data.logBatchRecord('Legacy to Enterprise Team Members',insertErrorsMap );
                     errorLogMap.putall(insertErrorsMap);                  
                  }
                    
                  if(insertErrorShareMap.size() > 0){
                     Core_Data.logBatchRecord('Legacy to Enterprise Team Shares', insertErrorShareMap);
                      errorLogMap.putall(insertErrorShareMap); 
                  }         
       }
      
        ReqSyncErrorLogHelper errorWrapper = new ReqSyncErrorLogHelper(errorLogMap ,errorMasterLst);
        return errorWrapper;
      
   } // End of Copy method

}