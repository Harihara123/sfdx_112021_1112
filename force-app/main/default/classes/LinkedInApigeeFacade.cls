@RestResource(urlMapping='/LinkedIn/*')
global with sharing class LinkedInApigeeFacade {

	private static final String EXPORTCANDIDATEPROFILE = 'EXPORT_CANDIDATE_PROFILE';

	//assume authentication is handled on apigee and they will keep the connection alive

	@HttpGet
	global static void doGet() {

	}

	@HttpPost
	global static Integer doPost() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		if(req.requestBody != null){
            Map<String, Object> content = verifyRequest(req, res);
            if(content != null) {
                //for the first release the service will take a request body and always return status 200
                return 200; 
    		}
            else{
                //return an error if the request was made without a body or 
                return 400;
            }
        }else{
            return 500;
        }
	}

	private static Map<String, Object> verifyRequest(RestRequest req, RestResponse res) {
		try {
			Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
			res.statusCode = 200;
			return results;
		}
		catch (Exception e) {
			res.responseBody = Blob.valueOf(JSON.serialize(new Response(e.getMessage())));
			res.statusCode = 400;
			return null;
		}
	}

	public class Response {
		public String errorMessage;
		
		public Response(String message) {
			errorMessage = message;
		}
	}
}