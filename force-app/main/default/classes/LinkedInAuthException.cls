/**
* @author Ryan Upton Acumen Solutions for Allegis Group 
* @date 3/20/2018
* @group LinkedIn Functions
* @group-content LinkedinAuthException.html
* @description Exception thrown from LinkedIn integration classes when an authentication error occurs
*/ 
public with sharing class LinkedInAuthException extends Exception{
}