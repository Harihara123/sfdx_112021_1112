/**
* @author Ryan Upton Acumen Solutions for Allegis Group 
* @date 3/14/2018
* @group LinkedIn Functions
* @group-content LinkedinAuthHandlerForAts.html
* @description This class is used to handle the authentication between Salesforce and LinkedIn's Recruiter API
*   this class is used by other classes and methods that lookup, sync, link, unlink and delete candidates
*   from ATS and LinkedIn
*/ 
public with sharing class LinkedInAuthHandlerForAts {

    @TestVisible
    private String mAccessToken;
    /**
     * @description this is the named location of the protected custom setting containing LinkedIn's OAuth
     * client secret
     */
     @TestVisible
    private String mClientSecret;
    /**
     * @description this is the named location of the protected custom setting containing LinkedIn's OAuth
     * clientId
     */
    @TestVisible
    private String mClientId;
    /**
     * @description this is the named location of LinkedIn's OAuth authorization end point
     */
     @TestVisible
    private String mAuthorizationURL;
    /**
     * @description this is the expiry value for the oAuth token returned from the LinkedIn authorization URL. This denotes how long
     * the access token is valid
     */
    @TestVisible
    private String mExpiry;
    /**
     * @description local LinkedInEnviroment variable. The LinkedIn enivornment indicates the authentication configuration we're looking for DEV, TEST, PROD
     */
	@TestVisible
	private LinkedInEnvironment mEnvironment;
	
	public LinkedInAuthHandlerForAts(LinkedInEnvironment environment){
		mEnvironment = environment;
	}
	/**
    * @description Looks up the protectected custom setting for consumer secret. This is needed to complete OAuth 
    * connection to LinkedIn and generate a Bearer Toke for follow-up API calls to LinkedIn Recruiter. This method
    * must be called prior to making any other calls to LinkedIn.
    *
    * @return Boolean indicating the key has been successfully retrieved and is ready to make an authorization call
    * to LinkedIn
    * @example 
    * LinkedInDao linkedInAuthData = new LinkedInAuthHandler(LinkedInEnvironment.PROD).getLinkedInAuthorization();
    */
    public LinkedInDAO getLinkedInAuthorization(){
        if(mEnvironment != null){
	        LinkedInDAO mAuthDao = new LinkedInDAO();
	        mAuthDAO.mLastStatus = 'Not Initialized';
	        //ensure we can load LinkedIn configuration values for the target envionment
	        if(getConfigurationItems()){
	        	AuthJSON mAuthReturn = callAuthService();
	        	System.debug(mAuthReturn);
	        	mAccessToken = mAuthReturn.access_token;
	        	mExpiry = mAuthReturn.expires_in;
	        	LinkedInDAO mLinkedInDao = new LinkedInDao();
	        	mAuthDao.mAuthToken = mAccessToken;
	        	mAuthDao.mExpiryValue = mExpiry;
	        	mAuthDao.mLastStatus = '200';
	        }
	        return mAuthDao;
        }else{
        	return null;
        }
              
    } 
	/**
    * @description Looks up the protectected custom setting for consumer secret. This is needed to complete OAuth 
    * connection to LinkedIn and generate a Bearer Token for follow-up API calls to LinkedIn Recruiter. 
    * @param environment The name of the Salesforce runtime environment. Values are dev, test, and prod. This will
    * retrieve the proper LinkedIn consumer secret for the right environment.
    * @return Boolean indicating the key has been successfully retrieved and the instance is ready to make authorization 
	* calls to LinkedIn
	* @example 
	* if (ref.lookupAuthReferences(LinkedInEnvironment.PROD));
    */
    public Boolean getConfigurationItems(){
    	String mConfigEnvironment = mEnvironment.name();
    	try{
        	LinkedInIntegration__mdt mConfig = [SELECT ClientId__c,ClientSecret__c,LinkedInAuthUrl__c 
											FROM LinkedInIntegration__mdt where DeveloperName = :mConfigEnvironment LIMIT 1];
	    	mClientId = mConfig.ClientId__c;
	        mClientSecret = mConfig.ClientSecret__c;
	        mAuthorizationURL = mConfig.LinkedInAuthUrl__c;
        	return true;
    	}catch(System.QueryException sqe){
    		return false;
    	}	 
    }
	@TestVisible
	private AuthJSON callAuthService(){
		HttpRequest mRequest = new HttpRequest();
		mRequest = new HttpRequest();
		String mAuthBase = mAuthorizationURL;
		mAuthBase +='?grant_type=client_credentials';
		mAuthBase += '&client_id=' + mClientId;
		mAuthBase += '&client_secret=' + mClientSecret;
		mRequest.setEndpoint(mAuthBase);
		mRequest.setBody(mAuthorizationURL);
		mRequest.setMethod('POST');
		String mAuthHeaderValue = 'Bearer ' + mClientSecret;
		mRequest.setHeader('Authorization', mAuthHeaderValue);
		Http mHttp = new Http();
		HttpResponse mResponse = new HttpResponse();
		mResponse = mHttp.send(mRequest);
		if(mResponse.getStatusCode() != 200){
			throw new LinkedInAuthException('Unable to authorize your request to LinkedIn API');
		}
		System.debug('Body = ' + mResponse.getBody());
		AuthJSON mAuthJson = (AuthJSON)JSON.deserialize(mResponse.getBody(), AuthJSON.class);
		return mAuthJson;
	} 
		 
	public class LinkedInDAO{
		public String mAuthToken{ get; set;}
		public String mExpiryValue {get; set; }
		public String mLastStatus {get; set; }		
	}
	
	public class AuthJSON{
		public String access_token {get; set; }
		public String expires_in {get; set; }
	} 
}