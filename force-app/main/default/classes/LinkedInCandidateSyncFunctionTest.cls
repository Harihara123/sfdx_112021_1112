@isTest
public class LinkedInCandidateSyncFunctionTest  {
    
    @IsTest
    static void test_createCandidateRecord() {
        setLinkedInAuthMock();
        User integrationUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId =: Label.Profile_System_Integration LIMIT 1];
        Account talentAccount;
        integrationUser.OPCO__c = 'TEK';
        update integrationUser;
        System.runAs(integrationUser) {
            talentAccount = CreateTalentTestData.createTalent();
        
        
        Contact oldTalentContact = [SELECT Id, Name, FirstName, LastName, Email, Other_Email__c, Work_Email__c, CreatedDate, LastModifiedDate FROM Contact WHERE AccountId =: talentAccount.Id LIMIT 1];
        Map<Id, Contact> oldMap = new Map<Id, Contact>{oldTalentContact.Id => oldTalentContact};
        Contact newTalentContact = oldTalentContact.clone(true, true, true, true);
        newTalentContact.Email = 'something@else.com';
        newTalentContact.Other_Email__c = 'somethingOther@else.com';
        newTalentContact.Work_Email__c = 'somethingWork@else.com';
        newTalentContact.FirstName = 'newFirstName';
        newTalentContact.LastName = 'newLastName';
        Map<Id, Contact> newMap = new Map<Id, Contact>{newTalentContact.Id => newTalentContact};
        
        Test.startTest();
        LinkedInCandidateSyncFunctions.createCandidateRecord(oldMap, newMap);
        Test.stopTest();
            
        }
    }

    @IsTest
    static void test_deleteCandidateRecord() {
        setLinkedInAuthMock();
        User integrationUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId =: Label.Profile_System_Integration LIMIT 1];
        Account talentAccount;
        integrationUser.OPCO__c = 'TEK';
        update integrationUser;
        System.runAs(integrationUser) {
            talentAccount = CreateTalentTestData.createTalent();

        Contact talentContact = [SELECT Id, Name, Email FROM Contact WHERE AccountId =: talentAccount.Id LIMIT 1];
        Map<Id, Contact> oldMap = new Map<Id, Contact>{talentContact.Id => talentContact};

        Test.startTest();
        LinkedInCandidateSyncFunctions.deleteCandidateRecord(oldMap);
        Test.stopTest();
            
        }
    }

    @IsTest
    static void test_parseJSONOnGetResponse() {
        String jsonStr = '{"text": "this is some text"}';
        Test.startTest();
        LinkedInCandidateSyncFunctions.parseJSONonGetResponse(jsonStr);
        Test.stopTest();
    }

    @IsTest
    static void test_updateCandidatesOnSuccessfulMatch() {
        setLinkedInAuthMock();
        User integrationUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId =: Label.Profile_System_Integration LIMIT 1];
        Account talentAccount;
        System.runAs(integrationUser) {
            talentAccount = CreateTalentTestData.createTalent(); 
        }

        Contact talentContact = [SELECT Id, Name, FirstName, LastName, Email, Other_Email__c, Work_Email__c, CreatedDate, LastModifiedDate, LinkedIn_Person_URN__c FROM Contact WHERE AccountId =: talentAccount.Id LIMIT 1];
        Map<Id, String> matchedContacts = new Map<Id, String>{talentContact.Id => 'PERSONURN'};

        Test.startTest();
        LinkedInCandidateSyncFunctions.updateCandidatesOnSuccessfulMatch(matchedContacts);
        Test.stopTest();
    }

    static void setLinkedInAuthMock() {
        LinkedInAuthHandlerForATS.AuthJSON mReturnJson = new LinkedInAuthHandlerForATS.AuthJSON();
        mReturnJson.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
        mReturnJson.expires_in = '1799';
        LinkedInIntegrationHttpMock tMock = new LinkedInIntegrationHttpMock(mReturnJson, 200);
        Test.setMock(HttpCalloutMock.class, tMock);
    }
}