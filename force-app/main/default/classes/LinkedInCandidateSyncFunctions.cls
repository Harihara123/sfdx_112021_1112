/**
* @author Kaavya Karanam 
* @date 03/28/2018
*
* @group LinkedIn
* @group-content LinkedInCandidateSyncFunctions.html
*
* @description Apex class to hold methods used to sync candidates to and from LinkedIn
*/
public class LinkedInCandidateSyncFunctions  {

 	/**
    * @description Method to create candidate records to sync from list of updated or added contacts
    * @param oldMap - Map containing the old values of contacts
	* newMap - Map containing the new values of contacts
    * @return None
	* @example createCandidateRecord(oldMap,newMap);
    */ 
    public static void createCandidateRecord(Map<Id, Contact>oldMap, Map<Id, Contact>newMap) {
        List<Contact> candidatesWithTEKOwner = new List<Contact>(); 
        ATS_UserOwnershipModal runningUserInfo = BaseController.getCurrentUserWithOwnership();
        if(runningUserInfo.haslinkedInSync != null && runningUserInfo.haslinkedInSync) {
            for(Contact c:[select Id, OwnerId, Owner.OPCO__c, RecordTypeId, RecordType.Name from Contact where Id IN:newMap.keySet() and RecordType.Name='Talent']) {
                candidatesWithTEKOwner.add(newMap.get(c.Id));
            }
            if(!candidatesWithTEKOwner.isEmpty()) { 
                Boolean isSandbox = LinkedInUtility.isSandbox();             
                LinkedInEnvironment envVariable = LinkedInUtility.getEnvironmentName(isSandbox); 
                LinkedInIntegration__mdt configItems = LinkedInUtility.getLinkedInConfiguration(envVariable);				
                Map<String,LinkedInWebServiceCandidateRecord> candidates = new Map<String,LinkedInWebServiceCandidateRecord>(); 
                for(Contact c:candidatesWithTEKOwner) {
					setCandidateRecord(c,oldMap,configItems,candidates);
                }   
                if(!candidates.isEmpty()) {
                    createJsonStringForCandidatePut(candidates,isSandbox);
                }
            }           
        }
    }

 	/**
    * @description Method to set object of the candidate record 
    * @param oldMap - Map containing the old values of contacts
	* c - Contact which is added or updated
	* configItems - Metadata record containing the configuration information of linkedin integration
	* candidates - Map containing the linkedin key to the set candidate records
    * @return None
	* @example setCandidateRecord(c,oldMap,configItems);
    */ 
	private static void setCandidateRecord(Contact c, Map<Id, Contact>oldMap,LinkedInIntegration__mdt configItems,Map<String,LinkedInWebServiceCandidateRecord> candidates ) {
	    if(oldMap == null ||
        (oldMap!=null && (
        (c.Email != oldMap.get(c.Id).Email) ||
        (c.Other_Email__c != oldMap.get(c.Id).Other_Email__c) ||
        (c.Work_Email__c != oldMap.get(c.Id).Work_Email__c)  ||
        (c.FirstName != oldMap.get(c.Id).FirstName) ||
        (c.LastName != oldMap.get(c.Id).LastName)
        ) ) ) {
            String idString = getLinkedinKey(c.Id,configItems.IntegrationContext__c);
            LinkedInWebServiceCandidateRecord candidate = new LinkedInWebServiceCandidateRecord();                                       
            candidate.atsCreatedAt = c.CreatedDate.getTime();
            candidate.atsLastModifiedAt = c.LastModifiedDate.getTime();
            candidate.emailAddresses = new List<String>();
            if (c.Email!=null) {
                candidate.emailAddresses.add(c.Email);              
            }
            if (c.Other_Email__c!=null) {
                candidate.emailAddresses.add(c.Other_Email__c);             
            }
            if (c.Work_Email__c!=null) {
                candidate.emailAddresses.add(c.Work_Email__c);              
            }
            // this returns the wrong value ryan upton 4/26/28 candidate.externalProfileUrl = (c.LinkedIn_URL__c!=null?c.LinkedIn_URL__c:'www.linkedin.com');
            //candidate.externalProfileUrl = Url.getSalesforceBaseUrl().toExternalForm() + '/'+ c.id;
            candidate.externalProfileUrl = label.Instance_URL  + '/'+ c.id;
            candidate.firstName = (c.FirstName!=null?c.FirstName:''); 
            candidate.lastName = (c.LastName!=null?c.LastName:'');                      
            candidates.put(idString,candidate); 
        } 
	}

	public static void deleteCandidateRecord(Map<Id, Contact>oldMap){
	    List<Contact> candidatesWithTEKOwner = new List<Contact>(); 
        ATS_UserOwnershipModal runningUserInfo = BaseController.getCurrentUserWithOwnership();
        if(runningUserInfo.haslinkedInSync != null && runningUserInfo.haslinkedInSync) {
            for(Contact c:[select Id, OwnerId, Owner.OPCO__c, RecordTypeId, RecordType.Name from Contact where Id IN:oldMap.keySet() and RecordType.Name='Talent']) {
                candidatesWithTEKOwner.add(oldMap.get(c.Id));
            }
            if(!candidatesWithTEKOwner.isEmpty()) {  
        	    List<String>candidatesToDelete = new List<String>();
        		Boolean isSandbox = LinkedInUtility.isSandbox();             
                LinkedInEnvironment envVariable = LinkedInUtility.getEnvironmentName(isSandbox); 
                LinkedInIntegration__mdt configItems = LinkedInUtility.getLinkedInConfiguration(envVariable);
                String integrationContext = configItems.IntegrationContext__c;
                for(Contact mContact: candidatesWithTEKOwner){
                	candidatesToDelete.add(getLinkedInKey(mContact.Id, integrationContext));
                }
                if(!Test.isRunningTest()) {
                    LinkedInIntegrationWebserviceCallout.deleteCandidateTalentToLinkedIn(candidatesToDelete, isSandbox);   
                }
            }
        }
    }

 	/**
    * @description Method to create the JSON string for PUT callout to sync candidates
    * @param candidates - Map of candidate key to candidates record
	* isSandbox - Boolean variable indicating if the logged in environment is a sandbox or PROD
    * @return None
	* @example createJsonStringForCandidatePut(jsonStr);
    */ 	
    public static void createJsonStringForCandidatePut(Map<String,LinkedInWebServiceCandidateRecord> candidates,Boolean isSandbox) {
        if(candidates!=null) {
			Map<String,String> candidateKeyToJsonStringMap = new Map<String,String>();
			for(String key:candidates.keySet()) { 
				JSONGenerator gen = JSON.createGenerator(true);    
				gen.writeStartObject();      
				gen.writeFieldName('entities');
				gen.writeStartObject();
				gen.writeFieldName(key);
				gen.writeStartObject();
				gen.writeNumberField('atsCreatedAt',candidates.get(key).atsCreatedAt);
				gen.writeNumberField('atsLastModifiedAt',candidates.get(key).atsLastModifiedAt);
				gen.writeStringField('firstName', candidates.get(key).firstName);
				gen.writeStringField('lastName', candidates.get(key).lastName);
				gen.writeStringField('externalProfileUrl', candidates.get(key).externalProfileUrl);
				gen.writeFieldName('emailAddresses');
				gen.writeStartArray();
				if(!candidates.get(key).emailAddresses.isEmpty()) {
					for(String stringValue:candidates.get(key).emailAddresses) {
						gen.writeString(stringValue);
					}           
				}
				gen.writeEndArray();            
				gen.writeFieldName('phoneNumbers');
				gen.writeStartArray();
				gen.writeEndArray();
				gen.writeFieldName('addresses');
				gen.writeStartArray();
				gen.writeEndArray();
				gen.writeEndObject(); 
				gen.writeEndObject(); 
				gen.writeEndObject();    
				String jsonString = gen.getAsString();
				System.debug('coming here'+jsonString);
				candidateKeyToJsonStringMap.put(key,jsonString);
			}
			if(candidateKeyToJsonStringMap != null) {
                if(System.isBatch()){
                    //LinkedInIntegrationWebserviceCallout.saveCandidateTalentToLinkedInNonBatch(candidateKeyToJsonStringMap,isSandbox);          
                }else{
                    LinkedInIntegrationWebserviceCallout.saveCandidateTalentToLinkedIn(candidateKeyToJsonStringMap,isSandbox);          
                }				
			}
		
		}
    }

 	/**
    * @description Method to parse the JSON string from GET Response to get matched candidates
    * @param jsonStr - JSON String from the response 
    * @return None
	* @example parseJSONOnGetResponse(jsonStr);
    */ 
    public static void parseJSONOnGetResponse(String jsonStr) {
        // Parse entire JSON response.
        Map<String,Object> responseParse = (Map<string,Object>)JSON.deserializeUntyped(jsonStr);    
        Object results = responseParse.get('results'); 
        if(results != null) {		
			Map<Id,String> matchedContacts = new Map<Id,String>();       
            JSONParser parser = JSON.createParser(JSON.serialize(results));
            while (parser.nextToken() != null) {
                // Start at the array of candidates.
                if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to
                        //  find next candidate object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            LinkedInWebServiceCandidateRecord candidate = (LinkedInWebServiceCandidateRecord)parser.readValueAs(LinkedInWebServiceCandidateRecord.class);
                            // For debugging purposes, serialize again to verify what was parsed.
                            String s = JSON.serialize(candidate);
                            system.debug('Serialized candidate: ' + s);
                            if(candidate!=null) {
								if(candidate.manualMatchedMember != null) {
									matchedContacts.put(candidate.atsCandidateId,candidate.manualMatchedMember);
								} else if(candidate.matchedMembers != null && !candidate.matchedMembers.isEmpty()) {
									matchedContacts.put(candidate.atsCandidateId,candidate.matchedMembers.get(0));
								}
                            }

                            // Skip the child start array and start object markers.
                            parser.skipChildren();
                        }
                    }
				}
			}
			if(matchedContacts!=null) {
				updateCandidatesOnSuccessfulMatch(matchedContacts);
            }
        }

    }

 	/**
    * @description Method to update the candidate with Person URN on successful match at LinkedIn
    * @param matchedContacts - map of candidates who matched 
    * @return None
	* @example updateCandidatesOnSuccessfulMatch(matchedContacts);
    */ 
	public static void updateCandidatesOnSuccessfulMatch(Map<Id,String> matchedContacts) {
		List<Contact> contactsToBeUpdated = new List<Contact>();
		for(Contact c:[select Id,LinkedIn_Person_URN__c from Contact where Id IN:matchedContacts.keySet() and recordTypeId=:SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId()]) {					
			//check if the value you get back is different from the one you GET
			if((c.LinkedIn_Person_URN__c == '' || c.LinkedIn_Person_URN__c == null) ||
				(c.LinkedIn_Person_URN__c!=null && (c.LinkedIn_Person_URN__c != matchedContacts.get(c.Id)))) {
				c.LinkedIn_Person_URN__c = matchedContacts.get(c.Id);
				contactsToBeUpdated.add(c);
			}
		}
		System.debug(contactsToBeUpdated);
		if(!contactsToBeUpdated.isEmpty()) {
			update contactsToBeUpdated;                
		}	
	}

 	/**
    * @description Method to generate the LinkedIn Key for each candidate record
    * @param Id - candidate Id
	* integrationContext-Value of organization id in the integration context.  
    * @return String containing key for the particular linkedin candidate
	* @example getLinkedinKey(Id,integrationContext);
    */ 
    public static String getLinkedinKey(String Id,String integrationContext) {
        return 'atsCandidateId='+ Id +'&dataProvider=ATS&integrationContext='+integrationContext;
    }

}