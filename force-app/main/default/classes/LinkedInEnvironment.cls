/**
* @author Ryan Upton for Acumen Solutions 
* @date 3/14/2018
* @group LinkedIn Functions
* @group-content LinkedinEnvironment.html
* @description This is an enumeration passed to LinkedIn integration functions to ensure type safety of values
* between the different environment: DEV, TEST, PROD, LOAD
*/ 
public enum LinkedInEnvironment{
		DEV,
		TEST,
		PROD,
		LOAD
}