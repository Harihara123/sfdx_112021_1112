public class LinkedInInMailHistoryAuraHandler  {
    @AuraEnabled
    public static String retrieveWebServiceData(String requestUrl, String seatHolderId) {
        LinkedInInMailHistoryWS inMailHistoryWS = new LinkedInInMailHistoryWS(requestUrl, seatHolderId);
        return inMailHistoryWS.actualResponse();
    }
}