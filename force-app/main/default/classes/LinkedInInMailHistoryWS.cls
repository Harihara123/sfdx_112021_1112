public class LinkedInInMailHistoryWS extends LinkedInRecuiterWebService {

    public LinkedInInMailHistoryWS(String requestUrl, String seatholder) {
        super.webServiceEndpoint = 'callout:LinkedInRecruiterEndpoint/conversationEvents?';
        super.requestUrl = requestUrl;
        super.seatHolderId = seatholder;
    }

    public override String buildRequestParams() {
        String webServiceUrl = super.webServiceEndpoint;
        webServiceUrl += 'q=criteria';
        webServiceUrl += '&viewer=urn:li:seat:' + super.seatHolderId;
        webServiceUrl += '&start=' + super.startItem;
        webServiceUrl += '&count=' + super.countItems;

        return webServiceUrl;
    }

}