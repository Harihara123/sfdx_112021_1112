@isTest
public class LinkedInInMailHistoryWSTest  {
    public static String seatHolderId = '123456789';

    @isTest
    public static void buildRequestParams_givenParams_shouldReturnRequest() {
        LinkedInInMailHistoryWS ws = new LinkedInInMailHistoryWS('test', seatHolderId);
        ws.beforeDatetime = 'SomeDate';

        String expected = 'callout:LinkedInRecruiterEndpoint/conversationEvents?q=criteria&viewer=urn:li:seat:123456789&start=0&count=25';
        String result;

        Test.startTest();
            result = ws.buildRequestParams();
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return request');
    }
}