/**
* @author Ryan Upton Acumen Solutions for Allegis Group 
* @date 3/21/2018
* @group LinkedIn Functions
* @group-content LinkedinIntegrationHttpMock.html
* @description HttpCalloutMock for simulating API requests and responses from the LinkedIn Recruiter API
*/ 
@isTest
global class LinkedInIntegrationHttpMock implements HttpCalloutMock{
	
	private LinkedInAuthHandlerForATS.AuthJSON tJsonBody;
    private String tResponseBody;
	private Integer tStatusCode = 200;
	
	/**
    * @description Initializes the mock with sample API JSON response and http status code.
  	* @param LinkedInAuthHandlerForATS.AuthJSON JSON serialization helper class containing JSON response
  	* @param Integer simulated Http status code
    * @example 
    * LinkedInAuthHandlerForATS.AuthJSON response = new LinkedInAuthHandlerForATS.AuthJSON()
    * response.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL';
    * response.expires_in = '1799';
    * LinkedInIntegrationHttpMock mHttpMock = new LinkedInIntegrationHttpMock(response, 200);
    */
	public LinkedInIntegrationHttpMock(LinkedInAuthHandlerForATS.AuthJSON response, Integer statusCode){
		tJsonBody = response;
		tStatusCode = statusCode;
	} 
	/**
    * @description Generate a mock Http reponse for test classes calling the LinkedIn Recruiter API.
  	* @param HttpRequest
    */

    public LInkedInIntegrationHttpMock(String response, Integer statusCode) {
        tResponseBody = response;
        tStatusCode = statusCode;
    }

    global HttpResponse respond(HttpRequest tRequest){
    	HttpResponse tResponse = new HttpResponse();
    	tResponse.setHeader('Content-Type','application-json');
        String tBody;
        if(tJsonBody != null) {
            tBody = JSON.serialize(tJsonBody);
        }
        else {
            tBody = tResponseBody;
        }
        tResponse.setBody(tBody);
    	tResponse.setStatusCode(tStatusCode);
    	return tResponse;
    }
}