@isTest
public class LinkedInIntegrationWSCalloutTest  {
    
    @IsTest 
    static void test_saveCandidateTalentToLinkedIn() {
        setLinkedInAuthMock();
        Boolean isSandbox = LinkedInUtility.isSandbox();             
        
        String key = '123abc';
        Map<String, String> candidateKeyToJsonStringMap = new Map<String, String>();
        candidateKeyToJsonStringMap.put(key, generateCandidate(key));
        
        Test.startTest();
        LinkedInIntegrationWebserviceCallout.saveCandidateTalentToLinkedIn(candidateKeyToJsonStringMap, isSandbox);
        Test.stopTest();
    }

    @IsTest 
    static void test_deleteCandidateTalentToLinkedIn() {
        setLinkedInAuthMock();
        Boolean isSandbox = LinkedInUtility.isSandbox();             
        
        String key = '123abc';
        List<String> candidateList = new List<String>();
        candidateList.add(generateCandidate(key));

        Test.startTest();
        LinkedInIntegrationWebserviceCallout.deleteCandidateTalentToLinkedIn(candidateList, isSandbox);
        Test.stopTest();
    }

    @IsTest 
    static void test_getTalentStatusFromLinkedin() {
        setLinkedInAuthMock();

	    Boolean isSandbox = LinkedInUtility.isSandbox(); 
        LinkedInEnvironment envVariable = LinkedInUtility.getEnvironmentName(isSandbox); 
        LinkedInIntegration__mdt configItems = LinkedInUtility.getLinkedInConfiguration(envVariable);
        String contactId = '000111222333444555';
        String linkedinKey = LinkedInCandidateSyncFunctions.getLinkedinKey(contactId,configItems.IntegrationContext__c);
        
        Test.startTest();
        LinkedInIntegrationWebserviceCallout.getTalentStatusFromLinkedin(linkedinKey,isSandbox);
        Test.stopTest();
    }

    static void setLinkedInAuthMock() {
		LinkedInAuthHandlerForATS.AuthJSON mReturnJson = new LinkedInAuthHandlerForATS.AuthJSON();
    	mReturnJson.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
    	mReturnJson.expires_in = '1799';
        LinkedInIntegrationHttpMock tMock = new LinkedInIntegrationHttpMock(mReturnJson, 200);
		Test.setMock(HttpCalloutMock.class, tMock);
    }

    static String generateCandidate(String key) {
		JSONGenerator gen = JSON.createGenerator(true);    
		gen.writeStartObject();      
		gen.writeFieldName('entities');
		gen.writeStartObject();
		gen.writeFieldName(key);
		gen.writeStartObject();
		gen.writeNumberField('atsCreatedAt',System.now().getTime());
		gen.writeNumberField('atsLastModifiedAt', System.now().getTime());
		gen.writeStringField('firstName', 'firstName');
		gen.writeStringField('lastName', 'lastName');
		gen.writeStringField('externalProfileUrl', 'someExternalURL');
		gen.writeFieldName('emailAddresses');
		gen.writeStartArray();
		gen.writeString('email@test.com');
		gen.writeEndArray();            
		gen.writeFieldName('phoneNumbers');
		gen.writeStartArray();
		gen.writeEndArray();
		gen.writeFieldName('addresses');
		gen.writeStartArray();
		gen.writeEndArray();
		gen.writeEndObject(); 
		gen.writeEndObject(); 
		gen.writeEndObject();    
		String jsonString = gen.getAsString();
		System.debug('coming here'+jsonString);
        return jsonString;
    }
}