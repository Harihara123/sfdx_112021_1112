/**
* @author Kaavya Karanam 
* @date 03/27/2018
*
* @group LinkedIn
* @group-content LinkedInIntegrationWebserviceCallout.html
*
* @description Apex class to hold callout methods used to sync candidates to and from LinkedIn
*/
public class LinkedInIntegrationWebserviceCallout  {
    /**
     * @description this is the string to generate the candidate endpoint for PUT and GET requests
     */
    private static final String candidateEndPoint = 'callout:LinkedInRecruiterEndpoint/atsCandidates?';
    private static WebServiceRequestInterface WebServiceRequestDI = new WebServiceRequest();
    /**
    * @description Asynchronous(Future) method to make the PUT and GET callouts for candidate sync
    * @param candidateKeyToJsonStringMap - Map of parameters key to the JSON string for each candidate that was created or edited
    * isSandbox-Boolean variable indicating if sandbox or prod - true or false.  
    * @return None
    * @example saveCandidateTalentToLinkedIn(candidateKeyToJsonStringMap,true);
    */ 
    @Future(callout = true)
    public static void saveCandidateTalentToLinkedIn(Map<String,String> candidateKeyToJsonStringMap,Boolean isSandbox) {
        if(candidateKeyToJsonStringMap != null) {
            for(String params:candidateKeyToJsonStringMap.keySet()) {               
                String jsonString = candidateKeyToJsonStringMap.get(params);
                params = params.replace('atsCandidateId','ids[0].atsCandidateId');
                params = params.replace('dataProvider','ids[0].dataProvider');
                params = params.replace('integrationContext','ids[0].integrationContext');
                String syncCandidateEndPoint = candidateEndPoint + params;
                String authToken = LinkedInUtility.createAuthorizationHeader(isSandbox);
                HttpRequest newRequest = new HttpRequest();
                newRequest.setHeader('Authorization', authToken);   
                newRequest.setHeader('x-restli-method', 'batch_update');
                WebServiceResponse response = WebServiceRequestDI.putJson(syncCandidateEndPoint, jsonString, newRequest);
                if(response.StatusCode == 200) {        
                    getTalentStatus(syncCandidateEndPoint, authToken);
                }            
            }       
        }
    }       
    @Future(callout = true)
    public static void deleteCandidateTalentToLinkedIn(List<String> candidates, Boolean isSandbox){
        if(! candidates.isEmpty()){
            for(String candidate: candidates){
                candidate = candidate.replace('atsCandidateId','ids[0].atsCandidateId');
                candidate = candidate.replace('dataProvider','ids[0].dataProvider');
                candidate = candidate.replace('integrationContext','ids[0].integrationContext');
                String syncCandidateEndPoint = 'https://api.linkedin.com/v2/atsCandidates?' + candidate;
                String authToken = LinkedInUtility.createAuthorizationHeader(isSandbox);
                HttpRequest newRequest = new HttpRequest();
                newRequest.setHeader('Authorization', authToken);   
                //newRequest.setHeader('x-restli-method', 'batch_update'); 
                WebServiceResponse response = WebServiceRequestDI.deleteJson(syncCandidateEndPoint, newRequest);
                System.debug('DELETE call to LinkedIn for Candidate ' + candidate + ' to endpoint ' + syncCandidateEndPoint);
                System.debug(response.statusCode);
            } 
        }
    } 

    public static void getTalentStatusFromLinkedin(String linkedinKey, Boolean isSandbox) {
        String syncCandidateEndPoint = candidateEndPoint + linkedinKey;
        String authToken = LinkedInUtility.createAuthorizationHeader(isSandbox);
        getTalentStatus(syncCandidateEndPoint, authToken);
    }

    /**
    * @description Method to make the GET callouts to fetch the person URN
    * @param syncCandidateEndPoint - Endpoint for GET request
    * authToken - Token string to set authorization header 
    * @return None
    * @example getTalentStatus(syncCandidateEndPoint, authToken);
    */   
    private static void getTalentStatus(String syncCandidateEndPoint, String authToken) {
        HttpRequest getRequest = new HttpRequest();
        getRequest.setHeader('Authorization', authToken);   
        WebServiceResponse getResponse = WebServiceRequestDI.getJson(syncCandidateEndPoint, getRequest);
        if(getResponse.StatusCode == 200) {
            LinkedInCandidateSyncFunctions.parseJSONOnGetResponse(getResponse.Response);
        }
    }
}