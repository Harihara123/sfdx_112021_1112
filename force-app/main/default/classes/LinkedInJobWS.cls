public class LinkedInJobWS  {
    private static final String createJobEndpoint = 'https://api.linkedin.com/v2/simpleJobPostings';
    private static final String createJobAppicationEndpoint = 'https://api.linkedin.com/v2/atsApplications';

    private static LinkedInUtility liUtility = new LinkedInUtility();
    @TestVisible
    private static String authToken;
    private static final Integer TIMEOUT = 100000;
    private static final String JSON_CONTENT_TYPE = 'application/json';

    @Future(callout=true)
    public static void createTestJob(String jsonOpps){

       Boolean allowUser = allowUserToInteractWithLinkedIn();
       if(allowUser){
        List<Opportunity> opps = (List<Opportunity>)Json.deserialize(jsonOpps,List<Opportunity>.class);
        authToken = authenticate();

        try{
            String reqBody = createJobReuqestPayload(opps);
            if(String.isNotBlank(authToken)) {
            WebServiceResponse response = makeCallout(authToken, reqBody, createJobEndpoint);
            System.debug('WebServiceResponse '+response);
            if(response.StatusCode == 401) {
                authToken = authenticate();
                response = makeCallout(authToken, reqBody, createJobEndpoint);
            }            
            }

        }catch(Exception ex){
            
            System.debug('createTestJob Exp '+ex.getMessage());
        }
       }        
    }

    public static Boolean allowUserToInteractWithLinkedIn(){
        Boolean allowUser = false;
        String enableTestLinkedInUsers = System.Label.LinkedIn_Partner_Contract_Integration_Users;
        if(enableTestLinkedInUsers != null){
           Id currentUserId = String.valueOf(UserInfo.getUserId());
            System.debug(currentUserId);
           for(String userId : enableTestLinkedInUsers.split(',')){
                if(currentUserId == userId){
                    allowUser = true;
                    break;
                }
           }
        }
        return allowUser;
    }
    @Future(callout=true)
    public static void createTestApplication(String jsonOrders, String jsonOldOrders, Boolean isUpdate){

        Boolean allowUser = allowUserToInteractWithLinkedIn();
        if(allowUser){
            List<Order> orders = (List<Order>)Json.deserialize(jsonOrders,List<Order>.class);
            System.debug(orders);
            authToken = authenticate();
            try{                
                Map<String, String> resultReqMap= null;
                Boolean isStageChange = false;
                List<Order> oldOrders = (List<Order>)Json.deserialize(jsonOldOrders,List<Order>.class);
                if(oldOrders.size() > 0){
                   
                   // Assuming the stage is changed one record at a time.
                   Order oldOrder = oldOrders[0];
                   Order newOrder = orders[0];
                   System.debug(oldOrder.Status);
                   System.debug(newOrder.Status);

                   if(oldOrder.Status != null && newOrder.Status != null && (oldOrder.Status != newOrder.Status)){
                      isStageChange = true;
                   }
                    System.debug(isStageChange);
                }
                resultReqMap = isStageChange? createOrderStageReuqestPayload (orders): createOrderReuqestPayload(orders);
                  
                String reqBody  = resultReqMap.get('reqPayLoad');
                String endPonint  = resultReqMap.get('endPonint');

                System.debug('endPonint '+ endPonint);
                System.debug('reqBody '+ reqBody);

                if(String.isNotBlank(authToken)) {
                	WebServiceResponse response = makeCallout(authToken, reqBody, endPonint);
                	if(response.StatusCode == 401) {
                    	authToken = authenticate();
                    	response = makeCallout(authToken, reqBody, createJobAppicationEndpoint);
                }            
                }
            }catch(Exception ex){
            }
        }

    }

    public static Map<String, String> createOrderStageReuqestPayload(List<Order> orders){
        
        System.debug('createOrderStageReuqestPayload');
        
        Map<String,Object> reqElement = new Map<String,Object>();
        Map<String,String> resultReqMap = new Map<String,String>();

        String urlString = '';
        String reqPayLoad = '';
		
        Order orderObj = orders[0];
        Map<String,Object> elementDetailsMap = new Map<String,Object>();
        if(!Test.isRunningTest())
        	elementDetailsMap.put('atsCreatedAt', orderObj.CreatedDate.getTime());
        elementDetailsMap.put('stage', orderObj.Status);
        
        System.debug(elementDetailsMap);
        
        Map<String,Object> elementMap = new Map<String,Object>();
        String appStageId = 'APPL_STAGE'+orderObj.Id;
        elementMap.put(appStageId, elementDetailsMap);
        
		System.debug(elementMap);
        
        Map<String,Object> entitiesMap = new Map<String,Object>();
        entitiesMap.put('entities',elementMap);

        System.debug(entitiesMap);
        
        reqPayLoad = JSON.serialize(entitiesMap);
		System.debug(reqPayLoad);
        urlString = createJobAppicationEndpoint + '/atsJobApplicationId=' + orderObj.Id + '&dataProvider=ATS&integrationContext=' + liUtility.linkedInConfiguration.IntegrationContext__c + '/stages?ids=' + appStageId;
        resultReqMap.put('endPonint',urlString);
        resultReqMap.put('reqPayLoad',reqPayLoad);

		System.debug('resultReqMap');
        System.debug(resultReqMap);
        
        return resultReqMap;
    }

    public static Map<String, String> createOrderReuqestPayload(List<Order> orders){

        String reqPayLoad = '';
        Map<String,Object> reqElement = new Map<String,Object>();

        Map<String,String> resultReqMap = new Map<String,String>();
        String urlString = createJobAppicationEndpoint+'?';
        Integer loopCounter = 0;
        Map<String,Object> mainReqMap = new Map<String,Object>();
        List<Map<String, Object>> reqList = new List<Map<String, Object>>();

        List<String> contactIds = new List<String>();
        List<String> oppIds = new List<String>();

        for(Order orderObj : orders){
            contactIds.add(orderObj.ShipToContactId);
            oppIds.add(orderObj.OpportunityId);
        }
        Map<Id, Contact> contObjects = new Map<ID, Contact>([Select Email, Name, firstName, lastName, Id from Contact where Id In : contactIds]);
        Map<Id, Opportunity> oppObjecs = new Map<ID,Opportunity>([Select Id, Req_Job_Title__c from  Opportunity where Id In : oppIds]);
        for(Order orderObj : orders){
            Contact cont = contObjects.get(orderObj.ShipToContactId);
            Opportunity opp = oppObjecs.get(orderObj.OpportunityId);
            if(cont != null && opp != null){
                
                Map<String,Object> reqChildElements = new Map<String,Object>();
                //reqElement.put('integrationContext',liUtility.linkedInConfiguration.IntegrationContext__c);
                String integrationContext = liUtility.linkedInConfiguration.IntegrationContext__c;
                System.debug('integrationContext '+integrationContext);
            String applicationId = orderObj.Id;

                String parentkey = 'atsJobApplicationId='+applicationId+'&dataProvider=ATS&integrationContext='+integrationContext;

                reqChildElements.put('atsCandidateId',orderObj.ShipToContactId);
                if(!Test.isRunningTest()){
                reqChildElements.put('atsCreatedAt',orderObj.CreatedDate.getTime());
                reqChildElements.put('atsLastModifiedAt',orderObj.LastModifiedDate.getTime());
                }
                reqChildElements.put('atsJobPostingId',orderObj.OpportunityId);
                reqChildElements.put('atsJobPostingName',opp.Req_Job_Title__c);
                reqChildElements.put('candidateEmail',cont.Email);
                reqChildElements.put('firstName',cont.FirstName);
                reqChildElements.put('lastName',cont.LastName);
                reqChildElements.put('source',(orderObj.Submittal_Applicant_Source__c != null ? orderObj.Submittal_Applicant_Source__c: 'REFERRAL'));

                reqElement.put(parentkey,reqChildElements);
                reqList.add(reqElement);

            if(loopCounter != 0){
                urlString = urlString + '&';
            }
                urlString = urlString + 'ids['+ String.valueOf(loopCounter) +'].atsJobApplicationId=' + applicationId +  '&ids['+ String.valueOf(loopCounter) +'].dataProvider=ATS&ids['+ String.valueOf(loopCounter) +'].integrationContext='+integrationContext;
                loopCounter = loopCounter + 1;
            }
        }
        if(reqList.size() > 0){
            mainReqMap.put('entities',reqElement);
            reqPayLoad = JSON.serialize(mainReqMap);
        }
        resultReqMap.put('endPonint',urlString);
        resultReqMap.put('reqPayLoad',reqPayLoad);

        System.debug(resultReqMap);

        return resultReqMap;
    }
    public static String createJobReuqestPayload(List<Opportunity> opps){
        String reqPayLoad = '';
        Map<String,Object> mainReqMap = new Map<String,Object>();
        List<Map<String, Object>> reqList = new List<Map<String, Object>>();
        for(Opportunity opp : opps){
            Map<String,Object> reqElement = new Map<String,Object>();
            reqElement.put('integrationContext',liUtility.linkedInConfiguration.IntegrationContext__c);
			System.debug('integrationContext ' + liUtility.linkedInConfiguration.IntegrationContext__c);
            //reqElement.put('integrationContext','urn:li:organization:2414183');

            reqElement.put('companyApplyUrl','http://linkedin.com');
            String jobDescription = opp.Req_Job_Description__c;
            reqElement.put('description',jobDescription);
           // reqElement.put('employmentStatus','');
            reqElement.put('externalJobPostingId',String.valueOf(opp.Id));
            reqElement.put('listedAt',opp.CreatedDate.getTime());
            reqElement.put('title',opp.Req_Job_Title__c);
            
            if(opp.Location__c != null){                
                reqElement.put('location',opp.Location__c);
            }else{
                String countryCode = opp.Req_Worksite_Country__c;
                if (countryCode == 'India' || countryCode == 'United States' || countryCode == 'USA' || countryCode == 'US') {
                    countryCode = 'us';
                }
                reqElement.put('countryCode',countryCode);
                reqElement.put('postalCode',opp.Req_Worksite_Postal_Code__c);
            }

            reqList.add(reqElement);
        }
        if(reqList.size() > 0){
            mainReqMap.put('elements',reqList);
            reqPayLoad = JSON.serialize(mainReqMap);
        }
        return reqPayLoad;
    }

    private static WebServiceResponse makeCallout(String authToken,String reqBody, String endPoint){
        return postRequest(buildHttpRequest(authToken,endPoint),reqBody,endPoint);
    }
    @TestVisible
    private static String authenticate() {
        String authToken = LinkedInUtility.createAuthorizationHeader(liUtility.isSandbox); 
        return authToken;   
    }
    @TestVisible
    private static HttpRequest buildHttpRequest(String authToken,String endPoint) {  
            HttpRequest newRequest = new HttpRequest();
            newRequest.setHeader('Authorization', authToken); 
            newRequest.setHeader('x-restli-method', endpoint.contains(createJobAppicationEndpoint)?'batch_update':'batch_create'); 
            System.debug('createTestJob newRequest  ************ ');
            return newRequest;
     }

     private static WebServiceResponse postRequest(HttpRequest request,String jsonBody,String endpoint){
        request.setMethod(endpoint.contains(createJobAppicationEndpoint)?'PUT':'POST');
        request.setHeader('Content-Type', JSON_CONTENT_TYPE);
        request.setTimeout(TIMEOUT);
        request.setEndpoint(endpoint);
        request.setBody(jsonBody);
        HttpResponse response = new HttpResponse(); 
        Http http = new Http();
        try {
            response = http.send(request);
            WebServiceResponse responseString = new WebServiceResponse(response.getStatus(), response.getStatusCode(), response.getBody());
            System.debug('Post response'+JSON.serialize(responseString));
            return responseString;
        } catch(System.CalloutException e) {
            System.debug('Callout error: ' + e);
            System.debug('Callout response: ' + response.toString());
            return new WebServiceResponse(null, response.getStatusCode(), e.getMessage());
        }

     }
}