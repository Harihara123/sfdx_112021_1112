@isTest
public class LinkedInJobWS_Test  {
    

    @isTest
    public static void testCreationOfJob(){
        LinkedInIntegrationHttpMock testMock = new LinkedInIntegrationHttpMock(sampleResponse(), 200);
        Test.setMock(HttpCalloutMock.class,testMock);
        Test.startTest();
        LinkedInJobWS.createTestJob(sampleRequst());
        Test.stopTest();
    }

    private static String sampleResponse() {        
        return '{"elements": [{"location": "sample","id": "id100","status": 200}]}';
    }
    private static String sampleRequst(){
     Opportunity opp= new Opportunity(
            Name = 'TESTOPP',
            CloseDate = system.today(),
            StageName = 'test',
            Req_Job_Description__c = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890124',
            Req_Job_Title__c = 'Sr Java Developer   ',
            Location__c = 'Hanover, MD'
        );
        
      insert opp;
        System.assert(opp.Name=='TESTOPP');
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        return json.serialize(oppList);
    }
    @isTest
    public static void testcreateTestApplication(){
        Account acc = TestData.newAccount(1, 'Client');
        Contact c = TestData.newContact(acc.Id, 1, 'Client');
        Id u = UserInfo.getUserId();
        insert acc;
        insert c;
       
         
        Order o1 = TestData.newOrder(acc.Id, c.Id, u, 'Proactive');
        Order o2 = TestData.newOrder(acc.Id, c.Id, u, 'Proactive');
        
        List<Order> oList = new List<Order>();
        List<Order> oList2 = new List<Order>();
        oList.add(o1);
        oList2.add(o2);
        insert oList;
        
        String jsonOrd = JSON.Serialize(oList);
        String jsonOldOrd = JSON.serialize(oList2);
         System.Debug('Inside Test Application Method');
        Test.startTest();
       LinkedInIntegrationHttpMock testMock = new LinkedInIntegrationHttpMock(sampleResponse(), 200);
        Test.setMock(HttpCalloutMock.class,testMock);
        LinkedInJobWS.createTestApplication(jsonOrd, jsonOldOrd, true);
        Test.stopTest();
   
    }
    
   @isTest
    public static void testallowUserToInteractWithLinkedIn(){
        Test.startTest();
        LinkedInJobWS.allowUserToInteractWithLinkedIn();
        Test.stopTest();
    }
    
    @isTest
    public static void testCreateOrderStageReuqestPayload(){
        
        Account acc = TestData.newAccount(1, 'Client');
        Contact c = TestData.newContact(acc.Id, 1, 'Client');
        Id u = UserInfo.getUserId();
        insert acc;
        insert c;
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
       
        Opportunity opp1= new Opportunity(
            Name = 'TESTOPP',
            CloseDate = system.today(),
            StageName = 'test',
            Req_Job_Description__c = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890124',
            Req_Job_Title__c = 'Sr Java Developer',
           Req_Worksite_Country__c='India',
            Req_Worksite_Postal_Code__c='1234',
             CreatedDate=system.today()
           
        );
        System.assert(opp1.StageName=='test');
        
	Opportunity opp3 = TestData.newOpportunity(acc.Id, 1);  
        insert opp3;
      insert opp1;
        
        List<Opportunity> oppList=new List<Opportunity>();
        oppList.add(opp3);
        oppList.add(opp1);
        Order o1 = TestData.newOrder1(acc.Id, c.Id,opp3.Id,u, 'Proactive');
        Order o2 = TestData.newOrder1(acc.Id, c.Id,opp1.Id,u, 'Proactive');
       System.assert(o1.Status=='Interviewing');
        List<Order> oList = new List<Order>();
        oList.add(o1);
        oList.add(o2);
        insert oList;  
       
        Datetime yesterday = Datetime.now().addDays(-1);
		 for(Order ord: oList){
		Test.setCreatedDate(ord.Id, yesterday);
          }
         Order o3 = TestData.newOrder(acc.Id, c.Id, u, 'Proactive');
        Order o4 = TestData.newOrder(acc.Id, c.Id, u, 'Proactive');
        
        System.assert(o1.Status=='Interviewing');
        List<Order> oList1 = new List<Order>();
        oList1.add(o3);
        oList1.add(o4);
        insert oList1;
        for(Order ord: oList1){
		Test.setCreatedDate(ord.Id, yesterday);
		
        }
        
        Test.startTest();
         
         LinkedInJobWS.createJobReuqestPayload(oppList);
      LinkedInJobWS.createOrderReuqestPayload(oList);
      LinkedInJobWS.createOrderStageReuqestPayload(oList1);
        Test.stopTest();
        
    }
   
}