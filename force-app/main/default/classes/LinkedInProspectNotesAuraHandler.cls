public class LinkedInProspectNotesAuraHandler  {
    @AuraEnabled
    public static String retrieveWebServiceData(String requestUrl, String seatHolderId) {
        LinkedInProspectNotesWS prospectNotesWS = new LinkedInProspectNotesWS(requestUrl, seatHolderId);
        return prospectNotesWS.actualResponse();
    }
}