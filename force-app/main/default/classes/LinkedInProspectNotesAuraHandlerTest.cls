@isTest
public class LinkedInProspectNotesAuraHandlerTest  {
    public static String seatHolderId = '123456789';
    private static String sampleResponse() {
        return '{ "elements": [{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 1","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000},{"owner": "urn:li:seat:987654321","note": "Testing out the Notes functionality 2","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:f6g7h8i9j0","active": true,"id": 1285510423,"lastModified": 1489192513000},{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 3","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000},{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 4","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000},{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 5","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000}],"paging": {"total": 10,"count": 5,"start": 0,"links": [{"rel": "next","href": "/v2/prospectNotes?beforeDate=1489192513000&contract=urn%3Ali%3Acontract%3A1234567&count=5&q=criteria&start=5&owners=urn%3Ali%3Aseat%3A123456789","type": "application/json"}]}}';
    }

    @isTest
    public static void retrieveWebServiceData_givenParams_shouldReturnRequest() {
        String result;
        LinkedInIntegrationHttpMock testMock = new LinkedInIntegrationHttpMock(sampleResponse(), 200);
        Test.setMock(httpCalloutMock.class, testMock);

        Test.startTest();
            result = LinkedInProspectNotesAuraHandler.retrieveWebServiceData('test', seatHolderId);
        Test.stopTest();

        System.assert(String.isNotEmpty(result), 'Should return a request');
    }
}