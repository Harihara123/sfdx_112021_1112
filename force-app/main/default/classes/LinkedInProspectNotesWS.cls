public class LinkedInProspectNotesWS extends LinkedInRecuiterWebService {

    public LinkedInProspectNotesWS(String reqUrl, String seatholder) {
        super.webServiceEndpoint = 'callout:LinkedInRecruiterEndpoint/prospectNotes?';
        super.requestUrl = requestUrl;
        super.seatHolderId = seatholder;
    }

    public override String buildRequestParams() {
        String webServiceUrl = super.webServiceEndpoint;
        webServiceUrl += 'q=criteria';
        webServiceUrl += '&contract=' + super.contractId;
        webServiceUrl += '&owners=urn:li:seat:' + super.seatHolderId;
        webServiceUrl += '&beforeDate=' + super.beforeDatetime;
        webServiceUrl += '&start=' + super.startItem;
        webServiceUrl += '&count=' + super.countItems;

        return webServiceUrl;
    }

}