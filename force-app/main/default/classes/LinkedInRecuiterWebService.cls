public virtual class LinkedInRecuiterWebService  {
    public String webServiceEndpoint;
    public String requestUrl;
    public String seatHolderId;
    public WebServiceRequestInterface webServiceRequestDI = new WebServiceRequest();
    public String startItem = '0';
    public String countItems = '25';
    public String beforeDatetime = String.valueOf(System.Now().getTime());

    public String authenticationToken;

    public LinkedInUtility utilities = new LinkedInUtility();
    public String contractId = utilities.linkedInConfiguration.ContractId__c;

    public WebServiceResponse getWebServiceResponse() {
        try {
            System.debug('requestUrl:' + requestUrl);
            return utilities.makeCallout(requestUrl, authenticationToken, webServiceRequestDI);
        } catch (LinkedInAuthException ex) {
            throw new AuraHandledException('Unable to connect to LinkedIn servers. Please try again in a bit.');
        }
    }

    public String actualResponse() {
        requestUrl = initializationOrPagination(requestUrl);

        if (String.isBlank(authenticationToken)) {
            authenticationToken = utilities.createAuthorizationHeaderInstance(utilities.isSandbox);
        }

        WebServiceResponse response = getWebServiceResponse();

        return response.Response;
    }

    public Boolean shouldPaginate(String requestURL) {
        return String.isBlank(requestURL);
    }

    public String initializationOrPagination(String requestURL) {
        if(shouldPaginate(requestURL)) {
            return buildRequestParams();
        } else {
            return webServiceEndpoint + requestURL;
        }
    }

    public virtual String buildRequestParams() {
        return null;
    }
}