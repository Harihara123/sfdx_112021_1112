@isTest
public class LinkedInRecuiterWebServiceTest  {
    public static String webServiceUrl = 'https://api.linkedin.com/v2/prospectNotes?q=criteria&contract=urn:li:contract:1234567&owners=urn:li:seat:123456789&beforeDate=1489192513000&start=0&count=5';
    public static String seatHolderId = 'urn:li:seat:123456789';
    public static String authenticationToken = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
    public static LinkedInRecuiterWebService ws = new LinkedInRecuiterWebService ();

    private static void setup() {
        ws.authenticationToken = authenticationToken;
        ws.seatHolderId = seatHolderId;
        ws.requestUrl = 'test';
    }

    private static String sampleResponse(String webServiceUrl) {
        return '{ "elements": [{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 1","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000},{"owner": "urn:li:seat:987654321","note": "Testing out the Notes functionality 2","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:f6g7h8i9j0","active": true,"id": 1285510423,"lastModified": 1489192513000},{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 3","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000},{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 4","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000},{"owner": "urn:li:seat:123456789","note": "Testing out the Notes functionality 5","created": 1489192513000,"contract": "urn:li:contract:1234567","member": "urn:li:person:a1b2c3d4e5","active": true,"id": 1323073443,"lastModified": 1489192513000}],"paging": {"total": 10,"count": 5,"start": 0,"links": [{"rel": "next","href": "/v2/prospectNotes?beforeDate=1489192513000&contract=urn%3Ali%3Acontract%3A1234567&count=5&q=criteria&start=5&owners=urn%3Ali%3Aseat%3A123456789","type": "application/json"}]}}';
    }

    @isTest
    public static void shouldPaginate_givenNull_shouldReturnTrue() {
        setup();
        Boolean expected = true;
        Boolean result;

        Test.startTest();
            result = ws.shouldPaginate(null);
        Test.stopTest();

        System.assertEquals(expected, result, 'Should Return True');
    }

    @isTest
    public static void shouldPaginate_givenEmptyString_shouldReturnTrue() {
        setup();
        Boolean expected = true;
        Boolean result;

        Test.startTest();
            result = ws.shouldPaginate('');
        Test.stopTest();

        System.assertEquals(expected, result, 'Should Return True');
    }

    @isTest
    public static void shouldPaginate_givenString_shouldReturnFalse() {
        setup();
        Boolean expected = false;
        Boolean result;

        Test.startTest();
            result = ws.shouldPaginate('something');
        Test.stopTest();

        System.assertEquals(expected, result, 'Should Return False');
    }

    @isTest
    public static void getWebServiceResponse_givenValidRequest_shouldReturn200() {
        setup();
        ws.authenticationToken = authenticationToken;
        LinkedInIntegrationHttpMock testMock = new LinkedInIntegrationHttpMock(sampleResponse(webServiceUrl), 200);

        Integer expected = 200;
        Integer result;

        Test.setMock(httpCalloutMock.class, testMock);


        Test.startTest();
            result = ws.getWebServiceResponse().StatusCode;
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return 200 status');
    }

    @isTest
    public static void getWebServiceResponse_givenNoAuthenticationToken_shouldReturn401Unauthorized() {
        setup();
        ws.authenticationToken = '';
        LinkedInIntegrationHttpMock testMock = new LinkedInIntegrationHttpMock(sampleResponse(webServiceUrl), 401);

        Integer expected = 401;
        Integer result;

        Test.setMock(httpCalloutMock.class, testMock);


        Test.startTest();
            result = ws.getWebServiceResponse().StatusCode;
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return 401: unauthorized status');
    }

        @isTest
    public static void actualResponse_givenInvalidAuthenticationToken_shouldReturnValidResponse() {
        setup();
        ws.authenticationToken = 'invalid token';
        LinkedInIntegrationHttpMock testMock = new LinkedInIntegrationHttpMock(sampleResponse(webServiceUrl), 401);

        String expected = sampleResponse('');
        String result;

        Test.setMock(httpCalloutMock.class, testMock);
        System.debug('ws.requestUrl' + ws.requestUrl);
        Test.startTest();
            result = ws.actualResponse();
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return valid response');
    }

    @isTest
    public static void actualResponse_givenValidRequest_shouldReturnValidResponse() {
        setup();
        ws.authenticationToken = authenticationToken;
        LinkedInIntegrationHttpMock testMock = new LinkedInIntegrationHttpMock(sampleResponse(webServiceUrl), 200);

        String expected = sampleResponse('');
        String result;

        Test.setMock(httpCalloutMock.class, testMock);

        Test.startTest();
            result = ws.actualResponse();
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return valid response');
    }

    @isTest
    public static void buildRequestParams_givenParams_shouldReturnRequest() {
        setup();
        ws.webServiceEndpoint = 'callout:LinkedInRecruiterEndpoint/test?';
        ws.requestUrl = '';
        ws.seatHolderId = seatHolderId;

        String expected = null;
        String result;

        Test.startTest();
            result = ws.buildRequestParams();
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return request');
    }


    @isTest
    public static void initializationOrPagination_givenNull_shouldReturnRequest() {
        setup();
        ws.webServiceEndpoint = 'callout:LinkedInRecruiterEndpoint/test?';
        ws.seatHolderId = seatHolderId;

        String expected = null;
        String result;

        Test.startTest();
            result = ws.initializationOrPagination(null);
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return request');
    }

    @isTest
    public static void initializationOrPagination_givenUrl_shouldReturnRequest() {
        setup();
        ws.webServiceEndpoint = 'callout:LinkedInRecruiterEndpoint/test?';
        ws.seatHolderId = seatHolderId;

        String expected = 'callout:LinkedInRecruiterEndpoint/test?test';
        String result;

        Test.startTest();
            result = ws.initializationOrPagination('test');
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return request');
    }
}