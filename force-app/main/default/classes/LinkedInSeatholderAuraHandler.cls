public class LinkedInSeatholderAuraHandler  {
    @AuraEnabled
    public static String retrieveWebServiceData(String requestUrl) {
        LinkedInSeatholderWS ws = new LinkedInSeatholderWS(requestUrl);
        return ws.actualResponse();
    }
}