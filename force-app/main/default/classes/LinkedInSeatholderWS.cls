public class LinkedInSeatholderWS extends LinkedInRecuiterWebService {
    public LinkedInSeatholderWS(String requestUrl) {
        super.webServiceEndpoint = 'callout:LinkedInRecruiterEndpoint/seats?';
        super.requestUrl = requestUrl;
    }

    public override String buildRequestParams() {
        String webServiceUrl = super.webServiceEndpoint;
        webServiceUrl += 'q=seatsByAttributes';
        webServiceUrl += '&contracts=' + super.contractId;
        webServiceUrl += '&start=' + super.startItem;
        webServiceUrl += '&count=' + super.countItems;
        return webServiceUrl;
    }
}