@isTest
public class LinkedInSeatholderWSTest  {
    @isTest
    public static void buildRequestParams_givenParams_shouldReturnRequest() {
        LinkedInSeatholderWS ws = new LinkedInSeatholderWS('test');
        ws.contractId='urn:li:contract:123456789';

        String expected = 'callout:LinkedInRecruiterEndpoint/seats?q=seatsByAttributes&contracts=urn:li:contract:123456789&start=0&count=25';
        String result;

        Test.startTest();
            result = ws.buildRequestParams();
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return request');
    }
}