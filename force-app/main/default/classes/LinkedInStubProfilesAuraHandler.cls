public class LinkedInStubProfilesAuraHandler  {
    @AuraEnabled
    public static String retrieveWebServiceData(String requestUrl, String seatHolderId) {
        LinkedInStubProfilesWS ws = new LinkedInStubProfilesWS(requestUrl, seatHolderId);
        return ws.actualResponse();
    }
}