public class LinkedInStubProfilesWS extends LinkedInRecuiterWebService {
    public LinkedInStubProfilesWS(String reqUrl, String seatholder) {
        super.webServiceEndpoint = 'callout:LinkedInRecruiterEndpoint/mailStubs?';
        super.requestUrl = requestUrl;
        super.seatHolderId = seatholder;
    }

    public override String buildRequestParams() {
        String webServiceUrl = super.webServiceEndpoint;
        webServiceUrl += 'q=contract';
        webServiceUrl += '&contract=' + super.contractId;
        webServiceUrl += '&owners=urn:li:seat:' + super.seatHolderId;
        webServiceUrl += '&start=' + super.startItem;
        webServiceUrl += '&count=' + super.countItems;

        return webServiceUrl;
    }
}