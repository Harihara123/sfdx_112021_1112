@IsTest
public class LinkedInStubProfilesWSTest  {
    public static String seatHolderId = '123456789';

    @isTest
    public static void buildRequestParams_givenParams_shouldReturnRequest() {
        LinkedInStubProfilesWS ws = new LinkedInStubProfilesWS('test', seatHolderId);
        ws.contractId = 'urn:li:contract:123456789';

        String expected = 'callout:LinkedInRecruiterEndpoint/mailStubs?q=contract&contract=urn:li:contract:123456789&owners=urn:li:seat:123456789&start=0&count=25';
        String result;

        Test.startTest();
            result = ws.buildRequestParams();
        Test.stopTest();

        System.assertEquals(expected, result, 'Should return request');
    }
}
