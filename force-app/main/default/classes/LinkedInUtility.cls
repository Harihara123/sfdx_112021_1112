/**
* @author Kaavya Karanam 
* @date 04/19/2018
*
* @group LinkedIn
* @group-content LinkedInUtility.html
*
* @description Apex class to hold utility methods used across LinkedIn classes
*/
public class LinkedInUtility {

    //configuration variables
    public Boolean isSandbox {get; set;}
    public LinkedInEnvironment envName {get; set;}
    public LinkedInIntegration__mdt linkedInConfiguration {get; set;}

    public LinkedInUtility() {
        if(isSandbox == null) {
            //change this when we get the contractId for dev/test
            isSandbox = LinkedInUtility.isSandbox();
            //isSandbox = false;
        }
        if(envName == null && isSandbox != null) {
            envName = LinkedInUtility.getEnvironmentName(isSandbox);
        }
        if(linkedInConfiguration == null && envName != null) {
            linkedInConfiguration = LinkedInUtility.getLinkedInConfiguration(envName);
        }
    }
    
 	/**
    * @description Fetches the authorization token and generates the Authorization header for LinkedIn callouts
    * @param Boolean variable indicating if sandbox or prod - true or false.  
    * @return Authorization header for LinkedIn callouts
	* @example createAuthorizationHeader(true);
    */   
    public static String createAuthorizationHeader(Boolean isSandbox) {
        LinkedInEnvironment envVariable = getEnvironmentName(isSandbox);	
        LinkedInAuthHandlerForAts auth = new LinkedInAuthHandlerForAts(envVariable);
        LinkedInAuthHandlerForAts.LinkedInDAO results = auth.getLinkedInAuthorization();    
        return 'Bearer ' + results.mAuthToken;
    }

    //non static variant, need to refactor other classes to get away from static
    public String createAuthorizationHeaderInstance(Boolean isSandbox) {
        LinkedInEnvironment envVariable = getEnvironmentName(isSandbox);	
        LinkedInAuthHandlerForAts auth = new LinkedInAuthHandlerForAts(envVariable);
        LinkedInAuthHandlerForAts.LinkedInDAO results = auth.getLinkedInAuthorization();    
        return 'Bearer ' + results.mAuthToken;
    }
 	/**
    * @description Indicates if sandbox or prod.
    * @param None 
    * @return Boolean value indicating if sandbox or not
	* @example isSandbox();
    */     
    public static Boolean isSandbox(){
        Organization org = new Organization();
        org = [select IsSandbox from Organization limit 1];
        return org.IsSandbox;
    } 
	/**
    * @description Looks up the protected custom setting and gets the linkedin configuration items for each environment.
    * @param The name of the Salesforce runtime environment. Values are dev, test, and prod. 
    * @return Custom setting record of Linkedin configuration for the corresponding environment
	* @example getLinkedInConfiguration(LinkedInEnvironment.PROD);
    */      
    public static LinkedInIntegration__mdt getLinkedInConfiguration(LinkedInEnvironment envName){
        return [SELECT ClientId__c,ClientSecret__c,LinkedInAuthUrl__c,IntegrationContext__c, ContractId__c FROM LinkedInIntegration__mdt where DeveloperName = :envName.name() LIMIT 1];
    } 
	/**
    * @description Fetches the LinkedInEnvironment variable for the logged in organization.
    * @param Boolean variable indicating if sandbox or prod - true or false. 
    * @return LinkedInEnvironment variable for DEV or PROD
	* @example getEnvironmentName(true);
    */     
    public static LinkedInEnvironment getEnvironmentName(Boolean isSandbox) {
        LinkedInEnvironment envVariable;
        if (isSandbox) {
            envVariable = LinkedInEnvironment.DEV;
        } else {
            envVariable = LinkedInEnvironment.PROD;
        }
        return envVariable;   
    } 

    //buildHttpRequest and makeCallout could be placed somewhere else...
    public HttpRequest buildHttpRequest(String authToken) {
        HttpRequest newRequest = new HttpRequest();
        newRequest.setHeader('Authorization', authToken); 
        return newRequest;
    }

    public WebServiceResponse makeCallout(String webServiceURL, String authToken, WebServiceRequestInterface webServiceRequestDI) {
        System.debug('authToken: ' + authToken);
        HttpRequest newRequest = buildHttpRequest(authToken);
        WebServiceResponse response = WebServiceRequestDI.getJson(webServiceUrl, newRequest);
        System.debug('response' + response.Response);
        return response;
    }
}