/**
* @author Kaavya Karanam 
* @date 03/27/2018
*
* @group LinkedIn
* @group-content LinkedInWebServiceCandidateRecord.html
*
* @description Apex class to get or set 'Candidate' information for LinkedIn callouts
*/
public class LinkedInWebServiceCandidateRecord  {
	public String atsCandidateId {get;set;}
	public String dataProvider {get;set;}
	public String integrationContext {get;set;}
	public List<CandidateAddress> addresses {get;set;}
	public Long atsCreatedAt {get;set;}
	public Long atsLastModifiedAt {get;set;}
	public List<String> emailAddresses {get;set;}
	public String externalProfileUrl {get;set;}
	public String firstName {get;set;}
	public String lastName {get;set;}
	public List<CandidatePhoneNumber> phoneNumbers {get;set;}
	public String manualMatchedMember {get;set;}
	public List<String> matchedMembers {get;set;}

	public class CandidateAddress {
		public String line1 {get;set;}
		public String city {get;set;}
		public String geographicArea {get;set;}
		public String geographicAreaType {get;set;}		
		public String postalCode {get;set;}
		public String country {get;set;}
	}

	public class CandidatePhoneNumber {
		public String phoneNumber {get;set;}
		public String extension {get;set;}
	}
}