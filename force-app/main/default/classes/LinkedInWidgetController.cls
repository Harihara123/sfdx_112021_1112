/**
* @author Anson Quach Acumen Solutions for Allegis Group 
* @date 4/20/2018
* @group LinkedIn Functions
* @group-content LinkedinWidgetController.html
* @description This class is used to actually handle the contents of the iframe for a lightning component
*   for LinkedIn due to Lightning Locker Service 
*/ 
global class LinkedInWidgetController {
	public String apiKey {get; set;}
	public String extensions {get; set;}
	public String atsCandidateId {get; set;}
	public String integrationContext {get; set;}
	public String showUnlinkURL {get; set;}
	public String confirmUnlink {get; set;}
	public String onLink {get; set;}
	public String unLink {get; set;}
	public String width {get; set;}

	/**
    * @description Looks up the protectected custom setting for consumer secret.
    * The custom metadata values that are needed is the integrationContext and the apiKey
    * If it cannot find the custom metadata configuration, the constructor will throw 
    * an exception.
    *
    * @return void
    * @example 
    * LinkedInWidgetController controller = new LinkedInWidgetController();
    */	
	public LinkedInWidgetController() {
		try {
			LinkedInIntegration__mdt mConfig = LinkedInUtility.getLinkedInConfiguration(
				LinkedInUtility.getEnvironmentName(LinkedInUtility.isSandbox()));
			apiKey = 'api_key: ' + mConfig.ClientId__c;
			integrationContext = mConfig.IntegrationContext__c;
			extensions = 'extensions: HcmWidget@https://www.linkedin.com/recruiter/widget/hcm';
			showUnlinkURL = 'true';
			confirmUnlink = 'true';
			width = '500';
			atsCandidateId = ApexPages.currentPage().getParameters().get('atsCandidateId');
		}
		catch(System.QueryException qe) {
			qe.setMessage('There are no LinkedInConfiguration__mdt');
		}
	}

	/**
    * @description Remote method that updates the contact's personURN on either a link or an unlink.
    * unlink will blank out the personURN, otherwise the new one is provided.
    *
    * @return void
    * @example 
    * LinkedInWidgetController.updatePersonURN('someCandidateMergeFieldOnPage', 'returnedFromTheWidget');
    */
	@RemoteAction
	global static void updatePersonURN(String atsCandidateId, String newPersonURN) {
		String mRecordTypeId = SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
		Contact candidate = [SELECT Id, LinkedIn_Person_URN__c FROM Contact WHERE Id =: atsCandidateId and recordTypeId=: mRecordTypeId];
		if(candidate !=null){
			candidate.LinkedIn_Person_URN__c = newPersonURN;
			update candidate; 
		}
	}
}