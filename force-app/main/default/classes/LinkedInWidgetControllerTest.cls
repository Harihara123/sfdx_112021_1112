@isTest
private class LinkedInWidgetControllerTest {

	private static final String NEWPERSONURN = 'aNewPersonURN';
	
	@isTest 
	static void configurationDependentConstruction() {
		String key = 'someCandidateId';
		PageReference iframeLinkedInWidget = Page.LinkedInWidget;
		iframeLinkedInWidget.getParameters().put('atsCandidateId', key);
		Test.setCurrentPage(iframeLinkedInWidget);
		Test.startTest();
			LinkedInWidgetController controller = new LinkedInWidgetController();
		Test.stopTest();
		System.assertEquals(key, controller.atsCandidateId, 'Did not get: ' + key);
	}

	@isTest
	static void updatePersonURN() {
		LinkedInAuthHandlerForATS.AuthJSON mReturnJson = new LinkedInAuthHandlerForATS.AuthJSON();
		mReturnJson.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
		mReturnJson.expires_in = '1799';
		LinkedInIntegrationHttpMock tMock = new LinkedInIntegrationHttpMock(mReturnJson, 200);
		Test.setMock(HttpCalloutMock.class, tMock);

		Id accountTalentRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
		Account newCandidateAccount = new Account(
			Name = 'John Smith'
			, Talent_Ownership__c = 'TEK'
			, RecordTypeId = accountTalentRT);
		insert newCandidateAccount;

		Id contactTalentRT = Schema.SobjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
		Contact newCandidate = new Contact(
			FirstName = 'John'
			, LastName = 'Smith'
			, Email = 'test@gmail.com'
			, RecordTypeId = contactTalentRT
			, AccountId = newCandidateAccount.Id);
		insert newCandidate;

		PageReference iframeLinkedInWidget = Page.LinkedInWidget;
		iframeLinkedInWidget.getParameters().put('atsCandidateId', newCandidate.Id);
		Test.setCurrentPage(iframeLinkedInWidget);
		Test.startTest();
			LinkedInWidgetController controller = new LinkedInWidgetController();
			LinkedInWidgetController.updatePersonURN(newCandidate.Id, NEWPERSONURN);
		Test.stopTest();
		Contact testingResult = [SELECT Id, LinkedIn_Person_URN__c FROM Contact WHERE Id =: newCandidate.Id];
		System.assertEquals(NEWPERSONURN, testingResult.LinkedIn_Person_URN__c, 'Did not update personURN, expecting: ' + NEWPERSONURN);

	}
}