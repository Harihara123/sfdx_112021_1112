public class LogUtil {
    
    static integer batchSize = 200;
    
    public static void log(List<Log__c> messeges, String mode) {
        BypassLogic bl = new BypassLogic();
        if(BypassLogic.shouldNotBypass('LogUtil')){
            List<Log__c> insertRecords = new List<Log__c>();
            integer batchCount = 0;
            System.debug(LoggingLevel.DEBUG, messeges.size());
            for(Log__c log : messeges) {
                batchCount++;
                insertRecords.add(log);
                if (batchCount == batchSize) {
                    persistLocalLog(insertRecords);
                    insertRecords.clear();
                    batchCount = 0;
                } else if (batchCount == messeges.size()) {
                    persistLocalLog(insertRecords);
                    insertRecords.clear();
                    batchCount = 0;
                }
            }
        }
        
    }
    
    private static void persistLocalLog(List<Log__c> insertRecords) {
        //Database.insert(insertRecords);
        Database.upsert(insertRecords, Log__c.Id);
    }
    
    private static void persistRemoteLog(List<Log__c> insertRecords) {
        //TO-DO Impletement later based on external logging system
    }
    
}