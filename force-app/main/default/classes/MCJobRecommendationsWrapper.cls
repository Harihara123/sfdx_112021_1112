public class MCJobRecommendationsWrapper {
	public String EventDefinitionKey;
	public Data Data;
	public String ContactKey;

	public class Data {
		public String Talent_Name;
		public String Talent_Contact_ID;
		public String Talent_Account_ID;
		public String Req_Title2;
		public String Req_Title1;
		public String Req_Title;
		public String Req_State2;
		public String Req_State1;
		public String Req_State;
		public Date Req_Date_Posted2;
		public Date Req_Date_Posted1;
		public Date Req_Date_Posted;
        public Decimal Relevance_Score;
		public Decimal Relevance_Score1;
		public Decimal Relevance_Score2;
        public String Req_Description;
		public String Req_Description1;
		public String Req_Description2;
		public String Req_City2;
		public String Req_City1;
		public String Req_City;
		public String OpportunityId2;
		public String OpportunityId1;
		public String OpportunityId;
		public String Opco;
		public String Name;
		public String Id;
		public String Comments;
        public String Req_Product1;
        public String Req_Product2;
        public String Req_Product;
        public String Profile_Name;
        public String UserGUID;
	}
    
}