/**
  @author : Debasis Mohabhoi
  @date : 05/06/2021
  @Description : S-216773 - This class is used to Create record in MC journey via API 
 **/
Public with sharing class MCJourneyController {

	/**
	 * @description 
	 * @param eventList 
	 */
	//@AuraEnabled
	@Future(callout = true)
	public static void callMCJourneyService(List<String> eventList) {
		system.debug('listId' + eventList);
		Map<String, Object> respMap = new Map<String, Object> ();
		List<String> talentIdList = new List<String>();
		List<Contact> talentList = New List<Contact>();
		Map<String,String> whoIdEmailMap = new Map<String,String>();
		String payload ='';
		String oAuthTokenMDTName = 'MCJourneyNonProd';
		if (Utilities.getSFEnv() == 'prod') {
			oAuthTokenMDTName = 'MCJourneyProduction';
		}
		
		Map<String, Object> oTokenMap = WebServiceSetting.oAuthTokenUsingMDT(oAuthTokenMDTName);		
		MCJourneyWrapper mcj = New MCJourneyWrapper (); //List of wrapper class for body
		MCJourneyWrapper mcw = new MCJourneyWrapper();
		//Query to fetch all the fields from Event,Submittal and Job posting and loop over to create wrapper list
		List<Event> evList = New List<Event> ([Select Id, whoId, whatId, Job_Posting__r.Id, Job_Posting__r.Name, Job_Posting__r.Owner.FirstName,
		                                      Job_Posting__r.Owner.LastName, Order_Id__r.OpportunityId, Order_Id__r.Opportunity.Name,
		                                      Job_Posting__r.Opportunity__c, Job_Posting__r.Opportunity__r.Name,
		                                      Who.FirstName, who.LastName, Who.Email, Job_Posting__r.Owner.Email,Job_Posting__r.Job_Title__c
		                                      from Event where ICID__c = 'csr_exp_group-b_04262021' and Id in :eventList]);
											  // WITH SECURITY_ENFORCED - can not be used as polymorphic key who is used in query
		for(Event e:evList){
			talentIdList.add(e.WhoId);
		}
		talentList =[Select Id, Email from Contact where Id in :talentIdList WITH SECURITY_ENFORCED];
		for(Contact c:talentList){
			whoIdEmailMap.put(c.Id,c.Email);			
		}
		
		for (Event ev : evList) {
		
		MCJourneyWrapper.Data md = new MCJourneyWrapper.Data();
		
			mcw.ContactKey  = ev.WhoId; 
			mcw.EventDefinitionKey = String.valueOf( oTokenMap.get('apievent_key'));
			md.ContactID = ev.WhoId; 
			md.OpportunityID = ev.Job_Posting__r.Opportunity__c; 
			md.JobPostingID = ev.Job_Posting__r.Id; 
			md.ApplicantFirstName = ev.Who.FirstName; 
			md.ApplicantLastName = ev.Who.LastName; 
			md.ApplicantEmailAddress = whoIdEmailMap.get(ev.WhoId); 
			md.RecruiterFirstName = ev.Job_Posting__r.Owner.FirstName; 
			md.RecruiterLastName = ev.Job_Posting__r.Owner.LastName; 
			md.RecruiterUserID = ev.Job_Posting__r.Owner.Id; 
			md.RecruiterEmailAddress = ev.Job_Posting__r.Owner.Email; 
			//md.JobPostingName = ev.Job_Posting__r.Name;
			md.JobPostingName = ev.Job_Posting__r.Name + ' ' + ev.Job_Posting__r.Job_Title__c;//D-17754 - if required please block the above line and unblock this
			md.OpportunityName = ev.Job_Posting__r.Opportunity__r.Name; 
			mcw.Data = md;
			mcj = mcw;
			
		}
		
		Http httpCon = new Http();
		HttpRequest request = new HttpRequest();
		HttpResponse response = new HttpResponse();		
		Integer timeoutInMilli = Integer.valueOf(oTokenMap.get('timeout')) * 1000;
		request.setHeader('Authorization', 'Bearer ' +  oTokenMap.get('access_token'));
		request.setHeader('Content-type', 'application/json');
		request.setEndpoint((String) oTokenMap.get('endPointUrl'));
		request.setMethod('POST');
		request.setTimeout(timeoutInMilli);
		request.setBody(JSON.serialize(mcw, true)); 
		
		try {
		response = httpCon.send(request);					
			
			if (response.getStatusCode() == 200 || response.getStatusCode() == 201)
			{		
			}
		} catch(CalloutException cExp) {
			
			ConnectedLog.LogException('ICID/MCJourneyAPI', 'MCJourneyController', 'callMCJourneyService', cExp);
		} catch(DmlException dExp) {			
			ConnectedLog.LogException('ICID/MCJourneyAPI', 'MCJourneyController', 'callMCJourneyService', dExp);
		} catch(Exception exp) {			
			ConnectedLog.LogException('ICID/MCJourneyAPI', 'MCJourneyController', 'callMCJourneyService', exp);
		}
	}
	public static String IsProductionOrg() {
		return([select IsSandbox from Organization WITH SECURITY_ENFORCED limit 1].IsSandbox ? 'Sandbox' : 'Production');
	}
	public static HttpResponse callMktCloudAPI(String endPointURL, String accessToken, String payload) {
		String oAuthTokenMDTName = 'MCJourneyNonProd';
		if (Utilities.getSFEnv() == 'prod') {
			oAuthTokenMDTName = 'MCJourneyProduction';
		}
		Map<String, Object> respMap = new Map<String, Object> ();
		oAuth_Setting__mdt oAuthSetting = oAuth_Setting__mdt.getInstance(oAuthTokenMDTName);
		HttpRequest httpRequest = new HttpRequest();
		httpRequest.setMethod('POST');

		if (!String.isBlank(accessToken))
		httpRequest.setHeader('Authorization', 'Bearer ' + accessToken);
		httpRequest.setHeader('Content-Type', 'application/json');
		httpRequest.setEndpoint(endPointURL);
		httpRequest.setBody(payload);
		Http http = new Http();
		HttpResponse httpResponse;

		httpResponse = http.send(httpRequest);	
		respMap = (Map<String, Object>) JSON.deserialize(httpResponse.getBody(), Map<String, String>.class);
			String timeout = String.valueOf(oAuthSetting.Timeout_In_Second__c);
			respMap.put('endPointUrl', oAuthSetting.Endpoint_URL__c); //This URL will be use for callout
			respMap.put('timeout', timeout);
		return httpResponse;
	}
	
}