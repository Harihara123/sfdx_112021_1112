@isTest
global class MCJourneyController_Test {
	@IsTest()
	public static  void callMCJourneyServiceTest() {
		
		DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo', Value__c = 'Aerotek, Inc;TEKsystems, Inc.');
		insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update', Value__c = 'False');
		insert DRZSettings1;
		List<Opportunity> lstOpportunitys = new List<Opportunity> ();
		Account clientAcc = TestData.newAccount(1, 'Client');
		insert clientAcc;

		Contact con = TestData.newContact(clientAcc.id, 1, 'Recruiter');
		Insert con;

		String reqRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

		Opportunity newOpp = new Opportunity();
		newOpp.Name = 'New ReqOpportunities';
		newOpp.Accountid = clientAcc.Id;
		newOpp.RecordTypeId = reqRecordTypeID;
		Date closingdate = system.today();
		newOpp.CloseDate = closingdate.addDays(25);
		newOpp.StageName = 'Draft';
		newOpp.Req_Total_Positions__c = 2;
		newOpp.Req_Job_Title__c = 'Salesforce Architect';
		newOpp.EnterpriseReqSkills__c = '[{"name":"Salesforce.com","favorite":false},{"name":"Lightning","favorite":false},{"name":"Apex","favorite":false}]';
		newOpp.Req_Skill_Details__c = 'Salesforce';
		newOpp.Req_Job_Description__c = 'Testing';
		newOpp.Req_Pay_Rate_Max__c = 900;
		newOpp.Req_Pay_Rate_Min__c = 901;
		newOpp.Pay_Rate_Max_Multi_Currency__c = 900;
		newOpp.Pay_Rate_Min_Multi_Currency__c = 901;
		newOpp.Req_Bill_Rate_Max__c = 300000;
		newOpp.Req_Bill_Rate_Min__c = 100000;
		newOpp.Currency__c = 'USD - U.S Dollar';
		newOpp.Req_EVP__c = 'test EVP';
		newOpp.Req_Job_Level__c = 'Expert Level';
		newOpp.Req_Rate_Frequency__c = 'Hourly';
		newOpp.Req_Duration_Unit__c = 'Year(s)';
		newOpp.Req_Qualification__c = 'Additional Skill - database';
		newOpp.Req_Hiring_Manager__c = con.Name;
		newOpp.Opco__c = 'Aerotek, Inc.';
		newOpp.Req_Division__c = 'Aerotek CE';
		newOpp.BusinessUnit__c = 'Board Practice';
		newOpp.Req_Product__c = 'Permanent';
		newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
		newOpp.Req_Worksite_Street__c = '987 Hidden St';
		newOpp.Req_Worksite_City__c = 'Baltimore';
		newOpp.Req_Worksite_Postal_Code__c = '21228';
		newOpp.Req_Worksite_Country__c = 'United States';
		newOpp.Req_Duration_Unit__c = 'Day(s)';

		insert newOpp;

		List<Job_Posting__c> jpList = new List<Job_Posting__c> ();
		Job_Posting__c jp = new Job_Posting__c();

		jp.Job_Title__c = 'Test Administrator';
		jp.Skills__c = 'Core Java and Python';
		jp.Allegis_Employment_Type__c = 'Contract';
		jp.Industry_Sector__c = 'Accounting';
		jp.External_Job_Description__c = 'Test Administrator';
		jp.Salary_Frequency__c = null;
		jp.Posting_Status__c = 'Delivery In Progress';
		jp.Opportunity__c = newOpp.Id;
		jp.opco__c = 'AERO';
		jpList.add(jp);

		insert jpList;
       
        Id OSID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();//added RecType Condition by akshay ATS - 3344
        Order submittal = new Order(
                                AccountId = con.AccountId,
                                ShipToContactId = con.Id,
                                /*ATS_Job__c = jpList[0].Id,*/
                                EffectiveDate = System.today(),
                                Status = 'Linked',
                                RecordTypeId = OSID
                            );
        insert submittal;
       
		
			Event event = new Event();
			event.WhatId = submittal.Id;
			event.WhoId = submittal.ShipToContactId;
			event.Application_Id__c = '12345678';
			event.Inviter_Full_Name__c = 'Test Inviter Name';
			event.JB_Source_ID__c = '123456';
			event.Vendor_Application_ID__c ='123456789';
			event.Vendor_Segment_ID__c = '123432134';
			//event.Job_Posting__c = submittal.ATS_Job__c;
			event.DurationInMinutes = 1;
			event.StartDateTime = DateTime.now();
			event.Subject = submittal.Status;
			event.Type = submittal.Status;
			event.Activity_Type__c = submittal.Status;
			event.Source__c = 'Test';
			event.Feed_Source_ID__c = '12341';
			event.ECID__c = '12345';
			event.ICID__c = 'csr_exp_group-b_04262021';
			insert event;
			System.assertEquals(event.ICID__c,'csr_exp_group-b_04262021');
			List<String> evs = New List<String>();
			evs.add(event.Id);
			//MCJourneyController.callMCJourneyService(evs);	

			Test.startTest();
       	 	Test.setMock(HttpCalloutMock.class, new OAuthMockResponse());
			MCJourneyController.callMCJourneyService(evs);
			MCJourneyController.IsProductionOrg();
			Test.stopTest();	
	}
	global class OAuthMockResponse implements HttpCalloutMock{
		global HTTPResponse respond(HTTPRequest req) {
			String responseString ='{"token_type":"Bearer","expires_in":"35","ext_expires_in":"35","expires_on":"15","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
            						+'"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';
		
		
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setBody(responseString);
			res.setStatusCode(200);
			res.setStatus('OK');
			return res;
		}
	}
}