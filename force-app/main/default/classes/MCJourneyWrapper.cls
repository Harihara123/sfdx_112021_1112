public class MCJourneyWrapper {

	public String ContactKey;
	public String EventDefinitionKey;
	public Data Data;

	public class Data {
		public String ContactID;
		public String OpportunityID;
		public String JobPostingID;
		public String ApplicantFirstName;
		public String ApplicantLastName;
		public String ApplicantEmailAddress;
		public String RecruiterFirstName;
		public String RecruiterLastName;
		public String RecruiterUserID;
		public String RecruiterEmailAddress;
		public String JobPostingName;
		public String OpportunityName;
	}

	
	public static MCJourneyWrapper parse(String json) {
		return (MCJourneyWrapper) System.JSON.deserialize(json, MCJourneyWrapper.class);
	}
}