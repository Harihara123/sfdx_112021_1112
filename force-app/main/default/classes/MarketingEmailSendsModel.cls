public with sharing class MarketingEmailSendsModel {
    @AuraEnabled
    public Integer totalItems {get;set;}
    
	@AuraEnabled
    public String emailID {get;set;}

    @AuraEnabled
    public String emailSendNumber {get;set;}

	@AuraEnabled
    public String emailName {get;set;}

	@AuraEnabled
    public String subjectLine {get;set;}

    @AuraEnabled
    public String fromName {get;set;}

    @AuraEnabled
    public String fromEmail {get;set;}

    @AuraEnabled
    public String campaignName {get;set;}

    @AuraEnabled
    public Datetime dateSent {get;set;}

    @AuraEnabled
    public String opened {get;set;}

    @AuraEnabled
    public String contactId {get;set;}
}