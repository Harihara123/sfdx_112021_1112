global without sharing class MassIngestBatch implements Database.Batchable<sobject>, Database.Stateful,Database.AllowsCallouts{

    private List<Map<String,String>> batchesToDo = new List<Map<String,String>>();
    private String queryString = 'Select Id from Account Limit 1';
    private Integer batchSize = 200;
    private HerokuConnectQueueHelper qh = new HerokuConnectQueueHelper();
    
    public MassIngestBatch() {}
    
    public MassIngestBatch(List<Map<String,String>> btd, String qs, Integer bs) {
        batchesToDo = btd;
        queryString = qs;
        batchSize = bs;
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String first = batchesToDo[0].get('first');
        String last = batchesToDo[0].get('last');
        
        return Database.getQueryLocator(queryString + ' WHERE Id >= :first AND Id <= :last');        
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        GooglePubSubHelper.RaiseQueueEvents(scope);
    }
        
    static boolean recursive = false;


    global void finish(Database.BatchableContext BC) {
        if(!recursive) {
        recursive = true;
        this.batchesToDo.remove(0);
        if(batchesToDo.size() > 0) {
            Database.executeBatch(new MassIngestBatch(this.batchesToDo,queryString,batchSize), batchSize);
        }}
    }
}