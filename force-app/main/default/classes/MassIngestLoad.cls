global without sharing class MassIngestLoad {
    
     
    
    
    private static List<Map<String, String>> chunkObject(String sObjectName, Integer batchSize) {
        String query = 'SELECT Id FROM '+sObjectName;
        
        List<sObject> sobjList = Database.query(query+' ORDER BY Id ASC LIMIT 1');
        String start = sobjList[0].Id;
        system.debug('StartID:'+start);
        
        sobjList = Database.query(query+' ORDER BY Id DESC LIMIT 1');
        String finish = sobjList[0].Id;
        system.debug('FinishID:'+finish);

        List<Map<String, String>> output = new List<Map<String, String>>();
        
        if(!samePod(start,finish)) {
            system.debug('Split PODS - needs creative PK Chunking');
            String workingPOD = start;
            while(!samePod(workingPOD,finish)) {
                output.addAll(ChunkService.chunkIdRange(workingPOD,maximumPodPKChunk(sObjectName,workingPOD),batchSize));
                workingPOD = nextPodPKChunk(sObjectName,workingPOD);
            }
            output.addAll(ChunkService.chunkIdRange(workingPOD,maximumPodPKChunk(sObjectName,workingPOD),batchSize));
        } else {
            output.addAll(ChunkService.chunkIdRange(start,finish,batchSize));
        }
        system.debug(output);
        
        return output;
    }
    
    private static boolean samePod(String firstId, String secondId) {
        return (firstId.left(5)==secondId.left(5));
    }
    
    private static String maximumPodPKChunk(String sObjectName, String pKiD) {
        String maxPK = pKiD.left(5)+'zzzzzzzzz';
        String query = 'SELECT Id FROM '+sObjectName;
        List<sObject> sobjList = Database.query(query+' WHERE Id <= :maxPK ORDER BY Id DESC LIMIT 1');
        
        return sobjList[0].Id;
    }
    
    private static String nextPodPKChunk(String sObjectName, String pKiD) {
        String maxPK = pKiD.left(5)+'zzzzzzzzz';
        String query = 'SELECT Id FROM '+sObjectName;
        List<sObject> sobjList = Database.query(query+' WHERE Id > :maxPK ORDER BY Id ASC LIMIT 1');
        system.debug(sobjList[0].Id);
        
        return sobjList[0].Id;
    }

    public static void createBatches(String sObjectName, List<String> ignoreFields, Integer batchSize, Integer concurrentBatches)   {
        MassIngestLoad.createBatches(sObjectName,ignoreFields,batchSize,concurrentBatches,50000000);
    }

    
    public static void createBatches(String sObjectName, List<String> ignoreFields, Integer batchSize, Integer concurrentBatches, Integer chunkSize)   {
        
        
        String queryString = 'SELECT ' + HerokuConnectQueueHelper.fetchFieldNames(sObjectName,ignoreFields) + ' FROM ' + sObjectName;
        system.debug(queryString);
        //Create Batches
        
        List<Map<String, String>> chunks = chunkObject(sObjectName,chunkSize);
        system.debug(chunks.size());
        
        if(chunks.size()>(concurrentBatches*10)) {
        
        List<List<Map<String, String>>> batches = new List<List<Map<String, String>>>();
        
        for(Integer i = 0; i < concurrentBatches; i++) {
            List<Map<String, String>> newBatch = new List<Map<String, String>>();
            for(Integer j = i; j < chunks.size(); j = j+concurrentBatches) {
                newBatch.add(chunks[j]);
            }
            system.debug(newBatch.size());
            batches.add(newBatch);
        }
        
        for(Integer i = 0; i < concurrentBatches; i++) {
            if(batches[i].size()>0) {
            system.debug(batches[i]);
            
            Database.executeBatch(new MassIngestBatch(batches[i],queryString,batchSize), batchSize);
            }
        }
        } else {
            chunks.clear();
            MassIngestLoad.createBatches(sObjectName,ignoreFields,batchSize,concurrentBatches,chunkSize/2);
        }
           
    }  

}