public class MatchAPICalloutsQueueble implements Queueable, Database.AllowsCallouts {
	public List<Id> eventIds;
	private Integer maxRecordsToBeProcessed;
	public List<Id> eventIdsTobeProcessed;
	public List<Id> eventIdsTobeQueued;
    public Map<Id,TalentFitment__c> talentFitmentMap;
	public Map<String, String> contactOpcoMap;
    public Map<String, String> optOpcoMap;
    List<Event> eventList;
    List<TalentFitment__c> updatetftList;
	Map<Id,Contact> contactMapJ2T;
    List<TalentFitment__c> notIngestedRecs;
	App_Prioritization_Settings__c settings;
	Decimal goodFitThreshold;
	Decimal greatFitThreshold;
	Static String CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID = 'APPPRIORITY/MatchAPICalloutsException';
	Static String CONNECTED_LOG_CLASS_NAME = 'MatchAPICalloutsQueueble';

	public MatchAPICalloutsQueueble(List<Id> eventRecIds,Map<Id,TalentFitment__c> talentFitmentMap) {
		this.eventIds = eventRecIds;
        this.talentFitmentMap = talentFitmentMap;
        this.updatetftList = new List<TalentFitment__c>();
		this.contactMapJ2T = new Map<Id,Contact>();
		this.contactOpcoMap = new Map<String, String>();
        this.optOpcoMap = new Map<String, String>();
        
        this.notIngestedRecs = new List<TalentFitment__c>();
		settings = App_Prioritization_Settings__c.getValues('AppPriority');
		maxRecordsToBeProcessed = (Integer)settings.RecordsToBeProcessesPerQueue__c;
		goodFitThreshold = settings.GoodFit__c;
		greatFitThreshold = settings.GreatFit__c;
	}

	public void execute(QueueableContext context) {
		try {

			if(eventIds.size() > maxRecordsToBeProcessed) {
				eventIdsTobeProcessed = new List<Id>();
				for(Integer i=0;i<maxRecordsToBeProcessed;i++) {
					eventIdsTobeProcessed.add(eventIds[i]);
				}

				eventIdsTobeQueued = new List<Id>();
				for(Integer i=maxRecordsToBeProcessed;i<eventIds.size();i++) {
					eventIdsTobeQueued.add(eventIds[i]);
				}
                //fetch TalentFitment records for eventIDs to be processed.               
				makecallouts(eventIdsTobeProcessed);
		
			} else {
				System.debug('else:'+eventIds.size());
				makecallouts(eventIds);
			}

			// Creating Chained Queuable jobs to overcome salesforce limits
			if(eventIdsTobeQueued != null && eventIdsTobeQueued.size() > 0 ) {
				Map<Id,TalentFitment__c> talentFitmentMapTobeQueued = new Map<Id,TalentFitment__c>();
				for(TalentFitment__c tft:talentFitmentMap.values()) {
					if(eventIdsTobeQueued.contains(tft.ApplicantId__c)) {
						talentFitmentMapTobeQueued.put(tft.Id,tft);
					}

				}

				ID jobID =  System.enqueueJob(new MatchAPICalloutsQueueble(eventIdsTobeQueued,talentFitmentMapTobeQueued));
			}
            
		} catch(Exception ex) {
			System.debug('Exception:'+ex.getLineNumber()+ex.getMessage());
			ConnectedLog.LogException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'execute','','', ex);			
		}	
	}

	public void makeCallouts(List<Id> eventIdLst) {
        List<Id> evtIdsToJ2T = new List<Id>();
        Set<Id> contactIds = new Set<Id>();		
        List<Event> evtList = new List<Event>();
        Set<Id> contactIdsJ2T = new Set<Id>();
        Map<Id,TalentFitment__c> eventFitmentMapJ2T = new  Map<Id,TalentFitment__c>();
        List<Event> eventListJ2T = new List<Event>();
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        Map<String,String> oppContactMap = new Map<String,String>();
        Map<Id,TalentFitment__c> eventFitmentMap = new  Map<Id,TalentFitment__c>();
		List<TalentFitment__c> finalList = new List<TalentFitment__c>();
        
        eventFitmentMap = fetchEventFitmentMap(talentFitmentMap.values());
		eventList = fetchEventDetails(eventIdLst);
		for(Event evt:eventList) {
			contactIds.add(evt.WhoId);
            contactOpcoMap.put(evt.WhoId, evt.Job_Posting__r.Opportunity__r.Req_OpCo_Code__c);
            optOpcoMap.put(evt.Job_Posting__r.Opportunity__c, evt.Job_Posting__r.Opportunity__r.Req_OpCo_Code__c);
		}		
		contactMap = fetchContactDetails(contactIds);		
        
        //Make Talent to Job API call -- Ensure records are Indexed in search else change the status to Not Ingested
		Set<Id> talentIds = fetchTalentIds(contactMap.values());		
		List<TalentFitment__c> talent2JobRespList = talentToJobAPI(talentIds,eventFitmentMap,contactMap);
		System.debug('talent2JobRespList:'+talent2JobRespList);
        
        eventFitmentMapJ2T = fetchEventFitmentMap(talent2JobRespList);
        for(TalentFitment__c tft : talent2JobRespList) {
            evtIdsToJ2T.add(tft.ApplicantId__c);            
        }
        eventListJ2T = fetchEventDetails(evtIdsToJ2T);
		for(Event evt:eventListJ2T) {
			contactIdsJ2T.add(evt.WhoId);
		}		
		contactMapJ2T = fetchContactDetails(contactIdsJ2T);
        
        for(Event evt:eventListJ2T) {
			if(oppContactMap.containsKey(evt.Job_Posting__r.Opportunity__c)) {
				String val = oppContactMap.get(evt.Job_Posting__r.Opportunity__c)+','+contactMapJ2T.get(evt.WhoId).AccountId;
				oppContactMap.put(evt.Job_Posting__r.Opportunity__c,val);
			} else {
				oppContactMap.put(evt.Job_Posting__r.Opportunity__c,contactMapJ2T.get(evt.WhoId).AccountId);
			}
		}       
       
		//JobToTalent API Call		
		Map<Id,TalentFitment__c> responseDetailList = jobToTalentAPI(oppContactMap,eventFitmentMapJ2T,contactMapJ2T,eventListJ2T);
		System.debug('JobToTalent responseDetailList:'+responseDetailList);
        //TBD optimize one update
		
		if(talent2JobRespList.size() > 0 && responseDetailList != null) {

			for(TalentFitment__c tft:talent2JobRespList) {
				if(responseDetailList.containsKey(tft.Id)) {
					tft.Score__c = responseDetailList.get(tft.Id).Score__c;
					tft.Matched_Candidate_Status__c = responseDetailList.get(tft.Id).Matched_Candidate_Status__c;
					//tft.Matched_Skills__c = responseDetailList.get(tft.Id).Matched_Skills__c;
					tft.API_Skills__c = responseDetailList.get(tft.Id).API_Skills__c;
				}
				finalList.add(tft);
			}
		} else {
			finalList.addAll(talent2JobRespList);
		}

                
		if(notIngestedRecs.size() > 0) {
            finalList.addAll(notIngestedRecs);
		}
		//Update Retry count
		for(TalentFitment__c tft:finalList) {
			List<String> lblParams = new List<String>();

			if(tft.Score__c != null) {
				if(tft.Score__c >= greatFitThreshold) {
					lblParams.add('(Great Fit For Req)');
				} else if(tft.Score__c >= goodFitThreshold && tft.Score__c < greatFitThreshold) {
					lblParams.add('(Good Fit For Req)');
				}else {
					lblParams.add('');
				}
			} else {
				lblParams.add('');
			}

			if(String.isNotBlank(tft.Applicant_Name__c)) {
				lblParams.add(tft.Applicant_Name__c) ;
			} else{
				lblParams.add('');
			}
			tft.email_subject__c = String.format(Label.ATS_Connected_Application, lblParams);

			if(!tft.Status__c.equals('Completed')) {
				tft.Retry_Count__c += 1;
			}
		}

		//Update Talentfitment recs	
		if(finalList.size() > 0)
		update finalList;
		
	}

	private List<Event> fetchEventDetails(List<Id> eventIds) {
		List<Event> evtList = [SELECT Id,Job_Posting__r.Opportunity__c,Job_Posting__r.Opportunity__r.Req_OpCo_Code__c,WhoId,Job_Posting__c FROM Event WHERE Id IN :eventIds ];
		//List<Event> evtList = [SELECT Id,Job_Posting__r.Opportunity__c,WhoId FROM Event WHERE Id=:eventId];
		
        return evtList;
	}

	private Map<Id,Contact> fetchContactDetails(Set<Id> contactIds) {
		Map<Id,Contact> contactMap = new Map<Id,Contact>();
		for(Contact con:[SELECT Id,AccountId, MailingCountry FROM Contact WHERE Id=: contactIds]) {
			contactMap.put(con.Id,con);
		}
		
		return contactMap;
	}

	@TestVisible
	private Map<Id,TalentFitment__c> jobToTalentAPI(Map<String,String> jobToTalentMap,Map<Id,TalentFitment__c> eventFitmentMap,Map<Id,Contact> contactMap,List<Event> eventList) {
		List<MatchAPIResponseWrapper> respList = new List<MatchAPIResponseWrapper>();
		Map<Id,TalentFitment__c> ftListUpdateMap = new Map<Id,TalentFitment__c>();
		Id userId = UserInfo.getUserId();
		List<Event> evtList = new List<Event>();
		ConnectedLog.LogSack sack = new ConnectedLog.LogSack();
		if(jobToTalentMap != null) {
			for(String key:jobToTalentMap.keySet()) {
				try {	
                    String contactIds = jobToTalentMap.get(key);
					String jobToTalentQ = 'size='+(Integer)settings.RecordsRetrievedOnJobToTalentAPI__c+'&type=job2talent&docId='+key+
					'&flt.group_filter=all&flt.candidate_id='+contactIds+'&userId='+userId+
					'&appId=ATS_AppPriority&opco='+optOpcoMap.get(key)+'&location=[]&rescore=true&dyna.rescore.normalizeLtr=true&dyna.rescore.ltrMin=-7&dyna.rescore.ltrMax=7';
                    System.debug('jobToTalentQ ======> ' + jobToTalentQ);
				
					String responseBody = SearchController.matchTalents(jobToTalentQ,'GET','','');
					evtList = fetchEventIdsFromJob(key,eventList);
					if(responseBody != null) {
						String refinedResponse = responseBody.replaceAll('\"_','\"x');
						System.debug('refinedResponse:'+refinedResponse);
					
						MatchAPIResponseWrapper resp = (MatchAPIResponseWrapper)JSON.deserialize(refinedResponse, MatchAPIResponseWrapper.class);
						respList.add(resp);
						
						if(resp.header != null) {
							if(resp.header.status == 200) {
								if(resp.response != null) {
									if(resp.response.hits.hits.size() > 0) {
										for(integer i=0;i<resp.response.hits.hits.size();i++) {
											for(Event evt:evtList) {
												if(contactMap.get(evt.WhoId).AccountId == resp.response.hits.hits[i].xid) {
													TalentFitment__c tftRec = eventFitmentMap.get(evt.Id);
													if(resp.response.hits.hits[i].xscore != null) {//change to _score
                                            			tftRec.Score__c = resp.response.hits.hits[i].xscore;
														tftRec.Matched_Candidate_Status__c = resp.response.hits.hits[i].xsource.candidate_status;
														tftRec.API_Skills__c = resp.response.hits.hits[i].xsource.skillsvector;
														tftRec.Status__c = 'Completed';

													}
													ftListUpdateMap.put(tftRec.Id,tftRec);
												}
											}
										}
									
									} else {
										for(Event evt:evtList) {
											TalentFitment__c tftRec = eventFitmentMap.get(evt.Id);
											tftRec.Status__c = 'Completed';
											tftRec.Score__c = 0;
											ftListUpdateMap.put(tftRec.Id,tftRec);
										}
									}
								}
							}
						}
                    
					}
				} catch(Exception ex) {
					if(evtList.size() >  0) {
						for(Event evt:evtList) {
							TalentFitment__c tftRec = eventFitmentMap.get(evt.Id);
							sack.AddException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'jobToTalentAPI',tftRec.Id,ex);
						}
					} else {
						sack.AddException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'jobToTalentAPI',ex);
					}	

				}
			}

		}
		//Write to new Relic if any exceptions logged
		sack.Write();

		return ftListUpdateMap;
	}

	@TestVisible
	private List<TalentFitment__c>  talentToJobAPI(Set<Id> talentIds, Map<Id,TalentFitment__c> eventFitmentMap,Map<Id,Contact> contactMap) {
		List<MatchAPIResponseWrapper> respList = new List<MatchAPIResponseWrapper>();
		List<TalentFitment__c> ftListUpdate = new List<TalentFitment__c>();
		Id userId = UserInfo.getUserId();
		List<Id> evtIds = new List<Id>();
		ConnectedLog.LogSack sack = new ConnectedLog.LogSack();
        
        Map<String, String> actMap = new Map<String, String>();
        for (Contact c: contactMap.values()) {
            actMap.put(c.AccountId, c.Id);
        }
		for(Id talentId:talentIds) {
			try {	
				// for story S-203460 When the Posting Country is other than USA and CAN, then send the location in the search.
				String talentToJobQ = '';
				Contact c = contactMap.get(actMap.get(talentId));
				if(c.MailingCountry =='USA' || c.MailingCountry =='CAN'){
					talentToJobQ = 'size='+(Integer)settings.RecordsRetrievedOnTalentToJobAPI__c+'&docId='+talentId+'&type=talent2job&flt.group_filter=all'+
					'&userId='+userId+'&facetFlt.req_stage=draft|qualified|presenting|interviewing&appId=ATS_AppPriority&opco='
                    +contactOpcoMap.get(actMap.get(talentId));
				}else{
					talentToJobQ = 'size='+(Integer)settings.RecordsRetrievedOnTalentToJobAPI__c+'&docId='+talentId+'&type=talent2job&flt.group_filter=all'+
					'&userId='+userId+'&facetFlt.req_stage=draft|qualified|presenting|interviewing&appId=ATS_AppPriority&opco='
                    +contactOpcoMap.get(actMap.get(talentId))+'&location=[]';
				}
                
                System.debug('talentToJobQ ======> ' + talentToJobQ);
				String responseBodyTalent = SearchController.matchJobs(talentToJobQ,'GET','','');
				String refinedResponse = responseBodyTalent.replaceAll('\"_','\"x');
				System.debug('refinedResponse:'+refinedResponse);
				if(refinedResponse != null) {
               
					MatchAPIResponseWrapper resp = (MatchAPIResponseWrapper)JSON.deserialize(refinedResponse, MatchAPIResponseWrapper.class);
					respList.add(resp);    
					Id conId = fetchContactIdsFromTalent(talentId,contactMap);
					evtIds = fetchEvtIdsFromTalent(conId);
                
					if(resp.header != null) {
						if(resp.header.status == 200) {
							if(resp.response != null) {
								if(resp.response.hits.hits.size() > 0) {
								
									String oppIds;
									for(Integer i=0;i<resp.response.hits.hits.size();i++ ) {
										if(String.isEmpty(oppIds)) {
											oppIds = resp.response.hits.hits[i].xid;
										} else {
											oppIds += ','+resp.response.hits.hits[i].xid;
										}

									}
									for(Id evtId:evtIds) {
											TalentFitment__c tftRec = eventFitmentMap.get(evtId);
                                			tftRec.Matched_Opportunities__c = oppIds;
											tftRec.responseTtoR__c = JSON.serialize(resp.response.hits);
											tftRec.Status__c = 'Partially Completed';
											ftListUpdate.add(tftRec);
									}
								} else {
										for(Id evtId:evtIds) {
											TalentFitment__c tftRec = eventFitmentMap.get(evtId);
											tftRec.responseTtoR__c = JSON.serialize(resp.response.hits);
											tftRec.Status__c = 'Partially Completed';
											ftListUpdate.add(tftRec);
										}

								}
							}
						}
					} else {
						for(Id evtId:evtIds) {
							TalentFitment__c tftRec = eventFitmentMap.get(evtId);                        
							tftRec.Status__c = 'Not Ingested';
							tftRec.Score__c = null;
							notIngestedRecs.add(tftRec);
						}
                    
					}
				}
			} catch(Exception ex) {
				if(evtIds.size() >  0) {
					for(Id evtId:evtIds) {
						TalentFitment__c tftRec = eventFitmentMap.get(evtId);
						sack.AddException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'talentToJobAPI',tftRec.Id,ex);
					}
				} else {
					sack.AddException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'talentToJobAPI',ex);
				}
			}
		}

		//Write to new Relic if any exceptions logged
		sack.Write();

		return ftListUpdate;
	}
    
   
    private Map<Id,TalentFitment__c> fetchEventFitmentMap(List<TalentFitment__c> talentFitmentList) {
        Map<Id,TalentFitment__c> eventFitmentMap = new Map<Id,TalentFitment__c>();
        if(talentFitmentList.size() > 0) {
            
            for(TalentFitment__c tft:talentFitmentList) {                
               
                eventFitmentMap.put(tft.ApplicantId__c,tft);
            }
            
        }
        return eventFitmentMap;
    }    
    @TestVisible
    private List<Event> fetchEventIdsFromJob(Id oppId, List<Event> eventList) {
        List<Event> evtList = new List<Event>();
        for(Event evt:eventList) {
            if(evt.Job_Posting__r.Opportunity__c == oppId) {
                evtList.add(evt);
            }
            
        }
        return evtList;
    }
    
    private Set<Id> fetchTalentIds(List<Contact> conList) {
        Set<Id> talentIds = new Set<Id>();
        for(Contact con: conList) {
            
            talentIds.add(con.AccountId);
        }
        return talentIds;
    }

	private Id fetchContactIdsFromTalent(Id accId,Map<Id,Contact> contactMap) {
		Id conId;
		for(Contact con:contactMap.values()) {
			if(con.AccountId == accId) {
				conId = con.Id;
			}
			
		}
		return conId;
		
	}

	private List<Id> fetchEvtIdsFromTalent(Id conId) {
		List<Id> evtIds = new List<Id>();
        for(Event evt:eventList) {
            if(evt.WhoId == conId) {
                evtIds.add(evt.Id);
            }
            
        }
        return evtIds;
		
	}
 }