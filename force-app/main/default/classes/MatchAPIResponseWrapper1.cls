public class MatchAPIResponseWrapper1  {
        
    public Header header;
    public Response response;
    public Hits responseDetails;

    public class Header {
        public Integer status;
        public Request request;
    }
    
    public class Response {
        public Hit hits;
    }
    
    public class Request {
        public Params params;
    }
    public class Params {
        public String docId;
    }

    public class Hit {
        public List<Hits> hits;
        public decimal max_score;
        public integer total;
    }

    public class Hits {
        public String xid;
        public Source xsource;
        public decimal xscore;

    }

    public class Source {
        public String xid;
        public String candidate_status;
        public String skillsvector;
        public String qualifications_last_resume_modified_date;
        
    }
    
}