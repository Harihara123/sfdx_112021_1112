public class Merge_Log {
    
       
    public static Merge_Log__c CustomException(String message, String description, String masterId, String duplicateId, Id userId )
    {
        DateTime TodayDateTime;
        Merge_Log__c excep = new  Merge_Log__c();
        excep.Exception__c = message;
        excep.Description__c = description;
        excep.Master__c = masterId;
        excep.Duplicate__c = duplicateId;
        excep.Subject__c = 'Talent Merge Log';
        excep.OwnerId = userId;
        TodayDateTime = datetime.parse(system.now().format());         
       
       
        return excep; 
    }    
  
    
}