@isTest
public class MockGooglePubSubCallout implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "access_token": "access123123123123.asdasd"}');
        res.setStatusCode(200);
        System.debug(res.getBody());
        return res;
    }
}