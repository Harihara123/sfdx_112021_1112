/*
 * Name : MockHttpResponseGenerator
 * Description : This class is used to create the mock resposes for the following Elastic job search services : 
 * 1. Setting the oAuth access token response
 * 2. Setting the Job search response
 */
@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {
        system.debug('REQ ENDPOINT ::: ' + req.getEndpoint());
        String endpoint = req.getEndpoint();
        endpoint = endpoint.left(endpoint.indexOfIgnoreCase('?', 0));
		system.debug('**TESTendpoint**' +  endpoint );
        // Create a fake response
        HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
        if( endpoint == 'https://sampletokenRequesturl.com/token'){        	
            res.setBody(generateaccessTokenResponse());
            res.setStatusCode(200);
            return res;	    
        }else if(endpoint == 'https://sampleSearchRequest.com/search'){
            res.setBody(generateSearchResopnse());
            res.setStatusCode(200);            
        }else if(endpoint == 'https://api-tst.allegistest.com/v1/failedApplication'){
			res.setBody(generateMulesoftResponse());
            res.setStatusCode(200); 
			res.setStatus('OK');
		}else if(endpoint == 'https://api-tst.allegistest.com/v1/oauth/tokens'){
			res.setBody(generateMulesoftToken());
            res.setStatusCode(200); 
			res.setStatus('OK');
		}
        return res;
        
    }
	public string generateMulesoftToken() {
		String responseString ='{"token_type":"Bearer","expires_in":"3599","ext_expires_in":"3599","expires_on":"1585229230","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
            					+'"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';

		return responseString;
	}
	public String generateMulesoftResponse() {
		String responseString ='{"token_type":"Bearer","expires_in":"3599","ext_expires_in":"3599","expires_on":"1585229230","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
            					+'"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS",'
                				+'"response":{"header": {"status": "OK"},'
                				+'"result": ['
                				+'{"id": "0031k000008DiGbAAK","latlong": "40.8882","longitude": "-74.0503","message": ""}'
               					 +']}}'; 
		return responseString;
	}
    public string generateaccessTokenResponse(){
        string jsonRes;
        jsonRes+= '{"accessToken": "MAZ5uyasnrcMvGnE9xxv9fUdBiA0","token_type": "BearerToken","expiresIn": "604799"}';
        return jsonRes;
    }
    public string generateSearchResopnse(){
        string jsonRes;
		jsonRes += '{';
		jsonRes +='\"header\": {';
		jsonRes += '\"status\": 200,';		
        jsonRes += '},';
		jsonRes+= '\"response\": {';
        jsonRes+= '\"took\": 23,';
        jsonRes+= '\"timed_out\": false,';
        jsonRes+= '\"_shards\": {';
        jsonRes+= '\"total\": 12,';
        jsonRes+= '\"successful\": 12,';
        jsonRes+= '\"failed\": 0';
        jsonRes+= '},';
        jsonRes+= '\"hits\": {';
        jsonRes+= '\"total\": 872,';
        jsonRes+= '\"max_score\": null,';
        jsonRes+= '\"hits\": [';
        jsonRes+= '{';
        jsonRes+= '\"_index\": \"rws-req-develop-20160927.164\",';
        jsonRes+= '\"_type\": \"reqdata\",';
        jsonRes+= '\"_id\": \"14528762\",';
        jsonRes+= '\"_score\": null,';
        jsonRes+= '\"_source\": {';
        jsonRes+= '\"reqjobtitle\": \"Salesforce.com QA\",';
        jsonRes+= '\"placementtype\": \"Contract\",';
        jsonRes+= '\"reqredzoneflg\": \"N\",';
        jsonRes+= '\"reqtopthreeskills\": \"1.5-7+ years of experience as a Salesforce.com QA doing unit, integration and regression testing (senior level)\",';
        jsonRes+= '\"qualifieddate\": \"2016-08-16T00:00:00Z\",';
        jsonRes+= '\"citystate\": \"Columbia, MD\",';
        jsonRes+= '\"divisionids\": [';
        jsonRes+= '\"1004\"';
        jsonRes+= '],';
        jsonRes+= '\"reqkeywords\": \"ERPSS, Salesforce.com, QA, Testing, Unit Testing, Regression Testing, Manual Testing, CRM, Agile\"';
        jsonRes+= '},';
        jsonRes+= '\"fields\": {';
        jsonRes+= '\"geo_distance\": [';
        jsonRes+= '2.25512540151662';
        jsonRes+= ']';
        jsonRes+= '},';
        jsonRes+= '\"sort\": [';
        jsonRes+= '1471305600000';
        jsonRes+= ']';
        jsonRes+= '}';
        jsonRes+= ']';
        jsonRes+= '},';
        jsonRes+= '\"facets\": {';
        jsonRes+= '\"placementtype\": {';
        jsonRes+= '\"_type\": \"terms\",';
        jsonRes+= '\"missing\": 0,';
        jsonRes+= '\"total\": 872,';
        jsonRes+= '\"other\": 0,';
        jsonRes+= '\"terms\": [';
        jsonRes+= '{';
        jsonRes+= '\"term\": \"Contract\",';
        jsonRes+= '\"count\": 463';
        jsonRes+= '},';
        jsonRes+= '{';
        jsonRes+= '\"term\": \"Contract To Hire\",';
        jsonRes+= '\"count\": 269';
        jsonRes+= '},';
        jsonRes+= '{';
        jsonRes+= '\"term\": \"Direct Placement\",';
        jsonRes+= '\"count\": 136';
        jsonRes+= '},';
        jsonRes+= '{';
        jsonRes+= '\"term\": \"Full Time Consultant\",';
        jsonRes+= '\"count\": 4';
        jsonRes+= '}';
        jsonRes+= ']';
        jsonRes+= '},';
        jsonRes+= '\"qualifieddate\": {';
        jsonRes+= '\"_type\": \"range\",';
        jsonRes+= '\"ranges\": [';
        jsonRes+= '{';
        jsonRes+= '\"from\": 1475625600000,';
        jsonRes+= '\"from_str\": \"2016-10-05\",';
        jsonRes+= '\"to\": 1476230400000,';
        jsonRes+= '\"to_str\": \"2016-10-12\",';
        jsonRes+= '\"count\": 0,';
        jsonRes+= '\"total_count\": 0,';
        jsonRes+= '\"total\": 0,';
        jsonRes+= '\"mean\": 0';
        jsonRes+= '},';
        jsonRes+= '{';
        jsonRes+= '\"from\": 1475020800000,';
        jsonRes+= '\"from_str\": \"2016-09-28\",';
        jsonRes+= '\"to\": 1476230400000,';
        jsonRes+= '\"to_str\": \"2016-10-12\",';
        jsonRes+= '\"count\": 0,';
        jsonRes+= '\"total_count\": 0,';
        jsonRes+= '\"total\": 0,';
        jsonRes+= '\"mean\": 0';
        jsonRes+= '},';
        jsonRes+= '{';
        jsonRes+= '\"from\": 1473638400000,';
        jsonRes+= '\"from_str\": \"2016-09-12\",';
        jsonRes+= '\"to\": 1476230400000,';
        jsonRes+= '\"to_str\": \"2016-10-12\",';
        jsonRes+= '\"count\": 0,';
        jsonRes+= '\"total_count\": 0,';
        jsonRes+= '\"total\": 0,';
        jsonRes+= '\"mean\": 0';
        jsonRes+= '},';
        jsonRes+= '{';
        jsonRes+= '\"from\": 1471046400000,';
        jsonRes+= '\"from_str\": \"2016-08-13\",';
        jsonRes+= '\"to\": 1476230400000,';
        jsonRes+= '\"to_str\": \"2016-10-12\",';
        jsonRes+= '\"count\": 64,';
        jsonRes+= '\"min\": 1471046400000,';
        jsonRes+= '\"max\": 1471305600000,';
        jsonRes+= '\"total_count\": 64,';
        jsonRes+= '\"total\": 94159670400000,';
        jsonRes+= '\"mean\": 1471244850000';
        jsonRes+= '},';
        jsonRes+= '{';
        jsonRes+= '\"to\": 1471046400000,';
        jsonRes+= '\"to_str\": \"2016-08-13\",';
        jsonRes+= '\"count\": 808,';
        jsonRes+= '\"min\": 1423008000000,';
        jsonRes+= '\"max\": 1470960000000,';
        jsonRes+= '\"total_count\": 808,';
        jsonRes+= '\"total\": 1186552540800000,';
        jsonRes+= '\"mean\": 1468505619801.9802';
        jsonRes+= '}';
        jsonRes+= ']';
        jsonRes+= '}';
        jsonRes+= '}';
        jsonRes+= '}}';    
        return jsonRes;
    }
}