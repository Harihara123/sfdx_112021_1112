/*
 * This is a CallOut mock class which handle SearchController api callouts for test classess.
 * It does handle service requests made to the services maps.googleapis.com.
 */
global class MockSearchController implements System.HttpCalloutMock{
    
    global MockSearchController() {

    }
    
	global System.HttpResponse respond(System.HttpRequest req) {
        String endPoint = req.getEndpoint();
        System.debug('MockSearchController Respond to Request - ' + req.getEndpoint());
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(handleRequest(endPoint));
        res.setStatusCode(200);
        return res;
    }
    
    private static String handleRequest(String endPoint){
        String GOOGLE_API_AUTO_COMPLETE = 'https://maps.googleapis.com/maps/api/place/autocomplete/';
        String GOOGLE_API_PLACE_DETAILS = 'https://maps.googleapis.com/maps/api/place/details';
        
        if(endPoint.contains(GOOGLE_API_AUTO_COMPLETE)){
            return prepareAutoCompleteResponse();
        }
        else if(endPoint.contains(GOOGLE_API_PLACE_DETAILS)){
            return getDetailsOfPlace(endPoint);
        }else{
            return searchResopnse();
        }
    }
    private static string getDetailsOfPlace(String endPoint){
        String response = '';
        // Parse endPoint url and get place id and return corresponding place details if needed.
        response = getGooglePlaceResult(endPoint);
        return response;
    }
    
    private static string getGooglePlaceResult(String placeId){
        String response = '';
        Map<Object,Object> respMap = new Map<Object,Object>();
		respMap.put('address_components',getAddressComponentsForPlace(placeId));
        respMap.put('formatted_address','5, 48 Pirrama Rd, Pyrmont NSW 2009, Australia');
        respMap.put('geometry',getGeometryOfPlace(placeId));
        respMap.put('types',getAddressTypes(placeId));
        Map<Object,Object> resultMap = new Map<Object,Object>{'result' => respMap};
        
        response = JSOn.serialize(resultMap);
        return response;
    }
    private static Map<Object,Object> getGeometryOfPlace(String placeId){
        String response = '';
        // handle response based on input if needed.
        Map<Object,Object> respMap = new Map<Object,Object>{'viewport' => getViewPortOfPlace(placeId) ,'location' => getLatLongOfPlace('-33.866651','151.195827')};
       // response = JSOn.serialize(respMap);
        return respMap;
    }
    
    private static Map<Object,Object> getViewPortOfPlace(String placeId){
        String response = '';
        // handle response based on input if needed.
        Map<Object,Object> northeast = new Map<Object,Object>{'lat' => '-33.8653881697085','lng' => '151.1969739802915'};
		Map<Object,Object> southwest = new Map<Object,Object>{'lat' => '-33.86808613029149','lng' => '151.1942760197085'};
        Map<Object,Object> respMap = new Map<Object,Object>{'northeast' => northeast ,'southwest' => southwest};
        //response = JSOn.serialize(respMap);
        return respMap;
    }
    private static Map<Object,Object> getLatLongOfPlace(String lat, String lng){
        String response = '';
        // handle response based on input if needed.
        if((lat == null || String.isEmpty(lat)) ||
           (lng == null || String.isEmpty(lng))){
            lat = '-33.8653881697085';
            lng = '151.1969739802915';
        }
        Map<Object,Object> respMap = new Map<Object,Object>{'lat' => lat,'lng' => lng};
        //response = JSOn.serialize(respMap);
        return respMap;
    }
    private static List<Map<String,String>> getAddressComponentsForPlace(String placeid){
        String response = '';
        List<Map<String,String>> address_components  = new List<Map<String,String>>{new Map<String,String>{'long_name'=>'5'},new Map<String,String>{'long_name'=>'48'}};
        //response = JSOn.serialize(respMap);
        return address_components;
    }
    private static List<String> getAddressTypes(String placeid){
        String response = '';
 //       response = JSOn.serialize();
        return new List<String>{'point_of_interest','establishment','administrative_area_level_1'};
     }
    private static string prepareAutoCompleteResponse(){
        String response = '';       
        List<Map<String,String>> places = new List<Map<String,String>>{new Map<String,String>{'place_id'=>'ChIJD7fiBh9u5kcRYJSMaMOCCwQ'},new Map<String,String>{'place_id'=>'ChIJrU3KAHG6EmsR5Uwfrk7azrI'}};
        Map<Object,Object> respMap = new Map<Object,Object>{'status' => 'OK','predictions' => places};
        response = JSOn.serialize(respMap);
        return response;
    }
    private static string searchResopnse(){

		String jsonResponse;
		jsonResponse = '{\r\n \"_id\": \"00625000006sElWAAU\",\r\n   \"_index\": \"ats-job-master-20180123.jan18-v2\",\r\n   \"_score\": 13.801794,\r\n   \"_source\": null,\r\n   \"city_name\": \"PHILADELPHIA\",\r\n   \"country_code\": \"UNITED STATES\",\r\n   \"country_sub_division_code\": \"PA\",\r\n   \"duration\": null,\r\n   \"duration_unit\": null,\r\n   \"education_requirement\": null,\r\n   \"geolocation\": {\r\n      \"lon\": -76.7661,\r\n      \"lat\": 39.5157\r\n   },\r\n   \"job_category_code\": null,\r\n   \"job_create_date\": \"2018-01-29\",\r\n   \"job_id\": \"00625000006sElWAAU\",\r\n   \"opco_name\": \"Allegis Partners, LLC\",\r\n   \"opportunity_name\": \"REQ - Test Sudheer Parent Test - group tester\",\r\n   \"organization_account_id\": \"1-1016VMO\",\r\n   \"organization_id\": \"0012400000pqWihAAE\",\r\n   \"organization_name\": \"Test Sudheer Parent Test\",\r\n   \"owner_id\": \"00524000005q3agAAA\",\r\n   \"owner_name\": \"Mark Brady\",\r\n   \"owner_psid\": \"Dataload78\",\r\n   \"position_description\": \"test description\",\r\n   \"position_end_date\": null,\r\n   \"position_id\": \"O-0823916\",\r\n   \"position_offering_type_code\": \"Permanent\",\r\n   \"position_open_quantity\": \"1\",\r\n   \"position_start_date\": \"2018-01-23\",\r\n   \"position_street_name\": \"TEST STREET\",\r\n   \"position_title\": \"group tester\",\r\n   \"postal_code\": \"21071\",\r\n   \"practice_area\": null,\r\n   \"qualifications_summary\": null,\r\n   \"remuneration_basis_code\": \"--None--\",\r\n   \"remuneration_bill_rate_max\": 0,\r\n   \"remuneration_bill_rate_min\": 0,\r\n   \"remuneration_interval_code\": null,\r\n   \"remuneration_pay_rate_max\": 0,\r\n   \"remuneration_pay_rate_min\": 0,\r\n   \"remuneration_salary_max\": 0,\r\n   \"remuneration_salary_min\": 0,\r\n   \"req_stage\": \"Interviewing\",\r\n   \"search_tags\": null,\r\n   \"skills\": \"\"\r\n}' ;		
        return jsonResponse;
	}
}