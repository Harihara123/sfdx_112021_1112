global class MoveSubmittalstoNP
{
   
    public static void processRecords(set<ID> recordIds)
    {   
    
    	 Set<Id> setOpportunityids = new Set<Id>();
         Set<Id> setOrderIds = new Set<Id>();
         List<Order> nonProceedingSubmittals = new List<Order>(); 
         Map<Id,Id> orderOppMap = new Map<Id,Id>();
         // Get those records based on the IDs
         //List<Account> accts = [SELECT Name FROM Account WHERE Id IN :recordIds];
         // Process records
         
         if(recordIds.size() > 0){
           System.debug('Record Ids list---->'+recordIds.size());	    
           Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([select Req_Total_Positions__c, Req_Total_Filled__c, (select Id, Status from Orders) from Opportunity where Id In: recordIds]);
          
           System.debug('Map Opps'+oppMap);
          
           List<aggregateResult> results = [select Status, Order.OpportunityId, COUNT(Order.Opportunity.Name),COUNT(Order.Opportunity.Req_Total_Positions__c) from Order 
                                            where Order.OpportunityId IN :recordIds and Recordtype.DeveloperName='OpportunitySubmission' and Status = 'Started'                                         
                                            group by Status, Order.OpportunityId 
                                            having COUNT(Order.Opportunity.Name) > 0];
           System.debug('AGG Opps'+results.size());
           
                                     
          if(results.size() > 0){
             for(AggregateResult ar : results)
             {
                Opportunity op = oppMap.get((ID)ar.get('OpportunityId'));
                System.debug('Order list size---->'+op.Orders.size());
                if(ar.get('expr0') == op.Req_Total_Positions__c){ 
                  for(Order od: op.Orders){
                   
                    if(od.Status != 'Started' && od.Status != 'Not Proceeding' && od.Status.left(14)!='Not Proceeding' && 
                       od.Status != 'Linked' && od.Status != 'Applicant' && od.Status != 'Offer Accepted'){
                       System.debug('Order Status---->'+od.Status); 
                        od.Status = 'Not Proceeding - '+od.Status;
                        od.Submittal_Not_Proceeding_Reason__c = 'Auto-update due to successful Placement';
                        nonProceedingSubmittals.add(od);
                   	  }
    				}
				}
			}
            TriggerStopper.opportunityAggregateTotalStopper = false;  
            update nonProceedingSubmittals;
		  }
		}
	}
	@AuraEnabled
    public static void processRecordsfromPostionObj(string OpptyrecordIds){
		Set<Id> OpptyRecIdSet=new Set<Id>();
        List<Order> updateSubmittals = new List<Order> ();
        if (OpptyrecordIds !=null){
			OpptyRecIdSet.add(OpptyrecordIds);
            List<Order> stampNP = New List<Order> ([SELECT Id, Status, Submittal_Not_Proceeding_Reason__c, MovetoNP_Flag__c FROM Order WHERE OpportunityId =:OpptyrecordIds]);
            system.debug('--stampNP--'+stampNP.size());
            if(!stampNP.isEmpty() && stampNP.size() > 100){
                //CRM_PositionCardToOrderBatchUpdate bcn = new CRM_PositionCardToOrderBatchUpdate(OpptyrecordIds,'','ApexCntrl');
				CRM_PositionCardToOrderBatchUpdate bcn = new CRM_PositionCardToOrderBatchUpdate(OpptyRecIdSet,'','ApexCntrl');
                ID batchprocessid = Database.executeBatch(bcn,50); 
            }else if(!stampNP.isEmpty() && stampNP.size() > 0 && stampNP.size() <= 100){
                for (Order od : stampNP){
                    if (od.Status != 'Offer Accepted' && od.Status != 'Started' && od.Status != 'Not Proceeding' && od.Status != 'Linked' && od.Status != 'Applicant' && od.Status.left(14) != 'Not Proceeding') {
                        od.Status = 'Not Proceeding - ' + od.Status;
                        od.Submittal_Not_Proceeding_Reason__c = 'Auto-update due to successful Placement';
                        updateSubmittals.add(od);
                    }                
                }
            }
            if(!updateSubmittals.isEmpty())
            Database.update(updateSubmittals,false);
        }
    }
    //public static void submittalsToNPForAutoClosedReq(set OpptyrecordIds){
    public static void submittalsToNPForAutoClosedReq(Set<Id> OpptySet){
        List<Order> updateSubmittals = new List<Order> ();
        //if (OpptyrecordIds !=null){
        if (OpptySet !=null){
            //List<Order> stampNP = New List<Order> ([SELECT Id, Status, Submittal_Not_Proceeding_Reason__c, MovetoNP_Flag__c FROM Order WHERE OpportunityId =:OpptyrecordIds]);
            List<Order> stampNP = New List<Order> ([SELECT Id, Status, Submittal_Not_Proceeding_Reason__c, MovetoNP_Flag__c FROM Order WHERE OpportunityId IN:OpptySet]);
            system.debug('submittalsToNPForAutoClosedReq:--stampNP--'+stampNP.size());
            if(!stampNP.isEmpty() && stampNP.size() > 100){
                //CRM_PositionCardToOrderBatchUpdate bcn = new CRM_PositionCardToOrderBatchUpdate(OpptyrecordIds,'','ApexCntrl');
                CRM_PositionCardToOrderBatchUpdate bcn = new CRM_PositionCardToOrderBatchUpdate(OpptySet,'','AutoCloseReq');
                ID batchprocessid = Database.executeBatch(bcn,50); 
            }else if(!stampNP.isEmpty() && stampNP.size() > 0 && stampNP.size() <= 100){
                for (Order od : stampNP){
                    if (od.Status != 'Offer Accepted' && od.Status != 'Started' && od.Status != 'Not Proceeding' && od.Status.left(14) != 'Not Proceeding') {
                        od.Status = 'Not Proceeding - ' + od.Status;
                        od.Submittal_Not_Proceeding_Reason__c = 'Other';
                        updateSubmittals.add(od);
                    }                
                }
            }
            if(!updateSubmittals.isEmpty())
            Database.update(updateSubmittals,false);
        }
    }
}