public class MuleSoftOAuth_Connector {
    
    public static OAuthWrapper getAccessToken(String serviceName){
        
        OAuthWrapper wrapObj = new OAuthWrapper();
        Mulesoft_OAuth_Settings__c serviceSettings = Mulesoft_OAuth_Settings__c.getValues(serviceName);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(serviceSettings.Token_URL__c);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String payload = 'client_id='+EncodingUtil.urlEncode(serviceSettings.ClientID__c,'UTF-8')+'&client_secret='+EncodingUtil.urlEncode(serviceSettings.Client_Secret__c,'UTF-8')+'&resource='+EncodingUtil.urlEncode(serviceSettings.Resource__c,'UTF-8')+'&grant_type=client_credentials';
       // System.debug(payload);

        req.setBody(payload);
      
        HttpResponse resp = new Http().send(req);
        //system.debug('Response-->'+resp.getBody());
        
        JSONParser parser = JSON.createParser(resp.getBody());
        //system.debug('Response2'+parser);
        while (null != parser.nextToken()) {
            if (parser.getCurrentName() == 'access_Token') {
                parser.nextToken();                 
                wrapObj.token = parser.getText();
                wrapObj.endPointURL = serviceSettings.Endpoint_URL__c;
            } /*else if (parser.getCurrentName() == 'expires_in') {
                parser.nextToken();
                Integer seconds = Integer.valueOf(parser.getText());
                Datetime newExpiry = System.now().addSeconds(Integer.valueOf(seconds * 0.75));
                
            }*/ 
        }
        return wrapObj;
    }
    
    public class OAuthWrapper{
        public String token;
        public String endPointURL;
        public OAuthWrapper(){
            this.token = '';
            this.endPointURL ='';
        }
    }
}