@isTest
public class MuleSoftOAuth_Test {
    public static testMethod void getAccessTokenTest(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Failed Application',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                         	Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
       	 	Test.setMock(HttpCalloutMock.class, new MulesoftOAuthMockResponse());	
 			MuleSoftOAuth_Connector.OAuthWrapper oauthWrap = MuleSoftOAuth_Connector.getAccessToken(serviceSettings.Name);
        	system.debug(oauthWrap);
        Test.stopTest();
    }
}