@isTest
public class MultiSelectLookupControllerTest {
  static testMethod void testRetrieveRecords() {
    Account acc = TestDataHelper.createAccount();
    insert acc;
      
    List<Contact> contactList = new List<Contact>();
    for(integer i=0;i<5;i++){
      Contact con = TestDataHelper.createContact();
      con.LastName = con.LastName + i;
      con.AccountId = acc.Id;
      contactList.add(con);
    }
    insert contactList;
    
	  test.startTest();
    MultiSelectLookupController.retrieveRecords('Contact', 'Id,Name', 'Name', 'Bob', ' AND AccountId = \'' +acc.Id+'\'', ' Order By Name', ' limit 3');
    test.stopTest();
    
  }
 
}