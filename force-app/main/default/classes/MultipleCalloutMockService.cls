@isTest
public class MultipleCalloutMockService implements HTTPCalloutMock {
	Public Static Final String MOCK_NOT_FOUND = 'Mock not found for this callout. Endpoint-';
	Map<String, CalloutMockService> calloutMockMap;

	 public MultipleCalloutMockService() {
        calloutMockMap = new Map<String, CalloutMockService>();
    }

	public MultipleCalloutMockService(Map<String, CalloutMockService> calloutMockMap) {
		this.calloutMockMap = calloutMockMap;
		if (this.calloutMockMap == null) {
			calloutMockMap = new Map<String, CalloutMockService> ();
		}
	}

	public void addCalloutToMock(String endpoint, CalloutMockService calloutMock) {
        calloutMockMap.put(endpoint, calloutMock);
    }

	public HTTPResponse respond(HTTPRequest request) {
	    CalloutMockService calloutMock = calloutMockMap.get(request.getEndpoint());
        if(calloutMock != null) {
			return calloutMock.respond(request);
        } else {
			throw new AllegisException(MOCK_NOT_FOUND + request.getEndpoint());
        }
    }
}