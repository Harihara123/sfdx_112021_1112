@IsTest
public class MultipleRequestMock implements HttpCalloutMock {
    Map<String, HttpCalloutMock> requests;

    public MultipleRequestMock(Map<String, HttpCalloutMock> requests) {
        this.requests = requests;
    }

    public HTTPResponse respond(HTTPRequest req) {
        System.debug('getEndPoint: ' + req.getEndpoint());
        HttpCalloutMock mock = requests.get(req.getEndpoint());
        if (mock != null) {
            System.debug('return mock' + mock.respond(req));
            return mock.respond(req);
        } else {
                return null;
        }
    }

    public void addRequestMock(String url, HttpCalloutMock mock) {
        requests.put(url, mock);
    }
}