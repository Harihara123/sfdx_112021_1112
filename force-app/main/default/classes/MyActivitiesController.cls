public with sharing class MyActivitiesController  {
	private static final String MY_ACTVTY_CTRL = 'MyActivitiesController';
	private static final String EXP = 'Exception:';
	private static final String TSK_EVT = 'TaskEvent';
    public static String userPrefOption{get;set;}

	private MyActivitiesController(){}

	/**
		@description : This method executed on first time My Activities tab load.
		@Author : Rakesh Rangu
		@Date -

		@description : User Preferences parameter added. This method get executed on selection of value from drop down(My Default, My Callsheet, Event This Week, Task This Week).
		@author : Neel Kamal
		@Date - 02/18/2020
	*/
	@AuraEnabled(cacheable=false)
	public static MyActivitiesInitWrapper getMyActivitiesInit(String userPreferences, String sortableField, String sortOrder) {
		userPreferences = String.escapeSingleQuotes(userPreferences);
		sortableField = String.escapeSingleQuotes(sortableField);
		sortOrder = String.escapeSingleQuotes(sortOrder);
		userPrefOption = userPreferences; //Assigning to this field since another method which is call from this method and also from UI.

		MyActivitiesInitWrapper initWrapper = new MyActivitiesInitWrapper();
		try {
			initWrapper =  MyActivitiesControllerHelper.fetchFilterCriteria(userPreferences, sortableField, sortOrder);
			String stDate = String.valueOf(initWrapper.startDate);
			String edDate = String.valueOf(initWrapper.endDate);
			
			initWrapper.myAcitvitiesWrapperList = getFilterActivities(initWrapper.filterList, stDate, edDate, sortableField, sortOrder);		
			initWrapper.currentUserWithOwnership =  BaseController.getCurrentUserWithOwnership();

			//Setting sorting info into MyActivitiesInitWrapper class because of required on UI side once first time page load.
			initWrapper.sortableField = MyActivitiesControllerHelper.sortableFieldCol;
			initWrapper.sortOrder = MyActivitiesControllerHelper.sortOrder;
			initWrapper.userPrefOption = userPrefOption;
		}catch(Exception exp) {
			System.debug(EXP+exp.getMessage()+':'+exp.getLineNumber());
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'getMyActivitiesInit', '', TSK_EVT, 'Error occur during fetching activities'));
			throw new AuraHandledException(Label.Error);
		}
		
		return initWrapper;
	}
	@AuraEnabled(cacheable=false)
	public static Contact getContactDetails(String contactId) {
		String conId = String.escapeSingleQuotes(contactId);
		Contact conDetail;
		try{
			conDetail = [Select Id, Full_Name__c, FirstName, LastName, Title, Related_Account_Name__c, Current_Employer__c, Candidate_Status__c, Customer_Status__c, Phone, Email, RecordType.Name From Contact WHERE Id=:conId];			
		}catch(Exception exp) {
			System.debug(EXP+exp.getMessage()+':'+exp.getLineNumber());
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'getContactDetails', '', 'Contact', 'Error occur during fetching contact detail'));
			throw new AuraHandledException(Label.Error);
		}
		return conDetail;
	}

	@AuraEnabled(cacheable=false)
	public static List<MyActivitiesWrapper> getMyActivities(String startDate, String endDate, Boolean isFilter) {
		final String stDate = String.escapeSingleQuotes(startDate);
		final String edDate = String.escapeSingleQuotes(endDate);

		List<MyActivitiesWrapper> activitiesList;
		try{
			 activitiesList = MyActivitiesControllerHelper.getMyActivities(stDate, edDate, isFilter);
		}catch(Exception exp) {
			System.debug(EXP+exp.getMessage()+':'+exp.getLineNumber());
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'getMyActivities', '', TSK_EVT, 'Error occur during fetching activities'));
			throw new AuraHandledException(Label.Error);
		}
		return activitiesList;
	}
	

	@AuraEnabled(cacheable=false)
	public static List<MyActivitiesWrapper> getFilterActivities(List<MyActivitiesControllerHelper.ConditionWrapper> filterData, String startDate, String endDate, String sortableField, String sortOrder) {
		startDate = String.escapeSingleQuotes(startDate);
		endDate = String.escapeSingleQuotes(endDate);
		sortableField = String.escapeSingleQuotes(sortableField);
		sortOrder = String.escapeSingleQuotes(sortOrder);

			if(userPrefOption == null) {
				MyActivitiesControllerHelper.tableColFieldApiMapping(sortableField, sortOrder);
			}
		

		List<MyActivitiesWrapper> allActivityList = new List<MyActivitiesWrapper>();


		try{
			MyActivitiesControllerHelper.buildCondition(filterData);
			allActivityList = MyActivitiesControllerHelper.getMyActivities(startDate, endDate,true);
            // TEMP - Saving user perferences.
           // MyActivitiesController.saveUserActivityPreferences(filterData);
		}catch(Exception exp) {
			System.debug(EXP+exp.getMessage()+':'+exp.getLineNumber());
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'getFilterActivities', '', TSK_EVT, 'Error occur during fetching filtered activities'));
			throw new AuraHandledException(Label.Error);
		}
		return allActivityList;
	}
    // This method saves users personal activitiy filter preferences
	@AuraEnabled
    public static void saveUserActivityPreferences(List<MyActivitiesControllerHelper.ConditionWrapper> filterData, String dateRangeKey, String startDate, String endDate, String sortableField, String sortOrder){
		startDate = String.escapeSingleQuotes(startDate);
		endDate = String.escapeSingleQuotes(endDate);
		sortableField = String.escapeSingleQuotes(sortableField);
		sortOrder = String.escapeSingleQuotes(sortOrder);
		dateRangeKey = String.escapeSingleQuotes(dateRangeKey);
       try{
           MyActivitiesControllerHelper.saveUserActivityPreferences(filterData, startDate, endDate, sortableField, sortOrder, dateRangeKey);
        }catch(Exception ex){
			ConnectedLog.LogException('MyActivitiesController','saveUserActivityPreferences',ex);
           throw new AuraHandledException(Label.Error);
        } 
    }
    
	@AuraEnabled(cacheable=false)
	public static ATS_UserOwnershipModal currentUserWithOwnership() {
		return BaseController.getCurrentUserWithOwnership();
	}

	@AuraEnabled(cacheable=false)
	public static Object getAttachmentDetails(String contactId) {
		Object attachDetail = null;
		try {
			String contId = String.escapeSingleQuotes(contactId);
			Contact con = MyActivitiesControllerHelper.getTalentAccountDetails(contId);
			Map<String,Object> params = new Map<String,Object>();
			params.put('recordId',con.AccountId);
			attachDetail = BaseController.performServerCall('ATS','TalentDocumentFunctions','getTalentResumeModel',params);
		}catch(Exception exp) {
			System.debug(EXP+exp.getMessage()+':'+exp.getLineNumber());
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'getAttachmentDetails', '', TSK_EVT, 'Error occur during fetching attchment details'));
			throw new AuraHandledException(Label.Error);
		}
		return attachDetail;
	}

	@AuraEnabled(cacheable=false)
	public static MyActivitiesControllerHelper.AdditionalProfileDetails getProfileDetails(String contactId) {
		MyActivitiesControllerHelper.AdditionalProfileDetails addlProfileDetails = null;		
		try{
			addlProfileDetails = MyActivitiesControllerHelper.getProfileDetails(contactId);
		} catch(Exception exp) {
			System.debug(EXP+exp.getMessage()+':'+exp.getLineNumber());
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'getProfileDetails', '', 'Contact', 'Error occur during fetching profile details'));
			throw new AuraHandledException(Label.Error);
		}
		return addlProfileDetails;
	}

	@AuraEnabled(cacheable=false)
	public static G2Details getG2Details(String contactId) {
		String conId = String.escapeSingleQuotes(contactId);
		G2Details g2Data = new G2Details();
		try {
			
			String taskSoql = 'SELECT Id, Description, CreatedDate, createdBy.Name FROM Task WHERE whoId =: conId AND Activity_Type__c=\'G2\' ORDER BY CreatedDate DESC LIMIT 1';
			System.debug('taskSoql:'+taskSoql);
			List<Task> taskList = Database.query(taskSoql);
			if(!taskList.isEmpty()) {
				g2Data.hasG2 = true;
				g2Data.lastModifiedDate = taskList[0].CreatedDate;
				g2Data.lastCreatedBy = taskList[0].CreatedBy.Name;
				g2Data.comments = taskList[0].Description;
			}
		} catch(Exception exp){
			System.debug(EXP+exp.getMessage()+':'+exp.getLineNumber());
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'getG2Details', '', 'Task', 'Error occur during fetching G2 details'));
			throw new AuraHandledException(Label.Error);
		}
		return g2Data;
	}

	public class G2Details {
		G2Details(){}
		@AuraEnabled public Datetime lastModifiedDate{get; set;}
		@AuraEnabled public String lastCreatedBy{get; set;}
		@AuraEnabled public String comments{get; set;}
		@AuraEnabled public Boolean hasG2{get; set;}
	}

	

	public class MyActivitiesInitWrapper {
		public MyActivitiesInitWrapper(){}
		@AuraEnabled public List<MyActivitiesWrapper> myAcitvitiesWrapperList{get; set;}
		@AuraEnabled public ATS_UserOwnershipModal currentUserWithOwnership{get; set;}
		@AuraEnabled public List<MyActivitiesControllerHelper.ConditionWrapper> filterList{get; Set;}
		@AuraEnabled public String userPrefOption {get;set;}
		@AuraEnabled public Date startDate{get;set;}
		@AuraEnabled public Date endDate{get;set;}
		@AuraEnabled public String dateRangeKey{get;set;}
		@AuraEnabled public String sortableField{get;set;}
		@AuraEnabled public String sortOrder{get;set;}
	}

	

	/*
	@Developer : Neel Kamal
	@Date : 11/18/2019
	@ Parameter : Task and List<TaskID> 
	@Description : massUpdate method will update Task records. First parameter task have values which need to update records having ids in 2nd parameter.
					Task object will build list of Task record by iterating over list of taskid assign value to task fields from task object parameter.
					Javascript already have resctriction for not sending more than 150 IDs in list.
	*/
	@AuraEnabled
	public static String massUpdate(Task task, List<ID> idList){
		
		if(idList != null && !idList.isEmpty()) {
			final List<Task> taskList = new List<task>();
			for(Id taskId : idList) {
				final Task tsk = new task(Id=taskId);
				if(String.isNotBlank(task.OwnerId)) {
					tsk.OwnerId = task.OwnerId;
				}
				if(String.isNotBlank(task.Status) && !task.Status.equalsIgnoreCase('--None--')) {
					tsk.Status = task.Status;				
				}
				if(String.isNotBlank(task.Type) && !task.Type.equalsIgnoreCase('--None--')) {
					tsk.Type = task.Type;
				}
				if(String.isNotBlank(task.Priority) && !task.Priority.equalsIgnoreCase('--None--')) {
					tsk.Priority = task.Priority;
				}
				if(String.isNotBlank(task.Subject)) {
					tsk.Subject = task.Subject;
				}
				if(task.ActivityDate != null) {
					tsk.ActivityDate = task.ActivityDate;
					
				}
				if(String.isNotBlank(task.Description)) {
					tsk.Description = task.Description;
				}
				taskList.add(tsk);
			}

			try {
				final List<Database.SaveResult> srList = Database.update(taskList, false);
				
				String errStr = '';
				for(Database.SaveResult sr : srList) {
					if(!sr.isSuccess()) {
						for(Database.Error err : sr.getErrors()) {
							errStr += err.getStatusCode() +' : '+ err.getMessage() + ' Affected Fields::: ' + err.getFields();
						}
						System.debug(errStr);
					}
					errStr = '';
				}
			}catch(DmlException exp) {
				System.debug('Exception-----'+exp);
				logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'massUpdate', '', 'Task', 'Error occur during mass update'));
				throw new AuraHandledException(Label.Error);
			}
		}
		return 'SUCCESS';
	}

	@AuraEnabled
	public static String massDelete(List<ID> taskIds, List<ID> eventIds){
		final List<Task> taskList = new List<Task>();
		final List<Event> eventList = new List<Event>();
		
		try{
			if(taskIds != null && !taskIds.isEmpty()) {
				for(Id tId : taskIds) {
					final Task tsk = new Task(Id=tId);
					taskList.add(tsk);
				}
				delete taskList;
			}

			if(eventIds != null && !eventIds.isEmpty()) {
				for(Id evtId : eventIds) {
					final Event evt = new Event(Id=evtId);
					eventList.add(evt);
				}
				delete eventList;
			}
		}catch(DmlException exp){			
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'massDelete', '', TSK_EVT, 'Error occur during mass delete'));
			throw new AuraHandledException(Label.Error);
		}
		return 'SUCCESS';
	}
	@AuraEnabled
	public static String deleteActivity(ID activityID) {
		try {
			Database.delete(activityId);
		}catch(DMLException exp) {
			logException(Core_Log.logException(MY_ACTVTY_CTRL, exp.getMessage(), 'deleteActivity', '', TSK_EVT, 'Error occur during delete'));
			throw new AuraHandledException(Label.Error);
		}
		return 'SUCCESS';
	}
	private static void logException(log__c logEx) {
        final List<log__c> lstLogs = new List<log__c>();
        lstLogs.add(logEx);
        Database.insert(lstLogs, false);
    }
}