public without sharing class MyActivitiesControllerHelper  {
	public static String eventCondition='';
	public static String taskCondition='';
	public static List<MyActivitiesControllerHelper.ConditionWrapper> filterOnResult = new List<MyActivitiesControllerHelper.ConditionWrapper>();	
	private static final  String EVT = 'Event';
	private static final  String TSK = 'Task';
	private static final Integer ZERO = 0; 
	private MyActivitiesControllerHelper(){}
	private static final String USER_ACTIVITY_FILTER_PREFERENCES = 'ActivityFilter';
	public static final String MY_DEFAULT='MyDefault';
	private static final String MY_CALL_SHEET='MyCallSheet';
	//public static final String userPrefDateLiterals = 'NO_LITERALS';
	public static String sortableFieldCol{get;set;}
	public static String sortableTaskFieldApi{get;set;}
	public static String sortableEventFieldApi{get;set;}
	public static String sortOrder{get;set;}

	/*
	Fetches Activities based on filter
	*/
	public static List<MyActivitiesWrapper> getMyActivities(String startDate, String endDate, Boolean isFilter) {

		final ID ownerId = UserInfo.getUserId();
		final List<MyActivitiesWrapper> allActivityListNew = new List<MyActivitiesWrapper>();
		List<Task> taskListNew = new List<task>();
		List<Event> eventListNew = new List<Event>();
		Date startDt;
		Date endDt; 
		String eventQuery = 'SELECT Id, whoid, who.name,  who.RecordType.Name, type, subject , WhatId, what.Name, ownerid,owner.Name , Description, AccountId, '
							+'StartDateTime, EndDateTime, Pre_Meeting_Notes__c,First_Visit__c, Submittal_Interviewers__c, Interview_Status__c, Not_Proceeding_Reason__c'							
							+' from event where  OwnerId =: ownerId and type not in( \'Linked\',\'Applicant\') ';
        
		String taskQuery = 'SELECT Id, whoid, who.name, who.RecordType.Name, Type,subject, Status, ActivityDate, WhatId, what.Name, Priority, ownerid,owner.Name, Description, AccountId'
							+' from task where OwnerId =: ownerId ' 
							+' and status not in ( \'Waiting on someone else\', \'Deferred\') ';
		
		if(String.isNotBlank(startDate) && String.isNotBlank(endDate)) {
			startDt = Date.valueOf(startDate);
			endDt = Date.valueOf(endDate);
			//endDt = Date.newInstance(endDt.year(),endDt.month(),endDt.day());
			Time startTime = Time.newInstance(0,0,0,0);
			Time endTime = Time.newInstance(23,59,59,0);

			Datetime startDateTime = Datetime.newInstance(startDt.year(),startDt.month(),startDt.day(),startTime.hour(),startTime.minute(),startTime.second());
			Datetime endDateTime = Datetime.newInstance(endDt.year(),endDt.month(),endDt.day(),endTime.hour(),endTime.minute(),endTime.second());
			
			eventQuery += ' and StartDateTime >=: startDateTime and EndDateTime <=: endDateTime';
			//eventQuery += ' and StartDateTime >=: startDt and StartDateTime <=: endDt';
			taskQuery += ' and ActivityDate >=: startDt and ActivityDate <=: endDt';
		}

		if(!isFilter) {
			eventQuery += ' and who.RecordType.Name IN (\'Client\', \'Talent\')';
			taskQuery +=  ' and who.RecordType.Name IN (\'Client\', \'Talent\')';
		}

		if(String.isNotBlank(eventCondition)) {
			eventQuery += eventCondition;
		}

		if(String.isNotBlank(taskCondition)) {
			taskQuery += taskCondition;
		}	
			
		String taskLimit = System.Label.MyActivitiesTaskLimit;
		String eventLimit = System.Label.MyActivitiesEventLimit;

		

		//Adding sorting on class level
		MyActivitiesWrapper.SORT_BY = sortableFieldCol;
		MyActivitiesWrapper.SORT_ORDER = sortOrder;

		//Adding sorting in SOQL		
		if(String.isNotBlank(sortableTaskFieldApi)) {
			taskQuery += ' ORDER BY '+ sortableTaskFieldApi + ' ' + sortOrder +', ActivityDate DESC, Lastmodifieddate DESC limit ' + taskLimit;
		}
		else {
			taskQuery += ' ORDER BY ActivityDate DESC, Lastmodifieddate DESC limit ' + taskLimit;
		}

		if(String.isNotBlank(sortableEventFieldApi)) {
			eventQuery += ' ORDER BY ' + sortableEventFieldApi + '  ' + sortOrder + ', StartDateTime DESC, Lastmodifieddate DESC limit ' + eventLimit ;
		}
		else {
			eventQuery += ' ORDER BY  StartDateTime DESC, Lastmodifieddate DESC limit ' + eventLimit ;
		}
		//eventQuery += ' ORDER BY ' + sortString + ' StartDateTime DESC, Lastmodifieddate DESC limit ' + eventLimit ;
		//taskQuery += ' ORDER BY '+ sortString +' ActivityDate DESC, Lastmodifieddate DESC limit ' + taskLimit;

		System.debug('getMyActivities::eventQuery:::::'+eventQuery); //don't delete this debug
		System.debug('getMyActivities::taskQuery::::::'+taskQuery); //don't delete this debug
		
		if(String.isNotBlank(taskCondition) || !isFilter){ 
			taskListNew =  Database.query(taskQuery);
		}
		if(String.isNotBlank(eventCondition) || !isFilter){
			eventListNew =  Database.query(eventQuery);			
		}
		
		

		if(!filterOnResult.isEmpty() && isFilter) {
			for(MyActivitiesControllerHelper.ConditionWrapper cond:filterOnResult) {
				if(cond.objName == EVT && cond.fieldValue != null) {
					if(cond.fieldValue.contains(',')) { // if both are selected
						List<String> values	 = cond.fieldValue.split(',');
						if(!values.isEmpty()) {
							for (String value:values){
								allActivityListNew.addAll(MyActivitiesControllerHelper.getActivityEventWrapper(value,eventListNew));
							}
						}
					} else {//if condition has one filter selected

						allActivityListNew.addAll(MyActivitiesControllerHelper.getActivityEventWrapper(cond.fieldValue,eventListNew));
					}
				}
			}
		}
		if(!eventListNew.isEmpty() && !isFilter) {			
			for(Event evt:eventListNew) {				
				final MyActivitiesWrapper activity = new MyActivitiesWrapper(evt);
				
				allActivityListNew.add(activity);
			}
		}

		if(!taskListNew.isEmpty()) {
			for(Task tsk:taskListNew) {
				final MyActivitiesWrapper activity = new MyActivitiesWrapper(tsk);
				allActivityListNew.add(activity);
			}			
		}
		
		allActivityListNew.sort();
		return allActivityListNew;
	}

	/*
	Fetches Additional information contact for profile tab
	*/
	public static AdditionalProfileDetails getProfileDetails(String contactId) {
		final AdditionalProfileDetails addlProfileDetails = new AdditionalProfileDetails();		
		String conId = String.escapeSingleQuotes(contactId);
		final Contact con = getTalentAccountDetails(conId);
		final String accountId = con.AccountId;
			
			final String soqlString = 'SELECT CreatedDate FROM Talent_Document__c' 
								+' WHERE Talent__c =: accountId AND mark_for_deletion__c = false  AND' 
								+' Document_Type__c != \'Other\' AND Document_Type__c != \'communities\' AND HTML_Version_Available__c = True  ORDER BY Default_Document__c DESC,CreatedDate DESC LIMIT 1';

		
			final List<Talent_Document__c> resumeDetails = Database.query(soqlString);
			if(!resumeDetails.isEmpty() && resumeDetails[0].CreatedDate != null) {
					addlProfileDetails.resumeLastUpdatedDate = resumeDetails[0].CreatedDate.date();
			}

			final String accountSoql = 'SELECT Talent_Latest_Submittal_Status__c,Talent_Latest_Submittal_Timestamp__c,Do_Not_Contact__c,Talent_Current_Employer_Text__c,Current_Employer__c,Current_Employer__r.Name FROM Account WHERE Id=:accountId LIMIT 1';
			
			final List<Account> accountDetails = Database.query(accountSoql);
			List<Order> ordList = new List<Order>();
			ordList = [ SELECT Id, Submittal_Not_Proceeding_Reason__c, Comments__c, ShipToContactId,Display_LastModifiedDate__c, Status  
					from Order 
					where ShipToContactId =: conId order by Display_LastModifiedDate__c desc limit 1];
		
			if(!accountDetails.isEmpty()) {	
				addlProfileDetails.doNotContact = accountDetails[0].Do_Not_Contact__c;
				if(accountDetails[0].Talent_Latest_Submittal_Status__c != null) {
					addlProfileDetails.recentSubmittal = accountDetails[0].Talent_Latest_Submittal_Status__c;
				}
				else{
					addlProfileDetails.recentSubmittal = '';
				}
				if(accountDetails[0].Talent_Latest_Submittal_Timestamp__c != null) {
					addlProfileDetails.recentSubmittalDate = accountDetails[0].Talent_Latest_Submittal_Timestamp__c.date();
				}
				if(accountDetails[0].Talent_Current_Employer_Text__c != null) {
					addlProfileDetails.currentEmployerText = accountDetails[0].Talent_Current_Employer_Text__c;
				}
				else{
					addlProfileDetails.currentEmployerText = '';
				}
				if(accountDetails[0].Current_Employer__c != null) {
					addlProfileDetails.currentEmployerId = accountDetails[0].Current_Employer__c;
				}
				else{
					addlProfileDetails.currentEmployerId = '';
				}
				
				if(accountDetails[0].Current_Employer__r.Name != null) {
					addlProfileDetails.currentEmployerName = accountDetails[0].Current_Employer__r.Name;
				}
				else{
					addlProfileDetails.currentEmployerName = '';
				}

			}
			if(!ordList.isEmpty()) {
				addlProfileDetails.comments = ordList[0].Comments__c;
			}
		
		return addlProfileDetails;
	}

	/*
	Wrapper class to serialize filters
	*/
	public class ConditionWrapper {
		public ConditionWrapper() {}
		@AuraEnabled public  String fieldName{get;Set;}
		@AuraEnabled public  String fieldValue{get;Set;}
		@AuraEnabled public  String fieldLabel{get;Set;}
		@AuraEnabled public  String operator{get;Set;}
		@AuraEnabled public  String objName{get;Set;}
		@AuraEnabled public  Boolean isfilteronResult{get;Set;}
	}
	/*
		Parse the filters to prepare Activities Where condition
	*/
	public static void buildCondition(List<ConditionWrapper> conditions) {
		List<ConditionWrapper> eventFilter = new List<ConditionWrapper>();
		List<ConditionWrapper> taskFilter = new List<ConditionWrapper>();
		Integer noTaskCounter = 0, noEventCounter = 0;
		for(ConditionWrapper cond: conditions) {
			
			if(cond.objName.equalsIgnoreCase(TSK)) {
				if(String.isNotBlank(cond.fieldValue)) {
					taskFilter.add(cond);
						
				} else {
					noTaskCounter++;
					taskFilter = new List<ConditionWrapper>();
				}
			} 

			else if(cond.objName.equalsIgnoreCase(EVT)) {
				if(String.isNotBlank(cond.fieldValue) ) {
					if(cond.fieldName.equalsIgnoreCase('description')) {
						filterOnResult.add(cond);
					} else {
						eventFilter.add(cond);
					}
				} else {
					noEventCounter++;
					eventFilter = new List<ConditionWrapper>();
				}
			}

			else if(cond.objName.equalsIgnoreCase('Both')) {
				if(String.isNotBlank(cond.fieldValue)) {
					taskFilter.add(cond);
					eventFilter.add(cond);						
				} else {
					noTaskCounter++;
					noEventCounter++;
					taskFilter = new List<ConditionWrapper>();
					eventFilter = new List<ConditionWrapper>();
				}

			}
		}

		if(noTaskCounter==0 && !taskFilter.isEmpty()) {
			stringifyCondition(taskFilter,TSK);
		}

		if(noEventCounter == 0 && !eventFilter.isEmpty()) {
			stringifyCondition(eventFilter,EVT);
		}

	}
	/*
	Prepares Activities Where condition
	*/
	public static void stringifyCondition( List<ConditionWrapper> conditions, String objName) {
		String activityCondition='';
		for(ConditionWrapper cond : conditions) {
			if(String.isBlank(activityCondition)) {
				activityCondition = getCondition(cond,'AND');
			} else {
				if(activityCondition.contains(cond.fieldName)) {// with same api field ex: Not proceeding on Type and other fields on type
					activityCondition = activityCondition.substring(0, activityCondition.indexOf(cond.fieldName)) +  '(' 
									+ activityCondition.substring(activityCondition.indexOf(cond.fieldName), activityCondition.length());
					activityCondition += getConditionModifier(cond, 'OR' );
				} else {
					activityCondition += getCondition(cond, 'AND');
				}
			}
		}

		if(objName.equalsIgnoreCase(TSK)) {
			taskCondition = activityCondition;
		} else if(objName.equalsIgnoreCase(EVT)) {
			eventCondition = activityCondition;
		}
	}
	/*
	Utility method
	*/
	private static String getFormatedValueHavingInClause(String values) {
		String formattedValue = '';
		if(String.isNotBlank(values)) {
			for(String str :  values.split(',')) {
				if(String.isBlank(formattedValue)) {
					formattedValue = '\'' + str.trim() + '\'';
				}
				else {
					formattedValue += ', \'' + str.trim() + '\'';
				}
			}
		} else {

			formattedValue = '\' \'';
		}	

		return formattedValue;
	}
	/*
	Prepares where condition
	*/
	private static String getCondition(ConditionWrapper cond, String operator) {
		String condition='';
		
		if(String.isBlank(condition)) {
			if(cond.operator.equalsIgnoreCase('IN')) {
				condition = ' '+operator+' '+ cond.fieldName + ' ' +cond.operator + '( ' +getFormatedValueHavingInClause(cond.fieldValue) +' )';
			}
						
			else {
				condition = ' '+operator+' '+ cond.fieldName + cond.operator + ' \''+cond.fieldValue+'\'';	
			}
		}
		return condition;
	}
	
	private static String getConditionModifier(ConditionWrapper cond, String operator) {
		String condition='';
		condition = ' '+operator+' '+ cond.fieldName + ' ' +cond.operator + '( ' +getFormatedValueHavingInClause(cond.fieldValue) +' )' +' ) ';
		
		return condition;
	}
	/*
		Filter method to filter events based on with & without notes in description fields
	*/
	private static List<MyActivitiesWrapper> getActivityEventWrapper(String filterData,List<Event> evtList) {
		List<MyActivitiesWrapper> activityList = new List<MyActivitiesWrapper>();
		if(evtList.size() > 0) {
			for(Event evt:evtList) {				
				MyActivitiesWrapper activity = new MyActivitiesWrapper(evt);
				if(filterData.equalsIgnoreCase('with notes')) {
					if(activity.type != null && activity.type.equalsIgnoreCase('interviewing')) {
						if(fetchParsedComments(activity) != null) {
							activityList.add(activity);
						}
					} else if(activity.descripton != null && !activity.descripton.equals('')) {
						activityList.add(activity);
					} 
				} else if(filterData.equalsIgnoreCase('without notes')) {
					if(activity.type != null && activity.type.equalsIgnoreCase('interviewing') && activity.descripton != null && !activity.descripton.equals('')) {
						if(fetchParsedComments(activity) == null) {
							activityList.add(activity);
						}

					} else if(activity.descripton == null || activity.descripton == '') {
						activityList.add(activity);
					}
				}
			}
		}
		return activityList;
	}
	/*
		Parse description fields for Interviewing type events
	*/
	private static MyActivitiesWrapper fetchParsedComments(MyActivitiesWrapper activity) {
		MyActivitiesWrapper newActivity;
		String cmt = escapeSpecialCharacter(activity.descripton);
		Comments commentsData = (Comments) System.JSON.deserialize(cmt, Comments.class);
		if((commentsData.CandidateComments != null && !commentsData.CandidateComments.equals('')) || 
			(commentsData.ClientComments != null && !commentsData.ClientComments.equals(''))) {
			newActivity = activity;
				
		}
		return newActivity;
	}

	/*
		Fetches Account information
	*/	
	public static Contact getTalentAccountDetails(String contactId) {
		return [SELECT AccountId FROM Contact WHERE Id =: contactId LIMIT 1];

	}
	
	/*
		Wrapper class to serialize Json Additional contact details on profile tab
	*/
	public class AdditionalProfileDetails {
		public AdditionalProfileDetails(){}
		@AuraEnabled public Date resumeLastUpdatedDate{get; set;}
		@AuraEnabled public String recentSubmittal{get; set;}
		@AuraEnabled public Date recentSubmittalDate{get; set;}
		@AuraEnabled public String comments{get; set;}
		@AuraEnabled public Boolean doNotContact{get; set;}
		@AuraEnabled public String currentEmployerText{get; set;}
		@AuraEnabled public String currentEmployerId{get; set;}
		@AuraEnabled public String currentEmployerName{get; set;}
	}

	/*
		Wrapper class to deserialize description field
	*/
	public class Comments {
		public Comments() {}
		@AuraEnabled public String ClientComments{get; set;}
		@AuraEnabled public String CandidateComments{get; set;}

	}

	/*
		Fetches MyActivities Filter Settings
	*/
	public static  List<ConditionWrapper> fetchUserPrefSettings(String userPref) {
		
		List<ConditionWrapper> defaultfilterJSON = new List<ConditionWrapper>();
		List<MyActivityFilterSettings__mdt> defaultFiltersList = [SELECT Id, Default_Filter_Preferences__c FROM MyActivityFilterSettings__mdt where DeveloperName =: userPref ];

		if(defaultFiltersList.size() > 0) {
			defaultfilterJSON = (List<ConditionWrapper>)JSON.deserialize(defaultFiltersList[0].Default_Filter_Preferences__c,List<ConditionWrapper>.class);
		}

		return defaultfilterJSON;
	}

	/*
	There are some special character like Single quote('), Double Quote("), Carriage return(\r) or New Line(\n). Need to remove these character before deserialze of JSON.
	*/
	public static String escapeSpecialCharacter(String cmt) {
		String escapedJSON=cmt;
		if(String.isNotBlank(cmt) && cmt.deleteWhitespace().length() > 2) {
			String CLIENT_COMMENT =  '"ClientComments"';
			String CAND_COMMENT =  '"CandidateComments"';
			Integer clCmtIndex = cmt.indexOf(CLIENT_COMMENT);
			Integer candCmtIndex = cmt.indexOf(CAND_COMMENT);
			String candCmt; 
			String clntCmt;
			if(clCmtIndex == -1) {
				candCmt = cmt.substring(candCmtIndex + CAND_COMMENT.length()+1);
				candCmt = candCmt.substring(candCmt.indexOf('"')+1);
				candCmt = candCmt.substring(0,candCmt.lastIndexOf('"'));
			}
			else if(candCmtIndex == -1) {
				clntCmt = cmt.substring(clCmtIndex + CLIENT_COMMENT.length()+1);
				clntCmt = clntCmt.substring(clntCmt.indexOf('"')+1);
				clntCmt = clntCmt.substring(0,clntCmt.lastIndexOf('"'));
			}
			else if(candCmtIndex > clCmtIndex) {
				clntCmt = cmt.substring(clCmtIndex + CLIENT_COMMENT.length()+1, candCmtIndex-1);
				clntCmt = clntCmt.substring(clntCmt.indexOf('"')+1);
				clntCmt = clntCmt.substring(0,clntCmt.lastIndexOf('"'));

				candCmt = cmt.substring(candCmtIndex + CAND_COMMENT.length()+1, cmt.length()-1);
				candCmt = candCmt.substring(candCmt.indexOf('"')+1);
				candCmt = candCmt.substring(0,candCmt.lastIndexOf('"'));
			}
			else {
				candCmt = cmt.substring(candCmtIndex + CAND_COMMENT.length()+1, clCmtIndex-1);
				candCmt = candCmt.substring(candCmt.indexOf('"')+1);
				candCmt = candCmt.substring(0,candCmt.lastIndexOf('"'));

				clntCmt = cmt.substring(clCmtIndex + CLIENT_COMMENT.length()+1, cmt.length()-1);
				clntCmt = clntCmt.substring(clntCmt.indexOf('"')+1);
				clntCmt = clntCmt.substring(0,clntCmt.lastIndexOf('"'));
			}

		
			escapedJSON='{' +  CLIENT_COMMENT + ':"';

			if(String.isNotBlank(clntCmt)) {
				clntCmt = clntCmt.replaceAll('\r','').replaceAll('\n','').replaceAll('\'','\\\'');
				escapedJSON += clntCmt;
			}
			escapedJSON += '",' + CAND_COMMENT + ':"';
			if(String.isNotBlank(candCmt)) {
				candCmt = candCmt.replaceAll('\r','').replaceAll('\n','').replaceAll('\'','\\\'');
				escapedJSON +=  candCmt;
			}

			escapedJSON += '"}';
		}
		return escapedJSON;
	}

	 // This method saves users personal activitiy filter preferences
	@AuraEnabled
    public static void saveUserActivityPreferences(List<MyActivitiesControllerHelper.ConditionWrapper> filterData, String startDate, String endDate, String sortableField, String sortOrder, String dateRangeKey){
			
        String userPrefJSON = JSON.serialize(filterData);
		Date startDte = Date.valueOf(startDate);
		Date endDte = Date.valueOf(endDate);
		Date todayDte = Date.today();
		Integer stDays = todayDte.daysBetween(startDte);
		Integer edDays = todayDte.daysBetween(endDte);
		String dateInfoJSON = dateInfoJSON(stDays, edDays, dateRangeKey);

        List<User_Search_Log__c> existingPrefs = MyActivitiesControllerHelper.getUserActivitiesPreferences();
		if(existingPrefs.size() > 0) {
            User_Search_Log__c existingPref = existingPrefs[0];
            existingPref.Search_Params__c = userPrefJSON;
			existingPref.Search_Params_Continued__c = sortableField + ',' + sortOrder;
			existingPref.Search_Params_ESR2C__c = dateInfoJSON;    
			
            update existingPref;
        }else{
            User_Search_Log__c userPrefObj = new User_Search_Log__c();
            userPrefObj.User__c = UserInfo.getUserId();
            userPrefObj.Type__c = USER_ACTIVITY_FILTER_PREFERENCES; 
			userPrefObj.Search_Params__c = userPrefJSON;
			userPrefObj.Search_Params_ESR2C__c = dateInfoJSON;  
			userPrefObj.Search_Params_Continued__c = sortableField + ',' + sortOrder;
            insert userPrefObj; 
        }
    }


    public static List<User_Search_Log__c> getUserActivitiesPreferences() {
        List<User_Search_Log__c> userPrefList = [SELECT Id,Name,OwnerId,Search_Params__c, Search_Params_Continued__c, Search_Params_ESR2C__c, Search_Params_Continued_ESR2C__c, Type__c,User__c FROM User_Search_Log__c WHERE User__c =: UserInfo.getUserId() AND Type__c =: USER_ACTIVITY_FILTER_PREFERENCES];
        return userPrefList;
    }

	/***
		@author : Neel Kamal
		@date : 02/11/2020
		@description : This method first check if user already have filter in User_Search_log__c then return saved value. 
						Otherwise fetch Default filter from metadata and return the values.
	*/
	public static MyActivitiesController.MyActivitiesInitWrapper fetchFilterCriteria(String userPreferences, String sField, String sOrder) {
		MyActivitiesController.MyActivitiesInitWrapper initWrapper = new MyActivitiesController.MyActivitiesInitWrapper();
		List<ConditionWrapper> filterJSON = new List<ConditionWrapper>();
		
		Date todayDate = System.today();
		Date stDate = System.today().addDays(-90);
		Date edDate = System.today();
		String dateRangeKey = 'custom'; //if no user preferences saved then value will be custom in combox of date range.
		tableColFieldApiMapping(sField, sOrder);

		if(userPreferences.equals(MY_DEFAULT) || userPreferences.equals('NO_UESER_PREF')) {
			List<User_Search_Log__c> userPrefList  = getUserActivitiesPreferences();
			User_Search_Log__c userPref;
			if(userPrefList != null && userPrefList.size() > 0) {
				MyActivitiesController.userPrefOption = MY_DEFAULT;
				userPref= userPrefList.get(0);
				if(String.isNotBlank(userPref.Search_Params_Continued__c)) {
					List<String> sortInfo = (userPref.Search_Params_Continued__c).split(',');
					tableColFieldApiMapping(sortInfo.get(0), sortInfo.get(1));
				}

				Map<String, String> dateInfoMap = (Map<String, String>)JSON.deserialize(userPref.Search_Params_ESR2C__c, Map<String, String>.class);

				Integer fromDays = Integer.valueOf(dateInfoMap.get('startDays'));
				Integer toDays = Integer.valueOf(dateInfoMap.get('endDays'));
				dateRangeKey = dateInfoMap.get('rangeKey');

				stDate = todayDate.addDays(fromDays);
				edDate = todayDate.addDays(toDays);

				filterJSON = (List<ConditionWrapper>)JSON.deserialize(userPref.Search_Params__c,List<ConditionWrapper>.class); //No need to check null of Search_Params__c 			
			}
			else {//If there no save user preferences then get default filter data from metadata
				if(userPreferences.equals('NO_UESER_PREF')) {
					MyActivitiesController.userPrefOption = MY_CALL_SHEET;
				}
				filterJSON = MyActivitiesControllerHelper.fetchUserPrefSettings(MY_CALL_SHEET);
			}
		}
		else {
			if(!userPreferences.equals(MY_CALL_SHEET)) {
				DateTime currDate = DateTime.newInstance(todayDate.year(), todayDate.month(), todayDate.day());  
				Integer numDay = weekdaysMap.get(currDate.format('EEEE'));
				stDate = todayDate.addDays(-(numDay-1));
				edDate = todayDate.addDays(7-numDay);
				dateRangeKey = 'this-week';
			}
			
			filterJSON = MyActivitiesControllerHelper.fetchUserPrefSettings(userPreferences);
			
		}

		initWrapper.startDate = stDate;
		initWrapper.endDate = edDate;
		initWrapper.dateRangeKey = dateRangeKey;
		initWrapper.filterList = filterJSON;

		return initWrapper;
	}


	//This mapping is calculating the Saved Start Date and End date in relative of login date. 
	public static Map<String, Integer> weekdaysMap = new Map<String, Integer>() ; 
	static {
		weekdaysMap.put('Sunday', 1);
		weekdaysMap.put('Monday', 2);
		weekdaysMap.put('Tuesday', 3);
		weekdaysMap.put('Wednesday',4);
		weekdaysMap.put('Thursday', 5);
		weekdaysMap.put('Friday', 6);
		weekdaysMap.put('Saturday', 7);
	}

	/*
		Since sorting colum name is returning from UI. So need to convert inot api name for using the field as SORT field in SOQL.
		No need to set Due Date field mapping since this field will be by default sorting in SOQL.
		No need to sorting for Task/Event column because values are setting while building wrapper class data.
		@author : Neel Kamal
	*/
	public static  void tableColFieldApiMapping(String colName, String sOrder){
		Map<String, String> tableColFieldApiMap = new Map<String, String>();
		tableColFieldApiMap.put(Label.ATS_Contact.toLowerCase(), 'Who.Name');
		tableColFieldApiMap.put(Label.ATS_TYPE.toLowerCase(), 'Activity_Type__c');
		tableColFieldApiMap.put(Label.ATS_SUBJECT.toLowerCase(), 'Subject');
		tableColFieldApiMap.put(Label.ATS_RELATED_TO.toLowerCase(), 'What.name');
		tableColFieldApiMap.put(Label.ATS_STATUS.toLowerCase(), 'Status');
		tableColFieldApiMap.put(Label.ATS_PRIORITY.toLowerCase(), 'Priority');
		//tableColFieldApiMap.put(ATS_TASK_EVENT, 'activityType');
		tableColFieldApiMap.put(Label.ATS_Contact_Type.toLowerCase(),'Who.RecordType.Name');
		
		sortableFieldCol = colName;
		sortableTaskFieldApi = tableColFieldApiMap.get(colName.toLowerCase());
		sortOrder = sOrder;

		if(colName.toLowerCase() == 'status' || colName.toLowerCase() == 'priority') {
			sortableEventFieldApi = '';
		}
		else {
			sortableEventFieldApi = tableColFieldApiMap.get(colName.toLowerCase());
		}
	}

	private static String dateInfoJSON(Integer stDays, Integer edDays, String rangeKey) {
		Map<String, String> dateJSONMap = new Map<String, String>();
		dateJSONMap.put('startDays', String.valueOf(stDays));
		dateJSONMap.put('endDays', String.valueOf(edDays));
		dateJSONMap.put('rangeKey', rangeKey);

		return JSON.serializePretty(dateJSONMap);
	}

}