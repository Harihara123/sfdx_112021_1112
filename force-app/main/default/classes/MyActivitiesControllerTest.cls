@isTest
public class MyActivitiesControllerTest  { 
	//private static Account accTalent;
	//private static Contact conTalent;
	@TestSetup
	static void testDataSetup(){
		List<RecordType> contactRecordTypeIds = [Select Id From RecordType Where SobjectType = 'Contact' and (Name = 'Client' OR Name='Talent')];
        
        User user1 = TestDataHelper.createUser('System Administrator');
        insert user1;

        Account acc1 = TestDataHelper.createAccount();  
        insert acc1;        

        Account acc = TestDataHelper.createAccount();       
        acc.Talent_Latest_Submittal_Status__c='Submitted';  
        acc.Talent_Latest_Submittal_Timestamp__c = System.today();
        acc.Do_Not_Contact__c =false;
        acc.Talent_Current_Employer_Text__c='test test';
        acc.Current_Employer__c=acc1.Id;
        insert acc;
        System.assertEquals(acc.Current_Employer__c, acc1.Id);
        List<Contact> clist = new List<Contact>();
        
        final Contact con1 = TestDataHelper.createContact();
        con1.RecordTypeId = contactRecordTypeIds[0].Id;
        con1.AccountId = acc.Id;
        clist.add(con1);
                

        final Contact con = TestDataHelper.createContact();
        con.RecordTypeId = contactRecordTypeIds[1].Id;
        con.AccountId = acc.Id;
        clist.add(con);

		

		final Contact con2 = TestDataHelper.createContact();
        con2.RecordTypeId = contactRecordTypeIds[1].Id;
        con2.AccountId = acc1.Id;
        clist.add(con2);
        
        insert clist;

		System.assertEquals(con.RecordTypeId, contactRecordTypeIds[1].Id);
        
       // System.runAs(user1) {
            List<Task> tlist = new List<Task>();
            final Task taskObj = new Task();
            taskObj.Type = 'Service Touchpoint';
            taskObj.WhatId = con.AccountId;
            taskObj.Completed_Flag__c = true;
            taskObj.ActivityDate = system.today();
            taskObj.WhoId = con.Id;
            taskObj.Subject = 'Call';
            taskObj.Status = 'Not Started';
            taskObj.Priority = 'Med';
            taskObj.Activity_Type__c='G2';
            tlist.add(taskObj);

            final Task taskObj2 = new Task();
            taskObj2.Type = 'G2';
            taskObj2.WhatId = con.AccountId;
            taskObj2.Completed_Flag__c = true;
            taskObj2.ActivityDate = system.today()-2;
            taskObj2.WhoId = con.Id;
            taskObj2.Subject = 'Call';
            taskObj2.Status = 'In Progress';
            taskObj2.Priority = 'Med';
            taskObj2.Activity_Type__c='Email';
            tlist.add(taskObj2);

            final Task taskObj3 = new Task();
            taskObj3.Type = 'G2';
            taskObj3.WhatId = con1.AccountId;
            taskObj3.Completed_Flag__c = true;
            taskObj3.ActivityDate = system.today()-2;
            taskObj3.WhoId = con1.Id;
            taskObj3.Subject = 'Call';
            taskObj3.Status = 'G2';
            taskObj3.Priority = 'Med';
            taskObj3.Activity_Type__c='G2';
            tlist.add(taskObj3);
            insert tlist;

            final List<Event> elist = new List<Event>();
            final Event evt = new Event();
            evt.WhatId = con.AccountId;
            evt.StartDateTime = system.now();
            evt.WhoId = con.Id;
            evt.Subject = 'Call';
            evt.Type = 'Meal';
            evt.DurationInMinutes = 120;
			evt.Description = 'test';
            elist.add(evt);

            Event evt2 = new Event();
            evt2.WhatId = con.AccountId;
            evt2.StartDateTime = system.now()+3;
            evt2.WhoId = con.Id;
            evt2.Subject = 'Meeting';
            evt2.Type = 'Meeting';
            evt2.DurationInMinutes = 60;
			evt2.Description = 'test';
			elist.add(evt2);

			evt2 = new Event();
            evt2.WhatId = con.AccountId;
            evt2.StartDateTime = system.now()+3;
            evt2.WhoId = con.Id;
            evt2.Subject = 'Submittal';
            evt2.Type = 'Submittal';
            evt2.DurationInMinutes = 60;
			evt2.Description = 'test';
			elist.add(evt2);

			evt2 = new Event();
            evt2.WhatId = con.AccountId;
            evt2.StartDateTime = system.now()+3;
            evt2.WhoId = con.Id;
            evt2.Subject = 'Interviewing';
            evt2.Type = 'Interviewing';
            evt2.DurationInMinutes = 60;
			evt2.Description = '{"CandidateComments":"Technically good","ClientComments":"Technically good"}';

			elist.add(evt2);

            insert elist;

			
		//}
	}

	@IsTest 
	static void testGetMyActivitiesInit() {
		Test.startTest();
			
			//Test data for MyDefault user preferences
			Date todayDate = System.today();
			Date stDate;
			Date edDate;
			List<Event> evt = [select WhatId, whoid, subject, startDateTime, Type, Description  from event]; 
			System.debug('#########evt List############--'+evt);
			MyActivitiesController.MyActivitiesInitWrapper initWrapper = MyActivitiesController.getMyActivitiesInit('MyDefault', 'dueDate', 'asc');
			System.assertEquals(initWrapper.myAcitvitiesWrapperList.size(), 1);
			for(MyActivitiesWrapper myAcitvitiesWrapper : initWrapper.myAcitvitiesWrapperList){
				System.assertEquals(myAcitvitiesWrapper.type, 'Service Touchpoint');
			}
			System.debug('########myActivitiesWrapper#############--'+initWrapper.myAcitvitiesWrapperList);
			//System.assertEquals(initWrapper.myAcitvitiesWrapperList.size(), 0);

			saveUserActivityPreferences();

			List<User_Search_Log__c> uslList = [SELECT Id,Name,OwnerId,Type__c,User__r.name,   Search_Params_ESR2C__c, Search_Params_Continued_ESR2C__c,Search_Params__c FROM User_Search_Log__c WHERE type__c='ActivityFilter'];
			System.assertEquals(uslList.size(), 1);

			MyActivitiesController.getMyActivitiesInit('MyDefault', 'subject', 'asc');
			MyActivitiesController.getMyActivitiesInit('MyDefault', 'activityType', 'asc');

			initWrapper = MyActivitiesController.getMyActivitiesInit('MyDefault', 'contactName', 'asc');
			System.assertNotEquals(initWrapper.myAcitvitiesWrapperList.size(), 0);

			MyActivitiesController.getMyActivitiesInit('MyCallSheet', 'priority', 'asc');
			initWrapper = MyActivitiesController.getMyActivitiesInit('MyEventThisWeek', 'contactType', 'asc');
			
			DateTime currDate = DateTime.newInstance(todayDate.year(), todayDate.month(), todayDate.day());  
			Integer numDay = MyActivitiesControllerHelper.weekdaysMap.get(currDate.format('EEEE'));
			stDate = todayDate.addDays(-(numDay-1));
			edDate = todayDate.addDays(7-numDay);

			System.assertEquals(initWrapper.startDate, stDate);

		Test.stopTest();
		//MyTaskThisWeek
		//MyEventThisWeek
		//MyCallSheet
	}

	@IsTest 
	static void testGetMyActivities() {
		Test.startTest();
			Date todayDate = System.today();
			Date stDate = todayDate.addDays(-60);
			Date edDate = todayDate.addDays(10);
			List<MyActivitiesWrapper> activitiesList = MyActivitiesController.getMyActivities(String.valueOf(stDate), String.valueOf(edDate), true);
			try{
				MyActivitiesController.getMyActivities('23we', String.valueOf(edDate), true);
			}catch(Exception asEx){
				System.debug('success');
			}
		Test.stopTest();
	}

	@IsTest
	static void testGetFilterActivities() {
		Test.startTest();
			final List<MyActivitiesControllerHelper.ConditionWrapper> conditionList = new List<MyActivitiesControllerHelper.ConditionWrapper>();
           
			MyActivitiesControllerHelper.ConditionWrapper condition4 = new MyActivitiesControllerHelper.ConditionWrapper();
			condition4.fieldName = 'Status';
			condition4.fieldValue = 'Not Started,In Progress,Completed';
			condition4.fieldLabel = 'Not Started,In Progress,Completed';
			condition4.objName = 'Task';
			condition4.operator = 'IN';
			condition4.isfilteronResult = false;
			conditionList.add(condition4);   

			condition4 = new MyActivitiesControllerHelper.ConditionWrapper();
			condition4.fieldName = 'Type';
			condition4.fieldValue = 'Attempted Contact,BD Call,Call,Email,G2,To Do,Performance Feedback,Reference Check,Service Touchpoint,Other';
			condition4.fieldLabel = 'AttemptedContact_taskType,BDCall_taskType,Call_taskType,Email_taskType,G2_taskType,ToDo_taskType,PerformanceFeedback_taskType,ReferenceCheck_taskType,ServiceTouchpoint_taskType,Other_taskType';
			condition4.objName = 'Task';
			condition4.operator = 'IN';
			condition4.isfilteronResult = false;
			conditionList.add(condition4); 



			final MyActivitiesControllerHelper.ConditionWrapper condition3 = new MyActivitiesControllerHelper.ConditionWrapper();
			condition3.fieldName = 'Type';
			condition3.fieldValue = 'Not Proceeding%';
			condition3.fieldLabel = 'NotProceeding_eventType';
			condition3.objName = 'Event';
			condition3.operator = ' LIKE';
			condition3.isfilteronResult = false;

			conditionList.add(condition3);
			final MyActivitiesControllerHelper.ConditionWrapper condition6 = new MyActivitiesControllerHelper.ConditionWrapper();
			condition6.fieldName = 'Type';
			condition6.fieldValue = 'Meeting,Meal,Service Touchpoint,Performance Feedback,Presentation,Other,Internal Interview,Interviewing,Offer Accepted';
			condition6.fieldLabel = 'Meeting_eventType,Meal_eventType,ServiceTouchpoint_eventType,PerformanceFeedback_eventType,Presentation_eventType,Other_eventType,InternalInterview_eventType,Interviewing_eventType,OfferAccepted_eventType';
			condition6.objName = 'Event';
			condition6.operator = 'IN';
			condition6.isfilteronResult = false;
			conditionList.add(condition6);

			final MyActivitiesControllerHelper.ConditionWrapper condition7 = new MyActivitiesControllerHelper.ConditionWrapper();
			condition7.fieldName = 'description';
			condition7.fieldValue = 'with notes,without notes';
			condition7.objName = 'Event';
			condition7.operator = 'IN';
			condition7.isfilteronResult = true;

			conditionList.add(condition7);	
			
			final MyActivitiesControllerHelper.ConditionWrapper condition2 = new MyActivitiesControllerHelper.ConditionWrapper();
			condition2.fieldName = 'who.RecordType.name';
			condition2.fieldValue = 'Talent,Client';
			condition2.fieldLabel = 'Client,Talent';
			condition2.objName = 'Both';
			condition2.operator = 'IN';
			condition2.isfilteronResult = false;

			conditionList.add(condition2);		

			final Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;
			insert new DRZSettings__c(Name = 'OpCo', Value__c='Aerotek, Inc;TEKsystems, Inc.');

			Account accTalent = [select id from account where Talent_Latest_Submittal_Status__c='Submitted' LIMIT 1];
			Contact conTalent = [select id from contact where AccountId =: accTalent.id LIMIT 1];

			final Order o = new Order();
			o.Name = 'TestOrder';
			o.AccountId = accTalent.Id;
			o.ShipToContactId = conTalent.Id;
			//o.OpportunityId = opp2.Id;
			o.Status = 'Linked';
			o.EffectiveDate = System.today();
			o.RecordTypeId = recTypeId;
			o.Comments__c = 'test';
			insert o;
			System.assertEquals(o.AccountId,  accTalent.Id);
			try {
				Date todayDate = System.today();
				Date stDate = todayDate.addDays(-60);
				Date edDate = todayDate.addDays(10);
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'subject', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'subject', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'contactName', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'contactName', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'type', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'type', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'relatedTo', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'relatedTo', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'activityType', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'activityType', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'dueDate', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'dueDate', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'status', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'status', 'desc');

				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'priority', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'priority', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'activityType', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'activityType', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'contactType', 'asc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'contactType', 'desc');
				MyActivitiesController.getFilterActivities(conditionList,String.valueOf(stDate),String.valueOf(edDate), 'switchDefaulttest', 'desc');

				MyActivitiesController.getFilterActivities(conditionList,'2323sdfsf',String.valueOf(edDate), 'type', 'asc'); //negative testing
			}
			catch(exception e){
				System.debug('error : ' + e);
			}
		Test.stopTest();
	}

	@isTest 
	public static void saveUserActivityPreferences(){
		List<MyActivitiesControllerHelper.ConditionWrapper> condList = new List<MyActivitiesControllerHelper.ConditionWrapper>();
 		MyActivitiesControllerHelper.ConditionWrapper cond = new MyActivitiesControllerHelper.ConditionWrapper();
		cond.fieldLabel = '';
		cond.fieldName = 'Type';
		cond.fieldValue = '';
		cond.isfilteronResult = false;
		cond.objName = 'Task';
		cond.operator = 'IN';
		condList.add(cond);

		cond = new MyActivitiesControllerHelper.ConditionWrapper();
		cond.operator = 'IN';
		cond.objName = 'Event';
		cond.isfilteronResult = false;
		cond.fieldValue = 'Meeting,Meal,Out Of Office,Service Touchpoint,Performance Feedback,Presentation,Other,Internal Interview,Interviewing';
		cond.fieldName = 'Type';
		cond.fieldLabel = 'Meeting_eventType,Meal_eventType,OutOfOffice_eventType,ServiceTouchpoint_eventType,PerformanceFeedback_eventType,Presentation_eventType,Other_eventType,InternalInterview_eventType,Interviewing_eventType';
		condList.add(cond);

		cond = new MyActivitiesControllerHelper.ConditionWrapper();
		cond.operator = 'IN';
		cond.objName = 'Both';
		cond.isfilteronResult = false;
		cond.fieldValue = 'Client,Talent';
		cond.fieldName = 'who.RecordType.Name';
		cond.fieldLabel = 'Client,Talent';
		condList.add(cond);

		cond = new MyActivitiesControllerHelper.ConditionWrapper();
		cond.operator = 'IN';
		cond.objName = 'Task';
		cond.isfilteronResult = false;
		cond.fieldValue = '';
		cond.fieldName = 'Status';
		cond.fieldLabel = '';
		condList.add(cond);

		cond = new MyActivitiesControllerHelper.ConditionWrapper();
		cond.operator = 'IN';
		cond.objName = 'Event';
		cond.isfilteronResult = true;
		cond.fieldValue = 'With Notes,Without Notes';
		cond.fieldName = 'Description';
		cond.fieldLabel = 'With Notes,Without Notes';
		condList.add(cond);

		Date td = System.today();
		String stdate = String.valueOf(td.addDays(-30));
		String eddate = String.valueOf(td.addDays(10));
		MyActivitiesController.saveUserActivityPreferences(condList, 'custom',stdate, eddate, 'relatedTo','asc');
		//check if already saved user prefernces 
		MyActivitiesController.saveUserActivityPreferences(condList, 'this-week',stdate, eddate, 'status', 'asc');

		//CODE Coverage of exception logging
		try{
			MyActivitiesController.saveUserActivityPreferences(condList, 'last-week', '234dfdf', eddate, 'subject', 'asc');
		}catch(Exception exp) {
		}
		
	}

	@IsTest
	public static void testCurrentUserWithOwnership() {
		Test.startTest();
			MyActivitiesController.currentUserWithOwnership();
		Test.stopTest();
	}

	@IsTest
	public static void testGetAttachmentDetails() {
		//Test.startTest();
			List<Contact> ctList = [Select id from Contact];
			 MyActivitiesController.getAttachmentDetails(ctList.get(0).id);
		//Test.startTest();
	}

	 @IsTest
    public static  void testResumeDetails() {
		Test.startTest();
			Account account = CreateTalentTestData.createTalentAccount();
        
			Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 5242880, 'Somefile.docx', 'Resume');
			//Bellow Attachment is not used anywhere so blocked it
			//Attachment[] atts = [SELECT Id from Attachment where ParentId = :validAttachmentDocx.Id];		
        
			List<RecordType> contactRecordTypeIds = [Select Id From RecordType Where SobjectType = 'Contact' and (Name = 'Client' OR Name='Talent')];
			Contact con = TestDataHelper.createContact();
			con.RecordTypeId = contactRecordTypeIds[1].Id;
			con.AccountId = account.Id;
			insert con;
			System.assertEquals(con.FirstName,'Bob');
			MyActivitiesController.getProfileDetails(con.Id);

        Test.stopTest();
    }

	@IsTest
	static void testMassUpdate() {
		Test.startTest();
		Account accTalent = [select id from account where Talent_Latest_Submittal_Status__c='Submitted' LIMIT 1];
		Contact conTalent = [select id from contact where AccountId =: accTalent.id LIMIT 1];
		
		
		Task taskObjNew = new Task();
        taskObjNew.Type = 'Email';
        taskObjNew.WhatId = accTalent.Id;
        taskObjNew.Completed_Flag__c = true;
        taskObjNew.ActivityDate = system.today();
        taskObjNew.WhoId = conTalent.Id;
        taskObjNew.Subject = 'Call';
        taskObjNew.Status = 'In Progress';
        taskObjNew.Priority = 'High';
        taskObjNew.Activity_Type__c='Call';
        taskObjNew.OwnerId=UserInfo.getUserId();
		taskObjNew.Description = 'test';
		
		List<ID> taskIDList = new List<ID>();
		for(Task tsk : [select id from Task where type not in('G2', 'Reference Check') ]){
			taskIDList.add(tsk.id);
		}
		
		MyActivitiesController.massUpdate(taskObjNew,taskIDList);
		Test.stopTest();
	}
	
	@IsTest
	public static void testMassDelete() {
		List<ID> taskIDList = new List<ID>();
		List<ID> evtIDList = new List<ID>();
		Test.startTest();

			for(Task tsk : [select Id from Task where type not in('G2', 'Reference Check')]) {
				taskIDList.add(tsk.Id);
			}
			for(Event evt : [select id from Event where type in('Meeting', 'Meal', 'Out of Office', 'Performance Feedback', 'Presentation', 'Other','Internal Interview')]) {
				evtIDList.add(evt.Id);
			}

			MyActivitiesController.massDelete(taskIDList, evtIDList);
		Test.stopTest();
	}

    @IsTest
	public static void testDeleteActivity() {
		Test.startTest();
			CreateTalentTestData.createTalent();

			Task tsk = [select id from Task limit 1];

			MyActivitiesController.deleteActivity(tsk.Id);
		Test.stopTest();
		
	}
	@IsTest
	public static void negTestDeleteActivity() {
		Test.startTest();
			try {
				final String startDate = String.valueOf(System.today());
				final String endDate = String.valueOf(System.today()-7);
				//MyActivitiesController.getMyActivitiesInit('1234567890',endDate);
				MyActivitiesController.getContactDetails('01234567890');
				MyActivitiesController.deleteActivity('00U59900002S37rEAC'); //fake id to get exception
			}catch(Exception exp) {
				System.debug(exp);
			}
		Test.stopTest();
	}
	@IsTest
	public static void testGetProfileDetails() {
		Test.startTest();
			CreateTalentTestData.createTalent();
			Contact con = [Select id FROM Contact limit 1];

			MyActivitiesController.getProfileDetails(con.Id);
			MyActivitiesController.getContactDetails(con.Id);
			MyActivitiesController.getG2Details(con.Id);
		Test.stopTest();
	}

	String userPrefMyDefault = '[{"operator":"IN","objName":"Task","isfilteronResult":false,"fieldValue":"","fieldName":"Type","fieldLabel":""},{"operator":"IN","objName":"Event","isfilteronResult":false,"fieldValue":"Meeting,Meal,Out Of Office,Service Touchpoint,Performance Feedback,Presentation,Other,Internal Interview,Interviewing","fieldName":"Type","fieldLabel":"Meeting_eventType,Meal_eventType,OutOfOffice_eventType,ServiceTouchpoint_eventType,PerformanceFeedback_eventType,Presentation_eventType,Other_eventType,InternalInterview_eventType,Interviewing_eventType"},{"operator":"IN","objName":"Both","isfilteronResult":false,"fieldValue":"Client,Talent","fieldName":"who.RecordType.Name","fieldLabel":"Client,Talent"},{"operator":"IN","objName":"Task","isfilteronResult":false,"fieldValue":"","fieldName":"Status","fieldLabel":""},{"operator":"IN","objName":"Event","isfilteronResult":true,"fieldValue":"With Notes,Without Notes","fieldName":"Description","fieldLabel":"With Notes,Without Notes"}]';
}