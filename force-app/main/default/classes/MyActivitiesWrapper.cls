global class MyActivitiesWrapper implements Comparable {

	@AuraEnabled public String Id {get; Set;}
	@AuraEnabled public String contactName {get; Set;}
	@AuraEnabled public String contactId {get; Set;}
	@AuraEnabled public String type {get; set;}
	@AuraEnabled public Date dueDate {get; set;}
	@AuraEnabled public String subject {get; set;}
	@AuraEnabled public String relatedTo {get; set;}
	@AuraEnabled public String relatedToId {get; set;}
	@AuraEnabled public String status {get; set;}
	@AuraEnabled public String priority {get; set;}
	@AuraEnabled public String assignedTo {get; set;}
	@AuraEnabled public String assignedToId {get; set;}
	@AuraEnabled public String contactType {get; set;}
	@AuraEnabled public String activityType {get; set;}
	@AuraEnabled public String descripton {get; set;}
	@AuraEnabled public String preMeetingNote {get; set;}
	@AuraEnabled public Datetime StartDateTime {get; set;}
	@AuraEnabled public Datetime EndDateTime {get; set;}
	@AuraEnabled public String eventCategory {get; set;}
	@AuraEnabled public Boolean firstVisit {get; set;}
	@AuraEnabled public String submittalInterviewers {get; set;}
	@AuraEnabled public String interviewStatus {get; set;}
	@AuraEnabled public Boolean isMassEditable{get;set;}
	@AuraEnabled public Boolean isMassDeletable{get;set;}
	@AuraEnabled public String notProceedReason{get;set;}
	@AuraEnabled public String accountId{get;set;}

	private static final String SORT_ASC = 'ASC';
	private static final String SORT_DESC = 'DESC';
	public static String SORT_ORDER = MyActivitiesControllerHelper.sortOrder;
	public static String SORT_BY = 'dueDate'; //value set for by default
	/*public static String DUE_DATE_SORT = 'dueDate';
	public static String CONTACT_NAME_SORT = 'contactName';
	public static String TYPE_SORT = 'type';
	public static String SUBJECT_SORT = 'subject';
	public static String RELATEDTO_SORT = 'relatedTo';
	public static String STATUS_SORT = 'status';
	public static String PRIORITY_SORT = 'priority';
	public static String CONTACT_TYPE_SORT = 'contactType';
	public static String ACTIVITY_TYPE = 'activityType';*/

	//yourDate.format('dd-MM-yyyy');
	//public MyActivitiesWrapper(String id, String conName, String conId, String type, Date dueDate, String subject, String relatedTo, String relatedToId, String status, String priority, String assignedTo, String assignedToId, String contactType, String activityType, String description) {
		public MyActivitiesWrapper(Task tsk) {
			this.Id = (String)tsk.id;
			this.contactName = (String)tsk.Who.Name;
			this.contactId  = (String)tsk.WhoId;
			this.accountId = tsk.AccountId;
			this.type = tsk.Type;
			//Datetime dt =  datetime.newInstance(tsk.ActivityDate.year(), tsk.ActivityDate.month(),tsk.ActivityDate.day());
			//this.dueDate.newInstance(this.dueDate.year(), this.dueDate.month(),this.dueDate.day());
			//this.dueDate = dt.formatGMT('MM/dd/yyyy');
			this.dueDate = tsk.ActivityDate;
			this.subject = tsk.Subject;
			this.relatedTo = tsk.What.name;
			this.relatedToId = (String)tsk.WhatId;
			this.status = tsk.Status;
			this.priority = tsk.Priority;
			this.assignedTo = tsk.Owner.Name;
			this.assignedToId = (String)tsk.OwnerId;
			this.contactType = tsk.Who.RecordType.Name;
			this.activityType = 'Task';
			this.descripton = (String)tsk.Description;
			this.isMassEditable = taskRecordEditable(tsk.Type);
			this.isMassDeletable = isDeletable(tsk.type);
		}

		public MyActivitiesWrapper(Event evt) {
			this.Id = (String)evt.Id;
			this.contactName = evt.Who.Name;
			this.contactId  = (String)evt.WhoId;			
			this.accountId = evt.AccountId;
			this.type = evt.Type;
			//Datetime dt =  datetime.newInstance(evt.StartDateTime.year(), evt.StartDateTime.month(),evt.StartDateTime.day());
			//this.dueDate = dt.formatGMT('MM/dd/yyyy');
			this.dueDate = evt.StartDateTime.date();
			this.subject =evt.Subject;
			this.relatedTo = evt.What.name;
			this.relatedToId = (String)evt.WhatId;
			this.status = '';
			this.priority = '';
			this.assignedTo = evt.Owner.Name;
			this.assignedToId = (String)evt.OwnerId;
			this.contactType = evt.who.RecordType.Name;
			this.activityType = 'Event';
			if('Interviewing'.equals(type)) {
				this.descripton = MyActivitiesControllerHelper.escapeSpecialCharacter((String)evt.Description);
			}
			else {
				this.descripton = (String)evt.Description;
			}
			this.StartDateTime = evt.StartDateTime;
			this.EndDateTime = evt.EndDateTime;
			this.preMeetingNote = evt.Pre_Meeting_Notes__c;
			this.firstVisit = evt.First_Visit__c;
			this.submittalInterviewers = evt.Submittal_Interviewers__c;
			this.interviewStatus = evt.Interview_Status__c;
			this.notProceedReason = evt.Not_Proceeding_Reason__c;
			this.isMassEditable = false;
			this.eventCategory = categorizeEvent( evt.Type);
			this.isMassDeletable = isDeletable(evt.type);
		}
	 
	 /*
	 categorises events based on type
	 */
	 private static String categorizeEvent(String type) {
		String[] general = new String[] {'Meeting', 'Meal', 'Out of Office', 'Performance Feedback', 'Presentation', 'Other','Internal Interview'};
		String eventCategory;
		if(general.contains(type)) {
			eventCategory =  'general';
		}
		else if('Interviewing'.equals(type)) {
			eventCategory = 'Interviewing';
		}
		else {
			eventCategory = 'Submittal';
		}

		return eventCategory;	
	 } 

	 /*
	 Marks Activities which are editable or not
	 */
	 private static Boolean taskRecordEditable(String type) {
		Boolean isMassEditable = true;

		if(type.equalsIgnoreCase('Merge Completed') || type.equalsIgnoreCase('G2') || type.equalsIgnoreCase('Reference Check')) {
			isMassEditable = false;
		}

		return isMassEditable;
	 }

	 /*
	 Marks Activities which are deletable 
	 */
	 private static Boolean isDeletable(String type) {
		Boolean isMassDeletable = false;

		 if(!(type == 'Reference Check' || type =='Submitted' || type.left(14) =='Not Proceeding' || type =='Interviewing' || 
			  type =='Offer Accepted' || type =='Started' || type =='Merge Completed' || type.equalsIgnoreCase('G2') || type.equalsIgnoreCase('Reference Check'))){
			isMassDeletable = true;
		 }
		 return isMassDeletable;
	 }

	 
	 /*
		Since we are getting data by using separate Object (Task and Event) So need to do sorting first on SOQL then on class level.
	 */
	 global Integer compareTo(Object compareTo) {
		MyActivitiesWrapper compareToEventTask = (MyActivitiesWrapper)compareTo;

		switch on SORT_BY {
			when 'dueDate'{
				return sortByDueDate(compareToEventTask);
			}
			when 'contactName' {
				return sortByContactName(compareToEventTask);
			}
			when 'type' {
				return sortByType(compareToEventTask);
			}
			when 'subject' {
				return sortBySubject(compareToEventTask); 
			}
			when 'relatedTo' {
				return sortByRelatedTo(compareToEventTask); 
			}
			when 'status' {
				return  sortByStatus(compareToEventTask);
			}
			when 'priority' {
				return sortByPriority(compareToEventTask);
			}
			when 'contactType' {
				return sortByContactType(compareToEventTask);
			}
			when 'activityType' {
				return sortByActivityType(compareToEventTask);
			}
			when else {
				return sortByDueDate(compareToEventTask);
			}
		}
    }

	private Integer sortByDueDate(MyActivitiesWrapper wrapper) {
		if(SORT_ORDER == SORT_ASC) {
			if (dueDate == wrapper.dueDate) {
				return 0;
			}   
			if (dueDate < wrapper.dueDate) {
				return 1;
			}   
			return -1;
		}
		else {
			if (wrapper.dueDate == dueDate) {
				return 0;
			}   
			if (wrapper.dueDate < dueDate) {
				return 1;
			}   
			return -1;
		}
		
	}

	private Integer sortByContactName(MyActivitiesWrapper wrapper) {
		String thisString = this.contactName.toLowerCase();
		String wrapString = wrapper.contactName.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}	
	}

	private Integer sortByType(MyActivitiesWrapper wrapper) {
		String thisString = this.type.toLowerCase();
		String wrapString = wrapper.type.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}
	}

	private Integer sortBySubject(MyActivitiesWrapper wrapper) {
		String thisString = this.subject.toLowerCase();
		String wrapString = wrapper.subject.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}	
	}

	private Integer sortByRelatedTo(MyActivitiesWrapper wrapper) {
		String thisString = this.relatedTo.toLowerCase();
		String wrapString = wrapper.relatedTo.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}
	}

	private Integer sortByStatus(MyActivitiesWrapper wrapper) {
		String thisString = this.status.toLowerCase();
		String wrapString = wrapper.status.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}
	}

	private Integer sortByPriority(MyActivitiesWrapper wrapper) {
		String thisString = this.priority.toLowerCase();
		String wrapString = wrapper.priority.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}	
	}

	private Integer sortByActivityType(MyActivitiesWrapper wrapper) {
		String thisString = this.activityType.toLowerCase();
		String wrapString = wrapper.activityType.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}	
	}

	private Integer sortByContactType(MyActivitiesWrapper wrapper) {
		String thisString = this.activityType.toLowerCase();
		String wrapString = wrapper.activityType.toLowerCase();
		if(SORT_ORDER == SORT_ASC) {
			return thisString.compareTo(wrapString);
		}
		else {
			return wrapString.compareTo(thisString);
		}	
	}
}