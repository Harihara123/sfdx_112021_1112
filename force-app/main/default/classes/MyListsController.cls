public class MyListsController  {

    @AuraEnabled
    public static List<Tag_Definition__c> currentUserLists() {
        return [SELECT Id, Candidate_Count__c, LastModifiedDate, Tag_Name__c FROM Tag_Definition__c WHERE OwnerID =: UserInfo.getUserId() ORDER BY Tag_Name__c ASC];
    }

    @AuraEnabled
    public static void deleteList(Tag_Definition__c deleteList) {
        delete deleteList;
    }

    @AuraEnabled
    public static Tag_Definition__c insertList(String tagName) {
        Tag_Definition__c newTag = new Tag_Definition__c();

        String scrubbedTagName = String.escapeSingleQuotes(tagName);
        newTag.Tag_Name__c = scrubbedTagName;


        Integer duplicateCheck = [SELECT COUNT() FROM Tag_Definition__c WHERE Tag_Name__c =: newTag.Tag_Name__c AND OwnerId =: UserInfo.getUserId()];
        if(duplicateCheck > 0) {
            throw new DuplicateException('Cannot Create List With Duplicate Name');
        }

        if(String.isBlank(newTag.Tag_Name__c) || String.isBlank(newTag.Tag_Name__c)) {
            throw new DuplicateException('Cannot Create List With Empty Name');
        } 

        insert newTag; 

        return [SELECT Id, Candidate_Count__c, LastModifiedDate, Tag_Name__c FROM Tag_Definition__c WHERE Id =: newTag.Id];

    }

    @AuraEnabled
    public static String updateList(Tag_Definition__c changedList) {
        changedList.Tag_Name__c = String.escapeSingleQuotes(changedList.Tag_Name__c);

        Integer duplicateCheck = [SELECT COUNT() FROM Tag_Definition__c WHERE Tag_Name__c =: changedList.Tag_Name__c AND OwnerId =: UserInfo.getUserId()];

        if(duplicateCheck > 0) {
            return 'Unable to save List with duplicate Name';
        } 

        if(String.isBlank(changedList.Tag_Name__c) || String.isBlank(changedList.Tag_Name__c)) {
            throw new DuplicateException('Cannot Create List With Empty Name');
        } 

        try {
            update changedList;
        } catch (DmlException ex) {
            return 'Unable to save List, please refresh the page and try again.';
        }

        return 'SUCCESS';
    }

    public class DuplicateException extends Exception {}
}