@isTest
public class MyListsControllerTest  {

    @isTest
    public static void currentUserLists_givenUserWithLists_returnsListsBelongingToUser() {
        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'Test List';

        insert testList;

        Test.startTest();
            List<Tag_Definition__c> resultLists = new List<Tag_Definition__c>();
            resultLists = MyListsController.currentUserLists();

            System.assertEquals(1, resultLists.size(), 'The inserted list should have been returned.');
        Test.stopTest();
    }

    @isTest
    public static void currentUserLists_givenUserWithNoLists_returnsNoLists() {
        Test.startTest();
            List<Tag_Definition__c> resultLists = new List<Tag_Definition__c>();
            resultLists = MyListsController.currentUserLists();

            System.assertEquals(0, resultLists.size(), 'The inserted list should have been returned.');
        Test.stopTest();
    }

    @isTest
    public static void deleteList_givenListToDelete_deletesList() {
        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'Test List';

        insert testList;

        Test.startTest();
            List<Tag_Definition__c> preResultLists = new List<Tag_Definition__c>();
            List<Tag_Definition__c> postResultLists = new List<Tag_Definition__c>();

            preResultLists = MyListsController.currentUserLists();

            MyListsController.deleteList(testList);

            postResultLists = MyListsController.currentUserLists();

            System.assertEquals(1, preResultLists.size(), 'The initial list should be in the system.');
            System.assertEquals(0, postResultLists.size(), 'The list should have been removed from the system.');
        Test.stopTest();
    }

    @isTest
    public static void updateList_givenListToUpdate_updatesTagName() {
        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'Test List';

        insert testList;

        testList.Tag_Name__c = 'Changed Name';

        Test.startTest();
            String successString = MyListsController.updateList(testList);
            Tag_Definition__c updatedList = MyListsController.currentUserLists()[0];

            System.assertEquals('SUCCESS', successString, 'The update method should have told us there was a successful update.');
            System.assertEquals('Changed Name', updatedList.Tag_Name__c, 'The name of the list should have been updated');

        Test.stopTest();
    }

    @isTest
    public static void updateList_givenListToUpdateWithDuplicateName_doesNotupdateTagName() {
        Tag_Definition__c testList = new Tag_Definition__c();
        testList.Tag_Name__c = 'Test List';

        Tag_Definition__c testList2 = new Tag_Definition__c();
        testList2.Tag_Name__c = 'Test List 2';

        insert testList;
        insert testList2;

        testList.Tag_Name__c = 'Test List';

        Test.startTest();
            String successString = MyListsController.updateList(testList);
            Tag_Definition__c updatedList = [SELECT Id, Tag_Name__c FROM Tag_Definition__c WHERE Id =: testList2.Id LIMIT 1];

            System.assertEquals('Unable to save List with duplicate Name', successString, 'The update method should have told us there was not a successful update.');
            System.assertEquals('Test List 2', updatedList.Tag_Name__c, 'The name of the list should not have been updated');

        Test.stopTest();
    }

    @isTest
    public static void insertList_givenListWithNonDuplicateName_insertsList() {
        Tag_Definition__c insertedTag;

        Test.startTest();
            insertedTag = MyListsController.insertList('Test Name');
        Test.stopTest();

        System.assertNotEquals(null, insertedTag.Id, 'The Id should have been set on the inserted value.');
    }

    @isTest
    public static void insertList_givenListWithDuplicateName_doesNotInsertList() {
        Tag_Definition__c testTag = new Tag_Definition__c();
        testTag.Tag_Name__c = 'Test Name';
        insert testTag;

        Boolean insertFailed = false;

        Test.startTest();
            try {
                MyListsController.insertList('Test Name');
            } catch(MyListsController.DuplicateException ex) {
                insertFailed = true;
            }
        Test.stopTest();

        System.assert(insertFailed, 'The list with a duplicate name should not have been inserted.');
    }


}