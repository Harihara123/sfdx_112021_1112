public class N2PExternalDBController  {
    Static String errorType = 'ATS_N2P_EXTERNAL_DB_CONTROLLER_EXCEPTIONS';
    Static String N2P_G2_FLOW_ALGORITHM_NAME = 'G2';
    Static String N2P_Log_Type_ID = 'N2P/Postgres';


                   

    public static Boolean createEditModalRecordPostN2P(Contact acutalContact, Contact contact, Account acutalAccount,Account account, String g2Comments,String g2Source,Integer duration,String parsedInfo, Boolean g2checked, Boolean disable){
        return N2PExternalDBController.insertN2PG2RecordInPostGres(acutalContact,contact,acutalAccount,account,g2Comments,N2P_G2_FLOW_ALGORITHM_NAME,duration,parsedInfo, g2checked, disable);
    }

    public static Boolean createEditModalRecordNoN2P(Contact originalContact, Contact con, Account originalAccount, Account acc, String source, Integer duration, String g2Comments, Boolean g2checked, Boolean disable){
        return N2PExternalDBController.insertN2Ppostgres(originalContact, con, originalAccount, acc, source, duration, g2Comments, g2checked, disable);
    }



    // This inserts a reocrd in PostGres post edit modal
    public static Boolean insertN2PG2RecordInPostGres(Contact acutalContact, Contact con, Account acutalAccount,Account acc, String g2Comments,String algorithm,Integer duration,String parsedInfo, Boolean g2checked, Boolean disable){
        Boolean insertStatus = false;
        try{
            Map<String, Object> parserRes = (Map<String, Object>)JSON.deserializeUntyped(parsedInfo);

            Map<String, Object> accSkillFullString = new Map<String, Object>();
            Map<String, Object> actualAcSkillFullString = new Map<String, Object>();

            if(acc.Skills__c != null){
                accSkillFullString = (Map<String, Object>)JSON.deserializeUntyped(acc.Skills__c);
            }
            if(acutalAccount.Skills__c != null){
                actualAcSkillFullString = (Map<String, Object>)JSON.deserializeUntyped(acutalAccount.Skills__c);
            }

  

            List<Object> accSkillString = (List<Object>) accSkillFullString.get('skills');
            List<Object> actualAccSkillString = (List<Object>) actualAcSkillFullString.get('skills');


            User me = BaseController.getCurrentUser();
            User userDetails = TalentAddEditController.getUserDetailsN2P();
            n2p_n2pfields__x row = new n2p_n2pfields__x ();
            row.parent_id__c = Decimal.valueOf(con.N2pTransactionId__c);
            row.inputnotes__c = g2Comments;
            row.parsingalgorithm__c = algorithm;
            row.userid__c = me.Id;
            row.duration__c = duration;
            row.g2_completed__c = g2checked;

            row.jobtitle__c = con.Title;

            if(userDetails != null) {
                if(userDetails.Email != null)
                    row.useremail__c = userDetails.Email;

                if(userDetails.Name != null)
                    row.username__c = userDetails.Name;

                if(userDetails.Office__c != null)
                    row.officename__c = userDetails.Office__c;

                if(userDetails.Office_Code__c != null)
                    row.officecode__c = userDetails.Office_Code__c;
            }

            // Verify if user override parse responded JobTitle
            String parsJT = (String)parserRes.get('JobTitle');
            row.jobtitleparseridentified__c = parsJT;
            if( parsJT!= null && parsJT.length()>0){
                if(parsJT != con.Title){
                    row.jobtitleuserremoved__c = parsJT;
                    row.jobtitleuseradded__c = con.Title;
                }
            }else if(acutalContact.Title != null && (acutalContact.Title != con.Title)){
                row.jobtitleuserremoved__c = acutalContact.Title;
                row.jobtitleuseradded__c = con.Title;
            }


            row.currentemployer__c = acc.Talent_Current_Employer_Text__c;
            String parseCE = (String)parserRes.get('CurrentEmployer');
            row.currentemployeruseridentified__c = parseCE;
            if( parseCE != null && parseCE.length()>0){
                if(parseCE != acc.Talent_Current_Employer_Text__c){
                    row.currentemployeruserremoved__c = parseCE;
                    row.currentemployeruseradded__c = acc.Talent_Current_Employer_Text__c;
                }
            }else if(acutalAccount.Talent_Current_Employer_Text__c != null && (acutalAccount.Talent_Current_Employer_Text__c != acc.Talent_Current_Employer_Text__c)){
                row.currentemployeruserremoved__c = acutalAccount.Talent_Current_Employer_Text__c;
                row.currentemployeruseradded__c = acc.Talent_Current_Employer_Text__c;
            }

            row.currentcompanydisabled__c = disable;

            row.skills__c = acc.Skills__c;
            // Verify if user override parser responded skills
            List<Object> parsSkillStr = (List<Object>)parserRes.get('Skills');


            List<String> parsSkills = new List<String>();
            List<String> accSkills = new List<String>();
            List<String> actualAccSkills = new List<String>();

            if(parsSkillStr != null ){
                for(Object obj : parsSkillStr){
                    parsSkills.add(String.valueOf(obj).toLowercase());
                }
            } 
            if(accSkillString != null){
                for(Object obj : accSkillString){                    
                    accSkills.add(String.valueOf(obj).toLowercase());
                }
            }
            if(actualAccSkillString != null){
                for(Object obj : actualAccSkillString){                    
                    actualAccSkills.add(String.valueOf(obj).toLowercase());
                }
            }

            Integer userRemovSkills = parsSkills.size();
            for(String skl : parsSkills){
                if(accSkills.contains(skl.toLowercase())){
                    userRemovSkills=userRemovSkills-1;
                }
            }
            // Check if user removed any skills from actual account
            for(String skl : actualAccSkills){
                if(!accSkills.contains(skl.toLowercase())){
                    userRemovSkills++;
                }
            }
            Integer userAddedSkills = 0;
            for(String skl : accSkills){
                if((!actualAccSkills.contains(skl)) && (!parsSkills.contains(skl))){
                    userAddedSkills= userAddedSkills + 1;
                }
            }
            row.cnt_skillsparseridentified__c = Double.valueOf(parsSkills.size());
            row.cnt_skillsuserremoved__c = Double.valueOf(userRemovSkills);
            row.cnt_skillsuseradded__c = Double.valueOf(userAddedSkills);

            // Maility address - Location
            row.locationcity__c = con.MailingCity;
            row.locationcountry__c = con.MailingCountry;
            row.locationstate__c = con.MailingState;

            Map<String, Object> parserLocation = (Map<String, Object>)parserRes.get('Living');
            String finalLocationStr = (con.MailingCity !=null ? (con.MailingCity + ',') : '') +
                                                      (con.MailingState !=null ? (con.MailingState + ',') : '') +
                                                      (con.MailingState !=null ? con.MailingState : '');
            String parserLocaitonStr = null;
            System.debug('parserLocation '+parserLocation);
            
            if(parserLocation != null && parserLocation.size()>0){                
                
                String parCity = (String)parserLocation.get('City');
                String parState = (String)parserLocation.get('State');
                String parCountry = (String)parserLocation.get('Country');
                parserLocaitonStr = (parCity !=null ? (parCity + ',') : '') +
                                                  (parState !=null ? (parState + ',') : '') +
                                                  (parCountry !=null ? parCountry : '');
                row.locationparseridentified__c = parserLocaitonStr;
                // Check if user removed the parse details
                Boolean userChangedLocation = true;
                if(con.MailingCity == parCity){
                    if(con.MailingCountry == parCountry){
                        if(con.MailingState == parState){
                            userChangedLocation = false;
                        }
                    }
                }
                if(userChangedLocation){
                    row.locationuserremoved__c = row.locationparseridentified__c;
                    row.locationuseradded__c = finalLocationStr;
                }
                
            }else{
                // check if user changed location on just edit modal
                String prevLocationStr = (acutalContact.MailingCity !=null ? (acutalContact.MailingCity + ',') : '') +
                                         (acutalContact.MailingState !=null ? (acutalContact.MailingState + ',') : '') +
                                         (acutalContact.MailingState !=null ? acutalContact.MailingState : '');
                System.debug(prevLocationStr +'_______'+ finalLocationStr);
                if(prevLocationStr != finalLocationStr)
                {
                    row.locationuserremoved__c = prevLocationStr;
                    row.locationuseradded__c = finalLocationStr;
                }          
                                      
            }



            // Relocation details
            String actualLocPrefStr = null;
            String finalLocPrefStr = null;
            List<Object> actualLocPrefs = null;
            List<Object> fianlLocPrefs = null;

            if(acutalAccount.Talent_Preference_Internal__c != null) {
                actualLocPrefStr = acutalAccount.Talent_Preference_Internal__c;
                Map<String, Object> m2 = (Map<String, Object>)JSON.deserializeUntyped(actualLocPrefStr);
                Map<String, Object> geo2 = (Map<String, Object>)m2.get('geographicPrefs');
                actualLocPrefs = (List<Object>)geo2.get('desiredLocation');

             }
             if(account.Talent_Preference_Internal__c != null) {
                finalLocPrefStr = acc.Talent_Preference_Internal__c;
                Map<String, Object> m2 = (Map<String, Object>)JSON.deserializeUntyped(finalLocPrefStr);
                Map<String, Object> geo2 = (Map<String, Object>)m2.get('geographicPrefs');
                fianlLocPrefs= (List<Object>)geo2.get('desiredLocation');                
             }
             Map<String, Object> parserRelLocation = (Map<String, Object>)parserRes.get('Relocation');
             String parserRelLocaitonStr = null;

             if(parserRelLocation != null){                
                

                String parCity = (String)parserRelLocation.get('City');
                String parState = (String)parserRelLocation.get('State');
                String parCountry = (String)parserRelLocation.get('Country');

                parserRelLocaitonStr = (parCity != null ? parCity : '');
                parserRelLocaitonStr = (parserRelLocaitonStr.length()>0 ? (parserRelLocaitonStr +',') : parserRelLocaitonStr) + (parState != null ? parState : '');
                parserRelLocaitonStr = (parserRelLocaitonStr.length()>0 ? (parserRelLocaitonStr +',') : parserRelLocaitonStr) + (parCountry != null ? parCountry : '');

                row.relocationparseridentified__c = parserRelLocaitonStr;
                 
            }

            System.debug('REL == ' + actualLocPrefStr);
            System.debug('REL == ' + finalLocPrefStr);
           // if(actualLocPrefStr != finalLocPrefStr){
                String acCity = null;
                String acState = null;
                String acCountry = null;

                for(Object finalPrefObj : fianlLocPrefs){
                    Map<String, Object> locPref = (Map<String, Object>) finalPrefObj ;
                    acCity = (String)locPref.get('city');
                    acState = (String)locPref.get('state');
                    acCountry = (String)locPref.get('country');
                    Boolean locPrefFound = true;
                    if(actualLocPrefs != null){
                        for(Object actPrefObj: actualLocPrefs){
                            Map<String, Object> actLocPref = (Map<String, Object>) actPrefObj ;
                            String acCity1 = (String)actLocPref.get('city');
                            String acState1 = (String)actLocPref.get('state');
                            String acCountry1 = (String)actLocPref.get('country');
                            if(acCity == acCity1 && acState == acState1 && acCountry == acCountry1){ 
                                locPrefFound = true;

                                acCity = null;
                                acState = null;
                                acCountry = null;                        

                                break;
                            }
                            locPrefFound = false;
                        }
                    }
                    if(!locPrefFound){                        
                        break;
                    }
                }


                String finalRelString = (acCity != null ? acCity : '');
                finalRelString = (finalRelString.length()>0 ? (finalRelString +',') : finalRelString) + (acState != null ? acState : '');
                finalRelString = (finalRelString.length()>0 ? (finalRelString +',') : finalRelString) + (acCountry != null ? acCountry : '');

                row.relocationcity__c =  acCity;
                row.relocationstate__c = acState;
                row.reloactioncountry__c = acCountry;  
                System.debug('LOC '+parserRelLocaitonStr +'==='+ finalRelString);                            
                if(parserRelLocaitonStr != finalRelString){
                    
                    row.relocationuserremoved__c = parserRelLocaitonStr;
                    row.relocationuseradded__c = finalRelString;
                }

                // Check Actual Prefer Got deleted...
                Set<Map<String, Object>> removedPref = new Set<Map<String, Object>>();
                Map<String, Object> userAddPrefOnModal = new Map<String, Object>();

                System.debug('LOC actualLocPrefs ' +actualLocPrefs);
                System.debug('LOC fianlLocPrefs ' +fianlLocPrefs);

                if(actualLocPrefs != null){
                for(Object finalPrefObj : actualLocPrefs){

                    Map<String, Object> locPref = (Map<String, Object>) finalPrefObj;
                    acCity = (String)locPref.get('city');
                    acState = (String)locPref.get('state');
                    acCountry = (String)locPref.get('country');
                    Map<String, Object> locPrefDeleted = null;
                    locPrefDeleted = locPref;

                    for(Object actPrefObj: fianlLocPrefs){
                        Map<String, Object> actLocPref = (Map<String, Object>) actPrefObj ;
                        String acCity1 = (String)actLocPref.get('city');
                        String acState1 = (String)actLocPref.get('state');
                        String acCountry1 = (String)actLocPref.get('country');

                        String tempFinalLoc = (acCity1 != null ? acCity1 : '');
                        tempFinalLoc = (tempFinalLoc.length()>0 ? (tempFinalLoc +',') : tempFinalLoc) + (acState1 != null ? acState1 : '');
                        tempFinalLoc = (tempFinalLoc.length()>0 ? (tempFinalLoc +',') : tempFinalLoc) + (acCountry1 != null ? acCountry1 : '');
                        System.debug('LOC 0 ' + tempFinalLoc +' === '+ finalRelString);
                        if(tempFinalLoc == finalRelString){
                            System.debug('LOC continue');
                            continue;
                        }

                        System.debug('LOC 2 ' + acCity1 +'--'+ acState1 +'--'+ acCountry1);
                        System.debug('LOC 1 '+acCity +'--'+ acState +'--'+ acCountry );
                        if(acCity == acCity1 && acState == acState1 && acCountry == acCountry1)
                        { 
                             System.debug('LOC FOUND ' + locPref);
                             locPrefDeleted = null;
                             //break; 
                        }else{
                            userAddPrefOnModal.put(tempFinalLoc,actLocPref);
                        }
                    }
                    if(locPrefDeleted != null){
                        System.debug('LOC NOT FOUND ' + locPrefDeleted);
                        removedPref.add(locPrefDeleted);
                    }

                     System.debug('LOC NEWLY  ADDED ' + userAddPrefOnModal);
                     Map<String, Object> userAddPrefOnModal1 = new Map<String, Object>();
                     if(actualLocPrefs != null){
                        for(Object finalPrefObj1 : actualLocPrefs){

                            Map<String, Object> locPref2 = (Map<String, Object>) finalPrefObj1;
                            System.debug('LOC1 ' + locPref2);
                            acCity = (String)locPref2.get('city');
                            acState = (String)locPref2.get('state');
                            acCountry = (String)locPref2.get('country');
                            String tempFinalLoc = (acCity != null ? acCity : '');
                            tempFinalLoc = (tempFinalLoc.length()>0 ? (tempFinalLoc +',') : tempFinalLoc) + (acState != null ? acState : '');
                            tempFinalLoc = (tempFinalLoc.length()>0 ? (tempFinalLoc +',') : tempFinalLoc) + (acCountry != null ? acCountry : '');
                            if(userAddPrefOnModal.keySet().contains(tempFinalLoc)){
                                userAddPrefOnModal.remove(tempFinalLoc);
                            }
                        }
                    }
                    System.debug('LOC NEWLY  ADDED 1 ' + userAddPrefOnModal);
                }
                }

                System.debug('Actual Prefer Got Changed...' + removedPref);
                if(removedPref.size()>0){
                    row.relocationuserremoved__c = JSON.serialize(removedPref);
                }
                if(userAddPrefOnModal.size()>0){
                    row.relocationuseradded__c = JSON.serialize(userAddPrefOnModal.values());
                }
           // }

            row.talentaccountid__c = acc.Id;
            row.talentcontactid__c = con.Id;

            User curUser = BaseController.getCurrentUser();
            row.created_by__c = String.valueOf(curUser.Id);
            row.modified_by__c = String.valueOf(curUser.Id);
            DateTime curDT = Datetime.now();
            row.created_date__c = curDT;
            row.modified_date__c = curDT;

            Database.SaveResult sr = Database.insertAsync(row);
            if(!sr.isSuccess()){
                insertStatus = true;
            }
            System.debug('insertN2PG2RecordInPostGres '+insertStatus);
        }catch(Exception ex){
            System.debug(ex);
            ConnectedLog.LogException(N2P_Log_Type_ID, 'insertN2PG2RecordInPostGres', errorType, con.Id, ex);
        }
        return insertStatus;
    }

    @AuraEnabled
   public static Boolean insertN2Ppostgres(Contact originalContact, Contact con, Account originalAccount, Account acc, String source, Integer duration, String g2Comments, Boolean g2checked, Boolean disable) {
    Boolean insertStatus = false;
        try {

            User me = BaseController.getCurrentUser();
            User userDetails = TalentAddEditController.getUserDetailsN2P();
            n2p_n2pfields__x row = new n2p_n2pfields__x ();

            row.created_by__c = String.valueOf(me.Id);
            row.modified_by__c = String.valueOf(me.Id);
            DateTime curDT = Datetime.now();
            row.created_date__c = curDT;
            row.modified_date__c = curDT;
            row.inputnotes__c = g2Comments;
            row.g2_completed__c = g2checked;

            if(userDetails != null) {
                if(userDetails.Email != null)
                    row.useremail__c = userDetails.Email;

                if(userDetails.Name != null)
                    row.username__c = userDetails.Name;

                if(userDetails.Office__c != null)
                    row.officename__c = userDetails.Office__c;

                if(userDetails.Office_Code__c != null)
                    row.officecode__c = userDetails.Office_Code__c;
            }

            if(source == 'modal') {
                row.parsingalgorithm__c = 'Edit Modal';
                row.userid__c = me.Id;
                row.duration__c = duration;

                row.jobtitle__c = con.Title;
                if(originalContact.Title != con.Title) {
                    row.jobtitleuserremoved__c = originalContact.Title;
                    row.jobtitleuseradded__c = con.Title;
                }

				row.currentemployer__c = acc.Talent_Current_Employer_Text__c;
				if(originalAccount.Talent_Current_Employer_Text__c != acc.Talent_Current_Employer_Text__c) {
					row.currentemployeruserremoved__c = originalAccount.Talent_Current_Employer_Text__c;
					row.currentemployeruseradded__c = acc.Talent_Current_Employer_Text__c;
				}
				row.currentcompanydisabled__c = disable;


                row.skills__c = acc.Skills__c;
                List<Object> oriSkills = new List<Object>();
                List<Object> savedSkills = new List<Object>();

                if( originalAccount.Skills__c != null ) {
                    Map<String, Object> origin = (Map<String, Object>) JSON.deserializeUntyped(originalAccount.Skills__c);
                    oriSkills = (List<Object>) origin.get('skills');
					//---------------------------
					System.debug('oriSkills----'+oriSkills);
					Set<Object> oriSkillsSet= new Set<Object>(oriSkills);
					oriSkills= new List<Object>(oriSkillsSet);
					System.debug('oriSkills----'+oriSkillsSet);
					//----------------------------
                }
                if( acc.Skills__c != null){
                    Map<String, Object> saved = (Map<String, Object>) JSON.deserializeUntyped(acc.Skills__c);   
                    savedSkills = (List<Object>) saved.get('skills');
					//---------------------------
					System.debug('savedSkills----'+savedSkills);
					Set<Object> savedSkillsSet= new Set<Object>(savedSkills);
					savedSkills= new List<Object>(savedSkillsSet);
					System.debug('savedSkills----'+savedSkills);
					//----------------------------
                }
				System.debug('oriSkills----'+JSON.deserializeUntyped(acc.Skills__c));
                System.debug('oriSkills----'+oriSkills.size()+'-------savedSkills-----'+savedSkills.size());
				Integer count = 0;
                if( (oriSkills.size() > 0) && (savedSkills.size() > 0) ){
                    for( Object i : savedSkills){
                        for( Object j : oriSkills){
                            if( i == j )
                                count += 1;
                        }
                    }
					//Integer rsremoved= oriSkills.size() - count;
					//rsremoved = Math.abs(rsremoved);
                    row.cnt_skillsuserremoved__c = oriSkills.size() - count;
					//row.cnt_skillsuserremoved__c = rsremoved;
                    row.cnt_skillsuseradded__c = savedSkills.size() - count;
                }
                else if( (oriSkills.size() == 0) && (savedSkills.size() > 0) ) {
                    row.cnt_skillsuseradded__c = savedSkills.size();
                    row.cnt_skillsuserremoved__c = 0;
                }
                else if( (oriSkills.size() > 0) && (savedSkills.size() == 0) ) {
                    row.cnt_skillsuseradded__c = 0;
                    row.cnt_skillsuserremoved__c = oriSkills.size();
                }

                if(acc.Talent_Preference_Internal__c != null){
                    String x = acc.Talent_Preference_Internal__c;
                    Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(x);
                    Map<String, Object> geo = (Map<String, Object>)m.get('geographicPrefs');
                    List<Object> des = (List<Object>)geo.get('desiredLocation');

                    if(originalAccount.Talent_Preference_Internal__c != null) {
                        String y = originalAccount.Talent_Preference_Internal__c;
                        Map<String, Object> m2 = (Map<String, Object>)JSON.deserializeUntyped(y);
                        Map<String, Object> geo2 = (Map<String, Object>)m2.get('geographicPrefs');
                        List<Object> des2 = (List<Object>)geo2.get('desiredLocation');
                    
                

						if(des != null && !des.isEmpty()){
							Map<String, Object> pref = (Map<String, Object>) des[0];

							//row.relocationcity__c = (String) pref.get('city');
							//row.reloactioncountry__c = (String) pref.get('country');
							//row.relocationstate__c = (String) pref.get('state');
                            Object reCity = pref.get('city');
                            Object reCountry = pref.get('country');
                            Object reState =  pref.get('state');

                            row.relocationcity__c = (reCity != null ? (String)reCity : '');
                            row.reloactioncountry__c =  (reCountry != null ? (String)reCountry : '');
                            row.relocationstate__c = (reState != null ? (String)reState : '');

                            if(des2 != null && !des2.isEmpty()){
                                Map<String, Object> pref2 = (Map<String, Object>) des2[0];
                                if( (String)pref.get('city') != (String)pref2.get('city') || (String)pref.get('state') != (String)pref2.get('state') || (String)pref.get('country') != (String)pref2.get('country') ){

                                    row.relocationuserremoved__c = (String)pref2.get('city') + ',' + (String)pref2.get('state') + ',' + (String)pref2.get('country');
                                    row.relocationuseradded__c = (String)pref.get('city') + ',' + (String)pref.get('state') + ',' + (String)pref.get('country');
                                }

                            }
                            else{
                                System.debug('No previous relocation preference');
                                row.relocationuseradded__c = row.relocationcity__c + ',' + row.relocationstate__c + ',' + row.reloactioncountry__c;
                            }
                        } else {
                            if( des2 != null && !des2.isEmpty() ){
                                Map<String, Object> pref2 = (Map<String, Object>) des2[0];
                                row.relocationuserremoved__c = (String)pref2.get('city') + ',' + (String)pref2.get('state') + ',' + (String)pref2.get('country');
                            }
                            else {
                                System.debug('No previous relocation preference');
                            }
                        }
                    }// We may to have consider user entered relocation in the else block # S-182799
                    else{
                        if(des != null && !des.isEmpty()){
                            Map<String, Object> pref = (Map<String, Object>) des[0];
                            Object reCity =  pref.get('city');
                            Object reCountry =  pref.get('country');
                            Object reState =  pref.get('state');

                            row.relocationcity__c = (reCity != null ? (String)reCity : '');
                            row.reloactioncountry__c =  (reCountry != null ? (String)reCountry : '');
                            row.relocationstate__c = (reState != null ? (String)reState : '');
                            row.relocationuseradded__c = row.relocationcity__c + ',' + row.relocationstate__c + ',' + row.reloactioncountry__c;
                        }
                    }

				}

				row.locationcity__c = con.MailingCity;
				row.locationcountry__c = con.MailingCountry;
				row.locationstate__c = con.MailingState;

				//row.locationparseridentified__c = (con.MailingCity != null ? con.MailingCity + ',' : '') + (con.MailingState != null || con.MailingState != '' ? con.MailingState + ',' : '') + (con.MailingCountry != null ? con.MailingCountry : '');
                String locMaCity = (con.MailingCity != null?con.MailingCity:''); 
                String locMaOrigCity = (originalContact.MailingCity != null?originalContact.MailingCity:'');

                String locMaState = (con.MailingState != null?con.MailingState:''); 
                String locMaOrigState = (originalContact.MailingState != null?originalContact.MailingState:'');

                String locMaCountry = (con.MailingCountry != null?con.MailingCountry :''); 
                String locMaOrigCountry = (originalContact.MailingCountry != null?originalContact.MailingCountry:'');

				if((locMaCity.trim()!=locMaOrigCity.trim()) || (locMaState.trim()!=locMaOrigState.trim()) || (locMaCountry!=locMaOrigCountry)) {
					row.locationuserremoved__c = originalContact.MailingCity + ',' + originalContact.MailingState + ',' + originalContact.MailingCountry;
					row.locationuseradded__c = con.MailingCity + ',' + con.MailingState + ',' + con.MailingCountry;
				}

                row.talentaccountid__c = acc.Id;
                row.talentcontactid__c = con.Id;
            }
            
            Database.SaveResult sr = Database.insertAsync(row);
            if(!sr.isSuccess()){
                insertStatus = true;
            }
            System.debug('InsertN2pPostgres: ' + insertStatus);
        }
        catch(Exception e) {
            System.debug(e);
            ConnectedLog.LogException(N2P_Log_Type_ID, 'insertN2Ppostgres', errorType, con.Id, e);
        }
        
        return insertStatus;
   }
}