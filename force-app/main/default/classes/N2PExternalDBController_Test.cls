@IsTest
public class N2PExternalDBController_Test  {

    public testmethod static void testCreateN2PPostEditModalRecord(){

        Test.startTest();
        String parsedInfo = '{\"Relocation\":{\"City\":\"Houston\",\"State\":\"Texas\",\"Country\":\"United States\"},\"Living\":{},\"JobTitle\":\"Senior java developer\",\"DesiredRate\":\"\",\"Skills\":[\"java\",\"python\"],\"G2Comments\":\"Phani lives in washington dc he is good at java, python. He is looking to relocate to Huston as a Senior java developer.\"}';
        Account prevAcc = CreateTalentTestData.createTalent(); 
        prevAcc.Skills__c = '{"skills":["java","python"]}';
        prevAcc.Talent_Preference_Internal__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":"","desiredLocation":[{"country":"United States","state":"Texas","city":"Houston"},{"country":"United States","state":"Kansas","city":"Olathe"}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
        upsert prevAcc;
                 
        Account finalAcc = prevAcc.clone();
        finalAcc.Skills__c = '{"skills":["java","python","mysql","j2ee","development"]}';
        finalAcc.Talent_Preference_Internal__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":"","desiredLocation":[{"country":"United States","state":"New Jersey","city":"Madison"},{"country":"United States","state":"Texas","city":"Houston"},{"country":"United States","state":"Kansas","city":"Olathe"}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';

        upsert finalAcc;

        System.debug('TDBCONTR act acc '+prevAcc.Skills__c);
        System.debug('TDBCONTR fin acc '+finalAcc.Skills__c);

        Contact prevCont = CreateTalentTestData.createTalentContact(prevAcc);
        Contact finCont = prevCont.clone();
        finCont.Title = 'Java developer';
        finCont.N2pTransactionId__c = '1212';
        finCont.MailingCity = 'Hanover';
        finCont.MailingState = 'MD';
        finCont.MailingCountry = 'USA';
        upsert finCont;

        System.debug('TDBCONTR '+prevCont.Title);

        System.debug('TDBCONTR '+prevCont.MailingCity);
        System.debug('TDBCONTR '+prevAcc.Talent_Preference_Internal__c);

        String source= 'n2p';
        String g2Comments = 'Phani lives in washington dc he is good at java, python. He is looking to relocate to Huston as a Senior java developer.';
        Integer duration = 20;
        // acutalContact,contact,acutalAccount,account,g2Comments,g2Source,duration,parsedInfo
        Boolean res = N2PExternalDBController.createEditModalRecordPostN2P(prevCont,finCont,prevAcc,finalAcc,g2Comments,source,duration,parsedInfo, true, true);
        System.debug('res ' +res);
        Test.stopTest();
    }

    //Testing Edit Modal record insert - No N2P tool used
    public testmethod static void testCreateEditModalRecord() {

        Test.startTest();
        Account prevAcc = CreateTalentTestData.createTalent(); 
        prevAcc.Skills__c = '{"skills":["java","python"]}';
        prevAcc.Talent_Preference_Internal__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":"","desiredLocation":[{"country":"United States","state":"Texas","city":"Houston"},{"country":"United States","state":"Kansas","city":"Olathe"}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
        upsert prevAcc;
                 
        Account finalAcc = prevAcc.clone();
        finalAcc.Skills__c = '{"skills":["java","python","mysql","j2ee","development"]}';
        finalAcc.Talent_Preference_Internal__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":"","desiredLocation":[{"country":"United States","state":"New Jersey","city":"Madison"},{"country":"United States","state":"Texas","city":"Houston"},{"country":"United States","state":"Kansas","city":"Olathe"}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';

        upsert finalAcc;

        System.debug('TDBCONTR act acc '+prevAcc.Skills__c);
        System.debug('TDBCONTR fin acc '+finalAcc.Skills__c);

        Contact prevCont = CreateTalentTestData.createTalentContact(prevAcc);
        Contact finCont = prevCont.clone();
        finCont.Title = 'Java developer';
        finCont.N2pTransactionId__c = '1212';
        finCont.MailingCity = 'Hanover';
        finCont.MailingState = 'MD';
        finCont.MailingCountry = 'USA';
        upsert finCont;


        System.debug('TDBCONTR '+prevCont.Title);

        System.debug('TDBCONTR '+prevCont.MailingCity);
        System.debug('TDBCONTR '+prevAcc.Talent_Preference_Internal__c);

        String source= 'modal';
        String g2Comments = 'Alan works at T Rowe Price as a software developer. He loves his job. He currently lives in Hueston but wants to move to SoHo NYC eventually. He currently makes $55/hr. He would like to make 65/hr. He prefers C2H but is open to other options.His contract will be ending in December. He is proficient in java and Seibel .';
        Integer duration = 11;

        Boolean res = N2PExternalDBController.createEditModalRecordNoN2P(prevCont, finCont, prevAcc, finalAcc, source, duration, g2Comments, true, true); 
        System.debug('res ' +res);
    }
}