public with sharing class NOtifyApigeeOnOpportunityDelete
{

  static ApigeeOAuthSettings__c settings;
  public class InvocableApiException extends Exception {}

  public NOtifyApigeeOnOpportunityDelete() 
   {
        settings = new ApigeeOAuthSettings__c();
        settings = ApigeeOAuthSettings__c.getValues('DeleteOpportunity');
        System.debug('settings::::'  + settings);
   }
   
    public static void getAccessToken()
    {        
        oAuth_Connector oauth = new oAuth_Connector();
        oauth.token_url = settings.Token_URL__c;
        oauth.client_Id = settings.Client_Id__c;
        oauth.client_Secret = settings.Client_Secret__c;
        system.debug('****TOKEN B4**** ' + oauth);
        oauth = oauth.getAccessToken(oauth);
        settings.OAuth_Token__c = oauth.access_Token;
        settings.Token_Expiration__c = oauth.expires_In;
        system.debug('****settings**** ' + settings); 
    }
    
    public static Map<String,Opportunity> deleteOpportunity(string opportunityId)
    {
      
         
        settings = new ApigeeOAuthSettings__c();
        settings = ApigeeOAuthSettings__c.getValues('DeleteOpportunity');
        System.debug('settings::::'  + settings);
        
        
        if ((settings.Token_Expiration__c == null) || (settings.Token_Expiration__c < System.now())) 
        {
            getAccessToken();
        }
        
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
        System.debug('settings.OAuth_Token__c >>>>>>>> ' + settings.OAuth_Token__c);
        string endpoint = settings.Service_URL__c + opportunityId + '?type=job';
        req.setEndpoint(endpoint);
        req.setMethod(settings.Service_Http_Method__c);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + settings.OAuth_Token__c);
        req.setTimeout(120000);
        
        String responseBody;
        try 
        {
            HttpResponse response = connection.send(req);
            System.debug('response >>>>>>>> ' + response);
            responseBody = response.getBody();
            System.debug('responseBody >>>>>>>> ' + responseBody);
            
            if (String.isBlank(responseBody)) 
            {
                throw new InvocableApiException('Apigee search service call returned empty response body!');
            }
            else if(response.getStatus() == 'OK' && response.getStatusCode() ==200)
            {
                System.debug('Successfully deleted in APIGEE'); 
            
            }
            else
            {
              System.debug('GATEWAY TIMEOUT');   
            }
             update settings;
        } 
        catch (Exception ex) 
        {
            // Callout to apigee failed
            System.debug(LoggingLevel.ERROR, 'Apigee call to Search Service failed! ' + ex.getMessage());
        }
       
       
        
        return null;
    }



}