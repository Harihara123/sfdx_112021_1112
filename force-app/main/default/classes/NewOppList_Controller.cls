public with sharing class NewOppList_Controller {

    @AuraEnabled
    public static User fetchUser(){
        System.debug('Inside FetchUser NewOppList_Controller');
        Id userId = UserInfo.getUserId();
        User_Organization__c office =null;
        List<User> usrList = [SELECT OPCO__c,Office_Code__c,CompanyName FROM User WHERE Id = :userId LIMIT 1];
        
        User usr=usrList[0];
        
        System.debug('usrList----->'+usrList.size());
        if(usrList.size() > 0 ){
         
         System.debug('usrList----->'+usrList[0].Office_Code__c);    
         
        /* List<Talent_Role_to_Ownership_Mapping__mdt> OwnershipList = [SELECT Opco_Code__c,Talent_Ownership__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: usrList[0].OPCO__c LIMIT 1];
         String ownershipOpco =OwnershipList.size() > 0 ?  OwnershipList[0].Talent_Ownership__c: '';
         String opco = usrList[0].CompanyName;*/
           if(!String.IsBlank(usrList[0].Office_Code__c)){
              List<User_Organization__c> orgList = [select Id, Name from User_Organization__c where Office_Code__c =: usrList[0].Office_Code__c and OpCo_Code__c  =: usrList[0].OPCO__c LIMIT 1];
              
               office = orgList.size() > 0 ? orgList[0]: new User_Organization__c();
               usr.Office_Code__c=office.id;
           }
        }  
          System.debug('USer data '+ usr);
        return usr;        
    }
    
    @AuraEnabled
    public static list<string> getObjName(string recordId){
        String ObjectName = Id.valueOf(recordId).getsObjectType().getDescribe().getName();
        list<string> AccValues = new list<string>();
        if(ObjectName == 'Contact'){
            list<Contact> ConAccName = [select AccountId,Account.Name from Contact where id =:recordId limit 1] ;
            for(Contact x:ConAccName){
                AccValues.add(x.AccountId);
                AccValues.add(x.Account.Name);
                
            }
            //return string.valueof(ConAccName[0].AccountId);
        }else{
            list<Account> ConAccName = [select id,Name from Account where id =:recordId limit 1] ;
            for(Account x:ConAccName){
                AccValues.add(x.Id);
                AccValues.add(x.Name);
            }
            //return string.valueof(recordId);
        }
        return AccValues;
    }
    
    
    @AuraEnabled
    public static Boolean getRTAccessDetailsForProfiles()
    {
        String ProfileUserId = UserInfo.getProfileId();
        List<OpportunityRecordTypeVisibility__c> opptyVisibiltyList = OpportunityRecordTypeVisibility__c.getAll().values();
        for(OpportunityRecordTypeVisibility__c varSkill:opptyVisibiltyList)
        {
            if(varSkill.Name == ProfileUserId && varSkill.VisibleRecordType__c)
            {
                return true;
            }
        }
        return false;
    }
    
    
    @AuraEnabled
    public static Boolean isGroupMember(){
    
         /*List<groupmember> groupMembers = [SELECT userorgroupid FROM groupmember 
                                            WHERE group.DeveloperName  = 'TEK_Create_New_Req_Access'
                                            AND userorgroupid =: UserInfo.getUserId()];*/
        List<groupmember> groupMembers = [SELECT userorgroupid FROM groupmember 
                                            WHERE group.DeveloperName  in('TEK_Create_New_Req_Access','Aerotek_Create_New_Req_Access') 
                                            AND userorgroupid =: UserInfo.getUserId()];
    
            boolean isGroupMember = (groupMembers.size() == 0)?false:true;
            
            return isGroupMember;
        
    }
    
    @AuraEnabled
    public static Boolean isAerotekGroupMember(){
    
         List<groupmember> groupMembers = [SELECT userorgroupid FROM groupmember 
                                            WHERE group.DeveloperName  = 'Aerotek_Create_New_Req_Access'
                                            AND userorgroupid =: UserInfo.getUserId()];
    
            boolean isAerotekGroupMember = (groupMembers.size() == 0)?false:true;
            
            return isAerotekGroupMember;
        
    }

   
}