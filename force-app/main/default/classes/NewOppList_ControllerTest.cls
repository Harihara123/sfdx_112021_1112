@IsTest
public class NewOppList_ControllerTest {

    static testMethod void Test_User(){
        
        TestData TdAcc = new TestData(1);
        
        User usr=TestDataHelper.createUser('System Administrator');
        usr.Office_Code__c='01384';
        usr.OPCO__c='GB1';
        insert usr;
        
        User_Organization__c uoc=new User_Organization__c();
        uoc.Office_Code__c='01384';
        uoc.OpCo_Code__c='GB1';
        uoc.Name='Office Name';
        insert uoc;
        
        
        Test.startTest();
        
        

        List<Account> lstNewAccounts = TdAcc.createAccounts();  
        Account accnt = TestData.newAccount(1);
        accnt.Name = 'testAccount';
        insert accnt; 
        
        Contact lstNewContact = TdAcc.createContactWithoutAccount(lstNewAccounts[0].id); 
        lstNewContact.Account_Search__c= accnt.Id;
        lstNewContact.Account = lstNewAccounts[0] ;
        lstNewContact.Account.name='test123';
        update lstNewContact;
        
        
        
        System.runAs(usr){
            User us=NewOppList_Controller.fetchUser();
            list<string> ObjName = NewOppList_Controller.getObjName(string.valueof(accnt.id));
            list<string> ObjName2 = NewOppList_Controller.getObjName(string.valueof(lstNewContact.id));
            System.assert(usr!=null);
        }
        Test.stopTest();
        
        
    }
    
    public static TestMethod void isGrpMemberTest()
    {
        User usr=TestDataHelper.createUser('System Administrator');
        usr.Office_Code__c='01384';
        usr.OPCO__c='GB1';
        Insert usr;
            
        //Create Parent Group
        Group grp = new Group();
        grp.name = 'Test Group1';
        grp.Type = 'Regular'; 
        Insert grp; 
        
        Test.startTest(); 
            
           //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = Usr.Id;
        grpMem1.GroupId = grp.Id;
        //sInsert grpMem1;    
            
            
        NewOppList_Controller Data = new NewOppList_Controller();
        NewOppList_Controller.isGroupMember();
     
        
        Test.stopTest();
    }
    
    public static TestMethod void RTAccessDetailsForProfilesTest()
    {
        
        Id userId = UserInfo.getUserId();
        OpportunityRecordTypeVisibility__c typrCustomSetting = new OpportunityRecordTypeVisibility__c();
        typrCustomSetting.Name = userId;
        typrCustomSetting.VisibleRecordType__c = true;
        insert typrCustomSetting;
        
        //NewOppList_Controller controllerObject = new NewOppList_Controller();
        NewOppList_Controller.getRTAccessDetailsForProfiles();
        
    }
    
    
   
    
}