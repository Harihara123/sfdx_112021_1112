global class NoOp  {

    @InvocableMethod(label='Contact No Op' description='Halt HRXML generation in specific context.')
    public static void stopHRXMLGeneration() {
        System.debug('Stopped process builder'); 
    }

}