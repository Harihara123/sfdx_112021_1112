/*
* This class processes the UI for notes exctration. This includes saving the "raw" note into 
* a call (in Connected ATS, you will need to rewrite saveNewNote() to store to existing G2 notes)
*, call our Python ML model w/ the note text and return a JSON w/ "structured" response to paint UI
* with extractions for recruiter to correct. Recruiter can correct/add new extractions (calling getNormalizedEntity() )
* surgically. Once all corrections are processed, recruiter will hit "Validate" (which calls updateContactWithExtraction() )
*. After we save the fields to SFDC, UI again calls logExtractionModelTrainingData() to log this entire extraction 
* response for purposes of retraining the model w/ user corrections to make it better over time.
*
* ATS Developers. Please find all the TODO! below, these are where updates are needed
*/
public with sharing class NotesParserController {
    public static Boolean DEBUG_LOGGING = true;
	//Rajeesh update env specific variables.
    //public static String EXTRACTOR_URL = 'https://ag-note-extractor-staging.herokuapp.com';
    //public static String EXTRACTOR_AUTH_USERNAME = 'allegis';
    //public static String EXTRACTOR_AUTH_PASSWORD = 'flylikeaneagle';
    public static String EXTRACTOR_URL = '';
    public static String EXTRACTOR_AUTH_USERNAME = '';
    public static String EXTRACTOR_AUTH_PASSWORD = '';
    
	public static String GOOGLE_PUBSUB_URL = 'https://pubsub.googleapis.com';

    /**
    * Simply saves the call (without process extraction). 
    * TODO!   Connected ATS needs to change this step to process this into the 'G2' note section.
    * The example below is illustrative in the scratch-org saving it as a 'Call' (which I don't think we do in ATS)
    * 
    */
    @AuraEnabled    
    public static Id saveNewNote(String contactId, String noteText) {
        Task nt = new Task();

        nt.WhoId = contactId;
        //nt.WhatId = contactId;
        nt.ActivityDate = date.today();
        nt.Description = noteText;
        //String briefDesc = noteText.length() > 50 ? noteText.substring(0, 50) + '...' : noteText;
        nt.Status = 'Completed';
        nt.Subject = 'Candidate Summary/G2 Completed by ' + UserInfo.getUserName();
        nt.Type = 'G2';
        nt.TaskSubtype = 'Task';
        //nt.ActivityType = 'G2';
        nt.OwnerId = UserInfo.getUserId();
        Database.SaveResult sr = Database.insert(nt, false);
        
        if (sr.isSuccess()) {
            return sr.getId();
        } else {
            return null;
        }
        

    }
	/* Rajeesh 4/8/2019: Seperating prod vs nonprod environments.*/
	public static Boolean isSandbox(){
		Organization org = new Organization();
        org = [select IsSandbox from Organization limit 1];
        return org.IsSandbox;
	}
	/* Rajeesh 4/8/2019 get env specific variables */
	public static void updateEnvVar(){
		Boolean isSandbox = NotesParserController.isSandbox();
		List<NotesParserEnv__mdt> envList = new List<NotesParserEnv__mdt>([select URL__c,auth_uname__c,auth_pwd__c from NotesParserEnv__mdt where isSandbox__c=:isSandbox]);
		EXTRACTOR_URL = envList[0].URL__c;
		EXTRACTOR_AUTH_USERNAME = envList[0].auth_uname__c;
		EXTRACTOR_AUTH_PASSWORD = envList[0].auth_pwd__c;
		System.debug('EXTRACTOR_URL'+EXTRACTOR_URL+':EXTRACTOR_AUTH_USERNAME'+EXTRACTOR_AUTH_USERNAME);

	}
  /* @AuraEnabled
    public static String getUserId() {      
        if (NotesParserController.DEBUG_LOGGING) System.debug('UserInfo.getUserId():' + UserInfo.getUserId());
        return UserInfo.getUserId();
    }*/
    /*
    * After user corrects the extractions, and saves the data to SFDC, UI sends down this same data 
    * to be used as "training data" to update the model w/ the recruiters corrections to make the model better.ApexPages
    * We do this by pushing this JSON to Google Pub/Sub, .. then the google ecosystem this runs on has a 4 hr. "sweep" operation
    * that graps up these messages and writes them to files. These files are then consumed by a scheduled process to retrain the model,
    * then re-deploy the model, all happening "live", so model is updated without any downtime (because Heroku, the server hosting the model)
    * will only "repoint" the running instance once a hot deploy is complete.
    */
    @AuraEnabled
    public static void logExtractionModelTrainingData(String trainingJson) {      
        if (NotesParserController.DEBUG_LOGGING) System.debug('log training data:' + trainingJson)  ;
		//Rajeesh 4/8/2019 Avoid sending training data from Sandbox
		Boolean isSandbox = NotesParserController.isSandbox();
		System.debug('N2P: isSandbox'+isSandbox);
		String endpoint='';
		if(!isSandbox){
			endpoint = GOOGLE_PUBSUB_URL + '/v1/projects/moneypenny-224413/topics/topic_training:publish?access_token=';
			
		}
		else{
			endpoint = GOOGLE_PUBSUB_URL + '/v1/projects/moneypenny-224413/topics/topic_test_training:publish?access_token=';
			
			System.debug('N2P: sending training data in sanbox test training environments');
		}
		GooglePubSubHelperNotes.makeCalloutAsync(trainingJson, endpoint);
    }
    /*
    * When recruiter validates/corrects the extractions, we use this corrected data (in JSON format) to update
    * the corresponding fields in SFDC where extractions were mentioned. I needed to fetch Contact here and pass through
    * to an "Impl" method because my Unit Test would not "see" the Contact record I created in the Test (I used SeeAllData=true).
    * I am sure there is some way to do this better, but I am not sure how to (and don't have time to learn everything about Apex development).
    */
    @AuraEnabled
    public static Contact updateContactWithExtraction(String contactId, String extractions) {
        if (NotesParserController.DEBUG_LOGGING) System.debug('contactId:' + contactId);
        if (NotesParserController.DEBUG_LOGGING) System.debug('entered updateContactWithExtraction');
        if (NotesParserController.DEBUG_LOGGING) System.debug(extractions);
        Contact contactToUpdate = [SELECT MailingCity, MailingCountry, MailingState, AccountId from Contact Where Id = :contactId];
        if (NotesParserController.DEBUG_LOGGING) System.debug('contactToUpdate:' + contactToUpdate);
        NotesParserController.updateContactWithExtractionImpl(contactToUpdate, extractions);
        return contactToUpdate;
    }
    
    public static void updateContactWithExtractionImpl(Contact contactToUpdate, String extractions) {       
        //breakapart address into city/state/country components
        if (NotesParserController.DEBUG_LOGGING) System.debug('entered updateContactWithExtractionImpl');
        if (NotesParserController.DEBUG_LOGGING) System.debug(extractions);
        String city = null;
        String state = null;
        String country = null;
        // enumerate the set of extractions, by key = ENTITY type, e.g. 
        // this extraction contains all entries but SFDC ATS currently only processes LIVING_LOCATION,  DESIRED_HOURLY_RATE, and CURRENT_JT (Hourly rate is misnomer, includes daily and salaried)
        // and then hand off to a method for each (different data/parsing for each type)
        // to parse the structure out of the JSON and update SFDC. For now, we are only handling
        // a subset of all the extractions to process to SFDC (some extractions have no fields to store data in SFDC)
        //, however this extraction is also being logged elsewhere to be used later.
        if (extractions!=null && !''.equals(extractions)) {
               Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(extractions);
               if (NotesParserController.DEBUG_LOGGING) System.debug(m);
               for (String key : m.keySet()) {
                   if ('LIVING_LOCATION'.equals(key)) {
                       NotesParserController.processGeographyExtraction(key, m.get(key), contactToUpdate);
                   } else if ('CURRENT_JT'.equals(key)) {
                       if (NotesParserController.DEBUG_LOGGING) System.debug('entered CURRENT_JT update');
                       String title = (String) ( (Map<String, Object>) m.get(key)).get('resolvedEntity');
                       if (NotesParserController.DEBUG_LOGGING) System.debug('entered CURRENT_JT title:' + title);
                       contactToUpdate.Title = title;
                       update contactToUpdate;
                   } else if ('DESIRED_HOURLY_RATE'.equals(key)) {// This is a misnomer. It is not just Hourly rates, but salary and daily as well
                       if (NotesParserController.DEBUG_LOGGING) System.debug('entered DESIRED_HOURLY_RATE update');
                       NotesParserController.processPayExtraction(key, m.get(key), contactToUpdate, false);
                   } else if ('CURRENT_HOURLY_RATE'.equals(key)) {
                       if (NotesParserController.DEBUG_LOGGING) System.debug('entered CURRENT_HOURLY_RATE update');
                       //TODO! If we don't have a desired hourly.. and contact record has not pay information, 
                       // can we assume this is the desired as well, and write it as well? 
                       NotesParserController.processPayExtraction(key, m.get(key), contactToUpdate, true);
                   }

                   if (NotesParserController.DEBUG_LOGGING) System.debug(key);
               }
        }
        
        //.Title
     
    }
    /**
    * The structure of the "resolvedEntity" node here is the exact structure of Google Places API response, e.g:
    * resolvedEntity={address_components=({long_name=New York, short_name=New York, types=(locality, political)}, {long_name=New York, short_name=NY, types=(administrative_area_level_1, political)}, {long_name=United States, short_name=US, types=(country, political)}), distance=2150.67517145181, formatted_address=New York, NY, USA, geometry={location={lat=40.7127753, lng=-74.0059728}, viewport={northeast={lat=40.9175771, lng=-73.70027209999999}, southwest={lat=40.4773991, lng=-74.25908989999999}}}, id=7eae6a016a9c6f58e2044573fb8f14227b6e1f96, name=New York, normalized_name=New York, NY, USA (2150 km.), place_id=ChIJOwg_06VPwokRYv534QaPC8g, reference=ChIJOwg_06VPwokRYv534QaPC8g, scope=GOOGLE, ...}
    *
    * TODO! I would imagine Connected ATS already has code that knows how to parse this and store in in the Contact
    * records Address fields that displays at the top of the ATS Candidate Page. I think this because there is a 
    * type-ahead feature in ATS Candidate Page for entering a candidate's address, and thus I would imagine you have
    * code that parses Google Places API response and pushes into SFDC Contact record. My code below is really
    * just illustrative in an SFDC scratch-org, and I do not know the exact logic needed in ATS's case.
    */
    private static void processGeographyExtraction(String label, Object json, Contact contactToUpdate) {
        
        String city=null;
        String state=null;
        String country=null;
 System.debug('object----'+json);       
        Map<String, Object> m = (Map<String, Object>) json;
        Map<String, Object> googlePlacesResponse = (Map<String, Object>) m.get('resolvedEntity');
        List<Object> addyComponents = (List<Object>) googlePlacesResponse.get('address_components');
        
        String backupToCity = null; // not sure logic to get city, so trying this... 
        for (Object addyComponentObj : addyComponents) {
            Map<String, Object> addyComponent = (Map<String, Object>) addyComponentObj;
            List<Object> types = (List<Object>) addyComponent.get('types');
            for (Object type : types) {
                if (((String) type) == 'administrative_area_level_1') {
                    state = (String) addyComponent.get('long_name');
                } else if (((String) type) == 'locality') {
                    city = (String) addyComponent.get('long_name');
                } else if (((String) type) == 'country') {
                    country = (String) addyComponent.get('long_name');
                }else if (((String) type) == 'administrative_area_level_2') {
                    backupToCity = (String) addyComponent.get('long_name');
                }
            }
            if (city == null) {
                city = backupToCity;
            }
        }
        if (NotesParserController.DEBUG_LOGGING) System.debug('city:' + city + ', state:' + state + ', country:' + country );
        try {                      
            if (NotesParserController.DEBUG_LOGGING) System.debug(contactToUpdate);
            if (country!=null) {
                contactToUpdate.MailingCity = city;
                contactToUpdate.MailingState = state;
                //TODO! Not sure what you are doing here.. seems you write GBR to MailingCountry even though
                // Google gives back "United Kingdom"
                // same for USA = "United States"
                String origCountry = country;
                if ('United Kingdom'.equals(country)) {
                    country = 'GBR';
                } else if ('United States'.equals(country)) {
                    country = 'USA';
                }
                //TODO! Seems pretty lame, but looks like we redundently capture the country into this field as well
                contactToUpdate.Talent_Country_Text__c = origCountry;
                //contactToUpdate.Mailing_Address_Formatted__c
                contactToUpdate.MailingCountry = country;
            }
            
            update contactToUpdate;
            
        }  catch (DmlException e) {
            if (NotesParserController.DEBUG_LOGGING) System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
        if (NotesParserController.DEBUG_LOGGING) System.debug('here update complete');
    }
    /*
    * Code below correctly parses the pay data into fields for which I know are needed by the 
    * desired pay rate/current pay rate/current salary fields present on the Candidate G2 form in ATS.
    * Code is illustrative below, but will need to be implemented by ATS.
    */
    private static void processPayExtraction(String label, Object json, Contact contactToUpdate, Boolean onlyUpdateIfEmpty) {
        Map<String, Object> m = (Map<String, Object>) json;
        Map<String, Object> payResponse = (Map<String, Object>) m.get('resolvedEntity');
        if (NotesParserController.DEBUG_LOGGING) System.debug('before account' + contactToUpdate);
        String accountId = contactToUpdate.AccountId;
        if (NotesParserController.DEBUG_LOGGING) System.debug('accountId' + accountId);
        Account account = ([SELECT Id, Desired_Salary__c, Desired_Salary_Max__c, Desired_Rate_Max__c, Desired_Rate__c, Desired_Rate_Frequency__c FROM Account where Id = :accountId LIMIT 1]).get(0);
        Double amount  = (Double) payResponse.get('amount');
        //frequency has 3 values: "daily", "hourly", and "yearly"
        String frequency = (String) payResponse.get('frequency');
        if (NotesParserController.DEBUG_LOGGING) System.debug('amount:' + amount + ', frequency:' + frequency);
        Boolean performUpdate = !onlyUpdateIfEmpty;
        if (!performUpdate) {
            if (NotesParserController.DEBUG_LOGGING) System.debug('inside performUpdate');
            // see if existing value is blank, if so, then perform it
             if ('yearly'.equals(frequency)) {
                 if (NotesParserController.DEBUG_LOGGING) System.debug('inside yearly' + account.Desired_Salary__c + '|');
                 if (account.Desired_Salary__c==null) {
                     performUpdate = true;
                 }
             } else {
                 if (NotesParserController.DEBUG_LOGGING) System.debug('inside daily' + account.Desired_Rate__c + '|');
                if (account.Desired_Rate__c==null) {
                     performUpdate = true;
                 }
             }
        }
        if (NotesParserController.DEBUG_LOGGING) System.debug('account:' + account);
        if (performUpdate) {
             if ('yearly'.equals(frequency)) {
                //TODO! Update the "Salary" G2 fields 
                //Desired Min Salary/Desired Max Salary            
                account.Desired_Salary__c = amount;      
                account.Desired_Salary_Max__c  = amount;
                if (NotesParserController.DEBUG_LOGGING) System.debug('yearly:' +  account.Desired_Salary__c );
            } else {
                //TODO! Update the Current or Desired "Rate" G2 fields 
                //Desired Min Rate/Desired Max Rate
                account.Desired_Rate__c = amount;
                account.Desired_Rate_Max__c = amount;
                account.Desired_Rate_Frequency__c =frequency;
                if (NotesParserController.DEBUG_LOGGING) System.debug('daily:' +  account.Desired_Rate__c + '|' +  account.Desired_Rate_Frequency__c);
            }
            update account;
        }
    }
    /**
    * This method is called when user add's the LIVING_LOCATION or RELOC_LOCATION tagging manually on the UI
    *, e.g. it is not extracted by the entity recognizer. We need to fetch the normalized geo (from pythom, which in turn calls Google API)
    * so UI can paint a dropdown for user to select the correct normalized form of the geo, .e.g. does the highlighted phrase
    * "Columbia" mean Columbia, MD or Columbia, SC
    */
    @AuraEnabled
    public static String getNormalizedEntity(String contactId,  String noteText, String city, String state, String country,String geoLoc) {
        return getNotesExtractionImpl(contactId,  noteText, city, state, country,geoLoc, true);
    }
    /**
    * This is the method that is called to process the raw Note the user entered, do a REST API call to our 
    * Python model to try to identify the extracted entities, and then send back to UI in a JSON response
    * to paint/highlight the extractions in the notes for recruiter correction.
    */
    @AuraEnabled
    public static String getNotesExtraction(String contactId,  String noteText, String city, String state, String country, String geoLoc) {
        return getNotesExtractionImpl(contactId,  noteText, city, state, country,geoLoc, false);
    }
    /*
    * This method handles the full extractions from notes, as well as the surgical fetching of the "normalization"
    * for a user-entered/manually highlighted entity.. I did this because much of the code was the same between these two
    * separate operations (although, logically, they are pretty disimiliar operations, one actually asks Python model to 
    * identify the extractions [through a Neural Network] and then normalize, while the other just asks for the normalization step)
    * We have overloaded the "noteText" parameter to mean two different things.. it is the "full" text (unextracted), and then
    * just the highlighted text (in the surgical normalization use-case)
    */
    private static String getNotesExtractionImpl(String contactId,  String noteText, String city, String state, String country,String geoLoc, Boolean isNormalizedEntityMethodInvocation) {
        NotesParserController.updateEnvVar(); 

		HttpRequest req = new HttpRequest();
        System.debug('------geoLoc'+geoLoc);
        // we need the city/state/country of current candidate to allow google geo code to do a geographic proximity calculation
        // to determine the best geo matches to ambiguous cities, e.g. is Columbia in Maryland or South Carolina?
        // if UI passes in , use it, else look up from SFDC        
        noteText = EncodingUtil.urlEncode(noteText, 'UTF-8');
		String path = isNormalizedEntityMethodInvocation ? 'normalizeentity' : 'notesextraction';
		String url='';
		if (contactId!=null) {
				Contact contact = ([SELECT Id, AccountId, Name, Phone, MailingCity, MailingState, MailingCountry FROM Contact where Id = :contactId LIMIT 1]).get(0);
				String accountId = contact.AccountId;
				city = contact.MailingCity;
				state = contact.MailingState;
				country = contact.MailingCountry;
				//TODO! Uncomment below (and fix if needed) need to fetch the country from the current user, or some other way, e.g. 
				// use the Talent_Ownership__c to drive US vs. UK for now. This is critical for the NER model to work properly, because, 
				// for instance, recruiters say "He wants 100", in US this is hourly rate, in UK this is daily. Or for LIVING_LOCATION, if recruiter says "York", US
				// vs. UK will give different values based on Country.
				/*if (country == null || ''.equals(country)) {
					Account account = ([SELECT Id, Talent_Ownership__c FROM Account where Id = :accountId LIMIT 1]).get(0);
					talentOwnership = account.Talent_Ownership__c;
					if ('AG_EMEA'.equals(talentOwnership)) {
						country = 'United Kingdom'
					} else {
						country = 'USA'
					}
				}*/
            
			}

		// call notes extractor service
			
		if (city==null) city= '';
		city = EncodingUtil.urlEncode(city, 'UTF-8');
		if (state==null) state= '';
		state = EncodingUtil.urlEncode(state, 'UTF-8');
		if (country==null) country= '';
		country = EncodingUtil.urlEncode(country, 'UTF-8');
        
		if(String.isNotBlank(geoLoc)){
			String[] geoLocArr = geoLoc.split(',');
			System.debug('-------geolocarr----'+geoLocArr);
			url = EXTRACTOR_URL + '/' + path + '/?q=' + noteText + '&biasLat=' + geoLocArr[0] + '&biasLng=' + geoLocArr[1]+ '&city=' + city + '&state=' + state + '&country='+ country;
		}
		else{
			
			
			url = EXTRACTOR_URL + '/' + path + '/?q=' + noteText + '&city=' + city + '&state=' + state + '&country='+ country;
		}
System.debug('--notes parser---'+url);        
        req.setEndpoint(url);
        req.setMethod('GET');
        Blob headerValue = Blob.valueOf(NotesParserController.EXTRACTOR_AUTH_USERNAME + ':' + NotesParserController.EXTRACTOR_AUTH_PASSWORD);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setTimeout(10000);//check with Carl before changing timeout -riju's recommendation - Not good checked with Carl.
        
        HttpResponse res = new HTTP().send(req);
        
        if(res.getStatusCode() == 200) {          
			System.debug('notes parser resp'+res.getBody());  
            return res.getBody();
        }  else {
            if (NotesParserController.DEBUG_LOGGING) System.debug('here7:' + res.getStatusCode());
        }
        return '';        
    }
    /**
    * AWARE!! This code is just for test purposes. Used on unit test.
    */
   /* @AuraEnabled
    public static void demoModeDataReset(String contactId) {
       NotesParserController.testDataReset(contactId, false);
        //}
        
    }
	*/
    /**
    * AWARE!! This code is just for test purposes. Used on unit test.
    */
   /* public static Contact testDataReset(String contactId, Boolean insertMe) {
        Contact contactToUpdate;
        //breakapart address into city/state/country components
        if (NotesParserController.DEBUG_LOGGING) System.debug('here');        
       
        //List<String> contactsForDemo = new list<String> {'0035B00000LLhwPQAT'};
        //for (String contactId : contactsForDemo) {
            try {
                if (insertMe) {
                    contactToUpdate = new Contact(LastName='Chuck Norris');
                } else {
                    contactToUpdate = [SELECT MailingCity, MailingCountry, MailingState, Title from Contact Where Id = :contactId];
                }
                
                //if ('0035B00000LLhwPQAT'.equals(contactId)) {
                    contactToUpdate.MailingCity = 'London';
                    contactToUpdate.MailingState = 'England';
                    contactToUpdate.MailingCountry = 'United Kingdom';
                    contactToUpdate.Title = '';
                //}            
                if (insertMe) {
                    insert contactToUpdate;
                } else {
                    update contactToUpdate;
                }
                
            }  catch (DmlException e) {
                if (NotesParserController.DEBUG_LOGGING) System.debug('An unexpected error has occurred: ' + e.getMessage());
            }
        return contactToUpdate;
    }*/
}