global class NotesParser_GooglePubSubCalloutMock implements System.HttpCalloutMock{

         global NotesParser_GooglePubSubCalloutMock() {

        }
        global System.HTTPResponse respond(HTTPRequest req) {
            
                    HttpResponse res = new HttpResponse();
                    res.setHeader('Content-Type', 'application/json');
					String EXTRACTION= '{"LIVING_LOCATION":{"resolvedEntity":{"address_components":[{"long_name":"New York","short_name":"New York","types":["locality","political"]},{"long_name":"New York","short_name":"NY","types":["administrative_area_level_1","political"]},{"long_name":"United States","short_name":"US","types":["country","political"]}],"formatted_address":"New York, NY, USA","geometry":{"location":{"lat":40.7127753,"lng":-74.0059728},"viewport":{"northeast":{"lat":40.9175771,"lng":-73.70027209999999},"southwest":{"lat":40.4773991,"lng":-74.25908989999999}}},"id":"7eae6a016a9c6f58e2044573fb8f14227b6e1f96","name":"New York","place_id":"ChIJOwg_06VPwokRYv534QaPC8g","reference":"ChIJOwg_06VPwokRYv534QaPC8g","scope":"GOOGLE","types":["locality","political"],"utc_offset":-300,"vicinity":"New York","normalized_name":"New York, NY, USA (2150 km.)","distance":2150.67517145181},"label":"LIVING_LOCATION","startIdx":14,"endIdx":22,"unnormalizedResolvedEntity":"NewYork"},"RELOC_LOCATION":{"resolvedEntity":{"address_components":[{"long_name":"York","short_name":"York","types":["locality","political"]},{"long_name":"York","short_name":"York","types":["postal_town"]},{"long_name":"York","short_name":"York","types":["administrative_area_level_2","political"]},{"long_name":"England","short_name":"England","types":["administrative_area_level_1","political"]},{"long_name":"United Kingdom","short_name":"GB","types":["country","political"]}],"formatted_address":"York, UK","geometry":{"location":{"lat":53.95996510000001,"lng":-1.0872979},"viewport":{"northeast":{"lat":53.9912585,"lng":-1.0139137},"southwest":{"lat":53.9259345,"lng":-1.1472695}}},"id":"99e839b8ad6fc609c55d1ae325dd55061af3d8fc","name":"York","place_id":"ChIJ8WWY4UDDeEgR0eRUiomrdEc","reference":"ChIJ8WWY4UDDeEgR0eRUiomrdEc","scope":"GOOGLE","types":["locality","political"],"utc_offset":0,"vicinity":"York","normalized_name":"York, UK (108 km.)","distance":108.20864411170328},"label":"RELOC_LOCATION","startIdx":44,"endIdx":50,"unnormalizedResolvedEntity":"London"},"CURRENT_JT":{"resolvedEntity":"software developer","label":"CURRENT_JT","startIdx":66,"endIdx":84},"CURRENT_HOURLY_RATE":{"resolvedEntity":{"amount":450,"frequency":"yearly","normalized_name":"450.0 yearly"},"label":"CURRENT_HOURLY_RATE","startIdx":89,"endIdx":94,"unnormalizedResolvedEntity":"450"},"DESIRED_HOURLY_RATE":{"resolvedEntity":{"amount":450,"frequency":"daily","normalized_name":"450.0 daily"},"label":"DESIRED_HOURLY_RATE","startIdx":103,"endIdx":116,"unnormalizedResolvedEntity":"450 - 500 a day"}}';
                    //Map<String,Object> respMap = new Map<String,Object>();
                    //respMap.put('responseMsg','SUCCESS');
                    //respMap.put('responseDetail','saved successfully.');
                    //respMap.put('applicationIdList',new List<String>());
                    //String jsonResponse=JSON.serialize(respMap);
                    System.debug('jsonResponse '+EXTRACTION);
                    res.setBody(EXTRACTION);
                    res.setStatusCode(200);
                    return res;

        }
    }