@isTest 
private class ObjectMetaDataTest {

	@isTest
	private static void syncObjectMetaDataTest() {
		
		Connected_Data_Export_Switch__c dataExport = new Connected_Data_Export_Switch__c();
        dataExport.Insert_Allowed_All_Users__c =false;
        dataExport.Name='DataExport';
        dataExport.Google_Pub_Sub_Call_Flag__c =false;
        dataExport.Data_Extraction_Insert_Flag__c = false;
        dataExport.Data_Extraction_Query_Flag__c = false;
        dataExport.Max_Allowed_Queued_Job__c  = 3;
             
        dataExport.PubSub_URL__c = 'https://pubsub.googleapis.com/v1/projects/connected-ingest-dev/topics/ingest10x:publish?access_token=';
		dataExport.SyncObject_TopicName__c = 'https://pubsub.googleapis.com/v1/projects/connected-ingest-dev/topics/ingest10x-sObjectSync:publish?access_token=';
		insert dataExport;

		Test.startTest();
		ObjectMetatDataScheduler sh1 = new ObjectMetatDataScheduler();
		String sch = '0 0 23 * * ?';
		system.schedule('Sync Object Test', sch, sh1);
		Test.stopTest(); 
	}
}