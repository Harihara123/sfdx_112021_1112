public with sharing class ObjectMetaDataUtility  {

	public static string getObjectMetaData(List<String> objectList) {		
		String commaSepratedFields = ''; 		
		String output ='';
		for (string sObjectName : objectList) {
			commaSepratedFields = getFieldNamesByObject(sObjectName); 
			if (String.isEmpty(output)) {
				output = '[{"sObject":"' + sObjectName + '","fields":[' + commaSepratedFields + ']}';
			} else {
			 output+= ',{"sObject":"' + sObjectName + '","fields":[' + commaSepratedFields + ']}'; 			
			}
		}
		output+=']';
		system.debug('JSON:'+ output);
		return output;
	}

	public static string getObjectMetaData(String sObjectName) {
		List<String> lcaseIF = new List<String>();
		String commaSepratedFields = getFieldNamesByObject(sObjectName); 		
		String output = '{"sObject":"' + sObjectName + '","fields":[' + commaSepratedFields + ']}';
		system.debug('JSON:'+ output);
		return output;
	}

	private static string getFieldNamesByObject(String sObjectName) {
		List<String> lcaseIF = new List<String>();
		string commaSepratedFields = '';
		for(Schema.SObjectField fld: Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().values()){ 
					Schema.DescribeFieldResult F = fld.getDescribe();
					String fieldType = F.getType().name();
					String fieldName = F.getName(); 
					Integer fieldLength = F.getLength();
					String field = '{"name":"' + fieldName + '","type":"' + fieldType + '","length":"' + fieldLength +'"}';
					if (!lcaseIF.contains(fieldName.toLowerCase())) { 
						if(commaSepratedFields == null || commaSepratedFields == '') {
								commaSepratedFields = field;
						} else { 
							commaSepratedFields = commaSepratedFields + ', ' + field; 
						} 
					} 
		} 
		return commaSepratedFields;
	}


}