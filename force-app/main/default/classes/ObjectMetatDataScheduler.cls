global class ObjectMetatDataScheduler implements Schedulable {
		
    private static String ingest10xTopicName = 'ingest10x';
	private static String syncObjectTopicName = Connected_Data_Export_Switch__c.getInstance('DataExport').SyncObject_TopicName__c;
	private static String PubSub_URL = Connected_Data_Export_Switch__c.getInstance('DataExport').PubSub_URL__c.replace(ingest10xTopicName, syncObjectTopicName);
	private static string objects = Label.ATS_SyncObject_ObjectsList;
	
	global void execute(SchedulableContext sc) {	
		// Object List need to be replaced by a Custom Label	or custom settings?
		//string objects = 'Account,Contact,Opportunity,Reqs__c,User_Search_Feedback__c,Order,Event,Task,Talent_Document__c,Talent_Experience__c,Talent_Work_History__c,User,User_Activity__c,Tag_Definition__c,Contact_Tag__c,User_Organization__c,Pipeline__c,Job_Posting__c,Job_Title__c'; 
		List<String> objectList = objects.split(',');
		string payload;
	   	for (string sObjectName : objectList) {
			payload = ObjectMetaDataUtility.getObjectMetaData(sObjectName);
			if (String.isNotBlank(payload) && !Test.isRunningTest()) {				
				GooglePubSubHelper.makeCalloutAsync(payload, PubSub_URL);
			}
		}
		
	}
}