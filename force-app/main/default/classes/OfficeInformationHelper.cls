global class OfficeInformationHelper{
    static User_Organization__c org,region;
    static Map<string,string> opcoMapping = new Map<string,string>{'ONS' => 'Aerotek','TEK' => 'TEKsystems'};
    static Map<string,string> reverseOpcoMapping = new Map<string,string>{'Aerotek' => 'ONS', 'TEKsystems' =>  'TEK'};
         
    webservice static User_Organization__c getOfficeInformation() 
    {
           try 
           {
               // query the office code based on the logged in user
               User user = [select OPCO__c,Office_Code__c 
                            from User 
                            where id = :userInfo.getUserId()];
               
              getOffice(user);
               
           }catch (QueryException e)
           {}
           return org;                      
    }
    // Method invoked when the user information is already queried.
    webservice static User_Organization__c getOffice(User user)
    {
          //query the approprite office information
           if(user.Office_Code__c != Null && user.OPCO__c != Null) 
           {
              
              try {
                   string orgCompositeCode = user.OPCO__c + '-'+ user.Office_Code__c;
                   string orgCode = user.OPCO__c + '-'+ user.Office_Code__c;
                   
                   if(opcoMapping.get(user.OPCO__c) != Null)
                       orgCompositeCode = opcoMapping.get(user.OPCO__c) + '-'+ user.Office_Code__c;
                   
                   if(reverseOpcoMapping.get(user.OPCO__c) != Null)
                       orgCode = reverseOpcoMapping.get(user.OPCO__c) + '-'+ user.Office_Code__c;
                  
                   org = [select Name,Office_Name__c 
                         from  User_Organization__c 
                         where (Office_Composition_Id__c = :orgCompositeCode or Office_Composition_Id__c = :orgCode)
                         and Active__c = true
                         and RecordType.Name = :Label.User_Organization_Office_Record_Type
                         limit 1];
                } catch(QueryException e) {
                }                                           
           } 
           return org; 
    }
    //Method to retrieve the region information based on logged in user region
    webservice static User_Organization__c getRegion(string regionCode)
    {
          //query the appropriate region information
          try {
               region = [select Name 
                         from  User_Organization__c 
                         where Region_Composite_Id__c = :regionCode
                         and Active__c = true
                         and RecordType.Name = :Label.User_Organization_Region_Record_Type
                         limit 1];
            } catch(QueryException e) {
            }                                           
           return region; 
    }
    
}