@RestResource(urlMapping='/onboardingtracker/v1')
Global class OnBoardingTracker  {
    @HttpGet
    global static onBoardingRequestWrapper getOnBoarding(){
        onBoardingRequestWrapper onBoardingList = new onBoardingRequestWrapper();
        return onBoardingList;
    }
    @HttpPost
    global static responseWrapper postOnBoarding(List<onBoardingRequestWrapper> onBoardingRequestWrapper){
        RestRequest  req     = RestContext.request;
        RestResponse res = RestContext.response;
        List<OnboardingStatus__c> obsList = new List<OnboardingStatus__c>();  
        List<onBoardingRequestWrapper> havingNullValues = new List<onBoardingRequestWrapper>();
        for(onBoardingRequestWrapper d : onBoardingRequestWrapper){
            OnboardingStatus__c obs = new OnboardingStatus__c();
            if((!String.isBlank(d.Onboarding_Status)) && (!String.isBlank(d.ACT_Id)) &&(!String.isBlank (d.ESF_Id))
               &&(!String.isBlank(d.Source_System)) &&(!String.isBlank(d.RWS_Candidate_ID))){  
                   obs.Screening_Status__c = d.Onboarding_Status;                   
                   obs.Account_Name__c = d.Account_Name;
                   obs.ACT_Id__c = d.ACT_Id;
                   obs.Job_Title__c = d.Job_Title ;
                   obs.Peoplesoft_Id__c = d.Peoplesoft_Id;
                   obs.Screeing_LastUpdated__c = d.Event_DateTime;
                   obs.ESF_Id__c = d.ESF_Id;
                   obs.SourceSystemId__c = d.Source_System;
                   obs.RWS_Candidate_Id__c = d.RWS_Candidate_ID;
                   obsList.add(obs);
               }else{
                   havingNullValues.add(d);
               }
            //obs.Active_Status__c = d.Active_Status;
            //obs.ClearToStart_Status__c = d.ClearToStart_Status;
            //obs.Onboarding_Document_LastUpdated__c = d.Onboarding_Document_LastUpdated;
            //obs.Id = d.Id;
            // obs.Active_LastUpdated__c = d.Active_LastUpdated;
            //obs.ClearToStart_LasteUpdated__c = d.ClearToStart_LasteUpdated;
            //obs.MDM_Id__c = d.MDM_Id;obs.ESF_Id__c = d.ESF_Id;
        }
        if(obsList.size() > 0){
            insert obsList;
        }
        responseWrapper rw= new responseWrapper();
        rw.havingNullVaues= havingNullValues;
        rw.succesRecords = obsList;
        rw.numberofrecordsInserted = obsList.size();
        rw.numberofrecordsFailed = havingNullValues.size();
        return rw;
        
    }
    
    global class responseWrapper{
        public List<onBoardingRequestWrapper> havingNullVaues {get;set;}
        public List<OnboardingStatus__c> succesRecords {get;set;}
        public integer numberofrecordsInserted {get;set;}
        public integer numberofrecordsFailed {get;set;}
    }
    global class onBoardingRequestWrapper{ 
        public string Onboarding_Status {get;set;}
        public string Id {get;set;}
        public string Account_Name {get;set;}
        public string ACT_Id {get;set;}
        public string ESF_Id {get;set;}
        public string Job_Title {get;set;}
        public string Peoplesoft_Id {get;set;}
        public datetime Event_DateTime {get;set;}
        public string Source_System {get;set;}
        public string RWS_Candidate_ID {get;set;}
        // public string   Onboarding_Document_LastUpdated  {get;set;}
        // public datetime Active_LastUpdated {get;set;}
        // public datetime ClearToStart_LasteUpdated {get;set;}
        // public string    ClearToStart_Status  {get;set;}    
        //public boolean    Active_Status    {get;set;}
        // public string MDM_Id {get;set;}
        
        
    }
}