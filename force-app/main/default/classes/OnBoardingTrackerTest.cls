@istest
public class OnBoardingTrackerTest {
    static testMethod void test1() {    
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/onboardingtracker/v1';
        request.httpMethod = 'GET';
        RestContext.request = request;
        OnBoardingTracker.onBoardingRequestWrapper rw = OnBoardingTracker.getOnBoarding();        
    }
    static testMethod void test2() {
        List<OnBoardingTracker.onBoardingRequestWrapper> dList = new List<OnBoardingTracker.onBoardingRequestWrapper>();
        OnBoardingTracker.onBoardingRequestWrapper d = new OnBoardingTracker.onBoardingRequestWrapper();
        d.ACT_Id= '68921';
        d.Onboarding_Status ='In Progress';         
        d.Account_Name ='Power Systems Mfg.,Llc';        
        d.Job_Title ='Utility Worker';
        d.Peoplesoft_Id= '42088435';
        d.Event_DateTime= system.now();
        d.ESF_Id = '4480807';
        d.Source_System = 'ESCREEN';
        d.RWS_Candidate_ID = '6575755';
         dList.add(d);
        OnBoardingTracker.onBoardingRequestWrapper d1 = new OnBoardingTracker.onBoardingRequestWrapper();
        d1.ACT_Id= '68921';
        d1.Onboarding_Status ='';         
        d1.Account_Name ='Power Systems Mfg.,Llc';        
        d1.Job_Title ='Utility Worker';
        d1.Peoplesoft_Id= '42088435';
        d1.Event_DateTime= system.now();
        d1.ESF_Id = '4480807';
        d1.Source_System = 'ESCREEN';
        d1.RWS_Candidate_ID = '6575755';
        dList.add(d1);
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/onboardingtracker/v1';
        request.httpMethod = 'Post';
        RestContext.request = request;
        OnBoardingTracker.responseWrapper rw = OnBoardingTracker.postOnBoarding(dList);    
        system.assertEquals(1, rw.numberofrecordsFailed);
        system.assertEquals(1, rw.numberofrecordsInserted);
        system.assertEquals('68921', rw.succesRecords[0].ACT_Id__c);
    }
}