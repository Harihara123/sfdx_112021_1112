public class OnetimeCommentOrderUpdateBatch implements Database.Batchable <sObject> {

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            select id 
            from Order 
            where (Opportunity.isclosed = False and Opportunity.recordtype.name ='REQ') OR (Opportunity.isclosed = True and Opportunity.LastModifiedDate = LAST_N_DAYS:30 and Opportunity.recordtype.name ='REQ')
        ]);
    }
    
    public void execute(Database.BatchableContext BC, List<Order> scope) {
        map<id,id> OrderMap = new map<id,id>();
        list<id> OrderString = new list<id>();
        for(AggregateResult a : [select MAX(id), WhatId, Max(LastModifiedDate) 
                                 from Event  
                                 where WhatId in:scope group by WhatId]){
            //system.debug('---Event---'+a);
            OrderMap.put((id)a.get('WhatId'),(id)a.get('Id'));
            OrderString.add((id)a.get('expr0'));
        }
        system.debug('--OrderMap--'+OrderMap.keyset()+'---OrderMap size---'+OrderMap.size()+'--OrderString size--'+OrderString.size());
        Map<Id,string> EventDescription = new Map<Id,string>();
        list<Event> EVT = [Select id, WhatId, Description 
                           from Event 
                           where Id in:OrderString];
        for(Event t: EVT){
            //system.debug('--t---'+t);
            EventDescription.put(t.WhatId, t.Description);
        }
        system.debug(EventDescription);
        List<Order> OrderUpdate = new List<Order>();
        List<Order> OD = [SELECT Comments__c,LastModifiedDate,Source_System_LastModifiedDate__c, LastModifiedById , Source_System_LastModifiedBy__c  
                          FROM Order 
                          where id in: OrderMap.keyset()];//id = '8011q000000W0xaAAC'
        try{
            for(Order O : OD){
            system.debug('in'+o);
                if(EventDescription.containsKey(O.id) && EventDescription.get(O.id) !='' && EventDescription.get(O.id) !=null){ 
                    if(EventDescription.get(O.id).startsWith('{"')){
                        string des = string.valueof(EventDescription.get(O.id));
                        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(des);
                        //O.Comments__c = 'Client Comments:'+m.get('ClientComments')+'\r\n'+'Candidate Comments:'+m.get('CandidateComments');
						if(m.get('ClientComments') !=null){
							O.Comments__c = 'Client Comments:'+m.get('ClientComments')+'\r\n';
						}else{
							O.Comments__c ='Client Comments:'+'\r\n';
						}
						if(m.get('CandidateComments') !=null){
							O.Comments__c = O.Comments__c +'Candidate Comments:'+m.get('CandidateComments');
						}else{
							O.Comments__c = O.Comments__c +'Candidate Comments:';
						}
                    }else{
                        O.Comments__c = EventDescription.get(O.id);
                    }
                    O.Source_System_LastModifiedDate__c = O.LastModifiedDate;
					O.Source_System_LastModifiedBy__c = O.LastModifiedById;
                    OrderUpdate.add(O);
                }
            }
            system.debug('--OrderUpdate size--'+OrderUpdate.size()+'---OrderUpdate--'+OrderUpdate);
            if (!OrderUpdate.isEmpty())
            Database.Update(OrderUpdate,false);
        }catch(Exception ex){
            system.debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());
        }      
    }
    
    public void finish(Database.BatchableContext BC) {        
    }
}