public with sharing class OpportunityContactRoleTriggerHandler  {

private static boolean hasBeenProccessed = false;

   public void OnAfterInsert(List<OpportunityContactRole> newRecords) {
       callDataExport(newRecords);
       updatePotentialAccounts(newRecords);
    }
     
     
    public void OnAfterUpdate (List<OpportunityContactRole> newRecords) {        
       callDataExport(newRecords);
    }

	public void OnAfterDelete (Map<Id, OpportunityContactRole>oldMap) { 
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');       
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'OpportunityContactRoleTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' OpportunityContactRole records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'OpportunityContactRole');
            hasBeenProccessed = true; 
        }
    }

    private static void callDataExport(List<OpportunityContactRole> newRecords) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'OpportunityContactRoleTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' OpportunityContactRole records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'OpportunityContactRole');
            hasBeenProccessed = true; 
        }

    } 
    
    private static void updatePotentialAccounts(List<OpportunityContactRole> newRecords) {
        newRecords = [Select Id, OpportunityId, Opportunity.Potential_Accounts__c, Opportunity.RecordType.Name, Contact.RecordType.Name, Contact.Account.Name from OpportunityContactRole where Id IN: newRecords];
        Map<Id, Opportunity> opportunitiesToUpdate = new Map<Id, Opportunity> ();
        Opportunity opp;
        for(OpportunityContactRole contactRole: newRecords) {
            if(contactRole.Contact.RecordType.Name == 'Client' && contactRole.Opportunity.RecordType.Name == 'Talent First Opportunity') {
                if(opportunitiesToUpdate.containsKey(contactRole.OpportunityId)) {
                    opp = opportunitiesToUpdate.get(contactRole.OpportunityId);
                } else {
                    opp = new Opportunity(
                        Id=contactRole.OpportunityId,
                        Potential_Accounts__c=contactRole.Opportunity.Potential_Accounts__c                    
                    );
                }
                if(opp.Potential_Accounts__c != null) {
                    if(!opp.Potential_Accounts__c.contains(contactRole.Contact.Account.Name)) {
                        opp.Potential_Accounts__c = opp.Potential_Accounts__c+'; '+contactRole.Contact.Account.Name;
                    }
                } else {
                    opp.Potential_Accounts__c = contactRole.Contact.Account.Name;
                }
                
                opportunitiesToUpdate.put(opp.Id, opp);
            }
		}
        
        if(!opportunitiesToUpdate.values().isEmpty()) update opportunitiesToUpdate.values();
    }
}