@isTest
public class OpportunityContactRoleTriggerHandlerTest {
	
    static testMethod void updatePotentialAccounts_postiveScenario(){
       Account accnt = TestData.newAccount(1);
			accnt.Name = 'testAccount';
			accnt.Talent_Committed_Flag__c = true;
			accnt.Source_System_Id__c = null;
			insert accnt; 
        Contact cont = TestData.newContact(accnt.Id,1,'Client');
        insert cont;
        Opportunity opp= new Opportunity(
            Name = 'TESTOPP',
            recordTypeId =  Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Talent First Opportunity').getRecordTypeId(),
            CloseDate = system.today(),
            StageName = 'test',
            accountId= accnt.Id,
            Req_Job_Description__c = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890124',
            Req_Job_Title__c = 'Sr Java Developer   ',
            Location__c = 'Hanover, MD',
            CreatedDate=system.today()
        );
        insert opp;
        List<OpportunityContactRole> lst = new List<OpportunityContactRole>();
        OpportunityContactRole contactRole = new OpportunityContactRole(ContactId = cont.Id,OpportunityId =opp.Id);
        OpportunityContactRole contactRole1 = new OpportunityContactRole(ContactId = cont.Id,OpportunityId =opp.Id);
        lst.add(contactRole);
        lst.add(contactRole1);
        insert lst;
        System.assertEquals(contactRole.Contact.Account.Name,opp.Potential_Accounts__c );
       // OpportunityContactRoleTriggerHandler.updatePotentialAccounts(lst);
    }
    static testMethod void updatePotentialAccounts_negativeScenario(){
       Account accnt = TestData.newAccount(1);
			accnt.Name = 'testAccount';
			accnt.Talent_Committed_Flag__c = true;
			accnt.Source_System_Id__c = null;
			insert accnt; 
        Contact cont = TestData.newContact(accnt.Id,1,'Client');
        insert cont;
        Opportunity opp= new Opportunity(
            Name = 'TESTOPP',
            recordTypeId =  Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales Opportunity').getRecordTypeId(),
            CloseDate = system.today(),
            StageName = 'test',
            accountId= accnt.Id,
            Req_Job_Description__c = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890124',
            Req_Job_Title__c = 'Sr Java Developer   ',
            Location__c = 'Hanover, MD',
            CreatedDate=system.today()
        );
        insert opp;
        List<OpportunityContactRole> lst = new List<OpportunityContactRole>();
        OpportunityContactRole contactRole = new OpportunityContactRole(ContactId = cont.Id,OpportunityId =opp.Id);
        OpportunityContactRole contactRole1 = new OpportunityContactRole(ContactId = cont.Id,OpportunityId =opp.Id);
        lst.add(contactRole);
        lst.add(contactRole1);
        insert lst;
        System.assertEquals(null,opp.Potential_Accounts__c );
       // OpportunityContactRoleTriggerHandler.updatePotentialAccounts(lst);
    }
}