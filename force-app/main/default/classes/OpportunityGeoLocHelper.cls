public class OpportunityGeoLocHelper {
    Set<Id> opportunityIds = new Set<Id>();
    public Map<Id, Opportunity> opportunityMap;
    public List<Opportunity> opptyList {get;set;}
   
    public Map<String,Opportunity> geoLocationOppMap {get;set;}
    public Map<String,Opportunity> finalGeoLocationOppMap {get;set;}
    public static List<Id> opptyIdList;
    public OpportunityGeoLocHelper(Set<Id> reqIds){
        opportunityIds = reqIds;
        opptyIdList = new List<Id>();
        opptyList = new List<Opportunity>();
    
        finalGeoLocationOppMap = new Map<String,Opportunity>();
        init(opportunityIds);
        
    }
    private void init(Set<Id> oppIds){
       
        opportunityMap = new Map<Id, Opportunity>([select Name, EnterpriseReqSkills__c,StageName,(select id,Req_HRXML_0__c,Req_Hrxml_Opportunity__c ,Req_HRXML_Timestamp_c__c,Req_HRXML_Field_Updated__c,Req_HRXML_Status_Flag__c from Hrxmls__r),(select Skill__c, Competency__c,Level__c, Years_of_Experience__c, Skill_Usage_Description__c from skills__r)
                                                   ,Probability, Recordtype.Name,Account.Name, Account.Id,Account.Siebel_ID__c, Closedate,Start_Date__c,Account.BillingLatitude, Req_Skill_Details__c,
                                                   Req_Product__c,Req_Job_Title__c,Req_Job_Description__c,Req_Onet_SOC_Code__c,Req_Qualification__c,Account.BillingLongitude,Account.Industry,Account.NAICS_Code__c,
                                                   Req_Bill_Rate_Min__c,Req_Bill_Rate_Max__c,Req_Pay_Rate_Min__c,Req_Pay_Rate_Max__c,Req_Salary_Min__c,Account_Country__c,Req_RVT_Experience_Level_Confidence__c,
                                                   Req_Salary_Max__c, Req_Bonus__c, Req_Other_Compensations__c,Req_Total_Comp_Formula__c,Req_Standard_Burden__c,Account_Zip__c,
                                                   Req_Max_Spread__c,Req_Min_Spread__c,Amount, Req_Open_Positions__c, Req_Total_Positions__c, Req_Worksite_Street__c, Account.Account_Latitude__c,
                                                   Req_Worksite_City__c, Req_Worksite_State__c,Req_Worksite_Country__c,Req_Worksite_Postal_Code__c,Account.Account_Longitude__c,
                                                   Req_Duration__c,Req_Duration_Unit__c,Req_OFCCP_Required__c,Req_Export_Controls_Questionnaire_Reqd__c,Account.Account_Zip__c,
                                                   Req_Drug_Test_Required__c, Req_Background_Check_Required__c,Req_Compliance__c, Req_Can_Use_Approved_Sub_Vendor__c,
                                                   Req_Loss_Wash_Reason__c,Req_Red_Zone__c, Req_Government__c, Req_Exclusive__c, Source_System_Id__c,Account.Account_Country__c,
                                                   Opportunity_Num__c, Req_Travel__c, Organization_Office__r.Name,Req_Payment_terms__c,Req_Terms_of_engagement__c, 
                                                   Req_Flat_Fee__c,Req_Fee_Percent__c, Req_Min_Book_of_Business__c, Req_Work_Remote__c, Req_Practice_Area__c, Req_Search_Tags_Keywords__c,
                                                   Req_Contact_like_in_hires__c,OpCo__c, Req_Division__c, Id, Req_Additional_1st_Year_Compensation__c,
                                                   (select contact.firstname,contact.lastname,contact.Id,Role, IsPrimary from opportunitycontactroles),
                                                   createddate, Req_Minimum_Education_Required__c, Req_Willing_to_Travel__c, Req_Visa_Sponsorship__c, Immediate_Start__c,
                                                   Apex_Context__c, Req_Total_Spread__c,Req_Rate_Frequency__c, Retainer_Fee__c, Req_Job_Level__c,Req_Geolocation__Latitude__s, Req_Geolocation__Longitude__s,Req_OpCo_Code__c,OwnerId,
                                                   Owner.Name,owner.Peoplesoft_Id__c,Bill_Rate_Max_Multi_Currency__c,Bill_Rate_Min_Multi_Currency__c,Pay_Rate_Max_Multi_Currency__c,Pay_Rate_Min_Multi_Currency__c,
                                                   Salary_Max_Multi_Currency__c,Salary_Min_Multi_Currency__c,Bill_Rate_max_Normalized__c,Bill_Rate_min_Normalized__c,Pay_Rate_Max_Normalized__c,Pay_Rate_Min_Normalized__c,Legacy_Product__c,Req_Job_Code__c,
                                                   Req_TGS_Requirement__c from opportunity where Id IN : oppIds]);
        
        for(Opportunity opp : opportunityMap.Values())  
        {
            if(opp.StageName != 'Staging' && opp.RecordType.Name == 'Req'){
               
                    opptyList.add(opp);
                    
            }   
            
        } 
    }
    
    
    public void getGeoLocationFromAPI(){
        geoLocationOppMap  = new Map<String,Opportunity>();        
        try{
            if(opptyList.size() > 0 && opptyList.size() < 201){
                  geoLocationOppMap  = GetGeoLocationFromMulesoft.getLocationOpportunityByZip(opptyList);  
            System.debug('geoLocationOppMap@@@'+geoLocationOppMap);
			}
                
           
        }catch(Exception e){
            System.debug(logginglevel.WARN,'API Message: ' + e.getMessage());
        }
    }
    public void updateOpportunitiesWithLoc(){ 
        
         List<Opportunity> oppList = new List<Opportunity>();
    Opportunity opp;
    List<Database.SaveResult> results;
    system.debug('opportunity map'+ opportunityMap);
    
      
      try {
            
            //update opportunities that have lat/long from Soa
            System.debug('GEO MAP '+geoLocationOppMap);
            if(geoLocationOppMap != null && geoLocationOppMap.size() > 0){
                
                
                for(Opportunity reqoppty: geoLocationOppMap.Values()){
                 if((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) && reqoppty.Req_GeoLocation__Latitude__s != null && reqoppty.Req_GeoLocation__Longitude__s != null){
                    finalGeoLocationOppMap.put(reqoppty.Id, reqoppty);
                 }
                }
                
               /*if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration){
                  update geoLocationOppMap.Values(); 
               }else */if(finalGeoLocationOppMap != null && finalGeoLocationOppMap.size() > 0){
                  update finalGeoLocationOppMap.Values();
               }
                
            }
            
        } catch (DmlException de) {
            Integer numErrors = de.getNumDml();
            System.debug(logginglevel.WARN,'getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                System.debug(logginglevel.WARN,'getDmlFieldNames=' + de.getDmlFieldNames(i));
                System.debug(logginglevel.WARN,'getDmlMessage=' + de.getDmlMessage(i));
            }
        } catch (Exception e) {
            System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
        }
    }
    
}