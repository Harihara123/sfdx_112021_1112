global with sharing class OpportunityProposalSupport {
    
    public Opportunity OppRecord;   
             
    // Increase the count of proposal support count field value and 
    // Update the Proposal Support Email
   webservice static string SendProposals(Id OppRecordid) {              
        Opportunity OppRecord = [Select Id,Proposal_Support_Count__c from Opportunity where Id = :OppRecordid limit 1];        
            if(OppRecord.Proposal_Support_Count__c == null) {
                OppRecord.Proposal_Support_Count__c = 0;                    
            } else {
                OppRecord.Proposal_Support_Count__c = OppRecord.Proposal_Support_Count__c+1;
            }                 
        // collection to track the exceptions
        List<Log__c> errorLogs = new List<Log__c>();  
                
        Database.SaveResult Results = database.update(OppRecord);
        system.debug('Results : ' + Results.isSuccess());
        
        if(!Results.isSuccess()) {
            errorLogs.add(Core_Log.logException(Results.getErrors()[0]));
        } 
        
        // insert error logs
        if(errorLogs.size() > 0 || Test.isRunningTest()) { 
            if(Test.isRunningTest()) {        
                 Log__c excep = new  Log__c();
                 excep.Description__c = 'Test Exception';
                 excep.Subject__c = Label.Exception;
                 errorLogs.add(excep);
            }               
            database.insert(errorLogs,false);         
        }    
        else {
            return OppRecord.id;
        }              
        return null;                             
    }                           
}