public with sharing class OpportunityRoutingController{
     public Boolean flag{get;set;}
    
    public OpportunityRoutingController(ApexPages.StandardController controller){
        if(UserInfo.getUiThemeDisplayed()=='Theme4d'){
           flag=true;
       }
       else{
           flag=false;
       }
    }
    public pagereference getRouter(){
        map<string,Routing_Controller_Settings__c> RoutingControllerSettings = Routing_Controller_Settings__c.getAll();
        String Opp_Opcoid = RoutingControllerSettings.get('Opp_Opco').FieldID__c;
        List<User> usr = [Select Id,Office__c,OPCO__c FROM User Where Id=: userinfo.getuserid()];
        system.debug(usr[0].OPCO__c);
        String Opco_Mapping;
        
        if(usr[0].OPCO__c ==null){
        
        Opco_Mapping = '';
        }
        
        else{
        
       OPCO_Picklist_Mapping__c OPCO_Mapping_Setting = OPCO_Picklist_Mapping__c.getValues(usr[0].OPCO__c);    
       
       
       system.debug( OPCO_Mapping_Setting );
        if(OPCO_Mapping_Setting <> Null)
        Opco_Mapping = OPCO_Mapping_Setting.Picklist_Value__c;
        }
        
        
        
            
            system.debug(Opco_Mapping);
        
        
        
       
     
        string Rectypeparm= '';
        string chosenRT= ApexPages.CurrentPage().GetParameters().Get('Recordtype');
       /* if(chosenRT!=null){
            Rectypeparm = '&RecordType='+chosenRT;
        }
        else {
           chosenRT=''; 
        } */
        
        String Accountid = ApexPages.CurrentPage().GetParameters().Get('accid');
        String urlVal = Apexpages.currentPage().getUrl();
        system.debug(urlVal);
        // string url;
         PageReference url;
       if(Accountid!=null){
          Url = new PageReference('/006/e');
          Url.getParameters().put('retURL', '006');
          Url.getParameters().put(Opp_Opcoid, Opco_Mapping);
          Url.getParameters().put('accid', Accountid);
          Url.getParameters().put('opp11', 'Interest');
          Url.getParameters().put('nooverride', '1');
          if(chosenRT!=null){
          Url.getParameters().put('RecordType', chosenRT);    
          }
       // Url ='/006/e?retURL=%2F006%2Fo&'+Opp_Opcoid+'='+Opco_Mapping+'&accid='+Accountid+Rectypeparm +'&opp11=Interest'+'&nooverride=1';
        }
        
        else{
          Url = new PageReference('/006/e');
          Url.getParameters().put('retURL', '006');
          Url.getParameters().put(Opp_Opcoid, Opco_Mapping);          
          Url.getParameters().put('opp11', 'Interest');
          Url.getParameters().put('nooverride', '1');
          if(chosenRT!=null){
              Url.getParameters().put('RecordType', chosenRT);
          }
            // Url ='/006/e?retURL=%2F006%2Fo&'+Opp_Opcoid+'='+Opco_Mapping+Rectypeparm+'&opp11=Interest'+'&nooverride=1';
        }
        
   /*     PageReference p ;   
        p = new PageReference(Url);
       return p; */
     return Url;  
    }
}