public class OpportunityStrategicPlanTriggerHandler
{

    public static void OnBeforeInsert(List<Opportunity_Strategic_Initiative__c> lstRecords) 
    {
        List<Teaming_Partner__c> lstTeamingPartner = new List<Teaming_Partner__c>();
        set<Id> setId = new set<Id>();
        
        for(Opportunity_Strategic_Initiative__c osi : lstRecords)   
        {
            setId.add(osi.Strategic_Initiative__c);
        }
        try
        {
            Map<Id,Strategic_Initiative__c> mapStrategic = new Map<Id,Strategic_Initiative__c>([select id,Primary_Account__c from Strategic_Initiative__c where id =: setId and Recordtype.Name = 'Partnership Plan']);
            
            for(Opportunity_Strategic_Initiative__c osi : lstRecords)   
            {
                if(mapStrategic.get(osi.Strategic_Initiative__c) != null && mapStrategic.get(osi.Strategic_Initiative__c).Primary_Account__c != null)
                {
                    Teaming_Partner__c tp = new Teaming_Partner__c();
                    tp.Opportunity__c = osi.Opportunity__c;
                    tp.Account__c = mapStrategic.get(osi.Strategic_Initiative__c).Primary_Account__c;
                    lstTeamingPartner.add(tp);
                }
            } 
            if(!lstTeamingPartner.isEmpty())
            {
                insert lstTeamingPartner;
            }
        }
        catch(Exception e)
        {
            lstRecords[0].addError('Account cannot be same for the partnership plan and related Opportunity.');
        }
    }
    
    public static void OnAfterInsert(List<Opportunity_Strategic_Initiative__c> lstRecords) 
    {
        Set<Id> setStrategicPlanId = new Set<Id>();
        for(Opportunity_Strategic_Initiative__c osi : lstRecords)
        {
            setStrategicPlanId.add(osi.Strategic_Initiative__c);
        }
        
        SendEmail(setStrategicPlanId,lstRecords);
    }
    
    public static void OnAfterDelete(List<Opportunity_Strategic_Initiative__c> lstRecords) 
    {
        List<Teaming_Partner__c> lstTeamingPartner = new List<Teaming_Partner__c>();
        set<Id> setId = new set<Id>();
        set<Id> setPrimaryAccountIds = new set<Id>();
        set<Id> setOpportunityIds = new set<Id>();
        
        
        for(Opportunity_Strategic_Initiative__c osi : lstRecords)   
        {
            setId.add(osi.Strategic_Initiative__c);
        }
        
        Map<Id,Strategic_Initiative__c> mapStrategic = new Map<Id,Strategic_Initiative__c>([select id,Primary_Account__c from Strategic_Initiative__c where id =: setId]);
        
        for(Opportunity_Strategic_Initiative__c osi : lstRecords)   
        {
            if(mapStrategic.get(osi.Strategic_Initiative__c) != null && mapStrategic.get(osi.Strategic_Initiative__c).Primary_Account__c != null)
            {
                setPrimaryAccountIds.add(mapStrategic.get(osi.Strategic_Initiative__c).Primary_Account__c);
                setOpportunityIds.add(osi.Opportunity__c);
            }
        } 
        
        lstTeamingPartner = [select id from Teaming_Partner__c where Opportunity__c in: setOpportunityIds and Account__c in:  setPrimaryAccountIds];
        if(!lstTeamingPartner.isEmpty())
        {
            delete lstTeamingPartner;
        }
    }
    
    public static void SendEmail(Set<Id> setStrategicPlanId,List<Opportunity_Strategic_Initiative__c> lstRecords) 
    {
        List<Strategic_Initiative_Member__c> lstTeamMember = [Select id,User__c from Strategic_Initiative_Member__c where Strategic_Initiative__r.Recordtype.Name = 'Partnership Plan' and Strategic_Initiative__c =: setStrategicPlanId];
        if(!lstTeamMember.isEmpty())
        {
            List<Id> lstids= new List<Id>();
            for(Strategic_Initiative_Member__c sim : lstTeamMember) 
            {
                lstids.add(sim.User__c);
            }
            EmailTemplate et=[Select id from EmailTemplate where name = 'Notification_Opportunity_PartnerPlan_VF' limit 1];
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(lstids);
            mail.setTargetObjectId(lstids[0]);
            mail.setSenderDisplayName('System Admin');
            mail.setTemplateId(et.id);
            mail.setWhatId(lstRecords[0].id);
            mail.saveAsActivity = false;
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

}