@isTest
private class OpportunityStrategicPlanTriggerTest
{
     static User user = TestDataHelper.createUser('System Administrator');
     
    static testmethod void strategicplanTest()
    {
         List<Opportunity> lstOpportunity = new List<Opportunity>();   
         List<Strategic_Initiative__c> lstNewStratInitiative = new List<Strategic_Initiative__c>(); 
         List<Strategic_Initiative_Member__c> StratInitiativeMembers = new List<Strategic_Initiative_Member__c>();       
        
        // insert OA Calculator Data
        TestDataHelper.createOACaculatorData();
        Test.startTest(); 
       
        if(user != Null) {
            system.runAs(user) {    
                
                TestData TdAcc = new TestData(1);

                // Get the TEK recordtype Id from a name        
                string TEKRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_Tek_System_RecordType_for_Assessment_Forms).getRecordTypeId();
                
                for(Account Acc : TdAcc.createAccounts()) 
                {                     
                    Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                         RecordTypeId = TEKRecordType, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                        Response_Type__c = 'Formal RFx' , stagename = 'Interest', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',Services_GP__c = 20,CloseDate = system.today()+1);  
                        
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                        
                         
                }                
                // Insert list of Opportunity
                insert lstOpportunity; 
                
                string recordTypeId = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Partnership Plan').getRecordTypeId();
                
                for(Account Acc : TdAcc.createAccounts()) 
                {
                    Strategic_Initiative__c newStratInitiative = new Strategic_Initiative__c(
                    Name = 'SampleStrategy', status__c = 'Active', Start_Date__c = system.today(), opco__c = 'Aerotek', Impacted_Region__c ='test',
                    End_Date__c =  system.today()+1,Primary_Account__c=acc.Id, Recordtypeid = recordTypeId);
                    lstNewStratInitiative.add(newStratInitiative);
                }
                insert lstNewStratInitiative;
                
                for(Strategic_Initiative__c SI : lstNewStratInitiative) {
                    Strategic_Initiative_Member__c NewStratInitiativeMember = new Strategic_Initiative_Member__c (
                    Strategic_Initiative__c = SI.id, User__c = user.id);
                    
                    // Load Stratgic initiatives members
                    StratInitiativeMembers.add(NewStratInitiativeMember);
                }  
                
                // insert Stratgic initiatives members
                insert StratInitiativeMembers;
                
                Opportunity_Strategic_Initiative__c osi = new Opportunity_Strategic_Initiative__c();
                osi.Opportunity__c = lstOpportunity[0].id;
                osi.Strategic_Initiative__c = lstNewStratInitiative[0].id;
                insert osi;
                
                delete osi;
            }
        }
    }
}