/***************************************************************************
Name        : OpportunityTeamMemberTriggerHandler
Created By  : Krishna Chitneni
Date        : 21 Feb 2018
Story/Task  : S-291651 / T360150
Purpose     : Class that contains all of the functionality called by the
              OpportunityTeamMemberTrigger. All contexts should be in this class.
*****************************************************************************/
public with sharing class OpportunityTeamMemberTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, OpportunityTeamMember> oldMap = new Map<Id, OpportunityTeamMember>();
    Map<Id, OpportunityTeamMember> newMap = new Map<Id, OpportunityTeamMember>();
	public Static Boolean isSFDRZUpdate = false;
    private static boolean hasBeenProccessed = false;
    
    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<OpportunityTeamMember> newRecords) {

    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, OpportunityTeamMember> newMap, List<OpportunityTeamMember> newRecords) {
        createTeamMemberEvent(null,newMap, 'Add');
        countAllocatedRecruiters(null,newMap, 'Add');
        TrackAllocationSourceController.trackAllocatedRecruiters(newRecords);
        List<OpportunityTeamMember> newRecs = newMap.values();
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
		if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'OpportunityTeamMemberTriggerHandler', 'OnAfterInsert', 
                    'Triggered ' + newRecs.size() + ' OpportunityTeamMember records: ' + CDCDataExportUtility.joinObjIds(newRecs));
            }
		    PubSubBatchHandler.insertDataExteractionRecord(newRecs, 'OpportunityTeamMember');
		    hasBeenProccessed = true; 
		}

        raiseGCPSyncEvents(newRecs, false);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, OpportunityTeamMember>oldMap, Map<Id, OpportunityTeamMember>newMap) {

    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, OpportunityTeamMember>oldMap, Map<Id, OpportunityTeamMember>newMap) {
    	//List<OpportunityTeamMember> lstMembers = newMap.values();
        createTeamMemberEvent(oldMap,newMap, 'Edit');
        countAllocatedRecruiters(oldMap,newMap, 'Edit');
		List<OpportunityTeamMember> newRecs = newMap.values();
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
		if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'OpportunityTeamMemberTriggerHandler', 'OnAfterUpdate', 
                    'Triggered ' + newRecs.size() + ' OpportunityTeamMember records: ' + CDCDataExportUtility.joinObjIds(newRecs));
            }
		    PubSubBatchHandler.insertDataExteractionRecord(newRecs, 'OpportunityTeamMember');
		    hasBeenProccessed = true; 
		}

        raiseGCPSyncEvents(newRecs, false);
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, OpportunityTeamMember>oldMap) {
        if(!TriggerStopper.deleteRecursiveReqTeamMembersStopper)
        {
            TriggerStopper.deleteRecursiveOpportunityTeamMembersStopper = true;
            DeleteReqTeamMembers(oldMap);
        }
    
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, OpportunityTeamMember>oldMap) {
     	//List<OpportunityTeamMember> lstMembers = oldMap.values();
        createTeamMemberEvent(oldMap,null, 'Delete');
        countAllocatedRecruiters(oldMap,newMap, 'Delete');
		List<OpportunityTeamMember> recs = oldMap.values();
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
		if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'OpportunityTeamMemberTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + recs.size() + ' OpportunityTeamMember records: ' + CDCDataExportUtility.joinObjIds(recs));
            }
		    PubSubBatchHandler.insertDataExteractionRecord(recs, 'OpportunityTeamMember');
		    hasBeenProccessed = true; 
		}

        raiseGCPSyncEvents(recs, true);
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUndelete (Map<Id, OpportunityTeamMember>newMap) {
    }


    
    /************************************************************************************************************
    Orignal Header  : trigger DeleteReqTeamMembers on OpportunityTeamMember (after delete)
    Name            :  DeleteReqTeamMembers
    Version         :
    Description     :
    ***************************************************************************************************************/
    public void DeleteReqTeamMembers(Map<Id, OpportunityTeamMember> oldMap) 
    {
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) 
        { 
            //List<OpportunityTeamMember> lstOpportunityTeamMembers = [select id,userid,teammemberrole,Opportunity.Req_origination_System_Id__c from //OpportunityTeamMember where id in:oldMap.keyset()];
            
            set<id> oppid = new set<id>();
            for(OpportunityTeamMember oppTeamMember : oldMap.values())
            {
                oppid.add(oppTeamMember.opportunityId);
            }
            List<Reqs__c> lstReq = [select id from Reqs__c where EnterpriseReqId__c = : oppid];
            Id reqid = null;
            if(!lstReq.isEmpty())
             {
                 reqid = lstReq[0].id;
                 
             }
             else
             {
                 List<Opportunity> lstOpportunity = [select id,Source_System_Id__c from Opportunity where id = : oppid];
                 reqid = lstOpportunity[0].Source_System_Id__c;
             }
             
             if(reqid != null)
             {
                 List<Req_Team_Member__c> lstReqTeamMember = [select id from Req_Team_Member__c where User__c =: oldMap.values()[0].userid and Requisition_Team_Role__c=:oldMap.values()[0].teammemberrole and Requisition__c =: reqid];
                 
                 if(!lstReqTeamMember.isEmpty())
                 delete lstReqTeamMember;
             }
        }
    }
    
    
    public void createTeamMemberEvent(Map<Id,OpportunityTeamMember>oldMap, Map<Id,OpportunityTeamMember>newMap, String eventType){
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration){ 
            string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            Map<Id,OpportunityTeamMember>newReqOpptyMap = new Map<Id,OpportunityTeamMember>();
            Map<Id,OpportunityTeamMember>oldReqOpptyMap = new Map<Id,OpportunityTeamMember>();
            Map<Id,String> TeamMemberJSon = new Map<Id,String>();
            list<string> TeamMemberOpptyId = new list<string>();
            list<string> RemoveTeamMemberOpptyId = new list<string>();
            List<OpportunityTeamMember> TeamJSonList = new List<OpportunityTeamMember>();
            if(eventType == 'Add' || eventType == 'Edit'){
                for(OpportunityTeamMember O:[Select id,TeamMemberRole,OpportunityId,isDRZUpdate__c,UserId,LastModifiedById,LastModifiedDate 
                                             from OpportunityTeamMember 
                                             where id in:newMap.keyset() and Opportunity.RecordTypeId=:reqRecordTypeId and Opportunity.isClosed = false]){
                	newReqOpptyMap.put(O.id,O);
                }
                if(!newReqOpptyMap.isEmpty()){
                    if(oldMap == null){
                        for(OpportunityTeamMember O: newReqOpptyMap.values()){
                            if(O.TeamMemberRole == 'Allocated Recruiter' && O.isDRZUpdate__c == False){
                                TeamMemberOpptyId.add(O.OpportunityId);
                                TeamJSonList.add(O);
                                TeamMemberJSon.put(O.OpportunityId,GenerateJSON(TeamJSonList));
                            }
                        }
                    }else{
                        for(OpportunityTeamMember O: newReqOpptyMap.values()){
                            if(O.isDRZUpdate__c == False){
                                if(O.TeamMemberRole == 'Allocated Recruiter' && oldMap.get(O.id).TeamMemberRole != 'Allocated Recruiter'){
                                    TeamMemberOpptyId.add(O.OpportunityId);
                                    TeamJSonList.add(O);
                                    TeamMemberJSon.put(O.OpportunityId,GenerateJSON(TeamJSonList));
                                }else if(O.TeamMemberRole != 'Allocated Recruiter' && oldMap.get(O.id).TeamMemberRole == 'Allocated Recruiter'){
                                    RemoveTeamMemberOpptyId.add(O.OpportunityId);
                                    TeamJSonList.add(O);
                                    TeamMemberJSon.put(O.OpportunityId,GenerateJSON(TeamJSonList));
                                }
                            }
                        }
                    }
                }
            }
            if(eventType == 'Delete'){
				for(OpportunityTeamMember O: oldMap.values()){
					if(O.TeamMemberRole == 'Allocated Recruiter' && O.isDRZUpdate__c == False){
						RemoveTeamMemberOpptyId.add(O.OpportunityId);
						TeamJSonList.add(O);
						TeamMemberJSon.put(O.OpportunityId,GenerateJSON(TeamJSonList));
					}
				}
            }
            system.debug('---TeamMemberJSon---'+TeamMemberJSon);
            if(!TeamMemberOpptyId.isEmpty()){
                DRZEventPublisher.publishOppEventForTeamMember(TeamMemberOpptyId,TeamMemberJSon,'ADD_MEMBER');
				if(System.Label.SF_DRZ_Flag == 'False' && isSFDRZUpdate == false)
                DRZ_SFCommonUtilCntrl.UpdateDRZCardTeamMember(TeamMemberOpptyId);
            }
            if(!RemoveTeamMemberOpptyId.isEmpty()){
                DRZEventPublisher.publishOppEventForTeamMember(RemoveTeamMemberOpptyId,TeamMemberJSon,'REMOVE_MEMBER');
				if(System.Label.SF_DRZ_Flag == 'False' && isSFDRZUpdate == false)
                DRZ_SFCommonUtilCntrl.UpdateDRZCardTeamMember(RemoveTeamMemberOpptyId);
            }
        }
    }
	@TestVisible
    private String GenerateJSON(List<OpportunityTeamMember> TeamMember){
        String oppTeamJson = '';
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject();
        gen.writeFieldName('OppTeamMember');
        gen.writeStartArray(); 
        if(!TeamMember.isEmpty()){
            for (OpportunityTeamMember otm : TeamMember) {
                gen.writeStartObject();
                gen.writeStringField('UserSFId',otm.UserId);
                gen.writeStringField('OpportunityId',otm.OpportunityId);
                gen.writeStringField('TeamMemberRole',otm.TeamMemberRole);
                gen.writeStringField('LastModifiedById',otm.LastModifiedById);
                gen.writeDateTimeField('DateTime',otm.LastModifiedDate);
                gen.writeEndObject();
            }
            gen.writeEndArray();
            gen.writeEndObject();
            oppTeamJson = gen.getAsString();
        }
        system.debug('--oppTeamJson--'+oppTeamJson);
        return oppTeamJson;
    }

    private static void raiseGCPSyncEvents(List<OpportunityTeamMember> otmList, Boolean isDelete) {
        List<GCP_Sync_Request_Event__e> syncEvtList = new List<GCP_Sync_Request_Event__e>();
        for (OpportunityTeamMember otm : otmList) {
            GCP_Sync_Request_Event__e gsre = new GCP_Sync_Request_Event__e(
                isDelete__c = isDelete,
                ObjectId__c = otm.Id,
                ObjectJson__c = JSON.serialize(otm),
                SObjectName__c = 'OpportunityTeamMember'
            );
            syncEvtList.add(gsre);
        }

        if (syncEvtList.size() > 0) {
            // Call method to publish events
            List<Database.SaveResult> results = EventBus.publish(syncEvtList);
            
            // Inspect publishing result for each event
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    System.debug('Successfully published event.');
                } else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Error returned: ' +
                                    err.getStatusCode() +
                                    ' - ' +
                                    err.getMessage());
                    }
                }       
            }
        }

    }
    
    public void countAllocatedRecruiters(Map<Id,OpportunityTeamMember>oldMap, Map<Id,OpportunityTeamMember>newMap, String eventType) {
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        List<OpportunityTeamMember> opportunityTeamMembersToProcess = new List<OpportunityTeamMember> ();
        Set<Id> opportunityIds = new Set<Id> ();
        if(eventType == 'Add'){
            for(OpportunityTeamMember member: newMap.values()) {
                if(
                    member.TeamMemberRole != null && 
                    member.TeamMemberRole == 'Allocated Recruiter'
                ) {
                    opportunityIds.add(member.OpportunityId);
                }
            }
        }
        else if(eventType == 'Edit'){
            for(OpportunityTeamMember member: newMap.values()) {
                if(
                    member.TeamMemberRole != null && 
                    member.TeamMemberRole != oldMap.get(member.Id).TeamMemberRole &&
                    (
                        member.TeamMemberRole == 'Allocated Recruiter' ||
                        oldMap.get(member.Id).TeamMemberRole == 'Allocated Recruiter'
                    )
                ) {
                	opportunityIds.add(member.OpportunityId);
                }
            }
        } else if(eventType == 'Delete'){
            for(OpportunityTeamMember member: oldMap.values()) {
                if(
                    member.TeamMemberRole != null && 
                    member.TeamMemberRole == 'Allocated Recruiter'
                ) {
                	opportunityIds.add(member.OpportunityId);
                }
            }
        }
        
        if(!opportunityIds.isEmpty()) {
            List<Opportunity> opportunities = [Select Id,Number_of_Allocated_Recruiters__c,Allocated_Recruiter_Score_ED__c,ED_Outcome__c, (Select Id from OpportunityTeamMembers where TeamMemberRole = 'Allocated Recruiter') from Opportunity where Id IN: opportunityIds AND RecordType.Name = 'Req'];
            
            if(!opportunities.isEmpty()) {
                for(Opportunity opp: opportunities) {
                    opp.Number_of_Allocated_Recruiters__c = opp.OpportunityTeamMembers.size();
                    if(opp.Number_of_Allocated_Recruiters__c > 0 && (Opp.Allocated_Recruiter_Score_ED__c ==null || Opp.Allocated_Recruiter_Score_ED__c == 0 )){
        Opp.Allocated_Recruiter_Score_ED__c = Opp.ED_Outcome__c;
        }
                }
                
                update opportunities;
            }
        }
	}
}