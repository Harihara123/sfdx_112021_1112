/**************************************************************************************************************************
Name            :  Opportunity_AssessmentHelper
Version         :   1.0 
Created Date    :   29 May 2012
Function        :   Class to calculate Probabilty and Business Value fields on the Opportunity. It looks through the custom settings
                    OA_Calculator__c for the related scores and then add them up. 
Modification Log:
-----------------------------------------------------------------------------
* Developer                  Date                   Description
* ----------------------------------------------------------------------------                 
* Pavan                      29/05/2012              created
*****************************************************************************************************************************/

public class Opportunity_AssessmentHelper{
   
    static Map<String,Double> oppCalMap = new Map<String,Double>();   //fetching the values from custom setting OA_
    static Map<string,List<OA_Calculator__c>> oaCalculator = new Map<string,List<OA_Calculator__c>>();
    static Map<string,List<OA_Calculator_Weights_mapping__c>> oaWeights = new Map<string,List<OA_Calculator_Weights_mapping__c>>();
    // below variables play a key role in differentiating the fields for calculation.
    static final String SECTION_B = 'Section B';
    static final string SECTION_A = 'Section A';
    static double sumMaxScore_Probability_SectionA = 0;
    static double sumMaxScore_Probability_SectionB = 0;
    static double sumMaxScore_Probability_NonRfx = 0;
    static double sumMaxScore_BV = 0;    
    //static variable to track the triggering events
    public static boolean isEvaluated = false;
    static string Weights_To_be_Used = 'ONS_Weights__c';
    static double POA_Multiplier = 100;
    static double BV_Multiplier = 100;
    static boolean Stage_criteria = false;
    static boolean Override_Standard_Probability = false;
    
    /***********************************************************************
    Method to initialize OA Calculator collections
    ************************************************************************/
    public static Map<string,List<OA_Calculator__c>> init()
    {
        // populate the oppty fields map
        for(OA_Calculator__c flds : OA_Calculator__c.getAll().values())
        {
               if(oaCalculator.get(flds.Field_API_Name__c) != Null)
               {
                   oaCalculator.get(flds.Field_API_Name__c).add(flds);
               }
               else
               {
                  List<OA_Calculator__c> temp = new List<OA_Calculator__c>();
                  temp.add(flds);
                  oaCalculator.put(flds.Field_API_Name__c,temp);
               }    
        }
        System.debug('!!!!!oaCalculator in init(): '+oaCalculator);
        return oaCalculator;
    }

    /***********************************************************************
    Method to initialize weights maping collections
    ************************************************************************/
    public static Map<string,List<OA_Calculator_Weights_mapping__c>> WeightsInit()
    {
        // populate the oppty fields map
        for(OA_Calculator_Weights_mapping__c flds : OA_Calculator_Weights_mapping__c.getAll().values())
        {
               if(oaWeights.get(flds.Opco__c) != Null)
               {
                   oaWeights.get(flds.Opco__c).add(flds);
               }
               else
               {
                  List<OA_Calculator_Weights_mapping__c> temp = new List<OA_Calculator_Weights_mapping__c>();
                  temp.add(flds);
                  oaWeights.put(flds.Opco__c,temp);
               }    
        }
        System.debug('!!!!!oaWeights in WeightsInit(): '+oaWeights);
        return oaWeights;
    }
    /*************************************************************************
    Method to calculate the BV and probability percentile
    ***************************************************************************/
    public static void assessOpportunity(List<opportunity> OpportunitiesFiltered)
    {
        //initialize OA Cal collections
        init();        
        //initialize Weights mapping collections
        WeightsInit();
        //assess filtered opportunities
        for(opportunity opp : OpportunitiesFiltered)
        {
           system.debug('!!!!! opp.Probability in assessment begining: '+opp.Probability);
           boolean isRFPType = false;
           Double oppty_RFX_N = 0;
           Double oppty_NonRFX_N = 0;
           Double opptyBVScore_N = 0;
           Double opptyScore = 0;
           Double rfpDenominator = 0; 
           if(opp.Response_Type__c == Label.OA_Calculator_Oppty_Type || opp.Response_Type__c == 'RFx')
               isRFPType = true;

           // get weight mappings by opco and business unit
           GetWeights(opp.OpCo__c, opp.BusinessUnit__c);  
           // calculate max score for BV and probability values
           calculateMaxScore();  
           // perform the calculation
            for(string fld : oaCalculator.keyset())
            {
                    if(opp.get(fld) != Null)
                    {
                        System.debug('!!!!! fld '+fld);
                           for(OA_Calculator__c calc : oaCalculator.get(fld))
                           {                             
                                  if(opp.get(fld) == calc.Assessment_Values__c)
                                  {
                                        //verify if the field is considered for Standard probability calculation
                                        if(calc.Consider_for_Standard_Probability__c)
                                        {
                                          if(isRFPType){
                                            if(calc.RFX__c)
                                            {
                                                oppty_RFX_N += (calc.Score__c * (Double)calc.get(Weights_To_be_Used));    
                                                System.debug('***** Rfx '+fld +'-'+calc.Assessment_Values__c+'-' +calc.Score__c +'*'+ (Double)calc.get(Weights_To_be_Used) +'='+oppty_RFX_N);                                        
                                            break;
                                            }                                                                                          
                                          }else{
                                            if(calc.Non_RFX__c)
                                            {
                                                oppty_NonRFX_N += (calc.Score__c * (Double)calc.get(Weights_To_be_Used));
                                                System.debug('***** Non Rfx '+fld +'-'+calc.Assessment_Values__c+'-' +calc.Score__c +'*'+ (Double)calc.get(Weights_To_be_Used) +'='+oppty_NonRFX_N);
                                            break;
                                            }                                                                                            
                                          }      
                                         }

                                        if(calc.Consider_for_BV__c){
                                            opptyBVScore_N +=  (calc.Score__c * (Double)calc.get(Weights_To_be_Used)); 
                                            System.debug('***** BV '+fld +'-'+calc.Assessment_Values__c+'-' +calc.Score__c +'*'+ (Double)calc.get(Weights_To_be_Used) +'='+opptyBVScore_N); 
                                            break; 
                                        }    
                                  }

                           }
                    }
            }
            System.debug('!!!!! oppty_RFX_N in opp.Probability: '+oppty_RFX_N);
            System.debug('!!!!! oppty_NonRFX_N in opp.Probability: '+oppty_NonRFX_N);
            System.debug('!!!!! opptyBVScore_N in opp.Probability: '+opptyBVScore_N);
            // assign the calculated score to oppty accordingly
            if(isRFPType)
            {  
                rfpDenominator = sumMaxScore_Probability_SectionA + sumMaxScore_Probability_SectionB; 
                if(rfpDenominator >0){
                    opptyScore = oppty_RFX_N/rfpDenominator;
                    System.debug('!!!!! opptyScore if RFX: '+opptyScore);
                }         
                
            }
            else
            {
                if(sumMaxScore_Probability_NonRfx > 0){
                opptyScore = oppty_NonRFX_N/sumMaxScore_Probability_NonRfx;
                System.debug('!!!!! opptyScore if Non RFX: '+opptyScore);
                }                 
            }
             // Normalize the values accordingly
             opp.Probability_new__c = (opptyScore * POA_Multiplier).round();
             if(sumMaxScore_BV > 0)
             opp.Business_Value__c = (opptyBVScore_N /sumMaxScore_BV) * BV_Multiplier; 
            // Stage Criteria 
            if(Override_Standard_Probability){
              if(opp.StageName!= 'Closed Won' && opp.StageName!= 'Closed Lost')
            opp.Probability = (opp.Probability_new__c).round();
            
            if(Stage_criteria && opp.StageName == 'Interest')
            opp.Probability = 20;
            else if(Stage_criteria && opp.StageName == 'Negotiation' && opp.Probability > 63)
            opp.Probability = 95;
            else if(Stage_criteria && opp.StageName == 'Negotiation' && opp.Probability <= 63){
                opp.Probability = 95;
                isEvaluated = false;
            }
            else if (opp.StageName== 'Closed Won')
            opp.Probability = 100;
            else if (opp.StageName== 'Closed Lost')
            opp.Probability = 0;  
            }         
            
            
            /*opp.Probability = opptyScore * 80;
            opp.Business_Value__c = (opptyBVScore /sumMaxScore_BV) * 100; 
            
            if(opp.StageName!= 'Closed Won' && opp.StageName!= 'Closed Lost')
            opp.Probability_new__c = opp.Probability;
            else if (opp.StageName== 'Closed Won')
            opp.Probability_new__c = 100;
            else if (opp.StageName== 'Closed Lost')
            opp.Probability_new__c = 0;*/
            
            system.debug('!!!!! opp.Probability final: '+opp.Probability);
            system.debug('!!!!! opp.Business_Value__c final: '+opp.Business_Value__c);
        }
        
    }
    /************************************************************
    Method to calculate Max BV value and Max Probability value
    **************************************************************/
    static void calculateMaxScore()
    {
        //calculate the SumMaxScore
        for(string fld : oaCalculator.keyset())
        {
            double max_Probability_SectionA = 0;
            double max_Probability_SectionB = 0;
            double max_Probability_NonRfx = 0;
            double max_BV = 0;
            double max_Probability_SectionA_WWeight = 0;
            double max_Probability_SectionB_WWeight = 0;
            double max_Probability_NonRfx_WWeight = 0;
            double max_BV_WWeight = 0;
            // loop over the related picklist values
            for(OA_Calculator__c calc : oaCalculator.get(fld))
            {
                // identify the max score among the picklist values for probability
                if(calc.Consider_for_Standard_Probability__c)
                {
                    if(calc.RFX__c && calc.Value_based_on_Oppty_Type__c != Null && calc.Value_based_on_Oppty_Type__c.Trim().Length() > 0)
                    {
                        if(calc.Value_based_on_Oppty_Type__c == SECTION_A)
                        {
                            if(max_Probability_SectionA < calc.Score__c)
                            {
                                max_Probability_SectionA = calc.Score__c;
                                max_Probability_SectionA_WWeight = (calc.Score__c * (Double)calc.get(Weights_To_be_Used));
                            }                                
                        }
                        else
                        {
                            if(max_Probability_SectionB < calc.Score__c)
                            {
                                max_Probability_SectionB = calc.Score__c;
                                max_Probability_SectionB_WWeight = (calc.Score__c * (Double)calc.get(Weights_To_be_Used));
                            }                                
                        }
                    }
                    if(calc.Non_RFX__c)
                    {
                               if(max_Probability_NonRfx < calc.Score__c)
                               {
                                  max_Probability_NonRfx = calc.Score__c;  
                                  max_Probability_NonRfx_WWeight = (calc.Score__c * (Double)calc.get(Weights_To_be_Used));                                         
                               }
                    }
                }
                // identify the max score among the picklist values for BV calculation
                if(calc.Consider_for_BV__c)
                {
                    if(max_BV < calc.Score__c)
                    {
                        max_BV = calc.Score__c;
                        max_BV_WWeight = (calc.Score__c * (Double)calc.get(Weights_To_be_Used));
                    }
                }
            }
            sumMaxScore_Probability_SectionA += max_Probability_SectionA_WWeight;
            sumMaxScore_Probability_SectionB += max_Probability_SectionB_WWeight;
            sumMaxScore_Probability_NonRfx += max_Probability_NonRfx_WWeight;
            sumMaxScore_BV += max_BV_WWeight;                        
        }
            System.Debug('!!!!! sumMaxScore_Probability_SectionA in calculateMaxScore: '+sumMaxScore_Probability_SectionA);
            System.Debug('!!!!! sumMaxScore_Probability_SectionB in calculateMaxScore: '+sumMaxScore_Probability_SectionB);
            System.Debug('!!!!! sumMaxScore_Probability_NonRfx in calculateMaxScore: '+sumMaxScore_Probability_NonRfx);
            System.Debug('!!!!! sumMaxScore_BV in calculateMaxScore: '+sumMaxScore_BV);
    }

    /************************************************************
    Method to Get Weight mapping
    **************************************************************/
    static void GetWeights(string OPCO, string BusinessUnit)
    {
        //calculate the SumMaxScore
        for(string fld : oaWeights.keyset())
        {
            
            // loop over the related picklist values
            for(OA_Calculator_Weights_mapping__c calc : oaWeights.get(fld))
            {
                // identify the max score among the picklist values for probability
                if(calc.Opco__c == OPCO && calc.Business_Unit__c == BusinessUnit)
                {
                    Weights_To_be_Used = calc.Weights_to_be_used__c;
                    POA_Multiplier = calc.POA_Multiplier__c;
                    BV_Multiplier = calc.BV_Multiplier__c;
                    Stage_criteria = calc.Use_Stage_Criteria__c; 
                    Override_Standard_Probability = calc.Override_Standard_Probability__c;
                }
                
            }
                                   
        }
            System.Debug('!!!!! Weights_To_be_Used in GetWeights: '+Weights_To_be_Used);
            System.Debug('!!!!! POA_Multiplier in GetWeights: '+POA_Multiplier);
            System.Debug('!!!!! BV_Multiplier in GetWeights: '+BV_Multiplier);
            System.Debug('!!!!! Stage_criteria in GetWeights: '+Stage_criteria);
    }
}