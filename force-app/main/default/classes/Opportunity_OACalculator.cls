public class Opportunity_OACalculator
{
    
    public static void calculationForOACalculator(List<Opportunity> newOpps)
    {
        
        System.debug('Inside Opportunity_OACalculator 1');
        //List<Opportunity> newOpps = new List<Opportunity>();
        string reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        
        List<Id> oppAccountList=new List<Id>();
        List<Id> oppHiringManList=new List<Id>();
        List<Id> oppOwnerIdList=new List<Id>();
        for(Opportunity opp:newOpps){
            oppAccountList.add(opp.AccountId);
            oppHiringManList.add(opp.Req_Hiring_Manager__c );
            oppOwnerIdList.add(opp.OwnerId);
        }
        
        
        Map<Id,Account> accountMapList =new Map<Id,Account>([select id,Total_Starts__c,Total_Currents__c from Account where id =:oppAccountList]);
        
        
        Map<Id,Contact> contactMapList =new Map<Id,Contact>( [select id,Total_Starts__c,Total_Currents__c from Contact where id =: oppHiringManList ]);
        
        Map<Id,Event> eventMapList= new Map<Id,Event>([select id,OwnerId,WhoId from Event where OwnerId =: oppOwnerIdList and WhoId =: oppHiringManList and CreatedDate= Last_N_Days:7]);
        
        for(Opportunity opp:newOpps){
            if(opp.RecordTypeId == reqRecordTypeId && (opp.Opco__c == 'AG_EMEA' || opp.Opco__c == 'Aerotek, Inc' || opp.Opco__c == 'TEKsystems, Inc.'))
            {
                System.debug('Inside Opportunity_OACalculator 2');
                try
                {
                    string reqCompletionScore =opp.ReqCompletition__c;
                    Map<string,OA_Calculator__c> mapOACalculator = new Map<string,OA_Calculator__c>();
                    List<Opportunity> lstOpportunityToCalculate = new List<Opportunity>();
                    decimal numerator = 0;
                    decimal denominator = 0;
                    
                    for(OA_Calculator__c oac : OA_Calculator__c.getAll().values())
                    {
                        string keyNumerator = oac.Field_API_Name__c + '_' + oac.Opco__c + '_' + oac.Product__c + '_' + oac.Assessment_Values__c;
                        system.debug('keyNumerator:::: ' + keyNumerator);
                        mapOACalculator.put(keyNumerator,oac);
                    }
                    System.debug('Inside Opportunity_OACalculator 55');     
                    //  List<Account> lstAccount = [select id,Total_Starts__c,Total_Currents__c from Account where id =: opp.AccountId];
                    // List<Contact> lstContact = [select id,Total_Starts__c,Total_Currents__c from Contact where id =: opp.Req_Hiring_Manager__c ];
                    
                    System.debug('Inside Opportunity_OACalculator 3');    
                    if(accountMapList.containsKey(opp.AccountId)){
                        string accountStatus = getStatus(accountMapList.get(opp.AccountId).Total_Starts__c,accountMapList.get(opp.AccountId).Total_Currents__c);
                        string accountsKey = 'Account_Status__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + accountStatus;
                        numerator += getNumerator(accountsKey,opp.Opco__c,mapOACalculator);
                        denominator += getdenominator(accountsKey,opp.Opco__c,mapOACalculator);
                    }
                    System.debug('Inside Opportunity_OACalculator 4');    
                    if(contactMapList.containsKey(opp.Req_Hiring_Manager__c)){
                        string contactStatus = getStatus(contactMapList.get(opp.Req_Hiring_Manager__c).Total_Starts__c,contactMapList.get(opp.Req_Hiring_Manager__c).Total_Currents__c);
                        string contactsKey = 'Contact_Status__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + contactStatus;
                        numerator += getNumerator(contactsKey,opp.Opco__c,mapOACalculator);
                        denominator += getdenominator(contactsKey,opp.Opco__c,mapOACalculator);
                    }        
                    System.debug('Inside Opportunity_OACalculator 5');   
                    if(!string.isBlank(reqCompletionScore) && (opp.Opco__c != 'AG_EMEA'))
                    {
                        string reqCompletionScoreKey = 'Req_Completion_Score__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + reqCompletionScore;
                        numerator += getNumerator(reqCompletionScoreKey,opp.Opco__c,mapOACalculator);
                        denominator += getdenominator(reqCompletionScoreKey,opp.Opco__c,mapOACalculator);
                    }
                    System.debug('Inside Opportunity_OACalculator 6');
                    if(!string.isBlank(opp.Req_Terms_of_engagement__c) && (opp.Opco__c != 'AG_EMEA'))
                    {
                        string termsOfEngagementKey = 'Req_Terms_of_engagement__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + opp.Req_Terms_of_engagement__c;
                        numerator += getNumerator(termsOfEngagementKey,opp.Opco__c,mapOACalculator);
                        denominator += getdenominator(termsOfEngagementKey,opp.Opco__c,mapOACalculator);
                    }
                    System.debug('Inside Opportunity_OACalculator 7');
                    string interviewDateStatus = opp.Req_Interview_Date__c == null ? 'No' : 'Yes';
                    string interviewDateKey = 'Req_Interview_Date__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + interviewDateStatus;
                    numerator += getNumerator(interviewDateKey,opp.Opco__c,mapOACalculator);
                    denominator += getdenominator(interviewDateKey,opp.Opco__c,mapOACalculator);
                    System.debug('Inside Opportunity_OACalculator 6');
                    if(opp.Opco__c != 'AG_EMEA'){
                        string dateClientOpenedPositions = (opp.Req_Date_Client_Opened_Position__c != null && (opp.Req_Date_Client_Opened_Position__c.daysBetween(System.Today()) <= 30))? 'Within a month' : 'Greater than a month';
                        string dateClientOpenedPositionsKey = 'Req_Date_Client_Opened_Position__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + dateClientOpenedPositions;
                        numerator += getNumerator(dateClientOpenedPositionsKey,opp.Opco__c,mapOACalculator);
                        denominator += getdenominator(dateClientOpenedPositionsKey,opp.Opco__c,mapOACalculator);
                    }
                    
                    string reqSummaryNumberKey = 'ReqSummarNumber__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + opp.ReqSummarNumber__c;
                    numerator += getNumerator(reqSummaryNumberKey,opp.Opco__c,mapOACalculator);
                    denominator += getdenominator(reqSummaryNumberKey,opp.Opco__c,mapOACalculator);
                    
                    List<Event> lstEvent=new List<Event>();
                    for(Id evtId:eventMapList.keySet()){
                        Event evt= eventMapList.get(evtId);
                        if(evt.OwnerId==opp.OwnerId && evt.WhoId==opp.Req_Hiring_Manager__c){
                            lstEvent.add(evt);
                        }
                    }
                    // List<Event> lstEvent = [select id from Event where OwnerId =: opp.OwnerId and WhoId =: opp.Req_Hiring_Manager__c and CreatedDate= Last_N_Days:7];
                    string hiringManagerMeetstatus = lstEvent.isEmpty() ? 'Not Met' : 'Met';
                    string hiringManagerMeetKey = 'Hiring_Manager_Meet__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + hiringManagerMeetstatus;
                    numerator += getNumerator(hiringManagerMeetKey,opp.Opco__c,mapOACalculator);
                    denominator += getdenominator(hiringManagerMeetKey,opp.Opco__c,mapOACalculator);
                    System.debug('Inside Opportunity_OACalculator 8');
                    if(opp.Req_Total_Spread__c != null )
                    {
                        string totalSpreadStatus = getTotalSpreadStatus(opp.Req_Total_Spread__c,opp.Opco__c );
                        string totalSpreadKey = 'Req_Total_Spread__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + totalSpreadStatus;
                        numerator += getNumerator(totalSpreadKey,opp.Opco__c,mapOACalculator);
                        denominator += getdenominator(totalSpreadKey,opp.Opco__c,mapOACalculator);
                    } 
                    //S-248037    
                    string exclusive;
                    if(opp.Opco__c == 'AG_EMEA'){
                        
                        if(opp.Req_Exclusive__c == True) {
                            exclusive ='Yes';
                        } else{
                            exclusive ='No';
                        }                       
                        string exclusiveKey = 'Req_Exclusive__c' + '_' + opp.Opco__c + '_' + opp.Req_Product__c + '_' + exclusive;
                        numerator += getNumerator(exclusiveKey,opp.Opco__c,mapOACalculator);
                        denominator += getdenominator(exclusiveKey,opp.Opco__c,mapOACalculator);
                        System.debug('Inside Opportunity_OACalculator 10'+numerator +'- '+denominator);
                    } 
                    opp.Req_Priority_Score__c = ((numerator/denominator).setScale(2))*100;
                    
                    System.debug('Inside Opportunity_OACalculator 9');
                }
                catch(Exception e)
                {
                    opp.Req_Priority_Score__c = 0;
                }
            }
            else{
                opp.Req_Priority_Score__c = null;
            }
        }
    }
    
    
    
    private static decimal getNumerator(string key, string opco,Map<string,OA_Calculator__c> mapOACalculator)
    {
        decimal numeratorWeight = 0;
        decimal scoreNumerator = 0;
        
        if(mapOACalculator.get(key) != null)
        {
            scoreNumerator = mapOACalculator.get(key).Score__c;
            
            if(opco == 'AG_EMEA')
            {
                numeratorWeight = mapOACalculator.get(key).EMEA_Weights__c;
            }
            else if(opco == 'TEKsystems, Inc.')
            {
                numeratorWeight = mapOACalculator.get(key).TEKSystems_Weights__c;
            }
            else if(opco == 'Aerotek, Inc')
            {
                numeratorWeight = mapOACalculator.get(key).Aerotek_Weights__c;
            }
        }
        
        decimal numerator = numeratorWeight * scoreNumerator;
        system.debug('numerator:::::'+numerator);
        return numerator;
    }
    
    private static decimal getdenominator(string key, string opco,Map<string,OA_Calculator__c> mapOACalculator)
    {
        decimal numeratorWeight = 0;
        decimal scoreDenominator = 0;
        
        if(mapOACalculator.get(key) != null)
        {
            scoreDenominator = mapOACalculator.get(key).Maximum_Possible_Score__c;
            
            if(opco == 'AG_EMEA')
            {
                numeratorWeight = mapOACalculator.get(key).EMEA_Weights__c;
            }
            else if(opco == 'TEKsystems, Inc.')
            {
                numeratorWeight = mapOACalculator.get(key).TEKSystems_Weights__c;
            }
            else if(opco == 'Aerotek, Inc')
            {
                numeratorWeight = mapOACalculator.get(key).Aerotek_Weights__c;
            }
        }
        
        decimal denominator = numeratorWeight * scoreDenominator;
        system.debug('denominator:::' + denominator);
        return denominator;
    }
    
    private static string getTotalSpreadStatus(decimal totalSpread, string opco)
    {
        string status = '';
        if(opco == 'AG_EMEA' ){
            if(totalSpread <= 5000)
                status = 'tier1';
            else if(totalSpread > 5000 && totalSpread <= 10000)
                status = 'tier2';
            else if(totalSpread > 10000 && totalSpread <= 20000)
                status = 'tier3';
            else if(totalSpread > 20000 && totalSpread <= 30000)
                status = 'tier4';
            else if(totalSpread > 30000)
                status = 'tier5';
        }else{
            if(totalSpread <= 5000)
                status = 'tier1';
            else if(totalSpread > 5000 && totalSpread <= 10000)
                status = 'tier2';
            else if(totalSpread > 10000 && totalSpread <= 15000)
                status = 'tier3';
            else if(totalSpread > 15000 && totalSpread <= 20000)
                status = 'tier4';
            else if(totalSpread > 20000 && totalSpread <= 25000)
                status = 'tier5';
            else if(totalSpread > 25000 && totalSpread <= 30000)
                status = 'tier6';
            else if(totalSpread > 30000 && totalSpread <= 35000)
                status = 'tier7';
            else if(totalSpread > 35000)
                status = 'tier8';
        }
        return status;
    }
    
    
    private static string getStatus(decimal totalStarts, decimal totalCurrents)
    {
        string status = '';
        if(totalStarts == 0)
        {
            status = 'Red';
        }
        else if(totalCurrents > 0)
        {
            status = 'Green';
        }
        else
        {
            status = 'Yellow';
        }
        return status;
    }
}