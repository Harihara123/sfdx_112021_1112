@isTest(seeAllData=true)
public class Opportunity_OACalculator_Test {
    public static testMethod void OA_Calculator_Test() {
        
	TestData TdAcc = new TestData(1);
    List<Account> lstNewAccounts = TdAcc.createAccounts();
	List<Contact> lstNewContacts = TdAcc.createContacts('Client');        
    List<Opportunity> lstOpportunity = new List<Opportunity>();
    String ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                
	
     for(Account Acc : TdAcc.createAccounts()) {          
        Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                         RecordTypeId = ReqRecordType, Formal_Decision_Criteria__c = 'Defined', Opco__c ='AG_EMEA',BusinessUnit__c ='EASI',
                        Response_Type__c = 'Formal RFx' , stagename = 'Interest', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined', Sales_Organization__c='Central',
                        Req_Product__c = 'Contract',ReqCompletition__c='Yellow',Req_Total_Spread__c=38000,                 
                                                     
                        Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',Services_GP__c = 20,CloseDate = system.today()+1,Req_Hiring_Manager__c=lstNewContacts[0].id);  
                        
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                }                
                // Insert list of Opportunity
                insert lstOpportunity;  
                lstOpportunity[0].Amount= 123;              
                lstOpportunity[0].stagename = 'Negotiation';                
                lstOpportunity[0].Describe_Customer_Pain__c ='ABCDE';
                lstOpportunity[0].Desired_Business_Outcomes__c ='ABCDE';
                lstOpportunity[0].How_will_we_win__c = 'ABCDE';
                lstOpportunity[0].What_is_our_Value_message__c = 'ABCDE';
                lstOpportunity[0].Pricing_Approved_By_SSG__c = TRUE;
                lstOpportunity[0].Document_Request_Date__c = system.today();
                lstOpportunity[0].Opco__c ='AG_EMEA';//'TEKsystems, Inc.';
                lstOpportunity[0].DeliveryModel__c= 'Project';
                lstOpportunity[0].Positioning_Profile_of_Comp_Landscape__c='We are the exclusive vendor';
                lstOpportunity[0].Delivery_Organization__c='test';
                lstOpportunity[0].Client_Relationship__c='Existing GS Client';
                lstOpportunity[0].BusinessUnit__c ='Aston Carter';
                lstOpportunity[0].Location__c='test';
                lstOpportunity[0].Project_Length_Duration_Weeks__c=43;
                lstOpportunity[0].Number_of_Resources__c=23;
                lstOpportunity[0].Global_Services_Opportunity_Type__c ='eee';
                lstOpportunity[0].Description='test';
               lstOpportunity[0].Start_Date__c=date.today();
        
        update lstOpportunity;
        
    }
}