public class OrderCreation implements Queueable {
    
    private List<Opportunity> newOpps;
    private Set<Id> accIds;
    
    public OrderCreation(List<Opportunity> newOpps, Set<Id> accIds) {
        this.newOpps = newOpps;
        this.accIds = accIds;
    }
    
    public void execute(QueueableContext context) {        
        
        Map<Id, Id> accTalentMap = new Map<Id, Id>();
        List<Order> lstOrder = new List<Order>();
        Order ord;        
        String submitalRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
        for(Contact c: [Select Id, AccountId From Contact Where AccountId IN :accIds]) {
            accTalentMap.put(c.AccountId, c.Id);
        }
        for(Opportunity o: newOpps) {                        
            ord = new Order();
            ord.AccountId=o.AccountId;
            ord.OpportunityId=o.Id;
            ord.Status='Submitted';
            ord.EffectiveDate = System.today();   
            ord.ShipToContactId = accTalentMap.get(o.Originating_Opportunity__r.AccountId);         
            ord.RecordTypeId = submitalRecTypeId;
            lstOrder.add(ord);             
        }
        if(!lstOrder.isEmpty()) {
            insert lstOrder;
        } 
    }    
}