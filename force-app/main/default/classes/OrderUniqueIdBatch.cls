public class OrderUniqueIdBatch implements Database.Batchable <sObject> {

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('Select Id,Unique_id__c,ShiptoContactId,OpportunityId,ATS_Job__c from Order Where Unique_id__c Like \'001%\' AND ATS_job__c = NULL');
    }

    public void execute(Database.BatchableContext BC, List<Order> scope) {
        List<Order> orders = new List<Order>();
        for (Order o : scope){
            o.Unique_id__c = String.valueOf(o.ShiptoContactId) + String.valueOf(o.OpportunityId);
            orders.add(o);
        }
        Database.Update(orders,false);
    }

    public void finish(Database.BatchableContext BC){
        // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('OrderUniqueId Updates ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
       
}