global class OrderWrapper implements Comparable {

    public Order order;
    
    // Constructor
    public OrderWrapper(Order o) {
        order = o;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        OrderWrapper compareToOrder = (OrderWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (order.effectiveDate < compareToOrder.order.effectiveDate) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (order.effectiveDate > compareToOrder.order.effectiveDate) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}