global class Outer_Set_Req_Origination_System implements Schedulable{

     //Call from Scheduled Job
     global void execute(SchedulableContext ctx)
     {  
     
         //Declare an object of the Batch Class Batch_RetryOutOfSyncRecords
         Batch_Set_Req_Origination_System SetReqRecords = new Batch_Set_Req_Origination_System();
         
         //Execute the Batch_RetryOutOfSyncRecords Batch, with 200 records per batch
         //Performs Start(), Excecute() and Finish() methods
         Database.executeBatch(SetReqRecords,2000);
                     
     }
     
}