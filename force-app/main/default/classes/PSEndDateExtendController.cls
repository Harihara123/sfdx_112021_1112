public class PSEndDateExtendController {
/******************************************************************************************************************************
Author          : Rajeesh Othenandiyil
Story			: S-58562 - Web Service Call for End Date Change
Purpose         : Call out to peoplesoft through SOA to request for position end date change.
				  Fire and forget. Process to update salesforce takes about 4 hours. Do not allow
				  subsequent firing until 4 hours once fired.
				  Data needed for the service call.
				  1. PSPositionId ex. 6                ------- Talent_Work_History__c.Position_Id__c - text
				  2. ESFId							   ------- Talent_Work_History__c.ESFId__c - text 
				  3. PSEmpId						   ------- Account.Peoplesoft_ID__c - text
														       Talent_Work_History__c.SourceId__c (has R. in it)-has more in it.
				  4. PSJobReqNumber					   ------- Talent_Work_History__c.JobRequisitionNumber__c - text
				  5. PartnerPersonId - RWS Candidate Id------- Talent_Work_History__c.RWS_Id__c - number
				  6. Candidate First name              ------- Contact fname
				  7. Candidate Last Name               ------- Contact lname
				  8. Candidate Middle Name             ------- Contact middle name
				  9. End Date                          ------- Talent_Work_History__c.End_Date__c
				  10. Business Rule 2 ex: ""           ------- not needed?
				  11. Security2 ex:""                  ------- not needed?
				  12."@ModifiedBy": "05329962"         ------- peoplesoft id of the running user.

Test Class      : PSEndDateExtendController_Test.cls
Date Created    : 03-29-2018
Modification log: 
******************************************************************************************************************************/
	public PSEndDateExtendController(){}
	
  	@AuraEnabled
	public static String sendPositionEndDateChangeRequest(Id twhId, String endDate){
		//ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues('Peoplesoft End Date Change POST');
         Map<String,String> serviceSettings = WebServiceSetting.getMulesoftOAuth('Peoplesoft End Date Change POST');
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
		String requestBody = getRequestBody(twhId,endDate);
		String responseString = '';
		
		String responseBody='';
		if(String.isBlank(requestBody)){
			System.debug('Request body is blank. Required fields may be missing. Request not sent to peoplesoft!');
			responseString='ERROR: Required fields missing. Request not sent to peoplesoft!';

		}
		else{

			req.setEndpoint(serviceSettings.get('endPointUrl'));
			req.setMethod('POST');
			req.setTimeout(5000);
		
			req.setBody(requestBody);
			req.setHeader('Content-Type', 'application/json');
			//req.setHeader('Authorization', 'Bearer ' + serviceSettings.OAuth_Token__c);
			req.setHeader('Authorization', 'Bearer '+serviceSettings.get('oToken'));
			
			try {
				HttpResponse response = connection.send(req);
				responseBody = response.getBody();
                system.debug('response status code >>'+response.getStatusCode());
				System.debug('responseBody >>>>>>>> ' + responseBody);
				/*
					Success response:
					
					{
						"EndDateAutomationResponse": {
							"@MessageCode": 0,
							"@Message": "The end date extesion request has been sent successfully.",
							"TEXT": "NULL"
						}
					}
					
					
					
					Validation Error: response
					{
						"EndDateAutomationResponse": {
							"@MessageCode": 102,
							"@Message": "Invalid input parameter: EsfID/CandidateID/PsJobReqNbr/PsEmplID=3215204/2228899/0005094953/05526633",
							"TEXT": "NULL"
						}
					}
				*/
				// Parse JSON response.
				JSONParser parser = JSON.createParser(response.getBody());
				String message='';
				Integer messageCode;//0 is success
				while (parser.nextToken() != null) {
					if (parser.getCurrentToken() == JSONToken.FIELD_NAME)
					{
						if(parser.getText() == '@MessageCode') {
							parser.nextToken();
							messageCode = parser.getIntegerValue();
							System.debug('Messagecode:'+messageCode);
						}
						if(parser.getText() == '@Message') {
							parser.nextToken();
							message = parser.getText();
							System.debug(': message'+message);
						}
				
					}
				}
				System.debug('Messagecode:'+messageCode+': message'+message);
				if(messageCode == 0){
					responseString = 'SUCCESS:'+message;
					//update sent date
					updateSentDate(twhId,Datetime.now());
				}
				else if(messageCode>0){
					responseString = 'VALIDATION ERROR:'+message;
				}
				else{
					responseString = 'EXCEPTION: Unexpected error happened.';
				}
				
			} catch (Exception ex) {
				// Callout to apigee failed
				System.debug(LoggingLevel.ERROR, 'Mulesoft call to Peoplesoft End Date Change failed! ' + ex.getMessage());
				responseString='EXCEPTION: Mulesoft call to Peoplesoft End Date Change failed!';
				//throw ex; 
			}
		}
		return responseString;
	}
	private static void updateSentDate(Id twhId, DateTime dt){
		//update the PS change end date time field.
		Talent_Work_History__c twh = new Talent_Work_History__c(Id=twhId,PS_last_change_req_date__c=dt);
		update twh;
	}
	public static String getRequestBody(Id reqId,String endDate){
		//System.debug('reqId'+reqId+': endDate'+endDate);
		Talent_Work_History__c twh = [Select id,ESFId__c,RWS_Id__c,JobRequisitionNumber__c,Position_Id__c,Talent__c,Talent__r.Peoplesoft_ID__c from Talent_Work_History__c where id =:reqId limit 1 ];
		//Anything else to add in where clause to limit talent contact record to 1.?
		Contact con = [Select id,AccountId, FirstName,LastName,MiddleName from Contact where AccountId = :twh.Talent__c limit 1];
		User user = [select id, Name, Peoplesoft_Id__c from User where id = :Userinfo.getUserId()];
		String reqBody = '';
		String empIdWithPrefix='';
		String empId='';
		String fstName = '';
		String midName='';
		Integer posId;
		fstName = String.isNotBlank(con.FirstName)?con.FirstName:'';
		midName = String.isNotBlank(con.MiddleName)?con.MiddleName:'';
		empIdWithPrefix = twh.Talent__r.Peoplesoft_ID__c;
		if(String.isNotBlank(empIdWithPrefix)){
			
            if(empIdWithPrefix.startsWith('R.')){
                empId = empIdWithPrefix.right(empIdWithPrefix.length()-2);
            }else{
                empId = empIdWithPrefix;
            }
		}
		if(String.isNotBlank(twh.Position_Id__c)){
			posId = Integer.valueOf(twh.Position_Id__c);
		}
		reqBody='{"EndDateAutomationRequest" :{"@PsPositionID":"'+posId
				+'","ESFForm":{"@ESFID":"'+twh.ESFId__c
				+'","@ModifiedBy":"'+user.Peoplesoft_Id__c
				+'","@PSEmplID":"'+empId
				+'","@PSJobReqNBR":"'+twh.JobRequisitionNumber__c
				+'","@PartnerPersonID":"'+twh.RWS_Id__c
				+'","ESFApplicant":{"@CandidateFirstName":"'+fstName
				+'","@CandidateLastName":"'+con.LastName
				+'","@CandidateMiddleName":"'+midName
				+'","@EndDate":"'+endDate
  				+'"}},"BusinessRule2":'+'""'
  				+', "Security2":'+'""'
  				+'}}';
  		System.debug('reqBody==>'+reqBody);
		return reqBody;
	}
}