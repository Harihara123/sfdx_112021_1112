@isTest
public class PSEndDateExtendController_Test {
    static testmethod void testSendPSEndDateChangeRequest(){
        Account a = new Account();
        a.name = 'Test Candidate';
        a.Peoplesoft_ID__c = '099999';
        a.Talent_Preference__c = '{"skills":"My Skills","goals":"My Goals","interests":"My interests","certifications":"My certs"}';
        insert a;
        //System.debug('created account---'+a.id);
        Id RecordTypeId = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();  
        Contact c = new Contact();
        c.accountId = a.Id;
        c.firstname = 'John';
        c.lastname = 'John';
        c.About_Me__c = 'Talent profile summary';
        c.Facebook_URL__c = 'facebook.com';
        c.LinkedIn_URL__c = 'linkedin.com';
        c.email = 'myemail@example.com';
        c.phone = '1234567890';
        c.title = 'my title';
        c.MailingCity = 'my city';
        c.MailingCountry = 'my country';
        c.MailingPostalCode = '60345';
        c.MailingState = 'NY';
        c.MailingStreet = 'My street';
        c.Peoplesoft_ID__c = '099999';
        c.RecordTypeId =RecordTypeId;
        insert c;
        //System.debug('created contact---'+c.id);
        Talent_Work_History__c  twh= new Talent_Work_History__c (Current_Assignment__c =true,End_Date__c=Date.today() + 4, Start_Date__c = Date.today() -4,Talent__c=a.id, uniqueId__c ='23049');
        insert twh;
        //System.debug('created twh---'+twh.id);
        //use admin, the record is not visible to single desk user.
        String profileName = 'System Administrator';//'Single Desk 1';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'testlst';
        testUser.Firstname = 'Testy';
        testUser.Alias = 'test';
        testUser.CommunityNickname = '12347';
        profile prof = [ select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
        insert testUser;
		//System.debug('created user---'+testUser.id);
        //create the custom setting
        insert new ApigeeOAuthSettings__c(name = 'Peoplesoft End Date Change POST',Client_Id__c='123',Service_Http_Method__c='POST',Service_URL__c='http://test.com',Token_URL__c='http://test1.com');
		insert new Mulesoft_OAuth_Settings__c(name='Peoplesoft End Date Change POST',ClientID__c='123',Client_Secret__c='hjkj',Resource__c='56565',Endpoint_URL__c='http://test.com',Token_URL__c='http://test1.com');		
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('test_psenddatechangeresponse');
       	
        Test.startTest();
       // Test.setMock(HttpCalloutMock.class, new WebServiceSetting_Test.OAuthMockResponse());
       // WebServiceSetting.getMulesoftOAuth('Peoplesoft End Date Change POST');
        Test.setMock(HttpCalloutMock.class, mock);
        String endDateStr = Date.newInstance(2050, 1, 1).format();
        System.runAs(testUser) {
				PSEndDateExtendController.sendPositionEndDateChangeRequest(twh.Id,endDateStr);
			}
        
        Test.stopTest();
    }
}