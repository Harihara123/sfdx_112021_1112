public class PhenomApplctnReconciliationController {
    @future(callout=true)
    public static void callFailedApllicationService(String opco_name, DateTime startDate, DateTime endDate) {
        //opco_name should have value like :Aerotek,TEKsystems,EASi,Aston Carter
 
        system.debug('opco_name:: '+opco_name);
        system.debug('startDate:: '+startDate);
        system.debug('endDate:: '+endDate);
        
        Integer size_128K_unquoted = (128 * 1024) - 2;
        String pattern = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
        Map<String,String> serviceSettings = new Map<String,String>();
        Phenom_Reconciliation_Request_Log__c requestLog = new Phenom_Reconciliation_Request_Log__c();
        
        requestLog.API_Name__c ='FailedApplication';
        requestLog.OPCO__c = opco_name;
        requestLog.StartDate__c = startDate;
        requestLog.EndDate__c = endDate;
        
        Phenom_Authorization__mdt phenomClient = [select Client_ID__c, OPCO__c 
                                                  from Phenom_Authorization__mdt 
                                                  where OPCO__c=: opco_name.toUpperCase() limit 1];
        
        //MuleSoftOAuth_Connector.OAuthWrapper serviceSettings = MuleSoftOAuth_Connector.getAccessToken('Phenom Failed Job Application');
        if(!Test.isRunningTest()){
        	serviceSettings = WebServiceSetting.getMulesoftOAuth('Phenom Failed Job Application');
            if(serviceSettings.size() == 0){
                PhenomApplctnReconciliationController.sendEmailMethod('OAuth Token Failure', 
                                                                              '', 
                                                                              opco_name);
            }
        }else{
            serviceSettings.put('oToken','12356');
            serviceSettings.put('endPointUrl','https://api-tst.allegistest.com/v1/failedApplication');
        }
        
        
        String body = '{"lastUpdatedStartDateTime":"'+startDate.format(pattern, 'UTC')+'","lastUpdatedEndDateTime":"'+endDate.format(pattern, 'UTC')+'","common":{"clientID":"'+phenomClient.Client_ID__c+'"}}';
        system.debug('RequestBody->'+body);
         
            Http httpCon = new Http();
            HttpRequest req = new HttpRequest();
            HttpResponse resp = new HttpResponse();
            
            req.setEndpoint(serviceSettings.get('endPointUrl'));
            req.setMethod('POST');
            
            req.setHeader('Authorization', 'Bearer '+serviceSettings.get('oToken'));
            //req.setHeader('x-opco', opco_name);
            //req.setHeader('x-transaction-id',String.valueOf(system.now()));
            req.setHeader('Content-Type', 'application/json');
            req.setBody(body);
        	requestLog.Request_Body__c = body;
            try {
                    resp = httpCon.send(req);
                    if(resp.getBody().length() <= (size_128K_unquoted)){
                        requestLog.API_Response__c = resp.getBody();
                    }else{
                        requestLog.API_Response__c = resp.getBody().substring(0, size_128K_unquoted);
                    }
                    	
            
                	system.debug('Response-->'+ resp.getBody());
                    if (resp.getStatusCode() == 200) {
                        PhenomFailedApplicationWrapper.ResponseClass responseObject = PhenomFailedApplicationWrapper.parse(resp.getBody());
                        processResponseData(responseObject.data,opco_name);
                       	
                        requestLog.Status__c= 'Request Processed';
                       	requestLog.Total_Records__c = String.valueOf(responseObject.data.size());
                    } 
                    else {
                        requestLog.Message__c = resp.getStatusCode()+'-'+resp.getBody();
                        requestLog.Status__c ='Request Failed';
                        
                        ConnectedLog.LogInformation('PhenomApplctnReconciliationController', 
                                                    'callFailedApllicationService', 
                                                    resp.getStatusCode()+'-'+resp.getBody());
                        
                        PhenomApplctnReconciliationController.sendEmailMethod(resp.getStatusCode()+'-'+resp.getBody(), 
                                                                              body, 
                                                                              opco_name);
                        
                    }
                	insert requestLog;
                } catch(CalloutException exp) {
                    System.debug(exp);
                    requestLog.Message__c = exp.getMessage()+' - Line No:'+exp.getLineNumber();
                    requestLog.Status__c ='Request Failed';
                    insert requestLog;
                    PhenomApplctnReconciliationController.sendEmailMethod(exp.getMessage(), 
                                                                              body, 
                                                                              opco_name);
                    ConnectedLog.LogException('PhenomApplctnReconciliationController', 'callFailedApllicationService', exp);
                        
                }    
      
        
        
    }
    public static void processResponseData(List<PhenomFailedApplicationWrapper.Data> responseList, String opco_name){
        try{
            Set<String> applicationIDs = new Set<String>();
            Map<String,PhenomFailedApplicationWrapper.Data> applicationResponseMap = new Map<String,PhenomFailedApplicationWrapper.Data>();
            List<Careersite_Application_Details__c> listToBeUpserted = new List<Careersite_Application_Details__c>();
            List<Retry_Summary_Detail__c> retrySummaryList = new List<Retry_Summary_Detail__c>();
            Set<String> vApplicationId = new Set<String>(); // to avoid duplicate records.
            // insert records in Application Detail  Staging Object
            for(PhenomFailedApplicationWrapper.Data obj : responseList){
                  if(!vApplicationId.contains(obj.applicationId)){
                        vApplicationId.add(obj.applicationId);
                        Careersite_Application_Details__c detailObject = new Careersite_Application_Details__c();
                        applicationResponseMap.put(obj.applicationId,obj);
                        Datetime adt = system.now();
                        
                        if(obj.applicationDateTime != ''){ 
                            adt = (DateTime)JSON.deserialize('"' + obj.applicationDateTime + '"', DateTime.class);    
                        }
                        
                        detailObject.OPCO__c = opco_name;
                        detailObject.Application_Source__c ='Phenom';
                        detailObject.Application_Date_Time__c = adt;  
                        detailObject.Adobe_Ecvid__c = obj.additionalData.AdobeEcvid;
                        detailObject.Posting_Id__c = obj.jobId;
                        detailObject.Vendor_Application_Id__c = obj.applicationId;
                        detailObject.Message__c = 'FAILURE';
                       	detailObject.Application_Status__c= obj.applicationStatus;
                        if(obj.failedToReprocess.equalsIgnoreCase('true')){
                            detailObject.Retry_Status__c ='PENDING';
                        }else{
                            detailObject.Retry_Status__c ='INELIGIBLE';
                        }
                        
                        listToBeUpserted.add(detailObject);
                }
            }
            
            if(listToBeUpserted.size()>0){
                upsert listToBeUpserted Vendor_Application_Id__c;
                
                // below logic to insert record in related retry summary record
                for(Careersite_Application_Details__c ad: listToBeUpserted){
                    
                    PhenomFailedApplicationWrapper.Data wrapObject = applicationResponseMap.get(ad.Vendor_Application_Id__c);
   					Datetime rdt = system.now();
                    if(wrapObject.lastRetryDateTime != '' && wrapObject.lastRetryDateTime != null){
                		rdt = (DateTime)JSON.deserialize('"' + wrapObject.lastRetryDateTime + '"', DateTime.class);    
                	}					                 
                    Retry_Summary_Detail__c retrySummaryObject = new Retry_Summary_Detail__c();
                    retrySummaryObject.Application__c =  ad.Id;
                    retrySummaryObject.Transaction_ID__c = wrapObject.transactionId;
                    retrySummaryObject.Failed_to_Reprocess__c = wrapObject.failedToReprocess;
                    retrySummaryObject.Message__c = wrapObject.applicationStatus;
                    retrySummaryObject.Last_Retry_DateTime__c = rdt; 
                    retrySummaryList.add(retrySummaryObject);
                }
                upsert retrySummaryList Transaction_ID__c;
            }
        }Catch(Exception e){
            System.debug(e);
            ConnectedLog.LogException('PhenomApplctnReconciliationController', 'processResponseData', e);
        }
        
    }
    
    public static void sendEmailMethod(String strErrorMessage, String requestBody, String opco){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                List<String> toAddresses = new List<String>();
				toAddresses = Label.Phenom_Error_Notification.split(',');
				system.debug(toAddresses);
                mail.setToAddresses(toAddresses); 
                String strEmailBody;
                              
                mail.setSubject('Phenom Reconciliation failedApplications Service Failure for '+opco); 
              
                //Prepare the body of the email
                
                strEmailBody  ='Hi Team,\n\nPlease find Service failure details:\n\n ';
                strEmailBody +='RequestBody: '+requestBody+'\n\n';
                strEmailBody +='Message: '+ strErrorMessage;                
               
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
    
}