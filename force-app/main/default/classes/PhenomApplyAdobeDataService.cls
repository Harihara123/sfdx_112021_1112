@RestResource(urlMapping='/Person/PhenomApplyAdobeData/V1/*')
// URL:  /services/apexrest/Person/PhenomApplyAdobeData/V1/*
/*
 	{
      "applicationData": [
    		{
                "AdobeECVID": " ",
                "opco" ",
                "jobId": "",
                "transactionTime": " "
            }
        ]
	}
*/
global class PhenomApplyAdobeDataService {
    
    @HttpPost
    global static void doPost() {
        String responseMessage ='';
		String errorMessage ='The following mandatory field(s) are not supplied in the input request:';
        RestRequest req = RestContext.request;        
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        Boolean validRequest = true;
        Integer improperRecordCount =0;
        try{
            Blob body = req.requestBody;
            String requestJSONBody = body.toString();
            
            List<Careersite_Application_Details__c> listToBeUpserted = new List<Careersite_Application_Details__c>();
            PhenomFailedApplicationWrapper.WCSResponseClass wrapObject = PhenomFailedApplicationWrapper.parseWCSApplication(requestJSONBody);
            if(wrapObject.applicationData.size()>0){
                for(PhenomFailedApplicationWrapper.WCS_ApplicationData obj : wrapObject.applicationData){
                    if(obj.AdobeECVID == '' || obj.AdobeECVID == null ){
                        validRequest=false;
                        improperRecordCount++;
                        
                    }else{
                        DateTime adt = system.now();
                        if(obj.transactionTime != null && obj.transactionTime != ''){
                            adt = (DateTime)JSON.deserialize('"' + obj.transactionTime + '"', DateTime.class);
                        }
                        Careersite_Application_Details__c detailObject = new Careersite_Application_Details__c();
                        detailObject.OPCO__c = obj.opco;
                        detailObject.Application_Source__c ='WCS';
                        detailObject.Application_Date_Time__c = adt;  
                        detailObject.Adobe_Ecvid__c = obj.AdobeECVID;
                        detailObject.Posting_Id__c = 'JP-'+obj.jobId;
                        detailObject.Vendor_Application_Id__c = obj.AdobeECVID+'-'+obj.jobId;
                        detailObject.Message__c = 'SUCCESS';
                        detailObject.Retry_Status__c ='INELIGIBLE';
                        listToBeUpserted.add(detailObject);
                    }
                }
                
                if(listToBeUpserted.size()>0){
                    upsert listToBeUpserted Vendor_Application_Id__c;
                }
            }
            if(validRequest){
            	responseMessage ='{"Message":"All applications received by Connected"}';    
            }else if(improperRecordCount != wrapObject.applicationData.size()){
				responseMessage = '{"Message":"Records with missing Adobe ECVID not accepted. Remaining applications Received by Connected"}'; 
            }else{
                responseMessage = '{"Message":"Records with missing Adobe ECVID not accepted."}'; 
            }
            res.statusCode = 200;
            res.responseBody = Blob.valueOf(responseMessage);        	
        }catch(Exception e){
            system.debug('Exception::'+e.getMessage());
			res.statusCode = 400;
			responseMessage = '{"Message":"The payload could not be parsed. Exception message: '+e.getMessage()+'"}';
            res.responseBody = Blob.valueOf(responseMessage);
			ConnectedLog.LogException('PhenomApplyAdobeDataService', 'doPost', e);
        }
        
    }
    
}