@isTest
public class PhenomApplyAdobeDataServiceTest {

	static testMethod void  testMethod1(){
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{'
      								+'"applicationData": [{'
                					+'"AdobeECVID":"1234",'
                					+'"opco":"Aerotek",'
                					+'"jobId":"5689",'
                					+'"tansactionTime":"2020-04-04T05:45:47Z"'
           							+' },{'
                					+'"AdobeECVID":"",'
                					+'"opco":"",'
                					+'"jobId":"",'
                					+'"tansactionTime":"2020-04-04T05:45:47Z"'
            						+'}]}}';
            
            req.requestURI = '/services/apexrest/Person/PhenomApplyAdobeData/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            PhenomApplyAdobeDataService.doPost();
       }
    }
    static testMethod void  testMethod2(){
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{'
      								+'"applicationData": [{'
                					+'"AdobeECVID":"1234",'
                					+'"opco":"",'
                					+'"jobId":"",'
                					+'"tansactionTime":" "'
           							+' },{'
                					+'"AdobeECVID":"2456",'
                					+'"opco":"",'
                					+'"jobId":"",'
                					+'"tansactionTime":" "'
            						+'}]}}';
            
            req.requestURI = '/services/apexrest/Person/PhenomApplyAdobeData/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            PhenomApplyAdobeDataService.doPost();
       }
    }
    static testMethod void  testMethod3(){
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{'
      								+'"applicationData": [{'
                					+'"AdobeECVID":"1234",,'
                					+'"opco":"",'
                					+'"jaobId":"",'
                					+'"tansactionTime":" "'
           							+' },{'
                					+'"AdobeECVID":"",'
                					+'"opco":"",'
                					+'"jobId":"",'
                					+'"tansactionTime":" "'
            						+'}]}}';
            
            req.requestURI = '/services/apexrest/Person/PhenomApplyAdobeData/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            PhenomApplyAdobeDataService.doPost();
       }
    }
    static testMethod void  testMethod4(){
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{'
      								+'"applicationData": [{'
                					+'"AdobeECVID":"",'
                					+'"opco":"",'
                					+'"jobId":"",'
                					+'"tansactionTime":" "'
           							+' },{'
                					+'"AdobeECVID":"",'
                					+'"opco":"",'
                					+'"jobId":"",'
                					+'"tansactionTime":" "'
            						+'}]}}';
            
            req.requestURI = '/services/apexrest/Person/PhenomApplyAdobeData/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            PhenomApplyAdobeDataService.doPost();
       }
    }
}