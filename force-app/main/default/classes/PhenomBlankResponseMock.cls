global class PhenomBlankResponseMock implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {
        String responseString ='{'
        						+'"totalRecords": "0",'
        						+'"noOfPages":"0",'
        						+'"currentPage":"null",'
        						+'"data":[]'
								+'}';
		
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(responseString);
        res.setStatusCode(200);
        return res;
    }

}