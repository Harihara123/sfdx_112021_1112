@isTest
global class PhenomFailedAppExcpMockResponse implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {
        String responseString ='{"message": "Invalid Opco" }';
                             
		
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(responseString);
        res.setStatusCode(400);
        return res;
    }
}