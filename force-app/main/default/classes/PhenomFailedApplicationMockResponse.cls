@isTest
global class PhenomFailedApplicationMockResponse implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {
        String responseString ='{"data":['
                                  +'{'
                                  +'  "applicationId": "JA2H1YB7187QBTGGXFN3",'
                                  +'  "transactionId": "123456",'
                                  +'  "jobId": "6959608",'
                                  +'  "applicationStatus": "Internal server error",'
                                  +'  "applicationDateTime": "2019-12-10T16:47:09Z",'
                                  +'  "failedToReprocess": "false",'
                                  +'  "lastRetryDateTime": "2019-12-12T16:47:09Z",'
                                  +'  "additionalData": {'
                                  +'    "AdobeEcvid": "434"'
                                  +'  }'
                                  +'}'
            +']}';
		
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(responseString);
        res.setStatusCode(200);
        return res;
    }
}