public class PhenomFailedApplicationWrapper {
	/*
	 old wrapper---
	{ 
        "data":[{
            "applicationId": "", // Phenom job application Id.
            "transactionId":"",
            "jobId":"",
        "failureStatus":"",
        "applicationDateTime":"",
        "failedToReprocess":"",
        "lastRetryDateTime":"",
        "additionalData": // This additional data supports customer specific
        attributes and these are optional
        {
            "AdobeEcvid": "",
            }
		}]
	}
	new wrapper-------
	{
        "totalRecords": "", // total records count
        "noOfPages":"50", // no of pages
        "currentPage":"", //pageNumber of current result
        "data": [{
            "applicationId": "", // Phenom job application Id.
            "transactionId":"",
            "jobId":"",
            "applicationStatus":"",
            "applicationDateTime":"",
            "failedToReprocess":"",
            "lastRetryDateTime":"",
            "additionalData": 
            {
            "AdobeEcvid": "",
            }
        }]
	}
-------------------------
WCS Application Body:
{
"applicationData": [

		{
			"AdobeECVID": " ",
			"opco": " ",
			"jobId": "",
			"transactionTime": " "
		}
	]
}



*/
    public class ResponseClass {
		public String totalRecords;
		public String noOfPages;
		public String currentPage;
		public List<Data> data;
	}

	public class Data {
		public String applicationId;
		public String transactionId;
		public String jobId;
		public String failureStatus;
        public String applicationStatus;
		public String applicationDateTime;
		public String failedToReprocess;
		public String lastRetryDateTime;
		public AdditionalData additionalData;
	}
	
    public class AdditionalData {
		public String AdobeEcvid;
	}
    public class WCSResponseClass{
        public List<WCS_ApplicationData> applicationData;// for WCS Service
    }
	public class WCS_ApplicationData {
		public String AdobeECVID;
		public String opco;
		public String jobId;
		public String transactionTime;
	}
    public static PhenomFailedApplicationWrapper.ResponseClass parse(String json) {
		return (PhenomFailedApplicationWrapper.ResponseClass) System.JSON.deserialize(json, PhenomFailedApplicationWrapper.ResponseClass.class);
	}
    
    public static PhenomFailedApplicationWrapper.WCSResponseClass parseWCSApplication(String json) {
		return (PhenomFailedApplicationWrapper.WCSResponseClass) System.JSON.deserialize(json, PhenomFailedApplicationWrapper.WCSResponseClass.class);
	}
}