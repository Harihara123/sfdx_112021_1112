@isTest
global class PhenomGetApplicationExceptionMock implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {
        String responseString ='{'
        						+'"totalRecords": "200",'
        						+'"noOfPages":"10",'
        						+'"currentPage":"1",'
        						+'"daata": [{'
            					+'"applicationId": "54321",'
            					+'"transactionId":"12345",'
            					+'"jobId":"2424",'
            					+'"applicationStatus":"failure:504",'
            					+'"applicationDateTime":"2020-04-03T10:31:26Z",'
            					+'"failedToReprocess":"true",'
            					+'"lastRetryDateTime":"2020-04-03T10:31:26Z",'
            					+'"additionalData":' 
            					+'{"AdobeEcvid": "avcid"}'
						        +'}'
            					+']'
								+'}';
		
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(responseString);
        res.setStatusCode(200);
        return res;
    }
}