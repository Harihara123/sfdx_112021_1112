@isTest
public class PhenomGetApplication_v2_test {
public static testMethod void getAccessTokenTest1(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Get Job Application Data',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                            Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
            insert serviceSettings;
        Test.startTest();
             
            //Test.setMock(HttpCalloutMock.class, new WebServiceSetting_Test.OAuthMockResponse());
            Test.setMock(HttpCalloutMock.class, new PhenomGetApplicationMockResponse());
    		PhenomReconciliationGetApplication_v2.ParamWRapper wrap = new PhenomReconciliationGetApplication_v2.ParamWRapper();
			wrap.opco_name ='Aerotek';
			wrap.startDate = system.now().addHours(-3);
			wrap.endDate = system.now(); 
			wrap.status = '';
			wrap.pageNumber=1;
			PhenomReconciliationGetApplication_v2.callApi(wrap);        
    
        Test.stopTest();
        
    }
    public static testMethod void getAccessTokenTest2(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Get Job Application Data',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                            Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
         
            Test.setMock(HttpCalloutMock.class, new PhenomFailedAppExcpMockResponse());
            PhenomReconciliationGetApplication_v2.ParamWRapper wrap = new PhenomReconciliationGetApplication_v2.ParamWRapper();
			wrap.opco_name ='Aerotek';
			wrap.startDate = system.now().addHours(-3);
			wrap.endDate = system.now();
			wrap.status = 'FAILURE';
			wrap.pageNumber=1;
			PhenomReconciliationGetApplication_v2.callApi(wrap);
            
        Test.stopTest();
    }
    
    public static testMethod void getAccessTokenTest3(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Get Job Application Data',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                            Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
         
            Test.setMock(HttpCalloutMock.class, new PhenomGetApplicationExceptionMock());
            PhenomReconciliationGetApplication_v2.ParamWRapper wrap = new PhenomReconciliationGetApplication_v2.ParamWRapper();
			wrap.opco_name ='Aerotek';
			wrap.startDate = system.now().addHours(-3);
			wrap.endDate = system.now();
			wrap.status = '';
			wrap.pageNumber=1;
			PhenomReconciliationGetApplication_v2.callApi(wrap);
            
        Test.stopTest();
    }
	
    public static testMethod void getAccessTokenTest4(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Get Job Application Data',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                            Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
         
            Test.setMock(HttpCalloutMock.class, new PhenomBlankResponseMock());
            PhenomReconciliationGetApplication_v2.ParamWRapper wrap = new PhenomReconciliationGetApplication_v2.ParamWRapper();
			wrap.opco_name ='Aerotek';
			wrap.startDate = system.now().addHours(-3);
			wrap.endDate = system.now();
			wrap.status = '';
			wrap.pageNumber=1;
			PhenomReconciliationGetApplication_v2.callApi(wrap);
            
        Test.stopTest();
    }
    public static testMethod void getAccessTokenTest5(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Get Job Application Data',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                            Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
         
            Test.setMock(HttpCalloutMock.class, new PhenomGetApplicationDataIssueMock());
            PhenomReconciliationGetApplication_v2.ParamWRapper wrap = new PhenomReconciliationGetApplication_v2.ParamWRapper();
			wrap.opco_name ='Aerotek';
			wrap.startDate = system.now().addHours(-3);
			wrap.endDate = system.now();
			wrap.status = '';
			wrap.pageNumber=1;
			PhenomReconciliationGetApplication_v2.callApi(wrap);
            
        Test.stopTest();
    }
    
    public static testMethod void getAccessTokenTest6(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Get Job Application Data',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                            Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
         
            Test.setMock(HttpCalloutMock.class, new PhenomFailedAppExcpMockResponse());
            PhenomReconciliationGetApplication_v2.ParamWRapper wrap = new PhenomReconciliationGetApplication_v2.ParamWRapper();
			wrap.opco_name ='Aerotek';
			wrap.startDate = system.now().addHours(-3);
			wrap.endDate = system.now();
			wrap.status = '';
			wrap.pageNumber=1;
			PhenomReconciliationGetApplication_v2.callApi(wrap);
            
        Test.stopTest();
    }
    
    public static testMethod void getAccessTokenTest7(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Get Job Application Data',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                            Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
         
            Test.setMock(HttpCalloutMock.class, new PhenomGetApplicationMockResponse());
            Phenom_Reconciliation_Request_Log__c plog = new Phenom_Reconciliation_Request_Log__c(); 
			plog.OPCO__c='Aerotek';		
        	plog.StartDate__c= system.now().addHours(-3);
			plog.EndDate__c= system.now();
			plog.Processed_Page_Number__c = 2;
			insert plog;
        List<Id> ids = new List<Id>();
        ids.add(plog.id);
        PhenomReconciliationGetApplication_v2.invokeGetJobApplication(ids);
            
        Test.stopTest();
    }
}