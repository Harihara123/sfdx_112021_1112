public class PhenomReconciliationGetApplication_v2{

@InvocableMethod(label='Get Application Data')
    public static void invokeGetJobApplication(List<Id> ids) {
        
         	Phenom_Reconciliation_Request_Log__c plog=[Select id,StartDate__c,Ignore_Slot__c,Status__c,
                                                             		EndDate__c,Processed_Page_Number__c,OPCO__c 
                                                             		from Phenom_Reconciliation_Request_Log__c
                                                             		where id=:ids[0]];
            
            PhenomReconciliationGetApplication_v2.ParamWrapper wrapObject = new PhenomReconciliationGetApplication_v2.ParamWrapper();
            wrapObject.opco_name =plog.OPCO__c;
			wrapObject.startDate =plog.StartDate__c;
			wrapObject.endDate = plog.EndDate__c;
			wrapObject.status = '';
			wrapObject.pageNumber=Integer.valueOf(plog.Processed_Page_Number__c);
            
            ID jobID = System.enqueueJob(new QueueablePhenomGetApplication(wrapObject));
	}    
    
    
public static void callApi(ParamWRapper wrapObject){
    ID jobID = System.enqueueJob(new QueueablePhenomGetApplication(wrapObject));
}

    
public class QueueablePhenomGetApplication implements Queueable, Database.AllowsCallouts{
       	public ParamWRapper paramObject; 
       	public QueueablePhenomGetApplication(ParamWRapper wrapObject){
                    this.paramObject = wrapObject;
        }
       public void execute(QueueableContext QC){
                system.debug('opco_name:: '+paramObject.opco_name);
                system.debug('startDate:: '+paramObject.startDate);
                system.debug('endDate:: '+paramObject.endDate);
                system.debug('status:: '+paramObject.status);
                system.debug('pageNumber:: '+paramObject.pageNumber);
                                                         
                Integer size_128K_unquoted = (128 * 1024) - 2;
                String pattern = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
               
                ResponseWrapper responseWrapObject = new ResponseWrapper();
                Phenom_Reconciliation_Request_Log__c requestLog = new Phenom_Reconciliation_Request_Log__c();
                List<PhenomFailedApplicationWrapper.Data> dataList = new List<PhenomFailedApplicationWrapper.Data>();
                PhenomFailedApplicationWrapper.ResponseClass responseObject = new PhenomFailedApplicationWrapper.ResponseClass();
                Boolean splitCallout = false;
                Integer callOutCounter = 1;
                try{
                    requestLog.API_Name__c ='GetApplication';
                    requestLog.OPCO__c = paramObject.opco_name;
                    requestLog.StartDate__c = paramObject.startDate;
                    requestLog.EndDate__c = paramObject.endDate;
                    
                    Phenom_Authorization__mdt phenomClient = [select Client_ID__c, OPCO__c 
                                                              from Phenom_Authorization__mdt 
                                                              where OPCO__c=: paramObject.opco_name.toUpperCase() 
                                                              limit 1];
                    MuleSoftOAuth_Connector.OAuthWrapper serviceSettings = MuleSoftOAuth_Connector.getAccessToken('Phenom Get Job Application Data');
                    
                   responseWrapObject = callService(prepareJsonBody(paramObject.startDate.format(pattern,'UTC'),paramObject.endDate.format(pattern,'UTC'),
                                                               phenomClient.Client_ID__c,String.valueOf(paramObject.pageNumber),
                                                               paramObject.status),
                                                      serviceSettings.endPointURL,
                                                      serviceSettings.token,paramObject.opco_name);
                    
                    requestLog.Processed_Page_Number__c = paramObject.pageNumber;
                    
                    if(responseWrapObject.responseString != '' && responseWrapObject.responseString != null){
                        
                        responseObject = PhenomFailedApplicationWrapper.parse(responseWrapObject.responseString );
                        //system.debug('Initial Response-->'+responseWrapObject.responseString);
                        if(responseObject.data.size()>0 && responseObject.noOfPages !='0'){
                            
                                dataList.addAll(responseObject.data);
                                if(responseObject.noOfPages !='1'){
                                for(Integer i=paramObject.pageNumber+1;i<=Integer.valueOf(responseObject.noOfPages);i++){
                                    requestLog.Processed_Page_Number__c = i;
                                    //system.debug('Counter---> '+callOutCounter+' PageNumber--> '+i);
                                    if(callOutCounter < 25){
                                            callOutCounter++;
                                            responseObject = new PhenomFailedApplicationWrapper.ResponseClass();
                                            responseWrapObject = new ResponseWrapper();
                                            responseWrapObject = callService(prepareJsonBody(paramObject.startDate.format(pattern,'UTC'),paramObject.endDate.format(pattern,'UTC'),
                                                                                   phenomClient.Client_ID__c,String.valueOf(i),
                                                                                   paramObject.status),
                                                                  serviceSettings.endPointURL,
                                                                  serviceSettings.token,paramObject.opco_name); 
                                        if(responseWrapObject.responseString != '' && responseWrapObject.responseString != null){
                                            responseObject = PhenomFailedApplicationWrapper.parse(responseWrapObject.responseString );
                                            dataList.addAll(responseObject.data);    
                                        }else{
                                            requestLog.Message__c = responseWrapObject.message;
                                            requestLog.API_Response__c = responseWrapObject.apiResponse;
                                            requestLog.Request_Body__c = responseWrapObject.requestString;
                                            requestLog.Status__c ='Request Failed';
                                            dataList = new List<PhenomFailedApplicationWrapper.Data>();
                                            break;
                                        }
                                        
                                    }else{
                                        splitCallout = true;
                                        break;
                                    }
                                    
                                }
                            }
                            if(dataList.size()>0){
                                processResponseData(dataList,paramObject.opco_name);
                                requestLog.Message__c ='SUCCESS';
                                if(splitCallout){
                                    requestLog.Status__c='Request Partially Processed';
                                 }else{
                                    requestLog.Status__c= 'Request Processed';
                                 }
                                
                                requestLog.Request_Body__c = responseWrapObject.requestString;
                                requestLog.Total_Records__c = String.valueOf(dataList.size());
                                if(responseWrapObject.apiResponse.length() <= (size_128K_unquoted)){
                                    requestLog.API_Response__c = responseWrapObject.apiResponse;
                                }else{
                                    requestLog.API_Response__c = responseWrapObject.apiResponse.substring(0, size_128K_unquoted);
                                }
                            }
                        }else{// empty response
                            requestLog.Message__c ='SUCCESS';
                            requestLog.Total_Records__c = '0';
                            requestLog.Status__c= 'Request Processed';
                            requestLog.API_Response__c = responseWrapObject.apiResponse;
                            requestLog.Request_Body__c = responseWrapObject.requestString;
                        }
                        
                        
                    }else{
                        requestLog.Message__c = responseWrapObject.message;
                        requestLog.API_Response__c = responseWrapObject.apiResponse;
                        requestLog.Request_Body__c = responseWrapObject.requestString;
                        requestLog.Status__c ='Request Failed';
                    }    
                    insert requestLog;
                }Catch(Exception e){
                    system.debug(e.getStackTraceString());
                    requestLog.Message__c = e.getMessage()+' - Line No: '+e.getLineNumber();
                    requestLog.API_Response__c = responseWrapObject.apiResponse;
                    requestLog.Request_Body__c = responseWrapObject.requestString;
                    requestLog.Status__c ='Request Failed';            
                    insert requestLog;
                    ConnectedLog.LogException('PhenomReconciliationGetApplication_v2', 'getJobApplicationData', e);
                }
            }
            public string prepareJsonBody(String startDateTime,String endDateTime,String clientId,String page, String status){
            String body;
            if(status ==''){
                body = '{"lastUpdatedStartDateTime":"'+ startDateTime+ '",'
                            +'"lastUpdatedEndDateTime":"'+endDateTime + '",'
                            +'"page":"'+page+'",'
                            +'"common":{"clientID":"'+clientId+'"} }';
               
            }else{
                body = '{"lastUpdatedStartDateTime":"'+ startDateTime+'",'
                            +'"lastUpdatedEndDateTime":"'+ endDateTime +'",'
                            +'"page":"'+page+'",'
                            +'"submissionStatus":"'+status.toUpperCase()+'",'
                            +'"common":{"clientID":"'+clientId+'"} }';
            }
            return body;
        }
        public ResponseWrapper callService(String body, String endpoint, String token, String opco_name ){
            String responseString='';
            ResponseWrapper responseObject = new ResponseWrapper();
            
            Http httpCon = new Http();
            HttpRequest req = new HttpRequest();
            HttpResponse resp = new HttpResponse();
            
            req.setEndpoint(endpoint);
            req.setMethod('POST');
            
            req.setHeader('Authorization', 'Bearer '+token);
            //req.setHeader('x-opco', opco_name);
            //req.setHeader('x-transaction-id',String.valueOf(system.now()));
            req.setHeader('Content-Type', 'application/json');
            req.setBody(body);
            system.debug('RequestBody-->'+body);
            responseObject.requestString =body; 
            try {	
                    resp = httpCon.send(req);
                    responseObject.apiResponse = resp.getBody();
                    system.debug('ResponseBody-->'+ resp.getBody());
                    //system.debug('ResponseStatusCode-->'+ resp.getStatusCode());
                    if (resp.getStatusCode() == 200) {
                      responseObject.responseString =resp.getBody();
                    } 
                    else {
                        ConnectedLog.LogInformation('PhenomReconciliationGetApplication_v2', 
                                                    'callService', 
                                                    resp.getStatusCode()+'-'+resp.getBody());
                        sendEmailMethod(resp.getStatusCode()+'-'+resp.getBody(),body,opco_name,0);
                        responseObject.Message = resp.getStatusCode()+'-'+resp.getBody();
                        
                    }
                
                    return responseObject;
                } catch(CalloutException exp) {
                    
                    System.debug(exp);
                    sendEmailMethod(exp.getMessage(),body,opco_name,0);
                    ConnectedLog.LogException('PhenomReconciliationGetApplication_v2', 'callService', exp);
                    responseObject.Message=exp.getMessage()+' -Line No: '+exp.getLineNumber();
                    responseObject.requestString =body;
                    return responseObject;        
                }
            
        }
        public void processResponseData(List<PhenomFailedApplicationWrapper.Data> responseList, String opco_name){
            try{
                //system.debug('Inside processResponseData Opco-->'+opco_name);
                //system.debug('responseList-->'+responseList);
                Integer failedApplicationCount =0;
                Set<String> applicationIDs = new Set<String>();
                Map<String,PhenomFailedApplicationWrapper.Data> applicationResponseMap = new Map<String,PhenomFailedApplicationWrapper.Data>();
                List<Careersite_Application_Details__c> listToBeUpserted = new List<Careersite_Application_Details__c>();
                List<Retry_Summary_Detail__c> retrySummaryList = new List<Retry_Summary_Detail__c>();
                Set<String> vApplicationId = new Set<String>(); // if same list has duplicate records
                // insert records in Application Detail  Staging Object
                for(PhenomFailedApplicationWrapper.Data obj : responseList){
                    if(!vApplicationId.contains(obj.applicationId)){
                        vApplicationId.add(obj.applicationId);
                        Careersite_Application_Details__c detailObject = new Careersite_Application_Details__c();
                        applicationResponseMap.put(obj.applicationId,obj);
                        Datetime adt = system.now();
                        
                        if(obj.applicationDateTime != ''){
                            adt = (DateTime)JSON.deserialize('"' + obj.applicationDateTime + '"', DateTime.class);    
                        }
                        detailObject.OPCO__c = opco_name;
                        detailObject.Application_Source__c ='Phenom';
                        detailObject.Application_Date_Time__c = adt;  
                        detailObject.Adobe_Ecvid__c = obj.additionalData.AdobeEcvid;
                        detailObject.Posting_Id__c = obj.jobId;
                        detailObject.Vendor_Application_Id__c = obj.applicationId;
                        detailObject.Application_Status__c= obj.applicationStatus;
                        if(obj.applicationStatus.containsIgnoreCase('success')){
                            detailObject.Message__c = 'SUCCESS';
                        }else{
                            detailObject.Message__c = 'FAILURE';
                            failedApplicationCount++;
                        }
                        
                        if(obj.applicationStatus.containsIgnoreCase('success')){
                            detailObject.Retry_Status__c ='INELIGIBLE';
                        }else{
                            if(obj.failedToReprocess.equalsIgnoreCase('true')){
                            detailObject.Retry_Status__c ='PENDING';
                            }else{
                                detailObject.Retry_Status__c ='INELIGIBLE';
                            }    
                        }
                        
                        
                        listToBeUpserted.add(detailObject);    
                    }
                    
                }
                
                if(listToBeUpserted.size()>0){
                    upsert listToBeUpserted Vendor_Application_Id__c;
                    
                    // below logic to insert record in related retry summary record
                    for(Careersite_Application_Details__c ad: listToBeUpserted){
                        PhenomFailedApplicationWrapper.Data wrapObject = applicationResponseMap.get(ad.Vendor_Application_Id__c);
                        Datetime rdt = system.now();
                        if(wrapObject.lastRetryDateTime != '' && wrapObject.lastRetryDateTime != null){
                            rdt = (DateTime)JSON.deserialize('"' + wrapObject.lastRetryDateTime + '"', DateTime.class);    
                        }					                 
                        Retry_Summary_Detail__c retrySummaryObject = new Retry_Summary_Detail__c();
                        retrySummaryObject.Application__c =  ad.Id;
                        retrySummaryObject.Transaction_ID__c = wrapObject.transactionId;
                        retrySummaryObject.Failed_to_Reprocess__c = wrapObject.failedToReprocess;
                        retrySummaryObject.Message__c = wrapObject.applicationStatus;
                        retrySummaryObject.Last_Retry_DateTime__c = rdt; 
                        retrySummaryList.add(retrySummaryObject);
                    }
                    upsert retrySummaryList Transaction_ID__c;
                }
                if(failedApplicationCount >= Integer.valueOf(Label.Phenom_Failed_Application_Count_Threshold)){
                    sendEmailMethod('','',opco_name,failedApplicationCount);
                }
            }Catch(Exception e){
                System.debug(e.getMessage());
                ConnectedLog.LogException('PhenomReconciliationGetApplication_v2', 'processResponseData', e);
            }
            
        }
        
        public void sendEmailMethod(String strErrorMessage, String requestBody, String opco, Integer failureCount){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                    List<String> toAddresses = new List<String>();
                    toAddresses = Label.Phenom_Error_Notification.split(',');
                    system.debug(toAddresses);
                    mail.setToAddresses(toAddresses); 
                    String strEmailBody;
                    if(failureCount >= 1){
                            mail.setSubject('Phenom Failed Application Count for '+opco); 
                            strEmailBody  ='Hi Team,\n\nPlease find Failed Application Count:\n\n ';
                            strEmailBody +='Count: '+ failureCount;
                            strEmailBody +='\n\n kindly take the necessary actions.';
                    }else{
                            mail.setSubject('Phenom Reconciliation GetApplication Service Failure for '+opco); 
                            strEmailBody  ='Hi Team,\n\nPlease find Service failure details:\n\n ';
                            strEmailBody +='RequestBody: '+requestBody+'\n\n';
                            strEmailBody +='Message: '+ strErrorMessage;
                    }               
                                    
                   
                    mail.setPlainTextBody(strEmailBody);   
                    
                    //Send the Email
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        }
        
    }
    public class ResponseWrapper{
            public string responseString;
            public string message;
            public string requestString;
            public string apiResponse;
         }
    public class ParamWrapper{
            public String opco_name;
            public DateTime startDate;
            public DateTime endDate;
            public String status;
            public Integer pageNumber;
   }
}