public class PhenomReconciliationSuccessComparison {
    public static void compareRecords(List<Careersite_Application_Details__c> stagingApplicationList){
        try{
                Map<String,Careersite_Application_Details__c> stagingApplicationMap = new Map<String,Careersite_Application_Details__c>();
                List<Careersite_Application_Details__c> finalUpdateList = new List<Careersite_Application_Details__c>();
                Set<String> vApplicationIdSet = new Set<String>();
                
                for (Careersite_Application_Details__c obj: stagingApplicationList){
                    stagingApplicationMap.put(obj.Vendor_Application_Id__c,obj);
                }
                
                List<Event> eventApplicationList =[select id, Application_Id__c, Vendor_Application_ID__c, Job_Posting__c
                                                    	FROM Event 
                                                   		where Vendor_Application_ID__c IN: stagingApplicationMap.keySet()
                                                  		AND CreatedDate =  LAST_N_DAYS:3];
                
                for(Event e: eventApplicationList){
                    if(stagingApplicationMap.containsKey(String.valueOf(e.Vendor_Application_ID__c))
                      	&& !vApplicationIdSet.contains(String.valueOf(e.Vendor_Application_ID__c))){
                            Careersite_Application_Details__c tempObject = new Careersite_Application_Details__c();
                            tempObject =stagingApplicationMap.get(String.valueOf(e.Vendor_Application_ID__c));
                            tempObject.Present_In_Event__c = true;
                            finalUpdateList.add(tempObject);
                            vApplicationIdSet.add(String.valueOf(e.Vendor_Application_ID__c));
                    }    
                }
                if(finalUpdateList.size()>0){
                    update finalUpdateList;
                }
        }catch(Exception e){
            System.debug(e.getMessage());
            ConnectedLog.LogException('PhenomReconciliationSuccessComparison','compareRecords',e);
        }    
     }
        
}