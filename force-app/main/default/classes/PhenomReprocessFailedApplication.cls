public class PhenomReprocessFailedApplication{
	//opco : Aerotek,TEKsystems,EASi,Aston Carter
	//criteriaType : ByApplicationID, ByFailureStatus, ByLastUpdatedDate
	public static void doPhenomReprocessFailedApplication(FailedApplicationRequest faRequest) {
      
		FailedApplicationResponse faResponse = reprocessFailedApplications(faRequest);
        if(faResponse != null){
                for(Careersite_Application_Details__c cad: faRequest.applicationList){
                    if(faResponse.statusCode.contains('200')){
                    cad.Retry_Status__c ='PROCESSED';
                    }else{
                    cad.Retry_Status__c ='RETRYFAIL';
                    }
                    cad.Retry_Date__c = System.now();
                    cad.Retry_Message__c = faResponse.statusMessage;
                }
            
            try{
                update faRequest.applicationList;
            }catch(Exception ex){
                System.debug(ex.getMessage());
                ConnectedLog.LogException('PhenomReprocessFailedApplication', 'doPhenomReprocessFailedApplication', ex);
               
            }    
        }
		
	}

    public static FailedApplicationResponse reprocessFailedApplications(FailedApplicationRequest faRequest){
            FailedApplicationResponse faResponse = new FailedApplicationResponse();
        	Phenom_Reconciliation_Request_Log__c requestLog = new Phenom_Reconciliation_Request_Log__c();
        	requestLog.OPCO__c = faRequest.opco;
        	requestLog.API_Name__c ='retryApplication';
            // Custom setting or Custom metadata -This need to finalise
            Mulesoft_OAuth_Settings__c serviceSettings = Mulesoft_OAuth_Settings__c.getValues('Phenom Reprocess Failed Application'); 
    		String requestBody = createFailedApplicationRequestJSON(faRequest);
            Http httpCon = new Http();
            HttpRequest req = new HttpRequest();
    
            req.setEndpoint(serviceSettings.Endpoint_URL__c);
            req.setMethod('POST');
            req.setHeader('Authorization', 'Bearer ' + faRequest.oAuthToken); 
          //  req.setHeader('x-opco', faRequest.opco);
            req.setHeader('x-transaction-id', String.valueOf(system.now()));
            req.setHeader('Content-Type', 'application/json');
			req.setBody(requestBody);
            requestLog.Request_Body__c = '//'+faRequest.criteriaType+'\n'+requestBody;
            try {
                HttpResponse res = httpCon.send(req);
                requestLog.API_Response__c = res.getBody();
                if (res.getStatusCode() == 200) {
                   faResponse = (FailedApplicationResponse)JSON.deserialize(res.getBody(), FailedApplicationResponse.class);
                    if(faResponse.statusCode.contains('200')){
                    	requestLog.Status__c='Request Processed';    
                    }else{
                        requestLog.Status__c='Request Failed';
                    }
                    
                }else{
                    requestLog.Message__c= res.getBody();
                    requestLog.Status__c='Request Failed';
                    faResponse.statusCode= String.valueOf(res.getStatusCode());
                    faResponse.statusMessage = res.getBody();
                    sendEmailMethod(res.getStatusCode()+'-'+res.getBody(), requestBody,faRequest.opco);
                    
                }
                insert requestLog;
            } catch(Exception ex) {
                System.debug(ex.getMessage());
                 requestLog.Message__c= ex.getMessage();
                 requestLog.Status__c='Request Failed';
                 insert requestLog;
                
                faResponse = null;
                PhenomApplctnReconciliationController.sendEmailMethod(ex.getMessage(), 
                                                                              requestBody, 
                                                                              faRequest.opco);
                ConnectedLog.LogException('PhenomReprocessFailedApplication', 'reprocessFailedApplications', ex);
                
            }
    return faResponse;
    }	
	
	public static String createFailedApplicationRequestJSON(FailedApplicationRequest faRequest){
        Phenom_Authorization__mdt phenomClient = [select Client_ID__c, OPCO__c from Phenom_Authorization__mdt where OPCO__c=: faRequest.opco.toUpperCase() limit 1];
        String jsonRequest = '{"criteria": {';
            if(!String.isBlank(faRequest.criteriaType) && faRequest.criteriaType == 'ByFailureStatus'){
                jsonRequest += '"failureStatus" : "'+faRequest.failureStatus+'"';
            }else if(!String.isBlank(faRequest.criteriaType) && faRequest.criteriaType == 'ByApplicationID'){
                jsonRequest += '"applicationIds" : '+JSON.serialize(faRequest.applicationIds);
            }else if(!String.isBlank(faRequest.criteriaType) && faRequest.criteriaType == 'ByLastUpdatedDate'){
                jsonRequest += '"lastUpdatedStartDateTime" : "'+faRequest.lastUpdatedStartDateTime+'",';
                jsonRequest += '"lastUpdatedEndDateTime" : "'+faRequest.lastUpdatedEndDateTime+'"';
            }
        jsonRequest += '},"common":{"clientID":"'+phenomClient.Client_ID__c+'"} }';
    
        return jsonRequest; 
	}


	public Class FailedApplicationRequest{
        public String opco; //opco : Aerotek,TEKsystems,Easi,AstonCarter
        public String criteriaType; //criteriaType : ByApplicationID, ByFailureStatus, ByLastUpdatedDate
        public String failureStatus;
        public List<String> applicationIds; //Max Size 10
        public String lastUpdatedStartDateTime;
        public String lastUpdatedEndDateTime;
        public List<Careersite_Application_Details__c> applicationList;
        public String oAuthToken;
    
        public FailedApplicationRequest(String opco, String criteriaType, String failureStatus,
                                        List<Careersite_Application_Details__c> applicationList, String oAuthToken){
            this.opco = opco;
            this.criteriaType = criteriaType;
            this.failureStatus = failureStatus;
            this.applicationList = applicationList;
            this.oAuthToken = oAuthToken;
        }
    
        public FailedApplicationRequest(String opco,String criteriaType, List<String> applicationIds,
                                        List<Careersite_Application_Details__c> applicationList, String oAuthToken){
            this.opco = opco;
            this.criteriaType = criteriaType;
            this.applicationIds = applicationIds;
            this.applicationList = applicationList;
            this.oAuthToken = oAuthToken;
        }
    
        public FailedApplicationRequest(String opco, String criteriaType, String lastUpdatedStartDateTime,
                                        String lastUpdatedEndDateTime,List<Careersite_Application_Details__c> applicationList,
                                        String oAuthToken){
            this.opco = opco;
            this.criteriaType = criteriaType;
            this.lastUpdatedStartDateTime = lastUpdatedStartDateTime;
            this.lastUpdatedEndDateTime = lastUpdatedEndDateTime;
            this.applicationList = applicationList;
            this.oAuthToken = oAuthToken;
        }

	}

    public Class FailedApplicationResponse{
        public String statusCode;
        public String statusMessage;
    }
    
    public static void sendEmailMethod(String strErrorMessage, String requestBody, String opco){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
                List<String> toAddresses = new List<String>();
				toAddresses = Label.Phenom_Error_Notification.split(',');
				system.debug(toAddresses);
                mail.setToAddresses(toAddresses); 
                String strEmailBody;
                              
                mail.setSubject('Phenom Reconciliation reprocessFailedApplication Service Failure for '+opco); 
              
                //Prepare the body of the email
                
                strEmailBody  ='Hi Team,\n\nPlease find Service failure details:\n\n ';
                strEmailBody +='RequestBody: '+requestBody+'\n\n';
                strEmailBody +='Message: '+ strErrorMessage;                
               
                mail.setPlainTextBody(strEmailBody);   
                
                //Send the Email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
    
}