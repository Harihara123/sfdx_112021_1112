global class PhenomReprocessFailedApplicationBatch implements Database.Batchable<Sobject>,Database.AllowsCallouts{
    
    global String strQuery;
    global String criteriaType;//criteriaType : ByApplicationID, ByFailureStatus, ByLastUpdatedDate
    global String opco;//opco : Aerotek,TEKsystems,Easi,AstonCarter
   // global String oAuthToken;
    global List<String> applicationIds; // ['H1YB717QBTGGXFN3JA28','JA2H1YB7187QBTGN3GX8','2H1YBJA71TGGXFN387QB']
   // global Boolean vendorIdFlag = false;
    global String failureStatus;
    global String lastUpdatedStartDateTime;
    global String lastUpdatedEndDateTime;
    
    
	global PhenomReprocessFailedApplicationBatch(String opco,String criteriaType, List<String> applicationIds){ 
        this.opco = opco;
		this.criteriaType = criteriaType;
		this.applicationIds = applicationIds;
    }
    
    global PhenomReprocessFailedApplicationBatch(String opco,String criteriaType, String failureStatus){ 
        this.opco = opco;
		this.criteriaType = criteriaType;
        this.failureStatus = failureStatus;
    }
    
	global PhenomReprocessFailedApplicationBatch(String opco,String criteriaType, DateTime lastUpdatedStartDateTime, DateTime lastUpdatedEndDateTime){ 
        String pattern = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
        this.opco = opco;
		this.criteriaType = criteriaType;
        this.lastUpdatedStartDateTime = lastUpdatedStartDateTime.format(pattern, 'UTC');
        this.lastUpdatedEndDateTime = lastUpdatedEndDateTime.format(pattern, 'UTC');
    }
    
    global database.Querylocator start(Database.BatchableContext context){
		strQuery = 'Select Id, Vendor_Application_Id__c, OPCO__c,Retry_Message__c,Retry_Date__c,Retry_Status__c from Careersite_Application_Details__c ';
		strQuery +=	'where Application_Source__c=\'Phenom\' AND Retry_Status__c = \'PENDING\' AND OPCO__c =\''+opco+'\'';
		
        if(criteriaType == 'ByFailureStatus'){
        	strQuery +=	' AND  Application_Status__c = \''+failureStatus+'\'';//failure-504 /FAILURE
        }else if(criteriaType == 'ByApplicationID'){
                if(applicationIds != null && applicationIds.Size() >  0){
                    strQuery +=	' AND  Vendor_Application_Id__c IN :applicationIds ';
                }
         }else if(criteriaType == 'ByLastUpdatedDate'){
                strQuery +=	' AND  Application_Date_Time__c >= '+lastUpdatedStartDateTime+' AND  Application_Date_Time__c <= '+lastUpdatedEndDateTime;
         }
        
            
        System.debug(strQuery);
        return Database.getQueryLocator(strQuery);          
    }
    
    global void execute(Database.BatchableContext context , Careersite_Application_Details__c[] scope){
        
		PhenomReprocessFailedApplication.FailedApplicationRequest faRequest;
       
		Map<String,String> serviceSettings = WebServiceSetting.getMulesoftOAuth('Phenom Reprocess Failed Application');
        String token =serviceSettings.get('oToken');
        applicationIds = new List<String>(); // irrespestive how many id they send at the time of batch 
        									//call  out only 10 will be executed.
        
          	for(Careersite_Application_Details__c cad: scope){
            	applicationIds.add(cad.Vendor_Application_Id__c);
			}
		
       if(criteriaType == 'ByFailureStatus'){
        	faRequest = new PhenomReprocessFailedApplication.FailedApplicationRequest(opco,criteriaType, failureStatus,
                                                                                      scope, token);
	   }else if(criteriaType == 'ByApplicationID'){
        faRequest = new PhenomReprocessFailedApplication.FailedApplicationRequest(opco,criteriaType, applicationIds,
                                                                                  scope, token);
	   }else if(criteriaType == 'ByLastUpdatedDate'){
         faRequest = new PhenomReprocessFailedApplication.FailedApplicationRequest(opco,criteriaType, lastUpdatedStartDateTime,
                                        										 lastUpdatedEndDateTime,scope, token);
	   }   
	   PhenomReprocessFailedApplication.doPhenomReprocessFailedApplication(faRequest);
	   
    }
    
    global void finish(Database.BatchableContext context){
    }
    
}