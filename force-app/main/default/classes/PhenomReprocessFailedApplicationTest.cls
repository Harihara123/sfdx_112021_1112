@isTest
public class PhenomReprocessFailedApplicationTest {

    
     public static testMethod void retryFailedApplicationTest(){
        List<Mulesoft_OAuth_Settings__c>  serviceSettingsList = new List<Mulesoft_OAuth_Settings__c>();
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Failed Job Application',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                           Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        serviceSettingsList.add(serviceSettings);
         
        Mulesoft_OAuth_Settings__c serviceSettings1 = new Mulesoft_OAuth_Settings__c(name ='Phenom Reprocess Failed Application',
																	Endpoint_URL__c='https://api-tst.allegistest.com/v1/reprocessFailedApplication',
																	OAuth_Token__c='123456788889',
																	ClientID__c ='abe1234',
																   Client_Secret__c ='hkbkheml',
																	Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
																	Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
																);
		serviceSettingsList.add(serviceSettings1);
        insert serviceSettingsList;
        Test.startTest(); 
        Careersite_Application_Details__c detailObject = new Careersite_Application_Details__c();

        detailObject.OPCO__c = 'Easi';
        detailObject.Application_Source__c ='Phenom';
        detailObject.Application_Date_Time__c = System.now();  
        detailObject.Adobe_Ecvid__c = '434';
        detailObject.Posting_Id__c = '6959608';
        detailObject.Vendor_Application_Id__c = 'JA2H1YB7187QBTGGXFN3';
        detailObject.Retry_Status__c ='PENDING';
        detailObject.Application_Status__c ='PENDING';
        insert detailObject;
         
        
          Test.setMock(HttpCalloutMock.class, new WebServiceSetting_Test.OAuthMockResponse());
          Test.setMock(HttpCalloutMock.class, new PhenomFailedApplicationMockResponse());
         
       	  String responseJSON = '{ "statusCode": "200 - Success", "statusMessage": "Request Successfully Processed"}';
          String contentType = 'application/json';
          Integer statusCode = 200;
         
         calloutMockResponse mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
         Test.setMock(HttpCalloutMock.class, mkResponse);
       //  PhenomApplctnReconciliationController.callFailedApllicationService('Easi',String.valueOf(system.now().addHours(-24)),String.valueOf(system.now()));
         
         
         PhenomReprocessFailedApplicationBatch resFailedAppl = new PhenomReprocessFailedApplicationBatch('Easi', 'ByApplicationID', new List<String>{'JA2H1YB7187QBTGGXFN3'});
         Id batchJobId = Database.executeBatch(resFailedAppl, 10);
         
         resFailedAppl = new PhenomReprocessFailedApplicationBatch('Easi', 'ByFailureStatus', 'PENDING');
         batchJobId = Database.executeBatch(resFailedAppl, 10);
         
         resFailedAppl = new PhenomReprocessFailedApplicationBatch('Easi', 'ByApplicationID', new List<String>());
         batchJobId = Database.executeBatch(resFailedAppl, 10);
                  
         DateTime dt1 = system.now().addDays(1); //End Date
         DateTime  strtDt1 = dt1.addDays(-10);   //Start Date
         resFailedAppl = new PhenomReprocessFailedApplicationBatch('Easi','ByLastUpdatedDate', strtDt1 ,dt1);
         batchJobId = Database.executeBatch(resFailedAppl, 10);
         
         serviceSettingsList[1].Endpoint_URL__c='https://api-tst.allegistest.com/v1/fakeReprocessFailedApplication';
         update serviceSettingsList;
         
		 
         
        Test.stopTest();
    }
    
    public static testMethod void retryFailedApplicationTest1(){
        List<Mulesoft_OAuth_Settings__c>  serviceSettingsList = new List<Mulesoft_OAuth_Settings__c>();
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Failed Job Application',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                           Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        serviceSettingsList.add(serviceSettings);
         
        Mulesoft_OAuth_Settings__c serviceSettings1 = new Mulesoft_OAuth_Settings__c(name ='Phenom Reprocess Failed Application',
																	Endpoint_URL__c='https://api-tst.allegistest.com/v1/fakeReprocessFailedApplication',
																	OAuth_Token__c='123456788889',
																	ClientID__c ='abe1234',
																   Client_Secret__c ='hkbkheml',
																	Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
																	Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
																);
		serviceSettingsList.add(serviceSettings1);
        insert serviceSettingsList;
        Test.startTest(); 
        Careersite_Application_Details__c detailObject = new Careersite_Application_Details__c();

        detailObject.OPCO__c = 'Easi';
        detailObject.Application_Source__c ='Phenom';
        detailObject.Application_Date_Time__c = System.now();  
        detailObject.Adobe_Ecvid__c = '434';
        detailObject.Posting_Id__c = '6959608';
        detailObject.Vendor_Application_Id__c = 'JA2H1YB7187QBTGGXFN3';
        detailObject.Retry_Status__c ='PENDING';
        detailObject.Application_Status__c ='PENDING';
        insert detailObject;
         
        
          Test.setMock(HttpCalloutMock.class, new WebServiceSetting_Test.OAuthMockResponse());
          Test.setMock(HttpCalloutMock.class, new PhenomFailedApplicationMockResponse());
         
       	  String responseJSON = '{ "statusCode": "400 - Success", "statusMessage": "Request Successfully Processed"}';
          String contentType = 'application/json';
          Integer statusCode = 400;
         
         calloutMockResponse mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
         Test.setMock(HttpCalloutMock.class, mkResponse);
       
         mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
      	 PhenomReprocessFailedApplicationBatch   resFailedAppl = new PhenomReprocessFailedApplicationBatch('Easi', 'ByApplicationID', new List<String>());
         Id batchJobId = Database.executeBatch(resFailedAppl, 10);
         
        Test.stopTest();
    }
}