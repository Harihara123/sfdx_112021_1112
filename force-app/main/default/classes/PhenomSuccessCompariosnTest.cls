@isTest
public class PhenomSuccessCompariosnTest {
	@TestSetup
    static void testSetup() {
        
        Apex_Queries__c csetting1 = new Apex_Queries__c(name ='GetStagingSuccessRecords',
                                                                            Class_Name__c='PhenomSuccessRecordComparisonBatch',
                                                                            Method_Name__c='start',
                                                                            Query_String_1__c ='SELECT Id, Application_Date_Time__c, Vendor_Application_Id__c,Present_In_Event__c,' 
                                                       											+'Message__c FROM Careersite_Application_Details__c',
                                                                            Query_String_2__c ='where Application_Source__c=\'Phenom\' AND Message__c=\'SUCCESS\'' 
                                                       											+'AND Present_In_Event__c=false AND CreatedDate = LAST_N_DAYS:3'
                                                                            );
        
        insert csetting1;
        
        
        Apex_Queries__c csetting2 = new Apex_Queries__c(name ='GetWCSStagingSuccessRecords',
                                                                            Class_Name__c='PhenomSuccessRecordComparisonBatch',
                                                                            Method_Name__c='start',
                                                                            Query_String_1__c ='SELECT Id, Application_Date_Time__c, Vendor_Application_Id__c,Present_In_Event__c,' 
                                                       											+'Message__c FROM Careersite_Application_Details__c',
                                                                            Query_String_2__c ='where Application_Source__c=\'WCS\' AND Message__c=\'SUCCESS\'' 
                                                       											+'AND Present_In_Event__c=false AND CreatedDate = LAST_N_DAYS:3'
                                                                            );
        
        insert csetting2;
        
        List<Account> accList = new List<Account>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;
        
        Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
        ct.Other_Email__c = 'other0@testemail.com';
        ct.Title = 'test title';
        ct.MailingState = 'test text';
        insert ct;
        
        /*
        Order o = new Order();
        o.Name = 'TestOrder';
        o.EffectiveDate = System.today();
        o.AccountId = accList[0].id;
        o.ShipToContactId = ct.Id;
        o.Status = 'Applicant';
        insert o;
        */
        
        
        Event evt = new Event();
        evt.StartDateTime = Datetime.now();
        evt.EndDateTime = Datetime.now().addHours(1);
        evt.Vendor_Application_ID__c='v1234';
        evt.WhoId = ct.Id;
        //evt.WhatId = o.Id;
        insert evt;
        
        
        
        Careersite_Application_Details__c  cobj = new Careersite_Application_Details__c();
        cobj.Vendor_Application_Id__c='v1234';
        cobj.Message__c='SUCCESS';
        cobj.Application_Source__c='Phenom';
        cobj.Present_In_Event__c = false;
        cobj.Application_Date_Time__c = system.now();
        insert cobj;
        
        Job_Posting__c jp = new Job_Posting__c(Source_System_id__c='R.567887', OFCCP_Flag__c=true, Job_Title__c='Warehouse Associate');
		insert jp;
        
        Event evt2 = new Event();
        evt2.StartDateTime = Datetime.now();
        evt2.EndDateTime = Datetime.now().addHours(1);
        evt2.Vendor_Application_ID__c='v1234';
        evt2.WhoId = ct.Id;
        evt2.Ecvid__c='12345';
        evt2.Job_Posting__c = jp.Id;
        insert evt2;
        
        
        
        Careersite_Application_Details__c  cobj2 = new Careersite_Application_Details__c();
        cobj2.Vendor_Application_Id__c='12345-R.567887';
        cobj2.Message__c='SUCCESS';
        cobj2.Application_Source__c='WCS';
        cobj2.Present_In_Event__c = false;
        cobj2.Application_Date_Time__c = system.now();
        insert cobj2;
        
    }
    
    @isTest
    public static void method1() {
        
        Test.startTest();
     	SchedulePhenomSuccessComparison.setup('0 45 * * * ?', 'schedule success comparison');
        Test.stopTest();
    }
}