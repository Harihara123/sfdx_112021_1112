global class PhenomSuccessRecordComparisonBatch implements Database.Batchable<Sobject>{
    global String source;
    
    global PhenomSuccessRecordComparisonBatch(String source){ 
        this.source = source;
    }
    
	global database.Querylocator start(Database.BatchableContext context){
        String strQuery;
        Apex_Queries__c queryObject;
        system.debug('this.source'+this.source);
        if(this.source == 'Phenom'){
            queryObject = Apex_Queries__c.getValues('GetStagingSuccessRecords');
        }else{
            queryObject = Apex_Queries__c.getValues('GetWCSStagingSuccessRecords');
        }
		
        strQuery = queryObject.Query_String_1__c+' '+queryObject.Query_String_2__c;
        
        return Database.getQueryLocator(strQuery);          
    }
    
    global void execute(Database.BatchableContext context , Careersite_Application_Details__c[] scope){
        if(this.source =='Phenom'){
        	PhenomReconciliationSuccessComparison.compareRecords(scope);    
        }else{
            WCSSuccessComparison.compareRecords(scope);
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        if(a.NumberOfErrors > 0){
        		// Send an email to the Apex job's submitter notifying of job completion.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
                mail.setToAddresses(toAddresses);
            String subject = 'Phenom Reconciliation Success Record Comparison Batch Status:';
            
            mail.setSubject(subject + a.Status);
            mail.setPlainTextBody
                ('The batch Apex job processed ' + a.TotalJobItems +
                 ' batches with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
        }
        
    }
}