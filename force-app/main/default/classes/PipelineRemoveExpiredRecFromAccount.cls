public class PipelineRemoveExpiredRecFromAccount  implements Database.Batchable<sObject>{
	public Map<Id, String> accIDPeoplSoftIdMap = new Map<Id, String>();
	private  static Map<Integer, String> targetFieldsMap = null;
	
	static {
		targetFieldsMap = new Map<Integer, String>();
		targetFieldsMap.put(0,'Target_Account_Stamp_0__c');
		targetFieldsMap.put(1,'Target_Account_Stamp_1__c');
		targetFieldsMap.put(2,'Target_Account_Stamp_2__c');
		targetFieldsMap.put(3,'Target_Account_Stamp_3__c');
		targetFieldsMap.put(4,'Target_Account_Stamp_4__c');
		targetFieldsMap.put(5,'Target_Account_Stamp_5__c');
	}

	public class PipelineRemoveExpiredRecFromAccountException extends Exception {}

	public PipelineRemoveExpiredRecFromAccount(Map<ID, String> accIDPeoplesoftMap) {
		this.accIDPeoplSoftIdMap = accIDPeoplesoftMap;
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator([select id, Target_Account_Stamp_0__c, Target_Account_Stamp_1__c, Target_Account_Stamp_2__c,Target_Account_Stamp_3__c, Target_Account_Stamp_4__c, Target_Account_Stamp_5__c, (select id, search_ingestion_trigger_timestamp__c from Contacts) from account where id =:accIDPeoplSoftIdMap.keySet()]);
	} 

	public void execute(Database.BatchableContext ctx, List<Account> scope){
		System.debug('scope---'+scope);
		if(scope.size()>0){
			try{
				List<Account> accList = new List<Account>();	
				List<Contact> conList = new List<Contact>();
				
				for(Account accScope : scope) {
					
					String pplsID = accIDPeoplSoftIdMap.get(accScope.ID);							
					
					Account acc = new account(Id=accScope.ID);
					
					if(pplsID.length() > 0) {
						for(Integer i=0; pplsID.length() > 0; i++) {
							if(pplsID.length() >= 255){
								acc.put(targetFieldsMap.get(i), pplsID.substring(0, 255));
								pplsID = pplsID.substring(255);
							}
							else { 
								acc.put(targetFieldsMap.get(i), pplsID);
								pplsID = '';
							}
						}
					}
					else {
						//updating all target field with blank because precaution of rare scenario :
						// all pipeline record of 6 field expire. 
						for(Integer i=0; i < targetFieldsMap.size(); i++) {
							acc.put(targetFieldsMap.get(i), '');
						}
					}									
					accList.add(acc);
					for(Contact con: accScope.Contacts){
						con.search_ingestion_trigger_timestamp__c = System.now();
						conList.add(con);
					}
				}

				ConnectedLog.LogSack sack = new ConnectedLog.LogSack();
				Database.SaveResult[] srList = Database.update(conList, false);
				for (Integer i=0; i<srList.size(); i++) {
						if (!srList[i].isSuccess()) {
							Database.Error err = srList[i].getErrors()[0];
							sack.AddException('PipelineRemoveExpiredRecFromAccount', 'execute', conList[i].Id, new PipelineRemoveExpiredRecFromAccountException(err.getMessage()));
						}
				}

				System.debug('accList----'+accList);
				Database.SaveResult[] accListRes = Database.update(accList,false);
				for (Integer i=0; i<accListRes.size(); i++) {
						if (!accListRes[i].isSuccess()) {
							Database.Error err = accListRes[i].getErrors()[0];
							sack.AddException('PipelineRemoveExpiredRecFromAccount', 'execute', accList[i].Id, new PipelineRemoveExpiredRecFromAccountException(err.getMessage()));
						}
				}
				
			sack.Write();

			}catch(Exception e){
            	ConnectedLog.LogException('PipelineRemoveExpiredRecFromAccount', 'execute', e);
        	}
		}
	}

	public void finish(Database.BatchableContext ctx){
		System.debug('Finish accIDPeoplSoftIdMap--------'+accIDPeoplSoftIdMap);
		
		String query = 'SELECT Id FROM Account WHERE ID IN';
        
		/*
		if(accIDPeoplSoftIdMap != null ){
			HRXMLBatch hBatch = new HRXMLBatch(accIDPeoplSoftIdMap.keySet(), query);
			Database.executeBatch(hBatch, 50);
		}
		*/

		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CompletedDate
                                          FROM AsyncApexJob WHERE Id = :ctx.getJobId()];
        Map<String, String> mss = new Map<String, String>{
            'JobId' => a.Id,
            'Status' => a.Status,
            'NumberOfErrors' => String.valueOf(a.NumberOfErrors),
            'JobItemsProcessed' => String.valueOf(a.JobItemsProcessed),
            'TotalJobItems' => String.valueOf(a.TotalJobItems),
            'CompletedDate' => String.valueOf(a.CompletedDate)
        };
            
        ConnectedLog.LogInformation('PipelineRemoveExpiredRecFromAccount', 'execute', 'PipelineRemoveExpiredRecFromAccount batch complete', mss);
	}
}