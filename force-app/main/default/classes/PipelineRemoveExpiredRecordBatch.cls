/***************************************************************************************************************************************
* Description - This batch will remove cnadidate from user's pipeline after 90 days. 
				Fetch reocrds have expration date less than today. then delete fetched records from pipeline and 
				also search Peoplesoft ID value from account fileds Target_Account_Stamp_0__c to  Target_Account_Stamp_5__c and 
				remove peoplesoft id.
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Neel Kamal                  12/09/2018              Created
*****************************************************************************************************************************************/
public class PipelineRemoveExpiredRecordBatch  implements Database.Batchable<sObject>, Database.stateful, Schedulable{
	public Set<ID> talentAccts = new Set<Id>();
	public boolean haveException = false;
	public Map<Id, String> accIDPeoplesoftMap = new Map<Id, String>();
	private  static Map<Integer, String> targetFieldsMap = null;
    Date sd = null;
    Date ed = null;
	String query = 'select Id, User__r.Peoplesoft_Id__c, contact__c, contact__r.accountId, contact__r.account.Target_Account_Stamp_0__c, contact__r.account.Target_Account_Stamp_1__c, contact__r.account.Target_Account_Stamp_2__c, contact__r.account.Target_Account_Stamp_3__c, contact__r.account.Target_Account_Stamp_4__c, contact__r.account.Target_Account_Stamp_5__c from Pipeline__c';
	
	static { 
		targetFieldsMap = new Map<Integer, String>();
		targetFieldsMap.put(0,'Target_Account_Stamp_0__c');
		targetFieldsMap.put(1,'Target_Account_Stamp_1__c');
		targetFieldsMap.put(2,'Target_Account_Stamp_2__c');
		targetFieldsMap.put(3,'Target_Account_Stamp_3__c');
		targetFieldsMap.put(4,'Target_Account_Stamp_4__c');
		targetFieldsMap.put(5,'Target_Account_Stamp_5__c');
	}

	public PipelineRemoveExpiredRecordBatch(){
        query += ' where Expiry_Date__c < TODAY'; 
    }
    
    public PipelineRemoveExpiredRecordBatch(String startDate, String stopDate){
        sd = Date.valueOf(startDate);
        ed = Date.valueOf(stopDate);
    }

	public Database.QueryLocator start(Database.BatchableContext ctx) {
		if(sd!=null && ed!=null)
        	return Database.getQueryLocator(query+' where Expiry_Date__c>=:sd AND Expiry_Date__c<= :ed');
        else
            return Database.getQueryLocator(query);
	}

	public void execute(Database.BatchableContext ctx, List<Pipeline__c> pipelineScope) {
		System.debug('Number of pipeline records fetched---'+pipelineScope.size());
		//Map<Id, String> accIDPeoplSoftIdMap = new Map<Id, String>();
		String pplsID = '';

		for(pipeline__c pipeline : pipelineScope) {
			System.debug('Removing info---'+pipeline.User__r.Peoplesoft_Id__c+'----'+pipeline.contact__r.accountId);
			//Removing peoplesoft id of user 
			if(accIDPeoplesoftMap.containsKey(pipeline.contact__r.accountId)) {
				pplsID = accIDPeoplesoftMap.get(pipeline.contact__r.accountId);
				pplsID = pplsID.remove(pipeline.User__r.Peoplesoft_Id__c+':');
				
				if(pplsID.length() == 1) {//if there is only one peoplesoft ID then separator ':' will be remain in string after removing peoplesoft id.
					pplsID='';
				}
				System.debug('else---pplsID---'+pplsID);
				accIDPeoplesoftMap.put(pipeline.contact__r.accountId, pplsID);
			}
			else { 
				String pplsoftID='';
				//Concatenating all peoplesofids from all 6 field of account once.
				if(pipeline.contact__c != null && pipeline.contact__r.accountId != null){ 
					for(Integer j : targetFieldsMap.keySet()) { 
						if(String.isNotBlank((String)pipeline.contact__r.account.get(targetFieldsMap.get(j)))) {	
							pplsoftID += (String)pipeline.contact__r.account.get(targetFieldsMap.get(j));
						}
					}
				}

				pplsID = pplsoftID.remove(pipeline.User__r.Peoplesoft_Id__c+':');
				System.debug('if---pplsID---'+pplsID);
				if(pplsID.length() == 1) {//if there is only one peoplesoft ID then separator ':' will be remain in string after removing peoplesoft id.
					pplsID='';
				}
				accIDPeoplesoftMap.put(pipeline.contact__r.accountId, pplsID);
			}
			
		}	

		//Deleting expired records from pipeline and updating account targets fields.
		Savepoint sp = null; //setting savepoint if either update or delelete failure both object will be rollback
		try{
			sp = Database.setSavepoint();			
			delete pipelineScope;
		}Catch(Exception dex){
			System.debug('PipelineRemoveExpiredRecordBatch:::::'+dex);
			haveException = true;
			Database.rollback(sp); //Rollback if exception
		}
		
		
	}

	public void finish(Database.BatchableContext ctx){
		System.debug('Finish ::: PipelineRemoveExpiredRecordBatch ::: accIDPeoplesoftMap--------'+accIDPeoplesoftMap);
		
		if(accIDPeoplesoftMap != null ){
			PipelineRemoveExpiredRecFromAccount hBatch = new PipelineRemoveExpiredRecFromAccount(accIDPeoplesoftMap);
			Database.executeBatch(hBatch, 200);
		}		
	}

	public void execute(SchedulableContext sctx){
		PipelineRemoveExpiredRecordBatch plExpBatch = new PipelineRemoveExpiredRecordBatch();
		//PipelineRemoveExpiredRecordBatch plExpBatch = new PipelineRemoveExpiredRecordBatch('2020-11-23','2020-11-29');
		Database.executeBatch(plExpBatch, 200);
	}
}