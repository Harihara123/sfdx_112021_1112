@istest
public class PipelineRemoveExpiredRecordBatch_Test  {
	static testmethod void testPipelineExp() {
		List<User> usrList =  TestDataHelper.createUsers(2,'System Administrator');		
			
		TestData td = new TestData(10);
		List<Account> accList = td.createAccounts();
		Integer j=0;
		for(Account acc : accList) {
			if(j<5) {
				acc.Target_Account_Stamp_0__c = ':'+usrList.get(0).id+':'+usrList.get(1).id+':';
			}
			else {
				acc.Target_Account_Stamp_0__c = ':'+usrList.get(0).id+':';
			}
					
		}
		update accList;

		List<Contact> conList = new List<Contact>();
		for(integer i=0;i<10;i++){
			Contact c = new Contact(AccountID = accList[i].id, LastName = 'Contact'+i);
			conList.add(c);
		}
		insert conList;

		List<Pipeline__c> plList = new List<Pipeline__c>();

		for(Integer i=0; i<10; i++) {
			Pipeline__c pipeline;
			if(i<5){
				pipeline = new Pipeline__c(Contact__c = conList.get(i).Id, User__c = usrList.get(1).id, Expiry_Date__c = System.today().addDays(-1));
				plList.add(pipeline);
			}
			pipeline = new Pipeline__c(Contact__c = conList.get(i).Id, User__c = usrList.get(0).id, Expiry_Date__c = System.today().addDays(-1));
			plList.add(pipeline);					
		}
		insert plList;
		/*for(Integer i=5; i<10; i++) {
			Pipeline__c pipeline = new Pipeline__c(Contact__c = conList.get(i).Id, User__c = usr.Id, Expiry_Date__c = System.today().addDays(1));
			plList.add(pipeline);
		}*/
		System.runAs(usrList.get(0)) {
			Test.startTest();
				PipelineRemoveExpiredRecordBatch batch = new PipelineRemoveExpiredRecordBatch();
				Database.executeBatch(batch, 200);
			Test.stopTest();	
		}
	}
}