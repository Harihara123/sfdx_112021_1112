public with sharing class PipelineTriggerHandler  {

 private static boolean hasBeenProccessed = false;

   public void OnAfterInsert(List<Pipeline__c> newRecords) {
       callDataExport(newRecords);
    }
     
     
    public void OnAfterUpdate (List<Pipeline__c> newRecords) {        
       callDataExport(newRecords);
    }

	public void OnAfterDelete (Map<Id, Pipeline__c>oldMap) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'PipelineTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' Pipeline__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Pipeline__c');
            hasBeenProccessed = true; 
        }
    }

    private static void callDataExport(List<Pipeline__c> newRecords) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'PipelineTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' Pipeline__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Pipeline__c');
            hasBeenProccessed = true; 
        }

    } 

}