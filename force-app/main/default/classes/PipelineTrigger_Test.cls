@isTest
private class PipelineTrigger_Test{
  static testMethod void test_PipelineTrigger(){
   test.startTest();
    Pipeline__c pipeline_Obj = new Pipeline__c();
    Insert pipeline_Obj; 
   test.stopTest();
  }

  static testMethod void test_UseCase1(){
   test.startTest();
    Pipeline__c pipeline_Obj = new Pipeline__c();
    pipeline_Obj.Expiry_Date__c = Date.newInstance(2016, 12, 9);
    Insert pipeline_Obj;
    System.assertEquals(Date.newInstance(2016, 12, 9),[SELECT id, Expiry_Date__c FROM Pipeline__c where Id=:pipeline_Obj.Id].Expiry_Date__c);
    pipeline_Obj.Expiry_Date__c = Date.newInstance(2020, 1, 10);
    update pipeline_Obj; 
    System.assertEquals(Date.newInstance(2020, 1, 10),[SELECT id, Expiry_Date__c FROM Pipeline__c where Id=:pipeline_Obj.Id].Expiry_Date__c);
    pipeline_Obj.Expiry_Date__c = Date.newInstance(2020, 1, 10);
    Delete pipeline_Obj; 
    test.stopTest();
}
}