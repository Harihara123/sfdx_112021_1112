//ACE: Preference Centre Class
//Procedures for updating the Contact Preference options
public without sharing class PreferenceCentreClass 
{    
        public static string OptType;
        private class OptOut
        {
            public string Name { get; set; }
            //public boolean Value { get; set; }
            public string Value { get; set; }	// changed from boolean to string,Expected values are True, False, Undefined for ATS-3677
            public string Type { get; set; }	// Would be OptIn or OptOut (default to OptOut for backward ACE),ATS-3677
            public DateTime CapturedDate { get; set; }
            public string Brand { get; set; }
            public string Country { get; set; }
            public string Source { get; set; }
        }

        private class ContactMethod
        {
            public string Type { get; set; } 
            public string Value { get; set; }
            public string Source { get; set; }

            //Current Preferences
            public List<OptOut> OptOuts { get; set; }
            
            //List of Previous Preferences
            public List<OptOut> OptOutHistory { get; set; }
        }
    
		private class PrefCentre
        {
            public string version { get; set; } //Version 1.0
            public string Contact_Id { get; set; }
            public datetime Last_Updated {get; set; }
            public List<ContactMethod> Contact_Methods { get; set; }
    	}
    
        //Update the Preference Centre Options for a Contact
        public static Contact UpdatePreferenceCentre(Contact c){
            string errorValue = '';
            string channels = '';
            string emailOptOut =''; // Updated from Boolean to String for ACE - 522
            string mobileOptOut = ''; //Updated from Boolean to String for ACE - 522
            
            //only update preferences if primary email address
            if(c.PrefCentre_ContactMethodValue__c == c.Preferred_Email_Value__c){
                errorValue = 'ERROR: Non-Primary Email Entered.';
            }
            
            //Update by Brand
            if(c.PrefCentre_Brand__c == 'Allegis'){
               channels = c.PrefCentre_Allegis_OptOut_Channels__c;
               emailOptOut = c.PrefCentre_Allegis_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_Allegis_OptOut_Mobile__c;
            }else if(c.PrefCentre_Brand__c == 'TEKsystems'){
               channels = c.PrefCentre_TEKsystems_OptOut_Channels__c;
               emailOptOut = c.PrefCentre_TEKsystems_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_TEKsystems_OptOut_Mobile__c;
            }else if(c.PrefCentre_Brand__c == 'Aerotek'){
               channels = c.PrefCentre_Aerotek_OptOut_Channels__c;
               emailOptOut = c.PrefCentre_Aerotek_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_Aerotek_OptOut_Mobile__c;
            }else if(c.PrefCentre_Brand__c == 'MLA'){
               channels = c.PrefCentre_MLA_OptOut_Channels__c;
               emailOptOut = c.PrefCentre_MLA_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_MLA_OptOut_Mobile__c;
            }else if(c.PrefCentre_Brand__c == 'AllegisPartners'){
               channels = c.PrefCentre_AllegisPartners_OptOut_Channe__c;
               emailOptOut = c.PrefCentre_AllegisPartners_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_AllegisPartners_OptOut_Mobile__c;
            }else if(c.PrefCentre_Brand__c == 'AGS'){
               channels = c.PrefCentre_AGS_OptOut_Channels__c;
               emailOptOut = c.PrefCentre_AGS_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_AGS_OptOut_Mobile__c;
            }else if(c.PrefCentre_Brand__c == 'AstonCarter'){
               channels = c.PrefCentre_AstonCarter_OptOut_Channels__c;
               emailOptOut = c.PrefCentre_AstonCarter_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_AstonCarter_OptOut_Mobile__c;
            }else if(c.PrefCentre_Brand__c == 'EASi'){
               channels = c.PrefCentre_EASi_OptOut_Channels__c;
               emailOptOut = c.PrefCentre_EASi_OptOut_Email__c;
               mobileOptOut = c.PrefCentre_EASi_OptOut_Mobile__c;
            }
            
            //Update JSON
            c.PrefCentre_Structured__c = GetUpdatedStructuredJSON(c,'OptOut', channels, emailOptOut, mobileOptOut);//ATS_3677 Added new Param 'OptOut'
            c.PrefCentre_Update_Error__c = errorValue;
            
            return c;
        }
    
    	//Update the JSON Structured Preferences (on Contact Update/Insert) and return
    	public static string GetUpdatedStructuredJSON(Contact c,string Otype, string channels, string emailOptOut, string mobileOptOut)//ATS_3677 changed method signature to allow optout type, Changed from boolean to string for emailoptout and mobileoptout for ACE - 522
        {
            OptType = Otype;//ATS_3677 populates the type of OptOut,"OptOut" for Ace and "OptIn" for ATS
            String currentP = c.PrefCentre_Structured__c;
            PrefCentre p = new PrefCentre();
            
            //Get the Current PrefCentre object
            if(currentP != null){
                if(currentP != ''){
                   try{
                        p = (PrefCentre)Json.deserialize(currentP,PrefCentre.class); 
                   } catch (Exception e){
                       //If current PrefCentre is invalid, continue with new.
                   } 
                }
            }
        
        	p.Last_Updated = c.PrefCentre_CapturedDate__c;
        	p.version = '1.0';
	        p.Contact_Id = c.Id;
        
            if (p.Contact_Methods == null){
                p.Contact_Methods = new List<ContactMethod>();
            }
            
            ContactMethod newCM = new ContactMethod();
            newCM.Type = c.PrefCentre_ContactMethodType__c;
            newCM.Value = c.PrefCentre_ContactMethodValue__c;
            newCM.Source = c.PrefCentre_CMSource__c;
        
            //Define New Preference for this contact method
            newCM.OptOuts = new list<OptOut>();
            
            //Store channels that have been opted out
            if(channels != null){
                for(string channel : channels.split(';')){
                    newCM.OptOuts.add(SetPreferenceValues(c, channel, emailOptOut)); // added new parameter changed from true to emailOpout for ACE - 522
                }    
            }
            
            if(OptType == 'OptIn'){//ATS_3677 added this condition to populate a new channel Addl Jobs which is not a picklist value, so adding manually
               newCM.OptOuts.add(SetPreferenceValues(c, 'Additional Jobs', emailOptOut)); // added new parameter changed from false to emailOpout for ACE - 522
            }
         
            //Store email opt out and mobile opt out
            newCM.OptOuts.add(SetPreferenceValues(c,'Email_Opt_Out',emailOptOut));
            newCM.OptOuts.add(SetPreferenceValues(c,'Mobile_Opt_Out',mobileOptOut)); 

        
            //Look for existing Contact Method and update with new preferences
            Boolean cmFound = false;
            for(ContactMethod cm : p.Contact_Methods){
                if(cm.Value == c.PrefCentre_ContactMethodValue__c){
                    cmFound = true;
                    
                    //Move existing preferences into history
                    if(cm.OptOutHistory == null){
                        cm.OptOutHistory = new List<OptOut>();
                    }
                    
                    //If the preferences for this Brand & Country are already stored, move them to the History. 
                    //for(Integer j = 0; j < cm.OptOuts.size(); j++)
                    for(Integer j = cm.OptOuts.size() - 1; j >= 0 ; j--)//for loop iteration changed by akshay for ATS-3677
                    {
                        OptOut prC = cm.OptOuts.get(j);
                        if((prC.Brand == c.PrefCentre_Brand__c) && (prC.Country == c.PrefCentre_Country__c))
                        {
                            cm.OptOutHistory.add(prC);
                            cm.OptOuts.remove(j);
                        }
                    }
    
                    //Update CM with new preferences
                    cm.OptOuts.addAll(newCM.OptOuts);
                }
            }
       
            //If no existing Contact Method was found, add the new one.
            if(cmFound != true){
                p.Contact_Methods.add(newCM);
            }
            
            return Json.serialize(p);
        }
    
    	private static OptOut SetPreferenceValues(Contact c, string name, string value) // Changed from Boolean to String ACE -522
        {
            OptOut pr = new OptOut();
            pr.Type = OptType;//ATS_3677  storing the OptOut Type in the JSON
            pr.Brand = c.PrefCentre_Brand__c;
            pr.Country = c.PrefCentre_Country__c;
            pr.CapturedDate = c.PrefCentre_CapturedDate__c;
            pr.Source = c.PrefCentre_CapturedSource__c;
            pr.Name = name;
            if(value == 'N/A')
                pr.Value = 'true';
            else//ATS_3677 added the else logic as value for OptIn type is undefined and Optout is false
            {   
                if(OptType == 'OptOut')
                    pr.Value = 'false';
                else
                    pr.Value = 'undefined';
            }
            return pr;
    	} 
}