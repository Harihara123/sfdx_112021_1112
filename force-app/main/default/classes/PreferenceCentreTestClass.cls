@isTest(SeeAllData=false) 
public class PreferenceCentreTestClass {
    public static testMethod void test_GetUpdatedStructedJSON(){
        Contact c = new Contact();
        c.LastName = 'Data';
        c.FirstName = 'Test';
        c.Email = 'test@email.com';
        c.PrefCentre_Structured__c = '{testdata}'; //Ensure bad json format doesn't fail
        c.PrefCentre_Brand__c = 'Aerotek';
        c.PrefCentre_Country__c = 'United Kingdom';
        c.PrefCentre_CapturedSource__c = 'Job Board';
        c.PrefCentre_CapturedDate__c = DateTime.now();
        c.PrefCentre_ContactMethodValue__c = 'test@email.com';
        c.PrefCentre_ContactMethodType__c = 'Email';
        c.PrefCentre_CMSource__c = 'Resume';
        
        string channels = 'Career Advice;Company News;Industry Trends;Insights;Job Opportunities;Training Development;VIP Events';    
        
        string json = PreferenceCentreClass.GetUpdatedStructuredJSON(c,'OptOut', channels,'N/A', 'N/A');
        
        System.assertNotEquals('', json, 'ERROR: Preference Centre preferences not saved!');
        System.debug(json);
        
        c.PrefCentre_Structured__c = json;
        
        string json2 = PreferenceCentreClass.GetUpdatedStructuredJSON(c,'OptOut', channels, 'N/A', 'N/A');
        System.debug(json2);
        System.assertNotEquals(json, json2, 'ERROR: Preference Centre preferences not saved!');
        
        //added by akshay for ATS-3677
        string json3 = PreferenceCentreClass.GetUpdatedStructuredJSON(c,'OptIn', channels, 'N/A', 'N/A');
        System.debug(json3);
    }
    public static testMethod void test_PrefCentre_Trigger(){
        Test.startTest();
        
        Contact c = new Contact();
        c.LastName = 'Data';
        c.FirstName = 'Test';
        c.Email = 'test@email.com';
        c.MobilePhone = '0123456789';
        c.PrefCentre_Brand__c = 'Aerotek';
        c.PrefCentre_Country__c = 'United Kingdom';
        c.PrefCentre_CapturedSource__c = 'Job Board';
        c.PrefCentre_CapturedDate__c = DateTime.now();
        c.PrefCentre_ContactMethodValue__c = 'test@email.com';
        c.PrefCentre_ContactMethodType__c = 'Email';
        c.PrefCentre_CMSource__c = 'Resume';
   
        Schema.DescribeFieldResult F = Contact.PrefCentre_AGS_OptOut_Channels__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        insert c;
        
        Contact c1 = [SELECT id, PrefCentre_Structured__c FROM Contact WHERE id = :c.id ];
        System.assertNotEquals('', c1.PrefCentre_Structured__c);
        
        c.PrefCentre_CapturedDate__c = DateTime.now();
        c.PrefCentre_ContactMethodType__c = 'Mobile';
        c.PrefCentre_ContactMethodValue__c = '0123456789';
        
        update c;
        
        Contact c2 = [SELECT id, PrefCentre_Structured__c FROM Contact WHERE id = :c.id ];
        System.assertNotEquals('', c2.PrefCentre_Structured__c);
                
        
        update c;
        
        Contact c3 = [SELECT id, PrefCentre_Structured__c FROM Contact WHERE id = :c.id ];
        System.assertNotEquals('', c3.PrefCentre_Structured__c);
        
        List<string> brands = new List<string>{'Allegis', 'TEKsystems', 'Aerotek', 'MLA', 'AllegisPartners', 'AGS', 'AstonCarter', 'EASi'};
        
        integer i = 1;
        
        for(string brand : brands){
            c.PrefCentre_Brand__c = brand;
            c.PrefCentre_CapturedDate__c = DateTime.now().addMinutes(i);
            
            update c;
            
            Contact c4 = [SELECT id, PrefCentre_Structured__c FROM Contact WHERE id = :c.id ];
            System.assertNotEquals('', c4.PrefCentre_Structured__c);
            
            i = i + 1;
        }
        
        Test.stopTest();
    }
}