/*
@author: Neel Kamal
@date: 04/21/2020
@description: Inovkable method called from Process Builder 'Generate PE for send Proactive Submittal to RWS'
			  It schedule the Class ScheduleProactiveSubmittalPEGeneration run after 5 minute of current time.
			  Because once Proactive Submittal status changed to offer accepted then Opportunity has also created.
			  Opportunity need to sync first in RWS application before Submittal sync.
*/
public class ProactiveSubmittalPEGeneration  {	
	public static String label='Generate PE to Send Proactive Submittal to RWS';
	
	@InvocableMethod
	public static void SchedulePEGeneration(List<Id> orderIds) {
		Datetime currTime = System.now();
		currTime = currTime.addMinutes(5);
		Integer ss = currTime.second();
		Integer mm = currTime.minute() ;
		Integer hh = currTime.hour();
		Integer day = currTime.day();
		Integer mn = currTime.month();
		Integer yr = currTime.year();
		String CRON_EXP = String.valueOf(ss) + ' ' + String.valueOf(mm) + ' ' + String.valueOf(hh) + ' ' + String.valueOf(day) + ' ' + String.valueOf(mn) + ' ' +  '?'  + ' ' + String.valueOf(yr) ;

		label = label + currTime.getTime();

		System.schedule(label,  CRON_EXP, new ScheduleProactiveSubmittalPEGeneration(orderIds));
	}
}