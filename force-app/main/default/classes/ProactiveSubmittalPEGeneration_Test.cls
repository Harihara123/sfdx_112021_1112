@isTest
public class ProactiveSubmittalPEGeneration_Test  {
	
	@TestSetup
	static void testDataSetup() {
		
		User usr = TestDataHelper.createUser('Single Desk 1');
        insert usr; 

		Account acc1 = TestData.newAccount(1, 'Client');
		insert acc1;
		Contact ct1 = TestData.newContact(acc1.id, 3, 'Client') ;
		insert ct1;

		Order sub = TestData.newOrder(acc1.id, ct1.id, usr.id, 'Proactive');
        insert sub;

	}

	@IsTest
	static void schedulePEGeneration_Test() {
		List<Id> ordList = new List<Id>();
		Test.startTest();
			for(Order ord : [Select Id, ShipToContactId, CreatedById From Order ]) {
				ordList.add(ord.id);
			}
			ProactiveSubmittalPEGeneration.schedulePEGeneration(ordList);	

			CronTrigger job = [SELECT Id,  TimesTriggered, NextFireTime, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger ORDER BY CreatedDate DESC LIMIT 1];
			System.assert(job.NextFireTime > System.now(), 'Job Scheduled');

		Test.stopTest();
	}
}