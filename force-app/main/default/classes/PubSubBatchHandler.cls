public class PubSubBatchHandler  {

    public static Integer BatchQueryMaxCount = Integer.valueOf(Connected_Data_Export_Switch__c.getInstance('DataExport').Data_Extraction_Query_Limit__c);

    public static Boolean isAllUsersAllowed =  Connected_Data_Export_Switch__c.getInstance('DataExport').Insert_Allowed_All_Users__c;

    private static final String QUERY_STR = 'Select Object_Ref_ID__c from Data_Extraction_EAP__c where Object_Name__c =:objName ';
    private static final String WITHOUT_FAILED_REC = ' AND Not_Processed__c = null order by CreatedDate ASC limit ';
    private static final String WITH_FAILED_REC    = ' order by CreatedDate ASC limit ';
    private static final String Query_Count_Select = 'SELECT count() FROM Data_Extraction_EAP__c ';
    private static final String Query_Count_Without_Failed_Records = 'where Not_Processed__C = null ';


     //Insets a record into the Data_Extraction_EAP__c on Object on trigger calls
     public static void insertDataExteractionRecord(List<SObject> listRecords, String objectName) {

          if (isUserAllowedTransaction())  { 
               String ids = '';
               try {
                    List<Data_Extraction_EAP__c> listDE = new List<Data_Extraction_EAP__c>();
                    for (SObject sobj : listRecords) {
                         Data_Extraction_EAP__c dE = new Data_Extraction_EAP__c(Object_Ref_ID__c = sobj.Id, Object_Name__c = objectName);
                         listDE.add(dE);
                         ids += sobj.Id + ',';
                    }
                    insert listDE; 
                    ConnectedLog.LogInformation('GCPSync/DataExtraction/Insert', 'PubSubBatchHandler', 'insertDataExteractionRecord', 
                              'Inserted ' + objectName + ' ' + listDE.size() + ' records ' + ids);
               } catch(Exception e) {
                    System.debug(logginglevel.ERROR, ' Failed to insert record -  Data_Extraction_EAP__c   !  '  + e.getMessage());
                    ConnectedLog.LogException('GCPSync/DataExtraction/Insert/Exception', 'PubSubBatchHandler', 'insertDataExteractionRecord', e);
               } 
          }
     }

   //Insets a record into the Data_Extraction_EAP__c on Object on trigger calls
   public static void insertDataExtractionRecordByIds(List<Id> listIds, String objectName) {

     if (isUserAllowedTransaction()) {
          String ids = '';
          try {
               List<Data_Extraction_EAP__c> listDE = new List<Data_Extraction_EAP__c>();
               for (Id recordId : listIds) {
                    Data_Extraction_EAP__c dE = new Data_Extraction_EAP__c(Object_Ref_ID__c = recordId, Object_Name__c = objectName);
                    listDE.add(dE);
                    ids += recordId + ',';
               }
               insert listDE; 
               ConnectedLog.LogInformation('GCPSync/DataExtraction/Insert', 'PubSubBatchHandler', 'insertDataExtractionRecordByIds', 
                              'Inserted ' + objectName + ' ' + listDE.size() + ' records ' + ids);
          } catch(Exception e) {
               System.debug(logginglevel.ERROR, ' Failed to insert record -  Data_Extraction_EAP__c   !  '  + e.getMessage());
               ConnectedLog.LogException('GCPSync/DataExtraction/Insert/Exception', 'PubSubBatchHandler', 'insertDataExtractionRecordByIds', e);
          } 
      }
  }

     //insert a record into the Data_Extraction_EAP__c on Object on Google Pub-Sub calls 
     public static void insertDataExteractionRecordOnFailedPubSubCalls(Set<String> idSet, String objectName) {
          ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();
          try {
               List<Data_Extraction_EAP__c> existingListObj = [select  Object_Ref_ID__c from Data_Extraction_EAP__c where Object_Ref_ID__c IN :idSet];

               List<String> existingList = new List<String>();
               String exids = '';
               String ids = '';

               for (Data_Extraction_EAP__c de :  existingListObj) {
                    existingList.add(de.Object_Ref_ID__c);
                    exids += de.Object_Ref_ID__c + ',';
               }
               if (existingList.size() > 0) {
                    idSet.removeAll(existingList);
                    logSack.AddInformation('GCPSync/DataExtraction/DropExisting', 'PubSubBatchHandler', 'insertDataExteractionRecordOnFailedPubSubCalls', 
                                'Dropped ' + existingList.size() + ' existing ' + objectName + ' records: ' + exids);
               }
               Integer allowedDMLRWS = getAllowedDMLRows(idSet.size());
               if (idSet.size() <= allowedDMLRWS) {
                    List<Data_Extraction_EAP__c> listDE = new List<Data_Extraction_EAP__c>();
                    for (String refIdStr : idSet) {
                         Data_Extraction_EAP__c dE = new Data_Extraction_EAP__c(Object_Ref_ID__c = refIdStr, Object_Name__c = objectName, Not_Processed__c = '1');
                         listDE.add(dE);
                         ids += refIdStr + ',';
                    }
                    insert listDE; 
                    logSack.AddInformation('GCPSync/DataExtraction/Insert', 'PubSubBatchHandler', 'insertDataExteractionRecordOnFailedPubSubCalls', 
                                'Inserted ' + idSet.size() + ' ' + objectName + ' records: ' + ids);
               }
          } catch(Exception e) {
               System.debug(logginglevel.ERROR, ' Failed to insert Not Processed Record for -  Data_Extraction_EAP__c   !  '  + e.getMessage());
               logSack.AddException('GCPSync/DataExtraction/Insert/Exception', 'PubSubBatchHandler', 'insertDataExteractionRecordOnFailedPubSubCalls', e);
          } finally {
               logSack.Write();
          }
     }
   

  public static Integer getAvailableRecordsTobeProcessed(Boolean processFaliedRecord) { 
       String queryStr = Query_Count_Select  + (processFaliedRecord  ? '' : Query_Count_Without_Failed_Records);
       return  Database.countQuery(queryStr);
  }

  public static String getQueryStr(Boolean processFaliedRecord) {
     return  QUERY_STR + (processFaliedRecord  ? WITH_FAILED_REC : WITHOUT_FAILED_REC) + String.valueOf(BatchQueryMaxCount);
  }

  public static Integer getQueuedAsyncApexJob() {
     return  Database.countQuery('select count()  FROM AsyncApexJob where ApexClass.Name = \'PubSubQueuableFetchFromStagingTable\' and (Status=\'Queued\' OR Status=\'Processing\') ');
  }

  public static Integer getAllowedDMLRows(Integer deleteMaxCount) {
     Integer allowedDMLRows = Limits.getLimitDMLRows()-Limits.getDMLRows(); 
     return ( deleteMaxCount <= allowedDMLRows ? deleteMaxCount : allowedDMLRows);
  }
    
  public static Integer getAllowedDMLRows() { 
     return  Limits.getLimitDMLRows()-Limits.getDMLRows(); 
  }


  private static Boolean isUserAllowedTransaction() {


   if ( isAllUsersAllowed  ||  ((UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) 
                                          || (UserInfo.getName() == DataExportUtility.ATS_DATS_MIGRATION 
                                                || UserInfo.getName() == DataExportUtility.TALENT_INTEGERATION) ) )  { 
           return true; 


  } else {
           return false; 
  }
} 

}