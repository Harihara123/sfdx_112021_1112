@RestResource(urlMapping='/GooglePubSubBatchRESTHelper/*')
global with sharing class PubSubBatchRESTJSON  {

    private class GPSBatchRESTHHelperJSON {
        public String[] objectList;
        public Boolean processFailedRecord;
        public String topicName;  
    }

          
    @HttpPost
    global static void doPost() {

         String body = RestContext.request.requestBody.toString();
         system.debug(body);

         if(body != null) { 
            body = JSON.serializePretty(JSON.deserializeUntyped(body));
            GPSBatchRESTHHelperJSON reqParam = (GPSBatchRESTHHelperJSON)JSON.deserialize(body, GPSBatchRESTHHelperJSON.class);

            Long startTime = DateTime.now().getTime();     
         
            Integer availableRecordsTobeProcessed = PubSubBatchHandler.getAvailableRecordsTobeProcessed(reqParam.processFailedRecord);
            Integer maxCountQueuedAsyncApexJob = PubSubBatchHandler.getQueuedAsyncApexJob();

            if (isAllowedToCallQueuable(availableRecordsTobeProcessed, maxCountQueuedAsyncApexJob)) {
                   System.enqueueJob(new PubSubQueuableFetchFromStagingTable(reqParam.objectList, reqParam.processFailedRecord, reqParam.topicName));
            }
             
            RestContext.response.addHeader('Content-Type', 'application/json');
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeNumberField('AvilableRecorsToBeProcsses', availableRecordsTobeProcessed);
            gen.writeNumberField('BatchRecorsSizeToBeProcsses',  PubSubBatchHandler.BatchQueryMaxCount);
            gen.writeNumberField('NumberOfQueuedJobs',  maxCountQueuedAsyncApexJob);
            gen.writeBooleanField('DataExtractProcess_ON', Connected_Data_Export_Switch__c.getInstance('DataExport').Data_Extraction_Query_Flag__c);
            gen.writeNumberField('TransactionTimeMilSec',  (DateTime.now().getTime() -  startTime));
            gen.writeEndObject();
            RestContext.response.responseBody = Blob.valueOf(gen.getAsString());
        } 
   }
    
   @TestVisible
   private static boolean isAllowedToCallQueuable(Integer availableRecordsTobeProcessed, Integer maxCountQueuedAsyncApexJob) {
         return ( (availableRecordsTobeProcessed > 0 
                      && Connected_Data_Export_Switch__c.getInstance('DataExport').Data_Extraction_Query_Flag__c
                          &&  maxCountQueuedAsyncApexJob < Connected_Data_Export_Switch__c.getInstance('DataExport').Max_Allowed_Queued_Job__c) 
                             ? true : false );
    }

    
}