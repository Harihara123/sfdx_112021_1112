public class PubSubQueuableCallouts  implements Queueable, Database.AllowsCallouts  {

       Map<String, set<String>> objIdMap = new Map<String, set<String>>(); 
       String topicName = ''; 


       public  PubSubQueuableCallouts( Map<String, set<String>> objIdMap, String topicName) {
           this.objIdMap = objIdMap; 
           this.topicName = topicName; 
       }

       public void execute(QueueableContext context) {

               Map<String, set<String>> faildRecordsIdMap = new Map<String, Set<String>>(); 

               for (String objName: objIdMap.keySet()) { 
                    Set<String> objIdSet = objIdMap.get(objName);
                    try {
                         GooglePubSubHelper.RaiseQueueEventsWithTopicName(Database.Query('SELECT ' + HerokuConnectQueueHelper.fetchFieldNames(objName,DataExportUtility.ObjectMap.get(objName)) + ' FROM ' + objName + ' WHERE Id IN :objIdSet ALL ROWS'), topicName);
                     } catch (Exception e)  {
                        System.debug(logginglevel.ERROR, ' Failed to extract SObject for  Google PubSub call!  -- '  + e.getMessage());
                        faildRecordsIdMap.put(objName, objIdSet);
                    }
              }

              for (String oName: faildRecordsIdMap.keySet()) { 
                    PubSubBatchHandler.insertDataExteractionRecordOnFailedPubSubCalls(faildRecordsIdMap.get(oName), oName);
              }             
    }



}