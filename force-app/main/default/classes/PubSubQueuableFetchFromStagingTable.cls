public class PubSubQueuableFetchFromStagingTable  implements Queueable { 


       private static String QueryStr; 

       String[] obJNameSet = new list<String>();  
       Boolean processFailedRecord = false; 
       String topicName = ''; 

       public  PubSubQueuableFetchFromStagingTable(String[] obJNameSet, Boolean processFailedRecord, String topicName) { 
                  
           this.obJNameSet = obJNameSet;  
           this.processFailedRecord = processFailedRecord; 
           this.topicName = topicName; 
       }

       public void execute(QueueableContext context) {

             QueryStr = PubSubBatchHandler.getQueryStr(processFailedRecord);

             Integer DeleteMaxCount_ForEachObject = PubSubBatchHandler.getAllowedDMLRows()/obJNameSet.size();

             Map<String, set<String>> objIdMap = new Map<String, Set<String>>(); 

             for (String objName: obJNameSet) { 
           
                      Data_Extraction_EAP__c[] dEList = Database.query(QueryStr); 

                      if (dEList.size() > 0 ) {

                          Set<String> setIds = new Set<String>();
                          for (Data_Extraction_EAP__c dE : dEList) {
                                    setIds.add(dE.Object_Ref_ID__c);
                          }

                          Database.delete([SELECT Id FROM Data_Extraction_EAP__c where Object_Ref_ID__c IN:setIds limit :DeleteMaxCount_ForEachObject], false);
                          objIdMap.put(objName, setIds);
                     }
             }  

            if (Connected_Data_Export_Switch__c.getInstance('DataExport').Google_Pub_Sub_Call_Flag__c && objIdMap.size() > 0 ) { 
                 ID jobID = System.enqueueJob(new PubSubQueuableCallouts(objIdMap, topicName));
            }

      }

}