public class QueableJobPostingOpportunityUpdate implements Queueable{
    Public List<Job_Posting__c> jobpostingList = new List<Job_Posting__c>();
    public QueableJobPostingOpportunityUpdate(List<Job_Posting__c> jbPostingList){
        this.jobpostingList = jbPostingList;
    }
    public void execute(QueueableContext QC){
        try{
                Map<id,Job_Posting__c> oppMap= new Map<id,Job_Posting__c> ();
                for(Job_Posting__c jp : jobpostingList){
                    if(jp.Status_Id__c != null){
                        oppMap.Put(jp.Opportunity__c,jp);
                    }
                }
            
            List<Opportunity> oppList = [select id,opco__c, isExclusiveJob__c,Job_Posting_Description__c 
                                         from opportunity 
                                         where id in: oppMap.keyset()];
            
            List<Opportunity> newOppList = new List<Opportunity>();
            String opcoLst = System.Label.TC_External_Job_Desc_Config;
            //system.debug('opcoLst:::'+opcoLst);
            List<String>opcoList = opcoLst.Split(';');
            for(Opportunity opp : oppList){
                if(oppMap.get(opp.Id).Status_Id__c == 12281.0){
                    opp.isExclusiveJob__c = true;
                    
                }else{
                    opp.isExclusiveJob__c = false;
                }
                if(oppMap.containsKey(opp.Id) && oppMap.get(opp.Id).Status_Id__c != null 
                   && oppMap.get(opp.Id).Status_Id__c != 12281.0 && oppMap.containsKey(opp.Id) 
                   && oppMap.get(opp.Id).External_Job_Description__c != null 
                   && oppMap.get(opp.Id).External_Job_Description__c != '' 
                   && opp.Opco__c != null && opcoList.contains(opp.Opco__c)){
                       //system.debug('Inside for loop');
                    if((oppMap.get(opp.Id).External_Job_Description__c).length() > 131072) {
                        opp.Job_Posting_Description__c = (oppMap.get(opp.Id).External_Job_Description__c).substring(0,131072);
                    }else{
                        opp.Job_Posting_Description__c = oppMap.get(opp.Id).External_Job_Description__c;
                    }
                } 
                newOppList.add(opp);
            }
            update newOppList;    
        }Catch(Exception e){
            system.debug('Exception:::'+e.getMessage());
            ConnectedLog.LogException('QueableJobPostingOpportunityUpdate', 'excecute', e);
        }
        
    }
}