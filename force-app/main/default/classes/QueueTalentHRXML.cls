/***************************************************************************
Name        : QueueTalentHRXML
Created By  : Akshay Konijeti
Date        : 01/20/2017
Story/Task  : ATS-3126
Purpose     : To generate HRXML asynchronously using queueable apex.
Scope for 
Improvement : Using a boolean variable on Talent record, before calling the HRXMl service, 
              we can check if the same record has been entered in the Jobs Queue so that the method gets executed only once instead of multiple times.
*****************************************************************************/
public class QueueTalentHRXML {
    
    @InvocableMethod(label='Update HRXML' description='Populate HRXML field for a given list of Account Ids.')
    public static void updateHRXML(List<Id> accountIds) {
		System.debug('This is QueueTalentHRXML');
		System.debug('limits:'+ Limits.getQueueableJobs());
		if (Limits.getQueueableJobs() == 0) {
		ID jobID = System.enqueueJob(new QueueableHRxml(accountIds));
		}
    }
    
    public class QueueableHRxml implements Queueable, Database.AllowsCallouts{
        public List<ID> accntIds; 
        public QueueableHRxml(List<ID> accIds){
            accntIds = accIds;
		}
        
        public void execute(QueueableContext QC){
			//S-96258 - Get GeoLocation from SOA - Karthik
			List<Id> acctIdList = new List<Id>();
			List<Id> acctIdListHRXML = new List<Id>();
			for (Contact c : [SELECT Id, AccountId, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, Talent_Country_Text__c, MailingLatitude, MailingLongitude, GoogleMailingLatLong__Latitude__s, GoogleMailingLatLong__Longitude__s FROM Contact WHERE Contact.AccountId IN : accntIds]){
				System.debug('MailingLatitude:: ' + c.MailingLatitude + ', MailingLongitude::' + c.MailingLongitude);
				if(c.MailingLatitude == null && c.MailingLongitude == null && c.MailingCountry != null && c.Talent_Country_Text__c != null &&
					(c.MailingCity != null || c.MailingPostalCode != null)) {
						acctIdList.add(c.AccountId);
				}
				//else{ for story S-133717 update hrxml even if lat,long is null
					acctIdListHRXML.add(c.AccountId);
				// }
			}

			if (acctIdList.size() > 0) {
				List<Contact> contactList = GetLatLongFromSOA.getLatLong(acctIdList);
				System.debug('UpdatedContactList::'+contactList);
				HRXMLService.updateMailingLatLong(contactList);
			}
			if (acctIdListHRXML.size() > 0) {
				HRXMLService.updateHRXML(acctIdListHRXML);
			}
		} 
    } 
}