/***************************************************************************************************************************************
* Name        - RWSCandidateMatchScheduledProcess 
* Description - This class is used for scheduling the logic for identifying contacts created/updated since the last time job ran 
                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       1/16/2013             Created
*****************************************************************************************************************************************/
global class RWSCandidateMatchScheduledProcess implements Schedulable
{
     
         global void execute(SchedulableContext ctx)
        {           
             Batch_RWSCandidateMatch batch = new Batch_RWSCandidateMatch('SELECT Id, FirstName, LastName, Phone, HomePhone, MobilePhone, OtherPhone, Email, Other_Email__c, RWS_Candidate_URL__c FROM Contact');
                                       
             ID batchprocessid = Database.executeBatch(batch,200); 
             System.debug('Returned batch process ID: ' + batchProcessId);                             
        }
     
}