global class RWSInteractionsCalloutMock implements System.HttpCalloutMock{

         global RWSInteractionsCalloutMock() {

        }
        global System.HTTPResponse respond(HTTPRequest req) {
            
                    HttpResponse res = new HttpResponse();
                    res.setHeader('Content-Type', 'application/json');

                    Map<String,Object> respMap = new Map<String,Object>();
                    respMap.put('responseMsg','SUCCESS');
                    respMap.put('responseDetail','saved successfully.');
                    respMap.put('applicationIdList',new List<String>());
                    String jsonResponse=JSON.serialize(respMap);
                    System.debug('jsonResponse '+jsonResponse);
                    res.setBody(jsonResponse);
                    res.setStatusCode(200);
                    return res;

        }
    }