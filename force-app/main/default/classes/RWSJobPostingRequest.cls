public class RWSJobPostingRequest {

	public String hrEmplId;
    public String fromHrEmplId;
    public String toHrEmplId;
	public List<String> postingIdList;

	
	public static RWSJobPostingRequest parse(String json) {
		return (RWSJobPostingRequest) System.JSON.deserialize(json, 
		RWSJobPostingRequest.class);
	}
}