public class RWSJobPostingResponse {
   
    public class PostingIdList {
         @AuraEnabled
		public String postingId;
         @AuraEnabled
		public String code;
         @AuraEnabled
		public String message;
	}
    
    @AuraEnabled
	public List<PostingIdList> postingIdList;
    
     @AuraEnabled
	public String responseMsg;

	@AuraEnabled
	public String sfdcMsg;
    
     @AuraEnabled
	public String responseDetail;
    
     @AuraEnabled
	public String viewId;

	
	public static RWSJobPostingResponse parse(String json) {
		return (RWSJobPostingResponse) System.JSON.deserialize(json, RWSJobPostingResponse.class);
	}

}