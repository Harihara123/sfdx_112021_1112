public class RWSJobPostingRestCallout {
    

    public static RWSJobPostingResponse refreshJobPosting(RWSJobPostingRequest request){
        
        String jsonRWSResponse=JSON.serialize(request);
        
        String jsonPretty=JSON.serializePretty(request);
        
        System.debug('JSON Prettify===>'+jsonPretty);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        RWSJobPostingResponse response=new RWSJobPostingResponse();
        Http http = new Http();
		String restUrl=System.Label.RWS_Job_Posting_URL;
        //req.setEndpoint('https://demo5289692.mockable.io/RWS/rest/posting/refresh');
        req.setEndpoint(restUrl+'/RWS/rest/posting/refresh');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(jsonRWSResponse);
        req.setTimeout(5000);
     //   req.setCompressed(true); // otherwise we hit a limit of 32000

        try {
            
            
              
                res = http.send(req);
                // Parse the JSON response
                System.debug(res.getBody());
                response= (RWSJobPostingResponse)JSON.deserialize(res.getBody(), RWSJobPostingResponse.class);
                System.debug('Response from RWS--->'+response);
               
            
        } catch(Exception e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
            response.responseMsg='ERROR';
            response.responseDetail=e.getMessage();
        }
          return response;
        
    }
    
     
    
    public static RWSJobPostingResponse inactivateJobPosting(RWSJobPostingRequest request){
       
      
         String jsonRWSResponse=JSON.serialize(request);
        
          String jsonPretty=JSON.serializePretty(request);
        
        System.debug('JSON Prettify===>'+jsonPretty);
        
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        RWSJobPostingResponse response=new RWSJobPostingResponse();
        Http http = new Http();
        String restUrl=System.Label.RWS_Job_Posting_URL;
        req.setEndpoint(restUrl+'/RWS/rest/posting/inactivate');
        //req.setEndpoint('https://demo5289692.mockable.io/RWS/rest/posting/inactivate');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(jsonRWSResponse);
        req.setTimeout(5000);
     //   req.setCompressed(true); // otherwise we hit a limit of 32000

        try {
                res = http.send(req);
                // Parse the JSON response
                System.debug(res.getBody());
                response= (RWSJobPostingResponse)JSON.deserialize(res.getBody(), RWSJobPostingResponse.class);
                System.debug('Response from RWS--->'+response);
            
           
        } catch(Exception e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
            response.responseMsg='ERROR';
            response.responseDetail=e.getMessage();
        }
         return response;
        
        
    }
    
    public static RWSJobPostingResponse transferJobPosting(RWSJobPostingRequest request){
       
      
         String jsonRWSResponse=JSON.serialize(request,true);
        
          String jsonPretty=JSON.serializePretty(request);
        
        System.debug('JSON Prettify===>'+jsonRWSResponse);
        
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        RWSJobPostingResponse response=new RWSJobPostingResponse();
        Http http = new Http();
		String restUrl=System.Label.RWS_Job_Posting_URL;
        req.setEndpoint(restUrl+'/RWS/rest/posting/transfer');
        //req.setEndpoint('https://demo5289692.mockable.io/RWS/rest/posting/inactivate');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(jsonRWSResponse);
        req.setTimeout(5000);
     //   req.setCompressed(true); // otherwise we hit a limit of 32000

        try {
                res = http.send(req);
                // Parse the JSON response
                System.debug(res.getBody());
                response= (RWSJobPostingResponse)JSON.deserialize(res.getBody(), RWSJobPostingResponse.class);
                System.debug('Response from RWS--->'+response);
            
           
        } catch(Exception e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
            response.responseMsg='ERROR';
            response.responseDetail=e.getMessage();
        }
         return response;
        
        
    }
    
    public static RWSJobPostingResponse mockResponse(){
        		RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
        		rwsResponse.responseMsg='SUCCESS';
                rwsResponse.responseDetail='Posting(s) refreshed successfully.';
                rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
                RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
                sPosting.code='SUCCESS';
                sPosting.message='Posting(s) refreshed successfully.';
                sPosting.postingId='555555';
                rwsResponse.postingIdList.add(sPosting);
        
        return rwsResponse;
    }
    
   
    
    public static void updateProgress(RWSJobPostingResponse response,Job_Posting__c jpc){
        
        System.debug('Inside UpdateProgres-jpc--------->'+jpc);
        System.debug('Inside UpdateProgres-response--------->'+response);
        if(response.responseMsg=='SUCCESS' && jpc!=null){
            System.debug('Inside UpdateProgres IF condition for loop--->'+response);
            jpc.SFPosting_Status__c='In Progress';
            
            try{ 
          		update jpc;
        	}catch(Exception e){
            	System.debug('Exception occured during update of job posting'+e);
        	}
        }
        
    }

}