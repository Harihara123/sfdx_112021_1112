public with sharing class RecruiterOpenReqsController  {
	public List<Opportunity> myOpemopptyWithTeam;

	@AuraEnabled
	public static List<Opportunity> getOpenReqs() {
			
		List<OpportunityTeamMember> openReqInOppTeam = [select opportunityId from OpportunityTeamMember where userId =:UserInfo.getUserId()];
		List<Id> OppIds = new List<Id>();
		For (OpportunityTeamMember OTM : openReqInOppTeam) {
			OppIds.add(OTM.OpportunityId);
		}

		List<Opportunity> myOpenOppty = [select Id, Name,AccountId, Owner.Name, Opportunity_Num__c, Account_name_Fourmula__c, Req_Hiring_Manager__r.Name ,Req_Job_Title__c,Req_Worksite_City__c,Req_Worksite_State__c,Req_Worksite_Country__c,CreatedDate, Req_Red_Zone__c, StageName,
										Req_GeoLocation__Latitude__s,Req_GeoLocation__Longitude__s
										from opportunity where (OwnerId =:UserInfo.getUserId() OR
										 Id in :OppIds) and Isclosed = false and recordtype.name = 'Req' and StageName != 'Staging' Order by createdDate desc limit 500];
		
		return myOpenOppty;
	}

	@AuraEnabled
    public static String getCurrentUser(){
        User user = [SELECT id, Name, Profile.Name, OPCO__c, Profile.PermissionsModifyAllData, Peoplesoft_Id__c, CompanyName
                        FROM User WHERE id = :Userinfo.getUserId() LIMIT 1][0];
		
		return JSON.serialize(user);
	}
}