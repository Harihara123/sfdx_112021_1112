@RestResource(urlMapping='/reference/globalLOV/V0.1/*')
global with sharing class ReferenceGlobalLOVV0_1 {
    
    @HttpGet
    global static void getLOVValues() {
        Map<String, Object> p = new Map<String, Object>();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String picklistName = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        String searchString = RestContext.request.params.get('search');
        String flds = '["text_value_2__c"]';
        
            
        //System.debug(req);
        
        p.put('type', 'Global_LOV__c');
        p.put('searchString', searchString);
        p.put('nameFieldOverride', '');
        p.put('idFieldOverride', '');
        p.put('useCache', false);
        p.put('sortCondition', 'Number_Value__c DESC');
        p.put('addlReturnFields', flds);
        
                
        switch on picklistName.toLowerCase() {
            when 'educationinstitutions' {
                p.put('recTypeName', 'Education  Institutions');
                 string jsonstring = ((String)BaseController.performServerCall('Lookup', '', 'searchSObject', p));
                 res.addheader('Content-Type', 'application/json');
                 res.responseBody=  Blob.valueOf(jsonstring);
               
            }            
            when 'jobtitles' {
                 string jsonstring = (ReferenceGlobalLOVV0_1.LookupJobTitles(p, req));
                 res.addheader('Content-Type', 'application/json');
                 res.responseBody=  Blob.valueOf(jsonstring);
           //     return ReferenceGlobalLOVV0_1.LookupJobTitles(p, req);
            }
            when 'skills' {
                string jsonstring = (ReferenceGlobalLOVV0_1.LookupSkills(p, req));
                 res.addheader('Content-Type', 'application/json');
                 res.responseBody=  Blob.valueOf(jsonstring);
             //   return ReferenceGlobalLOVV0_1.LookupSkills(p, req);
            } 
            when 'certifications' {
                p.put('recTypeName', 'CertificationList');
                  string jsonstring = ((String)BaseController.performServerCall('Lookup', '', 'searchSObject', p));
                 res.addheader('Content-Type', 'application/json');
                 res.responseBody=  Blob.valueOf(jsonstring);
              //  return (String)BaseController.performServerCall('Lookup', '', 'searchSObject', p);
            } 
            when else {
             //   return null;
            }
        }
    }
    
    Private static String LookupJobTitles(Map<String, Object> p, RestRequest req) {
        String opco = req.params.get('opco');
        p.put('whereCondition', '');
        
        if (!String.isBlank(opco)) {            
            p.put('whereCondition', 'text_value_2__c = \'' + opco + '\'');            
        }
    p.put('recTypeName', 'JobTitleList');
        
        return (String)BaseController.performServerCall('Lookup', '', 'querySObject', p);
    }

    Private static String LookupSkills(Map<String, Object> p, RestRequest req) {
        String opco = req.params.get('opco');
        p.put('whereCondition', '');
        
        if (!String.isBlank(opco)) {
            p.put('whereCondition', 'text_value_2__c = \'' + opco + '\'');            
        }
        p.put('recTypeName', 'MatchSkillList');
        
        return (String)BaseController.performServerCall('Lookup', '', 'querySObject', p);
    }

}