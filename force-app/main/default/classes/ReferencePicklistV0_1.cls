@RestResource(urlMapping='/reference/picklist/V0.1/*')
global with sharing class ReferencePicklistV0_1 {
    
    @HttpGet
    global static Map<String, String> getPickListValues() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String picklistName = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        
        switch on picklistName.toLowerCase() {
            when 'degreelevels' {
                return (Map<String, String>)BaseController.performServerCall('ATS', 'TalentEducationFunctions', 'getDegreeLevelList', null);
            }
            when 'documenttypes' {
                return (Map<String, String>)BaseController.performServerCall('ATS', 'TalentDocumentFunctions', 'getDocumentTypeList', null);
            }
            when else {
                return null;
            }
        }
    }

}