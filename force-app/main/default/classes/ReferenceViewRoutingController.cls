public class ReferenceViewRoutingController {
    
   private final ApexPages.StandardController controller;
    
    public ReferenceViewRoutingController(ApexPages.StandardController controller) {
        this.controller = controller;
    }
    
    @AuraEnabled
     // this method returns the talent_c by passing the reference account Id of Recommended_By__c
    public static Id getMeReferenceContact(String AccId){
       return  getContactId(AccId);
    }
    
    
    @AuraEnabled
     // this method returns the talent_c by passing the reference account Id of Recommended_By__c
    public static Id getContactIdReferenceAccount (String AccId){
        
       List<Talent_Recommendation__c> recs = [select Talent_Contact__r.AccountId from Talent_Recommendation__c where Recommendation_From__r.AccountId = :AccId LIMIT 1];
       if(recs.size() > 0){
            return  getContactId(recs[0].Talent_Contact__r.AccountId);
        } else{
           return null; 
        }
    }
    
    /*
    @AuraEnabled
    // this method returns the talent_c by passing the reference contact Id of Recommendation_From__c
    public static Id getContactIdReferenceContact (String contactId){
        
       //List<Talent_Recommendation__c> recs = [select Talent_Contact__r.AccountId, Talent_Contact__r.Id, Recommendation_From__r.AccountId from Talent_Recommendation__c where Recommendation_From__r.Id = :contactId LIMIT 1];
       //if(recs.size() > 0){
            return  getContactId(contactId);
        //} else{
          // return null; 
        //}
    }
    */
    // This method always takes account Id in the param and returns contact Id 
    @AuraEnabled
    public static Id getContactId(Id idValue) {
       system.debug('#####'+idValue);
       //List<Talent_Recommendation__c> recs = [select Talent_Contact__r.AccountId, Talent_Contact__r.Id from Talent_Recommendation__c where Recommendation_From__r.AccountId = : idValue LIMIT 1];
       List<Contact> con = [Select Id from contact where AccountId =: idValue LIMIT 1];
       if(con.size() > 0){
           return con[0].Id;
       }  else {
               return null; 
           }
    }
    
    

}