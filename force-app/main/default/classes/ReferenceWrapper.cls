public class ReferenceWrapper{
  @AuraEnabled
  public Account account {get;set;}
  @AuraEnabled
  public contact contact {get;set;}
  @AuraEnabled
  public Talent_Recommendation__c recommendation {get;set;}
  @AuraEnabled
  public Boolean disableContactField {get;set;}
   @AuraEnabled
  public Boolean rwsRecord {get;set;}
  
  public ReferenceWrapper(){
      this.account = new Account();
      this.contact = new contact();
      this.recommendation = new Talent_Recommendation__c();
      this.disableContactField =false;
      this.rwsRecord=false;
  }
}