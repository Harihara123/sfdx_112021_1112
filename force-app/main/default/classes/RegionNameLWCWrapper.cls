global class RegionNameLWCWrapper {
    
    @AuraEnabled public String RegionNameKey {get; Set;}
    @AuraEnabled public String RegionNameValue {get; Set;}
    
     public RegionNameLWCWrapper() {        
    }

}