@isTest
public class RelatedListControllerTest  {
	
	static testMethod void initDataTest() {
		Account acc = TestData.newAccount(1);
		insert acc;

		Contact con = TestData.newContact(acc.Id, 1, 'Client');
		insert con;

		System.debug('RelatedListControllerTest---'+con);
		Map<String, Object> inputMap = new Map<String, Object>();
		String fields='LastName, FirstName, Title, Email';
		inputMap.put('fields', fields);
		inputMap.put('relatedFieldApiName','AccountId'); 
		inputMap.put('recordId', acc.id);
		inputMap.put('numberOfRecords', 5);
		inputMap.put('sobjectApiName', 'Contact');
		inputMap.put('sortedBy', 'Title');
		inputMap.put('sortedDirection', 'ASC');

		System.debug(JSON.serializePretty(inputMap)); 

		String resultJson = RelatedListController.initData(JSON.serializePretty(inputMap));
		//Map<String, Object> resultMap = (Map<String, Object>)JSON.deserializeUntyped(resultJson);
		//System.debug('RelatedListControllerTest:::result map:::'+resultMap.get('records'));
		//Object obj = <>resultMap.get('records');
		//System.debug('RelatedListControllerTest:::result map::obj:::'+obj.get('LastName'));
		//System.assertEquals(con.FirstName, conReslt.Name);
		//System.debug(resultJson);

		RelatedListController.deleteRecord(con.Id);
		
	}
	
}