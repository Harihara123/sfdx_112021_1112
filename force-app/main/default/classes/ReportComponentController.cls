public with sharing class ReportComponentController {
    @AuraEnabled
    public static String getReportMetadata ( Id reportId ){
        return JSON.serializePretty(Reports.ReportManager.runReport( reportId, true ) );
    }

    @AuraEnabled
    public static String getRecruiterCurrentTalentReport(Id reportId) {
        return reportJSONGenerator.generateJSONForCurrentTalentReport(reportId);
    }

    @AuraEnabled
    public static String getMySubmittalsReport(Id reportId) {
        return reportJSONGenerator.generateJSONForSubmittalsReport(reportId);
    }

    @AuraEnabled
    public static String checkUserOPCO ( ){
        String opco = ServiceTouchpointController.getCurrentUserOpco();

        return opco;
    }
	// Start STORY S-125537 Added by Siva 4/8/2019
	@AuraEnabled
    public static Map<String, String> getCaseRecordTypeIds(){
        Map<String, String> caseRecordTypeIds = new Map<String, String>();
		caseRecordTypeIds.put('talent', Schema.SObjectType.Case.getRecordTypeInfosByName().get('Talent').getRecordTypeId());
		caseRecordTypeIds.put('csc', Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collocated Services Center').getRecordTypeId());
        return caseRecordTypeIds;
    }
	// End STORY S-125537 Added by Siva 4/8/2019
}