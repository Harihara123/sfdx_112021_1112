@isTest
private class ReportComponentControllerTest {
    
    @isTest(seeAllData=true) static void getReportMetadata_validID(){
        Report aReport = [ SELECT Id, Name FROM Report LIMIT 1];
        Test.startTest();
            String reportJSON = ReportComponentController.getReportMetadata(aReport.Id);
        Test.stopTest();

        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(reportJSON);
        Map<String, Object> reportMetadata = (Map<String, Object>)m.get('reportMetadata');
        System.assertEquals( aReport.Name, reportMetadata.get('name'), 'The correct report should be returned' );
    }

    @isTest static void getReportMetadata_invalidID(){
        Id bogusId = '00Q000000000000';
        NoDataFoundException expectedEx;
        String reportJSON;
        try{
            Test.startTest();
                reportJSON = ReportComponentController.getReportMetadata(bogusId);
            Test.stopTest();
        } catch( NoDataFoundException ex ){
            expectedEx = ex;
        }

        System.assertEquals( null, reportJSON, 'No report json should be returned' );
        System.assertNotEquals( null, expectedEx, 'An exception should been thronw to be handled by the front-end' );
    }

    @isTest
    static void getCurrentUserOPCO_givenOPCO_returnsOPCO() {
        String result = '';

        Id uId = UserInfo.getUserId();

        User cUser = [SELECT Id, OPCO__c FROM User WHERE Id =: uId];

        cUser.OPCO__c = 'TEK';

        update cUser;

        Test.startTest();
            result = ReportComponentController.checkUserOPCO();
        	Map<String, String> getCaseRecordTypeIds = ReportComponentController.getCaseRecordTypeIds();
        Test.stopTest();

        System.assertEquals('TEK', result, 'The User\'s OPCO should be returned.');
    }
	 @isTest(seeAllData=true) 
	 static void getCurrentTalentReport(){
        Report aReport = [ SELECT Id, Name FROM Report LIMIT 1];
        Test.startTest();
            String CurrentTalentReport = ReportComponentController.getRecruiterCurrentTalentReport(aReport.Id);
			String SubmittalsReport = ReportComponentController.getMySubmittalsReport(aReport.Id);			
        Test.stopTest();        
    }	
}