public class ReportJSON  {

    public class DataCell {
        public Object value {get; set;}
        public String label {get; set;}
        public DataCell (String label, Object value) {
            this.value = value;
            this.label = label;
        }
    }

    public class DataCells {
        public List<DataCell> dataCells {get; set;}
        public DataCells () {
            dataCells = new List<DataCell>();
        }
    }

    public class FactMap {
        public String key {get; set;}
        public List<DataCells> rows {get; set;}
        public Aggregates aggregates {get; set;}
        public FactMap(String key) {
            this.key = key;
            this.rows = new List<DataCells>();
        }
    }

    public class GroupingsDown {
        public List<Map<String,String>> groupings {get; set;}

        public GroupingsDown() {
            this.groupings = new List<Map<String,String>>();
        }
    }

    public class Aggregates {
        public Integer value {get; set;}
        public String label {get; set;}
    }

    public class DetailColumnInfo {        
        public String name {get; set;}
        public String label {get; set;}
        public String dataType {get; set;}
        public DetailColumnInfo(String name, string label) {
            this.name = name;
            this.label = label;
        }

        public DetailColumnInfo(String name, string label, String dataType) {
            this.name = name;
            this.label = label;
            this.dataType = dataType;
        }
    }

    public class GroupingColumnInfo {
        public String name {get; set;}
        public String label {get; set;}
        public String dataType {get; set;}
        public Integer groupingLevel {get; set;}
    }

    public class ReportExtendedMetadata {
        public Map<String, DetailColumnInfo> detailColumnInfo {get; set;}
        public Map<String, GroupingColumnInfo> groupingColumnInfo {get; set;}
        public ReportExtendedMetadata () {
           this.detailColumnInfo = new Map<String, DetailColumnInfo>();
           this.groupingColumnInfo = new Map<String, GroupingColumnInfo>();
        }
    }

    public class ReportMetadata {
        public List<String> detailColumns {get; set;}
        public String name {get; set;}
        public ReportMetadata () {
            this.detailColumns = new List<String>();
        }
    }

    public class RootObject {
        public Map<String, FactMap> factMap {get; set;}
        public GroupingsDown groupingsDown {get; set;}
        public ReportExtendedMetadata reportExtendedMetadata {get; set;}
        public ReportMetadata reportMetadata {get; set;}
    }
}