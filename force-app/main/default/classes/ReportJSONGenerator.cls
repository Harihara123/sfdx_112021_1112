public class ReportJSONGenerator  {
    
    public static String generateJSONForCurrentTalentReport(String reportId) {

        ReportJSON.FactMap factMap = new ReportJSON.FactMap('T!T');        
        ReportJSON.GroupingsDown groupingsDown = new ReportJSON.GroupingsDown();
        ReportJSON.ReportExtendedMetadata reportExtendedMetadata = new ReportJSON.ReportExtendedMetadata();
        ReportJSON.ReportMetadata reportMetadata = new ReportJSON.ReportMetadata();

        ReportJSON.RootObject reportInstance = new ReportJSON.RootObject();
        reportInstance.factMap = new Map<String, ReportJSON.factMap>();

        //get the set of accountIds to filter this query.  'My Account and Account Team' ownership

        Id contactTalentRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        for(Contact record : [  SELECT Id, Name,
                                    AccountId, 
                                    Account.Name, 
                                    Account.Talent_Start_Date__c, 
                                    Account.Talent_End_Date__c, 
                                    Service_Status__c, 
                                    Days_Since_Service__c, 
                                    Account.Talent_Current_Employer_Formula__c,
									CreatedDate
                                FROM 
                                    Contact
                                WHERE
                                    RecordTypeId =: contactTalentRT
                                    AND Candidate_Status__c = 'Current'
                                    AND accountId IN: myAccountAndAccountTeamFilter(UserInfo.getUserId()) ORDER BY Account.Talent_End_Date__c ASC
                                LIMIT 2000
                              ]) {

            ReportJSON.DataCells row = new ReportJSON.DataCells();
            // Match Talent
            row.dataCells.add(new ReportJSON.DataCell(record.Id, 'MatchTalentURL'));
            //Account.Name
            row.dataCells.add(new ReportJSON.DataCell(record.Account.Name, record.AccountId));
            //Account.Talent_Start_Date__c
            row.dataCells.add(new ReportJSON.DataCell((record.Account.Talent_Start_Date__c != null) ? record.Account.Talent_Start_Date__c.format() : '-', record.Account.Talent_Start_Date__c));
            //Account.Talent_End_Date__c
            row.dataCells.add(new ReportJSON.DataCell((record.Account.Talent_End_Date__c != null) ? record.Account.Talent_End_Date__c.format() : '-', record.Account.Talent_End_Date__c));
            //Service_Status__c
            row.dataCells.add(new ReportJSON.DataCell((record.Service_Status__c != null) ? record.Service_Status__c : '-', record.Service_Status__c));
            //Days_Since_Service__c
            row.dataCells.add(new ReportJSON.DataCell((record.Days_Since_Service__c != null) ? String.valueOf(record.Days_Since_Service__c) : '-', record.Days_Since_Service__c));
            //Account.Talent_Current_Employer_Formula__c
            row.dataCells.add(new ReportJSON.DataCell((record.Account.Talent_Current_Employer_Formula__c != null) ? String.valueOf(record.Account.Talent_Current_Employer_Formula__c) : '-', record.Account.Talent_Current_Employer_Formula__c));
            //Name //STORY S-125537 Added Contact's Name, Id by Siva 4/8/2019
            row.dataCells.add(new ReportJSON.DataCell(record.Name, record.Id));
			factMap.rows.add(row);
        }

        if(factMap.rows.size() > 0) {
            reportInstance.factMap.put('T!T', factMap);
        }
		//STORY S-124835 Changed Talent Start Date, Talent End Date and Service Status Labels by Siva 3/26/2019
        reportExtendedMetadata.detailColumnInfo.put('Contact.Match', new ReportJSON.DetailColumnInfo('Contact.Match', 'Match'));

        reportExtendedMetadata.detailColumnInfo.put('Account.Name', new ReportJSON.DetailColumnInfo('Account.Name', 'Talent Name'));
        reportExtendedMetadata.detailColumnInfo.put('Account.Talent_Start_Date__c', new ReportJSON.DetailColumnInfo('Account.Talent_Start_Date__c', 'Start Date'));
        reportExtendedMetadata.detailColumnInfo.put('Account.Talent_End_Date__c', new ReportJSON.DetailColumnInfo('Account.Talent_End_Date__c', 'End Date'));
        reportExtendedMetadata.detailCOlumnInfo.put('Contact.Service_Status__c', new ReportJSON.DetailColumnInfo('Contact.Service_Status__c', 'Status'));
        reportExtendedMetadata.detailColumnInfo.put('Contact.Days_Since_Service__c', new ReportJSON.DetailColumnInfo('Contact.Days_Since_Service__c', 'Days Since Service'));
        reportExtendedMetadata.detailColumnInfo.put('Account.Talent_Current_Employer_Formula__c', new ReportJSON.DetailColumnInfo('Account.Talent_Current_Employer_Formula__c', 'Account Name'));
        try {
            if(String.isNotBlank(reportId)) {
                Report thisReport = [SELECT Id, Name, DeveloperName, Format FROM Report WHERE Id =: reportId];
                reportMetadata.name = thisReport.Name;
            }
        } catch(Exception ex) {
            throw new NoDataFoundException();
        }

        reportMetadata.detailColumns.add('Contact.Match');
        reportMetadata.detailColumns.add('Account.Name');
        reportMetadata.detailColumns.add('Account.Talent_Start_Date__c');
        reportMetadata.detailColumns.add('Account.Talent_End_Date__c');
        reportMetadata.detailColumns.add('Contact.Service_Status__c');
        reportMetadata.detailColumns.add('Contact.Days_Since_Service__c');
        reportMetadata.detailColumns.add('Account.Talent_Current_Employer_Formula__c');

        groupingsDown.groupings = null;

        reportInstance.groupingsDown = groupingsDown;    
        reportInstance.reportExtendedMetadata = reportExtendedMetadata; 
        reportInstance.reportMetadata = reportMetadata;

        String jsonResults = JSON.serialize(reportInstance);
        return jsonResults;
    }

    public static String generateJSONForSubmittalsReport(String reportId) {
        String jsonResults = '';

        ReportJSON.RootObject reportInstance = new ReportJSON.RootObject();

        Map<String, List<Order>> submittalMap = ReportJSONGenerator.getSubmittalsForReport();

        ReportJSON.FactMap factMap = new ReportJSON.FactMap('T!T');        
        ReportJSON.GroupingsDown gd = new ReportJSON.GroupingsDown();
        ReportJSON.ReportExtendedMetadata rem = new ReportJSON.ReportExtendedMetadata();
        ReportJSON.ReportMetadata reportMetadata = new ReportJSON.ReportMetadata();

        reportMetadata.detailColumns.add('Order.ShipToContact.Name');
        reportMetadata.detailColumns.add('Order.Status');
        reportMetadata.detailColumns.add('Order.Display_LastModifiedDate__c');
        reportMetadata.detailColumns.add('Order.ShipToContact.Title');
        reportMetadata.detailColumns.add('Order.Opportunity.View_Talent_Details__c');


        try {
            if(String.isNotBlank(reportId)) {
                Report thisReport = [SELECT Id, Name, DeveloperName, Format FROM Report WHERE Id =: reportId];
                reportMetadata.name = thisReport.Name;
            }
        } catch(Exception ex) {
            throw new NoDataFoundException();
        }


        reportInstance.reportMetadata = reportMetadata;


        ReportJSON.GroupingColumnInfo gci = new ReportJSON.GroupingColumnInfo();
        gci.name = 'Order.Opportunity.Name';
        gci.label = 'Opportunity Name';
        gci.dataType = 'STRING_DATA';
        gci.groupingLevel = 0;
         

        rem.groupingColumnInfo.put('Order.Opportunity.Name', gci);
       

        rem.detailColumnInfo.put('Order.Display_LastModifiedDate__c', new ReportJSON.DetailColumnInfo('Order.Display_LastModifiedDate__c', System.Label.ATS_LAST_MODIFIED_DATE, 'DATETIME_DATA'));
        rem.detailColumnInfo.put('Order.ShipToContact.Title', new ReportJSON.DetailColumnInfo('Order.ShipToContact.Title', System.Label.ATS_CURRENT_TITLE, 'STRING_DATA'));
        rem.detailColumnInfo.put('Order.Status', new ReportJSON.DetailColumnInfo('Order.Status', System.Label.ATS_STATUS, 'PICKLIST_DATA'));
        rem.detailColumnInfo.put('Order.ShipToContact.Name', new ReportJSON.DetailColumnInfo('Order.ShipToContact.Name', System.Label.ATS_TALENT_NAME, 'STRING_DATA'));
        rem.detailColumnInfo.put('Order.Opportunity.View_Talent_Details__c', new ReportJSON.DetailColumnInfo('Order.Opportunity.View_Talent_Details__c', System.Label.ATS_View_Talent_Details, 'STRING_DATA'));
        reportInstance.factMap = new Map<String, ReportJSON.factMap>();
        Integer groupNumber = 0;
        for(String key : submittalMap.keySet()) {
            Map<String,String> tempMap = new Map<String,String>();
            tempMap.put('value', submittalMap.get(key)[0].Opportunity.Id);
            tempMap.put('label', key);
            tempMap.put('key', String.valueOf(groupNumber));
            tempMap.put('groupings', null);

            gd.groupings.add(tempMap);

            reportInstance.factMap.put(String.valueOf(groupNumber) + '!T', generateFactMapGroup(groupNumber, submittalMap.get(key)));

            groupNumber++;
        }      


        reportInstance.groupingsDown = gd;
        reportInstance.reportExtendedMetadata = rem;

        return JSON.serializePretty(reportInstance);
    }


    public static ReportJSON.FactMap generateFactMapGroup(Integer groupingNumber, List<Order> submittals) {
        Map<String, ReportJSON.FactMap> returnMap = new Map<String, ReportJSON.FactMap>();

        ReportJSON.FactMap fMap = new ReportJSON.FactMap(String.valueOf(groupingNumber) + '!T');
        ReportJSON.Aggregates agg = new ReportJSON.Aggregates();

        agg.label = String.valueOf(submittals.size());
        agg.value = submittals.size();

        fMap.aggregates = agg;

        for(Order submittal : submittals) {

            ReportJSON.DataCells row = new ReportJSON.DataCells();
            row.dataCells.add(new ReportJSON.DataCell(submittal.ShipToContact.Name, submittal.ShipToContactId));
            row.dataCells.add(new ReportJSON.DataCell(submittal.Status, submittal.Status));
            row.dataCells.add(new ReportJSON.DataCell(submittal.Display_LastModifiedDate__c.format('MMM dd, yyyy'), String.valueOf(submittal.Display_LastModifiedDate__c)));
            row.dataCells.add(new ReportJSON.DataCell((submittal.ShipToContact.Title != null ? submittal.ShipToContact.Title : '-'), submittal.ShipToContact.Title));
            row.dataCells.add(new ReportJSON.DataCell(submittal.Opportunity.View_Talent_Details__c, submittal.Opportunity.View_Talent_Details__c));
            fMap.rows.add(row);
        }

        return fMap;
    }


    public static Map<String, List<Order>> getSubmittalsForReport() {
        List<String> allowedStatus = new List<String>{'Submitted', 'Interviewing', 'Offer Accepted'};
        Id userId = UserInfo.getUserId();

        Map<String, List<Order>> submittalMap = new Map<String, List<Order>>();

        List<Order> submittals = [

            SELECT 
                Id, 
                Opportunity.Name, 
                Opportunity.View_Talent_Details__c,
                OpportunityId,
                ShipToContactId,
                ShipToContact.Name,
                LastModifiedDate,
                Display_LastModifiedDate__c,
                ShipToContact.Title,
                Status
            FROM Order 
            WHERE Status IN: allowedStatus 
            AND Display_LastModifiedDate__c = LAST_90_DAYS
            AND OwnerId =: userId
            ORDER BY LastModifiedDate DESC
        ];

        for(Order o : submittals) {
            if(submittalMap.get(o.Opportunity.Name) != null) {
                List<Order> temp = submittalMap.get(o.Opportunity.Name);
                temp.add(o);
                submittalMap.put(o.Opportunity.Name, temp);
            } else {
                List<Order> newList = new List<Order>{o};
                submittalMap.put(o.Opportunity.Name, newList);
            }
        }
        
        return submittalMap;

    }


    public static Set<Id> myAccountAndAccountTeamFilter(Id userId) {
        Set<Id> accountIds = new Set<Id>();
        for(AccountTeamMember member : [SELECT Id, AccountId FROM AccountTeamMember WHERE UserId =: userId LIMIT 2000]) {
            accountIds.add(member.AccountId);
        }
        Set<Id> filter = new Set<Id>();
        for(Account acc : [SELECT Id FROM Account WHERE Id IN: accountIds OR OwnerId =: userId LIMIT 2000]) {
            filter.add(acc.Id);
        }
        return filter;
    }
}