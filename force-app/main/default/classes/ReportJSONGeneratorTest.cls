@istest
private class ReportJSONGeneratorTest  {
    
    @isTest
    static void generateJSONForCurrentTalentReport() {
        //data setup
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;
        talentAcc.G2_Completed__c = true;
        talentAcc.Candidate_Status__c = 'Current';
        insert talentAcc;

        Contact ct = TestData.newContact(talentAcc.id, 1, 'Talent'); 
        ct.Email = 'test@allegisgroup.com';
        ct.Title = 'test title';
        ct.Talent_State_Text__c = 'test text';
        ct.MailingState = 'test text';
        insert ct;

        Test.startTest();
            String results = ReportJSONGenerator.generateJSONForCurrentTalentReport('');
        Test.stopTest();
        System.assertNotEquals(results, null, 'should not be blank');
        Map<String, Object> reportInstance = (Map<String, Object>)JSON.deserializeUntyped(results);
        Map<String, Object> factMap = (Map<String,Object>)reportInstance.get('factMap');
        Map<String, Object> TT = (Map<String, Object>)factMap.get('T!T');
        List<Object> rows = (List<Object>)TT.get('rows');
        System.assertEquals(rows.size(), 1, 'Should have 1 row');
    }

    @isTest 
    static void generateJSONForSubmittalsReport_givenOneOpportunity_shouldHaveOneRows() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account talentAcc = TestData.newAccount(1, 'Client');
        talentAcc.Name = 'Test ACC';
        insert talentAcc;

        Opportunity req = TestData.newOpportunity(talentAcc.Id, 1);
        insert req;

        Order submittal = new Order();
        submittal.OpportunityId = req.Id;
        submittal.AccountId = talentAcc.Id;
        submittal.EffectiveDate = Date.today();
        submittal.Status = 'Interviewing';
        insert submittal;

        Test.startTest();
            String results = ReportJSONGenerator.generateJSONForSubmittalsReport('');
        Test.stopTest();

        System.assertNotEquals(results, null, 'should not be blank');

        Map<String, Object> reportInstance = (Map<String, Object>)JSON.deserializeUntyped(results);

        Map<String, Object> factMap = (Map<String,Object>)reportInstance.get('factMap');
        Set<String> groupings = factMap.keySet();
        System.assertEquals(1, groupings.size(), 'Should have 1 grouping');
    }

    @isTest 
    static void generateJSONForSubmittalsReport_givenTwoOpportunities_shouldHaveTwoRows() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account talentAcc = TestData.newAccount(1, 'Client');
        talentAcc.Name = 'Test ACC';
        insert talentAcc;

        Opportunity req = TestData.newOpportunity(talentAcc.Id, 1);
        insert req;

        Opportunity req2 = TestData.newOpportunity(talentAcc.Id, 2);
        insert req2;

        Order submittal = new Order();
        submittal.OpportunityId = req.Id;
        submittal.AccountId = talentAcc.Id;
        submittal.EffectiveDate = Date.today();
        submittal.Status = 'Interviewing';
        insert submittal;

        Order submittal2 = new Order();
        submittal2.OpportunityId = req2.Id;
        submittal2.AccountId = talentAcc.Id;
        submittal2.EffectiveDate = Date.today();
        submittal2.Status = 'Interviewing';
        insert submittal2;

        Test.startTest();
            String results = ReportJSONGenerator.generateJSONForSubmittalsReport('');
        Test.stopTest();

        System.assertNotEquals(results, null, 'should not be blank');

        Map<String, Object> reportInstance = (Map<String, Object>)JSON.deserializeUntyped(results);

        Map<String, Object> factMap = (Map<String,Object>)reportInstance.get('factMap');
        Set<String> groupings = factMap.keySet();
        System.assertEquals(2, groupings.size(), 'Should have 1 grouping');
    }
}