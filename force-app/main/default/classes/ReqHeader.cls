public with sharing class ReqHeader {

    @AuraEnabled(cacheable=false)    
    public static String initOppDetails(id recordId) {        
        FormattedData data = new FormattedData();

        data.details = getOppDetails(recordId);
        data.submittals = getOppSubmittalsCount(recordId);
        data.following = followRecord(recordId, true, true);

        return JSON.serialize(data);        
    }

    @AuraEnabled(cacheable=false)    
    public static SObject getOppDetails(id recordId) {  
        SObject returnObj = new Opportunity();      
        try {
            List<Opportunity> results = [
                SELECT Req_Job_Title__c, Name, Account.Name, Account.Id, Req_Hiring_Manager__r.Name,
                Req_Hiring_Manager__r.Id, Owner.Name, Owner.Id, Organization_Office__r.Name, Organization_Office__r.Id,
                Req_Open_Positions__c, StageName, Req_Grade__c, Req_Days_Open__c, IsClosed,Req_Product__c,Req_Duration__c,Req_Duration_Unit__c,
                Req_Bill_Rate_Max__c, Req_Pay_Rate_Max__c, Req_Skill_Specialty__c,Legacy_Product__r.Id, Legacy_Product__r.Name, Req_Qualifying_Stage__c,
                Probability, Start_Date__c, Req_Total_Positions__c, Req_Sub_Interviewing__c, Immediate_Start__c, Req_Exclusive__c, CreatedDate, CloseDate, Ed_Outcome__c
                FROM Opportunity
                WHERE Id =: recordId            
                LIMIT 1
            ];
            if (results!=null && !results.isEmpty()) {
                returnObj = results[0];
            }
            
        } catch (Exception e) {            
            system.debug(e);
        } 
        return returnObj;       
    }

    @AuraEnabled(cacheable=false) 
    public static Integer getOppSubmittalsCount(id recordId) {
        Integer count = -1;
        try {
            count = [SELECT count() FROM Order WHERE OpportunityId =: recordId AND status NOT IN ('Linked','Applicant')];           
        } catch (Exception e) {
            system.debug(e);
        }
        return count;
    }

    @AuraEnabled(cacheable=false) 
    public static Boolean followRecord(id recordId,Boolean isFollowed,Boolean OnLoad) {
        Boolean follow=false;
        system.debug('Har'+follow);
        try{
            if(OnLoad)   {
                EntitySubscription entitysubscription = [SELECT id FROM EntitySubscription WHERE parentId =: recordId AND subscriberId =: UserInfo.getUserId() LIMIT 1];
                if(entitysubscription!=null){
                    follow=true;
                }
                else{
                    follow=false;
                }
            }
            else {
                if(isFollowed == false && !OnLoad){
                    EntitySubscription entitysubscription = new EntitySubscription(parentId=recordId, SubscriberId=UserInfo.getUserId());
                    insert entitysubscription;
                    follow=true;
                    system.debug('har2'+follow);
                }
                if(isFollowed == true && !OnLoad){
                    EntitySubscription entitysubscription = [SELECT id FROM EntitySubscription WHERE parentId =: recordId AND subscriberId =: UserInfo.getUserId() LIMIT 1];
                    delete entitysubscription;
                    follow=false;
                    system.debug('har3'+follow);
                    
                }
                
            }
            
        }
        catch(Exception except){
            system.debug(except);
            
        }
        system.debug('har4'+follow);
        return follow;       
        
    }

    public class FormattedData {
        public SObject details;
        public Integer submittals;
        public Boolean following;
    }
    
    @AuraEnabled(cacheable=false) 
    public static Opportunity getOpportunity(id recordId) {
       
        return [Select Id, OPCO__c from Opportunity where Id =: recordId limit 1];
    }

}