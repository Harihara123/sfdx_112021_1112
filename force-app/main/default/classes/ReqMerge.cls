/*************************************************************************************************
Apex Class Name :  ReqMerge
Version         : 1.0 
Created Date    : April 06 2015
Function        : Class used to Merge VMS Req with Pro-active Req.  
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aditya                     04/06/2015              Original Version
**************************************************************************************************/ 
public class ReqMerge {

    private final Reqs__c reqObj;
    public String ID = 'id';
    public String PAreqid;
    public String VMSRequirementStatus;
    public String VMSRequirementClosedReason;
    public Integer MaxSubmissionsPerSupplier;
    public String VMSEndClient;
    public String ReqOriginationSystem;
    public String OriginationSystemReqName;
    public String MergedReqID;
    public String MergedReqSystem;
    public String DeclineReason = 'Req already entered manually';

    //Constructor
    public ReqMerge(ApexPages.StandardController stdcontroller) {
        this.reqObj = (Reqs__c)stdcontroller.getRecord(); 
        system.debug ('--------------------------test'+ reqObj); 
    }

    /* 
        Method to Merge VMS Req with Pro-active Req
    */
    public PageReference MergeReqs() {

        ID = reqObj.id;

        pageReference reqPage;

        /*
             list pareqs for Pro-active Req
             list vmsreq for VMS Req
        */
        list<Reqs__c> pareqs = new list<Reqs__c>();
        list<Reqs__c> vmsreq = new list<Reqs__c>();
       
        //Get Pro-active Req Id and VMS Req details
        vmsreq = [select VMS_Requirement_Status__c,VMS_Requirement_Closed_Reason__c,Max_Submissions_Per_Supplier__c,VMS_End_Client__c,id,Proactive_Req_Link__c, Req_Origination_System__c,Origination_System_Req_Name__c,Merged_Req_ID__c,Merged_Req_System__c from Reqs__c
                  where id=: ID];
         
        for(Reqs__c vms: vmsreq) {           
           
            PAreqid = vms.Proactive_Req_Link__c;
            VMSRequirementStatus = vms.VMS_Requirement_Status__c;
            VMSRequirementClosedReason = vms.VMS_Requirement_Closed_Reason__c;
            MaxSubmissionsPerSupplier = integer.valueOf(vms.Max_Submissions_Per_Supplier__c);
            VMSEndClient = vms.VMS_End_Client__c;
            MergedReqSystem = vms.Req_Origination_System__c;
            MergedReqID = vms.Origination_System_Req_Name__c;
            
        }
        //Assigning Decline Reason to make VMS req to close
        reqObj.Decline_Reason__c = DeclineReason;
        
        // Get Pro-active req details
        pareqs = [select VMS_Requirement_Status__c,VMS_Requirement_Closed_Reason__c,Max_Submissions_Per_Supplier__c,VMS_End_Client__c,id,Name,Proactive_Req__c,Proactive_Req_Link__c, Req_Origination_System__c from Reqs__c
                  where id =: PAreqid];
        
        for (Reqs__c prs: pareqs) {
        
            prs.VMS_Requirement_Status__c = VMSRequirementStatus;
            prs.VMS_Requirement_Closed_Reason__c = VMSRequirementClosedReason;
            prs.Max_Submissions_Per_Supplier__c = MaxSubmissionsPerSupplier;
            prs.VMS_End_Client__c = VMSEndClient;
            prs.VMS__c = True;
            prs.Proactive_Req__c = True;
            prs.Merged_with_VMS_Req__c = True;
        
                
            prs.Merged_Req_ID__c = MergedReqID;
            prs.Merged_Req_System__c = MergedReqSystem;
        
            reqObj.Merged_Req_ID__c = prs.Name;
            reqObj.Merged_Req_System__c = prs.Req_Origination_System__c;
                                
        }

        update pareqs;
        update reqObj;

        //Re-directing the Page to Pro-active req
        string returnurl;
        returnurl='/'+ PAreqid;
        return new ApexPages.PageReference(returnurl).SetRedirect(true);
    }

private final ApexPages.StandardController controller;

}