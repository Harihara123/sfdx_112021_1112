public class ReqPositionLogHelper{
    //wrapper class to maintain the req field mapping
    public class Req_FieldUpdateCheckForPositionLogs
    {
        public string associatedFieldLabel{get;set;}
        public string associatedFieldAPIName{get;set;}
        public boolean needReasonForChange{get;set;}
        public string reasonFieldAPIName{get;set;}
        // constructor
        public Req_FieldUpdateCheckForPositionLogs(string associatedFieldLabel,string associatedFieldAPIName,boolean needReasonForChange,string reasonFieldAPIName)
        {
           this.associatedFieldLabel = associatedFieldLabel;
           this.associatedFieldAPIName = associatedFieldAPIName;
           this.needReasonForChange = needReasonForChange;
           this.reasonFieldAPIName = reasonFieldAPIName; 
        }
    }
    // method to create data for the wrapper class
    public static List<Req_FieldUpdateCheckForPositionLogs> getReqFieldInformation()
    {
           List<Req_FieldUpdateCheckForPositionLogs> reqFieldInfo = new List<Req_FieldUpdateCheckForPositionLogs>();
           reqFieldInfo.add(new Req_FieldUpdateCheckForPositionLogs('Fill','Filled__c',false,''));
           reqFieldInfo.add(new Req_FieldUpdateCheckForPositionLogs('Wash','Wash__c',true,'Loss_Wash_Reason_Selected__c'));
           reqFieldInfo.add(new Req_FieldUpdateCheckForPositionLogs('Lost','Loss__c',true,'Loss_Wash_Reason_Selected__c'));
           
           return reqFieldInfo;
    }
}