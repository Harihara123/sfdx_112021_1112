public class ReqRouterActionTriggerHandler {
    private static boolean hasBeenProccessed = false;
    
    public void ReqRouterActionTriggerHandler() {
    }
    
	public void syncToGCP(List<Req_Router_Action__c> syncList) {
        if (!Test.isRunningTest() && syncList != null && syncList.size() > 0) {
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'ReqRouterActionTriggerHandler', 'syncToGCP', 
                        'Triggered ' + syncList.size() + ' Req_Router_Action__c records: ' + CDCDataExportUtility.joinObjIds(syncList));
                }
                PubSubBatchHandler.insertDataExteractionRecord(syncList, 'Req_Router_Action__c');
                hasBeenProccessed = true; 
            }
        }
    }
}