@isTest 
public class ReqRoutingControllerTest  {
	
	@testSetup
	static void setup() {
		DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        	 insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='True');
        insert DRZSettings1;
		TestData tdAccts = new TestData(1);
		TestData tdConts = new TestData();
		string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
		List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
		Job_Title__c jt1 = new Job_Title__c();
		jt1.Name = 'test1 Title';
		insert jt1;

		Product2 prod = new Product2(Name = 'New Product', 
			Division_Name__c = 'Aerotek',
			OpCo__c = 'Aerotek, Inc',
			Category_Id__c = '000025',
			Category__c = 'Welder',
			Job_Code__c = 'Welder',
			OpCo_Id__c = 'ONS',
			Segment_Id__c = 'POWER',
			Segment__c = 'Traditional Power',
			Skill_Id__c = '000926',
			Skill__c = 'Welder - Submerged',
			Jobcode_Id__c = '600091');
        
        insert prod;
        

		string rectype='Talent';
        List<Contact> talentcontLst = tdConts.createContacts(rectype);
		List<Account> talentAccList = tdAccts.createAccounts();

		 string clientRectype='Client';
        List<Contact> clientContLst = tdConts.createContacts(clientRectype);

		Opportunity newOpp = new Opportunity();
        newOpp.Name = 'New ReqOpportunity';
        newOpp.Accountid = talentAccList[0].Id;
        newOpp.RecordTypeId = ReqRecordType;
                
        Date closingdate = system.today();
        newOpp.CloseDate = closingdate.addDays(25);
        newOpp.Req_Division__c = 'Aerotek';
        newOpp.StageName = 'Draft';
        newOpp.Req_Total_Positions__c = 2.0;
        newOpp.Req_Client_Job_Title__c = jt1.Id;
        newOpp.Req_Job_Description__c = 'Testing';
        newOpp.Req_HRXML_Field_Updated__c = true;
        newOpp.Req_Hiring_Manager__c = clientContLst[0].Id;
        newOpp.Client_Relationship__c = 'New Client';
        newOpp.DeliveryModel__c = 'Advisory';
        newOpp.Global_Services_Opportunity_Type__c='Renewal/Extension';
        newOpp.Location__c ='Local Field Office';
                
        
        newOpp.Opco__c = 'Aerotek, Inc';
        newOpp.BusinessUnit__c = 'Board Practice';
        newOpp.Req_Product__c = 'Permanent';
        newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
        newOpp.Req_Worksite_Street__c = '987 Hidden St';
        newOpp.Req_Worksite_City__c = 'Baltimore';
        newOpp.Req_Worksite_Postal_Code__c = '21228';
        newOpp.Req_Worksite_Country__c = 'United States';
        newOpp.Legacy_Product__c = prod.Id;
        newOpp.EnterpriseReqSkills__c = '[{"name":"AS/400","favorite":true},{"name":" RPG","favorite":true},{"name":" Cobol","favorite":true}]';
        newOpp.Currency__c = 'USD - U.S Dollar';
        newOpp.Req_Job_Title__c = 'Clinical Program Administrator';
        newOpp.Req_Qualification__c = 'test';
		newOpp.StageName = 'Started';
        newOpp.Apex_Context__c = true; 
        newOpp.Amount = 20000;
        newOpp.Description ='Test';
        newOpp.Type='New Business';
        newOpp.Global_Services_Opportunity_Type__c='New Business';
        newOpp.Pricing_Approved_By_SSG__c =true;
        newOpp.Project_Length_Duration_Weeks__c=100;
        newOpp.Describe_Customer_Pain__c='Testing';
        newOpp.Desired_Business_Outcomes__c='Testing';
        newOpp.How_will_we_win__c='Testing';
        newOpp.What_is_our_Value_message__c = 'Testing';
        newOpp.Start_Date__c = Date.today();
        newOpp.Services_GP__c = 10;
        newOpp.Document_Request_Date__c = Date.today();
        newOpp.Level_of_Scope_Definition__c = 'Precisely Defined';
        newOpp.Billing_Information__c = 'New York USA';
        newOpp.Delivery_Organization__c = 'Deployment';
        newOpp.Delivery_Readiness__c = 'Early Qualification ? Delivery Not Yet Engaged';
        newOpp.Discipline_Skills_Required__c = 'Product Engineering';
        newOpp.Number_of_Resources__c = 500;
        newOpp.Pricing_Billing_Type__c = 'Fixed Price';
        newOpp.Primary_Service_Type__c ='Biometrics';
        newOpp.Revenue_for_Current_Year_Only__c =2000;
        newOpp.Value_Added_Model__c ='Level 1 ? Selection of Resources';
        newOpp.Account_Prioritization__c = 'A';

		insert newOpp;
		Profile p = [SELECT id, Name FROM Profile WHERE Name ='System Administrator' LIMIT 1 ];
		User u = new User(
            CompanyName = 'TestComp',
            FirstName = 'first',
            LastName = 'last',
            LocaleSidKey = 'en_CA',
            UserName = 'testfirst.last@allegisgroup.com',
            TimeZoneSidKey = 'America/Indiana/Indianapolis',
            Email = 'testfrt.last@allegisgroup.com',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Alias = 't12345',
            Team__c = 'Birmingham AC Perm',
            OPCO__c = 'TEM',
            Region__c='POPULUS',
            Region_Code__c='POPUL',
            ProfileId = p.Id
        );

		insert u;


        Req_Routing_Search_Criteria__c criteria = new Req_Routing_Search_Criteria__c(
            Search_Criteria__c = '{"FilterCriteria":[{"fieldSlug":"isAllocation","component":"toggle","reqSearchParams":["flt.allocated_recruiter_count"],"displayTitle":{"en_US":"Has Allocation","fr_CA":"Has Allocation"},"defaultFacet":true,"selected":true,"selectable":false,"isToggle":true,"selectedValues":[]},{"fieldSlug":"isExclusive","component":"toggle","reqSearchParams":["flt.is_exclusive"],"displayTitle":{"en_US":"Exclusive","fr_CA":"Exclusive"},"defaultFacet":true,"selected":true,"selectable":false,"isToggle":true,"selectedValues":[]},{"fieldSlug":"isRemote","component":"toggle","reqSearchParams":["flt.work_remote"],"displayTitle":{"en_US":"Remote","fr_CA":"Remote"},"defaultFacet":true,"selected":true,"selectable":false,"isToggle":true,"selectedValues":[]},{"fieldSlug":"isVMS","component":"toggle","reqSearchParams":["flt.req_vms"],"displayTitle":{"en_US":"VMS","fr_CA":"VMS"},"defaultFacet":true,"selected":true,"selectable":false,"isToggle":true,"selectedValues":[]},{"fieldSlug":"isSubVendor","component":"toggle","reqSearchParams":["flt.can_use_approved_sub_vendor"],"displayTitle":{"en_US":"Can Use Approved Sub-Vendor","fr_CA":"Can Use Approved Sub-Vendor"},"defaultFacet":true,"selected":true,"selectable":false,"isToggle":true,"selectedValues":[]},{"fieldSlug":"skill_specialty","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.req_skill_specialty","facetValue":"true"},{"facetKey":"facetOption.req_skill_specialty.size","facetValue":"50"},{"facetKey":"facetOption.req_skill_specialty.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.req_skill_specialty_filter.req_skill_specialty.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetFlt.req_skill_specialty"],"displayTitle":{"en_US":"Skill Specialties","fr_CA":"Skill Specialties"},"allowFreeText":false,"defaultFacet":true,"selected":true,"selectable":true,"isToggle":false,"selectedValues":[]},{"fieldSlug":"req_industy","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.req_skill_specialty","facetValue":"true"},{"facetKey":"facetOption.req_skill_specialty.size","facetValue":"50"},{"facetKey":"facetOption.req_skill_specialty.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.req_skill_specialty_filter.req_skill_specialty.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetFlt.req_skill_specialty"],"displayTitle":{"en_US":"Industries","fr_CA":"Industries"},"allowFreeText":false,"defaultFacet":true,"selected":false,"selectable":false,"isToggle":false,"selectedValues":[]},{"fieldSlug":"organization_name","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.organization_name","facetValue":"true"},{"facetKey":"facetOption.organization_name.size","facetValue":"50"},{"facetKey":"facetOption.organization_name.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.organization_name_filter.organization_name.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetFlt.organization_name"],"displayTitle":{"en_US":"Companies","fr_CA":"Companies"},"allowFreeText":false,"defaultFacet":true,"selected":true,"selectable":true,"isToggle":false,"selectedValues":[]},{"fieldSlug":"req_stage","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.req_stage","facetValue":"true"},{"facetKey":"facetOption.req_stage.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.req_stage_filter.req_stage.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetFlt.req_stage"],"displayTitle":{"en_US":"Stages","fr_CA":"Stages"},"allowFreeText":false,"defaultFacet":true,"selected":true,"selectable":true,"isToggle":false,"selectedValues":[]},{"fieldSlug":"req_location","component":"lwcFacetLocation","reqSearchParams":["location"],"displayTitle":{"en_US":"Location (radius is set to 30 miles)","fr_CA":"Location (radius is set to 30 miles)"},"allowFreeText":false,"defaultFacet":true,"selected":true,"selectable":true,"isToggle":false,"selectedValues":[]},{"fieldSlug":"office","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.office","facetValue":"true"},{"facetKey":"facetOption.office.size","facetValue":"50"},{"facetKey":"facetOption.office.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.office_filter.office.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetFlt.office"],"displayTitle":{"en_US":"Office","fr_CA":"Bureau"},"allowFreeText":false,"defaultFacet":false,"selected":false,"selectable":true,"isToggle":false,"selectedValues":[]},{"fieldSlug":"req_skills","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.job_skills","facetValue":"true"},{"facetKey":"facetOption.job_skills.skills.size","facetValue":"50"},{"facetKey":"facetOption.job_skills.skills.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.job_skills_filter.job_skills.skills.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetOption.job_skills.skills.selected","nested_facet_filter.job_skills"],"displayTitle":{"en_US":"Skills","fr_CA":"Compétences"},"allowFreeText":false,"defaultFacet":false,"selected":false,"selectable":true,"isToggle":false,"selectedValues":[]},{"fieldSlug":"job_title","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.position_title","facetValue":"true"},{"facetKey":"facetOption.position_title.size","facetValue":"50"},{"facetKey":"facetOption.position_title.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.position_title_filter.position_title.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetOption.position_title.selected"],"displayTitle":{"en_US":"Job Title","fr_CA":"Job Title"},"allowFreeText":true,"defaultFacet":false,"selected":false,"selectable":false,"isToggle":false,"selectedValues":[]},{"fieldSlug":"req_owner","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.owner_name","facetValue":"true"},{"facetKey":"facetOption.owner_name.size","facetValue":"50"},{"facetKey":"facetOption.owner_name.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.owner_name_filter.owner_name.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetFlt.owner_name"],"displayTitle":{"en_US":"Req Owner","fr_CA":"Req Owner"},"allowFreeText":false,"defaultFacet":false,"selected":false,"selectable":true,"isToggle":false,"selectedValues":[]}]}}',
            Search_Name__c = 'DefaultSearchCriteria',
            Search_Type__c = 'DefaultSearchCriteria'
        );
        insert criteria;
        
        ApigeeOAuthSettings__c apigee = new ApigeeOAuthSettings__c();
        apigee.Name = 'Req Search Service';
        apigee.Service_URL__c = 'https://api-tst.allegistest.com/v1/search/ats/sources/job/search?';
        apigee.Service_Http_Method__c = 'GET';
        apigee.OAuth_Token__c = 'zEyvMGi0DmVDOjDVupFBiNY3fQ8V';
        apigee.Client_Id__c = '1QYcoEvo4ueqvG42PyHP7TE9TxjeuKJM ';
        apigee.Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials&';
        insert apigee;
        
        OpportunityTeamMember otm = new OpportunityTeamMember(
        	UserId= u.Id, 
            //User_Name__c= u.Username, 
            OpportunityId= newOpp.Id,
            TeamMemberRole='Allocated Recruiter'
        );
        
        insert otm;
        
        Support_Request__c sr = new Support_Request__c(
        	Opportunity__c = newOpp.Id            
        );
        
        insert sr;
        
        List<AuditTrialSummary__c> listats = new List<AuditTrialSummary__c>();
        AuditTrialSummary__c ats = new AuditTrialSummary__c(
        	User__c = u.Id,
            New_Text__c = 'Allocated Recruiter',
            Object__c = 'OpportunityTeamMember',
            Parent_Record_Name__c = newOpp.Id
        );
        listats.add(ats);
        ats = new AuditTrialSummary__c(
        	User__c = u.Id,
            New_Text__c = 'Allocated Recruiter',
            Object__c = 'OpportunityTeamMember'
        );
        listats.add(ats);
        
        insert listats;
        
        Profile pSd = [Select Id From Profile Where Name LIKE 'Single Desk%' LIMIT 1];
        User usr = new User(
            CompanyName = 'TestComp',
            FirstName = 'first',
            LastName = 'last',
            LocaleSidKey = 'en_CA',
            UserName = 'testfirst1.last@allegisgroup.com',
            TimeZoneSidKey = 'America/Indiana/Indianapolis',
            Email = 'testfrt1.last@allegisgroup.com',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Alias = 't123451',
            Team__c = 'Birmingham AC Perm',
            OPCO__c = 'TEM',
            Region__c='POPULUS',
            Region_Code__c='POPUL',
            ProfileId = pSd.Id,
            IsActive = true
        );    
        insert usr;
        
	}
	
	static testMethod void saveUserActionsTest() {
		
		List<Opportunity> newOpp = [SELECT Id FROM Opportunity];
		List<User> userList  = [SELECT Id FROM User WHERE Email = 'testfrt.last@allegisgroup.com'];
		Test.startTest();
		String [] oppIds = new List<String>();
		oppIds.add(newOpp[0].Id);
		ReqRoutingController rc = new ReqRoutingController();
		ReqRoutingController.UserActions userActionObject = new ReqRoutingController.UserActions();

		userActionObject.actionType='save';
		userActionObject.currentUserId = userList[0].Id;
		userActionObject.opportunityId = newOpp[0].Id;

		ReqRoutingController.saveUserActions(userActionObject);
		ReqRoutingController.GetOpportunityDetails(oppIds);

		Integer count = [SELECT COUNT() FROM Req_Router_Action__c];
		System.assertEquals(1,count);
		Test.stopTest();
	}

    static testMethod void reqRouterActionsTest() {
        
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];
        List<User> userList  = [SELECT Id FROM User WHERE Email = 'testfrt.last@allegisgroup.com'];

        Req_Router_Action__c reqAction = new Req_Router_Action__c();
        reqAction.Action_Type__c = 'save';
        reqAction.CurrentUser__c = userList[0].Id;
        reqAction.Opportunity__c = newOpp[0].Id;

        insert reqAction;

        reqAction.Action_Type__c = 'hide';

        update reqAction;

        delete reqAction;
    }

    static testMethod void getRecruitersTest() {
		ReqRoutingController.getRecruiters('Alex');
    }
    
    static testMethod void saveDeliveryStatusTest() {
        Support_Request__c request = new Support_Request__c();
        
		ReqRoutingController.saveDeliveryStatus(request);
    }

    static testMethod void getDefaultFiltersTest() {
        String outputFilters = ReqRoutingController.getDefaultFilters();
    }

    static testMethod void getDefaultFilterDataTest() {
        String facetData = '{"fieldSlug":"skill_specialty","component":"lwcFacetCheckboxTypeahead","facetsRequest":[{"facetKey":"facetOption.req_skill_specialty","facetValue":"true"},{"facetKey":"facetOption.req_skill_specialty.size","facetValue":"50"},{"facetKey":"facetOption.req_skill_specialty.contains","facetValue":"--value--"}],"incomingFacetData":{"items":"aggregations.req_skill_specialty_filter.req_skill_specialty.buckets","name":"key","count":"doc_count"},"reqSearchParams":["facetFlt.req_skill_specialty"],"displayTitle":{"en_US":"Skill Specialties","fr_CA":"Skill Specialties"},"allowFreeText":false,"defaultFacet":true,"selected":true,"selectable":true,"isToggle":false,"selectedValues":[]}';

        Test.setMock(HttpCalloutMock.class, new ReqRoutingMock());
        
        String outputFilterData = ReqRoutingController.getDefaultFilterData(facetData, '');   
    }

    static testMethod void getReqsFromFacetsTest() {
        String outputFilters = ReqRoutingController.GetDefaultFilters();
        ReqRoutingFacetModel facets = ReqRoutingFacetModel.parse(outputFilters);
        
        Test.setMock(HttpCalloutMock.class, new ReqRoutingMock());
        
        ReqRoutingController.getReqsFromFacets('java', '20', '0', JSON.serialize(facets), '', '', '');
    }
    
	static testMethod void upsertCachedSearch() {
        String outputFilters = ReqRoutingController.GetDefaultFilters();
		ReqRoutingController.upsertCachedSearch('12345678', outputFilters); // Once for empty condition
		ReqRoutingController.upsertCachedSearch('12345678', outputFilters); // Once more for not empty
    }
    
    static testMethod void parseErrorBodyTest() {
        ReqRoutingController.parseErrorBody('{"message":"sample message"}');
    }
    
    static testMethod void getOppRecruitersTest() {
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];
        List<Id> oppId = new List<Id>();
        for(Opportunity o: newOpp){
            oppId.add(o.Id);
        }
        system.debug(ReqRoutingController.getOppRecruiters(oppId));
        system.assertEquals(newOpp.size(), ReqRoutingController.getOppRecruiters(oppId).size());
    }
    
    static testMethod void getSupportRequestsTest() {
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];
        List<Id> oppId = new List<Id>();
        for(Opportunity o: newOpp){
            oppId.add(o.Id);
        }
        system.debug(ReqRoutingController.getSupportRequests(oppId));
        system.assertEquals(1, ReqRoutingController.getSupportRequests(oppId).size());
    }
    
    static testMethod void getRecordTypeIdTest() {
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];        
        system.debug(ReqRoutingController.getRecordTypeId(newOpp[0].Id));
        List<RecordType> rids = [select Id, Name from RecordType where sObjectType='Support_Request__c' AND Name='Aerotek Delivery Center'];
        system.assertEquals(rids[0].Id, ReqRoutingController.getRecordTypeId(newOpp[0].Id));
    }
    
    static testMethod void getContactsToShareTest() {
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];        
        system.debug(ReqRoutingController.getContactsToShare('test', newOpp[0].Id));
        system.assertEquals(1, ReqRoutingController.getContactsToShare('test', newOpp[0].Id).size());
    }
    
    static testMethod void getPastAllocatedTest() {
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];        
        system.debug(ReqRoutingController.getPastAllocated(newOpp[0].Id));
        List<OpportunityTeamMember> pastUsers = [select Id from OpportunityTeamMember where OpportunityId=:newOpp[0].Id];
        system.assertEquals(pastUsers.size(), ReqRoutingController.getPastAllocated(newOpp[0].Id).size());
    }
    
    static testMethod void saveAllocatedRecruiterTest() {
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];  
        User u = [Select Id From User LIMIT 1];
        system.debug(ReqRoutingController.saveAllocatedRecruiter(u, newOpp[0].Id));
    }
    
    static testMethod void getPastSharedReqUsersTest() {
        List<Opportunity> newOpp = [SELECT Id FROM Opportunity];  
        User u = [Select Id From User LIMIT 1];
		User usr = [Select Id From User Where Profile.Name LIKE 'Single Desk%' AND IsActive=true LIMIT 1];                
        List<Id> oppId = new List<Id>();
        for(Opportunity o: newOpp){
            oppId.add(o.Id);
        }
        List<User> usrs = new List<User>{usr};
        ReqRoutingController.wrapSharedReqUsers obj = new ReqRoutingController.wrapSharedReqUsers();
        obj.reqId = newOpp[0].Id;
        obj.sharedReqUsers = usrs;
        ReqRoutingController.UserActions usrActions = new ReqRoutingController.UserActions();
        usrActions.opportunityId = newOpp[0].Id;
        usrActions.actionType = 'Share';
        usrActions.currentUserId = usr.Id;
        usrActions.isActive = true;
        usrActions.targetUser = new list<User>{usr};
        ReqRoutingController.UserActions usrActions2 = new ReqRoutingController.UserActions();
        usrActions2.opportunityId = newOpp[0].Id;
        usrActions2.actionType = 'Share';
        usrActions2.currentUserId = usr.Id;
        usrActions2.isActive = true;
        usrActions2.targetUser = new list<User>{u};
        List<Req_Router_Action__c> listrra = new List<Req_Router_Action__c>();
        System.runAs(usr) {
            Req_Router_Action__c rra = new Req_Router_Action__c(
        	Opportunity__c = newOpp[0].Id,
            TargetUser__c = usr.Id,
            Action_Type__c = 'Share',
            isActive__c = true,
            CurrentUser__c = usr.Id
            );       
            listrra.add(rra);
            rra = new Req_Router_Action__c(
        	Opportunity__c = newOpp[0].Id,
            TargetUser__c = u.Id,
            Action_Type__c = 'Share',
            isActive__c = true,
            CurrentUser__c = usr.Id
            );       
            listrra.add(rra);
            insert listrra;
            system.debug(ReqRoutingController.getPastSharedReqUsers(oppId));
            system.debug(ReqRoutingController.getPastSharedUsers(oppId));
        	system.debug(ReqRoutingController.shareUserActions(usrActions));
            system.debug(ReqRoutingController.shareUserActions(usrActions2));
            system.assertEquals(1, ReqRoutingController.getPastSharedReqUsers(oppId).size());
            system.assertEquals(2, ReqRoutingController.getPastSharedUsers(oppId).size());
        }  
        CustomNotificationType cnt = [Select Id From CustomNotificationType LIMIT 1];
        List<String> usrIds = new List<String>();
        for(User us: usrs) {
            usrIds.add(us.Id);
        }
        ReqRoutingController.notifySharingToUsers(usrIds, String.valueOf(newOpp[0].Id), cnt);
    }
  
}