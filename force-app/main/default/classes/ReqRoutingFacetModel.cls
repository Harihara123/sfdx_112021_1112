public class ReqRoutingFacetModel {
	public List<FilterCriteria> FilterCriteria;
	public class FilterCriteria {
		public String component;
		public String fieldSlug;
		public List<facetsRequest> facetsRequest;
		public incomingFacetData incomingFacetData;
		public List<String> reqSearchParams;
        public displayTitle displayTitle;
        public List<String> selectedValues;
		public Boolean allowFreeText;
		public Boolean defaultFacet;
		public Boolean selected;
		public Boolean selectable;
		public Boolean isToggle;
	}
	public class facetsRequest {
		public String facetKey;
		public String facetValue;
	}
	public class incomingFacetData {
		public String items;
		public String name;
		public String count;
	}
	public class name {
		public String key;
		public String value;
	}
	public class count {
		public String key;
		public String value;
	}
	public class displayTitle {
		public String en_US;
		public String fr_CA;
	}
	public static ReqRoutingFacetModel parse(String json){
		return (ReqRoutingFacetModel) System.JSON.deserialize(json, ReqRoutingFacetModel.class);
	}
}