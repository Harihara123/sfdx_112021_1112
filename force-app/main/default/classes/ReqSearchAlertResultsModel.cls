global class ReqSearchAlertResultsModel  {
  global List<ReqSearchAlertResultModel> results { get; set;}
  
  global class ReqSearchAlertResultModel {
    global string alertId { get; Set;}
    global string ownerId { get; Set;}
    global string jobStage { get; Set;}
    global string positionTitle { get; Set;}
    global string jobDuration { get; Set;}
    global string jobDurationUnit { get; Set;}
    global string city { get; Set;}
    global string state { get; Set;}
    global string country { get; Set;}
    global DateTime jobCreatedDate { get; Set;}
    global string skills { get; Set;}
    global string jobId { get; Set;}
    global DateTime alertMatchTimestamp { get; Set;}
    global string[] alertTrigger { get; Set;}
    global Boolean workRemote { get; Set;}
    global Boolean canUseApprovedSubVendor { get; Set;}
    global string accountName { get; Set;}
    global string positionId { get; Set;}
    global string jobOwner { get; Set;}
    global String jobMaxRate { get; Set;}
    global String jobMinRate { get; Set;}
    global String opcoName { get; Set;}
    global String reqDivision { get; Set;}
  }
}