@isTest
public class ReqSearchAlertResultsQueueable_Test  {
    public static testmethod void queueables(){ 
            Search_Alert_Criteria__c criteria1 = new Search_Alert_Criteria__c( Alert_Criteria__c = 'Test', Name='Test', Send_Email__c= true, Query_String__c = 'xyz' );
        System.assert(criteria1.Alert_Criteria__c=='Test');
        System.assert(criteria1.Name=='Test');
         System.assert(criteria1.Send_Email__c==true);
         System.assert(criteria1.Query_String__c=='xyz');
            insert criteria1;
        	
            //String jsonResult1 = '{"candidateId":"0030D000009F3YvQAK","givenName":"Valarie","familyName":"Peterson","candidateStatus":"Current","city":"Meridian","state":"Mississippi","country":"United States","createdDate":"2020-09-15T15:08:24.000Z","alertId":"'+criteria1.Id+'","teaser":"This is a resume teaser for this alert match","alertTrigger":null,"alertMatchTimestamp":"2019-02-13T15:08:24.000Z"}';
           // Search_Alert_Result__c result1 = new Search_Alert_Result__c( Alert_Criteria__c = criteria1.Id , Results__c = jsonResult1 , Date__c = System.today(), Version__c='v2');
            //insert result1;
        
            Search_Alert_Criteria__c criteria2 = new Search_Alert_Criteria__c( Alert_Criteria__c = criteria1.Id, Name='ReqTest', Alert_Trigger__c='Req Created', Send_Email__c= true);
        System.assert(criteria2.Name=='ReqTest');
         System.assert(criteria2.Alert_Trigger__c=='Req Created');
         System.assert(criteria2.Send_Email__c==true);    
        insert criteria2;
            System.debug('criteria2.Id---------'+criteria2.Id);
            List<String> arrayOfTrigger = new List<String>();
            arrayOfTrigger.add('R2C');
            String jsonResult2 = '{ "workRemote": true, "state": "MD", "skills": "java", "reqDivision": "Applications", "positionTitle": null, "positionId": null, "ownerId": "005240000080cM8AAI", "opcoName": "TEKsystems, Inc.", "jobStage": null, "jobOwner": "API Autouser", "jobMinRate": "80.0", "jobMaxRate": "100.0", "jobId": "0067A000007oEvCQAU", "jobDurationUnit": "Month(s)", "jobDuration": "4.0", "jobCreatedDate": null, "country": "UNITED STATES", "city": "Baltimore", "canUseApprovedSubVendor": true, "alertTrigger": [ "Req Created" ], "alertMatchTimestamp": "2020-09-15T09:30:44.305Z", "alertId":"'+criteria2.id+'" "accountName": null }';
            String jsonResult3='{"workRemote":true,"state":"MD","skills":["Salesforce Architect","Agile","Enterprise Architecture","Sfdc"],"reqDivision":"Applications","positionTitle":"null","positionId":"null","ownerId":"005240000080cM8AAI","opcoName":"TEKsystems Inc.","jobStage":"null","jobOwner":"API Autouser","jobMinRate":"80.0","jobMaxRate":"100.0","jobId":"0067A000007oEvCQAU","jobDurationUnit":"Month(s)","jobDuration":"4.0","jobCreatedDate":"'+System.today()+'","country":"UNITED STATES","city":"Baltimore","canUseApprovedSubVendor":true,"alertTrigger":"'+arrayOfTrigger+'","alertMatchTimestamp":"2020-09-15T09:30:44.305Z","alertId":"'+criteria2.id+'"}';
            System.debug('Json3--------'+jsonResult3);
            Search_Alert_Result__c result2 = new Search_Alert_Result__c( Alert_Criteria__c = criteria2.Id, Results__c = jsonResult3 , Date__c = Date.today(), Version__c='v2');
            insert result2;         

            ReqSearchAlertResultsModel.ReqSearchAlertResultModel rs = new ReqSearchAlertResultsModel.ReqSearchAlertResultModel();
            rs.alertId =criteria2.id;
            rs.ownerId= UserInfo.getUserId();
            rs.jobStage= '';
            rs.positionTitle= 'Developer';
            rs.jobDurationUnit= 'Month(s)';
            rs.jobDuration= '4.0';
            rs.city= 'Baltimore';
            rs.state= 'MD';
            rs.country= 'UNITED STATES';
            rs.jobCreatedDate= System.today();
            rs.skills= '["Salesforce Architect", "Agile", "Enterprise Architecture", "Sfdc"]';
            //rs.jobId= '0067A000007oEvCQAU';
            rs.alertMatchTimestamp= System.today();
            //rs.alertTrigger= '';
            rs.workRemote= true;
            rs.canUseApprovedSubVendor= true;
            rs.accountName ='';
            rs.positionId= '';
            rs.jobOwner= 'API Autouser';            
            rs.jobMinRate= '10.0';
            rs.jobMaxRate= '100.0';    
            rs.opcoName= 'TEKsystems Inc.';
            rs.reqDivision= 'Applications'; 

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            //string requestJSONBody = jsonResult3;   
            string requestJSONBody = JSON.serialize(rs);            
            req.requestURI = '/reqalertresults/v1old/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
        
            //ATSReqSearchAlertResultsService  apc = new ATSReqSearchAlertResultsService();
            //Test.startTest();
                        
           
            //ATSReqSearchAlertResultsService.doPost();
            //ReqSearchAlertResultsModel searchResults = (ReqSearchAlertResultsModel) JSON.deserialize(requestJSONBody, ReqSearchAlertResultsModel.class);            
            //.enqueueJob(new ReqSearchAlertResultsQueueable(searchResults)); 
            /*ReqSearchAlertResultsModel rs = new ReqSearchAlertResultsModel();
             list<ReqSearchAlertResultsModel.ReqSearchAlertResultModel> tempReqResultList = new list<ReqSearchAlertResultsModel.ReqSearchAlertResultModel>();
             ReqSearchAlertResultsModel wrapResults =  new ReqSearchAlertResultsModel();
            
             wrapResults =  (ReqSearchAlertResultsModel)JSON.deserialize (jsonResult3, ReqSearchAlertResultsModel.class);
             System.debug('wrapResults--------------------'+wrapResults);
             tempReqResultList.addAll(wrapResults.results);
             ReqSearchAlertResultsQueueable.getNewAlertResultEntry(tempReqResultList[0]);
             string existing = String.valueOf(tempReqResultList[0]);
             ReqSearchAlertResultsQueueable.addEntryToResults(existing, tempReqResultList[0]);
            ATSReqSearchAlertResultsService.doPost();*/
            //Test.stopTest();            
         
        }
}