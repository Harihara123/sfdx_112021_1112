public class ReqSkills
{

    public class Skills
    {
        public string name;
        public boolean favorite;
        
        public Skills(string n, boolean f)
        {
            name = n;
            favorite = f;
        }
        
    }
    
    public static List<Skills> getAllParsedSkills(string inputJson)
    {
        return (List<Skills>) System.JSON.deserialize(inputJson,List<Skills>.class);
    }
    
    public static string getFavouriteSkills(string inputJson)
    {
        List<Skills> lstSkills = (List<Skills>) System.JSON.deserialize(inputJson,List<Skills>.class);
        string favoriteSkillsJSON = '';
        
        if(!lstSkills.isEmpty())
        {
            for(Skills objSkills : lstSkills)
            {
                if(objSkills.favorite == true)
                {
                    if(favoriteSkillsJSON == '')
                    favoriteSkillsJSON = objSkills.name;
                    else
                    favoriteSkillsJSON += ','+objSkills.name;
                }
            }
        }
        return favoriteSkillsJSON;
    }
    
    public static string getNonFavouriteSkills(string inputJson)
    {
        List<Skills> lstSkills = (List<Skills>) System.JSON.deserialize(inputJson,List<Skills>.class);
        string nonFavoriteSkillsJSON = '';
        
        if(!lstSkills.isEmpty())
        {
            for(Skills objSkills : lstSkills)
            {
                 if(objSkills.favorite == false)
                {
                    if(nonFavoriteSkillsJSON == '')
                    nonFavoriteSkillsJSON = objSkills.name;
                    else
                    nonFavoriteSkillsJSON += ','+objSkills.name;
                }
            }
        }
        return nonFavoriteSkillsJSON;
    }

}