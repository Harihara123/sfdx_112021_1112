public with sharing class ReqSkillsTriggerHandler{
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    

   //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Req_Skill__c> newSkills){
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, Req_Skill__c> newMap) {
        
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Req_Skill__c>oldMap, Map<Id, Req_Skill__c>newMap) {
        
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Req_Skill__c>oldMap, Map<Id, Req_Skill__c>newMap) {
        List<Req_Skill__c> newSkills = newMap.values();
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
             UpdateOpportunityonSkillChange(afterUpdate,oldMap,newSkills);
        }
    }
    
     /***********************************************************************
      Method to update opportunity based on Skills added/Updated -- Preetham
    ***********************************************************************/
    public void UpdateOpportunityonSkillChange(String Context, Map<Id, Req_Skill__c> TriggerOldMap, List<Req_Skill__c>TriggerNew)
    { 
        List<Log__c> errorLogs = new List<Log__c>();
        List<Opportunity> oppUpdatedList = new List<Opportunity>();
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Set<Id> setOpportunityids = new Set<Id>();
        System.debug('HRXML MAIN'+TriggerNew);
        for(Req_Skill__c r :TriggerNew) {
                  if ( r.Req__c != null)
                       //&& r.Req__c.RecordTypeId == ReqRecordType &&
                       // (r.Skill__c != TriggerOldMap.get(r.Id).Skill__c || r.Level__c != TriggerOldMap.get(r.Id).Level__c ||
                       //  r.Years_of_Experience__c != TriggerOldMap.get(r.Id).Years_of_Experience__c || r.Name != TriggerOldMap.get(r.Id).Name)){
                       {
                       setOpportunityids.add(r.Req__c);
                 }
           }
        System.debug('HRXML ONE'+setOpportunityids);
        
         if(setOpportunityids.size() > 0){  
           List<Opportunity> oppList = [select Id,Req_HRXML_Field_Updated__c from opportunity where Id IN : setOpportunityids];
            System.debug('HRXML'+oppList); 
             if(oppList.size() > 0){
                for(Opportunity o: oppList)
                {
                  o.Req_HRXML_Field_Updated__c  = true;
                   oppUpdatedList.add(o);
                }
                    
               if(oppUpdatedList.size() > 0){
                   for(database.SaveResult result : database.update(oppUpdatedList,false)){
                        // Insert the log record accordingly
                        if(!result.isSuccess()){
                           errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                        }
                    }
                    
                   if(errorLogs.size() > 0)
                        database.insert(errorLogs,false);
               }     
                 
             }  
         }
         
    }



}