public class ReqSyncErrorLogHelper{
  public List<String> errorList;
  public Map<Id,String> errorMap;
  
  public ReqSyncErrorLogHelper(){
    errorList = new List<String>();
    errorMap = new Map<Id,String>();
  }
  
  public ReqSyncErrorLogHelper(Map<Id,String> exceptionMap, List<String> errorMessages){
    errorList = new List<String>();
    errorMap = new Map<Id,String>();
     
    if(exceptionMap.size() > 0){
        errorMap.putall(exceptionMap);
     }
    if(errorMessages.size() > 0){
       errorList.addAll(errorMessages);
    }
  }
  
  public ReqSyncErrorLogHelper(Map<Id,String> exceptionMap){
    errorMap = new Map<Id,String>();
     
    if(exceptionMap.size() > 0){
        errorMap.putall(exceptionMap);
     }
  }
  
  
}