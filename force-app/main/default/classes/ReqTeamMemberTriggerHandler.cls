/***************************************************************************
Name        : ReqTeamMemberTriggerHandler
Created By  : Suraj Rajoria (Appirio)
Date        : 21 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : Class that contains all of the functionality called by the
              ReqTeamMemberTrigger. All contexts should be in this class.
*****************************************************************************/
public with sharing class ReqTeamMemberTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
      Map<Id, Req_Team_Member__c> oldMap = new Map<Id, Req_Team_Member__c>();
    Map<Id, Req_Team_Member__c> newMap = new Map<Id, Req_Team_Member__c>();

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Req_Team_Member__c> newRecords) {

    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, Req_Team_Member__c>newMap,List<Req_Team_Member__c> newRecords) {
     //   ReqTeamMeber_AutofollowReqs(afterInsert,oldMap,newMap,newRecords);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Req_Team_Member__c>oldMap, Map<Id, Req_Team_Member__c>newMap) {

    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Req_Team_Member__c>oldMap, Map<Id, Req_Team_Member__c>newMap) {
        List<Req_Team_Member__c> newRecords = newMap.values();
      //  ReqTeamMeber_AutofollowReqs(afterUpdate,oldMap,newMap,newRecords);
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Req_Team_Member__c>oldMap) {
         if(!TriggerStopper.deleteRecursiveOpportunityTeamMembersStopper)
        {
            TriggerStopper.deleteRecursiveReqTeamMembersStopper = true;
            DeleteOpportunityTeamMembers(oldMap);
        }
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Req_Team_Member__c>oldMap) {
        List<Req_Team_Member__c> oldRecords =oldMap.values();
     //   ReqTeamMeber_AutofollowReqs(afterDelete,oldMap,newMap,oldRecords);
     
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUndelete (Map<Id, Req_Team_Member__c>newMap) {
      ReqTeamMember_UndeleteTrigger(afterUndelete,newMap);
    }


    /************************************************************************************************************
    Orignal Header : trigger ReqTeamMeber_AutofollowReqs on Req_Team_Member__c (after insert,after update,after delete)
    Name        :  ReqTeamMeber_AutofollowReqs
    Version     : 1.0
    Description :  Trigger to subscribe/unsubscribe reqTeam members to the Req during the req team member creation/deletion.
    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                   Date                   Description
    *-----------------------------------------------------------------------------
    * Pavan                     05/31/2012              Created
    ***************************************************************************************************************/
 /*   public void ReqTeamMeber_AutofollowReqs(String Context, Map<Id, Req_Team_Member__c>oldMap,Map<Id, Req_Team_Member__c>newMap,List<Req_Team_Member__c> records) {
       Map<string,set<string>> subscriptionInfo = new Map<string,set<string>>();
       Map<string,set<string>> updatedSubscriptions = new Map<string,set<string>>();
       List<Req_Team_Member__c> TriggerNew =  records;
       //boolean isUpdate = Trigger.isUpdate;
        if(Context.equals(afterInsert)){
              processSubscriptionInfo(afterInsert,TriggerNew,oldMap,subscriptionInfo,updatedSubscriptions);
              // create subscriptions accordingly
              autoFollowHelper.addSubscriptions(subscriptionInfo);
        }
        else if(Context.equals(afterUpdate)){
              processSubscriptionInfo(afterUpdate,TriggerNew,oldMap,subscriptionInfo,updatedSubscriptions);
              // create subscriptions accordingly
              if(subscriptionInfo.size() > 0)
                  autoFollowHelper.addSubscriptions(subscriptionInfo);
              //delete the older subscriptions
              if(updatedSubscriptions.size() > 0)
                  autoFollowHelper.removeSubscriptions(updatedSubscriptions);
        }else{
              processSubscriptionInfo(afterDelete,oldMap.values(),oldMap,subscriptionInfo,updatedSubscriptions);
              // remove subscriptions
              autoFollowHelper.removeSubscriptions(subscriptionInfo);
        }
     } */
    /*******************************************************************
    Method to populate the subscription info map
    ********************************************************************/
 /*   public void processSubscriptionInfo(String Context, List<Req_Team_Member__c> reqTeams, Map<Id, Req_Team_Member__c> oldMap,
                                        Map<string,set<string>> subscriptionInfo, Map<string,set<string>> updatedSubscriptions){
         for(Req_Team_Member__c member : reqTeams){
           boolean processMember = true;

           if(Context.equals(afterUpdate)){ //in case of update add the new and delete the old members
             if(member.User__c != Null && member.User__c != oldMap.get(member.Id).User__c)
                populateMap(member,oldMap,updatedSubscriptions);
             else
                processMember = false;
           }
           // process the team members accordingly
           if(processMember){
               if(member.User__c != Null){
                   if(subscriptionInfo.get(member.Requisition__c) != Null)
                      subscriptionInfo.get(member.Requisition__c).add(member.User__c);
                   else{
                      set<string> temp = new set<string>();
                      temp.add(member.User__c);
                      subscriptionInfo.put(member.Requisition__c,temp);
                   }
               }
           }
         }
    } */
    /***********************************************************************************
    Method to populate the updatedSubscriptions Map
    ***********************************************************************************/
 /*   public void populateMap(Req_Team_Member__c member, Map<Id, Req_Team_Member__c> oldMap, Map<string,set<string>> updatedSubscriptions){
       if(updatedSubscriptions.get(member.Requisition__c) != Null)
          updatedSubscriptions.get(member.Requisition__c).add(oldMap.get(member.Id).User__c);
       else{
          set<string> temp = new set<string>();
          temp.add(oldMap.get(member.Id).User__c);
          updatedSubscriptions.put(member.Requisition__c,temp);
       }
    } */


  /************************************************************************************************************
    Orignal Header  : trigger ReqTeamMember_UndeleteTrigger on Req_Team_Member__c (after undelete)
    Name            :  ReqTeamMember_UndeleteTrigger
    Version         :
    Description     :
    ***************************************************************************************************************/
    public void ReqTeamMember_UndeleteTrigger(String Context, Map<Id, Req_Team_Member__c> newMap) {

         Req_Team_Member__c[] allModifiedReqTeamMems  = [select Id,Deleted__c  from Req_Team_Member__c where id in :newMap.KeySet()];
         for(Req_Team_Member__c undeletedReqTeamMems : allModifiedReqTeamMems){
            undeletedReqTeamMems.Deleted__c = false;
         }
         update allModifiedReqTeamMems;

    }
    
    
    /************************************************************************************************************
    Orignal Header  : trigger DeleteOpportunityTeamMembers on Req_Team_Member__c (after delete)
    Name            :  DeleteOpportunityTeamMembers
    Version         :
    Description     :
    ***************************************************************************************************************/
    public void DeleteOpportunityTeamMembers(Map<Id, Req_Team_Member__c> oldMap) 
    {
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) 
        { 
            
            //List<Req_Team_Member__c> lstReqTeamMembers = [select id,User__c,Requisition_Team_Role__c,Requisition__r.EnterpriseReqId__c from Req_Team_Member__c where id=:oldMap.keyset()];
            //if(!lstReqTeamMembers.isEmpty())
            //{
                set<id> reqid = new set<id>();
                for(Req_Team_Member__c reqTeamMember : oldMap.values())
                {
                    reqid.add(reqTeamMember.Requisition__c);
                }
                
                List<Opportunity> lstOpp = [select id from opportunity where Source_System_Id__c  = : reqid];
                Id oppid = null;
                 if(!lstOpp.isEmpty())
                 {
                     oppid = lstOpp[0].id;
                     
                 }
                 else
                 {
                     List<Reqs__c> lstReq = [select id,EnterpriseReqId__c from Reqs__c where id = : reqid];
                     oppid = lstReq[0].EnterpriseReqId__c;
                 }
                 
                 if(oppid != null)
                 {
                     List<OpportunityTeamMember> lstOpportunityTeamMember = [select id from opportunityteammember where userid =: oldMap.values()[0].User__c and teammemberrole=:oldMap.values()[0].Requisition_Team_Role__c and opportunityid =: oppid];
                     
                     if(!lstOpportunityTeamMember.isEmpty())
                     delete lstOpportunityTeamMember;
                 }
             //}
        }
    }

}