global class ReqTeamNotificationHelper{
    static integer result;
    webservice static integer notifyReqTeam(string reqId)
    {
           //query reqTeam members
           if(reqId != Null)
           {
                 List<Req_Team_Member__c> teamMembers = new List<Req_Team_Member__c>();
                 try
                 {
                     for(Req_Team_Member__c member : [SELECT User__c,Send_Req_Update_Notification__c FROM Req_Team_Member__c WHERE Requisition__c = :reqId limit :Limits.getLimitQueryRows()])
                     {
                            // donot send email to the user clicking the button
                            if(member.User__c != userInfo.getUserId())
                            {
                                member.Send_Req_Update_Notification__c = system.now();
                                teamMembers.add(member);
                            }
                     }
                     result = teamMembers.size();
                     //update reqteams
                     if(result > 0)
                     {
                        update teamMembers;
                     }
                 }catch(Exception e)
                 {
                   // use dummy value to represent an error
                   result = 999999999;
                 }
           }
           return result;
    }
}