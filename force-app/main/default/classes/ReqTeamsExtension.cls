public class ReqTeamsExtension
{
      public String       selectedRecruiterTeam   {get;set;}
      Map<string,List<Team_Members__c>> recruiterTeamMembers = new Map<string,List<Team_Members__c>>();
      ReqsExtension_ReqTeamMembersHelper reqteamsHelper;
      string returnURL;
      //constructor 
      public ReqTeamsExtension()
      {
        reqteamsHelper = new ReqsExtension_ReqTeamMembersHelper();
        recruiterTeamMembers.putAll(reqteamsHelper.retrieveRecruiterTeamMembers());
        returnURL = ApexPages.currentPage().getParameters().get('id');
      }
      /*
            @name           getDefaultRecruiterTeam
            @description    Get the default recruiter team for the logged in user
      */
      public list<SelectOption> getDefaultRecruiterTeams() 
      {
            return reqteamsHelper.availableReqTeams();
      }
      
      /*
        @name           saveTeamMembers
        @description    creates Req Team Member based on the selected recruitment team  
     */
        public pageReference saveTeamMembers()
        {
            pageReference reqPage;
            try{
                //insert req team members accordingly
                reqteamsHelper.createTeamMembers(selectedRecruiterTeam,returnURL,recruiterTeamMembers);
                reqPage = new PageReference('/'+returnURL);
                reqPage.setRedirect(true);
            
            }catch(Exception e)
            {ApexPages.addMessages(e);}
            return reqPage;  
        }
        /*
        @name           Cancel
        @description    Redirect to Req detail page  
       */
       public pagereference Cancel()
       {
              pageReference reqPage = new PageReference('/'+returnURL);
              reqPage.setRedirect(true);
              return reqPage;
       }
}