public with sharing class ReqValidationController {
                            
    static string lsterror;
    final static string DRAFT_OTHER_REASON = 'Draft_Other_Reason__c';
    final static string OTHER = 'Other';
    
    public static string validateReqs(Reqs__c Req, boolean IsTGSRecordType,Map<string,string> recordTypeMap) {
          
             // check for the field values
            if(Req.Draft_Reason__c == OTHER && Req.Draft_Other_Reason__c == Null) 
                processErrorMessage(DRAFT_OTHER_REASON);
            for(Reqs_Validation__c fields : Reqs_Validation__c.getall().values()) 
            {
               if(fields.Stage_Name__c != Null && fields.Stage_Name__c == Req.Stage__c && Req.Placement_Type__c != Null && fields.Placement_Type__c.contains(Req.Placement_Type__c) && (fields.Record_Type_Name__c == Null || (fields.Record_Type_Name__c != Null && fields.Record_Type_Name__c == recordTypeMap.get(req.RecordTypeId))) && Req.get(fields.Field_API__c) == Null) 
               {
                    processErrorMessage(fields.Field_API__c);
               }
            }                                                                                      
        return lsterror;
    }  
    
    private static void processErrorMessage(string fld)
    {
        if(lsterror != Null)
             lsterror = lsterror + ',' + fld;
        else
             lsterror = fld;
    }      
}