/*************************************************************************************************
Apex Class Name :  ReqsExtension
Version         : 1.0 
Created Date    : 29 JAN 2012
Function        : Class used to create a Req.  
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Anand Agrawal              01/27/2012              Original Version
* Pavan                      03/28/2012              Updated/refactored the controller based on change to requirement
* Pavan                      06/26/2012              Updated product hierarchy logic to fix a defect
* Rajkumar                  16/05/2013              Added Staging variables and condition check...
* Rajkumar                  11/06/2013              Added product validations and condition check...
* Sudheer                   06/26/2013              Send Email notification when user edits Owner on a Staging req
* Sudheer                   06/27/2013              Added additional logic to NOT carry VMS fields at Clone 
* Sudheer                   08/27/2014              Added additional logic to NOT copy Template field at Clone 
* Aditya                    03/06/2015              Added additional login to Not Copy Req Disposition field at Clone (TGS)
* Aditya V(VA)              03/26/2015              Making few fields to Null, when req created.
                                                (Decline_Reason__c,Merged_Req_ID__c,Merged_Req_System__c,Proactive_Req_Link__c) 
* Nidish                    5/13/2015               Replaced Queries in line 95 and 550 with custom settings 
* Aditya V(VA)              08/31/2015              Added additional logic to default Origination Partner System Id  to '1004', If Origination System is 'Salesforce'
**************************************************************************************************/ 
global with sharing class ReqsExtension {     
         
    public List<String> oprComList              {get;set;}
    public List<String> divList                 {get;set;}
    public List<String> segmentList             {get;set;}
    public List<String> jobCodeList             {get;set;}
    public List<String> catList                 {get;set;}
    public List<SelectOption> skillSelOpp       {get;set;}
    public String       selectedSkill           {get;set;}
    public String       selectedCat             {get;set;}
    public String       selectedJobCode         {get;set;}
    public String       selectedSegment         {get;set;}
    public String       selectedOprCom          {get;set;}
    public String       selectedDiv             {get;set;}
    public String       selectedRecruiterTeam   {get;set;}
    public Boolean      ShowTgssection          {get;set;}
    //Added for Staging Req
    public Boolean      isStagingReq                {get; set;}
    public string oldOwnerId;
    public string oldReqStage;
    //Boolean variable for Product Requird field validation
    Public Boolean  isProductExists     {get; set;}
    //property invoked from javascript remoting
    public static boolean isSaved                 {get;set;}
    public string lsterror{get;set;}
    public Boolean hasEvaluated{get;set;} 
    Reqs__c reqObj;
    boolean isLoad;
    string sourceSobject;
    string retUrl;
    string TgsRecordtypeid;
    string sourceSObjectPrefix;
    ReqsExtension_AddressInformationHelper addressHelper = new ReqsExtension_AddressInformationHelper();
    final string DRAFT = 'Draft';
    final string  STAGING = 'Staging';
    final string ACCOUNTID = 'AccountId';
    final string ID = 'id';
       
    final string RECORD_TYPE = 'RecordType';
    final string GOVT_ACCOUNT = 'govtAccount';
    final string NUM_POSITIONS = 'Num_Positions_to_Update__c';
    final string SKILL_REQUIRED = 'Skill Missing';
    Map<string,string> opcoMapping = new Map<string,string>{'ONS' => 'Aerotek, Inc','TEK' => 'TEKsystems, Inc.'};
    set<string> fillLossWashErrorCollection = new set<string>{'Loss','Wash'}; 
    ReqsExtension_ReqTeamMembersHelper reqteamsHelper;
    final static string YES = 'Yes';
    final static string FUNDING_APPROVED = 'Funding_Approved__c';
    final static string OFCCP_REQUIRED = 'OFCCP_Required__c';
    final string CLONE = 'clone';
    final string OWNERID = 'ownerid';
    final string RETURN_URL = 'retURL';
    final string SOURCE_REQ = 'sourceReq';
    Map<string,List<Team_Members__c>> recruiterTeamMembers = new Map<string,List<Team_Members__c>>();
    Map<string,string> recordTypeMap = new Map<string,string>();
    Map<string,Id> recordTypeIdNameMap = new Map<string,Id>();
    set<string> SobjectId = new set<string>();
    boolean isClone = false;


    
    
    //describe Reqs object
    Map<String, Schema.SObjectField> reqFields = Reqs__c.getSObjectType().getDescribe().fields.getMap();
    // Method to initialize variables
    private void init()
    {
        oprComList      = new List<string>();
        divList         = new List<string>();
        segmentList     = new List<string>();
        jobCodeList     = new List<string>();
        catList         = new List<string>();
        // populate the operating companies
 /*       AggregateResult[] OpComps = [SELECT OpCo__c, Count(Name) 
                                     FROM Product2 
                                     WHERE isActive = true
                                     GROUP BY OpCo__c
                                     ORDER BY OpCo__c];
    
        for (AggregateResult ar : OpComps)  
        {
            oprComList.add((string)ar.get('OpCo__c'));                           
        } */
         // commenting Query and using custom settings to get opco list - Nidish
        map<string,Product_OPCO_List__c> ProductOPCOMap = Product_OPCO_List__c.getAll();
        oprComList.addAll(ProductOPCOMap.keySet());
        oprComList.sort();
        // assign the default opco accordingly
        if((!isClone || (isClone && reqObj.Product__c == Null))&&!isStagingReq)
        {
             assignOpco();
        }
         // invoke the function related to opco to get the available opco values
        if(selectedOprCom != Null )
            ChangeOprCom(); 

        // get the recruiter teams for the logged in user
        recruiterTeamMembers.putAll(reqteamsHelper.retrieveRecruiterTeamMembers());
        
         // query the Source object accordingly to populate the address fields
        if(sourceSobject != Null && !isClone)
        {
               for(Account obj : database.query(addressHelper.getQueryInformation()))// loop iterates only once
               {
                      //populate the account field
                      if(reqObj.Primary_Contact__c != Null && reqObj.Account__c == Null)
                          reqObj.Account__c = string.valueOf(obj.get(ID));
                      // populate the Nationalaccount owner field
                      if(sourceSobject == Label.AccountIdPrefix && TgsRecordtypeid != Label.Reqs_TGS_Recordtype)
                      {
                             for(string fld : Account_National_Owner_Fields__c.getAll().keyset())
                             {
                                   if(string.valueOf(obj.get(fld)) != Null)
                                   {
                                         reqObj.National_Account_Manager__c = string.valueOf(obj.get(fld));
                                         break;
                                   }
                             }
                      }
                   //assign the req address fields
                   reqObj = addressHelper.processReqAddressInformation(obj,reqObj);
                   reqObj.OFCCP_Required__c = obj.OFCCP_Compliant__c;
                    //Added below extra logic in sprint17 to accomodate Export Controls Logic
                   if(obj.Export_Controls_compliance_required__c==true && !reqobj.Export_Controls_compliance_required__c){
                       reqObj.Export_Controls_compliance_required__c=true;
                   reqObj.Compliance__c='Export Controls Questionnaire Required;';// CRMX-1238 updated text from Export Controls; to Export Controls Questionnaire Required.
                       //End of Export Controls logic
                   }
               }
        }
        SobjectId.clear();        
    }
    /*
        Method to default opco and division based on logged in user
    */
    private void assignOpco()
    {
         // Get the logged in user OPCO and Division 
        User loggedInUser   = [Select OPCO__c,Division__c,Office_Code__c from User where id = :userInfo.getUserId()];
        //default the opco value based on users opco
        if(loggedInUser.OPCO__c != Null && opcoMapping.get(loggedInUser.OPCO__c.toUppercase()) != Null)
            selectedOprCom      = opcoMapping.get(loggedInUser.OPCO__c.toUppercase());
        else     
            selectedOprCom      = loggedInUser.OPCO__c;
       //default the division based on logged in user's division
        selectedDiv         = loggedInUser.Division__c;
    }
    // constructor
    public ReqsExtension(ApexPages.StandardController controller)
    {
       isLoad = true;
       isSaved = false;
       hasEvaluated = false;
       isStagingReq = false;   
       isProductExists = false;    
       retUrl = ApexPages.currentPage().getParameters().get(RETURN_URL);
       reqteamsHelper = new ReqsExtension_ReqTeamMembersHelper();
       reqObj = (reqs__c)controller.getRecord();
       // get the record type names
       for(RecordType rec : [Select Name from RecordType where SobjectType = 'Reqs__c']){
            recordTypeMap.put(rec.Id,rec.Name);
            recordTypeIdNameMap.put(rec.Name,rec.Id);
       }
       // check if the record type is specific to TGS
       TgsRecordtypeid = ApexPages.currentPage().getParameters().get(RECORD_TYPE); 
       ShowTgssection = False;
       // check for record type and set the boolean variable accordingly
       if(TgsRecordtypeid == Label.Reqs_TGS_Recordtype) 
           ShowTgssection = True; 
      // check if the source of invocation is clone button
       if(ApexPages.currentPage().getParameters().get(CLONE) != Null)
           isClone = true;
       
           
        // check if the source of invocation is clone button
       if(ApexPages.currentPage().getParameters().get(STAGING) != Null)
           isStagingReq = true;
       
       
       
       if(isClone)
       {
              // invoke the process to clone Req
              Reqs__c reqTemp = retrieveReqInformation(ApexPages.currentPage().getParameters().get(ID));
              reqObj = reqTemp.clone(false);
              //update field values in cloned Req
              //retrieve custom setting data
              for(ReqsCustomFields__c fields : ReqsCustomFields__c.getAll().values())
              {
                  reqObj.put(fields.Name,fields.Default_Value__c);
              }
              //reset Filled/Loss/Wash Values
              reqObj.Filled__c = reqObj.Wash__c = reqObj.Loss__c = 0;
           
           reqObj.Export_Controls_compliance_required__c=false;
              
              //  **06/26/2013 Sudheer added additional conditions for Not copy, part of IR2
              reqObj.Req_Origination_System__c = 'Salesforce';
              reqObj.VMS_Requirement_Status__c = Null;
              reqObj.VMS_Requirement_Closed_Reason__c = Null; 
              reqObj.Max_Submissions_Per_Supplier__c = Null; 
           
              
              // ** 08/31/2015 Aditya Added additional logic to default Origination Partner System Id  to '1004', If Origination System is 'Salesforce'
              reqObj.Orig_Partner_System_Id__c = '1004';
              
              // ** 03/06/2015 Aditya added additional condition for Not Copy Req Disposition field value, Part of IR2.1c (Req CR)            
              reqObj.Req_Disposition__c = Null;
              
              // ** 03/26/2015 Aditya added additional condition for Not Copy few field values, Part of IR2.1c (Req CR)
              reqObj.Decline_Reason__c = Null;
              reqObj.Merged_Req_ID__c = Null;
              reqObj.Merged_Req_System__c = Null;
              reqObj.Proactive_Req_Link__c = Null;
              
              //assign the remaining field values
              reqObj.SourceReq__c = ApexPages.currentPage().getParameters().get(SOURCE_REQ);
              if(!reqObj.Product__r.isActive)
                  reqObj.Product__c = Null;
              else
              {
                   selectedOprCom = reqObj.Product__r.Opco__c;
                   selectedDiv = reqObj.Product__r.Division_Name__c;
                   selectedJobCode = reqObj.Product__r.Job_code__c;
                   selectedCat = reqObj.Product__r.category__c;
                   selectedSkill = reqObj.Product__c;
                   selectedSegment = reqObj.Product__r.segment__c;
              }     
              //Update the stage value to draft for staging req  when clone button clicked          
              if(reqTemp.Stage__c == STAGING){
                reqObj.Stage__c=DRAFT;
                reqObj.RecordTypeId= Label.Req_Staffing_RecordType;
              } 
             
            // sudheer 08/28/2014 for IR2.1 not to copy Req template flag
            reqObj.Template__c = False;              

       } 
       
       //adding Staging Req Edit Logic here       
       else if(isStagingReq){
                    Reqs__c reqTemp = retrieveReqInformation(ApexPages.currentPage().getParameters().get(ID));
                    oldOwnerId= reqTemp.owner.Id;
                    oldReqStage = reqTemp.Stage__c;
                     if(reqTemp.Product__c !=null){
                            isProductExists= true;
                    }
                    if(reqTemp.Product__r.isActive){ 
                       selectedOprCom = reqTemp.Product__r.Opco__c;
                       selectedDiv = reqTemp.Product__r.Division_Name__c;
                       selectedJobCode = reqTemp.Product__r.Job_code__c;
                       selectedCat = reqTemp.Product__r.category__c;
                       selectedSkill = reqTemp.Product__c;
                       selectedSegment = reqTemp.Product__r.segment__c;
                    }                   
       }       
       else
       {
               reqObj.Stage__c = DRAFT;
               //Govt account is populated only when the source object is govt type opportunity
               string govtAccount = ApexPages.currentPage().getParameters().get(GOVT_ACCOUNT);
               if(retUrl != Null)
               {
                   retUrl = retUrl.replace('/','');
                   sourceSobject = retUrl.subString(0,3);
                   system.debug('retUrl:'+retUrl);
                   boolean isOppty = false;
                   // salesforce id is of 18 digit length, process retUrl
                   if(retUrl.length() >= 15)
                   {
                       string subString = retUrl;
                       //get the salesforce 15 digit id
                       if(retUrl.contains('?'))
                           subString = retUrl.subString(0,retUrl.indexOf('?'));
                       //check if the length of the string is greater than 15
                       if(subString.Length() >= 15)
                           retUrl = retUrl.subString(0,15);
                       else
                           retUrl = subString;
                   }  
                   // check if the processed return url
                   if(retUrl.length() >= 15)
                   {         
                       if(sourceSobject != Label.AccountIdPrefix)
                       {
                              try
                              {
                                  // get the related accountId if the source object is contact or opportunity
                                  string qry = 'select AccountId from ';
                                  if(sourceSobject == Label.OpportunityIdPrefix)
                                  {
                                      qry += 'opportunity ';
                                      isOppty = true;
                                  }
                                  else
                                      qry += 'contact ';
                                  
                                  // append the where clause
                                  retUrl = String.escapeSingleQuotes(retUrl); //modified for sec review
                                  qry = qry + 'where id = \'' + retUrl + '\'';
                                  //query the appropriate information
                                  Sobject obj = database.query(qry);
                                  // add the accountId to the collection
                                  SobjectId.add(string.valueOf(obj.get(ACCOUNTID)));
                                  if(isOppty)
                                  {
                                       //assign the opportunityId if it is blank
                                       if(reqObj.Opportunity__c == Null)
                                           reqObj.Opportunity__c = string.valueOf(obj.get(ID));
                                       
                                       // If govt Account is not Null then populate End Client
                                       if(govtAccount != Null)
                                       {    
                                           reqObj.Account__c = govtAccount;
                                           reqObj.End_Client__c = string.valueOf(obj.get(ACCOUNTID));
                                       }
                                       else
                                          reqObj.Account__c = string.valueOf(obj.get(ACCOUNTID));
                                  } 
                              }catch(Exception e)
                              {
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
                              }
                       }
                       else
                           SobjectId.add(retUrl);
                  }
               }
          }  
      init(); 
      
      // prepopulate the logged in user as the owner
      if(!isStagingReq)
      reqObj.OwnerId = UserInfo.getUserId();
      
     /* //Finding VMS fields edit permission...
     //Only IS admin can edit the VMS field..  
     if(string.valueOf(Profile.Name).contains('Admin')){        
            isVMSEditEnabled = true;
     }*/
      //assign office
      if(!ShowTgssection&& !isStagingReq)
         reqObj.Organization_Office__c = assignLoggedInUserOffice();
      else if(!isStagingReq)
         reqObj.Organization_Office__c = Null; 
  }
    public Reqs__c getReqObj(){return reqObj;}
    /*
        Method to retrive logged in user's office
    */
    private string assignLoggedInUserOffice()
    {
           string loggedInUserOffice;
           User_Organization__c org = OfficeInformationHelper.getOfficeInformation();
           
          // Get Organization recordId from Organization object from logged in user office code.
           if(org != Null) 
           {
               loggedInUserOffice = org.Id;        
           }
           return loggedInUserOffice;
    }
    /*
      Method to retrieve the req information
    */
    private Reqs__c retrieveReqInformation(string reqId)
    {
          string reqQuery = 'select owner.Id,Owner.Email, Product__r.isActive,Product__r.OpCo__c,Product__r.Division_Name__c,Product__r.segment__c,Product__r.Job_code__c,Product__r.category__c,Product__r.Skill__c';
          //query record information and populate accordingly
           for(string fld : reqFields.keyset())
           {
                  if(reqFields.get(fld).getDescribe().isUpdateable() && fld.toLowerCase() != OWNERID)
                  {
                       reqQuery = reqQuery + ',' + fld;  
                  }
           }
            reqId = String.escapeSingleQuotes(reqId) ; //modified for sec review   
           reqQuery = reqQuery + ' from Reqs__c where id = \''+reqId+'\'';
           // query req information
           return database.query(reqQuery); 
    }
   
      /*
        @name           saveReq
        @description    Update the req with product information.
                        Also creates Req Team Member based on the selected recruitment team  
    */
    public pageReference saveReq()
    {
    
    
        pageReference reqPage;
        
        try{
                Reqs__c reqTemp = reqObj;
                
                              
                //upsert the req obj
                if(reqTemp != Null && reqTemp.product__c != Null)
                    reqObj.Product__c = reqTemp.product__c;
                    
                 if(reqObj.Stage__c == STAGING && isStagingReq!=true){
                    reqObj.Stage__c.addError('The stagging Req can not be created');
                     lsterror = SKILL_REQUIRED;   
                 }
                if(reqObj.Stage__c==STAGING && selectedSkill ==null &&( selectedDiv !=null|| selectedOprCom!=null || selectedCat !=null || selectedSegment !=null || selectedJobCode !=null) ){
                         reqObj.Product__c.addError('Please complete the product hierarchy');
                         lsterror = SKILL_REQUIRED;   
                }
                if(reqObj.Stage__c==STAGING&& isProductExists && selectedSkill ==null){
                         reqObj.Product__c.addError('You cannot blank current product selection. Please complete the hierarchy');
                         lsterror = SKILL_REQUIRED;   
                }
               //Defect #- 27978    - Deactivate the validation rule and add the below code snippet.. 
               //NON VMS Product hierarchey required for Drafted or Qualified
              /*  if(reqObj.Stage__c !=STAGING && selectedSkill !=null && selectedSkill == 'VMS'  ){
                         reqObj.Product__c.addError('Complete Product Heirarchy(other than VMS) to proceed further- Front End');
                         lsterror = SKILL_REQUIRED;   
                }*/
                // check for the status of product associated
                if(reqObj.Product__c == Null && reqObj.Stage__c!=STAGING ){
                    reqObj.addError(Label.Req_Product_Error_Message);
                    lsterror = SKILL_REQUIRED;    
                }   
                
                //Assign the record type as "Staffing Req" when staging value changed from "Staging" to "Qualified/Draft"                
                if(reqObj.Stage__c!=STAGING && oldReqStage==STAGING){                                   
                    reqObj.RecordTypeId=recordTypeIdNameMap.get('Staffing Req');
                }
                if(lsterror == Null && reqObj.Stage__c!=STAGING)
                    lsterror = ReqValidationController.validateReqs(reqObj,ShowTgssection,recordTypeMap);
                    
                //Assign the record type as "Staging Req" after the validation rules validate..    
               if(reqObj.Stage__c!=STAGING && oldReqStage==STAGING){ 
                   reqObj.RecordTypeId=recordTypeIdNameMap.get('Staging Req');  
               }                  
                 // check for ofccp field validations
            if(reqObj.Account__r.OFCCP_Compliant__c && !reqObj.OFCCP_Required__c && reqObj.Stage__c!=STAGING) {
                reqObj.OFCCP_Required__c.addError(Label.Req_OFCCP_Error_Message);
            } 
                    
       //Added extra logic in sprint17 to accomodate Export Controls Logic 
        system.debug('before for');
               for(Account obj : [select id,OFCCP_Compliant__c,Export_Controls_compliance_required__c from Account where id =:reqobj.Account__c])// loop iterates only once
               {
                   if(obj.Export_Controls_compliance_required__c && reqObj.Export_Controls_compliance_required__c==false){
                       system.debug('inside for');
                       system.debug('reqobj'+ reqObj.Export_Controls_compliance_required__c);
                       reqObj.Export_Controls_compliance_required__c=true;
                       if(reqobj.compliance__c!=null && !string.valueof(reqobj.compliance__c).contains('Export Controls')){
                           
                        reqobj.compliance__c='Export Controls Questionnaire Required;'+reqobj.compliance__c; // CRMX-1238 updated text from Export Controls; to Export Controls Questionnaire Required.
                       }
                       else if(reqobj.compliance__c==null){
                           reqobj.compliance__c='Export Controls Questionnaire Required;'; // CRMX-1238 updated text from Export Controls; to Export Controls Questionnaire Required.
                       }
                           
                       system.debug(reqobj.compliance__c);
                   }
                   else if(reqobj.compliance__c!=null && string.valueof(reqobj.compliance__c).contains('Export Controls') && reqObj.Export_Controls_compliance_required__c==false){
                       reqobj.compliance__c= string.valueof(reqobj.compliance__c.replace('Export Controls Questionnaire Required;','')); // CRMX-1238 updated text from Export Controls; to Export Controls Questionnaire Required.
                   }
               }
        
               
            
            
                    
                //check for funding values
                if(reqObj.Stage__c == label.Req_Stage && reqObj.Funding_Approved__c != Null && reqObj.Funding_Approved__c != YES && ShowTgssection &&reqObj.Stage__c!=STAGING)
                    reqObj.Funding_Approved__c.addError(Label.Req_Funding_Approved_Error_Message);
                    
                // check for the remaining fields
                if(lsterror == Null) 
                {
                    upsert reqObj;
                    reqPage = new PageReference('/' + reqObj.Id);

                    // Send email when it is Staging Reqs and only when current Owner is not new Owner
                    //  06/26/2013 Sudheer *** Send Email notification when user edits Owner on a Staging req ****       
                    if(oldOwnerId!=null){
                        Reqs__c updateReq=[Select Id, Name, Owner.Email,Owner.Id From Reqs__c where Id=:reqObj.Id];
                                                        
                        if(isStagingReq && updateReq.Owner.Id != oldOwnerId)
                        {                       
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setSubject('Req transferred to you.');
                            String[] toAddresses = new String[]{ updateReq.Owner.Email};
                            mail.setToAddresses(toAddresses); 
                            mail.setPlainTextBody('Req ' + updateReq.Name +' has been assigned to you. Please click on the link below to view the record. \n' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + updateReq.Id);  
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
                        }
                    }
                    //insert req team members accordingly
                    reqteamsHelper.createTeamMembers(this.selectedRecruiterTeam,reqObj.Id,recruiterTeamMembers); 
                } 
        }catch(Exception e)
        {
            ApexPages.addMessages(e);
            lsterror = SKILL_REQUIRED;  
        }
        return reqPage;
    }
    /*
      @name           displayErrorMessages
      @description    Action function invoked to reRender the form to dispay error messages
    */
    public void displayErrorMessages()
    {
       if(!hasEvaluated)
           hasEvaluated = true;
    }
    public void clearErrorMessages()
    {
       lstError = Null;
       isSaved = true;
    }
   
    /*
        @name           getDefaultRecruiterTeam
        @description    Get the default recruiter team for the logged in user
    */
    public list<SelectOption> getDefaultRecruiterTeams() 
    {
        return reqteamsHelper.availableReqTeams();
    }
    
    /*
        @name           getOprCompSelOpp
        @description    Get the options available for operating company
    */
    public list<SelectOption> getOprCompSelOpp() 
    { 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for (string s : oprComList) {
            options.add(new SelectOption(s, s));
        }
        return options;     
    }
    /*
        @name           getDivSelOpp
        @description    Get the options available for division based on selected operating company
    */
    public list<SelectOption> getDivSelOpp() 
    { 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for (string s : divList) {
            options.add(new SelectOption(s, s));
        }
        return options;     
    }
    /*
        @name           ChangeOprCom
        @description    Retrieve the divisions available for operating company
    */
    public void ChangeOprCom()
    {
        // reset the collections
        if(!isLoad)
        {
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
            selectedSegment = Null;
        }
        divList  = new List<String>();
        segmentList     = new List<string>();
        jobCodeList     = new List<string>();
        catList         = new List<string>();
        skillSelOpp = new List<SelectOption>();
    /*    AggregateResult[] divisions = [SELECT Division_Name__c, Count(Name) 
                                       FROM Product2 
                                       WHERE isActive = true and opco__c = :selectedOprCom
                                       GROUP BY Division_Name__c  
                                       ORDER BY Division_Name__c];
        if(divisions != null && divisions.size() > 0)
        {
            for (AggregateResult ar : divisions)  {
                divList.add((string)ar.get('Division_Name__c'));
            }           
        }else
        {
            selectedOprCom = Null;
            selectedDiv = Null;
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
            selectedSegment = Null;
        } */
        // commenting Query and using custom settings to get Division list - Nidish
        for(Product_Division_List__c ProductDivisionList : Product_Division_List__c.getall().values()){
        if(ProductDivisionList.OPCO__c == selectedOprCom){
        divList.add(ProductDivisionList.Division__c);        
        }        
        }
        if(divList!= null && divList.size() > 0){
        divList.sort();
        }  
        else{
            selectedOprCom = Null;
            selectedDiv = Null;
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
            selectedSegment = Null;
        }      
      
        if(selectedDiv != Null)
            changeDiv();
            
    }
   
    /*
        @name           ChangeSegment
        @description    Retrieve the options available for Job Code based on the selected operating company,division and segment
    */
    
    public void ChangeSegment()
    {
        //clear the value in selected job code
        if(!isLoad)
        {
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
        }
        // reset collections
        jobcodeList = new List<String>();
        catList         = new List<string>();
        skillSelOpp = new List<SelectOption>();
        AggregateResult[] jobcodes = [SELECT Job_code__c, Count(Name) 
                                      FROM Product2 
                                      WHERE isActive = true and opco__c = :selectedOprCom
                                      and Division_name__c = :selectedDiv
                                      and segment__c = :selectedSegment
                                      GROUP BY Job_code__c  
                                      ORDER BY Job_code__c];
        system.debug('jobcodes :'+jobcodes.size());                              
        if(jobcodes != Null && jobcodes.size() > 0 )
        {
            for (AggregateResult ar : jobcodes)  
            {
                jobcodeList.add((string)ar.get('Job_code__c'));
            }
        }else
        {
            selectedSegment  = Null;
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
        }
        if(selectedJobCode != Null)
            changeJobCode();
        
    }
   
    /*
        @name           getSegmentSelOpp
        @description    Get the options available for Segment
    */
    public list<SelectOption> getSegmentSelOpp() 
    { 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for (string s : segmentList) {
            options.add(new SelectOption(s, s));
        }
        return options;     
    }
    /*
        @name           ChangeDiv
        @description    Retrieve the options available for Segment based on the selected operating company and division 
    */
    public void ChangeDiv()
    {
        if(!isLoad)
        {
            selectedSegment = Null;
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
        }
        segmentList = new List<String>();
        jobCodeList     = new List<string>();
        catList         = new List<string>();
        skillSelOpp = new List<SelectOption>();
        AggregateResult[] segments = [SELECT Segment__c, Count(Name) 
                                      FROM Product2 
                                      WHERE isActive = true and Division_name__c = :selectedDiv and opCo__c = :selectedOprCom
                                      GROUP BY Segment__c  
                                      ORDER BY Segment__c];
        if(segments != null && segments.size() > 0)
        {
            for (AggregateResult ar : segments)  
            {
                segmentList.add((string)ar.get('Segment__c'));
            }
        }else
        {
            selectedDiv = Null;
            selectedSegment = Null;
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
        }
        if(selectedSegment != Null)
            ChangeSegment();
    }
    
    /*
        @name           getJobCodeSelOpp
        @description    Get the options available for Job Code 
    */
    public list<SelectOption> getJobCodeSelOpp() 
    { 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for (string s : jobCodeList) {
            options.add(new SelectOption(s, s));
        }
        return options;     
    }
    /*
        @name           ChangeJobCode
        @description    Retrieve the options available for Competency based on the selected operating company,division,segment and job code 
    */
    public void ChangeJobCode()
    {
        if(!isLoad)
        {
            selectedCat = Null;
            selectedSkill = Null;
        }
        catList = new List<String>();
        skillSelOpp = new List<SelectOption>();
        AggregateResult[] jobCodes = [SELECT Category__c, Count(Name) 
                                      FROM Product2 
                                      WHERE isActive = true and opco__c = :selectedOprCom
                                      and Division_name__c = :selectedDiv
                                      and Segment__c = :selectedSegment
                                      and Job_code__c = :selectedJobCode
                                      GROUP BY Category__c  
                                      ORDER BY Category__c];
        if(jobCodes != null && jobCodes.size() > 0){
            for (AggregateResult ar : jobCodes)  {
                catList.add((string)ar.get('Category__c'));
            }
        }else{
            selectedJobCode = Null;
            selectedCat = Null;
            selectedSkill = Null;
        }
        if(selectedCat != Null)
            ChangeCat();
    }
    /*
        @name           getCatSelOpp
        @description    Get the options available for Competency  
    */
    public list<SelectOption> getCatSelOpp() { 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for (string s : catList) {
            options.add(new SelectOption(s, s));
        }
        return options;     
    }
    /*
        @name           ChangeJobCode
        @description    Retrieve the options available for Skill based on the selected operating company,division,segment,job code and competency 
    */
    public void ChangeCat()
    {
        if(!isLoad)
         selectedSkill = Null;
        
        skillSelOpp = new List<SelectOption>();
        skillSelOpp.add(new SelectOption('','--None--'));
        AggregateResult[] jobCodes = [SELECT id,Skill__c, Count(Name) 
                                      FROM Product2 
                                      WHERE isActive = true and opco__c = :selectedOprCom
                                      and Division_name__c = :selectedDiv
                                      and Segment__c = :selectedSegment
                                      and Job_code__c = :selectedJobCode
                                      and category__c = :selectedCat
                                      GROUP BY Skill__c,id  
                                      ORDER BY Skill__c];
        if(jobCodes != null && jobCodes.size() > 0)
        {
            for (AggregateResult ar : jobCodes)  
            {
                SelectOption temp = new SelectOption((string)ar.get('id'),(string)ar.get('Skill__c'));
                skillSelOpp.add(temp);
            }
        }else
        {
            selectedCat = Null;
            selectedSkill = Null;
        }
         if(selectedSkill != Null)
            changeSkill();
         //reset the boolean variable
         isLoad = false;   
       
    }
    /*
        @name           changeSkill
        @description    Assign the req product with selected skill information 
    */
   public void changeSkill()
   {
    system.debug('Testing Change SKill'+selectedSkill);
    system.debug('Testing Change SKill'+reqObj);
      if(selectedSkill != Null)
         reqObj.product__c = selectedSkill;
      else
         reqObj.product__c = selectedSkill;
   } 
  
}