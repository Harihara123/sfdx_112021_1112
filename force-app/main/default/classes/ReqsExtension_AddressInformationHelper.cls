public class ReqsExtension_AddressInformationHelper{
    
    // Method to maintain the address query information
    public string getQueryInformation()
    {
        return 'Select ShippingStreet,OFCCP_Compliant__c,Export_Controls_compliance_required__c,TEKsystems_National_Owner__c,Aerotek_SSO_Owner__c,ShippingState,ShippingPostalCode,ShippingCity,shippingCountry,BillingCountry,BillingStreet,BillingState,BillingPostalCode,BillingCity From Account where id in :SObjectId';
    }
    //wrapper class for Address field information
    private class Req_AddressFieldMapping
    {
            public string shippingAddressFields{get;set;}
            public string billingAddressFields{get;set;}
            public string reqAddressFields{get;set;}
            //constructor
            public Req_AddressFieldMapping(string shippingAddressFields,string billingAddressFields,string reqAddressFields)
            {
               this.shippingAddressFields = shippingAddressFields;
               this.billingAddressFields = billingAddressFields;
               this.reqAddressFields = reqAddressFields;
            }
    }
    //Method to maintain addressField Mappings
    private List<Req_AddressFieldMapping> getAddressMapping()
    {
        List<Req_AddressFieldMapping> mappings = new List<Req_AddressFieldMapping>();
        mappings.add(new Req_AddressFieldMapping('ShippingStreet','BillingStreet','Address__c'));
        mappings.add(new Req_AddressFieldMapping('ShippingState','BillingState','State__c'));
        mappings.add(new Req_AddressFieldMapping('ShippingCity','BillingCity','City__c'));
        mappings.add(new Req_AddressFieldMapping('ShippingPostalCode','BillingPostalCode','Zip__c'));
        mappings.add(new Req_AddressFieldMapping('ShippingCountry','BillingCountry','Worksite_Country__c'));
        return mappings;
    }
    //Method to process address information for reqs
    public Reqs__c processReqAddressInformation(Account obj,Reqs__c reqObj)
    {
         boolean checkBillingAddress = true;
         // loop over the address fields custom setting
          for(Req_AddressFieldMapping fld : getAddressMapping())
          {
                 if(checkBillingAddress && obj.get(fld.shippingAddressFields) != Null)
                 {
                    checkBillingAddress = false;
                 }
                 //assign the address accordingly
                 if(checkBillingAddress)
                    reqObj.put(fld.reqAddressFields,obj.get(fld.BillingAddressFields));
                 else
                    reqObj.put(fld.reqAddressFields,obj.get(fld.ShippingAddressFields));
          }
          return reqObj; 
    }
    
}