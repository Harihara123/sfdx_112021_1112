public class ReqsExtension_ReqTeamMembersHelper{
    
    List<Recruiter_Team__c> recTeams = new List<Recruiter_Team__c>();
    Map<string,List<Team_Members__c>> recruiterTeamMembers = new Map<string,List<Team_Members__c>>();
    
    //constructor
    public ReqsExtension_ReqTeamMembersHelper()
    {
      // get the recruiter teams for the logged in user
        for(Recruiter_Team__c recruiterTeams : [SELECT Name,(SELECT Name,Salesforce_User__c,Contact__c from Default_sales_team__r) FROM Recruiter_Team__c WHERE (Public__c = true or OwnerId = :userInfo.getUserId())])
        {
             recruiterTeamMembers.put(recruiterTeams.Id,recruiterTeams.Default_sales_team__r);
             recTeams.add(recruiterTeams); 
        }
    }
    // method to get the appropriate reqteam members
    public Map<string,List<Team_Members__c>> retrieveRecruiterTeamMembers()
    {
          return recruiterTeamMembers;
    }
    // Method to retrieve appropriate reqTeams
    public List<SelectOption> availableReqTeams()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','---None---'));
        for(Recruiter_Team__c oRecTeam : recTeams)
        {
            options.add(new SelectOption(oRecTeam.Id, oRecTeam.Name));                  
        }
        return options; 
    }
    // Method to get the appropriate team members from the selected recuiter teams
    public void createTeamMembers(string selectedRecruiterTeam,string reqId,Map<string,List<Team_Members__c>> recruiterMembers)
    {
        Map<String,Schema.RecordTypeInfo> reqTeamsRecordType  = Schema.SObjectType.Req_Team_Member__c.getRecordTypeInfosByName();
        List<Req_Team_Member__c> ReqTeamMemberList        = new List<Req_Team_Member__c>();  
        // loop over the recriter team members    
        if(selectedRecruiterTeam != Null && recruiterMembers.get(selectedRecruiterTeam) != Null)
        {    
            for(Team_Members__c oTeamMember : recruiterMembers.get(selectedRecruiterTeam))
            {
                Req_Team_Member__c rtm         = new Req_Team_Member__c();
                if(oTeamMember.contact__c != Null)
                {
                    rtm.contact__c             = oTeamMember.contact__c;
                    rtm.recordtypeid           = reqTeamsRecordType.get(Label.Req_RecruiterTeamRecordType).getRecordTypeId();
                }
                else
                {
                    rtm.User__c                = oTeamMember.Salesforce_User__c; 
                    rtm.recordtypeid           = reqTeamsRecordType.get(Label.Req_Team_Member_Salesforce_User_Type).getRecordTypeId();
                }   
                rtm.Requisition__c             = reqId;
                ReqTeamMemberList.add(rtm);  
            }
            if(ReqTeamMemberList.size() > 0)
            {
                database.insert(ReqTeamMemberList,false);
            }
        }
    }
   
}