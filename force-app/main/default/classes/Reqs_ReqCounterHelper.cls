/**********************************************************************
Class Name:  Reqs_ReqCounterHelper
Version     : 1.0 
Function    : Count the No of Reqs associated to an Account.
 
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Pavan                       05/26/2012              Created
* Pavan                       06/21/2012              Updated the Method signature and processing logic 
                                                      to avoid too many query rows exception, returned by
                                                      subquery
*Rajkumar					5/17/2013		Added null condition to verify Account API value for VMS req                                                                                                      
******************************************************************************/
Public class Reqs_ReqCounterHelper{
    public static void countReqsAgainstAccount(Map<string,List<Reqs__c>> accountIds,integer multiplier,boolean isUpdate)
    {
        List<Account> accountsToUpdate = new List<Account>();
        List<Log__c> errorLogs = new List<Log__c>();
        final string ACCOUNT_API = 'Account__c';
        
        final string OPEN_POSITIONS_API = 'Open_Positions__c'; 
        
        //get the account information
        if(accountIds.size() > 0)
        {
            for(Account acc : [select No_of_Reqs__c,No_of_Open_Positions__c  
                               from Account 
                               where Id in :accountIds.keyset() 
                               limit :Limits.getLimitQueryRows()]) 
            {
                // set the null values accordingly
                if(acc.No_of_Reqs__c == Null)
                    acc.No_of_Reqs__c = 0;
                if(acc.No_of_Open_Positions__c == Null) 
                   acc.No_of_Open_Positions__c = 0;
                
                //calculate number of open positions by looping over the reqs
                for(Reqs__c req : accountIds.get(acc.Id))
                {
                    boolean addReqs = true;
                    if(!isUpdate)
                    {
                        if(multiplier > 0)
                            acc.No_of_Open_Positions__c += req.Open_Positions__c;
                        else
                          acc.No_of_Open_Positions__c -= req.Open_Positions__c;
                    }
                    else // in case of update
                    {
                           if(string.valueOf(Trigger.oldMap.get(req.Id).get(ACCOUNT_API))!=null 
                          				&& Id.valueOf(string.valueOf(Trigger.oldMap.get(req.Id).get(ACCOUNT_API))) != req.get(ACCOUNT_API))
                                acc.No_of_Open_Positions__c += req.Open_Positions__c;
                         else
                         {
                             double delta = req.Open_Positions__c - Double.valueOf(string.valueOf(Trigger.oldMap.get(req.Id).get(OPEN_POSITIONS_API)));
                             acc.No_of_Open_Positions__c += delta;
                             addReqs = false;
                         }
                    }
                    //update the number of reqs counter
                    if(addReqs)
                       acc.No_of_Reqs__c = acc.No_of_Reqs__c + (1 * multiplier);
                }
                
                accountsToUpdate.add(acc);
            }
        }
        
        // update associated Accounts
        if(accountsToUpdate.size() > 0)
        {
            //set the flag variable
            TriggerStopper.accountUpdatedWithReqOpenPositions = true;
            for(database.saveResult result : database.update(accountsToUpdate,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {                  
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        } 
       // insert log records
       if(errorLogs.size() > 0)
           database.insert(errorLogs,false);
     }   
     
   public static void ReqsCreateddatetoContact(set<Id> Contactids) {
        List<Contact> ContactsToUpdate = new List<Contact>();
        List<Log__c> errorLogs = new List<Log__c>(); 
        //get the account information
        if(Contactids.size() > 0)
        {
            for(Contact cont : [select Last_Req_Created_Date__c,
                (select id, createddate from Requisitions__r order by createddate desc limit 1)   
                               from Contact
                               where Id in :Contactids 
                               limit :limits.getLimitQueryRows()]) 
            {
               if(cont != null) {
                    cont.Last_Req_Created_Date__c = null;                                                          
                    if(cont.Requisitions__r.size() > 0)
                        cont.Last_Req_Created_Date__c = date.valueof(cont.Requisitions__r[0].createddate);                 
                }                                    
                ContactsToUpdate.add(cont);
            }
        }  
        
        // update associated Primary Contacts
        if(ContactsToUpdate.size() > 0)
        {
            for(database.saveResult result : database.update(ContactsToUpdate,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {                  
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        } 
       // insert log records
       if(errorLogs.size() > 0)
           database.insert(errorLogs,false);          
    }  
     
     
    
    public static void countReqsAgainstContact(Map<string,List<Reqs__c>> contactIds,integer multiplier,boolean isUpdate){
        List<Contact> ContactsToUpdate = new List<Contact>();
        final string CONTACT_API = 'Primary_Contact__c';
        final string FILLED_API = 'Filled__c';

        List<Log__c> errorLogs = new List<Log__c>(); 
        //get the account information
        if(Contactids.size() > 0)
        {
        System.debug('********************'+Contactids);
            for(Contact cont : [select Last_Req_Created_Date__c,Total_Filled__c,
                (select id, createddate from Requisitions__r order by createddate desc limit 1)   
                               from Contact
                               where Id in :Contactids.keyset() 
                               limit :limits.getLimitQueryRows()]) 
            {
            
            
               if(cont != null) {
                    cont.Last_Req_Created_Date__c = null;                                                          
                    if(cont.Requisitions__r.size() > 0)
                        cont.Last_Req_Created_Date__c = date.valueof(cont.Requisitions__r[0].createddate);                 
                } 
               
                 if(cont.Total_Filled__c== Null)
                    cont.Total_Filled__c= 0;    
                
                 for(Reqs__c req : contactIds.get(cont.Id))
                 {
                    boolean addReqs = true;
                    if(!isUpdate)
                    {
                        if(multiplier > 0)
                            cont.Total_Filled__c += req.Filled__c;
                        else
                            cont.Total_Filled__c -= req.Filled__c;
                    }
                    else // in case of update
                    {
                         if(string.valueOf(Trigger.oldMap.get(req.Id).get(CONTACT_API))!=NULL 
                         		&&  Id.valueOf(string.valueOf(Trigger.oldMap.get(req.Id).get(CONTACT_API))) != null 
                         				&& Id.valueOf(string.valueOf(Trigger.oldMap.get(req.Id).get(CONTACT_API))) != req.get(CONTACT_API))
                              cont.Total_Filled__c += req.Filled__c;
                         else
                         {
                             double delta = req.Filled__c- Double.valueOf(string.valueOf(Trigger.oldMap.get(req.Id).get(FILLED_API)));
                             cont.Total_Filled__c += delta;
                             addReqs = false;
                         }
                    }
                    //update the number of reqs counter
                   
                 }
                
                
                
                
                //The apex_context will be used to bypass the Contact Work_Mobile_Mailing Validation
                cont.Apex_Context__c = true;                                    
                ContactsToUpdate.add(cont);
            }
        }  
        
        // update associated Primary Contacts
        if(ContactsToUpdate.size() > 0)
        {
            for(database.saveResult result : database.update(ContactsToUpdate,false))
            {
                // insert the log record accordingly
                if(!result.isSuccess())
                {                  
                     errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                }
            }
        } 
       // insert log records
       if(errorLogs.size() > 0)
           database.insert(errorLogs,false);          
    }
             
}