/***************************************************************************************************************************************
* Name        - RetryOuter_Scheduled_Click 
* Description - Class used to Retry records (accounts and reqs) identified earlier as Out of sync using a scheduled job or through button click
                
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Dhruv                       01/25/2013            Created
* Pratz                       01/30/2013            Updated to accomodate for Batch Apex
*****************************************************************************************************************************************/
global class RetryOuter_Scheduled_Click implements Schedulable
{
     //Call from Button Click : 'Reconcile Now'  
     webservice static void execute()
     {        
         //Declare an object of the Batch Class Batch_RetryOutOfSyncRecords
         Batch_RetryOutOfSyncRecords objRetryOutOfSyncRecords = new Batch_RetryOutOfSyncRecords();
         
         //Execute the Batch_RetryOutOfSyncRecords Batch, with 200 records per batch
         //Performs Start(), Excecute() and Finish() methods
         Database.executeBatch(objRetryOutOfSyncRecords,200);                                    
     }
     
     //Call from Scheduled Job
     global void execute(SchedulableContext ctx)
     {  
     
         //Declare an object of the Batch Class Batch_RetryOutOfSyncRecords
         Batch_RetryOutOfSyncRecords objRetryOutOfSyncRecords = new Batch_RetryOutOfSyncRecords();
         
         //Execute the Batch_RetryOutOfSyncRecords Batch, with 200 records per batch
         //Performs Start(), Excecute() and Finish() methods
         Database.executeBatch(objRetryOutOfSyncRecords,200);
                     
     }
     
}