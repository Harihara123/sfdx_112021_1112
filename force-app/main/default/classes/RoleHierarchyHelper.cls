global class RoleHierarchyHelper{

    public static final string topNode = 'TOP';
    public static final string leafNode = 'LEAF';
    public static List<UserRole> lstUserRoleForInsert = new List<UserRole>();
    public static Map<Integer,List<UserInformation>> mapLevelRoleRecord = new Map<Integer,List<UserInformation>>(); 
    public static Map<String,UserRole> mapUsrRoleInfo = new Map<String,UserRole>(); // to map quried and updated roles with PS_ID
    public static List<SaveResult> SaveResultObjectList = new List<SaveResult>();
    public static List<User> FinalUserObjList = new List<User>();
  //  public static List<User> UserObjQuery = new List<User>();
    public static Map<String,User> mapPeopleSoftUserId = new Map<String,User>();  // maps ps id with user object query
    public Integer totalRecords = 0;
   
   public static void ProcessUserRoleInfo(List<User> userInfoList)
    {
        TriggerStopper.RoleHierarchyStopperInsert= True; 
        TriggerStopper.RoleHierarchyStopperUpdate= True; 
        Set<string> SetPsIds = new Set<string>();
        Set<string> SetAllPsIds = new Set<string>();                    // to store all psIDs and sup_PSids
        List<User> UserObjList = new List<User>();                      // User object query result                     
        Map<string, User> UserPsIdMap = new Map<string, User>();        // Map of PS_Ids to User
        Set<string> SetSupervisorPSIds = new Set<string>();
        List<UserInformation> UserInformationObject = new List<UserInformation>(); 
        Map<string, UserInformation> UserInformationPsIdMap = new Map<string, UserInformation>();
        UserInformation UserinformationRecord = new UserInformation();
        List<Log__c> errors = new List<Log__c>();
         Map<String,String> Jobcodes = new Map<String,String>();
         List<TopNodeOpcoCodes__c> codelst=new List<TopNodeOpcoCodes__c>();
        List<SaveResult> SaveResultObject = new List<SaveResult>();
        //  UserObjQuery = [Select Id, Peoplesoft_Id__c, Supervisor_PS_id__c, OPCO__c, Firstname, LastName, IsActive, UserRoleId  from User where IsActive = TRUE and UserType ='Standard']; // querying all active users
        for(User usrRecord : [Select Id, Peoplesoft_Id__c,ProfileId,Profile.Name,ODS_Jobcode__c, Supervisor_PS_id__c, OPCO__c, Firstname, LastName, IsActive, UserRoleId  from User where IsActive = TRUE and UserType ='Standard']){ // querying all active users
        // for(User usrRecord : UserObjQuery){
            if(usrRecord.Peoplesoft_Id__c <> null)
            mapPeopleSoftUserId.put(usrRecord.Peoplesoft_Id__c, usrRecord);                
        } 
        
        //codeLst = TopNodeOpcoCodes__c.getall().values();
         for(TopNodeOpcoCodes__c opcodes:TopNodeOpcoCodes__c.getall().values()){
         jobcodes.put(opcodes.Jobcode__c,opcodes.Jobcode__c);
         
         }
        
        for (User UserInfo: userInfoList){                          // initial loop to feed all collections          
            if(UserInfo.Peoplesoft_Id__c <> NULL)
                SetAllPsIds.add(UserInfo.Peoplesoft_Id__c);
            if(UserInfo.Supervisor_PS_Id__c <> NULL)
                SetAllPsIds.add(UserInfo.Supervisor_PS_Id__c);
        }
        
        SetAllPsIds.addall(getAllSupervisorIds(SetAllPsIds)); // To get all possible supervisors
        
            System.debug('@@@@ Set of app PsIds and supervisorIds - SetAllPsIds: ' +SetAllPsIds);
       if(SetAllPsIds.size() > 0){   // making sure to not get users without PsId
     //   UserObjList = [Select Id, Peoplesoft_Id__c, Supervisor_PS_id__c, OPCO__c, Firstname, LastName, IsActive, UserRoleId  from User where Peoplesoft_Id__c IN:SetAllPsIds and UserType in ('Standard','CsnOnly')]; 
           for(String Pid: SetAllPsIds){
               if(mapPeopleSoftUserId.get(pid) <> NULL){
                   UserObjList.add(mapPeopleSoftUserId.get(pid));
               }
           }
       }
            System.debug('@@@@ User Query Result - UserObjList: ' +UserObjList);
       if(UserObjList.size() > 0) {                                 // making sure if list is not null before looping it
            for (User UserObjInfo: UserObjList){                    // looping userobjlist to create UserInformation records 
                UserPsIdMap.put(UserObjInfo.Peoplesoft_Id__c, UserObjInfo); 
                SetSupervisorPSIds.add(UserObjInfo.Supervisor_PS_id__c);
                UserInformation UserInformation = new UserInformation();  
                UserInformation.OperatingCompanyCode = UserObjInfo.OPCO__c;
              //  UserInformation.OperatingCompanyName = UserObjInfo.CompanyName;
                UserInformation.ActiveStatus = UserObjInfo.IsActive;
                UserInformation.LastName = UserObjInfo.LastName;
                UserInformation.FirstName = UserObjInfo.Firstname;
                UserInformation.PeopleSoftId = UserObjInfo.Peoplesoft_Id__c;
                UserInformation.SupervisorId =UserObjInfo.Supervisor_PS_id__c;
                UserInformation.OdsJobCode =UserObjInfo.ODS_Jobcode__c;
                
                UserInformationPsIdMap.put(UserInformation.PeopleSoftId, UserInformation);  // adding UserInformation to map with ps_ID
            }         
       }   
            System.debug('@@@@ UserInformation and PsID map from query results - UserInformationPsIdMap: ' +UserInformationPsIdMap);

        for (User UserInfo: userInfoList){  
            if(!UserInformationPsIdMap.containsKey(UserInfo.Peoplesoft_Id__c)){     // check if user is existing user, if not, create new UserInformation record, if yes, update.
                UserInformation UserInformation = new UserInformation();  
                UserInformation.OperatingCompanyCode = UserInfo.OPCO__c;
                UserInformation.ActiveStatus = UserInfo.IsActive;
                UserInformation.LastName = UserInfo.LastName;
                UserInformation.FirstName = UserInfo.Firstname;
                UserInformation.PeopleSoftId = UserInfo.Peoplesoft_Id__c;
                UserInformation.SupervisorId =UserInfo.Supervisor_PS_id__c;
                UserInformation.OdsJobCode =UserInfo.ODS_Jobcode__c;
                UserInformationPsIdMap.put(UserInformation.PeopleSoftId, UserInformation);
            }else{
                UserinformationRecord = UserInformationPsIdMap.get(UserInfo.Peoplesoft_Id__c);
                UserinformationRecord.OperatingCompanyCode = UserInfo.OPCO__c;
                UserinformationRecord.ActiveStatus = UserInfo.IsActive;
                UserinformationRecord.LastName = UserInfo.LastName;
                UserinformationRecord.FirstName = UserInfo.Firstname;
                UserinformationRecord.PeopleSoftId = UserInfo.Peoplesoft_Id__c;
                UserinformationRecord.SupervisorId =UserInfo.Supervisor_PS_id__c;
                UserInformationRecord.OdsJobCode =UserInfo.ODS_Jobcode__c;
                
            }
            System.debug('@@@@ UserInformation and PsID map after adding new psid and before adding supervisor records - UserInformationPsIdMap: ' +UserInformationPsIdMap);
            if(!UserInformationPsIdMap.containsKey(UserInfo.Supervisor_PS_id__c) && UserInfo.Supervisor_PS_id__c <> null){ // check if supervisor exists or not, if not creat Userinformation record with dummy names 
                UserInformation UserInformation = new UserInformation();  
                UserInformation.OperatingCompanyCode = UserInfo.OPCO__c;
                UserInformation.ActiveStatus = UserInfo.IsActive;
                UserInformation.LastName = 'LastName';
                UserInformation.FirstName = 'FirstName';
                UserInformation.PeopleSoftId = UserInfo.Supervisor_PS_id__c;
                UserInformation.SupervisorId ='';
                UserInformation.OdsJobCode =UserInfo.ODS_Jobcode__c;
                UserInformationPsIdMap.put(UserInformation.PeopleSoftId, UserInformation);
            } 
            System.debug('@@@@ UserInformation and PsID map after adding supervisor records - UserInformationPsIdMap: ' +UserInformationPsIdMap);
        }        
            
        if(UserInformationPsIdMap.values().size() > 0){
            for(UserInformation UserInfo: UserInformationPsIdMap.values()){ // looping over to update LevelDescr
                if(UserInfo.PeopleSoftId == UserInfo.SupervisorId|| UserInfo.SupervisorId == '' || UserInfo.SupervisorId == null||(jobcodes!=null&&(UserInfo.OdsJobCode==jobcodes.get(userInfo.OdsJobCode)))){ // checks for the top nodes
                  UserInfo.LevelDescr = 'TOP';
                  UserInfo.Level = 0;
                }
                if(!SetSupervisorPSIds.contains(UserInfo.PeopleSoftId) && UserInfo.LevelDescr <>'TOP'){  // checking if user is supervisor of any one
                   UserInfo.LevelDescr = 'LEAF';  
                }
            }

            for(integer i = 0; i<UserInformationPsIdMap.values().size(); i++){  // looping to assign level values
                for(UserInformation UserInfo: UserInformationPsIdMap.values()){
                    if(UserInformationPsIdMap.get(UserInfo.SupervisorId) <> null && UserInfo.LevelDescr <> 'TOP' && UserInformationPsIdMap.get(UserInfo.SupervisorId).Level == i){
                        UserInfo.Level = i+1;
                    } 
                }
            }                 
        }    
        
        if(UserInformationPsIdMap.size() >0)
        UserInformationObject = UserInformationPsIdMap.values();
        
        System.debug('@@@@ UserInformationPsIdMap values in a list - UserInformationObject: ' +UserInformationObject);
        
        if(UserInformationObject.size() >0)
        upsertUserRole(UserInformationObject);
        
        //Update users with roles---------
        if(mapUsrRoleInfo.size() >0)
        {
            for(User usr: UserObjList)
            {
                System.debug('usr.Profile.Name:'+usr.Profile.Name);
                if(mapUsrRoleInfo.containskey(usr.Peoplesoft_Id__c) && usr.Profile.Name != 'Allegis Chatter Only')
                {
                    usr.UserRoleId = mapUsrRoleInfo.get(usr.Peoplesoft_Id__c).id;
                }
                else if(mapUsrRoleInfo.containskey(usr.Supervisor_PS_id__c+'-TEAM') && usr.Profile.Name != 'Allegis Chatter Only')
                {
                  usr.UserRoleId = mapUsrRoleInfo.get(usr.Supervisor_PS_id__c+'-TEAM').id;
              }
              else if(usr.Profile.Name == 'Allegis Chatter Only')
              {
                  //usr.userRoleId = '00E6E000001Fgne';
                  usr.userRoleId = Label.AllegisChatterOnlyRoleId;
              }
            else 
                {
                    usr.UserRoleId = Label.ORPHANED_ROLES_ID; 
                }
                
          
                
                FinalUserObjList.add(usr);
            }  
        }
        System.debug('@@@@ Final list of users with roles FinalUserObjList: '+FinalUserObjList);
        
        if(FinalUserObjList.size() >0){          
            String tempString = '';
            Integer numOfUpdates = 0;
            Integer totalRecords = 0;
            integer i =1;
            for(database.upsertResult UserSaveResult : database.upsert(FinalUserObjList,false))
            {   
                numOfUpdates++;
                saveResult saveResult = new saveResult();
                saveResult.success = UserSaveResult.isSuccess();
                saveResult.externalId = FinalUserObjList[i-1].Peoplesoft_Id__c;
                if(UserSaveResult.isSuccess())
                {
                    
                    saveResult.id = UserSaveResult.getId();
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+'\n';
                }
                else
                {             
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+','+UserSaveResult.getErrors()[0].getMessage()+'\n';
                    saveResult.simpleError = UserSaveResult.getErrors()[0].getMessage();
                    errors.add(Core_Log.logException(UserSaveResult.getErrors()[0]));
                }
                SaveResultObject.add(SaveResult);
                i = i+1;
            }
            if(tempString!=null) //# comment it later 
            {
                Core_Data.multipleLogRecord('upsertUserAzure',tempString,totalRecords,numOfUpdates);
                
            } 
        }
    }     


   public static List<SaveResult> upsertUserRole(List<UserInformation> userRoleInfoList)
    {
        Integer totalRecords = 0;
        List<SaveResult> SaveResultObject = new List<SaveResult>();
        List<Log__c> errors = new List<Log__c>();
        
       
        UserRole UserRoleFirstLevelObj = new UserRole();
        UserRole userRoleObj = new UserRole();
        List<UserRole> UserRoles = new List<UserRole>();         // List of roles that needs to be inserted for opco level roles
        List<UserRole> updateUserRoles = new List<UserRole>();   // List of rols that needs to be updated for opco level roles
        List<UserRole> userRolesTopLevel = new List<UserRole>();
        List<UserRole> updateTopUserRoles = new List<UserRole>(); 
        Set<Integer> setLevel = new Set<Integer>(); // Set of possible level numbers
        List<Integer> lstLevel = new List<Integer>();
        Set<String> RoleIds = new Set<String>();
       /*    for(WS_User_Exclude_Roles__c Pr : WS_User_Exclude_Roles__c.getall().values()){ // looping custom settings with list of roles that needs to be excluded from query.
           RoleIds.add(Pr.RoleID__c);
           system.debug('!!!!Excluded Roleids!!!!!! '+RoleIds);
           }  */
      //  List<UserRole> allUserRoles = [Select Name, Id from UserRole where Id Not IN: RoleIds];  // Querying userrole object
        Set<String> setSuperVisorId = new Set<String>();      
        
        //Loop over all the Useroles and get the EmpId and Correponding ROle in a Map
        for(UserRole userRole: [Select Name, Id from UserRole]) // Querying userrole object
        {
           if(userRole.Name.contains('(') && userRole.Name.contains(')') )  // to check if role contains psid
            mapUsrRoleInfo.put(userRole.Name.substring(userRole.Name.indexof('(')+1,userRole.Name.indexof(')')),userRole); // mapping User roles to with PS ID.
                
        }
        system.debug('@@@@ mapUsrRoleInfo: '+mapUsrRoleInfo) ; 
        //Loop over the information and populate setLevel(Set of possible level numbers) and mapLevelRoleRecord(Map of Segregated records based on levels)
        //Populate setSupervisorId(Set that holds the supervisor Ids for all the leaf records)
        
        for(UserInformation usrRoleInfo : userRoleInfoList)
        {
            if(usrRoleInfo.LevelDescr != topNode && usrRoleInfo.Level!=null)    
            setLevel.add(Integer.valueof(usrRoleInfo.Level));   //Populate the set with all possible level numbers.
            

          if(usrRoleInfo.LevelDescr == leafNode)   //#
            {
                setSuperVisorId.add(usrRoleInfo.SuperVisorId);  //For all leaf records. populate the set with supervisor Id's of leaves.
                saveResult saveResult = new saveResult();
                saveResult.success = true;
                saveResult.externalId = usrRoleInfo.peoplesoftId;
                saveResultObject.add(saveResult);
            }    
            if(usrRoleInfo.Level!=null)  //# may need to check for top node
            {
                if(!mapLevelRoleRecord.containskey(Integer.valueof(usrRoleInfo.Level)))
                {
                    List<UserInformation> lstUserRoleinfo = new List<UserInformation>();
                    lstUserRoleinfo.add(usrRoleInfo);
                    mapLevelRoleRecord.put(Integer.valueof(usrRoleInfo.Level),lstUserRoleinfo);     //If the key(Level) is not already present, make a new Key value pair.
                }
                else 
                {   
                    if(usrRoleInfo.LevelDescr != topNode)
                    mapLevelRoleRecord.get(Integer.valueof(usrRoleInfo.Level)).add(usrRoleInfo);    //Key is present then let the map grow for particular level with the new record entry.
                }
            }        
        }
            System.debug('@@@@ Set of all levels setLevel: ' +setLevel);
            System.debug('@@@@ Set of supervisor ids of leaf nodes setSuperVisorId: ' +setSuperVisorId);
            System.debug('@@@@ map of level and role records mapLevelRoleRecord: ' +mapLevelRoleRecord);
        lstlevel.addall(setLevel);
        
        Integer lstSize = lstLevel.size();      //obtain the total number of levels present.
        lstLevel.sort();                        //Sort the list of levels to have minimum level as the 0th element.
        Integer maxLevel = 0;
        Integer minLevel = 0;
        if(lstSize == 0)
        {
        maxLevel = 0;
        minLevel = 0;
        }
        else 
        {
          maxLevel = lstLevel[lstSize -1];   //get the maxlevel of load.
          minLevel = lstLevel[0];             //get the min level of load.
        }
        //Integer levelNumber = maxLevel;
        
        for(UserInformation usrRoleInfo : userRoleInfoList)     //Loop over the list of records recieved to add First level (opco level) roles to updateUserRoles and UserRoles Lists.
        {
            
            try
            {   
                if(!mapUsrRoleInfo.containskey(usrRoleInfo.OperatingCompanyCode))  //If key is not present for opco Role, then make a UserRole for this record
                {                                       
                    UserRole usrRoleFirstLevel = new UserRole();
                    usrRoleFirstLevel.Name = usrRoleInfo.OperatingCompanyName+' '+'('+usrRoleInfo.OperatingCompanyCode+')';
                    usrRoleFirstLevel.ParentRoleId = Label.AG_US_Role_ID;           //Attach the role to AG-US(Allegis All) Role.
                    UserRoles.add(usrRoleFirstLevel);
                }
                else             // If key is present.
                {
                    
                    UserRoleFirstLevelObj = mapUsrRoleInfo.get(usrRoleInfo.OperatingCompanyCode);               //Do an update if key is already there.
                    UserRoleFirstLevelObj.Name  = usrRoleInfo.OperatingCompanyName+' '+'('+usrRoleInfo.OperatingCompanyCode+')';
                    updateUserRoles.add(UserRoleFirstLevelObj);
                   
                }   
            }
                       
            catch(Exception e)
            {
               errors.add(Core_Log.logException(e));   //Catch the exception in Log Record.
               throw e;
            }
            
        }
            System.debug('@@@@ UserRoles list after opco level roles UserRoles: ' +UserRoles);
            System.debug('@@@@ updateUserRoles list after opco level roles updateUserRoles: ' +updateUserRoles);
        
        // combining updateUserRoles and UserRoles lists into one set setUserRole and FinalUsrRoleList
        Set<UserRole> setUserRole = new Set<UserRole>();
        List<UserRole> FinalUsrRoleList = new List<UserRole>();
        for(UserRole usrRole : UserRoles)
            setUserRole.add(usrRole);  
        for(UserRole usrRoleUpdates : updateUserRoles)  
            setUserRole.add(usrRoleUpdates);
        for(UserRole u : setUserRole)
            FinalUsrRoleList.add(u);
        
           System.debug('@@@@ FinalUsrRoleList list after opco level roles, FinalUsrRoleList: ' +FinalUsrRoleList); 
        if(FinalUsrRoleList.size()>0)
        {    
            
            String tempString = '';
            
            Integer numOfUpdates = 0;
            integer i =1;
            for(database.upsertResult UserSaveResult : database.upsert(FinalUsrRoleList,false))
            {   
                numOfUpdates++;
                saveResult saveResult = new saveResult();
                saveResult.success = UserSaveResult.isSuccess();
                saveResult.externalId = FinalUsrRoleList[i-1].Name.substring(FinalUsrRoleList[i-1].Name.Indexof('(')+1,FinalUsrRoleList[i-1].Name.Indexof(')'));
                if(UserSaveResult.isSuccess())
                {
                    
                    saveResult.id = UserSaveResult.getId();
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+'\n';
                }
                else
                {             
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+','+UserSaveResult.getErrors()[0].getMessage()+'\n';
                    saveResult.simpleError = UserSaveResult.getErrors()[0].getMessage();
                    errors.add(Core_Log.logException(UserSaveResult.getErrors()[0]));
                }
                SaveResultObject.add(SaveResult);
                i = i+1;
            }
            if(tempString!=null)   //# should be commented logs results to logs object
            {
                Core_Data.multipleLogRecord('upsertUserRoleAzure',tempString,totalRecords,numOfUpdates);
                
            }
        
        }    
            
            
        for(UserRole usrRole : FinalUsrRoleList)        //loop over the upserted list and populate the map with the new RoleIds thus created. 
            mapUsrRoleInfo.put(usrRole.Name.substring(usrRole.Name.indexof('(')+1,usrRole.Name.indexof(')')), usrRole);
                 
        for(UserInformation usrRoleInfo : userRoleInfoList)     //Loop over the List of records recieved to add topnode roles to userRolesTopLevel list.
        {
            try
            {   
                
                if(mapUsrRoleInfo.containskey(usrRoleInfo.OperatingCompanyCode) && !mapUsrRoleInfo.containskey(usrRoleInfo.PeopleSoftId) && usrRoleInfo.LevelDescr == topNode)  //If key is not present for Opcos, make a first level UserRole for Top Level Records 
                {                                  
                    UserRole usrRoleTopLevel = new UserRole();
                    usrRoleTopLevel.Name = usrRoleInfo.LastName+' '+usrRoleInfo.FirstName+' '+'('+usrRoleInfo.PeopleSoftId+')';
                    usrRoleTopLevel.ParentRoleId =  mapUsrRoleInfo.get(usrRoleInfo.OperatingCompanyCode).Id;
                    userRolesTopLevel.add(usrRoleTopLevel);
                }
                else if(mapUsrRoleInfo.containskey(usrRoleInfo.OperatingCompanyCode) && mapUsrRoleInfo.containskey(usrRoleInfo.PeopleSoftId)&& usrRoleInfo.LevelDescr == topNode)            // If key is present.
                {  
                    UserRoleFirstLevelObj = mapUsrRoleInfo.get(usrRoleInfo.PeopleSoftId);   
                    UserRoleFirstLevelObj.Name  = usrRoleInfo.LastName+' '+usrRoleInfo.Firstname+' '+' '+'('+usrRoleInfo.PeopleSoftId+')';
                    userRolesTopLevel.add(UserRoleFirstLevelObj);
                }   
            }
            catch(exception e)
            {
                errors.add(Core_Log.logException(e));
                throw e; 
            }
        }
            System.debug('@@@@ userRolesTopLevel list after TopNode roles, userRolesTopLevel: ' +userRolesTopLevel);    
        Set<UserRole> setTopLevelUserRoles = new Set<UserRole>();
        List<UserRole> finalTopLevelUserRoleList = new List<UserRole>();
        for(UserRole usrRoleTopLevel : userRolesTopLevel)
            setTopLevelUserRoles.add(usrRoleTopLevel);
        for(UserRole u : setTopLevelUserRoles)
            finalTopLevelUserRoleList.add(u); 
        
            System.debug('@@@@ finalTopLevelUserRoleList list after TopNode roles, finalTopLevelUserRoleList: ' +finalTopLevelUserRoleList);
        if(finalTopLevelUserRoleList.size()>0)
        {    
            
            String tempString = '';
            
            Integer numOfUpdates = 0;
            integer i =1;
            for(database.upsertResult UserSaveResult : database.upsert(finalTopLevelUserRoleList,false))
            {   
                numOfUpdates++;
                saveResult saveResult = new saveResult();
                saveResult.success = UserSaveResult.isSuccess();
                saveResult.externalId = finalTopLevelUserRoleList[i-1].Name.substring(finalTopLevelUserRoleList[i-1].Name.Indexof('(')+1,finalTopLevelUserRoleList[i-1].Name.Indexof(')'));
                if(UserSaveResult.isSuccess())
                {
                    
                    saveResult.id = UserSaveResult.getId();
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+'\n';
                }
                else
                {             
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+','+UserSaveResult.getErrors()[0].getMessage()+'\n';
                    saveResult.simpleError = UserSaveResult.getErrors()[0].getMessage();
                    errors.add(Core_Log.logException(UserSaveResult.getErrors()[0]));
                }
                SaveResultObject.add(SaveResult);
                i = i+1;
            }
            if(tempString!=null) //# comment it later 
            {
                Core_Data.multipleLogRecord('upsertUserRoleAzure',tempString,totalRecords,numOfUpdates);
                
            }
        
        }        
            
        
        for(UserRole usrRole : finalTopLevelUserRoleList)    //Loop over the upserted list and populate the map with Code strings and UserRole record. 
            mapUsrRoleInfo.put(usrRole.Name.substring(usrRole.Name.indexof('(')+1,usrRole.Name.indexof(')')), usrRole);   
        
        if( minLevel!=0 && maxLevel!=0)
        {        
            if(mapLevelRoleRecord.get(minLevel).size()>0 )   //call a recursion method that looks for min and max value of levels and inserts the Roles based on levels in the order.
            {
                insertRoleLists(minLevel,maxLevel);         //control goes to insertRoleLists(Integer,Integer) method
                system.debug('@@@@ SaveResultObjectList after minLevel, maxlevel insert: '+SaveResultObjectList);
                
            }
        }    
        for(Integer i = 0 ; i< SaveResultObjectList.size(); i++ )  // adding SaveResultObjectList to SaveResultObject
        {
            SaveResultObject.add(SaveResultObjectList[i]);
            
        }
        
        //control comes back here after call to insertRoleLists(Integer,Integer) method execution.
        List<String> lstSupervisorId = new List<String>();          //declare a list that can hold the values from setSupervisorid.
        List<UserRole> lstSuperVisorRoles = new List<UserRole>();
        lstSupervisorId.addall(setSupervisorid);       
        for( Integer i=0; i < lstSupervisorId.size() ; i++ )  //Loop over the list.
        {
            if(mapUsrRoleInfo.containskey(lstSupervisorId[i]) && !mapUsrRoleInfo.containskey(lstSupervisorId[i]+'-TEAM'))  //check if the supervisor is present or not. 
            {
                UserRole supervisorTeamRole = new UserRole();   
                supervisorTeamRole.Name =  mapUsrRoleInfo.get(lstSupervisorId[i]).Name.substring(0,mapUsrRoleInfo.get(lstSupervisorId[i]).Name.indexof('('))+'\'s'+' '+'Team'+' '+'('+lstSupervisorId[i]+'-TEAM'+')';   //Assign a proper name to the Team role of Supervisor. 
                supervisorTeamRole.ParentRoleId = mapUsrRoleInfo.get(lstSupervisorId[i]).Id;              
                lstSuperVisorRoles.add(supervisorTeamRole);
            }   
            else if(mapUsrRoleInfo.containskey(lstSupervisorId[i]) && mapUsrRoleInfo.containskey(lstSupervisorId[i]+'-TEAM'))
            {
                userRoleObj = mapUsrRoleInfo.get(lstSupervisorId[i]+'-TEAM');
                userRoleObj.Name = mapUsrRoleInfo.get(lstSupervisorId[i]).Name.substring(0,mapUsrRoleInfo.get(lstSupervisorId[i]).Name.indexof('('))+'\'s'+' '+'Team'+' '+'('+lstSupervisorId[i]+'-TEAM'+')';
                userRoleObj.ParentRoleId = mapUsrRoleInfo.get(lstSupervisorId[i]).Id;
                lstSuperVisorRoles.add(userRoleObj);
            }
            
        }
            System.debug('@@@@ list of all supervisor team roles lstSuperVisorRoles: '+lstSuperVisorRoles);
        
        if(lstSuperVisorRoles.size()>0)
        {    
            
            String tempString = '';
            
            Integer numOfUpdates = 0;
            integer i =1;
            for(database.upsertResult UserSaveResult : database.upsert(lstSuperVisorRoles,false))
            {   
                numOfUpdates++;
              /*  saveResult saveResult = new saveResult();
                saveResult.success = UserSaveResult.isSuccess();
                if(lstSuperVisorRoles[i-1].Name.contains('TEAM')    
                    saveResult.externalId = lstSuperVisorRoles[i-1].Name.substring(lstSuperVisorRoles[i-1].Name.Indexof('(')+1,lstSuperVisorRoles[i-1].Name.Indexof(')'));
                else 
                    saveResult.externalId = lstSuperVisorRoles[i-1].Name.substring(lstSuperVisorRoles[i-1].Name.Indexof('(')+1,lstSuperVisorRoles[i-1].Name.Indexof(')'));
                if(UserSaveResult.isSuccess())
                {
                    
                    saveResult.id = UserSaveResult.getId();
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+'\n';
                }
                else
                {             
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+','+UserSaveResult.getErrors()[0].getMessage()+'\n';
                    saveResult.simpleError = UserSaveResult.getErrors()[0].getMessage();
                    errors.add(Core_Log.logException(UserSaveResult.getErrors()[0]));
                }
                SaveResultObject.add(SaveResult);*/
                
                saveResult saveResult = new saveResult();
                saveResult.success = UserSaveResult.isSuccess();
                saveResult.externalId = lstSuperVisorRoles[i-1].Name.substring(lstSuperVisorRoles[i-1].Name.Indexof('(')+1,lstSuperVisorRoles[i-1].Name.Indexof(')'));
                if(UserSaveResult.isSuccess())
                {                    
                    saveResult.id = UserSaveResult.getId();
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+'\n';
                }
                else
                {             
                    tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+','+UserSaveResult.getErrors()[0].getMessage()+'\n';
                    saveResult.simpleError = UserSaveResult.getErrors()[0].getMessage();
                    errors.add(Core_Log.logException(UserSaveResult.getErrors()[0]));
                }
                SaveResultObject.add(SaveResult);                
                i = i+1;
            }
            if(tempString!=null) //# comment it 
            {
                Core_Data.multipleLogRecord('upsertUserRoleAzure',tempString,totalRecords,numOfUpdates);
                
            }
        
        } 
        //insert lstSuperVisorRoles;          //insert all the superVisor Team roles. 
       for(UserRole usrRole : lstSuperVisorRoles)    //Loop over the upserted list and populate the map with Code strings and UserRole record. 
          mapUsrRoleInfo.put(usrRole.Name.substring(usrRole.Name.indexof('(')+1,usrRole.Name.indexof(')')), usrRole); 
        
        return  SaveResultObject; 
    }    
    
    public static List<SaveResult> insertRoleLists(Integer min, Integer max)
    {
        lstUserRoleForInsert.clear(); 
        Integer tempLevel = min;    //Store the value of minimum level in a temp variable.
        Integer tempMax = max;  
        
        if(mapLevelRoleRecord.get(tempLevel) != Null && tempLevel<= tempMax)    //check if there are values corresponding to the particular level and limit the recursion till the max level.
        {    
            for(UserInformation usrRoleInfo : mapLevelRoleRecord.get(tempLevel))   // loop over min level roles and add to lstUserRoleForInsert
            { 
                //If supervisor is present and Employee is not there.        
                if( mapUsrRoleInfo.containskey(usrRoleInfo.SupervisorId)&&!mapUsrRoleInfo.containskey(usrRoleInfo.PeopleSoftId)&& usrRoleInfo.levelDescr != topNode && usrRoleInfo.levelDescr != leafNode)
                {
                    System.debug('@@@@ min level UserRoleisgettingcreated');
                    UserRole usrRoleForInsert = new UserRole();
                    usrRoleForInsert.Name = usrRoleInfo.LastName+' '+usrRoleInfo.FirstName+' '+'('+usrRoleInfo.PeopleSoftId+')';
                    usrRoleForInsert.ParentRoleId = mapUsrRoleInfo.get(usrRoleInfo.supervisorId).Id;
                    lstUserRoleForInsert.add(usrRoleForInsert);
                }
                else if( mapUsrRoleInfo.containskey(usrRoleInfo.SupervisorId) && mapUsrRoleInfo.containskey(usrRoleInfo.PeopleSoftId)&& usrRoleInfo.levelDescr != topNode) // && usrRoleInfo.levelDescr != leafNode)
                {
                    UserRole usrRoleObj = mapUsrRoleInfo.get(usrRoleInfo.PeopleSoftId); 
                    usrRoleObj.Name  = usrRoleInfo.LastName+' '+usrRoleInfo.Firstname+' '+' '+'('+usrRoleInfo.PeopleSoftId+')';
                    usrRoleObj.ParentRoleId = mapUsrRoleInfo.get(usrRoleInfo.SupervisorId).Id;
                    lstUserRoleForInsert.add(usrRoleObj);
                }
                /*************************Review if  another if-else is required for the condition ! mapUsrRoleInfo.containskey(usrRoleInfo.SupervisorId)&&!mapUsrRoleInfo.containskey(usrRoleInfo.PeopleSoftId)&& usrRoleInfo.levelDescr != topNode && usrRoleInfo.levelDescr != leafNode && !getAppRoleStatus(usrRoleInfo.AppRoleName))***************/
                //if there is no Supervisor Id found, attch ity to Orphaned roles.  
                //else if(!mapUsrRoleInfo.containskey(usrRoleInfo.SupervisorId) && !mapUsrRoleInfo.containskey(usrRoleInfo.PeopleSoftId)&& usrRoleInfo.LevelDescr != '' && usrRoleInfo.LevelDescr != 'TOP' && usrRoleInfo.LevelDescr != leafNode)
                else if(!mapUsrRoleInfo.containskey(usrRoleInfo.SupervisorId) && !mapUsrRoleInfo.containskey(usrRoleInfo.PeopleSoftId)&& usrRoleInfo.LevelDescr != topNode && usrRoleInfo.LevelDescr != leafNode) 
                {
                    UserRole orphanUserRole = new UserRole();
                    orphanUserRole.Name = usrRoleInfo.LastName+' '+usrRoleInfo.FirstName+' '+'('+usrRoleInfo.PeopleSoftId+')';
                    orphanUserRole.ParentRoleId = LABEL.ORPHANED_ROLES_ID; 
                    lstUserRoleForInsert.add(orphanUserRole);
                    System.debug('I am in Orphaned Roles');
                }
                 
                
            }
                System.debug('@@@@ lstUserRoleForInsert list after min roles, lstUserRoleForInsert: ' +lstUserRoleForInsert);
                
            if(lstUserRoleForInsert.size()>0)
            {    
                System.debug('I am in recursion');
                String tempString = '';
                System.debug('@@@@ lstUserRoleForInsert: '+lstUserRoleForInsert);
                Integer numOfUpdates = 0;
                integer i =1;
                for(database.upsertResult UserSaveResult : database.upsert(lstUserRoleForInsert,false))
                {   
                    numOfUpdates++;
                    saveResult saveResult = new saveResult();
                    saveResult.success = UserSaveResult.isSuccess();
                    saveResult.externalId = lstUserRoleForInsert[i-1].Name.substring(lstUserRoleForInsert[i-1].Name.Indexof('(')+1,lstUserRoleForInsert[i-1].Name.Indexof(')'));
                    if(UserSaveResult.isSuccess())
                    {
                        
                        saveResult.id = UserSaveResult.getId();
                        tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+'\n';
                    }
                    else
                    {             
                        tempString = tempString+saveResult.id+','+UserSaveResult.isSuccess()+','+UserSaveResult.getErrors()[0].getMessage()+'\n';
                        saveResult.simpleError = UserSaveResult.getErrors()[0].getMessage();
                        
                    }
                    SaveResultObjectList.add(SaveResult);
                    i = i+1;
                }
                if(tempString!=null) //# comment this
                {
                    Core_Data.multipleLogRecord('upsertUserRoleAzure',tempString,200,numOfUpdates);
                    
                }
                
            
            }
            for(UserRole usrRole : lstUserRoleForInsert)        //loop over the upserted list and populate the map with the new RoleIds thus created. 
            mapUsrRoleInfo.put(usrRole.Name.substring(usrRole.Name.indexof('(')+1,usrRole.Name.indexof(')')), usrRole);
            
            System.debug('@@@@ mapUsrRoleInfo after min role mapUsrRoleInfo: '+mapUsrRoleInfo);
           //Call recursion method
            if(min == max)
            return SaveResultObjectList;
            else 
            insertRoleLists(tempLevel+1,tempMax);   
            
        } 
        return SaveResultObjectList; 
         
    }


    global class UserInformation{
        public String OperatingCompanyName {get;set;}
        public String OperatingCompanyCode {get;set;}
        public Boolean ActiveStatus        {get;set;}
        public String LastName             {get;set;}
        public String FirstName            {get;set;}              
        public String PeopleSoftId         {get;set;}        
        public String SupervisorId         {get;set;}
        public Integer Level               {get;set;}
        public String LevelDescr           {get;set;} 
         public String OdsJobCode           {get;set;}  
    }

    global class SaveResult {
        public String id; //will have the salesforce unique 18 digit id for records inserted successfully
        public String externalId; //will have the legacy unique key
        public boolean success; // will be either true or false
        public String simpleError; // will be blank in case of success else, it will have the appropriate error message.
    }

    
    private static Set<string> getAllSupervisorIds(Set<string> PSIds) {  
      Set<string> mapOfCurrentWithParentID = new Set<string>();  
        for(String pid: PSIds){
           if(mapPeopleSoftUserId.get(pid) <> NULL && mapPeopleSoftUserId.get(pid).Supervisor_PS_id__c <> Null && mapPeopleSoftUserId.get(pid).Supervisor_PS_id__c <> mapPeopleSoftUserId.get(pid).Peoplesoft_Id__c) 
           mapOfCurrentWithParentID.add(mapPeopleSoftUserId.get(pid).Supervisor_PS_id__c);  
        }  
      if(mapOfCurrentWithParentID.size() > 0){ 
        system.debug('@@@@ mapOfCurrentWithParentID before loop: '+mapOfCurrentWithParentID);
        mapOfCurrentWithParentID.addAll(getAllSupervisorIds(mapOfCurrentWithParentID));  
      }  
      return mapOfCurrentWithParentID;  
    } 
}