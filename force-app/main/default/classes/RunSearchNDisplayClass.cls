public  with sharing class RunSearchNDisplayClass  {
  @AuraEnabled
   public static List <Reference_Search__c> fetchAccount(String refName,String refCompany,String refTitle,String pocode,String cntry,String level_1,String level_2,String local,String level_3,String candidateTitle, String lat, String lng, String radius, String radiUnit) {
	String lcoation='';
	String loc='';
	String locFind='';
	String locWithNoLatLongContact='';
	String whereclauseLocation ='';
	String radiusString='';
	if(local != null && local != ''){
		lcoation += ' '+local;
		//System.debug('address info-local------------'+local);
	}
	if(level_3 != null && level_3 != ''){
		lcoation += ' '+level_3;
		//System.debug('address info-level_3------------'+level_3);
	}
	if(level_2 != null && level_2 != ''){
		lcoation += ' '+level_2;
		//System.debug('address info-level_2------------'+level_2);
	}
	if(level_1 != null && level_1 != ''){
		lcoation += ' '+level_1;
		//System.debug('address info-level_1------------'+level_1);
	}
	if(cntry != null && cntry != ''){
		lcoation += ' '+cntry;
		//System.debug('address info-cntry------------'+cntry);
	}
	if(pocode != null && pocode != ''){
		lcoation += ' '+pocode;
		//System.debug('address info-pocode------------'+pocode);
	}
	if((local != null && local != '') && ((level_3 == null || level_3 == '') && (level_2 == null || level_2 == '') && (level_1 == null || level_1 == '') && (cntry == null || cntry == '') && (pocode == null || pocode == '') )){
		
		local = local.replaceAll('[^a-zA-Z0-9\\s+]', '');
		//System.debug('strText ======> '+local);
		List<String> filterLogicSplittedbySpace = local.split(' ');
		for (String str : filterLogicSplittedbySpace){
			//system.debug('Split String-----------'+str);
			If(whereclauseLocation ==''){
				whereclauseLocation = ' MailingAddress__c like '+'\'%'+str+'%\'';
			}else{
				whereclauseLocation += ' AND MailingAddress__c like '+'\'%'+str+'%\'';
			}
		}
	}
	//System.debug('address info-whereclauseLocation------------'+whereclauseLocation);
	if(radiUnit =='mi'){
		//System.debug('address info-radiusUnit------------MIIIIIIIIIIIIIIIIIIIIIIIII');
		radiusString='),\'mi\'';
	}else{
		//System.debug('address info-radiusUnit------------KMMMMMMMMMMM');
		radiusString='),\'km\'';
	}
	//----------------------------------------------------------------------
	//System.debug('address info-lcoation------------'+lcoation);
	if(lcoation !=''){
	loc = lcoation.replaceAll('[^a-zA-Z0-9\\s+]', '');
		System.debug('strText ======> '+loc);
		List<String> filterLogicSplittedbySpace = loc.split(' ');
		for (String str : filterLogicSplittedbySpace){
			system.debug('Split String-----------'+str);
			if(String.isNotBlank(str)){
				If(locWithNoLatLongContact ==''){
					locWithNoLatLongContact = ' MailingAddress__c like '+'\'%'+str+'%\'';
				}else{
					locWithNoLatLongContact += ' AND MailingAddress__c like '+'\'%'+str+'%\'';
				}
			}
		}
	}
	//------------------------------------
	//System.debug('address info-radius------------'+radius);
	//System.debug('address info-lcoation------------'+lcoation);
	//System.debug('address info-----lat--------'+lat);
	//System.debug('address info-------lng------'+lng);
	String title ='';
	//String WhereClauseTitle ='';
   if(candidateTitle != null){
	//System.debug('candidateTitle-Before----------------'+candidateTitle);
	
	List<String> items = candidateTitle.split(',');
	for(String s:items){
	//System.debug('candidateTitle-for----------------'+s);
		s=s.replaceAll('[^a-zA-Z0-9\\s+]', '');		
		if(title!=''){
			title += ' OR '+s;
			//WhereClauseTitle +=  ' '+'AND Talent_Job_Duties__c like'+' '+'\'%'+s+'%\'';
		}else{
			title = s;
			//WhereClauseTitle = ' Talent_Job_Duties__c like'+' '+'\'%'+s+'%\'';
		}		
	}
    title = (title.length()>0?('('+title+')'):title);
	//System.debug('candidateTitle-After----------------'+title);
	//System.debug('candidateTitle-WhereClauseTitle----------------'+WhereClauseTitle);
   }
   String searchTerms = '';
   String searchTermsnew = '';
   If((!String.isEmpty(refName)) && refName != 'null'){
	searchTermsnew = refName;
   }
   If((!String.isEmpty(title)) && title != ''){
		if(searchTermsnew ==''){
			searchTermsnew = title;
		}else{
			searchTermsnew += ' AND '+title;
		}	
   }
   If((!String.isEmpty(refCompany)) && refCompany != 'null'){
		if(searchTermsnew ==''){
			searchTermsnew = refCompany;
		}else{
			searchTermsnew += ' AND '+refCompany;
		}	
   }
   If((!String.isEmpty(refTitle)) && refTitle != 'null'){
		if(searchTermsnew ==''){
			searchTermsnew = refTitle;
		}else{
			searchTermsnew += ' AND '+refTitle;
		}	
   }
   If((!String.isEmpty(lcoation)) && lcoation != 'null' &&  lcoation != ''){
		/*if(searchTermsnew ==''){
			searchTermsnew = lcoation;
		}else{
			searchTermsnew += ' AND '+lcoation;
		}*/
		 
		loc = lcoation.replaceAll('[^a-zA-Z0-9\\s+]', '');
			//System.debug('strText ======> '+loc);
			List<String> filterLogicSplittedbySpace = loc.split(' ');
			for (String str : filterLogicSplittedbySpace){
				//system.debug('Split String-----------'+str);
				if(String.isNotBlank(str)){
					
					if(locFind ==''){
						locFind = str;
					}else{
						locFind += ' OR '+str;
					}
					/*if(searchTermsnew ==''){
						searchTermsnew = str;
					}else{
						searchTermsnew += ' OR '+str;
					}*/
				}
			}
			if(searchTermsnew ==''){
				searchTermsnew = locFind;
			}else{
				searchTermsnew += ' AND ('+locFind+')';
			}
			
   }
   
   //System.debug('searchTermsnew-After----------------'+searchTermsnew);

    String whereclause = '';
	String whereclauseName = '';
        if( (!String.isEmpty(refName)) && refName != 'null'){
			String refnameBackup =refName;
            refName = '\'%'+refName+'%\'';
        searchTerms += refName;
		//whereclause = ' Where  Full_Name__c like '+refName ;
		//-------------------------------------
		List<String> filterLogicSplittedbySpace = refnameBackup.split(' ');
		for (String str : filterLogicSplittedbySpace){
			//system.debug('Split String-----------'+str);
			If(whereclauseName ==''){
				whereclauseName = ' Full_Name__c like '+'\'%'+str+'%\'';
			}else{
				whereclauseName += ' AND Full_Name__c like '+'\'%'+str+'%\'';
			}
		}
		if(whereclauseName !=''){
			whereclauseName = ' Where '+whereclauseName;
			whereclause = whereclauseName;
		}
		
		//---------------------------------------
        }
    
        if( searchTerms !=''){
            if((!String.isEmpty(refCompany)) && refCompany != 'null'){
			refCompany  = '\'%'+refCompany+'%\'';
			searchTerms += ' AND ';
			searchTerms +=  refCompany ;
			whereclause += ' AND Organization_Name__c like  '+refCompany;
		   }          
        }else{
		  if((!String.isEmpty(refCompany)) && refCompany != 'null'){
			refCompany  = '\'%'+refCompany+'%\'';                
			searchTerms +=  refCompany ;
			whereclause = ' Where Organization_Name__c like '+refCompany;
		  } 
    }

    if( searchTerms !=''){           
                if((!String.isEmpty(refTitle)) && refTitle != 'null'){
          refTitle  = '\'%'+refTitle+'%\'';
          searchTerms += ' AND ';
          searchTerms +=  refTitle ;
		  whereclause += ' AND Reference_Job_Title__c like  '+refTitle;
        }        
    }else{
		if((!String.isEmpty(refTitle)) && refTitle != 'null'){
		refTitle  = '\'%'+refTitle+'%\'';                
		searchTerms +=  refTitle ;
		whereclause = ' Where Reference_Job_Title__c like '+refTitle;
		} 
    }
	//System.debug('lcoation-After----------------'+lcoation);
	//System.debug('Before lcoation-searchTerms----------------'+searchTerms);
    if(searchTerms !=''){            
		  if((!String.isEmpty(lcoation)) && lcoation != 'null' && lcoation != ''){
			lcoation  = '\'%'+lcoation+'%\'';
			searchTerms += ' AND ';
			searchTerms +=  lcoation ;			
			if(lat != 'null' && lat != null && lat != '' && lng != 'null' && lng != null && lng != ''){
				//whereclause +=' AND DISTANCE(Recommendation_From__r.MailingAddress,GEOLOCATION('+lat+','+lng+radiusString+')< '+radius;
				whereclause +=' AND ((DISTANCE(Recommendation_ID__r.Talent_Contact__r.MailingAddress,GEOLOCATION('+lat+','+lng+radiusString+')< '+radius+ ') OR ('+locWithNoLatLongContact+'))';
			}else{
				lcoation = '\'%'+lcoation+'%\'';
				whereclause += ' AND '+whereclauseLocation; 
			}
		  }            
    }else{
		//System.debug('Inside Else----------------');
      if( lcoation != 'null' && lcoation != ''){
		//System.debug('Inside Else---block If-------------');
        //refCompany  = '\'%'+lcoation+'%\'';                
        searchTerms +=  lcoation ;
		//System.debug('Inside Else---block If--searchTerms-----------'+searchTerms);
			if(lat != 'null' && lat != null && lat != '' && lng != 'null' && lng != null && lng != ''){
				 whereclause =' Where (DISTANCE(Recommendation_ID__r.Talent_Contact__r.MailingAddress,GEOLOCATION('+lat+','+lng+radiusString+')< '+radius+ ') OR ('+locWithNoLatLongContact+')';
			}else{
				//lcoation = '\'%'+lcoation+'%\'';
				whereclause = ' Where '+whereclauseLocation;
			}		
      } 
    }
    //System.debug('searchTerms-----------------'+searchTermsnew);
    List<List<Reference_Search__c>> results = new List<List<Reference_Search__c>>();
	List<List<Reference_Search__c>> res = new List<List<Reference_Search__c>>();
    //if (searchTerms!=''){ 
	if (searchTermsnew!=''){    
	String searchQueryStr = 'FIND {'+searchTermsnew+'} IN ALL FIELDS RETURNING Reference_Search__c(Id,CreatedDate,Recommendation_ID__r.Client_Account__c,Talent_Job_Duties__c,Full_Name__c,Organization_Name__c,Project_Description__c,Recommendation_From__c,Recommendation_ID__c,Reference_Job_Title__c,Recommendation_ID__r.lastModifiedDate,Recommendation_ID__r.Talent_Contact__r.MailingAddress,Recommendation_From__r.Phone,Recommendation_From__r.HomePhone,Recommendation_From__r.MobilePhone,Recommendation_From__r.OtherPhone,Recommendation_From__r.Email,MailingAddress__c,Recommendation_ID__r.Email__c,Recommendation_ID__r.Home_Phone__c,Recommendation_ID__r.Mobile_Phone__c,Recommendation_ID__r.Other_Phone__c,Recommendation_ID__r.Work_Phone__c'+whereclause+'  ORDER BY CreatedDate DESC LIMIT 250 )';
	System.debug('results-----------------'+searchQueryStr);
	res = search.query(searchQueryStr);
	//System.debug('results-----------------'+res);                         
    }
	
    List < Reference_Search__c > finalResults = new List < Reference_Search__c > ();
    if(res.size() > 0){
      for(List<Reference_Search__c> trr:res){
        finalResults.addAll(trr);
      }
    }        

    return finalResults;
   }
    @AuraEnabled
    public static List<Reference_Search__c> getRefDetails(String refSearchId){
	Reference_Search__c getRFId = [Select Id,Recommendation_ID__r.Recommendation_From__c, Recommendation_From__c,Recommendation_ID__c,Recommendation_ID__r.Talent_Contact__c from Reference_Search__c where Id=:refSearchId AND Recommendation_ID__r.Talent_Contact__c != null  limit 250];
	List<Reference_Search__c> rs = new List<Reference_Search__c>();
	if(getRFId.Recommendation_ID__r.Recommendation_From__c == null){		
	rs = [Select Id, 
			Full_Name__c, 
			CreatedDate,
			Reference_Job_Title__c,
			Recommendation_ID__r.Client_Account__c,
			Recommendation_ID__r.Organization_Name__c,
			Recommendation_From__c,
			Recommendation_From__r.Id,
			Recommendation_From__r.Name,
			Recommendation_ID__r.Talent_Contact__c,
			Recommendation_ID__r.Talent_Contact__r.Name,
			Recommendation_ID__r.Talent_Contact__r.Title,
			Recommendation_ID__r.Talent_Contact__r.Email,
			Recommendation_ID__r.Talent_Contact__r.Phone,
			Recommendation_ID__r.Talent_Contact__r.OtherPhone,
			Recommendation_ID__r.Talent_Contact__r.HomePhone,
			Recommendation_ID__r.Start_Date__c,
			Recommendation_ID__r.End_Date__c,
			Recommendation_ID__r.Completed_By__c,
			Recommendation_ID__r.Completed_By__r.Name,
			Recommendation_ID__r.Talent_Job_Duties__c,
			Recommendation_ID__r.Project_Description__c
			from Reference_Search__c where 	Recommendation_ID__r.Talent_Contact__c =:getRFId.Recommendation_ID__r.Talent_Contact__c AND Id=:getRFId.Id];
		return rs;	
	}else{		
	rs = [Select Id, 
			Full_Name__c, 
			CreatedDate,
			Reference_Job_Title__c,
			Recommendation_ID__r.Client_Account__c,
			Recommendation_ID__r.Organization_Name__c,
			Recommendation_From__c,
			Recommendation_From__r.Id,
			Recommendation_From__r.Name,
			Recommendation_ID__r.Talent_Contact__c,
			Recommendation_ID__r.Talent_Contact__r.Name,
			Recommendation_ID__r.Talent_Contact__r.Title,
			Recommendation_ID__r.Talent_Contact__r.Email,
			Recommendation_ID__r.Talent_Contact__r.Phone,
			Recommendation_ID__r.Talent_Contact__r.OtherPhone,
			Recommendation_ID__r.Talent_Contact__r.HomePhone,
			Recommendation_ID__r.Start_Date__c,
			Recommendation_ID__r.End_Date__c,
			Recommendation_ID__r.Completed_By__c,
			Recommendation_ID__r.Completed_By__r.Name,
			Recommendation_ID__r.Talent_Job_Duties__c,
			Recommendation_ID__r.Project_Description__c
			from Reference_Search__c where Recommendation_ID__r.Recommendation_From__c =:getRFId.Recommendation_ID__r.Recommendation_From__c];
		return rs;
	}
	
    }
}