@isTest
public class RunSearchNDisplayClassTest  {

	@isTest static void test_methodOne() {
       //MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet
	    Account talentAcc = TestData.newAccount(1, 'Talent');
            talentAcc.Talent_Ownership__c = 'AP';
            talentAcc.Talent_Committed_Flag__c = true;
        	talentAcc.G2_Completed__c = true;
            insert talentAcc;

            Contact ct = TestData.newContact(talentAcc.id, 1, 'Talent'); 
            ct.FirstName='First Name';
			ct.LastName='Last Name';
			ct.Other_Email__c = 'other@testemail.com';
            ct.Title = 'Tester';
            ct.MailingCity = 'test text';
            ct.MailingState = 'test text';
			ct.MailingPostalCode = '50858';
			ct.MailingStreet = 'Ohio';
			ct.MailingCountry = 'USA';
			ct.MailingLatitude= 38.7378;
			ct.MailingLongitude=-90.6234;
			ct.MailingPostalCode='63304';
            insert ct;

			Talent_Recommendation__c tr = new Talent_Recommendation__c();
			 tr.First_Name__c =  'FirstName';
			 tr.Last_Name__c =  'LastName';
			 tr.Project_Description__c =  'Project Description Value';
			 tr.Talent_Job_Duties__c =  'Tester';
			 tr.Completion_Date__c =  System.today();
			 tr.Organization_Name__c =  'Org';
			 tr.Talent_Contact__c = ct.Id;
			 tr.Recommendation_From__c =  ct.Id;
			 tr.Reference_Job_Title__c =  'Tester';
			 tr.Home_Phone__c =  '9090909090';
			 tr.Mobile_Phone__c =  '9090909090';
			 tr.Other_Phone__c =  '9090909090';
			 tr.Work_Phone__c =  '9090909090'; 
			 tr.Email__c =  'test@test.com';
			 insert tr;		
			 
			 Reference_Search__c rs = new Reference_Search__c();
			 rs.Full_Name__c ='FirstName LastName';
			 rs.Talent_Job_Duties__c='Job Duties Value';
			 rs.MailingAddress__c='Weldon Spring, Missouri, 63304, USA';
			 rs.Organization_Name__c ='Org';	
			 rs.Project_Description__c ='Project Description Value';
			 rs.Recommendation_ID__c = tr.Id;
			 rs.Reference_Job_Title__c  ='Tester';
			 rs.Recommendation_From__c = ct.Id;
			 insert rs;
			RunSearchNDisplayClass.fetchAccount('FirstName LastName','Org','Tester','63304','USA','Weldon Spring','Missouri','','Missouri','Tester', '38.7378', '-90.6234', '30', 'mi') ;
			RunSearchNDisplayClass.fetchAccount('FirstName LastName','Org','','63304','USA','Weldon Spring','Missouri','','Missouri','', '38.7378', '-90.6234', '30', 'mi') ;
			RunSearchNDisplayClass.fetchAccount(' ','','','63304','USA','Weldon Spring','Missouri','','Missouri','', '38.7378', '-90.6234', '30', 'mi') ;
			RunSearchNDisplayClass.fetchAccount('FirstName LastName','Org','Tester','','','','','Weldon Spring Missouri 63304 USA','','Tester', '38.7378', '-90.6234', '30', 'mi') ;
			RunSearchNDisplayClass.fetchAccount('','','','','','','','Weldon Spring Missouri 63304 USA','','', '', '', '', '') ;
			RunSearchNDisplayClass.getRefDetails(rs.Id);
	}
	
}