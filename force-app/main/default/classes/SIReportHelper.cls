global class SIReportHelper{
    public final static string ALL = 'All';
    public final static string SHIPPING_CITY = 'ShippingCity';
    public final static string SHIPPING_COUNTRY = 'ShippingCountry';
    public final static string BILLING_CITY = 'BillingCity';
    public final static string BILLING_COUNTRY = 'BillingCountry';
    public final static string CUSTOM_RELATION = '__r';
    //wrapper class for SI Report Filters
    public class SIReportFilters{
        public string Filter{get;set;}
        public string query{get;set;}
        //constructor
        public SIReportFilters(string filter,string query)
        {
            this.Filter = filter;
            this.query = query;
        }
    }
    //wrapper class for SIReport Timeframes
    public class SIReportTimeFrame{
        public string equivalentDateLiteral{get;set;}
        public string Name{get;set;}
        //constructor
        public SIReportTimeFrame(string Name,string equivalentDateLiteral)
        {
          this.Name = Name;
          this.equivalentDateLiteral = equivalentDateLiteral;
        }
    }
    //Wrapper class for SI Report_DependentQuery
    public class SIReport_DependentQuery{
        public string query{get;set;}
        public string Name{get;set;}
        //constructor
        public SIReport_DependentQuery(string Name,string query)
        {
              this.Name = Name;
              this.query = query;
        }
    }
    //wrapper class for SIReport-RelatedObjectDateFilterFields
    public class SIReport_RelatedObjectDateFilterFields{
        public string sortBy{get;set;}
        public string Name{get;set;}
        public string fieldAPIName{get;set;}
        //constructor
        public SIReport_RelatedObjectDateFilterFields(string Name,string fieldAPIName,string sortBy)
        {
              this.Name = Name;
              this.fieldAPIName = fieldAPIName;
              this.sortBy = sortBy;
        }
    }
    //wrapper class for SI Report related object query information
    public class SIReport_RelatedObjectQuery{
        public string relatedObject{get;set;}
        public boolean hasReqs{get;set;}
        public string query{get;set;}
        //constructor
        public SIReport_RelatedObjectQuery(string relatedObject,boolean hasReqs,string query)
        {
           this.relatedObject = relatedObject;
           this.hasReqs = hasReqs;
           this.query = query;
        }
    }
    // wrapper class for SIReport_RelatedObjects
    public class SIReport_RelatedObjects{
        public string RelatedSObjectName{get;set;}
        public string Name{get;set;}
        //constructor
        public SIReport_RelatedObjects(string Name,string RelatedSObjectName)
        {
          this.Name = Name;
          this.RelatedSObjectName = RelatedSObjectName;
        }
    }
    //wrapper class for SIReport-RelatedObjectDateFilterFields
    public class SIReport_SObjectAssociatedFieldInfo{
        public string RelatedSobjectField{get;set;}
        public string Name{get;set;}
        public string siFieldAPIName{get;set;}
        //constructor
        public SIReport_SObjectAssociatedFieldInfo(string Name,string RelatedSobjectField,string siFieldAPIName)
        {
              this.Name = Name;
              this.RelatedSobjectField = RelatedSobjectField;
              this.siFieldAPIName = siFieldAPIName;
        }
    }
    /*****************************************************************
    Methods to create instances of wrapper class 
    *******************************************************************/
    public List<SIReportHelper.SIReportTimeFrame> getSIReportTimeFrames()
    {
        List<SIReportHelper.SIReportTimeFrame> filters = new List<SIReportHelper.SIReportTimeFrame>();
        filters.add(new SIReportHelper.SIReportTimeFrame('Last Week',' = LAST_WEEK'));
        filters.add(new SIReportHelper.SIReportTimeFrame('Last 30 days',' = LAST_N_DAYS:30'));
        filters.add(new SIReportHelper.SIReportTimeFrame('Last 90 days',' = LAST_90_DAYS'));
        filters.add(new SIReportHelper.SIReportTimeFrame('Custom',''));
        return filters;
    }
    public List<SIReportHelper.SIReport_DependentQuery> getSIReportDependentQuery()
    {
        List<SIReportHelper.SIReport_DependentQuery> filters = new List<SIReportHelper.SIReport_DependentQuery>();
        filters.add(new SIReportHelper.SIReport_DependentQuery('Account','select Account__c,Strategic_Initiative__c from Account_Strategic_Initiative__c where Strategic_Initiative__c = :currentSI'));
        filters.add(new SIReportHelper.SIReport_DependentQuery('Government Program','Select Name,Strategic_Initiative__c from Government_Program__c where Strategic_Initiative__c = :currentSI'));
        filters.add(new SIReportHelper.SIReport_DependentQuery('Opportunity','Select Opportunity__c,Strategic_Initiative__c from Opportunity_Strategic_Initiative__c where Strategic_Initiative__c = :currentSI'));
        return filters;
    }
    public List<SIReportHelper.SIReport_RelatedObjectDateFilterFields> getSIReportDateFilterFields()
    {
        List<SIReportHelper.SIReport_RelatedObjectDateFilterFields> filters = new List<SIReportHelper.SIReport_RelatedObjectDateFilterFields>();
        filters.add(new SIReportHelper.SIReport_RelatedObjectDateFilterFields('Events','ActivityDate','StartDateTime'));
        filters.add(new SIReportHelper.SIReport_RelatedObjectDateFilterFields('Tasks','ActivityDate','ActivityDate'));
        //KDOLCH 20140409 - Replacing line below to use CreatedDate instead of Start Date for Reqs.  ER-379.
        //filters.add(new SIReportHelper.SIReport_RelatedObjectDateFilterFields('Reqs__r','Start_Date__c','Status__c,Stage__c,Start_Date__c'));
        filters.add(new SIReportHelper.SIReport_RelatedObjectDateFilterFields('Reqs__r','CreatedDate','Status__c,Stage__c,CreatedDate'));
        return filters;
    }
    public List<SIReportHelper.SIReport_RelatedObjects> getSIReportRelatedObjects()
    {
        List<SIReportHelper.SIReport_RelatedObjects> filters = new List<SIReportHelper.SIReport_RelatedObjects>();
        filters.add(new SIReportHelper.SIReport_RelatedObjects('Account','All;Tasks;Events;Reqs__r'));
        filters.add(new SIReportHelper.SIReport_RelatedObjects('Government Program','All;Tasks;Events'));
        filters.add(new SIReportHelper.SIReport_RelatedObjects('Opportunity','All;Tasks;Events;Reqs__r'));
        return filters;
    }
    public List<SIReportHelper.SIReport_SObjectAssociatedFieldInfo> getSObjectAssociatedFieldInfo()
    {
        List<SIReportHelper.SIReport_SObjectAssociatedFieldInfo> filters = new List<SIReportHelper.SIReport_SObjectAssociatedFieldInfo>();
        string SIField = 'Strategic_Initiative__c';
        filters.add(new SIReportHelper.SIReport_SObjectAssociatedFieldInfo('Account','Account__c',SIField));
        filters.add(new SIReportHelper.SIReport_SObjectAssociatedFieldInfo('Government Program','Id',SIField));
        filters.add(new SIReportHelper.SIReport_SObjectAssociatedFieldInfo('Opportunity','Opportunity__c',SIField));
        return filters;
    }
    public List<SIReportHelper.SIReport_RelatedObjectQuery> getRelatedObjectQuery()
    {
       List<SIReportHelper.SIReport_RelatedObjectQuery> recs = new List<SIReportHelper.SIReport_RelatedObjectQuery>();
       //recs.add(new SIReportHelper.SIReport_RelatedObjectQuery('Account',true,'select Name,shippingCity,shippingCountry,BillingCity,BillingCountry,(select whoId,subject,status,ActivityDate,OwnerId from Tasks),(select whoId,subject,StartDateTime,EndDateTime,Description,Type,OwnerId from Events),(select Name,Start_Date__c,Stage__c,Status__c,Positions__c,Filled__c,Loss__c,Wash__c,OwnerId,Primary_Contact__c,Organization_Office__c from Reqs__r) from Account where id in :relatedObjects and isDeleted = false limit 1000'));
       //recs.add(new SIReportHelper.SIReport_RelatedObjectQuery('Opportunity',true,'select Name,(select whoId,subject,status,ActivityDate,OwnerId from Tasks),(select whoId,subject,StartDateTime,EndDateTime,Description,Type,OwnerId,DurationInMinutes from Events),(select Name,Start_Date__c,Stage__c,Status__c,Positions__c,Filled__c,Loss__c,Wash__c,OwnerId,Primary_Contact__c,Organization_Office__c from Reqs__r) from Opportunity where id in :relatedObjects and isDeleted = false limit 1000'));
       //KDOLCH 20140409 - Changes to accomodate for ER-0379 to change to created date instead of start date for Reqs.
       recs.add(new SIReportHelper.SIReport_RelatedObjectQuery('Account',true,'select Name,shippingCity,shippingCountry,BillingCity,BillingCountry,(select whoId,subject,status,ActivityDate,OwnerId from Tasks),(select whoId,subject,StartDateTime,EndDateTime,Description,Type,OwnerId from Events),(select Name,CreatedDate,Start_Date__c,Stage__c,Status__c,Positions__c,Filled__c,Loss__c,Wash__c,OwnerId,Primary_Contact__c,Organization_Office__c from Reqs__r) from Account where id in :relatedObjects and isDeleted = false limit 1000'));
       recs.add(new SIReportHelper.SIReport_RelatedObjectQuery('Opportunity',true,'select Name,(select whoId,subject,status,ActivityDate,OwnerId from Tasks),(select whoId,subject,StartDateTime,EndDateTime,Description,Type,OwnerId,DurationInMinutes from Events),(select Name,CreatedDate,Start_Date__c,Stage__c,Status__c,Positions__c,Filled__c,Loss__c,Wash__c,OwnerId,Primary_Contact__c,Organization_Office__c from Reqs__r) from Opportunity where id in :relatedObjects and isDeleted = false limit 1000'));
       recs.add(new SIReportHelper.SIReport_RelatedObjectQuery('Government Program',false,'select Name,(select subject,status,ActivityDate,OwnerId from Tasks),(select subject,StartDateTime,EndDateTime,Description,Type,OwnerId from Events) from Government_Program__c where id in :relatedObjects and isDeleted = false limit 1000'));
       return recs;
    }
    //Method to get the SI Members
    webservice static List<Strategic_Initiative_Member__c> retrieveSIMembers(string currentSI)
    {
          set<string> temp = new set<string>();
          temp.add(currentSI);
          return getInitiativeTeamMembers(temp);
    }
    //method to get the initiative team members
    private static List<Strategic_Initiative_Member__c> getInitiativeTeamMembers(set<string> siIds)
    {
          List<Strategic_Initiative_Member__c> siTeams = new List<Strategic_Initiative_Member__c>();
          for(Strategic_Initiative_Member__c member : [SELECT User__c,User__r.Name 
                                                       FROM Strategic_Initiative_Member__c 
                                                       WHERE Strategic_Initiative__c in :siIds
                                                       LIMIT :Limits.getLimitQueryRows()])
          {
                siTeams.add(member);
          }
          return siTeams;
    }
    //method to process the initiative team members
    private static string getTeamMembers(set<string> siTeams)
    {
           string teamMembers;
           for(Strategic_Initiative_Member__c teams : getInitiativeTeamMembers(siTeams))
           {
              if(teamMembers == Null)
                  teamMembers = teams.User__r.Name;
              else
                  teamMembers = teamMembers + ','+ teams.User__r.Name;
           }
           return teamMembers;
    }
    //Method to get the Accounts Related to SI
    webservice static string retrieveSIRelatedAccounts(string currentSI,boolean isTeam)
    {
          set<string> temp = new set<string>();
          temp.add(currentSI);
          if(!isTeam)
              return getAccountIds(temp);
          else
              return getTeamMembers(temp);    
    }
    webservice static string retrieveSIsInRegion(string region,boolean isTeam)
    {
           //query all SIs in a region
           set<string> SIs = new set<string>();
           for(Strategic_Initiative__c si : [SELECT Name 
                                             FROM Strategic_Initiative__c
                                             WHERE Impacted_Region__c = :region
                                             LIMIT :Limits.getLimitQueryRows()])
            {
                SIs.add(si.Id);
            }
            
            if(!isTeam)
                return getAccountIds(SIs);
            else
                return getTeamMembers(SIs);    
                                                
    }
    private static string getAccountIds(set<string> SIs)
    {
          string accountNames;
          if(SIs.size() > 0)
          {
              for(Account_Strategic_Initiative__c initiative : [SELECT Account__c,  Account__r.Siebel_ID__c 
                                                                FROM Account_Strategic_Initiative__c 
                                                                WHERE Strategic_Initiative__c in :SIs
                                                                LIMIT :Limits.getLimitQueryRows()])
              {
                   // string accountId = string.valueOf(initiative.Account__c).subString(0,15);
                    string accountId = string.valueOf(initiative.Account__r.Siebel_ID__c); // updated to replace ID field with Siebel Id -Nidish.
                    if(accountNames == Null)
                        accountNames = accountId;
                    else
                        accountNames = accountNames + ',' +accountId;
              }
              if(accountNames != Null)
                  accountNames = EncodingUtil.urlEncode(accountNames, 'UTF-8');
          }
          return accountNames; 
    }
}