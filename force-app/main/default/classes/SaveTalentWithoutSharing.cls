public without sharing class SaveTalentWithoutSharing {
    public static Id saveTalentRecord(Contact contact, Account account, String g2Comments,String g2Source){
       
	    account.Talent_Summary_Last_modified_by__c = UserInfo.getUserId();
		account.Talent_Summary_Last_Modified_date__c = System.now();
		System.debug('-------g2Source1--------'+g2Source);
		Id contactId = upsertAccountContactActivity(contact,account,g2Comments, 'Add',g2Source);
		return contactId;
        /*account.Name = contact.FirstName + ' ' + contact.LastName; 
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        account.Talent_Committed_Flag__c = true;
        account.Talent_Visibility__c ='Private'; // To fire sharing rule - Nidish
		if(account.G2_Completed__c == true){
            account.G2_Completed_By__c = UserInfo.getUserId();
            account.G2_Completed_Date__c = Date.Today();
		}
		System.Savepoint sp = Database.setSavepoint();
        try {
            upsert account;
			contact.AccountId = account.Id;
			contact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
			upsert contact;
			createG2Activity(account.Id,contact.Id,g2Comments,account.G2_Completed__c);
        } catch (DmlException ex) {
            System.debug('Failed account-contact validation --- ' + ex.getMessage());
			Database.rollback(sp);
            throw ex;
        }*/
        
		//return contact.Id;
    }

    public static Id saveTalentRecordModal(Contact contact, Account account, String g2Comments,String g2Source){
        //String g2Source;//comment this later.
        account.Talent_Summary_Last_modified_by__c = UserInfo.getUserId();
        account.Talent_Profile_Last_Modified_By__c = UserInfo.getUserId();
        account.Talent_Summary_Last_Modified_date__c = System.now();
        account.Talent_Profile_Last_Modified_Date__c = System.now();

		Id accountId = upsertAccountContactActivity(contact,account,g2Comments, 'Edit',g2Source);
		return accountId;
        /*
		if(account.G2_Completed__c == true){
            account.G2_Completed_By__c = UserInfo.getUserId();
            account.G2_Completed_Date__c = Date.Today();
		}
		account.Name = contact.FirstName + ' ' + contact.LastName; 
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        account.Talent_Committed_Flag__c = true;
        account.Talent_Visibility__c ='Private'; // To fire sharing rule - Nidish
		System.Savepoint sp = Database.setSavepoint();
        try {
            upsert account;
			contact.AccountId = account.Id;
			contact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
			upsert contact;
			createG2Activity(account.Id,contact.Id,g2Comments,account.G2_Completed__c);
        } catch (DmlException ex) {
            System.debug('Failed account-contact validation --- ' + ex.getMessage());
			Database.rollback(sp);
            throw ex;
        }*/
        
        //return account.Id;
    }
    
    public static Contact getTalent(String accountId) {
        try{
        Contact c =[SELECT FirstName, MiddleName, LastName, Title, Suffix, Salutation, OtherPhone, Work_Email__c, 
                MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry , 
				MailingLatitude, MailingLongitude, GoogleMailingLatLong__Latitude__s, GoogleMailingLatLong__Longitude__s, 
                Phone, WorkExtension__c, HomePhone, MobilePhone, Preferred_Phone__c, Email, Other_Email__c, Preferred_Email__c,
                Preferred_Name__c, Talent_State_Text__c, Talent_Country_Text__c, 
                Website_URL__c, LinkedIn_URL__c, 
                Related_Contact__c, Related_Contact__r.Name, Related_Contact__r.Id, 
                AccountId, Id, LastModifiedBy.Name, LastModifiedDate, 
                Account.Talent_Id__c, Account.Talent_Profile_Last_Modified_Date__c, Account.Talent_Source_Other__c, 
                Account.Talent_Race__c, Account.Talent_Gender__c, 
                Account.Talent_Race_Information__c, Account.Talent_Gender_Information__c,
                Account.Do_Not_Contact__c, Account.Do_Not_Contact_Date__c, Account.Do_Not_Contact_Reason__c,
                Account.G2_Completed_By__c, Account.G2_Completed_Date__c, Account.G2_Completed__c, Account.Date_Available__c
        FROM Contact where AccountId=: accountId];
            return c;
        }Catch(Exception e){
            return null;
        }
    }

	public static Contact getTalentById(String recordId) {
		try{
			Contact c =[SELECT Id, Name, Phone, WorkExtension__c, HomePhone, MobilePhone, OtherPhone, Preferred_Phone__c, Preferred_Phone_Value__c, Email, Work_Email__c, Other_Email__c, Preferred_Email__c, Preferred_Email_Value__c, LinkedIn_URL__c, AccountId,Account.Do_Not_Contact__c,Account.Do_Not_Contact_Date__c,Account.Do_Not_Contact_Reason__c
			FROM Contact where Id=: recordId];
			return c;
		}Catch(Exception e){
			return null;
		}
	}

    public static Contact getTalentForModal(String accountId) {
		try {
		//STORY S-109328 Added Communities Fields to Query by Siva
        Contact c =[SELECT Id, FirstName, MiddleName, LastName, Title, Suffix, Salutation,  OtherPhone, Work_Email__c, 
                MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,
				MailingLatitude, MailingLongitude, GoogleMailingLatLong__Latitude__s, GoogleMailingLatLong__Longitude__s, 
                Phone, HomePhone, WorkExtension__c, MobilePhone, Preferred_Phone__c, Email, Other_Email__c, Preferred_Email__c,
                Preferred_Name__c, Talent_State_Text__c, Talent_Country_Text__c, 
                Website_URL__c, LinkedIn_URL__c,  Candidate_Status__c,
                Related_Contact__c, Related_Contact__r.Name, Related_Contact__r.Id, 
                AccountId, LastModifiedBy.Name, LastModifiedDate, 
				TC_FirstName__c,TC_LastName__c, TC_Full_Name__c, TC_Mailing_Address__c, TC_Phone__c, TC_Email__c, TC_Title__c,
                Account.Talent_Id__c, Account.Talent_Profile_Last_Modified_Date__c, Account.Talent_Source_Other__c, 
                Account.Talent_Race__c, Account.Talent_Gender__c, 
                Account.Talent_Race_Information__c, Account.Talent_Gender_Information__c,
                Account.Do_Not_Contact__c, Account.Do_Not_Contact_Date__c, Account.Do_Not_Contact_Reason__c,
                Account.Name, Account.Talent_Preference_Internal__c, Account.Talent_Summary_Last_modified_by__r.Name, 
                Account.Talent_Summary_Last_Modified_date__c, Account.Talent_Overview__c,
                Account.Skills__c, Account.Skill_Comments__c, Account.Willing_to_Relocate__c,
                Account.Desired_Rate__c, Account.Desired_Rate_Max__c, Account.Desired_Salary__c,
                Account.Desired_Salary_Max__c, Account.Desired_Rate_Frequency__c, Account.Desired_Currency__c,
                Account.Desired_Placement_type__c, Account.Shift__c, Account.Desired_Schedule__c, Account.Desired_Bonus__c,
                Account.Desired_Bonus_Percent__c, Account.Desired_Additional_Compensation__c,
                Account.Desired_Total_Compensation__c, Account.Goals_and_Interests__c,
                Account.G2_Completed_By__c, Account.G2_Completed_Date__c, Account.G2_Completed__c,Account.Date_Available__c,
                Account.Talent_Security_Clearance_Type__c,Account.Talent_FED_Agency_CLR_Type__c, Account.Talent_Current_Employer__c,
                Account.Talent_Current_Employer_Text__c, RWS_DNC__c, 
                (select id, activityDate,Description, Owner.Name from tasks where WhatId = :accountId and Type = 'G2' and status ='Completed' order by CreatedDate  desc limit 1)
            FROM Contact where AccountId=: accountId];
             return c;
        }Catch(Exception e){
            return null;
        }
    }
    
    public static void createG2Activity(Id accountId, Id contactId, string g2Comments, boolean isG2Checked,String g2Source) {
    System.debug('-------g2Source2--------'+g2Source);
        String userName = Userinfo.getFirstName() + ' ' + UserInfo.getLastName();
        Task tsk = new Task();
        tsk.OwnerID = Userinfo.getUserId();
        tsk.Type = 'G2';
        tsk.Priority = 'Normal';
        tsk.WhatId = accountId;
        tsk.WhoId = contactId;
        tsk.ActivityDate = Date.Today();
        tsk.Status = 'Completed';
        tsk.Description = g2Comments;
		tsk.CallDisposition=g2Source;
		if(isG2Checked == true){
			tsk.Subject = 'Candidate Summary/G2 Completed by ' + userName;
            insert tsk;
        }
        else if(isG2Checked == false && g2Comments != null && !String.isBlank(g2Comments) && !String.isEmpty(g2Comments)){
			tsk.Subject = 'Candidate Summary/G2 Edited by ' + userName;
            insert tsk;
        }
		
	}

	public static Id upsertAccountContactActivity(Contact contact, Account account, String g2Comments, string operation,String g2Source) {
	System.debug('-------g2Source3--------'+g2Source);
		account.Name = contact.FirstName + ' ' + contact.LastName; 
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        account.Talent_Committed_Flag__c = true;
        account.Talent_Visibility__c ='Private'; // To fire sharing rule - Nidish
		if(account.G2_Completed__c == true){
            account.G2_Completed_By__c = UserInfo.getUserId();
            account.G2_Completed_Date__c = Date.Today();
		}
		System.Savepoint sp = Database.setSavepoint();
        try {
            upsert account;
			contact.AccountId = account.Id;
			contact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
            contact.OwnerId = Userinfo.getUserId();
			upsert contact;
			createG2Activity(account.Id,contact.Id,g2Comments,account.G2_Completed__c,g2Source);
        } catch (DmlException ex) {
            System.debug('Failed account-contact validation --- ' + ex.getMessage());
			Database.rollback(sp);
            throw ex;
        }

		if (operation == 'Add') {
			return contact.Id;
		} else {
			return account.Id;
		}
	}
}