global class SchedulableBatchUserActivityCleanup implements Schedulable {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
     *  System.schedule('user Activity record cleanup job', '0 35 22 ? * FRI *', new SchedulableBatchUserActivityCleanup());
	 */ 
	global void execute(SchedulableContext sc) {

       UserActivityRecord_Cleanup_Batch ua = new UserActivityRecord_Cleanup_Batch(); 
       database.executeBatch(ua);
		
	}
}