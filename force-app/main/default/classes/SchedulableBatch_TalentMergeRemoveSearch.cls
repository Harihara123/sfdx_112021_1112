global class SchedulableBatch_TalentMergeRemoveSearch implements Schedulable {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	global void execute(SchedulableContext sc) {
     String query = 'SELECT Id, ESearch_Dup_Id__c, ESearch_Fetched__c FROM TalentMerge_RemoveDup__c where ESearch_Fetched__c = null limit 1000'; 
     TalentMerge_Remove_ESearch_PocBatch ua = new TalentMerge_Remove_ESearch_PocBatch(query); 
     database.executeBatch(ua);
		
	}
}