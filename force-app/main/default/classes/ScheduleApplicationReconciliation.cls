global class ScheduleApplicationReconciliation implements Schedulable {
   // global static String cron = '0 35 * * * ?';
    global string  opco;
    global Integer intervalInMinutes;
    
    global ScheduleApplicationReconciliation(String opco, Integer int_minutes){
        this.opco = opco;
        this.intervalInMinutes = int_minutes;
    }
    global static String setup(String cron, String schedularLabel,String opco, Integer intervalInMinutes){
        
        return System.schedule(schedularLabel, cron, new ScheduleApplicationReconciliation(opco,intervalInMinutes));
    }
    
    global void execute(SchedulableContext sc) {
        	system.debug('opco--'+opco);
        	system.debug('Interval__'+intervalInMinutes);
        	DateTime endDate = system.now();
        	DateTime startDate = endDate.addMinutes(-(intervalInMinutes));
            PhenomApplctnReconciliationController.callFailedApllicationService(opco, startDate, endDate );
           
    }

}