global class ScheduleApplicationRefactoring implements Schedulable {
    global static String SCHEDULE = '0 30 * * * ?';
    global static String schedularLabel='Application Refactoring Replay 1';
    global static string flag ='Replay';
   
    global static String setup(){
        
        return System.schedule(schedularLabel, SCHEDULE, new ScheduleApplicationRefactoring());
    }
    
    global void execute(SchedulableContext sc) {
        system.debug('flag-->'+flag);
        if(flag== 'Replay'){
           
            
			List<String> status = new List<String>{'Exception','DeDupe Exception','Talent Upsert Exception','Resume Upload Exception','Order/Event Upsert Exception'};
        	List<Application_Staging__c> applicationList = [SELECT Id, OwnerId, Name, JSON_Payload__c, Content_Type__c, CreatedDate,Combination_Key__c,
														Exception_Logs__c, File_Name__c, Source__c, Candidate_Last_Name__c, Candidate_First_Name__c, 
														Status__c, Candidate_Phone_Type__c, Candidate_Email__c, Candidate_Phone__c, Application_Date__c,
														Candidate_LinkedIN_URL__c, Found_Duplicate__c, DeDupe_TimeStamp__c, Vendor_ID__c, 
														Duplicate_AccountID__c, Duplicate_ContactID__c, Talent_Upsert_Account_Id__c, Order_Id__c,
														Talent_Upsert_TimeStamp__c, Talent_Upsert_Contact_Id__c, Talent_Document_Id__c, Event_Id__c,
														Order_Upsert_TimeStamp__c, Resume_Upload_TimeStamp__c, Attachment_ID__c,Replay_Eligible__c 
														FROM Application_Staging__c
														where Status__c IN: status  AND Replay_Eligible__c = true 
                                                            AND Source__c ='Phenom' AND CreatedDate = LAST_N_DAYS:1 order by Application_Date__c desc];
        
            List<Application_Staging__c> updateList = new List<Application_Staging__c>();
            for(Application_Staging__c app : applicationList){
                app.Status__c ='Replay Record';
                updateList.add(app);
                
                
            }
            update updateList;
        }
            
    }

}