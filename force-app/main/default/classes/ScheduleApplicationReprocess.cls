global class ScheduleApplicationReprocess implements Schedulable {
     global string  opco;
    
    global ScheduleApplicationReprocess(String opco_name){
        this.opco = opco_name;
    }
    global static String setup(String cron, String schedularLabel,String opco){
        return System.schedule(schedularLabel, cron, new ScheduleApplicationReprocess(opco));
    }
    
    global void execute(SchedulableContext sc) {
        	system.debug('opco--'+opco);
        	
        	List<String> applicationIds = new List<String>();
			PhenomReprocessFailedApplicationBatch BatchObject = new PhenomReprocessFailedApplicationBatch(opco,'ByApplicationID', applicationIds);
			Database.executeBatch(BatchObject, 10);
           
    }

}