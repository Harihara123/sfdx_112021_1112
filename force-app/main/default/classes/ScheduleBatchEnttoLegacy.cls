global with sharing class ScheduleBatchEnttoLegacy implements Schedulable {
global void execute(SchedulableContext sc) {
  ID BatchId = Database.executeBatch(new BatchCopyEnttoLegacy(), INTEGER.VALUEOF(LABEL.ENTTOlEGACY));
}
 
Public static void EnttoLegacySchedulerMethod() {
  //string timeinterval = '0 15 * * * ?';
  //System.schedule('ScheduleBatchEnttoLegacy-Every15mins',timeinterval, new ScheduleBatchEnttoLegacy());
  
  
  System.schedule('ScheduleBatchEnttoLegacy 1', '0 0 * * * ?', new ScheduleBatchEnttoLegacy());
System.schedule('ScheduleBatchEnttoLegacy 2', '0 15 * * * ?', new ScheduleBatchEnttoLegacy());
System.schedule('ScheduleBatchEnttoLegacy 3', '0 30 * * * ?', new ScheduleBatchEnttoLegacy());
System.schedule('ScheduleBatchEnttoLegacy 4', '0 45 * * * ?', new ScheduleBatchEnttoLegacy());
  }
}