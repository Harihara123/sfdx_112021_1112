/***************************************************************************************************************************************
* Name        - ScheduleBatch_CopyEnterpriseToLegacy
* Description - Class used to invoke BatchCopyEnttoLegacy
                class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Preetham Uppu               02/12/2018               Created
*****************************************************************************************************************************************/

global class ScheduleBatch_CopyEnterpriseToLegacy  implements Schedulable
{
   global void execute(SchedulableContext sc)
   {
        BatchCopyEnttoLegacy entToLegacyCopyBatch = new BatchCopyEnttoLegacy();
        Database.executeBatch(entToLegacyCopyBatch);            
   }
}