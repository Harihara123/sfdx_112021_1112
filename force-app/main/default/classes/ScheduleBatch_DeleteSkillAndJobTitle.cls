/***************************************************************************************************************************************
* Name        - ScheduleBatch_DeleteSkillAndJobTitle
* Description - Class used to invoke  Batch_DeleteSkillAndJobTitle class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Krishna                  01/18/2018               Created
*****************************************************************************************************************************************/

global class ScheduleBatch_DeleteSkillAndJobTitle implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        Batch_DeleteSkillAndJobTitle objBatch_DeleteSkillAndJobTitle = new Batch_DeleteSkillAndJobTitle();
        objBatch_DeleteSkillAndJobTitle.QueryReq = 'select id from Global_LOV__c where LOV_Name__c in :lovNames';
        Database.executeBatch(objBatch_DeleteSkillAndJobTitle,200);
    }
}