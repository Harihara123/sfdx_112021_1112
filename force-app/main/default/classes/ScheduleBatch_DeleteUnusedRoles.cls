/***************************************************************************************************************************************
* Name        - ScheduleBatch_DeleteUnusedRoles
* Description - Class used to invoke Batch_DeleteUnusedRoles
                class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Krishna                   04/28/2017               Created
*****************************************************************************************************************************************/

global class ScheduleBatch_DeleteUnusedRoles  implements Schedulable
{
   global void execute(SchedulableContext sc)
   {
       Batch_DeleteUnusedRoles batchJob = new Batch_DeleteUnusedRoles();
       //batchJob.query =  'Select Id,userroleid from User where Isactive=false and profileId not in (select id from profile where UserLicenseId in : ulics)';
       batchJob.query =  'Select Id,userroleid from User where Isactive=false and UserType = \'Standard\'';
       database.executebatch(batchJob,200);
  }
}