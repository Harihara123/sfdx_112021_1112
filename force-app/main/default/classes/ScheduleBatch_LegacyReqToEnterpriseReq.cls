/***************************************************************************************************************************************
* Name        - ScheduleBatch_LegacyReqToEnterpriseReq
* Description - Class used to invoke Batch_CopyLegacyReqToEnterpriseReq
                class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
*Krishna Chitneni               02/12/2018               Created
*****************************************************************************************************************************************/

global class ScheduleBatch_LegacyReqToEnterpriseReq  implements Schedulable
{
    
   global void execute(SchedulableContext sc)
    {
        Batch_CopyLegacyReqToEnterpriseReq legacyToEntCopyBatch = new Batch_CopyLegacyReqToEnterpriseReq();
        Database.executeBatch(legacyToEntCopyBatch );            
    }
}