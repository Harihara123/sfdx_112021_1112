/***************************************************************************************************************************************
* Name        - ScheduleBatch_ProcessEnterpriseReqHRXML
* Description - Class used to invoke Batch_ProcessEnterpriseReqHRXML class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Preetham                  01/31/2017               Created
*****************************************************************************************************************************************/

global class ScheduleBatch_ProcessEnterpriseReqHRXML implements Schedulable
{
   global void execute(SchedulableContext sc){
       Batch_ProcessEnterpriseReqHRXML batchJob = new Batch_ProcessEnterpriseReqHRXML();
       database.executebatch(batchJob,50);
  }
}