/***************************************************************************************************************************************
* Name        - ScheduleBatch_updateOpportunities
* Description - Class used to invoke Batch_DeleteUnusedRoles
                class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Krishna                   04/28/2017               Created
*****************************************************************************************************************************************/

global class ScheduleBatch_updateOpportunities  implements Schedulable
{
   global void execute(SchedulableContext sc)
   {
       Batch_CurrencyChangeUpdate batchJob = new Batch_CurrencyChangeUpdate();     
       
       batchJob.query =  'Select Id,RecordTypeId,Currency__c,Amount,Req_Total_Spread__c,Retainer_Fee__c,Req_Salary_Min__c,Req_Salary_Max__c,Req_Flat_Fee__c,Req_Additional_1st_Year_Compensation__c,Req_Bill_Rate_Min__c,Req_Bill_Rate_Max__c,Req_Pay_Rate_Min__c,Req_Pay_Rate_Max__c,Total_Compensation_Max__c,Total_Compensation_Min__c,Amount_Multi_Currency__c,Total_Spread_Multi_Currency__c,Project_Retainer_Fee_Multi_Currency__c,Salary_Min_Multi_Currency__c,Salary_Max_Multi_Currency__c,Flat_Fee_Multi_Currency__c,Additional_1st_Year_Comp_Converted__c,Bill_Rate_Min_Multi_Currency__c,Bill_Rate_Max_Multi_Currency__c,Pay_Rate_Min_Multi_Currency__c,Pay_Rate_Max_Multi_Currency__c,Total_Compensation_Max_Converted__c,Total_Compensation_Min_Converted__c from Opportunity where stagename in: setStageName  and Currency__c != \'USD – U.S Dollar\' and Req_Legacy_Req_Id__c = null';
       database.executebatch(batchJob,200);
  }
}