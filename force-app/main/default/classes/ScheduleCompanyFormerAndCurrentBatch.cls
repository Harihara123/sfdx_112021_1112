/***************************************************************************************************************************************
* Name        - ScheduleCompanyFormerAndCurrentBatch
* Description - Class used to invoke updateCompanyFormerAndCurrentDataBatch
                class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Krishna                   04/07/2017               Created
*****************************************************************************************************************************************/

global class ScheduleCompanyFormerAndCurrentBatch  implements Schedulable
{
   global void execute(SchedulableContext sc){
       Batch_updateCompanyFormerAndCurrentData batchJob = new Batch_updateCompanyFormerAndCurrentData();
       batchJob.query =  'Select Id,Display_LastModifiedDate__c,AccountId,Opportunity.OpCo__c from Order where Display_LastModifiedDate__c >:dtLastBatchRunTime and Opportunity.OpCo__c in: setCompanyName';
       database.executebatch(batchJob,100);
  }
}