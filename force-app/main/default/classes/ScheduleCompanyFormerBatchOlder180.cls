/***************************************************************************************************************************************
* Name        - ScheduleCompanyFormerBatchOlder180
* Description - Class used to invoke Batch_updateCompanyFormerAndCurrentData
                class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Krishna                   04/07/2017               Created
*****************************************************************************************************************************************/

global class ScheduleCompanyFormerBatchOlder180  implements Schedulable
{
   global void execute(SchedulableContext sc){
       Batch_updateCompanyFormerAndCurrentData batchJob = new Batch_updateCompanyFormerAndCurrentData();
       batchJob.query =  'Select Id,Display_LastModifiedDate__c,AccountId,Opportunity.OpCo__c from Order where Display_LastModifiedDate__c <:dtDiffOfTodayDate175 and Display_LastModifiedDate__c >:dtDiffOfTodayDate185 and Opportunity.OpCo__c in: setCompanyName and status = \'Started\'';
       database.executebatch(batchJob,100);
  }
}