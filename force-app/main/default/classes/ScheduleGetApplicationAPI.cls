Global class ScheduleGetApplicationAPI implements Schedulable {
	//global static String cron = '0 00 01 * * ?';
    //global static String schedularLabel='Phenom Success/Failure Applications';
     global  String  opco;
     global  Integer intervalInMinutes;
     global  String submissionStatus;
    
    global ScheduleGetApplicationAPI(String opco, Integer int_minutes, String submission_Status){
        this.opco = opco;
        this.intervalInMinutes = int_minutes;
        this.submissionStatus = submission_Status;
    }
    global static String setup(String cron, String schedularLabel,String opco, Integer intervalInMinutes, String submissionStatus ){
        
        return System.schedule(schedularLabel, cron, new ScheduleGetApplicationAPI(opco,intervalInMinutes,submissionStatus));
    }
    /*
    global void execute(SchedulableContext sc) {
        	DateTime endDate;
        	DateTime startDate;
        List<Phenom_Reconciliation_Request_Log__c> requestLog = new List<Phenom_Reconciliation_Request_Log__c>([Select id,StartDate__c,EndDate__c 
                                                             				from Phenom_Reconciliation_Request_Log__c
                                                             				where (Status__c ='Request Processed' OR Ignore_Slot__c = true)
                                                             				AND OPCO__c =: opco AND API_Name__c ='GetApplication'
                                                                            order by EndDate__c  desc limit 1]);
        
        if(requestLog.size()>0){
             endDate = requestLog[0].EndDate__c.addMinutes(intervalInMinutes);
        	 startDate = requestLog[0].EndDate__c.addMinutes(-2);
        }else{
             endDate = system.now();
        	 startDate = endDate.addMinutes(-(intervalInMinutes));
        }

            PhenomGetJobApplicationController.getJobApplicationData(opco, startDate, endDate, submissionStatus);
    }
	*/
    global void execute(SchedulableContext sc){
        DateTime endDate;
        DateTime startDate;
        PhenomReconciliationGetApplication_v2.ParamWRapper wrap = new PhenomReconciliationGetApplication_v2.ParamWRapper();
        wrap.status = '';
        wrap.pageNumber=1;
        /*
        List<Phenom_Reconciliation_Request_Log__c> requestLog = new List<Phenom_Reconciliation_Request_Log__c>([Select id,StartDate__c,EndDate__c,OPCO__c 
                                                             				from Phenom_Reconciliation_Request_Log__c
                                                             				where (Status__c ='Request Processed' OR Ignore_Slot__c = true)
                                                             				AND OPCO__c =: opco AND API_Name__c ='GetApplication'
                                                                            order by EndDate__c  desc limit 1]);
        
        if(requestLog.size()>0){
        	wrap.opco_name =requestLog[0].OPCO__c;
            wrap.startDate = requestLog[0].EndDate__c.addMinutes(-2);
            wrap.endDate = requestLog[0].EndDate__c.addMinutes(intervalInMinutes);
        }else{
		*/
            wrap.opco_name =opco;
            wrap.endDate = system.now();
            wrap.startDate = wrap.endDate.addMinutes(-(intervalInMinutes));
            
        //}

 		PhenomReconciliationGetApplication_v2.callApi(wrap);
    }
}