public with sharing class ScheduleLTRProvisioner implements Schedulable {
    public static String schedule = '0 30 2 * * ?';

    public static void setup() {
		System.schedule('LTR Provisioner', SCHEDULE, new ScheduleLTRProvisioner());
    }
   	
    public void execute(SchedulableContext sc){
        LTRProvisioner batchJob = new LTRProvisioner();
        database.executebatch(batchJob);
    }
}