global with sharing class ScheduleLegacyToEnterpriseReq implements Schedulable {
global void execute(SchedulableContext sc) {
  ID BatchId = Database.executeBatch(new Batch_CopyLegacyReqToEnterpriseReq(),INTEGER.VALUEOF(LABEL.LegacytoEnterprise));
}
 
Public static void LegacyToEnterpriseReq() {
 
System.schedule('ScheduleBatchLegacytoEnt 1', '0 0 * * * ?', new ScheduleLegacyToEnterpriseReq());
System.schedule('ScheduleBatchLegacytoEnt 2', '0 15 * * * ?', new ScheduleLegacyToEnterpriseReq());
System.schedule('ScheduleBatchLegacytoEnt 3', '0 30 * * * ?', new ScheduleLegacyToEnterpriseReq());
System.schedule('ScheduleBatchLegacytoEnt 4', '0 45 * * * ?', new ScheduleLegacyToEnterpriseReq());
  }
}