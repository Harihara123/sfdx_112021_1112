Global class SchedulePhenomSuccessComparison implements Schedulable{

    global static String setup(String cron, String schedularLabel){
        return System.schedule(schedularLabel, cron, new SchedulePhenomSuccessComparison());
    }
    global void execute(SchedulableContext sc) {
        PhenomSuccessRecordComparisonBatch b1 = new PhenomSuccessRecordComparisonBatch('Phenom');
        database.executebatch(b1,2000);
        
        PhenomSuccessRecordComparisonBatch b2 = new PhenomSuccessRecordComparisonBatch('WCS');
        database.executebatch(b2,500);
    }
    
}