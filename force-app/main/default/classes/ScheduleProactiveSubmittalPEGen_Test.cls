@isTest
public class ScheduleProactiveSubmittalPEGen_Test  {
	@TestSetup
	static void testDataSetup() {
		User usr = TestDataHelper.createUser('Single Desk 1');
        insert usr; 

		Account acc1 = TestData.newAccount(1, 'Client');
		insert acc1;
		Contact ct1 = TestData.newContact(acc1.id, 3, 'Client') ;
		insert ct1;

		Order sub = TestData.newOrder(acc1.id, ct1.id, usr.id, 'Proactive');
        insert sub;
		System.debug('ScheduleProactiveSubmittalPEGen_Test--sub----'+sub);
	}
	@IsTest
	static void ScheduleProactiveSubmittalPEGenerationtest() {
		List<Id> ordList = new List<Id>();
		Test.startTest();
			for(Order ord : [Select Id, ShipToContactId, CreatedById From Order ]) {
				System.debug('ScheduleProactiveSubmittalPEGen_Test----'+ord);
				ordList.add(ord.id);
			}
			String label='Test :: ';
			Datetime currTime = System.now();
			currTime = currTime.addMinutes(5);
			Integer ss = currTime.second();
			Integer mm = currTime.minute() ;
			Integer hh = currTime.hour();
			Integer day = currTime.day();
			Integer mn = currTime.month();
			Integer yr = currTime.year();
			String CRON_EXP = String.valueOf(ss) + ' ' + String.valueOf(mm) + ' ' + String.valueOf(hh) + ' ' + String.valueOf(day) + ' ' + String.valueOf(mn) + ' ' +  '?'  + ' ' + String.valueOf(yr) ;

			label = label + currTime.getTime();

			System.schedule(label,  CRON_EXP, new ScheduleProactiveSubmittalPEGeneration(ordList));

		Test.stopTest();
	}
}