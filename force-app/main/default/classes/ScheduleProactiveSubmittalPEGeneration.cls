/**
@author: Neel Kamal
@date: 04/23/2020
@description: This class have the functionality to generate platform event. PE will subscribe by SOA to send the Proactive Submittal to RWS.
			  It is scheduled from invocable method ProactiveSubmittalPEGeneration.SchedulePEGeneration. 
			  It is schedule after 5 minute of Opportunity has been sent to RWS.
**/
public class ScheduleProactiveSubmittalPEGeneration implements Schedulable {
	List<Id> orderIds;
	
	public ScheduleProactiveSubmittalPEGeneration(List<Id> orderIds) {
		this.orderIds = orderIds;
	}
	//This method will genrate plateform event one Proactive Submittal status changed to 'Offer Accepted'. It will also log PE data into Order_Platform_Event_Log__c.
	public void execute(SchedulableContext sc) {
		List<Connected_To_RWS_Submittal_Sync__e> peList = new List<Connected_To_RWS_Submittal_Sync__e> ();
		List<Order_Platform_Event_Log__c> peLogList = new List<Order_Platform_Event_Log__c> ();
		
		for(Order ordr :  [Select Id, ShipToContactId, CreatedById from Order Where id =: orderIds]) {
			Connected_To_RWS_Submittal_Sync__e pe = new Connected_To_RWS_Submittal_Sync__e();
			pe.Id__c = ordr.Id;
			pe.ShipToContactId__c = ordr.ShipToContactId;
			peList.add(pe);

			Order_Platform_Event_Log__c peLog = new Order_Platform_Event_Log__c();
			peLog.Order_Id__c = ordr.Id;
			peLog.Name = 'Send Proactive Submittal to RWS';
			peLog.User__c = ordr.CreatedById;
			peLog.ShipToContactId__c = ordr.ShipToContactId;
			peLogList.add(peLog);
		}
		
		boolean isPublished = false;
		if (peList.size() > 0) {
			List<Database.SaveResult> results = EventBus.publish(peList);

			for (Database.SaveResult sr : results) {
				if (sr.isSuccess()) {
					isPublished = true;
				} else {
					String errors = 'PE Generation Error ::: ' ;
					Integer i = 1;
					for (Database.Error err : sr.getErrors()) {
						errors += i + '. '+ err.getStatusCode() + ' - ' + err.getMessage() + ' ';
						System.debug('OnOfferAccepted::::Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
					}
					
					ConnectedLog.LogInformation('ScheduleProactiveSubmittalPEGeneration', 'execute', errors);
				}
			}
		}

		if (isPublished && peLogList.size() > 0) {
			try {
				insert(peLogList);
			} catch(DMLException dexp) {
				ConnectedLog.LogException('ScheduleProactiveSubmittalPEGeneration', 'execute', dexp);
			}
		}
	}
}