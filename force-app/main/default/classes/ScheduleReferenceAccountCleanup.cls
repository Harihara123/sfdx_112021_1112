global class ScheduleReferenceAccountCleanup implements Schedulable{
    
    public static String SCHEDULE = '0 30 4 * * ?';

    global static String setup() {
        String schedularLabel;
        if (Test.isRunningTest()){
            schedularLabel ='Test Schedular';
        }else{
            schedularLabel ='Orphan Reference Accounts Cleanup';
        }
		return System.schedule(schedularLabel, SCHEDULE, new ScheduleReferenceAccountCleanup());
    }

    global void execute(SchedulableContext sc) {
      	UncommittedObjectsCleanupBatch b = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.REFERENCE_ACCOUNT, null);
      	database.executebatch(b);
   	}

}