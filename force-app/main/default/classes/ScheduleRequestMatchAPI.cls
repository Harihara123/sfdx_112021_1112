global class ScheduleRequestMatchAPI implements Schedulable {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	public Map<Id,TalentFitment__c> talentFitmentMap;
	public static Integer recordsCreatedBefore;
	public static Integer recordsCreatedAfter;
	public static Integer maxRetries;
	Static String CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID = 'APPPRIORITY/ScheduleRequestMatchAPI';
	Static String CONNECTED_LOG_CLASS_NAME = 'ScheduleRequestMatchAPI';
	public static List<TalentFitment__c> notEligibleRecs;
	
	global ScheduleRequestMatchAPI() {		
		
		talentFitmentMap = new Map<Id,TalentFitment__c>();
	}

	global void execute(SchedulableContext sc) {
		try{
			queueableCallouts();
		} catch(Exception ex) {
			ConnectedLog.LogException(CONNECTED_APPLICATION_PRIORITIZATION_EXCEPTION_LOG_TYPE_ID,CONNECTED_LOG_CLASS_NAME,'execute','','', ex);	
		}
	}
    
    public void queueableCallouts() {
		List<Id> eventIds = new List<Id>();
		App_Prioritization_Settings__c settings = App_Prioritization_Settings__c.getValues('AppPriority');
		recordsCreatedBefore = (Integer)settings.RecordsCreatedBeforeTime__c;
		recordsCreatedAfter = (Integer)settings.RecordsCreatedAfterTime__c;
		maxRetries = Integer.valueOf(Label.ATS_Max_Retries);
		notEligibleRecs = new List<TalentFitment__c>();
		//All Talent fitment records which are processed in queueable batch, their status is changed to Processing
		talentFitmentMap =  fetchTalentFitmentDetails();	
		System.debug('notEligibleRecs:'+notEligibleRecs);	
        updateTalentFitmentStatus();
		if(notEligibleRecs.size() > 0) {
			update notEligibleRecs;
		}
		
		for(TalentFitment__c tft:talentFitmentMap.values()) {
			if(!tft.IsNotEligible__c)
				eventIds.add(tft.ApplicantId__c);

		}
		if(eventIds.size() > 0) {
			Id jobID = System.enqueueJob(new MatchAPICalloutsQueueble(eventIds,talentFitmentMap));
		}
	}
	
	private static Map<Id,TalentFitment__c> fetchTalentFitmentDetails() {
		Map<Id,TalentFitment__c> talentFitmentMap = new Map<Id,TalentFitment__c>();
		DateTime recordsCreatedbeforeTime =Datetime.now().addMinutes(recordsCreatedBefore);
		DateTime recordsCreatedafterTime =Datetime.now().addHours(recordsCreatedAfter);
		List<String> statusList = new List<String>{'New','Not Ingested','Partially Completed'};
		for(TalentFitment__c talentFit:[SELECT Id,ApplicantId__c,Matched_Opportunities__c,Score__c,Status__c,API_Skills__c,
										Matched_Candidate_Status__c,Matched_Desired_Pay__c,Retry_Count__c,Applicant_Name__c,
										IsNotEligible__c,Email_Subject__c
                                        FROM TalentFitment__c WHERE Status__c IN :statusList AND CreatedDate <=
										:recordsCreatedbeforeTime AND CreatedDate >:recordsCreatedafterTime AND Retry_Count__c <= :maxRetries ORDER By CreatedDate DESC LIMIT 500]) {
			if(!talentFit.IsNotEligible__c) {
				talentFitmentMap.put(talentFit.Id,talentFit);
			} else {
				talentFit.Email_Subject__c = 'Connected Application From '+talentFit.Applicant_Name__c;
				talentFit.Status__c = 'Not Eligible'; 
				notEligibleRecs.add(talentFit);
			}
		}				
		return talentFitmentMap;
	}	
    
    private void updateTalentFitmentStatus() {
		List<TalentFitment__c> tftList = new List<TalentFitment__c>();
     	   
        if(talentFitmentMap != null) {
            
            for(TalentFitment__c tft:talentFitmentMap.values()) {                
				tft.Status__c = 'Processing'; 				
				tftList.add(tft);
            }
            update tftList; 
        }
    }
	
}