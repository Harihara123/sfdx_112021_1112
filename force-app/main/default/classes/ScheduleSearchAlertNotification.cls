global  class ScheduleSearchAlertNotification implements Schedulable{
    public static String SCHEDULE = '0 0 5 * * ?';
    public static String schedularLabel='Search Alert Notification';
    global static String setup() {
        
        return System.schedule(schedularLabel, SCHEDULE, new ScheduleSearchAlertNotification());
    }
    
    global void execute(SchedulableContext sc) {
        //SearchAlertNotificationBatch b = new SearchAlertNotificationBatch();
        //database.executebatch(b,50);
        
        SearchAlertNotificationBatch_v2 b2 = new SearchAlertNotificationBatch_v2();
        database.executebatch(b2,50);
    }
    
}