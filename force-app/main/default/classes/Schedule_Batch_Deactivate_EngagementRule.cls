/***************************************************************************************************************************************
* Name        - Schedule_Batch_Deactivate_EngagementRule
* Description - Class used to invoke Batch_Deactivate_EngagementRule class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Nidish                  11/17/2016               Created
*****************************************************************************************************************************************/

global class Schedule_Batch_Deactivate_EngagementRule implements Schedulable
{
   global void execute(SchedulableContext sc){
       Batch_Deactivate_EngagementRule batchJob = new Batch_Deactivate_EngagementRule();
       database.executebatch(batchJob,200);
  }
}