/***************************************************************************************************************************************
* Name        - Schedule_Batch_HRXML_CanLists
* Description - Class to invoke Batch_HRXML_CanLists class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Karthik                    12/11/2017             Created
*****************************************************************************************************************************************/

global class Schedule_Batch_HRXML_CanLists implements Schedulable
{	
	/**************S-223554 - *Tech Debt: Stop Generating HR-XML on My Lists Batch Job-Debasis*******/
	//-----------------------Blocking HRXML Candidate Lists Generation----------------//
    public static String SCHEDULE = '0 0 0/3 * * ?';

    global static void setup() {
		System.schedule('HRXML Candidate Lists', SCHEDULE, new Schedule_Batch_HRXML_CanLists());
    }
   	
    global void execute(SchedulableContext sc){
        
       Batch_HRXML_CanLists batchJob = new Batch_HRXML_CanLists();
        batchJob.QueryObject = 'Select Id, Contact__c, LastModifiedDate from Contact_Tag__c where LastModifiedDate >= : batchLastRun ALL ROWS';
        database.executebatch(batchJob,200);
    }
}