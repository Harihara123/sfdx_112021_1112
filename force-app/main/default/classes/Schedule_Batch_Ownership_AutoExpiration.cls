/***************************************************************************************************************************************
* Name        - Schedule_Batch_Ownership_AutoExpiration
* Description - Class to invoke Batch_Ownership_AutoExpiration class
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Karthik                    11/21/2017             Created
*****************************************************************************************************************************************/

global class Schedule_Batch_Ownership_AutoExpiration implements Schedulable
{
   global void execute(SchedulableContext sc){
       Batch_Ownership_AutoExpiration batchJob = new Batch_Ownership_AutoExpiration();
       database.executebatch(batchJob,200);
  }
}