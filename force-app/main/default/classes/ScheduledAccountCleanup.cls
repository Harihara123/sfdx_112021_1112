global class ScheduledAccountCleanup implements Schedulable {
    public static String SCHEDULE = '0 12 * * * ?';

    global static String setup() {
		return System.schedule('Talent Accounts Cleanup', SCHEDULE, new ScheduledAccountCleanup());
    }

    global void execute(SchedulableContext sc) {
      	UncommittedObjectsCleanupBatch b = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.ACCOUNT, null);
      	database.executebatch(b);
   	}
    
}