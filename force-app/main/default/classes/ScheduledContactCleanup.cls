global class ScheduledContactCleanup implements Schedulable {
    public static String SCHEDULE = '0 1 * * * ?';

    global static String setup() {
		return System.schedule('Talent Contacts Cleanup', SCHEDULE, new ScheduledContactCleanup());
    }

    global void execute(SchedulableContext sc) {
      	UncommittedObjectsCleanupBatch b = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.CONTACT, null);
      	database.executebatch(b);
   	}
}