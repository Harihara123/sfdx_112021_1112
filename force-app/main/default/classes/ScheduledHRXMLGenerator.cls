global class ScheduledHRXMLGenerator implements Schedulable {
  	// The minute of hour for first execution
  	public static Integer STARTING_MINUTE = 7;
  	private Integer BATCH_SIZE_PER_EXEC;
  	private String runId;
    private Integer dbBatchSize;

    /**
     * Setup scheduled jobs based on input
     * 
     * @param  batchesPerHour Number of batched per hour
     * @param  sizePerBatch   Size of each batch
     * @param  runId 		      The name for the custom setting entry that tracks this overall batch
     * @param  dbBatchSize    Size parameter for Database.executeBatch()
     * @return                The concated value of all the System.schedule() call return values
     */
    global static String setup(Integer batchesPerHour, Integer sizePerBatch, String runId, Integer dbBatchSize) {
    	String retVal = '';
    	// Interval for job scheduling.
    	Integer interval = 60 / batchesPerHour;
    	for (Integer i=0; i<batchesPerHour; i++) {
    		// Create cron expression
    		String cron = '0 ' + (STARTING_MINUTE + (i * interval)) + ' * * * ?';
    		retVal = '~' + System.schedule('Scheduled HRXML Generator - ' + i, cron, new ScheduledHRXMLGenerator(sizePerBatch, runId, dbBatchSize));
    		// System.debug('*************** ' + schedule + ' -- ' + sizePerBatch);
    	}
		  return retVal;
    }

    global ScheduledHRXMLGenerator(Integer sizePerBatch, String runId, Integer dbBatchSize) {
    	this.BATCH_SIZE_PER_EXEC = sizePerBatch;
    	this.runId = runId;
      	this.dbBatchSize = dbBatchSize;
    }

    global void execute(SchedulableContext sc) {
    	String batchQuery = 'SELECT Id, Source_System_Id__c FROM Account WHERE RecordType.Name=\'Talent\' ' 
            + 'AND Talent_Ownership__c=\'' + getTalentOwnership(runId) + '\' AND Source_System_Id__c > \'' + getLastProcessedTalentId(runId) 
            + '\' ORDER BY Source_System_Id__c ASC LIMIT ' + BATCH_SIZE_PER_EXEC;
    	System.debug('Batch query **** ' + batchQuery);
    	// Initiate the batch
      	HRXMLBatch bt = new HRXMLBatch(batchQuery);
      	Database.executeBatch(bt, dbBatchSize);

      	// Update the last processed talent in the custom settings.
      	updateLastProcessedTalentId(batchQuery, runId);
   	}

   	/**
   	 * Get the last processed talent id from the Custom settings.
   	 * @param  runId 		  The name for the custom setting entry that tracks this overall batch
   	 * @return the Last_Talent__c entry in the custom setting entry.
   	 */
   	@TestVisible 
    private static String getLastProcessedTalentId(String runId) {
   		String talentId = '';

   		Scheduled_HRXML_Run__c run = Scheduled_HRXML_Run__c.getValues(runId);
   		if (run != null) {
   			talentId = run.Last_Talent__c == null ? '' : run.Last_Talent__c;
   		}
   		
   		return talentId;
   	}

    /**
     * Get the Talent Ownership from the Custom settings.
     * @param  runId      The name for the custom setting entry that tracks this overall batch
     * @return the Talent_Ownership__c entry in the custom setting entry.
     */
    @TestVisible 
    private static String getTalentOwnership(String runId) {
      String ownership = '';

      Scheduled_HRXML_Run__c run = Scheduled_HRXML_Run__c.getValues(runId);
      if (run != null) {
        ownership = run.Talent_Ownership__c == null ? '' : run.Talent_Ownership__c;
      }
      
      return ownership;
    }

   	/**
   	 * Update the last processed talent id into the custom setting entry.
   	 * @param  batchQuery The query that was just processed. Used to find the last processed talent id
   	 * @param  runId      The name for the custom setting entry that tracks this overall batch
   	 * @return            true if updated, false if not.
   	 */
   	@TestVisible 
    private static Boolean updateLastProcessedTalentId(String batchQuery, String runId) {
   		List<Account> actList = Database.query(batchQuery);
   		String talentId = actList[actList.size() - 1].Source_System_Id__c;

   		Scheduled_HRXML_Run__c run = Scheduled_HRXML_Run__c.getValues(runId);
   		if (run != null) {
   			run.Last_Talent__c = talentId;
   			update run;
   			return true;
   		}

   		return false;
   	}
}