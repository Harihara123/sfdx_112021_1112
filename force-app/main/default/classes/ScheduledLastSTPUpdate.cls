global class ScheduledLastSTPUpdate implements Schedulable {
	public static String SCHEDULE = '0 20 * * * ?';

    global static String setup() {
		return System.schedule('Last Service Touchpoint date update', SCHEDULE, new ScheduledLastSTPUpdate());
    }

	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	global void execute(SchedulableContext sc) {
		BatchableLastSTPDateUpdater stpUpdater = new BatchableLastSTPDateUpdater(2);
		Database.executeBatch(stpUpdater);
	}

}