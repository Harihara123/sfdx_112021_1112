global class ScheduledTalentDocCleanup implements Schedulable {
    public static String SCHEDULE = '0 51 * * * ?';

    global static String setup() {
		return System.schedule('Talent Documents Cleanup', SCHEDULE, new ScheduledTalentDocCleanup());
    }

    global void execute(SchedulableContext sc) {
      	UncommittedObjectsCleanupBatch b = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.TALENT_DOCUMENT, null);
      	database.executebatch(b);
   	}
}