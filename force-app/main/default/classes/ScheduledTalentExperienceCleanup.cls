global class ScheduledTalentExperienceCleanup implements Schedulable {
    public static String SCHEDULE = '0 49 * * * ?';

    global static String setup() {
		return System.schedule('Talent Experience Cleanup', SCHEDULE, new ScheduledTalentExperienceCleanup());
    }

    global void execute(SchedulableContext sc) {
      	UncommittedObjectsCleanupBatch b = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.TALENT_EXPERIENCE, null);
      	database.executebatch(b);
   	}
}