public without sharing class Script_RWSCandidateResumeIds {        
    public static void readDocument(){
        Document[] docs = [select Body from Document where Name = 'RWS_Candidate_Resume_id'];
        List<String> recordIds = new List<String>();
        List<List<String>> allFields = Script_RWSCandidateResumeIds.parseCSV(docs[0].Body.toString(),true);
		for(List<String> tempList : allFields){
            if(tempList.size()>0){
				recordIds.add(tempList[0].trim());
            }            
        }
        List<talent_document__c> result = Script_RWSCandidateResumeIds.getTalentDocuments(recordIds);
        Script_RWSCandidateResumeIds.createCSVDocument(result);
    }
    
    public static List<talent_document__c> getTalentDocuments(List<String> inputIds){
        
        List<talent_document__c> result = new List<talent_document__c>();

        try{
            
            result = [Select source_system_id__c,id, Azure_URL__c from talent_document__c where source_system_id__c in :inputIds];
            
        }catch(Exception e){
            System.debug('Exception in getTalentDocuments ' + e.getCause());   
        }            
      
        return result;
        
    }
    
    public static void createCSVDocument(List<talent_document__c> result){
        List<String> csvRows = new List<String>();
        for(talent_document__c talentDoc : result){
            String rowValues = talentDoc.source_system_id__c + ',' + talentDoc.id + ','+talentDoc.Azure_URL__c;
            csvRows.add(rowValues);
        }
        try{
            String csvHeaders = 'Source System Id, Talent Doc Id, Azure URL';
            String fileName = 'RWS_Candidate_Resume_Result';
            String csvFile = csvHeaders + String.join(csvRows, '\n');
            //Create document
            Document doc = new Document();
            doc.Body = Blob.valueOf(csvFile);
            doc.Name = fileName;
            doc.Type = 'csv';
            doc.ContentType = 'application/vnd.ms-excel';
            doc.FolderId = UserInfo.getUserId();
            insert doc;
        }catch(Exception e){
            System.debug('createCSVDocument ' + e.getCause());
        }
        System.debug('createCSVDocument created document.');
    }
    
    
    
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
	List<List<String>> allFields = new List<List<String>>();

	// replace instances where a double quote begins a field containing a comma
	// in this case you get a double quote followed by a doubled double quote
	// do this for beginning and end of a field
	contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
	// now replace all remaining double quotes - we do this so that we can reconstruct
	// fields with commas inside assuming they begin and end with a double quote
	contents = contents.replaceAll('""','DBLQT');
	// we are not attempting to handle fields with a newline inside of them
	// so, split on newline to get the spreadsheet rows
	List<String> lines = new List<String>();
	try {
		lines = contents.split('\n');
	} catch (System.ListException e) {
		System.debug('Limits exceeded?' + e.getMessage());
	}
	Integer num = 0;
	for(String line : lines) {
		// check for blank CSV lines (only commas)
		if (line.replaceAll(',','').trim().length() == 0) break;
		
		List<String> fields = line.split(',');	
		List<String> cleanFields = new List<String>();
		String compositeField;
		Boolean makeCompositeField = false;
		for(String field : fields) {
			if (field.startsWith('"') && field.endsWith('"')) {
				cleanFields.add(field.replaceAll('DBLQT','"'));
			} else if (field.startsWith('"')) {
				makeCompositeField = true;
				compositeField = field;
			} else if (field.endsWith('"')) {
				compositeField += ',' + field;
				cleanFields.add(compositeField.replaceAll('DBLQT','"'));
				makeCompositeField = false;
			} else if (makeCompositeField) {
				compositeField +=  ',' + field;
			} else {
				cleanFields.add(field.replaceAll('DBLQT','"'));
			}
		}
		
		allFields.add(cleanFields);
	}
	if (skipHeaders) allFields.remove(0);
	return allFields;		
	}
}