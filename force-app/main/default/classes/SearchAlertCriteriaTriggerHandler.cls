public with sharing class SearchAlertCriteriaTriggerHandler  {
private static boolean hasBeenProccessed = false;

   public void OnAfterInsert(List<Search_Alert_Criteria__c> newRecords) {
       callDataExport(newRecords);
    }
     
     
    public void OnAfterUpdate (List<Search_Alert_Criteria__c> newRecords) {        
       callDataExport(newRecords);
    }

	public void OnAfterDelete (Map<Id, Search_Alert_Criteria__c>oldMap) { 
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');       
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'SearchAlertCriteriaTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' Search_Alert_Criteria__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Search_Alert_Criteria__c');
            hasBeenProccessed = true; 
        }
    }

    private static void callDataExport(List<Search_Alert_Criteria__c> newRecords) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'SearchAlertCriteriaTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' Search_Alert_Criteria__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Search_Alert_Criteria__c');
            hasBeenProccessed = true; 
        }

    } 


}