global class SearchAlertNotificationBatch implements Database.Batchable<sObject> {
    String Query;
    global SearchAlertNotificationBatch(){
        Query ='Select id, Date__c, Alert_Criteria__c,Results__c,Alert_Criteria__r.Name,'+
            	'Alert_Criteria__r.OwnerId,Alert_Criteria__r.Owner.Firstname, Alert_Criteria__r.Owner.Email, Alert_Criteria__r.Send_Email__c, Alert_Criteria__r.Alert_Trigger__c' +
            	' from Search_Alert_Result__c where  Date__c = Yesterday AND Alert_Criteria__r.Send_Email__c = true';
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
    	 return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<Search_Alert_Result__c> scope) {
        try {
            	AlertNotificationController.sendNotification(scope);
            
        } catch (Exception e) {
            System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
            System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
            System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
            System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
            System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
        String subject = 'Alert Notification Batch Status';
        
        mail.setSubject(subject + a.Status);
        mail.setPlainTextBody
            ('The batch Apex job processed ' + a.TotalJobItems +
             ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}