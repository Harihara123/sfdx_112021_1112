global class SearchAlertNotificationBatch_v2 implements Database.Batchable<sObject> {
    String Query;
    global SearchAlertNotificationBatch_v2(){
        Query ='SELECT Id, Name, Send_Email__c, Alert_Frequency__c,'+
            	'OwnerId,Owner.Firstname, Owner.Email,Alert_Trigger__c'+
            	' FROM Search_Alert_Criteria__c'+
            	' where Send_Email__c=true';
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
    	 return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<Search_Alert_Criteria__c> scope) {
        try {
            system.debug('Scope-->'+scope);
            Set<Id> alertIdSet = new Set<Id>();
            Map<String,String> tempMap = new Map<String,String>();
            List<AlertNotificationController.EmailNotificationWrapper> emailBodyWrapperList = new 
                													List<AlertNotificationController.EmailNotificationWrapper>();
            
            for(Search_Alert_Criteria__c ac : scope){
                alertIdSet.add(ac.Id);
            }
            
            for(Search_Alert_Result__c res : [select id,Alert_Criteria__c,Results__c from Search_Alert_Result__c 
                                             where Alert_Criteria__c IN: alertIdSet AND Date__c = Yesterday AND Version__c ='v2'
                                             order by LastModifiedDate desc] ){
                     if(tempMap.containsKey(string.valueOf(res.Alert_Criteria__c))){
                            String tempString =tempMap.get(res.Alert_Criteria__c)+','+res.Results__c; 
							tempMap.put(string.valueOf(res.Alert_Criteria__c),tempString);                         
                     }else{
                        tempMap.put(string.valueOf(res.Alert_Criteria__c),res.Results__c); 
                     }
                                                 
             }
            for(Search_Alert_Criteria__c ac : scope){
                if(tempMap.containsKey(String.valueOf(ac.Id))){
                    String resultJson ='{"results":['+tempMap.get(String.valueOf(ac.Id))+']}';
                 	AlertNotificationController.EmailNotificationWrapper wrapObject = new 
                        											AlertNotificationController.EmailNotificationWrapper();  
                    
                    wrapObject.alertName=ac.Name;
                    wrapObject.userEmail = ac.Owner.Email;
                    wrapObject.userName = ac.Owner.FirstName;
                    wrapObject.userId = ac.OwnerId;
                    
                    if(ac.Alert_Trigger__c =='Talent Created'){
                        list<SearchAlertResultsModel.SearchAlertResultModel> temp_resultList = 
                            						new list<SearchAlertResultsModel.SearchAlertResultModel>();
                        
                        wrapObject.subject = ac.Name +'- Talent Alert Results';
                        SearchAlertResultsModel wrapResults = (SearchAlertResultsModel)JSON.deserialize
                            									(resultJson, SearchAlertResultsModel.class);
                        
                        temp_resultList.addAll(wrapResults.results);
                        wrapObject.totalRecordCount = temp_resultList.size();
                        
                        Integer index=0;
                        for(SearchAlertResultsModel.SearchAlertResultModel obj : temp_resultList){
                            if(index<=9){
                                wrapObject.final_resultList.add(obj);  
                                index++;  
                            }
                            else{
                                break; 
                            }
                            
                        }
                        
                    }else{
                        list<ReqSearchAlertResultsModel.ReqSearchAlertResultModel> tempReqResultList = 
                            							new list<ReqSearchAlertResultsModel.ReqSearchAlertResultModel>();
                        wrapObject.subject = ac.Name +'- Req Alert Results';
                        ReqSearchAlertResultsModel wrapResults = (ReqSearchAlertResultsModel)JSON.deserialize
                            										(resultJson, ReqSearchAlertResultsModel.class);
                        
                        tempReqResultList.addAll(wrapResults.results);
                        wrapObject.totalRecordCount = tempReqResultList.size();
                        
                        Integer index=0;
                        for(ReqSearchAlertResultsModel.ReqSearchAlertResultModel obj : tempReqResultList){
                            if(index<=9){
                                wrapObject.final_req_resultList.add(obj);  
                                index++;  
                            }
                            else{
                                break; 
                            }
                            
                        }
                        
                    }
                	emailBodyWrapperList.add(wrapObject);
                }
             	 
            }
            
            if(emailBodyWrapperList.size()>0)
                AlertNotificationController.sendEmailMethod(emailBodyWrapperList);
            
        } catch (Exception e) {
            system.debug('message'+e.getMessage()+'--- Line Number'+e.getLineNumber());
            ConnectedLog.LogException('AlertNotificationBatch','SearchAlertNotificationBatch_v2', 'execute', e);            
        }
    }
    
    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
        String subject = 'Alert Notification Batch Status';
        
        mail.setSubject(subject + a.Status);
        mail.setPlainTextBody
            ('The batch Apex job processed ' + a.TotalJobItems +
             ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}