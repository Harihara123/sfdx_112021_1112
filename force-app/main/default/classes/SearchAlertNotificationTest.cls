@isTest
public class SearchAlertNotificationTest {
    
        public static testmethod void coverSchedular(){ 
            Test.startTest(); 
                ScheduleSearchAlertNotification.SCHEDULE='0 0 5 * * ?';
                ScheduleSearchAlertNotification.schedularLabel ='TestAlertSchedular';
                ScheduleSearchAlertNotification.setup();
            Test.stopTest();
         
        }
    	
    	public static testmethod void coverNotifications(){ 
            
			User thisUser = [SELECT Id,FirstName, Email FROM User WHERE Id = :UserInfo.getUserId()];

                Test.startTest(); 
                Search_Alert_Criteria__c criteria = new Search_Alert_Criteria__c( Alert_Criteria__c = 'Test', Name='Test', Alert_Frequency__c = 'Daily', Query_String__c = 'xyz' );
                insert criteria;
                criteria.OwnerId	= thisUser.id;
               
            	String jsonResult = '{"results":[{"candidateId":"0030D000009F3YvQAK","givenName":"Valarie","familyName":"Peterson","candidateStatus":"Current","city":"Meridian","state":"Mississippi","country":"United States","createdDate":"2019-01-24T15:08:24.000Z","alertId":"a3r0D0000004TzSQAU","teaser":"This is a resume teaser for this alert match","alertTrigger":null,"alertMatchTimestamp":"2019-02-13T15:08:24.000Z"}]}';
                Search_Alert_Result__c result = new Search_Alert_Result__c( Alert_Criteria__c = criteria.Id , Results__c = jsonResult , Date__c = System.today() );
                insert result;
                
                List<Search_Alert_Result__c> lstSearchAlertResult = new List<Search_Alert_Result__c>();
                lstSearchAlertResult.add(result);
                
            	AlertNotificationController a = new AlertNotificationController();
                AlertNotificationController.EmailNotificationWrapper b = new AlertNotificationController.EmailNotificationWrapper();
                AlertNotificationController.EmailNotificationWrapper.sendNotification(lstSearchAlertResult);
            
    }
    
    public static testmethod void coverSendEmailMethod(){ 
        
        list<SearchAlertResultsModel.SearchAlertResultModel> final_resultList = new List<SearchAlertResultsModel.SearchAlertResultModel>();
        SearchAlertResultsModel.SearchAlertResultModel obj = new SearchAlertResultsModel.SearchAlertResultModel();
        obj.candidateId = 'CID';
        obj.givenName = 'givenName';
        obj.familyName = 'familyName';
        obj.candidateStatus = 'candidateStatus';
        obj.city = 'city';
        obj.state = 'state';
        obj.country = 'country';
        obj.createdDate = System.now();
        obj.alertId = 'alertId';
        obj.teaser = 'teaser';
        obj.alertTrigger = null;
        obj.alertMatchTimestamp = System.now();
            
        final_resultList.add(obj);
        
        AlertNotificationController.EmailNotificationWrapper emailObj = new AlertNotificationController.EmailNotificationWrapper();
        emailObj.alertName 	= 'name';
        emailObj.userEmail 	= 'user@email.com';
        emailObj.userName 	= 'user_name';
        emailObj.subject 	= 'subject';
        emailObj.totalRecordCount = 1;
        emailObj.final_resultList = final_resultList;
        
        list<AlertNotificationController.EmailNotificationWrapper> wrapObjectList = new list<AlertNotificationController.EmailNotificationWrapper>();
        wrapObjectList.add(emailObj);
        AlertNotificationController.EmailNotificationWrapper.sendEmailMethod( wrapObjectList);
    }
    
    /*public static testmethod void coverExceptions(){ 
            
				User thisUser = [SELECT Id,FirstName, Email FROM User WHERE Id = :UserInfo.getUserId()];

                Test.startTest(); 
                Search_Alert_Criteria__c criteria = new Search_Alert_Criteria__c( Alert_Criteria__c = 'Test', Name='Test', Alert_Frequency__c = 'Daily', Query_String__c = 'xyz' );
                insert criteria;
                criteria.OwnerId	= thisUser.id;
               
			    SearchController.SearchAlertResult alertResult;				
				String jsonResult = '{"exception":[{"candidateId":"0030D000009F3YvQAK","results":[{"alertResult"}],"givenName":"Valarie","familyName":"Peterson","candidateStatus":"Current","city":"Meridian","state":"Mississippi","country":"United States","createdDate":"2019-01-24T15:08:24.000Z","alertId":"a3r0D0000004TzSQAU","teaser":"This is a resume teaser for this alert match","alertTrigger":null,"alertMatchTimestamp":"2019-02-13T15:08:24.000Z"}]}';
                Search_Alert_Result__c result = new Search_Alert_Result__c( Alert_Criteria__c = criteria.Id , Results__c = jsonResult , Date__c = System.today() );
				insert result;
                
                List<Search_Alert_Result__c> lstSearchAlertResult = new List<Search_Alert_Result__c>();
                lstSearchAlertResult.add(result);
                
            	AlertNotificationController a = new AlertNotificationController();
                AlertNotificationController.EmailNotificationWrapper b = new AlertNotificationController.EmailNotificationWrapper();
                AlertNotificationController.EmailNotificationWrapper.sendNotification(lstSearchAlertResult);
            
    }*/
    
}