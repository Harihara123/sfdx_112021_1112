global class SearchAlertResultHistoryBatch implements Database.Batchable<SObject>,Schedulable {
       
    global Database.QueryLocator start(Database.BatchableContext context) {
        
       
        return Database.getQueryLocator('SELECT Id,Date__c  from Search_Alert_Result__c where Date__c < LAST_N_DAYS:'+Label.SearchAlertClenupInterval);
        
    
    }
    global void execute(Database.BatchableContext context, List<Search_Alert_Result__c> scope) {
       
        Delete scope;
    }
     global void finish(Database.BatchableContext BC){
          
    }
    global void execute(SchedulableContext scon) {
      Database.executeBatch(new SearchAlertResultHistoryBatch(),500);
   }
    
}