global class SearchAlertResultsModel  {
	global List<SearchAlertResultModel> results { get; set;}

	global class SearchAlertResultModel {
		global string candidateId { get; set;}
		global string jobTitle { get; set;}
		global string givenName { get; Set;}
		global string familyName { get; Set;}
		global string candidateStatus { get; Set;}
		global string city { get; Set;}
		global string state { get; Set;}
		global string country { get; Set;}
		global Datetime createdDate { get; Set;}
		global string alertId {get; set; }
		global string teaser { get; set; }
		global string[] alertTrigger { get; set; }
		global Datetime alertMatchTimestamp { get; set; }
		global Datetime resumeLastModifiedDate { get; set; }
	}
}