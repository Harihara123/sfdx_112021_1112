public class SearchAndAssociateAccountHelper 
{
    @AuraEnabled
    public static void createStrategicRecords(String strategicObjectId,List<String> selectedAccIdList)
    {
        System.debug('strategicObjectId:'+strategicObjectId);
        System.debug('selectedAccIdList:'+selectedAccIdList);
        if(strategicObjectId != NULL && selectedAccIdList != NULL && !selectedAccIdList.isEmpty())
        {                        
            List<Account_Strategic_Initiative__c> accStrIniVarListToInsert = new List<Account_Strategic_Initiative__c>();
            for(String accId:selectedAccIdList)
            {
                Account_Strategic_Initiative__c accStrIniVar = new Account_Strategic_Initiative__c();
                accStrIniVar.Account__c = accId;
                accStrIniVar.Strategic_Initiative__c = strategicObjectId;
                accStrIniVarListToInsert.add(accStrIniVar);
            }
            if(!accStrIniVarListToInsert.isEmpty())
            {
                insert accStrIniVarListToInsert;
            }
        }
    }
    
    @AuraEnabled
    public static List<SObject> filterRequiredRecords(String featureItem,String searchString,String searchInFields,String returningFields,String targetObject, 
                                                      List<DetailSearchLookupController.ConditionWrapper> condition,String sortString,String strategicObjectId,
                                                      String limitValue)
    {
        System.debug('strategicObjectId:'+strategicObjectId);
        List<SObject> accountListToReturn = new List<SObject>();
        if(strategicObjectId != NULL)
        {  
            List<List<SObject>> retrivedList = DetailSearchLookupController.searchResult(featureItem,searchString,'ALL',returningFields,targetObject,condition,sortString,limitValue);
            if(retrivedList != NULL && !retrivedList.isEmpty())
            {
                List<SObject> accountList = retrivedList[0];                              
                List<Account_Strategic_Initiative__c> accStrIniVarListToInsert = [SELECT Account__c 
                                                                                  FROM Account_Strategic_Initiative__c
                                                                                  WHERE Strategic_Initiative__c =: strategicObjectId];
                Set<String> existingAccSet = new Set<String>();
                for(Account_Strategic_Initiative__c accVar:accStrIniVarListToInsert)
                {
                    existingAccSet.add(accVar.Account__c);
                }
                System.debug('existingAccSet:'+existingAccSet);
                for(SObject objVar:accountList)
                {
                    System.debug('Account Id:'+(String)objVar.get('Id'));
                    if(!existingAccSet.contains((String)objVar.get('Id')))
                    {
                        accountListToReturn.add(objVar);
                    }
                }
            }	
            System.debug('accountListToReturn:'+accountListToReturn.size());
        }
        return accountListToReturn;
    }	
}