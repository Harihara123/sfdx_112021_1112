@isTest
public class SearchAndAssociateAccountHelperTest 
{
    static testMethod void searcResultTest() 
    {
        Test.startTest();
        Id recordId1 = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Account Plan').getRecordTypeId();
        Account acc = TestDataHelper.createAccount();
        insert acc;
        
        Account acc1 = TestDataHelper.createAccount();
        insert acc1;
        
        Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative2',Start_Date__c = system.Today() - 30,
                                                                 recordTypeId = recordId1,
                                                                 Impacted_Region__c = 'test2',
                                                                 Account_Prioritization__c ='A',
                                                                 Primary_Account__c= acc.Id,
                                                                 OpCo__c='Aerotek2');
        
        insert si;
        
        Account_Strategic_Initiative__c accStratInit = new Account_Strategic_Initiative__c();
        accStratInit.Account__c = acc1.Id;
        accStratInit.Strategic_Initiative__c=si.Id;
        insert accStratInit;
        
        DetailSearchLookupController.ConditionWrapper cw = new DetailSearchLookupController.ConditionWrapper();
        cw.fieldName = 'recordtype.name';
        cw.fieldValue = 'Talent';
        List<DetailSearchLookupController.ConditionWrapper> cwList = new List<DetailSearchLookupController.ConditionWrapper>(); 
        cwList.add(cw);
        List<SObject> searchResults = SearchAndAssociateAccountHelper.filterRequiredRecords(NULL,'Test','Name','Name,BillingState,BillingCountry','Account',NULL,'Name desc',si.Id,'100');
        System.debug(searchResults);
        Test.stopTest();
    }
    
    static testMethod void searcResultTest1() 
    {
        Test.startTest();
        Id recordId1 = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Account Plan').getRecordTypeId();
        Account acc = TestDataHelper.createAccount();
        insert acc;
        
        Account acc1 = TestDataHelper.createAccount();
        insert acc1;
        
        Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative2',Start_Date__c = system.Today() - 30,
                                                                 recordTypeId = recordId1,
                                                                 Impacted_Region__c = 'test2',
                                                                 Account_Prioritization__c ='A',
                                                                 Primary_Account__c= acc.Id,
                                                                 OpCo__c='Aerotek2');
        
        insert si;
        
        Account_Strategic_Initiative__c accStratInit = new Account_Strategic_Initiative__c();
        accStratInit.Account__c = acc1.Id;
        accStratInit.Strategic_Initiative__c=si.Id;
        insert accStratInit;
        
        DetailSearchLookupController.ConditionWrapper cw = new DetailSearchLookupController.ConditionWrapper();
        cw.fieldName = 'recordtype.name';
        cw.fieldValue = 'Talent';
        List<DetailSearchLookupController.ConditionWrapper> cwList = new List<DetailSearchLookupController.ConditionWrapper>(); 
        cwList.add(cw);
        List<SObject> searchResults = SearchAndAssociateAccountHelper.filterRequiredRecords(NULL,'Test','Name','Name,BillingState,BillingCountry','Account',cwList,'Name desc',si.Id,'100');
        System.debug(searchResults);
        Test.stopTest();
    }
    
    static testMethod void createStrategicRecordsTest() 
    {
        Test.startTest();
        Id recordId1 = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Account Plan').getRecordTypeId();
        Account acc = TestDataHelper.createAccount();
        insert acc;
        
        Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative2',Start_Date__c = system.Today() - 30,
                                                                 recordTypeId = recordId1,
                                                                 Impacted_Region__c = 'test2',
                                                                 Account_Prioritization__c ='A',
                                                                 Primary_Account__c= acc.Id,
                                                                 OpCo__c='Aerotek2');
        
        insert si;
        List<Account_Strategic_Initiative__c> testList = [SELECT Id FROM Account_Strategic_Initiative__c];
        System.debug('testList:'+testList);
        try
        {
            SearchAndAssociateAccountHelper.createStrategicRecords(si.Id,new List<String>{acc.Id});    
        }
        catch(Exception exc)
        {
            
        }
        Test.stopTest();
    }
}