public class SearchLogFixer  {

	private class SearchLogArray {
        String[] searches {get;set;}
    }


	public static List<User_Search_Log__c> fix(Map<Id, List<User_Search_Log__c>> usrLogMap) {
		List<User_Search_Log__c> outLogList = new List<User_Search_Log__c>();
		Set<Id> users = usrLogMap.keySet();

		for (Id usrId : users) {
			List<String> lastList = new List<String>();
			List<String> saveList = new List<String>();

			for (User_Search_Log__c logEntry : usrLogMap.get(usrId)) {
				SearchLogArray ls = (SearchLogArray) JSON.deserialize(logEntry.Search_Params__c, SearchLogArray.class);
				if (logEntry.Type__c.contains('Last')) {
					lastList.addAll(ls.searches);
				} else {
					saveList.addAll(ls.searches);
				}
			}

			outLogList.add(splitSearchLogParams('AllLastSearches', sortAndTrimSearches(lastList), usrId));
			outLogList.add(splitSearchLogParams('AllSavedSearches', sortAndTrimSearches(saveList), usrId));
		}

		return outLogList;
	}

	private static String sortAndTrimSearches(List<String> searchList) {
		Map<Long, String> timeMap = new Map<Long, String>();
		List<Long> timeList = new List<Long>();

		for (String srch : searchList) {
            try {
			Integer st = srch.indexOf('execTime\":');
			Long t = Long.valueOf(srch.substring(st+10, srch.indexOf(',', st)));

			timeMap.put(t, srch);
			timeList.add(t);
            } catch (StringException ex) {
                System.debug('Ignoring string exception');
            }
		}

		timeList.sort();

		List<String> outList = new List<String>();
		Integer count = 0;
		for (Integer i=timeList.size()-1; count<20 && i>=0; i--) {
			String srch = timeMap.get(timeList[i]);
			if (srch.contains('"type\":\"')) {
				outList.add(srch);
				count++;
			}
		}

		SearchLogArray slArr = new SearchLogArray();
		slArr.searches = outList;
		String sl = JSON.serialize(slArr);
		System.debug(sl);
		return sl;
	}

	private static User_Search_Log__c splitSearchLogParams(String tp, String data, Id usrId) {
		User_Search_Log__c logEntry = new User_Search_Log__c(
			Type__c = tp,
            User__c = usrId
		);

		if (data.length() > 131072) {
			logEntry.Search_Params__c = '"' + data.substring(0, 131070) + '"';
			logEntry.Search_Params_Continued__c = '"' + data.substring(131070) + '"';
		} else {
			logEntry.Search_Params__c = data;
			logEntry.Search_Params_Continued__c = null;
		}

		return logEntry;
	}
}