global class SearchLogFixerBatch  implements Database.Batchable<sObject> {
	String query;

	global SearchLogFixerBatch(String q) {
        if (String.isBlank(q)) {
            query = 'select Id, Name from User where Id in (select User__c from User_Search_Log__c)';
        } else {
            this.query = q;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('Query>>>>>>>' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<User> scope) {
		Map<Id, List<User_Search_Log__c>> logMap = new Map<Id, List<User_Search_Log__c>>();
		List<User_Search_Log__c> saveList = new List<User_Search_Log__c>();

        system.debug ('with in execute >>>>>>>>>>>>>>>' + scope.size());
		for (User_Search_Log__c logEntry : [SELECT Id, User__c, Type__c, Search_Params__c, Search_Params_Continued__c FROM User_Search_Log__c where User__c IN :scope and Type__c like '%Searches']) {
			List<User_Search_Log__c> usrLogList = logMap.get(logEntry.User__c);
			if (usrLogList == null) {
				usrLogList = new List<User_Search_Log__c>();
			}
			usrLogList.add(logEntry);
			logMap.put(logEntry.User__c, usrLogList);
		}

		saveList.addAll(SearchLogFixer.fix(logMap));

		insert saveList;
    }

    global void finish(Database.BatchableContext BC) {
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, CompletedDate FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('HRXML Updates ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }
}