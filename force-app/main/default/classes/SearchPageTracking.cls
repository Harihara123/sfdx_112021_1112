public class SearchPageTracking {
    
   @AuraEnabled
   public static String saveRecord(String userActivityJSON) {

       User_Activity__c eap = new User_Activity__c();
       eap.name = 'session_name';
  
       eap.Session_data_JSON__c = userActivityJSON;

       insert eap;
       return String.valueOf(eap.id);
   }

      @AuraEnabled
    public static UserTrackingSwitch__c getUserTrackingSwith() {

        System.debug('farid UserTrackingSwitch__c '+ UserTrackingSwitch__c.getInstance('Param'));

        return UserTrackingSwitch__c.getInstance('Param');

    }
    
    @AuraEnabled
    public static Boolean checkGroupPermission() {
		return FeatureManagement.checkPermission('GroupA');
    }   
}