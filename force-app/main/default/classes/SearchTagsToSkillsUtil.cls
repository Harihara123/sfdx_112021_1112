/***************************************************************************
Name        : SearchTagsToSkillsUtil
Created By  : Preetham Uppu 
Date        : 12 April 2018
Story       : S-68659
Purpose     : Class that contains all of the functionality called by Batch to Convert Skills
*****************************************************************************/
public class SearchTagsToSkillsUtil{

  Set<Id> entReqIds = new Set<Id>();
  public Map<Id, Opportunity> oppMap;
  List<Log__c> errorLogs = new List<Log__c>();
  public Set<Id> errorIdSet = new Set<Id> ();
  public Map<Id,String> errorLogMap = new Map<Id,String>(); 
  public Map<Id,String> errorLogMap2 = new Map<Id,String>();
  List<String> errorLst = new List<String>();
  public Map<Id,String> insertErrorsMap = new Map<Id,String>();
  public Map<Id,String> exceptionMap = new Map<Id,String>();
 
  public SearchTagsToSkillsUtil(Set<Id> oppIds){
     entReqIds = oppIds;
     oppMap =  new Map<Id, Opportunity>([select Id, Name, Req_Search_Tags_Keywords__c, EnterpriseReqSkills__c from Opportunity where Id IN : oppIds]);
  }
  
  public  ReqSyncErrorLogHelper  searchTagsToSkills(){
  
   List<Opportunity> enterpriseReqs = new List<Opportunity>();
    for(Opportunity opty : oppMap.values()){
          String reqSearchTags = opty.Req_Search_Tags_Keywords__c;
          List<SkillsWrapper> entReqSkills = new List<SkillsWrapper>();
                 
           if(!String.IsBlank(reqSearchTags)){
              if(!string.isBlank(opty.EnterpriseReqSkills__c)){
                entReqSkills = (List<SkillsWrapper>)JSON.deserializeStrict(opty.EnterpriseReqSkills__c, List<SkillsWrapper>.Class);
              }
                 
              if(reqSearchTags.containsAny(',') || reqSearchTags.containsAny(';')){
               
                  List<String> skList = reqSearchTags.split('[,;]');
                    for(String s: skList){
                      SkillsWrapper pill = new SkillsWrapper(); 
                        if(!String.isBlank(s)) {
                            if(s.toLowerCase().equalsIgnoreCase('and') || s.toLowerCase().equalsIgnoreCase('or')){
                               //System.debug('each and or string '+s);
                            }else{
                                  pill.name = s;
                                  pill.favorite = true;
                                  entReqSkills.add(pill);
                            }   
                        } 
                    }
               
               }else{
                
                   String reqProperSkills = reqSearchTags.replaceAll('[^a-zA-Z0-9#.]', ' ');
                   List<String> skList = reqProperSkills.split('[\\s]');
                    for(String s: skList){
                      SkillsWrapper pill = new SkillsWrapper(); 
                        if(!String.isBlank(s)) {
                            if(s.toLowerCase().equalsIgnoreCase('and') || s.toLowerCase().equalsIgnoreCase('or')){
                               //System.debug('each and or string '+s);
                            }else{
                                  pill.name = s;
                                  pill.favorite = true;
                                  entReqSkills.add(pill);
                            }   
                        } 
                    }
               
                  }
                  
             }
                 
               if(entReqSkills.size() > 0){
                 opty.EnterpriseReqSkills__c = JSON.serialize(entReqSkills);
               }
      enterpriseReqs.add(opty);
    }
    
    if(enterpriseReqs.size() > 0){
        Integer numOfUpserts = 0;          
        Integer i = 1 ; 
        Integer totalRecords = 0;
        
         List<database.SaveResult> oppUpdateResults =  database.update(enterpriseReqs,false);
            for(Integer y=0; y < enterpriseReqs.size();y++){
                String errorRecord = 'Failed Skills Migration';
                String exceptionMsg = '';
                if(!oppUpdateResults[y].isSuccess()){
                  errorLogMap2.put(enterpriseReqs[y].Id, oppUpdateResults[y].getErrors()[0].getStatusCode() != null && oppUpdateResults[y].getErrors()[0].getFields()!= null ? String.ValueOf(oppUpdateResults[y].getErrors()[0].getStatusCode()) +' '+ ' Error Fields ' +  oppUpdateResults[y].getErrors()[0].getFields(): ' Either Status Code or Fields are Null ');
                }
             }
             
              if(errorLogMap2.size() > 0)
                 Core_Data.logBatchRecord('SearchTags-Skills',errorLogMap2);
    }
    
     if(errorLogMap2.size() > 0){
        exceptionMap.putall(errorLogMap2);
      }
      
      ReqSyncErrorLogHelper errorWrapper = new ReqSyncErrorLogHelper(exceptionMap,errorLst);
      return errorWrapper;
  }
 
 }