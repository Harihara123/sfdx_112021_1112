@isTest
public class SearchTagsToSkillsUtilTest {
	
	@isTest
	private static void testContstructor() {
		Opportunity clientOpportunity = dataSetup('Developer', 'Java');

		Test.startTest();
		SearchTagsToSkillsUtil results = new SearchTagsToSKillsUtil(new Set<Id>{clientOpportunity.Id});
		Test.stopTest();
		System.assert(results.oppMap.size() > 0, 'there should be an opp');
	}

	@isTest
	private static void testSearchTagsToSKillsUtil() {
		Opportunity clientOpportunity = dataSetup('Developer', '[{"name":"Java","favorite":true}]');

		Test.startTest();
		SearchTagsToSKillsUtil results = new SearchTagsToSKillsUtil(new Set<Id>{clientOpportunity.Id});
		ReqSyncErrorLogHelper log = results.searchTagsToSKills();
		Test.stopTest();
		System.debug('log: ' + log);
		System.assert(log != null, 'a log should always appear');
	}

	@isTest
	private static void testSearchTagsToSKillsUtil_List() {
		Opportunity clientOpportunity = dataSetup('Developer, BA', '[{"name":"Java","favorite":true},{"name":"Ruby","favorite":false}]');

		Test.startTest();
		SearchTagsToSKillsUtil results = new SearchTagsToSKillsUtil(new Set<Id>{clientOpportunity.Id});
		ReqSyncErrorLogHelper log = results.searchTagsToSKills();
		Test.stopTest();
		System.debug('log2: ' + log);
		System.assert(log != null, 'a log should always appear');
	}

	private static Opportunity dataSetup(String keyWords, String reqSkills) {
		//create clientaccounts and opportunities
		Account clientAccount = TestData.newAccount(1);
		insert clientAccount;
		Opportunity clientOpportunity = TestData.newOpportunity(clientAccount.Id, 1);
		clientOpportunity.Req_Search_Tags_Keywords__c = keyWords;
		clientOpportunity.EnterpriseReqSkills__c = reqSkills;
		insert clientOpportunity;
		return clientOpportunity;
	}
}