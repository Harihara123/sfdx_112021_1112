/*
  @author : Neel Kamal
  @date : 05/07/2020
  @Description : Business wanted to see report on Search Save Utilization. Means which user save save how many search criteria.
  Fullfil the requirement, get data from User_Search_Log__c parse the JSON and get the calcualtion of number of save record in JSON.
*/
public class SearchUtilizationCalculatorBatch implements Database.Batchable<sObject>, Schedulable {
	public Database.QueryLocator start(Database.BatchableContext bc) {
		//and LastModifiedDate =TODAY
		Integer days = Integer.valueOf(Label.Search_Utilization_Days);
		String soql = 'Select id, User__c, User__r.name, User__r.Office__c, User__r.OPCO__c, User__r.Division__c, Search_Params__c, Search_Params_Continued__c,Search_Params_Continued_ESR2C__c, Search_Params_ESR2C__c from User_Search_Log__c where type__c = \'AllSavedSearches\' and LastModifiedDate = LAST_N_DAYS:' + days;
		return Database.getQueryLocator(soql);
	}
	public void execute(Database.BatchableContext bc, List<User_Search_Log__c> srcLogList) {
		System.debug(srcLogList);
		Search_Utilization__c srcUtilizn;
		List<Search_Utilization__c> srcUtilizationList = new List<Search_Utilization__c> ();
		try {
			for (User_Search_Log__c srcLog : srcLogList) {
				String srcCriteria = getSearchLogParams(srcLog);
				
				SearchUtilizationWrapper srcUtilizWrapper = searchUtilizationInfo(srcCriteria);
				srcUtilizn = new Search_Utilization__c();
				srcUtilizn.recordTypeid= Utility.getRecordTypeId('Search_Utilization__c','Search Criteria');
				srcUtilizn.User__c = srcLog.User__c;
				srcUtilizn.Office__c = srcLog.User__r.Office__c;
				srcUtilizn.Opco__c = srcLog.User__r.OPCO__c;
				srcUtilizn.Division__c = srcLog.User__r.Division__c;
				srcUtilizn.Created_Date__c = System.today();

				srcUtilizn.Type_C__c = srcUtilizWrapper.type_C;
				srcUtilizn.Type_R__c = srcUtilizWrapper.type_R;
				srcUtilizn.Type_C2C__c = srcUtilizWrapper.type_C2C;
				srcUtilizn.Type_C2R__c = srcUtilizWrapper.type_C2R;
				srcUtilizn.Type_R2C__c = srcUtilizWrapper.type_R2C;
				srcUtilizn.Total__c = srcUtilizWrapper.total;

				srcUtilizationList.add(srcUtilizn);
			}
			insert srcUtilizationList;
		} Catch(DmlException dExp) {
			ConnectedLog.LogException('SearchUtilizationCalculatorBatch', 'execute', dExp);
		} Catch(Exception exp) {
			ConnectedLog.LogException('SearchUtilizationCalculatorBatch', 'execute', exp);
		}

	}
	public void finish(Database.BatchableContext bc) { }

	//Parse JSON and get number of record and also type(C, R, C2C, R2C, C2R) of search.
	private SearchUtilizationWrapper searchUtilizationInfo(String jsonStr) {
		SearchUtilizationWrapper srcWrapper = new SearchUtilizationWrapper();
		String searchType;
		if (String.isNotBlank(jsonStr)) {
			try {
				Map<String, List<String>> desrzStrs = (Map<String, List<String>>) JSON.deserializeStrict(jsonStr, Map<String, List<String>>.class);
				for (String searchStr : desrzStrs.get('searches')) {
					searchType = searchStr.substring(searchStr.indexOf('"type":') + 8, searchStr.indexOf('"type"') + 12);
					searchType = searchType.substringBefore('"');
					switch on searchType {
						when 'C' {
							srcWrapper.type_C += 1;
							srcWrapper.total += 1;
						}
						when 'R' {
							srcWrapper.type_R += 1;
							srcWrapper.total += 1;
						}
						when 'C2C' {
							srcWrapper.type_C2C += 1;
							srcWrapper.total += 1;
						}
						when 'C2R' {
							srcWrapper.type_C2R += 1;
							srcWrapper.total += 1;
						}
						when 'R2C' {
							srcWrapper.type_R2C += 1;
							srcWrapper.total += 1;
						}
					}
				}

			} catch(JSONException jExp) {
				System.debug(jExp);
				ConnectedLog.LogException('SearchUtilizationCalculatorBatch', 'searchUtilizationInfo', jExp);				
			}
		}
		
		return srcWrapper;
	}

	class SearchUtilizationWrapper {
		public Integer total = 0;
		public Integer type_C = 0;
		public Integer type_R = 0;
		public Integer type_C2C = 0;
		public Integer type_C2R = 0;
		public Integer type_R2C = 0;
	}

	private static String getSearchLogParams(User_Search_Log__c logEntry) {
        if (logEntry.Search_Params_Continued__c == null) {
            return logEntry.Search_Params__c;
        } else {
            Integer len = logEntry.Search_Params_Continued__c.length();
            return logEntry.Search_Params__c.substring(1, 131071) + logEntry.Search_Params_Continued__c.substring(1, len-1);
        }
    }

	public void execute(SchedulableContext sc) {
		SearchUtilizationCalculatorBatch batch = new SearchUtilizationCalculatorBatch();
		Database.executeBatch(batch);
	}

                       
}