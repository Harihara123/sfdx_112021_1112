@isTest
public class SearchUtilizationCalculatorBatch_Test  {
	
	@istest 
	static void executeBatchTest() {
		Test.startTest();
			List<User> usrs = TestDataHelper.createUsers(3, 'System Administrator');
		
			List<User_Search_Log__c> uSearchLogList = new List<User_Search_Log__c>();
			User_Search_Log__c searchLog = new User_Search_Log__c();
			searchLog.User__c = usrs.get(0).Id;
			searchLog.type__c = 'AllSavedSearches';
			searchLog.Search_Params__c = '{"searches":["{\\"execTime\\":1588972651690,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"C\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null,\\"facetFlt.qualifications_last_activity_date\\":null},\\"title\\":\\"Ajay_Oracle3\\"}","{\\"execTime\\":1588972640259,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"R\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null,\\"facetFlt.qualifications_last_activity_date\\":null},\\"title\\":\\"Ajay_Oracle1\\"}","{\\"execTime\\":1588972617256,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"R2C\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null},\\"title\\":\\"Ajay_oracle\\"}"]}';
			uSearchLogList.add(searchLog);

			searchLog = new User_Search_Log__c();
			searchLog.User__c = usrs.get(1).Id;
			searchLog.type__c = 'AllSavedSearches';
			searchLog.Search_Params__c = '{"searches":["{\\"execTime\\":1588972651690,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"C2C\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null,\\"facetFlt.qualifications_last_activity_date\\":null},\\"title\\":\\"Ajay_Oracle3\\"}","{\\"execTime\\":1588972640259,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"C2R\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null,\\"facetFlt.qualifications_last_activity_date\\":null},\\"title\\":\\"Ajay_Oracle1\\"}","{\\"execTime\\":1588972617256,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"R2C\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null},\\"title\\":\\"Ajay_oracle\\"}"]}';
			uSearchLogList.add(searchLog);

			searchLog = new User_Search_Log__c();
			searchLog.User__c = usrs.get(2).Id;
			searchLog.type__c = 'AllSavedSearches';
			searchLog.Search_Params__c = '{"searches":["{\\"execTime\\":1588972651690,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"C\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null,\\"facetFlt.qualifications_last_activity_date\\":null},\\"title\\":\\"Ajay_Oracle3\\"}","{\\"execTime\\":1588972640259,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"R\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null,\\"facetFlt.qualifications_last_activity_date\\":null},\\"title\\":\\"Ajay_Oracle1\\"}","{\\"execTime\\":1588972617256,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"R2C\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Oracle\\",\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate|former|current\\",\\"facetFlt.placement_type\\":\\"contract|any\\",\\"facetFlt.languages_spoken\\":\\"english|spanish|french|hindi\\",\\"facetFlt.shift_preferences\\":\\"First|Second|Weekend\\"},\\"filterCriteriaParams\\":{}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.placement_type\\":null,\\"facetFlt.languages_spoken\\":null,\\"facetFlt.shift_preferences\\":null},\\"title\\":\\"Ajay_oracle\\"}"]}';
			uSearchLogList.add(searchLog);

			insert uSearchLogList;

			SearchUtilizationCalculatorBatch batch = new SearchUtilizationCalculatorBatch();
			Database.executeBatch(batch);
		Test.stopTest();

		List<Search_Utilization__c> sUtilzn = [Select id from Search_Utilization__c ];
		System.debug('size----------'+sUtilzn.size());
		//System.assertEquals(3, sUtilzn.size()); //it is not working in parallel execution

	}

	@istest 
	static void executeBatchJSONExceptionTest() {
		Test.startTest();
			User usr = TestDataHelper.createUser('System Administrator');
			usr.Office__c = 'test office';

			Insert usr;

			User_Search_Log__c searchLog = new User_Search_Log__c();
			searchLog.User__c = usr.Id;
			searchLog.type__c = 'AllSavedSearches';
			searchLog.Search_Params__c = '{"searches":["{\\"execTime\\":1588712236836,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"C\\",\\"tzpe\\":\\"test\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Salesforce\\",\\"multilocation\\":[{\\"displayText\\":\\"Ellicott City, MD, USA\\",\\"latlon\\":[39.2673283,-76.7983067],\\"geoRadius\\":\\"30\\",\\"geoUnitOfMeasure\\":\\"mi\\",\\"useRadius\\":\\"SELECT\\",\\"alwaysUseRadius\\":false,\\"city\\":\\"Ellicott City\\",\\"state\\":\\"Maryland\\",\\"country\\":\\"United States\\"}],\\"facets\\":{\\"facetFlt.candidate_status\\":\\"candidate\\",\\"nested_filter.submitHistory\\":\\"[{\\\\\\"Date\\\\\\" : [\\\\\\"2020-03-02\\\\\\",\\\\\\"2020-05-05\\\\\\"]}]\\",\\"facetFlt.employment_position_title\\":\\"test manager|automation engineer|salesforce developer|unknown|business analyst ii/business analyst i|salesforce analyst|business analyst/coordinator|qa ninja|salesforce consultant|business analyst i|java developer|qa test analyst|salesforce admin|salesforce administrator|salesforce developer and administrator\\",\\"facetFlt.skills\\":\\"salesforce crm|apex|java|javascript|salesforce|visualforce|data loader|eclipse ide|lightning|qa analysis|qa software|salesforce software|salesforce.com|support|web service\\",\\"nested_facet_filter.myCandidateListsFacet,nested_filter.allCandidateListsFacet\\":\\"~ANY\\",\\"facetFlt.interaction\\":\\"g2|ioi|referencecheck|meeting\\",\\"facetFlt.qualifications_last_activity_date\\":\\"01/1/15-05/5/20\\",\\"flt.min_desired_payrate,flt.max_desired_payrate\\":\\"[;99999]~[0;]\\",\\"flt.total_desired_compensation\\":\\"[0;9999]\\",\\"facetFlt.languages_spoken\\":\\"english|spanish\\",\\"flt.qualifications_last_resume_modified_date\\":\\"[2020-03-02;2020-05-05T23:59:59.999Z]\\",\\"facetFlt.sov_months_experience\\":\\"missing|0|[1,12>|[12,24>|[24,60>|[60,120>|[120,>\\",\\"facetFlt.rollup\\":\\"High\\",\\"facetFlt.shift_preferences\\":\\"Second\\"},\\"filterCriteriaParams\\":{\\"facetFlt.employment_position_title\\":{\\"facetOption.employment_position_title.size\\":15,\\"facetOption.employment_position_title.contains\\":\\"\\",\\"facetOption.employment_position_title.selected\\":\\"test manager|automation engineer|salesforce developer|unknown|business analyst ii/business analyst i|salesforce analyst|business analyst/coordinator|qa ninja|salesforce consultant|business analyst i|java developer|qa test analyst|salesforce admin|salesforce administrator|salesforce developer and administrator\\"},\\"facetFlt.skills\\":{\\"facetOption.skills.size\\":15,\\"facetOption.skills.contains\\":\\"\\",\\"facetOption.skills.selected\\":\\"salesforce crm|apex|java|javascript|salesforce|visualforce|data loader|eclipse ide|lightning|qa analysis|qa software|salesforce software|salesforce.com|support|web service\\"}}},\\"fdp\\":{\\"facetFlt.candidate_status\\":null,\\"facetFlt.employment_position_title\\":null,\\"facetFlt.skills\\":null,\\"facetFlt.interaction\\":null,\\"facetFlt.qualifications_last_activity_date\\":null,\\"flt.min_desired_payrate,flt.max_desired_payrate\\":null,\\"flt.total_desired_compensation\\":null,\\"facetFlt.languages_spoken\\":null,\\"flt.qualifications_last_resume_modified_date\\":null,\\"facetFlt.sov_months_experience\\":null,\\"facetFlt.rollup\\":null,\\"facetFlt.shift_preferences\\":null},\\"title\\":\\"Neel_sf\\"}","{\\"execTime\\":1588712047381,\\"offset\\":0,\\"pageSize\\":30,\\"type\\":\\"R\\",\\"engine\\":\\"ES\\",\\"criteria\\":{\\"keyword\\":\\"Salesforce\\",\\"multilocation\\":[{\\"displayText\\":\\"Ellicott City, MD, USA\\",\\"latlon\\":[39.2673283,-76.7983067],\\"geoRadius\\":\\"30\\",\\"geoUnitOfMeasure\\":\\"mi\\",\\"useRadius\\":\\"SELECT\\",\\"alwaysUseRadius\\":false,\\"city\\":\\"Ellicott City\\",\\"state\\":\\"Maryland\\",\\"country\\":\\"United States\\",\\"facets\\":null,\\"filterCriteriaParams\\":null},\\"fdp\\":null,\\"title\\":\\"Neel_SF1\\"';

			insert searchLog;

		
			SearchUtilizationCalculatorBatch batch = new SearchUtilizationCalculatorBatch();
			Database.executeBatch(batch);
		Test.stopTest();

		List<Search_Utilization__c> sUtilzn = [Select id from Search_Utilization__c where User__c =: usr.id];
		//System.assertEquals(0, sUtilzn.size());
	}
}