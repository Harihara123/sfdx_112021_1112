public with sharing class SelfReportedInfo {
    @AuraEnabled(cacheable=false)
    public static string checkContactSelfReportedInfo(String recordId){
        try {
			 Contact cont = [Select Id, Account.TC_Talent_Profile_Last_Modified_Date__c, 
										TC_Title__c, 
										Account.Talent_Preference__c,
										Account.Skills__c,
										Full_Name__c,
										Email,
										TC_Mailing_Address__c,
										MailingStreet,
										MailingCity,
										MailingState,
										MailingLongitude,
										MailingLatitude,
										MailingCountry,
										Phone, 
										HomePhone, 
										OtherPhone, 
										MobilePhone From Contact WHERE Id =: recordId];
				User ut = [Select Id, Title from User Where ContactId =: recordId LIMIT 1];
								
				String TP = String.valueOf(cont.Account.Talent_Preference__c);
								
				List<GenerateJSON.Phone> phoneList = new List<GenerateJSON.Phone>();								
				GenerateJSON.Address addressDetails = new GenerateJSON.Address();
				GenerateJSON.JobTitle jobTitleDetails = new GenerateJSON.JobTitle();
				List<GenerateJSON.Skills> skilList = new List<GenerateJSON.Skills>();
				GenerateJSON.LastUpdate lastUpdateDetails = new GenerateJSON.LastUpdate();
				GenerateJSON.Source sourceDetails = new GenerateJSON.Source();
				GenerateJSON jsonObj = new GenerateJSON();
				jsonObj.ContactId = cont.Id;

				GenerateJSON.Profile profileObj = new GenerateJSON.Profile();

				lastUpdateDetails.UpdateDate = cont.Account.TC_Talent_Profile_Last_Modified_Date__c;
				lastUpdateDetails.Source = 'Community';
				profileObj.LastUpdate = lastUpdateDetails;
				profileObj.Name=cont.Full_Name__c;
				profileObj.Email = cont.Email;

				GenerateJSON.Phone homePhoneObj = new GenerateJSON.Phone();
				homePhoneObj.ContactNumber = cont.HomePhone;
				homePhoneObj.Type = 'Home';
				phoneList.add(homePhoneObj);

				GenerateJSON.Phone mobilePhoneObj = new GenerateJSON.Phone();
				mobilePhoneObj.ContactNumber = cont.MobilePhone;
				mobilePhoneObj.Type = 'Mobile';
				phoneList.add(mobilePhoneObj);

				GenerateJSON.Phone phoneObj = new GenerateJSON.Phone();
				phoneObj.ContactNumber = cont.Phone;
				phoneObj.Type = 'Phone';
				phoneList.add(phoneObj);

				GenerateJSON.Phone otherPhoneObj = new GenerateJSON.Phone();
				otherPhoneObj.ContactNumber = cont.OtherPhone;
				otherPhoneObj.Type = 'Other';
				phoneList.add(otherPhoneObj);

				profileObj.Phone = phoneList;

				addressDetails.Street = cont.MailingStreet;
				addressDetails.City = cont.MailingCity;
				addressDetails.State = cont.MailingState;
				addressDetails.Country = cont.MailingCountry;
				addressDetails.Longitude = cont.MailingLongitude;
				addressDetails.Latitude = cont.MailingLatitude;
				profileObj.Address = addressDetails;

				sourceDetails.User = cont.Full_Name__c;
				sourceDetails.Value = 'Community';
				jobTitleDetails.Source = sourceDetails;

				jobTitleDetails.Value = ut.Title;
				jobTitleDetails.ID = cont.Id;
				jobTitleDetails.Status = 'NEW';
				jobTitleDetails.UpdateDate = cont.Account.TC_Talent_Profile_Last_Modified_Date__c;
				profileObj.JobTitle = jobTitleDetails;

				
				JSONParser parser = JSON.createParser(cont.Account.Talent_Preference__c);
				List<String> skList = New List<String>();
				while (parser.nextToken() != null) {
					if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'skills') ){						
						System.debug('parser.getText()--------------'+parser.getText());
						System.debug('parser.nextValue()--------------'+parser.nextValue());
						string sk = parser.getText();
						System.debug('parser.nextToken()--------------'+parser.nextToken());
						System.debug('parser.getText()--------------'+parser.getText());
						System.debug('parser.nextValue()--------------'+parser.nextToken());
						System.debug('parser.getText()--------------'+parser.getText());
						skList.add(parser.getText());
					}
				}				
				
				for(String sk:skList){
					GenerateJSON.Skills skilDetails = new GenerateJSON.Skills();					
					skilDetails.Skills = sk;
					skilDetails.ID = cont.Id;
					skilDetails.Status = 'NEW';
					skilDetails.UpdateDate = cont.Account.TC_Talent_Profile_Last_Modified_Date__c;
					skilDetails.Source = sourceDetails;
					skilList.add(skilDetails);
				}				
				profileObj.Skill = skilList;
				jsonObj.Profile=profileObj;
				String jsonstr = JSON.serialize(jsonObj,true);
			   
				if (jsonstr !='') {
                 	return jsonstr;
					
				} else {
                    return '';
                }
            
        } catch(Exception e) {
            system.debug('Error message: ' +e );
            system.debug(e.getStackTraceString());
            return NULL;
            //return JSON.serialize(e);
        }
    }	
}