/*
 * Name : SelfReportedInfoTest
 * Description: Test class to test the SelfReportedInfo. This class will cover the SelfReportedInfoTest class and all other classes 
 *              that are related to this service.
 */
@isTest
public class SelfReportedInfoTest  {
	static testmethod void test_SelfReport(){ 
		Account a = new Account();
        a.name = 'Talent Candidate';
		a.TC_Talent_Profile_Last_Modified_Date__c = System.today();
		a.Talent_Preference__c =   '{"skills":{"data":"swing,core java,loader9i"},"certifications":{"data":""},"industries":{"data":""},"goals":{"data":""},"interests":{"data":""},"linkedin":{}}'; 
        insert a;

        Contact c = new Contact();
        c.accountId = a.Id;
        c.firstname = 'John';
        c.lastname = 'John';
        c.Peoplesoft_ID__c = '099999';        
        insert c;
		 user usr = new User();
		Profile aerotek = [SELECT Id FROM Profile WHERE Name='Aerotek Community User' Limit 1]; 
         usr = new User(Alias = 'aerocom', Email='aerotekCommUsr@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = aerotek.Id, TimeZoneSidKey='America/Los_Angeles', 
            UserName='aerotekCommUsr@testorg.com',contactId =c.id,Opco__c='ONS',UserGUID__c = 'SS12345');
        insert usr;
		
		SelfReportedInfo.checkContactSelfReportedInfo(c.Id);
	}
}