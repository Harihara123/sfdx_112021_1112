global class SendEmailtoCaseOwner implements Database.batchable<sObject>, Database.Stateful{ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query = 'select Id, createddate,contact.Account.Talent_Account_Manager__r.Id,contact.Account.Talent_Community_Manager__r.Id,CaseNumber,Status,Contact.Phone,contact.Account.Talent_Id__c, Contact.Email,createdbyid,contact.Account.Talent_End_Date__c,Owner.name,createdby.Profile.Name,Type,Contact.Name,contact.Account.Talent_Account_Manager__r.Name,contact.Account.Talent_Account_Manager__r.Email,contact.Account.Talent_Community_Manager__r.Name,contact.Account.Talent_Community_Manager__r.Email,OwnerId,Owner.Email,AccountId,Reason,CaseAgeByBusinesHours__c from case where status != \'Closed\' AND RecordType.DeveloperName =\'Talent\'';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext info, List<case> scope){
        List<case> caseToUpdate = new List<case>();
        List<String>caseIds = new List<String>();
        List<Talent_Saved_Job__c>tsjReclst = new List<Talent_Saved_Job__c>();
        Map<Id,List<Talent_Saved_Job__c>>caseIdTSJlstMap = new Map<Id,List<Talent_Saved_Job__c>>();
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        for(case a : scope){
            if(a.Type == 'Job Inquiry'){
                caseIds.add(a.Id);
            }
        }
        if(!caseIds.isEmpty()){
            tsjReclst = [select id,CaseId__c,name,OpportunityId__c,OpportunityId__r.Req_Job_Title__c,OpportunityId__r.Req_Worksite_City__c,OpportunityId__r.Req_Worksite_State__c,OpportunityId__r.Opportunity_Num__c,Search_Criteria__c,Location_Criteria__c,Distance_Criteria__c,Description__c,Job_Search_Source__c from Talent_Saved_Job__c where CaseId__c IN :caseIds];            
        }
        if(!tsjReclst.isEmpty()){
            for(Talent_Saved_Job__c tsj : tsjReclst){
                if(caseIdTSJlstMap.containsKey(tsj.CaseId__c)){
                    caseIdTSJlstMap.get(tsj.CaseId__c).add(tsj);
                }else{
                    caseIdTSJlstMap.put(tsj.CaseId__c,new List<Talent_Saved_Job__c>());
                    caseIdTSJlstMap.get(tsj.CaseId__c).add(tsj);
                }
            }
        }
        for(case a : scope){
            String ownerId = String.valueof(a.ownerId); 
            if(a.createdby.Profile.Name.contains('EMEA') && a.CaseAgeByBusinesHours__c == true && ownerId.startsWithIgnoreCase('005') && a.Type != 'IR35 Question' &&  a.Type != 'Ask A Question'){
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                
                String caseIdtemp= URL.getSalesforceBaseUrl().toExternalForm()+'/'+a.id;
                String caseId = '<a href='+caseIdtemp+'>HERE</a>';
                String Recordlinktemp = 'https://allegiscloud.sharepoint.com/portals/hub/_layouts/15/PointPublishing.aspx?app=video&p=p&chid=9c9cfd90-8a6d-4ccd-ac9a-0c0325b7d538&vid=cd33abdd-830f-4d97-98b5-56752cef7696';
                String Recordlink = '<a href='+Recordlinktemp+'>HERE</a>';
                String communityManagerName;
                
                if(a.contact.Account.Talent_Community_Manager__c != null && a.contact.Account.Talent_Community_Manager__r.Name!= null){
                    communityManagerName = a.contact.Account.Talent_Community_Manager__r.Name;
                }else{
                    communityManagerName = 'Community Manager';
                }
                String communityName;
                if(a.createdby.Profile.Name.contains('TEK')){
                    communityName  = 'The TEKsystems Community Team';   
                }else{
                    communityName = 'The Aston carter Community Team';
                }
                
                email.subject = 'Outstanding Talent Communities Case with '+ a.Contact.Name +'-'+ a.Type;
               if(a.Type == 'Address Change' || a.Type=='Name Change'){
                email.setHtmlBody ('Hi ' + a.Owner.Name+',' +'<br/><br/>' +
                                   'You have now have a Talent Communities case opened by '
                                   + a.Contact.Name+ ', which is now over 48 hours old. We have committed to get back to our contractors within 24-48 hours so please review the '
                                   +'case below and follow the steps below as soon as possible.'+ '<br/><br/>' +                                   
                                   +'Is this your first Communities case or are you not sure what to do? Please speak to '  
                                   +communityManagerName +' (Talent Community Manager) or your supervisor to discuss next steps. '
                                   +'Alternatively please click the link '+Recordlink+' for a short video on how to close the case.' +'<br/><br/><br/>' +
                                   + '<b>Once you have reached out to the Contractor and resolved their request please click the link</b> ' +caseId +' <b>Case to access the case and close the case on the system.</b>'+'<br/><br/>'+
                                   + 'Thank you for your continued support,'+'<br/>'+
                                   + communityName ) ;
                }else{
                    email.setHtmlBody ('Hi ' + a.Owner.Name+',' +'<br/><br/>' +
                                   'You have now have a Talent Communities case opened by '
                                   + a.Contact.Name+ ', which is now over 48 hours old. We have committed to get back to our contractors within 24-48 hours so please review the '
                                   +'case below and follow the steps below as soon as possible.'+ '<br/><br/>'
                                   + '<b>If you haven’t yet spoken with your contractor please reach out to them to understand the nature of their Job Inquiry/End Date Review and what support they require.</b>' + '<br/><br/>'                                                                     
                                   + '<b>Once you have spoken to the contractor and acknowledged their request, Please Click </b>' +caseId +' <b>to access the case and close it on the system.</b>'+'<br/><br/>'
                                   +'Case details can be found below. Should you need support on closing the case on connected, Please speak to '  
                                   + communityManagerName  +' (Talent Community Manager) or Click '+Recordlink+' for a video walkthrough.<br/><br/>'
                                   + 'Please remember a case needs to be closed when you first speak to the contractor NOT on completion of the request.'+'<br/><br/>'+
                                   + 'Thank you for your continued support,'+'<br/>'+
                                   + communityName) ;
                }
                if(a.Owner.Email != null){
                    email.setTargetObjectId(a.OwnerId);
                    //email.setToAddresses(new String[] {a.Owner.Email});
                }
                if(a.contact.Account.Talent_Community_Manager__c != null && a.contact.Account.Talent_Community_Manager__r.Email != null){
                    //email.setCcAddresses(new String[] {a.contact.Account.Talent_Community_Manager__r.Email});
                    email.setTargetObjectId(a.contact.Account.Talent_Community_Manager__r.Id);
                }
                email.setSaveAsActivity(false);
                mails.add(email);
                
            }else if((a.createdby.Profile.Name.contains('Aerotek') || a.createdby.Profile.Name.contains('AstonCarter') || a.createdby.Profile.Name.contains('EASi')) && ownerId.startsWithIgnoreCase('005') && a.createdby.Profile.Name.contains('EMEA') && a.CaseAgeByBusinesHours__c == true && (a.Type == 'Job Inquiry' ||  a.Type == 'End Date Review')){
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                
                String caseIdtemp= URL.getSalesforceBaseUrl().toExternalForm()+'/'+a.id;
                String caseId = '<a href='+caseIdtemp+'>HERE</a>';
                String Recordlinktemp = 'https://allegiscloud.sharepoint.com/portals/hub/_layouts/15/PointPublishing.aspx?app=video&p=p&chid=9c9cfd90-8a6d-4ccd-ac9a-0c0325b7d538&vid=cd33abdd-830f-4d97-98b5-56752cef7696';
                String Recordlink = '<a href='+Recordlinktemp+'>HERE</a>';
                String communityManagerName;
                if(a.contact.Account.Talent_Community_Manager__c != null && a.contact.Account.Talent_Community_Manager__r.Name!= null){
                    communityManagerName = a.contact.Account.Talent_Community_Manager__r.Name;
                }else{
                    communityManagerName = 'Community Manager';
                }
                String communityName;
                if(a.createdby.Profile.Name.contains('Aerotek')){
                    communityName  = 'The Aerotek Community Team';   
                }else if(a.createdby.Profile.Name.contains('EASi')){  
                    communityName = 'The ActTalent Community Team';
                }else{
                    communityName = 'The Aston Carter Community Team';
                }
                
                email.subject = 'Open Communities Case with '+ a.Contact.Name +'-'+ a.Type;
               if(a.Type == 'Job Inquiry'){
                    List<Talent_Saved_Job__c>tsjRec = new List<Talent_Saved_Job__c>();
                    tsjRec = caseIdTSJlstMap.get(a.Id);
                    String location;
                    String withIn;
                    String jobSource;
                    String selectedJobTitles;
                    
                    if(!tsjRec.isEmpty()){
                        location = tsjRec[0].OpportunityId__r.Req_Worksite_City__c + ',' + tsjRec[0].OpportunityId__r.Req_Worksite_State__c;
                        withIn = tsjRec[0].Distance_Criteria__c;
                        jobSource = tsjRec[0].Job_Search_Source__c;
                        for(Integer i=0;i<tsjRec.size();i++){
                            if(!String.isBlank(selectedJobTitles)){        
                                selectedJobTitles = selectedJobTitles + tsjRec[i].OpportunityId__r.Req_Job_Title__c + '('+tsjRec[i].OpportunityId__r.Opportunity_Num__c+');'; 
                            }else{
                                selectedJobTitles = tsjRec[i].OpportunityId__r.Req_Job_Title__c + '('+tsjRec[i].OpportunityId__r.Opportunity_Num__c+');';
                            }
                        }
                    }
                                        
                    email.setHtmlBody ('Hi ' + a.Owner.Name+',' +'<br/><br/>' +
                                   'You have a Communities case opened by '
                                   + a.Contact.Name+ ', which is now over 48 hours old. We have committed to respond to cases within 24-48 hours. '+ '<br/><br/>'
                                   + '<b>If you haven’t yet spoken with your contractor please reach out to them to resolve the case.</b>' + '<br/><br/>'                                                                     
                                   + '<b>Once you have spoken to the contractor and acknowledged their request, Please Click </b>' +caseId +' <b>to access the case and close it on the Connected.</b>'+'<br/><br/>'
                                   +'Case details can be found below. Should you need support on closing the case, please speak to '  
                                   + '<a href="'+'mailto:'+a.Contact.Account.Talent_Community_Manager__r.Email +'"> '+communityManagerName+'</a>'  +' or click '+Recordlink+' for a video walkthrough.<br/><br/>'
                                   + 'Please remember a case can be closed when you first speak to the contractor, NOT on completion of the request.'+'<br/><br/>'+
                                   
                                   + '<u>Search Criteria:</u>'+'<br/>'+
                                   + 'Location: '+ location +'<br/>'+
                                   + 'Within: '+ withIn +'<br/>'+
                                   + 'Job Source: '+ jobSource +'<br/>'+
                                   + 'Selected Client Job Titles: '+ selectedJobTitles +'<br/><br/><br/>'+
                                   
                                   + '<u>Additional Information:</u>'+'<br/>'+
                                   + 'Case Number: '+ a.CaseNumber+'<br/>'+
                                   + 'Status: '+ a.Status +'<br/>'+
                                   + 'Requested by: '+a.Contact.Name+'<br/>'+
                                   + 'Contact Email: '+a.Contact.Email+'<br/>'+
                                   + 'Contact Phone: '+a.Contact.Phone+'<br/>'+
                                   + 'Candidate ID: '+ a.contact.Account.Talent_Id__c+'<br/><br/><br/>'+
                                   + 'Thank you for your continued support,'+'<br/>'+
                                   + communityName) ;
                }else{
                    email.setHtmlBody ('Hi ' + a.Owner.Name+',' +'<br/><br/>' +
                                   'You have a Communities case opened by '
                                   + a.Contact.Name+ ', which is now over 48 hours old. We have committed to respond to cases within 24-48 hours. '+ '<br/><br/>'
                                   + '<b>If you haven’t yet spoken with your contractor please reach out to them to resolve the case.</b>' + '<br/><br/>'                                                                     
                                   + '<b>Once you have spoken to the contractor and acknowledged their request, Please Click </b>' +caseId +' <b>to access the case and close it on the Connected.</b>'+'<br/><br/>'
                                   +'Case details can be found below. Should you need support on closing the case, please speak to '  
                                    + '<a href="'+'mailto:'+a.Contact.Account.Talent_Community_Manager__r.Email +'"> '+communityManagerName+'</a>'  +' or click '+Recordlink+' for a video walkthrough.<br/><br/>'
                                   + 'Please remember a case can be closed when you first speak to the contractor, NOT on completion of the request.'+'<br/><br/>'+
                                   + '<li>Current End Date:'+ a.contact.Account.Talent_End_Date__c +'</li>'+'<br/><br/>'+
                                   + '<u>Additional Information:</u>'+'<br/>'+
                                   + 'Case Number: '+ a.CaseNumber+'<br/>'+
                                   + 'Status: '+ a.Status +'<br/>'+
                                   + 'Requested by: '+a.Contact.Name+'<br/>'+
                                   + 'Contact Email: '+a.Contact.Email+'<br/>'+
                                   + 'Contact Phone: '+a.Contact.Phone+'<br/>'+
                                   + 'Candidate ID: '+ a.contact.Account.Talent_Id__c+'<br/><br/><br/>'+
                                   
                                   + 'Thank you for your continued support,'+'<br/>'+
                                   + communityName) ;
                }
                if(a.Owner.Email != null){
                    System.debug('======'+a.Owner.Email);
                    //email.setToAddresses(new String[] {a.Owner.Email});
                    email.setTargetObjectId(a.OwnerId);
                }
                
                if((a.createdby.Profile.Name.contains('Aerotek') || a.createdby.Profile.Name.contains('AstonCarter')) && (a.contact.Account.Talent_Account_Manager__c != null && a.contact.Account.Talent_Account_Manager__r.Email != null)){
                    System.debug('======'+a.contact.Account.Talent_Account_Manager__r.Email);
                   // email.setCcAddresses(new String[] {a.contact.Account.Talent_Account_Manager__r.Email});
                    email.setTargetObjectId(a.contact.Account.Talent_Account_Manager__r.Id);
                }else if(a.contact.Account.Talent_Community_Manager__c != null && a.contact.Account.Talent_Community_Manager__r.Email != null){
                    System.debug('======'+a.contact.Account.Talent_Community_Manager__r.Email);
                   // email.setCcAddresses(new String[] {a.contact.Account.Talent_Community_Manager__r.Email});
                    email.setTargetObjectId(a.contact.Account.Talent_Community_Manager__r.Id);        
                }
                email.setSaveAsActivity(false);
                if(a.Owner.Email != null){
                    mails.add(email);
                }
            }
        }
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.subject = 'Open Communities Case with that has been processed Today';
        email.setHtmlBody ('The Number or record Processed Today for EMEA are '+ mails.size()) ;
        email.setToAddresses(new String[] {'smadduri@allegisgroup.com'});
        mails.add(email);
        
        List<Messaging.Email> allMails = new List<Messaging.Email>();
        for( Integer j = 0; j < mails.size(); j++ ){
            allMails.add(mails.get(j));
        }
        
        
        system.debug('-mails------' + mails);
        //send mail
        if(allMails.size()>0){
            Messaging.sendEmail(allMails);
            
        }
    }     
    global void finish(Database.BatchableContext info){   
    } 
}