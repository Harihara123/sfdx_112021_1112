public class ServiceTouchpointController  {

    @AuraEnabled
    public static SObject getRecentServiceActivity(Id talentId) {
        List<Task> taskList = new List<Task>();
        List<Event> eventList = new List<Event>();

        Id userId = UserInfo.getUserId();

        List<String> typeList = new List<String>{'Service Touchpoint', 'Performance Feedback'};
        taskList = [
            SELECT Id, 
                Completed__c, 
                LastModifiedBy.Name, 
                Interview_Candidate_Comments__c,  
                Interview_Client_Comments__c, 
                Description,
                Type,
                CreatedDate,
                Source_System_Id__c
            FROM Task 
            WHERE WhatId =: talentId 
            AND ((Type IN: typeList AND Completed_Flag__c = True)
            OR (Type IN: typeList AND Status = 'Completed' AND Source_System_Id__c LIKE 'R.%'))
        ];

        eventList = [   
            SELECT Id, 
                LastModifiedBy.Name, 
                Interview_Candidate_Comments__c, 
                Interview_Client_Comments__c, 
                Description,
                StartDateTime,
                Pre_Meeting_Notes__c,
                Type,
                CreatedDate,
                Source_System_Id__c 
            FROM Event 
            WHERE WhatId =: talentId 
            AND Type IN: typeList 
            AND Completed_Flag__c = True
            ORDER BY StartDateTime DESC
            LIMIT 1
        ];

        SObject returnValue;
        Task mostRecentTask;
        Datetime taskDatetime;
        if(taskList.size() > 0) {
            mostRecentTask = getMostRecentTask(taskList);
            taskDatetime = getDatetimeFromTask(mostRecentTask);
        }
        if(taskList.size() == 0 && eventList.size() == 0) {
            return null;
        } else if(eventList.size() == 0 && taskList.size() != 0) {
            return mostRecentTask;
        } else if(eventList.size() != 0 && taskList.size() == 0) {
            return eventList[0];
        }

        returnValue = taskDatetime > eventList[0].StartDateTime ? (SObject) mostRecentTask : (SObject) eventList[0];

        return returnValue;
    }

    public static Map<Id, DateTime> getRecentServiceActivityDateTimes(List<SObject> accountWrappers) {
        List<Id> talentIds = getAccountIds(accountWrappers);
        Map<Id, Datetime> recentServiceActivityDates = new Map<Id, DateTime>();
        Id userId = UserInfo.getUserId();
        Id accountTalentRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        List<String> typeList = new List<String>{'Service Touchpoint', 'Meal', 'Meeting', 'Performance Feedback'};

        for(Account talent : [SELECT Id
                                , (SELECT Completed__c, Source_System_Id__c, CreatedDate FROM Tasks WHERE (Type IN: typeList AND Completed_Flag__c = true) OR (Type IN: typeList AND Status = 'Completed' AND Source_System_Id__c LIKE 'R.%') ) 
                                , (SELECT StartDateTime, Source_System_Id__c, CreatedDate FROM Events WHERE Type IN: typeList AND Completed_Flag__c = true ORDER BY StartDateTime DESC)
                                FROM Account WHERE Id IN: talentIds AND RecordTypeId =: accountTalentRT]) {
            DateTime taskDateTime, eventDateTime;
            if(!talent.Tasks.isEmpty()) {
                Task mostRecent = getMostRecentTask(talent.Tasks);
                taskDateTime = getDatetimeFromTask(mostRecent);
            }
            if(!talent.Events.isEmpty()) {
                eventDateTime = talent.Events[0].StartDateTime;
            }

            if(taskDateTime == null && eventDateTime != null) {
                recentServiceActivityDates.put(talent.Id, eventDateTime);
            }
            else if(taskDateTime != null && eventDateTime == null) {
                recentServiceActivityDates.put(talent.Id, taskDateTime);
            }
            else if(taskDateTime != null && eventDateTime != null) {
                recentServiceActivityDates.put(talent.Id, eventDateTime > taskDateTime ? eventDateTime : taskDateTime);
            }
        }
        return recentServiceActivityDates;
    }

    @AuraEnabled
    public static Map<String, List<String>> getGuidedQuestionsForUserOpco() {
        String opco = ServiceTouchpointController.getCurrentUserOpco();
        List<String> opcos = new List<String> {'TEK', 'Aerotek', 'ONS', 'AG_EMEA'};
        Map<String, List<String>> questions = new Map<String, List<String>>();

        if(String.isEmpty(opco) || String.isBlank(opco) || !opcos.contains(opco)) {
            return questions;
        }

        //need tech debt to change the values of the guided questions to ONS, currently is Aerotek
        if(opco == 'ONS') {
            opco = 'Aerotek';
        }
        for(Service_Activity_Guided_Question__mdt serviceActionGuidedQuestion : [SELECT Category__c, Guided_Question_Text__c, Category_Order__c, Question_Order__c FROM Service_Activity_Guided_Question__mdt WHERE OPCO__c =: opco ORDER BY Category_Order__c, Question_Order__c]) {
            if(questions.get(serviceActionGuidedQuestion.Category__c) == null) {
                questions.put(serviceActionGuidedQuestion.Category__c, new List<String>{serviceActionGuidedQuestion.Guided_Question_Text__c});
            }
            else {
                questions.get(serviceActionGuidedQuestion.Category__c).add(serviceActionGuidedQuestion.Guided_Question_Text__c);
            }
        }
        return questions;
    }

    @AuraEnabled
    public static SObject getCurrentTalentAssignment(Id talentId) {
        SObject currentAssignment;

        String opco = ServiceTouchpointController.getCurrentUserOpco();
        //try-catches here
        if(opco == 'TEK' || opco == 'Aerotek' || opco == 'ONS') {
            try {
                currentAssignment = [
                    SELECT Id, 
                        Start_Date__c, 
                        End_Date__c, 
                        Organization_Name__c, 
                        Talent__c,
                        Talent__r.Talent_Current_Employer_Formula__c,
                        Job_Title__c 
                    FROM Talent_Work_History__c 
                    WHERE Talent__c =: talentId 
                    AND Current_Assignment__c = true
                    ORDER BY Start_Date__c DESC
                    LIMIT 1
                ];
            } catch(QueryException ex) {
                System.debug('No recent activity');
            }
        } else {
            try {
                currentAssignment = [
                    SELECT Id,
                        Start_Date__c,
                        End_Date__c,
                        Organization_Name__c,
                        Talent__c,
                        Talent__r.Talent_Current_Employer_Formula__c,
                        Title__c
                    FROM Talent_Experience__c
                    WHERE Talent__c =: talentId
                    AND Current_Assignment__c = true
                    ORDER BY Start_Date__c DESC
                    LIMIT 1
                ];
            } catch(QueryException ex) {
                System.debug('No recent activity');
            }
        }


        return currentAssignment;
    }

    public static String getCurrentUserOpco() {
        Id currentUserId = UserInfo.getUserId();
        User currentUser = [SELECT Id, OPCO__c FROM User WHERE Id =: currentUserId];

        if(currentUser.OPCO__c == 'TEK') {
            return currentUser.OPCO__c;
        }

        if(currentUser.OPCO__c == 'ONS'|| currentUser.OPCO__c == 'Aerotek') {
            return 'ONS';
        }

        Talent_Role_to_Ownership_Mapping__mdt ownershipRecord;
        //try catch
        if(!String.isEmpty(currentUser.OPCO__c)) {
            ownershipRecord = [SELECT Label, Talent_Ownership__c FROM Talent_Role_to_Ownership_Mapping__mdt WHERE Opco_Code__c =: currentUser.OPCO__c];
        }


        if(ownershipRecord != null) {
            return ownershipRecord.Talent_Ownership__c;
        }

        return '';
    }

    @TestVisible
    private static List<Id> getAccountIds(List<SObject> accountWrappers) {
        List<Id> accountIds = new List<Id>();
        for(SObject accountWrapper : accountWrappers) {
            if(accountWrapper.getSObjectType().getDescribe().getName() == 'Contact') {
                accountIds.add((Id)accountWrapper.get('AccountId'));
            } else {
                accountIds.add((Id)accountWrapper.get('WhatId'));
            }
        }
        return accountIds;
    }

    @TestVisible
    private static Task getMostRecentTask(List<Task> tasks) {
        Task mostRecent;
        for(Task aTask : tasks) {
            if(mostRecent == null) {
                mostRecent = aTask;
            }
            else {
                if(getDatetimeFromTask(aTask) >= getDatetimeFromTask(mostRecent)) {
                    mostRecent = aTask;
                }
            }
        }
        return mostRecent;
    }

    @TestVisible
    private static DateTime getDatetimeFromTask(Task aTask) {
        if(String.isNotBlank(aTask.Source_System_Id__c) && aTask.Source_System_Id__c.startsWith('R.')) {
            return aTask.CreatedDate;
        }
        return aTask.Completed__c;            
    }
}