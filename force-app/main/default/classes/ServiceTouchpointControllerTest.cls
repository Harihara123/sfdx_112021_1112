@isTest
public class ServiceTouchpointControllerTest  {

    @TestSetup
    static void setup() {
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;
        talentAcc.G2_Completed__c = true;
        insert talentAcc;

        Contact ct = TestData.newContact(talentAcc.id, 1, 'Talent'); 
        ct.Email = 'test@allegisgroup.com';
        ct.Title = 'test title';
        ct.Talent_State_Text__c = 'test text';
        ct.MailingState = 'test text';
        insert ct;
    }

    @isTest
    static void getRecentServiceActivity_givenNoRecentServiceActivity_returnsNoServiceActivity() {
        SObject result;

        Contact testContact = [SELECT Id, AccountId, Source_System_Id__c FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Test.startTest();
            result = ServiceTouchpointController.getRecentServiceActivity(testContact.AccountId);
        Test.stopTest();

        System.assertEquals(null, result, 'There should not have been any service activities returned');
    }

    @isTest
    static void getRecentServiceActivity_givenRecentEventActivity_returnsEvent() {
        SObject result;
        Contact testContact = [SELECT Id, AccountId FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Event ev = new Event();
        ev.Type = 'Service Touchpoint';
        ev.WhatId = testContact.AccountId;
        ev.DurationInMinutes = 20;
        ev.ActivityDateTime = DateTime.newInstance(Date.today(), Time.newInstance(0,0,0,0));
        ev.Completed_Flag__c = true;

        insert ev;

        Test.startTest();
            result = ServiceTouchpointController.getRecentServiceActivity(testContact.AccountId);
        Test.stopTest();

        System.assertNotEquals(null, result, 'An SObject should have been returned.');
        System.assertEquals('Event', String.valueOf(result.getSObjectType()), 'The returned value should have been an event.');

    }

    @isTest
    static void getRecentServiceActivity_givenRecentTaskActivity_returnsEvent() {
        SObject result;
        Contact testContact = [SELECT Id, AccountId FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Task tsk = new Task();
        tsk.Type = 'Service Touchpoint';
        tsk.WhatId = testContact.AccountId;
        tsk.Completed_Flag__c = true;

        insert tsk;

        Test.startTest();
            result = ServiceTouchpointController.getRecentServiceActivity(testContact.AccountId);
        Test.stopTest();

        System.assertNotEquals(null, result, 'An SObject should have been returned.');
        System.assertEquals('Task', String.valueOf(result.getSObjectType()), 'The returned value should have been an event.');

    }

    @isTest
    static void getRecentServiceActivity_givenRecentTaskAndEventActivity_returnsEarlierCreatedActivity() {
        SObject result;
        Contact testContact = [SELECT Id, AccountId FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Task tsk = new Task();
        tsk.Type = 'Service Touchpoint';
        tsk.WhatId = testContact.AccountId;
        tsk.Completed_Flag__c = true;

        insert tsk;

        Event ev = new Event();
        ev.Type = 'Service Touchpoint';
        ev.WhatId = testContact.AccountId;
        ev.DurationInMinutes = 20;
        ev.ActivityDateTime = DateTime.newInstance(Date.today(), Time.newInstance(0,0,0,0));

        insert ev;

        Test.startTest();
            result = ServiceTouchpointController.getRecentServiceActivity(testContact.AccountId);
        Test.stopTest();

        System.assertNotEquals(null, result, 'An SObject should have been returned.');

    }

    @isTest
    static void getGuidedQuestionsForUserOpco_givenUserInNoOPCO_returnsNoQuestions() {
        Map<String, List<String>> results = new Map<String, List<String>>();

        Contact testContact = [SELECT Id, AccountId FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Id uId = UserInfo.getUserId();

        User cUser = [SELECT Id, OPCO__c FROM User WHERE Id =: uId];

        cUser.OPCO__c = '';

        update cUser;

        Test.startTest();
            results = ServiceTouchpointController.getGuidedQuestionsForUserOpco();
        Test.stopTest();

        System.assertEquals(0, results.size(), 'There should not have been any questions returned');
    }

    @isTest
    static void getGuidedQuestionsForUserOpco_givenUserInOPCO_returnsQuestions() {
        Map<String, List<String>> results = new Map<String, List<String>>();

        Id uId = UserInfo.getUserId();

        User cUser = [SELECT Id, OPCO__c FROM User WHERE Id =: uId];

        cUser.OPCO__c = 'TEK';

        update cUser;

        Contact testContact = [SELECT Id, AccountId FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Test.startTest();
            results = ServiceTouchpointController.getGuidedQuestionsForUserOpco();
        Test.stopTest();

        System.assert(results.size() > 0, 'There should have been questions returned');
    }

    @isTest
    static void getCurrentTalentAssignment_givenTalentWithoutAssignment_returnsNoItems() {
        SObject result;

        Id uId = UserInfo.getUserId();

        User cUser = [SELECT Id, OPCO__c FROM User WHERE Id =: uId];

        cUser.OPCO__c = '';

        update cUser;

        Contact testContact = [SELECT Id, AccountId FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Test.startTest();
            result = ServiceTouchpointController.getCurrentTalentAssignment(testContact.Id);
        Test.stopTest();

        System.assertEquals(null, result, 'There should not have been items returned');
    }

    @isTest
    static void getCurrentTalentAssignment_givenTalentWithAssignment_returnsItem() {
        SObject result;

        Id uId = UserInfo.getUserId();

        User cUser = [SELECT Id, OPCO__c FROM User WHERE Id =: uId];

        cUser.OPCO__c = 'TEK';

        update cUser;

        Contact testContact = [SELECT Id, AccountId, Source_System_Id__c FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];

        Talent_Work_History__c twh = new Talent_Work_History__c();
        twh.Current_Assignment__c = true;
        twh.Talent__c = testContact.AccountId;
        twh.Job_Title__c = 'Test Title';
        twh.uniqueId__c = '24365234';
        insert twh;

        Test.startTest();
            result = ServiceTouchpointController.getCurrentTalentAssignment(testContact.AccountId);
        Test.stopTest();

        System.assertNotEquals(null, result, 'There should have been items returned');
        System.assertEquals('Talent_Work_History__c', String.valueOf(result.getSObjectType()), 'The return value should have been Talent Work History');
    }
    
    @isTest
    static void getRecentServiceActivityDateTimes_givenTask_returnDatetime() {
        Contact testContact = [SELECT Id, AccountId, Last_Service_Date__c, Source_System_Id__c FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];
        
        Datetime yesterday = System.now().addDays(-1);

        Test.startTest();
            Task oldTask = new Task();
            oldTask.Type = 'Service Touchpoint';
            oldTask.WhoId = testContact.Id;
            oldTask.WhatId = testContact.AccountId;
            oldTask.Completed__c = yesterday;
            oldTask.Completed_Flag__c = true;
            insert oldTask;
            
            testContact = [SELECT Id, AccountId, Last_Service_Date__c, Source_System_Id__c FROM Contact WHERE Email = 'test@allegisgroup.com' LIMIT 1];
            System.assertEquals(yesterday.date(), testContact.Last_Service_Date__c, 'should be today-1');
        Test.stopTest();
    }
}