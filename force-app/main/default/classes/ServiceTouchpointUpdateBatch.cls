global class ServiceTouchpointUpdateBatch implements Database.Batchable<SObject> {
	
	global ServiceTouchpointUpdateBatch() {
		
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
        //Query for recordtype id

        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();

        String queryString = 'SELECT Id, AccountId, Last_Service_Date__c, Account.Candidate_Status__c FROM Contact WHERE Account.Candidate_Status__c = \'Current\' AND RecordTypeId = \''+ devRecordTypeId + '\'';
        //String queryString = 'SELECT Id, AccountId, Last_Service_Date__c, Account.Candidate_Status__c FROM Contact WHERE Account.Candidate_Status__c = \'Current\' AND RecordTypeId = \'01224000000FFqVAAW\' AND LastModifiedById = \'005U0000000OfldIAC\' AND Merge_State__c = \'Post\' AND LastModifiedDate = Today AND Last_Service_Date__c = null';
		return Database.getQueryLocator(queryString);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Contact> scope) {
	    ActivityHelper.updateLastServiceDateOnContact(scope);
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		System.debug('batch finished');
	}
}