@isTest 
private class ServiceTouchpointUpdateBatch_Test {

	@isTest
	private static void ServiceTouchpointUpdateBatch_Method() {
		Account newAcc = BaseController_Test.createTalentAccount('');
		newAcc.Candidate_Status__c ='Current';
		update newAcc;
		Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
		Test.startTest();
		ServiceTouchpointUpdateBatch STPUpdateBatch = new ServiceTouchpointUpdateBatch();
		id jobId=Database.executeBatch(STPUpdateBatch);
		Test.stopTest();
	}
}