public class SessionValidatorController {
  @AuraEnabled(cacheable=true)
  public static String getUserSessionId(){
    String sessionId = '';
    PageReference vfPage = Page.SessionIdVFPage;
    String vfContent = vfPage.getContent().toString();
    Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
    endP = vfContent.indexOf('End_Of_Session_Id');
    sessionId = vfContent.substring(startP, endP);
    return sessionId;
 }
}