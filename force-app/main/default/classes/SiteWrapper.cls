/**
 * Thin wrapper around the Salesforce Site class. Initially created to facilitate unit testing 
 * (by allowing the wrapped methods to be mocked). Because the purpose of this class is to allow 
 * for mocking, the contained methods are NON-static.
 */
public class SiteWrapper {

    public SiteWrapper() {

    }
    
    /**
     * Allows users to log in to the current site with the given username and password, then takes 
     * them to the startUrl. If startUrl is not a relative path, it defaults to the site's designated 
     * index page.
     */
    public PageReference login(String username, String password, String startUrl, Boolean isJobseeker) {
        PageReference result = null;
        try{
        String sitePrefix = Site.getPathPrefix();
        system.debug('+++++getPrefix()123++++++'+(sitePrefix.contains('astoncartercom')));

		/*if((sitePrefix.contains('astoncartercom')) && (startUrl == null || startUrl == '')){
        system.debug('++++++++++++++++++');
            startUrl ='/s/';
        }else*/
		if(!(sitePrefix.contains('teksystemscom')) && !(String.isBlank(startUrl)) && (startUrl.contains('/s/group/CollaborationGroup/Recent'))){
        system.debug('++++++++++++++++++');
            startUrl ='/s/group/CollaborationGroup/Recent';
        }
        system.debug('+++++getPrefix()123++++++'+startUrl);
        system.debug('++++++++++30'+ ApexPages.getMessages());
        result = Site.login(username, password, startUrl);
        
            system.debug('++++++++++31'+ ApexPages.getMessages());
            if (result == null) {
            for (ApexPages.Message st : ApexPages.getMessages()) {
                string s= string.valueOf(st);
                system.debug('++++++++++33'+ s );
                if (s.contains('ApexPages.Message["Your login attempt has failed. Make sure the username and password are correct."]')) {
                    //s.replace('ApexPages.Message["Your login attempt has failed. Make sure the username and password are correct."]','ApexPages.Message["'+System.Label.TC_Login_Error+'"]');
                    String tempStr = s.replace('ApexPages.Message["Your login attempt has failed. Make sure the username and password are correct."]','hi');
                    system.debug('++++++++++34'+ tempStr );
                    PageReference pref = null;
                    
                    pref = Page.tc_login;
                    
                    if (!isJobseeker)
                        pref = Page.tc_login;
                    else
                        pref = Page.js_login;
                    
                    return pref;
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.Error,tempStr);
                    ApexPages.addMessage(msg);
                }
            } 
            }
        }catch(exception ex){

        } 
         return result;
    }

    /**
     * Adding overloaded method to tag login as jobseeker.
     */
    public PageReference login(String username, String password, String startUrl) {
        return this.login(username, password, startUrl, false);
    }
    
    /**
     * Changes the password of the current user.
     */
    public PageReference changePassword(String newPassword, String verifyNewPassword, String oldPassword) {
        PageReference result = null;
        result = Site.changePassword(newPassword, verifyNewPassword, oldPassword);
        return result;
    }

    /**
     * Returns the base URL of the current site that references and links should use. Note that this 
     * field may return the referring page's URL instead of the current request's URL. The returned 
     * value includes the path prefix and never ends with a / character. If the current request is not 
     * a site request, then this field returns an empty string.
     */
    public String getBaseUrl() {
        String result = '';

        /*
         * Returns the base URL of the current site that references and links should use. Includes the path prefix. 
         *  I believe this can be seen in the Salesforce config as Setup -> Build -> Develop -> Sites as the value of 
         *  the "Site URL". For example: 
         *    http://comci-allegisconnected.cs87.force.com/aerotek
         *  In PROD, I would think that this value would be something akin to "https://connect.teksystems.com", but 
         *  it may actually be more akin to the pre-PROD environments. Regardless, either should work here.
         */
        result = Site.getBaseUrl();
        
        return result;
    }

}