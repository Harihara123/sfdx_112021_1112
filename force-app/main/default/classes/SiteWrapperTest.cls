@isTest
private class SiteWrapperTest {

    /**
     * Contrived little test method just to get code coverage. The class being tested only exists 
     * to facilitate unit testing, so the unit tests on it, itself, aren't going to be very 
     * satisfying.
     */
    @isTest
    static void testLogin() {
        Test.startTest();
        SiteWrapper testSubjectImpl = new SiteWrapper();
        PageReference result = testSubjectImpl.login('username', 'password', null);
        System.assertEquals(null, result, 
            'The call to login should fail, given these bogus credentials.');
        Test.stopTest();
    }

    /**
     * Contrived little test method just to get code coverage. The class being tested only exists 
     * to facilitate unit testing, so the unit tests on it, itself, aren't going to be very 
     * satisfying.
     */
    @isTest
    static void testChangePassword() {
        Test.startTest();
        SiteWrapper testSubjectImpl = new SiteWrapper();
        PageReference result = testSubjectImpl.changePassword('newPassword', 'verifyNewPassword', 'oldPassword');
        System.assertEquals(null, result, 
            'The call to changePassword should fail, given these bogus credentials.');
        Test.stopTest();
    }

    /**
     * Contrived little test method just to get code coverage. The class being tested only exists 
     * to facilitate unit testing, so the unit tests on it, itself, aren't going to be very 
     * satisfying.
     */
    @isTest
    static void testGetBaseUrl() {
        Test.startTest();
        SiteWrapper testSubjectImpl = new SiteWrapper();
        String result = testSubjectImpl.getBaseUrl();
        System.assertEquals('', result, 
            'The call to getBaseUrl should always return an empty string in this test setting.');
        Test.stopTest();
    }

}