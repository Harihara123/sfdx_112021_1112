public class SkillBasedCaseRouting {
    @InvocableMethod
    public static void routingCasesToAgents(LIst<String> caseIds){
        //Create PSR
        //Add skills to the request for the case
        //Push it to queue
        List<Case> casesInserted = [SELECT id,subject,Center_Name__c from Case where ID IN: caseIds];
        system.debug('The cases are -->'+casesInserted);
        LIst<Skill> allSkillsInDB = [SELECT id,MasterLabel from Skill];
        system.debug('The skills are -->'+allSkillsInDB);
        for(Case caseRec : casesInserted){
            PendingServiceRouting psr= new PendingServiceRouting();
            psr.workItemId = caseRec.Id;
            psr.RoutingType = 'SkillsBased';
            psr.RoutingPriority = 1;
            psr.CapacityWeight = 5;
            psr.ServiceChannelId = getChannelId('Case'); //USE YOUR OWN SERVICE CHANNEL ID
            psr.RoutingModel = 'LeastActive';
            psr.IsReadyForRouting = FALSE; //DRAFT state
            Insert psr; //DONE WITH MY FIRST STEP
            system.debug('The PendingServiceRouting are -->'+psr);
            //FIND OUT THE SKILLS REQUIRED FOR A GIVEN CASE BASED ON ITS SUBJECT
            List<String> matchingSkillIds = new List<String>();
            for(Skill skillRec: allSkillsINDB){
                if(caseRec.Center_Name__c.contains(skillRec.MasterLabel)){
                    matchingSkillIds.add(skillRec.Id);
                }
            }
            system.debug('The matching skills are -->'+matchingSkillIds);
            List<SkillRequirement> skillReqsToInsert = new List<SkillRequirement>();
            //Associate matching skills with PSR request
            for(String matchingSkillId: matchingSkillIds){
                SkillRequirement skillRequ = new SkillRequirement();
                skillRequ.SkillId = matchingSkillId;
                skillRequ.RelatedRecordId = psr.id;
                skillRequ.SkillLevel = 10;
                skillReqsToInsert.add(skillRequ);
            }
            Insert skillReqsToInsert;
            system.debug('The matching skills with PSR -->'+skillReqsToInsert);
            //PUSH our request in to the queue
            psr.IsReadyForRouting = TRUE;
            Update PSR;
        }
    }
    public static String getChannelId(String channelName) {
        ServiceChannel channel = [Select Id From ServiceChannel Where DeveloperName = :channelName];
        return channel.Id;
    }
}