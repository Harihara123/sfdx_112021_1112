public with sharing class SmartSearchController {
    @AuraEnabled(cacheable=false)
    public static string getAgnosticFilters(String queryString){
        String responseBody = '';
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
        System.debug('Query str => ' + queryString);

        ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues('Smart Search Service');

        req.setHeader('Authorization', 'Bearer ' + serviceSettings.OAuth_Token__c);
        req.setEndpoint(serviceSettings.ServiceURL1__c + queryString);
        req.setMethod('GET');
        req.setTimeout(5000);

        try {
            HttpResponse response = connection.send(req);
            
            if (response.getStatusCode() == 200) {
                responseBody = response.getBody();
                
            }

            return responseBody;
        } catch (Exception ex) {
            // Callout to apigee failed
            ConnectedLog.LogException('SmartSearchController', 'getAgnosticFilters', ex);
            System.debug(LoggingLevel.ERROR, 'Apigee call to Req Search Service failed! ' + ex.getMessage());
            throw ex;
        }
    }
}