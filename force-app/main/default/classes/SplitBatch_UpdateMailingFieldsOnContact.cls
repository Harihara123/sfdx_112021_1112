global class SplitBatch_UpdateMailingFieldsOnContact {

	//thisQuery = 'SELECT Id, MailingState, MailingCountry FROM Contact WHERE Account.Is_Talent__c = true AND (MailingState != null OR MailingCountry != null) ';

	public static void run(String thisQuery, Integer thisBatchSize) {
		Integer batchSize = (thisBatchSize != null && thisBatchSize > 0) ? thisBatchSize : 50;
		if(String.isNotBlank(thisQuery)) {
			String splitter = thisQuery.containsIgnoreCase(' where ') ? ' AND Account.Talent_Id__c LIKE ' : ' WHERE Account.Talent_Id__c LIKE ';
			for(Integer i = 0; i < 10; i++) {
				Batch_updateMailingFieldsOnContact b = new Batch_updateMailingFieldsOnContact();
				b.query = thisQuery + splitter + '\'%' + i + '\'';
				Database.executeBatch(b, batchSize); 
			}
		}
	}
}