public class StartDateChangeTriggerHandler {    
	public static List<Case> updateCase(StartDateEvent__e PlatformEvent,Case caseRecord){        
        List<Case> caseListtoUpdate   = new List<Case>();        
        System.debug('**************caseRecord : '+caseRecord);
        if(caseRecord!=null){            
            //List<Case> caseList = [Select Id,VSB_Start_Date__c,Submittal__c from Case where Id =: caseId];        	
        	system.debug('Inside Case--');
            if(PlatformEvent.Start_Date__c!=null){
            	caseRecord.VSB_Start_Date__c = PlatformEvent.Start_Date__c;
            }
            if(PlatformEvent.Comments__c!=null){
            	caseRecord.Case_Issue_Description__c = PlatformEvent.Comments__c;
            }
            caseListtoUpdate.add(caseRecord);
            System.debug('Case start date : '+caseRecord.VSB_Start_Date__c);
            System.debug('Case Details : '+caseRecord);
		}		
		return caseListtoUpdate;
    }
    public static List<Event> updateEvent(StartDateEvent__e PlatformEvent,Event EventRecord){
    	List<Event> eventListToUpdate = new List<Event>();                
        if(EventRecord!=null){        	
        	if(PlatformEvent.Start_Date__c!=null){
            	EventRecord.Talent_Start_Date__c = PlatformEvent.Start_Date__c;
            }
            if(PlatformEvent.Comments__c!=null){
            	EventRecord.Description = PlatformEvent.Comments__c;
            }
            eventListToUpdate.add(EventRecord); 
        }        
        return eventListToUpdate;
    }    
    public static List<Order> updateSubmittal(StartDateEvent__e PlatformEvent,Order OrderRecord){
    	List<Order> OrderListToUpdate = new List<Order>();       
        if(OrderRecord!=null){
            if(PlatformEvent.Start_Date__c!=null){
            	OrderRecord.Start_Date__c = PlatformEvent.Start_Date__c;
            }
            if(PlatformEvent.Comments__c!=null){
            	OrderRecord.Comments__c = PlatformEvent.Comments__c;
            }
            OrderListToUpdate.add(OrderRecord);                
         }        
		return OrderListToUpdate;
    }    
    public static Map<Id,Order> getOrderIdListNew(Set<Id>recordId,String objType){
        List<Case> CaseLst=new List<Case>();
        List<Event> EventLst=new List<Event>();
        List<Order> OrderLst=new List<Order>();
        
        Map<Id,Order> OrderIdsWithCase=new Map<Id,Order>();
        
        if(objType=='Case'){
            Map<Id,Id> mapCaseOrder=new Map<Id,Id>();
        	CaseLst=[Select Submittal__c from Case where Id in: recordId];
            for(Case cs:CaseLst){
                mapCaseOrder.put(cs.Submittal__c,cs.Id);                
            }            
            if(mapCaseOrder.size()>0){
                OrderLst = [Select Id ,Start_Date__c,Comments__c from Order where Id in : mapCaseOrder.keySet()];
                for(Order ord:OrderLst){
                    if(!OrderIdsWithCase.containsKey(mapCaseOrder.get(ord.Id))){
                        OrderIdsWithCase.put(mapCaseOrder.get(ord.Id),ord);
                    }                
            	}
				return OrderIdsWithCase;
          	}else{
                return null;
            }                
        }else if(objType=='Event'){
            Map<Id,Id> mapEventOrder=new Map<Id,Id>();
            EventLst = [Select Id, Order_Id__c FROM Event Where Subject = 'Started' AND Id in: recordId];
            for(Event eveids:EventLst){
                mapEventOrder.put(eveids.Order_Id__c,eveids.Id);
            }
            if(mapEventOrder.size()>0){
                OrderLst = [Select Id ,Start_Date__c,Comments__c from Order where Id in : mapEventOrder.keySet()];
                for(Order ord:OrderLst){
                    if(!OrderIdsWithCase.containsKey(mapEventOrder.get(ord.Id))){
                        OrderIdsWithCase.put(mapEventOrder.get(ord.Id),ord);
                    }                
            	}
				return OrderIdsWithCase;
            }else
                return null;
        }else{
            return null;
        }
    }   
    public static Map<Id,Case> getCaseIdListNew(Set<Id>recordId,String objType){
        List<Case> CaseIdLst=new List<Case>();
        List<Event> EventLst=new List<Event>();        
        Map<Id,Case> CaseIdsWithOrder=new Map<Id,Case>();
        
        if(objType=='Order'){
            System.debug('**************recordId : '+recordId);
        	CaseIdLst = [Select Id,VSB_Start_Date__c,Case_Issue_Description__c,Submittal__c from Case where Submittal__c in: recordId order by createdDate DESC];            
            for(Case cs:CaseIdLst){
                if(!CaseIdsWithOrder.containsKey(cs.Submittal__c)){
                    CaseIdsWithOrder.put(cs.Submittal__c,cs);
                }                
            }            
            return CaseIdsWithOrder;
        }else if(objType=='Event'){
            Map<Id,Id> mapOrderEvent=new Map<Id,Id>();
            EventLst = [Select Id, Order_Id__c FROM Event Where Subject = 'Started' AND Id in: recordId];            
            for(Event ev :EventLst){                
                mapOrderEvent.put(ev.Order_Id__c,ev.Id);
            }
            if(mapOrderEvent.size()>0){
                CaseIdLst = [Select Id,VSB_Start_Date__c,Case_Issue_Description__c,Submittal__c from Case where Submittal__c in: mapOrderEvent.keySet() order by createdDate DESC];
                for(Case cs:CaseIdLst){
                    if(!CaseIdsWithOrder.containsKey(mapOrderEvent.get(cs.Submittal__c))){
                        CaseIdsWithOrder.put(mapOrderEvent.get(cs.Submittal__c),cs);
                    }                
            	}
				return CaseIdsWithOrder;
          	}else{
                return null;
            }            
        }else{
            return null;
        }
    }  
    public static Map<Id,Event> getEventIdListNew(Set<Id>recordId,String objType){
        List<Case> CaseIdLst=new List<Case>();
        List<Event> EventLstToUpdate=new List<Event>();
        List<Id> submittalIds=new List<Id>();
        
        Map<Id,Event> EventIdsWithOrder=new Map<Id,Event>();
        if(objType=='Order'){
            EventLstToUpdate = [SELECT Id,Talent_Start_Date__c,Description,CreatedDate,Order_Id__c  FROM Event WHERE Subject = 'Started' AND Order_Id__c in: recordId order by createdDate DESC] ;            
            for(Event ev:EventLstToUpdate){
                if(!EventIdsWithOrder.containsKey(ev.Order_Id__c)){
                	EventIdsWithOrder.put(ev.Order_Id__c,ev);
                }
            }            
            return EventIdsWithOrder;
        }else if(objType=='Case'){
            System.debug('*****************in case');
            Map<Id,Id> mapCaseOrder=new Map<Id,Id>();
            CaseIdLst = [Select Id, Submittal__c FROM Case Where Id in: recordId];
			System.debug('*****************CaseIdLst'+CaseIdLst);            
            for(Case cs :CaseIdLst){                
                mapCaseOrder.put(cs.Submittal__c,cs.Id);
            }
            EventLstToUpdate=[SELECT Id,Talent_Start_Date__c,Description,CreatedDate,Order_Id__c  FROM Event WHERE Subject = 'Started' AND Order_Id__c in: mapCaseOrder.keySet() order by createdDate DESC];
            for(Event ev:EventLstToUpdate){
                if(!EventIdsWithOrder.containsKey(mapCaseOrder.get(ev.Order_Id__c))){
                	EventIdsWithOrder.put(mapCaseOrder.get(ev.Order_Id__c),ev);
                }
            }
            return EventIdsWithOrder;
                       
        }else{
            return null;
        }
    }
}