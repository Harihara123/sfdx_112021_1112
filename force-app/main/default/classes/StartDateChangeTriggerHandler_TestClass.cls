@isTest
public class StartDateChangeTriggerHandler_TestClass {
 	@isTest
    public static void updateCaseTest(){
        User usr = TestDataHelper.createUser('Single Desk 1');        
        Account acc = TestData.newAccount(1,'Talent');
        Contact con = TestData.newContact(acc.Id, 1, 'Talent');
        Opportunity opp = TestData.newOpportunity(acc.Id, 1);
        Order ord = TestData.newOrder(acc.Id, con.Id,usr.Id,'OpportunitySubmission');
        
        //Case 
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = con.Id,
                                     ownerId = usr.Id, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
             
        //Event
        Event events = new Event(subject='Started',ActivityDateTime = system.now(),Talent_Start_Date__c=ord.Start_Date__c);
        
       test.startTest();        
       	StartDateEvent__e dateevent = New StartDateEvent__e();
        dateevent.Start_Date__c = Date.newInstance(2019, 12, 9);
        dateevent.Comments__c = 'Platform event Comment';
        
       StartDateChangeTriggerHandler.updateCase(dateevent, testCase);
       test.stopTest();
    }
    
    @isTest
    public static void updateEventTest(){
        User usr = TestDataHelper.createUser('Single Desk 1');        
        Account acc = TestData.newAccount(1,'Talent');
        Contact con = TestData.newContact(acc.Id, 1, 'Talent');
        Opportunity opp = TestData.newOpportunity(acc.Id, 1);
        Order ord = TestData.newOrder(acc.Id, con.Id,usr.Id,'OpportunitySubmission');
        //Case 
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = con.Id,
                                     ownerId = usr.Id, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
             
        //Event
        Event events = new Event(subject='Started',ActivityDateTime = system.now(),Talent_Start_Date__c=ord.Start_Date__c);
        
       test.startTest();        
       	StartDateEvent__e dateevent = New StartDateEvent__e();
        dateevent.Start_Date__c = Date.newInstance(2019, 12, 9);
        dateevent.Comments__c = 'Platform event Comment';
        
       StartDateChangeTriggerHandler.updateEvent(dateevent, events);
       test.stopTest();
    }
    
    @isTest
    public static void updateSubmittalTest(){
        User usr = TestDataHelper.createUser('Single Desk 1');        
        Account acc = TestData.newAccount(1,'Talent');
        Contact con = TestData.newContact(acc.Id, 1, 'Talent');
        Opportunity opp = TestData.newOpportunity(acc.Id, 1);
        Order ord = TestData.newOrder(acc.Id, con.Id,usr.Id,'OpportunitySubmission');
        //Case 
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = con.Id,
                                     ownerId = usr.Id, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            
        //Event
        Event events = new Event(subject='Started',ActivityDateTime = system.now(),Talent_Start_Date__c=ord.Start_Date__c);
        
       test.startTest();        
       	StartDateEvent__e dateevent = New StartDateEvent__e();
        dateevent.Start_Date__c = Date.newInstance(2019, 12, 9);
        dateevent.Comments__c = 'Platform event Comment';
        
       StartDateChangeTriggerHandler.updateSubmittal(dateevent, ord);
       test.stopTest();
    }
    
    @isTest
    public static void getOrderIdListNewTest(){
        Set<Id> OrderIdSet = new Set<Id>();
        Map<Id,Id> mapCaseOrder=new Map<Id,Id>();
        User usr = TestDataHelper.createUser('Single Desk 1');        
        Account acc = TestData.newAccount(1,'Talent');
        Contact con = TestData.newContact(acc.Id, 1, 'Talent');
        Opportunity opp = TestData.newOpportunity(acc.Id, 1);
        Order ord = TestData.newOrder(acc.Id, con.Id,usr.Id,'OpportunitySubmission');
        OrderIdSet.add(ord.Id);
        //Case 
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = con.Id,
                                     ownerId = usr.Id,
                                     Submittal__c = ord.Id,
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
             
        //Event
        Event events = new Event(subject='Started',ActivityDateTime = system.now(),Talent_Start_Date__c=ord.Start_Date__c);        
        mapCaseOrder.put(ord.Id, testCase.Id);
        
       test.startTest();        
       	StartDateEvent__e dateevent = New StartDateEvent__e();
        dateevent.Start_Date__c = Date.newInstance(2019, 12, 9);
        dateevent.Comments__c = 'Platform event Comment';
        
       StartDateChangeTriggerHandler.getOrderIdListNew(OrderIdSet, 'Case');
       StartDateChangeTriggerHandler.getOrderIdListNew(OrderIdSet, 'Event');
       StartDateChangeTriggerHandler.getOrderIdListNew(OrderIdSet, 'Abc'); 
       test.stopTest();
    }
    
    @isTest
    public static void getCaseIdListNewTest(){
        Set<Id> CaseIdSet = new Set<Id>();
        User usr = TestDataHelper.createUser('Single Desk 1');        
        Account acc = TestData.newAccount(1,'Talent');
        Contact con = TestData.newContact(acc.Id, 1, 'Talent');
        Opportunity opp = TestData.newOpportunity(acc.Id, 1);
        Order ord = TestData.newOrder(acc.Id, con.Id,usr.Id,'OpportunitySubmission');        
        //Case 
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = con.Id,
                                     ownerId = usr.Id, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            CaseIdSet.add(testCase.Id);
        //Event
        Event events = new Event(subject='Started',ActivityDateTime = system.now(),Talent_Start_Date__c=ord.Start_Date__c);
                
       test.startTest();        
       	StartDateEvent__e dateevent = New StartDateEvent__e();
        dateevent.Start_Date__c = Date.newInstance(2019, 12, 9);
        dateevent.Comments__c = 'Platform event Comment';
        
       StartDateChangeTriggerHandler.getCaseIdListNew(CaseIdSet, 'Order');
       StartDateChangeTriggerHandler.getCaseIdListNew(CaseIdSet, 'Event');
       StartDateChangeTriggerHandler.getCaseIdListNew(CaseIdSet, 'Abc'); 
       test.stopTest();
    }
    
    @isTest
    public static void getEventIdListNewTest(){
        Set<Id> EventIdSet = new Set<Id>();
        User usr = TestDataHelper.createUser('Single Desk 1');        
        Account acc = TestData.newAccount(1,'Talent');
        Contact con = TestData.newContact(acc.Id, 1, 'Talent');
        Opportunity opp = TestData.newOpportunity(acc.Id, 1);
        Order ord = TestData.newOrder(acc.Id, con.Id,usr.Id,'OpportunitySubmission');        
        //Case 
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FSG').getRecordTypeId();
            system.debug('-----'+recTypeId); 
            Case testCase = new Case(subject = 'CSC Test', description = 'CSC_Test_Group', 
                                     Type ='Payroll', 
                                     Sub_Type__c='Tax Error',
                                     AccountId = acc.id,
                                     ContactId = con.Id,
                                     ownerId = usr.Id, 
                                     Case_Issue_Description__c ='test Case Issue Description', 
                                     recordTypeId=recTypeId );       
            
        //Event
        Event events = new Event(subject='Started',ActivityDateTime = system.now(),Talent_Start_Date__c=ord.Start_Date__c);
        EventIdSet.add(events.Id);
        
       test.startTest();        
       	StartDateEvent__e dateevent = New StartDateEvent__e();
        dateevent.Start_Date__c = Date.newInstance(2019, 12, 9);
        dateevent.Comments__c = 'Platform event Comment';
        
       StartDateChangeTriggerHandler.getEventIdListNew(EventIdSet, 'Order');
       StartDateChangeTriggerHandler.getEventIdListNew(EventIdSet, 'Case');
       StartDateChangeTriggerHandler.getEventIdListNew(EventIdSet, 'Abc'); 
       test.stopTest();
    }
    
}