@isTest
public class StartDateChangeTrigger_Test {
    @isTest
    public static void testEvent(){
        
        Test.startTest();
        StartDateEvent__e platformEvent = New StartDateEvent__e();
        platformEvent.SObjectType__c = 'Case';
        platformEvent.Start_Date__c = Date.newInstance(2020, 10, 30);
        platformEvent.Comments__c = 'Platform event comment';
        Database.SaveResult sr = EventBus.publish(platformEvent);
        Test.stopTest();
    }
}