/***************************************************************************
Name        : Strat_Init_Memb_TriggerHandler
Created By  : JFreese (Appirio)
Date        : 23 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : Class that contains all of the functionality called by the
              Strat_Init_Memb_Trigger. All contexts should be in this class.
*****************************************************************************/

public with sharing class Strat_Init_Memb_TriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    Map<Id, Strategic_Initiative_Member__c> oldMap = new Map<Id, Strategic_Initiative_Member__c>();
    Map<Id, Strategic_Initiative_Member__c> newMap = new Map<Id, Strategic_Initiative_Member__c>();


    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Strategic_Initiative_Member__c> newRecs) {
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, Strategic_Initiative_Member__c> newMap) {
        StrategicInitiativeMember_AutofollowReqs(afterInsert, oldMap, newMap);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Strategic_Initiative_Member__c>oldMap,
                               Map<Id, Strategic_Initiative_Member__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Strategic_Initiative_Member__c>oldMap,
                               Map<Id, Strategic_Initiative_Member__c>newMap) {
        StrategicInitiativeMember_AutofollowReqs(afterUpdate, oldMap, newMap);
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Strategic_Initiative_Member__c>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Strategic_Initiative_Member__c>oldMap) {
        StrategicInitiativeMember_AutofollowReqs(afterDelete, oldMap, newMap);
    }

    /************************************************************************************************************
    * Original Header: trigger StrategicInitiativeMember_AutofollowReqs on Strategic_Initiative_Member__c(after insert,after update,after delete)
    Name        :  StrategicInitiativeMember_AutofollowReqs
    Version     : 1.0
    Description :  Trigger to subscribe/unsubscribe SI members to the SI during the creation/deletion.
    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                   Date                   Description
    *-----------------------------------------------------------------------------
    * Pavan                     06/01/2012              Created
    ***************************************************************************************************************/
    //Move declarations outside method so they can be accessed by the two subsequent methods
    Map<string,set<string>> subscriptionInfo = new Map<string,set<string>>();
    Map<string,set<string>> updatedSubscriptions = new Map<string,set<string>>();

    public void StrategicInitiativeMember_AutofollowReqs (String Context,
                                                          Map<Id, Strategic_Initiative_Member__c>oldMap,
                                                          Map<Id, Strategic_Initiative_Member__c>newMap)
    {
        List<Strategic_Initiative_Member__c> TriggerNew = newMap.values();
        List<Strategic_Initiative_Member__c> TriggerOld = oldMap.values();
        if(Context.equals(afterInsert))
        {
            processSubscriptionInfo(TriggerNew, oldMap, Context);
            // create subscriptions accordingly
            autoFollowHelper.addSubscriptions(subscriptionInfo);
        }
        else if(Context.equals(afterUpdate))
        {
            processSubscriptionInfo(TriggerNew, oldMap, Context);
            // create subscriptions accordingly
            if(subscriptionInfo.size() > 0)
                autoFollowHelper.addSubscriptions(subscriptionInfo);
            //delete the older subscriptions
            if(updatedSubscriptions.size() > 0)
                autoFollowHelper.removeSubscriptions(updatedSubscriptions);
        }
        else
        {
            processSubscriptionInfo(TriggerOld, oldMap, Context);
            // remove subscriptions
            autoFollowHelper.removeSubscriptions(subscriptionInfo);
        }
    }

    /*******************************************************************
    Method to populate the subscription info map
    ********************************************************************/
    private void processSubscriptionInfo(List<Strategic_Initiative_Member__c> siTeams, Map<Id, Strategic_Initiative_Member__c>oldMap, String Context)
    {
        for(Strategic_Initiative_Member__c member : siTeams)
        {
            boolean processMember = true;

            if(Context.equals(afterUpdate)) //in case of update add the new and delete the old members
            {
                if(member.User__c != Null && member.User__c != oldMap.get(member.Id).User__c)
                   populateMap(member, oldMap);
                else
                   processMember = false;
            }
            // process the team members accordingly
            if(processMember)
            {
                if(member.User__c != Null)
                {
                    if(subscriptionInfo.get(member.Strategic_Initiative__c) != Null)
                       subscriptionInfo.get(member.Strategic_Initiative__c).add(member.User__c);
                    else
                    {
                       set<string> temp = new set<string>();
                       temp.add(member.User__c);
                       subscriptionInfo.put(member.Strategic_Initiative__c,temp);
                    }
                }
            }
         }
    }
    /***********************************************************************************
    Method to populate the updatedSubscriptions Map
    ***********************************************************************************/
    private void populateMap(Strategic_Initiative_Member__c member, Map<Id, Strategic_Initiative_Member__c>oldMap)
    {
        if(updatedSubscriptions.get(member.Strategic_Initiative__c) != Null)
           updatedSubscriptions.get(member.Strategic_Initiative__c).add(oldMap.get(member.Id).User__c);
        else
        {
           set<string> temp = new set<string>();
           temp.add(oldMap.get(member.Id).User__c);
           updatedSubscriptions.put(member.Strategic_Initiative__c,temp);
        }
    }
}