public with sharing class StrategicPlanLinksController{
@AuraEnabled
public static String getStrategicReportUrl(string spId,string ReportLabel){
     String result='';
     String finalURL='';
      List<Account_Strategic_Initiative__c> accSIList = [select Account__r.Siebel_ID__c from Account_Strategic_Initiative__c Where Strategic_Initiative__c = :spId];
       if (accSIList.size() >0){
           Integer counter =0;
           for(Account_Strategic_Initiative__c aSI : accSIList){
              counter = counter + 1;
              if(accSIList.size() == 1){
                  finalURL = aSI.Account__r.Siebel_ID__c;
               }else{
                  finalURL += aSI.Account__r.Siebel_ID__c; 
                  finalURL = (accSIList.size() == counter) ? finalURL.trim() : finalURL +  ',';
               }
           }
  }
  if(ReportLabel=='Related Account Activities'){
       String reportUrl = String.IsBlank(finalURL) ? finalURL : finalURL.trim();
       result = '/one/one.app#/sObject/00OU00000015Ly7MAE/view?a:t=1487086515856&fv0='+reportUrl;
   }
  return result;
 }
 
 @AuraEnabled
public static String getRelatedAccountsWithReqsUrl(string spId,string ReportLabel){
     String result='';
     String finalURL='';
     string RelatedAccountReqsId='';
   RelatedAccountReqsId=Label.Report_Link_For_Related_Account_Reqs;
      List<Account_Strategic_Initiative__c> accSIList = [select Account__r.Siebel_ID__c from Account_Strategic_Initiative__c Where Strategic_Initiative__c = :spId];
       if (accSIList.size() >0){
           Integer counter =0;
           for(Account_Strategic_Initiative__c aSI : accSIList){
              counter = counter + 1;
              if(accSIList.size() == 1){
                  finalURL = aSI.Account__r.Siebel_ID__c;
               }else{
                  finalURL += aSI.Account__r.Siebel_ID__c; 
                  finalURL = (accSIList.size() == counter) ? finalURL.trim() : finalURL +  ',';
               }
           }
  }
  if(ReportLabel=='Related Account Reqs'){
       String reportUrl = String.IsBlank(finalURL) ? finalURL : finalURL.trim();
       result = '/one/one.app#/sObject/'+RelatedAccountReqsId+'/view?a:t=1487086515856&fv0='+reportUrl;
   }
  return result;
 }
 
 @AuraEnabled
public static String getTeamMembersActivityUrl(string spId,string ReportLabel){
     String result= '';
     String finalURL = '';
      List<Strategic_Initiative_Member__c> teamMemberList = [select User__r.Name from Strategic_Initiative_Member__c Where Strategic_Initiative__c = :spId];
       if (teamMemberList.size() >0){
           String memberList;
           Integer counter =0;
           for(Strategic_Initiative_Member__c s: teamMemberList){
              counter = counter + 1;
              if(teamMemberList.size() == 1){
                  finalURL = s.User__r.Name;
               }else{
                  finalURL += s.User__r.Name; 
                  finalURL = (teamMemberList.size() == counter) ? finalURL.trim() : finalURL +  ',';
               }
           }
  }
  if(ReportLabel=='Team Member Activities'){
       String reportUrl = String.IsBlank(finalURL) ? finalURL : finalURL.trim();
       result = '/one/one.app#/sObject/00OU00000015LyAMAU/view?a:t=1487086515856&fv0='+reportUrl;
   }
  return result;
 }

    @AuraEnabled
public static String getRelatedAccountOppReportUrl(string spId,string ReportLabel){
     String result='';
     String finalURL='';
      List<Account_Strategic_Initiative__c> accSIList = [select Account__r.Siebel_ID__c from Account_Strategic_Initiative__c Where Strategic_Initiative__c = :spId];
       if (accSIList.size() >0){
           Integer counter =0;
           for(Account_Strategic_Initiative__c aSI : accSIList){
              counter = counter + 1;
              if(accSIList.size() == 1){
                  finalURL = aSI.Account__r.Siebel_ID__c;
               }else{
                  finalURL += aSI.Account__r.Siebel_ID__c; 
                  finalURL = (accSIList.size() == counter) ? finalURL.trim() : finalURL +  ',';
               }
           }
  }
  if(ReportLabel=='Related Account Opportunities'){
       String reportUrl = String.IsBlank(finalURL) ? finalURL : finalURL.trim();
       result = '/one/one.app#/sObject/00OU00000015Ly8MAE/view?a:t=1487086515856&fv0='+reportUrl;
   }
  return result;
 } 

    @AuraEnabled
public static String getTeamMembersActivityAtAccountsUrl(string spId,string ReportLabel){
     String result= '';
     String finalAccountURL = '';
     String finalPlanURL = '';
     //Strategic Members Info
      List<Strategic_Initiative_Member__c> teamMemberList = [select User__r.Name from Strategic_Initiative_Member__c Where Strategic_Initiative__c = :spId];
       if (teamMemberList.size() >0){
           String memberList;
           Integer counter =0;
           for(Strategic_Initiative_Member__c s: teamMemberList){
              counter = counter + 1;
              if(teamMemberList.size() == 1){
                  finalPlanURL = s.User__r.Name;
               }else{
                  finalPlanURL += s.User__r.Name; 
                  finalPlanURL = (teamMemberList.size() == counter) ? finalPlanURL.trim() : finalPlanURL +  ',';
               }
            }
       }
      //Accounts Info
      List<Account_Strategic_Initiative__c> accSIList = [select Account__r.Siebel_ID__c from Account_Strategic_Initiative__c Where Strategic_Initiative__c = :spId];
       if (accSIList.size() >0){
           Integer counter =0;
           for(Account_Strategic_Initiative__c aSI : accSIList){
              counter = counter + 1;
              if(accSIList.size() == 1){
                  finalAccountURL = aSI.Account__r.Siebel_ID__c;
               }else{
                  finalAccountURL += aSI.Account__r.Siebel_ID__c; 
                  finalAccountURL = (accSIList.size() == counter) ? finalAccountURL.trim() : finalAccountURL +  ',';
               }
           }
         }
      
      
  if(ReportLabel=='Team Member Activities at Related Accounts'){
       String reportPlanUrl = String.IsBlank(finalPlanURL) ? finalPlanURL : finalPlanURL.trim();
       String reportAccountUrl = String.IsBlank(finalAccountURL) ? finalAccountURL : finalAccountURL.trim();
       result = '/one/one.app#/sObject/00OU0000002rGKnMAM/view?a:t=1487086515856&fv0='+reportPlanUrl +'&fv1='+reportAccountUrl;
   }
  return result;
 }
    
    @AuraEnabled
public static String OppsOwnedURL(string spId,string ReportLabel){
     String result='';
     String finalURL='';
    string ReqsOwnedReportId='';
   ReqsOwnedReportId=Label.Reqs_owned_by_Team_Members_at_Related_Accounts;
   
     String finalAccountURL = '';
     String finalPlanURL = '';
      List<Account_Strategic_Initiative__c> accSIList = [select Account__r.Siebel_ID__c from Account_Strategic_Initiative__c Where Strategic_Initiative__c = :spId];
       if (accSIList.size() >0){
           Integer counter =0;
           for(Account_Strategic_Initiative__c aSI : accSIList){
              counter = counter + 1;
              if(accSIList.size() == 1){
                  finalURL = aSI.Account__r.Siebel_ID__c;
               }else{
                  finalURL += aSI.Account__r.Siebel_ID__c; 
                  finalURL = (accSIList.size() == counter) ? finalURL.trim() : finalURL +  ',';
               }
           }
  }
    
    
     List<Strategic_Initiative_Member__c> teamMemberList = [select User__r.Name from Strategic_Initiative_Member__c Where Strategic_Initiative__c = :spId];
       if (teamMemberList.size() >0){
           String memberList;
           Integer counter =0;
           for(Strategic_Initiative_Member__c s: teamMemberList){
              counter = counter + 1;
              if(teamMemberList.size() == 1){
                  finalPlanURL = s.User__r.Name;
               }else{
                  finalPlanURL += s.User__r.Name; 
                  finalPlanURL = (teamMemberList.size() == counter) ? finalPlanURL.trim() : finalPlanURL +  ',';
               }
            }
       }
  if(ReportLabel=='Opportunities owned by Team Members at Related Account'){
       String reportUrl = String.IsBlank(finalURL) ? finalURL : finalURL.trim();
       String reportmembURL = String.IsBlank(finalPlanURL) ? finalPlanURL : finalPlanURL.trim();
       result = '/one/one.app#/sObject/00OU0000002rGKjMAM/view?a:t=1487086515856&fv0='+reportmembURL+'&fv1='+reportUrl;
   }
    
    if(ReportLabel=='Reqs owned by Team Members at Related Accounts'){
       String reportUrl = String.IsBlank(finalURL) ? finalURL : finalURL.trim();
       String reportmembURL = String.IsBlank(finalPlanURL) ? finalPlanURL : finalPlanURL.trim();
       result = '/one/one.app#/sObject/'+ReqsOwnedReportId+'/view?a:t=1487086515856&fv0='+reportmembURL+'&fv1='+reportUrl;
   }
    
    
  return result;
 }
 
}