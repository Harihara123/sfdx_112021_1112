/*******************************************************************
Name        : StrategicPlanTriggerHandler
Created By  : Nidish Rekulapalli
Date        : 10 July 2015
Purpose     : This trigger is invoked for all contexts
              and delegates control to StrategicPlanTriggerHandler
********************************************************************/
public with sharing class StrategicPlanTriggerHandler {
String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    String yes = 'Yes';
    String No = 'No';
    Map<Id, Strategic_Initiative__c> oldMap = new Map<Id, Strategic_Initiative__c>();
    Map<Id, Strategic_Initiative__c> newMap = new Map<Id, Strategic_Initiative__c>();
     //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Strategic_Initiative__c> StratPlanRec) {
    
  
        
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Strategic_Initiative__c> newRecords) {
        populatePrimaryAccount(afterInsert,oldMap,newMap,newRecords);
       
      
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Strategic_Initiative__c>oldMap, Map<Id, Strategic_Initiative__c>newMap) {
       AccountPrioritychange(oldmap,newmap);
        
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Strategic_Initiative__c>oldMap, Map<Id, Strategic_Initiative__c>newMap) {
        List<Strategic_Initiative__c> newRecords = newMap.values();
        populatePrimaryAccount(afterInsert,oldMap,newMap,newRecords); 
       
        
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Strategic_Initiative__c>oldMap) {
       
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Strategic_Initiative__c>oldMap) {

    }


   public void AccountPrioritychange(Map<Id, Strategic_Initiative__c>oldMap, Map<Id, Strategic_Initiative__c>newMap){
       for(Strategic_Initiative__c SI :newmap.values()){
           
           if(si.Account_Prioritization__C !=oldmap.get(si.id).Account_Prioritization__C){
               SI.Priority_Changed_Date__c=date.today();
               
           }
       }
               
           }
             
        
    



    //-------------------------------------------------------------------------
    // On after insert, after update trigger method to add Primary account to Account_Strategic_Initiative__c junction object when Primary account is updated on AP
    //-------------------------------------------------------------------------
    public void populatePrimaryAccount(String Context, Map<Id, Strategic_Initiative__c>oldMap, Map<Id, Strategic_Initiative__c>newMap,List<Strategic_Initiative__c> newRecords) {
        List<Account_Strategic_Initiative__c> AccountStrategicPlanRecords = new List<Account_Strategic_Initiative__c>();
        List<Strategic_Initiative__c> SPFiltered = new List<Strategic_Initiative__c>();
        List<Log__c> errorLogs = new List<Log__c>();
        Map<string,Account_Strategic_Initiative__c> existingRecords = new Map<string,Account_Strategic_Initiative__c>();
        set<string> accountIds = new set<string>();
        set<string> SPids = new set<string>();  
         string recordtypePartnershipPlanId = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Partnership Plan').getRecordTypeId();
       
                    // Loop over the contacts
                    for(Strategic_Initiative__c SP : newRecords)
                    {
                        if(SP.recordTypeId != recordtypePartnershipPlanId)
                        {
                       boolean addToList = true;                      
                       if(Context.equals(afterUpdate)){
                             if(SP.Primary_Account__c == oldMap.get(SP.Id).Primary_Account__c)
                                 addToList = false;                               
                       }
                       if(addToList){
                           accountIds.add(SP.Primary_Account__c);
                           SPFiltered.add(SP);
                           }
                       }
                    }
                
                //Query for associated records and update them accordingly
                if(accountIds.size() > 0){
                   for(Account_Strategic_Initiative__c rec : [SELECT Name, Account__c, CreatedById, Created_Using_Code__c, CreatedDate, IsDeleted, LastModifiedById, LastModifiedDate, Primary__c, Id, Strategic_Initiative__c, SystemModstamp, Unique_Identifier__c FROM Account_Strategic_Initiative__c where Account__c in :accountIds and Strategic_Initiative__c in :newMap.keyset()])
                     existingRecords.put(rec.Account__c+'_'+rec.Strategic_Initiative__c,rec);
                }
                //Loop over the filtered SPs
                for(Strategic_Initiative__c SP : SPFiltered){
                   Account_Strategic_Initiative__c associationRecord = new Account_Strategic_Initiative__c();
                   if(existingRecords.get(SP.Primary_Account__c+'_'+SP.Id) != Null)
                       associationRecord = existingRecords.get(SP.Primary_Account__c+'_'+SP.Id).clone(true);
                   else{
                       associationRecord.Account__c = SP.Primary_Account__c;                  //assign the Account Id value to new record
                       associationRecord.Strategic_Initiative__c = SP.id;
                   }
                   associationRecord.Primary__c = True;                         //Check the IsPrimary Checkbox
                   associationRecord.Created_Using_Code__c = True;              //Check the hidden field
                   AccountStrategicPlanRecords.add(associationRecord);
                }

                //insert the records accordingly
                if(AccountStrategicPlanRecords.size() > 0){
                    //set the boolean variable
                    for(database.upsertResult result : database.upsert(AccountStrategicPlanRecords,false)){
                        // insert the log record accordingly
                        if(!result.isSuccess()){
                             errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                        }
                    }
                }
                 // insert error logs
                if(errorLogs.size() > 0)
                    database.insert(errorLogs,false);
              
    } 

}