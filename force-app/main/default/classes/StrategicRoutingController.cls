// in  controller
public with sharing class StrategicRoutingController {
    public StrategicRoutingController(ApexPages.StandardController controller){
    }
    public pagereference getRouter(){
        map<string,Routing_Controller_Settings__c> RoutingControllerSettings = Routing_Controller_Settings__c.getAll();
        String PrimaryAccountID_FieldID = RoutingControllerSettings.get('PrimaryAccountID').FieldID__c;
        String PrimaryAccountName_FieldID = RoutingControllerSettings.get('PrimaryAccountName').FieldID__c;
        String ParentAccountPlanName_FieldID = RoutingControllerSettings.get('ParentAccountPlanName').FieldID__c;
        String ParentAccountPlanID_FieldID = RoutingControllerSettings.get('ParentAccountPlanID').FieldID__c;
        String ParentStrategicInitiativeName_FieldID = RoutingControllerSettings.get('ParentStrategicInitiativeName').FieldID__c;
        String ParentStrategicInitiativeID_FieldID = RoutingControllerSettings.get('ParentStrategicInitiativeID').FieldID__c;
        String GovernmentContractVehicleName_FieldID = RoutingControllerSettings.get('GovernmentContractVehicleName').FieldID__c;
        String GovernmentContractVehicleID_FieldID = RoutingControllerSettings.get('GovernmentContractVehicleID').FieldID__c;
        String OfficeName_FieldID = RoutingControllerSettings.get('OfficeName').FieldID__c;
        User usr = [Select Id,Office__c,OPCO__c FROM User Where Id=: userinfo.getuserid()];
        system.debug('********************'+ApexPages.CurrentPage());
        String chosenRT = ApexPages.CurrentPage().GetParameters().Get('RecordType');
        String PrimaryAccountID = ApexPages.CurrentPage().GetParameters().Get(PrimaryAccountID_FieldID);
        String PrimaryAccountName = ApexPages.CurrentPage().GetParameters().Get(PrimaryAccountName_FieldID);
        String ParentAP_id = ApexPages.CurrentPage().GetParameters().Get(ParentAccountPlanID_FieldID);
        String ParentSI_id = ApexPages.CurrentPage().GetParameters().Get(ParentStrategicInitiativeID_FieldID);
        String ParentGCV_id = ApexPages.CurrentPage().GetParameters().Get(GovernmentContractVehicleID_FieldID);
        String parentStrat = '';
        String parentStrategicID = '';
        String OpCo = '';
        String Office = ''; 
        // String ReturnURL = '%2Fa1X%3Ffcf%3D00BU00000021J2I%26rolodexIndex%3D-1%26page%3D1';
        String ReturnURL = '/a1X/o';
        String Opco_Mapping;
        OPCO_Picklist_Mapping__c OPCO_Mapping_Setting = OPCO_Picklist_Mapping__c.getValues(usr.OPCO__c);    
        if(OPCO_Mapping_Setting <> Null)
        Opco_Mapping = OPCO_Mapping_Setting.Picklist_Value__c;           
        System.debug('Parent_Strategic_Initiative__c=='+parentStrategicID);
        String Url ='';
        if(ParentAP_id <> Null && ParentAP_id <> ''){
        parentStrategicID = ParentAP_id;
        }else if(ParentSI_id <> Null && ParentSI_id <> ''){
        parentStrategicID = ParentSI_id;
        }else if(ParentGCV_id <> Null && ParentGCV_id <> ''){
        parentStrategicID = ParentGCV_id;
        }
        System.debug('*********ParentSI_id *********'+ParentSI_id );
        System.debug('*********ParentAP_id *********'+ParentAP_id );
        System.debug('*********ParentGCV_id *********'+ParentGCV_id);
        System.debug('*********parentStrategicID *********'+parentStrategicID );
        If(Opco_Mapping != '' && Opco_Mapping != null){
        OpCo ='&00NU00000035KOp='+EncodingUtil.urlEncode(Opco_Mapping,'UTF-8');
        }  
        If(usr.Office__c != '' && usr.Office__c != null){
        Office ='&'+OfficeName_FieldID+'='+EncodingUtil.urlEncode(usr.Office__c,'UTF-8');
        }
     
   If((parentStrategicID != '' && parentStrategicID != null) && parentStrategicID.contains('a1X') && parentStrategicID.length()>=15 && !parentStrategicID.contains('a1X?')){
        parentStrategicID = parentStrategicID.replace('/', '');
        Strategic_Initiative__c stratName;
        try{
        stratName = [Select Id, Name,recordType.Name From Strategic_Initiative__c Where Id =: parentStrategicID LIMIT 1];
        } catch(Exception ex){}
        if(stratName <> null){
        if(stratName.recordType.Name =='Account Plan' )
            parentStrat = '&'+ParentAccountPlanName_FieldID+'='+EncodingUtil.urlEncode(stratName.Name,'UTF-8')+'&'+ParentAccountPlanID_FieldID+'='+parentStrategicID+'&scontrolCaching=1'; //&retURL='+parentStrategicID;
           else
           if(stratName.recordType.Name =='Strategic Initiative' )
               parentStrat ='&'+ParentStrategicInitiativeName_FieldID+'='+EncodingUtil.urlEncode(stratName.Name,'UTF-8')+'&'+ParentStrategicInitiativeID_FieldID+'='+parentStrategicID+'&scontrolCaching=1'; //&retURL='+parentStrategicID;  
           else
           if(stratName.recordType.Name =='Government Contract Vehicle' )
               parentStrat ='&'+GovernmentContractVehicleName_FieldID+'='+EncodingUtil.urlEncode(stratName.Name,'UTF-8')+'&'+GovernmentContractVehicleID_FieldID+'='+parentStrategicID+'&scontrolCaching=1'; //&retURL='+parentStrategicID;  
          }
           ReturnURL =  parentStrategicID;
            //
        //parentStrat = '&CF00NJ0000001tdZx='+stratName;
    }  
    
    
  If(PrimaryAccountID<>NULL && PrimaryAccountID <>''){
    parentStrat = '&'+PrimaryAccountName_FieldID+'='+EncodingUtil.urlEncode(PrimaryAccountName,'UTF-8')+'&'+PrimaryAccountID_FieldID+'='+PrimaryAccountID;
    ReturnURL =  PrimaryAccountID;
    }   
 
     if(chosenRT <> null){   
         Url ='/a1X/e?retURL='+ReturnUrl+
         '&RecordType='+chosenRT +
         OpCo +
         Office +
         parentStrat+
        '&ent=01IU0000000FsiE&nooverride=1';
     }else{
        Url ='/a1X/e?retURL='+ReturnUrl+
         '&'+
         OpCo +
         Office +
         parentStrat+
        '&ent=01IU0000000FsiE&nooverride=1';
     }  
        PageReference p ;   
        p = new PageReference(Url);
        return p;
  }
}