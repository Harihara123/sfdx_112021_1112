public class SubmittalCardController  {

    public ApexPages.StandardSetController standardSetController {get;set;}
    public Boolean showButton {get;set;}
    public Boolean hasRecords {get;set;}
    public List<Order> submittalList = new List<Order>();
    private Integer size;

    public SubmittalCardController(ApexPages.StandardController ctrl, Integer size) {
        this.size = size;

        this.standardSetController = new ApexPages.StandardSetController(
            Database.getQueryLocator([SELECT Id, ShipToContactId, RecordTypeId, CreatedById, Account.Name,ATS_Job__r.Account_Name__c, ATS_Job__r.Account_City__c, ATS_Job__r.Account_State__c, ATS_Job__r.Job_Title__c, Opportunity.Account.Name, Opportunity.Account_City__c, Opportunity.Account_State__c, Opportunity.Req_Job_Title__c, Opportunity.Req_Client_Job_Title__r.Name,Opportunity.RecordType.Name, Display_LastModifiedDate__c, Status, Opportunity.OpCo__c, Opportunity.Req_OpCo_Code__c, Opportunity.Req_Product__c, Opportunity.Currency__c, Opportunity.Req_Total_Filled__c, Opportunity.Req_Open_Positions__c, Opportunity.Req_Total_Positions__c  
            FROM Order WHERE ShipToContactId = :ApexPages.currentPage().getParameters().get('id')
            AND RecordType.DeveloperName='OpportunitySubmission' 
            AND status != 'Applicant' 
            ORDER BY Display_LastModifiedDate__c DESC]));

        this.standardSetController.setPageSize(size);
        this.submittalList = this.getOpportunities().clone();
        this.showButton = this.getHasNextPage();
        this.hasRecords = this.getNumberOfRecords() > 0;
    }

    public SubmittalCardController(ApexPages.StandardController ctrl) {
        this(ctrl, 10);
    }

    public List<Order> getRecords() {
        return this.submittalList;
    }

    public List<Order> getOpportunities() {
        return (List<Order>) this.standardSetController.getRecords();
    }

    public void getNextPage() {
        this.standardSetController.next();
        this.submittalList.addAll((List<Order>) this.standardSetController.getRecords());
        this.showButton = this.getHasNextPage();
    }

    public Integer getNumberOfRecords() {
        return this.standardSetController.getResultSize();
    }

    public Boolean getHasNextPage() {
        return this.standardSetController.getHasNext();
    }
}