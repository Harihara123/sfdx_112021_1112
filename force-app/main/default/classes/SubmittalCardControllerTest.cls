@isTest
public class SubmittalCardControllerTest  {

    @TestSetup
    static void testSetup() {
		
		insert new DRZSettings__c(Name = 'OpCo', Value__c='Aerotek, Inc;TEKsystems, Inc.');
        Submittal_Status_Matrix__mdt ssm = new Submittal_Status_Matrix__mdt();
        List<String> lst = SubmittalStatusController.getAvailableStatuses('Allegis Global Solutions, Inc.', 'Applicant', True);
        
        List<Account> accList = new List<Account>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;

        Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
        ct.Other_Email__c = 'other0@testemail.com';
        ct.Title = 'test title';
        ct.MailingState = 'test text';

        insert ct;

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = accList[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp1;

        Opportunity opp2 = new Opportunity();
        opp2.Name = 'TESTOPP2';
        opp2.StageName = 'Offer Accepted';
        opp2.AccountId = accList[0].id;
        opp2.Req_Product__c = 'Permanent';
        opp2.CloseDate = System.today();
        opp2.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp2;

        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;

        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = ct.Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Linked';
        o.EffectiveDate = System.today();
        o.RecordTypeId = recTypeId;
        insert o;

        Order o2 = new Order();
        o2.Name = 'TestOrder';
        o2.AccountId = accList[0].id;
        o2.ShipToContactId = ct.Id;
        o2.OpportunityId = opp2.Id;
        o2.Status = 'Linked';
        o2.EffectiveDate = System.today();
        o2.RecordTypeId = recTypeId;
        insert o2;

        String orderStrg = JSON.serialize(o);
        
        DateTime startDateTime = Datetime.now();
        DateTime endDateTime = startDateTime.addHours(1);
        
        Event evt = new Event();
        evt.StartDateTime = Datetime.now();
        evt.EndDateTime = Datetime.now().addHours(1);
        evt.Submittal_Interviewers__c = 'AKR';
        evt.Activity_Type__c = 'Interviewing';
        evt.WhoId = ct.Id;
        evt.WhatId = o.Id;
        insert evt;

    }

    @isTest
    public static void constructor_givenStandardController_shouldConstruct() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        SubmittalCardController ctrl;

        Test.startTest();
            ctrl = new SubmittalCardController(new ApexPages.StandardController(new Contact()));
        Test.stopTest();

        System.assertNotEquals(null, ctrl, 'The Controller should have been instantiated.');
    }

    @isTest
    public static void getNumberOfRecords_givenTwoInsertedSubmittals_shouldReturnTotalNumberOfSubmittals() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Contact ct = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];

        PageReference vfPage = Page.SubmittalsMobileCard;
        Test.setCurrentPage(vfPage);
        ApexPages.currentPage().getParameters().put('id', ct.Id);

        SubmittalCardController ctrl = new SubmittalCardController(new ApexPages.StandardController(ct));

        ctrl.standardSetController.setPageSize(1);
        Integer setTotal;

        Test.startTest();
            setTotal = ctrl.getNumberOfRecords();
        Test.stopTest();

        System.assertEquals(2, setTotal, 'The total number of records should have been returned.');
    }

    @isTest
    public static void getRecords_givenTwoInsertedSubmittals_shouldReturnListOfOneSubmittal() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Contact con = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];

        PageReference vfPage = Page.SubmittalsMobileCard;
        Test.setCurrentPage(vfPage);
        ApexPages.currentPage().getParameters().put('id', con.Id);

        SubmittalCardController ctrl = new SubmittalCardController(new ApexPages.StandardController(con), 1);
        List<Order> recordList;

        Test.startTest();
            recordList = ctrl.getRecords();
        Test.stopTest();

        System.assertEquals(1, recordList.size(), 'The first page of records should have been returned.');
    }

    @isTest
    public static void getHasNextPage_givenTwoInsertedSubmittalsWithPageSize1_shouldReturnTrue() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Contact con = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];

        PageReference vfPage = Page.SubmittalsMobileCard;
        Test.setCurrentPage(vfPage);
        ApexPages.currentPage().getParameters().put('id', con.Id);

        SubmittalCardController ctrl = new SubmittalCardController(new ApexPages.StandardController(con), 1);
        Boolean hasNext = false;

        Test.startTest();
            hasNext = ctrl.getHasNextPage();
        Test.stopTest();

        System.assert(hasNext, 'The first page of records should have been returned.');
    }

    @isTest
    public static void getNextPage_givenTwoInsertedSubmittalsWithPageSize1_shouldReturnSecondRecord() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Contact con = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];

        PageReference vfPage = Page.SubmittalsMobileCard;
        Test.setCurrentPage(vfPage);
        ApexPages.currentPage().getParameters().put('id', con.Id);

        SubmittalCardController ctrl = new SubmittalCardController(new ApexPages.StandardController(con), 1);
        List<Order> pageOne;
        List<Order> pageTwo;

        Test.startTest();
            pageOne = ctrl.getRecords().clone();
            ctrl.getNextPage();
            pageTwo = ctrl.getRecords().clone();
        Test.stopTest();

        System.assertEquals(1, pageOne.size(), 'The first page of records should have been returned.');
        System.assertEquals(2, pageTwo.size(), 'The first and second page of records should have been returned.');
    }
}