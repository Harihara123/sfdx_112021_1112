public class SubmittalChartController {

@AuraEnabled
 public static String getchartJSON(string oppId){
     list<DataWrapper> chartData = new list<DataWrapper>();
     Boolean hasStarts = false;
     List<Order> ords = new List<Order>();
     Integer npval = 0;
     
    List<aggregateResult> results = [select Status, Order.OpportunityId, COUNT(Order.Opportunity.Name) from Order 
                                                  where Order.OpportunityId = :oppId
                                                  AND Recordtype.DeveloperName='OpportunitySubmission'
                                                  group by Status, Order.OpportunityId
                                                  having COUNT(Order.Opportunity.Name) > 0];
     
     if(results.size() > 0){
       for (AggregateResult ar : results){
               
           DataWrapper dw = new DataWrapper();
           
           if(String.Valueof(ar.get('Status')).left(14) == 'Not Proceeding'){
               npval += (Integer)ar.get('expr0');
           }
           else{
               dw.status = String.Valueof(ar.get('Status'));
               dw.count = (Integer)ar.get('expr0');
               chartData.add(dw);               
           }
           
		   if(dw.status == 'Started'){
               hasStarts = true;
           }
          }
         
         if(npval > 0 ){
         	DataWrapper dwnp = new DataWrapper();
         	dwnp.Status = 'Not Proceeding';
         	dwnp.count = npval;
         	chartdata.add(dwnp);
         }
         
         if(chartData.size() > 0){
             Id userId = UserInfo.getUserId();
             List<User> usrList = [SELECT OPCO__c,CompanyName FROM User WHERE Id = :userId LIMIT 1];
             chartdata[0].opco = usrList[0].CompanyName;
         }
         
         if(hasStarts){
             ords = [SELECT ShipToContact.Id, LastModifiedDate, ShipToContact.Name
                                  FROM Order where OpportunityId =:oppId and status = 'Started'];
             if(ords.size() > 0 && chartData.size() > 0){
                 chartData[0].stOrds = ords;
             }
         }
     }
     
     return System.json.serialize(chartData);
 }
    
  class DataWrapper{
       @AuraEnabled
       public String status;
       @AuraEnabled
       public integer count;
       @AuraEnabled
       public List<Order> stOrds {get;set;}
       @AuraEnabled
       public String opco;
   }
    
   
}