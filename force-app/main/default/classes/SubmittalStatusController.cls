public without sharing  class SubmittalStatusController  {
    @AuraEnabled
    @TestVisible public static List<String> getAvailableStatuses(String opco, String status, Boolean isAtsJob) {
        List<String> lst = new List<String>();

        List<Submittal_Status_Matrix__mdt> subStatuses;
        if (isAtsJob) {
            // Query for ATS_Job__c
            subStatuses = [SELECT Id,Label,OpCo__c,Business_Unit__c,Req_Product__c,Current_Status__c,Available_Status__c 
                            FROM Submittal_Status_Matrix__mdt 
                            WHERE Current_Status__c =: status and OpCo_Id__c = 'AGS'];
        } else {
            subStatuses = [SELECT Id,Label,OpCo__c,Business_Unit__c,Req_Product__c,Current_Status__c,Available_Status__c 
                            FROM Submittal_Status_Matrix__mdt 
                            WHERE Current_Status__c =: status and OpCo__c =: opco];
        }

        if (subStatuses.size() > 0) {
            String targets = subStatuses[0].Available_Status__c;
            if (targets.length() > 0) {
                String formattedStr = targets.substring(1, targets.length() - 1);
                List<String> lstStatuses = formattedStr.split(',');
                for(String s : lstStatuses) {
                    lst.add(s);          
                }
            }
        }

        return lst;
    }

    @AuraEnabled
    public static List<String> getSubmittalMassUpdate(List<String> subIds) {
        List<String> statuses =  new List<String> ();
        statuses = ActionsForSkuidMassUpdate.invokeApexAction(subIds); 
        return statuses;
    }

    @AuraEnabled
    public static String saveSubmittalMassUpdate(List<String> subIds,String newStatus,String npReason) {
        Map<Map<String,String>,Map<String,String>> matrix = new Map<Map<String,String>,Map<String,String>>();
        List<Order> saveOrders = new List<Order> ();
        if(String.isBlank(npReason)){
            for (String sid : subIds){
             saveOrders.add(new Order(Id = Id.valueOf(sid),Status = newStatus));
            }
        }else{
            for (String sid : subIds){
             saveOrders.add(new Order(Id = Id.valueOf(sid),Status = newStatus,Submittal_Not_Proceeding_Reason__c = npReason));
            }
        }
         
        
        try{
            update saveOrders;
            return 'SUCCESS';
        }catch(Exception e){
            return 'Exception';
        }
        
        
    }

    @AuraEnabled
    public static String saveSubmittalStatusUpdate(String submittalId, String status,String submittalEventId, String dispCode, String dispReason) {
        Order submittal = [SELECT Id, Status
                                FROM Order 
                                WHERE Id =: submittalId];

        submittal.Status = status;
        update submittal;
        // Updated corresponding event's dispositino details as applicable - S-106221
        if(submittalEventId != null){
            try{
                Event event = [Select Id,Disposition_Code__c, Disposition_Reason__c from Event where id=: submittalEventId];
                event.Disposition_Code__c = dispCode;
                event.Disposition_Reason__c = dispReason;
                update event;
            }catch(Exception ex){}
        }
        return submittal.Id;
    }

    @AuraEnabled
    public static String saveSubmittalInterview(String intEvent, String submittalId/*, Boolean isEditInt*/) {
        Event e = (Event)JSON.deserialize(intEvent, Event.class);
       
        
        /*if(!isEditInt){
            Order o = [Select Id, Status from Order where Id =: submittalId];
            o.Status = 'Interviewing';
            update o;
        }*/

        Order o = [Select Id, Status from Order where Id =: submittalId];
        if(o.Status != 'Interviewing'){
            o.Status = 'Interviewing';
            update o;
        }
        
        upsert e;   // Changed position by Salesforce Support Case #26764731
        return e.Id;
    }

    @AuraEnabled
    public static String saveSubmittalOfferAccepted(String subOffAcctd, Boolean radioLabel, String submittalId, String opptyId, 
                                                    Boolean clientCont, String contId) {
        Order o = (Order)JSON.deserialize(subOffAcctd, Order.class);
        upsert o;

		 if(radioLabel){
            List<Order> ords = new List<Order>();
            ords = [Select Id, Status from Order where OpportunityId =: opptyId and Id !=: submittalId and Status NOT IN ('Started','Offer Accepted','Linked','Applicant')];
            System.debug('Orders@@@'+ords);
           //Commenting out as logic is handled in ordertriggerhandler
           /*
            if(ords.size() > 0){
                for(Order ord: ords){
                    if(radioLabel)
                    {
                        ord.MovetoNP_Flag__c = false;
                    }
                    else
                    {
                        ord.MovetoNP_Flag__c = true;
                    }
                }
            }
            */
            TriggerStopper.opportunityAggregateTotalStopper=false;
            update ords;
        }
        
        if(clientCont){
            String recTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();

            Contact c = [Select Id, LastName, FirstName, AccountId, RecordTypeID, OtherPhone, Phone, MobilePhone, 
                        Email, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, MiddleName, LinkedIn_URL__c,
                        IsAllegisPlacedContact__c, IsPlacedFromSkuid__c, LeadSource from Contact where Id =: contId];

            // Create client contact by having Opp accoutn id as the account id on the contact.
            Opportunity oppRec = [SELECT AccountId FROM Opportunity WHERE Id =: opptyId];
            
            try{
                Contact con = new Contact();
                con.FirstName = c.FirstName;
                con.LastName = c.LastName;
                con.AccountId = oppRec.AccountId;//c.AccountId;
                con.RecordTypeId = recTypeID;
                con.Related_Contact__c = c.Id;
                con.OtherPhone = c.OtherPhone;
                con.Phone = c.Phone;
                con.MobilePhone = c.MobilePhone;
                con.Email = c.Email;
                con.MailingStreet = c.MailingStreet;
                con.MailingCity = c.MailingCity;
                con.MailingState = c.MailingState;
                con.MailingPostalCode = c.MailingPostalCode;
                con.MailingCountry = c.MailingCountry;
                con.MiddleName = c.MiddleName;
                con.LinkedIn_URL__c = c.LinkedIn_URL__c;
                con.IsAllegisPlacedContact__c = c.IsAllegisPlacedContact__c;
                con.IsPlacedFromSkuid__c = c.IsPlacedFromSkuid__c;
                con.LeadSource = c.LeadSource;
            
                insert con;
            }
            catch(DMLException e){
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
        }

        return o.Id;
    }

    /*@AuraEnabled
    public static Event queryInterviewEvent(String contactId, String submittalId) {
        Event evt = [SELECT Id, StartDateTime, EndDateTime, Submittal_Interviewers__c, Interview_Type__c, 
                        Interview_Status__c, Description FROM Event WHERE WhoId =: contactId 
                        AND WhatId =: submittalId and Type = 'Interviewing' ORDER BY CreatedDate DESC Limit 1];
        System.debug('evt@@@'+evt);
        return evt;
    }*/

    @AuraEnabled
    public static List<Event> queryInterviewEvents(String contactId, String submittalId) {
        List<Event> evts = [SELECT Id, StartDateTime, EndDateTime, Submittal_Interviewers__c, Interview_Type__c, 
                        Interview_Status__c, Description FROM Event WHERE WhoId =: contactId 
                        AND WhatId =: submittalId and Type = 'Interviewing' ORDER BY CreatedDate DESC];
        System.debug('evts@@@'+evts);
        return evts;
    }

    @AuraEnabled
    public static Order querySubmittal(String submittalId) {
        System.debug('submittalId for Order---->'+submittalId);
        Order o = [SELECT Id, ShipToContactId, ShipToContact.Related_Contact__c, Opportunity.Req_OFCCP_Required__c, Has_Application__c, Submittal_Placement_Source__c, Bill_Rate__c, 
                    Pay_Rate__c, Burden__c, Rate_Frequency__c, Salary__c, Bonus__c, Additional_Compensation__c, Opportunity.Currency__c,
                    Opportunity.Req_Total_Filled__c, Opportunity.Req_Open_Positions__c, Opportunity.Req_Total_Positions__c,Opportunity.Req_Total_Lost__c ,
                    Opportunity.AccountId, Opportunity.Req_OpCo_Code__c ,Opportunity.OpCo__c,Opportunity.Req_Total_Washed__c 
                    FROM Order WHERE Id =: submittalId];
        System.debug('Order@@@'+o);
        return o;
    }

    @AuraEnabled
    public static String removeLink(String submittalId) {
        Order o = [SELECT Id, Status from Order where Id =: submittalId];
        Event e = getApplicantEventRemoveLink(submittalId);

        if (e != null) {
            o.Status = 'Applicant';
            update o;
        } else {
            delete o;
        }

        return 'SUCCESS';
    }

	private static Event getApplicantEventRemoveLink(String submittalId) {
        Event[] e = [SELECT Id, WhatId FROM Event WHERE Job_Posting__c != null AND WhatId =: submittalId LIMIT 1];
        return (e != null && e.size() > 0) ? e[0] : null;
    }

    @AuraEnabled
    public static String updateTotalPositions(String opId){
        
        Opportunity op=[select id,Req_Total_Positions__c from Opportunity where id=:opId limit 1];
        Decimal posi=op.Req_Total_Positions__c+1;
        op.Req_Total_Positions__c=posi;
        op.Apex_Context__c=true;
        update op;
        return 'SUCCESS';
    }
    
    @AuraEnabled
    public static Event getApplicantEvent(String submittalId) {
        Event[] e = [SELECT Id, WhatId FROM Event WHERE Type='Applicant' AND WhatId =: submittalId LIMIT 1];
        return (e != null && e.size() > 0) ? e[0] : null;
    }

    @AuraEnabled
    public static String removeApplicant(String submittalId) {
        Order o = [SELECT Id, Status from Order where Id =: submittalId];
        delete o;

        return 'SUCCESS';
    }

    /**
        This method will save email request records
    **/
    @AuraEnabled
    public static String saveEmailRecords(String submittals) {
		map<string, string> EmailRecords = new map<string, string>();
        Integer errCount = 0;
        String saveStatus = 'SUCCESS';
        try{
            
            List<MCEmail> subRecords = (List<MCEmail>)System.JSON.deserializeStrict(submittals,List<MCEmail>.Class);
            List<MC_Email__c> mcEmails = new List<MC_Email__c>();
            for(MCEmail submital : subRecords){        
				EmailRecords.put(submital.submittalUniqueId, submital.emailTemplateName);
                mcEmails.add(new MC_Email__c(ContactID__c=submital.contactId,
                Salutation__c=submital.salutation,
                FirstName__c=submital.firstName,
                LastName__c=submital.lastName,
                PostingId__c=submital.jobPostingId,
                ApplicationDate__c=Date.valueOf(submital.applicationDate),
                PostingJobTitle__c=submital.jobPostingTitle,
                //ResponseEmailID__c=submital.,
                UserID__c=submital.userId,
                UserFullName__c=submital.userFullName,//submital.firstName+submital.lastName,
                UserEmail__c=submital.userEmail,
                UserTitle__c=submital.userTitle,
                UserPhone__c=submital.userPhone,
                OpCo__c=submital.opco,
                Contactemail__c=submital.contactEmail,
                Contactmobile__c=submital.contactMobile,
                ContactState__c=submital.contactState,
                ContactCountry__c=submital.contactCountry,
                ResponseEmailID__c=submital.emailTemplateName,
                //ContactLanguage__c=submital.jpOrder.ShipToContact.,
                UniqueID__c=submital.contactId+submital.emailTemplateName+submital.jobPostingId+submital.applicationDate));
            }
             System.debug('mcEmails inserted account.  ID: ' +mcEmails);
            Database.UpsertResult[] saveResultList = Database.upsert(mcEmails,MC_Email__c.UniqueID__c,false);
          
            for (Database.UpsertResult sr : saveResultList) {
                if (sr.isSuccess()) {
                    System.debug('Successfully inserted account.  ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        errCount++;
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug(' fields that affected this error: ' + err.getFields());
                    }
                }
            }
         if(errCount > 0){
            saveStatus = errCount + ' Talent have previously received the chosen response email and will not be sent a duplicate message. All other selections have been successfully sent.' ;
         }
        }catch(Exception exp){
            System.debug('saveEmailRecords ' + exp.getMessage());
            saveStatus = 'FAIL' ;
        }
		if(saveStatus == 'SUCCESS')
            updateSubmittalwithEmail(EmailRecords);
        return saveStatus;
    }
    /**
        This method will push disposition applicaiton and reasons to RWS
    **/


    @AuraEnabled
    public static List<Disposition_Matrix__mdt> getDispositionMatrix(){
     List<Disposition_Matrix__mdt> disMatrix = new List<Disposition_Matrix__mdt>([Select Disposition_Code_Label__c,Disposition_Code_Value__c,Disposition_Reason_Label__c,Disposition_Reason_Value__c from Disposition_Matrix__mdt ORDER BY Disposition_Code_Value__c]);

     return disMatrix;


    }

    @AuraEnabled
    //public static String saveMassDisposition(List<Id> AppEventIds,String Code,String Reason){
    public static String saveMassDisposition(String submittals,String Code,String Reason){

    String  saveStatus = 'ERROR';
    try{
        List<MCEmail> subRecords = (List<MCEmail>)System.JSON.deserializeStrict(submittals,List<MCEmail>.Class);
        Map<String,Object> dispositionData = new Map<String,Object>();
        List<Map<String,Object>> applications = new List<Map<String,Object>>();
        User user = [Select Peoplesoft_Id__c from User where id=:UserInfo.getUserId()];   
        dispositionData.put('hrEmplId',user.Peoplesoft_Id__c);

        List<event> appevents = new List<Event>();
        for(MCEmail submital : subRecords){
            appevents.add(new Event(Id = submital.eventId,Disposition_Code__c = Code,Disposition_Reason__c = Reason));

            Map<String,String> application = new Map<String,String>();
            application.put('vendorApplicationId',submital.applicationId);
            application.put('vendorId',submital.vendorId);
            applications.add(application); 
        }
        dispositionData.put('dispositionCode',Code);
        dispositionData.put('dispositionReasonCode',Reason);
        dispositionData.put('applicationIdList',applications);
        /* // commented as we have new story to handle this via OutBound Message S-107216
        try{
            // We will handle pushtoRWS in future method.
            String rwsResp = sendToRWS(dispositionData);
            Map<String, Object> rwsRespMap = (Map<String, Object>) JSON.deserializeUntyped(rwsResp);
            String responseMsg = String.valueOf(rwsRespMap.get('responseMsg'));
        }catch(Exception ex){
             System.debug('RWS could not save dispositions ' + ex.getMessage());
        }*/
        Database.update(appevents);
        saveStatus = 'SUCCESS';                          
    }catch(Exception e){
        System.debug('saveMassDisposition ' + e.getMessage());
        saveStatus = e.getMessage();
    }
    return saveStatus;
    }    
    @AuraEnabled
    public static String pushDispositionDetailsToRWS(String submittals,String disposCode, String disposReason){
        String saveStatus = 'SUCCESS';
        try{
            
            List<MCEmail> subRecords = (List<MCEmail>)System.JSON.deserializeStrict(submittals,List<MCEmail>.Class);
            Map<String,Object> dispositionData = new Map<String,Object>();
            List<Map<String,Object>> applications = new List<Map<String,Object>>();
            User user = [Select Peoplesoft_Id__c from User where id=:UserInfo.getUserId()];
           
            dispositionData.put('hrEmplId',user.Peoplesoft_Id__c);

            for(MCEmail submital : subRecords){                

                //dispCode = submital.dispositionCode;
                //dispReasonCode = submital.dispositionReasonCode;
                Map<String,String> application = new Map<String,String>();
                application.put('vendorApplicationId',submital.applicationId);
                application.put('vendorId',submital.vendorId);
                applications.add(application);                      
            }
            dispositionData.put('dispositionCode',disposCode);
            dispositionData.put('dispositionReasonCode',disposReason);
            dispositionData.put('applicationIdList',applications);
            return sendToRWS(dispositionData);
        }catch(Exception exp){
            System.debug('pushDispositionDetailsToRWS ' + exp.getMessage());
            saveStatus = 'FAIL';
        }
        return saveStatus;
    }

    private static String sendToRWS(Map<String,Object> dispositionData){

        //String url = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';

        ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues('saveDisposition');
        String url = serviceSettings.Service_URL__c;

        String reqBody = JSON.serialize(dispositionData);
        System.debug('service url '+url);
        System.debug('reqBody '+reqBody);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(reqBody);
        HttpResponse res = h.send(req);
        String respBody = res.getBody();
        System.debug('respBody '+respBody);
        return respBody;
    }

    public class MCEmail{
         
         @AuraEnabled public String contactId;
         @AuraEnabled public String salutation;
         @AuraEnabled public String firstName;
         @AuraEnabled public String lastName;

         @AuraEnabled public String emailTemplateName;

         @AuraEnabled public String jobPostingId;
         @AuraEnabled public String applicationDate;
         @AuraEnabled public String jobPostingTitle;
         @AuraEnabled public String userId;
         @AuraEnabled public String userFullName;
         @AuraEnabled public String userEmail;
         @AuraEnabled public String userTitle;
         @AuraEnabled public String userPhone;

         @AuraEnabled public String opco;
         @AuraEnabled public String contactEmail;
         @AuraEnabled public String contactMobile;
         @AuraEnabled public String contactState;
         @AuraEnabled public String contactCountry;
         @AuraEnabled public String language;

         // Adding following fields to push data to RWS on disposition
         @AuraEnabled public String eventId;
         @AuraEnabled public String vendorId;
         @AuraEnabled public String applicationId;
         @AuraEnabled public String dispositionCode;
         @AuraEnabled public String dispositionReasonCode;
		 @AuraEnabled public String submittalUniqueId;

    }
	
	public static void updateSubmittalwithEmail(map<string, string>EmailRecords){
        List<Order> OrderUpdate = new List<Order>();
        try{
            if(!EmailRecords.isEmpty()){
                for(Order a :[select id, Email_Type__c, Unique_Id__c 
                              from Order 
                              where Unique_Id__c in :EmailRecords.keyset()]){
                    if(EmailRecords.containsKey(a.Unique_Id__c)){
                        a.Email_Type__c = string.valueof(EmailRecords.get(a.Unique_Id__c));
                        OrderUpdate.add(a);
                    }
                }
            }
            system.debug('--OrderUpdate size--'+OrderUpdate.size()+'---OrderUpdate--'+OrderUpdate);
            if (!OrderUpdate.isEmpty())
                Database.Update(OrderUpdate,false);    
        }catch(Exception ex){
            system.debug('--error line No--' + ex.getLineNumber() + '-error Msg-' + ex.getMessage());
        } 
    }
    Public static void onStartDateChange(Date StartDate , String OriginationID, String Comments){
        StartDateEvent__e dateEvent = new  StartDateEvent__e(); 
        dateEvent.Start_Date__c = StartDate;
        dateEvent.OriginationID__c = OriginationID;
        if(OriginationID.startsWith('500')){
            dateEvent.SObjectType__c = 'Case';
        } else if(OriginationID.startsWith('801')){
            dateEvent.Comments__c = Comments;
            dateEvent.SObjectType__c = 'Order';            
        } else{
            dateEvent.Comments__c = Comments;
            dateEvent.SObjectType__c = 'Event';
        }
        System.debug('**************dateEvent*****'+dateEvent);
        Database.SaveResult sr = EventBus.publish(dateEvent);
        System.debug('*************SaveResult*****'+sr);
        if (sr.isSuccess()) {
      		System.debug('Successfully published event for Start Date.');
            System.debug('Start Date: '+StartDate);
   	    } 
        else {
    		for(Database.Error err : sr.getErrors()) {
       			System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
    		}
        }
       // return StartDate;
	}    
}