@isTest (SeeAllData=false)
public class SubmittalStatusController_Test {

	/*  
    private static testmethod void testSubmittalStatusController() {
        
        
        List<Account> accList = new List<Account>();
        List<opportunity>opplist =new list<opportunity>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;
        
        
        
        List<Contact> talentConList = new List<Contact>();
        for(Integer i=0; i<2 ; i++) {
            Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
            ct.Other_Email__c = 'other'+i+'@testemail.com';
            ct.Title = 'test title'+i;
            ct.MailingState = 'test text'+i;
            
            talentConList.add(ct);
        }
        insert talentConList;

        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        
        String reqRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

            Opportunity newOpp = new Opportunity();
            newOpp.Name = 'New ReqOpportunities';
            newOpp.Accountid = accList[0].id;
            newOpp.RecordTypeId = reqRecordTypeID;
            Date closingdate = system.today();
            newOpp.CloseDate = closingdate.addDays(25);
            newOpp.StageName = 'Draft';
            newOpp.Req_Total_Positions__c = 2;
            newOpp.Req_Job_Title__c = 'Salesforce Architect';
            newOpp.EnterpriseReqSkills__c = '[{"name":"Salesforce.com","favorite":false},{"name":"Lightning","favorite":false},{"name":"Apex","favorite":false}]';
            newOpp.Req_Skill_Details__c = 'Salesforce';
            newOpp.Req_Job_Description__c = 'Testing';
            newOpp.Pay_Rate_Max_Multi_Currency__c = 900;
            newOpp.Pay_Rate_Min_Multi_Currency__c = 901;
            newOpp.Req_Bill_Rate_Max__c = 300000;
            newOpp.Req_Bill_Rate_Min__c = 100000;
            newOpp.Currency__c = 'USD - U.S Dollar';
            newOpp.Req_EVP__c = 'test EVP';
            newOpp.Req_Job_Level__c = 'Expert Level';
            newOpp.Req_Rate_Frequency__c = 'Hourly';
            newOpp.Req_Duration_Unit__c = 'Year(s)';
            newOpp.Req_Qualification__c = 'Additional Skill - database';
            newOpp.Opco__c = 'TEKsystems';
            newOpp.BusinessUnit__c = 'Board Practice';
            newOpp.Req_Product__c = 'Permanent';
            newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
            newOpp.Req_Worksite_Street__c = '987 Hidden St';
            newOpp.Req_Worksite_City__c = 'Baltimore';
            newOpp.Req_Worksite_Postal_Code__c = '21228';
            newOpp.Req_Worksite_Country__c = 'United States';
            newOpp.Req_Duration_Unit__c = 'Day(s)';
            insert newOpp;
        
        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = talentConList[0].Id;
        o.OpportunityId = newOpp.Id;
        o.Status = 'Applicant';
        o.EffectiveDate = System.today();
        insert o;
        
        String orderStrg = JSON.serialize(o);
        
        DateTime startDateTime = Datetime.now();
        DateTime endDateTime = startDateTime.addHours(1);
         
        Event evt = new Event();
        evt.StartDateTime = Datetime.now();
        evt.EndDateTime = Datetime.now().addHours(1);
        evt.Submittal_Interviewers__c = 'AKR';
        evt.Activity_Type__c = 'Interviewing';
        evt.WhoId = talentConList[0].Id;
        evt.WhatId = o.Id;
      //  insert evt;
       // SubmittalStatusController.saveSubmittalOfferAccepted();
    }
	*/
    public static testMethod void testPushDispositionDetailsToRWS() {

        Test.startTest();
        
        List<String> lst = SubmittalStatusController.getAvailableStatuses('Allegis Global Solutions, Inc.', 'Applicant', True);
        List<String> lst1 = SubmittalStatusController.getAvailableStatuses('Allegis Global Solutions, Inc.', 'Applicant', False);

        Map<String, String> m1= new Map<String, String> {'applicationId'=> 'vapp_101', 'vendorId'=>'vendor_1'};
        Map<String, String> m2= new Map<String, String> {'applicationId'=> 'vapp_102', 'vendorId'=>'vendor_2'};
        String inputString = JSON.serialize(new List<Map<String, String>> { m1, m2});
        // insert a record in ApigeeOAuthSettings__c for saveDisposition
        ApigeeOAuthSettings__c serviceSettings = new ApigeeOAuthSettings__c();
        serviceSettings.Client_Id__c = '121111';
        serviceSettings.Name = 'saveDisposition';
        serviceSettings.Service_Http_Method__c = 'POST';
        serviceSettings.Service_URL__c = 'https://rws.allegistest.com/RWS/rest/posting/saveDisposition';
        serviceSettings.Token_URL__c = '--';
        insert serviceSettings;
        Test.setMock(HttpCalloutMock.class,new RWSInteractionsCalloutMock());
        String response = SubmittalStatusController.pushDispositionDetailsToRWS(inputString,'DS1','DS1_Reason');
        Test.stopTest();
        System.assertNotEquals(response, 'SUCCESS');
    }
    public static testMethod void testSaveSubmittalMassUpdate(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        List<Account> accList = new List<Account>();
        List<opportunity>opplist =new list<opportunity>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;
       
        List<Contact> talentConList = new List<Contact>();
        for(Integer i=0; i<10 ; i++) {
            Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
            ct.Other_Email__c = 'other'+i+'@testemail.com';
            ct.Title = 'test title'+i;
            ct.MailingState = 'test text'+i;
            
            talentConList.add(ct);
        }
        insert talentConList;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = accList[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        //insert opp1;
        opplist.add(opp1);
        
        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = talentConList[0].Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Applicant';
        o.EffectiveDate = System.today();
        insert o;
        
        Test.startTest();
        List<String> subIds = new List<String>();
		subIds.add(String.valueOf(o.Id));
		//List<String> statuses = SubmittalStatusController.getSubmittalMassUpdate(subIds);
		String result = SubmittalStatusController.saveSubmittalMassUpdate(subIds,'Applicant','TestNpReason');
        String result1 = SubmittalStatusController.saveSubmittalMassUpdate(subIds,'Applicant','');
        Test.stopTest();
        System.assertEquals(result,'SUCCESS');
    }
    
    public static testMethod void testGetSubmittalMassUpdate(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        List<Account> accList = new List<Account>();
        List<opportunity>opplist =new list<opportunity>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;
       
        List<Contact> talentConList = new List<Contact>();
        for(Integer i=0; i<10 ; i++) {
            Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
            ct.Other_Email__c = 'other'+i+'@testemail.com';
            ct.Title = 'test title'+i;
            ct.MailingState = 'test text'+i;
            
            talentConList.add(ct);
        }
        insert talentConList;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = accList[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp1;
        opplist.add(opp1);
        
        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = talentConList[0].Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Applicant';
        o.EffectiveDate = System.today();
        insert o;
        
        Test.startTest();
        List<String> subIds = new List<String>();
		subIds.add(String.valueOf(o.Id));
		List<String> statuses = SubmittalStatusController.getSubmittalMassUpdate(subIds);
        String remove =  SubmittalStatusController.removeApplicant(o.Id);
        String updateOppty = SubmittalStatusController.updateTotalPositions(opp1.Id);
        Test.stopTest();
        system.assertEquals(remove, 'SUCCESS');
    }
    
    public static testMethod void testSaveEmailRecords(){
        Map<String, String> m1= new Map<String, String> {'applicationId'=> 'vapp_101', 'vendorId'=>'vendor_1','applicationDate'=>'2020-11-25'};
        Map<String, String> m2= new Map<String, String> {'applicationId'=> 'vapp_102', 'vendorId'=>'vendor_2','applicationDate'=>'2020-11-25'};
        String inputString = JSON.serialize(new List<Map<String, String>> { m1, m2});
        
        Test.startTest();
        String sub = SubmittalStatusController.saveEmailRecords(inputString);
        String response = SubmittalStatusController.saveMassDisposition(inputString,'DS1','DS1_Reason');
        List<Disposition_Matrix__mdt> disp = SubmittalStatusController.getDispositionMatrix();
        Test.stopTest();
        system.assertNotEquals(disp, null);
    }
	
	public static testMethod void testupdateSubmittalwithEmail(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        List<Account> accList = new List<Account>();
        List<opportunity>opplist =new list<opportunity>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);        
        Insert accList;
        
        List<Contact> talentConList = new List<Contact>();
        for(Integer i=0; i<10 ; i++) {
            Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
            ct.Other_Email__c = 'other'+i+'@testemail.com';
            ct.Title = 'test title'+i;
            ct.MailingState = 'test text'+i;            
            talentConList.add(ct);
        }
        insert talentConList;                
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = accList[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        //insert opp1;
        opplist.add(opp1);
        
        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = talentConList[0].Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Applicant';
        o.EffectiveDate = System.today();
        o.Unique_Id__c = string.valueof(talentConList[0].Id) + string.valueof(opp1.Id);
        insert o;
        
        Job_Posting__c jP1 = new Job_Posting__c(Currency_Code__c='111', 
                                                 Delete_Retry_Flag__c=false, Job_Title__c = 'testJobTitle',Opportunity__c=opplist[0].Id, Posting_Detail_XML__c='Sample Description',
                                                 Private_Flag__c=false,RecordTypeId=Utility.getRecordTypeId('Job_Posting__c','Posting'),Source_System_id__c='R.112233',State__c='MD');
        
        insert jP1;
        
        
        Event evt = new Event();
        evt.StartDateTime = Datetime.now();
        evt.EndDateTime = Datetime.now().addHours(1);
        evt.Submittal_Interviewers__c = 'AKR';
        evt.Activity_Type__c = 'Interviewing';
        evt.WhoId = talentConList[0].Id;
        evt.WhatId = o.Id;
        insert evt;
        
                
        map<string, string>EmailRecords = new map<string, string>();
        MC_Email__c mcemail = new MC_Email__c(ResponseEmailID__c ='ATS_GOOD_CANDIDATE_CANT_REACH_PHONE', ContactID__c = talentConList[0].id, PostingId__c = 'R.112233');
        insert mcemail;         
        EmailRecords.put(string.valueof(o.Unique_Id__c),string.valueof(mcemail.ResponseEmailID__c));        
        
        Test.startTest();
        SubmittalStatusController.updateSubmittalwithEmail(EmailRecords);
        
        SubmittalStatusController.queryInterviewEvents(String.valueOf(talentConList[0].Id),String.valueOf(o.Id));
        SubmittalStatusController.querySubmittal(String.valueOf(o.Id));
        SubmittalStatusController.getApplicantEvent(o.Id);
        
        SubmittalStatusController.onStartDateChange(System.today(),String.valueOf(o.Id),'test_comments');
        SubmittalStatusController.onStartDateChange(System.today(),'00012xyz','test_comments');
        
        SubmittalStatusController.removeLink(o.Id);
        
        SubmittalStatusController.saveSubmittalStatusUpdate(String.valueOf(o.Id),'Not Proceeding',String.valueOf(evt.Id),'200','resaon');
        String result=SubmittalStatusController.saveSubmittalInterview(Json.serialize(evt),String.valueOf(o.Id));
        
        Test.stopTest();
        System.assertNotEquals(result,'');
    }
}