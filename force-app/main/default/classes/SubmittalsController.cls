/******************************************************************************************************************************
* Name        - SubmittalsController 
* Description - Controller to retrieve the submittal information for a req 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pavan                       03/01/2012             Created
* Pavan                       03/02/2012             Refactored the methods in common helper class
* Pavan                       03/05/2012             Included properties to refer to error messages on VF page.
* Pavan                       03/06/2012             Updated line 139 to fix run time exception related to invalid decimal
* Pavan                       03/07/2012             Updated date format to logged in user's profile
* Dhruv                       04/23/2012             Added the logic for sorting of dates as these can't be treated as normal strings. 
* Rajkumar                    5/21/2013              Part of IR Phase-2 implementation, OriginationSys is replaced with Req Origination System field
* Sudheer                     6/27/2013              Part of IR Phase-2 implementation, OriginationSys is replaced with System Code i.e. Origination_Partner_System_Id__c
* Nidish                      5/06/2015              Part of IR.2.1.D implementation, OriginationSys is replaced with Text field i.e. Orig_Partner_System_Id__c
* Hareesh                     09/15/2015             Implemented asynchronous callouts using continuation method.This is done in Sprint-1
********************************************************************************************************************************/
public with sharing class SubmittalsController implements Core_UI_Pager.PagerInterface {
    
    public final static String DEFAULT_SORTORDER = 'ASC';
    public final static String DESCEND = 'DESC';
    public final static String SORT_ASCEND = 'sortAsc';
    public final static String SORT_DESCEND = 'sortDesc';
    public final static String ZERO = '0';
    public static final Integer DEFAULT_PAGE_SIZE = 100;
    public Reqs__c ReqObj ; 
       /***** Private Variables *****/
    private String SiebelId;
    private String OriginationID;
    private String OriginationSys;
    private List<SubmittalWrapper> listSubmittalsFull;
    private List<SubmittalWrapper> listSubmittalsFullSorted;

   //Sorting & Paging properties
    private Core_UI_Pager.Pager pager = null;
    
    // Refactor out
    public Integer prvSortColIndex {get; set;} { prvSortColIndex = 0; }
    public Integer sortColIndex {get; set;} { sortColIndex = 6; }
    public String sortOrder {get; set;} { sortOrder = SORT_DESCEND; }
    
    /***** Visualforce Properties *****/
    public boolean calloutfault { get; set; } 
    public String exceptionMessage { get;set; }
    public String exceptionDescription { get;set; }
    public List<SubmittalWrapper> listSubmittalsPage { get; set; } 
    List<WS_TIBCO_SubmittalType.SubmittalLogData2_element> SubmittalsToProcess = new List<WS_TIBCO_SubmittalType.SubmittalLogData2_element>();  

    //=============================================================================================
    //Standard Controller Method(Constructor)
    //============================================================================================= 
 
    public SubmittalsController(ApexPages.StandardController controller) 
    {   
        calloutfault = false;
        Reqs__c Requisition = (Reqs__c) controller.getRecord();
        SiebelId = Requisition.Siebel_ID__c;
        //Requisition = [Select Origination_System_Id__c, Origination_System__c from Reqs__c where Id =: Requisition.Id];
        Requisition = [Select Origination_System_Id__c, Req_Origination_System__c, Orig_Partner_System_Id__c from Reqs__c where Id =: Requisition.Id];
        ReqObj = Requisition ;
        
        
     } 
     
     //the below action method has been replaced with startrequest continuation method. The blow code will not be used going forward
        public void submittalsInformation()
        {
        //System.debug('===>OriginationID'+Requisition.Name);
        OriginationID = ReqObj.Origination_System_Id__c;
        
        System.debug('===>OriginationID'+OriginationID);
        //OriginationSys = ReqObj.Origination_System__c;
        //OriginationSys = ReqObj.Req_Origination_System__c;
        OriginationSys = ReqObj.Orig_Partner_System_Id__c;  
                
        System.debug('===>OriginationSys '+OriginationSys);
        // get the submittals information
        if(OriginationID != Null)
        {
       
         // Get full list of submittal
            listSubmittalsFull = new List<SubmittalWrapper>();
            //listSubmittalsFull = getSubmittals(Label.GetSubmittals_Label,SiebelId);
            listSubmittalsFull = getSubmittals(OriginationSys,OriginationID);
            System.debug('--->listSubmittalsFull'+listSubmittalsFull);
            if(listSubmittalsFull.size() > 0){
                // Setup Pager
              //  pager = new Core_UI_Pager.Pager(listSubmittalsFull.size(), Core_UI_Page.DEFAULT_PAGE_SIZE); // Commented to make default page size dynamic
                pager = new Core_UI_Pager.Pager(listSubmittalsFull.size(), DEFAULT_PAGE_SIZE);
                // Get sorted list
                listSubmittalsFullSorted = sortRecords(listSubmittalsFull,DESCEND,6); 
                // Bind data       
                BindData(pager.getCurrentPage());
            
            }else{ if(exceptionMessage!= Label.WS_TIBCO_Endpoint_Unreachable)
                  if( exceptionMessage != Label.WebService_Error) exceptionMessage = Label.No_Submittals_found;     //Error Message to be displayed when Siebel Id is Incorrect.
                  else exceptionMessage = Label.WebService_Error;
            }     
         }
        else{exceptionMessage = Label.Siebel_Record_Id_Error_Message; }  //Error Message to be displayed when Siebel Id is not provided. 
           
    }
    
    //===============================================================================================
    //                Data Access
    //===============================================================================================
    
    //Wrapper class for Submittals
    public class SubmittalWrapper{
        public String SiebelID {get;set;}
        public String Status {get;set;}
        public String CandidateId {get;set;}
        public String CandidateName {get;set;}
        public Decimal DL {get;set;}
        public Decimal BillRate {get;set;}
        public String DtSubmitted {get;set;}
        public String SubmittedBy {get;set;}
        
        
        // Pavan[02/24/2012] updated the method to remove too many return statements
       //========================Method Definition=================================================================
      //Method Name:       getColumn
      //Method Type :         Custom Controller Method 
      //Description:       This method is used to assign a colValue corresponding to particular ColIndex.
      //Input Parameters:   Integer(colIndex) 
      //Output Parameters:   String(colValue)
      //===========================================================================================================
        public String getColumn(Integer colIndex) 
        {
            String colValue;
            if(colIndex == 1)
                colValue = Status;
            else if(colIndex == 2)
                colValue = CandidateId;
            else if(colIndex == 3)
                colValue = CandidateName;
            else if(colIndex == 4)
                colValue = String.valueOf(DL);
            else if(colIndex == 5)
                colValue = String.valueOf(BillRate);
            else if(colIndex == 6)
                colValue = DtSubmitted;
            else if(colIndex == 7)
                colValue = SubmittedBy;
            
            // return the value
            return colValue;
        }
    }
    
    
    //===================================Method Definition===========================================
    //Mthod Name:       getSubmittals
    //Method Type:          Custom Controller Method 
    //Description:       This method calls the Tibco Webservice to fetch the Submittals Information.
    //Input Parameters:   SIEBEL Label and Siebel Id.
    //Output Parameters:   List<SubmittalWrapper>
    //this method is not beeing used after implementing asynchronous callouts
    //=================================================================================================    
    public List<SubmittalWrapper> getSubmittals(String ReqSysID,String ReqID) 
    {
        List<SubmittalWrapper> lstReturn = new List<SubmittalWrapper>();
        List<WS_TIBCO_SubmittalType.SubmittalLogData2_element> Submittals = new List<WS_TIBCO_SubmittalType.SubmittalLogData2_element>();
        
        try{ 
            if(Test.isRunningTest()){ 
        Submittals.add(WS_TestData.getSubmittalRecord());
        WS_TIBCO_Submittal wsSub = new WS_TIBCO_Submittal();
        WS_TIBCO_SubmittalType.SubmittalLogData2_element retElement = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();      
        retElement.BillRate='123';
        retElement.CandidateFirstName='123';
        retElement.CandidateGUID='123';
        retElement.CandidateLastName='123';
        retElement.CandidateMiddleName='123';
        retElement.CandidateName='123';
        retElement.DL='123';
        retElement.DateSubmitted='2015-10-02T10:27:12-04:00';
        retElement.HRUserID='123';
        retElement.PartnerCandidateID='123';
        retElement.PartnerCandidateSysID='123';
        retElement.PartnerReqID='123';
        retElement.PartnerReqSubmittalID='123';
        retElement.PartnerReqSubmittalSysID='123';
        retElement.PartnerReqSysID='123';
        retElement.ReqGUID='123';
        retElement.ReqSubmittalGUID='123';
        retElement.SubmittalStatus='123';
        retElement.SubmittedBy='123';
        Submittals.add(retElement);
        WS_TIBCO_SubmittalType.SubmittalLogData2_element retElement1 = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();      
        retElement1.BillRate= NULL;
        retElement1.CandidateFirstName='123';
        retElement1.CandidateGUID='123';
        retElement1.CandidateLastName='123';
        retElement1.CandidateMiddleName='123';
        retElement1.CandidateName='123';
        retElement1.DL=NULL;
        retElement1.DateSubmitted='';
        retElement1.HRUserID='123';
        retElement1.PartnerCandidateID='123';
        retElement1.PartnerCandidateSysID='123';
        retElement1.PartnerReqID='123';
        retElement1.PartnerReqSubmittalID='123';
        retElement1.PartnerReqSubmittalSysID='123';
        retElement1.PartnerReqSysID='123';
        retElement1.ReqGUID='123';
        retElement1.ReqSubmittalGUID='123';
        retElement1.SubmittalStatus='123';
        retElement1.SubmittedBy='123';
        Submittals.add(retElement1); 
        WS_TIBCO_SubmittalType.SubmittalLogData2_element retElement2 = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();      
        retElement2.DateSubmitted='abc';
        retElement1.DL= 'abc';
        Submittals.add(retElement1);        
            }              
            else{
                WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint WSSubmittal = new WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint();
                WSSubmittal.timeout_x = Integer.valueof(System.Label.SyncWebServiceTimeout);
                //  where ( PartnerReqID  = '1-8N7J2U')
                String ReqGUID,SortColumn,SortDirection, SubmittalInput_Filter = 'where ( PartnerReqID  = ' + '\'' + ReqID + '\'' + ')';
                
                try {Submittals = WSSubmittal.PullSubmittalLogSF(ReqSysID, ReqID,ReqGUID,SubmittalInput_Filter,SortColumn,SortDirection);}
                
                catch(CalloutException ex){exceptionDescription = ex.getMessage();
                System.debug('-----exceptionDescription'+exceptionDescription);
                calloutfault = true;
                        
                if(ex.getMessage().contains('WebService returned a SOAP Fault')) exceptionMessage = Label.WebService_Error;
                else exceptionMessage = Label.WS_TIBCO_Endpoint_Unreachable;    
            // code that gets executed for test methods
                }    
            }
            
        if(Submittals != Null) {
                SubmittalWrapper obj;
                for(WS_TIBCO_SubmittalType.SubmittalLogData2_element respRecord : Submittals) {
                    obj = new SubmittalWrapper();                        //Instantiate Submittals Wrapper
                    obj.SiebelID = respRecord.ReqSubmittalGUID;                  //Assign Siebel ID value 
                    obj.Status = respRecord.SubmittalStatus;                  //Assign Status 
                    obj.CandidateId = respRecord.PartnerCandidateID;              //Assign Candidate ID 
                    obj.CandidateName = respRecord.CandidateName;                //Assign Candidate Name 
                    if(respRecord.DL != Null && respRecord.DL.trim().length() > 0)        //Check if DL has value or not.
                        obj.DL = Decimal.valueOf(respRecord.DL);                //Assign Decimal converted value to DL
                    else
                       obj.DL = Decimal.valueOf(ZERO);                      //If no DL value, assign 0  
                   
                    if(respRecord.BillRate != Null && respRecord.BillRate.trim().length() > 0)   //Check if Bill Rate has value or not.  
                        obj.BillRate = Decimal.valueOf(respRecord.BillRate);             //Assign BillRate value
                    else 
                        obj.BillRate = Decimal.Valueof(ZERO);
                    if(respRecord.DateSubmitted != Null && respRecord.DateSubmitted != '') {
                        try {
                            Datetime DtSubmitted = Datetime.valueOf((respRecord.DateSubmitted).subString(0,10) +' '+ Label.TimeStamp_for_Submittals);
                            obj.DtSubmitted = DtSubmitted.format(Core_Data.retrieveLocaleDateFormat()); //Assign Date Submittted in Local Date Format obtained from method in Core_Data Class.
                        }
                        catch(TypeException ex){  }
                        }
                      obj.SubmittedBy = respRecord.SubmittedBy;                      //Assign Submitted By.
                     lstReturn.add(obj);
                }
            }
        } 
        catch(CalloutException ex) 
        {
             exceptionDescription = ex.getMessage();
             calloutfault = true;
             Core_Log.logException(ex);
             System.debug('+++++++'+ ex.getMessage());
             exceptionMessage = Label.WS_TIBCO_Endpoint_Unreachable; 
         }
         catch(Exception ex) 
         {
           calloutfault = true;
            Core_Log.logException(ex);
            exceptionDescription = ex.getMessage();
            //calloutfault = true;
             exceptionMessage = Label.WS_TIBCO_Endpoint_Unreachable;
        }
        return lstReturn;                                        //Return list of Submittal Informations.
    }
    
    /** Start - workaround to overcome Long-Running Callouts**/
        //List<WS_TIBCO_SubmittalType.SubmittalLogData2_element> SubmittalsToProcess = new List<WS_TIBCO_SubmittalType.SubmittalLogData2_element>();
        AsyncWS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2Future WS_Resp_toProcess = new AsyncWS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2Future(); 
        public Continuation startRequest() {
            System.debug('!!!!!!!!!!!!!In request method!!!!!');
            Continuation con = new Continuation(60);
            con.continuationMethod='processResponse';
            
            if(Test.isRunningTest()){ 
        //Submittals.add(WS_TestData.getSubmittalRecord());
        WS_TIBCO_Submittal wsSub = new WS_TIBCO_Submittal();
        WS_TIBCO_SubmittalType.SubmittalLogData2_element retElement = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();      
        retElement.BillRate='123';
        retElement.CandidateFirstName='123';
        retElement.CandidateGUID='123';
        retElement.CandidateLastName='123';
        retElement.CandidateMiddleName='123';
        retElement.CandidateName='123';
        retElement.DL='123.00';
        retElement.DateSubmitted='2015-10-02T10:27:12-04:00';
        retElement.HRUserID='123';
        retElement.PartnerCandidateID='123';
        retElement.PartnerCandidateSysID='123';
        retElement.PartnerReqID='123';
        retElement.PartnerReqSubmittalID='123';
        retElement.PartnerReqSubmittalSysID='123';
        retElement.PartnerReqSysID='123';
        retElement.ReqGUID='123';
        retElement.ReqSubmittalGUID='123';
        retElement.SubmittalStatus='123';
        retElement.SubmittedBy='123';
        SubmittalsToProcess.add(retElement);
        WS_TIBCO_SubmittalType.SubmittalLogData2_element retElement1 = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();      
        retElement1.BillRate= NULL;
        retElement1.CandidateFirstName='123';
        retElement1.CandidateGUID='123';
        retElement1.CandidateLastName='123';
        retElement1.CandidateMiddleName='123';
        retElement1.CandidateName='123';
        retElement1.DL=NULL;
        retElement1.DateSubmitted='';
        retElement1.HRUserID='123';
        retElement1.PartnerCandidateID='123';
        retElement1.PartnerCandidateSysID='123';
        retElement1.PartnerReqID='123';
        retElement1.PartnerReqSubmittalID='123';
        retElement1.PartnerReqSubmittalSysID='123';
        retElement1.PartnerReqSysID='123';
        retElement1.ReqGUID='123';
        retElement1.ReqSubmittalGUID='123';
        retElement1.SubmittalStatus='123';
        retElement1.SubmittedBy='123';
        SubmittalsToProcess.add(retElement1); 
        WS_TIBCO_SubmittalType.SubmittalLogData2_element retElement2 = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();      
        retElement2.DateSubmitted='abc';
        retElement1.DL= '123';
        SubmittalsToProcess.add(retElement1); 
        processResponse();       
            }              
            try{
                  OriginationID = ReqObj.Origination_System_Id__c;
                
                System.debug('===>OriginationID'+OriginationID);
                OriginationSys = ReqObj.Orig_Partner_System_Id__c;
                System.debug('===>OriginationSys '+OriginationSys);
                // get the submittals information
                if(OriginationID != Null){
                    //WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint WSSubmittal = new WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint();
                    AsyncWS_TIBCO_Submittal.AsyncSubmittalLogServiceSFHTTPEndpoint WSSubmittal = new AsyncWS_TIBCO_Submittal.AsyncSubmittalLogServiceSFHTTPEndpoint();
                    //WSSubmittal.timeout_x = 60000;
                    //  where ( PartnerReqID  = '1-8N7J2U')
                    String ReqGUID,SortColumn,SortDirection, SubmittalInput_Filter = 'where ( PartnerReqID  = ' + '\'' + OriginationID + '\'' + ')';
                    try {
                        WS_Resp_toProcess = WSSubmittal.beginPullSubmittalLogSF(con, OriginationSys, OriginationID,ReqGUID,SubmittalInput_Filter,SortColumn,SortDirection);
                        system.debug('Result'+WS_Resp_toProcess);
                    }catch(CalloutException ex){
                        exceptionDescription = ex.getMessage();
                        System.debug('-----exceptionDescription'+exceptionDescription);
                        calloutfault = true;
                        if(ex.getMessage().contains('WebService returned a SOAP Fault')) exceptionMessage = Label.WebService_Error;
                        else exceptionMessage = Label.WS_TIBCO_Endpoint_Unreachable;
                    }  
                }
                else{exceptionMessage = Label.Siebel_Record_Id_Error_Message; }  
            }
            catch(CalloutException ex) 
            {
                 exceptionDescription = ex.getMessage();
                 calloutfault = true;
                 Core_Log.logException(ex);
                 System.debug('+++++++'+ ex.getMessage());
                 exceptionMessage = Label.WS_TIBCO_Endpoint_Unreachable; 
             }
             catch(Exception ex) 
             {
               calloutfault = true;
                Core_Log.logException(ex);
                exceptionDescription = ex.getMessage();
                //calloutfault = true;
                 exceptionMessage = Label.WS_TIBCO_Endpoint_Unreachable;
            }
            return con; 
        }
        
        public Object processResponse() {
        System.debug('!!!!!!!!!!!!!In response method!!!!!');
            listSubmittalsFull = new List<SubmittalWrapper>();
         if(!Test.isRunningTest()) {   
            if(WS_Resp_toProcess != null)
            try{
                SubmittalsToProcess = WS_Resp_toProcess.getValue();
                }
                
                catch(CalloutException ex){
                        exceptionDescription = ex.getMessage();
                        System.debug('-----exceptionDescription'+exceptionDescription);
                    if(exceptionDescription.containsIgnoreCase('SOAP Fault') || exceptionDescription.containsIgnoreCase('IO Exception')){               
                        calloutfault = true;
                        Core_Log.logException(ex); 
                        if(exceptionDescription.containsIgnoreCase('WebService returned a SOAP Fault')) exceptionMessage = Label.WebService_Error;
                        else exceptionMessage = Label.WS_TIBCO_Endpoint_Unreachable;  
                    }else{
                      submittalsInformation();  
                  }                   
       
                }
                
               }   
                
        System.debug('!!!!!!!!!!!!!!Response!!'+SubmittalsToProcess);        
            if(SubmittalsToProcess != Null) {
                SubmittalWrapper obj;
                for(WS_TIBCO_SubmittalType.SubmittalLogData2_element respRecord : SubmittalsToProcess) {
                    obj = new SubmittalWrapper();                        //Instantiate Submittals Wrapper
                    obj.SiebelID = respRecord.ReqSubmittalGUID;                  //Assign Siebel ID value 
                    obj.Status = respRecord.SubmittalStatus;                  //Assign Status 
                    obj.CandidateId = respRecord.PartnerCandidateID;              //Assign Candidate ID 
                    obj.CandidateName = respRecord.CandidateName;                //Assign Candidate Name 
                    if(respRecord.DL != Null && respRecord.DL.trim().length() > 0)        //Check if DL has value or not.
                        obj.DL = Decimal.valueOf(respRecord.DL);                //Assign Decimal converted value to DL
                    else
                       obj.DL = Decimal.valueOf(ZERO);                      //If no DL value, assign 0  
                   
                    if(respRecord.BillRate != Null && respRecord.BillRate.trim().length() > 0)   //Check if Bill Rate has value or not.  
                        obj.BillRate = Decimal.valueOf(respRecord.BillRate);             //Assign BillRate value
                    else 
                        obj.BillRate = Decimal.Valueof(ZERO);
                    if(respRecord.DateSubmitted != Null && respRecord.DateSubmitted != '') {
                        try {
                            Datetime DtSubmitted = Datetime.valueOf((respRecord.DateSubmitted).subString(0,10) +' '+ Label.TimeStamp_for_Submittals);
                            obj.DtSubmitted = DtSubmitted.format(Core_Data.retrieveLocaleDateFormat()); //Assign Date Submittted in Local Date Format obtained from method in Core_Data Class.
                        }
                        catch(TypeException ex){  }
                        }
                      obj.SubmittedBy = respRecord.SubmittedBy;                      //Assign Submitted By.
                     listSubmittalsFull.add(obj);
                }
                system.debug('processed Result'+listSubmittalsFull);
                if(listSubmittalsFull.size() > 0){
                    pager = new Core_UI_Pager.Pager(listSubmittalsFull.size(), DEFAULT_PAGE_SIZE);
                    listSubmittalsFullSorted = sortRecords(listSubmittalsFull,DESCEND,6); 
                    BindData(pager.getCurrentPage());
                }else{ if(exceptionMessage!= Label.WS_TIBCO_Endpoint_Unreachable)
                      if( exceptionMessage != Label.WebService_Error) exceptionMessage = Label.No_Submittals_found;     //Error Message to be displayed when Siebel Id is Incorrect.
                      else exceptionMessage = Label.WebService_Error;
                } 
            }
            else{
            if(exceptionMessage!= Label.WS_TIBCO_Endpoint_Unreachable)
                      if( exceptionMessage != Label.WebService_Error){
                       exceptionMessage = Label.No_Submittals_found;
                       calloutfault = true;     //Error Message to be displayed when Siebel Id is Incorrect.
                       }
                      
                      else exceptionMessage = Label.WebService_Error;
                      }
            
            return null;    
        }

    /** End - workaround to overcome Long-Running Callouts**/
    
    //=========================================================================================================
    //                  UI Events 
    //=========================================================================================================
    
    //============================Method Definition=============================================================
    //Method Name:       sortAction
    //Method Type :         Custom Controller Method 
    //Description:       This method is called from VF page and contains logic for checking which column 
    //            header is clicked and sorts based on that column. If clicked twice sort order changes.
    //Input Parameters:   None
    //Output Parameters:   None
    //===========================================================================================================
    /* ** ** ** ** Sorting Function called from VF ** ** ** ** */
    public void sortAction(){
        String order = DEFAULT_SORTORDER;

        /*This checks to see if the same header was click two times in a row, if so 
        it switches the order.*/
        if(prvSortColIndex == sortColIndex)
        {
            order = DESCEND;
            sortOrder = SORT_DESCEND;
            prvSortColIndex = 0;
        }else
        {
            sortOrder = SORT_ASCEND;
            prvSortColIndex = sortColIndex;
        }
        
         listSubmittalsFullSorted = sortRecords(listSubmittalsFull, order, sortColIndex); 
         System.debug('&&&&&&listSubmittalsFullSorted'+listSubmittalsFullSorted);
         BindData(pager.getCurrentPage()); //Bind VF table with list of specified page
    }
    
    //================================Method Definition=========================================================
    //Method Name:       Pagination Function
    //Method Type :         Custom Controller Method 
    //Description:       This function is a collection of methods firstBtnClick(),previousBtnClick(),nextBtnClick()
    //            lastBtnClick(),getPreviousButtonEnabled(),getNextButtonEnabled() and rediretcs the 
    //            user to desired page.The logic is written in the Core_UI_Pager class.
    //Input Parameters:   None
    //Output Parameters:   Boolean for getPreviousButtonEnabled()and getNextButtonEnabled().
    //===========================================================================================================
   
    public void firstBtnClick(){
        pager.moveToPage(1);
        BindData(pager.getCurrentPage());
    }
    
    public void previousBtnClick(){
        pager.movePrevPage();
        BindData(pager.getCurrentPage());
    }   
    
    public void nextBtnClick(){
        pager.moveNextPage();
        BindData(pager.getCurrentPage());
    }
    
    public void lastBtnClick(){
        BindData(pager.getTotalPageNumber());
    }
    
    public Boolean getPreviousButtonEnabled() {
         return(pager == null? false:pager.getPreviousButtonStatus());    
       // return(pager.getPreviousButtonStatus());
    }

    public Boolean getNextButtonEnabled() {
    return(pager == null? false:pager.getNextButtonStatus(listSubmittalsFullSorted.size()));
       // return(pager.getNextButtonStatus(listSubmittalsFullSorted.size()));  
    }
    
    //============================Method Definition=============================================================
    //Method Name:       BindData
    //Method Type :         Custom Controller Method 
    //Description:       This method is used to calculate the amount of data that is to be shown on a single 
    //            page. It calls the Core_UI_Pager class for the Binding logic.
    //Input Parameters:   Integer(PageIndex) 
    //Output Parameters:   None
    //===========================================================================================================
    private void BindData(Integer newPageIndex) {
        try {
                Map<String,Integer> minMaxMap = new Map<String,Integer>();
                listSubmittalsPage = new List<SubmittalWrapper>();
                minMaxMap = Pager.getMinMaxValues(newPageIndex , listSubmittalsFullSorted.size());
                Integer min = minMaxMap.get('Min');
                Integer max = minMaxMap.get('Max');
                for(Integer x = min; x < max; x++) 
                {
                    listSubmittalsPage.add(listSubmittalsFullSorted[x]);
                }
                 pager.moveToPage(newPageIndex);
                 if (listSubmittalsPage == Null || listSubmittalsPage.size() <= 0)
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,Label.No_Data_Available_for_View));
        }
        catch(Exception ex) 
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
       //     errors.add(CommonHelper.logException(ex));
        }
    }
    
    //==============================Method Definition ===========================================================
    //Method Name:       sortRecords
    //Method Type :         Custom Controller Method 
    //Description:       This method is used sort data based on the column selected. It makes a map of data 
    //            with particular column as key and rest of the data as value.It segregates the Numbers
    //            from the strings and sorts them accordingly.   
    //Input Parameters:   List<SubmittalWrapper>(lstRecords), String(sortOrder), Integer(colIndex) 
    //Output Parameters:   List<SubmittalWrapper>
    //===========================================================================================================  
    public List<SubmittalWrapper> sortRecords(List<SubmittalWrapper> lstRecords, String sortOrder, Integer colIndex) 
    {
        /*Fill all values in a Map with combination of column value and unique value
          This would need to fetch record after key Sorting*/
        System.debug('Inside Sorting of Starts:'+lstRecords.size()+'- sortOrder:'+sortOrder+'- colIndex:'+colIndex);
        List<SubmittalWrapper> retSubmittal = new List<SubmittalWrapper>();
        
        Map<String, List<SubmittalWrapper>> mapSubmittalforNumber = new Map<String, List<SubmittalWrapper>> ();
        
        for(SubmittalWrapper rec : lstRecords)
        {
             String tmpValue = '';
            if(rec.getColumn(colIndex)!= null) 
               tmpValue = rec.getColumn(colIndex);
            if(mapSubmittalforNumber.containsKey(tmpValue)) 
            {
               mapSubmittalforNumber.get(tmpValue).add(rec);
                
            }
            else 
            {
               mapSubmittalforNumber.put(tmpValue, new List<SubmittalWrapper>());
               mapSubmittalforNumber.get(tmpValue).add(rec);
            }
        }     
          //Sort all keys
        List<string>sortedKeys = new List<string>();
        if(colIndex == 2 || colIndex == 4 || colIndex == 5)           //Check for columns which are of Number type and sort accordingly.
        {
          List<double>sortKeys = new List<double>();
          Map<double, List<SubmittalWrapper>> tempMapSubmittalforNumber = new Map<double, List<SubmittalWrapper>> ();
            for(string key : mapSubmittalforNumber.keyset())
            {
              if(key != Null && key.trim().length() > 0)
              {
                sortKeys.add(double.valueOf(key));
                tempMapSubmittalforNumber.put(double.valueOf(key),mapSubmittalforNumber.get(key));
              }
              else 
              {
                  retSubmittal  = lstRecords; 
                  return retSubmittal;
              }
              
              
             }
            sortKeys.sort();
            for(double val : sortKeys)
              sortedKeys.add(string.valueOf(val));
            //clear the map
            mapSubmittalforNumber.clear();
            for(double key : tempMapSubmittalforNumber.keyset())
              mapSubmittalforNumber.put(string.valueOf(key),tempMapSubmittalforNumber.get(key));
        }
        else if(colIndex == 6)                       //Dhruv[04/09/2012]. Else block for sorting datetime data type. 
        /*{   
            integer flag=0;
            List<date> sortedKeysDate = new List<date>();            //Make a list of dates                
            List<String> nullDateKeys = new List<String>();
            for(string key : mapSubmittalforNumber.keyset())            //iterate over the map of data.
            {
                system.debug('====>key '+key+'##mapStartsforNumber'+mapSubmittalforNumber ); 
                if(key != null && key.trim().length() > 0){
                 //flag=true;
                sortedKeysDate.add(date.parse(key));
                }
                else                         // Added the else block, if the values are null,then return the recieved list and no Sorting required
                {
                    retSubmittal = lstRecords;
                    return  retSubmittal;
                }
                flag++;
                //Populate the list with parsed date(yyyy/mm/dd).
            }             
            if(sortedKeysDate != null && pager != null) {
                sortedKeysDate.sort();                                    //Sort the list                                  
                sortedKeys = pager.getOriginalkey(sortedKeysDate);                 
                System.debug('====sortedKeys '+sortedKeys);
            }

            }*/
         {   integer flag=0;
                List<date> sortedKeysDate = new List<date>();            //Make a list of dates                
                List<String> nullDateKeys = new List<String>();
                List<String> sortedTempKeys = new List<String>();
                for(string key : mapSubmittalforNumber.keyset())            //iterate over the map of data.
                {
                    system.debug('====>key '+key+'##mapSubmittalforNumber'+mapSubmittalforNumber); 
                    if(key != null && key.trim().length() > 0){
                     //flag=true;
                    sortedKeysDate.add(date.parse(key));
                    }
                    else{
                        nullDateKeys.add(key);
                    }
                    flag++;
                    //Populate the list with parsed date(yyyy/mm/dd).
                }             
                if(sortedKeysDate != null && pager != null) {
                    sortedKeysDate.sort();                                    //Sort the list                                  
                    sortedTempKeys = pager.getOriginalkey(sortedKeysDate);  
                    for(String tempKey : nullDateKeys) {sortedKeys.add(tempKey);}    
                    for(String tempKeyDate : sortedTempKeys){
                        sortedKeys.add(tempKeyDate);
                    }                 
                    System.debug('====sortedKeys '+sortedKeys);
                }

            }    
        
          
        else
        {
          for(string key : mapSubmittalforNumber.keyset())    //If Column is string type then sort accordingly.
              sortedKeys.add(key);
             sortedKeys.sort();
         }   
        //fetch values from Map in sorted way and fill in new List
        if(sortOrder == DEFAULT_SORTORDER) //Ascending Order 
        {
            for(Integer i = 0; i < sortedKeys.size(); i++) {
                retSubmittal.addAll(mapSubmittalforNumber.get(sortedKeys[i]));
            }
        }
        else //Decending Order
        { 
            for(Integer i = sortedKeys.size() - 1; i >= 0; i--){
                retSubmittal.addAll(mapSubmittalforNumber.get(sortedKeys[i]));
            }
        }
       System.debug('****retSubmittal'+retSubmittal);
       
    return retSubmittal;
  }
    
}