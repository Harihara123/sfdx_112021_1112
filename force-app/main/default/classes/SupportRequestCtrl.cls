public class SupportRequestCtrl {
    
    @AuraEnabled
    public static boolean updateApprovedPositions(String recordId , integer approvedPositions){
        try{
            List<Support_request__c> listSupportReq = [select id, Alternate_Delivery_Positions_approved__c 
                                                       FROM Support_request__c where id =: recordId limit 1];
            
            if (!listSupportReq.isEmpty()){
                Support_request__c supportReq = listSupportReq[0];
                supportReq.Alternate_Delivery_Positions_approved__c = approvedPositions;
                update supportReq;
                return true;
            }
        }catch(exception ex){
            throw new AuraHandledException('There was some error, Please contact administrator');
        }
        
        return false;
    }
    
    @AuraEnabled
    public static boolean isQueueMember( String recordId ){
        String recordOwnerId = null;
        Set<Id> publicGroupIds = new Set<Id>();
        Set<Id> groupUserIds = new Set<Id>();
        
        List<Support_request__c> listSupportReq = [Select id, Owner.name from Support_Request__c where id =: recordId ];
        if (!listSupportReq.isEmpty()){
            recordOwnerId = 	listSupportReq[0].OwnerId;
            
            List<GroupMember> publicGroups = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: recordOwnerId];
            
            for(GroupMember pg : publicGroups){
			    publicGroupIds.add(pg.UserOrGroupId);
            }

            List<GroupMember> groupUsers = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN: publicGroupIds];
            for(GroupMember pgu : groupUsers){
			    groupUserIds.add(pgu.UserOrGroupId);
            }
            
        }
      
        if( groupUserIds.contains( UserInfo.getUserId() ) ){
            return true;
        }else{
            return false;
        }
        
    }
    @AuraEnabled
    public static boolean isEscalationRequestCreated(String recordId){
        try{
            List<Support_request__c> listSupportReq = [select id FROM Support_request__c where Opportunity__c =: recordId and recordtype.name = 'Escalation' and Status__c != 'Complete'];
            
            if (!listSupportReq.isEmpty()){
                return true;
            }
        }catch(exception ex){
            throw new AuraHandledException('There was some error, Please contact administrator');
        }
        
        return false;
    }
    public static PageReference redirectEscalation(){
        string value = Apexpages.currentPage().getParameters().get('value');
        
        PageReference pageRef = new PageReference('/' + value);
        pageRef.setRedirect(true);
        return pageRef;
    }
    @AuraEnabled
    public static boolean isSupportRequestCreated(String recordId){
        try{
            List<Support_request__c> listSupportReq = [select id FROM Support_request__c where Opportunity__c =: recordId and recordtype.name = 'Proposal' and Status__c != 'Complete' and Status__c != 'Cancelled'];
            
            if (!listSupportReq.isEmpty()){
                return true;
            }
        }catch(exception ex){
            throw new AuraHandledException('There was some error, Please contact administrator');
        }
        
        return false;
    }
    public static PageReference redirect(){
        string value = Apexpages.currentPage().getParameters().get('value');
        
        PageReference pageRef = new PageReference('/' + value);
        pageRef.setRedirect(true);
        return pageRef;
    }
    @AuraEnabled
    public static boolean isOpportunityClosed(String recordId){
        try{
            List<Opportunity> listOpportunity = [select id FROM Opportunity where id =: recordId and isClosed = true and StageName = 'Closed Won' limit 1];
            
            if (!listOpportunity.isEmpty()){
                return false;
            }
        }catch(exception ex){
            throw new AuraHandledException('There was some error, Please contact administrator');
        }
        
        return true;
    }
	@AuraEnabled
    public static list<Opportunity> getOpptyData(string recordId){
        list<Opportunity> OpptyList = [Select id, Impacted_Regions__c, Location__c, Division__c from Opportunity where id =:recordId];
        if(!OpptyList.isEmpty()){
            return OpptyList;
        }else{
            return null;
        }
    }
}