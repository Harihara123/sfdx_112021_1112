@isTest
public class SupportRequestCtrl_Test  {
    
    static User user = TestDataHelper.createUser('System Administrator');
    
    static testMethod void  coverUpdateApprovedPositions(){
       
        string ADC_recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Alternate Delivery Center').getRecordTypeId();
        
        Alternate_Delivery_Office_Queues__c setting = new Alternate_Delivery_Office_Queues__c();
        setting.Name = '00570 - SRC Torrance CA';
        setting.Queue_Name__c = 'SRC Torrance CA Distribution Team';
        insert setting;
        
        Support_request__c obj = new Support_request__c( Alternate_Delivery_Shared_Positions__c = 10, 
                                                        Alternate_Delivery_Office__c = '00570 - SRC Torrance CA',
                                                        recordtypeid = ADC_recordtypeid );
        insert obj;
        
        system.runAs(user) {
            
            Group grp = new Group( Name = 'Test Queue', Type = 'QUEUE' );
            insert grp;
            QueuesObject queue = new QueuesObject (queueid = grp.id, sobjectType = 'Support_request__c' );
            insert queue;
            
        }
        
        SupportRequestCtrl.updateApprovedPositions( obj.id, 5 );
        SupportRequestCtrl.isQueueMember( obj.id );
    
    }
    
}