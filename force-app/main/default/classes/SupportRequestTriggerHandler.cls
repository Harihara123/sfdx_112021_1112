/***************************************************************************
Name        : SupportRequestTriggerHandler
Created By  : Nidsih Rekulapalli
Date        : 21 March 2016
Story/Task  : CRMX-642
Purpose     : Support Request Creation
********************************************************************/              
public with sharing class SupportRequestTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String beforeUndelete = 'beforeUndelete';
    Map<Id, Support_Request__c> oldMap = new Map<Id, Support_Request__c>();
    Map<Id, Support_Request__c> newMap = new Map<Id, Support_Request__c>();
    string Esc_Req_recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Escalation').getRecordTypeId();
    string Proposal_Req_recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Proposal').getRecordTypeId();
    string ADC_recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Aerotek Delivery Center').getRecordTypeId();
    
    list<String> queID = new List<String>();
    list<String> queName = new List<String>();
    Map<String,Id> groupMap = new Map<String,Id>();
    string prdteam;
    public static Boolean isFirstTime  = true;
    
    private static boolean hasBeenProccessed = false;

    
    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Support_Request__c> triggerNew) {
        RequestQueueHandler(triggerNew);
        ProposalQueueHandler(triggerNew);
        UpdateManagerEmail(triggerNew); 
        
    }
    
    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Support_Request__c> triggerNew, Map<Id, Support_Request__c>newMap) {
       // alternateDeliveryCenterApproval(triggerNew);
        //SupportRequestTriggerHandler.updateOpportunityFields(null, newMap);
        updateOpportunityFieldsForADCS(triggerNew,NULL);
        TrackAllocationSourceController.trackSupportRequests(triggerNew);
        
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'SupportRequestTriggerHandler', 'OnAfterInsert', 
                    'Triggered ' + triggerNew.size() + ' Support_Request__c records: ' + CDCDataExportUtility.joinObjIds(triggerNew));
            }
            PubSubBatchHandler.insertDataExteractionRecord(triggerNew, 'Support_Request__c');
            hasBeenProccessed = true; 
        }
    }
    
    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Support_Request__c>oldMap, Map<Id, Support_Request__c>newMap) {
        //changeOwnerForAssociatedOffice(oldMap, newMap);
    }
    
    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Support_Request__c>oldMap, Map<Id, Support_Request__c>newMap) {
        if( SupportRequestTriggerHandler.isFirstTime ){
           // sendApprovalAterUpdate(oldMap, newMap);
        }
        //SupportRequestTriggerHandler.updateOpportunityFields(oldMap, newMap);
        updateOpportunityFieldsForADCS(newMap.values(),oldMap);
        
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'SupportRequestTriggerHandler', 'OnAfterUpdate', 
                    'Triggered ' + newMap.values().size() + ' Support_Request__c records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'Support_Request__c');
            hasBeenProccessed = true; 
        }
    }
    
    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Support_Request__c>oldMap) {
        
    }
    
    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Support_Request__c>oldMap) {
    	updateOpportunityFieldsForADCS(oldMap.values(),NULL);
        
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'SupportRequestTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' Support_Request__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Support_Request__c');
            hasBeenProccessed = true; 
        }
    }
    
    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, Support_Request__c>oldMap) {
        
    }
    //for sending the support to a queue as per record types.
    public void RequestQueueHandler(List<Support_Request__c> newRecords) {
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) {   
            
            List<User> UserRegion = new list<user>();
            
            String RequestOwner;
            
            UserRegion = [Select Region__c,Business_Unit_Region__c From User Where Id = :UserInfo.getUserId()];
            //list of all queues of  Support_Request__c   
            for(QueueSobject que : [SELECT QueueId,SobjectType FROM QueueSobject WHERE SobjectType = 'Support_Request__c']){
                queID.add(que.QueueId);
            }
            System.debug('!!!!!queID!!!!!!' +queID);
            
            for(Group grp : [Select Id,Name from Group Where Type='Queue' and Id IN : queID]){
                groupMap.put(grp.Name,grp.Id);
                queName.add(grp.Name);
            } 
            System.debug('!!!!!groupMap!!!!!!' +groupMap);
            System.debug('!!!!!queName!!!!!!' +queName);
            for(Support_Request__c request : newRecords){
                
                /*if( request.recordtypeid == ADC_recordtypeid ){
                    
                   // Alternate_Delivery_Office_Queues__c obj = Alternate_Delivery_Office_Queues__c.getValues( request.Alternate_Delivery_Office__c );
                   Map<String, Alternate_Delivery_Office_Queues__c> objMap = Alternate_Delivery_Office_Queues__c.getAll();
                    Alternate_Delivery_Office_Queues__c obj = objMap.get(request.Alternate_Delivery_Office__c);
                    if(obj != null){
                        QueueSObject QueueID = [Select Queue.Id, Queue.Name, Queue.Type from QueueSObject WHERE Queue.Type ='Queue' AND Queue.Name =: obj.Queue_Name__c Limit 1];
                        request.OwnerId = QueueID.Queue.Id;
                    }
                    
                }else */if(request.recordtypeid==Esc_Req_recordtypeid){
                        
                    if(request.OpCo__c.contains('TEKsystems') || request.OpCo__c.contains('Aerotek')){
                        
                        if(request.Business_Unit__C=='Clinical Solutions'||request.Business_Unit__C=='EASI'||request.Business_Unit__C=='Government'||request.Business_Unit__C=='Strategic Delivery Solutions'||request.Business_Unit__C=='International' || request.Business_Unit__C== 'Government Operations' || request.Business_Unit__C== 'Government Strategic Sales' || request.Business_Unit__C== 'Managed Resources'){
                            if(groupMap.size()>0 && groupMap.containsKey('Aerotek '+request.Business_Unit__C)){
                                RequestOwner = groupMap.get('Aerotek '+request.Business_Unit__C);
                                system.debug('!!!!RequestOwner - Business unit!!!' +RequestOwner);
                            }   
                            
                            
                        }
                        
                        //Flex Team Que request logic start
                        if(request.OpCo__c == 'Teksystems, Inc.' && request.Business_Unit__c == 'TEK Staffing' && request.Delivery_Model__c == 'Flexible Capacity'){
                            if(groupMap.size()>0 && groupMap.containsKey('Flex Distribution Team')){
                                RequestOwner = groupMap.get('Flex Distribution Team');
                                system.debug('--Flex Request Owner--'+RequestOwner);
                            }   
                        }
                        //Flex Team Que request logic end
                        
                        
                        if(request.Business_Unit__c=='Vertical Sales' && request.Revenue__c >=5000000){
                            
                            if(request.Product_Team__c <> NULL && request.Product_Team__c <>'' ){
                                prdteam= request.Product_Team__c.removestart('VS ');
                                
                                if(groupMap.size()>0 && groupMap.containsKey('Aerotek '+prdteam)){
                                    RequestOwner = groupMap.get('Aerotek '+prdteam);
                                    system.debug('!!!!RequestOwner - prdteam!!!' +RequestOwner);
                                }   
                            }
                        }
                        if( (RequestOwner == null) ){
                            if(UserRegion.size() > 0){
                                String userRegionName = '';
                                    if(request.OpCo__c.contains('Aerotek')){
                                        userRegionName = UserRegion[0].Business_Unit_Region__c+' BDE';
                                    }else{
                                        userRegionName = UserRegion[0].Region__c+' BDE';
                                    }
                            
                                    if(groupMap.size()>0 && groupMap.containsKey(userRegionName)){
                                        RequestOwner = groupMap.get(userRegionName);
                                        system.debug('!!!!RequestOwner - Region!!!' +RequestOwner);
                                    }
                            }    
                        }
                        
                        if(RequestOwner == NULL){
                            if(groupMap.size()>0 && groupMap.containsKey('Orphaned Queue')){
                                request.ownerId = groupMap.get('Orphaned Queue');                      
                            }
                        }else{
                            request.ownerId = RequestOwner;
                        }
                    }
                    
                    if(request.OpCo__c.contains('AG_EMEA') &&groupMap.size()>0) {
                        
                        request.ownerid=groupmap.get('EMEA Escalation Request Team');
                        
                        
                    }
                    
                }
                
            }
        }
    }
    
    
    
    
    Public void  ProposalQueueHandler(List<Support_Request__c> newRecords){
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) { 
            //list of all queues of  Support_Request__c   
            for(QueueSobject que : [SELECT QueueId,SobjectType FROM QueueSobject WHERE SobjectType = 'Support_Request__c']){
                queID.add(que.QueueId);
            }
            System.debug('!!!!!queID!!!!!!' +queID);
            for(Group grp : [Select Id,Name from Group Where Type='Queue' and Id IN : queID]){
                groupMap.put(grp.Name,grp.Id);
                system.debug('grpmap'+groupMap);
                queName.add(grp.Name);
            } 
            for(Support_Request__c request : newRecords){
                
                if(request.recordtypeid==Proposal_Req_recordtypeid){
                    
                    
                    system.debug('opcotest'+request.Opportunity__r.OpCo__c);
                    if(request.OpCo__c.contains('Aerotek')&& groupMap.size()>0) {
                        if(request.Opportunity_Record_Type__c.contains('Aerotek EASi')){
                            request.ownerid=groupmap.get('EASi Proposal Support Team');
                        }else{
                            request.ownerid=groupmap.get('Aerotek Proposal Team');
                        }
                    }   
                    
                    else if(request.OpCo__c.contains('TEKsystems')&&groupMap.size()>0) {
                        request.ownerid=groupmap.get('TEK Proposal Team');
                    }      
                    
                    else if(request.OpCo__c.contains('AG_EMEA')&&groupMap.size()>0){
                        request.ownerid=groupmap.get('AG EMEA Proposal Team');
                    }
                    
                }
                
            }
            
        }
    }
    

    /*public void alternateDeliveryCenterApproval(List<Support_Request__c> newRecords) {
        for(Support_Request__c sp:newRecords){
            if(sp.RecordTypeId == ADC_recordtypeid){
                Approval.ProcessSubmitRequest altReq = new Approval.ProcessSubmitRequest();
                altReq.setProcessDefinitionNameOrId('Alternate_Delivery_Center_Approval_v1_6');
                altReq.setComments('Submitted for Approval');
                altReq.setObjectId(sp.Id);
                try{
                    Approval.ProcessResult res= Approval.process(altReq);
                }
                catch(Exception e){
                    System.debug('Support Request Approval Errors '+e);
                }
            }
                
        }
    }    */
    
    public void UpdateManagerEmail(List<Support_Request__c> newRecords) {
        
        
        if(UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration) { 
            set<id> managerroleid =new set<id>();
            Map<Id, User> userMap = new Map<Id, User>([SELECT UserRoleId ,userrole.parentroleid,Peoplesoft_Id__c,userrole.name,id,email, LastName FROM User WHERE Id = :UserInfo.getUserId()]);
            
            for(user usp:usermap.values()){
                managerroleid.add(usp.userrole.parentroleid);
                
            }
            
            
            
            List<user> managerUser =new List<user>();
            managerUser=[select id,email from user where userroleid in:managerroleid LIMIT 1];
            
            
            for(Support_Request__c sprt:newRecords){
                if(managerUser.size()==1){
                    sprt.Manager_Email__c=manageruser[0].email;
                }
                
            }
            
            
        }
    }

    /*public void changeOwnerForAssociatedOffice(Map<Id, Support_Request__c> oldMap , Map<Id, Support_Request__c> newMap ) {

        if (newMap != null) {

            boolean isQueueMember = isQueueMember( oldMap.values() );

            for (Support_Request__c sp : newMap.values()) {
                
                try{ 
                    
                    if( sp.RecordTypeId == ADC_recordtypeid && sp.Alternate_Delivery_Office__c != oldMap.get( sp.id ).Alternate_Delivery_Office__c  ){

                        if(isQueueMember){

                            Alternate_Delivery_Office_Queues__c obj = Alternate_Delivery_Office_Queues__c.getValues( sp.Alternate_Delivery_Office__c );
                            QueueSObject QueueID = [Select Queue.Id, Queue.Name, Queue.Type from QueueSObject WHERE Queue.Type ='Queue' AND Queue.Name =: obj.Queue_Name__c Limit 1];
                            sp.OwnerId      = QueueID.Queue.Id;
                            sp.Status__c    = Null;
                            
                        }else{
                            sp.addError('Only Queue Member Can Change The Office...!');
                        }
                    }

                }catch(Exception e){
                        System.debug('Updateing Owner Caused Error - Contact Admin '+e);
                }
            
            }
    
        }
    }   

    public void sendApprovalAterUpdate( Map<Id, Support_Request__c> oldMap, Map<Id, Support_Request__c> newMap) {

        if (newMap != null) {

            boolean isQueueMember = isQueueMember( oldMap.values() );
            for (Support_Request__c sp : newMap.values()) {
                if( sp.RecordTypeId == ADC_recordtypeid && sp.Alternate_Delivery_Office__c != oldMap.get( sp.id ).Alternate_Delivery_Office__c  ){
                    if(isQueueMember){
                        Approval.ProcessSubmitRequest altReq = new Approval.ProcessSubmitRequest();
                        altReq.setProcessDefinitionNameOrId('Alternate_Delivery_Center_Approval_v1_6');
                        altReq.setComments('Submitted for Approval');
                        altReq.setObjectId(sp.Id);
                        
                        try{
                            Approval.ProcessResult res= Approval.process(altReq);
                        }catch(Exception e){
                            system.debug('-----Error----- : ' + e.getMessage() );
                        }
                    }
                }
            
            }

            SupportRequestTriggerHandler.isFirstTime = false;
        }
    }   

    private boolean isQueueMember( List<Support_Request__c> supportReqs ){
       
        if( !supportReqs.isEmpty() ){

            String recordOwnerId = supportReqs[0].OwnerId;
            Set<Id> publicGroupIds = new Set<Id>();
            Set<Id> groupUserIds = new Set<Id>();
        
            List<GroupMember> publicGroups = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: recordOwnerId];
            for(GroupMember pg : publicGroups){
                publicGroupIds.add(pg.UserOrGroupId);
            }

            List<GroupMember> groupUsers = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN: publicGroupIds];
            for(GroupMember pgu : groupUsers){
                groupUserIds.add(pgu.UserOrGroupId);
            }
            
            if( groupUserIds.contains( UserInfo.getUserId() ) ){
                return true;
            }
        }    

        return false;
    }
    // Story S-166514 - To update Shared poistion, delivery type and delivery office
    public static void updateOpportunityFields(Map<Id, Support_Request__c>oldMap, Map<Id, Support_Request__c>newMap){
        List<Opportunity> lstOpportunityToUpdate = new List<Opportunity>();
        Set<ID> opportunityIdSet = new Set<ID>();
        Set<ID> supportIdSet = new Set<ID>();
        Map<string,string> ADOffice = new Map<string,string>();
        Map<string,string> DOffice = new Map<string,string>();
        list<Support_Request__c> DeliveryCenterList = new list<Support_Request__c>();
        Map<string,string> DeliveryCenterDetailed = new Map<string,string>();
        for (Support_Request__c support_Request : newMap.values()) {
            if(support_Request.RecordTypeId == '0121E0000004Ag8QAE'){
                if(oldMap != null){
                    if(support_Request.Status__c == 'Approved' && oldMap.get(support_Request.Id).Status__c != 'Approved'){
                        opportunityIdSet.add(support_Request.Opportunity__c);
                        supportIdSet.add(support_Request.Id);
                    }
                }else{
                    if(support_Request.Status__c == 'Approved'){
                        opportunityIdSet.add(support_Request.Opportunity__c);
                        supportIdSet.add(support_Request.Id);
                    }
                }
            }
        }
        
        //update opportunity
        if(!opportunityIdSet.isEmpty()){
            for(Support_Request__c SR : [Select Id, Alternate_Delivery_Office__c, Alternate_Delivery_Type__c, Alternate_Delivery_Positions_approved__c, Status__c,Opportunity__c 
                                         from Support_Request__c 
                                         where Opportunity__c IN : opportunityIdSet]){
                if(SR.Status__c == 'Approved'){
                    if(SR.Alternate_Delivery_Positions_approved__c != null){
                        DeliveryCenterList.add(SR);
                        DeliveryCenterDetailed.put(SR.Opportunity__c,GenerateJSON(DeliveryCenterList));
                    }
                }
            }
        }
        if(!supportIdSet.isEmpty()){
            for(Opportunity newOppty : [select id, Delivery_Model__c, Req_RRC_Positions__c, Delivery_Office__c, (select id, toLabel(Alternate_Delivery_Office__c), Alternate_Delivery_Type__c, Alternate_Delivery_Positions_approved__c, status__c,Opportunity__c from Support_Requests__r where id IN : supportIdSet) from Opportunity where id IN : opportunityIdSet]){
                Decimal sharedPosition = 0;
                for(Support_Request__c sp : newOppty.Support_Requests__r){
                    if(sp.Status__c == 'Approved'){
                        if(sp.Alternate_Delivery_Positions_approved__c != null){
                            sharedPosition = sharedPosition + sp.Alternate_Delivery_Positions_approved__c;
                        }
                        if(newOppty.Delivery_Model__c !=null){
                            list<string> DM = newOppty.Delivery_Model__c.split(';');                    
                            for(string s:DM){
                                ADOffice.put(s,s);
                            }
                        }
                        if(newOppty.Delivery_Model__c != null){
                            if(!ADOffice.containskey(sp.Alternate_Delivery_Type__c) && sp.Alternate_Delivery_Type__c!=null){
                                newOppty.Delivery_Model__c = newOppty.Delivery_Model__c + ';' + sp.Alternate_Delivery_Type__c;
                            }else{
                                //do nothing
                            }
                        }else{    
                            newOppty.Delivery_Model__c = sp.Alternate_Delivery_Type__c;
                        }
                        if(newOppty.Delivery_Office__c!=null){
                            list<string> DMs = newOppty.Delivery_Office__c.split(';');
                            for(string d:DMs){
                                DOffice.put(d,d);
                            }
                        }
                        if(newOppty.Delivery_Office__c != null){
                            if(!DOffice.containskey(sp.Alternate_Delivery_Office__c) && sp.Alternate_Delivery_Office__c !=null){
                                newOppty.Delivery_Office__c = newOppty.Delivery_Office__c + ';' + sp.Alternate_Delivery_Office__c;
                            }else{
                                //do nothing
                            }
                        }else{
                            newOppty.Delivery_Office__c = sp.Alternate_Delivery_Office__c;  
                        }
                    }
                }
                if(newOppty.Req_RRC_Positions__c != null)
                    newOppty.Req_RRC_Positions__c = newOppty.Req_RRC_Positions__c + sharedPosition;
                else
                    newOppty.Req_RRC_Positions__c = sharedPosition;   
                if(DeliveryCenterDetailed.containskey(newOppty.Id))
                    newOppty.Delivery_Center_Detailed__c = DeliveryCenterDetailed.get(newOppty.Id);
                lstOpportunityToUpdate.add(newOppty);
                
            }
        }
        try{   
            if(lstOpportunityToUpdate.size() > 0){
                update lstOpportunityToUpdate;
            }
        }
        catch(exception ex){
             newMap.values()[0].addError(ex.getMessage());
        }    
    }*/
	public static Map<string,string> returnPickListMap(String objectName, String fieldName){
        Map<String,String> picklistValueMap = new Map<String,String>();
        Schema.SObjectType sO = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = sO.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
            picklistValueMap.put(pickListVal.getLabel(),pickListVal.getValue());                
        }
        return picklistValueMap;
    }
    public static String GenerateJSON(List<Support_Request__c> SupportRequestList){
		Map<String, String> jsonImpl = new Map<String, String>();
        String DeliveryCenterJson = '';
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject();
        gen.writeFieldName('DeliveryCenters');
        gen.writeStartArray(); 
        if(!SupportRequestList.isEmpty()){
			jsonImpl = returnPickListMap('Opportunity','Delivery_Office__c');
            for(Support_Request__c SR : SupportRequestList){
                if(jsonImpl.containskey(SR.Alternate_Delivery_Office__c)){
                    gen.writeStartObject();
                    gen.writeNumberField('Commits',SR.Alternate_Delivery_Positions_approved__c);                
                    gen.writeStringField('DeliveryCenterID',jsonImpl.get(SR.Alternate_Delivery_Office__c));
                    gen.writeStringField('DeliveryCenterName',SR.Alternate_Delivery_Office__c);
                    gen.writeEndObject();
                }
            }
            gen.writeEndArray();
            gen.writeEndObject();
            DeliveryCenterJson = gen.getAsString();
        }
        system.debug('--DeliveryCenterJson--'+DeliveryCenterJson);
        return DeliveryCenterJson;
    }
     public static void updateOpportunityFieldsForADCS(List<Support_Request__c> newSupportRequestList,Map<Id,Support_Request__c> oldSupportRequestMap)
    {
        Map<String, String> jsonImpl = new Map<String, String>();
        jsonImpl = returnPickListMap('Opportunity','Delivery_Office__c');
		String ADCSRecordTypeId =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Delivery Center').getRecordTypeId();
        String ADCRecordTypeId =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Aerotek Delivery Center').getRecordTypeId();
        
        List<Opportunity> opportunityRecordsToUpdate = new List<Opportunity>();
        Set<ID> opportunityIdSet = new Set<ID>();  
        list<Support_Request__c> DeliveryCenterList = new list<Support_Request__c>();
        Map<string,string> DeliveryCenterDetailed = new Map<string,string>();
        for(Support_Request__c supportRequestRecord : newSupportRequestList) 
        {
            if(supportRequestRecord.Status__c == 'Approved' &&
               (supportRequestRecord.RecordTypeId == ADCSRecordTypeId || supportRequestRecord.RecordTypeId == ADCRecordTypeId))
            {
                if(Trigger.isInsert || Trigger.isDelete)
                {
                    opportunityIdSet.add(supportRequestRecord.Opportunity__c);
                }
                if(Trigger.isUpdate)
                {
                    if(supportRequestRecord.Alternate_Delivery_Office__c != oldSupportRequestMap.get(supportRequestRecord.Id).Alternate_Delivery_Office__c ||
                       supportRequestRecord.Alternate_Delivery_Positions_approved__c != oldSupportRequestMap.get(supportRequestRecord.Id).Alternate_Delivery_Positions_approved__c ||
                       supportRequestRecord.Alternate_Delivery_Type__c != oldSupportRequestMap.get(supportRequestRecord.Id).Alternate_Delivery_Type__c)
                    {
                        opportunityIdSet.add(supportRequestRecord.Opportunity__c);
                    }
                }
            }
        }
        System.debug('opportunityIdSet:'+opportunityIdSet);
        if(!opportunityIdSet.isEmpty())
        {
            
            for(Support_Request__c SR : [Select Id, TOLABEL(Alternate_Delivery_Office__c), Alternate_Delivery_Type__c, Alternate_Delivery_Positions_approved__c, Status__c,Opportunity__c 
                                         from Support_Request__c 
                                         where Opportunity__c IN : opportunityIdSet]){
                if(SR.Status__c == 'Approved'){
                    if(SR.Alternate_Delivery_Positions_approved__c != null){
                        DeliveryCenterList.add(SR);
                        DeliveryCenterDetailed.put(SR.Opportunity__c,GenerateJSON(DeliveryCenterList));
                    }
                }
            }
            for(Opportunity opptyRecord:[SELECT Id,Delivery_Model__c,Req_RRC_Positions__c,Delivery_Office__c,RecordTypeId,OpCo__c, 
                                        (SELECT id,Alternate_Delivery_Open_Date__c,Comments_Reason_for_Compensations__c,
                                         Alternate_Delivery_Shared_Positions__c,TOLABEL(Alternate_Delivery_Office__c),
                                         Alternate_Delivery_Positions_approved__c,Alternate_Delivery_Type__c
                                         FROM Support_Requests__r) 
                                         FROM Opportunity WHERE id IN : opportunityIdSet])
            {
                Decimal sharedPositionsForPrimary = 0;
                //Decimal sharedPositionForSecondary = 0;
                Set<String> deliveryOfficeSet = new Set<String>();
                Set<String> alternateDeliveryTypeSet = new Set<String>();
                String deliveryOffice = '';
                String alternateDeliveryType = '';
                String comments = '';
                Opportunity opptyRecordToUpdate = new Opportunity();
                opptyRecordToUpdate.Id = opptyRecord.Id;
                for(Support_Request__c supportRequestRecord:opptyRecord.Support_Requests__r)
                {
                    if(supportRequestRecord.Alternate_Delivery_Positions_approved__c != NULL)
                    {
                        sharedPositionsForPrimary = sharedPositionsForPrimary + supportRequestRecord.Alternate_Delivery_Positions_approved__c;
                    }
                    /*if(supportRequestRecord.Alternate_Delivery_Shared_Positions__c != NULL)
                    {
                        sharedPositionForSecondary = sharedPositionForSecondary + supportRequestRecord.Alternate_Delivery_Shared_Positions__c;
                    }*/
                    if(supportRequestRecord.Alternate_Delivery_Office__c != NULL && 
                       !deliveryOfficeSet.contains(supportRequestRecord.Alternate_Delivery_Office__c))
                    {
                        deliveryOfficeSet.add(supportRequestRecord.Alternate_Delivery_Office__c);
                        deliveryOffice = deliveryOffice + ';' + jsonImpl.get(supportRequestRecord.Alternate_Delivery_Office__c);
                    }
                    if(supportRequestRecord.Alternate_Delivery_Type__c != NULL && 
                       !alternateDeliveryTypeSet.contains(supportRequestRecord.Alternate_Delivery_Type__c))
                    {
                        alternateDeliveryTypeSet.add(supportRequestRecord.Alternate_Delivery_Type__c);
                        alternateDeliveryType = alternateDeliveryType + ';' + supportRequestRecord.Alternate_Delivery_Type__c;
                    }                    
                    if(Trigger.isInsert || Trigger.isDelete)
                    {
                        if(comments != NULL && supportRequestRecord.Comments_Reason_for_Compensations__c != NULL &&
                           (comments.length() + supportRequestRecord.Comments_Reason_for_Compensations__c.length()) < Opportunity.Comments__c.getDescribe().getLength())
                        {
                            comments = comments + ';' + supportRequestRecord.Comments_Reason_for_Compensations__c;
                        }
                    }
                }
                if(comments != NULL)
                {
                    comments = comments.removeStart(';');
                }
                if(opptyRecord.OpCo__c == 'TEKsystems, Inc.' || opptyRecord.OpCo__c == 'AG_EMEA')
                {
                    System.debug('In Req Record Type');
                    opptyRecordToUpdate.Delivery_Office__c = deliveryOffice;
                    opptyRecordToUpdate.Req_RRC_Positions__c = sharedPositionsForPrimary;
                    opptyRecordToUpdate.Comments__c= comments;
		    if(DeliveryCenterDetailed.containskey(opptyRecordToUpdate.Id))
                        opptyRecordToUpdate.Delivery_Center_Detailed__c = DeliveryCenterDetailed.get(opptyRecordToUpdate.Id);
		    if(Trigger.isInsert)
                    {
                        opptyRecordToUpdate.Comments__c= comments;
                        if(opptyRecord.Support_Requests__r.size() == 1)
                        {
                            opptyRecordToUpdate.Req_RRC_Open_Date__c = opptyRecord.Support_Requests__r[0].Alternate_Delivery_Open_Date__c;
                        }
                    }
                    if(Trigger.isDelete && opptyRecord.Support_Requests__r.size() == 0)
                    {
                        opptyRecordToUpdate.Req_RRC_Open_Date__c = NULL;
                        opptyRecordToUpdate.Comments__c= NULL;
                        opptyRecordToUpdate.Req_RRC_Positions__c = 0;
                        //if(DeliveryCenterDetailed.containskey(opptyRecordToUpdate.Id))
                        opptyRecordToUpdate.Delivery_Center_Detailed__c = NULL;// DeliveryCenterDetailed.get(opptyRecordToUpdate.Id);
                        opptyRecordToUpdate.Delivery_Office__c  = NULL;
                    }
                }
                else if(opptyRecord.OpCo__c == 'Aerotek, Inc')
                {    
                    System.debug('In Strategic Record Type');
                    opptyRecordToUpdate.Req_RRC_Positions__c = sharedPositionsForPrimary;
                    opptyRecordToUpdate.Delivery_Office__c = deliveryOffice;
                    opptyRecordToUpdate.Delivery_Model__c = alternateDeliveryType;
                    if(DeliveryCenterDetailed.containskey(opptyRecordToUpdate.Id))
                        opptyRecordToUpdate.Delivery_Center_Detailed__c = DeliveryCenterDetailed.get(opptyRecordToUpdate.Id);
                    if(Trigger.isInsert && opptyRecord.Support_Requests__r.size() == 1)
                    {
                        opptyRecordToUpdate.Req_RRC_Open_Date__c = opptyRecord.Support_Requests__r[0].Alternate_Delivery_Open_Date__c;
                    }
                    if(Trigger.isDelete && opptyRecord.Support_Requests__r.size() == 0)
                    {
                        opptyRecordToUpdate.Req_RRC_Positions__c = 0;
                        //if(DeliveryCenterDetailed.containskey(opptyRecordToUpdate.Id))
                        opptyRecordToUpdate.Delivery_Center_Detailed__c = NULL; //DeliveryCenterDetailed.get(opptyRecordToUpdate.Id);
                        opptyRecordToUpdate.Delivery_Office__c = NULL;
                        opptyRecordToUpdate.Delivery_Model__c = NULL;                       
                    }
                }
                opportunityRecordsToUpdate.add(opptyRecordToUpdate);
            }
            System.debug('opportunityRecordsToUpdate:'+opportunityRecordsToUpdate);
        }
        update opportunityRecordsToUpdate;
        /*try
        {        
            if(opportunityRecordsToUpdate.size() > 0)
            {
                update opportunityRecordsToUpdate;
            }
        }
        catch(exception ex)
        {
             System.debug('Exception:'+ex.getMessage());
        }  */  
    }

    /*
//list of records to be assigned to queue.
List<Support_Request__c> supportReq = new List<Support_Request__c>();
//create a list of record types.
List<String> queueList = new List<String>();
queueList.add('Finance Support Request');
queueList.add('Proposal Support Request');
queueList.add('Solution Support Request');
queueList.add('Compensation Request');
queueList.add('Escalaton Request');
Map<String,Id> groupMap = new Map<String,Id>();
Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Support_Request__c.getRecordTypeInfosById();
//query to fetch the respective queue and ids.
for(Group grp : [Select Id,Name from Group Where Type='Queue' and Name IN : queueList]){
groupMap.put(grp.Name,grp.Id);
}
//iterate the records and assign the request to queue per the record types.
for(Support_Request__c request : newRecords){
System.debug('request.RecordType.Name=='+request.RecordType.Name);
if(rtMap.get(request.RecordTypeId).getName() == 'Compensation') {
request.ownerId = groupMap.get('Compensation Request');
}if(rtMap.get(request.RecordTypeId).getName() == 'Finance') {
request.ownerId = groupMap.get('Finance Support Request');
}if(rtMap.get(request.RecordTypeId).getName() == 'Proposal') {
request.ownerId = groupMap.get('Proposal Support Request');
}if(rtMap.get(request.RecordTypeId).getName() == 'Solution') {
request.ownerId = groupMap.get('Solution Support Request');
}if(rtMap.get(request.RecordTypeId).getName() == 'SS&O') {
request.ownerId = groupMap.get('Solution Support Request');
}
supportReq.add(request);
}

//insert supportReq;

*/    
}