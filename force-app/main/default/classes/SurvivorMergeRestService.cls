@RestResource(urlMapping='/Person/SurvivorMerge/V1')
global without sharing class SurvivorMergeRestService {
   
    @HttpPost
    global static void matchAndMergeTalents(String Id1,String Id2) 
    {   
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        Map<String, Object> responseData = new Map<String, Object>();
        String param1 = Id1; //params.ID1;
        String param2 = Id2; //params.ID2;
        System.debug(param1);
        System.debug(param2);
        List<Contact> talents = ATSSurvivorshipMatch.getContactDetails(param1,param2);
        Map<String, String> paramMap = new Map<String, String>();
        Map<String, String> candidateIdMap = new Map<String, String>();
        try{
            	for (Contact c : talents) {
                    if (c.Id == param1 || c.AccountId == param1) {
                        paramMap.put(c.Id, param1);
                    } else if (c.Id == param2 || c.AccountId == param2) {
                        paramMap.put(c.Id, param2);
                    }
            	}
                Contact talent1 = talents[0];
                Contact talent2 = talents[1];
            	candidateIdMap.put(talent1.Id, talent1.Talent_Id__c);
            	candidateIdMap.put(talent2.Id, talent2.Talent_Id__c);
            	candidateIdMap.put(talent1.AccountId, talent1.Talent_Id__c);
            	candidateIdMap.put(talent2.AccountId, talent2.Talent_Id__c);
            	responseData = (Map<String, Object>)JSON.deserializeUntyped(ATSSurvivorshipMatch.preCheckRules(talent1,talent2, paramMap));
           }catch(Exception ex){
                responseData.put('Message',ex.getMessage());
                responseData.put('Success','false');
                response.responseBody = Blob.valueOf(JSON.serialize(responseData));
           }
         
            if(responseData.containsKey('Success') && responseData.get('Success') == 'true'){
                String MasterID = (String) responseData.get('MasterID');
				String VictimID = (String) responseData.get('VictimID');
                MasterID = candidateIdMap.get(MasterID);
                VictimID = candidateIdMap.get(VictimID);
                responseData = new Map<String, Object>();
                responseData = (Map<String, Object>)JSON.deserializeUntyped(TalentMergeController.talentMerge(MasterID,VictimID));
                if(responseData.get('Success') == 'true'){
                    responseData.put('Message','Survivor and Master have been identified and Merged Successfully');
                }
                response.responseBody = Blob.valueOf(JSON.serialize(responseData)); 
            }
     }
    
    }