public class SyncReqRWSOpportunityController
{
     private final ApexPages.StandardController controller;
     public string url {get;set;}
     
   
    public SyncReqRWSOpportunityController(ApexPages.StandardController controller)
    {
        this.controller = controller;
        url = '';
    }
    
    public void onLoad()
    
    {
    Set<Id> setoppIds = new Set<Id>();
        //System.debug('Inside onLoad method');
         Opportunity opp = [Select id,OpCo__c,Req_origination_System_Id__c,Req_Origination_Partner_System_Id__c From Opportunity Where Id = :ApexPages.currentPage().getParameters().get('id')];
         
         string oppid= ApexPages.currentPage().getParameters().get('id');
         setoppIds.add(oppid);
         if(opp.OpCo__c == 'TEKsystems, Inc.' || opp.OpCo__c == 'Aerotek, Inc')
         {
             System.debug('Inside if condition');
             
              EnterprisetolegacyUtil reqUtil = new EnterprisetolegacyUtil(setoppIds);
                  reqUtil.CopyEnterprisetoLegacy();
                  url='Sucessfully Synched';
             
             //url = '/apex/IntegrationOpenPage?searchType=RWS_VIEW_REQ&OriginationSystemId=' + opp.Req_origination_System_Id__c +'&OriginationSystem='+ opp.Req_Origination_Partner_System_Id__c;
         }
         
         
         
         //System.debug('url:::: ' + url);
    }
}