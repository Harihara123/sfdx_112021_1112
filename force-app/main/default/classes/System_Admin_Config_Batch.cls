global class System_Admin_Config_Batch implements Database.Batchable<Sobject> ,Database.stateful{
	
    global String strQuery;
    global String ParamToUpdate;
    global String EntryParam;
    global Integer SequenceNum = 0;
    global System_Admin_Config_Batch(Integer ObjSequenceNumber){
        SequenceNum = ObjSequenceNumber;        
        system.debug('--SequenceNum in Contst--'+SequenceNum);
        if(ObjSequenceNumber != null && ObjSequenceNumber != 0){
            for(System_Admin_Config__c x:[Select id, Name, Entry_Parameter__c, Parameter_to_Update__c, SOQL_Query__c 
                                          from System_Admin_Config__c 
                                          where Sequence_Number__c =:SequenceNum]){
                EntryParam = x.Entry_Parameter__c;
                ParamToUpdate = x.Parameter_to_Update__c;
                strQuery = x.SOQL_Query__c;
            }
        }
        system.debug('--strQuery--'+strQuery);
    }
    global database.Querylocator start(Database.BatchableContext context){
    	return Database.getQueryLocator(strQuery);   
    }
    global void execute(Database.BatchableContext context , list<User> scope){
        system.debug('--EntryParam--'+EntryParam);
        system.debug('--ParamToUpdate--'+ParamToUpdate);
        List<CollaborationGroupMember> CollaborationGroupMembers = new List<CollaborationGroupMember>();
        List<GroupMember> GroupMemberlist = new List<GroupMember>();
        List<PermissionSetAssignment> PermList = new list<PermissionSetAssignment>();
        try{
            if(!scope.isEmpty()){
                for(User x:scope){
                    if(EntryParam == 'Chatter Group'){
                        CollaborationGroupMember member = new CollaborationGroupMember();
                        member.MemberId = x.Id ;
                        member.CollaborationGroupId = id.valueof(ParamToUpdate);
                        CollaborationGroupMembers.add(member);
                    }else if(EntryParam == 'Public Group'){
                        GroupMember GM = new GroupMember();
                        GM.GroupId = id.valueof(ParamToUpdate);
                        GM.UserOrGroupId = x.Id;
                        GroupMemberlist.add(GM);   
                    }else if(EntryParam == 'Permissionset'){
                        PermissionSetAssignment psa = new PermissionSetAssignment();
                        psa.PermissionSetId = id.valueof(ParamToUpdate);
                        psa.AssigneeId = x.Id;
                        PermList.add(psa);
                    }                    
                }
                //SequenceNum = SequenceNum+1;
            }
            if(!CollaborationGroupMembers.isEmpty()){
                database.insert(CollaborationGroupMembers,false);
            }
            if(!GroupMemberlist.isEmpty()){
                database.insert(GroupMemberlist,false);
            }
            if(!PermList.isEmpty()){
                database.insert(PermList,false);
            }
        }catch(Exception ex){
            system.debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());            
        }
    }
    global void finish(Database.BatchableContext context){
        SequenceNum = SequenceNum+1;
    	system.debug('--SequenceNum in finish--'+SequenceNum);
        if(SequenceNum !=0 && SequenceNum !=null){
            list<System_Admin_Config__c> SAC = [Select id from System_Admin_Config__c where Sequence_Number__c =:SequenceNum];
            if(!SAC.isEmpty()){
                System_Admin_Config_Batch batchJob = new System_Admin_Config_Batch(SequenceNum);
                database.executebatch(batchJob);
            }
        }
    }
}