global class System_Admin_Config_Schedule implements Schedulable{
	global Integer ObjSequenceNumber;
    global System_Admin_Config_Schedule(Integer ObjSequenceNumber){
        this.ObjSequenceNumber = ObjSequenceNumber;
    }
    global void execute(SchedulableContext sc){        
		System_Admin_Config_Batch batchJob = new System_Admin_Config_Batch(ObjSequenceNumber);
        database.executebatch(batchJob);
	}
}