public class TC_userManagementController {

    @AuraEnabled(cacheable=true)
    public static Boolean IsCommunityManager(){
        Boolean isCommunityManager = false;
        List<User>loggedinUserInfo = new List<User>();
       
        String loggedinUserId = String.valueOf(userInfo.getUserId());
        List<PermissionSetAssignment> loggedinUserPermissionSet = new List<PermissionSetAssignment>();
        if(!String.isBlank(loggedinUserId)){
            loggedinUserPermissionSet =[SELECT AssigneeId, PermissionSetId ,PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name LIKE '%Community_Manager%' AND AssigneeId =: loggedinUserId];
            
            if(!loggedinUserPermissionSet.isEmpty()){
                isCommunityManager = true;
            }
        }
        return isCommunityManager;
    }

    @AuraEnabled(cacheable=false)
    public static List<userManagementLWCWrapper> GetKeyContactIdsRunningUser(String strAccName, string pickListValue) {
        List<userManagementLWCWrapper>wrapperLst = new List<userManagementLWCWrapper>();
        List<User>loggedinUserInfo = new List<User>();
        List<String> loggedinUserOpcolst = new List<String>();
        String loggedinUserOpco;
        String loggedinUserId = String.valueOf(userInfo.getUserId());
        List<PermissionSetAssignment> loggedinUserPermissionSet = new List<PermissionSetAssignment>();
        if(!String.isBlank(loggedinUserId)){
            loggedinUserInfo = [Select Id, Name,Email,Peoplesoft_id__c, Opco__c from user where Id =: loggedinUserId];
            loggedinUserPermissionSet =[SELECT AssigneeId, PermissionSetId ,PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name LIKE '%Community_Manager%' AND AssigneeId =: loggedinUserId];
            loggedinUserOpco = loggedinUserInfo[0].Opco__c;
        }
        
        if(!loggedinUserInfo.isEmpty()){
            loggedinUserOpcolst.add(loggedinUserInfo[0].opco__c);
            String OpCoList = System.Label.TC_usermnagementOpcolst;
        	List<String> OpCos = OpCoList.split(';');
            if(OpCoList.contains(loggedinUserInfo[0].opco__c)){
                loggedinUserOpcolst.add('TEK');
                loggedinUserOpcolst.add('ONS');
                loggedinUserOpcolst.add('AG_EMEA');
            }
        }
        List<User> usrLst = new List<User>();
        String userType ='CspLitePortal';
        System.debug('---------'+pickListValue);
        string nameStr = '%' + strAccName + '%';
        
        if(!loggedinUserPermissionSet.isEmpty()){
            if(!String.isBlank(pickListValue) && pickListValue =='All Users'){
            usrLst = [Select Id, AccountId, FirstName, LastName, ContactId,Name, Peoplesoft_Id__c,Account.Talent_End_Date__c,Opco__c, email,Phone,IsActive,Office_Code__c,Account.Talent_Finish_Code__c,Username,Talent_Active_Status_Override__c
                      ,Reason_for_Override__c, LastLoginDate,Office__c,CreatedDate,Region__c,Contact.HomePhone,Contact.TC_Mailing_Address__c,Account.Talent_End_Date_New__c, Account.Talent_Start_Date__c
                      ,Account.Talent_Recruiter__c ,Account.Talent_Account_Manager__c ,Account.Talent_Community_Manager__c ,Account.Talent_CSA__c,Account.Talent_Employee_Service_Specialist__c
                      ,Account.Talent_Biller__c ,Account.Talent_Community_Rep__c, Account.Talent_Program_Manager__c,Account.Talent_Delivery_Manager__c
                      ,Account.Talent_Biller__r.Name,Account.Talent_Community_Manager__r.Name,Account.Talent_Recruiter__r.Name,Account.Talent_Account_Manager__r.Name,Account.Talent_CSA__r.Name
                      ,Account.Talent_Community_Rep__r.Name,Account.Talent_Program_Manager__r.Name,Account.Talent_Delivery_Manager__r.Name,Account.Talent_Employee_Service_Specialist__r.Name
                      ,Account.Talent_Biller__r.isActive,Account.Talent_Community_Manager__r.isActive,Account.Talent_Recruiter__r.isActive,Account.Talent_Account_Manager__r.isActive,Account.Talent_CSA__r.isActive
                      ,Account.Talent_Community_Rep__r.isActive,Account.Talent_Program_Manager__r.isActive,Account.Talent_Delivery_Manager__r.isActive,Account.Talent_Employee_Service_Specialist__r.isActive
                      ,Account.Talent_Biller__r.Id,Account.Talent_Community_Manager__r.Id,Account.Talent_Recruiter__r.Id,Account.Talent_Account_Manager__r.Id,Account.Talent_CSA__r.Id
                      ,Account.Talent_Community_Rep__r.Id,Account.Talent_Program_Manager__r.Id,Account.Talent_Delivery_Manager__r.Id,Account.Talent_Employee_Service_Specialist__r.Id
                      from user where (peoplesoft_Id__c =: strAccName OR Name =: strAccName OR FirstName =: strAccName OR LastName =: strAccName OR Email =:strAccName) AND UserType =: userType AND Opco__c IN: loggedinUserOpcolst];
            }else if(!String.isBlank(pickListValue) && pickListValue =='My Users'){
                
            usrLst = [Select Id, AccountId, FirstName, LastName,ContactId,Name, Peoplesoft_Id__c,Account.Talent_End_Date__c,Opco__c, email,Phone,IsActive,Office_Code__c,Account.Talent_Finish_Code__c,Username,Talent_Active_Status_Override__c
                      ,Reason_for_Override__c, LastLoginDate,Office__c,CreatedDate,Region__c,Contact.HomePhone,Contact.TC_Mailing_Address__c,Account.Talent_End_Date_New__c, Account.Talent_Start_Date__c
                      ,Account.Talent_Recruiter__c ,Account.Talent_Account_Manager__c ,Account.Talent_Community_Manager__c ,Account.Talent_CSA__c,Account.Talent_Employee_Service_Specialist__c
                      ,Account.Talent_Biller__c ,Account.Talent_Community_Rep__c, Account.Talent_Program_Manager__c,Account.Talent_Delivery_Manager__c
                      ,Account.Talent_Biller__r.Name,Account.Talent_Community_Manager__r.Name,Account.Talent_Recruiter__r.Name,Account.Talent_Account_Manager__r.Name,Account.Talent_CSA__r.Name
                      ,Account.Talent_Community_Rep__r.Name,Account.Talent_Program_Manager__r.Name,Account.Talent_Delivery_Manager__r.Name,Account.Talent_Employee_Service_Specialist__r.Name
                      ,Account.Talent_Biller__r.IsActive,Account.Talent_Community_Manager__r.isActive,Account.Talent_Recruiter__r.isActive,Account.Talent_Account_Manager__r.isActive,Account.Talent_CSA__r.isActive
                      ,Account.Talent_Community_Rep__r.isActive,Account.Talent_Program_Manager__r.isActive,Account.Talent_Delivery_Manager__r.isActive,Account.Talent_Employee_Service_Specialist__r.isActive
                      ,Account.Talent_Biller__r.Id,Account.Talent_Community_Manager__r.Id,Account.Talent_Recruiter__r.Id,Account.Talent_Account_Manager__r.Id,Account.Talent_CSA__r.Id
                      ,Account.Talent_Community_Rep__r.Id,Account.Talent_Program_Manager__r.Id,Account.Talent_Delivery_Manager__r.Id,Account.Talent_Employee_Service_Specialist__r.Id
                      from user where ((peoplesoft_Id__c =: strAccName OR Name =: strAccName OR FirstName =: strAccName OR LastName =: strAccName OR Email =:strAccName) AND UserType =: userType AND Opco__c IN:loggedinUserOpcolst AND
                      (Account.Talent_Community_Manager__r.Id =: loggedinUserId OR Account.Talent_Account_Manager__r.Id =: loggedinUserId OR Account.Talent_Recruiter__r.Id =: loggedinUserId))];
            }
        }else{
          if(!String.isBlank(pickListValue) && pickListValue =='All Users'){
            usrLst = [Select Id, AccountId, FirstName, LastName, ContactId,Name, Peoplesoft_Id__c,Account.Talent_End_Date__c,Opco__c, email,Phone,IsActive,Office_Code__c,Account.Talent_Finish_Code__c,Username,Talent_Active_Status_Override__c
                      ,Reason_for_Override__c, LastLoginDate,Office__c,CreatedDate,Region__c,Contact.HomePhone,Contact.TC_Mailing_Address__c,Account.Talent_End_Date_New__c, Account.Talent_Start_Date__c
                      ,Account.Talent_Recruiter__c ,Account.Talent_Account_Manager__c ,Account.Talent_Community_Manager__c ,Account.Talent_CSA__c,Account.Talent_Employee_Service_Specialist__c
                      ,Account.Talent_Biller__c ,Account.Talent_Community_Rep__c, Account.Talent_Program_Manager__c,Account.Talent_Delivery_Manager__c
                      ,Account.Talent_Biller__r.Name,Account.Talent_Community_Manager__r.Name,Account.Talent_Recruiter__r.Name,Account.Talent_Account_Manager__r.Name,Account.Talent_CSA__r.Name
                      ,Account.Talent_Community_Rep__r.Name,Account.Talent_Program_Manager__r.Name,Account.Talent_Delivery_Manager__r.Name,Account.Talent_Employee_Service_Specialist__r.Name
                      ,Account.Talent_Biller__r.isActive,Account.Talent_Community_Manager__r.isActive,Account.Talent_Recruiter__r.isActive,Account.Talent_Account_Manager__r.isActive,Account.Talent_CSA__r.isActive
                      ,Account.Talent_Community_Rep__r.isActive,Account.Talent_Program_Manager__r.isActive,Account.Talent_Delivery_Manager__r.isActive,Account.Talent_Employee_Service_Specialist__r.isActive
                      ,Account.Talent_Biller__r.Id,Account.Talent_Community_Manager__r.Id,Account.Talent_Recruiter__r.Id,Account.Talent_Account_Manager__r.Id,Account.Talent_CSA__r.Id
                      ,Account.Talent_Community_Rep__r.Id,Account.Talent_Program_Manager__r.Id,Account.Talent_Delivery_Manager__r.Id,Account.Talent_Employee_Service_Specialist__r.Id
                      from user where peoplesoft_Id__c =: strAccName AND UserType =: userType AND Opco__c IN: loggedinUserOpcolst];
            }else if(!String.isBlank(pickListValue) && pickListValue =='My Users'){
                
            usrLst = [Select Id, AccountId, FirstName, LastName, ContactId,Name, Peoplesoft_Id__c,Account.Talent_End_Date__c,Opco__c, email,Phone,IsActive,Office_Code__c,Account.Talent_Finish_Code__c,Username,Talent_Active_Status_Override__c
                      ,Reason_for_Override__c, LastLoginDate,Office__c,CreatedDate,Region__c,Contact.HomePhone,Contact.TC_Mailing_Address__c,Account.Talent_End_Date_New__c, Account.Talent_Start_Date__c
                      ,Account.Talent_Recruiter__c ,Account.Talent_Account_Manager__c ,Account.Talent_Community_Manager__c ,Account.Talent_CSA__c,Account.Talent_Employee_Service_Specialist__c
                      ,Account.Talent_Biller__c ,Account.Talent_Community_Rep__c, Account.Talent_Program_Manager__c,Account.Talent_Delivery_Manager__c
                      ,Account.Talent_Biller__r.Name,Account.Talent_Community_Manager__r.Name,Account.Talent_Recruiter__r.Name,Account.Talent_Account_Manager__r.Name,Account.Talent_CSA__r.Name
                      ,Account.Talent_Community_Rep__r.Name,Account.Talent_Program_Manager__r.Name,Account.Talent_Delivery_Manager__r.Name,Account.Talent_Employee_Service_Specialist__r.Name
                      ,Account.Talent_Biller__r.IsActive,Account.Talent_Community_Manager__r.isActive,Account.Talent_Recruiter__r.isActive,Account.Talent_Account_Manager__r.isActive,Account.Talent_CSA__r.isActive
                      ,Account.Talent_Community_Rep__r.isActive,Account.Talent_Program_Manager__r.isActive,Account.Talent_Delivery_Manager__r.isActive,Account.Talent_Employee_Service_Specialist__r.isActive
                      ,Account.Talent_Biller__r.Id,Account.Talent_Community_Manager__r.Id,Account.Talent_Recruiter__r.Id,Account.Talent_Account_Manager__r.Id,Account.Talent_CSA__r.Id
                      ,Account.Talent_Community_Rep__r.Id,Account.Talent_Program_Manager__r.Id,Account.Talent_Delivery_Manager__r.Id,Account.Talent_Employee_Service_Specialist__r.Id
                      from user where ((peoplesoft_Id__c =: strAccName ) AND UserType =: userType AND Opco__c IN:loggedinUserOpcolst AND
                      (Account.Talent_Community_Manager__r.Id =: loggedinUserId OR Account.Talent_Account_Manager__r.Id =: loggedinUserId OR Account.Talent_Recruiter__r.Id =: loggedinUserId))];
            }    
        }
        
        
        System.debug('---------');
        if(usrLst.isEmpty()) {
            System.debug('---------');
            //throw new AuraHandledException('No Record Found..');
        }
        for(User u:usrLst){
            userManagementLWCWrapper lwcEachWrapper = new userManagementLWCWrapper();
            lwcEachWrapper.Id = u.Id;
            lwcEachWrapper.opco = u.Opco__c;
            lwcEachWrapper.PeopleSoftId = u.Peoplesoft_Id__C;
            lwcEachWrapper.name = u.name;
            lwcEachWrapper.Email = u.email;
            lwcEachWrapper.Phone = u.Phone;
            lwcEachWrapper.FirstName = u.FirstName;
            lwcEachWrapper.LastName = u.LastName;
            lwcEachWrapper.HomePhone= u.Contact.HomePhone;
            lwcEachWrapper.Active = u.IsActive;
            lwcEachWrapper.Officecode = u.Office_Code__c;
            lwcEachWrapper.userId = u.Id;
            lwcEachWrapper.contactId = u.ContactId;
            lwcEachWrapper.accountId = u.AccountId;
            lwcEachWrapper.Finishcode = u.Account.Talent_Finish_Code__c;
            lwcEachWrapper.Username = u.Username;
            lwcEachWrapper.StatusOverride = u.Talent_Active_Status_Override__c;
            lwcEachWrapper.ReasonforOverride = String.isBlank(u.Reason_for_Override__c)?'':u.Reason_for_Override__c;
            lwcEachWrapper.Lastlogin = u.LastLoginDate;
            if(u.LastLoginDate != null ){
                lwcEachWrapper.buttonValue = 'Send Reset Password Link';    
            }else{
                lwcEachWrapper.buttonValue = 'Send Welcome Email Link';    
            }
            if(!loggedinUserPermissionSet.isEmpty()){
                lwcEachWrapper.isCommunityManager = true;
            }else{
                lwcEachWrapper.isCommunityManager = false;    
            }
            lwcEachWrapper.Office = u.Office__c;
            lwcEachWrapper.Createddate = u.CreatedDate;
            lwcEachWrapper.Region = u.Region__c;
            lwcEachWrapper.Address = u.Contact.TC_Mailing_Address__c;
            lwcEachWrapper.Enddate = u.Account.Talent_End_Date__c==null?null:u.Account.Talent_End_Date__c;
            lwcEachWrapper.Startdate = u.Account.Talent_Start_Date__c==null?null:u.Account.Talent_Start_Date__c;
            if(loggedinUserOpco !='GB1'){
                lwcEachWrapper.Recruiter = String.isBlank(u.Account.Talent_Recruiter__r.Name)?'':u.Account.Talent_Recruiter__r.Name;
                lwcEachWrapper.RecruiterId = String.isBlank(u.Account.Talent_Recruiter__r.Id)?'':u.Account.Talent_Recruiter__r.Id;
                lwcEachWrapper.RecruiterActive = u.Account.Talent_Recruiter__r.IsActive;
            }
            lwcEachWrapper.AccountManager = String.isBlank(u.Account.Talent_Account_Manager__r.Name)?'':u.Account.Talent_Account_Manager__r.Name;
            lwcEachWrapper.AccountManagerId = String.isBlank(u.Account.Talent_Account_Manager__r.Id)?'':u.Account.Talent_Account_Manager__r.Id;
            lwcEachWrapper.AccountManagerActive = u.Account.Talent_Account_Manager__r.IsActive;
            lwcEachWrapper.CommunityManager = String.isBlank(u.Account.Talent_Community_Manager__r.Name)?'':u.Account.Talent_Community_Manager__r.Name;
            lwcEachWrapper.CommunityManagerId = String.isBlank(u.Account.Talent_Community_Manager__r.Id)?'':u.Account.Talent_Community_Manager__r.Id;
            lwcEachWrapper.CommunityManagerActive = u.Account.Talent_Community_Manager__r.IsActive;
            lwcEachWrapper.TalentCSA = String.isBlank(u.Account.Talent_CSA__r.Name)?'':u.Account.Talent_CSA__r.Name;
            lwcEachWrapper.TalentCSAId = String.isBlank(u.Account.Talent_CSA__r.Id)?'':u.Account.Talent_CSA__r.Id;
            lwcEachWrapper.CSAactive = u.Account.Talent_CSA__r.IsActive;
            if(loggedinUserOpco =='GB1'){
                lwcEachWrapper.Talentbbiller = String.isBlank(u.Account.Talent_Biller__r.Name)?'':u.Account.Talent_Biller__r.Name;
                lwcEachWrapper.TalentbillerId = String.isBlank(u.Account.Talent_Biller__r.Id)?'':u.Account.Talent_Biller__r.Id;
                lwcEachWrapper.BillerActive = u.Account.Talent_Biller__r.IsActive;
            }
            if(loggedinUserOpco =='ONS'){
                lwcEachWrapper.CommunityRep = String.isBlank(u.Account.Talent_Community_Rep__r.Name)?'':u.Account.Talent_Community_Rep__r.Name;
                lwcEachWrapper.CommunityRepId = String.isBlank(u.Account.Talent_Community_Rep__r.Id)?'':u.Account.Talent_Community_Rep__r.Id;
                lwcEachWrapper.CommunityRepActive = u.Account.Talent_Community_Rep__r.IsActive;
                lwcEachWrapper.TalentProgramManager = String.isBlank(u.Account.Talent_Program_Manager__r.Name)?'':u.Account.Talent_Program_Manager__r.Name;
                lwcEachWrapper.TalentProgramManagerId = String.isBlank(u.Account.Talent_Program_Manager__r.Id)?'':u.Account.Talent_Program_Manager__r.Id;
                lwcEachWrapper.ProgramManagerActive = u.Account.Talent_Program_Manager__r.IsActive;
                lwcEachWrapper.TalentDeliveryManager = String.isBlank(u.Account.Talent_Delivery_Manager__r.Name)?'':u.Account.Talent_Delivery_Manager__r.Name;
                lwcEachWrapper.TalentDeliveryManagerId = String.isBlank(u.Account.Talent_Delivery_Manager__r.Id)?'':u.Account.Talent_Delivery_Manager__r.Id;
                lwcEachWrapper.DeliveryManagerActive = u.Account.Talent_Delivery_Manager__r.IsActive;
                lwcEachWrapper.TalentEmployeeServiceSpecialist = String.isBlank(u.Account.Talent_Employee_Service_Specialist__r.Name)?'':u.Account.Talent_Employee_Service_Specialist__r.Name;
                lwcEachWrapper.TalentEmployeeServiceSpecialistId = String.isBlank(u.Account.Talent_Employee_Service_Specialist__r.Id)?'':u.Account.Talent_Employee_Service_Specialist__r.Id;
                lwcEachWrapper.ESSActive = u.Account.Talent_Employee_Service_Specialist__r.IsActive;
            }
            system.debug('-----------My Wrapper'+lwcEachWrapper);
            wrapperLst.add(lwcEachWrapper);
        }
        return wrapperLst;
    }

    @AuraEnabled
    public static void updateUserRecord(List<userManagementLWCWrapper> wrapperRec, Id userId , Id contactId, Id accountId) {
        System.debug('-------------My New Values'+wrapperRec[0]);
        System.debug('-------------My New Values'+userId);
        System.debug('-------------My New Values'+contactId);
        System.debug('-------------My New Values'+accountId);
        List<User>userLst = new List<User>();
        List<Account>accLst = new List<Account>();
        List<Contact> conlst = new List<Contact>();
        User u = new User();
        u.Talent_Active_Status_Override__c = wrapperRec[0].StatusOverride;
        if(!String.isBlank(wrapperRec[0].Username)){
            u.Username = wrapperRec[0].Username;
        }
            u.IsActive = wrapperRec[0].Active;
        if(!String.isBlank(wrapperRec[0].ReasonforOverride)){
            u.Reason_for_Override__c = wrapperRec[0].ReasonforOverride;
        }
        if(!String.isBlank(wrapperRec[0].FirstName)){
            u.FirstName= wrapperRec[0].FirstName;
        }
        if(!String.isBlank(wrapperRec[0].LastName)){
            u.LastName= wrapperRec[0].LastName;
        }
        if(wrapperRec[0].StatusOverride){
            if(wrapperRec[0].StatusOverride && String.isBlank(wrapperRec[0].ReasonforOverride)){
                u.Reason_for_Override__c = 'This is System stamped Reason to Override';    
            }
        }else{
                u.Reason_for_Override__c = '';    
        }
        u.id = Id.valueOf(userId);
        u.Phone = wrapperRec[0].Phone;
        //u.Peoplesoft_Id__c = wrapperRec[0].PeopleSoftId;
        userLst.add(u);
        List<Database.SaveResult> psupaterresults = Database.update(userLst, false);
        //List<Database.UpsertResult> results = Database.upsert(userLst, User.PeopleSoft_Id__c, false);
        System.debug('--------------- wrapperRec[0].Recruiter'+ wrapperRec[0].Recruiter);
        System.debug('--------------- wrapperRec[0].RecruiterId'+ wrapperRec[0].RecruiterId);
        Map<String,String> keyContactMap = new Map<String,String>();
        if(!String.isBlank(wrapperRec[0].RecruiterId) && wrapperRec[0].RecruiterId.startsWith('005')){
            keyContactMap.Put('Recruiter',wrapperRec[0].RecruiterId);
        }
        System.debug('--------------- wrapperRec[0].AccountManager'+ wrapperRec[0].AccountManager);
        System.debug('--------------- wrapperRec[0].AccountManagerId'+ wrapperRec[0].AccountManagerId);
        if(!String.isBlank(wrapperRec[0].AccountManagerId) && wrapperRec[0].AccountManagerId.startsWith('005')){
            keyContactMap.Put('AccountManager',wrapperRec[0].AccountManagerId);
        }
        System.debug('--------------- wrapperRec[0].CommunityManager'+ wrapperRec[0].CommunityManager);
        if(!String.isBlank(wrapperRec[0].CommunityManagerId) && wrapperRec[0].CommunityManagerId.startsWith('005')){
            keyContactMap.Put('CommunityManager',wrapperRec[0].CommunityManagerId);
        }
        System.debug('--------------- wrapperRec[0].TalentCSA'+ wrapperRec[0].TalentCSA);
        if(!String.isBlank(wrapperRec[0].TalentCSAId) && wrapperRec[0].TalentCSAId.startsWith('005')){
            keyContactMap.Put('TalentCSA',wrapperRec[0].TalentCSAId);
        }
        System.debug('--------------- wrapperRec[0].Talentbbiller'+ wrapperRec[0].Talentbbiller);
        if(!String.isBlank(wrapperRec[0].TalentbbillerId) && wrapperRec[0].TalentbbillerId.startsWith('005')){
            keyContactMap.Put('Talentbbiller',wrapperRec[0].TalentbbillerId);
        }
        System.debug('--------------- wrapperRec[0].CommunityRep'+ wrapperRec[0].CommunityRep);
        if(!String.isBlank(wrapperRec[0].CommunityRepId) && wrapperRec[0].CommunityRepId.startsWith('005')){
            keyContactMap.Put('CommunityRep',wrapperRec[0].CommunityRepId);
        }
        System.debug('--------------- wrapperRec[0].TalentProgramManager'+ wrapperRec[0].TalentProgramManager);
        if(!String.isBlank(wrapperRec[0].TalentProgramManagerId) && wrapperRec[0].TalentProgramManagerId.startsWith('005')){
            keyContactMap.Put('TalentProgramManager',wrapperRec[0].TalentProgramManagerId);
        }
        System.debug('--------------- wrapperRec[0].TalentDeliveryManager'+ wrapperRec[0].TalentDeliveryManager);
        if(!String.isBlank(wrapperRec[0].TalentDeliveryManagerId) && wrapperRec[0].TalentDeliveryManagerId.startsWith('005')){
            keyContactMap.Put('TalentDeliveryManager',wrapperRec[0].TalentDeliveryManagerId);
        }
        System.debug('--------------- wrapperRec[0].TalentEmployeeServiceSpecialist'+ wrapperRec[0].TalentEmployeeServiceSpecialist);
        if(!String.isBlank(wrapperRec[0].TalentEmployeeServiceSpecialistId) && wrapperRec[0].TalentEmployeeServiceSpecialistId.startsWith('005')){
            keyContactMap.Put('TalentEmployeeServiceSpecialist',wrapperRec[0].TalentEmployeeServiceSpecialistId);
        }
        
        Account a = new Account();
        if(keyContactMap.keySet().contains('Recruiter')){
            a.Talent_Recruiter__c = keyContactMap.get('Recruiter');    
        }
        if(keyContactMap.keySet().contains('AccountManager')){
            a.Talent_Account_Manager__c = keyContactMap.get('AccountManager');    
        }
        if(keyContactMap.keySet().contains('CommunityManager')){
            a.Talent_Community_Manager__c = keyContactMap.get('CommunityManager');    
        }
        if(!String.isBlank(wrapperRec[0].FirstName) && !String.isBlank(wrapperRec[0].LastName)){
            a.Name= wrapperRec[0].FirstName +' ' + wrapperRec[0].LastName;
        }
        
        if(keyContactMap.keySet().contains('TalentCSA')){
            a.Talent_CSA__c = keyContactMap.get('TalentCSA');    
        }
        if(keyContactMap.keySet().contains('Talentbbiller')){
            a.Talent_Biller__c = keyContactMap.get('Talentbbiller');    
        }
        if(keyContactMap.keySet().contains('CommunityRep')){
            a.Talent_Community_Rep__c = keyContactMap.get('CommunityRep');    
        }
        if(keyContactMap.keySet().contains('TalentProgramManager')){
            a.Talent_Program_Manager__c = keyContactMap.get('TalentProgramManager');    
        }
        if(keyContactMap.keySet().contains('TalentDeliveryManager')){
            a.Talent_Delivery_Manager__c = keyContactMap.get('TalentDeliveryManager');    
        }
        if(keyContactMap.keySet().contains('TalentEmployeeServiceSpecialist')){
            a.Talent_Employee_Service_Specialist__c = keyContactMap.get('TalentEmployeeServiceSpecialist');    
        }
        a.id = accountId;
        accLst.add(a);
        if(wrapperRec[0].Active){
        //update accLst;
            List<Database.SaveResult> accLstUpdate = Database.update(accLst, false);
        }
        Contact c = new Contact();
        if(!String.isBlank(wrapperRec[0].Email)){
            c.Email = wrapperRec[0].Email;
        }
        if(!String.isBlank(wrapperRec[0].HomePhone)){
            c.HomePhone= wrapperRec[0].HomePhone; 
        }
        if(!String.isBlank(wrapperRec[0].FirstName)){
            c.FirstName= wrapperRec[0].FirstName;
            c.TC_FirstName__c = wrapperRec[0].FirstName;
        }
        if(!String.isBlank(wrapperRec[0].LastName)){
            c.LastName = wrapperRec[0].LastName;
            c.TC_LastName__c = wrapperRec[0].LastName;
        }
        c.Id = contactId;
        conlst.add(c);
        if(wrapperRec[0].Active){
            List<Database.SaveResult> conLstUpdate = Database.update(conlst, false);
        }
        
    }
    @AuraEnabled(cacheable=false)
    public static void resetUserPassword(Id userId) {
        System.debug('-------------userId'+userId);
        if(!String.isBlank(userId)){
          //  testtesttest.testtesttesttesttesttest(userId);
            //system.resetPassword(userId, true);
            System.debug('-------------isSuccess'+userId);
        }
    }
    
    
}