public with sharing class TagDefinitionTriggerHandler  {

 private static boolean hasBeenProccessed = false;


   public void OnAfterInsert(List<Tag_Definition__C> newRecords) {
       callDataExport(newRecords);

    }


     
    public void OnAfterUpdate (List<Tag_Definition__C> newRecords) {        
       callDataExport(newRecords);

    }

     public void OnAfterDelete (Map<Id, Tag_Definition__C> oldMap) {
       callDataExport(oldMap.values());

    }


    private static void callDataExport(List<Tag_Definition__C> newRecords) {
           
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TagDefinitionTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' Tag_Definition__C records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Tag_Definition__C');
            hasBeenProccessed = true; 
        }

    }


}