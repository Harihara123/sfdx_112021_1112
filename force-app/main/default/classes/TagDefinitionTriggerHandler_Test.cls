@isTest
public class TagDefinitionTriggerHandler_Test{
    static testMethod void testInsertTag(){
         List<Tag_Definition__C> tagList = new List<Tag_Definition__C>();
         for(Integer i = 0 ; i<10 ; i++){
              Tag_Definition__C tag = new Tag_Definition__C();
              tagList.add(tag);
         }
         if(tagList != null){
          insert tagList; 
        }
        Test.startTest();
        update tagList;
        Test.stopTest();    
     }
    static testMethod void test_OnAfterDelete_UseCase1(){
    List<Tag_Definition__C> tag_Obj  =  new List<Tag_Definition__C>();
    System.assertEquals(false,tag_Obj.size()>0);
    TagDefinitionTriggerHandler obj01 = new TagDefinitionTriggerHandler();
    obj01.OnAfterDelete(new Map<Id,Tag_Definition__C>());
  }
}