public with sharing class TalentAddEditController {

    @AuraEnabled
    public static Contact getTalent(String accountId) {
        Contact con = SaveTalentWithoutSharing.getTalent(accountid);
        return con;
    }

    @AuraEnabled
    public static Contact getTalentById(String recordId) {
        Contact con = SaveTalentWithoutSharing.getTalentById(recordId);
        return con;
    }

    @AuraEnabled
    public static Contact getTalentForModal(String accountId) {
        Contact con = SaveTalentWithoutSharing.getTalentForModal(accountid);
        return con;
    }

    @AuraEnabled
    public static Id saveTalentRecord(Contact contact, Account account, String g2Comments, String g2Source){
        Id accId = SaveTalentWithoutSharing.saveTalentRecord(contact, account, g2Comments,g2Source);
        return accid;
    }
     
    @AuraEnabled
    public static Id saveTalentRecordModal(Contact contact, Account account, String g2Comments,String g2Source,Integer duration,Contact acutalContact, Account acutalAccount,String parsedInfo, Boolean g2checked, Boolean disable){
        Id accId = SaveTalentWithoutSharing.saveTalentRecordModal(contact, account, g2Comments,g2Source);
        // Verify and create a G2 Type record in the postgres if it is N2P Flow
        if(contact.N2pTransactionId__c != null &&
           g2Source != null && g2Source == 'n2p'){
            System.debug('saveTalentRecordModal parsedInfo ' + parsedInfo);
            N2PExternalDBController.createEditModalRecordPostN2P(acutalContact,contact, acutalAccount,account, g2Comments,g2Source,duration,parsedInfo, g2checked, disable);
        }
        else if(g2Source != null && g2Source == 'modal'){
            System.debug('saveTalentRecordModal - Edit Modal');
            N2PExternalDBController.createEditModalRecordNoN2P(acutalContact, contact, acutalAccount, account, g2Source, duration, g2Comments, g2checked, disable);
        }
        return accid;
    }

    //S-95818 Inline edit on Talent Header - Karthik
    @AuraEnabled
    public static void saveTalentInlineEdit(Contact contact, Account account){
        try {
            upsert contact;
            if (account != null) {
                upsert account;
            }
        } catch (DmlException ex) {
            System.debug('Failed --- ' + ex.getMessage());
            throw new AuraHandledException ('Something went wrong: '+ ex.getMessage());
        }
    }

    @AuraEnabled
    public static List<String> getPicklistValues(String objectStr, string fld) {
        List<String> allOpts = new list < String > ();
        // Get the object type of the SObject.
        //Schema.sObjectType objType = objObject.getSObjectType();
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectStr);
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
        // Get a map of fields for the SObject
        Map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
         
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
        fieldMap.get(fld).getDescribe().getPickListValues();
         
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
           allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    } 

    /*@AuraEnabled
    public static User getCurrentUser(){
        User user = [SELECT id, Name, Profile.Name, OPCO__c, Profile.PermissionsModifyAllData
                        FROM User WHERE id = :Userinfo.getUserId() LIMIT 1][0];
        return user;
    }*/
    
    @AuraEnabled
    public static ATS_UserOwnershipModal getCurrentUser(){
        ATS_UserOwnershipModal uoModal = new ATS_UserOwnershipModal();
        User user = [select id, Name, CompanyName, OPCO__c, Country, Profile.Name, Profile.PermissionsModifyAllData from User where id = :Userinfo.getUserId()];
        uoModal.usr = user;

        if(!String.isBlank(user.OPCO__c) ) {
            List<Talent_Role_to_Ownership_Mapping__mdt> OwnershipList = [SELECT Opco_Code__c,Ownership_Category__c,Talent_Ownership__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: user.OPCO__c];      
            
            if(OwnershipList != null && OwnershipList.size() > 0) {
                uoModal.userOwnership = OwnershipList.get(0).Talent_Ownership__c;
                uoModal.userCategory = OwnershipList.get(0).Ownership_Category__c;
            }
            System.debug('Ownership info ---'+uoModal);
        }
        
        return uoModal;

    }

    @AuraEnabled
    public static Contact getRelatedContact(String contactId){
        system.debug('allOpts ---->' + contactId);
        Contact contact = [SELECT id, Name from Contact WHERE id =: contactId LIMIT 1];
        return contact;
    }
   
    //S-65378 - Added by KK: Method to get a map of 2 digit country code to ISO3 country code
    @AuraEnabled
    public static Map<String,String> getCountryMappings(){
        Map<String,String> countriesMap = new Map<String,String>();
        for(CountryMapping__mdt country:[SELECT CountryCode__c,CountryName__c,Id,Label FROM CountryMapping__mdt]) {
            countriesMap.put(country.Label,country.CountryCode__c);
        }
        return countriesMap;
    }
    
     // Todo :- Change the name of method to original name .
    @AuraEnabled(cacheable=true)
    public static CountryState getCountryMappingsWithAbbr(){        
        Map<String,CountryMapping__mdt> countriesMap = new Map<String,CountryMapping__mdt>();
        for(CountryMapping__mdt country:[SELECT CountryCode__c,CountryName__c,Id,Label FROM CountryMapping__mdt]) {
            countriesMap.put(country.Label,country);
        }
        Map<String,Global_LOV__c> globalLovStateMap = new Map<String,Global_LOV__c>();
        for(Global_LOV__c glov:[SELECT Text_Value_2__c,Text_Value_4__c,Text_Value_3__c FROM Global_LOV__c WHERE LOV_Name__c = 'StateList' and Text_Value_4__c!=null]){
            globalLovStateMap.put(glov.Text_Value_2__c,glov);
        }
        CountryState cs=new CountryState();
        cs.countriesMap=countriesMap;
        cs.globalLovStateMap=globalLovStateMap;
        return cs;
    }


    @AuraEnabled
    public static Map<String, String> getLanguages(){
        return BaseController.getLanguages();
    }
    
    /*
    Rajeesh restrict access to limited set of users using permission sets. Needed for experiment and for pilot.
    This function looks at users permission Set, and decides whether to give access or not.
   */
   @AuraEnabled
   public static Boolean hasAccessToNotesParser(){
    Boolean hasAccess=false;
    //Rajeesh 4/24/2019 Riju's suggestion to use feature management api 
    Boolean hasCustomPermission = FeatureManagement.checkPermission('N2P');
    System.debug('hasCustomPermission'+hasCustomPermission);
    /* 
    List<PermissionSetAssignment> usrPermSets = [Select Id, PermissionSet.Name,AssigneeId from PermissionSetAssignment where AssigneeId=:UserInfo.getUserId()];
    for(PermissionSetAssignment psa: usrPermSets){
        if(psa.PermissionSet.Name.equals('ATS_Notes_Parser')){
            hasAccess = true;
        }
    }*/

    return hasCustomPermission;
   }
   @AuraEnabled
   public static Boolean hasAccessToG2Parser(){
        // Get the user permissions
        Boolean hasAccess=false;
        Boolean hasCustomPermission = FeatureManagement.checkPermission('G2Parser');
        System.debug('hasCustomPermission'+hasCustomPermission);

        return hasCustomPermission;
   }
   //Get User details used for N2P metrics
   @AuraEnabled
   public static User getUserDetailsN2P(){
        User x = BaseController.getCurrentUser();
        User user = [SELECT id, Name, Email, Office__c, Office_Code__c FROM User WHERE id =: x.Id LIMIT 1];
        return user;
   }

   @AuraEnabled
   public static Boolean enableCurrentCompany (String AccountId) {
        List<Date> dates = new List<Date>();
        Boolean toggle = false;
        Boolean dateCheck = false;
        List<Talent_Work_History__c> WHDate = [SELECT Id, End_Date__c, Current_assignment_Formula__c FROM Talent_Work_History__c WHERE Talent__c =: AccountId AND Current_assignment_Formula__c =: true ];
        if(WHDate.size() > 0){
            for(Integer i = 0; i < WHDate.size(); i++){
                dates.add(WHDate[i].End_Date__c);
            }
        }

        Boolean hasCurrentAssignment = false;
        Date currentDate = Date.today();
        for(Date d : dates){
            if(d == null || d >= currentDate){
                dateCheck = true;
            }
        }

        for( Talent_Work_History__c twh : WHDate ){
            if(twh.Current_assignment_Formula__c == true)
                hasCurrentAssignment = true;
        }

        

       if(hasCurrentAssignment == true || dateCheck == true){
            toggle = true;
       }

        return toggle;
    }

    @AuraEnabled
    public static Boolean hasCurrentAssignment (String AccountId){
        List<Talent_Work_History__c> TWHList = [Select Id, Current_Assignment__c FROM Talent_Work_History__c WHERE Talent__c =: AccountId AND Current_Assignment__c =: true];
        List<Talent_Experience__c> TEList = [Select Id, Current_Assignment__c FROM Talent_Experience__c WHERE Talent__c =: AccountId AND Current_Assignment__c =: true];
        Boolean hasCurrentAssignment = false;

        for( Talent_Work_History__c TWH : TWHList ){
            if(TWH.Current_Assignment__c == true){
                hasCurrentAssignment = true;
            }
        }
        for(Talent_Experience__c TE : TEList){
            if(TE.Current_Assignment__c == true){
                hasCurrentAssignment = true;
            }
        }
        
        return hasCurrentAssignment;
    }

    /* This class will remove the Current_Assignment__c = True from the Talent_Work_History__c
     * And Talent_Experience__c records associated with given Account after the 
     * Account.Talent_Current_Employer__c field has been modified
     */
    @AuraEnabled
    public static void removeCurrentAssignmentAfterEdit(String AccountId){
        System.debug('removeCurrentAssignmentAfterEdit');
        System.debug(AccountId);
        List<Talent_Experience__c> TEList = [SELECT Id, Current_Assignment__c FROM Talent_Experience__c WHERE Talent__c =: AccountId];
        List<Talent_Work_History__c> TWHList = [SELECT Id, Current_Assignment__c FROM Talent_Work_History__c WHERE Talent__c =: AccountId];

        //Set Current_Assignment to false for Talent_Experiences with Talent__c = associated Talent Account
        for( Talent_Experience__c TE: TELIST ){
            if( TE.Current_Assignment__c == true){
                TE.Current_Assignment__c = false;
            }
        }
        update TEList;

        //Update Current Assignments for Talent Work History Records
        for( Talent_Work_History__c TWH : TWHList ){
            if( TWH.Current_Assignment__c == true ){
                TWH.Current_Assignment__c = false;
            }
        }
        update TWHList;
    }

    public class CountryState{
      
        @AuraEnabled
        public Map<String,CountryMapping__mdt> countriesMap{get;set;}
        
        @AuraEnabled
        public Map<String,Global_LOV__c> globalLovStateMap{get;set;}
        
   }
}