@isTest
private class TalentAddEditController_Test implements HttpCalloutMock {
    
	static testMethod void getTalentNSaveTalentRecord() {
        Test.startTest();
            Account talentAcc = TestData.newAccount(1, 'Talent');
            talentAcc.Talent_Ownership__c = 'AP';
            talentAcc.Talent_Committed_Flag__c = true;
        	talentAcc.G2_Completed__c = true;
            insert talentAcc;
        
        	Account tAcc = TestData.newAccount(1, 'Talent');
            tAcc.Talent_Ownership__c = 'AP';
            tAcc.Talent_Committed_Flag__c = true;
        	insert tAcc;

            Contact ct = TestData.newContact(talentAcc.id, 1, 'Talent'); 
            ct.Other_Email__c = 'other@testemail.com';
            ct.Title = 'test title';
            ct.Talent_State_Text__c = 'test text';
            ct.MailingState = 'test text';
            insert ct;
            
            string g2Comments = 'G2 Completed';
            
            Contact ct2 = TestData.newContact(tAcc.Id, 1, 'Talent');
            ct.Other_Email__c = 'anothertest@email.com';
            ct.Title = 'testing tester';
            insert ct2;
        
            TalentAddEditController.getUserDetailsN2P();

            //create the custom setting
            insert new ApigeeOAuthSettings__c(name = 'SOALatLong', Client_Id__c='1234567', Service_Http_Method__c='POST', Service_URL__c='http://allegisgrouptest.com',Token_URL__c='http://test.com');         
            Test.setMock(HttpCalloutMock.class, new TalentAddEditController_Test());

            TalentAddEditController.getTalent(talentAcc.id); 
            TalentAddEditController.getTalentForModal(talentAcc.id); 
            TalentAddEditController.saveTalentRecord(ct, talentAcc,'','inline-edit');
            TalentAddEditController.saveTalentRecordModal(ct, talentAcc, g2Comments,'inline-edit', 20, ct2, tAcc, '{some: Json}', true, true);
            //TalentAddEditController.saveTalentRecordModal(ct, tAcc, g2Comments,'inline-edit');
            TalentAddEditController.getRelatedContact(ct.id);
            //TalentAddEditController.saveTalentInlineEdit(ct);
        Test.stopTest();       
    }
    
    static testMethod void getPicklistValues() {
        Test.startTest();
            String objName = 'Contact';
            String fldName = 'Currency__c';
            TalentAddEditController.getPicklistValues(objName, fldName);
        Test.stopTest();
    }

    static testMethod void getCurrentUser() {
        Test.startTest();

        String profileName = 'Single Desk 1';
        User testUser = new User();
        testUser.Username = 'testuser@allegisgroup.com.test';
        testUser.Email = 'test@allegisgroup.com';
        testUser.Lastname = 'testlst';
        testUser.Firstname = 'Testy';
        testUser.Alias = 'test';
        testUser.OPCO__c = 'GB5';
        testUser.CommunityNickname = '12347';
        profile prof = [ select id from profile where Name = :profileName limit 1];
        testUser.ProfileId = prof.ID;
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false;
        testUser.isActive = true;
        insert testUser;
            System.runAs(testUser) {
                TalentAddEditController.getCurrentUser();
            }
        Test.stopTest();
    }

    //S-65378 - Added by KK
    static testMethod void getCountryMappingsTest() {
        Test.startTest();
            Map<String,String> countryMap = TalentAddEditController.getCountryMappings();
            system.assertNotEquals(0,countryMap.size());
            system.assertEquals(countryMap.get('US'),'USA');
        Test.stopTest();
    }

    private static string latLongResopnse(){
        String jsonResponse;
        jsonResponse = '{\r\n \"_id\": \"00625000006sElWAAU\",\r\n   \"_index\": \"ats-job-master-20180123.jan18-v2\",\r\n   \"_score\": 13.801794,\r\n   \"_source\": null,\r\n   \"city_name\": \"PHILADELPHIA\",\r\n   \"country_code\": \"UNITED STATES\",\r\n   \"country_sub_division_code\": \"PA\",\r\n   \"duration\": null,\r\n   \"duration_unit\": null,\r\n   \"education_requirement\": null,\r\n   \"geolocation\": {\r\n      \"lon\": -76.7661,\r\n      \"lat\": 39.5157\r\n   },\r\n   \"job_category_code\": null,\r\n   \"job_create_date\": \"2018-01-29\",\r\n   \"job_id\": \"00625000006sElWAAU\",\r\n   \"opco_name\": \"Allegis Partners, LLC\",\r\n   \"opportunity_name\": \"REQ - Test Sudheer Parent Test - group tester\",\r\n   \"organization_account_id\": \"1-1016VMO\",\r\n   \"organization_id\": \"0012400000pqWihAAE\",\r\n   \"organization_name\": \"Test Sudheer Parent Test\",\r\n   \"owner_id\": \"00524000005q3agAAA\",\r\n   \"owner_name\": \"Mark Brady\",\r\n   \"owner_psid\": \"Dataload78\",\r\n   \"position_description\": \"test description\",\r\n   \"position_end_date\": null,\r\n   \"position_id\": \"O-0823916\",\r\n   \"position_offering_type_code\": \"Permanent\",\r\n   \"position_open_quantity\": \"1\",\r\n   \"position_start_date\": \"2018-01-23\",\r\n   \"position_street_name\": \"TEST STREET\",\r\n   \"position_title\": \"group tester\",\r\n   \"postal_code\": \"21071\",\r\n   \"practice_area\": null,\r\n   \"qualifications_summary\": null,\r\n   \"remuneration_basis_code\": \"--None--\",\r\n   \"remuneration_bill_rate_max\": 0,\r\n   \"remuneration_bill_rate_min\": 0,\r\n   \"remuneration_interval_code\": null,\r\n   \"remuneration_pay_rate_max\": 0,\r\n   \"remuneration_pay_rate_min\": 0,\r\n   \"remuneration_salary_max\": 0,\r\n   \"remuneration_salary_min\": 0,\r\n   \"req_stage\": \"Interviewing\",\r\n   \"search_tags\": null,\r\n   \"skills\": \"\"\r\n}' ;     
        return jsonResponse;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(latLongResopnse());
        res.setStatusCode(200);  
        System.debug('Respond to Request - ' + req.getEndpoint());
        return res;
    }
    
    static testmethod void getCountryMappingsWithAbrTest(){
        Test.startTest();
            TalentAddEditController.CountryState cs = TalentAddEditController.getCountryMappingsWithAbbr();
        Test.stopTest();
    }
    
    static testmethod void checkN2Ppermissions(){
        Test.startTest();
            TalentAddEditController.hasAccessToG2Parser();
            TalentAddEditController.hasAccessToNotesParser();
        Test.stopTest();
    }

    @isTest    
    public static void testEnableCurrentCompany(){
        Account acc = TestData.newAccount(1);
        insert acc;
        Contact c = TestData.newContact(acc.Id, 2, 'Talent');
        insert c;
        Talent_Work_History__c twh = new Talent_Work_History__c();
        Talent_Experience__c te = new Talent_Experience__c();
        twh.Talent__c = acc.Id;
        twh.Current_Assignment__c = false;
        twh.Organization_Name__c = 'Allegis';
        twh.End_Date__c = date.Today()-1;
        insert twh;

        Test.startTest();
            Boolean toggle;
            toggle = TalentAddEditController.enableCurrentCompany(acc.Id);
        Test.stopTest();
        system.assertEquals(false, toggle);
    }
    
    @isTest    
    public static void testHasCurrentAssignment(){
        Account acc = TestData.newAccount(1);
        insert acc;
        Contact c = TestData.newContact(acc.Id, 2, 'Talent');
        insert c;
        Talent_Work_History__c twh = new Talent_Work_History__c();
        Talent_Experience__c te = new Talent_Experience__c();
        twh.Talent__c = acc.Id;
        twh.Current_Assignment__c = true;
        twh.Organization_Name__c = 'Allegis';
        twh.End_Date__c = date.valueOf('2019-12-05');
        insert twh;

        Test.startTest();
            Boolean bool;
            bool = TalentAddEditController.hasCurrentAssignment(acc.Id);
        Test.stopTest();
        system.assertEquals(true, bool);
    }
    
    @isTest
    public static void testRemoveCurrentAssignmentAfterEdit(){
        Account acc = TestData.newAccount(1);
        insert acc;
        Contact c = TestData.newContact(acc.Id, 2, 'Talent');
        insert c;
        Talent_Work_History__c twh = new Talent_Work_History__c();
        twh.Talent__c = acc.Id;
        twh.Current_Assignment__c = true;
        twh.Organization_Name__c = 'Allegis';
        twh.End_Date__c = date.valueOf('2020-12-05');
        insert twh;
        
        Test.startTest();
            TalentAddEditController.removeCurrentAssignmentAfterEdit(acc.Id);
        Test.stopTest();
        List<Talent_Work_History__c> TWHList = [SELECT Id, Current_Assignment__c FROM Talent_Work_History__c WHERE Talent__c =: acc.Id];
        system.assertEquals(false, TWHList[0].Current_Assignment__c);
    }
    
}