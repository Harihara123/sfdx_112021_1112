public without sharing class TalentAddEditReferenceController {
    public static Boolean isHandlerCalled = false;
    //Create a wrapper initialization method 
    @AuraEnabled
    public static ReferenceWrapper initWrapper(){
        ReferenceWrapper reference = new ReferenceWrapper();
        return reference;    
    }
    
    //Create a Method to get piclist values using global describe call
    @AuraEnabled
    public static Map<String, List<String>> getAllPicklistValues(String objectStr, string fld) {
        Map<String, List<String>> objectPicklistValuesMap = new Map<String, List<String>>();
        List<String> lstfields = fld.split(',');
        Integer i = 0;
        for(String fldName : lstfields){
            if(i == 0){
                objectPicklistValuesMap.put('Contact.Solution', TalentAddEditReferenceController.picklistValues('Contact', fldName));
                i++;
            }
            else{
                objectPicklistValuesMap.put('Recommendation.'+ fldName, TalentAddEditReferenceController.picklistValues('Talent_Recommendation__c', fldName));
            }    
        }
        return objectPicklistValuesMap;
    }
    public static List<String> picklistValues(String objectStr, string fld){    
        List<String> allOpts = new list < String > ();
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectStr);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
    }
    
    
    @AuraEnabled
    public static ReferenceWrapper getTalentReference(Id referenceId){
        
        ReferenceWrapper reference = new ReferenceWrapper();
        Contact contactObj = new Contact();
        Account accountObj = new Account();
        Talent_Recommendation__c recommendation = [Select Id,Relationship__c,Relationship_Duration__c, Client_Account__r.Id, Client_Account__c, End_Date__c, Start_Date__c, 
                                                   Interaction_Frequency__c,Talent_Current_Job__c,Talent_Job_Duration__c,Talent_Job_Title__c, Talent_Job_Duties__c, Opportunity__c, Client_Account__r.Name,
                                                   Rehire_Status__c,Quality__c,Workload__c,Quality_Description__c, Workload_Description__c,Competence__c,Recommendation_From__c, Opportunity__r.Id,Recommendation_From__r.RecordType.DeveloperName,
                                                   Competence_Description__c,Leadership__c,Leadership_Description__c,Collaboration__c,Collaboration_Description__c, Opportunity__r.Name,
                                                   Trustworthy__c,Trustworthy_Description__c,Likeable__c,Likeable_Description__c,Outlook__c, Outlook_Description__c, Reference_Check_Completed__c, Organization_Name__c,
                                                   Team_Player__c,Team_Player_Description__c,Professionalism__c, Professionalism_Description__c,Recommendation__c, 
                                                   Growth_Opportunities__c,Comments__c, Completion_Date__c, Project_Description__c, Non_technical_skills__c, Cultural_Environment__c, Additional_Reference_Notes__c, Reference_Type__c,
                                                   First_Name__c,Last_Name__c,Source_System_Id__c,Email__c,Home_Phone__c,Mobile_Phone__c,Other_Phone__c,Work_Phone__c,Reference_Job_Title__c,Completed_By__c       
                                                   from Talent_Recommendation__c where Id =: referenceId
                                                  ];
        
        if (recommendation.Source_System_Id__c != null && recommendation.Source_System_Id__c != '' && recommendation.Recommendation_From__c == null){
            reference.rwsRecord =true;
            reference.disableContactField = true;
        }
        
        if(recommendation.Recommendation_From__c != null){
            contactObj = [ Select id, AccountId, FirstName, MiddleName, LastName,Email, Title, Salutation, WorkExtension__c,
                          HomePhone,Phone,MobilePhone, Suffix, recordtype.Name 
                          from Contact where id =: recommendation.Recommendation_From__c];        
            accountObj = [Select Id, Name from Account where Id =: contactObj.AccountId];
            if(contactObj.recordtype.Name =='Talent' || contactObj.recordtype.Name =='Client'){
                reference.disableContactField = true;
            }
            
        }else{
            contactObj.firstname = recommendation.First_Name__c;
            contactObj.Lastname = recommendation.Last_Name__c;
            contactObj.Email = recommendation.Email__c;
            contactObj.MobilePhone = recommendation.Mobile_Phone__c;
            contactObj.phone = recommendation.Work_Phone__c;
            contactObj.HomePhone = recommendation.Home_Phone__c;
            contactObj.Title = recommendation.Reference_Job_Title__c;
        }
        
        
        
        reference.account = accountObj;
        reference.contact = contactObj;
        reference.recommendation = recommendation;                
        return reference;    
    }
    
    //Create a Method for saving/Inserting Talent_Reference, Account and contact
    @AuraEnabled
    public static String saveTalentReference(Contact contact, Talent_Recommendation__c Recommendation, String talentContactId, Boolean shouldInsert, String mode, String action){
        // declare variable here 
        
        system.debug('###$$$###'+Recommendation);
        String message = 'success';
        
        Savepoint sp = Database.setSavepoint();
        try{
            if(action == 'SaveAndComplete'){
                Recommendation.Completion_Date__c = date.today();
                Recommendation.Reference_Check_Completed__c = true;
                Recommendation.Completed_By__c = UserInfo.getUserId();
            }
            if(shouldInsert == true)
                message = TalentAddEditReferenceController.insertAccountContact(contact, Recommendation, talentContactId);
            if(shouldInsert == false)
                message = TalentAddEditReferenceController.updateAccountContact(contact, Recommendation);    
        }
        catch(Exception Ex){
            // Rollback all changes
            system.debug('###$$$###'+ex.getMessage());
            message = ex.getMessage();
            Database.rollback(sp);
        }
        return message;    
    }
    
    public static String updateAccountContact(Contact contact, Talent_Recommendation__c Recommendation){
        
        system.debug('###$$$### update'+contact);
        system.debug('###$$$### update'+Recommendation);
        try{
            if(contact.id != null ){
                update contact;
            }
            
            if(Recommendation.id != null ){
                update Recommendation;
            }
        }
        catch(Exception Ex){
            
            String message = ex.getMessage();
            
        }
        
        
        //Talent_Recommendation__c rec = [Select Id, Opportunity__c from Talent_Recommendation__c where Id =: Recommendation.Id];
        
        //if(rec.Opportunity__c != null && Recommendation.Opportunity__c != null)
        //    Recommendation.Opportunity__c = rec.Opportunity__c;
        //else
        //    Recommendation.Opportunity__c = Recommendation.Opportunity__c;
        ////system.debug('@@@####'+Recommendation.Opportunity__c); 
        //update Recommendation;
        return 'Record updated';
    }
    
    public static String insertAccountContact(Contact contact, Talent_Recommendation__c Recommendation, String talentContactId){
        RecordType contactRecordType = [Select Id, Name, sobjecttype from RecordType where sobjecttype = 'Contact' and Name = 'Reference'];
        RecordType accountRecordType = [Select Id, Name, sobjecttype from RecordType where sobjecttype = 'Account' and Name = 'Reference'];
        Contact talentContact = [Select Id, AccountId from Contact where Id =: talentContactId];
        Account account = new Account();
        account.RecordtypeId = accountRecordType.Id;
        account.Name = contact.FirstName + ' ' + contact.LastName;
        account.Talent_Visibility__c = 'Private';

        insert account;
        
        contact.RecordtypeId = contactRecordType.Id;
        contact.AccountId = account.id;
        system.debug('###$$$### insert'+contact);
        insert contact;
        
        Recommendation.Talent_Contact__c = talentContact.Id;
        Recommendation.Recommendation_From__c = contact.Id;
        Recommendation.Opportunity__c = Recommendation.Opportunity__r.Id;
        //Recommendation.Talent__c = talentContact.AccountId;
        system.debug('###$$$### update'+Recommendation);
        insert Recommendation;
        return account.Id;
    }
    
    @AuraEnabled
    public static Map<String, List<Talent_Recommendation__c>> getContactReferences(Id contactId){
        
        Map<String, List<Talent_Recommendation__c>> refrences = new Map<String, List<Talent_Recommendation__c>>();
        
        try{

            Id AccountId = [SELECT AccountId FROM Contact WHERE ID =:contactId LIMIT 1 ].AccountId;
            
            List<Talent_Recommendation__c> referencesReceived  = [SELECT Id, Client_Account__r.Id, Recommendation_From__r.RecordType.DeveloperName, 
                                                                  Recommendation_From__r.FirstName, Recommendation_From__r.LastName, Recommendation_From__r.Full_Name__c, Recommendation_From__r.Email, Recommendation_From__r.Phone, 
                                                                  Recommendation_From__r.HomePhone, Recommendation_From__r.MobilePhone, Recommendation_From__r.OtherPhone,
                                                                  Recommendation_From__r.Company_Name__c , Relationship__c, CreatedDate, CreatedBy.Name, Reference_Check_Completed__c, 
                                                                  Client_Account__r.Name, Organization_Name__c, Reference_Type__c, Recommendation_From__r.WorkExtension__c,
                                                                  First_Name__c,Last_Name__c,Source_System_Id__c,Email__c,Home_Phone__c,Mobile_Phone__c,Other_Phone__c,Work_Phone__c,Completed_By__c      
                                                                  FROM Talent_Recommendation__c where Talent_Contact__r.AccountId =: AccountId order by LastModifiedDate desc];
            refrences.put('recieved', referencesReceived);
            
            List<Talent_Recommendation__c> referencesGiven      = [SELECT Id, Talent_Contact__r.FirstName, Talent_Contact__r.LastName, Talent_Contact__r.Title, Talent_Contact__r.Company_Name__c,Completed_By__c,
                                                                   Talent_Contact__r.Email, Talent_Contact__r.CreatedBy.Name, CreatedDate, Talent_Contact__r.Account.Talent_Current_Employer_Formula__c       
                                                                   FROM Talent_Recommendation__c where Recommendation_From__r.AccountId =: AccountId order by LastModifiedDate desc];
            
            
            refrences.put('given', referencesGiven);

        } catch (Exception ex) {
            throw new AuraHandledException('There was an error, please contact Administrator: ' + ex.getMessage());
        }
        
        
        return refrences;    
    }
    
    @AuraEnabled
    public static boolean deleteContactReference(Id recordId){
        Talent_Recommendation__c talentRecommendationObj = [Select Id,Recommendation_From__r.AccountID,Recommendation_From__c, Recommendation_From__r.RecordType.DeveloperName from Talent_Recommendation__c where id =: recordId ];
        
        try {
            if(talentRecommendationObj.Recommendation_From__c != null){
                if(talentRecommendationObj.Recommendation_From__r.RecordType.DeveloperName == 'Reference'){
                    List<Talent_Recommendation__c> otherReferenceList = new List<Talent_Recommendation__c>([Select Id, Recommendation_From__r.AccountID,Recommendation_From__c from Talent_Recommendation__c where id !=: talentRecommendationObj.id AND Recommendation_From__c =: talentRecommendationObj.Recommendation_From__c]);
                    if(otherReferenceList.size() <= 0){
                        Account referenceAccount = [select id, name from Account where id=: talentRecommendationObj.Recommendation_From__r.AccountID];
                        delete referenceAccount;
                    }
                }    
            }
            delete talentRecommendationObj;
            return true;
        } catch (DmlException e) {            
            
            return false;
        }
        
    }
    
    @AuraEnabled
    public static List<DocumentDataWrapper> loadDocumentData(List<Id> referenceIds, Id talentId){
        List<DocumentDataWrapper> refDocDataList = new List<DocumentDataWrapper>();
        for (Id referenceId : referenceIds) {
            DocumentDataWrapper refDocData = new DocumentDataWrapper();
            Talent_Recommendation__c recommendation = [Select Id,Relationship__c,Relationship_Duration__c, Client_Account__c, End_Date__c, Start_Date__c, 
                                                       Client_Account__r.Id, Interaction_Frequency__c,Talent_Current_Job__c,Talent_Job_Duration__c,Talent_Job_Title__c, Talent_Job_Duties__c, Opportunity__c, Client_Account__r.Name,
                                                       Rehire_Status__c,Quality__c,Workload__c,Quality_Description__c, Workload_Description__c,Competence__c,Recommendation_From__c, Opportunity__r.Id,
                                                       Competence_Description__c,Leadership__c,Leadership_Description__c,Collaboration__c,Collaboration_Description__c, Opportunity__r.Name, 
                                                       Trustworthy__c,Trustworthy_Description__c,Likeable__c,Likeable_Description__c,Outlook__c, Outlook_Description__c, Reference_Check_Completed__c, Organization_Name__c,
                                                       Team_Player__c,Team_Player_Description__c,Professionalism__c, Professionalism_Description__c,Recommendation__c, 
                                                       First_Name__c,Last_Name__c,Source_System_Id__c,Email__c,Home_Phone__c,Mobile_Phone__c,Other_Phone__c,Work_Phone__c,Completed_By__c,
                                                       Growth_Opportunities__c,Comments__c, Completion_Date__c, Project_Description__c, Non_technical_skills__c, Cultural_Environment__c, Additional_Reference_Notes__c from Talent_Recommendation__c where Id =: referenceId
                                                      ];
            
            if(recommendation.Recommendation_From__c != null ){
                Contact contact = [ Select id, AccountId, FirstName, MiddleName, LastName,Email, Title, Salutation, WorkExtension__c, 
                                   HomePhone,Phone,MobilePhone, Suffix from Contact where id =: recommendation.Recommendation_From__c];
                
                Account account = [Select Id, Name from Account where Id =: contact.AccountId];  
                
                refDocData.reference.account = account;
                refDocData.reference.contact = contact;
                
            }                                                      
            
            Contact talent = [ Select id, AccountId, FirstName, MiddleName, LastName,Email, Title, Salutation,
                              HomePhone,Phone,MobilePhone, Suffix, Company_Name__c, Account.Talent_End_Date__c,
                              Account.Talent_Start_Date__c from Contact where id =: talentId                          
                             ];
            
            refDocData.reference.recommendation = recommendation;     
            refDocData.talent = talent;
            
            refDocDataList.add(refDocData);
            
        }
        
        return refDocDataList;  
        
        
    }
    
    
    
    @AuraEnabled
    public static ATS_UserOwnershipModal getCurrentUserWithOwnership() {
        ATS_UserOwnershipModal uoModal = new ATS_UserOwnershipModal();
        User user = [select id, Name, CompanyName, OPCO__c, Profile.Name, Profile.PermissionsModifyAllData from User where id = :Userinfo.getUserId()];
        uoModal.usr = user;
        
        if(!String.isBlank(user.OPCO__c) ) {
            Talent_Role_to_Ownership_Mapping__mdt[] Ownership = [SELECT Opco_Code__c,Talent_Ownership__c,LinkedIn_Sync__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: user.OPCO__c];      
            if(Ownership!=null && !Ownership.isEmpty()) {
                uoModal.userOwnership = Ownership[0].Talent_Ownership__c;
                uoModal.haslinkedInSync = Ownership[0].LinkedIn_Sync__c;            
            }
        }
        
        return uoModal;
    }
    
    @AuraEnabled
    public static string getLogoUrl(String name) {
        
        if(name==null){
            User user = [select id, Name, CompanyName, OPCO__c, Profile.Name, Profile.PermissionsModifyAllData from User where id = :Userinfo.getUserId()];
            List<Talent_Role_to_Ownership_Mapping__mdt> Ownership = [SELECT Opco_Code__c,Talent_Ownership__c,LinkedIn_Sync__c FROM Talent_Role_to_Ownership_Mapping__mdt Where Opco_Code__c =: user.OPCO__c limit 1];      
            if(Ownership!=null && !Ownership.isEmpty()) {
                name = Ownership[0].Talent_Ownership__c;
            }
        }
        
        
        string logoURl = '#';
        string imgString;
        blob img;
        List<Document> documents = [Select Id,Name,Body,LastModifiedById from Document WHERE Name =: name LIMIT 1];
        if(documents !=null && !documents.isEmpty()){
            string strOrgId = UserInfo.getOrganizationId();
            String fullFileURL = URL.getSalesforceBaseUrl().toExternalForm();
            logoURl = fullFileURL + '/servlet/servlet.ImageServer?id=' + documents[0].Id + '&oid=' + strOrgId;

            img = documents[0].Body;
            imgString = EncodingUtil.base64Encode(img);
        }
        
        return imgString;
        //return logoURl;
        
    }

    class BadPhoneException extends Exception {}

    @AuraEnabled
    public static String callDedupeService(String firstName, String lastName, List<String> email, List<String> phone){
        
        List<String> legitPhone = TalentDedupeSearch.excludeFakePhone(phone);

        System.debug('excludeFakePhone===> ' + legitPhone.size());
        if(!phone.isEmpty() && legitPhone.size() == 0){
            BadPhoneException e = new BadPhoneException();
            String errorLabel = System.Label.ATS_BAD_PHONE_EXCEPTION;
            e.setMessage(errorLabel);
            throw e;
        }

        if(!TalentAddEditReferenceController.isHandlerCalled){
            
            TalentAddEditReferenceController.isHandlerCalled = true; 
            
            String fullName             = firstName+' '+lastName;
            String returnCount          = 'MANY';
            List<String> recordTypes    = new List<String>{'Talent','Client','Reference'};
                boolean IsAtsTalent         = true;
            boolean ignoreIs_ATS_Talent = true;
            
            List<List<SObject>> duplicateTalents = TalentDedupeSearch_v2.retriveDuplicateTalents( fullName, email, phone, '', '', recordTypes , returnCount, IsAtsTalent, ignoreIs_ATS_Talent );
            
            if( duplicateTalents.isEmpty() || duplicateTalents[0].isEmpty() ){
                return '[{"Message" : "No duplicates found"}]'; 
            }else{
                return JSON.serialize( duplicateTalents[0] );
            }
            
            
        }else{
            return null;
            
        }
        
        
    } 
    
    
    @AuraEnabled
    public static ReferenceWrapper generateReferenceType(Id recordId){
        
        ReferenceWrapper ref = new ReferenceWrapper();
        Contact contact = [ Select id, AccountId, FirstName, MiddleName, LastName,Email, Title, Salutation, WorkExtension__c, HomePhone,Phone,MobilePhone, Suffix, recordtype.Name from Contact where id =: recordId];
        Account account = [Select Id, Name from Account where Id =: contact.AccountId];  
        
        ref.account = account;
        ref.contact = contact;  
        if(contact.recordtype.Name =='Talent' || contact.recordtype.Name =='Client'){
            ref.disableContactField = true;
        }
        return ref;
    }
    
    @AuraEnabled
    public static String findDuplicateRecords(ID contactId){
        Contact contact = [select id from Contact where id =: contactId];
        List<Contact> lstDupeContact = new List<Contact>();    
        List<Contact> lstContact = new List<Contact>();
        Set<ID> dupeContactId = new Set<ID>();
        lstContact.add(contact);
        List<Datacloud.MatchRecord> matchDupeRecord;
        Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(lstContact);
        for (Datacloud.FindDuplicatesResult findDupeResult : results) {
            for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                    
                    for (Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                        dupeContactId.add(matchRecord.getRecord().Id);
                        System.debug('Duplicate Record: ' + matchRecord.getRecord().Id);
                    }
                }
            }
        }
        List<Contact> lstcon = [Select Id, Name, Title, Email, RecordType.Name, Salutation, HomePhone,Phone,MobilePhone, Suffix, Company_Name__c from Contact where Id IN : dupeContactId];
        return JSON.serialize(lstcon);
    }
    @AuraEnabled
    public static Boolean connectTalentReferences(String recommendationId, String talentId, Talent_Recommendation__c selectedrecommendation, String referenceFrom, Contact referenceContact, String buttonClicked ){
        system.debug('Recommenndation ID:'+ recommendationId);
        system.debug('talentId:'+ talentId);
        system.debug('selectedrecommendation before update:'+ selectedrecommendation);
        system.debug('referenceFrom :'+ referenceFrom);
        system.debug('referenceContact :'+ referenceContact);
        system.debug('buttonClicked :'+ buttonClicked);
        //system.assert(false);
        Savepoint sp = Database.setSavepoint();
        
        try{
            if(selectedrecommendation.id != null){
                if(selectedrecommendation.Recommendation_From__r.RecordType.DeveloperName == 'Reference'){   
                    
                    List<Talent_Recommendation__c> otherRecommendations= [Select Id, Recommendation_From__r.AccountId from Talent_Recommendation__c where id !=: recommendationId AND Recommendation_From__c =: selectedrecommendation.Recommendation_From__c];
                    if(otherRecommendations.size() == 0){
                        
                        Id oldAccountId = selectedrecommendation.Recommendation_From__r.AccountId;
                        delete [select id from account where id =: oldAccountId ];
                    }
                }
                selectedrecommendation.Recommendation_From__c   = referenceFrom;
                if(buttonClicked != 'Save'){
                    selectedrecommendation.Completion_Date__c = date.today();
                    selectedrecommendation.Reference_Check_Completed__c = true;
                    selectedrecommendation.Completed_By__c = UserInfo.getUserId();
                }
                update selectedrecommendation;
                system.debug('selectedrecommendation after update:'+ selectedrecommendation);
            }else{
                selectedrecommendation.Recommendation_From__c       = referenceFrom;
                selectedrecommendation.Talent_Contact__c            = talentId;
                if(buttonClicked != 'Save'){
                    selectedrecommendation.Completion_Date__c = date.today();
                    selectedrecommendation.Reference_Check_Completed__c = true;
                    selectedrecommendation.Completed_By__c = UserInfo.getUserId();
                }
                    
                selectedrecommendation.Opportunity__c = selectedrecommendation.Opportunity__r.Id;
                insert selectedrecommendation;
                
            }
            if(referenceContact != null){
                update referenceContact;
            }
        }catch(exception ex){
            Database.rollback(sp);
            throw new AuraHandledException('There was an error, please contact Administrator: ' + ex.getMessage());
            
        }
    
    return true;
    
} 


public class DocumentDataWrapper{
    @AuraEnabled
    public ReferenceWrapper reference {get;set;}
    @AuraEnabled
    public contact talent {get;set;}
    
    public DocumentDataWrapper(){
        this.talent = new Contact();
        this.reference = new ReferenceWrapper();
        
    }
}

}