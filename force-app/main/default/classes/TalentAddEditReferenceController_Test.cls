@isTest
public class TalentAddEditReferenceController_Test {
    static testMethod void saveTalentReferenceTest1(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Test.startTest();
		Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
		System.assert(newClientAcc.Name=='Test Client');
		System.assert(newClientAcc.ShippingStreet=='Address St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='USA');

		Test.stopTest();
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test Client',mailingStreet = 'Address St',mailingCity='City',Email='tast@talent.com', mailingCountry='USA',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
        System.assert(newClientCon.lastName=='Test Client');
		System.assert(newClientCon.mailingStreet=='Address St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='USA');
        System.assert(newClientCon.Email=='tast@talent.com');
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c = newClientCon.id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.saveTalentReference(RefernceContact,tRecom,String.valueOf(masterContact.id),true,'Add',''); 
    }
    
	static testMethod void saveTalentReferenceTest2(){
        Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
         //Client Contact
        Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);
        Contact newClientCon = new Contact(lastName='Test Client',mailingStreet = 'Address St',mailingCity='City',Email='tast@talent.com', mailingCountry='USA',AccountID = newClientAcc.id);
        Database.insert(newClientCon);
        System.assert(newClientAcc.Name=='Test Client');
		System.assert(newClientAcc.ShippingStreet=='Address St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='USA');
         System.assert(newClientCon.lastName=='Test Client');
		System.assert(newClientCon.mailingStreet=='Address St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='USA');
        System.assert(newClientCon.Email=='tast@talent.com');

        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        //trecom.Talent__c= masterAccount.id;
        trecom.Talent_Contact__c = masterContact.id;
        trecom.Recommendation_From__c = newClientCon.id;
        insert trecom;

        Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
        Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
        TalentAddEditReferenceController.saveTalentReference(RefernceContact,tRecom,String.valueOf(masterContact.id),false,'edit','SaveAndComplete');
        TalentAddEditReferenceController.getTalentReference(trecom.id);
        TalentAddEditReferenceController.deleteContactReference(trecom.id);
        System.assertEquals(trecom.Recommendation_From__c,newClientCon.id);
        TalentAddEditReferenceController.getContactReferences(masterContact.id);
    }
	static testMethod void saveTalentReferenceTest3(){
        String obj1='Contact';
        String fld='Preferred_Email__c';
        TalentAddEditReferenceController.picklistValues(obj1,fld);
        TalentAddEditReferenceController.getAllPicklistValues(obj1,fld);      
        TalentAddEditReferenceController.initWrapper();
        TalentAddEditReferenceController.getCurrentUserWithOwnership();
        TalentAddEditReferenceController.getLogoUrl('Logo Tek');
        System.assert(obj1=='Contact');
        System.assert(fld=='Preferred_Email__c');
    }
	static testMethod void saveTalentReferenceTest4(){
        Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
        //Client Contact
        Account newClientAcc = new Account(Name='Test1 Client',ShippingStreet = 'Address1 St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);
        Contact newClientCon = new Contact(lastName='Test Client',mailingStreet = 'Address St',mailingCity='City',Email='tast1@talent.com', mailingCountry='USA',AccountID = newClientAcc.id);
        Database.insert(newClientCon);
         System.assert(newClientAcc.Name=='Test1 Client');
		System.assert(newClientAcc.ShippingStreet=='Address1 St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='USA');
         System.assert(newClientCon.lastName=='Test Client');
		System.assert(newClientCon.mailingStreet=='Address St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='USA');
        System.assert(newClientCon.Email=='tast1@talent.com');
        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        //trecom.Talent__c= masterAccount.id;
        trecom.Talent_Contact__c = masterContact.id;
        trecom.Recommendation_From__c = newClientCon.id;
        insert trecom;
        Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
        Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
        TalentAddEditReferenceController.saveTalentReference(RefernceContact,tRecom,String.valueOf(masterContact.id),false,'Add','');
        System.assertEquals(trecom.Recommendation_From__c,newClientCon.id);
        
    }
	static testMethod void saveTalentReferenceTest5(){
        Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
        //Client Contact
        Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
        Database.insert(newClientAcc);
        Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
        Database.insert(newClientCon);
         System.assert(newClientAcc.Name=='Test2 Client');
		System.assert(newClientAcc.ShippingStreet=='Address12 St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='US');
         System.assert(newClientCon.lastName=='Test 2Client');
		System.assert(newClientCon.mailingStreet=='Addres2s St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='US');
        System.assert(newClientCon.Email=='tast1@tal2ent.com');
        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        //trecom.Talent__c= masterAccount.id;
        trecom.Talent_Contact__c = masterContact.id;
        trecom.Recommendation_From__c=null;
        trecom.Source_System_Id__c=newClientAcc.Id;
        insert trecom;
        Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
        Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
        TalentAddEditReferenceController.saveTalentReference(RefernceContact,tRecom,String.valueOf(masterContact.id),true,'edit','');
		System.assertEquals(trecom.Source_System_Id__c,newClientAcc.Id);
    }
    static testMethod void saveTalentReferenceTest6(){
        Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
        //Client Contact
        Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
        Database.insert(newClientAcc);
        Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
        Database.insert(newClientCon);
        System.assert(newClientAcc.Name=='Test2 Client');
		System.assert(newClientAcc.ShippingStreet=='Address12 St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='US');
         System.assert(newClientCon.lastName=='Test 2Client');
		System.assert(newClientCon.mailingStreet=='Addres2s St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='US');
        System.assert(newClientCon.Email=='tast1@tal2ent.com');
        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        //trecom.Talent__c= masterAccount.id;
        trecom.Talent_Contact__c = masterContact.id;
       // trecom.Recommendation_From__c=null;
      //  trecom.Source_System_Id__c=newClientAcc.Id;
        insert trecom;
        Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
        Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
        TalentAddEditReferenceController.saveTalentReference(RefernceContact,tRecom,String.valueOf(masterContact.id),true,'edit','');
        System.assertEquals(newClientCon.lastName,'Test 2Client');
        
    }
    static testMethod void updateAccountContactTest7(){
        Contact RefernceContact = null;
        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        trecom.Talent_Contact__c = null;
        TalentAddEditReferenceController.updateAccountContact(RefernceContact,tRecom);
        System.assert(trecom.Talent_Contact__c==null);
        
    }
    static  testMethod void getTalentReferenceTest8(){
         Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
        //Client Contact
        Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
        Database.insert(newClientAcc);
        Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
        Database.insert(newClientCon);
         System.assert(newClientAcc.Name=='Test2 Client');
		System.assert(newClientAcc.ShippingStreet=='Address12 St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='US');
         System.assert(newClientCon.lastName=='Test 2Client');
		System.assert(newClientCon.mailingStreet=='Addres2s St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='US');
        System.assert(newClientCon.Email=='tast1@tal2ent.com');
        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        //trecom.Talent__c= masterAccount.id;
        trecom.Talent_Contact__c = masterContact.id;
       //trecom.Recommendation_From__c=newClientCon.id;
      // trecom.Source_System_Id__c=newClientAcc.Id;
        insert trecom;
        TalentAddEditReferenceController.getTalentReference(trecom.Id);
       System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);
    }
    static  testMethod void getTalentReferenceTest9(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		 System.assert(newClientAcc.Name=='Test2 Client');
		System.assert(newClientAcc.ShippingStreet=='Address12 St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='US');
         System.assert(newClientCon.lastName=='Test 2Client');
		System.assert(newClientCon.mailingStreet=='Addres2s St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='US');
        System.assert(newClientCon.Email=='tast1@tal2ent.com');
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=null;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		TalentAddEditReferenceController.getTalentReference(trecom.Id); 
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);      
    }
   /* static testMethod void insertAccountContactTest10(){
        
         Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
         Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
        Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
        //Client Contact
        Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
        Database.insert(newClientAcc);
        Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
        Database.insert(newClientCon);
        //
        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        trecom.Talent_Contact__c = masterContact.id;
       // trecom.Talent_Contact__c = masterContact.id;
       trecom.Recommendation_From__c=RefernceContact.id;
       trecom.Opportunity__c=trecom.Opportunity__r.Id;
    //  trecom.Source_System_Id__c=newClientAcc.Id;
        insert trecom;
        
         TalentAddEditReferenceController.insertAccountContact(RefernceContact,tRecom,String.valueOf(masterContact.id));
    } */
    static testMethod void getContactReferencesTest11(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		 System.assert(newClientAcc.Name=='Test2 Client');
		System.assert(newClientAcc.ShippingStreet=='Address12 St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='US');
         System.assert(newClientCon.lastName=='Test 2Client');
		System.assert(newClientCon.mailingStreet=='Addres2s St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='US');
        System.assert(newClientCon.Email=='tast1@tal2ent.com');
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=null;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.getContactReferences(RefernceContact.Id);
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
    static testMethod void deleteContactReferenceTest12(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Test.startTest();
		Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
		System.assertEquals('Test Client',newClientAcc.Name);
		Test.stopTest();
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test Client',mailingStreet = 'Address St',mailingCity='City',Email='tast@talent.com', mailingCountry='USA',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		 System.assert(newClientAcc.Name=='Test Client');
		System.assert(newClientAcc.ShippingStreet=='Address St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='USA');
         System.assert(newClientCon.lastName=='Test Client');
		System.assert(newClientCon.mailingStreet=='Address St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='USA');
        System.assert(newClientCon.Email=='tast@talent.com');
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c = newClientCon.id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.deleteContactReference(tRecom.id);
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }

    static testMethod void loadDocumentDataTest14(){      
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Test.startTest();
		Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
		System.assertEquals('Test Client',newClientAcc.Name);
		System.assertEquals('Address St',newClientAcc.ShippingStreet);
		System.assertEquals('City',newClientAcc.ShippingCity);
		System.assertEquals('USA',newClientAcc.ShippingCountry);

		Test.stopTest();
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test Client',mailingStreet = 'Address St',mailingCity='City',Email='tast@talent.com', mailingCountry='USA',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		System.assert(newClientAcc.Name=='Test Client');
		System.assert(newClientAcc.ShippingStreet=='Address St');
		System.assert(newClientAcc.ShippingCity=='City');
		System.assert(newClientAcc.ShippingCountry=='USA');
         System.assert(newClientCon.lastName=='Test Client');
		System.assert(newClientCon.mailingStreet=='Address St');
		System.assert(newClientCon.mailingCity=='City');
		System.assert(newClientCon.mailingCountry=='USA');
        System.assert(newClientCon.Email=='tast@talent.com');
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c = newClientCon.id;
		insert trecom;
		List<Id> referenceId = new List<Id>();
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		referenceId.add(tRecom.Id);
		TalentAddEditReferenceController.loadDocumentData( referenceId,masterContact.Id);
		TalentAddEditReferenceController.getCurrentUserWithOwnership();
		TalentAddEditReferenceController.getLogoUrl('Test');
		TalentAddEditReferenceController.getLogoUrl(null);
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }    
    static testMethod void callDedupeServicetest17(){
		List<String> Email=new List<String>();
		List<String> Phone=new List<String>();
		Email.add('Test@test.com');
		Email.add('Test@test.com');
		Phone.add('325647889');
		Phone.add('325647889');
		TalentAddEditReferenceController.callDedupeService('Test','Test2',Email,Phone);
		System.assert(Email!=null); 
        System.assert(Phone!=null); 
    }
    static testMethod void callDedupeServicetest18(){
		List<String> Email=new List<String>();
		List<String> Phone=new List<String>();
		Email.add('Test@test.com');
		Email.add('Test@test.com');
		Phone.add('325647889');
		Phone.add('325647889');
		TalentAddEditReferenceController.callDedupeService('Test','Test2',Email,Phone);
        System.assert(Email!=null); 
        System.assert(Phone!=null); 
    }
    
    static testMethod void findDuplicateRecordstest19(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		//
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=null;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.findDuplicateRecords(masterContact.id);
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
    static testMethod void connectTalentReferencestest20(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		//
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		Talent_Recommendation__c tRecom1 = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=null;
		trecom.Source_System_Id__c=newClientAcc.Id;
		trecom.Id=null;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.connectTalentReferences(tRecom1.Id,masterContact.Id,tRecom,newClientCon.id,RefernceContact,'');
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
    /* static testMethod void connectTalentReferencestest28(){
        Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
        //Client Contact
        Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
        Database.insert(newClientAcc);
        Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
        Database.insert(newClientCon);
        //
        Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
        //trecom.Talent__c= masterAccount.id;
        trecom.Talent_Contact__c = masterContact.id;
       trecom.Recommendation_From__c=null;
      trecom.Source_System_Id__c=newClientAcc.Id;
        insert trecom;
       Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
        Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
         TalentAddEditReferenceController.connectTalentReferences('','',null,newClientCon.id,RefernceContact,'Save');
    }*/
    static testMethod void connectTalentReferencestest21(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		Talent_Recommendation__c tRecom1 = new Talent_Recommendation__c();
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=null;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.connectTalentReferences(tRecom.Id,masterContact.Id,tRecom1,newClientCon.id,RefernceContact,'Save');
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
    static testMethod void connectTalentReferencestest22(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		//
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=newClientCon.id;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.connectTalentReferences(tRecom.Id,masterContact.Id,tRecom,newClientCon.id,RefernceContact,'');
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
	static testMethod void connectTalentReferencestest23(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		//
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=newClientCon.id;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.connectTalentReferences(tRecom.Id,masterContact.Id,tRecom,null,RefernceContact,'');
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    } 
    static testMethod void connectTalentReferencestest24(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		//
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		tRecom.Id=null;
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=newClientCon.id;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.connectTalentReferences(tRecom.Id,masterContact.Id,tRecom,newClientCon.id,RefernceContact,'');
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
    static testMethod void connectTalentReferencestest25(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		//
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		Talent_Recommendation__c tRecom1 = new Talent_Recommendation__c();
		//tRecom.Id=null;
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=newClientCon.id;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.connectTalentReferences(tRecom1.Id,masterContact.Id,tRecom,newClientCon.id,RefernceContact,'');
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
    static testMethod void connectTalentReferencestest26(){
		Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
		Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
		//Client Contact
		Account newClientAcc = new Account(Name='Test2 Client',ShippingStreet = 'Address12 St',ShippingCity='City', ShippingCountry='US');
		Database.insert(newClientAcc);
		Contact newClientCon = new Contact(lastName='Test 2Client',mailingStreet = 'Addres2s St',mailingCity='City',Email='tast1@tal2ent.com', mailingCountry='US',AccountID = newClientAcc.id);
		Database.insert(newClientCon);
		//
		Talent_Recommendation__c tRecom = new Talent_Recommendation__c();
		//Talent_Recommendation__c tRecom1 = new Talent_Recommendation__c();
		tRecom.Id=null;
		//trecom.Talent__c= masterAccount.id;
		trecom.Talent_Contact__c = masterContact.id;
		trecom.Recommendation_From__c=newClientCon.id;
		trecom.Source_System_Id__c=newClientAcc.Id;
		insert trecom;
		Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
		Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
		TalentAddEditReferenceController.connectTalentReferences(tRecom.Id,masterContact.Id,tRecom,newClientCon.id,RefernceContact,'');
		System.assertEquals(trecom.Talent_Contact__c ,masterContact.id);  
    }
    static testMethod  void generateReferenceTypetest27(){
        Account masterAccount = TalentAddEditReferenceController_Test.createTalentAccount();
        Contact masterContact = TalentAddEditReferenceController_Test.createTalentContact(masterAccount.Id);
        Account RefernceAccount = TalentAddEditReferenceController_Test.createRefernceAccount();
        Contact RefernceContact = TalentAddEditReferenceController_Test.createRefernceContact(masterAccount.Id);
        TalentAddEditReferenceController.generateReferenceType(RefernceContact.Id);
		System.assertEquals(masterContact.id ,masterContact.id);  
    }
    static Account createRefernceAccount(){
        List<RecordType> types = [SELECT Id,Name from RecordType where Name = 'Reference' And SobjectType = 'Account' limit 1]; 
        Account newAcc = new Account(Name='TestRefernceAccount', RecordTypeId = types[0].Id);
        Database.insert(newAcc);
		System.assertEquals(types[0].Id ,newAcc.RecordTypeId);  
        return newAcc;
		
    }
    static Contact createRefernceContact(String accountID){
        List<RecordType> types = [SELECT Id,Name from RecordType where Name = 'Reference' And SobjectType = 'Contact'];
        Contact newC = new Contact(Firstname='Test', LastName='Refernce',RecordTypeID=types[0].id
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com'); 
        Database.insert(newC);
		System.assertEquals(types[0].Id ,newC.RecordTypeId);  
        return newC;
		
    }
     static Account createTalentAccount(){
        List<RecordType> types = [SELECT Id,Name from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);     
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP); 
        Database.insert(newAcc);
		System.assertEquals(types[0].Name ,'Talent');   
        return newAcc;
		
    }
    static Contact createTalentContact(String accountID){       
        Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
    
        Database.insert(newC);
		System.assertEquals(newC.FirstName ,'Test'); 
        return newC;
		
    }
}