public class TalentAssesmentController {
    
    //Create a wrapper initialization method 
    @AuraEnabled
    public static Assessment__c initWrapper(){
        Assessment__c assesment = new Assessment__c();
	      return assesment;    
    }
    
    //Create a Method to get piclist values using global describe call
    @AuraEnabled
    public static Map<String, List<String>> getAllPicklistValues(String objectStr, string fld) {
        Map<String, List<String>> objectPicklistValuesMap = new Map<String, List<String>>();
        List<String> lstfields = fld.split(',');
        Integer i = 0;
        for(String fldName : lstfields){
                objectPicklistValuesMap.put('Assessment.'+ fldName, TalentAssesmentController.picklistValues(objectStr, fldName));
                
        }
        return objectPicklistValuesMap;
    }
    public static List<String> picklistValues(String objectStr, string fld){    
        List<String> allOpts = new list < String > ();
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectStr);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values =
        fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
           allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
    }
    @AuraEnabled
    public static String saveTalentAssesment( Assessment__c assessmentObj, String talentContactId, Boolean shouldInsert, String mode ){
        String message = 'success';
        system.debug('assessmentObj-->'+assessmentObj);
        Savepoint sp = Database.setSavepoint();
        try{
            
            if(shouldInsert == true && mode == 'Add'){
                
                if(talentContactId !=  null && talentContactId !=''){
                   // system.debug('talentContactId :'+talentContactId);
                    Contact talentContact = [Select Id, AccountId from Contact where Id =: talentContactId];
                    assessmentObj.Talent__c = talentContact.id;    
                }
                
                insert assessmentObj;
            }
            if(shouldInsert == false && mode != 'Add'){
                update assessmentObj;
            }
            
            return message;
            
        }
        catch(Exception Ex){
            // Rollback all changes
            message = ex.getMessage();
            system.debug(Ex);
            Database.rollback(sp);
            return message;
        }
        
    }
    
    
    @AuraEnabled
    public static Assessment__c getTalentAssessment(Id assessmentId){
        Assessment__c assesment = new Assessment__c();
        assesment =  [select id, Assessment_Name__c, Comments__c,Vendor__c,Assessment_Link__c, Achieved_Score__c,Quiz_Assessment__c, CreatedBy.Name, Assesment_Date__c, CreatedDate, Talent__r.FirstName, Talent__r.LastName  from Assessment__c where id =: assessmentId];
       // system.debug('assmnnt: '+ assesment);
        return assesment;
    }
    
    
    @AuraEnabled
    public static List<Assessment__c> getTalentAssessments(Id contactId){
        
        List<Assessment__c> lstAssessments  =[select id, Assessment_Name__c, Comments__c, Assessment_Link__c,Vendor__c,Quiz_Assessment__c, Achieved_Score__c, CreatedBy.Name, Assesment_Date__c, CreatedDate, Talent__r.FirstName, Talent__r.LastName  from Assessment__c where Talent__c =: contactId +'X' ];
        //system.assert(false, contactId);
        return lstAssessments  ;
    }
    
    @AuraEnabled
    public static boolean deleteTalentAssessment(Id assessmentId){
        
        Assessment__c objAssessments  =[select Id from Assessment__c where id =: assessmentId ];
        try {
            delete objAssessments;
            return true;
        } catch (DmlException e) {
            return false;
        }
    }

     @AuraEnabled
    public static List<Assessment__c> getSortedTalentAssessments(Id contactId, String sortField, Boolean sortAsc) {

         String sortOrder = sortAsc ? ' ASC' : ' DESC';

        String soqlQuery = 'select id, Assessment_Name__c, Comments__c, Assessment_Link__c, Quiz_Assessment__c, Vendor__c,  Max_score__c, Min_score__c, Achieved_Score__c, CreatedBy.Name, Assesment_Date__c, ' +
                           'CreatedDate, Talent__r.FirstName, Talent__r.LastName FROM Assessment__c WHERE Talent__c = ' + 
                           '\'' + contactId + '\'' + ' ORDER BY ' + sortField + ' ' + sortOrder + ' NULLS LAST';
        
        List<Assessment__c> lstAssessments =  Database.query(soqlQuery);

        return lstAssessments  ;
    }
    
    @AuraEnabled
    public static boolean validateQuizAssessmentValue(String quiz , String vendor){
    	List<Global_LOV__c> lov = new List<Global_LOV__c>();
        lov = [select id from Global_lov__c where Text_Value__c =: quiz and Text_Value_2__c =: vendor];
        
        if(lov.size() > 0 ){
            return true;
        }else{
            return false;
        }
	        
    }
    
}