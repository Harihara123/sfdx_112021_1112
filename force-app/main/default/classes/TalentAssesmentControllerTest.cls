@isTest
public class TalentAssesmentControllerTest{
    //Test data inset methods
    public static Account createRefernceAccount(){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        
        
        Account newAcc = new Account(Name='TestAccount', RecordTypeId = types[0].Id);
        
        
        Database.insert(newAcc);
        return newAcc;
    }
    public static Contact createRefernceContact(String accountID){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Contact'];
        
        
        Contact newC = new Contact(Firstname='Test', LastName='Refernce',RecordTypeID=types[0].id
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com');
     
        
        
        Database.insert(newC);
        return newC;
    }
    public static Account createTalentAccount(){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(newAcc);
        return newAcc;
    }
    public static Contact createTalentContact(String accountID){
        Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(newC);
        return newC;
    }
    
    public static testMethod void talentAssesmentPicklistTest(){
        Account masterAccount = TalentAssesmentControllerTest.createTalentAccount();
        Contact masterContact = TalentAssesmentControllerTest.createTalentContact(masterAccount.Id);
        TalentAssesmentController.initWrapper();
        Map<String, List<String>> picklistValueMap = TalentAssesmentController.getAllPicklistValues('Contact', 'LeadSource');
    }
    
    public static testMethod void saveTalentAssesmentAddTest(){
        Account masterAccount = TalentAssesmentControllerTest.createTalentAccount();
        Contact masterContact = TalentAssesmentControllerTest.createTalentContact(masterAccount.Id);
        Assessment__c assessmentObj = new Assessment__c();
        assessmentObj.Assessment_Name__c = 'Test';
        TalentAssesmentController.saveTalentAssesment(assessmentObj, masterContact.Id, true, 'Add');
        TalentAssesmentController.saveTalentAssesment(assessmentObj, masterContact.Id, false, 'Edit');        
        TalentAssesmentController.getTalentAssessments(masterContact.Id);  
        TalentAssesmentController.getTalentAssessment(assessmentObj.Id);  
        TalentAssesmentController.deleteTalentAssessment(assessmentObj.Id); 
        TalentAssesmentController.getSortedTalentAssessments(masterContact.Id, 'Name', true);  
    }
    
    public static testMethod void exceptionTest(){
        Assessment__c assessmentObj = new Assessment__c();
        try{        
            TalentAssesmentController.saveTalentAssesment(assessmentObj, 'hjkasdhjk3434', true, 'Add');  
            TalentAssesmentController.saveTalentAssesment(assessmentObj, 'njdklasdkla13123', false, 'Edit');
            TalentAssesmentController.deleteTalentAssessment('jsdhnjkashdjas'); 
        }
        catch(Exception ex){}    
    }
}